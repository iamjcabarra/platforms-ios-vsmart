//
//  VSCoreDataStack.swift
//  V-Smart
//
//  Created by Ryan Migallos on 27/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import Foundation
import CoreData

// CORE DATA STACK
class VSCoreDataStack  {
    
    fileprivate var store:NSPersistentStore?
    
    fileprivate var masterContext:NSManagedObjectContext!
    fileprivate var mainContext:NSManagedObjectContext!
    fileprivate var workerContext:NSManagedObjectContext!
    
    init(name:String) {
        
        let modelName = "\(name)"
        let sqlfilename = "\(name).sqlite"
        
        // STEP 1
        // Managed object model
        let model = self.coreDataModel(modelName)
        
        // STEP 2
        // Persistent store coordinator
        let psc = NSPersistentStoreCoordinator(managedObjectModel:model)
        
        // STEP 3
        // Managed object model compatibility test
        let url = self.persistenceStoreFile(sqlfilename)!
        let compatible = self.isCompatible(storefile: url, atCoordinator:psc)
        if (compatible == false) {
            self.store = nil
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        // MANAGED OBJECT CONTEXT
        //////////////////////////////////////////////////////////////////////////////////////////
        
        // DATA FLUSHER
        // Core data stack for the master thread
        // [MASTER] -> [PERSISTENTSTORE]
        self.masterContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        self.masterContext.persistentStoreCoordinator = psc
        self.masterContext.mergePolicy = NSMergePolicy(merge: .mergeByPropertyStoreTrumpMergePolicyType)
        self.masterContext.name = "MasterContext"
        
        // UI-RELATED CONTEXT
        // Core data stack for the main thread
        // [MAIN] -> [MASTER]
        self.mainContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        self.mainContext.parent = self.masterContext
        self.mainContext.mergePolicy = NSMergePolicy(merge: .mergeByPropertyStoreTrumpMergePolicyType)
        self.mainContext.name = "MainContext"
        
        // BACKGROUND CONTEXT
        // Core data stack for the worker thread
        // [WORKER] -> [MAIN]
        self.workerContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        self.workerContext.parent = self.mainContext
        self.workerContext.mergePolicy = NSMergePolicy(merge: .mergeByPropertyStoreTrumpMergePolicyType)
        self.workerContext.name = "WorkerContext"

        //////////////////////////////////////////////////////////////////////////////////////////
        // CREATE PERSISTENCE STORE
        //////////////////////////////////////////////////////////////////////////////////////////
        
        let sqliteConfig = ["journal_mode": "DELETE"]
        
        let options:[AnyHashable: Any] = [NSMigratePersistentStoresAutomaticallyOption: true,
                      NSInferMappingModelAutomaticallyOption: true,
                      NSSQLitePragmasOption: sqliteConfig]
        do {
            try self.store = psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at:url, options:options)
            print("Successful loading store...");
        }
        catch let error {
            print("Error loading store... \(error)");
        }
    }
    
    // MARK: - Modeling
    fileprivate func coreDataModel(_ name: String) -> NSManagedObjectModel {
        let bundle = Bundle.main
        let modelURL = bundle.url(forResource: name, withExtension:"momd")
        return NSManagedObjectModel(contentsOf: modelURL!)!
    }
    
    // MARK: - Helper
    fileprivate func applicationStoresDirectory() -> URL? {
        let documentsDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let storesDirectoryPath = (documentsDirectoryPath as NSString).appendingPathComponent("Stores")
        
        let fileManager = FileManager.default
        if (!fileManager.fileExists(atPath: storesDirectoryPath)) {
            do {
                try fileManager.createDirectory(atPath: storesDirectoryPath, withIntermediateDirectories: true, attributes: nil)
                print("Successful creating application stores directory...");
            }
            catch let error {
                print("Error creating application stores directory... \(error)");
                return nil
            }
        }
        
        return URL(fileURLWithPath: storesDirectoryPath)
    }
    
    // MARK: - Store File Path
    fileprivate func persistenceStoreFile(_ name:String) -> URL? {
        if let fileURL = self.applicationStoresDirectory() {
            return fileURL.appendingPathComponent(name)
        }
        return nil
    }
    
    fileprivate func isCompatible(storefile storeURL:URL, atCoordinator coordinator:NSPersistentStoreCoordinator) -> Bool {
        let path = storeURL.path
        
        let fileManager = FileManager.default
        if (fileManager.fileExists(atPath: path)) {
            print("Checking model for compatibility...")
            do {
                let meta = try NSPersistentStoreCoordinator.metadataForPersistentStore(ofType: NSSQLiteStoreType, at: storeURL, options: nil)
                let coordinatorModel = coordinator.managedObjectModel
                if ( coordinatorModel.isConfiguration(withName: nil, compatibleWithStoreMetadata: meta) ) {
                    print("Model is compatible...")
                    return true
                    
                } else {
                    do {
                        try fileManager.removeItem(atPath: path)
                        print("Model is not compatible...")
                        print("Removed file at path \(path)")
                    } catch let error {
                        print("Error removing file at path \(path)... \(error)");
                    }
                }
                
            } catch let error {
                print("Error checking for model compatibility... \(error)");
            }
        }
        print("File does not exist...")
        return false
    }
    
    // MARK: - Saving
    fileprivate func saveContext() -> Bool {
        let ctx = self.workerContext
        return self.saveObjectContext(ctx!)
    }
    
    func saveObjectContext(_ ctx: NSManagedObjectContext) -> Bool {
        var success = false

        ctx.performAndWait {
            do {
                try ctx.save()
                print("Success saving tree context...");
                success = true
            }
            catch let error {
                print("Error saving tree context... \(error)");
            }
        
        if let parentContext = ctx.parent {
                success = self.saveObjectContext(parentContext)
            }
        }
        
        return success
    }
    
    // MARK: - Worker
    
    func retrieveEntity(_ entity: String, context: NSManagedObjectContext, filter:NSPredicate?) -> NSManagedObject {
//        let fetchRequest = NSFetchRequest(entityName: entity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest(entityName: entity)
        
        if let predicate = filter {
            fetchRequest.predicate = predicate
        }
        
        do {
            let items = try context.fetch(fetchRequest)
            //let items = try context.execute(fetchRequest) as [NSManagedObject]
            if (items.count > 0) {
                //                print("data retrieved!")
                return items.last!
            }
        }
        catch let error {
            print("Error retrieving data for entity \(entity): \(error)")
        }
        return NSEntityDescription.insertNewObject(forEntityName: entity, into: context)
    }
    
    func retrieveEntity(_ entity:String, filter:NSPredicate?) -> NSManagedObject? {
//        let fetchRequest = NSFetchRequest.init(entityName: entity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        if let predicate = filter {
            fetchRequest.predicate = predicate
        }
        
        do {
            let items = try self.workerContext.fetch(fetchRequest)
            if (items.count > 0) {
                return items.last
            }
        } catch let error {
            print("Error retrieving data for entity \(entity): \(error)")
        }
        
        return nil
    }
    
    func retrieveEntity(_ entity: String, properties: [String], filter: NSPredicate?, sortKey: String?, ascending: Bool = true) -> [Any]? {
        
//        let fetchRequest = NSFetchRequest(entityName: entity)
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.resultType = .dictionaryResultType
        fetchRequest.returnsDistinctResults = true
        fetchRequest.propertiesToFetch = properties
        
        if let predicate = filter {
            fetchRequest.predicate = predicate
        }
        
        if let key = sortKey {
            let selector = #selector(NSString.localizedStandardCompare(_:))
            let descriptor = NSSortDescriptor.init(key: key, ascending: ascending, selector:selector)
            fetchRequest.sortDescriptors = [descriptor]
        }
        
        do {
            let items = try workerContext.fetch(fetchRequest)
            print("Retrieved entity properties ---> \(items)")
            return items
        }
        catch let error {
            print("Error fetching data for entity \(entity): \(error)")
        }
        
        return nil
    }
    
    func retrieveObjects(forEntity entity:String, withFilter:NSPredicate? = nil) -> [NSManagedObject]? {
//        let fetchRequest = NSFetchRequest(entityName: entity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        if let predicate = withFilter {
            fetchRequest.predicate = predicate
        }
        
        do {
            let items = try self.workerContext.fetch(fetchRequest)
            if (items.count > 0) {
                return items
            }
        } catch let error {
            print("Error retrieving data for entity \(entity): \(error)")
        }
        
        return nil
    }
    
    func retrieveObjects(forEntity entity: String, predicate: NSPredicate? = nil, sortDescriptor: NSSortDescriptor?) -> [NSManagedObject]? {
//        let fetchRequest = NSFetchRequest(entityName: entity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        
        if let p = predicate {
            fetchRequest.predicate = p
        }
        
        if let s = sortDescriptor {
            fetchRequest.sortDescriptors = [s]
        }
        
        do {
            let items = try self.workerContext.fetch(fetchRequest)
            if (items.count > 0) {
                return items
            }
        } catch let error {
            print("Error retrieving data for entity \(entity): \(error)")
        }
        
        return nil
    }
    
    func clearEntity(_ entity: String, ctx: NSManagedObjectContext, filter: NSPredicate?) -> Bool {
        
        var success = false
        
//        let fetchRequest = NSFetchRequest(entityName: entity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        
        if let predicate = filter {
            fetchRequest.predicate = predicate
        }
        
        ctx.performAndWait {
            do {
                let items = try ctx.fetch(fetchRequest)
                for mo in items {
                    ctx.delete(mo)
                }
                
            }
            catch let error {
                print("Error clearing data for entity \(entity): \(error)")
            }
            
            success = self.saveObjectContext(ctx)
        }
        
        //////////////////////////////////////////
        // iOS 9 Batch Delete Request
        //////////////////////////////////////////
        // Create Batch Delete Request
        //        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        //
        //        do {
        //            try ctx.executeRequest(batchDeleteRequest)
        //            success = true
        //        } catch {
        //            print("Error clearing data for entity \(entity): \(error)")
        //            success = false
        //        }
        
        return success
    }
    
    func updateEntity(_ name:String, predicate:NSPredicate, dictionary:[String:AnyObject]) -> Bool {
        guard let mo = self.retrieveEntity(name, filter: predicate) else {
            return false
        }
        let keys = Array(dictionary.keys)
        for k in keys {
            mo.setValue(dictionary[k], forKey: k)
        }
        return saveObjectContext(mo.managedObjectContext!)
    }
    
    func updateObjectsForEntity(_ entity: String, predicate: NSPredicate, dictionary: [String: AnyObject]) -> Bool {
        guard let objects = self.retrieveObjects(forEntity: entity, withFilter: predicate) else {
            return false
        }
        
        var success = false
        let keys = Array(dictionary.keys)
        
        for mo in objects {
            for k in keys {
                mo.setValue(dictionary[k], forKey: k)
            }
            
            success = self.saveObjectContext(mo.managedObjectContext!)
        }
        
        return success
    }
    
    // MARK: - Data Filters
    func createPredicate(key k:String, withValue value:String, isExact:Bool=false) -> NSPredicate {
        let leftExpression = NSExpression(forKeyPath: k)
        let rightExpression = NSExpression(forConstantValue: value)
        let options:NSComparisonPredicate.Options = [.diacriticInsensitive, .caseInsensitive]
        
        let type:NSComparisonPredicate.Operator = (isExact) ? .equalTo : .contains
        
        let predicate = NSComparisonPredicate(leftExpression: leftExpression,
                                              rightExpression: rightExpression,
                                              modifier: .direct,
                                              type: type,
                                              options: options)
        return predicate
    }
    
    func objectContext() -> NSManagedObjectContext! {
        return self.workerContext
    }
    
    func objectMainContext() -> NSManagedObjectContext! {
        return self.mainContext
    }
    
    func displayRecords(_ entity:String) {
//        let request = NSFetchRequest(entityName: entity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        
        do {
            let items = try self.workerContext!.fetch(fetchRequest)
            if (items.count > 0) {
                for mo in items {
                    print("---")
                    for k in mo.entity.propertiesByName.keys {
                        if let v = mo.value(forKey: k) {
                            print("MO DEBUG [\(k)] = \(v)")
                        }
                    }
                }
            }
        } catch let error {
            print("error : \(error)")
        }
    }
}

/////////////////////
//// EXETENSIONS ////
/////////////////////

extension NSComparisonPredicate {
    
    convenience init(keyPath k: String, withValue value: Any!, isExact:Bool=false) {
        let leftExpression = NSExpression(forKeyPath: k)
        let rightExpression = NSExpression(forConstantValue: value)
        let options:NSComparisonPredicate.Options = [.diacriticInsensitive, .caseInsensitive]
        
        let type:NSComparisonPredicate.Operator = (isExact) ? .equalTo : .contains
        
        self.init(leftExpression: leftExpression,
                  rightExpression: rightExpression,
                  modifier: .direct,
                  type: type,
                  options: options)
    }
}



