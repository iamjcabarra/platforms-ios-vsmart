//
//  VSNetworkRequestHelper.swift
//  V-Smart
//
//  Created by Julius Abarra on 28/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

class CPMNetworkRequestHelper {
    
    var isProductionMode = true
    
    // MARK: - App Default URLs
    
    func appBaseURL() -> String {
        if let server = UserDefaults.standard.string(forKey: "baseurl_preference") {
            return server.trimmingCharacters(in: CharacterSet.whitespaces)
        }
        
        return ""
    }
    
    func appServiceURL() -> String {
        if let server = UserDefaults.standard.string(forKey: "serviceurl_preference") {
            return server.trimmingCharacters(in: CharacterSet.whitespaces)
        }
        
        return ""
    }
    
    func appBookURL() -> String {
        if let server = UserDefaults.standard.string(forKey: "bookurl_preference") {
            return server.trimmingCharacters(in: CharacterSet.whitespaces)
        }
        
        return ""
    }
    
    // MARK: - Internal Methods
    
    func buildRequestURLFromEndPoint(_ endPoint: String) -> URL? {
        prettyFunction()
        
        var path = "http://\(appBaseURL())\(endPoint)" as NSString
        
        if (path.range(of: "vsmart-rest-dev").length > 0) {
            if (isProductionMode) {
                path = path.replacingOccurrences(of: appBaseURL(), with: appServiceURL()) as NSString
                path = path.replacingOccurrences(of: "vsmart-rest-dev", with: "") as NSString
                path = path.replacingOccurrences(of: "//v1", with: "/v1") as NSString
                path = path.replacingOccurrences(of: "//v2", with: "/v2") as NSString
                path = path.replacingOccurrences(of: "http:/v", with: "http://v") as NSString
            }
        }

        if let url = URL(string: path as String) {
            print("Built url: \(url)")
            return url
        }
        
        return nil
    }
    
    func buildURLRequestForMethod(_ method: String, url: URL, body: AnyObject?) -> NSMutableURLRequest {
        prettyFunction()
        
        let request = NSMutableURLRequest.init(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = method
        
        if let postBody = body {
            do {
                let data = try JSONSerialization.data(withJSONObject: postBody, options: JSONSerialization.WritingOptions(rawValue: 0))
                request.httpBody = data
                
                if let jsonString = NSString.init(data: data, encoding: String.Encoding.utf8.rawValue) {
                    print("jsonString: <start>----\(jsonString)---<end>")
                }
            }
            catch let error {
                print("Error building url request: \(error)")
            }
        }
        
        return request
    }
    
    // MARK: - Debugging Helper
    
    fileprivate func prettyFunction(_ file: NSString = #file, function: String = #function, line: Int = #line) {
        print("<start>--- file: \(file.lastPathComponent) function:\(function) line:\(line) ---<end>")
    }
}
