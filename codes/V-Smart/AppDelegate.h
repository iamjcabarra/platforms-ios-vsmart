//
//  AppDelegate.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/14/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IdeaPadViewController.h"
#import "SSCalendarViewController.h"
#import "SSPeopleViewController.h"
#import "UserProfileViewController.h"
#import "DirectoryWatcher.h"
#import "SSSettingsViewController.h"

@class ViewController;
@class ResourceManager;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAppearanceContainer>
{
    DirectoryWatcher *directoryWatcher;
    NSTimer *directoryWatcherTimer;
}

@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;
@property (strong, nonatomic) IdeaPadViewController *ideaPadViewController;
@property (strong, nonatomic) SSCalendarViewController *calendarViewController;
@property (strong, nonatomic) SSPeopleViewController *peopleViewController;
@property (strong, nonatomic) UserProfileViewController *userProfileViewController;
@property (strong, nonatomic) SSSettingsViewController *settingsViewController;
+ (ResourceManager *)resourceInstance;
@end
