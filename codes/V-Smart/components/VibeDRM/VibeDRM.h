//
//  VibeDRM.h
//  VibeDRM
//
//  Created by Earljon Hidalgo on 3/20/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>

@interface VibeDRM : NSObject
{
    NSString* cipherKey;
}

@property (retain) NSString* cipherKey;

- (VibeDRM *) initWithKey:(NSString *) key;

- (NSData *) decrypt:(NSData *) cipherText;

- (NSData *) transform:(CCOperation) encryptOrDecrypt data:(NSData *) inputData;

+ (NSData *) md5:(NSString *) stringToHash;
@end
