
//
//  UICollectionViewLeftAlignedLayout.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 25/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//
//http://stackoverflow.com/questions/13017257/how-do-you-determine-spacing-between-cells-in-uicollectionview-flowlayout/15554667#15554667

#import <UIKit/UIKit.h>

@interface UICollectionViewLeftAlignedLayout : UICollectionViewFlowLayout

@end