//
//  ELCSlider.h
//  ELCSiderController
//
//  Created by Collin Ruffenach on 10/27/10.
//  Copyright 2010 ELC Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SliderValueViewController.h"
#import "Chapter.h"

@protocol ELCSliderDelegate <NSObject>
@required
-(void) didLetGoSliderKnob:(int)bookIndex;
@end

@interface ELCSlider : UISlider {

	UIPopoverController *popoverController;
	SliderValueViewController *sliderValueController;
    id<ELCSliderDelegate> delegate;
    
    int currentIndex;
}

@property (nonatomic, retain) NSArray *chapters;
@property (retain) id delegate;
@end
