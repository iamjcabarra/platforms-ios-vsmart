
#import <UIKit/UIKit.h>


@interface SliderValueViewController : UIViewController {

	IBOutlet UILabel *sliderValue;
}

@property (nonatomic, retain) IBOutlet UILabel *sliderValue;

-(void)updateSliderValueTo:(CGFloat)_value;
-(void)updateSliderValueTextTo:(NSString *) _textValue;
@end
