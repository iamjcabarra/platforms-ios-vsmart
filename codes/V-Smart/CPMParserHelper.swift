//
//  CPMParserHelper.swift
//  V-Smart
//
//  Created by Julius Abarra on 28/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import Foundation

class CPMParserHelper: NSObject {
    
    func parseResponseData(_ data: Data) -> AnyObject? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as AnyObject?
        }
        catch let error {
            print("Error parsing response data: \(error)")
        }
        
        return nil
    }
    
    func okayToParseResponse(_ response: NSDictionary?) -> Bool {
        if let r = response {
            print("Response: \(r)")
            
            if let parseMeta = r["_meta"] as? [String: AnyObject] {
                if let status = parseMeta["status"] as? String {
                    if (status.lowercased() == "success") {
                        return true
                    }
                }
            }
        }
        
        return false
    }
    
    func stringObject(_ object: AnyObject?) -> String {
        guard let o = object else {
            return ""
        }
        
        var string = "\(o)"
        
        if (string == "null" || string == "<null>" || string == "(null)") {
            string = ""
        }

        return string
    }

}
