//
//  UIViewController+Popup.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 8/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    PopupViewAnimationSlideBottomTop = 1,
    PopupViewAnimationSlideRightLeft,
    PopupViewAnimationSlideLeftRight,
    PopupViewAnimationSlideBottomBottom,
    PopupViewAnimationFade
} PopupViewAnimation;

@interface UIViewController (Popup)
-(void)presentPopupViewController:(UIViewController*)popupViewController animationType:(PopupViewAnimation)animationType;
-(void)presentPopupViewControllerWithKeyboardHeight:(UIViewController*)popupViewController animationType:(PopupViewAnimation)animationType;
-(void)dismissPopupViewControllerWithanimationType:(PopupViewAnimation)animationType;

@end