//
//  CALayer+Additions.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 9/11/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (Additions)
+ (id)maskLayerWithCorners:(UIRectCorner)corners radii:(CGSize)radii frame:(CGRect)frame;
@end
