//
//  NSString+Additions.h
//
//  Created by Earljon Hidalgo on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)
- (BOOL) isIn: (NSString *) strings, ... NS_REQUIRES_NIL_TERMINATION;
- (NSString *) stringByDecodingXMLEntities;
- (NSString *) stringByTruncatingText:(int) length;
- (NSString *) randomString;
- (NSString *) randomNumber;

- (NSDate*) dateFromString;
- (CGSize) sizeWithDefaultFont:(UIFont *)fontToUse;
@end
