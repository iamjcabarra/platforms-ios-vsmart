
#import <Foundation/Foundation.h>

@interface UIWebView (SearchWebView)

- (NSInteger) highlightAllOccurencesOfString:(NSString*)str;
-(void) removeAllHighlights;
-(void) highlightSelectedStrings;
-(void) removeSelectedHighlights;
@end