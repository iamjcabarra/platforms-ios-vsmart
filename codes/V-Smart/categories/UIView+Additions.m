//
//  UIView+Additions.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/16/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "UIView+Additions.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (Additions)
- (UIImage *)imageRepresentation {
    UIGraphicsBeginImageContext(self.bounds.size);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


-(void)hide {
    self.alpha = 0.0f;
}


-(void)show {
    self.alpha = 1.0f;
}

- (void)fadeOut:(float)duration {
//    UIView *view = self;
    self.alpha = 0.0f;
//    [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
//        view.alpha = 0.0f;
//    } completion:nil];
}

- (void)fadeIn:(float)duration {
//    UIView *view = self;
//        view.alpha = 1.0f;
//    UIView *view = self;
    self.alpha = 1.0f;
//    [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
//        view.alpha = 1.0f;
//    } completion:nil];
}

-(void)fadeOut {
//    UIView *view = self;
//    view.alpha = 0.0f;
//    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
//        view.alpha = 0.0f;
//    } completion:nil];
    [self fadeOut: 0.2];
}

-(void)fadeIn {
//    UIView *view = self;
//    view.alpha = 1.0f;
//    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
//        view.alpha = 1.0f;
//    } completion:nil];
    [self fadeIn:0.2];
}

-(void)fadeOutAndRemoveFromSuperview {
    UIView *view = self;
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
}


- (NSArray *)superviews {
    NSMutableArray *superviews = [[NSMutableArray alloc] init];
    
    UIView *view = self;
    UIView *superview = nil;
    while (view) {
        superview = [view superview];
        if (!superview) {
            break;
        }
        
        [superviews addObject:superview];
        view = superview;
    }
    
    return superviews;
}

- (id)firstSuperviewOfClass:(Class)superviewClass {
    for (UIView *view = [self superview]; view != nil; view = [view superview]) {
        if ([view isKindOfClass:superviewClass]) {
            return view;
        }	
    }
    return nil;
}

-(void) shake {
    CGAffineTransform leftShake = CGAffineTransformMakeTranslation(-5, 0);
    CGAffineTransform rightShake = CGAffineTransformMakeTranslation(5, 0);
    
    //    view.transform = leftShake;  // starting point
    //
    //    [UIView beginAnimations:@"shake_button" context:(__bridge void *)(view)];
    //    [UIView setAnimationRepeatAutoreverses:YES]; // important
    //    [UIView setAnimationRepeatCount:5];
    //    [UIView setAnimationDuration:0.06];
    //    [UIView setAnimationDelegate:self];
    //    [UIView setAnimationDidStopSelector:@selector(shakeEnded:finished:context:)];
    //
    //    view.transform = rightShake; // end here & auto-reverse
    //
    //    [UIView commitAnimations];
    
    [UIView animateWithDuration:0.06f animations:^{
        [UIView setAnimationRepeatAutoreverses:YES]; // important
        [UIView setAnimationRepeatCount:5];
        self.transform = leftShake;
        self.transform = rightShake;
    } completion:^(BOOL finished) {
        if (finished) {
            self.transform = CGAffineTransformIdentity;
        }
    }];
}

- (CGFloat)top {
    return self.frame.origin.y;
}

- (void)setTop:(CGFloat)top {
    CGRect frame = self.frame;
    frame.origin.y = top;
    self.frame = frame;
}

- (CGFloat)bottom {
    return self.top + self.height;
}

- (void)setBottom:(CGFloat)bottom {
    self.top = bottom - self.height;
}

- (CGFloat)left {
    return self.frame.origin.x;
}

- (void)setLeft:(CGFloat)left {
    CGRect frame = self.frame;
    frame.origin.x = left;
    self.frame = frame;
}

- (CGFloat)right {
    return self.left + self.width;
}

- (void)setRight:(CGFloat)right {
    self.left = right - self.width;
}

- (CGFloat)centerX {
    return self.center.x;
}

- (void)setCenterX:(CGFloat)centerX {
    self.center = CGPointMake(centerX, self.centerY);
}

- (CGFloat)centerY {
    return self.center.y;
}

- (void)setCenterY:(CGFloat)centerY {
    self.center = CGPointMake(self.centerX, centerY);
}

- (CGFloat)height {
    return self.frame.size.height;
}

- (void)setHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGFloat)width {
    return self.frame.size.width;
}

- (void)setWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

@end
