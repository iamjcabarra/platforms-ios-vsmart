//
//  NSMutableArray+Shuffle.h
//  Booklatan
//
//  Created by Earljon Hidalgo on 11/26/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Shuffle)
-(void)shuffle;
-(void)shuffleV21;
@end
