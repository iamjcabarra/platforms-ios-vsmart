//
//  UIColor+MoreColors.m
//  Booklatan
//
//  Created by Earljon Hidalgo on 5/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIColor+MoreColors.h"

#define vendColor(r, g, b) static UIColor *ret; if (ret == nil) ret = [[UIColor colorWithRed:(CGFloat)r/255.0 green:(CGFloat)g/255.0 blue:(CGFloat)b/255.0 alpha:1.0] retain]; return ret

@implementation UIColor (MoreColors)
+ (id)aliceBlue                                         {vendColor(240, 248, 255);}
+ (id)alizarin                                          {vendColor(227, 38, 54);}
+ (id)amaranth                                          {vendColor(229, 43, 80);}
+ (id)amber                                             {vendColor(255, 191, 0);}
+ (id)amethyst                                          {vendColor(153, 102, 204);}
+ (id)apricot                                           {vendColor(251, 206, 177);}
+ (id)aqua                                              {vendColor(0, 255, 255);}
+ (id)aquamarine                                        {vendColor(127, 255, 212);}
+ (id)armyGreen                                         {vendColor(75, 83, 32);}
+ (id)asparagus                                         {vendColor(123, 160, 91);}
+ (id)atomicTangerine                                   {vendColor(255, 153, 102);}
+ (id)auburn                                            {vendColor(111, 53, 26);}
+ (id)azure                                             {vendColor(0, 127, 255);}
+ (id)blueViolet                                        {vendColor(138, 43, 226);}
+ (id)pink                                              {vendColor(255, 192, 203);}
+ (id)indigo                                            {vendColor(0, 65, 106);}
+ (id)brown                                             {vendColor(150, 75, 0);}
@end
