//
//  NSString+Additions.m
//
//  Created by Earljon Hidalgo on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#import "NSString+Additions.h"

@implementation NSString (Additions)

- (BOOL) isIn: (NSString *) strings, ...
{
    BOOL isFound = NO;
    
    va_list args;
    va_start(args, strings);
    for (NSString *arg = strings; arg != nil; arg = va_arg(args, NSString*))
    {
        if ([self caseInsensitiveCompare:arg] == NSOrderedSame) {
            isFound = YES;
            break;
        }
    }
    va_end(args);
    
    return isFound;
}

- (NSString *) stringByDecodingXMLEntities {
    NSUInteger myLength = [self length];
    NSUInteger ampIndex = [self rangeOfString:@"&" options:NSLiteralSearch].location;
    
    // Short-circuit if there are no ampersands.
    if (ampIndex == NSNotFound) {
        return self;
    }
    // Make result string with some extra capacity.
    NSMutableString *result = [NSMutableString stringWithCapacity:(myLength * 1.25)];
    
    // First iteration doesn't need to scan to & since we did that already, but for code simplicity's sake we'll do it again with the scanner.
    NSScanner *scanner = [NSScanner scannerWithString:self];
    do {
        // Scan up to the next entity or the end of the string.
        NSString *nonEntityString;
        if ([scanner scanUpToString:@"&" intoString:&nonEntityString]) {
            [result appendString:nonEntityString];
        }
        if ([scanner isAtEnd]) {
            goto finish;
        }
        // Scan either a HTML or numeric character entity reference.
        if ([scanner scanString:@"&amp;" intoString:NULL])
            [result appendString:@"&"];
        else if ([scanner scanString:@"&apos;" intoString:NULL])
            [result appendString:@"'"];
        else if ([scanner scanString:@"&quot;" intoString:NULL])
            [result appendString:@"\""];
        else if ([scanner scanString:@"&lt;" intoString:NULL])
            [result appendString:@"<"];
        else if ([scanner scanString:@"&gt;" intoString:NULL])
            [result appendString:@">"];
		else if ([scanner scanString:@"&#174;" intoString:NULL])
			[result appendString:@"®"];
		else if ([scanner scanString:@"&#169;" intoString:NULL])
			[result appendString:@"©"];
		else if ([scanner scanString:@"&#8482;" intoString:NULL])
			[result appendString:@"™"];
        else if ([scanner scanString:@"&#" intoString:NULL]) {
            BOOL gotNumber;
            unsigned charCode;
            NSString *xForHex = @"";
            
            // Is it hex or decimal?
            if ([scanner scanString:@"x" intoString:&xForHex]) {
                gotNumber = [scanner scanHexInt:&charCode];
            }
            else {
                gotNumber = [scanner scanInt:(int*)&charCode];
            }
            if (gotNumber) {
                [result appendFormat:@"%u", charCode];
            }
            else {
                NSString *unknownEntity = @"";
                [scanner scanUpToString:@";" intoString:&unknownEntity];
                [result appendFormat:@"&#%@%@;", xForHex, unknownEntity];
                //Log(@"Expected numeric character entity but got &#%@%@;", xForHex, unknownEntity);
            }
            [scanner scanString:@";" intoString:NULL];
        }
        else {
            NSString *unknownEntity = @"";
            [scanner scanUpToString:@";" intoString:&unknownEntity];
            NSString *semicolon = @"";
            [scanner scanString:@";" intoString:&semicolon];
            [result appendFormat:@"%@%@", unknownEntity, semicolon];
            //Log(@"Unsupported XML character entity %@%@", unknownEntity, semicolon);
        }
    }
    while (![scanner isAtEnd]);
    
finish:
    return result;
}

- (NSString *) randomString {
    return [NSString stringWithFormat:@"%c%c%c%c%c",
            (char)(65 + (arc4random() % 25)),
            (char)(48 + (arc4random() % 9)),
            (char)(65 + (arc4random() % 25)),
            (char)(48 + (arc4random() % 35)),
            (char)(65 + (arc4random() % 15))];
}

- (NSString *) randomNumber {
    return [NSString stringWithFormat:@"0%i%i%i%i%i",
            arc4random() % 9,
            arc4random() % 9,
            arc4random() % 9,
            arc4random() % 9,
            arc4random() % 9];
}

- (NSDate*) dateFromString{
    NSString *string = [NSString stringWithString:self];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    string = [string stringByReplacingOccurrencesOfString:@":" withString:@""]; // this is such an ugly code, is this the only way?
//    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *returnDate = [dateFormatter dateFromString: string];
    
    return returnDate;
}

- (NSString *) stringByTruncatingText:(int) length {
    NSRange stringRange = {0, MIN([self length], length)};
    stringRange = [self rangeOfComposedCharacterSequencesForRange:stringRange];
    NSString *shortString = [self substringWithRange:stringRange];
    return shortString;
}

- (CGSize) sizeWithDefaultFont:(UIFont *)fontToUse {
    if ([self respondsToSelector:@selector(sizeWithAttributes:)])
    {
        NSDictionary* attribs = @{NSFontAttributeName:fontToUse};
        return ([self sizeWithAttributes:attribs]);
    }
    // still running iOS 6, ugh!
    return ([self sizeWithFont:fontToUse]);
}

@end
