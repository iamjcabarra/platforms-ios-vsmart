//
//  UIButton+Additions.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/16/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Additions)

+ (UIButton *) buttonWithLabel:(NSString *)buttonName forTarget:(id) target withAction:(SEL) selector;
+ (UIButton *) buttonWithLabel:(NSString *)buttonName inFrame:(CGRect) frame forTarget:(id) target withAction:(SEL) selector;
+ (UIButton *) buttonWithImageNamed:(NSString *)imageName;
+ (UIButton *) buttonWithImage:(UIImage *)image inFrame:(CGRect) frame forTarget:(id) target withAction:(SEL) selector;
+ (UIButton *) buttonWithImage:(UIImage *)image forTarget:(id) target withAction:(SEL) selector;
@end
