//
//  UIImageView+Additions.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 9/11/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "UIImageView+Additions.h"
#import "CALayer+Additions.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIImageView (Additions)

-(void)maskRoundCorners:(UIRectCorner)corners radius:(CGFloat)radius {
    // To round all corners, we can just set the radius on the layer
    if ( corners == UIRectCornerAllCorners ) {
        self.layer.cornerRadius = radius;
        self.layer.masksToBounds = YES;
    } else {
        // If we want to choose which corners we want to mask then
        // it is necessary to create a mask layer.
        self.layer.mask = [CALayer maskLayerWithCorners:corners radii:CGSizeMake(radius, radius) frame:self.bounds];
    }
}

-(void) applyShadowBorder {
    [self.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.layer setShadowOpacity:0.7f];
    [self.layer setShadowRadius:4.0f];
    [self.layer setShadowOffset:CGSizeMake(0, 10)];
    [self setClipsToBounds:NO];
}

@end
