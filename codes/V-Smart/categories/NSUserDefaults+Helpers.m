//
//  NSUserDefaults+Helpers.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/31/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "NSUserDefaults+Helpers.h"

@implementation NSUserDefaults (Helpers)
-(void) setCustomObject: (id) object forKey: (NSString *) key {
    if ([object respondsToSelector:@selector(encodeWithCoder:)] == NO) {
        VLog(@"Error save object to NSUserDefaults. Object must respond to encodeWithCoder: message");
        return;
    }
    
    if ([key isEqualToString:@"GLOBAL_ACCOUNT"])
        VLog(@"saveArchive - %@", key);

    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    //VLog(@"Encoded: %@", encodedObject);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
}

- (id) getCustomObjectForKey: (NSString *) key {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    id obj = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return obj;
}
@end
