//
//  UIColor+MoreColors.h
//  Booklatan
//
//  Created by Earljon Hidalgo on 5/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MoreColors)
+ (id)aliceBlue;
+ (id)alizarin;
+ (id)amaranth;
+ (id)amber;
+ (id)amethyst;
+ (id)apricot;
+ (id)aqua;
+ (id)aquamarine;
+ (id)armyGreen;
+ (id)asparagus;
+ (id)atomicTangerine;
+ (id)auburn;
+ (id)azure;
+ (id)blueViolet;
+ (id)pink;
+ (id)indigo;
+ (id)brown;
@end
