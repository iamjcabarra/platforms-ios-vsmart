//
//  NSArray+Shuffle.m
//  Booklatan
//
//  Created by Earljon Hidalgo on 5/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSArray+Shuffle.h"

@implementation NSArray (Shuffle)

-(NSArray *)shuffledArray
{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:[self count]];
    
    NSMutableArray *copy = [self mutableCopy];
    while ([copy count] > 0)
    {
        int index = arc4random() % [copy count];
        id objectToMove = [copy objectAtIndex:index];
        [array addObject:objectToMove];
        [copy removeObjectAtIndex:index];
    }
    
    //[copy release];
    return array;
}
@end
