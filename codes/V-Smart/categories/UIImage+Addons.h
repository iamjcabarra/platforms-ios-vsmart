//
//  UIImage+Addons.h
//  Booklatan
//
//  Created by Earljon Hidalgo on 11/10/11.
//  Copyright (c) 2011 Vibal Foundadtion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Addons)
+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
- (UIImage*)scaleImageToSize:(CGSize)newSize;
//+ (UIImage*)imageWithShadow: (UIImage *) img;
- (UIImage*) imageToGrayscale;
- (UIImage *) overlayImage:(UIImage*) topImage atPoint:(CGPoint) point;
- (UIImage*)imageWithShadow;
- (UIImage *)imageNamed:(NSString *)name withColor:(UIColor *)color;

+ (UIImage *)imageFromPDF:(NSURL *)filePath;
+ (UIImage *)imageFromPDF:(NSURL *)filePath pass:(NSString *)key;
+ (UIImage *)imageNamed:(NSString *)name withColor:(UIColor *)color;
- (BOOL)imageIsEqualTo:(UIImage *)image2;
@end
