//
//  UIWebView+Private.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 3/30/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "UIWebView+Private.h"

@implementation UIWebView (Private)
- (NSString *) fetchAllHighlights {
    NSString *jsonResult = [self evalJS:@"vibeGetHighlights()"];
    return jsonResult;
}

- (void) removeHighlights {
    [self evalJS:@"vibeRemoveAllHighlights()"];
}

- (void) applyHighlightForData:(NSString *) range {
    [self evalJS:[NSString stringWithFormat:@"vibeHighlightStoredData('%@')", range]];
}

- (NSString *) undoHighlightForSelection {
    NSString *selectedRange = [self evalJS:@"vibeUnhighlightSelected()"];
    return selectedRange;
}

- (NSString *) applyHighlightForSelection: (NSString *) cssSuffix {
    NSString *highlightSelection = [NSString stringWithFormat:@"vibeHighlightSelected('vibeHighlight%@')", cssSuffix];
    NSString *selectedRange = [self evalJS:highlightSelection];
    return selectedRange;
}

- (NSInteger)highlightAllOccurencesOfString:(NSString*)str {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"SearchWebView" ofType:@"js"];
    NSString *jsCode = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    [self stringByEvaluatingJavaScriptFromString:jsCode];
    
    NSString *startSearch = [NSString stringWithFormat:@"MyApp_HighlightAllOccurencesOfString('%@');",str];
    [self stringByEvaluatingJavaScriptFromString:startSearch];
    
    //    VLog(@"%@", [self stringByEvaluatingJavaScriptFromString:@"console"]);
    return [[self stringByEvaluatingJavaScriptFromString:@"MyApp_SearchResultCount;"] intValue];
}

- (void)removeAllHighlights {
    [self stringByEvaluatingJavaScriptFromString:@"MyApp_RemoveAllHighlights()"];
}

- (void) highlightSelectedStrings
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"SearchWebView" ofType:@"js"];
    NSString *jsCode = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    [self stringByEvaluatingJavaScriptFromString:jsCode];
    
    NSString *startSearch   = [NSString stringWithFormat:@"MyApp_StylizeHighlightedString()"];
    [self stringByEvaluatingJavaScriptFromString:startSearch];
}

- (void) removeSelectedHighlights {
    [self stringByEvaluatingJavaScriptFromString:@"MyApp_RemoveCurrentHighlights()"];
}

- (int) getPageIndex: (int) startRange andEndRange:(int) endRange forWebviewWidth:(int) width {
    NSString *query = [NSString stringWithFormat:@"vibeFindPage(%d, %d, %d)", startRange, endRange, width];
    //VLog(@"QUERY: %@", query);
    
    int result = [[self evalJS:query] intValue];
    
    return result;
}

- (int) getPageIndex: (NSString *) elementIdOrPath forWebviewWidth: (int) width forEncyclopedia:(BOOL) isEncyclopediaEntry {
    NSString *query = [NSString stringWithFormat:@"vibeFindPageNumberForEncyclopedia('%@', %d)", elementIdOrPath, width];
    
    if (!isEncyclopediaEntry) {
        query = [NSString stringWithFormat:@"vibeFindPageNumber('%@', %d)", elementIdOrPath, width];
    }
    int pageIndex = [[self evalJS:query] intValue];
    return pageIndex;
}

- (int) getPageIndex: (NSString *) elementId forWebviewWidth: (int) width {
    NSString *query = [NSString stringWithFormat:@"vibeFindPageNumber('%@', %d)", elementId, width];
    int pageIndex = [[self evalJS:query] intValue];
    return pageIndex;
}

- (void) applyExerciseSecurityForJson: (NSString *) jsonString isTeacher:(BOOL)isTeacher{
//    jsonString = @"[{\"exer_id\":\"exer1\",\"isOpened\":0}]";
    
//    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    
//    jsonString = @"[{  \"isOpened\" : 1,  \"exer_id\" : \"exer1\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer2\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer3\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer4\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer5\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer6\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer7\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer8\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer9\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer10\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer11\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer12\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer13\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer14\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer15\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer16\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer17\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer18\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer19\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer20\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer21\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer22\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer23\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer24\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer25\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer26\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer27\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer28\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer29\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer30\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer31\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer32\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer33\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer34\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer35\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer36\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer37\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer38\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer39\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer40\"},{  \"isOpened\" : 0,  \"exer_id\" : \"exer41\"}]";
    
    if (isTeacher) {
        [self evalJS:[NSString stringWithFormat:@"lockExercise('%@','%@')", jsonString, @"true" ]];
    } else {
        [self evalJS:[NSString stringWithFormat:@"lockExercise('%@','%@')", jsonString, @"false" ]];
    }
}

- (void) applyExerciseAnswersForJson: (NSString *) jsonString {
    [self evalJS:[NSString stringWithFormat:@"setJsonAnswer('%@')", jsonString]];
}

- (NSString *) fetchExerciseAnswers {
    NSString *result = [self evalJS:@"vibeGetBookAnswers()"];
    return result;
}

- (void) applyPageTheme: (NSString *) themeName {
    [self evalJS:[NSString stringWithFormat:@"applyColor('%@')", themeName]];
}

- (void) applyFontFamily: (NSString *) fontFamily {
    [self evalJS:[NSString stringWithFormat:@"applyFontFamily('%@')", fontFamily]];
}

- (void) applySmoothTransitionEffect {
    NSString *bodyAnimation = [NSString stringWithFormat:@"addCSSRule('body', '-webkit-transition: background-color ease .80s;font-size ease 1s;-webkit-font-smoothing:antialiased;')"];
    [self evalJS:bodyAnimation];
}

- (void) applyCustomStyles {
    [self stringByEvaluatingJavaScriptFromString:[self _formatCSS:@"highlight" withColor:@"yellow"]];
    // For searching text
    [self stringByEvaluatingJavaScriptFromString:[self _formatCSS:@"searchHighlight" withColor:@"yellow"]];
    
    NSString *cssStyle = [NSString stringWithFormat:@"addCSSRule('.%@', 'border-bottom: %@;')", @"vibeHighlightNote", @"2px solid #FE2E64"];
    [self stringByEvaluatingJavaScriptFromString:cssStyle];
    
    //[self stringByEvaluatingJavaScriptFromString:[self formatCSS:@"vibeHighlightNote" withColor:@"#fee370"]];
    [self stringByEvaluatingJavaScriptFromString:[self _formatCSS:@"vibeHighlightRed" withColor:@"#ffb1ad"]];
    [self stringByEvaluatingJavaScriptFromString:[self _formatCSS:@"vibeHighlight" withColor:@"#fee370"]];
    [self stringByEvaluatingJavaScriptFromString:[self _formatCSS:@"vibeHighlightBlue" withColor:@"#3fcfff"]];
    [self stringByEvaluatingJavaScriptFromString:[self _formatCSS:@"vibeHighlightYellow" withColor:@"#fdfeab"]];
    [self stringByEvaluatingJavaScriptFromString:[self _formatCSS:@"vibeHighlightGreen" withColor:@"#a1ffa2"]];
}

- (NSString *) _formatCSS: (NSString *) styleName withColor: (NSString *) color {
    NSString *highlightStyle = [NSString stringWithFormat:@"addCSSRule('.%@', 'background-color: %@;')", styleName, color];
    return highlightStyle;
}

- (void) clearSelection {
    self.userInteractionEnabled = NO;
    self.userInteractionEnabled = YES;
}

- (void) stopVideoPlayer {
    [self evalJS:@"stopPlayer()"];
}

- (void) attachJS: (NSString *) filename {
    NSString *file = [Utils prepareJSFile:filename];
    //VLog(@"JS_FILE: %@", file);
    [self stringByEvaluatingJavaScriptFromString:file];
}

- (NSString *) evalJS: (NSString *) command {
    NSString *result = [self stringByEvaluatingJavaScriptFromString:command];
    return result;
}

- (int) width {
    return (int) roundf(self.frame.size.width);
}

- (CGSize)windowSize {
    CGSize size;
    size.width = [[self stringByEvaluatingJavaScriptFromString:@"window.innerWidth"] integerValue];
    size.height = [[self stringByEvaluatingJavaScriptFromString:@"window.innerHeight"] integerValue];
    return size;
}

- (CGPoint)scrollOffset {
    CGPoint pt;
    pt.x = [[self stringByEvaluatingJavaScriptFromString:@"window.pageXOffset"] integerValue];
    pt.y = [[self stringByEvaluatingJavaScriptFromString:@"window.pageYOffset"] integerValue];
    return pt;
}

- (int) scrollWidth {
    int totalWidth = [[self evalJS:@"document.documentElement.scrollWidth"] intValue];
    return totalWidth;
}

- (CGRect)rectForSelectedText {
    CGRect selectedTextFrame = CGRectFromString([self stringByEvaluatingJavaScriptFromString:@"getRectForSelectedText()"]);
    return selectedTextFrame;
}

- (void) pageCurlAnimation: (NSString *) fillMode withDelegate: (id) delegate {
    CATransition *animation = [CATransition animation];
    [animation setDelegate:delegate];
    [animation setDuration:0.5];
    [animation setTimingFunction:UIViewAnimationCurveEaseInOut];
    animation.type = [fillMode isEqualToString:kCAFillModeForwards] ? @"pageUnCurl" : @"pageCurl";
    
    animation.subtype = kCATransitionFromRight;
    animation.fillMode = kCAFillModeBackwards;
    animation.startProgress = 0.3;
    [animation setRemovedOnCompletion:NO];
    
    [self.layer addAnimation:animation forKey:@"pageCurlAnimation"];
}

- (void) attachjQuery {
    [self attachJS:@"jquery"];
}

- (void) attachRangyFiles {
    [self attachJS:@"log4javascript_stub"];
    [self attachJS:@"rangy-core"];
    [self attachJS:@"rangy-cssclassapplier"];
    [self attachJS:@"rangy-highlighter"];
}

- (void) attachVibeFiles {
    [self attachJS:@"jstools"];
    [self attachJS:@"json2"];
    [self attachJS:@"vibebook-rangy"];
    [self attachJS:@"vibebook-helpers"];
}
@end
