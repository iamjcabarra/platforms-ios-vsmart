//
//  UIWebView+Private.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 3/30/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWebView (Private)
- (NSString *) fetchAllHighlights;
- (void) removeHighlights;
- (void) applyHighlightForData:(NSString *) range;
- (NSString *) undoHighlightForSelection;
- (NSString *) applyHighlightForSelection: (NSString *) cssSuffix;
- (NSInteger) highlightAllOccurencesOfString:(NSString*)str;
- (void) removeAllHighlights;
- (void) highlightSelectedStrings;
- (void) removeSelectedHighlights;
- (void) applyExerciseSecurityForJson: (NSString *) jsonString isTeacher:(BOOL)isTeacher;
- (void) applyExerciseAnswersForJson: (NSString *) jsonString;
- (NSString *) fetchExerciseAnswers;
- (void) applyPageTheme: (NSString *) themeName;
- (void) applyFontFamily: (NSString *) fontFamily;
- (void) applySmoothTransitionEffect;
- (void) applyCustomStyles;
- (void) attachJS: (NSString *) filename;
- (void) clearSelection;
- (void) stopVideoPlayer;
- (NSString *) evalJS: (NSString *) command;

- (int) getPageIndex: (NSString *) elementIdOrPath forWebviewWidth: (int) width forEncyclopedia:(BOOL) isEncyclopediaEntry;
- (int) getPageIndex: (int) startRange andEndRange:(int) endRange forWebviewWidth:(int) width;
- (int) getPageIndex: (NSString *) elementId forWebviewWidth: (int) width;
- (int) width;
- (CGSize)windowSize;
- (CGPoint)scrollOffset;
- (int) scrollWidth;
- (CGRect)rectForSelectedText;
- (void) pageCurlAnimation: (NSString *) fillMode withDelegate: (id) delegate;

- (void) attachjQuery;
- (void) attachRangyFiles;
- (void) attachVibeFiles;
@end
