//
//  NSDate+Helper.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 8/7/12.
//  Copyright (c) 2012 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Helper)
- (NSUInteger)daysAgo;
- (NSUInteger)daysAgoAgainstMidnight;
- (NSString *)stringDaysAgo;
- (NSString *)stringDaysAgoAgainstMidnight:(BOOL)flag;
- (NSUInteger)weekday;

+ (NSDate *)dateFromString:(NSString *)string;
+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format;
+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)string;
+ (NSString *)stringFromDate:(NSDate *)date;
+ (NSString *)stringForDisplayFromDate:(NSDate *)date;
+ (NSString *)stringForDisplayFromDate:(NSDate *)date prefixed:(BOOL)prefixed;
+ (NSString *)stringForDisplayFromDate:(NSDate *)date prefixed:(BOOL)prefixed alwaysDisplayTime:(BOOL)displayTime;

- (NSString *)string;
- (NSString *)stringWithFormat:(NSString *)format;
- (NSString *)stringWithDateStyle:(NSDateFormatterStyle)dateStyle timeStyle:(NSDateFormatterStyle)timeStyle;

- (NSDate *)beginningOfWeek;
- (NSDate *)beginningOfDay;
- (NSDate *)endOfWeek;

+ (NSString *)dateFormatString;
+ (NSString *)timeFormatString;
+ (NSString *)timestampFormatString;
+ (NSString *)dbFormatString;

-(NSString*)getDurationFrom:(NSDate*)date;

-(NSInteger)getYear;
-(NSInteger)getMonth;
-(NSInteger)getDay;

- (NSString *) relativeDateString;

+(void)getStartDayAndEndDayOfTheWeek:(NSDate*)date startDate:(NSDate**)startDate endDate:(NSDate**)endDate;


////////////////////////////////////////////////////////////
// TIME AGO
////////////////////////////////////////////////////////////
- (NSString *)timeAgoSimple;
- (NSString *)timeAgo;
- (NSString *)timeAgoWithLimit:(NSTimeInterval)limit;
- (NSString *)timeAgoWithLimit:(NSTimeInterval)limit dateFormat:(NSDateFormatterStyle)dFormatter andTimeFormat:(NSDateFormatterStyle)tFormatter;
- (NSString *)timeAgoWithLimit:(NSTimeInterval)limit dateFormatter:(NSDateFormatter *)formatter;
- (NSString *)dateTimeAgo;
- (NSString *)dateTimeUntilNow;

@end
