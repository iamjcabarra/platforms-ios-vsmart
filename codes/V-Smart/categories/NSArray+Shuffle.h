//
//  NSArray+Shuffle.h
//  Booklatan
//
//  Created by Earljon Hidalgo on 5/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Shuffle)
-(NSArray *)shuffledArray;
@end
