//
//  UIButton+Additions.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/16/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "UIButton+Additions.h"

@implementation UIButton (Additions)

+ (UIButton *) buttonWithLabel:(NSString *)buttonName forTarget:(id) target withAction:(SEL) selector {
    UIButton *button = nil;
    
    if (buttonName != nil) {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = (CGRect){CGPointZero, 100, 100};
        [button setTitle:buttonName forState:UIControlStateNormal];
        [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    }
    
    return button;
}

+ (UIButton *) buttonWithLabel:(NSString *)buttonName inFrame:(CGRect) frame forTarget:(id) target withAction:(SEL) selector {
    UIButton *button = nil;
    
    if (buttonName != nil) {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = frame;
        [button setTitle:buttonName forState:UIControlStateNormal];
        [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    }
    
    return button;
}


+ (UIButton *) buttonWithImageNamed:(NSString *)imageName {
    UIImage *image = [UIImage imageNamed:imageName];
    UIButton *button = nil;
    
    if (image != nil) {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = (CGRect){CGPointZero,image.size};
        [button setImage:image forState:UIControlStateNormal];
    }
    
    return button;
}

+ (UIButton *) buttonWithImage:(UIImage *)image forTarget:(id) target withAction:(SEL) selector {
    UIButton *button = nil;
    
    if (image != nil) {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = (CGRect){CGPointZero,image.size};
        [button setImage:image forState:UIControlStateNormal];
        
        [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    }
    
    return button;
}

+ (UIButton *) buttonWithImage:(UIImage *)image inFrame:(CGRect) frame forTarget:(id) target withAction:(SEL) selector {
    UIButton *button = nil;
    
    if (image != nil) {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = frame;
        [button setImage:image forState:UIControlStateNormal];
        
        [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    }
    
    return button;
}



@end
