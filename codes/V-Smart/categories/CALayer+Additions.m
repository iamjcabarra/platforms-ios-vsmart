//
//  CALayer+Additions.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 9/11/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "CALayer+Additions.h"
#import <QuartzCore/QuartzCore.h>

@implementation CALayer (Additions)
+ (id)maskLayerWithCorners:(UIRectCorner)corners radii:(CGSize)radii frame:(CGRect)frame {
    
    // Create a CAShapeLayer
    CAShapeLayer *mask = [CAShapeLayer layer];
    
    // Set the frame
    mask.frame = frame;
    
    // Set the CGPath from a UIBezierPath
    mask.path = [UIBezierPath bezierPathWithRoundedRect:mask.bounds byRoundingCorners:corners cornerRadii:radii].CGPath;
    
    // Set the fill color
    mask.fillColor = [UIColor whiteColor].CGColor;
    
    return mask;
}
@end
