//
//  UIImageView+Additions.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 9/11/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Additions)
-(void)maskRoundCorners:(UIRectCorner)corners radius:(CGFloat)radius;
-(void) applyShadowBorder;
@end
