//
//  UIImage+Addons.m
//  Booklatan
//
//  Created by Earljon Hidalgo on 11/10/11.
//  Copyright (c) 2011 Vibal Foundadtion. All rights reserved.
//

#import "UIImage+Addons.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIImage (Addons)
+ (UIImage*)imageWithImage:(UIImage*)image 
			  scaledToSize:(CGSize)newSize;
{
	UIGraphicsBeginImageContext( newSize );
	[image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
	UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return newImage;
}

- (UIImage*)scaleImageToSize:(CGSize)newSize
{
	return [UIImage imageWithImage:self scaledToSize:newSize];
}

//+ (UIImage*)imageWithShadow: (UIImage *) img {
//    CGColorSpaceRef colourSpace = CGColorSpaceCreateDeviceRGB();
//    CGContextRef shadowContext = CGBitmapContextCreate(NULL, img.size.width + 10, img.size.height + 10, 
//                                                       CGImageGetBitsPerComponent(img.CGImage), 0, 
//                                                       colourSpace, kCGImageAlphaPremultipliedLast);
//    CGColorSpaceRelease(colourSpace);
//    
//    CGContextSetShadowWithColor(shadowContext, CGSizeMake(5, -5), 8, [UIColor blackColor].CGColor);
//    CGContextDrawImage(shadowContext, CGRectMake(0, 10, img.size.width, img.size.height), img.CGImage);
//    
//    CGImageRef shadowedCGImage = CGBitmapContextCreateImage(shadowContext);
//    CGContextRelease(shadowContext);
//    
//    UIImage * shadowedImage = [UIImage imageWithCGImage:shadowedCGImage];
//    CGImageRelease(shadowedCGImage);
//    
//    return shadowedImage;
//}

- (UIImage*)imageWithShadow {
    CGColorSpaceRef colourSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef shadowContext = CGBitmapContextCreate(NULL, self.size.width + 8, self.size.height + 8,
                                                       CGImageGetBitsPerComponent(self.CGImage), 0, 
                                                       colourSpace, kCGBitmapAlphaInfoMask);
    CGColorSpaceRelease(colourSpace);
    
    CGContextSetShadowWithColor(shadowContext, CGSizeMake(5, -5), 5, [UIColor darkGrayColor].CGColor);
    CGContextDrawImage(shadowContext, CGRectMake(0, 10, self.size.width, self.size.height), self.CGImage);
    
    CGImageRef shadowedCGImage = CGBitmapContextCreateImage(shadowContext);
    CGContextRelease(shadowContext);
    
    UIImage * shadowedImage = [UIImage imageWithCGImage:shadowedCGImage];
    CGImageRelease(shadowedCGImage);
    
    return shadowedImage;
}

- (UIImage*) imageToGrayscale {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGRect imageRect = CGRectMake(0.0f, 0.0f, self.size.width, self.size.height);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    // Draw a white background
    CGContextSetRGBFillColor(ctx, 1.0f, 1.0f, 1.0f, 1.0f);
    CGContextFillRect(ctx, imageRect);
    
    // Draw the luminosity on top of the white background to get grayscale
    [self drawInRect:imageRect blendMode:kCGBlendModeLuminosity alpha:1.0f];
    
    // Apply the source image's alpha
    [self drawInRect:imageRect blendMode:kCGBlendModeDestinationIn alpha:1.0f];
    
    UIImage* grayscaleImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return grayscaleImage;
}

- (BOOL)imageIsEqualTo:(UIImage *)image2
{
    NSData *data1 = UIImagePNGRepresentation(self);
    NSData *data2 = UIImagePNGRepresentation(image2);
    
    return [data1 isEqual:data2];
}

- (UIImage *) overlayImage:(UIImage*) topImage atPoint:(CGPoint) point
{
    UIGraphicsBeginImageContextWithOptions(self.size, FALSE, 0.0);
    [self drawInRect:CGRectMake( 0, 0, self.size.width, self.size.height)];
    [topImage drawInRect:CGRectMake( point.x, point.y, topImage.size.width, topImage.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage *)imageFromPDF:(NSURL *)filePath {
    
    NSURL* pdfFileUrl = filePath;
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfFileUrl);
    
    CGPDFPageRef page = CGPDFDocumentGetPage(pdf, 1);
    
    CGRect aRect = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
    UIGraphicsBeginImageContext(aRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, 0.0, aRect.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextTranslateCTM(context, -(aRect.origin.x), -(aRect.origin.y));
    
    CGContextSetGrayFillColor(context, 1.0, 1.0);
    CGContextFillRect(context, aRect);
    
    CGAffineTransform pdfTransform = CGPDFPageGetDrawingTransform(page, kCGPDFCropBox, aRect, 0, false);
    CGContextConcatCTM(context, pdfTransform);
    CGContextDrawPDFPage(context, page);
    
    UIImage *thumbnail = UIGraphicsGetImageFromCurrentImageContext();
    
    CGContextRestoreGState(context);
    UIGraphicsEndImageContext();
    CGPDFDocumentRelease(pdf);
    
    return thumbnail;
}

+ (UIImage *)imageFromPDF:(NSURL *)filePath pass:(NSString *)key {
    
    NSURL* pdfFileUrl = filePath;
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfFileUrl);
    
    char text[128]; // char array buffer for the string conversion
    [key getCString:text maxLength:126 encoding:NSUTF8StringEncoding];
    CGPDFDocumentUnlockWithPassword(pdf, text);
    
    CGPDFPageRef page = CGPDFDocumentGetPage(pdf, 1);
    
    CGRect aRect = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
    UIGraphicsBeginImageContext(aRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, 0.0, aRect.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextTranslateCTM(context, -(aRect.origin.x), -(aRect.origin.y));
    
    CGContextSetGrayFillColor(context, 1.0, 1.0);
    CGContextFillRect(context, aRect);
    
    CGAffineTransform pdfTransform = CGPDFPageGetDrawingTransform(page, kCGPDFCropBox, aRect, 0, false);
    CGContextConcatCTM(context, pdfTransform);
    CGContextDrawPDFPage(context, page);
    
    UIImage *thumbnail = UIGraphicsGetImageFromCurrentImageContext();
    
    CGContextRestoreGState(context);
    UIGraphicsEndImageContext();
    CGPDFDocumentRelease(pdf);
    
    return thumbnail;
}

+ (UIImage *)imageNamed:(NSString *)name withColor:(UIColor *)color
{
    return [UIImage imageNamed:name];
    
    
//    // load the image
//    NSString *imageName = @"color_template";
//    UIImage *img = [UIImage imageNamed:imageName];
//    
//    // begin a new image context, to draw our colored image onto
//    UIGraphicsBeginImageContext(img.size);
//    
//    // get a reference to that context we created
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    
//    // set the fill color
//    [color setFill];
//    
//    // translate/flip the graphics context (for transforming from CG* coords to UI* coords
//    CGContextTranslateCTM(context, 0, img.size.height);
//    CGContextScaleCTM(context, 1.0, -1.0);
//    
//    // set the blend mode to color burn, and the original image
//    CGContextSetBlendMode(context, kCGBlendModeColorBurn);
//    CGRect rect = CGRectMake(0, 0, img.size.width, img.size.height);
//    CGContextDrawImage(context, rect, img.CGImage);
//    
//    // set a mask that matches the shape of the image, then draw (color burn) a colored rectangle
//    CGContextClipToMask(context, rect, img.CGImage);
//    CGContextAddRect(context, rect);
//    CGContextDrawPath(context,kCGPathFill);
//    
//    // generate a new UIImage from the graphics context we drew onto
//    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    //return the color-burned image
//    return coloredImg;
}
@end
