//
//  NTMNotebookViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 08/08/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class NTMNotebookViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet var topPanelViewTitleLabel: UILabel!
    @IBOutlet var topPanelViewLeftPanelImage: UIImageView!
    @IBOutlet var topPanelViewLeftPanelButton: UIButton!
    @IBOutlet var topPanelViewRightPanelButton: UIButton!
    @IBOutlet var notebookCollectionView: UICollectionView!
    @IBOutlet var addNotebookButton: UIButton!
    
    fileprivate var notebooks: [[String: Any]]!
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notebooks = [
            ["name": "Math", "bg_color": 0],
            ["name": "Science", "bg_color": 1],
            ["name": "English", "bg_color": 2],
            ["name": "PE", "bg_color": 3],
            ["name": "Physics", "bg_color": 4]]
        
        notebookCollectionView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Collection View Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notebooks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NTMConstants.CellIdentifier.NOTEBOOK, for: indexPath) as! NTMNotebookCollectionViewCell
        let notebook: [String: Any] = notebooks[indexPath.row]
        let name: String = notebook["name"] as! String
        let colorCode: Int = notebook["bg_color"] as! Int
        
        cell.notebookNameLabel.text = name
        cell.notebookImage.image = cell.noteBookImage(forColorCode: colorCode)
        
        cell.notebookActionButton.addTarget(self, action: #selector(showActionMenu(_:)), for: .touchUpInside)
        
        return cell
    }
    
    // MARK: - Button Event Handlers

    /// Presents action menu as user clicks on button in the notebook.
    ///
    /// - parameter sender: A UIButton
    @objc fileprivate func showActionMenu(_ sender: UIButton) {
        let actionMenu = NTMNotebookActionMenuViewController(nibName: "NTMNotebookActionMenuViewController", bundle: nil)
        actionMenu.modalPresentationStyle = .popover
        actionMenu.preferredContentSize = CGSize(width: 160.0, height: 45.0)
        actionMenu.popoverPresentationController?.permittedArrowDirections = .right
        actionMenu.popoverPresentationController?.sourceView = sender
        actionMenu.popoverPresentationController?.sourceRect = sender.bounds
        self.present(actionMenu, animated: true, completion: nil)
    }
    
}
