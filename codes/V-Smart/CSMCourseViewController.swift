//
//  CSMCourseViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 28/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class CSMCourseViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {
    
    @IBOutlet fileprivate var tableView: UITableView!
    @IBOutlet fileprivate var loadMoreView: UIView!
    @IBOutlet fileprivate var loadMoreButton: UIButton!
    
    fileprivate let courseCellIdentifier = "course_cell_identifier"
    
    // MARK: - Data Manager
    
    fileprivate lazy var courseDataManager: CSMDataManager = {
        let csdm = CSMDataManager.sharedInstance
        return csdm
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadCourseListForRequestType(.load)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: List Course
    
    fileprivate func loadCourseListForRequestType(_ requestType: CSMCourseListRequestType) {
        if (requestType != .refresh) {
            let indicatorString = requestType == .load ? "\(NSLocalizedString("Loading", comment: ""))..." : "\(NSLocalizedString("Searching", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
        }
        
        self.courseDataManager.requestCourseList { (dataBlock) in
            if let data = dataBlock {
                DispatchQueue.main.async(execute: {
                    let count = data["count"] as! Int
                    self.title = "\(NSLocalizedString("Course List", comment: "")) (\(count))"  // unchecked
                    //self.temporaryCourseCategoryListCount = count
                })
            }
            else {
                DispatchQueue.main.async(execute: {
                    //let message = NSLocalizedString("There was an error loading this page.", comment: "") // unchecked
                    //self.showNotificationMessage(message)
                })
            }
            
            DispatchQueue.main.async(execute: {
                //self.reloadFetchedResultsController()
                //requestType != .Refresh ? HUD.hideUIBlockingIndicator() : self.tableRefreshControl.endRefreshing()
            })
        }
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        //self.shouldShowEmptyPlaceholderView((sectionData.numberOfObjects > 0) ? false : true)
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.courseCellIdentifier, for: indexPath) as! CSMCoureTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: CSMCoureTableViewCell, atIndexPath indexPath: IndexPath) {
        //if let mo = fetchedResultsController.object(at: indexPath) as? NSManagedObject {
        let mo = fetchedResultsController.object(at: indexPath)
            guard
                let course_name = mo.value(forKey: "course_name") as? String,
                let section_name = mo.value(forKey: "section_name") as? String,
                let grade_level = mo.value(forKey: "grade_level") as? String,
                let schedule = mo.value(forKey: "schedule") as? String
                else { return }
            
            cell.courseNameLabel.text = course_name
            cell.gradeLevelLabel.text = "\(section_name) - \(grade_level)"
            cell.courseScheduleLabel.text = schedule
        //}
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {

        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = self.courseDataManager.getMainContext()
        
        //let fetchRequest = NSFetchRequest(entityName: CSMConstants.Entity.COURSE)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: CSMConstants.Entity.COURSE)
        fetchRequest.fetchBatchSize = 20
        
        let descriptorSelector = #selector(NSString.localizedCaseInsensitiveCompare(_:))
        let sortDescriptor = NSSortDescriptor.init(key: "course_name", ascending: true, selector: descriptorSelector)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.tableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! CSMCoureTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }

}
