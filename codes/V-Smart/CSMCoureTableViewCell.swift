//
//  CSMCoureTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 22/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

class CSMCoureTableViewCell: UITableViewCell {
    
    @IBOutlet var courseNameLabel: UILabel!
    @IBOutlet var gradeLevelLabel: UILabel!
    @IBOutlet var courseScheduleLabel: UILabel!
    
}
