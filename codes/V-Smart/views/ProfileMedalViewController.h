//
//  ProfileMedalViewController.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/23/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProfileMedalViewControllerDelegate;

@interface ProfileMedalViewController : UIViewController
{

}

@property (nonatomic, assign) id<ProfileMedalViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *headerText;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSArray *headerSets;
@property (nonatomic, strong) NSString *medalType;
@property (weak, nonatomic) IBOutlet UITableView *tvwResults;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil medalType:(NSString *) type;

-(IBAction)close:(id)sender;
@end

@protocol ProfileMedalViewControllerDelegate <NSObject>

@optional
-(void)didCloseProfileMedalWindow:(ProfileMedalViewController *) popViewController;

@end
