//
//  UserProfileViewBasic.h
//  V-Smart
//
//  Created by VhaL on 4/21/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileViewBasic : UIView
@property (nonatomic, strong) IBOutlet UILabel *detail1;
@property (nonatomic, strong) IBOutlet UILabel *detail2;
@property (nonatomic, strong) IBOutlet UILabel *birthday;
@property (nonatomic, strong) IBOutlet UILabel *sex;
@property (nonatomic, strong) IBOutlet UILabel *address;
@property (nonatomic, strong) IBOutlet UILabel *facebook;
@property (nonatomic, strong) IBOutlet UILabel *twitter;
@property (nonatomic, strong) IBOutlet UILabel *instagram;
@property (nonatomic, strong) IBOutlet UILabel *contact;
@property (nonatomic, strong) IBOutlet UILabel *contactPerson;
@property (nonatomic, strong) IBOutlet UILabel *birthPlace;

@property (nonatomic, strong) IBOutlet UITextField *editLastName;
@property (nonatomic, strong) IBOutlet UITextField *editFirstName;
@property (nonatomic, strong) IBOutlet UITextField *editMiddleName;
@property (nonatomic, strong) IBOutlet UITextField *editSuffix;

@property (nonatomic, strong) IBOutlet UITextField *editDetail1;
@property (nonatomic, strong) IBOutlet UITextField *editDetail2;
@property (nonatomic, strong) IBOutlet UITextField *editBirthday;
@property (nonatomic, strong) IBOutlet UITextField *editSex;
@property (nonatomic, strong) IBOutlet UITextView *editAddress;
@property (nonatomic, strong) IBOutlet UITextField *editFacebook;
@property (nonatomic, strong) IBOutlet UITextField *editTwitter;
@property (nonatomic, strong) IBOutlet UITextField *editInstagram;
@property (nonatomic, strong) IBOutlet UITextField *editContact;
@property (nonatomic, strong) IBOutlet UITextField *editContactPerson;
@property (nonatomic, strong) IBOutlet UITextField *editBirthPlace;

@end

@interface UserProfileViewOther : UIView
@property (nonatomic, strong) IBOutlet UILabel *spokenLanguage;
@property (nonatomic, strong) IBOutlet UILabel *hobbies;
@property (nonatomic, strong) IBOutlet UILabel *sports;
@property (nonatomic, strong) IBOutlet UILabel *nationality;
@property (nonatomic, strong) IBOutlet UITextView *aboutme;
@property (nonatomic, strong) IBOutlet UIButton *changePassword;
@property (nonatomic, strong) IBOutlet UITextField *editSpokenLanguage;
@property (nonatomic, strong) IBOutlet UITextField *editHobbies;
@property (nonatomic, strong) IBOutlet UITextField *editSports;
@property (nonatomic, strong) IBOutlet UITextField *editNationality;
@property (nonatomic, strong) IBOutlet UITextView *editAboutme;
@end

@interface UserProfileViewSchool : UIView
@property (nonatomic, strong) IBOutlet UILabel *preschoolname;
@property (nonatomic, strong) IBOutlet UILabel *preschoolyear;
@property (nonatomic, strong) IBOutlet UILabel *preschooladdress;
@property (nonatomic, strong) IBOutlet UILabel *elementaryname;
@property (nonatomic, strong) IBOutlet UILabel *elementaryyear;
@property (nonatomic, strong) IBOutlet UILabel *elementaryaddress;
@property (nonatomic, strong) IBOutlet UILabel *highschoolname;
@property (nonatomic, strong) IBOutlet UILabel *highschoolyear;
@property (nonatomic, strong) IBOutlet UILabel *highschooladdress;

@property (nonatomic, strong) IBOutlet UITextField *editPreschoolname;
@property (nonatomic, strong) IBOutlet UITextField *editPreschoolyear;
@property (nonatomic, strong) IBOutlet UITextView *editPreschooladdress;
@property (nonatomic, strong) IBOutlet UITextField *editElementaryname;
@property (nonatomic, strong) IBOutlet UITextField *editElementaryyear;
@property (nonatomic, strong) IBOutlet UITextView *editElementaryaddress;
@property (nonatomic, strong) IBOutlet UITextField *editHighschoolname;
@property (nonatomic, strong) IBOutlet UITextField *editHighschoolyear;
@property (nonatomic, strong) IBOutlet UITextView *editHighschooladdress;
@end

@interface UserProfileViewStatistics : UIView
@property (nonatomic, strong) IBOutlet UILabel *postedTopics;
@property (nonatomic, strong) IBOutlet UILabel *repliedTopics;
@property (nonatomic, strong) IBOutlet UILabel *feedback;
@property (nonatomic, strong) IBOutlet UILabel *receivedLikes;
@property (nonatomic, strong) IBOutlet UILabel *likedSoFar;
@property (nonatomic, strong) IBOutlet UILabel *sharing;
@property (nonatomic, strong) IBOutlet UILabel *percentCompleted;
@end