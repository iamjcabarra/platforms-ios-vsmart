//
//  UpdateAvatarViewCell.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 11/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class UpdateAvatarViewCell: UICollectionViewCell {
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var selectedIndicator: UIImageView!
    
    
    override func prepareForReuse() {
        selectedIndicator.isHidden = true
        self.tag = 0
    }
    
    override func awakeFromNib() {
        avatarImage.clipsToBounds = true
        avatarImage.layer.borderColor = UIColor.gray.cgColor
        avatarImage.layer.borderWidth = 3
    }
}
