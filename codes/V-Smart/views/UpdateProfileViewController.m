//
//  UpdateProfileViewController.m
//  V-Smart
//
//  Created by VhaL on 4/8/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "UpdateProfileViewController.h"
#import "UserProfileViewInfo.h"
#import "JMSPeopleItem.h"
#import "HMSegmentedControl.h"
#import "ChangePasswordController.h"
#import "TGTBDatePickerView.h"

@interface UpdateProfileViewController () <ChangePasswordDelegate, TGTBDatePickerViewDelegate>{
    BOOL isTeacher;
}
@property (weak, nonatomic) IBOutlet UIButton *editBirthdayButton;

// PICKER VIEWS
@property (strong, nonatomic) TGTBDatePickerView *datePickerView;

@property (nonatomic, weak) IBOutlet UILabel *labelFullName;
@property (nonatomic, weak) IBOutlet UITextField *textFieldNickName;
@property (nonatomic, weak) IBOutlet UILabel *labelAlias;
@property (nonatomic, weak) IBOutlet UIImageView *imageViewAvatar;
@property (nonatomic, weak) IBOutlet UIView *viewDetail;

@property (nonatomic, weak) IBOutlet UILabel *segmentedControlPlaceHolder;

@property (nonatomic, strong) UserProfileViewBasic *viewBasic;
@property (nonatomic, strong) UserProfileViewOther *viewOther;
@property (nonatomic, strong) UserProfileViewSchool *viewSchool;

@property (nonatomic, strong) HMSegmentedControl *segmentedControl;

@property (nonatomic, strong) NSString *userPassword;
@property (nonatomic, strong) NSString *userPosition;

@end

@implementation UpdateProfileViewController
@synthesize buttonUpdate, textFieldNickName;
@synthesize viewBasic, viewOther, viewSchool, viewDetail;

- (AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

-(IBAction)buttonUpdateTapped:(id)sender{
    
    if (viewBasic.editFirstName.text.length <= 0 || viewBasic.editLastName.text.length <= 0){

        /* localizable strings */
        NSString *namesShouldNotBeEmpty = NSLocalizedString(@"First & Last name should not be blank", nil); //checked
        [self.view makeToast:namesShouldNotBeEmpty duration:2.0f position:@"center"];
        
        return;
    }
    
    int user_type_id = 4;
    if ( [self.userPosition isEqualToString:@"superadmin"] ) {
        user_type_id = 1;
    } else if ( [self.userPosition isEqualToString:@"admin"] ) {
        user_type_id = 2;
    } else if ( [self.userPosition isEqualToString:@"teacher"] ) {
        user_type_id = 3;
    } else if ( [self.userPosition isEqualToString:@"student"] ) {
        user_type_id = 4;
    }
    
    // ENHANCEMENT#766
    // jca-12-21-15
    // Remove leading and trailing spaces from first name, middle name and last name
    //NSString *lName = [NSString stringWithFormat:@"%@", viewBasic.editLastName.text];
    NSString *lName = [self stringValue:viewBasic.editLastName.text];
    NSString *trimmedLN = [lName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //NSString *mName = [NSString stringWithFormat:@"%@", viewBasic.editMiddleName.text];
    NSString *mName = [self stringValue:viewBasic.editMiddleName.text];
    NSString *trimmedMN = [mName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //NSString *fName = [NSString stringWithFormat:@"%@", viewBasic.editFirstName.text];
    NSString *fName = [self stringValue:viewBasic.editFirstName.text];
    NSString *trimmedFN = [fName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    // Remove spaces in between and leave only one
    trimmedLN = [self leaveOneSpaceInBetween:trimmedLN];
    trimmedMN = [self leaveOneSpaceInBetween:trimmedMN];
    trimmedFN = [self leaveOneSpaceInBetween:trimmedFN];

    NSLog(@"LN %@", trimmedLN);
    NSLog(@"MN %@", trimmedMN);
    NSLog(@"FN %@", trimmedFN);
    
    NSDictionary *dict = @{
    @"vibe_id" : @"0",
    @"last_name":  /* [NSString stringWithFormat:@"%@", viewBasic.editLastName.text] */  trimmedLN,
    @"first_name": /* [NSString stringWithFormat:@"%@", viewBasic.editFirstName.text] */ trimmedFN,
    @"mid_name": /* [NSString stringWithFormat:@"%@",viewBasic.editMiddleName.text] */   trimmedMN,
    @"suffix_name": /*[NSString stringWithFormat:@"%@",viewBasic.editSuffix.text]*/ [self stringValue:viewBasic.editSuffix.text],
    @"email": /*[NSString stringWithFormat:@"%@",[self account].user.email]*/ [self stringValue:[self account].user.email],
//    @"username": /*[NSString stringWithFormat:@"%@",[self account].user.username]*/ [self stringValue:[self account].user.username], // FIX ISSUE 01/25/2017
//    @"avatar": "/uploads/26/avatars/20140425/8e3a6feb273283b9b8abd31f7c7b0252.png",
    @"address": /*[NSString stringWithFormat:@"%@",viewBasic.editAddress.text]*/ [self stringValue:viewBasic.editAddress.text],
    @"contact": /*[NSString stringWithFormat:@"%@",viewBasic.editContact.text]*/ [self stringValue:viewBasic.editContact.text],
    @"contact_person": /*[NSString stringWithFormat:@"%@",viewBasic.editContactPerson.text]*/ [self stringValue:viewBasic.editContactPerson.text],
    @"birth_date": /*[NSString stringWithFormat:@"%@",viewBasic.editBirthday.text]*/ [self stringValue:viewBasic.editBirthday.text],
    @"birth_place": /*[NSString stringWithFormat:@"%@",viewBasic.editBirthPlace.text]*/ [self stringValue:viewBasic.editBirthPlace.text],
    @"nationality": /*[NSString stringWithFormat:@"%@",viewOther.editNationality.text]*/ [self stringValue:viewOther.editNationality.text],
    @"gender": /*[NSString stringWithFormat:@"%@",viewBasic.editSex.text]*/ [self stringValue:viewBasic.editSex.text],
//    @"title": @"",
    @"nickname": /*[NSString stringWithFormat:@"%@",self.textFieldNickName.text]*/ [self stringValue:self.textFieldNickName.text],
    @"spoken_language": /*[NSString stringWithFormat:@"%@",viewOther.editSpokenLanguage.text]*/ [self stringValue:viewOther.editSpokenLanguage.text],
    @"about_me": /*[NSString stringWithFormat:@"%@",viewOther.editAboutme.text]*/ [self stringValue:viewOther.editAboutme.text],
    @"hobbies": /*[NSString stringWithFormat:@"%@",viewOther.editHobbies.text]*/ [self stringValue:viewOther.editHobbies.text],
    @"sports": /*[NSString stringWithFormat:@"%@",viewOther.editSports.text]*/ [self stringValue:viewOther.editSports.text],
    @"facebook": /*[NSString stringWithFormat:@"%@",viewBasic.editFacebook.text]*/ [self stringValue:viewBasic.editFacebook.text],
    @"twitter": /*[NSString stringWithFormat:@"%@",viewBasic.editTwitter.text]*/ [self stringValue:viewBasic.editTwitter.text],
    @"instagram": /*[NSString stringWithFormat:@"%@",viewBasic.editInstagram.text]*/ [self stringValue:viewBasic.editInstagram.text],
    @"pre_school_name": /*[NSString stringWithFormat:@"%@",viewSchool.editPreschoolname.text]*/ [self stringValue:viewSchool.editPreschoolname.text],
    @"pre_school_address": /*[NSString stringWithFormat:@"%@",viewSchool.editPreschooladdress.text]*/ [self stringValue:viewSchool.editPreschooladdress.text],
    @"pre_school_year": /*[NSString stringWithFormat:@"%@",viewSchool.editPreschoolyear.text]*/ [self stringValue:viewSchool.editPreschoolyear.text],
    @"elem_school_name": /*[NSString stringWithFormat:@"%@",viewSchool.editElementaryname.text]*/ [self stringValue:viewSchool.editElementaryname.text],
    @"elem_school_address": /*[NSString stringWithFormat:@"%@",viewSchool.editElementaryaddress.text]*/ [self stringValue:viewSchool.editElementaryaddress.text],
    @"elem_school_year": /*[NSString stringWithFormat:@"%@",viewSchool.editElementaryyear.text]*/ [self stringValue:viewSchool.editElementaryyear.text],
    @"high_school_name": /*[NSString stringWithFormat:@"%@",viewSchool.editHighschoolname.text]*/ [self stringValue:viewSchool.editHighschoolname.text],
    @"high_school_address": /*[NSString stringWithFormat:@"%@",viewSchool.editHighschooladdress.text]*/ [self stringValue:viewSchool.editHighschooladdress.text],
    @"high_school_year": /*[NSString stringWithFormat:@"%@",viewSchool.editHighschoolyear.text]*/ [self stringValue:viewSchool.editHighschoolyear.text],
    @"password": /*[NSString stringWithFormat:@"%@",self.userPassword]*/ [self stringValue:self.userPassword],
    @"user_type_id" : [NSString stringWithFormat:@"%@",[NSNumber numberWithInt:user_type_id]]
    };
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:&err];
    NSString * jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSString *url = VS_FMT([Utils buildUrl:kEndPointUpdateProfile], [self account].user.id);
    
    
    /*
     NOTE:
     Get a pointer to the JSONHTTPClient headers
     set the schoolcode_base64
     */
    NSMutableDictionary *headers = [JSONHTTPClient requestHeaders];
    headers[@"code"] = [Utils getSettingsSchoolCodeBase64];
    
    [JSONHTTPClient postJSONFromURLWithString:url bodyString:jsonString completion:^(NSDictionary *json, JSONModelError *err) {

        if (!err) {
            AccountInfo *account = [self account];

//            account.user.firstname = self.viewBasic.editFirstName.text;
//            account.user.middlename = self.viewBasic.editMiddleName.text;
//            account.user.lastname = self.viewBasic.editLastName.text;
//            account.user.suffix = self.viewBasic.editSuffix.text;
            
            account.user.firstname = trimmedFN;
            account.user.middlename = trimmedMN;
            account.user.lastname = trimmedLN;
            account.user.suffix = self.viewBasic.editSuffix.text;
            
            [Utils saveArchive:account forKey:kGlobalAccount];
            
            UIViewController *parent = (UIViewController *)self.delegate;
            [parent dismissPopupViewControllerWithanimationType:PopupViewAnimationSlideBottomBottom];
            
            /* localizable strings */
            NSString *updateProfileSuccess = NSLocalizedString(@"Update profile success.", nil); //checked
            [parent.view makeToast:updateProfileSuccess duration:2.0f position:@"center"];
            
            [self.delegate performSelector:@selector(updateProfile) withObject:nil];
            
        }
        else{
            
            /* localizable strings */
            NSString *updateProfileFailed = NSLocalizedString(@"Update profile failed.", nil); //checked
            [self.view makeToast:updateProfileFailed duration:2.0f position:@"center"];
            
        }

    }];
}

- (void)birthdateButtonAction:(UIButton *)button {
    
    // Create Date Picker Controller
    self.datePickerView = [[TGTBDatePickerView alloc] initWithNibName:@"TGTBDatePickerView" bundle:nil];
    self.datePickerView.dateType = 0;
    self.datePickerView.labelString = @"Set Birthdate";
    self.datePickerView.delegate = self;
    
    // Present As Modal
    self.datePickerView.modalPresentationStyle = UIModalPresentationPopover;
    self.datePickerView.preferredContentSize = CGSizeMake(315.0f, 263.0f);
    self.datePickerView.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
    self.datePickerView.popoverPresentationController.sourceView = button;
    self.datePickerView.popoverPresentationController.sourceRect = button.bounds;
    
    NSDate *dateToDisplay = [NSDate date];
    if ([self.viewBasic.editBirthday.text length] > 0) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-mm-dd";
        NSDate *birthdate = [dateFormatter dateFromString:self.viewBasic.editBirthday.text];
        if (birthdate != nil) {
            dateToDisplay = birthdate;
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.datePickerView.datePicker setDate:dateToDisplay];
        self.datePickerView.datePicker.datePickerMode = UIDatePickerModeDate;
    });
    
    [self presentViewController:self.datePickerView animated:true completion:nil];
}

-(void)selectedDate:(NSString *)dateString {
    self.viewBasic.editBirthday.text = dateString;
}

- (NSString *)leaveOneSpaceInBetween:(NSString *)string {
    NSCharacterSet *spaces = [NSCharacterSet whitespaceCharacterSet];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF != ''"];
    NSArray *tempArray = [[string componentsSeparatedByCharactersInSet:spaces] filteredArrayUsingPredicate:predicate];
    
    return string = [tempArray componentsJoinedByString:@" "];;
}

- (void)viewDidLoad{
    [super viewDidLoad];

    isTeacher = [[self account].user.position isEqualToString:kModeIsTeacher];
    
    //self.userPassword = [NSString stringWithFormat:@"%@", [self account].user.password];
    //self.userPosition = [NSString stringWithFormat:@"%@", [self account].user.position];
    self.userPassword = [self stringValue:[self account].user.password];
    self.userPosition = [self stringValue:[self account].user.position];
    
    [self addSubviews];
    [self addHMSegmentedControl];
    [self clearValues];

    //viewBasic.editFirstName.text = [self account].user.firstname;
    //viewBasic.editMiddleName.text = [self account].user.middlename;
    //viewBasic.editLastName.text = [self account].user.lastname;
    //viewBasic.editSuffix.text = [self account].user.suffix;
    
    viewBasic.editFirstName.text = [self stringValue:[self account].user.firstname];
    viewBasic.editMiddleName.text = [self stringValue:[self account].user.middlename];
    viewBasic.editLastName.text = [self stringValue:[self account].user.lastname];
    viewBasic.editSuffix.text = [self stringValue:[self account].user.suffix];
    
    viewBasic.editFirstName.delegate = self;
    viewBasic.editMiddleName.delegate = self;
    viewBasic.editLastName.delegate = self;
    viewBasic.editSuffix.delegate = self;
    
//    [self.buttonUpdate addTarget:self action:@selector(buttonUpdateTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [viewBasic.editFirstName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [viewBasic.editMiddleName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [viewBasic.editLastName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [viewBasic.editSuffix addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [viewOther.changePassword addTarget:self action:@selector(buttonActionchangePassword:) forControlEvents:UIControlEventTouchUpInside];

    [self.editBirthdayButton addTarget:self action:@selector(birthdateButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self initializePhase];
}

- (void)buttonActionchangePassword:(id)sender
{
    ChangePasswordController *cp = [[ChangePasswordController alloc] initWithNibName:@"ChangePasswordController" bundle:nil];
    cp.modalPresentationStyle = UIModalPresentationFormSheet;
    cp.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    cp.delegate = self;
    [self presentViewController:cp animated:YES completion:nil];
}

- (void)didFinishChangedPasswordWithStatus:(BOOL)status {
    
    if (status == YES) {
        UIViewController *parent = (UIViewController *)self.delegate;
        [parent dismissPopupViewControllerWithanimationType:PopupViewAnimationSlideBottomBottom];
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"NOTIF_CHANGE_PASSWORD_DONE" object:nil];
    }
}

-(IBAction)textFieldDidChange:(id)sender{
    NSString *midName = viewBasic.editMiddleName.text;
    NSString *suffixName = viewBasic.editSuffix.text;
    
    NSString *lastName = viewBasic.editLastName.text;
    NSString *firstName = viewBasic.editFirstName.text;
    
    if ([midName isEqualToString:@"<null>"])
        midName = @"";
    
    if ([suffixName isEqualToString:@"<null>"])
        suffixName = @"";
    
    if (0 < midName.length)
        self.labelFullName.text = [NSString stringWithFormat:@"%@ %@ %@", firstName, midName, lastName];
    else
        self.labelFullName.text = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    
    if (0 < suffixName.length)
        self.labelFullName.text = [NSString stringWithFormat:@"%@ %@", self.labelFullName.text, suffixName];
}

-(void)clearValues{
    self.labelFullName.text = @"";
    self.textFieldNickName.text = @"";
    self.imageViewAvatar.image = nil;
    self.labelAlias.text = @"";
    
    viewBasic.editDetail1.text = @"";
    viewBasic.editDetail2.text = @"";
    viewBasic.editBirthday.text = @"";
    viewBasic.editSex.text = @"";
    viewBasic.editAddress.text = @"";
    viewBasic.editFacebook.text = @"";
    viewBasic.editTwitter.text = @"";
    viewBasic.editInstagram.text = @"";
    viewBasic.editContact.text = @"";
    viewBasic.editContactPerson.text = @"";
    viewBasic.editBirthPlace.text = @"";
    
    viewOther.editSpokenLanguage.text = @"";
    viewOther.editHobbies.text = @"";
    viewOther.editSports.text = @"";
    viewOther.editAboutme.text = @"";
    viewOther.editNationality.text = @"";
    
    viewSchool.editPreschoolname.text = @"";
    viewSchool.editPreschoolyear.text = @"";
    viewSchool.editPreschooladdress.text = @"";
    viewSchool.editElementaryname.text = @"";
    viewSchool.editElementaryyear.text = @"";
    viewSchool.editElementaryaddress.text = @"";
    viewSchool.editHighschoolname.text = @"";
    viewSchool.editHighschoolyear.text = @"";
    viewSchool.editHighschooladdress.text = @"";
}

-(void)addHMSegmentedControl{
    
    /* localizable strings */
    NSString *basicLabel = NSLocalizedString(@"Basic", nil); //checked
    NSString *otherInfoLabel = NSLocalizedString(@"Other Info", nil); //checked
    NSString *schoolsLabel = NSLocalizedString(@"Schools", nil); //checked
    
    NSArray *headers = nil;
    if (isTeacher) {
        headers = @[basicLabel, otherInfoLabel];
    } else {
        headers = @[basicLabel, otherInfoLabel, schoolsLabel];
    }
    
    /*
     * LEGACY
     *
    self.segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:headers];
    self.segmentedControl.frame = CGRectMake(0, self.segmentedControlPlaceHolder.frame.origin.y, self.view.frame.size.width, 45);
    self.segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    self.segmentedControl.font = FONT_NEUE_LIGHT(15);
    self.segmentedControl.selectedSegmentIndex = 0;
    self.segmentedControl.selectionIndicatorHeight = 4.0f;
    self.segmentedControl.backgroundColor = UIColorFromHex(0x41adbe);
    self.segmentedControl.textColor = [UIColor whiteColor];
    self.segmentedControl.selectedTextColor = [UIColor whiteColor];
    self.segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    self.segmentedControl.selectionLocation = HMSegmentedControlSelectionLocationUp;
     */
    
    self.segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:headers];
    self.segmentedControl.frame = CGRectMake(0, self.segmentedControlPlaceHolder.frame.origin.y, self.view.frame.size.width, 45);
    self.segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    self.segmentedControl.selectedSegmentIndex = 0;
    self.segmentedControl.selectionIndicatorHeight = 4.0f;
    self.segmentedControl.backgroundColor = UIColorFromHex(0x41adbe);
    self.segmentedControl.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor],
                                                   NSFontAttributeName : FONT_NEUE_LIGHT(15) };
    self.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationUp;
    
    [self.segmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.segmentedControl];
}

-(void)addSubviews{
    NSArray *xibArray = [[NSBundle mainBundle] loadNibNamed:@"UpdateProfileViewController" owner:self options:nil];
    for (id xibObject in xibArray) {
        if ([xibObject isKindOfClass:[UserProfileViewBasic class]]) {
            viewBasic = (UserProfileViewBasic *)xibObject;
        }
        if ([xibObject isKindOfClass:[UserProfileViewOther class]]) {
            viewOther = (UserProfileViewOther *)xibObject;
        }
        if ([xibObject isKindOfClass:[UserProfileViewSchool class]]) {
            viewSchool = (UserProfileViewSchool *)xibObject;
        }
    }
    
    CGRect rect = CGRectMake(0, 0, viewDetail.frame.size.width, viewDetail.frame.size.height);
    viewBasic.frame = viewOther.frame = viewSchool.frame = rect;
    
    [viewDetail addSubview:viewBasic];
    [viewDetail addSubview:viewOther];
    
    if (!isTeacher) {
        [viewDetail addSubview:viewSchool];
    }
    
    [viewDetail bringSubviewToFront:viewBasic];
}

-(IBAction)segmentedControlChangedValue:(id)sender{
    
    switch (self.segmentedControl.selectedSegmentIndex) {
        case 0:
        {
            [viewDetail bringSubviewToFront:viewBasic];
        }
            break;
            
        case 1:
        {
            [viewDetail bringSubviewToFront:viewOther];
        }
            break;
            
        case 2:
        {
            [viewDetail bringSubviewToFront:viewSchool];
            
        }
            break;
            
        case 3:
        {
        }
            break;
            
        default:
            break;
    }
}

-(void)initializePhase{
    
    [self clearValues];
    [Utils adjustBorder:textFieldNickName];
    
    self.labelAlias.layer.cornerRadius = self.labelAlias.frame.size.height/2;
    self.imageViewAvatar.layer.cornerRadius = self.imageViewAvatar.frame.size.height/2;
    
    if (!self.account.user.avatar && self.account.user.avatar.length <= 0){
        self.labelAlias.hidden = NO;
        self.imageViewAvatar.hidden = YES;
        self.labelAlias.text = [VSmartHelpers aliasFromFirstName:self.account.user.firstname andLastName:self.account.user.lastname];
        self.labelAlias.backgroundColor = [VSmart sharedInstance].aliasBackgroundColor;
    }
    else{
        NSString *baseURL = [Utils buildUrl:[self account].user.avatar];
        self.labelAlias.hidden = YES;
        self.imageViewAvatar.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:baseURL]]];
        self.imageViewAvatar.hidden = NO;
    }
    
    NSString *url = VS_FMT([Utils buildUrl:kEndPointGetUserProfile], [self account].user.id);
    
    [JSONHTTPClient getJSONFromURLWithString:url completion:^(NSDictionary *json, JSONModelError *err) {
        if(err == nil){
            NSArray *arrayRecords = [json objectForKey:@"records"];
            [VSmartHelpers saveLastUserDefaultProfile:arrayRecords];
            [self fillUIFields:arrayRecords];
        }
        else{
            /* localizable strings */
            NSString *connectionErrorMessage = NSLocalizedString(@"Please make sure you are connected to the server", nil); //checked
            [self.view makeToast:connectionErrorMessage duration:2.0f position:@"center"];
            
//            NSArray *arrayRecords = [VSmartHelpers loadLastUserDefaultProfile];
//            if (arrayRecords)
//                [self fillUIFields:arrayRecords];
        }
    }];
}

-(void)fillUIFields:(NSArray*)arrayRecords{
    
    self.labelFullName.text = @"";
    self.textFieldNickName.text = @"";
    self.labelAlias.text = @"";
    
    NSDictionary *dictProfile;
    NSDictionary *dictSection;
    NSArray *arrayAdvisoryClasses;
    NSArray *arraySubjects;
    
    for (NSDictionary *dict in arrayRecords) {
        
        //Profile
        if ([dict objectForKey:@"Profile"]) {
            dictProfile = [[dict objectForKey:@"Profile"] objectAtIndex:0];
            
            //NSString *avatar = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"avatar"]];
            //NSString *firstName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"first_name"]];
            //NSString *lastName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"last_name"]];
            //NSString *midName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"mid_name"]];
            //NSString *suffixName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"suffix_name"]];
            //
            //NSString *nickname = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"nickname"]];
            //NSString *birthday = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"birth_date"]];
            //NSString *sex = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"gender"]];
            //NSString *address = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"address"]];
            //NSString *facebook = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"facebook"]];
            //NSString *twitter = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"twitter"]];
            //NSString *instagram = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"instagram"]];
            //NSString *contact = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"contact"]];
            //NSString *contactPerson = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"contact_person"]];
            //NSString *birthPlace = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"birth_place"]];
            
            // Bug Fix: Displaying of null values
            // jca-04222016
            NSString *avatar = [self stringValue:[dictProfile objectForKey:@"avatar"]];
            NSString *firstName = [self stringValue:[dictProfile objectForKey:@"first_name"]];
            NSString *lastName = [self stringValue:[dictProfile objectForKey:@"last_name"]];
            NSString *midName = [self stringValue:[dictProfile objectForKey:@"mid_name"]];
            NSString *suffixName = [self stringValue:[dictProfile objectForKey:@"suffix_name"]];
            
            NSString *nickname = [self stringValue:[dictProfile objectForKey:@"nickname"]];
            NSString *birthday = [self stringValue:[dictProfile objectForKey:@"birth_date"]];
            NSString *sex = [self stringValue:[dictProfile objectForKey:@"gender"]];
            NSString *address = [self stringValue:[dictProfile objectForKey:@"address"]];
            NSString *facebook = [self stringValue:[dictProfile objectForKey:@"facebook"]];
            NSString *twitter = [self stringValue:[dictProfile objectForKey:@"twitter"]];
            NSString *instagram = [self stringValue:[dictProfile objectForKey:@"instagram"]];
            NSString *contact = [self stringValue:[dictProfile objectForKey:@"contact"]];
            NSString *contactPerson = [self stringValue:[dictProfile objectForKey:@"contact_person"]];
            NSString *birthPlace = [self stringValue:[dictProfile objectForKey:@"birth_place"]];
            
            //if ([midName isEqualToString:@"<null>"])
            //    midName = @"";
            //
            //if ([suffixName isEqualToString:@"<null>"])
            //    suffixName = @"";
            
            if (0 < midName.length)
                self.labelFullName.text = [NSString stringWithFormat:@"%@ %@ %@", firstName, midName, lastName];
            else
                self.labelFullName.text = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
            
            if (0 < suffixName.length)
                self.labelFullName.text = [NSString stringWithFormat:@"%@ %@", self.labelFullName.text, suffixName];
            
            if (avatar.length <= 0){
                self.labelAlias.hidden = NO;
                self.imageViewAvatar.hidden = YES;
                self.labelAlias.text = [VSmartHelpers aliasFromFirstName:firstName andLastName:lastName];
                self.labelAlias.backgroundColor = [VSmart sharedInstance].aliasBackgroundColor;
            }
            else{
                self.labelAlias.hidden = YES;
                self.imageViewAvatar.hidden = NO;
            }
            
            self.textFieldNickName.text = IsEmpty(nickname) ? @"" : nickname;
            
            viewBasic.editBirthday.text = IsEmpty(birthday) ? @"" : birthday;
            viewBasic.editSex.text = IsEmpty(sex) ? @"" : sex;
            viewBasic.editAddress.text = IsEmpty(address) ? @"" : address;
            viewBasic.editFacebook.text = IsEmpty(facebook) ? @"" : facebook;
            viewBasic.editTwitter.text = IsEmpty(twitter) ? @"" : twitter;
            viewBasic.editInstagram.text = IsEmpty(instagram) ? @"" : instagram;
            viewBasic.editContact.text = IsEmpty(contact) ? @"" : contact;
            viewBasic.editContactPerson.text = IsEmpty(contactPerson) ? @"" : contactPerson;
            viewBasic.editBirthPlace.text = IsEmpty(birthPlace) ? @"" : birthPlace;
            
            //NSString *spokenLanguage = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"spoken_language"]];
            //NSString *hobbies = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"hobbies"]];
            //NSString *sports = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"sports"]];
            //NSString *aboutMe = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"about_me"]];
            //NSString *nationality = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"nationality"]];
            
            NSString *spokenLanguage = [self stringValue:[dictProfile objectForKey:@"spoken_language"]];
            NSString *hobbies = [self stringValue:[dictProfile objectForKey:@"hobbies"]];
            NSString *sports = [self stringValue:[dictProfile objectForKey:@"sports"]];
            NSString *aboutMe = [self stringValue:[dictProfile objectForKey:@"about_me"]];
            NSString *nationality = [self stringValue:[dictProfile objectForKey:@"nationality"]];
            
            viewOther.editSpokenLanguage.text = IsEmpty(spokenLanguage) ? @"" : spokenLanguage;
            viewOther.editHobbies.text = IsEmpty(hobbies) ? @"" : hobbies;
            viewOther.editSports.text = IsEmpty(sports) ? @"" : sports;
            viewOther.editNationality.text = IsEmpty(nationality) ? @"" : nationality;
            viewOther.editAboutme.text = IsEmpty(aboutMe) ? @"" : aboutMe;
            
            //NSString *preSchoolAddress = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"pre_school_address"]];
            //NSString *preSchoolName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"pre_school_name"]];
            //NSString *preSchoolYear = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"pre_school_year"]];
            //
            //NSString *elemSchoolAddress = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"elem_school_address"]];
            //NSString *elemSchoolName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"elem_school_name"]];
            //NSString *elemSchoolYear = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"elem_school_year"]];
            //
            //NSString *highSchoolAddress = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"high_school_address"]];
            //NSString *highSchoolName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"high_school_name"]];
            //NSString *highSchoolYear = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"high_school_year"]];
            
            NSString *preSchoolAddress = [self stringValue:[dictProfile objectForKey:@"pre_school_address"]];
            NSString *preSchoolName = [self stringValue:[dictProfile objectForKey:@"pre_school_name"]];
            NSString *preSchoolYear = [self stringValue:[dictProfile objectForKey:@"pre_school_year"]];
            
            NSString *elemSchoolAddress = [self stringValue:[dictProfile objectForKey:@"elem_school_address"]];
            NSString *elemSchoolName = [self stringValue:[dictProfile objectForKey:@"elem_school_name"]];
            NSString *elemSchoolYear = [self stringValue:[dictProfile objectForKey:@"elem_school_year"]];
            
            NSString *highSchoolAddress = [self stringValue:[dictProfile objectForKey:@"high_school_address"]];
            NSString *highSchoolName = [self stringValue:[dictProfile objectForKey:@"high_school_name"]];
            NSString *highSchoolYear = [self stringValue:[dictProfile objectForKey:@"high_school_year"]];
            
            viewSchool.editPreschoolname.text = IsEmpty(preSchoolName) ? @"" : preSchoolName;
            viewSchool.editPreschoolyear.text =  IsEmpty(preSchoolYear) ? @"" : preSchoolYear;
            viewSchool.editPreschooladdress.text = IsEmpty(preSchoolAddress) ? @"" : preSchoolAddress;
            
            viewSchool.editElementaryname.text = IsEmpty(elemSchoolName) ? @"" : elemSchoolName;
            viewSchool.editElementaryyear.text = IsEmpty(elemSchoolYear) ? @"" : elemSchoolYear;
            viewSchool.editElementaryaddress.text = IsEmpty(elemSchoolAddress) ? @"" : elemSchoolAddress;
            
            viewSchool.editHighschoolname.text = IsEmpty(highSchoolName) ? @"" : highSchoolName;
            viewSchool.editHighschoolyear.text = IsEmpty(highSchoolYear) ? @"" : highSchoolYear;
            viewSchool.editHighschooladdress.text = IsEmpty(highSchoolAddress) ? @"" : highSchoolAddress;
    
            continue;
        }
        
        //Section
        if ([dict objectForKey:@"Section"]) {
            dictSection = [[dict objectForKey:@"Section"] objectAtIndex:0];
            
            //NSString *firstName = [NSString stringWithFormat:@"%@", [dictSection objectForKey:@"adviser_first_name"]];
            //NSString *lastName = [NSString stringWithFormat:@"%@", [dictSection objectForKey:@"adviser_last_name"]];
            //NSString *midName = [NSString stringWithFormat:@"%@", [dictSection objectForKey:@"adviser_middle_name"]];
            //NSString *suffixName = [NSString stringWithFormat:@"%@", [dictSection objectForKey:@"adviser_suffix_name"]];
            //NSString *grade = [NSString stringWithFormat:@"%@", [dictSection objectForKey:@"grade"]];
            //NSString *section = [NSString stringWithFormat:@"%@", [dictSection objectForKey:@"section"]];
            
            NSString *firstName = [self stringValue:[dictSection objectForKey:@"adviser_first_name"]];
            NSString *lastName = [self stringValue:[dictSection objectForKey:@"adviser_last_name"]];
            NSString *midName = [self stringValue:[dictSection objectForKey:@"adviser_middle_name"]];
            NSString *suffixName = [self stringValue:[dictSection objectForKey:@"adviser_suffix_name"]];
            NSString *grade = [self stringValue:[dictSection objectForKey:@"grade"]];
            NSString *section = [self stringValue:[dictSection objectForKey:@"section"]];
            
            midName = IsEmpty(midName) ? @"" : midName;
            suffixName = IsEmpty(suffixName) ? @"" : suffixName;
            
            NSString *fullName;
            
            if (0 < midName.length)
                fullName = [NSString stringWithFormat:@"%@ %@ %@", firstName, midName, lastName];
            else
                fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
            
            if (0 < suffixName.length)
                fullName = [NSString stringWithFormat:@"%@ %@", fullName, suffixName];
            
            
            viewBasic.editDetail1.text = [NSString stringWithFormat:@"%@ - %@", grade, section];
            
            continue;
        }
        
        
        //Advisory Classes
        if ([dict objectForKey:@"Advisory_Classes"]) {
            arrayAdvisoryClasses = [dict objectForKey:@"Advisory_Classes"];
            
            NSString *advisoryClass = @"";
            
            for (NSDictionary *dict in arrayAdvisoryClasses) {
                
                if (advisoryClass.length <= 0)
                    //advisoryClass = [NSString stringWithFormat:@"%@", [dict objectForKey:@"section"]];
                    advisoryClass = [self stringValue:[dict objectForKey:@"section"]];
                else
                    //advisoryClass = [NSString stringWithFormat:@"%@, %@", advisoryClass, [dict objectForKey:@"section"]];
                    advisoryClass = [NSString stringWithFormat:@"%@, %@", advisoryClass, [self stringValue:[dict objectForKey:@"section"]]];
            }
            
            continue;
        }
        
        //Subjects
        if ([dict objectForKey:@"Subjects"]) {
            arraySubjects = [dict objectForKey:@"Subjects"];
            
            for (NSDictionary *dict in arraySubjects) {
                
                if (viewBasic.editDetail2.text <= 0)
                    //viewBasic.editDetail2.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"name"]];
                    viewBasic.editDetail2.text = [self stringValue:[dict objectForKey:@"name"]];
                else
                    //viewBasic.editDetail2.text = [NSString stringWithFormat:@"%@, %@", viewBasic.editDetail2.text, [dict objectForKey:@"name"]];
                    viewBasic.editDetail2.text = [NSString stringWithFormat:@"%@, %@", [self stringValue:viewBasic.editDetail2.text], [self stringValue:[dict objectForKey:@"name"]]];
            }
            
            continue;
        }
        
        if ([dict objectForKey:@"Badges"]) {
            continue;
        }
    }
    
    
    if ([[self account].user.position isEqualToString:kModeIsStudent]){
    }
}

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

@end
