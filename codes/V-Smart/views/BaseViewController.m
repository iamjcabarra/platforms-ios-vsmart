//
//  BaseViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/16/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

//#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#import "BaseViewController.h"
#import "CustomBadge.h"
#import "AboutViewController.h"
#import "ResourceManager.h"
#import "MainHeader.h"
#import "V_Smart-Swift.h"
#import "GlobalNotificationList.h"

@interface BaseViewController () <UIPopoverPresentationControllerDelegate>

@property (nonatomic, strong) UILabel *platinumLabelCount;
@property (nonatomic, strong) UILabel *goldLabelCount;
@property (nonatomic, strong) UILabel *silverLabelCount;
@property (nonatomic, strong) UILabel *bronzeLabelCount;
// custom
@property (nonatomic, strong) UIView *baseHeaderView;
@property (nonatomic, strong) UIImageView *imageView;

@property (nonatomic, strong) UILabel *popupNameLabel;
//@property (nonatomic, strong) UILabel *avatarNameLabel;

@property (nonatomic, strong) UIButton *globeButton;
@property (nonatomic, strong) CustomBadge *badgeObject;

// BUG FIX
@property (nonatomic, strong) ResourceManager *rm;
@property (nonatomic, strong) UIView *medalView;

@property (nonatomic, strong) GNListViewController *globalNotifListV2;

@property (assign, nonatomic) BOOL isVersion25;

@property (nonatomic, strong) UIButton *homeButton;

@property (nonatomic, strong) GNDataManager *gnm;

@end

@implementation BaseViewController
@synthesize popoverController;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.rm = [AppDelegate resourceInstance];
    self.gnm = [GNDataManager sharedInstance];
    
    profileImageView = [[EGOImageView alloc] initWithPlaceholderImage:IMAGE_DEFAULT_PROFILE];
    profileImageView.delegate = self;
    
    [self setupBaseViews];
    [self setupToolbarItems];
    [self setupJumpMenuItems];
    
    self.view.backgroundColor = UIColorFromHex(0xeeeeee);
	// Do any additional setup after loading the view.
    [self profileHeight];
    
    //NSString *uuidTest = [[[UIDevice currentDevice] uniqueGlobalDeviceIdentifier] copy];
    //NSLog(@"UUID: %@", uuidTest);
    
    VS_NCADD(kNotificationProfileReload, @selector(reloadAvatar) )
    VS_NCADD(kNotificationGlobalReadUpdates, @selector(updateNotifCount) )
    VS_NCADD(@"NOTIFICATION_UPDATE_NOTIFICATION_BADGE", @selector(updateNotifCountV2) )
    VS_NCADD(kNotificationHideProfileView, @selector(hideProfileViewForPlayList) )
    
    // iOS 7: 14:10:9F:E5:F8:FB
    [self refreshMedals];
    
    
    CGFloat version = [[Utils getServerInstanceVersion] floatValue];
    self.isVersion25 = (version >= VSMART_SERVER_MAX_VER);
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"GNStoryBoard" bundle:nil];
    self.globalNotifListV2 = (GNListViewController *)[sb instantiateViewControllerWithIdentifier:@"GNListView"];
    self.globalNotifListV2.modalPresentationStyle = UIModalPresentationPopover;
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self.navigationController setToolbarHidden:NO animated:animated];
    [super viewWillAppear:animated];
    
    if (self.isVersion25) {
        if (self.badgeObject == nil) {
            self.badgeObject = [CustomBadge customBadgeWithString:@"0" withStyle:[BadgeStyle oldStyle]];
            [self.globeButton addSubview:self.badgeObject];
        }
            [self updateNotifCountV2];
    }
    
    
}

- (void) updateNotifCountV2 {
    
    id unseen_count = [self.gnm fetchUserDefaultsObjectForKey:@"GN_USER_DEFINED_UNSEEN_COUNT"];
    NSString *unseen_count_string = [NSString stringWithFormat:@"%@", unseen_count];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if ((unseen_count == nil) || [unseen_count_string isEqualToString:@"0"]) {
            self.badgeObject.hidden = YES;
        } else {
            self.badgeObject.hidden = NO;
            [self.badgeObject autoBadgeSizeWithString:unseen_count_string];
//            self.badgeObject.badgeText = unseen_count_string;
//            [self.badgeObject setNeedsDisplay];
        }
    });
    
    
}


// GLOBAL NOTIFICATION
- (void)performSettingsAction:(UIButton *)sender {
    CGFloat version = [[Utils getServerInstanceVersion] floatValue];
    self.isVersion25 = (version >= VSMART_SERVER_MAX_VER);
    
    if (self.isVersion25) {
        UIPopoverPresentationController *popover =
        popover = self.globalNotifListV2.popoverPresentationController;
        popover.sourceView = sender;
        popover.sourceRect = sender.bounds;
        popover.delegate = self;
//        popover.permittedArrowDirections = 0;
        [self presentViewController:self.globalNotifListV2 animated:true completion:nil];
    } else {
//        //storyboard
        UIViewController *v = [self loadViewControllerWithIdentifier:@"GlobalNotifShortcut"];
        [self presentViewController:v animated:YES completion:nil];
    }
    
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

- (float) profileHeight {
    float height = VS_GET_FLOAT(kProfileHeight);
    //VLog(@"Height: %.02f", height);
    return height;
}

-(void) setProfileHeight:(float)profileHeight {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setFloat:profileHeight forKey:kProfileHeight];
    [userDefaults synchronize];
    
    VLog(@"Saving New Height: %.02f", profileHeight);
    
    VS_NCPOST(kNotificationProfileHeight)
}

-(void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

-(AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

-(void) hideJumpMenu: (BOOL) hide {
    if (hide) {
        [self.jumpMenu hide];
    }
    else {
        [self.jumpMenu fadeIn];
    }
}

-(NSString *) dashboardName {
    return @"Dashboard";
}

-(CGRect) toolbarSize {
    return self.navigationController.toolbar.frame;
}

-(CGRect) headerSize {
    return self.baseHeaderView.frame;
}

-(CGRect) profileSize {
    return self.baseProfileView.frame;
}

-(void) showOrHideMiniAvatar {
    float profileY = [self profileHeight];
    if (profileY == 0.0) {
        [self.avatarSmallView show];
    } else {
        [self.avatarSmallView hide];
    }
}

-(void) adjustProfileHeight {
    float profileY = [self profileHeight];
    CGRect frame = self.baseProfileView.frame;
    frame.origin.y = - (218.0f - 92.0f);
    
    if (profileY == 0.0) {
        [self.baseProfileView setFrame:frame];
    } else {
        [self.baseProfileView setFrame:CGRectMake(0, profileY, self.view.frame.size.width, 218.0)];
    }
}

-(void) reloadAvatar {
    if ([self account]) {
        VLog(@"reloadAvatar");
        NSString *baseURL = [Utils buildUrl:[self account].user.avatar];
        NSURL *imageURL = [NSURL URLWithString:baseURL];
        [profileImageView setImageURL:imageURL];
        
        [self updateProfile];
        
        //self.accountName.text = VS_FMT(@"%@ %@", self.account.user.firstname, self.account.user.lastname);
        self.accountType.text = VS_FMT(@"%@", [[NSString stringWithFormat:@"%@", self.account.user.position] capitalizedString]); // ENHANCEMENT#137: (120215)
        self.accountEmail.text = VS_FMT(@"%@", self.account.user.email); // ENHANCEMENT#137: (120215)
        
        //[self.accountName setNeedsDisplay];
        [self.accountType setNeedsDisplay];     // ENHANCEMENT#137: (120215)
        [self.accountEmail setNeedsDisplay];    // ENHANCEMENT#137: (120215)
        
        // ENHANCEMENT#138: Teacher should not have badges shown in his/her base profile view (120215)
        // Removed buttons (medal badges) from base profile view
        NSArray *subviews = [self.baseProfileView subviews];
        for (UIView *subview in subviews) {
            // Since there's no other button in base profile view except for medal badges (tested)
            if ([subview isKindOfClass:[UIButton class]]) {
                [subview removeFromSuperview];
            }
            
            // Remove medal view's border
            if ([subview isEqual:self.medalView]) {
                [subview removeFromSuperview];
                self.medalView = nil;
            }
        }
        
        // Remove labels (medal badges) from base profile view
        [self.platinumLabelCount removeFromSuperview];
        [self.goldLabelCount removeFromSuperview];
        [self.silverLabelCount removeFromSuperview];
        [self.bronzeLabelCount removeFromSuperview];
        
        // Re-initiate medal badges view
        [self setupProfileMedals];
    }
}

#pragma mark - Delegate Actions

-(void) didCloseProfileMedalWindow:(ProfileMedalViewController *)popViewController {
    //VLog(@"closing...");
    [self dismissPopupViewControllerWithanimationType:PopupViewAnimationSlideBottomTop];
}

#pragma mark - Private Helpers

-(void) popProfileMedalView: (NSString *) medalType {
    VLog(@"Medal: %@", medalType);
    popProfileMedalViewController = [[ProfileMedalViewController alloc] initWithNibName:@"ProfileMedalViewController" bundle:nil medalType:medalType];
    popProfileMedalViewController.view.frame = popProfileMedalViewController.view.frame;
    popProfileMedalViewController.delegate = self;

    [self presentPopupViewController:popProfileMedalViewController animationType:PopupViewAnimationSlideBottomTop];
}

-(void) setupProfileMedals {
//    CGRect rect = CGRectMake(263.0, 78, 22, 30);
//    UIButton *platinumButton = [UIButton buttonWithImage:[UIImage imageNamed:@"platinum1"] inFrame:rect forTarget:self withAction:@selector(medalAction:)];
//    platinumButton.tag = 0;
//    [self.baseProfileView addSubview:platinumButton];
//    
//    rect = CGRectMake(337.0, 78, 22, 30);
//    UIButton *goldButton = [UIButton buttonWithImage:[UIImage imageNamed:@"gold1"] inFrame:rect forTarget:self withAction:@selector(medalAction:)];
//    goldButton.tag = 1;
//    [self.baseProfileView addSubview:goldButton];
//    
//    rect = CGRectMake(409.0, 78, 22, 30);
//    UIButton *silverButton = [UIButton buttonWithImage:[UIImage imageNamed:@"silver1"] inFrame:rect forTarget:self withAction:@selector(medalAction:)];
//    silverButton.tag = 2;
//    [self.baseProfileView addSubview:silverButton];
//    
//    rect = CGRectMake(483.0, 78, 22, 30);
//    UIButton *bronzeButton = [UIButton buttonWithImage:[UIImage imageNamed:@"bronze1"] inFrame:rect forTarget:self withAction:@selector(medalAction:)];
//    bronzeButton.tag = 3;
//    [self.baseProfileView addSubview:bronzeButton];
//    
//    rect = CGRectMake(287.0, 82.0, 28, 21);
//    self.platinumLabelCount = [self createMedalLabel:self.platinumLabelCount inFrame:rect];
//    [self.baseProfileView addSubview:self.platinumLabelCount];
//
//    rect = CGRectMake(363.0, 82.0, 28, 21);
//    self.goldLabelCount = [self createMedalLabel:self.goldLabelCount inFrame:rect];
//    [self.baseProfileView addSubview:self.goldLabelCount];
//    
//    rect = CGRectMake(433.0, 82.0, 28, 21);
//    self.silverLabelCount = [self createMedalLabel:self.silverLabelCount inFrame:rect];
//    [self.baseProfileView addSubview:self.silverLabelCount];
//    
//    rect = CGRectMake(509.0, 82.0, 28, 21);
//    self.bronzeLabelCount = [self createMedalLabel:self.bronzeLabelCount inFrame:rect];
//    [self.baseProfileView addSubview:self.bronzeLabelCount];
    
    // ENHANCEMENT#138: Teacher should not have badges shown in his/her base profile view (120215)
    NSString *position = VS_FMT(@"%@", self.account.user.position);
    
    if  ([position isEqualToString:@"student"]) {
        // ENHANCEMENT#137: Adjust y position (120215)
        // CGRect rect = CGRectMake(263.0, 120, 22, 30);
        
        // Enhancement
        // jca-05052016
        // Add border for medal view
        CGRect rect = CGRectMake(260.0, 115, 300, 40);
        self.medalView = [[UIView alloc] initWithFrame:rect];
        self.medalView.backgroundColor = [UIColor clearColor];
        self.medalView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.medalView.layer.borderWidth = 1.0f;
        [self.baseProfileView addSubview:self.medalView];
        
        rect = CGRectMake(263.0, 120, 22, 30);
    UIButton *platinumButton = [UIButton buttonWithImage:[UIImage imageNamed:@"platinum1"] inFrame:rect forTarget:self withAction:@selector(medalAction:)];
    platinumButton.tag = 0;
    [self.baseProfileView addSubview:platinumButton];
    
        rect = CGRectMake(337.0, 120, 22, 30);
    UIButton *goldButton = [UIButton buttonWithImage:[UIImage imageNamed:@"gold1"] inFrame:rect forTarget:self withAction:@selector(medalAction:)];
    goldButton.tag = 1;
    [self.baseProfileView addSubview:goldButton];
    
        rect = CGRectMake(409.0, 120, 22, 30);
    UIButton *silverButton = [UIButton buttonWithImage:[UIImage imageNamed:@"silver1"] inFrame:rect forTarget:self withAction:@selector(medalAction:)];
    silverButton.tag = 2;
    [self.baseProfileView addSubview:silverButton];
    
        rect = CGRectMake(483.0, 120, 22, 30);
    UIButton *bronzeButton = [UIButton buttonWithImage:[UIImage imageNamed:@"bronze1"] inFrame:rect forTarget:self withAction:@selector(medalAction:)];
    bronzeButton.tag = 3;
    [self.baseProfileView addSubview:bronzeButton];
    
        rect = CGRectMake(290.0, 124, 28, 21);
    self.platinumLabelCount = [self createMedalLabel:self.platinumLabelCount inFrame:rect];
    [self.baseProfileView addSubview:self.platinumLabelCount];

        rect = CGRectMake(366.0, 124, 28, 21);
    self.goldLabelCount = [self createMedalLabel:self.goldLabelCount inFrame:rect];
    [self.baseProfileView addSubview:self.goldLabelCount];
    
        rect = CGRectMake(436.0, 124, 28, 21);
    self.silverLabelCount = [self createMedalLabel:self.silverLabelCount inFrame:rect];
    [self.baseProfileView addSubview:self.silverLabelCount];
    
        rect = CGRectMake(512.0, 124, 28, 21);
    self.bronzeLabelCount = [self createMedalLabel:self.bronzeLabelCount inFrame:rect];
    [self.baseProfileView addSubview:self.bronzeLabelCount];
}
}

-(UILabel *) createMedalLabel: (UILabel *) label inFrame:(CGRect) frame  {
    label = [[UILabel alloc] initWithFrame:frame];
    label.font = [UIFont fontWithName:@"Helvetica" size:17.0f];
    label.textAlignment = UITextAlignmentLeft;
    label.textColor = [UIColor darkGrayColor];
    label.text = @"0";
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    return label;
}


-(IBAction)updateProfile{ //this is triggered from Update Profile View Controller
    // Bug #1152
    // jca-04222016
    NSString *firstName = [self stringValue:self.account.user.firstname];
    NSString *lastName = [self stringValue:self.account.user.lastname];
    NSString *suffix = [self stringValue:self.account.user.suffix];
    
    // Bug #1345
    // jca-05132016
    // Show user's middle name
    NSString *middleName = [self stringValue:self.account.user.middlename];
    
    //self.accountName.text = VS_FMT(@"%@ %@", self.account.user.firstname, self.account.user.lastname);
    //self.accountName.text = VS_FMT(@"%@ %@ %@", firstName, lastName, suffix);
    
    self.accountName.text = VS_FMT(@"%@ %@ %@ %@", firstName, middleName, lastName, suffix);
    if ([middleName isEqualToString:@""]) {
        self.accountName.text = VS_FMT(@"%@ %@ %@", firstName, lastName, suffix);
    }
    
    //self.avatarSmallView.labelAlias.text = [VSmartHelpers aliasFromFirstName:self.account.user.firstname andLastName:self.account.user.lastname];
    self.avatarSmallView.labelAlias.text = [VSmartHelpers aliasFromFirstName:firstName andLastName:lastName];
    //self.popupNameLabel.text = VS_FMT(@"%@ %@", self.account.user.firstname, self.account.user.lastname);
    self.popupNameLabel.text = VS_FMT(@"%@ %@", firstName, lastName);
    
    UIImage *imageDefault = [UIImage imageNamed:@"default-photo"];
    if ([profileImageView.image imageIsEqualTo:imageDefault] && [self account]){
        self.avatarView.isUsingAlias = YES;
        self.avatarSmallView.isUsingAlias = YES;
    }
    
    if (self.avatarView.isUsingAlias){
        self.avatarView.labelAlias.backgroundColor = [VSmart sharedInstance].aliasBackgroundColor;
        self.avatarView.labelAlias.text = [VSmartHelpers aliasFromFirstName:self.account.user.firstname andLastName:self.account.user.lastname];
        [self.avatarView useAliasWithBigFont:YES];
        
        self.avatarSmallView.labelAlias.text = [VSmartHelpers aliasFromFirstName:self.account.user.firstname andLastName:self.account.user.lastname];
        self.avatarSmallView.labelAlias.backgroundColor = [VSmart sharedInstance].aliasBackgroundColor;
        [self.avatarSmallView useAliasWithBigFont:NO];
    }
}

-(void) setupBaseViews {
    VLog(@"setupBaseViews");
    
    float profileY = [self profileHeight];
    CGRect frame = self.baseProfileView.frame;
    frame.origin.y = - (218.0f - 92.0f);
    
    self.baseProfileView = [[UIView alloc] initWithFrame:CGRectMake(0, 92.0f, self.view.frame.size.width, 218.0)];
    
    if (profileY == 0.0) {
        [self.baseProfileView setFrame:frame];
    }
    
    self.accountName = [[UILabel alloc] initWithFrame:CGRectMake(263.f, 26.f, 485, 44)];
    //name.backgroundColor = UIColorFromHex(0x9e3908);
    self.accountName.font = [UIFont fontWithName:@"Helvetica" size:25.0f];
    self.accountName.textAlignment = UITextAlignmentLeft;
    self.accountName.textColor = [UIColor darkGrayColor];
    
    // Bug #1152
    // jca-04222016
    NSString *firstName = [self stringValue:self.account.user.firstname];
    NSString *lastName = [self stringValue:self.account.user.lastname];
    NSString *suffix = [self stringValue:self.account.user.suffix];
    
    // Bug #1345
    // jca-05132016
    // Show user's middle name
    NSString *middleName = [self stringValue:self.account.user.middlename];
    
    //self.accountName.text = VS_FMT(@"%@ %@", self.account.user.firstname, self.account.user.lastname);
    //self.accountName.text = VS_FMT(@"%@ %@ %@", firstName, lastName, suffix);
    
    self.accountName.text = VS_FMT(@"%@ %@ %@ %@", firstName, middleName, lastName, suffix);
    if ([middleName isEqualToString:@""]) {
        self.accountName.text = VS_FMT(@"%@ %@ %@", firstName, lastName, suffix);
    }
    
    self.accountName.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    //self.baseProfileView = [[UIView alloc] initWithFrame:CGRectMake(0, 92.0, self.view.frame.size.width, 218.0)];
    [self.view addSubview:self.baseProfileView];
    [self.baseProfileView addSubview:self.accountName];
    [self.baseProfileView setBackgroundColor:[UIColor whiteColor]];
    
    // ENHANCEMENT#137: Including type of account (position) and email address in base profile view (120215)
    self.accountType = [[UILabel alloc] initWithFrame:CGRectMake(263.f, 52.f, 485, 44)];
    self.accountType.font = [UIFont fontWithName:@"Helvetica" size:17.0f];
    self.accountType.textAlignment = UITextAlignmentLeft;
    self.accountType.textColor = [UIColor darkGrayColor];
    
    NSString *position = [self stringValue:self.account.user.position];
    //self.accountType.text = VS_FMT(@"%@", [[NSString stringWithFormat:@"%@", self.account.user.position] capitalizedString]);
    self.accountType.text = VS_FMT(@"%@", [[NSString stringWithFormat:@"%@", position] capitalizedString]);
    self.accountType.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [self.view addSubview:self.baseProfileView];
    [self.baseProfileView addSubview:self.accountType];
    [self.baseProfileView setBackgroundColor:[UIColor whiteColor]];
    
    self.accountEmail = [[UILabel alloc] initWithFrame:CGRectMake(263.f, 78.f, 485, 44)];
    self.accountEmail.font = [UIFont fontWithName:@"Helvetica" size:17.0f];
    self.accountEmail.textAlignment = UITextAlignmentLeft;
    self.accountEmail.textColor = [UIColor darkGrayColor];
    
    NSString *email = [self stringValue:self.account.user.email];
    //self.accountEmail.text = VS_FMT(@"%@", self.account.user.email);
    self.accountEmail.text = VS_FMT(@"%@", email);
    self.accountEmail.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [self.view addSubview:self.baseProfileView];
    [self.baseProfileView addSubview:self.accountEmail];
    [self.baseProfileView setBackgroundColor:[UIColor whiteColor]];
    
    [self setupProfileMedals];
    
    UIImageView *baseProfileImage = [[UIImageView alloc] initWithFrame:CGRectMake(56.0, 26.0, 166.0, 166.0)];
    baseProfileImage.contentMode = UIViewContentModeCenter;
    [baseProfileImage setImage:IMAGE_DEFAULT_PROFILE];
    [self.baseProfileView addSubview:baseProfileImage];
    
    if ([self account]) {
        NSURL *imageURL = [NSURL URLWithString:[Utils buildUrl:[self account].user.avatar]];
        [profileImageView setImageURL:imageURL];
    }
    
    //UIImage *image = [UIImage imageNamed:@"avatar"];
    
    CGRect rect = CGRectMake(0, 0, 160, 160);
    self.avatarView = [[AvatarView alloc] initWithFrame:CGRectInset(rect, 4, 4) applyShadow:NO];
    self.avatarView.backgroundColor = [UIColor clearColor];
    [self.avatarView setImage:profileImageView.image];
    
    /////////////////////check if image loaded is default
    UIImage *imageDefault = [UIImage imageNamed:@"default-photo"];
    if ([profileImageView.image imageIsEqualTo:imageDefault] && [self account]){
        VLog(@"equal image");
        //self.avatarView.labelAlias.text = [VSmartHelpers aliasFromFirstName:self.account.user.firstname andLastName:self.account.user.lastname];
        self.avatarView.labelAlias.text = [VSmartHelpers aliasFromFirstName:firstName andLastName:lastName];
        self.avatarView.labelAlias.backgroundColor = [VSmart sharedInstance].aliasBackgroundColor;
        [self.avatarView useAliasWithBigFont:YES];
    }
    
    [self.avatarView setNeedsDisplay];
    
    self.avatarView.tag = 10;
    self.avatarView.center = baseProfileImage.center;

    [self.baseProfileView addSubview:self.avatarView];
    //[self.baseProfileView setBackgroundColor:[UIColor azure]];
    
    self.baseHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 92.0)];
    [self.view addSubview:self.baseHeaderView];
    self.baseHeaderView.backgroundColor = [UIColor colorWithPatternImage:IMAGE_HEADER];
    
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.baseHeaderView.frame.size.width, self.baseHeaderView.frame.size.height)];
//    imageView.contentMode = UIViewContentModeCenter;
//    [imageView setImage:VSMART_LOGO];
//    [self.baseHeaderView addSubview:imageView];
    
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.baseHeaderView.frame.size.width, self.baseHeaderView.frame.size.height)];
    self.imageView.contentMode = UIViewContentModeCenter;
    [self.imageView setImage:VSMART_LOGO];
    [self.baseHeaderView addSubview:self.imageView];
    
    rect = CGRectMake(693, 18, 55, 55);
    self.avatarSmallView = [[AvatarView alloc] initWithFrame:CGRectInset(rect, 4, 4) applyShadow:NO];
    self.avatarSmallView.backgroundColor = [UIColor clearColor];
    [self.avatarSmallView setImage:profileImageView.image];
    self.avatarSmallView.tag = 20;
    
    /////////////////////check if image loaded is default
    imageDefault = [UIImage imageNamed:@"default-photo"];
    if ([profileImageView.image imageIsEqualTo:imageDefault] && [self account]){
        VLog(@"equal image");
        //self.avatarSmallView.labelAlias.text = [VSmartHelpers aliasFromFirstName:self.account.user.firstname andLastName:self.account.user.lastname];
        self.avatarSmallView.labelAlias.text = [VSmartHelpers aliasFromFirstName:firstName andLastName:lastName];
        self.avatarSmallView.labelAlias.backgroundColor = [VSmart sharedInstance].aliasBackgroundColor;
        [self.avatarSmallView useAliasWithBigFont:NO];
    }
    
    [self.avatarSmallView setNeedsDisplay];
    [self.baseHeaderView addSubview:self.avatarSmallView];
    
    
    _jumpMenu = [UIButton buttonWithLabel:VS_FMT(@"%@ ﹀", [self dashboardName])
                                  inFrame: CGRectMake(0, 0, 234.0, 102.0)
                                forTarget:self
                               withAction:@selector(jumpMenuAction:)];
    [self.baseHeaderView addSubview:_jumpMenu];
    
    VS_ENABLE_TAP(self.avatarView, self, @selector(handleAvatarChangeAction:));
    VS_ENABLE_TAP(self.avatarSmallView, self, @selector(handleAvatarSmallAction:));
}

-(void) imageViewLoadedImage:(EGOImageView *)imageView {
    [self.avatarView setImage:imageView.image];
    [self.avatarSmallView setImage:imageView.image];
    [self.view setNeedsLayout];
}

-(NSArray *) _buildJumpModules {
    
    NSArray *currentModule = nil;
    
    NSArray *modules = @[
//                         @{@"module_name": kModuleTextbooks, @"image": kModuleTextbooksJumpIcon, @"badge": @"0"},
//                         @{@"module_name": kModuleGradebook, @"image": kModuleGradebookJumpIcon, @"badge": @"10"},
//                         @{@"module_name": kModuleNotes, @"image": kModuleNotesJumpIcon, @"badge": @"20"},
//                         @{@"module_name": kModuleSocialStream, @"image": kModuleSocialStreamJumpIcon, @"badge": @"30"},
//                         @{@"module_name": kModuleSubjects, @"image": kModuleSubjectsJumpIcon, @"badge": @"0"},
//                         @{@"module_name": kModuleSchoolStream, @"image": kModuleSchoolStreamJumpIcon, @"badge": @"5"},
//                         @{@"module_name": kModuleCalendar, @"image": kModuleCalendarJumpIcon, @"badge": @"0"},
//                         @{@"module_name": kModuleEducationalApps, @"image": kModuleEducationalAppsJumpIcon, @"badge": @"0"},
//                         @{@"module_name": kModulePlaylists, @"image": kModulePlaylistsJumpIcon, @"badge": @"10"},
                         ];

    //VLog(@"Modules: %@", modules);
    //VLog(@"DashboardName: %@", [self dashboardName]);
    
    NSString *moduleName = [self dashboardName];
    
    if (![moduleName isEqualToString:kModuleDashboard]) {
        
        NSString *homeModule = NSLocalizedString(@"Home", nil);
        NSDictionary *homeMenuItem = @{@"module_name": homeModule, @"image": kModuleHomeJumpIcon, @"badge": @0};
        NSString *selectedDashboardItem = moduleName;
        
        NSMutableArray *jumpMenus = [NSMutableArray array];
        [jumpMenus addObject:homeMenuItem];
        
        for (int i = 0; i < modules.count; i++) {
            
            NSDictionary *item = (NSDictionary *)modules[i];
            
            if (![selectedDashboardItem isEqualToString:[item objectForKey:@"module_name"]]) {
                [jumpMenus addObject:item];
            }
        }
        
        currentModule = [NSArray arrayWithArray:jumpMenus];
        
    }
    else {
        
        currentModule = [NSArray arrayWithArray:modules];
    }
    
    NSLog(@"current module: %@", currentModule);
    
    return currentModule;
}

-(void) setupJumpMenuItems {
    
    NSArray *modules = [self _buildJumpModules];
    NSMutableArray *menuItems = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [modules count]; i++) {
        NSDictionary *item = (NSDictionary *) [modules objectAtIndex:i];
        REMenuItem *jumpMenuItem = [[REMenuItem alloc] initWithTitle:[item objectForKey:@"module_name"]
                                                            subtitle:nil
                                                               image:[UIImage imageNamed: [item objectForKey:@"image"]]
                                                    highlightedImage:nil
                                                              action:^(REMenuItem *item) {
                                                                  [self jumpMenuItemAction:item];
                                                              }];
        
        int badgeCount = [[item objectForKey:@"badge"] intValue];
        if (badgeCount != 0) {
            jumpMenuItem.badge = VS_FMT(@"%i", badgeCount);
        }
        
        jumpMenuItem.tag = i;
        [menuItems addObject:jumpMenuItem];
    }

    self.menu = [[REMenu alloc] initWithItems:menuItems];
    
    
    // Background view
    //
    //self.menu.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    //self.menu.backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    //self.menu.backgroundView.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.600];
    
    //self.menu.imageAlignment = REMenuImageAlignmentRight;
    self.menu.textColor = [UIColor whiteColor];
    self.menu.textShadowColor = [UIColor clearColor];
    self.menu.highlightedBackgroundColor = UIColorFromHex(0x1982B4);
    self.menu.highlightedTextShadowColor = [UIColor clearColor];
    self.menu.font = [UIFont fontWithName:@"Helvetica-Neue" size:14.0f];
    self.menu.closeOnSelection = YES;
    self.menu.cornerRadius = 4;
    self.menu.shadowRadius = 4;
    self.menu.backgroundColor = UIColorFromHex(0x0092c7);
    self.menu.shadowColor = [UIColor blackColor];
    self.menu.shadowOffset = CGSizeMake(0, 1);
    self.menu.shadowOpacity = 1;
    self.menu.imageOffset = CGSizeMake(5, -1);
    self.menu.waitUntilAnimationIsComplete = YES;
    self.menu.badgeLabelConfigurationBlock = ^(UILabel *badgeLabel, REMenuItem *item) {
        //badgeLabel.backgroundColor = [UIColor colorWithRed:0 green:179/255.0 blue:134/255.0 alpha:1];
        badgeLabel.backgroundColor = UIColorFromHex(0x1a1c1b);//UIColorFromHex(0x9e3908);
        badgeLabel.layer.borderColor = [UIColor clearColor].CGColor;
        //badgeLabel.layer.borderColor = [UIColor colorWithRed:0.000 green:0.648 blue:0.507 alpha:1.000].CGColor;
    };
    
}

-(void) setupToolbarItems {
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    CGRect buttonRect = CGRectMake(0, 0, 70, 44 );
    
    UIButton *calendarButton = [UIButton buttonWithImage:TOOLBAR_CALENDAR inFrame:buttonRect
                                               forTarget:self withAction:@selector(performCalendarAction:)];
    calendarButton.tag = 0;
    UIBarButtonItem *calendar = [[UIBarButtonItem alloc] initWithCustomView:calendarButton];
    
    
    
    UIButton *ideapadButton = [UIButton buttonWithImage:TOOLBAR_IDEAPAD inFrame:buttonRect
                                                   forTarget:self withAction:@selector(performNotificationAction:)];
    ideapadButton.tag = 1;
    UIBarButtonItem *ideaPad = [[UIBarButtonItem alloc] initWithCustomView:ideapadButton];
    ideaPad.tag = 1;

    
//    UIButton *homeButton = [UIButton buttonWithImage:TOOLBAR_HOME inFrame:buttonRect
//                                           forTarget:self withAction:@selector(performHomeAction:)];
    
    self.homeButton = [UIButton buttonWithImage:TOOLBAR_HOME inFrame:buttonRect
                                           forTarget:self withAction:@selector(performHomeAction:)];
    self.homeButton.tag = 2;
    UIBarButtonItem *home = [[UIBarButtonItem alloc] initWithCustomView:self.homeButton];
    
    
    
    UIButton *profileButton = [UIButton buttonWithImage:TOOLBAR_PROFILE inFrame:buttonRect
                                              forTarget:self withAction:@selector(performProfileAction:)];
    profileButton.tag = 3;
    UIBarButtonItem *profile = [[UIBarButtonItem alloc] initWithCustomView:profileButton];
    
    
//    UIButton *settingsButton = [UIButton buttonWithImage:TOOLBAR_SETTINGS inFrame:buttonRect
//                                               forTarget:self withAction:@selector(performSettingsAction:)];
//    settingsButton.tag = 4;
//    UIBarButtonItem *settings = [[UIBarButtonItem alloc] initWithCustomView:settingsButton];

    
    UIImage *globeImage = [UIImage imageWithImage:[UIImage imageNamed:@"icn_toolbar_globe"] scaledToSize:CGSizeMake(25, 25)];;
    self.globeButton = [UIButton buttonWithImage:globeImage inFrame:buttonRect
                                       forTarget:self withAction:@selector(performSettingsAction:)];
    self.globeButton.tag = 4;
    
//    self.badgeObject = [CustomBadge customBadgeWithString:@"0" withStyle:[BadgeStyle oldStyle]];
//    [self.globeButton addSubview:self.badgeObject];
    
    UIBarButtonItem *settings = [[UIBarButtonItem alloc] initWithCustomView:self.globeButton];
    
    NSArray *toolbarItems = @[flexibleItem, calendar, flexibleItem, ideaPad, flexibleItem, home, flexibleItem, profile, flexibleItem, settings, flexibleItem];
    
    [self setToolbarItems:toolbarItems];
}

- (void)enableHomeButton:(BOOL)enable {
//    self.homeButton.userInteractionEnabled = enable;
    self.homeButton.enabled = enable;
}

-(void) refreshMedals {
    NSDictionary *medals = [VSmartHelpers getMedals];
    VLog(@"Medals: %@", medals);
    
    [self.platinumLabelCount hide];
    [self.goldLabelCount hide];
    [self.silverLabelCount hide];
    [self.bronzeLabelCount hide];
    
    self.platinumLabelCount.text = [[medals objectForKey:kMedalPlatinum] stringValue];
    self.goldLabelCount.text = [[medals objectForKey:kMedalGold] stringValue];
    self.silverLabelCount.text = [[medals objectForKey:kMedalSilver] stringValue];
    self.bronzeLabelCount.text = [[medals objectForKey:kMedalBronze] stringValue];
    
    [self.platinumLabelCount fadeIn];
    [self.goldLabelCount fadeIn];
    [self.silverLabelCount fadeIn];
    [self.bronzeLabelCount fadeIn];
    
    [self.baseProfileView setNeedsLayout];
}

// BUG FIX 104
- (void) hideProfileViewForPlayList {
    [self toggleProfileFullView:NO];
}

#pragma mark - Popover View Delegate

-(void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index {
    VLog(@"[%li] popViewDelegate: %li", (long)popoverView.tag, (long)index);
    NSInteger tag = popoverView.tag;
    //tag 1 = mini action (from upper left)
    //tag 2 = view action (from avatar image)
    
    if (tag == 1) {
        switch (index) {
            case 0:{
                [self performUserProfileView];
            }
                break;
            case 1:{
                [self toggleProfileFullView:YES];
            }
                break;
            case 2:{
                [self signout];
            }
                break;
            default:
                break;
        }
    }
    
    if (tag == 2){
        switch (index) {
            case 0:{
                [self performUserProfileView];
                break;
            }
                
            case 1:{
                [self toggleProfileFullView:NO];
                break;
            }
                
            case 2:{
                [self showAvatarUploadView];
                break;
            }
                
            case 3:{
                [self updateProfileView];
                break;
            }

            case 4:{
                [self showAboutController];
                break;
            }
                
            case 5:{
                [self signout];
                break;
            }
                
            default:
                break;
        }
    }
    
    [popoverView dismiss:YES];
}

-(void)popoverViewDidDismiss:(PopoverView *)popoverView{
    //[popView release], pv = nil;
    VLog(@"popViewDelegate");
    //int tag = popoverView.tag;
    
}

-(void) showAvatarUploadView {
    UpdateAvatarViewController *vc = [[UpdateAvatarViewController alloc] init];
    vc.modalPresentationStyle = UIModalPresentationFormSheet;
    vc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)updateProfileView{
    
    updateProfileViewController = [[UpdateProfileViewController alloc] init];
    updateProfileViewController.delegate = self;
    [self presentPopupViewControllerWithKeyboardHeight:updateProfileViewController animationType:PopupViewAnimationFade];
}

- (void) showAboutController
{
    AboutViewController *avc = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
    avc.modalPresentationStyle = UIModalPresentationFormSheet;
    avc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:avc animated:YES completion:nil];
}

// -------------------------------------
// PEARSON SPECIFIC
// -------------------------------------

- (void) sendOffLineStatus {
    
    // SEND OFFLINE STATUS
//    ResourceManager *rm = [AppDelegate resourceInstance];
    [self.rm requestLogoutWithBlock:^(BOOL status) {
        if (status) {
            NSLog(@"[PEARSON] SIGN OUT SUCCESSFULLY");
            NSString *details = @"Logout successful via Apple iPad";
            [self.rm requestLogActivityWithModuleType:@"2" details:details];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[VSmart sharedInstance] logout];
                [self performHomeAction:nil];
                [self postLogout];
            });
        }
    }];
}

-(void) signout {
    
    [self sendOffLineStatus];
    
//    [[VSmart sharedInstance] logout];
//    [self performHomeAction:nil];
//    [self postLogout];
}

-(void) toggleProfileFullView: (BOOL) fullview {
    if (fullview) {
        
//        [UIView animateWithDuration:0.50 animations:^{
//            CGRect frame = self.baseProfileView.frame;
//            frame.origin.y = 92.0f;
//            frame.size = CGSizeMake(self.view.frame.size.width, 218.0);
//            [self.baseProfileView setFrame:frame];
//            float height = self.baseProfileView.frame.origin.y > 0 ? self.baseProfileView.frame.size.height : 0;
//            [self setProfileHeight:height];
//        } completion:^(BOOL finished) {
//            [self adjustModuleView];
//        }];
        
        // Use UIKitDynamics Animation Block (to give more emphasis)
        [UIView animateWithDuration:0.7f
                              delay:0.0f
             usingSpringWithDamping:0.6f
              initialSpringVelocity:1.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
            CGRect frame = self.baseProfileView.frame;
            frame.origin.y = 92.0f;
            frame.size = CGSizeMake(self.view.frame.size.width, 218.0);
            [self.baseProfileView setFrame:frame];
            
            float height = self.baseProfileView.frame.origin.y > 0 ? self.baseProfileView.frame.size.height : 0;
            [self setProfileHeight:height];
            
        } completion:^(BOOL finished) {
            [self adjustModuleView];
        }];
    } else {
        
//        [UIView animateWithDuration:0.50 animations:^{
//            CGRect frame = self.baseProfileView.frame;
//            frame.origin.y = - (218.0f - 92.0f);
//            [self.baseProfileView setFrame:frame];
//            float height = self.baseProfileView.frame.origin.y > 0 ? self.baseProfileView.frame.size.height : 0;
//            [self setProfileHeight:height];
//        } completion:^(BOOL finished) {
//            [self adjustModuleView];
//        }];
        
        // Use UIKitDynamics Animation Block (to give more emphasis)
        [UIView animateWithDuration:0.7f
                              delay:0.0f
             usingSpringWithDamping:0.6f
              initialSpringVelocity:1.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
            CGRect frame = self.baseProfileView.frame;
            frame.origin.y = - (218.0f - 92.0f);
            [self.baseProfileView setFrame:frame];
            float height = self.baseProfileView.frame.origin.y > 0 ? self.baseProfileView.frame.size.height : 0;
            [self setProfileHeight:height];
            
        } completion:^(BOOL finished) {
            [self adjustModuleView];
        }];
    }
}

#pragma mark - Actions

-(void) alertMessage: (NSString *) message {
    [self.view makeToast:message duration:2.0f position:@"center"];
}

-(void) jumpMenuItemAction: (id) sender {
    REMenuItem *item = (REMenuItem *)sender;
    
    //VLog(@"Item: %@", item.title);
    NSString *selectedJumpMenuItem = item.title;
    
    NSString *homeModule = NSLocalizedString(@"Home", nil);
    if ([selectedJumpMenuItem isEqualToString:homeModule]) {
        [self performHomeAction:nil];
    } else {
        VS_NCPOST_OBJ(kNotificationJumpMenuSelection, item.title)
    }
}

-(void) jumpMenuAction: (id) sender {

    if (self.menu.isOpen) {
        return [self.menu close];
    }
    
    //    [self.menu showFromRect:CGRectMake(0, 110, 220.0f, self.navigationController.view.frame.size.height) inView: self.navigationController.view];
    
    CGFloat xdelta = 0;
    CGFloat ydelta = 90;
    CGFloat width = 220.0;
    CGFloat height = self.navigationController.view.frame.size.height;
    
    CGRect framePosition = CGRectMake(xdelta, ydelta, width, height);
    
    [self.menu showFromRect:framePosition inView: self.navigationController.view]; //jcae82515
}

-(void) medalAction: (UIButton*) sender {
    int tag = (int)[sender tag];
    VLog(@"Tag: %i", tag);
    NSString *medalType = @"";
    if(tag == 0) {
        medalType = kMedalPlatinum;
    } else if (tag == 1) {
        medalType = kMedalGold;
    } else if (tag == 2) {
        medalType = kMedalSilver;
    } else {
        medalType = kMedalBronze;
    }
    
    [self popProfileMedalView:medalType];
}

#pragma mark - For implementation of concrete classes

-(void) adjustModuleView {
    //VS_NCPOST(kNotificationProfileHeight)
}

-(void) postLogout {
    
}

-(void) handleAvatarSmallAction: (id) sender {
    popViewPoint = [sender locationInView:self.view];
    
    /* localizable string*/
    NSString *signoutlabelSmall = NSLocalizedString(@"Signout", nil);
    NSString *togglefullprofileSmall = NSLocalizedString(@"Toggle full profile info", nil);
    
    float fixedWidth = 250.0f;
    UILabel *signOutLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, 0.f, fixedWidth, 30)];
    signOutLabel.backgroundColor = UIColorFromHex(0x9e3908);
    //titleLabel.font =
    signOutLabel.textAlignment = UITextAlignmentCenter;
    signOutLabel.textColor = [UIColor whiteColor];
    signOutLabel.text = signoutlabelSmall;

    UILabel *toggleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, 0.f, fixedWidth, 30)];
    toggleLabel.backgroundColor = UIColorFromHex(0x0092c7);

    toggleLabel.textAlignment = UITextAlignmentCenter;
    toggleLabel.textColor = [UIColor whiteColor];
    toggleLabel.text = togglefullprofileSmall;
    toggleLabel.layer.borderColor = UIColorFromHex(0x0f5b78).CGColor;
    toggleLabel.layer.borderWidth = 1;
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:18.f];
    NSString *currentName = VS_FMT(@"%@ %@", [self account].user.firstname, [self account].user.lastname);
    
    NSDictionary* attribs = @{NSFontAttributeName:font};
    CGRect rect = [currentName boundingRectWithSize:CGSizeMake(fixedWidth - 10 * 4.f, 1000.f)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:attribs
                                     context:nil];
    
    //CGSize textSize = [currentName sizeWithFont:font constrainedToSize:CGSizeMake(fixedWidth - 10 * 4.f, 1000.f) lineBreakMode:UILineBreakModeWordWrap];
    CGSize textSize = rect.size;
    UILabel *textView = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, textSize.width, textSize.height)];
    textView.backgroundColor = [UIColor clearColor];
    textView.userInteractionEnabled = NO;
    [textView setNumberOfLines:0]; //This is so the label word wraps instead of cutting off the text
    textView.font = font;
    textView.textAlignment = NSTextAlignmentLeft;
    textView.textColor = [UIColor whiteColor];
    textView.text = currentName;
    self.popupNameLabel = textView;
    
    UIView *content = [[UIView alloc] initWithFrame:CGRectMake(0, 0, fixedWidth, 100.0)];
    content.backgroundColor = UIColorFromHex(0x0092c7);
    content.layer.borderColor = UIColorFromHex(0x0f5b78).CGColor;
    content.layer.borderWidth = 1;
    [content addSubview:textView];
    
    NSArray *views = @[content, toggleLabel, signOutLabel];
    
    popView = [PopoverView showPopoverAtPoint:popViewPoint inView:self.view withViewArray:views delegate:self];
    popView.tag = 1;
}

-(void) handleAvatarChangeAction: (id) sender {
    popViewPoint = [sender locationInView:self.view];
    
    /* localizable string*/
    NSString *signoutlabel = NSLocalizedString(@"Signout", nil); //checked
    NSString *hideprofilelabel = NSLocalizedString(@"Hide profile info", nil); //checked
    NSString *viewfullprofile = NSLocalizedString(@"View full profile info", nil); //checked
    NSString *updateavatar = NSLocalizedString(@"Update avatar", nil); //checked
    NSString *updateprofile = NSLocalizedString(@"Update profile", nil); //checked
    NSString *aboutText = NSLocalizedString(@"About", nil); //checked
    
    float fixedWidth = 250.0f;
    UILabel *signOutLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, 0.f, fixedWidth, 30)];
    signOutLabel.backgroundColor = UIColorFromHex(0x9e3908);
    //titleLabel.font =
    signOutLabel.textAlignment = UITextAlignmentCenter;
    signOutLabel.textColor = [UIColor whiteColor];
    signOutLabel.text = signoutlabel;
    
    UILabel *toggleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, 0.f, fixedWidth, 30)];
    toggleLabel.backgroundColor = UIColorFromHex(0x0092c7);
    toggleLabel.textAlignment = UITextAlignmentCenter;
    toggleLabel.textColor = [UIColor whiteColor];
    toggleLabel.text = hideprofilelabel;
    toggleLabel.layer.borderColor = UIColorFromHex(0x0f5b78).CGColor;
    toggleLabel.layer.borderWidth = 1;

    UILabel *toggleFullLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, 0.f, fixedWidth, 30)];
    toggleFullLabel.backgroundColor = UIColorFromHex(0x0092c7);
    toggleFullLabel.textAlignment = UITextAlignmentCenter;
    toggleFullLabel.textColor = [UIColor whiteColor];
    toggleFullLabel.text = viewfullprofile;
    toggleFullLabel.layer.borderColor = UIColorFromHex(0x0f5b78).CGColor;
    toggleFullLabel.layer.borderWidth = 1;
    
    UILabel *avatarLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, 0.f, fixedWidth, 30)];
    avatarLabel.backgroundColor = UIColorFromHex(0x0092c7);
    avatarLabel.textAlignment = UITextAlignmentCenter;
    avatarLabel.textColor = [UIColor whiteColor];
    avatarLabel.text = updateavatar;
    avatarLabel.layer.borderColor = UIColorFromHex(0x0f5b78).CGColor;
    avatarLabel.layer.borderWidth = 1;
    
    UILabel *updateProfileLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, 0.f, fixedWidth, 30)];
    updateProfileLabel.backgroundColor = UIColorFromHex(0x0092c7);
    updateProfileLabel.textAlignment = UITextAlignmentCenter;
    updateProfileLabel.textColor = [UIColor whiteColor];
    updateProfileLabel.text = updateprofile;
    updateProfileLabel.layer.borderColor = UIColorFromHex(0x0f5b78).CGColor;
    updateProfileLabel.layer.borderWidth = 1;
    
    UILabel *aboutLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, 0.f, fixedWidth, 30)];
    aboutLabel.backgroundColor = UIColorFromHex(0x0092c7);
    aboutLabel.textAlignment = UITextAlignmentCenter;
    aboutLabel.textColor = [UIColor whiteColor];
    aboutLabel.text = aboutText;
    aboutLabel.layer.borderColor = UIColorFromHex(0x0f5b78).CGColor;
    aboutLabel.layer.borderWidth = 1;
    
    NSArray *views = @[toggleFullLabel, toggleLabel, avatarLabel, updateProfileLabel, aboutLabel, signOutLabel];
    
    popView = [PopoverView showPopoverAtPoint:popViewPoint inView:self.view withViewArray:views delegate:self];
    popView.tag = 2;
//    [UIView animateWithDuration:0.50 animations:^{
//        CGRect frame = self.baseProfileView.frame;
//        frame.origin.y = - (218.0f - 92.0f);
//        [self.baseProfileView setFrame:frame];
//        VLog(@"baseProfileView: %@", NSStringFromCGRect(self.baseProfileView.frame));
//        VLog(@"BaseProfileY: %.02f", ABS(self.baseProfileView.frame.origin.y));
//        float height = self.baseProfileView.frame.origin.y > 0 ? self.baseProfileView.frame.size.height : 0;
//        [self setProfileHeight:height];
//
//    } completion:^(BOOL finished) {
//        [self adjustModuleView];
//    }];
}

-(void) hideToolbar: (BOOL) hide {
    [self.navigationController setToolbarHidden:hide animated:YES];
}

-(void) performCalendarAction:(id)sender{
    UIButton *button = (UIButton*)sender;
    UIToolbar *toolbar = (UIToolbar*)[button superview];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.calendarViewController.view.hidden = NO;
    
    popoverController = [[UIPopoverController alloc] initWithContentViewController:appDelegate.calendarViewController];

    appDelegate.calendarViewController.parent = popoverController;
    
    [popoverController setPopoverContentSize:CGSizeMake(appDelegate.calendarViewController.view.frame.size.width,
                                                        appDelegate.calendarViewController.view.frame.size.height) animated:YES];
    
    [popoverController presentPopoverFromRect:CGRectMake(button.frame.origin.x + (button.frame.size.width/2), toolbar.frame.origin.y, 1, 1) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(void) performNotificationAction:(id)sender  {
    UIButton *button = (UIButton*)sender;
    UIToolbar *toolbar = (UIToolbar*)[button superview];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.ideaPadViewController.view.hidden = NO;
    
    popoverController = [[UIPopoverController alloc] initWithContentViewController:appDelegate.ideaPadViewController];
    appDelegate.ideaPadViewController.parent = popoverController;
    
    [popoverController setPopoverContentSize:CGSizeMake(appDelegate.ideaPadViewController.view.frame.size.width,
                                                        appDelegate.ideaPadViewController.view.frame.size.height) animated:YES];
    
    [popoverController presentPopoverFromRect:CGRectMake(button.frame.origin.x + (button.frame.size.width/2), toolbar.frame.origin.y, 1, 1) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

}

-(void) performHomeAction:(id)sender {
    if (self.menu.isOpen)
        [self.menu close];
    
    // TODO: Check if current view is already in dashboard.
    [self.navigationController popToRootViewControllerAnimated:YES];
    //[self.navigationController popViewControllerAnimated:YES];
}

-(void) performUserProfileView{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.userProfileViewController.view.hidden = NO;
    [appDelegate.userProfileViewController initializePhase];
    
    [self presentPopupViewController:appDelegate.userProfileViewController animationType:PopupViewAnimationSlideBottomBottom];
}

-(void) performProfileAction:(id)sender {
    UIButton *button = (UIButton*)sender;
    UIToolbar *toolbar = (UIToolbar*)[button superview];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.peopleViewController.view.hidden = NO;
    popoverController = [[UIPopoverController alloc] initWithContentViewController:appDelegate.peopleViewController];
    
    appDelegate.peopleViewController.parent = popoverController;
    
    [popoverController setPopoverContentSize:CGSizeMake(appDelegate.peopleViewController.view.frame.size.width,
                                                        appDelegate.peopleViewController.view.frame.size.height) animated:YES];
    
    [popoverController presentPopoverFromRect:CGRectMake(button.frame.origin.x + (button.frame.size.width/2), toolbar.frame.origin.y, 1, 1) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

//- (void)performSettingsAction:(id)sender {
//    
////    UIButton *button = (UIButton*)sender;
////    UIToolbar *toolbar = (UIToolbar*)[button superview];
////    
////    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
////    appDelegate.settingsViewController.view.hidden = NO;
////    
////    popoverController = [[UIPopoverController alloc] initWithContentViewController:appDelegate.settingsViewController];
////    
////    [popoverController setPopoverContentSize:CGSizeMake(appDelegate.settingsViewController.view.frame.size.width,
////                                                        appDelegate.settingsViewController.view.frame.size.height) animated:YES];
////    
////    [popoverController presentPopoverFromRect:CGRectMake(button.frame.origin.x + (button.frame.size.width/2), toolbar.frame.origin.y, 1, 1) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
//    
//    
//    //storyboard
//    
//    UIViewController *v = [self loadViewControllerWithIdentifier:@"GlobalNotifShortcut"];
//    [self presentViewController:v animated:YES completion:nil];
////    [self presentPopupViewController:v animationType:PopupViewAnimationSlideBottomTop];
//}

- (UIViewController *)loadViewControllerWithIdentifier:(NSString *)identifier {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"NotificationStoryBoard" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:identifier];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    // These values dynamically updates when UIInterfaceOrientation changes
    CGRect bounds = self.view.bounds;
    CGSize size = bounds.size;
    
    // size is now the width and height that we will have after the rotation
    NSLog(@"size: w:%.0f h:%.0f", size.width, size.height);
    
    // Set the new CGRect Values
    self.baseHeaderView.frame = CGRectMake(0, 0, size.width, 92.0f);
    
    // Set Logo
    CGRect headerBounds = self.baseHeaderView.bounds;
    self.imageView.frame = CGRectMake(headerBounds.origin.x, headerBounds.origin.y, headerBounds.size.width, headerBounds.size.height);
    self.imageView.contentMode = UIViewContentModeCenter;
    
    // Small Avatar
    CGFloat avatarDeltaX = headerBounds.size.width - (55 + 20);
    CGRect rect = CGRectMake(avatarDeltaX, 18, 55, 55);
    self.avatarSmallView.frame = CGRectInset(rect, 4, 4);
    
    // ENHANCEMENT#135: app must support landscape orientation
    // base profile view did not support landscape orientation before
    self.baseProfileView.width = self.view.frame.size.width;
}

- (void) updateNotifCount {
    
    // ANOTHER BUG FIX
    NSString *count = [self.rm readCountFromNotifications];
    
    if (self.badgeObject != nil) {
        [self.badgeObject removeFromSuperview];
    }
    
    if ([count isEqualToString:@"0"]) {
        return;
    }
    
//    self.badgeCount = count;
    self.badgeObject = [CustomBadge customBadgeWithString:count withStyle:[BadgeStyle oldStyle]];
    [self.globeButton addSubview:self.badgeObject];
}

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

@end
