//
//  LoginViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/15/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "LoginViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "VSmart.h"
#import "VSReachability.h"
#import "AccountResponse.h"
#import "GuestTableViewController.h"
#import "HUD.h"

@interface LoginViewController ()

@property (nonatomic, strong) UIPopoverController *popOverController;
@property (nonatomic, strong) GuestTableViewController *guestStudentTableViewController;
@property (nonatomic, strong) GuestTableViewController *guestTeacherTableViewController;

@property (strong, nonatomic) IBOutlet UILabel *versionLabel;
@property (strong, nonatomic) IBOutlet UIButton *previousLogin;

// Interface Builder static components
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIView *backgroundHolder;
@property (strong, nonatomic) IBOutlet UILabel *labelInstructions;
@property (strong, nonatomic) IBOutlet UILabel *labelLockTitle;
@property (weak, nonatomic) UIButton *buttonPress;

@property (assign, nonatomic) BOOL isConnected;

@property (weak, nonatomic) IBOutlet UIButton *showPasswordCheckBox;
@property (weak, nonatomic) IBOutlet UILabel *showPasswordLabel;

// NOTIFICATIONS
- (void)startConnectivityObserver;

@end

@implementation LoginViewController
@synthesize delegate = _delegate;
@synthesize guestStudentTableViewController, guestTeacherTableViewController;

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if ( self.interfaceOrientation == UIInterfaceOrientationPortrait ) {
        CAKeyframeAnimation *scaleAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
        NSArray *scaleValues                = @[[NSValue valueWithCATransform3D:CATransform3DScale(self.view.layer.transform, 1, 1, 1)],
                                                [NSValue valueWithCATransform3D:CATransform3DScale(self.view.layer.transform, 1.1, 1.1, 1)],
                                                [NSValue valueWithCATransform3D:CATransform3DScale(self.view.layer.transform, 1, 1, 1)] ];
        
        [scaleAnimation setValues:scaleValues];
        scaleAnimation.fillMode             = kCAFillModeForwards;
        scaleAnimation.removedOnCompletion  = NO;
        [self.backgroundHolder.layer addAnimation:scaleAnimation forKey:@"scale"];
    }
}

-(void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_tile"]];
    //[self test];
    
    self.guestStudentTableViewController = [[GuestTableViewController alloc] init];
    self.guestStudentTableViewController.delegate = self;
    [self.guestStudentTableViewController initializeGuestList:kModeIsStudent];
    
    self.guestTeacherTableViewController = [[GuestTableViewController alloc] init];
    self.guestTeacherTableViewController.delegate = self;
    [self.guestTeacherTableViewController initializeGuestList:kModeIsTeacher];
    
    self.previousLogin.hidden = YES;
    NSString *buttonText = NSLocalizedString(@"use previous login", nil);
    [self.previousLogin setTitle:buttonText forState:UIControlStateNormal];
    [self.previousLogin setTitle:buttonText forState:UIControlStateHighlighted];
    [self.previousLogin addTarget:self action:@selector(previousLoginAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.labelInstructions.text = NSLocalizedString(@"or start browsing now using our demo accounts.\nPlease select the mode you like to try.", nil);
    self.labelLockTitle.text = NSLocalizedString(@"Sign in to your account", nil);
    self.versionLabel.text = [VSmartHelpers getVersionCode];
    
    self.showPasswordLabel.text = NSLocalizedString(@"Show password", nil);
    [self.showPasswordCheckBox addTarget:self action:@selector(showPasswordAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self setupValidateConnectivity];
}

- (void)showPasswordAction:(UIButton *)sender {
    if (sender == self.showPasswordCheckBox) {
        [self.view endEditing:true];
        sender.selected = (sender.selected) ? NO : YES;
        self.passwordText.secureTextEntry = (sender.selected) ? NO : YES;
    }
}

- (void)setupValidateConnectivity {
    
    NSString *hostName = [NSString stringWithFormat:@"%@", [Utils getVibeServer] ];
    NSLog(@"server: %@", hostName);
    
    // Allocate a reachability object
    VSReachability* reach = [VSReachability reachabilityWithHostname:hostName];
    
    // Tell the reachability that we DON'T want to be reachable on 3G/EDGE/CDMA
    reach.reachableOnWWAN = YES;
    
    __weak typeof(self) wo = self;
    // Set the blocks
    reach.reachableBlock = ^(VSReachability *reach) {
        // keep in mind this is called on a background thread
        // and if you are updating the UI it needs to happen
        // on the main thread, like this:
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%@ reachable!", hostName);
            wo.isConnected = YES;
            self.previousLogin.hidden = YES;
        });
    };
    
    reach.unreachableBlock = ^(VSReachability *reach) {
        NSLog(@"%@ unreachable!", hostName);
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.isConnected = NO;
            self.previousLogin.hidden = NO;
        });
    };
    
    // Start the notifier, which will cause the reachability object to retain itself!
    [reach startNotifier];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self startConnectivityObserver];
}

- (void)startConnectivityObserver {
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    SEL notificationAction = @selector(previousButtonNotification:);
    [nc addObserver:self selector:notificationAction name:kVSmartConnectivityNotif object:nil];
}

- (void)previousButtonNotification:(NSNotification *)notification {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSString *status = [NSString stringWithFormat:@"%@", [notification object] ];
    NSLog(@"status : %@", status);
    if (![status isEqualToString:@""]) {
        BOOL ishidden = [status isEqualToString:@"1"];
        NSLog(@"hidden : %@", (ishidden) ? @"YES" : @"NO" );
        self.previousAccount = (AccountInfo *)[Utils getArchive:kPreviousAccount];
        if (self.previousAccount) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.previousLogin.hidden = ishidden;
            });
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self stopConnectivityObserver];
}

- (void)stopConnectivityObserver {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:kVSmartConnectivityNotif object:nil];
}

- (void)previousLoginAction:(id)sender {
    
    BOOL status = YES;
    [Utils saveArchive:self.previousAccount forKey:kGlobalAccount];
    
    if ([self.delegate respondsToSelector:@selector(didFinishLogin:withStatus:andResult:)]) {
        [self.delegate didFinishLogin:self withStatus:status andResult:nil];
    }
}

//-(void) test {
//    NSDictionary *account = [self accountFromFile];
//    NSLog(@"Account: %@", account);
//    
//    AccountInfo *info = [[AccountInfo alloc] initWithDictionary:account error:nil];
//    NSLog(@"Account: %@", info.account.user.avatar);
//}
//
//- (NSDictionary *) accountFromFile {
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"account_info" ofType:@"json"];
//    NSData *dataFile = [[NSData alloc] initWithContentsOfFile:filePath];
//    
//    NSDictionary *object = [NSJSONSerialization JSONObjectWithData:dataFile options:0 error:nil];
//    return object;
//}


-(IBAction)buttonLoginAsGuessTapped:(UIButton*)sender
{
    // Get pointer from the sender
    self.buttonPress = (UIButton *)sender;
    
    [_usernameText resignFirstResponder];
    [_passwordText resignFirstResponder];
    
    GuestTableViewController *guestController;
    
    if (sender.tag == 100) //student
        guestController = self.guestStudentTableViewController;
    
    if (sender.tag == 200) //teacher
        guestController = self.guestTeacherTableViewController;
    
    if (guestController.guestArray.count <= 0){
        
        if (sender.tag == 100) //student
            [self signInAsGuest:nil position:kModeIsStudent];
        
        if (sender.tag == 200) //teacher
            [self signInAsGuest:nil position:kModeIsTeacher];
    }
    else{
        
        self.popOverController = [[UIPopoverController alloc] initWithContentViewController:guestController];
        guestController.parent = self.popOverController;
        
        CGSize popOverSize = CGSizeMake(guestController.view.frame.size.width, guestController.view.frame.size.height);
        [self.popOverController setPopoverContentSize:popOverSize animated:YES];

        // Position PopoverController above button
        [self positionPopoverController:self.popOverController usingSender:sender];
    }
}

- (void)positionPopoverController:(UIPopoverController *)popOver usingSender:(UIView *)sender
{
    // Translate UIButton Bounds with Main View
    CGRect senderFrame = [sender convertRect:sender.bounds toView:self.view];
    
    // Size
    CGFloat width = (senderFrame.size.width / 2);
    CGFloat height = (senderFrame.size.height / 2);
    
    // Coordinates
    CGFloat deltaX = senderFrame.origin.x + width;
    CGFloat deltaY = senderFrame.origin.y + height;
    
    [popOver presentPopoverFromRect:CGRectMake(deltaX, deltaY, 1, 1) inView:self.view
           permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

//this will also be called from GuestTableViewController;
-(void)signInAsGuest:(NSDictionary*)value position:(NSString*)position{
    
    /* localizable strings */
    NSString *signinMessage = NSLocalizedString(@"Signing-in to V-Smart", nil);
    NSString *cannotSigninExistingError = NSLocalizedString(@"Cannot sign-in as existing guess", nil);
    NSString *cannotSigninNewError = NSLocalizedString(@"Cannot sign-in as new guest", nil);
    NSString *cannotConnectError = NSLocalizedString(@"Cannot connect to server", nil);
    NSString *welcomeStudent = NSLocalizedString(@"Welcome Student", nil);
    NSString *welcomeTeacher = NSLocalizedString(@"Welcome Teacher", nil);
    NSString *disclaimerMessage = NSLocalizedString(@"Disclaimer", nil);
    
    NSString *message = ( [position isEqualToString:kModeIsStudent] ) ? welcomeStudent : welcomeTeacher;
    
    if (value){
        //[HUD showUIBlockingIndicatorWithText:MSG_LOGIN_START];
        [HUD showUIBlockingIndicatorWithText:signinMessage];
        [[VSmart sharedInstance] connectAsExistingGuest:value resultBlock:^(AccountResponse *accountResponse) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            BOOL success = accountResponse.error.status == 0 ? YES : NO;
            
            if (success) {
                if ([self.delegate respondsToSelector:@selector(didFinishLogin:withStatus:andResult:)]) {
                    [self.delegate didFinishLogin:self withStatus:success andResult:accountResponse];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:cannotSigninExistingError];
                [self.signInButton shake];
            }
        } failureBlock:^(NSError *error, NSData *responseData) {
            VLog(@"Login Error: %@", [error localizedDescription]);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [SVProgressHUD showErrorWithStatus:cannotSigninExistingError];
        }];
    }
    else{
        //[HUD showUIBlockingIndicatorWithText:MSG_LOGIN_START];
        [HUD showUIBlockingIndicatorWithText:signinMessage];
        [[VSmart sharedInstance] connectAsGuestWithPosition:position platformType:VSmartPlatformTypeiPad resultBlock:^(AccountResponse *accountResponse) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            BOOL success = accountResponse.error.status == 0 ? YES : NO;
            
            if (success) {
                if ([self.delegate respondsToSelector:@selector(didFinishLogin:withStatus:andResult:)]) {
                    [self.delegate didFinishLogin:self withStatus:success andResult:accountResponse];
                    
                    NSString *showMessage = [NSString stringWithFormat:@"%@ %@", message, disclaimerMessage];
                    
                    [SVProgressHUD showSuccessWithStatus:showMessage];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:cannotSigninNewError];
                [self.signInButton shake];
            }
        } failureBlock:^(NSError *error, NSData *responseData) {
            VLog(@"Login Error: %@", [error localizedDescription]);
            [SVProgressHUD showErrorWithStatus:cannotConnectError];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.signInButton shake];
        }];
    }
}

-(IBAction)signInAction:(id)sender{
    
    [_usernameText resignFirstResponder];
    [_passwordText resignFirstResponder];
    
    if (self.isConnected) {
        NSString *signinMessage = NSLocalizedString(@"Signing-in to V-Smart", nil);
        NSString *invalidUserPassword = NSLocalizedString(@"Invalid username or password", nil);
        [HUD showUIBlockingIndicatorWithText:signinMessage];
        [[VSmart sharedInstance] connect:_usernameText.text
                            withPassword:_passwordText.text
                         andPlatformType:VSmartPlatformTypeiPad
                             resultBlock:^(AccountResponse *accountResponse) {
                                 
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            //VLog(@"Account: %@", accountResponse);
            //VLog(@"Account: %@", accountResponse.account.device.token);
            BOOL success = accountResponse.error.status == 0 ? YES : NO;
            
            if (success) {
                if ([self.delegate respondsToSelector:@selector(didFinishLogin:withStatus:andResult:)]) {
                    [self.delegate didFinishLogin:self withStatus:success andResult:accountResponse];
                }
            } else {
                //VLog(@"Failed");
                //[self startShake:self.signInButton];
                [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
                [SVProgressHUD showErrorWithStatus:invalidUserPassword];
                [self.signInButton shake];
            }
            
        } failureBlock:^(NSError *error, NSData *responseData) {
            VLog(@"Login Error: %@", [error localizedDescription]);
            //NSString *errorMessage = [[error userInfo] objectForKey:NSLocalizedDescriptionKey];
            [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
            [SVProgressHUD showErrorWithStatus:invalidUserPassword];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            //[self startShake:self.signInButton];
            [self.signInButton shake];
        }];
    } else {
        NSString *connectivityMessage = NSLocalizedString(@"Please ensure that you have a network connection so we can log you in.", nil);
        [SVProgressHUD showErrorWithStatus:connectivityMessage];
        [self.signInButton shake];
    }
}

#pragma mark - UITextField delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self signInAction:_signInButton];
    return true;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    // ENHANCMENT: Should support landscape orientation
    // Subclasses may override this method to perform additional actions immediately after the rotation.
    // Your implementation of this method must call super at some point during its execution.
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];

    // These values dynamically updates when UIInterfaceOrientation changes
    CGFloat frameWidth =  self.view.bounds.size.width;
    CGFloat frameHeight =  self.view.bounds.size.height;
    
    // These values are constance base on interface builder
    CGFloat containerWidth = self.containerView.frame.size.width;
    CGFloat containerHeight = self.containerView.frame.size.height;
    
    // Calculate delta x and y
    CGFloat deltaX = (frameWidth / 2) - (containerWidth / 2);
    CGFloat deltaY = (frameHeight / 2) - (containerHeight / 2);
    
    // Set the new CGRect Values
    self.containerView.frame = CGRectMake(deltaX, deltaY, containerWidth, containerHeight);
    
    // Reposition pop over controller
    if (self.buttonPress) {
        [self positionPopoverController:self.popOverController usingSender:self.buttonPress];
    }
}

@end
