//
//  GuestTableViewController.h
//  V-Smart
//
//  Created by VhaL on 3/26/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GuestAccount.h"

@interface GuestTableViewController : UIViewController
@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) id parent;
@property (nonatomic, strong) NSArray *guestArray;

-(void)initializeGuestList:(NSString*)kMode;
@end
