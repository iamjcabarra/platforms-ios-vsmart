//
//  AvatarViewController.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/21/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQGridView.h"

@interface AvatarViewController : UIViewController<AQGridViewDelegate, AQGridViewDataSource, UIPopoverControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet AQGridView *gridView;
@property (strong) UIPopoverController *popoverImageViewController;
@property (weak, nonatomic) IBOutlet UIButton *photoButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIButton *uploadButton;

-(IBAction)clearAllAction:(id)sender;
-(IBAction)uploadImage:(id)sender;
-(IBAction)avatarAction:(id)sender;
-(IBAction)close:(id)sender;
@end
