//
//  UpdateAvatarViewController.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 11/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit


protocol URLDownloadable {
    var downloadURL: URL { get }
    var downloadedData : Data? { get set }
    func getImageData(_ completion: @escaping ((_ data: Data?, _ response: URLResponse?, _ error: Error? ) -> Void))
}

extension URLDownloadable {
    func getImageData(_ completion: @escaping ((_ data: Data?, _ response: URLResponse?, _ error: Error? ) -> Void)) {
        
        DispatchQueue.global(qos: .background).async {
            
            let url:URL = self.downloadURL
            
            do {
                let data = try Data(contentsOf: url)
                DispatchQueue.main.async {
                    completion(data, nil, nil)
                }
            } catch {
                completion(nil, nil, error)
            }
            
        }
    }
}

struct Avatar : URLDownloadable {

    let id : String
    let name : String
    let avatar_url : URL
    let is_deleted : String
    let date_created : String
    let date_modified : String
    var downloadedData: Data?
    
    var isSelected : Bool
    
    var downloadURL: URL
    
    init(dictionary: NSDictionary) {
//        @{@"avatar_id":avatar_id,
//            @"name":name,
//            @"avatar_url":avatar_url,
//            @"is_deleted":is_deleted,
//            @"date_created":date_created,
//            @"date_modified":date_modified};
        
        self.id = dictionary["avatar_id"] as! String
        self.name = dictionary["name"] as! String
        self.avatar_url = dictionary["avatar_url"] as! URL
        self.is_deleted = dictionary["is_deleted"] as! String
        self.date_created = dictionary["date_created"] as! String
        self.date_modified = dictionary["date_modified"] as! String
        self.downloadURL = avatar_url
        self.isSelected = false
    }
}

class UpdateAvatarViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
//    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var chooseLbl: UILabel!
    
    @IBOutlet weak var noAvatarToDisplay: UILabel!
    // MARK: - Shared Data Manager
    fileprivate lazy var dataManager: ResourceManager = {
        let rm = ResourceManager.sharedInstance()
        return rm!
    }()
    
    var arrayOfAvatars:[Avatar] = [Avatar]()
    
    var selectedIndexRow: Int?
    
    var cameraPicker: UIImagePickerController?
    var galleryPicker: UIImagePickerController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.allowsMultipleSelection = false
        self.titleLabel.text = NSLocalizedString("Avatar Update", comment: "")
        self.chooseLbl.text = NSLocalizedString("Choose from", comment: "")
        
        self.uploadButton.setTitle(NSLocalizedString("Upload", comment: ""), for: UIControlState())
        self.cameraButton.setTitle(NSLocalizedString("Take a photo", comment: ""), for: UIControlState())
        self.galleryButton.setTitle(NSLocalizedString("Choose from Gallery", comment: ""), for: UIControlState())
        
        self.setUpTargets()
        
        // Resusable Cell
        let avatarCellNib = UINib(nibName: "UpdateAvatarViewCell", bundle: nil)
        self.collectionView.register(avatarCellNib, forCellWithReuseIdentifier: "avatarCell")
        
        self.dataManager.requestProfileAvatars { (avatars) in
            if avatars != nil {
                if (avatars?.count)! > 0 {
                    for avatar in avatars! {
                        let avatarDict = avatar as! NSDictionary
                        let structAvatar = Avatar(dictionary: avatarDict)
                        self.arrayOfAvatars.append(structAvatar)
                    }
                    DispatchQueue.main.async(execute: { 
                        self.collectionView.reloadData()
                    })
                }
            }
        }
        
        self.noAvatarToDisplay.text = NSLocalizedString("No available data to display", comment: "")
        
        self.addRecentAvatars()
    }
    
    func addRecentAvatars() {
        let savedImages = Utils.recentlyUsedImages()
        
        for savedImage in savedImages! {
            let savedImageUrl = URL(string: savedImage as! String)
            let data = try? Data(contentsOf: savedImageUrl!)
            
            let avatarDict = ["avatar_id":"1",
                              "name":"vibe",
                              "avatar_url":URL(fileURLWithPath: savedImage as! String),
                              "is_deleted":"0",
                              "date_created":"0000-00-00 00:00:00",
                              "date_modified":"0000-00-00 00:00:00"] as [String : Any]
            
            var newAvatar = Avatar(dictionary: avatarDict as NSDictionary)
            newAvatar.downloadedData = data
            self.arrayOfAvatars.append(newAvatar)
        }
    }
    
    func setUpTargets() {
        self.closeButton.addTarget(self, action: #selector(self.closeAction(_:)), for: .touchUpInside)
        self.uploadButton.addTarget(self, action: #selector(self.uploadAction(_:)), for: .touchUpInside)
        self.cameraButton.addTarget(self, action: #selector(self.takePhotoAction(_:)), for: .touchUpInside)
        self.galleryButton.addTarget(self, action: #selector(self.galleryAction(_:)), for: .touchUpInside)
    }
    
    func closeAction(_ button:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func takePhotoAction(_ button:UIButton) {
        if cameraPicker == nil {
            self.cameraPicker = UIImagePickerController()
            self.cameraPicker!.delegate = self
            self.cameraPicker!.sourceType = .camera
            self.cameraPicker!.allowsEditing = true
            self.cameraPicker!.showsCameraControls = true
        }
        
        self.present(cameraPicker!, animated: true, completion: nil)
    }
    
    func galleryAction(_ button:UIButton) {
        if galleryPicker == nil {
            self.galleryPicker = UIImagePickerController()
            self.galleryPicker!.delegate = self
            self.galleryPicker!.sourceType = .photoLibrary
            self.galleryPicker!.allowsEditing = true
            self.galleryPicker!.modalPresentationStyle = UIModalPresentationStyle.popover
        }
        
        let popover: UIPopoverPresentationController = galleryPicker!.popoverPresentationController!
        popover.sourceRect = button.bounds
        popover.sourceView = button
        present(galleryPicker!, animated: true, completion:nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        var newAvatar : Avatar?
        
        let usedImageData = UIImagePNGRepresentation(image)
        let filePath2 = "\(Utils.userPhotoDirectory())/\(Utils.randomNumber()).jpg"
        try? usedImageData?.write(to: URL(fileURLWithPath: filePath2), options: [])
            
        let avatarDict = ["avatar_id":"1",
                          "name":"vibe",
                          "avatar_url":URL(fileURLWithPath: "public/img/avatar/143kk53698c645daa0.png"),
                          "is_deleted":"0",
                          "date_created":"0000-00-00 00:00:00",
                          "date_modified":"0000-00-00 00:00:00"] as [String : Any]
        
        newAvatar = Avatar(dictionary: avatarDict as NSDictionary)
        newAvatar!.downloadedData = usedImageData
        newAvatar!.isSelected = true
        
        self.deselect()
        self.selectedIndexRow = 0
        self.dismiss(animated: true, completion: nil)
        self.arrayOfAvatars.insert(newAvatar!, at: 0)
        self.collectionView.reloadData()
    }
    
    func deselect() {
        if selectedIndexRow != nil {
            self.arrayOfAvatars[self.selectedIndexRow!].isSelected = false
        }
    }
    
    func adaptivePresentationStyleForPresentationController(_ controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func uploadAction(_ button:UIButton) {
        if self.selectedIndexRow != nil {
            let indexPath = IndexPath(item: self.selectedIndexRow!, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: false)
            
            if self.arrayOfAvatars[self.selectedIndexRow!].downloadedData != nil {
                let image = UIImage(data: self.arrayOfAvatars[self.selectedIndexRow!].downloadedData!)
                
                let uploadImage = NSLocalizedString("Uploading your image ...", comment: "")
//                SVProgressHUD.show(withStatus: uploadImage, maskType: )
//                SVProgressHUD.show(withStatus: <#T##String!#>)
//                SVProgressHUD.show(withStatus: uploadImage, maskType: SVProgressHUDMaskType.gradient)
//                let progressHUD = SVProgressHUD()
//                progressHUD.setDefaultMaskType(SVProgressHUDMaskType.gradient)
                SVProgressHUD.show(withStatus: uploadImage)
                self.dataManager.uploadAvatarImage(image, dataBlock: { (dictionary) in
                    let error = dictionary!["error"] as? NSString
                    
                    if error == nil {
                        print("dictionary!! = \(dictionary)")
                        
                        let url = dictionary?["url"] as! String
                        
                        DispatchQueue.main.async(execute: {
                            VSmart.sharedInstance().updateAvatar(url)
                            NotificationCenter.default.post(name: Notification.Name(rawValue: kNotificationProfileReloadV2), object: nil)
                            let successMessage = NSLocalizedString("Your avatar has been updated!", comment: "")
                            SVProgressHUD.showSuccess(withStatus: successMessage)
                        })
                        
                        self.sendAvatarUpdateActivity()
                        
                        // SUCCESS
                    } else {
                        // FAIL
                        print("ERROR IN UPLOAD ~ [\(error)]")
                        SVProgressHUD.dismiss()
                    }
                })
            }
        }
    }
    
    func sendAvatarUpdateActivity() {
        let queue = DispatchQueue(label: "com.pearson.course.UPLOADAVATARV2", attributes: [])
        queue.async { 
            let details = "New Profile image uploaded using Apple iPad"
            self.dataManager.requestLogActivity(withModuleType: "2", details: details)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.noAvatarToDisplay.isHidden = (self.arrayOfAvatars.count > 0) ? true : false
        
        return self.arrayOfAvatars.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "avatarCell", for: indexPath) as! UpdateAvatarViewCell
        
        cell.avatarImage.image = nil
        cell.avatarImage.backgroundColor = UIColor(hex6: 0x00AEDB)
        cell.activityIndicator.startAnimating()

        cell.selectedIndicator.isHidden = (self.arrayOfAvatars[(indexPath as NSIndexPath).row].isSelected) ? false : true
        
        cell.tag = (indexPath as NSIndexPath).row
        
        if self.arrayOfAvatars[(indexPath as NSIndexPath).row].downloadedData == nil {
            print("NIL!!!!")
            self.arrayOfAvatars[(indexPath as NSIndexPath).row].getImageData({ (data, response, error) in //.getImageData( { (data, response, error) in
                // fix for flickering issue
                if cell.tag == (indexPath as NSIndexPath).row {
                    DispatchQueue.main.async(execute: {
                        guard let data = data , error == nil else { return }
                        self.arrayOfAvatars[(indexPath as NSIndexPath).row].downloadedData = data
                        cell.activityIndicator.stopAnimating()
                        cell.avatarImage.image = UIImage(data: data)
                    })
                }
            })
        } else {
            print("MERON NA!!")
            DispatchQueue.main.async(execute: {
                cell.activityIndicator.stopAnimating()
                cell.avatarImage.image = UIImage(data: self.arrayOfAvatars[(indexPath as NSIndexPath).row].downloadedData!)
            })
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! UpdateAvatarViewCell
        let row = (indexPath as NSIndexPath).row
        
        self.deselectPreviousCell(selected: row)
        
        var avatar : Avatar = self.arrayOfAvatars[row]
        let isSelected = avatar.isSelected
        let isHidden = (isSelected) ? true : false
        
        avatar.isSelected = isSelected ? false : true
        self.selectedIndexRow = (isSelected) ? nil : row
        
        self.arrayOfAvatars.remove(at: row)
        self.arrayOfAvatars.insert(avatar, at: row)
        
        DispatchQueue.main.async { 
            cell.selectedIndicator.isHidden = isHidden
            self.uploadButton.isEnabled = (self.selectedIndexRow == nil) ? false : true
        }
    }
    
    func deselectPreviousCell(selected selectedRow: Int?) {
        if self.selectedIndexRow != nil {
            if self.selectedIndexRow != selectedRow || selectedRow == nil {
                var avatar : Avatar = self.arrayOfAvatars[self.selectedIndexRow!]
                avatar.isSelected = false
                self.arrayOfAvatars.remove(at: self.selectedIndexRow!)
                self.arrayOfAvatars.insert(avatar, at: self.selectedIndexRow!)
                
                let indexPath = IndexPath(item: self.selectedIndexRow!, section: 0)
                let visibleIndexPaths = collectionView.indexPathsForVisibleItems
                
                // Check if selected cell is visible
                let filteredVisibleIndexPaths = visibleIndexPaths.filter({ ($0 as NSIndexPath).row == self.selectedIndexRow })
                
                if filteredVisibleIndexPaths.count > 0 {
                    //visible remove selection
                    let cell = collectionView.cellForItem(at: indexPath) as! UpdateAvatarViewCell
                    DispatchQueue.main.async {
                        cell.selectedIndicator.isHidden = true
                    }
                } else {
                    self.selectedIndexRow = nil
                }
            }
        }
    }
}
