//
//  LoginViewController.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/15/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LoginViewController;

@protocol LoginViewControllerDelegate <NSObject>
-(void) didFinishLogin:(LoginViewController *) controller withStatus:(BOOL)success andResult:(id) result;
@end

@interface LoginViewController : UIViewController <UITextFieldDelegate>
{
    id<LoginViewControllerDelegate> delegate;
}

@property (retain) id delegate;
@property (weak, nonatomic) IBOutlet UITextField *usernameText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;

@property (strong, nonatomic) AccountInfo *previousAccount;

-(IBAction)signInAction:(id)sender;

@end
