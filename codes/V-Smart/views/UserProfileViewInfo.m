//
//  UserProfileViewBasic.m
//  V-Smart
//
//  Created by VhaL on 4/21/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "UserProfileViewInfo.h"

@implementation UserProfileViewBasic
@synthesize detail2, detail1, birthday, sex, address, facebook, twitter, instagram, contact, contactPerson, birthPlace;

@synthesize editDetail2, editDetail1, editBirthday, editSex, editAddress, editFacebook, editTwitter, editInstagram, editFirstName, editLastName, editMiddleName, editSuffix, editContact, editContactPerson, editBirthPlace;


-(void)awakeFromNib{
    [Utils adjustBorder:editDetail1];
    [Utils adjustBorder:editDetail2];
    [Utils adjustBorder:editDetail1];
    [Utils adjustBorder:editBirthday];
    [Utils adjustBorder:editSex];
    [Utils adjustBorder2:editAddress];
    [Utils adjustBorder:editFacebook];
    [Utils adjustBorder:editTwitter];
    [Utils adjustBorder:editInstagram];
    [Utils adjustBorder:editFirstName];
    [Utils adjustBorder:editLastName];
    [Utils adjustBorder:editMiddleName];
    [Utils adjustBorder:editSuffix];
    [Utils adjustBorder:editContact];
    [Utils adjustBorder:editContactPerson];
    [Utils adjustBorder:editBirthPlace];
}

@end

@implementation UserProfileViewOther
@synthesize spokenLanguage, hobbies, sports, aboutme, nationality;

@synthesize editSpokenLanguage, editHobbies, editSports, editAboutme, editNationality;

-(void)awakeFromNib{
    [Utils adjustBorder:editSpokenLanguage];
    [Utils adjustBorder:editHobbies];
    [Utils adjustBorder:editSports];
    [Utils adjustBorder2:editAboutme];
    [Utils adjustBorder:editNationality];
}
@end

@implementation UserProfileViewSchool
@synthesize preschooladdress, preschoolname, preschoolyear, elementaryaddress, elementaryname, elementaryyear, highschooladdress, highschoolname, highschoolyear;

@synthesize editPreschooladdress, editPreschoolname, editPreschoolyear, editElementaryaddress, editElementaryname, editElementaryyear, editHighschooladdress, editHighschoolname, editHighschoolyear;

-(void)awakeFromNib{
    [Utils adjustBorder2:editPreschooladdress];
    [Utils adjustBorder:editPreschoolname];
    [Utils adjustBorder:editPreschoolyear];
    [Utils adjustBorder2:editElementaryaddress];
    [Utils adjustBorder:editElementaryname];
    [Utils adjustBorder:editElementaryyear];
    [Utils adjustBorder2:editHighschooladdress];
    [Utils adjustBorder:editHighschoolname];
    [Utils adjustBorder:editHighschoolyear];
}
@end

@implementation UserProfileViewStatistics
@synthesize postedTopics, receivedLikes, repliedTopics, likedSoFar, sharing, feedback, percentCompleted;

@end