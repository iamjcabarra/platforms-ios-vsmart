//
//  ChangePasswordController.m
//  V-Smart
//
//  Created by Ryan Migallos on 6/10/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "ChangePasswordController.h"
#import "AppDelegate.h"

@interface ChangePasswordController ()  <UIPopoverControllerDelegate>

@property (strong, nonatomic) IBOutlet UITextField *currentPasswordField;
@property (strong, nonatomic) IBOutlet UITextField *updatedPasswordField;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordField;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) ResourceManager *rm;

@end

@implementation ChangePasswordController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.rm = [AppDelegate resourceInstance];

//    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
//    self.currentPasswordField.text = [NSString stringWithFormat:@"%@", account.user.password];
    [self.currentPasswordField becomeFirstResponder];
    [self.saveButton addTarget:self action:@selector(saveButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton addTarget:self action:@selector(cancelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.preferredContentSize = CGSizeMake(300, 300);
}

- (void)setStatusMessage:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.statusLabel.text = [NSString stringWithFormat:@"%@", message];
        self.updatedPasswordField.text = @"";
        self.confirmPasswordField.text = @"";
    });
}

- (BOOL)validateInput {
    
    BOOL status = YES;
    NSString *current_password = [NSString stringWithFormat:@"%@", self.currentPasswordField.text];
    NSString *updated_password = [NSString stringWithFormat:@"%@", self.updatedPasswordField.text];
    NSString *confirm_password = [NSString stringWithFormat:@"%@", self.confirmPasswordField.text];
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *old_password = [NSString stringWithFormat:@"%@", account.user.password];

    if ( !(current_password.length > 0) || !(updated_password.length > 0) || !(confirm_password.length > 0) ) {
        [self setStatusMessage:@"required fields must have inputs"];
        status = NO;
    }
    
    if ( !(updated_password.length > 7) || !(confirm_password.length > 7) ) {
        [self setStatusMessage:@"input must be atleast 8 characters"];
        status = NO;
    }
    
    if ( ![ updated_password isEqualToString: confirm_password  ] ) {
        [self setStatusMessage:@"confirm password mismatch"];
        status = NO;
    }
    
    if ( [old_password isEqualToString:updated_password ] || [old_password isEqualToString:confirm_password ] ) {
        [self setStatusMessage:@"input must be different with old password"];
        status = NO;
    }
    
    return status;
}

- (void)saveButtonAction:(id)sender
{
    BOOL status = [self validateInput];
    
    if (status) {
        __weak typeof(self) wo = self;
        
        AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
        
        NSString *userid = [NSString stringWithFormat:@"%d", account.user.id];
        NSString *current_password = [NSString stringWithFormat:@"%@", self.currentPasswordField.text];
        NSString *new_password = [NSString stringWithFormat:@"%@", self.confirmPasswordField.text];
        
        if ([new_password containsString:@" "]) {
            [wo setStatusMessage:@"Spaces are not allowed"];
            return;
        }
        
        [self.rm requestNewPasswordForUser:userid
                              withPassword:current_password
                               newPassword:new_password
                                 doneBlock:^(BOOL status) {
                                     if (status == YES) {
                                         wo.currentPasswordField.text = @"";
                                         [wo setStatusMessage:@"Change Password Successfully"];
                                         [wo cancelButtonAction:sender];
                                         if ([(NSObject *)wo.delegate respondsToSelector:@selector(didFinishChangedPasswordWithStatus:)]) {
                                             [wo.delegate didFinishChangedPasswordWithStatus:YES];
                                         }
                                     }
                                     
                                     if (status == NO) {
                                         wo.currentPasswordField.text = @"";
                                         [wo setStatusMessage:@"Change Password Failed"];
                                     }
                                 }];
    }
}

- (void)cancelButtonAction:(id)sender {

    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wo dismissViewControllerAnimated:YES completion:nil];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
