//
//  UserProfileViewController.m
//  V-Smart
//
//  Created by VhaL on 3/29/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "UserProfileViewController.h"
#import "JMSPeopleItem.h"
#import "HMSegmentedControl.h"
#import "UserProfileViewInfo.h"

@interface UserProfileViewController ()<UIScrollViewDelegate>
{
    BOOL isTeacher;
    
    NSTimer *timer;
    float progressValueFinish;
    bool startAnimate;
}

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) JMSRecordPeopleItem *profiles;

@property (nonatomic, weak) IBOutlet UILabel *labelFullName;
@property (nonatomic, weak) IBOutlet UILabel *labelNickName;
@property (nonatomic, weak) IBOutlet UILabel *labelAlias;
@property (nonatomic, weak) IBOutlet UIImageView *imageViewAvatar;
@property (nonatomic, weak) IBOutlet UIView *viewDetail;
@property (nonatomic, weak) IBOutlet UIButton *facebookButton;
@property (nonatomic, weak) IBOutlet UIButton *twitterButton;
@property (nonatomic, weak) IBOutlet UIButton *instagramButton;

@property (nonatomic, strong) UserProfileViewBasic *viewBasic;
@property (nonatomic, strong) UserProfileViewOther *viewOther;
@property (nonatomic, strong) UserProfileViewSchool *viewSchool;
@property (nonatomic, strong) UserProfileViewStatistics *viewStatistics;

@property (nonatomic, strong) HMSegmentedControl *segmentedControl;

- (IBAction)gotoSocial:(id)sender;
@end

@implementation UserProfileViewController
@synthesize parent, delegate, viewStatistics, viewSchool, viewOther, viewBasic, viewDetail;

-(AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

- (void)viewDidLoad{
    [super viewDidLoad];

    isTeacher = [[self account].user.position isEqualToString:kModeIsTeacher];
    timer = [NSTimer scheduledTimerWithTimeInterval:0.10 target:self selector:@selector(animateProgress:) userInfo:nil repeats:YES]; // Increase time interval from 0.05 to 0.05 so that progress view will still animate even if percentage is less than 10
    
    [self addSubviews];
    [self addHMSegmentedControl];
    [self clearValues];
}

-(void)clearValues{
    self.labelFullName.text = @"";
    self.labelNickName.text = @"";
    self.imageViewAvatar.image = nil;
    self.labelAlias.text = @"";

    viewBasic.detail1.text = @"";
    viewBasic.detail2.text = @"";
    viewBasic.birthday.text = @"";
    viewBasic.sex.text = @"";
    viewBasic.address.text = @"";
    viewBasic.facebook.text = @"";
    viewBasic.twitter.text = @"";
    viewBasic.instagram.text = @"";
    viewBasic.contact.text = @"";
    viewBasic.contactPerson.text = @"";
    viewBasic.birthPlace.text = @"";
    
    viewOther.spokenLanguage.text = @"";
    viewOther.hobbies.text = @"";
    viewOther.sports.text = @"";
    viewOther.aboutme.text = @"";
    viewOther.nationality.text = @"";
    
    viewSchool.preschoolname.text = @"";
    viewSchool.preschoolyear.text = @"";
    viewSchool.preschooladdress.text = @"";
    viewSchool.elementaryname.text = @"";
    viewSchool.elementaryyear.text = @"";
    viewSchool.elementaryaddress.text = @"";
    viewSchool.highschoolname.text = @"";
    viewSchool.highschoolyear.text = @"";
    viewSchool.highschooladdress.text = @"";
    
    viewStatistics.postedTopics.text = @"0";
    viewStatistics.repliedTopics.text = @"0";
    viewStatistics.feedback.text = @"0";
    viewStatistics.receivedLikes.text = @"0";
    viewStatistics.likedSoFar.text = @"0";
    viewStatistics.sharing.text = @"0";
}

-(void)addHMSegmentedControl{
    
    /* localizable strings */
    NSString *basicLabel = NSLocalizedString(@"Basic", nil); //checked
    NSString *otherInfoLabel = NSLocalizedString(@"Other Info", nil); //checked
    NSString *schoolsLabel = NSLocalizedString(@"Schools", nil); //checked
    NSString *statLabel = NSLocalizedString(@"Statistics", nil); //checked
    
    NSArray *headers = nil;
    if (isTeacher) {
        headers = @[basicLabel, otherInfoLabel, statLabel];
    } else {
        // Enhancement #1149
        // jca-04222016
        //headers = @[basicLabel, schoolsLabel, otherInfoLabel, statLabel];
        headers = @[basicLabel, otherInfoLabel, schoolsLabel, statLabel];
    }
    
    
    /*
     * LEGACY CODE
     *
     
    self.segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:headers];
    self.segmentedControl.frame = CGRectMake(0, 273, self.view.frame.size.width, 45);
    self.segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    self.segmentedControl.font = FONT_NEUE_LIGHT(15);
    self.segmentedControl.selectedSegmentIndex = 0;
    self.segmentedControl.selectionIndicatorHeight = 4.0f;
    self.segmentedControl.backgroundColor = UIColorFromHex(0x41adbe);
    self.segmentedControl.textColor = [UIColor whiteColor];
    self.segmentedControl.selectedTextColor = [UIColor whiteColor];
    self.segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    self.segmentedControl.selectionLocation = HMSegmentedControlSelectionLocationUp;
    */
    
    self.segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:headers];
    self.segmentedControl.frame = CGRectMake(0, 273, self.view.frame.size.width, 45);
    self.segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;

    self.segmentedControl.selectionIndicatorHeight = 4.0f;
    self.segmentedControl.backgroundColor = UIColorFromHex(0x41adbe);
    self.segmentedControl.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor],
                                                   NSFontAttributeName : FONT_NEUE_LIGHT(15) };
    self.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationUp;
    
    [self.segmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:self.segmentedControl];
    
    __weak typeof(self) weakSelf = self;
    [self.segmentedControl setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.scrollView scrollRectToVisible:CGRectMake(542 * index, 0, 542, 313) animated:YES];
    }];
}

-(void)addSubviews{
    [self.viewDetail setHidden:YES];
    NSArray *xibArray = [[NSBundle mainBundle] loadNibNamed:@"UserProfileViewController" owner:self options:nil];
    for (id xibObject in xibArray) {
        if ([xibObject isKindOfClass:[UserProfileViewBasic class]]) {
            viewBasic = (UserProfileViewBasic *)xibObject;
        }
        if ([xibObject isKindOfClass:[UserProfileViewOther class]]) {
            viewOther = (UserProfileViewOther *)xibObject;
        }
        if ([xibObject isKindOfClass:[UserProfileViewSchool class]]) {
            viewSchool = (UserProfileViewSchool *)xibObject;
        }
        if ([xibObject isKindOfClass:[UserProfileViewStatistics class]]) {
            viewStatistics = (UserProfileViewStatistics *)xibObject;
        }
    }
    
    //CGRect rect = CGRectMake(0, 0, viewDetail.frame.size.width, viewDetail.frame.size.height);
    //viewBasic.frame = viewOther.frame = viewSchool.frame = viewStatistics.frame = rect;
    
//    [viewDetail addSubview:viewBasic];
//    [viewDetail addSubview:viewOther];
//    [viewDetail addSubview:viewSchool];
//    [viewDetail addSubview:viewStatistics];
//    
//    [viewDetail bringSubviewToFront:viewBasic];
    
//    self.progressView.textColor = [UIColor whiteColor];
    self.progressView.textColor = [UIColor darkGrayColor];
    self.progressView.font = FONT_NEUE_THIN(15);
    self.progressView.thicknessRatio = 0.20f;
    self.progressView.progressFillColor = UIColorFromHex(0x56c877);
    self.progressView.roundedHead = 1;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 314, 542, 313)];
    [self.scrollView setBackgroundColor:[UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1]];
    [self.scrollView setPagingEnabled:YES];
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    
    // This is to avoid wrong computation of number of pages
    // which results to app crash (see scrollViewDidEndDecelerating)
    if (!isTeacher) {
        // Student's profile contains 4 views
        // 542 for each view
        [self.scrollView setContentSize:CGSizeMake(2168, 313)];
    }
    else {
        // Teacher's profile contains 3 views
        // 542 for each view
        [self.scrollView setContentSize:CGSizeMake(1626, 313)];
    }
    
    [self.scrollView setDelegate:self];
    [self.view addSubview:self.scrollView];
    
    CGRect frame = viewBasic.frame;
    
    if (!isTeacher) {
        [self.scrollView addSubview:viewBasic];
        [self.scrollView addSubview:viewOther];
        [self.scrollView addSubview:viewSchool];
        [self.scrollView addSubview:viewStatistics];
        
        [viewBasic setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        frame = viewOther.frame;
        [viewOther setFrame:CGRectMake(542, 0, frame.size.width, frame.size.height)];
        frame = viewSchool.frame;
        [viewSchool setFrame:CGRectMake(1084, 0, frame.size.width, frame.size.height)];
        frame = viewStatistics.frame;
        [viewStatistics setFrame:CGRectMake(1626, 0, frame.size.width, frame.size.height)];
    } else {
        [self.scrollView addSubview:viewBasic];
        [self.scrollView addSubview:viewOther];
        [self.scrollView addSubview:viewStatistics];
        
        [viewBasic setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        frame = viewOther.frame;
        [viewOther setFrame:CGRectMake(542, 0, frame.size.width, frame.size.height)];
        frame = viewStatistics.frame;
        [viewStatistics setFrame:CGRectMake(1084, 0, frame.size.width, frame.size.height)];
    }
}

-(IBAction)segmentedControlChangedValue:(id)sender{
    if (isTeacher){
        switch (self.segmentedControl.selectedSegmentIndex) {
            case 2:{
                self.progressView.progress = 0;
                startAnimate = YES;
                break;
            }
        }
    }
    else{
        switch (self.segmentedControl.selectedSegmentIndex) {
            case 3:{
                self.progressView.progress = 0;
                startAnimate = YES;
                break;
            }
        }
    }
}

- (IBAction)gotoSocial:(id)sender {
    NSString *text = @"";
    
    UIButton *b = (UIButton *)sender;
    NSInteger tag = b.tag;
    
    if (tag == 0) {
        text = VS_FMT(kSocialInstagram, viewBasic.instagram.text);
    } else if (tag == 1) {
        text = VS_FMT(kSocialFacebook, viewBasic.facebook.text);
    } else {
        text = VS_FMT(kSocialTwitter, viewBasic.twitter.text);
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:text]];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [self.segmentedControl setSelectedSegmentIndex:page animated:YES];
    
    // ENHANCEMENT#140: Should display percentage of completeness
    // Show profile completeness when swiping from other view to statistics view
    if (isTeacher) {
        if (page == 2) {
            self.progressView.progress = 0;
            startAnimate = YES;
        }
    }
    else {
        if (page == 3) {
            self.progressView.progress = 0;
            startAnimate = YES;
        }
    }
    
}

-(void)initializePhase{

    [self viewDidLoad];
    
    self.labelAlias.layer.cornerRadius = self.labelAlias.frame.size.height/2;
    self.imageViewAvatar.layer.cornerRadius = self.imageViewAvatar.frame.size.height/2;
    
    if (!self.account.user.avatar && self.account.user.avatar.length <= 0){
        self.labelAlias.hidden = NO;
        self.imageViewAvatar.hidden = YES;
        self.labelAlias.text = [VSmartHelpers aliasFromFirstName:self.account.user.firstname andLastName:self.account.user.lastname];
        self.labelAlias.backgroundColor = [VSmart sharedInstance].aliasBackgroundColor;
    }
    else{
        NSString *baseURL = [Utils buildUrl:[self account].user.avatar];
        self.labelAlias.hidden = YES;
        self.imageViewAvatar.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:baseURL]]];
        self.imageViewAvatar.hidden = NO;
    }
        
    NSString *url = VS_FMT([Utils buildUrl:kEndPointGetUserProfile], [self account].user.id);
    
    [JSONHTTPClient getJSONFromURLWithString:url completion:^(NSDictionary *json, JSONModelError *err) {
        if(err == nil){
            NSArray *arrayRecords = [json objectForKey:@"records"];
            [VSmartHelpers saveLastUserDefaultProfile:arrayRecords];
            [self fillUIFields:arrayRecords];            
        }
        else{
            /* localizable strings */
            NSString *connectionErrorMessage = NSLocalizedString(@"Please make sure you are connected to the server", nil); //checked
            [self.view makeToast:connectionErrorMessage duration:2.0f position:@"center"];
            
            NSArray *arrayRecords = [VSmartHelpers loadLastUserDefaultProfile];
            if (arrayRecords)
                [self fillUIFields:arrayRecords];
        }
    }];
}

- (NSString *)normalizeString:(NSString *)value {
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"]) {
        return @"-";
    }
    
    return value;
}

-(void)fillUIFields:(NSArray*)arrayRecords{
    
    self.labelFullName.text = @"";
    self.labelNickName.text = @"";
    self.labelAlias.text = @"";
    
    NSDictionary *dictProfile;
//    NSDictionary *dictStatistics;
    NSDictionary *dictSection;
    //            NSDictionary *dictSchool;
    //            NSDictionary *dictBadges;
    NSArray *arrayAdvisoryClasses;
    NSArray *arraySubjects;
    
    for (NSDictionary *dict in arrayRecords) {
        
        //Profile
        if ([dict objectForKey:@"Profile"]) {
            dictProfile = [[dict objectForKey:@"Profile"] objectAtIndex:0];
            
            NSString *avatar = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"avatar"]];
            NSString *firstName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"first_name"]];
            firstName = [self normalizeString:firstName];
            
            NSString *lastName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"last_name"]];
            lastName = [self normalizeString:lastName];
            
            NSString *midName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"mid_name"]];
            midName = [self normalizeString:midName];
            
            NSString *suffixName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"suffix_name"]];
            suffixName = [self normalizeString:suffixName];
            
            NSString *nickname = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"nickname"]];
            nickname = [self normalizeString:nickname];
            
            NSString *birthday = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"birth_date"]];
            birthday = [self normalizeString:birthday];
            
            NSString *sex = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"gender"]];
            sex = [self normalizeString:sex];
            
            NSString *address = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"address"]];
            address = [self normalizeString:address];
            
            NSString *facebook = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"facebook"]];
            facebook = [self normalizeString:facebook];
            
            NSString *twitter = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"twitter"]];
            twitter = [self normalizeString:twitter];
            
            NSString *instagram = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"instagram"]];
            instagram = [self normalizeString:instagram];
            
            NSString *contact = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"contact"]];
            contact = [self normalizeString:contact];
            
            NSString *contactPerson = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"contact_person"]];
            contactPerson = [self normalizeString:contactPerson];
            
            NSString *birthPlace = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"birth_place"]];
            birthPlace = [self normalizeString:birthPlace];
            
//            if ([midName isEqualToString:@"<null>"])
//                midName = @"";
            
//            if ([suffixName isEqualToString:@"<null>"])
//                suffixName = @"";
            
            if (0 < midName.length) {
                self.labelFullName.text = [NSString stringWithFormat:@"%@ %@ %@", firstName, midName, lastName];
            } else {
                self.labelFullName.text = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
            }
            
            if (0 < suffixName.length) {
                self.labelFullName.text = [NSString stringWithFormat:@"%@ %@", self.labelFullName.text, suffixName];
            }
            
            if (avatar.length <= 0){
                self.labelAlias.hidden = NO;
                self.imageViewAvatar.hidden = YES;
                self.labelAlias.text = [VSmartHelpers aliasFromFirstName:firstName andLastName:lastName];
                self.labelAlias.backgroundColor = [VSmart sharedInstance].aliasBackgroundColor;
            } else{
                self.labelAlias.hidden = YES;
                self.imageViewAvatar.hidden = NO;
            }
            
            self.labelNickName.text = nickname;
            
            viewBasic.birthday.text = birthday;
            if (!IsEmpty(birthday)) {
                NSDate *birthDate = [NSDate dateFromString:birthday withFormat:[NSDate dateFormatString]];
                viewBasic.birthday.text = [birthDate stringWithFormat:@"dd MMMM yyyy"];
            }

            viewBasic.sex.text = sex;
            viewBasic.address.text = address;
            viewBasic.facebook.text = facebook;
            viewBasic.twitter.text = twitter;
            viewBasic.instagram.text = instagram;
            viewBasic.contact.text = contact;
            viewBasic.contactPerson.text = contactPerson;
            viewBasic.birthPlace.text = birthPlace;
            
            UIImage *instagramImage = IsEmpty(instagram) ? [UIImage imageNamed:@"instagram-black-64"] : [UIImage imageNamed:@"instagram-64"];
            [self.instagramButton setImage:instagramImage forState:UIControlStateNormal];

            UIImage *facebookImage = IsEmpty(facebook) ? [UIImage imageNamed:@"facebook-black-64"] : [UIImage imageNamed:@"facebook-64"];
            [self.facebookButton setImage:facebookImage forState:UIControlStateNormal];
            
            UIImage *twitterImage = IsEmpty(twitter) ? [UIImage imageNamed:@"twitter-black-64"] : [UIImage imageNamed:@"twitter-64"];
            [self.twitterButton setImage:twitterImage forState:UIControlStateNormal];
            
            NSString *spokenLanguage = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"spoken_language"]];
            spokenLanguage = [self normalizeString:spokenLanguage];
            
            NSString *hobbies = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"hobbies"]];
            hobbies = [self normalizeString:hobbies];
            
            
            NSString *sports = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"sports"]];
            sports = [self normalizeString:sports];
            
            NSString *aboutMe = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"about_me"]];
            aboutMe = [self normalizeString:aboutMe];
            
            NSString *nationality = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"nationality"]];
            nationality = [self normalizeString:nationality];
            
            viewOther.spokenLanguage.text = spokenLanguage;
            viewOther.hobbies.text = hobbies;
            viewOther.sports.text = sports;
            viewOther.nationality.text = nationality;
            viewOther.aboutme.text = aboutMe;
            
            NSString *preSchoolAddress = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"pre_school_address"]];
            preSchoolAddress = [self normalizeString:preSchoolAddress];
            
            NSString *preSchoolName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"pre_school_name"]];
            preSchoolName = [self normalizeString:preSchoolName];
            
            NSString *preSchoolYear = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"pre_school_year"]];
            preSchoolYear = [self normalizeString:preSchoolYear];
            
            NSString *elemSchoolAddress = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"elem_school_address"]];
            elemSchoolAddress = [self normalizeString:elemSchoolAddress];
            
            NSString *elemSchoolName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"elem_school_name"]];
            elemSchoolName = [self normalizeString:elemSchoolName];
            
            NSString *elemSchoolYear = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"elem_school_year"]];
            elemSchoolYear = [self normalizeString:elemSchoolYear];
            
            NSString *highSchoolAddress = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"high_school_address"]];
            highSchoolAddress = [self normalizeString:highSchoolAddress];
            
            NSString *highSchoolName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"high_school_name"]];
            highSchoolName = [self normalizeString:highSchoolName];
            
            NSString *highSchoolYear = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"high_school_year"]];
            highSchoolYear = [self normalizeString:highSchoolYear];
            
            viewSchool.preschoolname.text = preSchoolName;
            viewSchool.preschoolyear.text = [NSString stringWithFormat:@"SY%@", preSchoolYear];
            viewSchool.preschooladdress.text = preSchoolAddress;
            
            viewSchool.elementaryname.text = elemSchoolName;
            viewSchool.elementaryyear.text = [NSString stringWithFormat:@"SY%@", elemSchoolYear];
            viewSchool.elementaryaddress.text = elemSchoolAddress;
            
            viewSchool.highschoolname.text = highSchoolName;
            viewSchool.highschoolyear.text = [NSString stringWithFormat:@"SY%@", highSchoolYear];
            viewSchool.highschooladdress.text = highSchoolAddress;
            
            NSString *infoPercentage = [NSString stringWithFormat:@"%@%%", [dictProfile objectForKey:@"info_percentage"]];
            viewStatistics.percentCompleted.text = infoPercentage;

            float percentage = 0;
            
            if ([infoPercentage rangeOfString:@"null"].location == NSNotFound) {
                percentage = [[dictProfile objectForKey:@"info_percentage"] doubleValue];
            }
            
            UIColor *innerColor = UIColorFromHex(0x32b6f4);
            if (percentage <= 40) {
                innerColor = UIColorFromHex(0xf49a86);
            } else if (percentage > 41 && percentage <= 75) {
                innerColor = UIColorFromHex(0xaacbf4);
            } else if (percentage > 75 && percentage <= 99) {
                innerColor = UIColorFromHex(0x71f49c);
            } 
            self.progressView.innerBackgroundColor = innerColor;
            
            progressValueFinish = percentage / 100;
//            self.progressView.progress = percentage / 100;

            continue;
        }
        
        //Section
        if ([dict objectForKey:@"Section"]) {
            dictSection = [[dict objectForKey:@"Section"] objectAtIndex:0];
            
            NSString *firstName = [NSString stringWithFormat:@"%@", [dictSection objectForKey:@"adviser_first_name"]];
            firstName = [self normalizeString:firstName];
            
            NSString *lastName = [NSString stringWithFormat:@"%@", [dictSection objectForKey:@"adviser_last_name"]];
            lastName = [self normalizeString:lastName];
            
            NSString *midName = [NSString stringWithFormat:@"%@", [dictSection objectForKey:@"adviser_middle_name"]];
            midName = [self normalizeString:midName];
            
            NSString *suffixName = [NSString stringWithFormat:@"%@", [dictSection objectForKey:@"adviser_suffix_name"]];
            suffixName = [self normalizeString:suffixName];
            
            NSString *grade = [NSString stringWithFormat:@"%@", [dictSection objectForKey:@"grade"]];
            grade = [self normalizeString:grade];
            
            NSString *section = [NSString stringWithFormat:@"%@", [dictSection objectForKey:@"section"]];
            section = [self normalizeString:section];
            
//            if ([midName isEqualToString:@"<null>"])
//                midName = @"";
            
//            if ([suffixName isEqualToString:@"<null>"])
//                suffixName = @"";
            
            NSString *fullName;
            
            if (0 < midName.length) {
                fullName = [NSString stringWithFormat:@"%@ %@ %@", firstName, midName, lastName];
            } else {
                fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
            }
            
            if (0 < suffixName.length) {
                fullName = [NSString stringWithFormat:@"%@ %@", fullName, suffixName];
            }
            
            viewBasic.detail1.text = [NSString stringWithFormat:@"%@ - %@", grade, section];
            
            continue;
        }
        
        //Advisory Classes
        if ([dict objectForKey:@"Advisory_Classes"]) {
            arrayAdvisoryClasses = [dict objectForKey:@"Advisory_Classes"];
            
            NSString *advisoryClass = @"";
            
            for (NSDictionary *dict in arrayAdvisoryClasses) {
                
                if (advisoryClass.length <= 0) {
                    advisoryClass = [NSString stringWithFormat:@"%@", [dict objectForKey:@"section"]];
                } else {
                    advisoryClass = [NSString stringWithFormat:@"%@, %@", advisoryClass, [dict objectForKey:@"section"]];
                }
            }
            
            continue;
        }
        
        //Subjects
        if ([dict objectForKey:@"Subjects"]) {
            arraySubjects = [dict objectForKey:@"Subjects"];
            
            for (NSDictionary *dict in arraySubjects) {
                
                if (viewBasic.detail2.text <= 0) {
                    viewBasic.detail2.text = [NSString stringWithFormat:@"%@", [dict objectForKey:@"name"]];
                } else {
                    viewBasic.detail2.text = [NSString stringWithFormat:@"%@, %@", viewBasic.detail2.text, [dict objectForKey:@"name"]];
                }
            }
            
            continue;
        }

        //Statistics
        NSString *statistics_object = [NSString stringWithFormat:@"%@", dict[@"Statistics"] ];
        
        NSDictionary *dictStatistics = nil;
        if ([statistics_object isEqualToString:@"0"]) {
            dictStatistics = @{@"count_liked":@"0",
                               @"count_posted":@"0",
                               @"count_received_likes":@"0",
                               @"count_replied":@"0"};
            
            if ([dictStatistics objectForKey:@"count_liked"]){
                NSString *countLike = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_liked"]];
                countLike = [self normalizeString:countLike];
                
                viewStatistics.likedSoFar.text = countLike;
            }
            
            if ([dictStatistics objectForKey:@"count_posted"]){
                NSString *countPosted = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_posted"]];
                countPosted = [self normalizeString:countPosted];
                
                viewStatistics.postedTopics.text = countPosted;
            }
            
            if ([dictStatistics objectForKey:@"count_received_likes"]){
                NSString *countReceivedLikes = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_received_likes"]];
                countReceivedLikes = [self normalizeString:countReceivedLikes];
                
                viewStatistics.receivedLikes.text = countReceivedLikes;
            }
            
            if ([dictStatistics objectForKey:@"count_replied"]){
                NSString *countReplied = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_replied"]];
                countReplied = [self normalizeString:countReplied];
                
                viewStatistics.repliedTopics.text = countReplied;
            }
            
            continue;
        }

        if (![statistics_object isEqualToString:@"0"]) {
            dictStatistics = [dict objectForKey:@"Statistics"];
            
            if ([dictStatistics objectForKey:@"count_liked"]){
                NSString *countLike = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_liked"]];
                countLike = [self normalizeString:countLike];
                
                viewStatistics.likedSoFar.text = countLike;
            }
            
            if ([dictStatistics objectForKey:@"count_posted"]){
                NSString *countPosted = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_posted"]];
                countPosted = [self normalizeString:countPosted];
                
                viewStatistics.postedTopics.text = countPosted;
            }
            
            if ([dictStatistics objectForKey:@"count_received_likes"]){
                NSString *countReceivedLikes = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_received_likes"]];
                countReceivedLikes = [self normalizeString:countReceivedLikes];
                
                viewStatistics.receivedLikes.text = countReceivedLikes;
            }
            
            if ([dictStatistics objectForKey:@"count_replied"]){
                NSString *countReplied = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_replied"]];
                countReplied = [self normalizeString:countReplied];
                
                viewStatistics.repliedTopics.text = countReplied;
            }
            
            continue;
        }
        
        //School
        //                if ([dict objectForKey:@"School"]) {
        //                    dictSchool = [dict objectForKey:@"School"];
        //
        ////                    NSString *gradeLevel = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"grade_level"]];
        //                    NSString *schoolAddress = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"school_address"]];
        //                    NSString *schoolName = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"school_name"]];
        //                    NSString *schoolYear = [NSString stringWithFormat:@"%@", [dictProfile objectForKey:@"school_year"]];
        //
        //                    viewSchool.preschoolname.text = schoolName;
        //                    viewSchool.preschoolyear.text = [NSString stringWithFormat:@"SY%@", schoolYear];
        //                    viewSchool.preschooladdress.text = schoolAddress;
        //
        //                    viewSchool.elementaryname.text = schoolName;
        //                    viewSchool.elementaryyear.text = [NSString stringWithFormat:@"SY%@", schoolYear];
        //                    viewSchool.elementaryaddress.text = schoolAddress;
        //
        //                    viewSchool.highschoolname.text = schoolName;
        //                    viewSchool.highschoolyear.text = [NSString stringWithFormat:@"SY%@", schoolYear];
        //                    viewSchool.highschooladdress.text = schoolAddress;
        //
        //                    continue;
        //                }
        
        //Badges
        if ([dict objectForKey:@"Badges"]) {
            //                    dictBadges = [[dict objectForKey:@"Badges"] objectAtIndex:0];
            continue;
        }
    }
    
    
    if ([[self account].user.position isEqualToString:kModeIsStudent]){
        //                self.labelDetail1.text = @"Student";
        //                self.labelDetail2.text = @"";
    }
}

-(IBAction)animateProgress:(id)sender{
    if (startAnimate){
        self.progressView.progress += 0.01;
        
        if (progressValueFinish <= self.progressView.progress){
            startAnimate = NO;
        }
    }
}

@end









