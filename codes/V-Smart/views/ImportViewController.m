//
//  ImportViewController.m
//  Booklatan
//
//  Created by Earljon Hidalgo on 5/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ImportViewController.h"

@interface ImportViewController ()
{
    BOOL hasActiveAccount;
}
@end

@implementation ImportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [_uuidLabel setText:[[VSmart sharedInstance] uuid]];
    NSString *account = [NSString stringWithFormat:@"%@", [[VSmart sharedInstance] email]];
    [_accountLabel setText:account];
    
    hasActiveAccount = !IsEmpty([[VSmart sharedInstance] email]);
}

-(void)viewDidUnload
{
    [self setUuidLabel:nil];
    [self setAccountLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return NO;
}

-(IBAction)closeView:(id)sender
{
    if (!hasActiveAccount) {
        [self dismissViewControllerAnimated:YES completion:^{
            AlertWithMessageAndDelegate(@"Login Required", @"No logged-in account found! Please login first before using the book import feature.", nil);
        }];
    } else {
        NSArray *epubs = [Utils importedBooks];
        //VLog(@"Epub Uploaded: \n%@", epubs);
        
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kImportServerPostUpdate object:epubs];
    }
}

-(void)dealloc {
//    [_uuidLabel release];
//    [_accountLabel release];
//    [super dealloc];
}
@end
