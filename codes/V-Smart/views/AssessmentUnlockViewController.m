//
//  AssessmentUnlockViewController.m
//  V-Smart
//
//  Created by VhaL on 4/5/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "AssessmentUnlockViewController.h"

@interface AssessmentUnlockViewController ()

@end

@implementation AssessmentUnlockViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(IBAction)buttonGoTapped:(id)sender{

    if ([self.delegate respondsToSelector:@selector(assessmentCodeUnlock:withCode:)])
        [self.delegate performSelector:@selector(assessmentCodeUnlock:withCode:) withObject:self.exerId withObject:self.textField.text];
}

@end
