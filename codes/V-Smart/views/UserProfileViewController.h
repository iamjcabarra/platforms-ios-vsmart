//
//  UserProfileViewController.h
//  V-Smart
//
//  Created by VhaL on 3/29/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PICircularProgressView.h"

@interface UserProfileViewController : UIViewController
@property (nonatomic, assign) id parent;
@property (nonatomic, assign) id delegate;
@property (weak, nonatomic) IBOutlet PICircularProgressView *progressView;
-(void)initializePhase;
@end











