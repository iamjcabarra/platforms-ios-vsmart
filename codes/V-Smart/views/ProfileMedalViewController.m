//
//  ProfileMedalViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/23/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "ProfileMedalViewController.h"
#import "ProfileMedalCell.h"
#import "CALayer+Additions.h"
#import "TapDetectingWindow.h"

@interface ProfileMedalViewController ()
{
    NSDateFormatter *formatter;
}
@end

@implementation ProfileMedalViewController
@synthesize items;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil medalType:(NSString *) type
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.medalType = [[NSString alloc] init];
        self.medalType = type;
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.view.layer.cornerRadius = 10;
    self.view.layer.masksToBounds = YES;
    
    //self.medalType = self.headerText.text;
    VLog(@"didLoad: %@", self.medalType);
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    self.headerText.layer.mask = [CALayer maskLayerWithCorners:UIRectCornerTopLeft | UIRectCornerTopRight radii:CGSizeMake(10.0f, 10.0f) frame:self.headerText.bounds];

    self.closeButton.layer.mask = [CALayer maskLayerWithCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight radii:CGSizeMake(10.0f, 10.0f) frame:self.closeButton.bounds];
    
    TapDetectingWindow *tapWindow = (TapDetectingWindow *)[[UIApplication sharedApplication].windows objectAtIndex:0];
    tapWindow.controllerThatObserves = nil;
    tapWindow.viewToObserve = nil;
    
    [self _changeHeaderLabel];
    [self populateData];
}

#pragma mark - Private Helpers
-(void) populateData {
    NSArray *results = [VSmartHelpers getBookExerciseResults];
    
    NSMutableArray *filtered = [[NSMutableArray alloc] init];
    
    VLog(@"MedalType: %@", self.medalType);
    NSMutableArray *headerArray = [[NSMutableArray alloc] init];
    
    for (BookExerciseResult *result in results) {
        float grade = ((float)(result.result)/(float)result.bookExercise.totalItems) * 50 + 50;
        
        VLog(@"BookName: %@: Result: %i/%i: Grade: %0.2f", result.bookTitle, result.result, result.bookExercise.totalItems, grade);
        
        if (grade >= 85 && grade < 90) {
            if([self.medalType isEqualToString:kMedalBronze]) {
                [filtered addObject:result];
                [headerArray addObject:result.bookTitle];
            }
        } else if (grade >= 90 && grade < 95) {
            if([self.medalType isEqualToString:kMedalSilver]) {
                [filtered addObject:result];
                [headerArray addObject:result.bookTitle];
            }
        } else if (grade >= 95 && grade < 98) {
            if([self.medalType isEqualToString:kMedalGold]) {
                [filtered addObject:result];
                [headerArray addObject:result.bookTitle];
            }
        } else if (grade >= 98) {
            if([self.medalType isEqualToString:kMedalPlatinum]) {
                [filtered addObject:result];
                [headerArray addObject:result.bookTitle];
            }
        }
    }
    
    VLog(@"Filtered: %@", filtered);
    _headerSets = [[NSOrderedSet orderedSetWithArray:headerArray] array];
    VLog(@"Sets: %@", _headerSets);
    
    NSMutableArray *resultItems = [[NSMutableArray alloc] init];
    for (NSString *header in _headerSets) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:header forKey:@"header"];
        
        NSMutableArray *itemRow = [[NSMutableArray alloc] init];
        for (BookExerciseResult *itemResult in filtered) {
            if ([itemResult.bookTitle isEqualToString:header]) {
                [itemRow addObject:itemResult];
            }
        }
        
        [dict setObject:itemRow forKey:@"row"];
        [resultItems addObject:dict];
    }
    
    self.items = [[NSArray alloc] initWithArray:resultItems];
    //VLog(@"resultItems: %@", self.items);
}

-(void) _changeHeaderLabel {
    NSString *text = @"";
    
    if ([self.medalType isEqualToString:kMedalPlatinum]) {
        text = @"Platinum Medals";
    } else if ([self.medalType isEqualToString:kMedalGold]) {
        text = @"Gold Medals";
    } else if ([self.medalType isEqualToString:kMedalSilver]) {
        text = @"Silver Medals";
    } else {
        text = @"Bronze Medals";
    }
    self.headerText.text = text;
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView
// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.items count];
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *rows = [[self.items objectAtIndex:section] objectForKey:@"row"];
    
    return [rows count] ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"ProfileMedalCellIdentifier";
    static NSString *CellNib = @"ProfileMedalCell";
    
    ProfileMedalCell *cell = (ProfileMedalCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellNib owner:self options:nil];
        cell = (ProfileMedalCell *)[nib objectAtIndex:0];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //cell.contentView.backgroundColor = [UIColor colorWithRed:220/255.0 green:237/255.0 blue:245/255.0 alpha:1.0];
    }
    
    BookExerciseResult *result = [[[self.items objectAtIndex:indexPath.section] objectForKey:@"row"]
                           objectAtIndex:indexPath.row];
    
    //BookExerciseResult *result = [self.items objectAtIndex:indexPath.row];
    cell.chapterName.text = result.chapterName;
    cell.latestScore.text = [NSString stringWithFormat:@"%i/%i", result.result, result.bookExercise.totalItems];
    cell.exerciseNumber.text = [NSString stringWithFormat:@"Exercise %i", result.bookExercise.exerciseId];
    
    [formatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
    NSString *dateString = [formatter stringFromDate:result.dateCreated];
    cell.dateTaken.text = dateString;

    
	return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    UIColor *color = ((indexPath.row % 2) == 0) ? UIColorFromHex(0xf4f4f4) : [UIColor whiteColor];
    cell.contentView.backgroundColor = color;
}

//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
//    return [self.items valueForKey:@"header"];
//}

//- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
//    return [self.headerSets indexOfObject:title];
//}

//- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section {
//	return [[self.items objectAtIndex:section] objectForKey:@"header"];
//    
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 51.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40.0)];
    header.backgroundColor = [UIColor whiteColor];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:header.frame];
    textLabel.text = VS_FMT(@"   %@", [[self.items objectAtIndex:section] objectForKey:@"header"]);
    textLabel.backgroundColor = [UIColor whiteColor];
    textLabel.textColor = [UIColor blackColor];
    textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0f];

    [header addSubview:textLabel];
    
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

-(IBAction)close:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCloseProfileMedalWindow:)]) {
        [self.delegate didCloseProfileMedalWindow:self];
    }
}
@end
