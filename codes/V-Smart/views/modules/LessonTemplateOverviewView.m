//
//  LessonTemplateOverviewView.m
//  V-Smart
//
//  Created by Julius Abarra on 08/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "LessonTemplateOverviewView.h"
#import "QuarterOptionPopOverViewController.h"
#import "TGTBDatePickerView.h"
#import "LessonPlanDataManager.h"

@interface LessonTemplateOverviewView () <UITextFieldDelegate, UITextViewDelegate, TGTBDatePickerViewDelegate>

@property (strong, nonatomic) LessonPlanDataManager *lpdm;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) TGTBDatePickerView *datePickerView;
@property (strong, nonatomic) UIPopoverController *datePickerPopOverController;
@property (nonatomic, retain) UIPopoverController *actionPopOverController;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *schoolNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *schoolAddressLabel;
@property (strong, nonatomic) IBOutlet UILabel *gradeLevelLabel;
@property (strong, nonatomic) IBOutlet UILabel *unitLabel;
@property (strong, nonatomic) IBOutlet UILabel *quarterLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeFrameLabel;

@property (strong, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) IBOutlet UITextField *schoolNameTextField;
@property (strong, nonatomic) IBOutlet UITextView *schoolAddressTextView;
@property (strong, nonatomic) IBOutlet UITextField *gradeLevelTextField;
@property (strong, nonatomic) IBOutlet UITextField *unitTextField;
@property (strong, nonatomic) IBOutlet UITextField *quarterTextField;
@property (strong, nonatomic) IBOutlet UITextField *startDateTextField;
@property (strong, nonatomic) IBOutlet UITextField *endDateTextField;

@property (strong, nonatomic) IBOutlet UIButton *startDateButton;
@property (strong, nonatomic) IBOutlet UIButton *endDateButton;
@property (strong, nonatomic) IBOutlet UIButton *quarterButton;

@property (assign, nonatomic) NSInteger timeFrameButtonTag;

@end

@implementation LessonTemplateOverviewView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Data Manager and Context
    self.lpdm = [LessonPlanDataManager sharedInstance];
    self.managedObjectContext = self.lpdm.mainContext;
    
    // Localize Labels
    [self localizeLabels];
    
    // Set Up Delegation
    [self setUpDelegation];
    
    // Set Up Date Picker
    [self setUpDatePickerView];
    
    // Quarter Button Action
    [self.quarterButton addTarget:self
                           action:@selector(showPopOverQuarterOption:)
                 forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Render Overview Data
    [self loadLessonContentOverviewData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)localizeLabels {
    NSString *title = NSLocalizedString(@"Title", nil);
    NSString *schoolName = NSLocalizedString(@"School Name", nil);
    NSString *schoolAddress = NSLocalizedString(@"School Address", nil);
    NSString *gradeLevel = NSLocalizedString(@"Grade Level", nil);
    NSString *unit = NSLocalizedString(@"Unit", nil);
    NSString *quarter = NSLocalizedString(@"Quarter", nil);
    NSString *timeFrame = NSLocalizedString(@"Time Frame", nil);
    
    self.titleLabel.text = [title uppercaseString];
    self.schoolNameLabel.text = [schoolName uppercaseString];
    self.schoolAddressLabel.text = [schoolAddress uppercaseString];
    self.gradeLevelLabel.text = [gradeLevel uppercaseString];
    self.unitLabel.text = [unit uppercaseString];
    self.quarterLabel.text = [quarter uppercaseString];
    self.timeFrameLabel.text = [timeFrame uppercaseString];
}

- (void)setUpDelegation {
    self.titleTextField.delegate = self;
    self.schoolNameTextField.delegate = self;
    self.schoolAddressTextView.delegate = self;
    self.gradeLevelTextField.delegate = self;
    self.unitTextField.delegate = self;
    self.quarterTextField.delegate = self;
    self.startDateTextField.delegate = self;
    self.endDateTextField.delegate = self;
}

- (void)loadLessonContentOverviewData {
    if (self.copiedLessonObject != nil) {
        NSString *name = [self.copiedLessonObject valueForKey:@"name"];
        NSString *school_name = [self.copiedLessonObject valueForKey:@"school_name"];
        NSString *school_address = [self.copiedLessonObject valueForKey:@"school_address"];
        NSString *grade_level = [self.copiedLessonObject valueForKey:@"level"];
        NSString *unit = [self.copiedLessonObject valueForKey:@"unit"];
        NSString *quarter = [self.copiedLessonObject valueForKey:@"quarter"];
        NSString *start_date = [self.copiedLessonObject valueForKey:@"start_date"];
        NSString *end_date = [self.copiedLessonObject valueForKey:@"end_date"];
        
        self.titleTextField.text = name;
        self.schoolNameTextField.text = school_name;
        self.schoolAddressTextView.text = school_address;
        self.gradeLevelTextField.text = grade_level;
        self.unitTextField.text = unit;
        self.startDateTextField.text = start_date;
        self.endDateTextField.text = end_date;
        
        if (![quarter isEqualToString:@""]) {
            quarter = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Quarter", nil), quarter];
        }
        
        self.quarterTextField.text = quarter;
    }
}

- (NSString *)keyForTextField:(UITextField *)textField {
    NSString *key = @"name";
    
    // Title
    if (textField == self.titleTextField) {
        key = @"name";
        return key;
    }
    
    // School Name
    if (textField == self.schoolNameTextField) {
        key = @"school_name";
        return key;
    }
    
    // Grade Level
    if (textField == self.gradeLevelTextField) {
        key = @"level";
        return key;
    }
    
    // Unit
    if (textField == self.unitTextField) {
        key = @"unit";
        return key;
    }
    
    // Quarter
    if (textField == self.quarterTextField) {
        key = @"quarter";
        return key;
    }
    
    // Start Date
    if (textField == self.startDateTextField) {
        key = @"start_date";
        return key;
    }
    
    // End Date
    if (textField == self.endDateTextField) {
        key = @"end_date";
        return key;
    }
    
    return key;
}

- (NSString *)keyForTextView:(UITextView *)textView {
    NSString *key = @"school_address";
    
    // School Address
    if (textView == self.schoolAddressTextView) {
        key = @"school_address";
        return key;
    }
    
    return key;
}

#pragma mark - Date Picker View

- (void)setUpDatePickerView {
    self.startDateButton.tag = 100;
    self.endDateButton.tag = 200;
    
    [self.startDateButton addTarget:self
                             action:@selector(showPopOverDatePicker:)
                   forControlEvents:UIControlEventTouchUpInside];
    
    [self.endDateButton addTarget:self
                           action:@selector(showPopOverDatePicker:)
                 forControlEvents:UIControlEventTouchUpInside];
}

- (void)showPopOverDatePicker:(id)sender {
    UIButton *button = (UIButton *)sender;
    self.timeFrameButtonTag = button.tag;

    // Create Date Picker Controller
    self.datePickerView = [[TGTBDatePickerView alloc] initWithNibName:@"TGTBDatePickerView" bundle:nil];
    self.datePickerView.dateType = button.tag;
    self.datePickerView.delegate = self;
    
    // Create Popover Controller
    self.datePickerPopOverController = [[UIPopoverController alloc] initWithContentViewController:self.datePickerView];
    self.datePickerPopOverController.popoverContentSize = CGSizeMake(315.0f, 263.0f);
    
    
    // Render Popover
    [self.datePickerPopOverController presentPopoverFromRect:button.bounds
                                                      inView:button
                                    permittedArrowDirections:UIPopoverArrowDirectionDown
                                                    animated:YES];
    
    [self.view endEditing:YES];
}

- (void)selectedDate:(NSString *)dateString {
    UITextField *textField = self.startDateTextField;
    
    if (self.timeFrameButtonTag == 100) {
        self.startDateTextField.text = dateString;
        textField = self.startDateTextField;
    }
    
    if (self.timeFrameButtonTag == 200) {
        self.endDateTextField.text = dateString;
        textField = self.endDateTextField;
    }
    
    NSString *key = [self keyForTextField:textField];
    NSDictionary *userEntries = @{key:dateString};
    NSString *lp_id = [self.lpdm stringValue:[self.copiedLessonObject valueForKey:@"lp_id"]];
    NSPredicate *predicate = [self.lpdm predicateForKeyPath:@"lp_id" andValue:lp_id];
    [self.lpdm updateLessonPlanLocally:kCopyLessonEntity predicate:predicate details:userEntries];
    [self postUpdateProgress];
}

#pragma mark - Quarter Option View

- (void)showPopOverQuarterOption:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    // Create Quarter Picker Controller
    QuarterOptionPopOverViewController *quarterOption = [[QuarterOptionPopOverViewController alloc]initWithNibName:@"QuarterOptionPopOverViewController" bundle:nil];
    
    // Create Popover Controller
    self.actionPopOverController = [[UIPopoverController alloc] initWithContentViewController:quarterOption];
    self.actionPopOverController.popoverContentSize = CGSizeMake(160.0, 140.0);
    
    [self.actionPopOverController presentPopoverFromRect:button.bounds
                                                  inView:button
                                permittedArrowDirections:UIPopoverArrowDirectionDown
                                                animated:YES];
    // Implement Action Buttons
    [quarterOption.butOption1 addTarget:self
                                 action:@selector(optionAction:)
                       forControlEvents:UIControlEventTouchUpInside];
    
    [quarterOption.butOption2 addTarget:self
                                 action:@selector(optionAction:)
                       forControlEvents:UIControlEventTouchUpInside];
    
    [quarterOption.butOption3 addTarget:self
                                 action:@selector(optionAction:)
                       forControlEvents:UIControlEventTouchUpInside];
    
    [quarterOption.butOption4 addTarget:self
                                 action:@selector(optionAction:)
                       forControlEvents:UIControlEventTouchUpInside];
}

- (void)optionAction:(id)sender {
    UIButton *button = (UIButton *)sender;
    NSInteger tag = button.tag;
    NSString *option = @"1";
    
    switch (tag) {
        case 0:
            option = @"1";
            break;
        case 1:
            option = @"2";
            break;
        case 2:
            option = @"3";
            break;
        case 3:
            option = @"4";
            break;
        default:
            break;
    }
    
    self.quarterTextField.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Quarter", nil), option];
    
    NSString *key = [self keyForTextField:self.quarterTextField];
    NSDictionary *userEntries = @{key:option};
    NSString *lp_id = [self.lpdm stringValue:[self.copiedLessonObject valueForKey:@"lp_id"]];
    NSPredicate *predicate = [self.lpdm predicateForKeyPath:@"lp_id" andValue:lp_id];
    [self.lpdm updateLessonPlanLocally:kCopyLessonEntity predicate:predicate details:userEntries];
    [self postUpdateProgress];
    
    [self.actionPopOverController dismissPopoverAnimated:YES];
    [self.actionPopOverController.delegate popoverControllerDidDismissPopover:self.actionPopOverController];
}

#pragma mark - Text Field Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (self.copiedLessonObject != nil) {
        if (textField == self.titleTextField) {
            if ([updatedText length] > 50) {
                return NO;
            }
        }
        
        if (textField == self.unitTextField) {
            NSCharacterSet *numberSet = [NSCharacterSet decimalDigitCharacterSet];
            
            if ([string rangeOfCharacterFromSet:[numberSet invertedSet]].location != NSNotFound) {
                return NO;
            }
        }
        
        if ([string length] == 0) {
            NSString *key = [self keyForTextField:textField];
            NSDictionary *userEntries = @{key:updatedText};
            NSString *lp_id = [self.lpdm stringValue:[self.copiedLessonObject valueForKey:@"lp_id"]];
            NSPredicate *predicate = [self.lpdm predicateForKeyPath:@"lp_id" andValue:lp_id];
            [self.lpdm updateLessonPlanLocally:kCopyLessonEntity predicate:predicate details:userEntries];
        }
        
        NSString *key = [self keyForTextField:textField];
        NSDictionary *userEntries = @{key:updatedText};
        NSString *lp_id = [self.lpdm stringValue:[self.copiedLessonObject valueForKey:@"lp_id"]];
        NSPredicate *predicate = [self.lpdm predicateForKeyPath:@"lp_id" andValue:lp_id];
        [self.lpdm updateLessonPlanLocally:kCopyLessonEntity predicate:predicate details:userEntries];
        [self postUpdateProgress];
    }
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    NSString *key = [self keyForTextView:textView];
    NSDictionary *userEntries = @{key:textView.text};
    NSString *lp_id = [self.lpdm stringValue:[self.copiedLessonObject valueForKey:@"lp_id"]];
    NSPredicate *predicate = [self.lpdm predicateForKeyPath:@"lp_id" andValue:lp_id];
    [self.lpdm updateLessonPlanLocally:kCopyLessonEntity predicate:predicate details:userEntries];
    [self postUpdateProgress];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

#pragma mark - Post Notification

- (void)postUpdateProgress {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateProgress"
                                                        object:nil
                                                      userInfo:nil];
}

@end
