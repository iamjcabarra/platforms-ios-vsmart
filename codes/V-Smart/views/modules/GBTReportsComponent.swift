//
//  GBTReportsComponent.swift
//  V-Smart
//
//  Created by Ryan Migallos on 18/10/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTReportsComponent: UIViewController, UIWebViewDelegate {

    @IBOutlet var webComponent: UIWebView!
    @IBOutlet var customBackButton: UIButton!
    
    private var initialURLRequest: URLRequest!
    private var stringURL = ""
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //TEST
//        let path = "http://dev.vsmartschool.com/reports/webviewschoolgrades/2"
//        let url:URL! = URL(string: path)
//        let request:URLRequest! = URLRequest(url: url)
//        self.webComponent.loadRequest(request)

//        let request:URLRequest! = gbdm.gradeBookReports()
//        print("------- REQUEST : \(request!)")
//        self.webComponent.loadRequest(request!)
        
        self.customBackButton.isHidden = true
        
        self.webComponent.delegate = self
        let request: URLRequest = gbdm.gradeBookReports()
        print("------- REQUEST : \(request)")
        self.stringURL = (request.url?.absoluteString.lowercased())!
        print("------- STRING URL : \(self.stringURL)")
        self.webComponent.loadRequest(request)
        
        let action = #selector(GBTReportsComponent.buttonBackAction(button:))
        self.customBackButton.addTarget(self, action:action , for: .touchUpInside)
    }

    func buttonBackAction(button: UIButton) {
        self.webComponent.goBack()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let newStringURL = webView.request!.url?.absoluteString.lowercased()
        print("NEW STRING URL: \(newStringURL)")
        self.customBackButton.isHidden = newStringURL == self.stringURL ? true : false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
