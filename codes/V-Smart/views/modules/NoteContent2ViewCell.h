//
//  NoteContent2ViewCell.h
//  V-Smart
//
//  Created by VhaL on 2/12/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteContent2ViewCell : UITableViewCell <UITextFieldDelegate>
@property (nonatomic, strong) IBOutlet UITextField *textFieldTitle;
@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) UILabel *cell1Label;
@property (nonatomic, strong) UITextView *cell3TextViewDescription;
@property (nonatomic, strong) UIButton *cell5SaveButton;
-(void)initializeCellWithDelegate:(id)parent;
-(IBAction)textFieldDidChange:(UITextField *)textField;
@end
