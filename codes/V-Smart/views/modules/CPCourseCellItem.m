//
//  CPCourseCellItem.m
//  V-Smart
//
//  Created by Julius Abarra on 28/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "CPCourseCellItem.h"

@interface CPCourseCellItem ()

@property (strong, nonatomic) IBOutlet UIImageView *courseImage;

@end

@implementation CPCourseCellItem

- (void)awakeFromNib {
    [super awakeFromNib];
    self.courseImage.image = [UIImage imageNamed:@"course-list2.png"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
