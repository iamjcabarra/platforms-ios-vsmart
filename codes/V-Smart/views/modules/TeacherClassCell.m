//
//  TeacherClassCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 4/20/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "TeacherClassCell.h"

@implementation TeacherClassCell

- (void)highlight:(BOOL)flag {

    UIColor *textColor = (flag) ? [UIColor whiteColor] : [UIColor darkTextColor];
    self.sectionLabel.textColor = textColor;
    
    UIColor *bgcolor = (flag) ? [UIColor darkTextColor] : [UIColor whiteColor];
    self.sectionLabel.backgroundColor = bgcolor;
    self.contentView.backgroundColor = bgcolor;
    self.selectedBackgroundView.backgroundColor = bgcolor;
}

@end
