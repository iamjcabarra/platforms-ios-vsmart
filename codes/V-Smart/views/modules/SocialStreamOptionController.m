//
//  SocialStreamOptionController.m
//  V-Smart
//
//  Created by Ryan Migallos on 5/18/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "SocialStreamOptionController.h"
#import "AppDelegate.h"

@interface SocialStreamOptionController ()
@property (strong, nonatomic) ResourceManager *rm;
@end

@implementation SocialStreamOptionController

static NSString *kOptionCellIdentifier = @"menu_option_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.rm = [AppDelegate resourceInstance];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    
    self.preferredContentSize = self.tableView.contentSize;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.optionList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kOptionCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    // Configure the cell...
    NSDictionary *data = (NSDictionary *)self.optionList[indexPath.row];
    NSString *title = [NSString stringWithFormat:@"%@", data[@"title"] ];
    
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    cell.textLabel.text = title;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *menu = self.optionList[indexPath.row];
    NSString *option = [NSString stringWithFormat:@"%@", menu[@"type"] ];
    
    if([_delegate respondsToSelector:@selector(selectedMenuOption:withData:point:)]){
        [_delegate selectedMenuOption:option withData:self.data point:self.point];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

@end
