//
//  PlayListDocumentViewer.m
//  V-Smart
//
//  Created by Ryan Migallos on 4/11/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "PlayListDocumentViewer.h"

@interface PlayListDocumentViewer ()
@property (strong, nonatomic) IBOutlet UIWebView *documentWebView;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;

@end

@implementation PlayListDocumentViewer

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.doneButton addTarget:self action:@selector(buttonDoneAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if (([self.mimeType isEqualToString:@"image/jpeg"]) || ([self.mimeType isEqualToString:@"image/png"])) {
        NSData *compressedData = [self compressImage:[UIImage imageWithData:self.contentData]];
        self.contentData = compressedData;
    }
    
    if ((self.contentData != nil) && (self.mimeType != nil)) {
    [self.documentWebView loadData:self.contentData MIMEType:self.mimeType textEncodingName:@"utf-8" baseURL:nil];
}

}

- (NSData *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return imageData;
}

- (void)buttonDoneAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    //    self.documentWebView = webView;
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    //    self.documentWebView = webView;
    
    if (error) {
        NSLog(@"document viewer error : %@", [error localizedDescription] );
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
