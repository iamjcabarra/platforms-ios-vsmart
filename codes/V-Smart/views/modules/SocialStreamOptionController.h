//
//  SocialStreamOptionController.h
//  V-Smart
//
//  Created by Ryan Migallos on 5/18/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SocialStreamOptionDelegate <NSObject>
@required
- (void)selectedMenuOption:(NSString *)option withData:(NSDictionary *)data point:(CGPoint)point;
@end

@interface SocialStreamOptionController : UITableViewController
@property (nonatomic, assign) CGPoint point;
@property (nonatomic, strong) NSDictionary *data;
@property (nonatomic, strong) NSArray *optionList;
@property (nonatomic, weak) id <SocialStreamOptionDelegate> delegate;

@end