//
//  QuestionImagePreview.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 04/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionImagePreview : UIViewController

@property(strong,nonatomic) NSDictionary *previewData;
@property(assign, nonatomic) NSInteger index;

@end
