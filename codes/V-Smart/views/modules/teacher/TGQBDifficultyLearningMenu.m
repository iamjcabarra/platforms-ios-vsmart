//
//  TGQBDifficultyLearningMenu.m
//  V-Smart
//
//  Created by Ryan Migallos on 11/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGQBDifficultyLearningMenu.h"
#import "TestGuruDataManager.h"
#import "TestGuruOptionMenu.h"
#import <QuartzCore/QuartzCore.h>

typedef NS_ENUM(NSInteger, TGOptionTypeStyle) {
    TGOptionTypeStyleValue1,
    TGOptionTypeStyleValue2,
    TGOptionTypeStyleValue3,
    TGOptionTypeStyleDefault,
};

@interface TGQBDifficultyLearningMenu() <OptionMenuDelegate, UIPopoverControllerDelegate>

// DIFFICULTY CONTROL
@property (strong, nonatomic) IBOutlet UIButton *difficultyButton;
@property (strong, nonatomic) IBOutlet UILabel *difficultyLabel;
@property (strong, nonatomic) IBOutlet UIView *difficultyView;
@property (strong, nonatomic) UIPopoverController *difficultyPopover;
@property (strong, nonatomic) NSDictionary *difficultyData;

// LEARNING CONTROL
@property (strong, nonatomic) IBOutlet UIButton *learningButton;
@property (strong, nonatomic) IBOutlet UILabel *learningLabel;
@property (strong, nonatomic) IBOutlet UIView *learningView;
@property (strong, nonatomic) UIPopoverController *learningPopover;
@property (strong, nonatomic) NSDictionary *learningData;

@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) NSManagedObject *mo;
@property (strong, nonatomic) NSString *question_id;

@end

@implementation TGQBDifficultyLearningMenu

- (void)awakeFromNib {
    // Initialization code
    [self.difficultyButton addTarget:self action:@selector(difficultyCustomAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.learningButton addTarget:self action:@selector(learningCustomAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.tm = [TestGuruDataManager sharedInstance];
    
    [self setupOptionMenuForDifficulty];
    [self setupOptionMenuForLearningSkill];
}

- (void)setObjectData:(NSManagedObject *)object {
    
    self.mo = object;
    self.question_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"id"] ];
}

- (void)setupOptionMenuForDifficulty
{
    [self.tm requestType:kEndPointTestGuruProficiencyLevel doneBlock:nil];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    TestGuruOptionMenu *om = (TestGuruOptionMenu *)[sb instantiateViewControllerWithIdentifier:@"question_option_menu"];
    om.entity = kProficiencyEntity;
    NSUInteger item_count = [self.tm fetchCountForEntity:kProficiencyEntity];
    CGFloat height = 44 * item_count;
    
    [self applyShadowToView:self.difficultyView];
    
    CGFloat width = self.difficultyView.bounds.size.width;
    om.preferredContentSize = CGSizeMake(width, height);
    om.delegate = self;
    self.difficultyPopover = [[UIPopoverController alloc] initWithContentViewController:om];
    self.difficultyPopover.popoverContentSize = CGSizeMake(width, height);
    self.difficultyPopover.delegate = self;
}

- (void)setupOptionMenuForLearningSkill
{
    [self.tm requestType:kEndPointTestGuruLearningSkill doneBlock:nil];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    TestGuruOptionMenu *om = (TestGuruOptionMenu *)[sb instantiateViewControllerWithIdentifier:@"question_option_menu"];
    om.entity = kSkillEntity;
    NSUInteger item_count = [self.tm fetchCountForEntity:kSkillEntity];
    CGFloat height = 44 * item_count;
    
    [self applyShadowToView:self.learningView];
    
    CGFloat width = self.learningView.bounds.size.width;
    om.preferredContentSize = CGSizeMake(width, height);
    om.delegate = self;
    self.learningPopover = [[UIPopoverController alloc] initWithContentViewController:om];
    self.learningPopover.popoverContentSize = CGSizeMake(width, height);
    self.learningPopover.delegate = self;
}

- (void)applyShadowToView:(UIView *)object {
    
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.5f;

    //    layer.borderColor = [[UIColor redColor] CGColor];
    //    layer.borderWidth = 0.5f;
}

- (void)difficultyCustomAction:(id)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    UIButton *b = (UIButton *)sender;
    [self.difficultyPopover presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
}

- (void)learningCustomAction:(id)sender {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    UIButton *b = (UIButton *)sender;
    [self.learningPopover presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)selectedOptionMenuForEntity:(NSString *)entity withObject:(NSManagedObject *)object {
    
    NSMutableDictionary *d = [NSMutableDictionary dictionary];
    NSArray *keys = object.entity.propertiesByName.allKeys;
    for (NSString *k in keys) {
        NSString *value = [NSString stringWithFormat:@"%@", [object valueForKey:k] ];
        [d setValue:value forKey:k];
    }
    
    if ([entity isEqualToString:kProficiencyEntity]) {
        [self setDifficultyValueForEntity:entity withData:d];
    }
    
    if ([entity isEqualToString:kSkillEntity]) {
        [self setLearningValueForEntity:entity withData:d];
    }
}

- (void)setDifficultyValueForEntity:(NSString *)entity withData:(NSDictionary *)data {
    
    self.difficultyData = [NSDictionary dictionaryWithDictionary:data];
    
    NSLog(@"%s : %@", __PRETTY_FUNCTION__, self.difficultyData);
    
    NSString *value = [NSString stringWithFormat:@"%@", data[@"value"] ];
    NSString *id_string = [NSString stringWithFormat:@"%@", data[@"id"] ];
    
    TGOptionTypeStyle optionStyle = [self typeForTitle:value];
    UIColor *selectedColor = [self colorForType:optionStyle];
    
    self.difficultyLabel.text = value;
    self.difficultyView.backgroundColor = selectedColor;
    
    if (self.difficultyPopover.isPopoverVisible) {
        
        /*
        proficiency_level_id
        proficiencyLevelName
        */
        
        NSDictionary *userData = @{@"proficiency_level_id": id_string,
                                   @"proficiencyLevelName": value};
        [self updateDetailsWithValue:userData];
        
        [self.difficultyPopover dismissPopoverAnimated:NO];
    }
}

- (void)setLearningValueForEntity:(NSString *)entity withData:(NSDictionary *)data {
    
    self.learningData = [NSDictionary dictionaryWithDictionary:data];
    
    NSLog(@"%s : %@", __PRETTY_FUNCTION__, self.difficultyData);
    
    NSString *value = [NSString stringWithFormat:@"%@", data[@"value"] ];
    NSString *id_string = [NSString stringWithFormat:@"%@", data[@"id"] ];
    
    TGOptionTypeStyle optionStyle = [self typeForTitle:value];
    UIColor *selectedColor = [self colorForType:optionStyle];
    
    self.learningLabel.text = value;
//    self.learningLabel.textColor = [UIColor darkGrayColor];
//    self.learningView.backgroundColor = selectedColor;
    self.learningLabel.textColor = [UIColor whiteColor];
    self.learningView.backgroundColor = UIColorFromHex(0x0080FF);
    
    if (self.learningPopover.isPopoverVisible) {
        
        /*
         learning_skills_id
         learningSkillsName
         */
        NSDictionary *userData = @{@"learning_skills_id": id_string,
                                   @"learningSkillsName": value};
        [self updateDetailsWithValue:userData];
        
        [self.learningPopover dismissPopoverAnimated:NO];
    }
}

- (UIColor *)colorForType:(TGOptionTypeStyle)option {
    
//    UIColor *selectedColor = UIColorFromHex(0x0080FF);
    UIColor *selectedColor = UIColorFromHex(0xFFFFFF);
    
    UIColor *easyColor = UIColorFromHex(0x68BF61);
    UIColor *moderateColor = UIColorFromHex(0xFFCD02);
    UIColor *difficultColor = UIColorFromHex(0xF8931F);
    
    if (option == TGOptionTypeStyleValue1) {
        selectedColor = easyColor;
    }
    
    if (option == TGOptionTypeStyleValue2) {
        selectedColor = moderateColor;
    }
    
    if (option == TGOptionTypeStyleValue3) {
        selectedColor = difficultColor;
    }
    
    return selectedColor;
}

- (TGOptionTypeStyle)typeForTitle:(NSString *)title_string {
    
    title_string = [title_string lowercaseString];
    
    TGOptionTypeStyle option_style = TGOptionTypeStyleDefault;
    
    if ([title_string isEqualToString:@"easy"]) {
        option_style = TGOptionTypeStyleValue1;
    }
    
    if ([title_string isEqualToString:@"moderate"]) {
        option_style = TGOptionTypeStyleValue2;
    }
    
    if ([title_string isEqualToString:@"difficult"]) {
        option_style = TGOptionTypeStyleValue3;
    }
    
    return option_style;
}

- (NSDictionary *)getDifficultyObject {
    return [NSDictionary dictionaryWithDictionary:self.difficultyData];
}

- (NSDictionary *)getLearningObject {
    return [NSDictionary dictionaryWithDictionary:self.learningData];
}

- (void)updateDetailsWithValue:(NSDictionary *)userData {
    
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:self.question_id];
    [self.tm updateQuestion:kQuestionEntity predicate:predicate details:userData];
}

@end
