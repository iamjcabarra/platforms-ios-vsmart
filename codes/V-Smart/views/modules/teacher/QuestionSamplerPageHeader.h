//
//  QuestionSamplerPageHeader.h
//  V-Smart
//
//  Created by Ryan Migallos on 03/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionSamplerPageHeader : UIView

- (void)displayObject:(NSManagedObject *)object withIndexRow:(NSInteger)indexRow withTotalRow:(NSInteger)totalRow;

@end
