//
//  TestQuestionsViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 26/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestQuestionsViewController.h"
#import "TestQuestionsCell.h"
#import "TestAssignQuestionsView.h"
#import "TestContentManager.h"
#import "TestGuruDataManager.h"
#import "HUD.h"

@interface TestQuestionsViewController () <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, TestAssignQuestionsViewDelegate>

@property (strong, nonatomic) TestContentManager *sharedTestContentManager;
@property (strong, nonatomic) TestGuruDataManager *tgdm;
@property (strong, nonatomic) TestAssignQuestionsView *questionsListView;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *addQuestionButton;
@property (strong, nonatomic) IBOutlet UIButton *deleteQuestionButton;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;

@property (strong, nonatomic) NSString *kTestQuestionCellIdentifier;
@property (strong, nonatomic) NSString *userid;

@property (strong, nonatomic) NSArray *assignedQuestions;
@property (strong, nonatomic) NSMutableArray *selectedQuestions;

@property (assign, nonatomic) int actionType;
@property (assign, nonatomic) BOOL isFirstCalled;

@end

@implementation TestQuestionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.userid = [NSString stringWithFormat:@"%d", account.user.id];
    
    // Use test content manager
    self.sharedTestContentManager = [TestContentManager sharedInstance];
    
    // Use test guru data manager
    self.tgdm = [TestGuruDataManager sharedInstance];
    self.managedObjectContext = self.tgdm.mainContext;
    
    // Set protocols for table view
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Allow selections to table view
    self.tableView.allowsSelection = YES;
    self.tableView.allowsMultipleSelection = YES;
    
    self.tableView.rowHeight = 50.f;
    
    // Set cell identifier
    self.kTestQuestionCellIdentifier = @"questionCellIdentifier";
    
    // Actions for buttons
    [self.addQuestionButton addTarget:self action:@selector(showQuestionsList:) forControlEvents:UIControlEventTouchUpInside];
    [self.deleteQuestionButton addTarget:self action:@selector(deleteQuestionAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.messageLabel.hidden = YES;
    self.deleteQuestionButton.enabled = NO;
    
    self.assignedQuestions = [NSArray array];
    self.selectedQuestions = [NSMutableArray array];
    
    self.isFirstCalled = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // List assigned questions
    [self listAllAssignedQuestions];
    
    // To avoid border cutting using grouped table view
    self.tableView.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - List All Assigned Questions

- (void)listAllAssignedQuestions {
    self.actionType = self.sharedTestContentManager.actionType;
    
    NSString *testid = self.sharedTestContentManager.testid;
    NSPredicate *predicate = nil;

    if (self.actionType == 2) {
        predicate = [self predicateForKeyPath:@"quiz_id" value:testid];
    }
    
    self.assignedQuestions = [self.tgdm getObjectsForEntity:kTestQuestionEntity predicate:predicate context:self.managedObjectContext];
    self.sharedTestContentManager.testAssignedQuestions = self.assignedQuestions;

    // For test assigned questions reminder
    self.messageLabel.hidden = YES;
    
    if (self.assignedQuestions == 0) {
        self.messageLabel.hidden = NO;
        self.messageLabel.text = NSLocalizedString(@"You cannot save this test without at least one question assigned to it.", nil);
    }
    
    [self.tableView reloadData];
    
    // For test changes made mapping
    [self setPreEntriesForTestAssignedQuestions];
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)setPreEntriesForTestAssignedQuestions {
    if (self.isFirstCalled) {
        if (self.assignedQuestions != nil) {
            NSMutableArray *qidList = [NSMutableArray array];
            
            for (NSManagedObject *mo in self.assignedQuestions) {
                NSString *qid = [NSString stringWithFormat:@"%@", [mo valueForKey:@"question_id"]];
                [qidList addObject:qid];
            }
            
            if (qidList != nil) {
                NSDictionary *userInfo = @{@"preAssignedQuestions":qidList};
                [self notifyTestForTestCreationPreEntries:userInfo];
            }
        }
    }
    
    self.isFirstCalled = NO;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.assignedQuestions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TestQuestionsCell *questionCell = [tableView dequeueReusableCellWithIdentifier:self.kTestQuestionCellIdentifier forIndexPath:indexPath];
    questionCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSManagedObject *mo = [self.assignedQuestions objectAtIndex:indexPath.row];
    NSString *questionName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"question_text"]];
    NSString *questionType = [NSString stringWithFormat:@"%@", [mo valueForKey:@"question_type_name"]];
    NSString *questionPoints = [NSString stringWithFormat:@"%.2f", [[mo valueForKey:@"points"] floatValue]];
    
    questionCell.questionNameLabel.text = questionName;
    questionCell.questionTypeLabel.text = questionType;
    questionCell.questionPointsLabel.text = questionPoints;
    
    if ([questionPoints floatValue] > 1.0) {
        questionCell.questionPointsTitleLabel.text = NSLocalizedString(@"Points", nil);
    }
    else {
        questionCell.questionPointsTitleLabel.text = NSLocalizedString(@"Point", nil);
    }
    
    BOOL isSelected = [self isQuestionSelected:mo];
    UIImage *buttonImage = [UIImage imageNamed:@"check_icn48.png"];
    
    if (isSelected) {
        buttonImage = [UIImage imageNamed:@"check_icn_active48.png"];
    }
    
    [questionCell.checkBoxButton setImage:buttonImage forState:UIControlStateNormal];
    
    return questionCell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TestQuestionsCell *questionCell = (TestQuestionsCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    NSManagedObject *mo = [self.assignedQuestions objectAtIndex:indexPath.row];
    [self.selectedQuestions addObject:mo];
    [self updateDeleteButtonState];
    
    UIImage *buttonImage = [UIImage imageNamed:@"check_icn_active48.png"];
    [questionCell.checkBoxButton setImage:buttonImage forState:UIControlStateNormal];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    TestQuestionsCell *questionCell = (TestQuestionsCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    NSManagedObject *mo = [self.assignedQuestions objectAtIndex:indexPath.row];
    [self.selectedQuestions removeObject:mo];
    [self updateDeleteButtonState];
    
    UIImage *buttonImage = [UIImage imageNamed:@"check_icn48.png"];
    [questionCell.checkBoxButton setImage:buttonImage forState:UIControlStateNormal];
}

- (void)updateDeleteButtonState {
    if (self.selectedQuestions.count > 0) {
        self.deleteQuestionButton.enabled = YES;
    }
    else {
        self.deleteQuestionButton.enabled = NO;
    }
}

- (BOOL)isQuestionSelected:(id)object {
    NSSet *set = [NSSet setWithArray:self.selectedQuestions];
    if ([set containsObject:object]) {
        return YES;
    }
    
    return NO;
}

#pragma mark - Add Question Action

- (void)showQuestionsList:(id)sender {
    __weak typeof(self) wo = self;
    [self.tgdm requestAssignQuestionListForUser:self.userid enableChoice:YES doneBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo performSegueWithIdentifier:@"showQuestionsList" sender:nil];
            });
        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showQuestionsList"]) {
        self.questionsListView = [segue destinationViewController];
        self.questionsListView.delegate = self;
        self.questionsListView.assignedQuestions = self.assignedQuestions;
    }
}

#pragma mark - Delete Question Action

- (void)deleteQuestionAction:(id)sender {
    NSString *avTitle = @"";
    NSString *message = NSLocalizedString(@"Are you sure you want to remove this question from this test?", nil);
    NSString *butPosR = NSLocalizedString(@"Yes", nil);
    NSString *butNegR = NSLocalizedString(@"No", nil);
    
    if (self.actionType == 1) {
        avTitle = NSLocalizedString(@"Create Test", nil);
    }
    
    if (self.actionType == 2) {
        avTitle = NSLocalizedString(@"Edit Test", nil);
    }
    
    if (self.selectedQuestions.count > 1) {
        message = NSLocalizedString(@"Are you sure you want to remove these questions from this test?", nil);
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:avTitle
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *posAction = [UIAlertAction actionWithTitle:butPosR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          
        NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Removing", nil)];
        [HUD showUIBlockingIndicatorWithText:indicatorString];
        
        BOOL isSuccessful = NO;
        
        for (NSManagedObject *mo in self.selectedQuestions) {
            isSuccessful = [self.tgdm removeQuestionFromTest:mo];
            
            if (!isSuccessful) {
                break;
            }
        }
        
        __weak typeof(self) wo = self;
       
        if (isSuccessful) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [wo showConfirmationMessageForDeleteAction:YES];
                [wo listAllAssignedQuestions];
                [wo updateDeleteButtonState];
                [wo notifyTestForActionPreparation];
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [wo showConfirmationMessageForDeleteAction:NO];
            });
        }
        
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *negAction = [UIAlertAction actionWithTitle:butNegR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                      }];
    [alert addAction:posAction];
    [alert addAction:negAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Confirmation Message: Delete Action

- (void)showConfirmationMessageForDeleteAction:(BOOL)status {
    NSString *avTitle = @"";
    NSString *butOkay = NSLocalizedString(@"Okay", nil);
    NSString *message = @"";
    
    if (self.actionType == 1) {
        avTitle = NSLocalizedString(@"Create Test", nil);
    }
    
    if (self.actionType == 2) {
        avTitle = NSLocalizedString(@"Edit Test", nil);
    }
    
    if (status) {
        message = NSLocalizedString(@"Question successfully removed from this test.", nil);
        
        if (self.selectedQuestions.count > 1) {
            message = NSLocalizedString(@"Questions successfully removed from this test.", nil);
        }
    
        [self.selectedQuestions removeAllObjects];
    }
    else {
        message = NSLocalizedString(@"Question was not successfully removed from this test.", nil);
        
        if (self.selectedQuestions.count > 1) {
            message = NSLocalizedString(@"Questions were not successfully removed from this test.", nil);
        }
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:avTitle
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *theAction = [UIAlertAction actionWithTitle:butOkay
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                      }];
    [alert addAction:theAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Test Assign Questions View Delegate

- (void)selectedQuestions:(NSArray *)questions {
    NSString *testid = self.sharedTestContentManager.testid;
    [self.tgdm insertQuestionToTest:questions testid:testid];
    [self listAllAssignedQuestions];
    [self notifyTestForActionPreparation];
}

#pragma mark - Test Notifications

- (void)notifyTestForTestCreationPreEntries:(NSDictionary *)d {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setPreEntriesForTestQuestion" object:self userInfo:d];
}

- (void)notifyTestForActionPreparation {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"prepareToSaveOrUpdateTest" object:nil];
}

@end
