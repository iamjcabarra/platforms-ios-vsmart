//
//  TGQBQuestionContentHeaderView.h
//  V-Smart
//
//  Created by Ryan Migallos on 22/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGQBQuestionContentHeaderView : UIView
@property (strong, nonatomic) IBOutlet UIButton *headerButtton;
@property (strong, nonatomic) IBOutlet UILabel *headerTitleLabel;
-  (instancetype)initWithFrame:(CGRect)rect;
@end
