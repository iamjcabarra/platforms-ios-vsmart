//
//  TGQBQuestionDetailViewController.h
//  V-Smart
//
//  Created by Ryan Migallos on 10/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGQBQuestionDetailDelegate <NSObject>
@optional
- (void)didFinishCreateOperation:(BOOL)state;
- (void)didFinishDeleteOperationWithObject:(NSManagedObject *)object;
@end

@interface TGQBQuestionDetailViewController : UIViewController
@property (weak, nonatomic) id <TGQBQuestionDetailDelegate> delegate;
@property (strong, nonatomic) NSString *competency_code;
@property (strong, nonatomic) NSManagedObject *mo;
@property (assign, nonatomic) BOOL editMode;

- (void)setQuestionObject:(NSManagedObject *)managedObject;

@end
