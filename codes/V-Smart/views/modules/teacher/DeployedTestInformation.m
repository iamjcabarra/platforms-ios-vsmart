//
//  DeployedTestInformation.m
//  V-Smart
//
//  Created by Ryan Migallos on 9/28/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "DeployedTestInformation.h"

@interface DeployedTestInformation ()

@property (strong, nonatomic) IBOutlet UILabel *quizTitle;
@property (strong, nonatomic) IBOutlet UILabel *quizDescription;
@property (strong, nonatomic) IBOutlet UILabel *questionCount;

@property (strong, nonatomic) IBOutlet UILabel *attemptDescription;
@property (strong, nonatomic) IBOutlet UILabel *timelimitDescription;
@property (strong, nonatomic) IBOutlet UILabel *passingrateDescription;

@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *startButton;

@end

@implementation DeployedTestInformation

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.startButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *attempts_text = [NSString stringWithFormat:@"%@", [self.quiz_mo valueForKey:@"attempts"] ];
    NSString *time_limit_text = [NSString stringWithFormat:@"%@", [self.quiz_mo valueForKey:@"time_limit"] ];
    NSString *passing_rate_text = [NSString stringWithFormat:@"%@", [self.quiz_mo valueForKey:@"passing_rate"] ];
    NSSet *quiz_set = [self.quiz_mo valueForKey:@"questions"];
    NSString *total_items_text = [NSString stringWithFormat:@"%lu", (unsigned long)quiz_set.count];
    
    NSString *attempt = @"";
    
    if ([attempts_text integerValue] > 1) {
        attempt = [NSString stringWithFormat:@"You can take this exam %@ more times", attempts_text ];
    }
    else {
        attempt = [NSString stringWithFormat:@"You can take this exam %@ more time", attempts_text ];
    }
    
//    NSString *attempt = [NSString stringWithFormat:@"You can take this exam %@ more times", attempts_text ];
    NSLog(@"attempt : %@", attempt);
    NSString *time_limit = [NSString stringWithFormat:@"You have %@ to complete the exam", time_limit_text ];
    NSLog(@"time_limit : %@",time_limit);
    NSString *passing_rate = [NSString stringWithFormat:@"The passing score for this exam is %@%%", passing_rate_text ];
    NSLog(@"passing_rate : %@", passing_rate);
    NSString *total_items = [NSString stringWithFormat:@"%@ items", total_items_text ];
    NSLog(@"total_items : %@", total_items);
    
//    __weak typeof(self) weakObject = self;
//    dispatch_async(dispatch_get_main_queue(), ^{
        self.quizTitle.text = [self.quiz_mo valueForKey:@"name"];
        self.quizDescription.text = [self.quiz_mo valueForKey:@"desc"];
        self.questionCount.text = total_items;
        self.attemptDescription.text = attempt;
        self.timelimitDescription.text = time_limit;
        self.passingrateDescription.text = passing_rate;
//    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buttonAction:(id)sender {
    
    UIButton *b = (UIButton *)sender;
    
    if (b == _startButton) {
        if ([_delegate respondsToSelector:@selector(buttonActionType:)]) {
            [_delegate buttonActionType:@"start"];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
    if (b == _cancelButton) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
