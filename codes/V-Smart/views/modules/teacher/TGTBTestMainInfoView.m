//
//  TGTBTestMainInfoView.m
//  V-Smart
//
//  Created by Julius Abarra on 07/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGTBTestMainInfoView.h"
#import "TestGuruDataManager.h"
#import <QuartzCore/QuartzCore.h>
#import "TGTBDatePickerView.h"
#import "TGTBTimePickerView.h"
#import "TGTBTermPickerView.h"
#import "TGTBResultTypePickerView.h"
#import "TGTBAssessmentTypePickerView.h"
#import "TBClassHelper.h"
#import "UITextView+Placeholder.h"

@interface TGTBTestMainInfoView () <UITextFieldDelegate, UITextViewDelegate, TGTBDatePickerViewDelegate, TGTBTimePickerView, TGTBTermPickerViewDelegate, TGTBAssessmentTypePickerViewDelegate>

// DATA MANAGER
@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) NSString *testid;

// CLASS HELPER
@property (strong, nonatomic) TBClassHelper *classHelper;

// PICKER VIEWS
@property (strong, nonatomic) TGTBDatePickerView *datePickerView;
@property (strong, nonatomic) TGTBTimePickerView *timePickerView;
@property (strong, nonatomic) TGTBTermPickerView *termPickerView;
@property (strong, nonatomic) TGTBResultTypePickerView *resultTypePickerView;
@property (strong, nonatomic) TGTBAssessmentTypePickerView *assessmentTypePickerView;

// NOTIFICATON
@property (strong, nonatomic) NSNotificationCenter *notif;

// TITLE
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITextField *titleField;

// INSTRUCTION
@property (strong, nonatomic) IBOutlet UILabel *generalInstructionLabel;
@property (strong, nonatomic) IBOutlet UITextView *generalInstructionField;

// GRADING TERM
@property (strong, nonatomic) IBOutlet UILabel *testTermLabel;
@property (strong, nonatomic) IBOutlet UITextField *testTermField;
@property (strong, nonatomic) IBOutlet UIButton *testTermButton;

// ASSESSMENT TYPE
@property (strong, nonatomic) IBOutlet UILabel *testAssessmentTypeLabel;
@property (strong, nonatomic) IBOutlet UITextField *testAssessmentTypeField;
@property (strong, nonatomic) IBOutlet UIButton *testAssessmentTypeButton;

// ATTEMPTS
@property (strong, nonatomic) IBOutlet UILabel *attemptsLabel;
@property (strong, nonatomic) IBOutlet UITextField *attemptsField;

// PASSING SCORE
@property (strong, nonatomic) IBOutlet UILabel *passingScoreLabel;
@property (strong, nonatomic) IBOutlet UITextField *passingScoreField;

// TIME LIMIT
@property (strong, nonatomic) IBOutlet UILabel *timeLimitLabel;
@property (strong, nonatomic) IBOutlet UIButton *timeLimitButton;
@property (strong, nonatomic) IBOutlet UITextField *timeLimitField;

// DATE OPEN
@property (strong, nonatomic) IBOutlet UILabel *dateScheduleLabel;
@property (strong, nonatomic) IBOutlet UIButton *openDateButton;
@property (strong, nonatomic) IBOutlet UITextField *openDateField;

// DATE CLOSE
@property (strong, nonatomic) IBOutlet UIButton *closeDateButton;
@property (strong, nonatomic) IBOutlet UITextField *closeDateField;

// EXPIRATION
@property (strong, nonatomic) IBOutlet UILabel *noExpiryLabel;
@property (strong, nonatomic) IBOutlet UIButton *noExpiryButton;

// GRADED
@property (strong, nonatomic) IBOutlet UILabel *testGradedLabel;
@property (strong, nonatomic) IBOutlet UISwitch *gradedSwitch;

// IMMEDIATES
@property (assign, nonatomic) NSInteger testScheduleButtonTag;
@property (assign, nonatomic) CGFloat testTotalScore;


//BACKWARDS COMPATIBILITY SUPPORT
@property (assign, nonatomic) BOOL isVersion25;


@end

@implementation TGTBTestMainInfoView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Data Manager
    self.tm = [TestGuruDataManager sharedInstance];
    self.testid = [self stringValue:self.testObject key:@"id"];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Notification
    self.notif = [NSNotificationCenter defaultCenter];
    
    [self customizeViews];
    [self setupLocalization];
    [self displayValues];
    [self setUpTimeAndDatePickerView];
    
    // Academic Term
    [self.testTermButton addTarget:self
                            action:@selector(showPopOverAcademicTermPicker:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    // Assessment Type
    [self.testAssessmentTypeButton addTarget:self
                                      action:@selector(showPopOverAssessmentTypePicker:)
                            forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat version = [[Utils getServerInstanceVersion] floatValue];
    
    NSLog(@"TEST GURU SERVER VERSION : %@", @(version) );
    
    self.isVersion25 = (version >= VSMART_SERVER_MAX_VER);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

//    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    CGFloat height = 48;
    
    if (self.isVersion25 == NO) {
        if (indexPath.row == 1) {
            height = 0;
        }
    }
    
    if (indexPath.row == 0) {
        height = 84;
    }

    if (indexPath.row == 2) {
        height = 184;
    }
    
    
    return height;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.testTotalScore = [self getTestTotalScore];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)getTestTotalScore {
    CGFloat testMaximumPassingScore = 0.0f;
    
    NSArray *questionList = [self.tm getObjectsForEntity:kQuestionEntity
                                               predicate:nil
                                                 context:self.tm.mainContext];
    
    for (NSManagedObject *mo in questionList) {
        CGFloat points = [[mo valueForKey:@"points"] floatValue];
        testMaximumPassingScore += points;
    }
    
    return testMaximumPassingScore;
}

- (void)customizeViews {
    self.titleField.delegate = self;
    self.generalInstructionField.delegate = self;
    self.attemptsField.delegate = self;
    self.passingScoreField.delegate = self;
 
    [self addFunctionForSwitch:self.gradedSwitch];
    
    self.timeLimitField.userInteractionEnabled = NO;
    self.openDateField.userInteractionEnabled = NO;
    self.closeDateField.userInteractionEnabled = NO;
    
    [self.noExpiryButton addTarget:self
                            action:@selector(noExpiryButtonAction:)
                  forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupLocalization {
    NSString *title_string = NSLocalizedString(@"Title", nil);
    NSString *title_placeholder = NSLocalizedString(@"Example: Math 3 Unit Test", nil);
    NSString *general_string = NSLocalizedString(@"General Instructions", nil);
    NSString *general_placeholder = NSLocalizedString(@"Type your instructions here", nil);
    NSString *term_string = NSLocalizedString(@"Term", nil);
    NSString *assessment_type_string = NSLocalizedString(@"Assessment Type", nil);
    NSString *attempts_string = NSLocalizedString(@"Attempt", nil);
    NSString *score_string = NSLocalizedString(@"Passing Score", nil);
    NSString *time_limit = NSLocalizedString(@"Time Limit", nil);
    NSString *date_schedule = NSLocalizedString(@"Date Validity", nil);
    NSString *test_graded = NSLocalizedString(@"Test is Graded", nil);
    NSString *no_expiration_date = NSLocalizedString(@"No Expiration Date", nil);
    
    self.titleLabel.text = [title_string uppercaseString];
    self.titleField.placeholder = title_placeholder;
    self.generalInstructionLabel.text = [general_string uppercaseString];
    self.generalInstructionField.placeholder = [NSString stringWithFormat:@"%@...", general_placeholder];
    self.testTermLabel.text = [term_string uppercaseString];
    self.testAssessmentTypeLabel.text = [assessment_type_string uppercaseString];
    self.attemptsLabel.text = [attempts_string uppercaseString];
    self.passingScoreLabel.text = [score_string uppercaseString];
    self.timeLimitLabel.text = [time_limit uppercaseString];
    self.dateScheduleLabel.text = [date_schedule uppercaseString];
    self.testGradedLabel.text = [test_graded uppercaseString];
    self.noExpiryLabel.text = no_expiration_date;
}

- (void)displayValues {
    if (self.testObject != nil) {
        
        NSString *name = [self stringValue:self.testObject key:@"name"];
        NSString *academic_term = [self stringValue:self.testObject key:@"academic_term_name"];
        NSString *assessment_type = [self stringValue:self.testObject key:@"assessment_type_name"];
        NSString *general_instruction = [self stringValue:self.testObject key:@"general_instruction"];
        NSString *attempts = [self stringValue:self.testObject key:@"attempts"];
        NSString *passing_score = [self stringValue:self.testObject key:@"passing_score"];
        NSString *time_limit = [self stringValue:self.testObject key:@"time_limit"];
        NSString *date_open = [self stringValue:self.testObject key:@"date_open"];
        NSString *date_close = [self stringValue:self.testObject key:@"date_close"];
        
        BOOL is_graded = [self switchValue:self.testObject key:@"is_graded"];
        
        date_open = [self.classHelper transformDateString:date_open
                                               fromFormat:kServerDateFormat
                                                 toFormat:kTGTBUIDateFormatVeryLongStyle];
        
        date_close = [self.classHelper transformDateString:date_close
                                                fromFormat:kServerDateFormat
                                                  toFormat:kTGTBUIDateFormatVeryLongStyle];
        self.titleField.text = name;
        self.testTermField.text = academic_term;
        self.testAssessmentTypeField.text = assessment_type;
        self.generalInstructionField.text = general_instruction;
        self.attemptsField.text = attempts;
        self.passingScoreField.text = passing_score;
        self.timeLimitField.text = time_limit;
        self.openDateField.text = date_open;
        self.closeDateField.text = date_close;
        self.gradedSwitch.on = is_graded;
        
        BOOL no_expiry = [self switchValue:self.testObject key:@"no_expiry"];
        self.noExpiryButton.selected = (no_expiry) ? NO : YES;
        [self noExpiryButtonAction:nil];
        
        [self updateAttemptLabel:[attempts floatValue]];
    }
}

- (BOOL)switchValue:(id)object key:(NSString *)key {
    BOOL state = NO;
    
    if (object != nil) {
        NSString *value = [self stringValue:object key:key];
        state = [value isEqualToString:@"1"];
        return state;
    }
    
    return state;
}

- (void)updateAttemptLabel:(CGFloat)attempt {
    NSString *text = [self.classHelper localizeString:@"Attempt"];
    
    if (attempt > 1) {
        text = [self.classHelper localizeString:@"Attempts"];
    }
    
    self.attemptsLabel.text = [text uppercaseString];
}

- (void)noExpiryButtonAction:(id)sender {
    [self.view endEditing:YES];
    BOOL isChecked = (self.noExpiryButton.selected) ? NO : YES;
    
    if (isChecked) {
        self.noExpiryButton.selected = YES;
        self.closeDateField.enabled = NO;
        self.closeDateButton.enabled = NO;
    }
    else {
        self.noExpiryButton.selected = NO;
        self.closeDateField.enabled = YES;
        self.closeDateButton.enabled = YES;
    }

    NSNumber *state = [NSNumber numberWithBool:self.noExpiryButton.selected];
    NSString *flag = [NSString stringWithFormat:@"%@", state];
    NSDictionary *userDataD = @{@"no_expiry":flag};
    [self updateDetailsWithValue:userDataD];
    [self autoCorrectTestSchedule];
}

- (NSString *)stringValue:(NSManagedObject *)mo key:(NSString *)key{
    return [self.tm stringValue:[mo valueForKey:key]];
}

- (void)applyShadowToView:(UIView *)object {
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.9f;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (self.testObject != nil) {
        if (textField == self.titleField) {
            if ([updatedText length] > 50) {
                return NO;
            }
        }
        
        if (textField == self.attemptsField) {
            NSCharacterSet *numberSet = [NSCharacterSet decimalDigitCharacterSet];
            if ([string rangeOfCharacterFromSet:[numberSet invertedSet]].location != NSNotFound) {
                return NO;
            }
            
            // JCA-02-17-2017
            // VIT#2562
            // Should not allow decimal point
            if ([string containsString:@"."]) {
                return NO;
            }
            
            [self updateAttemptLabel:[updatedText floatValue]];
        }
        
        if (textField == self.passingScoreField) {
            NSString *expression = @"^([0-9]*)(\\.([0-9]+)?)?$";
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                                   options:NSRegularExpressionCaseInsensitive
                                                                                     error:nil];
            NSUInteger noOfMatches = [regex numberOfMatchesInString:updatedText
                                                            options:0
                                                              range:NSMakeRange(0, [updatedText length])];
            if (noOfMatches == 0){
                return NO;
            }
            
            updatedText = [self.tm formatStringNumber:updatedText];
        }
        
        if ([string length] == 0) {
            NSString *key = [self keyForTextField:textField];
            NSDictionary *userData = @{key:updatedText};
            [self updateDetailsWithValue:userData];
            
            return YES;
        }
        
        NSString *key = [self keyForTextField:textField];
        NSDictionary *userData = @{key:updatedText};
        [self updateDetailsWithValue:userData];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.attemptsField || textField == self.passingScoreField) {
        textField.text = [self.tm formatStringNumber:textField.text];
    }
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    NSString *updatedText = [NSString stringWithFormat:@"%@", textView.text];
    
    if (self.testObject != nil) {
        NSString *key = [self keyForTextView:textView];
        NSDictionary *userData = @{key:updatedText};
        [self updateDetailsWithValue:userData];
    }
}

- (NSString *)keyForTextField:(UITextField *)textField {
    NSString *default_key = @"name";
    
    //TITLE FIELD
    if (textField == self.titleField) {
        default_key = @"name";
        return default_key;
    }
    
    //ATTEMPTS FIELD
    if (textField == self.attemptsField ) {
        default_key = @"attempts";
        return default_key;
    }
    
    // PASSING SCORE FIELD
    if (textField == self.passingScoreField ) {
        default_key = @"passing_score";
        return default_key;
    }
    
    return default_key;
}

- (NSString *)keyForTextView:(UITextView *)textView {
    NSString *default_key = @"general_instruction";
    
    if (textView == self.generalInstructionField) {
        default_key = @"general_instruction";
        return default_key;
    }
    
    return default_key;
}

- (NSString *)keyForSwitchView:(UISwitch *)switchView {
    NSString *default_key = @"is_graded";
    
    if (switchView == self.gradedSwitch) {
        default_key = @"is_graded";
        return default_key;
    }
    
    return default_key;
}

- (void)addFunctionForSwitch:(UISwitch *)view {
    [view addTarget:self
             action:@selector(triggerActionForSwitch:)
   forControlEvents:UIControlEventValueChanged];
}

- (void)triggerActionForSwitch:(UISwitch *)switchView {
    [self.view endEditing:YES];
    NSNumber *state = [NSNumber numberWithBool:switchView.on];
    NSString *flag = [NSString stringWithFormat:@"%@", state];
    NSString *key = [self keyForSwitchView:switchView];
    NSDictionary *userData = @{key:flag};
    [self updateDetailsWithValue:userData];
}

- (void)updateDetailsWithValue:(NSDictionary *)userData {
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:self.testid];
    [self.tm updateTest:kTestInformationEntity predicate:predicate details:userData];
    [self.notif postNotificationName:kNotificationTestCreateVerification object:nil];
}

#pragma mark - Time and Date Picker View

- (void)setUpTimeAndDatePickerView {
    self.openDateButton.tag = 100;
    self.closeDateButton.tag = 200;
    
    [self.openDateButton addTarget:self
                            action:@selector(showPopOverDatePicker:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    [self.closeDateButton addTarget:self
                             action:@selector(showPopOverDatePicker:)
                   forControlEvents:UIControlEventTouchUpInside];
    
    [self.timeLimitButton addTarget:self
                             action:@selector(showPopOverTimePicker:)
                   forControlEvents:UIControlEventTouchUpInside];
}

- (void)showPopOverTimePicker:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    // Create Time Picker Controller
    self.timePickerView = [[TGTBTimePickerView alloc] initWithNibName:@"TGTBTimePickerView" bundle:nil];
    self.timePickerView.delegate = self;
    
    // Present As Modal
    self.timePickerView.modalPresentationStyle = UIModalPresentationPopover;
    self.timePickerView.preferredContentSize = CGSizeMake(320.0f, 260.0f);
    self.timePickerView.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
    self.timePickerView.popoverPresentationController.sourceView = button;
    self.timePickerView.popoverPresentationController.sourceRect = button.bounds;
    [self presentViewController:self.timePickerView animated:true completion:nil];
    
    // End Editing
    [self.view endEditing:YES];
}

- (void)showPopOverDatePicker:(id)sender {
    UIButton *button = (UIButton *)sender;
    self.testScheduleButtonTag = button.tag;
    
    // Create Date Picker Controller
    self.datePickerView = [[TGTBDatePickerView alloc] initWithNibName:@"TGTBDatePickerView" bundle:nil];
    self.datePickerView.dateType = self.testScheduleButtonTag;
    self.datePickerView.delegate = self;
    
    // Present As Modal
    self.datePickerView.modalPresentationStyle = UIModalPresentationPopover;
    self.datePickerView.preferredContentSize = CGSizeMake(315.0f, 263.0f);
    self.datePickerView.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
    self.datePickerView.popoverPresentationController.sourceView = button;
    self.datePickerView.popoverPresentationController.sourceRect = button.bounds;
    
    NSDate *datePickerDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = kTGTBUIDateFormatVeryLongStyle;
    
    if (sender == self.openDateButton) {
        datePickerDate = [formatter dateFromString:self.openDateField.text];
    } else if (sender == self.closeDateButton) {
        datePickerDate = [formatter dateFromString:self.closeDateField.text];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.datePickerView.datePicker setDate:datePickerDate];
    });
    
    [self presentViewController:self.datePickerView animated:true completion:nil];
    
    // End Editing
    [self.view endEditing:YES];
}

#pragma mark - Time and Date Picker Delegate

- (void)selectedTime:(NSString *)timeString {
    self.timeLimitField.text = timeString;
    [self autoCorrectTestSchedule];
}

- (void)selectedDate:(NSString *)dateString {
    if (self.testScheduleButtonTag == 100) {
        self.openDateField.text = dateString;
    }
    
    if (self.testScheduleButtonTag == 200) {
        self.closeDateField.text = dateString;
    }
    
    [self autoCorrectTestSchedule];
}

#pragma mark - Time and Date Validation

- (void)autoCorrectTestSchedule {
    BOOL noExpiry = self.noExpiryButton.selected;
    
    if (!noExpiry) {
      // Selected Close Date
      NSString *closeDateString = self.closeDateField.text;
      
      // Time and Date in Seconds
      double timeLimitInSeconds = [self.classHelper convertTimeStringToSeconds:self.timeLimitField.text];
      double dateDifferenceInSeconds = [self.classHelper computeTimeDifferenceInSecondsFromDate:self.openDateField.text
                                                                                         toDate:self.closeDateField.text
                                                                                         fromFormat:kTGTBUIDateFormatVeryLongStyle
                                                                                       toFormat:kServerDateFormat];
          
      // Open Date < Close Date or Dates Difference < Time Limit
      if (dateDifferenceInSeconds < 0 || dateDifferenceInSeconds < timeLimitInSeconds) {
              NSDate *openDate = [self.classHelper convertStringToDate:self.openDateField.text format:kTGTBUIDateFormatVeryLongStyle];
          openDate = [openDate dateByAddingTimeInterval:(timeLimitInSeconds)];
              closeDateString = [self.classHelper convertDateToString:openDate format:kTGTBUIDateFormatVeryLongStyle];
      }
      
      // Close Date New Value
      self.closeDateField.text = closeDateString;
    }

    // Time Limit
    NSDictionary *userDataA = @{@"time_limit":self.timeLimitField.text};
    [self updateDetailsWithValue:userDataA];
    
    // Date Open
    NSString *dateOpenString = [self.classHelper transformDateString:self.openDateField.text
                                                          fromFormat:kTGTBUIDateFormatVeryLongStyle
                                                            toFormat:kServerDateFormat];
    
    NSDictionary *userDataB = @{@"date_open":dateOpenString};
    [self updateDetailsWithValue:userDataB];
    
    // Date Close
    NSString *dateCloseString = [self.classHelper transformDateString:self.closeDateField.text
                                                           fromFormat:kTGTBUIDateFormatVeryLongStyle
                                                             toFormat:kServerDateFormat];
    
    // Temporary
    if (noExpiry) {
        dateCloseString = @"0";
    }
    
    NSDictionary *userDataC = @{@"date_close":dateCloseString};
    [self updateDetailsWithValue:userDataC];
}

#pragma mark - Other Picker Views

- (void)showPopOverAcademicTermPicker:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    self.termPickerView = [[TGTBTermPickerView alloc] initWithNibName:@"TGTBTermPickerView" bundle:nil];
    self.termPickerView.delegate = self;
    
    // Present As Modal
    self.termPickerView.modalPresentationStyle = UIModalPresentationPopover;
    self.termPickerView.preferredContentSize = CGSizeMake(250.0f, 250.0f);
    self.termPickerView.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    self.termPickerView.popoverPresentationController.sourceView = button;
    self.termPickerView.popoverPresentationController.sourceRect = button.bounds;
    [self presentViewController:self.termPickerView animated:true completion:nil];
    
    // End Editing
    [self.view endEditing:YES];
}

- (void)showPopOverAssessmentTypePicker:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    self.assessmentTypePickerView = [[TGTBAssessmentTypePickerView alloc] initWithNibName:@"TGTBAssessmentTypePickerView" bundle:nil];
    self.assessmentTypePickerView.delegate = self;
    
    // Present As Modal
    self.assessmentTypePickerView.modalPresentationStyle = UIModalPresentationPopover;
    self.assessmentTypePickerView.preferredContentSize = CGSizeMake(250.0f, 250.0f);
    self.assessmentTypePickerView.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    self.assessmentTypePickerView.popoverPresentationController.sourceView = button;
    self.assessmentTypePickerView.popoverPresentationController.sourceRect = button.bounds;
    [self presentViewController:self.assessmentTypePickerView animated:true completion:nil];
    
    // End Editing
    [self.view endEditing:YES];
}

#pragma mark - Other Picker View Delegates

- (void)selectedTerm:(NSDictionary *)termObject {
    NSString *objID = [self.tm stringValue:termObject[@"id"]];
    NSString *name = [self.tm stringValue:termObject[@"name"]];
    self.testTermField.text = name;
    
    NSDictionary *data = @{@"academic_term_id": objID, @"academic_term_name": name};
    [self updateDetailsWithValue:data];
}

- (void)selectedAssessmentType:(NSDictionary *)assessmentTypeObject {
    NSString *objID = [self.tm stringValue:assessmentTypeObject[@"id"]];
    NSString *name = [self.tm stringValue:assessmentTypeObject[@"name"]];
    self.testAssessmentTypeField.text = name;
    
    NSDictionary *data = @{@"assessment_type_id": objID, @"assessment_type_name": name};
    [self updateDetailsWithValue:data];
}

@end
