//
//  TestGuruQuestionItemCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 28/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruQuestionItemCell.h"
#import <QuartzCore/QuartzCore.h>
#import <WebKit/WebKit.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import "Masonry.h"
#import "UIImageView+WebCache.h"

@interface TestGuruQuestionItemCell() <UIWebViewDelegate>//<WKScriptMessageHandler, WKUIDelegate, WKNavigationDelegate>

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIView *difficultyView;
@property (strong, nonatomic) IBOutlet UIView *publicView;
@property (strong, nonatomic) IBOutlet UIImageView *typeImage;
//@property (strong, nonatomic) IBOutlet UIView *webView;
//@property (strong, nonatomic) WKWebView *webObject;

@end

@implementation TestGuruQuestionItemCell

- (void)loadWebViewWithContents:(NSString *)string {
    
//    string = @"sum_(i=1)^n i^3=((n(n+1))/2)^2";
    
//    NSLog(@"------------------ WEB VIEW -------------------------");
    
    // Create WKWebViewConfiguration instance
//    WKWebViewConfiguration *configuration = [self loadConfiguration];
//    
//    // Initialize the WKWebView with the current frame and the configuration
//    if (self.webObject == nil) {
//        self.webObject = [[WKWebView alloc] initWithFrame:self.webView.frame configuration:configuration];
//    }
//    self.webObject.scrollView.scrollEnabled = NO;
//    [self.webView addSubview:self.webObject];
//    
//    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
//    [self.webObject mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(self.webView).with.insets(padding);
//    }];
    
//    NSString *htmlStartContent = @"<!DOCTYPE html><html><head><meta charset='UTF-8'></head><body><p style='color: #cc6699; font-size: 25px; font-family: verdana, arial, sans-serif'>";
//    NSString *htmlStartContent = @"<!DOCTYPE html><html><head><script src='https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=AM_HTMLorMML'><script><meta name='viewport' content='initial-scale=1.5'/><style> p{ font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: 500; font-size: 90%; }</style><meta charset='UTF-8'></head><body><p>";
//    NSString *htmlStartContent = @"<!DOCTYPE html><html><head><meta name='viewport' content='initial-scale=1.5'/><style> p{ font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: 500; font-size: 90%; }</style><meta charset='UTF-8'></head><body><p>";
//    NSString *htmlEndContent = @"</p></body></html>";
//    NSString *html_string = [NSString stringWithFormat:@"%@%@%@", htmlStartContent, string ,htmlEndContent];
//    [self.webObject loadHTMLString:html_string baseURL:nil];
    
    NSString *htmlStartContent = @"<!DOCTYPE html><html><head><meta name='viewport' content='initial-scale=1.5'/><style> p{ font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: 500; font-size: 90%; }</style><meta charset='UTF-8'></head><body><script type='text/javascript'>window.onload = function() { window.location.href = 'ready://' + document.body.offsetHeight;}</script><p>";
    NSString *htmlEndContent = @"</p></body></html>";
    NSString *html_string = [NSString stringWithFormat:@"%@%@%@", htmlStartContent, string ,htmlEndContent];
    //    myWebView.loadHTMLString(myHTML, baseURL:nil)
    [self.webView loadHTMLString:html_string baseURL:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [webView stopLoading];
    webView = nil;
}

//- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
//    NSLog(@"%s", __PRETTY_FUNCTION__);
//    [webView stopLoading];
//    webView = nil;
//}
//
//- (WKWebViewConfiguration *)loadConfiguration {
//    
//    // Create WKWebViewConfiguration instance
//    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
//    
//    // Setup WKUserContentController instance for injecting user script
//    WKUserContentController *controller = [[WKUserContentController alloc] init];
//    
//    [self setScript:@"MathJax.js" inController:controller injectionAtStart:YES];
//    
//    // Configure the WKWebViewConfiguration instance with the WKUserContentController
//    configuration.userContentController = controller;
//    
//    return configuration;
//}
//
//- (void)setScript:(NSString *)script inController:(WKUserContentController *)controller injectionAtStart:(BOOL)start {
//    
//    WKUserScriptInjectionTime time = WKUserScriptInjectionTimeAtDocumentEnd;
//    if (start) {
//        time = WKUserScriptInjectionTimeAtDocumentStart;
//    }
//    WKUserScript *myScript = [self userScript:script injectionTime:time];
//    [controller addUserScript:myScript];
//}
//
//
//- (void)setCutomScript:(NSString *)customScript inController:(WKUserContentController *)controller injectionAtStart:(BOOL)start {
//    
//    WKUserScriptInjectionTime time = WKUserScriptInjectionTimeAtDocumentEnd;
//    if (start) {
//        time = WKUserScriptInjectionTimeAtDocumentStart;
//    }
//    
//    WKUserScript *userScript = [[WKUserScript alloc] initWithSource:customScript injectionTime:time forMainFrameOnly:NO];
//    [controller addUserScript:userScript];
//}
//
//- (WKUserScript *)userScript:(NSString *)file injectionTime:(WKUserScriptInjectionTime)time {
//    WKUserScript *userScript = [[WKUserScript alloc] initWithSource:[self resourceFile:file]
//                                                      injectionTime:time
//                                                   forMainFrameOnly:NO];
//    return userScript;
//}
//
//- (NSString *)resourceFile:(NSString *)filename {
//    
//    NSString *name = [filename stringByDeletingPathExtension];
//    NSString *extension = [filename pathExtension];
//    NSBundle *bundle = [NSBundle mainBundle];
//    NSURL *path = [bundle URLForResource:name withExtension:extension];
//    NSString *string = [NSString stringWithContentsOfURL:path encoding:NSUTF8StringEncoding error:nil];
//    
//    return string;
//}
//
//- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
//
//}

- (void)awakeFromNib {
    
    self.webView.delegate = self;
//    self.webView
//    // Initialization code
//    
//    WKWebViewConfiguration *configuration = [self loadConfiguration];
//
//    // Initialize the WKWebView with the current frame and the configuration
//    if (self.webObject == nil) {
//        self.webObject = [[WKWebView alloc] initWithFrame:self.webView.frame configuration:configuration];
//    }
//    self.webObject.scrollView.scrollEnabled = YES;
//    [self.webView addSubview:self.webObject];
//    self.webObject.navigationDelegate = self;
//
//    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
//    [self.webObject mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(self.webView).with.insets(padding);
//    }];
//    
////    [self applyShadowToView:self.contentView];
}

- (void)applyShadowToView:(UIView *)object {
    
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.9f;
    
    //    layer.borderColor = [[UIColor redColor] CGColor];
    //    layer.borderWidth = 0.5f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)setQuestionTypeID:(NSString *)question_type_id {
//    
//    self.typeImage.image = nil;
//    
//    NSString *image_string = @"icon_true_or_false_150px";
//    
//    //ESSAY
//    if ([question_type_id isEqualToString:@"6"]) {
//        image_string = @"icon_essay_150px";
//    }
//    
//    //MULTIPLE CHOICE
//    if ([question_type_id isEqualToString:@"3"]) {
//        image_string = @"icon_multiple_choice_150px";
//    }
//
//    //SHORT ANSWER
//    if ([question_type_id isEqualToString:@"9"]) {
//        image_string = @"icon_short_answer_150px";
//    }
//    
//    //TRUE OR FALSE
//    if ([question_type_id isEqualToString:@"1"]) {
//        image_string = @"icon_true_or_false_150px";
//    }
//    
//    self.typeImage.image = [UIImage imageNamed:image_string];
//}

- (void)setQuestionTypeIcon:(NSString *)question_type_icon {
    [self.typeImage sd_setImageWithURL:[NSURL URLWithString:question_type_icon] ];
}

- (void)setDifficultyLevelID:(NSString *)difficulty_level_id {
    
    UIColor *easyColor = UIColorFromHex(0x68BF61);
    UIColor *moderateColor = UIColorFromHex(0xFFCD02);  
    UIColor *difficultColor = UIColorFromHex(0xF8931F);
    
    UIColor *color = [UIColor whiteColor];
    
    //EASY
    if ([difficulty_level_id isEqualToString:@"1"]) {
        color = easyColor;
    }
    
    //MODERATE
    if ([difficulty_level_id isEqualToString:@"2"]) {
        color = moderateColor;
    }

    //DIFFICULT
    if ([difficulty_level_id isEqualToString:@"3"]) {
        color = difficultColor;
    }
    
    self.questionDifficultyLabel.backgroundColor = color;
    self.difficultyView.backgroundColor = color;
    self.containerView.backgroundColor = color;
}

- (void)setPublic:(NSString *)is_public {
    UIColor *color = UIColorFromHex(0xe1f5fe);
    if ([is_public isEqualToString:@"1"]) {
        color = UIColorFromHex(0xf6fdf6);
    }
    
    self.publicView.backgroundColor = color;
}

/*
 QT --> ID : 1
 2015-10-28 16:58:51.591 V-Smart[10285:316405] QT --> VALUE : Easy
 2015-10-28 16:58:51.591 V-Smart[10285:316405] QT --> ID : 2
 2015-10-28 16:58:51.592 V-Smart[10285:316405] QT --> VALUE : Moderate
 2015-10-28 16:58:51.592 V-Smart[10285:316405] QT --> ID : 3
 2015-10-28 16:58:51.592 V-Smart[10285:316405] QT --> VALUE : Difficult
 */

@end
