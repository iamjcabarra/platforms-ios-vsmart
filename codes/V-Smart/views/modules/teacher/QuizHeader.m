//
//  QuizHeader.m
//  V-Smart
//
//  Created by Ryan Migallos on 9/9/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "QuizHeader.h"
#import "TestGuruDataManager.h"
#import "TestGuruOptionMenu.h"
#import "TestGuruDateMenu.h"

@interface QuizHeader() <UITextFieldDelegate, UIPopoverControllerDelegate, OptionMenuDelegate, DateOptionMenuDelegate>

// LABELS
@property (strong, nonatomic) IBOutlet UILabel *labelName;
@property (strong, nonatomic) IBOutlet UILabel *labelDescription;
@property (strong, nonatomic) IBOutlet UILabel *labelGrade;
@property (strong, nonatomic) IBOutlet UILabel *labelResult;
@property (strong, nonatomic) IBOutlet UILabel *labelCategory;
@property (strong, nonatomic) IBOutlet UILabel *labelType;
@property (strong, nonatomic) IBOutlet UILabel *labelDifficulty;
@property (strong, nonatomic) IBOutlet UILabel *labelSkill;
@property (strong, nonatomic) IBOutlet UILabel *labelAttempts;
@property (strong, nonatomic) IBOutlet UILabel *labelPassingRate;
@property (strong, nonatomic) IBOutlet UILabel *labelTimeLimit;
@property (strong, nonatomic) IBOutlet UILabel *labelOpenCloseDate;
@property (strong, nonatomic) IBOutlet UILabel *labelShuffleQuestion;
@property (strong, nonatomic) IBOutlet UILabel *labelDisplayFeedback;
@property (strong, nonatomic) IBOutlet UILabel *labelForceComplete;
@property (strong, nonatomic) IBOutlet UILabel *labelShuffleAnswer;
@property (strong, nonatomic) IBOutlet UILabel *labelAllowReview;
@property (strong, nonatomic) IBOutlet UILabel *labelAssignQuestions;

// FIELDS
@property (strong, nonatomic) IBOutlet UITextField *fieldName;
@property (strong, nonatomic) IBOutlet UITextField *fieldDescription;
@property (strong, nonatomic) IBOutlet UITextField *fieldGrade;
@property (strong, nonatomic) IBOutlet UITextField *fieldResult;
@property (strong, nonatomic) IBOutlet UITextField *fieldCategory;
@property (strong, nonatomic) IBOutlet UITextField *fieldType;
@property (strong, nonatomic) IBOutlet UITextField *fieldDifficulty;
@property (strong, nonatomic) IBOutlet UITextField *fieldSkill;
@property (strong, nonatomic) IBOutlet UITextField *fieldAttempts;
@property (strong, nonatomic) IBOutlet UITextField *fieldPassingRate;
@property (strong, nonatomic) IBOutlet UITextField *fieldTimeLimit;
@property (strong, nonatomic) IBOutlet UITextField *fieldOpeningDate;
@property (strong, nonatomic) IBOutlet UITextField *fieldClosingDate;

// SWITCH
@property (strong, nonatomic) IBOutlet UISwitch *switchShuffleQuestion;
@property (strong, nonatomic) IBOutlet UISwitch *switchDisplayFeedback;
@property (strong, nonatomic) IBOutlet UISwitch *switchForceComplete;
@property (strong, nonatomic) IBOutlet UISwitch *switchShuffleAnswer;
@property (strong, nonatomic) IBOutlet UISwitch *switchAllowReview;

// BUTTON
@property (strong, nonatomic) IBOutlet UIButton *buttonGrade;
@property (strong, nonatomic) IBOutlet UIButton *buttonResult;
@property (strong, nonatomic) IBOutlet UIButton *buttonCategory;
@property (strong, nonatomic) IBOutlet UIButton *buttonType;
@property (strong, nonatomic) IBOutlet UIButton *buttonDifficulty;
@property (strong, nonatomic) IBOutlet UIButton *buttonSkill;
@property (strong, nonatomic) IBOutlet UIButton *buttonTimeLimit;
@property (strong, nonatomic) IBOutlet UIButton *buttonOpeningDate;
@property (strong, nonatomic) IBOutlet UIButton *buttonClosingDate;
//@property (strong, nonatomic) IBOutlet UIButton *buttonAssignQuestion;

// POPOVER
@property (strong, nonatomic) UIPopoverController *pcGrade;
@property (strong, nonatomic) UIPopoverController *pcResult;
@property (strong, nonatomic) UIPopoverController *pcCategory;
@property (strong, nonatomic) UIPopoverController *pcType;
@property (strong, nonatomic) UIPopoverController *pcDifficulty;
@property (strong, nonatomic) UIPopoverController *pcSkill;
@property (strong, nonatomic) UIPopoverController *pcTimeLimit;
@property (strong, nonatomic) UIPopoverController *pcOpeningDate;
@property (strong, nonatomic) UIPopoverController *pcClosingDate;
@property (strong, nonatomic) NSManagedObject *mo;

// DICTIONARY
@property (strong, nonatomic) NSMutableDictionary *stageData;
@property (strong, nonatomic) NSMutableDictionary *resultData;
@property (strong, nonatomic) NSMutableDictionary *categoryData;
@property (strong, nonatomic) NSMutableDictionary *typeData;
@property (strong, nonatomic) NSMutableDictionary *difficultyData;
@property (strong, nonatomic) NSMutableDictionary *skillData;

@property (strong, nonatomic) NSDate *openingDate;
@property (strong, nonatomic) NSDate *closingDate;
@property (strong, nonatomic) NSDate *timeLimit;

@end

@implementation QuizHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super initWithCoder:aDecoder]) {
        [self initializeLocalization];
    }
    return self;
}

- (void)initializeLocalization {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [self localizeLabel:self.labelName withString:@"Name"];
    [self localizeLabel:self.labelDescription withString:@"Description"];
    [self localizeLabel:self.labelGrade withString:@"Grade"];
    [self localizeLabel:self.labelResult withString:@"Result Type"];
    [self localizeLabel:self.labelCategory withString:@"Quiz Category"];
    [self localizeLabel:self.labelType withString:@"Test Type"];
    [self localizeLabel:self.labelDifficulty withString:@"Difficulty Level"];
    [self localizeLabel:self.labelSkill withString:@"Learning Skill"];
    [self localizeLabel:self.labelAttempts withString:@"Attempts"];
    [self localizeLabel:self.labelPassingRate withString:@"Passing Rate"];
    [self localizeLabel:self.labelTimeLimit withString:@"Time Limit"];
    [self localizeLabel:self.labelOpenCloseDate withString:@"Opening and Closing Date"];
    [self localizeLabel:self.labelShuffleQuestion withString:@"Shuffle Question"];
    [self localizeLabel:self.labelDisplayFeedback withString:@"Display Feedback"];
    [self localizeLabel:self.labelForceComplete withString:@"Force Complete"];
    [self localizeLabel:self.labelShuffleAnswer withString:@"Shuffle Answer"];
    [self localizeLabel:self.labelAllowReview withString:@"Allow Review"];
    [self localizeLabel:self.labelAssignQuestions withString:@"Assign Questions"];
}

// ENH#863-jca-01-05-2016
// IBOutlets are not set in initWithCoder
- (void)awakeFromNib {
    [self initializeLocalization];
}

- (IBAction)buttonAction:(UIButton *)sender {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    UIButton *b = sender;
    if (b == self.buttonGrade) {
        [self optionPopoverForEntity:kStageTypeEntity button:b];
    }
    
    if (b == self.buttonResult) {
        [self optionPopoverForEntity:kResultTypeEntity button:b];
    }

    if (b == self.buttonCategory) {
        [self optionPopoverForEntity:kCategoryTypeEntity button:b];
    }
    
    if (b == self.buttonType) {
        [self optionPopoverForEntity:kTestTypeEntity button:b];
    }
    
    if (b == self.buttonDifficulty) {
        [self optionPopoverForEntity:kProficiencyEntity button:b];
    }
    
    if (b == self.buttonSkill) {
        [self optionPopoverForEntity:kSkillEntity button:b];
    }
    
    if (b == self.buttonTimeLimit) {
        [self dateType:kTimeLimitType popover:self.pcOpeningDate button:b];
    }
    
    if (b == self.buttonOpeningDate) {
        [self dateType:kOpeningType popover:self.pcOpeningDate button:b];
    }
    
    if (b == self.buttonClosingDate) {
        [self dateType:kClosingType popover:self.pcClosingDate button:b];
    }
}

- (void )optionPopoverForEntity:(NSString *)entity button:(UIButton *)button {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    UIButton *b = button;
    
    if ( [entity isEqualToString:kStageTypeEntity] ) {
        TestGuruOptionMenu *om = [self optionMenuForEntity:entity];
        self.pcGrade = [self popoverViewWithContentController:om];
        
        UIPopoverArrowDirection direction = UIPopoverArrowDirectionRight;
        [self.pcGrade presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:direction animated:YES];
    }
    
    if ( [entity isEqualToString:kResultTypeEntity] ) {
        TestGuruOptionMenu *om = [self optionMenuForEntity:entity];
        self.pcResult = [self popoverViewWithContentController:om];
        
        UIPopoverArrowDirection direction = UIPopoverArrowDirectionRight;
        [self.pcResult presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:direction animated:YES];
    }

    if ( [entity isEqualToString:kCategoryTypeEntity] ) {
        TestGuruOptionMenu *om = [self optionMenuForEntity:entity];
        self.pcCategory = [self popoverViewWithContentController:om];
        
        UIPopoverArrowDirection direction = UIPopoverArrowDirectionRight;
        [self.pcCategory presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:direction animated:YES];
    }

    if ( [entity isEqualToString:kTestTypeEntity] ) {
        TestGuruOptionMenu *om = [self optionMenuForEntity:entity];
        self.pcType = [self popoverViewWithContentController:om];
        
        UIPopoverArrowDirection direction = UIPopoverArrowDirectionRight;
        [self.pcType presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:direction animated:YES];
    }

    if ( [entity isEqualToString:kProficiencyEntity] ) {
        TestGuruOptionMenu *om = [self optionMenuForEntity:entity];
        self.pcDifficulty = [self popoverViewWithContentController:om];
        
        UIPopoverArrowDirection direction = UIPopoverArrowDirectionRight;
        [self.pcDifficulty presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:direction animated:YES];
    }

    if ( [entity isEqualToString:kSkillEntity] ) {
        TestGuruOptionMenu *om = [self optionMenuForEntity:entity];
        self.pcSkill = [self popoverViewWithContentController:om];
        
        UIPopoverArrowDirection direction = UIPopoverArrowDirectionRight;
        [self.pcSkill presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:direction animated:YES];
    }

}

- (TestGuruDateMenu *)dateMenuForType:(NSString *)type {
    
    // DEFINE SIZE
    CGSize size = CGSizeMake(350, 250);
    
    // CONTROLLER
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    TestGuruDateMenu *dateMenu = (TestGuruDateMenu *)[sb instantiateViewControllerWithIdentifier:@"test_date_picker_menu"];
    dateMenu.type = type;
    dateMenu.value = [self dateObjectValueFromType:type];
    dateMenu.preferredContentSize = size;
    dateMenu.delegate = self;
    
    return dateMenu;
}

- (TestGuruOptionMenu *)optionMenuForEntity:(NSString *)entity {
    
    // DEFINE SIZE
    CGSize size = CGSizeMake(250, 180);
    
    // CONTROLLER
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    TestGuruOptionMenu *om = (TestGuruOptionMenu *)[sb instantiateViewControllerWithIdentifier:@"question_option_menu"];
    om.entity = entity;
    om.preferredContentSize = size;
    om.delegate = self;
    
    return om;
}

- (UIPopoverController *)popoverViewWithContentController:(UIViewController *)content {
    
    // DEFINE SIZE
    CGSize size = CGSizeMake(250, 180);
    
    // POPOVER
    UIPopoverController *p = [[UIPopoverController alloc] initWithContentViewController:content];
    p.popoverContentSize = size;
    p.delegate = self;
    
    return p;
}

- (void)selectedOptionMenuForEntity:(NSString *)entity withObject:(NSManagedObject *)object {
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    NSArray *keys = object.entity.propertiesByName.allKeys;
    for (NSString *k in keys) {
        NSString *value = [NSString stringWithFormat:@"%@", [object valueForKey:k] ];
        [data setValue:value forKey:k];
    }
    
    NSString *fieldValue = [NSString stringWithFormat:@"%@", [object valueForKey:@"value"] ];
    
    __weak typeof(self) wo = self;
    if ( [entity isEqualToString:kStageTypeEntity] ) {
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.fieldGrade.text = fieldValue;
            wo.stageData = [NSMutableDictionary dictionaryWithDictionary:data];
            [wo.pcGrade dismissPopoverAnimated:YES];
        });
    }
    
    if ( [entity isEqualToString:kResultTypeEntity] ) {
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.fieldResult.text = fieldValue;
            wo.typeData = [NSMutableDictionary dictionaryWithDictionary:data];
            [wo.pcResult dismissPopoverAnimated:YES];
        });
    }

    if ( [entity isEqualToString:kCategoryTypeEntity] ) {
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.fieldCategory.text = fieldValue;
            wo.categoryData = [NSMutableDictionary dictionaryWithDictionary:data];
            [wo.pcCategory dismissPopoverAnimated:YES];
        });
    }
    
    
    if ( [entity isEqualToString:kTestTypeEntity] ) {
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.fieldType.text = fieldValue;
            wo.typeData = [NSMutableDictionary dictionaryWithDictionary:data];
            [wo.pcType dismissPopoverAnimated:YES];
        });
    }
    
    if ( [entity isEqualToString:kProficiencyEntity] ) {
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.fieldDifficulty.text = fieldValue;
            wo.difficultyData = [NSMutableDictionary dictionaryWithDictionary:data];
            [wo.pcDifficulty dismissPopoverAnimated:YES];
        });
    }
    
    if ( [entity isEqualToString:kSkillEntity] ) {
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.fieldSkill.text = fieldValue;
            wo.skillData = [NSMutableDictionary dictionaryWithDictionary:data];
            [wo.pcSkill dismissPopoverAnimated:YES];
        });
    }
}

- (void)dateType:(NSString *)type popover:(UIPopoverController *)popover button:(UIButton *)b {
    
    if ( [type isEqualToString:kTimeLimitType] ) {
        TestGuruDateMenu *om = [self dateMenuForType:type];
        self.pcTimeLimit = [self popoverViewWithContentController:om];
        
        UIPopoverArrowDirection direction = UIPopoverArrowDirectionRight;
        [self.pcTimeLimit presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:direction animated:YES];
    }
    
    if ( [type isEqualToString:kOpeningType] ) {
        TestGuruDateMenu *om = [self dateMenuForType:type];
        self.pcOpeningDate = [self popoverViewWithContentController:om];
        
        UIPopoverArrowDirection direction = UIPopoverArrowDirectionRight;
        [self.pcOpeningDate presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:direction animated:YES];
    }
    
    if ( [type isEqualToString:kClosingType] ) {
        TestGuruDateMenu *om = [self dateMenuForType:type];
        self.pcClosingDate = [self popoverViewWithContentController:om];
        
        UIPopoverArrowDirection direction = UIPopoverArrowDirectionRight;
        [self.pcClosingDate presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:direction animated:YES];
    }
}

- (UIPopoverController *)datePopoverForType:(NSString *)type controller:(id)controller {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // DEFINE SIZE
    CGSize size = CGSizeMake(350, 250);
    
    // CONTROLLER
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    TestGuruDateMenu *dateMenu = (TestGuruDateMenu *)[sb instantiateViewControllerWithIdentifier:@"test_date_picker_menu"];
    dateMenu.type = type;
    dateMenu.value = [self dateObjectValueFromType:type];
    
    dateMenu.preferredContentSize = size;
    dateMenu.delegate = controller;
    
    // POPOVER
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:dateMenu];
    popover.popoverContentSize = size;
    popover.delegate = controller;
    
    // RETURN
    return popover;
}

- (NSDate *)dateObjectValueFromType:(NSString *)type {
    
    NSDate *date_object = [NSDate date];
    
    if ([type isEqualToString:kTimeLimitType]) {
        date_object = self.timeLimit;
    }
    
    if ([type isEqualToString:kOpeningType]) {
        date_object = self.openingDate;
    }
    
    if ([type isEqualToString:kClosingType]) {
        date_object = self.closingDate;
    }
    
    return date_object;
}

- (NSTimeInterval)countDownObjectValueFromType:(NSString *)type {
    
    NSTimeInterval count_object = 0;
    if (self.timeLimit == nil) {
        self.timeLimit = [NSDate date];
    }
    
    if ([type isEqualToString:kTimeLimitType]) {
        count_object = [self.timeLimit timeIntervalSinceNow];
    }
    
    return count_object;
}

- (void)selectedDateForType:(NSString *)type withObject:(NSDictionary *)object {

    
    NSString *value = [NSString stringWithFormat:@"%@", object[@"time"] ];
    
    if ([type isEqualToString:kTimeLimitType]) {
        self.fieldTimeLimit.text = value;
        [self.pcTimeLimit dismissPopoverAnimated:YES];
    }
    
    if ([type isEqualToString:kOpeningType]) {
        self.openingDate = [self dateObjectFromStringObject:value];
        self.fieldOpeningDate.text = [self dateStringObjectFromDate:self.openingDate];
        [self.pcOpeningDate dismissPopoverAnimated:YES];
    }
    
    if ([type isEqualToString:kClosingType]) {
        self.closingDate = [self dateObjectFromStringObject:value];
        self.fieldClosingDate.text = [self dateStringObjectFromDate:self.closingDate];
        [self.pcClosingDate dismissPopoverAnimated:YES];
    }
}

#pragma mark - DISPLAY

- (void)localizeLabel:(UILabel *)label withString:(NSString *)string {
    label.text = NSLocalizedString(string, nil);
}

- (void)displayField:(UITextField *)field withKey:(NSString *)key withObject:(NSManagedObject *)mo {
    field.text = [NSString stringWithFormat:@"%@", [mo valueForKey:key] ];
}

- (NSMutableDictionary *)displayField:(UITextField *)field nameKey:(NSString *)name idKey:(NSString *)idkey
          withObject:(NSManagedObject *)mo {

    NSMutableDictionary *d = [NSMutableDictionary dictionary];
    
    NSString *value_string = [NSString stringWithFormat:@"%@", [mo valueForKey:name] ];
    NSString *id_string = [NSString stringWithFormat:@"%@", [mo valueForKey:idkey] ];
    
    field.text = value_string;
    
    [d setValue:value_string forKey:@"value"];
    [d setValue:id_string forKey:@"id"];
    
    return d;
}

- (void)displayDateField:(UITextField *)field withKey:(NSString *)key withObject:(NSManagedObject *)mo {
    
    NSString *date_string = [NSString stringWithFormat:@"%@", [mo valueForKey:key] ];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSString *template = @"yyyy-MM-dd HH:mm:ss";
    [formatter setDateFormat:template];
    NSDate *date = [formatter dateFromString:date_string];
    
    if (field == self.fieldOpeningDate) {
        self.openingDate = date;
    }
    
    if (field == self.fieldClosingDate) {
        self.closingDate = date;
    }
    
    field.text = [self dateStringObjectFromDate:date];
}

- (void)displayTimeLimitField:(UITextField *)field withKey:(NSString *)key withObject:(NSManagedObject *)mo {
    
    NSString *timelimit_string = [NSString stringWithFormat:@"%@", [mo valueForKey:key] ];
    NSDate *date = [self dateObjectForTimeLimit:timelimit_string];
    
    if (field == self.fieldTimeLimit) {
        self.timeLimit = date;
    }
    
    field.text = timelimit_string;
}

- (void)displaySwitch:(UISwitch *)switchField withKey:(NSString *)key withObject:(NSManagedObject *)mo {
    NSString *val = [NSString stringWithFormat:@"%@", [mo valueForKey:key] ];
    BOOL flag = ( [val isEqualToString:@"1"] ) ? YES : NO;
    switchField.on = flag;
}

- (void)displayData:(NSManagedObject *)object {
    
    self.mo = object;
    if (self.mo != nil) {
        
        NSString *entityName = self.mo.entity.name;
        NSLog(@"entity : %@", entityName);
        
        [self displayField:self.fieldName withKey:@"name" withObject:self.mo];
        [self displayField:self.fieldDescription withKey:@"desc" withObject:self.mo];
        
        // DICTIONARY TYPES
        self.stageData = [self displayField:self.fieldGrade nameKey:@"quiz_stage_name" idKey:@"quiz_stage_id" withObject:self.mo];
        self.resultData = [self displayField:self.fieldResult nameKey:@"quiz_result_type_name" idKey:@"quiz_result_type_id" withObject:self.mo];
        self.categoryData = [self displayField:self.fieldCategory nameKey:@"quiz_category_name" idKey:@"quiz_category_id" withObject:self.mo];
        self.typeData = [self displayField:self.fieldType nameKey:@"question_type_name" idKey:@"quiz_type_id" withObject:self.mo];
        self.difficultyData = [self displayField:self.fieldDifficulty nameKey:@"proficiency_level_name" idKey:@"proficiency_level_id" withObject:self.mo];
        self.skillData = [self displayField:self.fieldSkill nameKey:@"learning_skills_name" idKey:@"learning_skills_id" withObject:self.mo];

        // TIME LIMIT AND PASSING RATE
        [self displayField:self.fieldAttempts withKey:@"attempts" withObject:self.mo];
        [self displayField:self.fieldPassingRate withKey:@"passing_rate" withObject:self.mo];
        
        // DATE AND TIME LIMIT
        [self displayTimeLimitField:self.fieldTimeLimit withKey:@"time_limit" withObject:self.mo];
        [self displayDateField:self.fieldOpeningDate withKey:@"date_open" withObject:self.mo];
        [self displayDateField:self.fieldClosingDate withKey:@"date_close" withObject:self.mo];
        
        // SWITCH CONTROLS
        [self displaySwitch:self.switchShuffleQuestion withKey:@"quiz_shuffling_mode" withObject:self.mo];
        [self displaySwitch:self.switchDisplayFeedback withKey:@"show_feedbacks" withObject:self.mo];
        [self displaySwitch:self.switchForceComplete withKey:@"is_forced_complete" withObject:self.mo];
        [self displaySwitch:self.switchShuffleAnswer withKey:@"is_shuffle_answers" withObject:self.mo];
        [self displaySwitch:self.switchAllowReview withKey:@"allow_review" withObject:self.mo];
    }
}

- (void)displayDefaultValues {
    
    self.fieldName.text = @"";
    self.fieldDescription.text = @"";
    self.fieldGrade.text = [self defaulValueForEntity:kStageTypeEntity];
    self.fieldResult.text = [self defaulValueForEntity:kResultTypeEntity];
    self.fieldCategory.text = [self defaulValueForEntity:kCategoryTypeEntity];
    self.fieldType.text = [self defaulValueForEntity:kTestTypeEntity];
    self.fieldDifficulty.text = [self defaulValueForEntity:kProficiencyEntity];
    self.fieldSkill.text = [self defaulValueForEntity:kSkillEntity];
    self.fieldAttempts.text = @"1";
    self.fieldPassingRate.text = @"50";
    
    NSString *defaultTimeLimit = @"00:30:00";
    self.timeLimit = [self dateObjectForTimeLimit:defaultTimeLimit];
    self.fieldTimeLimit.text = defaultTimeLimit;
    
    NSDate *defaultDate = [NSDate date];
    self.openingDate = defaultDate;
    self.fieldOpeningDate.text = [self dateStringObjectFromDate:self.openingDate];
    
    self.closingDate = defaultDate;
    self.fieldClosingDate.text = [self dateStringObjectFromDate:self.closingDate];
}


// NOTE: This code assumes you have set the UITextField(s)'s delegate property to the object that will contain this code, because otherwise it would never be called.
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ( (textField == self.fieldAttempts) || (textField == self.fieldPassingRate) ) {
        
        // allow backspace
        if (!string.length)
        {
            return YES;
        }
        
        NSCharacterSet *numberSet = [NSCharacterSet decimalDigitCharacterSet];
        if ([string rangeOfCharacterFromSet:[numberSet invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        
        // verify max length has not been exceeded
        NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if (updatedText.length > 3) // 4 was chosen for SSN verification
        {
            // suppress the max length message only when the user is typing
            // easy: pasted data has a length greater than 1; who copy/pastes one character?
            if (string.length > 1)
            {
                // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
            }
            return NO;
        }
    }
    return YES;
}

- (NSString *)defaulValueForEntity:(NSString *)entity {
    
    TestGuruDataManager *tm = [TestGuruDataManager sharedInstance];
    NSDictionary *data = [tm fetchDefaultValueForEntity:entity];
    
    if ( [entity isEqualToString:kStageTypeEntity] ) {
        self.stageData = [NSMutableDictionary dictionaryWithDictionary:data];
        NSLog(@"graded : %@", self.stageData);
    }
    
    if ( [entity isEqualToString:kResultTypeEntity] ) {
        self.resultData = [NSMutableDictionary dictionaryWithDictionary:data];
        NSLog(@"resutl : %@", self.resultData);
    }
    
    if ( [entity isEqualToString:kCategoryTypeEntity] ) {
        self.categoryData = [NSMutableDictionary dictionaryWithDictionary:data];
        NSLog(@"category : %@", self.categoryData);
    }
    
    if ( [entity isEqualToString:kTestTypeEntity] ) {
        self.typeData = [NSMutableDictionary dictionaryWithDictionary:data];
        NSLog(@"type : %@", self.typeData);
    }
    
    if ( [entity isEqualToString:kProficiencyEntity] ) {
        self.difficultyData = [NSMutableDictionary dictionaryWithDictionary:data];
        NSLog(@"difficulty : %@", self.difficultyData);
    }
    
    if ( [entity isEqualToString:kSkillEntity] ) {
        self.skillData = [NSMutableDictionary dictionaryWithDictionary:data];
        NSLog(@"skill : %@", self.skillData);
    }
    
    NSString *value = [data valueForKey:@"value"];
    
    return value;
}

#pragma mark - DATES

- (NSDate *)dateObjectFromStringObject:(NSString *)string {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSString *template = @"yyyy-MM-dd HH:mm:ss";
    [formatter setDateFormat:template];
    return [formatter dateFromString:string];
}

- (NSString *)dateStringObjectFromDate:(NSDate *)date {
    
    NSString *date_string = [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle];
    
    return date_string;
}

- (NSString *)stringObjectFromDate:(NSDate *)date {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

    //DATE PROCESSING
    NSString *template1 = @"yyyy-MM-dd HH:mm:ss";
    [formatter setDateFormat:template1];
    NSString *new_date_string = [formatter stringFromDate:date];//RAW GMT
    NSString *utc_date_string = [self translateDate:new_date_string from:@"GMT" to:@"UTC"];//UTC FORMAT WITH -8 HOURS
    
    return utc_date_string;
}

- (id)translateDate:(NSString *)rawGMT
               from:(NSString *)fromTimeZone
                 to:(NSString *)toTimeZone {
    NSDate *date = nil;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    if([fromTimeZone isEqualToString:@"UTC"]) {
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    }
    
    if([fromTimeZone isEqualToString:@"GMT"]) {
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    }
    
    if([rawGMT length] > 0) {
        date = [dateFormatter dateFromString:rawGMT];
    } else {
        date = [NSDate date];
    }
    
    if([toTimeZone isEqualToString:@"UTC"]) {
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    }
    
    if([toTimeZone isEqualToString:@"GMT"]) {
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    }
    
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

- (NSDate *)dateObjectForTimeLimit:(NSString *)timelimitString {
    
//    NSString *string = @"22/04/2013 05:56";
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    // this is imporant - we set our input date format to match our input string
//    // if format doesn't match you'll get nil from your string, so be careful
//    [dateFormatter setDateFormat:@"dd/MM/yyyy hh:mm"];
//    NSDate *date = [[NSDate alloc] init];
//    // voila!
//    date = [dateFormatter dateFromString:string];
//    NSLog(@"dateFromString = %@", date);
//    
//    //date to timestamp
//    NSTimeInterval timeInterval = [date timeIntervalSince1970];
//    
    
    NSString *time_component = @"00:15:00";
    
    if ( (timelimitString.length == 0) || (timelimitString == nil ) ||
        [timelimitString isEqualToString:@"(null)"] || [timelimitString isEqualToString:@"<null>"] ) {
        timelimitString = time_component;
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *date_component = [formatter stringFromDate:[NSDate date]];
    NSString *date_and_time = [NSString stringWithFormat:@"%@ %@", date_component, timelimitString];
    NSString *template = @"yyyy-MM-dd HH:mm:ss";
    [formatter setDateFormat:template];
    NSDate *dateTime = [formatter dateFromString:date_and_time];
    
    return dateTime;
}

- (NSMutableDictionary *)getHeaderValues {
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    // FIELD TYPES
    [self getValueFromField:self.fieldName setKey:@"name" withDictionary:data];
    [self getValueFromField:self.fieldDescription setKey:@"description" withDictionary:data];
    
    // GRADE [STAGE]
    [self getDictionaryValue:self.stageData nameKey:@"quiz_stage_name" idKey:@"quiz_stage_id"
              withDictionary:data];
    
    // RESULT
    [self getDictionaryValue:self.resultData nameKey:@"quiz_result_type_name" idKey:@"quiz_result_type_id"
              withDictionary:data];
    
    // CATEGORY
    [self getDictionaryValue:self.categoryData nameKey:@"quiz_category_name" idKey:@"quiz_category_id"
              withDictionary:data];
    
    // TEST TYPE
    [self getDictionaryValue:self.typeData nameKey:@"question_type_name" idKey:@"quiz_type_id"
              withDictionary:data];
    
    // DIFFICULTY
    [self getDictionaryValue:self.difficultyData nameKey:@"proficiency_level_name" idKey:@"proficiency_level_id"
              withDictionary:data];
    
    // SKILL
    [self getDictionaryValue:self.skillData nameKey:@"learning_skills_name" idKey:@"learning_skills_id"
              withDictionary:data];
    
    // FIELD TYPES
    [self getValueFromField:self.fieldAttempts setKey:@"attempts" withDictionary:data];
    [self getValueFromField:self.fieldPassingRate setKey:@"passing_rate" withDictionary:data];
    [self getValueFromField:self.fieldTimeLimit setKey:@"time_limit" withDictionary:data];
    
    // DATE FIELDS
    [self getDateValue:self.openingDate setKey:@"date_open" withDictionary:data];
    [self getDateValue:self.closingDate setKey:@"date_close" withDictionary:data];
    
    // SWITCH VALUES
    [self getSwitchValue:self.switchShuffleQuestion setKey:@"quiz_shuffling_mode" withDictionary:data];
    [self getSwitchValue:self.switchDisplayFeedback setKey:@"show_feedbacks" withDictionary:data];
    [self getSwitchValue:self.switchForceComplete setKey:@"is_forced_complete" withDictionary:data];
    [self getSwitchValue:self.switchShuffleAnswer setKey:@"is_shuffle_answers" withDictionary:data];
    [self getSwitchValue:self.switchAllowReview setKey:@"allow_review" withDictionary:data];
    
    return data;
}

- (void)getDateValue:(NSDate *)date setKey:(NSString *)key withDictionary:(NSMutableDictionary *)dictionary {
    
    if (dictionary) {
        NSString *id_string = [NSString stringWithFormat:@"%@", [self stringObjectFromDate:date] ];
        [dictionary setValue:id_string forKey:key];
    }
}

- (void)getDictionaryValue:(NSDictionary *)object nameKey:(NSString *)namekey idKey:(NSString *)idkey withDictionary:(NSMutableDictionary *)dictionary {

    NSLog(@"object : %@", object);
    
    NSString *name_string = [NSString stringWithFormat:@"%@", object[@"value"] ];
    NSString *id_string = [NSString stringWithFormat:@"%@", object[@"id"] ];

    [dictionary setValue:name_string forKey:namekey];
    [dictionary setValue:id_string forKey:idkey];
}

- (void)getSwitchValue:(UISwitch *)box setKey:(NSString *)key withDictionary:(NSMutableDictionary *)dictionary {
    
    if (dictionary) {
        NSString *value = (box.on) ? @"1" : @"0";
        [dictionary setValue:value forKey:key];
    }
}

- (void)getValueFromField:(UITextField *)field setKey:(NSString *)key withDictionary:(NSMutableDictionary *)dictionary {
    
    if (dictionary) {
        NSString *value = [NSString stringWithFormat:@"%@", field.text];
        [dictionary setValue:value forKey:key];
    }
}

@end
