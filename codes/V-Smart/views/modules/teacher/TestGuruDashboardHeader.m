//
//  TestGuruDashboardHeader.m
//  V-Smart
//
//  Created by Ryan Migallos on 7/20/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruDashboardHeader.h"

@implementation TestGuruDashboardHeader

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // QUESTION    
//    self.questionImageView.image = [UIImage imageNamed:@"question_icn150"];
    self.questionImageView.image = [UIImage imageNamed:@"add_question_white72x72"];
    self.questionTitle.text = NSLocalizedString(@"Questions", nil);
    self.questionCount.text = @"0";
    
    // TESTS
//    self.testImageView.image = [UIImage imageNamed:@"test_icn150"];
    self.testImageView.image = [UIImage imageNamed:@"test_white72x72"];
    self.testTitle.text =  NSLocalizedString(@"Tests", nil);
    self.testCount.text = @"0";
    
    // DEPLOYED
//    self.deployImageView.image = [UIImage imageNamed:@"deploy_icn150"];
    self.deployImageView.image = [UIImage imageNamed:@"test_player_white72x72"];
    self.deployTitle.text = NSLocalizedString(@"Deployed Tests", nil);
    self.deployCount.text = @"0";
}

@end
