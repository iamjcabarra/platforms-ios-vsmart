//
//  QuestionGroupTableViewCell.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 21/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestGuruDataManager.h"
#import "TGQuestionDetailsView.h"

@protocol QuestionGroupTableViewDelegate <NSObject>
@required
- (void)transitionWithObject:(NSManagedObject *)editObject withSegueIdentifier:(NSString *)identifier withIndexPath:(NSIndexPath *)indexPath;
- (void)performReload;
- (void)performUpdateLabel;
@end

@interface QuestionGroupTableViewCell : UITableViewCell <NSFetchedResultsControllerDelegate, UIScrollViewDelegate, UITableViewDelegate, TGQuestionDetailsDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSDateFormatter *formatter;
@property (strong, nonatomic) NSMutableDictionary *activeDownloads;

@property (strong, nonatomic) dispatch_queue_t queue;
@property (strong, nonatomic) NSString *user_id;
@property (assign, nonatomic) BOOL editOperation;
@property (weak, nonatomic) IBOutlet UIButton *loadMoreButton;
@property (weak, nonatomic) IBOutlet UIView *footerContainer;

@property (assign, nonatomic) BOOL loadMoreEnabled;

@property (strong, nonatomic) NSString *section_id;
@property (strong, nonatomic) NSString *package_type_id;
@property (strong, nonatomic) NSString *course_id;
@property (strong, nonatomic) NSString *tag_id;

@property (strong, nonatomic) NSString *groupByType;

@property (assign, nonatomic) NSInteger current_page;
@property (assign, nonatomic) NSInteger total_filtered;
@property (assign, nonatomic) NSInteger total_pages;


@property (strong, nonatomic) NSDictionary *final_data;


@property (strong, nonatomic) NSDateFormatter *dateToStringFormatter;

@property (nonatomic, weak) id <QuestionGroupTableViewDelegate> delegate;

@property (strong, nonatomic) NSArray *sortDescriptors;

@end




