//
//  TGTMPackageTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 22/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class TGTMPackageTableViewCell: UITableViewCell {
    
    @IBOutlet var packageImage: UIImageView!
    @IBOutlet var packageTitleLabel: UILabel!
    @IBOutlet var packageDescriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
