//
//  TestInformationCollapsibleView.m
//  V-Smart
//
//  Created by Julius Abarra on 14/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "TestInformationCollapsibleView.h"
#import "TestInformationSegmentController.h"

@interface TestInformationCollapsibleView ()

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) IBOutlet UIView *containerBackgroundView;

@property (strong, nonatomic) TestInformationSegmentController *segmentViewController;

@end

@implementation TestInformationCollapsibleView

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.containerBackgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.containerBackgroundView.layer.borderWidth = 1.0f;
    
    [self.segmentedControl addTarget:self action:@selector(segmentedControlAction:) forControlEvents:UIControlEventValueChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)segmentedControlAction:(id)sender {
    // Test Main Info
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        [self.segmentViewController swapViewControllers:@"embedTestMainInfoSegment"];
    }
    // Test Settings
    if (self.segmentedControl.selectedSegmentIndex == 1) {
        [self.segmentViewController swapViewControllers:@"embedTestSettingsSegment"];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedSegmentViewContainer"]) {
        self.segmentViewController = [segue destinationViewController];
    }
}

@end
