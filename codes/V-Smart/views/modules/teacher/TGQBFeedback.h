//
//  TGQBFeedback.h
//  V-Smart
//
//  Created by Ryan Migallos on 12/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGQBFeedback : UITableViewCell

@property (strong, nonatomic) NSString *generalFeedback;
@property (strong, nonatomic) NSString *correctAnswerFeedback;
@property (strong, nonatomic) NSString *wrongAnswerFeedback;

- (void)setObjectData:(NSManagedObject *)object;
- (void)resizeContents;

@end
