//
//  TestGuruEmptyPlaceholder.m
//  V-Smart
//
//  Created by Ryan Migallos on 23/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruEmptyPlaceholder.h"

@interface TestGuruEmptyPlaceholder()

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
//@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
//@property (strong, nonatomic) IBOutlet UIButton *createButton;
@property (strong, nonatomic) NSString *type;
@end


@implementation TestGuruEmptyPlaceholder

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
//    [self initializeImagePlaceHolderWithType:self.type];
//    
//    NSString *submodule = [NSString stringWithFormat:@"%@", self.type];
//    NSString *message = @"";
//    
//    if ([submodule isEqualToString:@"QUESTION"]) {
//        message = NSLocalizedString(@"No available questions yet. Create one today.", nil);
//    }
//    
//    if ([submodule isEqualToString:@"TEST"]) {
//        message = NSLocalizedString(@"No available tests yet. Create one today.", nil);             // unchecked
//    }
//    
//    if ([submodule isEqualToString:@"DEPLOY"]) {
//        message = NSLocalizedString(@"No available deployed tests yet. Create one today.", nil);    // unchecked
//    }
//    
//    self.messageLabel.text = message;
//    
//    NSLog(@"submodule [%@]", submodule);
    
    NSString *button_title = NSLocalizedString(@"Create", nil);
    [self.createButton setTitle:button_title forState:UIControlStateNormal];
    [self.createButton setTitle:button_title forState:UIControlStateHighlighted];
    [self.createButton addTarget:self action:@selector(createActionForButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)initializeImagePlaceHolderWithType:(NSString *)objectType {
    
    self.type = [NSString stringWithFormat:@"%@", objectType];
    
    //QUESTION DEFAULT;
    NSString *image_string = @"add_question_placeholder";
    NSString *message = [NSString stringWithFormat:@"%@ \"%@\"", NSLocalizedString(@"No results found for", nil), objectType];
    BOOL createIsHidden = YES;

    if ([objectType isEqualToString:@"TESTGURU_QUESTION"]) {
        image_string = @"add_question_placeholder";
        message = NSLocalizedString(@"No available questions yet. Create one today.", nil);
        createIsHidden = NO;
    }
    
    if ([objectType isEqualToString:@"TESTGURU_TEST"]) {
        image_string = @"test_placeholder";
        message = NSLocalizedString(@"No available tests yet. Create one today.", nil);             // unchecked
        createIsHidden = NO;
    }
    
    if ([objectType isEqualToString:@"TESTGURU_DEPLOY"]) {
        image_string = @"test-player_placeholder";
        message = NSLocalizedString(@"No available deployed tests yet. Create one today.", nil);   // unchecked
        createIsHidden = NO;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.createButton.hidden = createIsHidden;
        self.messageLabel.text = message;
        self.imageView.image = [UIImage imageNamed:image_string];
    });
}

- (void)createActionForButton:(id)sender {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSDictionary *data = @{@"action":@"CREATE"};
    if ([(NSObject*)self.delegate respondsToSelector:@selector(didFinishSelectingOperationObject:)] ) {
        [self.delegate didFinishSelectingOperationObject:data];
    }
}

@end
