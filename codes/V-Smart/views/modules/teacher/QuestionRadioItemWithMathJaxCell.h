//
//  QuestionRadioItemWithMathJaxCell.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 01/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuestionRadioItemWithMathJaxCellDelegate <NSObject>
@optional
- (void)didFinishloadingChoice:(NSIndexPath *)indexPath;
@end

@interface QuestionRadioItemWithMathJaxCell : UITableViewCell <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeight;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *radioButton;


@property (weak, nonatomic) NSIndexPath *indexPath;
@property (weak, nonatomic) id <QuestionRadioItemWithMathJaxCellDelegate> delegate;

@property (assign, nonatomic) BOOL isForTestViewResults;

- (void)loadWebViewWithContents:(NSString *)string;
- (void)reconfigureWebViewForTestResults:(BOOL)reconfigure;
@end
