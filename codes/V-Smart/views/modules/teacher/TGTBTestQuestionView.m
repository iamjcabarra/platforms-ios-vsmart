//
//  TGTBTestQuestionView.m
//  V-Smart
//
//  Created by Julius Abarra on 07/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGTBTestQuestionView.h"
#import "TGTBQuestionItemCell.h"
#import "TestGuruDataManager.h"
#import "TGTBAssignQuestionView.h"
#import "QuestionSamplerView.h"

#import "V_Smart-Swift.h" //SWIFT 2.1.1 IMPLEMENTATION

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)

@interface TGTBTestQuestionView () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate, TGTBAssignQuestionDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) TestGuruDataManager *tm;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *selectAllCheckBoxButton;
@property (weak, nonatomic) IBOutlet UILabel *selectAllLabel;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;
@property (weak, nonatomic) IBOutlet UIButton *assignQuestionButton;
@property (weak, nonatomic) IBOutlet UILabel *selectedStatsLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emptyViewTopConstraint;

@property (strong, nonatomic) NSDateFormatter *formatter;
@property (strong, nonatomic) NSDateFormatter *dateToStringFormatter;

@property (strong, nonatomic) NSNotificationCenter *notif;

@property (strong, nonatomic) NSArray *sortDescriptors;
@property (assign, nonatomic) BOOL sortOrderAscending;

@property (assign, nonatomic) BOOL searchBarActive;

@end

@implementation TGTBTestQuestionView
static NSString *kQuestionTypeCellIdentifier = @"questionCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tm = [TestGuruDataManager sharedInstance];
    self.searchBar.delegate = self;
    self.notif = [NSNotificationCenter defaultCenter];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSString *guid = [self.testObject valueForKey:@"id"];
    NSLog(@"---------------- %s GUID: %@", __PRETTY_FUNCTION__, guid);
    
    NSString *imageName = @"add-btn-256px.png";
    
//    if (self.crudActionType == TGTBCrudActionTypeEdit) {
//        imageName = @"add-btn-256px.png";
//    }
    
    [self.assignQuestionButton setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    
    UINib *questionItemCellNib = [UINib nibWithNibName:@"TGTBQuestionItemCell" bundle:nil];
    [self.tableView registerNib:questionItemCellNib forCellReuseIdentifier:kQuestionTypeCellIdentifier];
    
    self.selectAllLabel.text = NSLocalizedString(@"Select All", nil);
    
    self.formatter = [[NSDateFormatter alloc] init];
    [self.formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [self.formatter setTimeZone:[NSTimeZone localTimeZone]];
    
    self.dateToStringFormatter = [[NSDateFormatter alloc] init];
    [self.dateToStringFormatter setDateFormat:@"EEE, MMM dd, yyyy hh:mm aa"];
    [self.dateToStringFormatter setTimeZone:[NSTimeZone localTimeZone]];
    

    self.sortOrderAscending = NO;
    [self setUpSortDescriptors];
    
    
    [self.selectAllCheckBoxButton addTarget:self action:@selector(selectAllAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.deleteButton addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.assignQuestionButton addTarget:self action:@selector(assignQuestionAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.assignQuestionButton customShadow:5.0f];
    [self.assignQuestionButton scaleAnimate:0.5];
    
//    [self updateAssignedStatistics];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self updateAssignedStatistics];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpSortDescriptors {
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"sort_date" ascending:self.sortOrderAscending];
    NSSortDescriptor *question_text = [NSSortDescriptor sortDescriptorWithKey:@"question_text" ascending:self.sortOrderAscending];
    NSSortDescriptor *question_id = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:self.sortOrderAscending];
    
    self.sortDescriptors = @[date_modified,question_text,question_id];
}

- (void)setupView:(NSInteger)count {
    BOOL emptyViewHidden = (count == 0) ? NO : YES;
    NSString *emptyMessage = NSLocalizedString(@"No assigned question yet", nil);
    CGFloat space = 0;
    
    if (self.searchBar.text.length > 0) {
        space = 71;
        emptyMessage = [NSString stringWithFormat:@"%@ '%@'", NSLocalizedString(@"No results found for", nil), self.searchBar.text];
    }
    
//    dispatch_async(dispatch_get_main_queue(), ^{
        self.emptyView.hidden = emptyViewHidden;
    self.emptyLabel.text = emptyMessage;
    self.emptyViewTopConstraint.constant = space;
//    });
}

- (void)updateAssignedStatistics {
    
    NSArray *array = [self.tm getObjectsForEntity:kQuestionEntity predicate:nil];
    
    CGFloat totalPoints = 0;
    NSInteger count = array.count;
    for (NSManagedObject *mo in array) {
        NSString *points = [NSString stringWithFormat:@"%@", [mo valueForKey:@"points"]];
        totalPoints = totalPoints + [points floatValue];
    }
    
    NSString *totalPointsString = [NSString stringWithFormat:@"%.02f", totalPoints];
    totalPointsString = [self.tm formatStringNumber:totalPointsString];
    
    self.selectedStatsLabel.text = [NSString stringWithFormat:@"%@ (%ld items / %@ points)", NSLocalizedString(@"Assigned Questions", nil), (long)count, totalPointsString];
}

#pragma mark - Search Bar delegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self reloadFetchedResultsController];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self.searchBarActive = NO;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self.searchBarActive = YES;
}

- (void)selectAllAction:(UIButton *)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSNumber *originalState = [NSNumber numberWithBool:sender.selected];
    BOOL newBool = (sender.selected) ? NO : YES;
    NSNumber *newState = [NSNumber numberWithBool:newBool];
    
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"is_selected" object:originalState];
    
    sender.selected = newBool;
    NSDictionary *userDetails = @{@"is_selected":newState};
    [self.tm updateObjectsFromEntity:kQuestionEntity details:userDetails predicate:predicate];
}

- (void)deleteAction:(UIButton *)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSNumber *selectedState = [NSNumber numberWithBool:YES];
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"is_selected" object:selectedState];
    
    NSArray *selectedItems = [self.tm getObjectsForEntity:kQuestionEntity predicate:predicate];
    NSInteger count = selectedItems.count;
    
    NSString *alertMessage = NSLocalizedString(@"Are you sure you want to unassign", nil);
    
    if (count > 0) {
        if (count == 1) {
            NSManagedObject *mo = [selectedItems lastObject];
            NSString *name = [mo valueForKey:@"name"];
            alertMessage = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Are you sure you want to unassign", nil), name];
        } else {
            
            NSString *firstHalf = NSLocalizedString(@"Are you sure you want to unassign", nil);
            NSString *secondHalf = NSLocalizedString(@"questions", nil);
            alertMessage = [NSString stringWithFormat:@"%@ %ld %@?", firstHalf, (long)count, secondHalf];
        }
    } else {
        alertMessage = NSLocalizedString(@"No question is selected", nil);
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Unassign", nil)
                                                                             message:alertMessage
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    if (count > 0) {
        UIAlertAction *yesAlertAction =
        [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                 style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * _Nonnull action) {
                                   [self.tm clearContentsForEntity:kQuestionEntity predicate:predicate];
                                   [self updateAssignedStatistics];
                                   [self.notif postNotificationName:kNotificationTestCreateVerification object:nil];
                               }];
        
        UIAlertAction *noAlertAction =
        [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
                                 style:UIAlertActionStyleDefault
                               handler:nil];
        
        [alertController addAction:yesAlertAction];
        [alertController addAction:noAlertAction];
    } else {
        UIAlertAction *cancelAlertAction =
        [UIAlertAction actionWithTitle:NSLocalizedString(@"Okay", nil)
                                 style:UIAlertActionStyleDefault
                               handler:nil];
                [alertController addAction:cancelAlertAction];
    }
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)assignQuestionAction:(UIButton *)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
    [self performSegueWithIdentifier:@"TGTB_ASSIGN_QUESTION_VIEW" sender:nil];
    });
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    
    [self setupView:count];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kQuestionTypeCellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TGTBQuestionItemCell *questionCell = (TGTBQuestionItemCell *)cell;
    [self configureQuestionCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(TGTBQuestionItemCell *)cell managedObject:(NSManagedObject *)mo
            objectAtIndexPath:(NSIndexPath *)indexPath {
    [cell shouldHideCheckbox:NO];
    
    NSString *name = [mo valueForKey:@"name"];
    //NSString *date_modified = [mo valueForKey:@"date_modified"];
    //date_modified = [date_modified stringByReplacingOccurrencesOfString:@"+0000" withString:@""];
    //NSDate *date = [self.formatter dateFromString:date_modified];
    //NSString *date_string_object = [[self.dateToStringFormatter stringFromDate:date] uppercaseString];
    NSString *formatted_date_modified = [mo valueForKey:@"formatted_date_modified"];
    NSString *questionTypeName = [mo valueForKey:@"questionTypeName"];
    NSString *proficiency_level_id = [mo valueForKey:@"proficiency_level_id"];
    NSString *points = [mo valueForKey:@"points"];
    NSString *package_type_icon = [mo valueForKey:@"package_type_icon"];
    [cell.packageTypeImage sd_setImageWithURL:[NSURL URLWithString:package_type_icon]];
    
    cell.questionTitleLabel.text = name;
    cell.questionTypeLabel.text = [questionTypeName uppercaseString];
    cell.questionDateLabel.text = formatted_date_modified;
    
    NSString *question_text = [mo valueForKey:@"question_text"];
    question_text = [question_text stringByReplacingOccurrencesOfString:@"-blank-" withString:@"_______________"];
    [cell loadWebViewWithContent:question_text];
    
    [cell updateDifficultyLevelView:proficiency_level_id];
    [cell updatePoints:points];
    
    [cell.checkBoxButton addTarget:self action:@selector(toggleCheckBoxButton:) forControlEvents:UIControlEventTouchUpInside];
    
    BOOL is_selected = [(NSNumber *)[mo valueForKey:@"is_selected"] boolValue];
    cell.checkBoxButton.selected = is_selected;
    cell.detailView.backgroundColor = (is_selected) ? UIColorFromHex(0xcdecd2) : [UIColor whiteColor];
}

- (void)toggleCheckBoxButton:(UIButton *)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *question_id = [mo valueForKey:@"id"];
    
    sender.selected = (sender.selected) ? NO : YES;
    NSDictionary *userDetails = @{@"is_selected":[NSNumber numberWithBool:sender.selected]};
    
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" object:question_id];
    [self.tm updateTest:kQuestionEntity predicate:predicate details:userDetails]; // meantime fix
    
    //    [self.tm updateObject:mo withDetails:userDetails];
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 185;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"SAMPLE_QUESTION_VIEW" sender:self];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"TGTB_ASSIGN_QUESTION_VIEW"]) {
        NSArray *items = [self.tm getObjectsForEntity:kQuestionEntity
                                         predicate:nil];
        
        NSMutableSet *setOfDictObjects = [NSMutableSet set];
        
        for (NSManagedObject *object in items) {
            NSArray *keys = object.entity.propertiesByName.allKeys;
            NSDictionary *dict = [object dictionaryWithValuesForKeys:keys];
            [setOfDictObjects addObject:dict];
        }
        
        TGTBAssignQuestionView *assignQuestion = (TGTBAssignQuestionView *)[segue destinationViewController];
        assignQuestion.setOfAssignedObjects = setOfDictObjects;
        assignQuestion.delegate = self;
    }
    
    
    if ([segue.identifier isEqualToString:@"SAMPLE_QUESTION_VIEW"]) {
        QuestionSamplerView *sampler = (QuestionSamplerView *)[segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        sampler.indexPath = indexPath;
        sampler.entityObject = kQuestionEntity;
        sampler.sortDescriptors = self.sortDescriptors;
        sampler.isTestPreview = YES;
    }
    
    
}

- (void)didFinishAssigning:(BOOL)status {
    [self.notif postNotificationName:kNotificationTestCreateVerification object:nil];
    [self updateAssignedStatistics];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     Get the new view controller using [segue destinationViewController].
     Pass the selected object to the new view controller.
}
*/


#pragma mark - Fetched results controller

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (NSFetchedResultsController *)fetchedResultsController {
//        BOOL isAscending = YES;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    
    NSPredicate *predicate;
    
    if (self.searchBarActive) {
        if (![self.searchBar.text isEqual:@""]) {
            NSLog(@"SEARCHBAR.TEXT [%@]", self.searchBar.text);
            predicate = [self.tm predicateForKeyPathContains:@"search_string" value:self.searchBar.text];
            [fetchRequest setPredicate:predicate];
        }
    }
    
    // Edit the sort key as appropriate.
    [fetchRequest setSortDescriptors:self.sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    
    NSString *section_name = nil; //(self.isGroupBy) ? @"section_name" : nil;
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:section_name
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}


@end
