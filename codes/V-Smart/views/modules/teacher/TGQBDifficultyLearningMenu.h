//
//  TGQBDifficultyLearningMenu.h
//  V-Smart
//
//  Created by Ryan Migallos on 11/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGQBDifficultyLearningMenu : UITableViewCell

- (void)setDifficultyValueForEntity:(NSString *)entity withData:(NSDictionary *)data;
- (NSDictionary *)getDifficultyObject;

- (void)setLearningValueForEntity:(NSString *)entity withData:(NSDictionary *)data;
- (NSDictionary *)getLearningObject;

- (void)setObjectData:(NSManagedObject *)object;

@end
