//
//  TGQBCompentencyViewController.m
//  V-Smart
//
//  Created by Ryan Migallos on 17/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGQBCompentencyViewController.h"
#import "TGQBLearningCompetencyViewController.h"
#import "TGQBCompetencyHistoryViewController.h"
#import "TestGuruDataManager.h"
#import "TestGuruOptionMenu.h"
#import "MainHeader.h"

@interface TGQBCompentencyViewController () <UIPopoverControllerDelegate, UISearchBarDelegate, OptionMenuDelegate, TGQBCompetencyDelegate, TGQBCompetencyHistoryDelegate>

//LABEL HEADINGS
@property (strong, nonatomic) IBOutlet UILabel *competencyTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *historyLabel;
@property (strong, nonatomic) IBOutlet UILabel *curriculumHeadingLabel;
@property (strong, nonatomic) IBOutlet UILabel *periodHeadingLabel;
@property (strong, nonatomic) IBOutlet UILabel *learningTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *learningCountLabel;

// VIEW
@property (strong, nonatomic) IBOutlet UIView *separatorView;
@property (strong, nonatomic) IBOutlet UIView *curriculumContainer;
@property (strong, nonatomic) IBOutlet UIView *periodContainer;

@property (strong, nonatomic) IBOutlet UIView *curriculumView;
@property (strong, nonatomic) IBOutlet UIView *periodView;

// INPUT FIELDS
@property (strong, nonatomic) IBOutlet UILabel *curriculumFieldLabel;
@property (strong, nonatomic) IBOutlet UILabel *periodFieldLabel;
@property (strong, nonatomic) IBOutlet UISearchBar *customSearchBar;

// BUTTONS
@property (strong, nonatomic) IBOutlet UIButton *curriculumButton;
@property (strong, nonatomic) IBOutlet UIButton *periodButton;
@property (strong, nonatomic) IBOutlet UIButton *chooseButton;

@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) TestGuruOptionMenu *curriculumOption;
@property (strong, nonatomic) UIPopoverController *curriculumPopover;

@property (strong, nonatomic) TestGuruOptionMenu *periodOption;
@property (strong, nonatomic) UIPopoverController *periodPopover;

@property (strong, nonatomic) TGQBLearningCompetencyViewController *competencyController;
@property (strong, nonatomic) TGQBCompetencyHistoryViewController *historyController;

// USER SELECTION STATE
@property (strong, nonatomic) NSString *curriculum_id;
@property (strong, nonatomic) NSString *curriculum_title;

@property (strong, nonatomic) NSString *period_id;
@property (strong, nonatomic) NSString *period_name;

@property (strong, nonatomic) NSString *competency_code;

@property (strong, nonatomic) NSDictionary *userData;

@end

@implementation TGQBCompentencyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tm = [TestGuruDataManager sharedInstance];
    
    [self.curriculumButton addTarget:self action:@selector(curriculumCustomAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.chooseButton addTarget:self action:@selector(chooseCompetencyAction:) forControlEvents:UIControlEventTouchUpInside];
    [self chooseButton:self.chooseButton enable:NO];
    
    [self applyShadowToView:self.competencyTitleLabel];
    [self applyShadowToView:self.curriculumHeadingLabel];
    [self applyShadowToView:self.periodHeadingLabel];
    [self applyShadowToView:self.learningTitleLabel];
    [self applyShadowToView:self.learningCountLabel];
    
    self.customSearchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.customSearchBar.delegate = self;
    
    [self setupCurriculumMenu];
    self.periodContainer.hidden = YES;
    self.separatorView.hidden = YES;
    
    self.curriculumFieldLabel.text = NSLocalizedString(@"Choose curriculum planner", nil);
    self.periodFieldLabel.text = NSLocalizedString(@"Choose period", nil);
    
    NSString *competency_count = [NSString stringWithFormat:@"%@ total records", @"0"];
    self.learningCountLabel.text = competency_count;
}

- (void)chooseButton:(UIButton *)button enable:(BOOL)enable {
    
    UIColor *color = (enable == YES) ? UIColorFromHex(0x68BF61) : [UIColor lightGrayColor];
    button.backgroundColor = color;
    button.userInteractionEnabled = enable;
}

- (void)selectedOptionMenuForEntity:(NSString *)entity withObject:(NSManagedObject *)object {
    
    if ([entity isEqualToString:kCourseCurriculumEntity]) {
        
        self.curriculum_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"curriculum_id"] ];
        self.curriculum_title = [NSString stringWithFormat:@"%@", [object valueForKey:@"value"] ];
        self.curriculumFieldLabel.text = [NSString stringWithFormat:@"%@", self.curriculum_title];
        self.periodContainer.hidden = NO;
        __weak typeof(self) wo = self;
        [self.tm requestTestGuruCurriculumOverviewForCurriculumID:self.curriculum_id doneBlock:^(BOOL status) {
            if (status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [wo.curriculumPopover dismissPopoverAnimated:YES];
                });
            }
        }];
    }

    if ([entity isEqualToString:kPeriodEntity]) {
        self.period_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"period_id"] ];
        self.period_name = [NSString stringWithFormat:@"%@", [object valueForKey:@"value"] ];
        self.periodFieldLabel.text = [NSString stringWithFormat:@"%@", self.period_name];
        
        NSDictionary *data = @{@"curriculum_id":self.curriculum_id,
                               @"curriculum_title":self.curriculum_title,
                               @"period_id":self.period_id,
                               @"period_name":self.period_name};
        
        __weak typeof(self) wo = self;
        [self.tm requestTestGuruLearningCompetenciesForPeriodID:self.period_id userData:data doneBlock:^(BOOL status) {
            if (status) {
                NSUInteger count = (NSUInteger)[wo.tm fetchCountForEntity:kCompetencyEntity];
                NSString *competency_count = [NSString stringWithFormat:@"%@ total records", @(count)];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [wo.periodPopover dismissPopoverAnimated:YES];
                    wo.learningCountLabel.text = competency_count;
                });
            }
        }];
    }
    
}

- (void)setupCurriculumMenu
{
//    [self.tm requestType:kEndPointTestGuruProficiencyLevel doneBlock:nil];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    self.curriculumOption = (TestGuruOptionMenu *)[sb instantiateViewControllerWithIdentifier:@"question_option_menu"];
    self.curriculumOption.entity = kCourseCurriculumEntity;
    NSUInteger item_count = [self.tm fetchCountForEntity:kCourseCurriculumEntity];
    CGFloat height = 50 * item_count;
    
    [self applyShadowToView:self.curriculumView];
    
    CGFloat width = self.curriculumView.bounds.size.width;
    self.curriculumOption.preferredContentSize = CGSizeMake(width, height);
    self.curriculumOption.delegate = self;
    self.curriculumPopover = [[UIPopoverController alloc] initWithContentViewController:self.curriculumOption];
    self.curriculumPopover.popoverContentSize = CGSizeMake(width, height);
    self.curriculumPopover.delegate = self;
    
    [self.periodButton addTarget:self action:@selector(periodCustomAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)curriculumCustomAction:(id)sender {

    [self setupCurriculumMenu];
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    UIButton *b = (UIButton *)sender;
    [self.curriculumPopover presentPopoverFromRect:b.bounds
                                            inView:b
                          permittedArrowDirections:UIPopoverArrowDirectionRight
                                          animated:YES];
}

- (void)setupPeriodMenu
{
//    [self.tm requestType:kEndPointTestGuruProficiencyLevel doneBlock:nil];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    self.periodOption = (TestGuruOptionMenu *)[sb instantiateViewControllerWithIdentifier:@"question_option_menu"];
    self.periodOption.entity = kPeriodEntity;
    NSUInteger item_count = [self.tm fetchCountForEntity:kPeriodEntity];
    CGFloat height = 44 * item_count;
    
    [self applyShadowToView:self.periodView];
    
    CGFloat width = self.periodView.bounds.size.width;
    self.periodOption.preferredContentSize = CGSizeMake(width, height);
    self.periodOption.delegate = self;
    self.periodPopover = [[UIPopoverController alloc] initWithContentViewController:self.periodOption];
    self.periodPopover.popoverContentSize = CGSizeMake(width, height);
    self.periodPopover.delegate = self;
}

- (void)periodCustomAction:(id)sender {
    
    [self setupPeriodMenu];
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    UIButton *b = (UIButton *)sender;
    [self.periodPopover presentPopoverFromRect:b.bounds
                                            inView:b
                          permittedArrowDirections:UIPopoverArrowDirectionRight
                                          animated:YES];
}

- (void)applyShadowToView:(UIView *)object {
    
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.5f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonCloseAction:(id)sender {
    [self closeButtonAction:sender];
}

- (void)closeButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)chooseCompetencyAction:(id)sender {
    
    
    [self.tm insertEntity:kCompetencyHistoryEntity userData:self.userData];
    
    if ([(NSObject *)self.delegate respondsToSelector:@selector(didFinishSelectingLearningCompetencyCode:)]) {
        [self.delegate didFinishSelectingLearningCompetencyCode:self.competency_code];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"tgqb_table_learning_competency_embed"]) {
        self.competencyController = (TGQBLearningCompetencyViewController *)[segue destinationViewController];
        self.competencyController.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"tgqb_table_history_competency_embed"]) {
        self.historyController = (TGQBCompetencyHistoryViewController *)[segue destinationViewController];
        self.historyController.delegate = self;
    }
}

#pragma mark - TGQBCompetencyDelegate

- (void)didFinishSelectingLearningCompetency:(NSManagedObject *)object {

    NSString *curriculum_id_string = [NSString stringWithFormat:@"%@", [object valueForKey:@"curriculum_id"] ];
    NSString *curriculum_title_string = [NSString stringWithFormat:@"%@", [object valueForKey:@"curriculum_title"] ];
    NSString *period_id_string = [NSString stringWithFormat:@"%@", [object valueForKey:@"period_id"] ];
    NSString *period_name_string = [NSString stringWithFormat:@"%@", [object valueForKey:@"period_name"] ];
    NSString *competency_id_string = [NSString stringWithFormat:@"%@", [object valueForKey:@"competency_id"] ];
    NSString *competency_name_string = [NSString stringWithFormat:@"%@", [object valueForKey:@"competency"] ];
    NSString *competency_code_string = [NSString stringWithFormat:@"%@", [object valueForKey:@"code"] ];
    
    self.competency_code = [NSString stringWithFormat:@"%@", [object valueForKey:@"code"] ];
    self.competencyTitleLabel.text = [NSString stringWithFormat:@"%@", [object valueForKey:@"code"] ];
    [self chooseButton:self.chooseButton enable:YES];
    
    NSDate *date_now = [NSDate date]; // DATE NOW
    
    self.userData = @{@"curriculum_id":curriculum_id_string,
                           @"curriculum_title":curriculum_title_string,
                           @"period_id":period_id_string,
                           @"period_name":period_name_string,
                           @"competency_id":competency_id_string,
                           @"competency":competency_name_string,
                           @"code":competency_code_string,
                           @"date_created":date_now};
}

#pragma mark - TGQBCompetencyHistoryDelegate

- (void)didFinishSelectingHistoricalData:(NSManagedObject *)object {
    
    self.competency_code = [NSString stringWithFormat:@"%@", [object valueForKey:@"code"] ];
    self.competencyTitleLabel.text = [NSString stringWithFormat:@"%@", [object valueForKey:@"code"] ];
    [self chooseButton:self.chooseButton enable:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    [self.competencyController competencySearchWithText:searchBar.text isActive:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    NSLog(@"%s", __PRETTY_FUNCTION__);
//    [self.competencyController competencySearchWithText:searchBar.text isActive:NO];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self.competencyController competencySearchWithText:searchBar.text isActive:YES];
}


@end
