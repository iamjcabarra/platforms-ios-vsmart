//
//  TGFeedBackItem.h
//  V-Smart
//
//  Created by Ryan Migallos on 09/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGFeedBackItemDelegate <NSObject>
@required
- (void)updatedText:(NSString *)text withIndexPath:(NSIndexPath *)indexPath;
@end

@interface TGFeedBackItem : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UILabel *feedbackPlaceholder;
@property (strong, nonatomic) NSIndexPath *indexObject;
@property (weak,   nonatomic) id <TGFeedBackItemDelegate> delegate;

@end
