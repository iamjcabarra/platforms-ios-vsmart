//
//  TestBankDeployController.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/26/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestBankDeployController : UIViewController

@property (nonatomic, strong) NSManagedObject *quiz_mo;

@end
