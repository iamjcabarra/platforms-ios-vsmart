//
//  UIViewCustomizeExtension.swift
//  V-Smart
//
//  Created by Ryan Migallos on 03/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
import QuartzCore

extension UIView {
    
    public func customShadow(_ rad:CGFloat) {
//        self.layer.shadowPath = UIBezierPath(rect: self.bounds).CGPath
//        self.layer.shadowColor = UIColor.lightGrayColor().CGColor
//        self.layer.shadowOffset = CGSizeMake(2.0, 2.0)
//        self.layer.shadowRadius = rad
//        self.layer.shadowOpacity = 0.5
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowRadius = rad
        self.layer.shadowOpacity = 0.3
        self.layer.masksToBounds = false
    }
    
    public func scaleAnimate(_ wait:TimeInterval) {
        
        UIView.animate(withDuration: 0.2, delay: wait, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            }) { (finished) -> Void in
                UIView.animate(withDuration: 0.2, animations: { () -> Void in
                    self.transform = CGAffineTransform(scaleX: 0.7, y: 0.7);
                    }, completion: { (finished) -> Void in
                        UIView.animate(withDuration: 0.2, animations: { () -> Void in
                            self.transform = CGAffineTransform.identity;
                        });
                })
        }
    }
    
    
    
    func applyCornerRadius(cornerRadius cr: CGFloat) {
//        self.layer.masksToBounds = true
        self.layer.cornerRadius = cr
    }
    
    
}

@IBDesignable
class UIViewCustomExtension: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 20.0 {
        didSet {
            layer.cornerRadius = cornerRadius;
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.black {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    
    
}
