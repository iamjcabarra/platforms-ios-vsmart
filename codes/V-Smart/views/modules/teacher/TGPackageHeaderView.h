//
//  TGPackageHeaderView.h
//  V-Smart
//
//  Created by Ryan Migallos on 04/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGPackageHeaderView : UICollectionReusableView

@property (strong, nonatomic) IBOutlet UILabel *headerLabel;

@end
