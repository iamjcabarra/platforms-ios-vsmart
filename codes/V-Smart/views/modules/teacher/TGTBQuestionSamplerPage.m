//
//  TGTBQuestionSamplerPage.m
//  V-Smart
//
//  Created by Ryan Migallos on 17/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGTBQuestionSamplerPage.h"

#import "UIImageView+WebCache.h"

#import "QuestionTextItemCell.h"
#import "QuestionPictureItemCell.h"
#import "QuestionRadioItemCell.h"
#import "QuestionTextAreaItemCell.h"
#import "QuestionImageRadioItemCell.h"
#import <AssetsLibrary/AssetsLibrary.h>

#import "QuestionTextWithMathJaxCell.h"
#import "QuestionRadioItemWithMathJaxCell.h"
#import "QuestionImageRadioItemWithMathJaxCell.h"

typedef NS_ENUM(NSUInteger, TGQuestionType) {
    TGQuestionTypeText,
    TGQuestionTypePicture,
    TGQuestionTypeRadioButton,
    TGQuestionTypeTextArea,
    TGQuestionTypeImageRadioButton,
    TGQuestionTypeTextWithMathJax,
    TGQuestionTypeChoiceWithMathJax,
    TGQuestionTypeImageChoiceWithMathJax
};

@implementation NSString (HtmlCustomCheck)

- (BOOL)containsHTML {
    NSString *stringcopy = [self copy];
    NSString *expression = @"<[^>]+>";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:stringcopy
                                                        options:0
                                                          range:NSMakeRange(0, [stringcopy length])];
    if (numberOfMatches == 1) {
        return YES;
    }
    
    return NO; //default value
}

@end

@interface TGTBQuestionSamplerPage() <UITableViewDataSource, QuestionTextWithMathJaxCellDelegate, QuestionRadioItemWithMathJaxCellDelegate, QuestionImageRadioItemWithMathJaxCellDelegate>


@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) NSArray *qItems;
@property (strong, nonatomic) NSArray *cItems;
@property (assign, nonatomic) BOOL clearTable;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (weak, nonatomic) IBOutlet UITableView *choicesTable;

@property (strong, nonatomic) UIImage *loadedAssetImage;
@property (strong, nonatomic) ALAssetsLibrary *assetslibrary;

@property (strong, nonatomic) NSMutableOrderedSet *choiceImageSet;

@end

@implementation TGTBQuestionSamplerPage

static NSString *kQTextID = @"cell_sample_question_text_id";
static NSString *kQImageID = @"cell_sample_question_picture_id";
static NSString *kQChoiceID = @"cell_sample_question_choice_id";
static NSString *kQTextAreaID = @"cell_sample_question_textarea_id";
static NSString *kQImageChoiceID = @"cell_sample_question_choice_image_id";

static NSString *kQTextWithMathJaxID = @"cell_sample_question_text_with_mathjax_id";
static NSString *kQChoiceWithMathJaxID = @"cell_sample_question_choice_with_mathjax_id";
static NSString *kQImageChoiceWithMathJaxID = @"cell_sample_question_choice_image_with_mathjax_id";

- (void)awakeFromNib {
    // Initialization code
    
    self.clearTable = YES;
    [self.table reloadData];
    [self.choicesTable reloadData];
    self.clearTable = NO;
    
    self.assetslibrary = [[ALAssetsLibrary alloc] init];
    
    // Resusable Cell
    UINib *questionTextNib = [UINib nibWithNibName:@"QuestionTextWithMathJaxCell" bundle:nil];
    [self.table registerNib:questionTextNib forCellReuseIdentifier:kQTextWithMathJaxID];
    
    // Resusable Cell
    UINib *choiceRadioNib = [UINib nibWithNibName:@"QuestionRadioItemWithMathJaxCell" bundle:nil];
    [self.choicesTable registerNib:choiceRadioNib forCellReuseIdentifier:kQChoiceWithMathJaxID];
    
    // Resusable Cell
    UINib *choiceImageRadioNib = [UINib nibWithNibName:@"QuestionImageRadioItemWithMathJaxCell" bundle:nil];
    [self.choicesTable registerNib:choiceImageRadioNib forCellReuseIdentifier:kQImageChoiceWithMathJaxID];
}

- (NSString *)stringValue:(id)object {
    
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"]) {
        return @"";
    }
    
    return value;
}

- (void)displayObject:(NSManagedObject *)object {
    
    NSLog(@"object ~>%@<~", object);
    
    if (object != nil) {
        
        [self.table reloadData];
        [self.choicesTable reloadData];
        
        self.choiceImageSet = [NSMutableOrderedSet orderedSet];
        
        NSMutableOrderedSet *qOrdered_set = [NSMutableOrderedSet orderedSet];
        NSMutableOrderedSet *cOrdered_set = [NSMutableOrderedSet orderedSet];
        NSMutableOrderedSet *imageSet = [NSMutableOrderedSet orderedSet];
        
        //        //QUESTION TEXT
        //        NSString *question_text = [self stringValue: [object valueForKey:@"question_text"] ];
        //
        //        TGQuestionType questionTextType = TGQuestionTypeText;
        //        if ([question_text containsHTML]) {
        //            questionTextType = TGQuestionTypeTextWithMathJax;
        //        }
        //
        //        NSDictionary *textData = @{@"type": @(questionTextType),
        //                                   @"content": question_text};
        //        [qOrdered_set addObject:textData];
        
        //QUESTION IMAGE (if any)
        NSString *image_one = [NSString stringWithFormat:@"%@", [object valueForKey:@"image_one"]];
        if ([self isNotEmpty:image_one]) {
            NSData *image_one_data = [NSData dataWithData:[object valueForKey:@"image_one_data"]];
            NSDictionary *imageData = @{@"index":@"1",
                                        @"data":image_one,
                                        @"image_data":image_one_data};
            [imageSet addObject:imageData];
        }
        
        NSString *image_two = [NSString stringWithFormat:@"%@", [object valueForKey:@"image_two"]];
        if ([self isNotEmpty:image_two]) {
            NSData *image_two_data = [NSData dataWithData:[object valueForKey:@"image_two_data"]];
            NSDictionary *imageData = @{@"index":@"2",
                                        @"data":image_two,
                                        @"image_data":image_two_data};
            [imageSet addObject:imageData];
        }
        
        NSString *image_three = [NSString stringWithFormat:@"%@", [object valueForKey:@"image_three"]];
        if ([self isNotEmpty:image_three]) {
            NSData *image_three_data = [NSData dataWithData:[object valueForKey:@"image_three_data"]];
            NSDictionary *imageData = @{@"index":@"3",
                                        @"data":image_three,
                                        @"image_data":image_three_data};
            [imageSet addObject:imageData];
        }
        
        NSString *image_four = [NSString stringWithFormat:@"%@", [object valueForKey:@"image_four"]];
        if ([self isNotEmpty:image_four]) {
            NSData *image_four_data = [NSData dataWithData:[object valueForKey:@"image_four_data"]];
            NSDictionary *imageData = @{@"index":@"4",
                                        @"data":image_four,
                                        @"image_data":image_four_data};
            [imageSet addObject:imageData];
        }
        
        
        //QUESTION CHOICES (if any)
        NSArray *choices = [[object valueForKey:@"choices"] allObjects];
        
        NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order_number" ascending:YES];
        NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
        NSArray *sortedChoices = [choices sortedArrayUsingDescriptors:descriptors];
        
        NSString *question_type_id = [self stringValue:[object valueForKey:@"question_type_id"]];
        BOOL show_choices = ![question_type_id isEqualToString:@"2"] && ![question_type_id isEqualToString:@"10"];
        NSMutableArray *answers = [NSMutableArray array]; // Ready for multiple blanks
                
        if ((sortedChoices != nil) &&  (sortedChoices.count > 0)) {
            for (NSManagedObject *c in sortedChoices) {
                NSString *choice_text = [self stringValue:[c valueForKey:@"text"]];
                NSString *choice_image = [self stringValue:[c valueForKey:@"choice_image"]];
                NSString *is_correct = [self stringValue:[c valueForKey:@"is_correct"]];
                NSData *choice_image_data = [NSData dataWithData:[c valueForKey:@"choice_image_data"]];
                NSString *choice_id = [self stringValue:[c valueForKey:@"id"]];
                
                TGQuestionType type = TGQuestionTypeRadioButton;
                
                BOOL choiceHasImage = choice_image.length > 0;
                
                if (choiceHasImage) {
                    NSDictionary *choiceDict = @{
                                                 @"choice_title":choice_text,
                                                 @"data":choice_image,
                                                 @"image_data":choice_image_data};
                    
                    [self.choiceImageSet addObject:choiceDict];
                }
                
                if ([choice_text containsHTML]) {
                    if (choiceHasImage) {
                        type = TGQuestionTypeImageChoiceWithMathJax;
                    }
                    else {
                        type = TGQuestionTypeChoiceWithMathJax;
                    }
                }
                else {
                    if (choiceHasImage) {
                        type = TGQuestionTypeImageRadioButton;
                    }
                }
                
                if (show_choices) {
                NSDictionary *choiceData = @{@"type": @(type),
                                             @"content": choice_text,
                                             @"choice_image": choice_image,
                                             @"is_correct": is_correct,
                                             @"choice_image_data":choice_image_data,
                                             @"choice_id":choice_id};
                    
                [cOrdered_set addObject:choiceData];
            }
                
                if (!show_choices) {
                    choice_text = [choice_text stringByReplacingOccurrencesOfString:@"," withString:@"/"];
                    
                    // CUSTOMIZE FOR FILL IN THE BLANKS ONLY
                    if ([question_type_id isEqualToString:@"2"]) {
                        choice_text = [NSString stringWithFormat:@"<font color=\"#3498DB\"><u>%@</u></font>", choice_text];
                    }
                    
                    [answers addObject:choice_text];
                }
            }
        }
        
        // QUESTION TEXT
        NSString *question_text = [self stringValue: [object valueForKey:@"question_text"]];
        TGQuestionType question_text_type = TGQuestionTypeText;
        
        // FILL IN THE BLANKS (NOTE: Refactor in multiple blanks are supported)
        if ([question_type_id isEqualToString:@"2"]) {
            NSString *string = answers.count > 0 ? answers[0] : @"__________";
            question_text = [question_text stringByReplacingOccurrencesOfString:@"-blank-" withString:string];
        }
        
        if ([question_text containsHTML]) {
            question_text_type = TGQuestionTypeTextWithMathJax;
        }
        
        NSDictionary *textData = @{@"type": @(question_text_type),
                                   @"content": question_text};
        
        [qOrdered_set addObject:textData];
        
        if (imageSet.count > 0) {
            NSDictionary *imageData = @{@"type": @(TGQuestionTypePicture),
                                        @"content": imageSet};
            [qOrdered_set addObject:imageData];
        }
        
        //        //ESSAY OR SHORT ANSWER
        //        NSString *question_type_id = [NSString stringWithFormat:@"%@",[object valueForKey:@"question_type_id"]];
        //        if ([question_type_id isEqual:@"6"] || [question_type_id isEqual:@"9"]) {
        //            NSDictionary *essayData = @{@"type": @(TGQuestionTypeTextArea)};
        //            [cOrdered_set addObject:essayData];
        //        }
        
        // ESSAY, SHORT ANSWER AND IDENTIFICATION
        if ([question_type_id isEqualToString:@"6"] || [question_type_id isEqualToString:@"9"] || [question_type_id isEqualToString:@"10"]) {
            NSString *text_area_content = [question_type_id isEqualToString:@"10"] && answers.count > 0 ? answers[0] : NSLocalizedString(@"Type your answer here...", nil);
            NSDictionary *essayData = @{@"type": @(TGQuestionTypeTextArea), @"text_area_content": text_area_content};
            [cOrdered_set addObject:essayData];
        }
        
        self.qItems = [NSArray arrayWithArray: [qOrdered_set array]];
        self.cItems = [NSArray arrayWithArray: [cOrdered_set array]];
        [self.table reloadData];
        [self.choicesTable reloadData];
    }
}

- (BOOL)isNotEmpty:(NSString *)string {
    BOOL isNotEmpty = NO;
    
    string = [string stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"<null>" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"null" withString:@""];
    
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceCharacterSet];
    string = [string stringByTrimmingCharactersInSet:whiteSpace];
    
    if (string.length > 0) {
        isNotEmpty = YES;
    }
    
    return isNotEmpty;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (self.clearTable) {
        return 0;
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    if (tableView == self.table) {
        count = [self.qItems count];
    } else {
        count = [self.cItems count];
    }
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *data = @{};
    if (tableView == self.table) {
        data = self.qItems[indexPath.row];
    } else {
        data = self.cItems[indexPath.row];
    }
    
    NSUInteger type = [(NSNumber *)data[@"type"] unsignedIntegerValue];
    NSString *identifier = [self identifierForType:type];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [self configureCell:cell tableView:tableView indexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (NSString *)identifierForType:(TGQuestionType)type {
    
    NSString *identifier = kQTextID;
    
    if (type == TGQuestionTypeText) {
        identifier = kQTextID;
    }
    
    if (type == TGQuestionTypePicture) {
        identifier = kQImageID;
    }
    
    if (type == TGQuestionTypeRadioButton) {
        identifier = kQChoiceID;
    }
    
    if (type == TGQuestionTypeImageRadioButton) {
        identifier = kQImageChoiceID;
    }
    
    if (type == TGQuestionTypeTextArea) {
        identifier = kQTextAreaID;
    }
    
    if (type == TGQuestionTypeTextWithMathJax) {
        identifier = kQTextWithMathJaxID;
    }
    
    if (type == TGQuestionTypeChoiceWithMathJax) {
        identifier = kQChoiceWithMathJaxID;
    }
    
    if (type == TGQuestionTypeImageChoiceWithMathJax) {
        identifier = kQImageChoiceWithMathJaxID;
    }
    
    return identifier;
}

- (void)configureCell:(UITableViewCell *)tvc tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = tvc.reuseIdentifier;
    
    if ([identifier isEqualToString:kQTextID]) {
        QuestionTextItemCell *cell = (QuestionTextItemCell *)tvc;
        [self configureTextCell:cell tableView:tableView indexPath:indexPath];
    }
    
    if ([identifier isEqualToString:kQImageID]) {
        QuestionPictureItemCell *cell = (QuestionPictureItemCell *)tvc;
        [self configurePictureCell:cell tableView:tableView indexPath:indexPath];
    }
    
    if ([identifier isEqualToString:kQChoiceID]) {
        QuestionRadioItemCell *cell = (QuestionRadioItemCell *)tvc;
        [self configureRadioButtonCell:cell tableView:tableView indexPath:indexPath];
    }
    
    if ([identifier isEqualToString:kQImageChoiceID]) {
        QuestionImageRadioItemCell *cell = (QuestionImageRadioItemCell *)tvc;
        [self configureImageRadioButtonCell:cell tableView:tableView indexPath:indexPath];
    }
    
    if ([identifier isEqualToString:kQTextAreaID]) {
        QuestionTextAreaItemCell *cell = (QuestionTextAreaItemCell *)tvc;
        [self configureTextAreaCell:cell tableView:tableView indexPath:indexPath];
    }
    
    if ([identifier isEqualToString:kQTextWithMathJaxID]) {
        QuestionTextWithMathJaxCell *cell = (QuestionTextWithMathJaxCell *)tvc;
        [self configureTextWithMathJaxCell:cell tableView:tableView indexPath:indexPath];
    }
    
    if ([identifier isEqualToString:kQChoiceWithMathJaxID]) {
        QuestionRadioItemWithMathJaxCell *cell = (QuestionRadioItemWithMathJaxCell *)tvc;
        [self configureRadioButtonWithMathJaxCell:cell tableView:tableView indexPath:indexPath];
    }
    
    if ([identifier isEqualToString:kQImageChoiceWithMathJaxID]) {
        QuestionImageRadioItemWithMathJaxCell *cell = (QuestionImageRadioItemWithMathJaxCell *)tvc;
        [self configureImageRadioButtonWithMathJaxCell:cell tableView:tableView indexPath:indexPath];
    }
    
}

- (void)configureTextCell:(QuestionTextItemCell *)cell tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    NSDictionary *data = self.qItems[indexPath.row];
    NSString *question_title = [self stringValue: data[@"content"] ];
    question_title = [question_title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    cell.questionText.text = question_title;
}

- (void)configureTextWithMathJaxCell:(QuestionTextWithMathJaxCell *)cell tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *data = self.qItems[indexPath.row];
    NSString *question_title = [self stringValue: data[@"content"] ];
    
    cell.delegate = self;
    cell.indexPath = indexPath;
    [cell loadWebViewWithContents:question_title];
}

- (void)configureRadioButtonWithMathJaxCell:(QuestionRadioItemWithMathJaxCell *)cell tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *data = self.cItems[indexPath.row];
    NSString *choice_title = [self stringValue: data[@"content"] ];
    
    cell.delegate = self;
    cell.indexPath = indexPath;
    [cell loadWebViewWithContents:choice_title];
    
    NSString *is_correct = [self stringValue: data[@"is_correct"] ];
    [cell.radioButton setImage:[UIImage imageNamed:@"gray_empty_button.png"] forState:UIControlStateNormal];
    if ([is_correct isEqualToString:@"1"] || [is_correct isEqualToString:@"100"] || ![is_correct isEqualToString:@"0"]) {
        [cell.radioButton setImage:[UIImage imageNamed:@"green_check_button.png"] forState:UIControlStateNormal];
    }
}

- (void)configureImageRadioButtonWithMathJaxCell:(QuestionImageRadioItemWithMathJaxCell *)cell tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    //    NSDictionary *data = self.cItems[indexPath.row];
    //    NSString *choice_title = [self stringValue: data[@"content"] ];
    //
    //    //    [cell load]
    //
    //    NSString *is_correct = [self stringValue: data[@"is_correct"] ];
    //    [cell.radioButton setImage:[UIImage imageNamed:@"vibe_radio_button_checked2_48px"] forState:UIControlStateNormal];
    //    if (![is_correct isEqualToString:@"0"]) {
    //        [cell.radioButton setImage:[UIImage imageNamed:@"vibe_radio_button_checked_48px"] forState:UIControlStateNormal];
    //    }
    
    
    NSDictionary *data = self.cItems[indexPath.row];
    NSString *choice_title = [self stringValue: data[@"content"] ];
    NSString *choice_image = [self stringValue: data[@"choice_image"] ];
    NSData *choice_image_data = [NSData dataWithData:data[@"choice_image_data"] ];
    NSIndexPath *indexPathCheck = self.indexPath;
    
    cell.delegate = self;
    cell.indexPath = indexPath;
    [cell loadWebViewWithContents:choice_title];
    
    NSString *is_correct = [self stringValue: data[@"is_correct"] ];
    [cell.radioButton setImage:[UIImage imageNamed:@"gray_empty_button.png"] forState:UIControlStateNormal];
    if ([is_correct isEqualToString:@"1"] || [is_correct isEqualToString:@"100"] || ![is_correct isEqualToString:@"0"]) {
        [cell.radioButton setImage:[UIImage imageNamed:@"green_check_button.png"] forState:UIControlStateNormal];
    }
    
    [cell.choiceImageButton addTarget:self action:@selector(choiceImageTapAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //    cell.buttonTitle.text = choice_title;
    NSURL *imageURL = [NSURL URLWithString:choice_image ];
    
    NSLog(@"CHOICE IMAGE [%@]", choice_image);
    
    if (indexPathCheck == self.indexPath) {
        
        
        if ([[imageURL scheme] isEqualToString:@"assets-library"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.choiceImage.image = [UIImage imageWithData:choice_image_data];
            });
        } else {
            [cell.choiceImage sd_setImageWithURL:imageURL];
        }
        //        [cell.choiceImage sd_setImageWithURL:imageURL];
        
//        NSDictionary *choiceDict = @{
//                                     @"choice_title":choice_title,
//                                     @"data":choice_image,
//                                     @"image_data":choice_image_data};
//        
////        if (cell.choiceImageButton.tag == 0) {
////            cell.choiceImageButton.tag = self.choiceImageSet.count + 1;
////            NSLog(@"CHOICE TAG [%ld]", cell.choiceImageButton.tag);
////        }
//        if (![self.choiceImageSet containsObject:choiceDict]) {
//            NSLog(@"Pasok add object");
//            [self.choiceImageSet addObject:choiceDict];
//        }
//        
//        if ([self.choiceImageSet containsObject:choiceDict]) {
//            NSInteger index = [self.choiceImageSet indexOfObject:choiceDict];
//            index += 1;
//            cell.choiceImageButton.tag = index;
//        }
        
        NSDictionary *choiceDict = @{
                                     @"choice_title":choice_title,
                                     @"data":choice_image,
                                     @"image_data":choice_image_data};
        
        if ([self.choiceImageSet containsObject:choiceDict]) {
            NSInteger index = [self.choiceImageSet indexOfObject:choiceDict];
            index += 1;
            cell.choiceImageButton.tag = index;
        }
        
    }
}

- (void)didFinishloading:(NSIndexPath *)indexPath {
    //    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.table reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (void)didFinishloadingChoice:(NSIndexPath *)indexPath {
    //    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.choicesTable reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (void)configurePictureCell:(QuestionPictureItemCell *)cell tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    NSDictionary *data = self.qItems[indexPath.row];
    NSOrderedSet *imageArray = [NSOrderedSet orderedSetWithOrderedSet:data[@"content"] ];
    NSInteger arrayCount = imageArray.count;
    
    [self setUpImageCell:cell count:arrayCount];
    [self setImage:cell array:imageArray];
}

- (void)configureRadioButtonCell:(QuestionRadioItemCell *)cell tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    NSDictionary *data = self.cItems[indexPath.row];
    NSString *choice_title = [self stringValue: data[@"content"] ];
    cell.buttonTitle.text = choice_title;
    
    NSString *is_correct = [self stringValue: data[@"is_correct"] ];
    [cell.radioButton setImage:[UIImage imageNamed:@"gray_empty_button.png"] forState:UIControlStateNormal];
    if ([is_correct isEqualToString:@"1"] || [is_correct isEqualToString:@"100"] || ![is_correct isEqualToString:@"0"]) {
        [cell.radioButton setImage:[UIImage imageNamed:@"green_check_button.png"] forState:UIControlStateNormal];
    }
}

- (void)configureImageRadioButtonCell:(QuestionImageRadioItemCell *)cell tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    NSDictionary *data = self.cItems[indexPath.row];
    NSString *choice_title = [self stringValue: data[@"content"] ];
    NSString *choice_image = [self stringValue: data[@"choice_image"] ];
    NSData *choice_image_data = [NSData dataWithData:data[@"choice_image_data"] ];
    NSIndexPath *indexPathCheck = self.indexPath;
    
    NSString *is_correct = [self stringValue: data[@"is_correct"] ];
    [cell.radioButton setImage:[UIImage imageNamed:@"gray_empty_button.png"] forState:UIControlStateNormal];
    if ([is_correct isEqualToString:@"1"] || [is_correct isEqualToString:@"100"] || ![is_correct isEqualToString:@"0"]) {
        [cell.radioButton setImage:[UIImage imageNamed:@"green_check_button.png"] forState:UIControlStateNormal];
    }
    
    [cell.choiceImageButton addTarget:self action:@selector(choiceImageTapAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.buttonTitle.text = choice_title;
    NSURL *imageURL = [NSURL URLWithString:choice_image ];
    
    NSLog(@"CHOICE IMAGE [%@]", choice_image);
    
    if (indexPathCheck == self.indexPath) {
        
        
        if ([[imageURL scheme] isEqualToString:@"assets-library"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.choiceImage.image = [UIImage imageWithData:choice_image_data];
            });
        } else {
            [cell.choiceImage sd_setImageWithURL:imageURL];
        }
        
        NSDictionary *choiceDict = @{
                                     @"choice_title":choice_title,
                                     @"data":choice_image,
                                     @"image_data":choice_image_data};
        
        if ([self.choiceImageSet containsObject:choiceDict]) {
            NSInteger index = [self.choiceImageSet indexOfObject:choiceDict];
            index += 1;
            cell.choiceImageButton.tag = index;
        }
        
//        NSDictionary *choiceDict = @{
//                                     @"choice_title":choice_title,
//                                     @"data":choice_image,
//                                     @"image_data":choice_image_data};
//        
////        if (cell.choiceImageButton.tag == 0) {
////            cell.choiceImageButton.tag = self.choiceImageSet.count + 1;
////            NSLog(@"CHOICE TAG [%ld]", cell.choiceImageButton.tag);
////        }
//        if (![self.choiceImageSet containsObject:choiceDict]) {
//            NSLog(@"Pasok add object");
//            [self.choiceImageSet addObject:choiceDict];
//        }
//        
//        if ([self.choiceImageSet containsObject:choiceDict]) {
//            NSInteger index = [self.choiceImageSet indexOfObject:choiceDict];
//            index += 1;
//            cell.choiceImageButton.tag = index;
//        }
    }
}

- (void)configureTextAreaCell:(QuestionTextAreaItemCell *)cell tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
//    cell.textArea.text = NSLocalizedString(@"Type your answer here...", nil);
    NSDictionary *data = self.cItems[indexPath.row];
    cell.textArea.text = [self stringValue: data[@"text_area_content"]];
}

- (void)setImage:(QuestionPictureItemCell *)cell array:(NSOrderedSet *)imageArray {
    
    BOOL oneHasImage = NO;
    BOOL twoHasImage = NO;
    BOOL threeHasImage = NO;
    
    NSIndexPath *indexPathCheck = self.indexPath;
    NSInteger imageArrayCount = imageArray.count;
    for (NSDictionary *imageDict in imageArray) {
        
        UIImageView *questionImageView = cell.qImageOne; // default image view
        
        NSString *imageIndex = [NSString stringWithFormat:@"%@", imageDict[@"index"]];
        
        NSString *imageURLString = [NSString stringWithFormat:@"%@", imageDict[@"data"]];
        NSURL *imageURL = [NSURL URLWithString:imageURLString];
        
        if (indexPathCheck == self.indexPath) {
            NSLog(@"IMAGE URL STRING [%@]", imageDict[@"data"]);
            
            if (imageArrayCount == 1) {
                questionImageView = cell.qImageOne;
                [cell.activityIndicator stopAnimating];
            }
            
            if (imageArrayCount == 2) {
                if (oneHasImage == NO) {
                    questionImageView = cell.qImageOne;
                    oneHasImage = YES;
                } else {
                    questionImageView = cell.qImageTwo;
                    [cell.activityIndicator stopAnimating];
                }
            }
            
            if (imageArrayCount == 3) {
                if ((twoHasImage) && (threeHasImage == NO)) {
                    questionImageView = cell.qImageThree;
                    [cell.activityIndicator stopAnimating];
                    threeHasImage = YES;
                }
                
                if ((oneHasImage) && (twoHasImage == NO)) {
                    questionImageView = cell.qImageTwo;
                    twoHasImage = YES;
                }
                
                if (oneHasImage == NO) {
                    questionImageView = cell.qImageOne;
                    oneHasImage = YES;
                }
                
            }
            
            if (imageArrayCount == 4) {
                if ([imageIndex isEqualToString:@"1"]) {
                    questionImageView = cell.qImageOne;
                }
                
                if ([imageIndex isEqualToString:@"2"]) {
                    questionImageView = cell.qImageTwo;
                }
                
                if ([imageIndex isEqualToString:@"3"]) {
                    questionImageView = cell.qImageThree;
                }
                
                if ([imageIndex isEqualToString:@"4"]) {
                    questionImageView = cell.qImageFour;
                    [cell.activityIndicator stopAnimating];
                }
            }
            
            if ([[imageURL scheme] isEqualToString:@"assets-library"]) {
                NSData *image_data = [NSData dataWithData:imageDict[@"image_data"]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    questionImageView.image = [UIImage imageWithData:image_data];
                });
            } else {
                [questionImageView sd_setImageWithURL:imageURL];
            }
            
            
            
        }
    }
}

- (void)setUpImageCell:(QuestionPictureItemCell *)cell count:(NSInteger )count {
    NSInteger imageCount = count;
    
    BOOL oneHidden = YES;
    BOOL twoHidden = YES;
    BOOL threeHidden = YES;
    BOOL fourHidden = YES;
    
    CGFloat oneLocation = 0;
    CGFloat twoLocation = 0;
    CGFloat threeLocation = 0;
    CGFloat fourLocation = 0;
    
    if (imageCount == 1) {
        oneHidden = NO;
    }
    
    if (imageCount == 2) {
        oneHidden = NO;
        twoHidden = NO;
        oneLocation -= 65.0f;
        twoLocation += 65.0f;
    }
    
    if (imageCount == 3) {
        oneHidden = NO;
        twoHidden = NO;
        threeHidden = NO;
        oneLocation -= 130.0f;
        threeLocation += 130.0f;
    }
    
    if (imageCount == 4) {
        oneHidden = NO;
        twoHidden = NO;
        threeHidden = NO;
        fourHidden = NO;
        oneLocation -= 195.0f;
        twoLocation -= 65.0f;
        threeLocation += 65.0f;
        fourLocation += 195.0f;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.qImageOne.hidden = oneHidden;
        cell.qImageTwo.hidden = twoHidden;
        cell.qImageThree.hidden = threeHidden;
        cell.qImageFour.hidden = fourHidden;
        
        cell.buttonOne.hidden = oneHidden;
        cell.buttonTwo.hidden = twoHidden;
        cell.buttonThree.hidden = threeHidden;
        cell.buttonFour.hidden = fourHidden;
        
        cell.qImageOneConstraint.constant = oneLocation;
        cell.qImageTwoConstraint.constant = twoLocation;
        cell.qImageThreeConstraint.constant = threeLocation;
        cell.qImageFourConstraint.constant = fourLocation;
    });
    
    cell.qImageOne.image = nil;
    cell.qImageTwo.image = nil;
    cell.qImageThree.image = nil;
    cell.qImageFour.image = nil;
    
    [cell.activityIndicator startAnimating];
    
    // for preview
    cell.buttonOne.tag = 1;
    cell.buttonTwo.tag = 2;
    cell.buttonThree.tag = 3;
    cell.buttonFour.tag = 4;
    
    [cell.buttonOne addTarget:self action:@selector(questionImageTapAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonTwo addTarget:self action:@selector(questionImageTapAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonThree addTarget:self action:@selector(questionImageTapAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonFour addTarget:self action:@selector(questionImageTapAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)questionImageTapAction:(UIButton *)sender {
    __weak typeof(self) wo = self;
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.table];
    NSIndexPath *indexPath = [self.table indexPathForRowAtPoint:buttonPosition];
    
    NSMutableDictionary *previewData = [NSMutableDictionary dictionary];
    NSDictionary *data = self.qItems[indexPath.row];
    NSOrderedSet *contentSet = data[@"content"];
    
    [previewData setObject:contentSet forKey:@"content"];
    
    if ([(NSObject*)wo.delegate respondsToSelector:@selector(transitionWithData:index:segueID:)]) {
        [wo.delegate transitionWithData:previewData index:sender.tag segueID:@"SAMPLE_QUESTION_IMAGE_VIEW"];
    }
}

- (void)choiceImageTapAction:(UIButton *)sender {
    __weak typeof(self) wo = self;
    NSMutableDictionary *previewData = [NSMutableDictionary dictionary];
    NSOrderedSet *contentSet = [NSOrderedSet orderedSetWithOrderedSet:self.choiceImageSet];
    
    [previewData setObject:contentSet forKey:@"content"];
    
    if ([(NSObject*)wo.delegate respondsToSelector:@selector(transitionWithData:index:segueID:)]) {
        [wo.delegate transitionWithData:previewData index:sender.tag segueID:@"SAMPLE_QUESTION_IMAGE_VIEW"];
    }
}

@end
