//
//  DeployedTestPreview.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/28/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeployedTestPreview : UIViewController

@property (nonatomic, strong) NSManagedObject *quiz_mo;

@end
