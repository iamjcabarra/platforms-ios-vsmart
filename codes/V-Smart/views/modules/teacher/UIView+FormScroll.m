//
//  UIView+FormScroll.m
//  V-Smart
//
//  Created by Julius Abarra on 25/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "UIView+FormScroll.h"

@implementation UIView (FormScroll)

- (void)scrollToPointY:(float)y {
    [UIView beginAnimations:@"registerScroll" context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.4];
    self.transform = CGAffineTransformMakeTranslation(0, y);
    [UIView commitAnimations];
}

- (void)scrollToView:(UIView *)view {
    CGRect theFrame = view.frame;
    float y = theFrame.origin.y - 15;
    y -= (y / 1.7);
    [self scrollToPointY:-y];
}


- (void)scrollElement:(UIView *)view toPointY:(float)y {
    CGRect theFrame = view.frame;
    float yOrig = theFrame.origin.y;
    float diff = y - yOrig;
    
    if (diff < 0) {
        [self scrollToPointY:diff];
    }
    else {
        [self scrollToPointY:0];
    }
}

@end
