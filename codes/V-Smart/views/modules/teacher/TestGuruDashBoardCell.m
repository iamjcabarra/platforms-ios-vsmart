//
//  TestGuruDashBoardCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 7/20/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruDashBoardCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation TestGuruDashBoardCell

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        // change to our custom selected background view
        UIView *bgv = [[UIView alloc] initWithFrame:self.frame];
        bgv.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0];
        bgv.layer.cornerRadius = 5.0f;
        
        self.selectedBackgroundView = bgv;
    }
    return self;
}

@end
