//
//  QuestionGroupTableViewCell.m
//  V-Smart
//
//  Created by Carmelito Bayarcal on 21/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "QuestionGroupTableViewCell.h"
#import "TestGuruQuestionItemCell.h"
#import "TestGuruQuestionGroupView.h"
#import "HUD.h"
#import "UIImageView+WebCache.h"

@interface NSString (HtmlCustomCheck)
- (BOOL)containsHTML;
@end

@implementation QuestionGroupTableViewCell


- (BOOL)containsHTML {
    
    NSString *stringcopy = [self copy];
    
    NSString *expression = @"<[^>]+>";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:stringcopy
                                                        options:0
                                                          range:NSMakeRange(0, [stringcopy length])];
    if (numberOfMatches == 1) {
        return YES;
    }
    
    return NO; //default value
}

static NSString *kQuestionTypeCellIdentifier = @"question_cell_identifier";

- (void)awakeFromNib {
    // Initialization code
    self.tm = [TestGuruDataManager sharedInstance];
    
    self.queue = dispatch_queue_create("com.vibetech.testguru.LIST",DISPATCH_QUEUE_SERIAL);
    
    self.user_id = [self.tm loginUser];
    
    UINib *questionItemCellNib = [UINib nibWithNibName:@"TestGuruQuestionItemCell" bundle:nil];
    [self.tableView registerNib:questionItemCellNib forCellReuseIdentifier:kQuestionTypeCellIdentifier];
    
    self.formatter = [[NSDateFormatter alloc] init];
    [self.formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [self.formatter setTimeZone:[NSTimeZone localTimeZone]];
    
    self.dateToStringFormatter = [[NSDateFormatter alloc] init];
    [self.dateToStringFormatter setDateFormat:@"EEE, MMM dd, yyyy hh:mm aa"];
    [self.dateToStringFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
//    [self.loadMoreButton addTarget:self action:@selector(loadMore:) forControlEvents:UIControlEventTouchUpInside];
}

//- (void)hasLoadMore:(BOOL)isLoadMoreEnabled {
//    CGRect tableFooterFrame;
//    
//    if (isLoadMoreEnabled) {
//        tableFooterFrame.size.height = 61;
//    } else {
//        tableFooterFrame.size.height = 0;
//    }
//    
//    __weak typeof(self) wo = self;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self.footerContainer setFrame:tableFooterFrame];
//        wo.loadMoreButton.hidden = NO;
//    });
//}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//    
//    // Configure the view for the selected state
//}


//- (void)loadMore:(UIButton *)sender {
//    NSLog(@"%s -----> ", __PRETTY_FUNCTION__);
//    
//    [self loadNextPage];
//}

//- (void)loadNextPage {
//    NSString *nextPage = [NSString stringWithFormat:@"%ld", (long)self.current_page + 1];
//    
//    if (self.total_pages > self.current_page) {
//        self.loadMoreButton.enabled = NO;
//        
//        NSString *groupBy = [NSString stringWithFormat:@"%@", self.groupByType];
//        
//        NSString *indicatorStr = NSLocalizedString(@"Loading", nil);
//        
//        [HUD showUIBlockingIndicatorWithText:indicatorStr];
//        
//        dispatch_queue_t queue = dispatch_queue_create("com.vibetech.groupby.LOADMORE",DISPATCH_QUEUE_SERIAL);
//        dispatch_group_t group = dispatch_group_create();
//        
//        if ([groupBy isEqualToString:@"TAGS"]) {
//            dispatch_group_enter(group);
//            dispatch_group_async(group, queue, ^{
//                [self.tm requestQuestionWithTagID:self.section_id withPackageID:self.package_type_id withPage:nextPage dataBlock:^(NSDictionary *data) {
//                    self.final_data = data;
//                    dispatch_group_leave(group);
//                }];
//            });
//            
//        } else if ([groupBy isEqualToString:@"QUESTION_TYPES"]) {
//            dispatch_group_enter(group);
//            dispatch_group_async(group, queue, ^{
//                [self.tm requestQuestionWithQuestionTypeID:self.section_id withPackageID:self.package_type_id withPage:nextPage dataBlock:^(NSDictionary *data) {
//                    self.final_data = data;
//                    dispatch_group_leave(group);
//                }];
//            });
//        } else if ([groupBy isEqualToString:@"DIFFICULTY_LEVELS"]) {
//            dispatch_group_enter(group);
//            dispatch_group_async(group, queue, ^{
//                [self.tm requestQuestionWithDifficultyLevelID:self.section_id withPackageID:self.package_type_id withPage:nextPage dataBlock:^(NSDictionary *data) {
//                    self.final_data = data;
//                    dispatch_group_leave(group);
//                }];
//            });
//        } else if ([groupBy isEqualToString:@"LEARNING_SKILLS"]) {
//            dispatch_group_enter(group);
//            dispatch_group_async(group, queue, ^{
//                [self.tm requestQuestionWithLearningSkillID:self.section_id withPackageID:self.package_type_id withPage:nextPage dataBlock:^(NSDictionary *data) {
//                    self.final_data = data;
//                    dispatch_group_leave(group);
//                }];
//            });
//        } else if ([groupBy isEqualToString:@"SHARED_STATUS"]) {
//            dispatch_group_enter(group);
//            dispatch_group_async(group, queue, ^{
//                [self.tm requestQuestionWithSharedStatusID:self.section_id withPackageID:self.package_type_id withPage:nextPage dataBlock:^(NSDictionary *data) {
//                    self.final_data = data;
//                    dispatch_group_leave(group);
//                }];
//            });
//        }
//        
//        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
//            if (self.final_data != nil) {
//                self.loadMoreButton.enabled = YES;
//                self.total_filtered = [self.final_data[@"total_filtered"] integerValue];
//                self.total_pages = [self.final_data[@"total_pages"] integerValue];
//                self.current_page = [self.final_data[@"current_page"] integerValue];
//                [self checkLoadMore];
//            }
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [HUD hideUIBlockingIndicator];
//            });
//        });
//    }
//}

//- (void)loadNextPage {
//    NSString *nextPage = [NSString stringWithFormat:@"%ld", (long)self.current_page + 1];
//    
//    if (self.total_pages > self.current_page) {
//        self.loadMoreButton.enabled = NO;
//        
//        NSString *groupBy = [NSString stringWithFormat:@"%@", self.groupByType];
//        NSString *indicatorStr = NSLocalizedString(@"Loading", nil);
//        
//        NSString *linkType = @"";
//        [HUD showUIBlockingIndicatorWithText:indicatorStr];
//        
//            //types : tag,difficultylevel,learningskill,type
//        
//        if ([groupBy isEqualToString:@"TAGS"]) {
//            
//            linkType = @"tag";
//            
//        } else if ([groupBy isEqualToString:@"QUESTION_TYPES"]) {
//            
//            linkType = @"type";
//            
//        } else if ([groupBy isEqualToString:@"DIFFICULTY_LEVELS"]) {
//            
//            linkType = @"difficultylevel";
//            
//        } else if ([groupBy isEqualToString:@"LEARNING_SKILLS"]) {
//            
//            linkType = @"learningskill";
//            
//        } else if ([groupBy isEqualToString:@"COMPETENCY_CODES"]) {
//            
//            linkType = @"competencycode";
//            
//        }
//        
//        dispatch_queue_t queue = dispatch_queue_create("com.vibetech.groupby.LOADMORE",DISPATCH_QUEUE_SERIAL);
//        dispatch_group_t group = dispatch_group_create();
//        
//        dispatch_group_enter(group);
//        dispatch_group_async(group, queue, ^{
//            [self.tm requestQuestionForLinkType:linkType withID:self.section_id withPackageID:self.package_type_id withCourseID:self.course_id withPage:nextPage withLimit:@"50" dataBlock:^(NSDictionary *data) {
//                    self.final_data = data;
//                    dispatch_group_leave(group);
//            }];
//        });
//        
//        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
//            if (self.final_data != nil) {
//                self.loadMoreButton.enabled = YES;
//                self.total_filtered = [self.final_data[@"total_filtered"] integerValue];
//                self.total_pages = [self.final_data[@"total_pages"] integerValue];
//                self.current_page = [self.final_data[@"current_page"] integerValue];
//                [self checkLoadMore];
//            }
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [HUD hideUIBlockingIndicator];
//            });
//        });
//    }
//}

//- (void)checkLoadMore {
//    CGRect tableFooterFrame;
//    
//    if (self.total_pages > self.current_page) {
//        tableFooterFrame.size.height = 61;
//    } else {
//        tableFooterFrame.size.height = 0;
//    }
//    
//    __weak typeof(self) wo = self;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self.footerContainer setFrame:tableFooterFrame];
//        wo.loadMoreButton.hidden = NO;
//    });
//}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    
    self.total_filtered = count;
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.tableView = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kQuestionTypeCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TestGuruQuestionItemCell *questionCell = (TestGuruQuestionItemCell *)cell;
    [self configureQuestionCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(TestGuruQuestionItemCell *)cell managedObject:(NSManagedObject *)mo
            objectAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *name = [mo valueForKey:@"name"];
    NSString *formatted_date_modified = [mo valueForKey:@"formatted_date_modified"];
    
    NSString *question_text = [mo valueForKey:@"question_text"];
    NSString *questionTypeName = [mo valueForKey:@"questionTypeName"];
    NSString *is_public = [mo valueForKey:@"is_public"];
    NSString *proficiency_level_id = [mo valueForKey:@"proficiency_level_id"];
    NSString *proficiency_name = [mo valueForKey:@"proficiencyLevelName"];
    NSString *question_type_icon = [mo valueForKey:@"question_type_icon"];
    
    NSString *package_type_id = [mo valueForKey:@"package_type_id"];
    
    cell.questionImage.image = nil;
    
    NSString *firstImage = [self getFirstImage:mo];
    if (firstImage.length > 0) {
        [cell.questionImage sd_setImageWithURL:[NSURL URLWithString:firstImage] ];
    }
    
    cell.contentView.backgroundColor = (indexPath.row%2) ? [UIColor groupTableViewBackgroundColor] : [UIColor whiteColor];
    
    cell.questionTitleLabel.text = name;
    cell.dateLabel.text = formatted_date_modified;
    cell.questionTextLabel.text = question_text;
    cell.questionTextLabel.hidden = YES;
    
    // FILL IN THE BLANKS
    NSString *question_type_id = [mo valueForKey:@"question_type_id"];
    if ([question_type_id isEqualToString:@"2"]) {
        question_text = [question_text stringByReplacingOccurrencesOfString:@"-blank-" withString:@"_______________"];
    }
    
    if ([question_text containsHTML]) {
        NSData *htmlData = [question_text dataUsingEncoding:NSUnicodeStringEncoding];
        NSDictionary *options = @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } ;
        NSAttributedString *attributed_string = [[NSAttributedString alloc] initWithData:htmlData
                                                                                 options:options
                                                                      documentAttributes:nil
                                                                                   error:nil];
        cell.questionTextLabel.attributedText = attributed_string;
    }
    
    [cell loadWebViewWithContents:question_text];
    
    cell.questionTypeLabel.text = [questionTypeName uppercaseString];
    cell.questionDifficultyLabel.text = proficiency_name;
    
    cell.editButton.hidden = YES;
    [cell.editButton addTarget:self action:@selector(editButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.editButton.hidden = ([package_type_id isEqualToString:@"1"]);
    
    [cell setDifficultyLevelID:proficiency_level_id];
    [cell setQuestionTypeIcon:question_type_icon];
    [cell setPublic:is_public];
}
- (NSString *)getFirstImage:(NSManagedObject *)mo {
    
    if (mo != nil) {
        NSString *image_one = [mo valueForKey:@"image_one"];
        if (image_one.length > 0) {
            return image_one;
        }
        
        NSString *image_two = [mo valueForKey:@"image_two"];
        if (image_two.length > 0) {
            return image_two;
        }
        
        NSString *image_three = [mo valueForKey:@"image_three"];
        if (image_three.length > 0) {
            return image_three;
        }
        
        NSString *image_four = [mo valueForKey:@"image_four"];
        if (image_four.length > 0) {
            return image_four;
        } else {
            return @"";
        }
    } else {
        return @"";
    }
}

- (void)editButtonAction:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    self.editOperation = YES;
    __weak typeof(self) wo = self;
    [self.tm deepCopyManagedObject:mo objectBlock:^(NSManagedObject *object) {
        if ([(NSObject*)wo.delegate respondsToSelector:@selector(transitionWithObject:withSegueIdentifier:withIndexPath:)]) {
            [wo.delegate transitionWithObject:object withSegueIdentifier:@"SHOW_EDIT_QUESTION_DETAILS_V22" withIndexPath:indexPath];
        }
    }];
    
//    [self.tm deepCopyManagedObject:mo doneBlock:^(BOOL status) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if ([(NSObject*)wo.delegate respondsToSelector:@selector(transitionWithQuestionID:edit:withSegueIdentifier:withIndexPath:)]) {
//                [wo.delegate transitionWithQuestionID:question_id edit:YES withSegueIdentifier:@"SHOW_EDIT_QUESTION_DETAILS_V22" withIndexPath:indexPath];
//            }
//        });
//    }];
}

- (void)deleteButtonAction:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self initiateDelete:mo];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([(NSObject*)self.delegate respondsToSelector:@selector(transitionWithObject:withSegueIdentifier:withIndexPath:)]) {
            [self.delegate transitionWithObject:nil withSegueIdentifier:@"SAMPLE_QUESTION_VIEW" withIndexPath:indexPath];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID] isEqualToString:@"1"]) {
        return NO;
    }
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];

        [self initiateDelete:mo];
    }
}

//- (void)initiateDelete:(NSManagedObject *)mo {
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete", nil)
//                                                                             message:NSLocalizedString(@"Are you sure you want to delete", nil)
//                                                                      preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction *yesAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
//                                                             style:UIAlertActionStyleCancel
//                                                           handler:^(UIAlertAction * _Nonnull action) {
//                                                               
//                                                               [self.tm requestRemoveQuestion:mo doneBlock:^(BOOL status) {
//                                                                   if (self.total_filtered == 1) {
//                                                                       if ([(NSObject*)self.delegate respondsToSelector:@selector(performReload:)]) {
//                                                                           [self performRequestGroupByFolder];
//                                                                       }
//                                                                   }
//                                                               }];
//                                                           }];
//    
//    UIAlertAction *noAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
//                                                            style:UIAlertActionStyleDefault
//                                                          handler:nil];
//    [alertController addAction:yesAlertAction];
//    [alertController addAction:noAlertAction];
//    
//    [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
//}

- (void)initiateDelete:(NSManagedObject *)mo {
    NSInteger numberOfRows = self.total_filtered;
    
    NSString *questionTitle = [NSString stringWithFormat:@"%@", [mo valueForKey:@"name"] ];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete", nil)
                                                                             message:NSLocalizedString(@"Are you sure you want to delete this item?", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               [self.tm requestRemoveQuestion:mo doneBlock:^(BOOL status) {
                                                                   NSString *message = (status) ? NSLocalizedString(@"Successfully deleted", nil) : NSLocalizedString(@"Failed to deleted", nil);
                                                                   
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       [self showSuccessDelete:message withQuestionTitle:questionTitle];
                                                                   });
                                                                   
                                                                   if (status) {
                                                                       NSLog(@"SELF.TOTAL FILTERED [%ld]", numberOfRows);
                                                                       if (numberOfRows == 1) {
                                                                           if ([(NSObject*)self.delegate respondsToSelector:@selector(performReload)]) {
                                                                               [self.delegate performReload];
                                                                           }
                                                                       } else {
                                                                           if ([(NSObject*)self.delegate respondsToSelector:@selector(performUpdateLabel)]) {
                                                                               [self.delegate performUpdateLabel];
                                                                           }
                                                                       }
                                                                   }
                                                               }];
                                                           }];
    
    UIAlertAction *noAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:nil];
    [alertController addAction:yesAlertAction];
    [alertController addAction:noAlertAction];
    
    [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
}

- (void)showSuccessDelete:(NSString *)message withQuestionTitle:(NSString *)question_title {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:[NSString stringWithFormat:@"%@ %@", message, question_title]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Okay", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
    [alertController addAction:cancelAction];
    
    [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    BOOL isAscending = NO;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionBankEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"sort_date" ascending:isAscending];
    NSSortDescriptor *question_text = [NSSortDescriptor sortDescriptorWithKey:@"question_text" ascending:isAscending];
    NSSortDescriptor *question_id = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:isAscending];
    [fetchRequest setSortDescriptors:@[date_modified, question_text, question_id]];
    
    // Edit the section name key path and cache name if appropriate.
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            //            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void)downloadImageForObject:(NSManagedObject *)mo indexPath:(NSIndexPath *)indexPath {
    
    if (self.activeDownloads == nil) {
        self.activeDownloads = [NSMutableDictionary dictionary];
    }
    
    NSManagedObject *mo_in_progress = [self.activeDownloads objectForKey:indexPath];
    if (mo_in_progress == nil) {
        
        [self.activeDownloads setObject:mo forKey:indexPath];
        
        NSData *image_data = [NSData dataWithData:[mo valueForKey:@"image_data"]] ;
        
        
        __weak typeof(self) wo = self;
        
        if (image_data == nil) {
            dispatch_async(self.queue, ^{
                [wo.tm downloadQuestionImageForObject:mo doneBlock:^(BOOL status) {
                    if (status) {
                        [wo.activeDownloads removeObjectForKey:indexPath];
                    }
                }];
            });
        } else {
            [self.activeDownloads removeObjectForKey:indexPath];
        }
        
        //        dispatch_async(self.queue, ^{
        //            [wo.tm downloadQuestionImageForObject:mo doneBlock:^(BOOL status) {
        //                if (status) {
        //                    [wo.activeDownloads removeObjectForKey:indexPath];
        //                }
        //            }];
        //        });
    }
}

@end
