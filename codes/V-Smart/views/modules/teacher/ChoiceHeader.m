//
//  ChoiceHeader.m
//  V-Smart
//
//  Created by Ryan Migallos on 8/4/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "ChoiceHeader.h"

@interface ChoiceHeader()

@property (strong, nonatomic) IBOutlet UILabel *choiceLabel;

@end


@implementation ChoiceHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-  (instancetype)initWithFrame:(CGRect)rect {
    
    self = [super initWithFrame:rect];
    if (self) {
        self.choiceLabel.text = NSLocalizedString(@"Choice", nil);
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChoiceHeader" owner:self options:nil];
        [nib[0] setFrame:rect];
        self = nib[0];
    }
    
    return self;
}

@end
