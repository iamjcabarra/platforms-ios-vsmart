//
//  TrueFalseItemTableViewCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 8/4/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "TrueFalseItemTableViewCell.h"

@implementation TrueFalseItemTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
