//
//  TestGuruDateMenu.m
//  V-Smart
//
//  Created by Ryan Migallos on 9/14/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruDateMenu.h"

@interface TestGuruDateMenu ()

@property (strong, nonatomic) IBOutlet UILabel *labelDateValue;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePickerObject;
@property (strong, nonatomic) IBOutlet UIButton *appleButton;
@property (strong, nonatomic) NSDateFormatter *formatter;

@end

@implementation TestGuruDateMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.formatter = [[NSDateFormatter alloc] init];
    
    NSLog(@"type : %@", self.type);
    NSLog(@"value : %@", self.value);
    [self.datePickerObject addTarget:self action:@selector(valueChangeForPickerView:) forControlEvents:UIControlEventAllEvents];
    [self.appleButton addTarget:self action:@selector(applyButtonAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (NSDate *)dateFromString:(NSString *)string format:(NSString *)pattern {
    self.formatter.dateFormat = pattern;
    NSDate *date = [self.formatter dateFromString:string];
    
    NSLog(@"date : %@", [date description]);
    
    return date;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([self.type isEqualToString:kOpeningType] || [self.type isEqualToString:kClosingType]) {
        //define picker date mode

        self.datePickerObject.datePickerMode = UIDatePickerModeDateAndTime;
        [self.datePickerObject setDate:self.value animated:YES];
        NSDate *date_object = self.datePickerObject.date;
        self.labelDateValue.text = [self dateStringObjectFromDate:date_object];
    }
    
    if ([self.type isEqualToString:kTimeLimitType]) {
        //define picker countdown mode
        self.datePickerObject.datePickerMode = UIDatePickerModeCountDownTimer;
        [self.datePickerObject setCountDownDuration:[self countDownValueFromDate:self.value]];
        NSTimeInterval ti = (NSTimeInterval)self.datePickerObject.countDownDuration;
        self.labelDateValue.text = [self stringFromTimeInterval:ti];
    }
}

- (void)valueChangeForPickerView:(UIDatePicker *)picker {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);

    __weak typeof(self) wo = self;
    UIDatePickerMode mode = picker.datePickerMode;
    if (mode == UIDatePickerModeDateAndTime) {
        wo.labelDateValue.text = [self dateStringObjectFromDate:picker.date];
    }
    
    if (mode == UIDatePickerModeCountDownTimer) {
        NSTimeInterval ti = (NSTimeInterval)picker.countDownDuration;
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.labelDateValue.text = [self stringFromTimeInterval:ti];
        });
    }
}

- (void)applyButtonAction:(UIButton *)button {
    
    NSString *defaultString = @"<empty>";
    
    UIDatePickerMode mode = self.datePickerObject.datePickerMode;
    if (mode == UIDatePickerModeDateAndTime) {
        defaultString = [self stringFromDateObject:self.datePickerObject.date];
    }
    
    if (mode == UIDatePickerModeCountDownTimer) {
        NSTimeInterval ti = (NSTimeInterval)self.datePickerObject.countDownDuration;
        defaultString = [self stringFromTimeInterval:ti];
    }
    
    NSDictionary *data = @{@"time":defaultString,
                           @"type":self.type};
    
    if ( [(NSObject*)self.delegate respondsToSelector:@selector(selectedDateForType:withObject:)] ) {
        [self.delegate selectedDateForType:self.type withObject:data];
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (NSString *)stringFromDateObject:(NSDate *)date {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSString *template = @"yyyy-MM-dd HH:mm:ss";
    [formatter setDateFormat:template];
    return [formatter stringFromDate:date];
}

- (NSInteger)countDownValueFromDate:(NSDate *)date {
    
    NSInteger seconds = 60;
    
    if (date) {
        // Split up the date components
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSCalendarUnit components = NSCalendarUnitHour | NSCalendarUnitMinute;
        NSDateComponents *time = [calendar components:components fromDate:date];
        seconds = ([time hour] * 60 * 60) + ([time minute] * 60);
    }
    
    return seconds;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSString *)dateStringObjectFromDate:(NSDate *)date {
    
    return [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle];
}

@end
