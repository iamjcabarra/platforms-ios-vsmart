//
//  TestGuruDashboardStats.h
//  V-Smart
//
//  Created by Ryan Migallos on 21/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruDashboardStats : UICollectionReusableView

#pragma mark - COUNTER LABELS

// QUESTION
@property (strong, nonatomic) IBOutlet UIImageView *questionImageView;
@property (strong, nonatomic) IBOutlet UILabel *questionTitle;
@property (strong, nonatomic) IBOutlet UILabel *questionCount;

// TESTS
@property (strong, nonatomic) IBOutlet UIImageView *testImageView;
@property (strong, nonatomic) IBOutlet UILabel *testTitle;
@property (strong, nonatomic) IBOutlet UILabel *testCount;

// DEPLOYED
@property (strong, nonatomic) IBOutlet UIImageView *deployImageView;
@property (strong, nonatomic) IBOutlet UILabel *deployTitle;
@property (strong, nonatomic) IBOutlet UILabel *deployCount;

@property (strong, nonatomic) IBOutlet UILabel *sectionLabel;

@end
