//
//  TBTestAvailableQuestionsView.m
//  V-Smart
//
//  Created by Julius Abarra on 03/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TBTestAvailableQuestionsView.h"
#import "TBAvailableQuestionItemCell.h"
#import "TBClassHelper.h"
#import "TBContentManager.h"
#import "TestGuruDataManager.h"
#import "UIImageView+WebCache.h"

////////////// NSSTRING HTML CATEGORY IMPLEMENTATION //////////////

@interface NSString (HtmlCustomCheck)
- (BOOL)containsHTML;
@end

@implementation NSString (HtmlCustomCheck)

- (BOOL)containsHTML {
    NSString *stringcopy = [self copy];
    NSString *expression = @"<[^>]+>";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:stringcopy
                                                        options:0
                                                          range:NSMakeRange(0, [stringcopy length])];
    if (numberOfMatches == 1) {
        return YES;
    }
    
    return NO;
}

@end

////////////// NSSTRING HTML CATEGORY IMPLEMENTATION //////////////

@interface TBTestAvailableQuestionsView () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) TBClassHelper *classHelper;
@property (strong, nonatomic) TBContentManager *tbcm;
@property (strong, nonatomic) TestGuruDataManager *tgdm;

@property (strong, nonatomic) IBOutlet UITableView *questionTableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) NSMutableSet *selectedQuestions;
@property (strong, nonatomic) NSMutableSet *selectedIndexPaths;

@end

@implementation TBTestAvailableQuestionsView

static NSString *kQuestionCellIdentifer = @"questionCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Content Manager
    self.tbcm = [TBContentManager sharedInstance];
    
    // Test Guru Data Manager
    self.tgdm = [TestGuruDataManager sharedInstance];
    
    // Question Table View
    [self setUpSelectedQuestionsView];
    
    self.selectedQuestions = [NSMutableSet set];
    self.selectedIndexPaths = [NSMutableSet set];
    
    // Just Temporary
    self.title = @"Select Questions";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Available Questions View

- (void)setUpSelectedQuestionsView {
    // Set Protocols
    self.questionTableView.dataSource = self;
    self.questionTableView.delegate = self;
    
    // Allow Selection
    self.questionTableView.allowsSelection = YES;
    self.questionTableView.allowsMultipleSelection = YES;
    
    // Default Height
    self.questionTableView.rowHeight = 165.0f;
    
    // Resusable Cell
    UINib *questionItemCellNib = [UINib nibWithNibName:@"TBAvailableQuestionItemCell" bundle:nil];
    [self.questionTableView registerNib:questionItemCellNib forCellReuseIdentifier:kQuestionCellIdentifer];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //NSInteger count = [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    //NSInteger count = [sectionInfo numberOfObjects];
    return 1;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
//    return [sectionInfo name];
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.questionTableView = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kQuestionCellIdentifer forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //[self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TBAvailableQuestionItemCell *questionCell = (TBAvailableQuestionItemCell *)cell;
    [self configureQuestionCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(TBAvailableQuestionItemCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
//    NSString *name = [mo valueForKey:@"name"];
//    NSString *date_modified = [mo valueForKey:@"date_modified"];
//    NSString *date_string = [self.classHelper transformDateString:date_modified
//                                                       fromFormat:@"yyyy-MM-dd HH:mm:ss"
//                                                         toFormat:@"EEE. MMM. dd, yyyy hh:mm a"];
//    
//    NSString *question_text = [mo valueForKey:@"question_text"];
//    NSString *questionTypeName = [mo valueForKey:@"questionTypeName"];
//    NSString *question_type_id = [mo valueForKey:@"question_type_id"];
//    NSString *proficiency_level_id = [mo valueForKey:@"proficiency_level_id"];
//    NSString *proficiency_name = [mo valueForKey:@"proficiencyLevelName"];
//    
//    cell.questionImage.image = nil;
//    
//    NSString *firstImage = [self getFirstImage:mo];
//    if (firstImage.length > 0) {
//        [cell.questionImage sd_setImageWithURL:[NSURL URLWithString:firstImage] ];
//    }
//    
//    cell.questionTitleLabel.text = name;
//    cell.dateLabel.text = [date_string uppercaseString];
//    cell.questionTextLabel.text = question_text;
//    cell.questionTextLabel.hidden = YES;
//    
//    if ([question_text containsHTML]) {
//        NSData *htmlData = [question_text dataUsingEncoding:NSUnicodeStringEncoding];
//        NSDictionary *options = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType};
//        NSAttributedString *attributed_string = [[NSAttributedString alloc] initWithData:htmlData
//                                                                                 options:options
//                                                                      documentAttributes:nil
//                                                                                   error:nil];
//        cell.questionTextLabel.attributedText = attributed_string;
//    }
//    
//    [cell loadWebViewWithContents:question_text];
//    
//    cell.questionTypeLabel.text = questionTypeName;
//    cell.questionDifficultyLabel.text = proficiency_name;
//    
//    [cell updateQuestionTypeView:question_type_id];
//    [cell updateDifficultyLevelView:proficiency_level_id];
    
    // Just Temporary
    [cell showSelection:YES];
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TBAvailableQuestionItemCell *cell = (TBAvailableQuestionItemCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell showSelection:![self.selectedIndexPaths containsObject:indexPath]];
    
    //NSManagedObject *mo = self.sectionData[indexPath.row];
    NSString *question_id = @"1";
    
    if (![self.selectedQuestions containsObject:question_id]) {
        [self.selectedQuestions addObject:question_id];
    }
    
    if (![self.selectedIndexPaths containsObject:indexPath]) {
        [self.selectedIndexPaths addObject:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    TBAvailableQuestionItemCell *cell = (TBAvailableQuestionItemCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell showSelection:NO];
    
    //NSManagedObject *mo = self.sectionData[indexPath.row];
    NSString *question_id =  @"1";
    
    if ([self.selectedQuestions containsObject:question_id]) {
        [self.selectedQuestions removeObject:question_id];
    }
    
    if ([self.selectedIndexPaths containsObject:indexPath]) {
        [self.selectedIndexPaths removeObject:indexPath];
    }
}

- (NSString *)getFirstImage:(NSManagedObject *)mo {
    if (mo != nil) {
        NSString *image_one = [mo valueForKey:@"image_one"];
        if (image_one.length > 0) {
            return image_one;
        }
        
        NSString *image_two = [mo valueForKey:@"image_two"];
        if (image_two.length > 0) {
            return image_two;
        }
        
        NSString *image_three = [mo valueForKey:@"image_three"];
        if (image_three.length > 0) {
            return image_three;
        }
        
        NSString *image_four = [mo valueForKey:@"image_four"];
        if (image_four.length > 0) {
            return image_four;
        }
    }
    
    return @"";
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController {
    BOOL isAscending = NO;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tgdm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionBankEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:isAscending];
    [fetchRequest setSortDescriptors:@[date_modified]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.questionTableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.questionTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.questionTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.questionTableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.questionTableView endUpdates];
}

#pragma mark - Reloading of Results

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.questionTableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

@end
