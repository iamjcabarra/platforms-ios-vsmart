//
//  TGQBCompetencyHistoryViewController.h
//  V-Smart
//
//  Created by Ryan Migallos on 18/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGQBCompetencyHistoryDelegate <NSObject>
@optional
- (void)didFinishSelectingHistoricalData:(NSManagedObject *)object;
@end

@interface TGQBCompetencyHistoryViewController : UITableViewController
@property (weak, nonatomic) id <TGQBCompetencyHistoryDelegate> delegate;
@end
