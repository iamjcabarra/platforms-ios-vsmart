//
//  TGTBAssessmentTypePickerView.h
//  V-Smart
//
//  Created by Julius Abarra on 16/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

@protocol TGTBAssessmentTypePickerViewDelegate <NSObject>

@required
- (void)selectedAssessmentType:(NSDictionary *)assessmentTypeObject;

@end

#import <UIKit/UIKit.h>

@interface TGTBAssessmentTypePickerView : UITableViewController

@property (weak, nonatomic) id <TGTBAssessmentTypePickerViewDelegate> delegate;

@end
