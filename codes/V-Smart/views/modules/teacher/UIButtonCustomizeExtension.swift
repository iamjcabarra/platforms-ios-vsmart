//
//  UIButtonCustomizeExtension.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 14/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
import QuartzCore

extension UIButton {
    
    public func customSetTitle(_ string: String!) {
        let title = NSLocalizedString(string, comment: "")
        self.setTitle(title, for: UIControlState())
        self.setTitle(title, for: .highlighted)
    }
    
    public func customSetImage(_ string: String!) {
        self.setImage(UIImage(named: string), for: UIControlState())
        self.setImage(UIImage(named: string), for: .highlighted)
    }
    
    public func asBackButtonWithTitle(_ string: String!, color:UIColor) {
        self.customSetTitle(string)
        self.customSetImage("vibe_chevron_left_26px-01.png")
        self.imageEdgeInsets = UIEdgeInsetsMake(2, -12, 2, 10)
        self.titleEdgeInsets = UIEdgeInsetsMake(0, -30, 0, 0)
        self.tintColor = color
        self.sizeToFit()
    }
    
}

@IBDesignable
class UIButtonCustomizeExtension: UIButton {
    
    @IBInspectable var colorCode: Int = 0 {
        didSet {
            self.backgroundColor = setColor(colorCode)
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 20.0 {
        didSet {
            layer.cornerRadius = cornerRadius;
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    func setColor(_ colorCode: Int) -> UIColor {
        
        switch(colorCode) {
            
        case 1:
            return UIColor(hex6: 0xf89f35)
        case 2:
            return UIColor(hex6: 0x68bf61)
        case 3:
            return UIColor(hex6: 0xef4136)
        case 4:
            return UIColor(hex6: 0x4274b9)
        case 5:
            return UIColor(hex6: 0x939297)
        case 6:
            return UIColor(hex6: 0xFAFAFA)
        default:
            return UIColor.white
        }
    }
}
