//
//  ClassesViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 9/9/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "ClassesViewController.h"

@interface ClassesViewController ()

@end

@implementation ClassesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self _setupWebview];
    [self _setupNotifications];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
}

-(void) _setupWebview {
    float profileY = [super profileHeight];
    float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    //float gridHeight = 638 + [super profileSize].size.height;
    
    float gridY = [super headerSize].size.height + profileY;
    
    self.theWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, gridY, self.view.frame.size.width, gridHeight + 44)];
    self.theWebView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	self.theWebView.autoresizesSubviews = YES;
	self.theWebView.delegate = self;
    self.theWebView.scalesPageToFit = YES;
    
    NSString *url = VS_FMT(@"http://vsmart.vibeapi.net/class/%i/%@", [super account].user.id, [super account].user.position);
    VLog(@"URL: %@", url);
    //url = @"http://google.com";
    //VLog(@"URL: %@", url);
    NSURLRequest *webRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [self.theWebView loadRequest:webRequest];
    
    for (id subview in self.theWebView.subviews)
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            ((UIScrollView *)subview).bounces = NO;
    
    [self.view addSubview:self.theWebView];
}

#pragma mark - UIWebView Delegate
-(void)webViewDidFinishLoad:(UIWebView *)theWebView
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [SVProgressHUD dismiss];
    VLog(@"Finished Loading View");
}

-(void)webView:(UIWebView *)theWebView didFailLoadWithError:(NSError *)error
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [SVProgressHUD dismiss];
	VLog(@"Error: %@", [error localizedDescription]);
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSString *loadingLabel = NSLocalizedString(@"Loading...",nil);
    [SVProgressHUD showWithStatus:loadingLabel maskType:SVProgressHUDMaskTypeGradient];
}

-(void) _setupNotifications {
    VS_NCADD(kNotificationProfileHeight, @selector(_refreshController:))
}

- (NSString *) dashboardName {
    
    NSString *moduleName = NSLocalizedString(@"Classes", nil);
    return moduleName;
    
//    return kModuleClasses;
}

#pragma mark - Post Notification Events
-(void) _refreshController: (NSNotification *) notification {
    VLog(@"Received: kNotificationProfileHeight");
    
    [UIView animateWithDuration:0.45 animations:^{
        float profileY = [super profileHeight];
        float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
        //float gridHeight = 638 + [super profileSize].size.height;
        
        float gridY = [super headerSize].size.height + profileY;
        [self.theWebView setFrame:CGRectMake(0, gridY, self.view.frame.size.width, gridHeight + 44)];
        [self.theWebView setNeedsLayout];
        
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            [super adjustProfileHeight];
        }
        [super showOrHideMiniAvatar];
    } completion:^(BOOL finished) {
        
    }];
    
}

-(void)dealloc
{
    [self.theWebView stopLoading];
 	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.theWebView.delegate = nil;
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [SVProgressHUD dismiss];
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
