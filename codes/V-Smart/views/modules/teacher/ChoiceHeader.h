//
//  ChoiceHeader.h
//  V-Smart
//
//  Created by Ryan Migallos on 8/4/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoiceHeader : UIView

@property (strong, nonatomic) IBOutlet UIButton *addChoiceButton;

@end
