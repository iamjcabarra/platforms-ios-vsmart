//
//  TestGuruTagCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 12/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruTagCell.h"

@interface TestGuruTagCell()

@property (strong, nonatomic) IBOutlet UILabel *typeLabel;
@property (strong, nonatomic) IBOutlet UITextField *tagField;
@property (strong, nonatomic) IBOutlet UIView *customBackground;
@property (strong, nonatomic) IBOutlet UIButton *sharedButton;

@end

@implementation TestGuruTagCell

- (void)awakeFromNib {
    // Initialization code
    [self.sharedButton addTarget:self
                          action:@selector(sharedAction:)
                forControlEvents:UIControlEventTouchUpInside];
}

- (void)sharedAction:(UIButton *)button {
    BOOL flag = (button.selected) ? NO : YES;
    
    
    
}

- (void)displayData:(NSDictionary *)data {
    
    if (data != nil) {
        NSString *title_string = data[@"title"];
        NSArray *options = data[@"options"];

        for (NSDictionary *d in options) {
            
            NSMutableString *tags_string = [NSMutableString string];
            if (d[@"tags"] != nil) {
                NSArray *items = d[@"tags"];
                for (NSString *s in items) {
                    [tags_string appendString:s];
                }
                NSLog(@"tags : %@", tags_string);
                self.tagField.text = tags_string;
            }
            
            if (d[@"shared"] != nil) {
                
                NSNumber *flag = d[@"shared"];
                BOOL shared = [flag boolValue];
                
                UIColor *color = (shared) ? [UIColor darkGrayColor] : UIColorFromHex(0x0080FF);
                self.customBackground.backgroundColor = color;
                
            }
        }
        
        self.typeLabel.text = title_string;
        [self setNeedsDisplay];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
