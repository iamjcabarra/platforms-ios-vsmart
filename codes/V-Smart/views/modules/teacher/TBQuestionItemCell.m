//
//  TBQuestionItemCell.m
//  V-Smart
//
//  Created by Julius Abarra on 10/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TBQuestionItemCell.h"
#import <QuartzCore/QuartzCore.h>
#import <WebKit/WebKit.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import "Masonry.h"

@interface TBQuestionItemCell () <WKScriptMessageHandler, WKUIDelegate>

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIView *difficultyView;
@property (strong, nonatomic) IBOutlet UIView *webView;
@property (strong, nonatomic) IBOutlet UIImageView *typeImage;

@property (strong, nonatomic) WKWebView *webObject;

@end

@implementation TBQuestionItemCell

- (void)awakeFromNib {
    // Configure WKWebView
    WKWebViewConfiguration *configuration = [self loadConfiguration];
    
    // Initialize the WKWebView with the current frame and the configuration
    if (self.webObject == nil) {
        self.webObject = [[WKWebView alloc] initWithFrame:self.webView.frame configuration:configuration];
    }
    
    self.webObject.scrollView.scrollEnabled = NO;
    [self.webView addSubview:self.webObject];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.webObject mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.webView).with.insets(padding);
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

#pragma mark - Webkit View Delegate

- (void)loadWebViewWithContents:(NSString *)string {
    NSString *htmlStartContent = @"<!DOCTYPE html><html><head><meta name='viewport' content='initial-scale=1.5'/><style> p{ font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: 500; font-size: 90%; }</style><meta charset='UTF-8'></head><body><p>";
    
    NSString *htmlEndContent = @"</p></body></html>";
    NSString *htmlString = [NSString stringWithFormat:@"%@%@%@", htmlStartContent, string, htmlEndContent];
    [self.webObject loadHTMLString:htmlString baseURL:nil];
}

- (WKWebViewConfiguration *)loadConfiguration {
    // Create WKWebViewConfiguration instance
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    
    // Setup WKUserContentController instance for injecting user script
    WKUserContentController *controller = [[WKUserContentController alloc] init];
    [self setScript:@"MathJax.js" inController:controller injectionAtStart:YES];
    
    // Configure the WKWebViewConfiguration instance with the WKUserContentController
    configuration.userContentController = controller;
    
    return configuration;
}

- (void)setScript:(NSString *)script inController:(WKUserContentController *)controller injectionAtStart:(BOOL)start {
    WKUserScriptInjectionTime time = WKUserScriptInjectionTimeAtDocumentEnd;
    
    if (start) {
        time = WKUserScriptInjectionTimeAtDocumentStart;
    }
    
    WKUserScript *myScript = [self userScript:script injectionTime:time];
    [controller addUserScript:myScript];
}


- (void)setCutomScript:(NSString *)customScript inController:(WKUserContentController *)controller injectionAtStart:(BOOL)start {
    WKUserScriptInjectionTime time = WKUserScriptInjectionTimeAtDocumentEnd;
    
    if (start) {
        time = WKUserScriptInjectionTimeAtDocumentStart;
    }
    
    WKUserScript *userScript = [[WKUserScript alloc] initWithSource:customScript injectionTime:time forMainFrameOnly:NO];
    [controller addUserScript:userScript];
}

- (WKUserScript *)userScript:(NSString *)file injectionTime:(WKUserScriptInjectionTime)time {
    WKUserScript *userScript = [[WKUserScript alloc] initWithSource:[self resourceFile:file]
                                                      injectionTime:time
                                                   forMainFrameOnly:NO];
    return userScript;
}

- (NSString *)resourceFile:(NSString *)filename {
    NSString *name = [filename stringByDeletingPathExtension];
    NSString *extension = [filename pathExtension];
    NSBundle *bundle = [NSBundle mainBundle];
    NSURL *path = [bundle URLForResource:name withExtension:extension];
    NSString *string = [NSString stringWithContentsOfURL:path encoding:NSUTF8StringEncoding error:nil];
    
    return string;
}

- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
}

- (void)applyShadowToView:(UIView *)object {
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.9f;
}

- (void)updateQuestionTypeView:(NSString *)questionTypeID {
    self.typeImage.image = nil;
    NSString *imageName = @"icon_essay_150px";
    
    // Essay
    if ([questionTypeID isEqualToString:@"6"]) {
        imageName = @"icon_essay_150px";
    }
    
    // Multiple Choice
    if ([questionTypeID isEqualToString:@"3"]) {
        imageName = @"icon_multiple_choice_150px";
    }
    
    // Short Answer
    if ([questionTypeID isEqualToString:@"9"]) {
        imageName = @"icon_short_answer_150px";
    }
    
    // True or False
    if ([questionTypeID isEqualToString:@"1"]) {
        imageName = @"icon_true_or_false_150px";
    }
    
    self.typeImage.image = [UIImage imageNamed:imageName];
}

- (void)updateDifficultyLevelView:(NSString *)difficultyLevelID {
    UIColor *color = [UIColor whiteColor];
    
    // Easy
    if ([difficultyLevelID isEqualToString:@"1"]) {
        color = UIColorFromHex(0x68BF61);
    }
    
    // Moderate
    if ([difficultyLevelID isEqualToString:@"2"]) {
        color = UIColorFromHex(0xFFCD02);
    }
    
    // Difficult
    if ([difficultyLevelID isEqualToString:@"3"]) {
        color = UIColorFromHex(0xF8931F);
    }
    
    self.difficultyView.backgroundColor = color;
    self.containerView.backgroundColor = color;
    self.questionDifficultyLabel.backgroundColor = color;
}

@end

