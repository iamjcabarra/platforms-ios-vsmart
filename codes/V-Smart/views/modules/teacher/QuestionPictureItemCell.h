//
//  QuestionPictureItemCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 03/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionPictureItemCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *qImageOne;
@property (weak, nonatomic) IBOutlet UIImageView *qImageTwo;
@property (weak, nonatomic) IBOutlet UIImageView *qImageThree;
@property (weak, nonatomic) IBOutlet UIImageView *qImageFour;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *qImageOneConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *qImageTwoConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *qImageThreeConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *qImageFourConstraint;

@property (weak, nonatomic) IBOutlet UIButton *buttonOne;
@property (weak, nonatomic) IBOutlet UIButton *buttonTwo;
@property (weak, nonatomic) IBOutlet UIButton *buttonThree;
@property (weak, nonatomic) IBOutlet UIButton *buttonFour;

@end
