//
//  TGGenericChoiceItem.m
//  V-Smart
//
//  Created by Ryan Migallos on 07/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGGenericChoiceItem.h"
#import "UIImageView+WebCache.h"

@interface TGGenericChoiceItem() <UITextFieldDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, UIImagePickerControllerDelegate>

//@property (strong, nonatomic) IBOutlet UIButton *uploadButton;
@property (strong, nonatomic) IBOutlet UIButton *checkButton;

@property (strong, nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) UIImagePickerController *imagePicker;

@end

@implementation TGGenericChoiceItem

- (void)awakeFromNib {
    // Initialization code
//    self.customTextField.placeholder = NSLocalizedString(@"Please enter a text for new items", nil);
    
    [self setupImagePickerControl];
    [self setupButtonTest];

    [self.checkButton addTarget:self action:@selector(choiceItemButttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.deleteButton addTarget:self action:@selector(choiceItemButttonAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)choiceItemButttonAction:(UIButton *)sender {
    
    TGGenericChoiceAction action = TGgenericChoiceDefault;
    
    if (sender == self.checkButton) {
        action = TGGenericChoiceCheck;
    }
    
    if (sender == self.deleteButton) {
        action = TGGenericChoiceDelete;
    }
    
    if ([(NSObject*)self.delegate respondsToSelector:@selector(didPressChoiceAction:withManagedObject:)] ) {
        [self.delegate didPressChoiceAction:action withManagedObject:self.mo];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.customTextField.text = @"";
    self.mo = nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // verify max length has not been exceeded
    // May issue dito
//    NSString *originalText = [NSString stringWithFormat:@"%@", textField.text];
    NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([(NSObject*)self.delegate respondsToSelector:@selector(updatedText:withManagedObject:)] ) {
        [self.delegate updatedText:updatedText withManagedObject:self.mo];
    }
    
    return NO;
}

////////////////////////////////////////////////////////////////
// ------------- IMAGE UPLOADING IMPLEMENTATION ------------- //
////////////////////////////////////////////////////////////////

- (void)setupImagePickerControl {
    
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = YES;
    self.imagePicker.modalPresentationStyle = UIModalPresentationPopover;
    self.popover = [[UIPopoverController alloc] initWithContentViewController:self.imagePicker];
}

- (void)setupButtonTest {
    
    SEL uploadButtonAction = @selector(choiceItemButttonImageUploadAction:);
    [self.uploadButton addTarget:self action:uploadButtonAction forControlEvents:UIControlEventTouchUpInside];
    
    SEL removeChoiceImageButtonAction = @selector(removeChoiceImageAction:);
    [self.removeChoiceImageButton addTarget:self action:removeChoiceImageButtonAction forControlEvents:UIControlEventTouchUpInside];
}

- (void)choiceItemButttonImageUploadAction:(UIButton *)sender {
    
    UIButton *b = sender;
    [self.popover presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)removeChoiceImageAction:(UIButton *)sender {
    self.uploadImageView.image = [UIImage imageNamed:@"icn_no_image.png"];
    self.removeChoiceImageButton.hidden = YES;
    
    TGGenericChoiceAction action = TGGenericChoiceUpload;
    if ([(NSObject*)self.delegate respondsToSelector:@selector(didPerformChoiceImageAction:withImageData:withStringURL:withManagedObject:)] ) {
        [self.delegate didPerformChoiceImageAction:action
                                     withImageData:nil
                                     withStringURL:@""
                                 withManagedObject:self.mo];
    }
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    NSURL *imageURL = [info valueForKey: UIImagePickerControllerReferenceURL];
    
    self.uploadImageView.image = chosenImage;
    self.removeChoiceImageButton.hidden = NO;
    
    CGFloat compression = 0.5;
    NSData *image_data = UIImageJPEGRepresentation(chosenImage, compression);
    
    TGGenericChoiceAction action = TGGenericChoiceUpload;
    if ([(NSObject*)self.delegate respondsToSelector:@selector(didPerformChoiceImageAction:withImageData:withStringURL:withManagedObject:)] ) {
        [self.delegate didPerformChoiceImageAction:action
                              withImageData:image_data
                              withStringURL:[imageURL absoluteString]
                          withManagedObject:self.mo];
    }

    [self.popover dismissPopoverAnimated:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self.popover dismissPopoverAnimated:YES];
}

@end
