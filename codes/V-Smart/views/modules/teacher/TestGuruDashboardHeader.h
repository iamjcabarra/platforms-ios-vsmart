//
//  TestGuruDashboardHeader.h
//  V-Smart
//
//  Created by Ryan Migallos on 7/20/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruDashboardHeader : UICollectionReusableView

// QUESTION
@property (strong, nonatomic) IBOutlet UIImageView *questionImageView;
@property (strong, nonatomic) IBOutlet UILabel *questionTitle;
@property (strong, nonatomic) IBOutlet UILabel *questionCount;

// TESTS
@property (strong, nonatomic) IBOutlet UIImageView *testImageView;
@property (strong, nonatomic) IBOutlet UILabel *testTitle;
@property (strong, nonatomic) IBOutlet UILabel *testCount;

// DEPLOYED
@property (strong, nonatomic) IBOutlet UIImageView *deployImageView;
@property (strong, nonatomic) IBOutlet UILabel *deployTitle;
@property (strong, nonatomic) IBOutlet UILabel *deployCount;

@end
