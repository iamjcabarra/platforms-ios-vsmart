//
//  TestGuruDataManager.m
//  V-Smart
//
//  Created by Ryan Migallos on 7/27/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruDataManager.h"

#import "Utils.h"
#import "AccountInfo.h"
#import "VSmartValues.h"
#import "NSDate+Helper.h"

static NSString *storeFilename = @"testgurudata.sqlite";

@interface TestGuruDataManager() <NSURLSessionTaskDelegate>

#pragma mark - PROPERTIES

@property (nonatomic, readwrite) NSManagedObjectContext *masterContext;
@property (nonatomic, readwrite) NSManagedObjectContext *mainContext;
@property (nonatomic, readwrite) NSManagedObjectContext *workerContext;

@property (nonatomic, readwrite) NSManagedObjectModel *model;
@property (nonatomic, readwrite) NSPersistentStore *store;
@property (nonatomic, readwrite) NSPersistentStoreCoordinator *coordinator;
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSDateFormatter *formatter;

@property (nonatomic, readwrite) TestGuruProgressBlock progressBlock;
@property (nonatomic, readwrite) TestGuruDoneProgressBlock doneProgressBlock;


@property (strong, nonatomic) NSUserDefaults *userDefaults;

@end

@implementation TestGuruDataManager

#pragma mark - SINGLETON

+ (instancetype)sharedInstance {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    static TestGuruDataManager *singleton = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        singleton = [[self alloc] init];
    });
    return singleton;
}

#pragma mark - SETUP

- (id)init {
    NSLog(@"Running %s", __PRETTY_FUNCTION__);
    
    self = [super init];
    if (!self) { return nil; }
    
    // Managed Object Model
    self.model = [self modelFromFrameWork:@"TestGuruDataModel"];
    // Persistent Store Coordinator
    self.coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    
    BOOL compatible = [self isCompatibleWithCoreDataMetadata:[self storeURL]];
    if (!compatible) {
        self.store = nil;
    }
    
    // CORE DATA 3-LAYER STACK for MASTER, MAIN, WORKER
    [self initializeManagedObjectsWithCoordinator:self.coordinator];
    
    //SESSION MANAGER
    self.session = [NSURLSession sharedSession];
    
    //DATE FORMATTER
    self.formatter = [[NSDateFormatter alloc] init];
    
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    
    return self;
}

- (void)initializeManagedObjectsWithCoordinator:(NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // DATA FLUSHER
    // [WARNING: YOU ARE NOT ALLOWED TO USE THIS CONTEXT]
    // Core Data Stack for the Master Thread [MASTER] -> [PERSISTENTSTORE]
    _masterContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_masterContext performBlockAndWait:^{
        [_masterContext setPersistentStoreCoordinator:persistentStoreCoordinator];
        [_masterContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // UI RELATED CONTEXT
    // Core Data Stack for the Main Thread [MAIN] -> [MASTER]
    _mainContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_mainContext setParentContext:_masterContext];
    [_mainContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    
    // BACKGROUND CONTEXT
    // Core Data Stack for the Worker Thread [WORKER] -> [MAIN]
    _workerContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_workerContext performBlockAndWait:^{
        [_workerContext setParentContext:_mainContext];
        [_workerContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // LOAD STORE FILE
    [self loadStore];
}

- (void)loadStore {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSDictionary *sqliteConfig = @{@"journal_mode" : @"DELETE"};//@{@"journal_mode" : @"WAL"}
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption  : @YES ,
                              NSInferMappingModelAutomaticallyOption        : @YES , //Light Weight Migration
                              NSSQLitePragmasOption                         : sqliteConfig
                              };
    NSError *error = nil;
    _store = [_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:[self storeURL] options:options error:&error];
    if (!_store) {
        //abort();
    } else {
        NSLog(@"Successfully added store: %@", _store);
    }
}

#pragma mark - MODELS (Initialization)

- (NSManagedObjectModel *)modelFromFrameWork:(NSString *)name {
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSURL *modelFileURL = [appBundle URLForResource:name withExtension:@"momd"];
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelFileURL];
    
    return model;
}

#pragma mark - SAVING

- (void)saveContext {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSManagedObjectContext *ctx = _workerContext;
    [ctx performBlockAndWait:^{
        [self saveTreeContext:ctx];
    }];
}

- (void)saveTreeContext:(NSManagedObjectContext *)context {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    if (!context) {
        return;
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"ERROR saving: %@", error);
    }
    
    if (context.parentContext) {
        [self saveTreeContext:context.parentContext];
    }
}

- (void)saveEntity:(NSString *)entity predicate:(NSPredicate *)predicate object:(NSDictionary *)data doneBlock:(TestGuruDoneBlock)doneBlock {

    NSManagedObjectContext *ctx = _workerContext;
    [ctx performBlockAndWait:^{
        NSManagedObject *mo = [self getEntity:kQuestionBankEntity predicate:predicate context:ctx];
        for (NSString *key in [data allKeys] ) {
            [mo setValue:[data valueForKey:key] forKey:key];
        }
        [self saveTreeContext:ctx];
        if (doneBlock) {
            doneBlock(YES);
        }
    }];
}

#pragma mark - PATHS

- (NSURL *)storeURL {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSURL *fileURL = [self applicationStoresDirectory];
    
    return [fileURL URLByAppendingPathComponent:storeFilename];
}

- (NSURL *)applicationStoresDirectory {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    NSString *filePath = [self applicationDocumentsDirectory];
    NSURL *storesDirectory = [[NSURL fileURLWithPath:filePath] URLByAppendingPathComponent:@"Stores"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtURL:storesDirectory withIntermediateDirectories:YES attributes:nil error:&error]) {
            NSLog(@"Successfully created Stores directory");
            if (error) {
                NSLog(@"Failed to create Stores directory : %@", error);
            }
        }
    }
    return storesDirectory;
}

- (NSString *)applicationDocumentsDirectory {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    return [NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - FAULTING (Memory Management)

- (void)faultObjectWithID:(NSManagedObjectID *)objectID inContext:(NSManagedObjectContext *)context {
    
    if (!objectID || !context) { return; }
    
    NSManagedObject *object = [context objectWithID:objectID];
    if (object.hasChanges) {
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"ERROR saving: %@", error);
        }
    }
    if (!object.isFault) {
        [context refreshObject:object mergeChanges:NO];
    }
    else {
        NSLog(@"Skipped faulting an object that is already a fault");
    }
    
    // Repeat the process if the context has a parent
    if (context.parentContext) {
        [self faultObjectWithID:objectID inContext:context.parentContext];
    }
}

#pragma mark - MIGRATION

- (BOOL)isCompatibleWithCoreDataMetadata:(NSURL *)storeUrl {
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *filePath = [NSString stringWithFormat:@"%@", storeUrl.path];
    
    if ([fm fileExistsAtPath:filePath]) {
        NSLog(@"checking model for compatibility...");
        
        NSError *error = nil;
        NSDictionary *metaData = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
                                                                                            URL:storeUrl
                                                                                          error:&error];
        NSManagedObjectModel *model = self.coordinator.managedObjectModel;
        
        if ([model isConfiguration:nil compatibleWithStoreMetadata:metaData]) {
            NSLog(@"model is compatible...");
            return YES;
            
        } else {
            
            if ([fm removeItemAtPath:filePath error:NULL]) {
                NSLog(@"removed file : %@", filePath);
                NSLog(@"model is incompatible...");
                return NO;
            }//REMOVE THE STORE FILE
            
        }
    }
    
    NSLog(@"file does not exists..");
    return NO;
}

- (BOOL)migrateStore:(NSURL *)sourceStore {
    NSLog(@"SKIPPED MIGRATION: Source is already compatible");
    
    BOOL success = NO;
    NSError *error = nil;
    
    //STEP 1 - Gather the Source, Destination and Mapping Model
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:sourceStore error:&error];
    NSManagedObjectModel *sourceModel = [NSManagedObjectModel mergedModelFromBundles:nil forStoreMetadata:sourceMetadata];
    NSManagedObjectModel *destinModel = _model;
    NSMappingModel *mappingModel = [NSMappingModel mappingModelFromBundles:nil forSourceModel:sourceModel destinationModel:destinModel];
    
    //STEP 2 - Perform Migration, assuming the mapping model isn't null
    if (mappingModel) {
        NSMigrationManager *migrationManager = [[NSMigrationManager alloc] initWithSourceModel:sourceModel destinationModel:destinModel];
        
        //OBSERVER
        [migrationManager addObserver:self forKeyPath:@"migrationProgress" options:NSKeyValueObservingOptionNew context:NULL];
        
        NSURL *destinStore = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Temp.sqlite"];
        success = [migrationManager migrateStoreFromURL:sourceStore
                                                   type:NSSQLiteStoreType
                                                options:nil
                                       withMappingModel:mappingModel
                                       toDestinationURL:destinStore
                                        destinationType:NSSQLiteStoreType
                                     destinationOptions:nil
                                                  error:&error];
        if (success) {
            //STEP 3 - Replace the old store with the new migrated store
            if ([self replaceStore:sourceStore withStore:destinStore]) {
                NSLog(@"SUCCESSFULLY MIGRATED %@ to the Current Model", sourceStore.path);
                [migrationManager removeObserver:self forKeyPath:@"migrationProgress"];
            }
            //STEP 3
        }
        else {
            NSLog(@"FAILED MIGRATION : %@", error);
        }
    }
    else {
        NSLog(@"FAILED MIGRATION: Mapping Model is null");
    }
    
    return YES;// indicates migration has finished, regardless of outcome
}

- (BOOL)replaceStore:(NSURL *)old withStore:(NSURL *)new {
    
    BOOL success = NO;
    NSError *Error = nil;
    if ([[NSFileManager defaultManager] removeItemAtURL:old error:&Error]) {
        
        Error = nil;
        if ([[NSFileManager defaultManager] moveItemAtURL:new toURL:old error:&Error]) {
            success = YES;
        }
        else {
            NSLog(@"FAILED to re-home new store %@", Error);
        }
    }
    else {
        NSLog(@"FAILED to remove old store %@: Error:%@", old, Error);
    }
    return success;
}

#pragma mark - WORKER

- (NSString *)baseURL {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *server = [NSString stringWithFormat:@"%@", [defaults stringForKey:@"baseurl_preference"] ];
    //    NSString *server = @"172.16.7.174"; //HARD CODING
    
    return server;
}

- (NSURL *)buildURL:(NSString *)string {
    
    NSString *path = [Utils buildUrl:string];
    NSLog(@"path : %@", path);
    return [NSURL URLWithString:path];
}

- (NSString *)emptyString:(NSString *)value {
    
    if ([value isEqualToString:@"(null)"]) {
        return @"";
    }
    return value;
}

- (NSString *)normalizeStringObject:(NSString *)string {

    NSCharacterSet *leadingTrailing = [NSCharacterSet whitespaceCharacterSet];
    NSString *value = [string stringByTrimmingCharactersInSet:leadingTrailing];
    
    return value;
}

- (NSString *)stringValue:(id)object {
    
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] ||  [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

- (BOOL)isArrayObject:(id)object {
    return [object isKindOfClass:[NSArray class]];
}

- (BOOL)isDictionaryObject:(id)object {
    return [object isKindOfClass:[NSDictionary class]];
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption | NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath object:(id)object {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:object];
    
    // predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption | NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (NSManagedObject *)getEntity:(NSString *)entity attribute:(NSString *)attribute
                     parameter:(NSString *)parameter context:(NSManagedObjectContext *)context {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (parameter) {
        
        // create predicate
        NSPredicate *predicate = [self predicateForKeyPath:attribute andValue:parameter];
        
        // set predicate
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
}

- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSUInteger)fetchCountForEntity:(NSString *)entity {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
    [request setIncludesSubentities:NO]; //Omit subentities. Default is YES (i.e. include subentities)
    
    NSError *err;
    
    NSManagedObjectContext *contex = _workerContext;
    NSUInteger count = [contex countForFetchRequest:request error:&err];
    if(count == NSNotFound) {
        return 0;
    }
    
    return count;
}

- (NSArray *)fetchObjectsForEntity:(NSString *)entity {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    NSError *err;
    NSManagedObjectContext *contex = _workerContext;
    NSArray *items = [contex executeFetchRequest:request error:&err];
    if(items.count > 0) {
        return items;
    }
    
    return 0;
}

- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return items;
    }
    
    return nil;
}

- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSManagedObjectContext *context = _workerContext;
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSManagedObject *)getFirstEntity:(NSString *)entity predicate:(NSPredicate *)predicate sortDescriptor:(NSSortDescriptor *) sortDescriptor {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (sortDescriptor) {
        [fetchRequest setSortDescriptors:@[sortDescriptor]];
    }
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSManagedObjectContext *context = _workerContext;
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items firstObject];
    }
    
    return nil;
}

- (NSManagedObject *)insertNewRecordForEntity:(NSString *)entity {
    
    NSManagedObjectContext *ctx = _workerContext;
    return [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:ctx];
}

- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
//    float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
//    if (ver >= 9.0) {
//        // Only executes on version 8 or above.
//        NSLog(@"%s OS %f", __PRETTY_FUNCTION__, ver);
//        
//        if (_store) {
//            
//            NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:fetchRequest];
//            NSError *deleteError = nil;
//            NSArray *list = [_coordinator executeRequest:delete withContext:_workerContext error:&deleteError];
//            
//            if (list != nil) {
//                return YES;
//            }
//            return NO;
//        }
//    }
//    
//    if (ver < 9.0) {
        // Only executes on version 8 or above.
//        NSLog(@"%s OS %f", __PRETTY_FUNCTION__, ver);
        
        if (predicate) {
            [fetchRequest setPredicate:predicate];
        }
        
        NSManagedObjectContext *context = _workerContext;
        NSArray *items = [context executeFetchRequest:fetchRequest error:nil];
        if ([items count] > 0) {
            for (NSManagedObject *lmo in items) {
                [context deleteObject:lmo];
            }
            [self saveTreeContext:context];
            
            return YES;
        }
//    }
    
    return NO;
}

- (BOOL)clearContentsForEntity:(NSString *)entity {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *lmo in items) {
            [context deleteObject:lmo];
        }
        [self saveTreeContext:context];
        
        return YES;
    }
    
    return NO;
}

- (BOOL)clearDataForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)ctx {
    
    //CLEAR CONTENTS
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate != nil) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *mo in items) {
            [ctx deleteObject:mo];
        }
        [self saveTreeContext:ctx];
    }
    
    return YES;
}

- (NSTimeInterval)parseTimeFromString:(NSString *)string {
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    
    NSInteger h, m, s;
    [scanner scanInteger:&h];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&m];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&s];
    
    return h * 3600 + m * 60 + s;
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (NSDate *)parseDateFromString:(NSString *)dateString {
    
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@".000Z" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" togo" withString:@""];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [self.formatter setTimeZone:gmt];
    NSString *template = @"yyyy-MM-dd HH:mm:ss";
    [self.formatter setDateFormat:template];
    NSDate *dateObject = [self.formatter dateFromString:dateString];
    return dateObject;
}

- (id)parseResponseData:(NSData *)data {
    
    if (data) {
        NSError *jsonError = nil;
        
        id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
        
        if (jsonError) {
            NSLog(@"JSON Error : %@", jsonError.localizedDescription);
        }
        
        if (!jsonError) {
            return object;
        }
    }
    
    return nil;
}

- (NSString *)jsonStringFromObject:(id)object {
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:object options:0 error:nil];
    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return string;
}

- (NSMutableURLRequest *)buildRequestWithMethod:(NSString *)method NSURL:(NSURL *)url body:(id)body {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:method];
    
    if (body) {
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:body options:0 error:&error];
        
        NSString *j_string = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        NSLog(@"j_string : <start>---- %@ ---<end>", j_string);
        [request setHTTPBody:postData];
    }
    
    return request;
}

- (BOOL)ownedByUser:(NSString *)userid {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return [user_id isEqualToString:userid];
}

- (NSString *)loginUser {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return user_id;
}

- (void)postMessage:(NSMutableDictionary *)data doneBlock:(TestGuruDoneBlock)doneBlock {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *avatar = [NSString stringWithFormat:@"%@", account.user.avatar];
    NSString *username = [NSString stringWithFormat:@"%@", account.user.email];
    int userId = account.user.id;
    
    data[@"user_id"] = [NSNumber numberWithInt:userId];
    data[@"avatar"] = avatar;
    data[@"username"] = username;
    data[@"is_attached"] = [NSNumber numberWithInt:0];
    NSLog(@"post message : %@", data);
    
    NSURL *postURL = [self buildURL:kEndPointPostSocialStream];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:postURL body:data];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              BOOL status = NO;
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary) {
                  NSLog(@"dictionary : %@", dictionary);
                  NSDictionary *meta = dictionary[@"_meta"];
                  NSString *metaStatus = [NSString stringWithFormat:@"%@", meta[@"status"]];
                  
                  NSArray *records = dictionary[@"records"];
                  if (records > 0) {
                      status = ![metaStatus isEqualToString:@"ERROR"] ? YES : NO;
                  }
                  
                  if (doneBlock) {
                      doneBlock(status);
                  }
              }
          }
      }];
    
    [task resume];
}

#pragma mark - TEST GURU API

//- (NSArray *)fetchFilterType:(NSString *)type selectedID:(NSString *)selected_id {
//    
////    kProficiencyEntity
//
//    NSString *entity = kProficiencyEntity;
//    
//    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
//    
//    
//    
//    
//    
//    
//    
//    
////    if (entity.length > 0) {
////        
////          for (NSDictionary *d in records) {
////              NSString *type_id = [self stringValue:d[@"id"]];
////              NSString *type_name = [self stringValue:d[@"value"]];
////              NSManagedObject *mo = [self getEntity:entity attribute:@"id" parameter:type_id context:ctx];
////              [mo setValue:type_id forKey:@"id"];
////              [mo setValue:type_name forKey:@"value"];
////          }
////        
////    }
//    
//}

- (void)requestType:(NSString *)type doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *entity = @"";
    
    //GRADED (a.k.a STAGE TYPE)
    if ([type isEqualToString:kEndPointTestGuruGradedType]) {
        entity = kStageTypeEntity;
    }
    
    //RESULT TYPE
    if ([type isEqualToString:kEndPointTestGuruResultType]) {
        entity = kResultTypeEntity;
    }
    
    //QUIZ CATEGORY
    if ([type isEqualToString:kEndPointTestGuruQuizCategory]) {
        entity = kCategoryTypeEntity;
    }

    //TEST TYPE
    if ([type isEqualToString:kEndPointTestGuruTestType]) {
        entity = kTestTypeEntity;
    }

    //LEVEL OF DIFFICULTY
    if ([type isEqualToString:kEndPointTestGuruProficiencyLevel]) {
        entity = kProficiencyEntity;
    }
    
    //DIFFICULTY LEVEL
    if ([type isEqualToString:kEndPointTestGuruDifficultyLevel]) {
        entity = kProficiencyEntity;
    }
    
    if ([type isEqualToString:kEndPointTestGuruLearningSkill]) {
        entity = kSkillEntity;
    }
    
    //QUESTION TYPE
    if ([type isEqualToString:kEndPointTestGuruQuestionType]) {
        entity = kQuestionTypeEntity;
    }
    
    if (entity.length > 0) {
    
        NSURL *url = [self buildURL:type];
        NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
        NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
          {
              if (error) {
                  NSLog(@"error %@", [error localizedDescription]);
              }
              
              if (!error) {
                  NSDictionary *dictionary = [self parseResponseData:responsedata];
                  if (dictionary[@"records"] != nil ) {
                      
                      NSArray *records = [NSArray arrayWithArray: dictionary[@"records"] ];
                      if (records.count > 0) {
                          NSManagedObjectContext *ctx = _workerContext;
                          
                          [ctx performBlock:^{
                              
//                              NSLog(@"records : %@", records);
                              
                              for (NSDictionary *d in records) {
                                  NSString *type_id = [self stringValue:d[@"id"]];
                                  NSString *type_name = [self stringValue:d[@"value"]];
                                  NSManagedObject *mo = [self getEntity:entity attribute:@"id" parameter:type_id context:ctx];
                                  [mo setValue:type_id forKey:@"id"];
                                  [mo setValue:type_name forKey:@"value"];
                              }
                              [self saveTreeContext:ctx];
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }];
                      }
                      
                      if (doneBlock) {
                          doneBlock(NO);
                      }
                  }
              }//end
          }];
        
        [task resume];
    }
}

//- (void)requestFilterOptionType:(NSString *)typeIdentifier title:(NSString *)title order:(NSString *)order
//                      doneBlock:(TestGuruDoneBlock)doneBlock {
//    
//    NSString *type = @"";
//    
//    //GRADED (a.k.a STAGE TYPE)
//    if ([typeIdentifier isEqualToString:kEndPointTestGuruGradedType]) {
//        type = kStageTypeEntity;
//    }
//    
//    //RESULT TYPE
//    if ([typeIdentifier isEqualToString:kEndPointTestGuruResultType]) {
//        type = kResultTypeEntity;
//    }
//    
//    //QUIZ CATEGORY
//    if ([typeIdentifier isEqualToString:kEndPointTestGuruQuizCategory]) {
//        type = kCategoryTypeEntity;
//    }
//    
//    //TEST TYPE
//    if ([typeIdentifier isEqualToString:kEndPointTestGuruTestType]) {
//        type = kTestTypeEntity;
//    }
//    
//    //LEVEL OF DIFFICULTY
//    if ([typeIdentifier isEqualToString:kEndPointTestGuruProficiencyLevel]) {
//        type = kProficiencyEntity;
//    }
//    
//    //DIFFICULTY LEVEL
//    if ([typeIdentifier isEqualToString:kEndPointTestGuruDifficultyLevel]) {
//        type = kProficiencyEntity;
//    }
//    
//    if ([typeIdentifier isEqualToString:kEndPointTestGuruLearningSkill]) {
//        type = kSkillEntity;
//    }
//    
//    //QUESTION TYPE
//    if ([typeIdentifier isEqualToString:kEndPointTestGuruQuestionType]) {
//        type = kQuestionTypeEntity;
//    }
//    
//    NSString *section_title = [NSString stringWithFormat:@"%@", title];
//    NSString *section_order = [NSString stringWithFormat:@"%@", order];
//    
//    if (type.length > 0) {
//        
//        NSURL *url = [self buildURL:typeIdentifier];
//        NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
//        NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
//          {
//              if (error) {
//                  NSLog(@"error %@", [error localizedDescription]);
//              }
//              
//              if (!error) {
//                  NSDictionary *dictionary = [self parseResponseData:responsedata];
//                  if (dictionary[@"records"] != nil && [self isArrayObject: dictionary[@"records"]] ) {
//                      
//                      NSMutableArray *records = [NSMutableArray arrayWithArray: dictionary[@"records"] ];
//                      NSDictionary *selectAll = [NSDictionary dictionary];
//                      NSString *hardCodedValue = @"";
//                      
//                      if ([type isEqual: kQuestionTypeEntity]) {
//                          hardCodedValue = @"All Types";
//                      }
//                      
//                      if ([type isEqual: kProficiencyEntity]) {
//                          hardCodedValue = @"All Levels";
//                      }
//                      
//                      if ([type isEqual: kSkillEntity]) {
//                          hardCodedValue = @"All Skills";
//                      }
//                      
//                      selectAll = @{
//                                    @"id":@"0",
//                                    @"value":hardCodedValue
//                                    };
//                      [records addObject:selectAll];
//                      
//                      if (records.count > 0) {
//                          NSManagedObjectContext *ctx = _workerContext;
//                          
//                          [ctx performBlock:^{
//                              
//                              NSPredicate *p4 = [self predicateForKeyPath:@"section_title" andValue:title];
//                              [self clearDataForEntity:kTestGuruFilterOptions withPredicate:p4 context:ctx];
//                              
////                              for (NSDictionary *d in records) {
//                              for (NSInteger i = 0; i<records.count; i++) {
//
//                                  NSDictionary *d = (NSDictionary *)records[i];
//                                  
//                                  NSString *type_id = [self stringValue:d[@"id"]];
//                                  NSString *type_value = [self stringValue:d[@"value"]];
//                                  NSString *type_order = [[NSNumber numberWithInteger:i] stringValue];
//                                  
//                                  NSPredicate *p1 = [self predicateForKeyPath:@"type" andValue:type];
//                                  NSPredicate *p2 = [self predicateForKeyPath:@"type_id" andValue:type_id];
//                                  NSPredicate *p3 = [self predicateForKeyPath:@"type_value" andValue:type_value];
////                                  NSPredicate *p4 = [self predicateForKeyPath:@"section_title" andValue:title];
//                                  NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[p1,p2,p3,p4]];
//                                  
//                                  NSManagedObject *mo = [self getEntity:kTestGuruFilterOptions
//                                                              predicate:predicate
//                                                                context:ctx];
//                                  if (mo == nil) {
//                                      mo = [NSEntityDescription insertNewObjectForEntityForName:kTestGuruFilterOptions
//                                                                         inManagedObjectContext:ctx];
//                                  }
//                                  
//                                  /*
//                                   section_order
//                                   section_title
//                                   type
//                                   type_id
//                                   type_value
//                                   */
//                                  [mo setValue:section_order forKey:@"section_order"];
//                                  [mo setValue:section_title forKey:@"section_title"];
//                                  [mo setValue:type forKey:@"type"];
//                                  [mo setValue:type_id forKey:@"type_id"];
//                                  [mo setValue:type_order forKey:@"type_order"];
//                                  [mo setValue:type_value forKey:@"type_value"];
//                                  [mo setValue:[NSDate date] forKey:@"date"];
//                                  
////                                  if (flag == nil) {
//                                      [mo setValue:@YES forKey:@"selected"];
////                                  }
//                              }
//                              
//                              [self saveTreeContext:ctx];
//                              if (doneBlock) {
//                                  doneBlock(YES);
//                              }
//                          }];
//                      }
//                  }
//                  
//              }//end
//          }];
//        
//        [task resume];
//    }
//}

- (void)requestFilterOptionType:(NSString *)typeIdentifier title:(NSString *)title order:(NSString *)order
                      doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *type = @"";
    
    //DIFFICULTY LEVEL
    if ([typeIdentifier isEqualToString:@"proficiency"]) {
        type = kProficiencyEntity;
    }
    
    //LEANING SKILL
    if ([typeIdentifier isEqualToString:@"learning"]) {
        type = kSkillEntity;
    }
    
    //QUESTION TYPE
    if ([typeIdentifier isEqualToString:@"questions/types"]) {
        type = kQuestionTypeEntity;
    }
    
    NSString *section_title = [NSString stringWithFormat:@"%@", title];
    NSString *section_order = [NSString stringWithFormat:@"%@", order];
    
    if (type.length > 0) {
        
        NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionFilterOptions, typeIdentifier] ];
        NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
        NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
          {
              if (error) {
                  NSLog(@"error %@", [error localizedDescription]);
              }
              
              if (!error) {
                  NSDictionary *dictionary = [self parseResponseData:responsedata];
                  if (dictionary[@"records"] != nil && [self isArrayObject: dictionary[@"records"]] ) {
                      
                      NSMutableArray *records = [NSMutableArray arrayWithArray: dictionary[@"records"] ];
                      NSDictionary *selectAll = [NSDictionary dictionary];
                      NSString *hardCodedValue = @"";
                      
                      if ([type isEqual: kQuestionTypeEntity]) {
                          hardCodedValue = @"All Types";
                      }
                      
                      if ([type isEqual: kProficiencyEntity]) {
                          hardCodedValue = @"All Levels";
                      }
                      
                      if ([type isEqual: kSkillEntity]) {
                          hardCodedValue = @"All Skills";
                      }
                      
                      selectAll = @{
                                    @"id":@"0",
                                    @"value":hardCodedValue
                                    };
                      [records addObject:selectAll];
                      
                      if (records.count > 0) {
                          NSManagedObjectContext *ctx = _workerContext;
                          
                          [ctx performBlock:^{
                              
                              NSPredicate *p4 = [self predicateForKeyPath:@"section_title" andValue:title];
                              [self clearDataForEntity:kTestGuruFilterOptions withPredicate:p4 context:ctx];
                              
//                              for (NSDictionary *d in records) {
                              for (NSInteger i = 0; i<records.count; i++) {

                                  NSDictionary *d = (NSDictionary *)records[i];
                                  
                                  NSString *type_id = [self stringValue:d[@"id"]];
                                  NSString *type_value = [self stringValue:d[@"value"]];
                                  NSString *type_order = [[NSNumber numberWithInteger:i] stringValue];
                                  
                                  NSPredicate *p1 = [self predicateForKeyPath:@"type" andValue:type];
                                  NSPredicate *p2 = [self predicateForKeyPath:@"type_id" andValue:type_id];
                                  NSPredicate *p3 = [self predicateForKeyPath:@"type_value" andValue:type_value];
//                                  NSPredicate *p4 = [self predicateForKeyPath:@"section_title" andValue:title];
                                  NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[p1,p2,p3,p4]];
                                  
                                  NSManagedObject *mo = [self getEntity:kTestGuruFilterOptions
                                                              predicate:predicate
                                                                context:ctx];
                                  if (mo == nil) {
                                      mo = [NSEntityDescription insertNewObjectForEntityForName:kTestGuruFilterOptions
                                                                         inManagedObjectContext:ctx];
                                  }
                                  
                                  /*
                                   section_order
                                   section_title
                                   type
                                   type_id
                                   type_value
                                   */
                                  [mo setValue:section_order forKey:@"section_order"];
                                  [mo setValue:section_title forKey:@"section_title"];
                                  [mo setValue:type forKey:@"type"];
                                  [mo setValue:type_id forKey:@"type_id"];
                                  [mo setValue:type_order forKey:@"type_order"];
                                  [mo setValue:type_value forKey:@"type_value"];
                                  [mo setValue:[NSDate date] forKey:@"date"];
                                  
//                                  if (flag == nil) {
                                      [mo setValue:@YES forKey:@"selected"];
//                                  }
                              }
                              
                              [self saveTreeContext:ctx];
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }];
                      }
                  }
                  
              }//end
          }];
        [task resume];
    }
}

- (void)updateFilterOptionObject:(NSManagedObject *)mo {
    
    BOOL flag = [(NSNumber *)[mo valueForKey:@"selected"] boolValue];
    NSNumber *selected = (flag) ? [NSNumber numberWithBool:NO] : [NSNumber numberWithBool:YES];
    
    NSManagedObjectContext *ctx = mo.managedObjectContext;
    [ctx performBlockAndWait:^{
        [mo setValue:selected forKey:@"selected"];
        [self saveTreeContext:ctx];
    }];
}

- (void)updateFilterOptionObjects:(NSSet *)selectedObjects deselect:(NSSet *)deselectedObjects {
    
    for (NSManagedObject *mo in selectedObjects) {
        NSNumber *selected = [NSNumber numberWithBool:YES];
        
        NSManagedObjectContext *ctx = mo.managedObjectContext;
        [ctx performBlockAndWait:^{
            [mo setValue:selected forKey:@"selected"];
            [self saveTreeContext:ctx];
        }];
    }
    
    for (NSManagedObject *mo in deselectedObjects) {
        NSNumber *selected = [NSNumber numberWithBool:NO];
        
        NSManagedObjectContext *ctx = mo.managedObjectContext;
        [ctx performBlockAndWait:^{
            [mo setValue:selected forKey:@"selected"];
            [self saveTreeContext:ctx];
        }];
    }

}

/////// QUESTION BANK CYCLE MANAGER ///////
#pragma mark - QUESTION CYCLE MANAGER

- (void)fetchQuestionList:(TestGuruListBlock)list {

    NSManagedObjectContext *ctx = _workerContext;
    
    // get the entity scratch pad
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortType = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:NO];
    [fetchRequest setSortDescriptors:@[sortType]];
    
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
    
    if ([items count] > 0) {
        
        if (list) {
            list([NSArray arrayWithArray:items]);
        }
    }
}

- (void)checkCompleteStates:(TestGuruNumberBlock)number {
    
    NSManagedObjectContext *ctx = _workerContext;
    
    // get the entity scratch pad
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionEntity];
    
    // predicate
    NSPredicate *predicate = [self predicateForKeyPath:@"complete" object:@"0"];
    [fetchRequest setPredicate:predicate];
    
    // Edit the sort key as appropriate.
//    NSSortDescriptor *sortType = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:NO];
//    [fetchRequest setSortDescriptors:@[sortType]];
    
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
    
    if (number) {
        number([[NSArray arrayWithArray:items] count]);
    }
}

- (NSArray *)filterOptionsSettedTo:(BOOL)selected_status {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTestGuruFilterOptions];
    NSPredicate *predicate = [self predicateForKeyPath:@"selected" object:[NSNumber numberWithBool:selected_status]];
    [fetchRequest setPredicate:predicate];
    
    NSManagedObjectContext *context = _workerContext;
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        items = @[];
        return items;
    }
    
    return items;
}

- (NSManagedObject *)firstManagedObject {
    
    NSManagedObjectContext *ctx = _workerContext;
    
    // get the entity scratch pad
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortType = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:NO];
    [fetchRequest setSortDescriptors:@[sortType]];
    
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
    
    if ([items count] > 0) {
        return [items firstObject];
    }
    
    return nil;
}

- (void)requestPaginationSettings:(TestGuruDoneBlock)doneBlock {
    
    NSURL *url = [self buildURL:kEndPointTGPagination];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          [self saveObject:@"10" forKey:kTG_PAGINATION_LIMIT];
                                      }
                                      
                                      if (!error) {
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary[@"records"] != nil ) {
                                              
                                              BOOL flag = [self isArrayObject:dictionary[@"records"]];
                                              
                                              if (flag) {
                                                  
                                                  NSArray *records = dictionary[@"records"];
                                                  if (records.count > 0) {
                                                      
                                                      NSDictionary *data = [records firstObject];
                                                      
                                                      NSManagedObjectContext *ctx = _workerContext;
                                                      
                                                      [ctx performBlock:^{
                                                          
                                                          NSString *setting_value = [data valueForKey:@"setting_value"];
                                                          
                                                          [self saveObject:setting_value forKey:kTG_PAGINATION_LIMIT];
                                                          
                                                          [self saveTreeContext:ctx];
                                                          if (doneBlock) {
                                                              doneBlock(YES);
                                                          }
                                                      }];
                                                  } else {
                                                      if (doneBlock) {
                                                          doneBlock(NO);
                                                      }
                                                  }
                                                  
                                              }
                                              
                                          }
                                      }//end
                                  }];
    
    [task resume];
}

- (void)requestPackageTypes:(TestGuruDoneBlock)doneBlock {
    [self clearContentsForEntity:kPackageTypeEntity];
    
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer]];
    NSURL *url = [self buildURL:kEndPointTestGuruPackageType];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL success = false;
            
            if (dictionary[@"records"] != nil) {
                BOOL flag = [self isArrayObject:dictionary[@"records"]];
                
                if (flag) {
                    NSArray *records = dictionary[@"records"];
                    
                    if (records.count > 0) {
                        success = true;
                        
                        NSManagedObjectContext *ctx = _workerContext;
                        
                        [ctx performBlock:^{
                            for (NSDictionary *d in records) {
                                NSString *package_id = [self stringValue:d[@"id"]];
                                NSString *package_value = [self stringValue:d[@"value"]];
                                NSString *package_description = [self stringValue:d[@"description"]];
                                NSString *package_image_url = [NSString stringWithFormat:@"%@/%@",homeurl, [self stringValue: d[@"image_url"]]];
                                
                                NSManagedObject *mo = [self getEntity:kPackageTypeEntity attribute:@"id" parameter:package_id context:ctx];
                                
                                [mo setValue:@([package_id integerValue]) forKey:@"id"];
                                [mo setValue:package_value forKey:@"value"];
                                [mo setValue:package_description forKey:@"package_description"];
                                [mo setValue:package_image_url forKey:@"image_url"];
                            }
                            
                            BOOL coordinator = [self isCoordinator];
                            
                            if (coordinator) {
                                NSString *package_id = @"10000";
                                NSString *package_value = @"Test Management";
                                NSString *package_description = @"A function provided for the user to view status of submitted tests.";
                                NSString *package_image_url = @"test_management_icon.png";
                            
                                NSManagedObject *mo = [self getEntity:kPackageTypeEntity attribute:@"id" parameter:package_id context:ctx];
                                
                                [mo setValue:@([package_id integerValue]) forKey:@"id"];
                                [mo setValue:package_value forKey:@"value"];
                                [mo setValue:package_description forKey:@"package_description"];
                                [mo setValue:package_image_url forKey:@"image_url"];
                            }
                            
                            [self saveTreeContext:ctx];
                        }];
                    }
                }
            }
            
            if (doneBlock) {
                doneBlock(success);
            }
        }
    }];
    
    [task resume];
}

- (void)requestQuestionType:(TestGuruDoneBlock)doneBlock {
    [self clearContentsForEntity:kQuestionTypeEntity predicate:nil];
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer]];

    NSURL *url = [self buildURL:kEndPointTestGuruQuestionType];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil && [self isArrayObject: dictionary[@"records"]]) {
                  
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {
                      NSManagedObjectContext *ctx = _workerContext;
                      
                      [ctx performBlock:^{
                          for (NSDictionary *d in records) {
                              NSLog(@"data %@", d);
                              
                              NSString *question_type_id = [self stringValue:d[@"id"]];
                              NSString *question_type_name = [self stringValue:d[@"value"]];
                              NSString *question_description = [self stringValue:d[@"description"]];
                              NSString *image_link = [NSString stringWithFormat:@"%@/%@", homeurl, [self stringValue:d[@"icon"]]];
                              NSString *question_image_url = image_link;
                              
                              NSManagedObject *mo = [self getEntity:kQuestionTypeEntity attribute:@"id" parameter:question_type_id context:ctx];
                              
                              [mo setValue:question_type_id forKey:@"id"];
                              [mo setValue:question_type_name forKey:@"value"];
                              [mo setValue:question_description forKey:@"desc"];
                              [mo setValue:question_image_url forKey:@"image_url"];
                          }
                          
                          [self saveTreeContext:ctx];
                          
                          if (doneBlock) {
                              doneBlock(YES);
                          }
                      }];
                  }
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
              }
          }
      }];
    
    [task resume];
}

- (NSDictionary *)questionObjectForID:(NSString *)question_id {
    
    NSString *image_url = @"";
    NSString *description = @"";
    
    //ESSAY
    if ([question_id isEqualToString:@"6"]) {
        image_url = @"icon_essay_150px";
        description = @"Essay questions require you to provide students with a question or statement to which they can respond in detail. Students are given the opportunity to type an answer into a text field.";
    }
    
    //MULTIPLE CHOICE
    if ([question_id isEqualToString:@"3"]) {
        image_url = @"icon_multiple_choice_150px";
        description = NSLocalizedString(@"Multiple Choice is a form of assessment in which students are asked to select the best possible answer out of the choices from a list", nil);
    }
    
    //SHORT ANSWER
    if ([question_id isEqualToString:@"9"]) {
        image_url = @"icon_short_answer_150px";
        description = @"Short answer questions are typically composed of a brief prompt that demands a written answer that varies in length from one or two words to a few sentences. They are most often used to test basic knowledge of key facts and terms.";
    }
    
    //TRUE OR FALSE
    if ([question_id isEqualToString:@"1"]) {
        image_url = @"icon_true_or_false_150px";
        description = NSLocalizedString(@"True or False is an alternative choice test in which the student indicates whether each of the several statements is true or false.", nil);
    }
    
    NSDictionary *data = @{@"image": image_url, @"description": description};
    
    return data;
}

- (void)requestProficiencyLevel:(TestGuruDoneBlock)doneBlock {
    
    NSURL *url = [self buildURL:kEndPointTestGuruProficiencyLevel];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil ) {
                  
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {
                      NSManagedObjectContext *ctx = _workerContext;
                      
//                      [self clearDataForEntity:kProficiencyEntity withPredicate:nil context:ctx];
                      [ctx performBlock:^{
                          
                          NSLog(@"records : %@", records);
                          
                          for (NSDictionary *d in records) {
                              NSString *proficiency_id = [self stringValue:d[@"id"]];
                              NSString *proficiency_name = [self stringValue:d[@"value"]];
                              
                              NSManagedObject *mo = [self getEntity:kProficiencyEntity attribute:@"id" parameter:proficiency_id context:ctx];
                              
                              [mo setValue:proficiency_id forKey:@"id"];
                              [mo setValue:proficiency_name forKey:@"value"];
                              
                          }
                          [self saveTreeContext:ctx];
                          if (doneBlock) {
                              doneBlock(YES);
                          }
                      }];
                  }
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
              }
          }//end
      }];
    
    [task resume];
}

- (void)requestLearningSkill:(TestGuruDoneBlock)doneBlock {
    
    NSURL *url = [self buildURL:kEndPointTestGuruLearningSkill];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil ) {
                  
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {
                      NSManagedObjectContext *ctx = _workerContext;
                      
//                      [self clearDataForEntity:kSkillEntity withPredicate:nil context:ctx];
                      [ctx performBlock:^{
                          
                          NSLog(@"records : %@", records);
                          
                          for (NSDictionary *d in records) {
                              NSString *learning_skill_id = [self stringValue:d[@"id"]];
                              NSString *learning_skill_name = [self stringValue:d[@"value"]];
                              NSManagedObject *mo = [self getEntity:kSkillEntity attribute:@"id" parameter:learning_skill_id context:ctx];
                              [mo setValue:learning_skill_id forKey:@"id"];
                              [mo setValue:learning_skill_name forKey:@"value"];
                          }
                          [self saveTreeContext:ctx];
                          if (doneBlock) {
                              doneBlock(YES);
                          }
                      }];
                  }
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
              }
          }//end
      }];
    
    [task resume];
}

- (void)requestQuestionCountForUser:(NSString *)userid dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionCount, userid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil ) {
                  if (dataBlock) {
                      dataBlock( dictionary[@"records"] );
                  }
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)requestTestCountForUser:(NSString *)userid dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruTestCount, userid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil ) {
                  if (dataBlock) {
                      dataBlock( dictionary[@"records"] );
                  }
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)requestDeployedTestCountForUser:(NSString *)userid dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruDeployedTestCount, userid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSLog(@"dictionary : %@", dictionary);
              if (dictionary[@"records"] != nil ) {
                  if (dataBlock) {
                      dataBlock( dictionary[@"records"] );
                  }
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)downloadQuestionImageForObject:(NSManagedObject *)object doneBlock:(TestGuruDoneBlock)doneBlock {

    if (object != nil) {
        
        NSString *image_url = [object valueForKey:@"image_url"];
        NSString *question_id = [object valueForKey:@"id"];
        
        if ( (image_url != nil) && (image_url.length > 0)) {
            
            NSURL *link = [NSURL URLWithString:image_url];
            NSURLSessionDownloadTask *dt = [self.session downloadTaskWithURL:link
                                                           completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                if (location) {
                    NSData *data = [NSData dataWithContentsOfURL:location];
                    if (data) {
                        NSManagedObjectContext *ctx = _workerContext;
                        [ctx performBlock:^{
                            NSManagedObject *mo = [self getEntity:kQuestionBankEntity attribute:@"id" parameter:question_id context:ctx];
                            [mo setValue:data forKey:@"image_data"];
                            [self saveTreeContext:ctx];
                        }];
                        if (doneBlock) {
                            doneBlock(YES);
                        }
                    }
                }
            }];
            
            [dt resume];
        }
    }
}

- (void)downloadImageForEntity:(NSString *)entity withObject:(NSManagedObject *)object doneBlock:(TestGuruDoneBlock)doneBlock {
    
    if (object != nil) {
        
        NSString *image_url = [object valueForKey:@"image_url"];
        NSString *parameter_id = [object valueForKey:@"id"];
        
        if ( (image_url != nil) && (image_url.length > 0)) {
            
            NSURL *link = [NSURL URLWithString:image_url];
            NSURLSessionDownloadTask *dt = [self.session downloadTaskWithURL:link
                                                           completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error)
            {
               if (location) {
                   
                   NSData *data = [NSData dataWithContentsOfURL:location];
                   if (data) {
                       
                       NSManagedObjectContext *ctx = _workerContext;
                       [ctx performBlock:^{
                           NSManagedObject *mo = [self getEntity:entity
                                                       attribute:@"id"
                                                       parameter:parameter_id
                                                         context:ctx];
                           
                           [mo setValue:data forKey:@"image_data"];
                           [self saveTreeContext:ctx];
                       }];
                       if (doneBlock) {
                           doneBlock(YES);
                       }
                   }
               }
            }];
            
            [dt resume];
        }
    }
}

- (void)downloadImageFromQuestionObject:(NSManagedObject *)mo binaryBlock:(TestGuruBinaryBlock)binaryBlock {

    if (mo != nil) {
        //URL PATH CREATION
        NSString *image_url = [mo valueForKey:@"image_url"];
        NSString *question_id = [mo valueForKey:@"id"];
        NSString *imageUrlPath = [Utils buildUrl:[NSString stringWithFormat:@"/%@", image_url] ];
        
        NSURLSession *s = [NSURLSession sharedSession];
        NSURL *imageURL = [NSURL URLWithString:imageUrlPath];
        NSURLSessionDownloadTask *dt = [s downloadTaskWithURL:imageURL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            
            if (location) {
                NSData *data = [NSData dataWithContentsOfURL:location];
                if (data) {
                    NSManagedObjectContext *ctx = _workerContext;
                    [ctx performBlock:^{
                        NSManagedObject *mo = [self getEntity:kQuestionBankEntity attribute:@"id" parameter:question_id context:ctx];
                        [mo setValue:data forKey:@"image_data"];
                        [self saveTreeContext:ctx];
                    }];
                    if (binaryBlock) {
                        binaryBlock(data);
                    }
                }

            }
        }];
        [dt resume];
    }
}

- (void)processQuestionData:(NSDictionary *)d sectionName:(NSString *)sectionName
                    homeurl:(NSString *)homeurl details:(BOOL)enable managedObjectContext:(NSManagedObjectContext *)ctx {
    
    NSLog(@"%s data : %@", __PRETTY_FUNCTION__, d);
    
    /*
     
     "correct_answer_feedback" = "Good Job!";
     "date_created" = "2015-06-02 09:34:04";
     "date_modified" = "2015-07-22 06:14:36";
     "general_feedback" = "General Feedback!";
     id = 492;
     "image_url" = "<null>";
     "is_deleted" = 0;
     "is_public" = 1;
     learningSkillsName = Knowledge;
     "learning_skills_id" = 1;
     name = wewe;
     points = "1.00";
     proficiencyLevelName = "Beginning (B)";
     "proficiency_level_id" = 1;
     questionTypeName = "True or False";
     "question_parent_id" = 0;
     "question_text" = wwewe;
     "question_type_id" = 1;
     tags = "";
     "user_id" = 35;
     "wrong_answer_feedback" = "Try again!";
     
     */
    
    NSString *obj_id = [self stringValue:d[@"id"]];
    NSString *name = [self stringValue:d[@"name"]];
    NSString *correct_answer_feedback = [self stringValue:d[@"correct_answer_feedback"]];
    NSString *date_created = [self stringValue:d[@"date_created"]];
    NSString *date_modified = [self stringValue:d[@"date_modified"]];
    NSString *general_feedback = [self stringValue:d[@"general_feedback"]];
    NSString *image_url = [NSString stringWithFormat:@"%@/%@", homeurl, [self stringValue:d[@"image_url"]] ];
    NSString *is_deleted = [self stringValue:d[@"is_deleted"]];
    NSString *is_public = [self stringValue:d[@"is_public"]];
    NSString *learningSkillsName = [self stringValue:d[@"learningSkillsName"]];
    NSString *learning_skills_id = [self stringValue:d[@"learning_skills_id"]];
    NSString *points = [self stringValue:d[@"points"]];

    //PACKAGE TYPE ID
    NSString *package_type_id = [self stringValue:d[@"package_type_id"]];
    
    NSString *proficiencyLevelName = [self stringValue:d[@"proficiencyLevelName"]];
    if (d[@"difficultyLevelName"] != nil) {
        proficiencyLevelName = [self stringValue:d[@"difficultyLevelName"]];
    }
    
    NSString *proficiency_level_id = [self stringValue:d[@"proficiency_level_id"]];
    if (d[@"difficulty_level_id"] != nil) {
        proficiency_level_id = [self stringValue:d[@"difficulty_level_id"]];
    }
    
    NSString *questionTypeName = [self stringValue:d[@"questionTypeName"]];
    NSString *question_parent_id = [self stringValue:d[@"question_parent_id"]];
    NSString *question_text = [self stringValue:d[@"question_text"]];
    NSString *question_type_id = [self stringValue:d[@"question_type_id"]];
    //                              NSString *tags = [self stringValue:d[@"tags"]];
    NSString *user_id = [self stringValue:d[@"user_id"]];
    NSString *wrong_answer_feedback = [self stringValue:d[@"wrong_answer_feedback"]];
    
    //                                  NSManagedObject *mo = [self getEntity:kQuestionBankEntity attribute:@"id" parameter:obj_id context:ctx];
    NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kQuestionBankEntity inManagedObjectContext:ctx];
    
    NSAttributedString *attributted_name = [[NSAttributedString alloc] initWithData:[name dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
    name = [attributted_name string];
    
    NSAttributedString *attributed_question_text = [[NSAttributedString alloc] initWithData:[question_text dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
    question_text = [attributed_question_text string];
    
    [mo setValue:obj_id forKey:@"id"];
    [mo setValue:name forKey:@"name"];
    [mo setValue:correct_answer_feedback forKey:@"correct_answer_feedback"];
    [mo setValue:date_created forKey:@"date_created"];
    [mo setValue:date_modified forKey:@"date_modified"];
    [mo setValue:general_feedback forKey:@"general_feedback"];
    [mo setValue:image_url forKey:@"image_url"];
    [mo setValue:is_deleted forKey:@"is_deleted"];
    [mo setValue:is_public forKey:@"is_public"];
    [mo setValue:learningSkillsName forKey:@"learningSkillsName"];
    [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
    [mo setValue:points forKey:@"points"];
    [mo setValue:proficiencyLevelName forKey:@"proficiencyLevelName"];
    [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];
    [mo setValue:questionTypeName forKey:@"questionTypeName"];
    [mo setValue:question_parent_id forKey:@"question_parent_id"];
    [mo setValue:question_text forKey:@"question_text"];
    [mo setValue:question_type_id forKey:@"question_type_id"];
    //                              [mo setValue:tags forKey:@"tags"];
    [mo setValue:user_id forKey:@"user_id"];
    [mo setValue:wrong_answer_feedback forKey:@"wrong_answer_feedback"];
    
    [mo setValue:package_type_id forKey:@"package_type_id"];
    
    NSString *search_string = [NSString stringWithFormat:@"%@ %@ %@", name, question_text, questionTypeName];
    [mo setValue:[search_string lowercaseString] forKey:@"search_string"];

    
    if (sectionName != nil) {
        NSString *section_name = [NSString stringWithFormat:@"%@", sectionName];
        [mo setValue:section_name forKey:@"section_name"];
    }
    
    if (enable) {
        [self requestDetailsForQuestion:mo doneBlock:nil];
    }
    
}

// TOTO
- (void)requestQuestionListForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionList, userid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSDictionary *meta = dictionary[@"_meta"];
              NSString *metaCount = [self stringValue:meta[@"count"]];
              NSString *metaStatus = meta[@"status"];
              
              NSManagedObjectContext *ctx = self.workerContext;
              
              if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
                  [ctx performBlock:^{
                      [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                  }];
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
                  
                  return;
              }
                  
              if (dictionary[@"records"] != nil ) {
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {
                      
                      // NSManagedObjectContext *ctx = self.workerContext;
                      
                      [ctx performBlock:^{
                      
                          BOOL status = [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                          if (status) {
                          
                              for (NSDictionary *d in records) {
                                  
                                  NSLog(@"data : %@", d);
                                  [self processQuestionData:d sectionName:@"" homeurl:homeurl details:NO managedObjectContext:ctx];
                              }
                              
                              [self saveTreeContext:ctx];
                              
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }
                          
                      }];
                      
                      
                  }
                  
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)requestTestGuruCurriculumListWithBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *course_id = [self stringValue:[self fetchObjectForKey:kTGQB_SELECTED_COURSE_ID]];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGQBCourseList, course_id]];
    
    NSString *entity = kCourseCurriculumEntity;
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              

              if ( (dictionary != nil) && (dictionary[@"records"] != nil ) ) {
                  NSArray *records = dictionary[@"records"];
                      
                      NSManagedObjectContext *ctx = _workerContext;
                  [self clearDataForEntity:entity withPredicate:nil context:ctx];
                  [self clearDataForEntity:kCompetencyEntity withPredicate:nil context:ctx];
                  
                  if (records.count > 0) {
                      
                      
                      [ctx performBlock:^{
                          
                          for (NSDictionary *d in records) {
                              
                              NSLog(@"data : %@", d);
                              
                              NSString *avatar = [self stringValue: d[@"avatar"]];
                              NSString *created_by = [self stringValue: d[@"created_by"]];
                              NSString *created_by_id = [self stringValue: d[@"created_by_id"]];
                              NSString *curriculum_description = [self stringValue: d[@"curriculum_description"]];
                              NSString *curriculum_id = [self stringValue: d[@"curriculum_id"]];
                              NSString *curriculum_status = [self stringValue: d[@"curriculum_status"]];
                              NSString *curriculum_status_remarks = [self stringValue: d[@"curriculum_status_remarks"]];
                              NSString *curriculum_territory_id = [self stringValue: d[@"curriculum_territory_id"]];
                              NSString *curriculum_territory_name = [self stringValue: d[@"curriculum_territory_name"]];
                              NSString *curriculum_title = [self stringValue: d[@"curriculum_title"]];;
                              NSString *date_created = [self stringValue: d[@"date_created"]];
                              NSString *date_modified = [self stringValue: d[@"date_modified"]];
                              NSString *grade_level_id = [self stringValue: d[@"grade_level_id"]];
                              NSString *grade_level_name = [self stringValue: d[@"grade_level_name"]];
                              NSString *school_profile_id = [self stringValue: d[@"school_profile_id"]];
                              NSString *school_profile_name = [self stringValue: d[@"school_profile_name"]];
                              NSString *sy_id = [self stringValue: d[@"sy_id"]];
                              NSString *sy_name = [self stringValue: d[@"sy_name"]];
                              

                              NSManagedObject *mo = [self getEntity:entity attribute:@"curriculum_id" parameter:curriculum_id context:ctx];
                              
                              if (mo == nil) {
                                  mo = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:ctx];
                              }
                              
                              [mo setValue:avatar forKey:@"avatar"];
                              [mo setValue:created_by forKey:@"created_by"];
                              [mo setValue:created_by_id forKey:@"created_by_id"];
                              [mo setValue:curriculum_description forKey:@"curriculum_description"];
                              [mo setValue:curriculum_id forKey:@"curriculum_id"];
                              [mo setValue:curriculum_status forKey:@"curriculum_status"];
                              [mo setValue:curriculum_status_remarks forKey:@"curriculum_status_remarks"];
                              [mo setValue:curriculum_territory_id forKey:@"curriculum_territory_id"];
                              [mo setValue:curriculum_territory_name forKey:@"curriculum_territory_name"];
                              [mo setValue:curriculum_title forKey:@"curriculum_title"];
                              [mo setValue:curriculum_title forKey:@"value"];
                              [mo setValue:date_created forKey:@"date_created"];
                              [mo setValue:date_modified forKey:@"date_modified"];
                              [mo setValue:grade_level_id forKey:@"grade_level_id"];
                              [mo setValue:grade_level_name forKey:@"grade_level_name"];
                              [mo setValue:school_profile_id forKey:@"school_profile_id"];
                              [mo setValue:school_profile_name forKey:@"school_profile_name"];
                              [mo setValue:sy_id forKey:@"sy_id"];
                              [mo setValue:sy_name forKey:@"sy_name"];
                              [mo setValue:curriculum_territory_id forKey:@"id"];
                          }
                          
                          [self saveTreeContext:ctx];
                          
                          if (doneBlock) {
                              doneBlock(YES);
                          }
                          
                      }];
                  }
            
              }
              
          }//end
      }];
    
    [task resume];
}

- (void)requestTestGuruCurriculumOverviewForCurriculumID:(NSString *)curriculumid doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGQBCurriculumOverview, curriculumid]];
    
    NSString *entity = kCourseCurriculumEntity;
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            
            
            if ( (dictionary != nil) && (dictionary[@"records"] != nil ) ) {
                NSDictionary *d = dictionary[@"records"];
                    
                NSManagedObjectContext *ctx = _workerContext;
                
                [ctx performBlock:^{
                    
                    NSString *curriculum_id = [self stringValue: d[@"curriculum_id"]];
                    NSManagedObject *mo = [self getEntity:entity attribute:@"curriculum_id" parameter:curriculum_id context:ctx];
                    
                    if ([self isArrayObject:d[@"periods"]]) {
                        NSArray *list = [NSArray arrayWithArray:d[@"periods"]];
                        [self processPeriod:list object:mo];
                    }//if ([self isArrayObject:d[@"periods"]])
                    

                    [self saveTreeContext:ctx];
                    
                    if (doneBlock) {
                        doneBlock(YES);
                    }
                    
                }];
                
            }
            
        }//end
    }];
    
    [task resume];
}

- (void)requestTestGuruLearningCompetenciesForPeriodID:(NSString *)periodid userData:(NSDictionary *)userData doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGQBLearningCompetecies, periodid]];
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            
            if ( (dictionary != nil) && (dictionary[@"records"] != nil) ) {
                NSDictionary *records = dictionary[@"records"];
                
                if (records[@"competencies"] != nil) {
                    NSArray *competencies = records[@"competencies"];
                    
                        NSManagedObjectContext *ctx = _workerContext;
                        
                    [self clearDataForEntity:kCompetencyEntity withPredicate:nil context:ctx];
                    
                    if ([competencies count] > 0) {

                        
                        [ctx performBlock:^{
                            
//                            BOOL cleared = [self clearContentsForEntity:kCompetencyEntity predicate:nil];
//                            if (cleared) {
                                
                                NSString *curriculum_id_string = [NSString stringWithFormat:@"%@", userData[@"curriculum_id"] ];
                                NSString *curriculum_title_string = [NSString stringWithFormat:@"%@", userData[@"curriculum_title"] ];
                                NSString *period_name_string = [NSString stringWithFormat:@"%@", userData[@"period_name"] ];
                                
                                for (NSDictionary *d in competencies) {
                                    NSLog(@"data : %@", d);
                                    
                                    NSString *code = [self stringValue: d[@"code"]];
                                    NSString *competency = [self stringValue: d[@"competency"]];
                                    NSString *competency_id = [self stringValue: d[@"competency_id"]];
                                    NSString *domain_id = [self stringValue: d[@"domain_id"]];
                                    NSString *period_id = [self stringValue: d[@"period_id"]];
                                    
                                    NSPredicate *predicate = [self predicateForKeyPath:@"code" andValue:code];
                                    NSManagedObject *mo = [self getEntity:kCompetencyEntity
                                                                predicate:predicate context:ctx];
                                    if (mo == nil) {
                                        mo = [NSEntityDescription insertNewObjectForEntityForName:kCompetencyEntity
                                                                           inManagedObjectContext:ctx];
                                    }
                                    
                                    [mo setValue:code forKey:@"code"];
                                    [mo setValue:curriculum_id_string forKey:@"curriculum_id"];
                                    [mo setValue:curriculum_title_string forKey:@"curriculum_title"];
                                    [mo setValue:competency forKey:@"competency"];
                                    [mo setValue:competency_id forKey:@"competency_id"];
                                    [mo setValue:domain_id forKey:@"domain_id"];
                                    [mo setValue:period_id forKey:@"period_id"];
                                    [mo setValue:period_name_string forKey:@"period_name"];
                                    
                                }//(NSDictionary *d in competencies)
                                
                                [self saveTreeContext:ctx];
                                if (doneBlock) {
                                    doneBlock(YES);
                                }
                                
//                            }//CLEARED
                            
                        }];
                    }
                }//(records[@"competencies"] != nil)
            }//( (dictionary != nil) &&  (dictionary[@"records"] != nil) )
        }//end
    }];
    
    [task resume];
}

- (void)processPeriod:(NSArray *)list object:(NSManagedObject *)object {
    
    NSString *curriculum_id = [object valueForKey:@"curriculum_id"];
    NSString *curriculum_title = [object valueForKey:@"curriculum_title"];
    NSManagedObjectContext *ctx = object.managedObjectContext;
    
    if ([list count] > 0) {
        
        BOOL status = [self clearDataForEntity:kPeriodEntity withPredicate:nil context:ctx];
        if (status == YES) {
            NSMutableSet *periods = [NSMutableSet set];
            for (NSDictionary *d in list) {
                
                NSString *period_id = [self stringValue: d[@"period_id"]];
                NSString *name = [self stringValue: d[@"name"]];
                NSString *date_created = [self stringValue: d[@"date_created"]];
                NSString *date_modified = [self stringValue: d[@"date_modified"]];
                NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kPeriodEntity
                                                                    inManagedObjectContext:ctx];
                [mo setValue:curriculum_id forKey:@"curriculum_id"];
                [mo setValue:curriculum_title forKey:@"curriculum_title"];
                [mo setValue:period_id forKey:@"period_id"];
                [mo setValue:name forKey:@"name"];
                [mo setValue:name forKey:@"value"];
                [mo setValue:date_created forKey:@"date_created"];
                [mo setValue:date_modified forKey:@"date_modified"];
                [mo setValue:period_id forKey:@"id"];
                
                [periods addObject:mo];
            }
            [object setValue:[NSSet setWithSet:periods] forKey:@"periods"];
        }
    }
}

- (void)requestQuestionListForTagsForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionListByTags, userid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSDictionary *meta = dictionary[@"_meta"];
              NSString *metaCount = [self stringValue:meta[@"count"]];
              NSString *metaStatus = meta[@"status"];
              
              NSManagedObjectContext *ctx = self.workerContext;
              
              if (dictionary == nil ||
                  dictionary[@"records"] == nil ||
                  [metaCount isEqualToString:@"0"] ||
                  [metaStatus isEqualToString:@"ERROR"]) {
                  
                  [ctx performBlock:^{
                      [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                  }];
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
                  
                  return;
              }
              
              if (dictionary[@"records"] != nil ) {
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {
                      
                      // NSManagedObjectContext *ctx = self.workerContext;
                      
                      [ctx performBlock:^{
                          
                          BOOL status = [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                          if (status) {
                              
                              for (NSDictionary *object in records) {

//                                  NSString *question_tag_id = [self stringValue:object[@"question_tag_id"]];
                                  NSString *question_tag_name = [self stringValue:object[@"question_tag_name"]];
                                  
                                  if ( [self isArrayObject:object[@"data"]] ) {
                                      
                                      NSArray *items = object[@"data"];
                                      for (NSDictionary *d in items) {
                                          
                                          NSLog(@"data : %@", d);
                                          [self processQuestionData:d
                                                        sectionName:question_tag_name
                                                            homeurl:homeurl
                                                            details:NO
                                               managedObjectContext:ctx];
                                      }
                                      
                                  }

                              }
                              
                              [self saveTreeContext:ctx];
                              
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }
                          
                      }];
                      
                      
                  }
                  
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)requestQuestionListForQuestionTypeForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionListByQuestionType, userid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSDictionary *meta = dictionary[@"_meta"];
              NSString *metaCount = [self stringValue:meta[@"count"]];
              NSString *metaStatus = meta[@"status"];
              
              NSManagedObjectContext *ctx = self.workerContext;
              
              if (dictionary == nil ||
                  dictionary[@"records"] == nil ||
                  [metaCount isEqualToString:@"0"] ||
                  [metaStatus isEqualToString:@"ERROR"]) {
                  
                  [ctx performBlock:^{
                      [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                  }];
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
                  
                  return;
              }
              
              if (dictionary[@"records"] != nil ) {
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {
                      
                      // NSManagedObjectContext *ctx = self.workerContext;
                      
                      [ctx performBlock:^{
                          
                          BOOL status = [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                          if (status) {
                              
                              for (NSDictionary *object in records) {
                                  
//                                  NSString *question_type_id = [self stringValue:object[@"question_type_id"]];
                                  NSString *question_type_name = [self stringValue:object[@"question_type_name"]];
                                  
                                  if ( [self isArrayObject:object[@"data"]] ) {
                                      
                                      NSArray *items = object[@"data"];
                                      for (NSDictionary *d in items) {
                                          
                                          NSLog(@"data : %@", d);
                                          [self processQuestionData:d
                                                        sectionName:question_type_name
                                                            homeurl:homeurl
                                                            details:NO
                                               managedObjectContext:ctx];
                                      }
                                      
                                  }
                                  
                              }
                              
                              [self saveTreeContext:ctx];
                              
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }
                          
                      }];
                      
                      
                  }
                  
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)requestQuestionListForDifficultyTypeForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionListByDifficultyType, userid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSDictionary *meta = dictionary[@"_meta"];
              NSString *metaCount = [self stringValue:meta[@"count"]];
              NSString *metaStatus = meta[@"status"];
              
              NSManagedObjectContext *ctx = self.workerContext;
              
              if (dictionary == nil ||
                  dictionary[@"records"] == nil ||
                  [metaCount isEqualToString:@"0"] ||
                  [metaStatus isEqualToString:@"ERROR"]) {
                  
                  [ctx performBlock:^{
                      [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                  }];
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
                  
                  return;
              }
              
              if (dictionary[@"records"] != nil ) {
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {
                      
                      // NSManagedObjectContext *ctx = self.workerContext;
                      
                      [ctx performBlock:^{
                          
                          BOOL status = [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                          if (status) {
                              
                              for (NSDictionary *object in records) {
                                  
//                                  NSString *question_difficulty_level_id = [self stringValue:object[@"question_difficulty_level_id"]];
                                  NSString *question_difficulty_level_name = [self stringValue:object[@"question_difficulty_level_name"]];
                                  
                                  if ( [self isArrayObject:object[@"data"]] ) {
                                      
                                      NSArray *items = object[@"data"];
                                      for (NSDictionary *d in items) {
                                          
                                          NSLog(@"data : %@", d);
                                          [self processQuestionData:d
                                                        sectionName:question_difficulty_level_name
                                                            homeurl:homeurl
                                                            details:NO
                                               managedObjectContext:ctx];
                                      }
                                      
                                  }
                                  
                              }
                              
                              [self saveTreeContext:ctx];
                              
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }
                          
                      }];
                      
                      
                  }
                  
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)requestQuestionListForLearningSkillTypeForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionListByLearningSkill, userid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSDictionary *meta = dictionary[@"_meta"];
              NSString *metaCount = [self stringValue:meta[@"count"]];
              NSString *metaStatus = meta[@"status"];
              
              NSManagedObjectContext *ctx = self.workerContext;
              
              if (dictionary == nil ||
                  dictionary[@"records"] == nil ||
                  [metaCount isEqualToString:@"0"] ||
                  [metaStatus isEqualToString:@"ERROR"]) {
                  
                  [ctx performBlock:^{
                      [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                  }];
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
                  
                  return;
              }
              
              if (dictionary[@"records"] != nil ) {
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {
                      
                      // NSManagedObjectContext *ctx = self.workerContext;
                      
                      [ctx performBlock:^{
                          
                          BOOL status = [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                          if (status) {
                              
                              for (NSDictionary *object in records) {
                                  
//                                  NSString *question_learning_skill_id = [self stringValue:object[@"question_learning_skill_id"]];
                                  NSString *question_learning_skill_name = [self stringValue:object[@"question_learning_skill_name"]];
                                  
                                  if ( [self isArrayObject:object[@"data"]] ) {
                                      
                                      NSArray *items = object[@"data"];
                                      for (NSDictionary *d in items) {
                                          
                                          NSLog(@"data : %@", d);
                                          [self processQuestionData:d
                                                        sectionName:question_learning_skill_name
                                                            homeurl:homeurl
                                                            details:NO
                                               managedObjectContext:ctx];
                                      }
                                  }
                                  
                              }
                              
                              [self saveTreeContext:ctx];
                              
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }
                          
                      }];
                      
                  }
                  
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)requestQuestionListForSharedStatusForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionListBySharedStatus, userid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSDictionary *meta = dictionary[@"_meta"];
              NSString *metaCount = [self stringValue:meta[@"count"]];
              NSString *metaStatus = meta[@"status"];
              
              NSManagedObjectContext *ctx = self.workerContext;
              
              if (dictionary == nil ||
                  dictionary[@"records"] == nil ||
                  [metaCount isEqualToString:@"0"] ||
                  [metaStatus isEqualToString:@"ERROR"]) {
                  
                  [ctx performBlock:^{
                      [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                  }];
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
                  
                  return;
              }
              
              if (dictionary[@"records"] != nil ) {
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {
                      
                      // NSManagedObjectContext *ctx = self.workerContext;
                      
                      [ctx performBlock:^{
                          
                          BOOL status = [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                          if (status) {
                              
                              for (NSDictionary *object in records) {
                                  
                                  NSString *shared_status_name = [self stringValue:object[@"shared_status"]];
                                  
                                  if ( [self isArrayObject:object[@"data"]] ) {
                                      
                                      NSArray *items = object[@"data"];
                                      for (NSDictionary *d in items) {
                                          
                                          NSLog(@"data : %@", d);
                                          [self processQuestionData:d
                                                        sectionName:shared_status_name
                                                            homeurl:homeurl
                                                            details:NO
                                               managedObjectContext:ctx];
                                      }
                                  }
                                  
                              }
                              
                              [self saveTreeContext:ctx];
                              
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }
                          
                      }];
                      
                  }
                  
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)requestQuestionListForUser:(NSString *)userid enableChoice:(BOOL)enable doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSManagedObjectContext *ctx = self.workerContext;
    
    [self clearDataForEntity:kQuestionBankEntity  withPredicate:nil context:ctx];
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionList, userid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil ) {
                  
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {
                      
//                      NSManagedObjectContext *ctx = self.workerContext;
                      [ctx performBlock:^{
                          
                          for (NSDictionary *d in records) {
                              
                              NSLog(@"data : %@", d);
                              [self processQuestionData:d
                                            sectionName:@""
                                                homeurl:homeurl
                                                details:enable
                                   managedObjectContext:ctx];
                          }
                          
                          [self saveTreeContext:ctx];
                          
                      }];
                      
                      if (doneBlock) {
                          doneBlock(YES);
                      }
                      
                  }
              }
          }//end
          
      }];
    
    [task resume];
}

//DUPLICATED FOR QUESTION LIST SELECTION
- (void)requestAssignQuestionListForUser:(NSString *)userid enableChoice:(BOOL)enable doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kQuestionBankEntity  withPredicate:nil context:ctx];
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionList, userid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil ) {
                  
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {
                      
                      //                      NSManagedObjectContext *ctx = self.workerContext;
                      [ctx performBlock:^{
                          
                          for (NSDictionary *d in records) {
                              
                              NSLog(@"data : %@", d);
    //                                                          [self processQuestionData:d
    //                                                                        sectionName:@""
    //                                                                            homeurl:homeurl
    //                                                                            details:enable
    //                                                               managedObjectContext:ctx];
                              NSLog(@"%s data : %@", __PRETTY_FUNCTION__, d);
                              
                              /*
                               
                               "correct_answer_feedback" = "Good Job!";
                               "date_created" = "2015-06-02 09:34:04";
                               "date_modified" = "2015-07-22 06:14:36";
                               "general_feedback" = "General Feedback!";
                               id = 492;
                               "image_url" = "<null>";
                               "is_deleted" = 0;
                               "is_public" = 1;
                               learningSkillsName = Knowledge;
                               "learning_skills_id" = 1;
                               name = wewe;
                               points = "1.00";
                               proficiencyLevelName = "Beginning (B)";
                               "proficiency_level_id" = 1;
                               questionTypeName = "True or False";
                               "question_parent_id" = 0;
                               "question_text" = wwewe;
                               "question_type_id" = 1;
                               tags = "";
                               "user_id" = 35;
                               "wrong_answer_feedback" = "Try again!";
                               
                               */
                              
                              NSString *obj_id = [self stringValue:d[@"id"]];
                              NSString *name = [self stringValue:d[@"name"]];
                              NSString *correct_answer_feedback = [self stringValue:d[@"correct_answer_feedback"]];
                              NSString *date_created = [self stringValue:d[@"date_created"]];
                              NSString *date_modified = [self stringValue:d[@"date_modified"]];
                              NSString *general_feedback = [self stringValue:d[@"general_feedback"]];
                              NSString *image_url = [NSString stringWithFormat:@"%@/%@", homeurl, [self stringValue:d[@"image_url"]] ];
                              NSString *is_deleted = [self stringValue:d[@"is_deleted"]];
                              NSString *is_public = [self stringValue:d[@"is_public"]];
                              NSString *learningSkillsName = [self stringValue:d[@"learningSkillsName"]];
                              NSString *learning_skills_id = [self stringValue:d[@"learning_skills_id"]];
                              NSString *points = [self stringValue:d[@"points"]];
                              NSString *package_type_id = [self stringValue:d[@"package_type_id"]];
                              
                              NSString *proficiencyLevelName = [self stringValue:d[@"proficiencyLevelName"]];
                              if (d[@"difficultyLevelName"] != nil) {
                                  proficiencyLevelName = [self stringValue:d[@"difficultyLevelName"]];
                              }
                              
                              NSString *proficiency_level_id = [self stringValue:d[@"proficiency_level_id"]];
                              if (d[@"difficulty_level_id"] != nil) {
                                  proficiency_level_id = [self stringValue:d[@"difficulty_level_id"]];
                              }
                              
                              NSString *questionTypeName = [self stringValue:d[@"questionTypeName"]];
                              NSString *question_parent_id = [self stringValue:d[@"question_parent_id"]];
                              NSString *question_text = [self stringValue:d[@"question_text"]];
                              NSString *question_type_id = [self stringValue:d[@"question_type_id"]];
                              //                              NSString *tags = [self stringValue:d[@"tags"]];
                              NSString *user_id = [self stringValue:d[@"user_id"]];
                              NSString *wrong_answer_feedback = [self stringValue:d[@"wrong_answer_feedback"]];
                              
                              NSManagedObject *mo = [self getEntity:kQuestionBankEntity attribute:@"id" parameter:obj_id context:ctx];
                              //    NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kQuestionBankEntity inManagedObjectContext:ctx];
                              
                              NSAttributedString *attributted_name = [[NSAttributedString alloc] initWithData:[name dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
                              name = [attributted_name string];
                              
                              NSAttributedString *attributed_question_text = [[NSAttributedString alloc] initWithData:[question_text dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
                              question_text = [attributed_question_text string];
                              
                              [mo setValue:obj_id forKey:@"id"];
                              [mo setValue:name forKey:@"name"];
                              [mo setValue:correct_answer_feedback forKey:@"correct_answer_feedback"];
                              [mo setValue:date_created forKey:@"date_created"];
                              [mo setValue:date_modified forKey:@"date_modified"];
                              [mo setValue:general_feedback forKey:@"general_feedback"];
                              [mo setValue:image_url forKey:@"image_url"];
                              [mo setValue:is_deleted forKey:@"is_deleted"];
                              [mo setValue:is_public forKey:@"is_public"];
                              [mo setValue:learningSkillsName forKey:@"learningSkillsName"];
                              [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
                              [mo setValue:points forKey:@"points"];
                              [mo setValue:proficiencyLevelName forKey:@"proficiencyLevelName"];
                              [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];
                              [mo setValue:questionTypeName forKey:@"questionTypeName"];
                              [mo setValue:question_parent_id forKey:@"question_parent_id"];
                              [mo setValue:question_text forKey:@"question_text"];
                              [mo setValue:question_type_id forKey:@"question_type_id"];
                                                          //                              [mo setValue:tags forKey:@"tags"];
                              [mo setValue:user_id forKey:@"user_id"];
                              [mo setValue:wrong_answer_feedback forKey:@"wrong_answer_feedback"];
                              [mo setValue:package_type_id forKey:@"package_type_id"];
                              
                              NSString *search_string = [NSString stringWithFormat:@"%@ %@ %@", name, question_text, questionTypeName];
                              [mo setValue:[search_string lowercaseString] forKey:@"search_string"];
                              

                              
                              if (enable) {
                                  [self requestDetailsForQuestion:mo doneBlock:nil];
                              }

                          }
                          
                          [self saveTreeContext:ctx];
                          
                      }];
                      
                      if (doneBlock) {
                          doneBlock(YES);
                      }
                      
                  }
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)requestDetailsForQuestion:(NSManagedObject *)mo doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *user_id = [self stringValue:[mo valueForKey:@"user_id"] ];
    NSString *question_id = [self stringValue:[mo valueForKey:@"id"] ];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionDetails, user_id, question_id] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil ) {

                  NSDictionary *records = dictionary[@"records"];
                  
//                  NSLog(@"records : %@", records);
                  
                  if (records.count > 0) {
                      
                      NSManagedObjectContext *ctx = mo.managedObjectContext;
                          [ctx performBlock:^{
                          
                              if ([self isArrayObject: records[@"question"] ]) {
                                  NSDictionary *question = [records[@"question"] lastObject];// DICTIONARY
                                  [self updateQuestionsWithData:question managedObject:mo];
                              }
                              
                              if ([self isArrayObject: records[@"choices"] ]) {
                                  NSArray *choices = records[@"choices"];
                                  [self processChoices:choices managedObject:mo];
                              }
                              
    //                          if ( [self isArrayObject: records[@"subs"] ] ) {
    //                              NSArray *subs = records[@"subs"];
    //                              NSLog(@"subs : %@", subs);
    ////                              [self processSubs:subs managedObject:mo];
    //                          }

                              if ( [self isArrayObject: records[@"tags"] ] ) {
                                  NSArray *tags = records[@"tags"];
                                  [self processTags:tags managedObject:mo];
                              }
                              
                              [self saveTreeContext:ctx];
                              
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }];
                  }
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)requestDetailsForQuestion:(NSManagedObject *)object withNewEntity:(NSString *)entity doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *user_id = [self stringValue:[object valueForKey:@"user_id"] ];
    NSString *question_id = [self stringValue:[object valueForKey:@"id"] ];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionDetails, user_id, question_id] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil ) {
                  
                  NSDictionary *records = dictionary[@"records"];
                  
//                  NSLog(@"records : %@", records);
                  if (records.count > 0) {
                      
                      NSManagedObjectContext *ctx = _workerContext;
                      
                      [ctx performBlock:^{
                          
                          BOOL cleared = [self clearDataForEntity:entity withPredicate:nil context:ctx];
                          
                          if (cleared) {
                              
                              NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:entity
                                                                                  inManagedObjectContext:ctx];
                              
                              if ([self isArrayObject: records[@"question"] ]) {
                                  NSDictionary *question = [records[@"question"] lastObject];// DICTIONARY
                                  [self updateQuestionsWithData:question managedObject:mo];
                              }
                              
                              if ([self isArrayObject: records[@"choices"] ]) {
                                  NSArray *choices = records[@"choices"];
                                  [self processChoices:choices managedObject:mo];
                              }
                              
    //                          if ( [self isArrayObject: records[@"subs"] ] ) {
    //                              NSArray *subs = records[@"subs"];
    //                              NSLog(@"subs : %@", subs);
    ////                              [self processSubs:subs managedObject:mo];
    //                          }
                              
                              if ( [self isArrayObject: records[@"tags"] ] ) {
                                  NSArray *tags = records[@"tags"];
                                  [self processTags:tags managedObject:mo];
                              }
                              
                              [self saveTreeContext:ctx];
                              
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                              
                              
                          }
                          
                      }];
                  }
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)requestDetailsForQuestionPreview:(NSManagedObject *)object withNewEntity:(NSString *)entity doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *user_id = [self stringValue:[object valueForKey:@"user_id"] ];
    NSString *question_id = [self stringValue:[object valueForKey:@"id"] ];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionDetails, user_id, question_id] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil ) {
                  
                  NSDictionary *records = dictionary[@"records"];
                  
                  //                  NSLog(@"records : %@", records);
                  if (records.count > 0) {
                      
                      NSManagedObjectContext *ctx = _workerContext;
                      
                      [ctx performBlock:^{
                          
                          NSManagedObject *mo = [self getEntity:entity attribute:@"id" parameter:question_id context:ctx];
                          
                          if ([self isArrayObject: records[@"question"] ]) {
                              NSDictionary *question = [records[@"question"] lastObject];// DICTIONARY
                              [self updateQuestionsWithData:question managedObject:mo];
                          }
                          
                          if ([self isArrayObject: records[@"choices"] ]) {
                              NSArray *choices = records[@"choices"];
                              [self processChoices:choices managedObject:mo];
                          }
                          
//                          if ( [self isArrayObject: records[@"subs"] ] ) {
//                              NSArray *subs = records[@"subs"];
//                              NSLog(@"subs : %@", subs);
////                              [self processSubs:subs managedObject:mo];
//                          }
                          
                          if ( [self isArrayObject: records[@"tags"] ] ) {
                              NSArray *tags = records[@"tags"];
                              [self processTags:tags managedObject:mo];
                          }
                          
                          [self saveTreeContext:ctx];
                          
                          if (doneBlock) {
                              doneBlock(YES);
                          }
                          
                      }];
                  }
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)updateQuestionsWithData:(NSDictionary *)d managedObject:(NSManagedObject *)mo {
    
    NSString *obj_id = [self stringValue:d[@"id"]];
    NSString *name = [self stringValue:d[@"name"]];
    NSString *correct_answer_feedback = [self stringValue:d[@"correct_answer_feedback"]];
    NSString *date_created = [self stringValue:d[@"date_created"]];
    NSString *date_modified = [self stringValue:d[@"date_modified"]];
    NSString *general_feedback = [self stringValue:d[@"general_feedback"]];
    NSString *image_url = [self stringValue:d[@"image_url"]];
    NSString *is_deleted = [self stringValue:d[@"is_deleted"]];
    NSString *is_public = [self stringValue:d[@"is_public"]];
    NSString *learningSkillsName = [self stringValue:d[@"learningSkillsName"]];
    NSString *learning_skills_id = [self stringValue:d[@"learning_skills_id"]];
    NSString *points = [self stringValue:d[@"points"]];
    NSString *proficiencyLevelName = [self stringValue:d[@"proficiencyLevelName"]];
    
    if (d[@"difficultyLevelName"] != nil) {
        proficiencyLevelName = [self stringValue:d[@"difficultyLevelName"]];
    }
    
    NSString *proficiency_level_id = [self stringValue:d[@"proficiency_level_id"]];
    if (d[@"difficulty_level_id"] != nil) {
        proficiency_level_id = [self stringValue:d[@"difficulty_level_id"]];
    }
    
    NSString *questionTypeName = [self stringValue:d[@"questionTypeName"]];
    NSString *question_parent_id = [self stringValue:d[@"question_parent_id"]];
    NSString *question_text = [self stringValue:d[@"question_text"]];
    NSString *question_type_id = [self stringValue:d[@"question_type_id"]];
//    NSString *tags = [self stringValue:d[@"tags"]];
    NSString *user_id = [self stringValue:d[@"user_id"]];
    NSString *wrong_answer_feedback = [self stringValue:d[@"wrong_answer_feedback"]];
    
    NSAttributedString *attributted_name = [[NSAttributedString alloc] initWithData:[name dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
    
    name = [attributted_name string];
    
    NSAttributedString *attributed_question_text = [[NSAttributedString alloc] initWithData:[question_text dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
    
    question_text = [attributed_question_text string];
    
    [mo setValue:obj_id forKey:@"id"];
    [mo setValue:name forKey:@"name"];
    [mo setValue:correct_answer_feedback forKey:@"correct_answer_feedback"];
    [mo setValue:date_created forKey:@"date_created"];
    [mo setValue:date_modified forKey:@"date_modified"];
    [mo setValue:general_feedback forKey:@"general_feedback"];
    [mo setValue:image_url forKey:@"image_url"];
    [mo setValue:is_deleted forKey:@"is_deleted"];
    [mo setValue:is_public forKey:@"is_public"];
    [mo setValue:learningSkillsName forKey:@"learningSkillsName"];
    [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
    [mo setValue:points forKey:@"points"];
    [mo setValue:proficiencyLevelName forKey:@"proficiencyLevelName"];
    [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];
    [mo setValue:questionTypeName forKey:@"questionTypeName"];
    [mo setValue:question_parent_id forKey:@"question_parent_id"];
    [mo setValue:question_text forKey:@"question_text"];
    [mo setValue:question_type_id forKey:@"question_type_id"];
//    [mo setValue:tags forKey:@"tags"];
    [mo setValue:user_id forKey:@"user_id"];
    [mo setValue:wrong_answer_feedback forKey:@"wrong_answer_feedback"];
}

- (void)insertEntity:(NSString *)entity userData:(NSDictionary *)userData {
    
    NSManagedObjectContext *ctx = _workerContext;
    
    if (userData != nil) {
        NSString *code = [userData valueForKey:@"code"];
        NSManagedObject *mo = [self getEntity:kCompetencyHistoryEntity attribute:@"code" parameter:code context:ctx];
        [ctx performBlock:^{
            for (NSString *key in [userData allKeys]) {
                [mo setValue:userData[key] forKey:key];
            }
            [self saveTreeContext:ctx];
        }];
    }
}

- (void)updateEntity:(NSString *)entity details:(NSDictionary *)dictionary predicate:(NSPredicate *)predicate {
    
    NSManagedObject *mo = [self getEntity:entity predicate:predicate];
    NSManagedObjectContext *ctx = mo.managedObjectContext;
    if (mo != nil) {
        
        [ctx performBlock:^{
            for (NSString *key in [dictionary allKeys]) {
                NSString *v = [NSString stringWithFormat:@"%@", dictionary[key] ];
                NSString *value = [self normalizeStringObject:v];
                [mo setValue:value forKey:key];
            }
            
            if ([entity isEqualToString:kChoiceItemEntity]) {
                NSManagedObject *q_mo = [mo valueForKey:@"question"];
                [self validateQuestionObject:q_mo];
            }
            
            [self saveTreeContext:ctx];
        }];
    }
}

- (void)updateChoiceItemForQuestion:(NSString *)questionid updateDetails:(NSDictionary *)dictionary predicate:(NSPredicate *)predicate {
    
    // PERFORM TO CLEAR CHECK STATES
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kChoiceItemEntity];
    [fetchRequest setPredicate: [self predicateForKeyPath:@"question_id" andValue:questionid] ];
    NSManagedObjectContext *context = _workerContext;
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count] > 0) {
        for (NSManagedObject *ci in items) {
            [ci setValue:@"0" forKey:@"is_correct"];
            [self saveTreeContext:ci.managedObjectContext];
        }
    }
    
    // PERFORM SETTING THE CORRECT ANSER
    NSManagedObject *mo = [self getEntity:kChoiceItemEntity predicate:predicate];
    NSManagedObjectContext *ctx = mo.managedObjectContext;
    if (mo != nil) {
        
        [ctx performBlock:^{
            for (NSString *key in [dictionary allKeys]) {
                NSString *v = [NSString stringWithFormat:@"%@", dictionary[key] ];
                NSString *value = [self normalizeStringObject:v];
                [mo setValue:value forKey:key];
            }
            NSManagedObject *q_mo = [mo valueForKey:@"question"];
            [self validateQuestionObject:q_mo];
            [self saveTreeContext:ctx];
        }];
    }
}

- (void)updateQuestion:(NSString *)entity predicate:(NSPredicate *)predicate details:(NSDictionary *)details {
    
    NSManagedObject *mo = [self getEntity:entity predicate:predicate];

    NSManagedObjectContext *ctx = mo.managedObjectContext;
    if (mo != nil) {
        NSArray *keys = [details allKeys];
        for (NSString *k in keys) {
            [mo setValue:details[k] forKey:k];
        }
        
//        if ([entity isEqualToString:kQuestionEntity]) {
//            [self validateQuestionObject:mo];
//        }
        
        if ([entity isEqualToString:kQuestionEntity]) {
            NSString *question_type_id = [self stringValue:[mo valueForKey:@"question_type_id"]];
            
            // FILL IN THE BLANKS AND IDENTIFICATION
            if ([question_type_id isEqualToString:@"2"] || [question_type_id isEqualToString:@"10"]) {
                NSString *question_id = [self stringValue:[mo valueForKey:@"id"]];
                NSPredicate *predicate = [self predicateForKeyPath:@"question_id" object:question_id];
                NSManagedObject *choice = [self getEntity:kChoiceItemEntity predicate:predicate];
                [self validateCompletionOfQuestionWithChoiceObject:choice];
            }
            // ALL OTHER QUESTION TYPES
            else {
               [self validateQuestionObject:mo];
            }
        }
        
        [self saveTreeContext:ctx];
    }
}

- (void)validateQuestionObject:(NSManagedObject *)mo {
  
    if ([mo.entity.name isEqualToString:kChoiceItemEntity]) {
        mo = [mo valueForKey:@"question"];
    }
    
    NSString *question_title = [self stringValue: [mo valueForKey:@"name"] ];
    NSString *question_description = [self stringValue: [mo valueForKey:@"question_text"] ];
    NSString *question_points = [self stringValue: [mo valueForKey:@"points"] ];
    NSString *proficiency_level_id = [self stringValue: [mo valueForKey:@"proficiency_level_id"] ];
    NSString *learning_skills_id = [self stringValue: [mo valueForKey:@"learning_skills_id"] ];
    
    NSSet *choice_set = [mo valueForKey:@"choices"];
    
    BOOL isComplete = NO;
    
    if ( (question_title.length > 0) &&
        (question_description.length > 0) &&
        (question_points.length > 0) &&
        (proficiency_level_id.length > 0) &&
        (learning_skills_id.length > 0) ) {
        isComplete = YES;
    }
    
    // IF MULTIPLE CHOICE OR TOF ONLY
    if (choice_set.count > 0) {
        for (NSManagedObject *c_mo in choice_set) {
            NSString *choice_text = [self stringValue:[c_mo valueForKey:@"text"] ];
            NSString *choice_image = [c_mo valueForKey:@"choice_image"];
            choice_text = [self normalizeStringObject:choice_text];
            if ((choice_text.length == 0)) {
                if (choice_image.length == 0) {
                    isComplete = NO;
                }
            }
        }
        
        NSPredicate *predicate = [self predicateForKeyPath:@"is_correct" andValue:@"100"];
        NSSet *filteredSet = [choice_set filteredSetUsingPredicate:predicate];
        
        if (filteredSet.count == 0) {
            isComplete = NO;
        }
    }
    
    NSString *flag = [NSString stringWithFormat:@"%@", @(isComplete) ];
    [mo setValue:flag forKey:@"complete"];
    
    NSNotificationCenter *notif = [NSNotificationCenter defaultCenter];
    [notif postNotificationName:kNotificationQuestionUpdate object:flag];
    
    //return isComplete;
}

- (BOOL)clearChoicesCheckStatusForQuestion:(NSString *)questionId {
    
    NSDictionary *properties = @{@"is_correct":@"0"};
    
    //PERFORM BATCH UPDATE
    NSBatchUpdateRequest *batchRequest = [NSBatchUpdateRequest batchUpdateRequestWithEntityName:kChoiceItemEntity];
    batchRequest.propertiesToUpdate = properties;
    batchRequest.predicate = [self predicateForKeyPath:@"question_id" andValue:questionId];
    batchRequest.resultType = NSUpdatedObjectsCountResultType;
    NSError *error = nil;
    NSManagedObjectContext *ctx = _workerContext;
    NSBatchUpdateResult *batchResult = (NSBatchUpdateResult *)[ctx executeRequest:batchRequest error:&error];
    if (error) {
        NSLog(@"%s ERROR %@", __PRETTY_FUNCTION__, [error localizedDescription]);
        return NO;
    }
    NSLog(@"%@ objects updated", batchResult.result);
    //PERFORM FETCH OF QUESTIONS
    
    return YES;
}

- (void)updateQuestionChoiceText:(NSString *)text withPredicate:(NSPredicate *)predicate {
    
    NSString *value = [NSString stringWithFormat:@"%@", text];//[self normalizeStringObject:text];
    NSManagedObject *mo = [self getEntity:kChoiceItemEntity predicate:predicate];
    
    if (mo != nil) {
        NSManagedObject *q_mo = [mo valueForKey:@"question"];
        
        [mo setValue:value forKey:@"text"];
        [self validateQuestionObject:q_mo];
        [self saveTreeContext:mo.managedObjectContext];
    }
}

- (void)processChoices:(NSArray *)choices managedObject:(NSManagedObject *)mo {
    
    NSManagedObjectContext *ctx = mo.managedObjectContext;
    
    NSMutableSet *choice_sets = [NSMutableSet set];
    if (choices.count > 0) {
        
//        NSLog(@"XXX QUESTION NAME : %@", [mo valueForKey:@"name"] );
//        NSLog(@"XXX QUESTION TYPE : %@", [mo valueForKey:@"questionTypeName"] );
//        NSLog(@"XXX QUESTION TYPE ID : %@", [mo valueForKey:@"question_type_id"] );
//        NSLog(@"XXX choices : %@", choices);
        
        for (NSDictionary *c in choices) {
            NSString *date_created = [self stringValue: c[@"date_created"] ];
            NSString *date_modified = [self stringValue: c[@"date_modified"] ];
            NSString *choice_id = [self stringValue: c[@"id"] ];
            NSString *is_correct = [self stringValue: c[@"is_correct"] ];
            NSString *is_deleted = [self stringValue: c[@"is_deleted"] ];
            NSString *order_number = [self stringValue: c[@"order_number"] ];
            NSNumber *order = [NSNumber numberWithDouble:[order_number doubleValue] ];
            NSString *question_id = [self stringValue: c[@"question_id"] ];
            NSString *suggestive_feedback = [self stringValue: c[@"suggestive_feedback"] ];
            NSString *choice_text = [self stringValue: c[@"text"] ];
            
            NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:kChoiceItemEntity inManagedObjectContext:ctx];
            [c_mo setValue:date_created forKey:@"date_created"];
            [c_mo setValue:date_modified forKey:@"date_modified"];
            [c_mo setValue:choice_id forKey:@"id"];
            [c_mo setValue:is_correct forKey:@"is_correct"];
            [c_mo setValue:is_deleted forKey:@"is_deleted"];
            [c_mo setValue:order forKey:@"order_number"];
            [c_mo setValue:question_id forKey:@"question_id"];
            [c_mo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
            [c_mo setValue:choice_text forKey:@"text"];
            
            [choice_sets addObject:c_mo];
        }
     
        // assign choices
        [mo setValue:[NSSet setWithSet:choice_sets] forKey:@"choices"];
    }
}

- (void)processSubs:(NSArray *)subs managedObject:(NSManagedObject *)mo {
    
//    NSManagedObjectContext *ctx = mo.managedObjectContext;
    
    if (subs.count > 0) {
        
//        for (NSDictionary *s in subs) {
//            
//            NSArray *answers = s[@"answer"];
//            for (NSDictionary *a in answers) {
//                
//                /*
//                 dateCreated = "2015-06-26 05:10:27";
//                 dateModified = "2015-06-26 05:10:27";
//                 id = 2612;
//                 isCorrect = 100;
//                 isDeleted = 0;
//                 orderNumber = 1;
//                 suggestiveFeedback = "suggestive feedback ACME 11";
//                 text = Choice11;
//                 */
//                
//                NSString *dateCreated = [self stringValue:a[@"dateCreated"]];
//                NSString *dateModified = [self stringValue:a[@"dateModified"]];
//                NSString *answer_id = [self stringValue:a[@"id"]];
//                NSString *isCorrect = [self stringValue:a[@"isCorrect"]];
//                NSString *isDeleted = [self stringValue:a[@"isDeleted"]];
//                NSString *orderNumber = [self stringValue:a[@"orderNumber"]];
//                NSString *suggestiveFeedback = [self stringValue:a[@"suggestiveFeedback"]];
//                NSString *answer_text = [self stringValue:a[@"text"]];
//            }
//        }
        
    }
}

- (void)processTags:(NSArray *)tags managedObject:(NSManagedObject *)mo {
    
    NSManagedObjectContext *ctx = mo.managedObjectContext;
    
    NSMutableSet *tag_sets = [NSMutableSet set];
    if (tags.count > 0) {
        
        for (NSDictionary *t in tags) {
            NSString *tag_id = [self stringValue: t[@"id"] ];
            NSString *tag_name = [self stringValue: t[@"tag"] ];
            
            NSManagedObject *t_mo = [NSEntityDescription insertNewObjectForEntityForName:kTagEntity inManagedObjectContext:ctx];
            [t_mo setValue:tag_id forKey:@"id"];
            [t_mo setValue:tag_name forKey:@"tag"];
            
            [tag_sets addObject:t_mo];
        }
        
        // assign tags
        [mo setValue:[NSSet setWithSet:tag_sets] forKey:@"tags"];
    }
}

- (NSSet *)processTags:(NSArray *)list {
    
    NSManagedObjectContext *ctx = _workerContext;
    
    NSMutableSet *tag_sets = [NSMutableSet set];
    if (list.count > 0) {
        
        for (NSManagedObject *t in list) {
            NSString *tag_value = [self stringValue: [t valueForKey:@"tag"] ];
            NSString *question_id = [self stringValue: [t valueForKey:@"question_id"] ];
            NSManagedObject *t_mo = [NSEntityDescription insertNewObjectForEntityForName:kTagEntity inManagedObjectContext:ctx];
            [t_mo setValue:tag_value forKey:@"tag"];
            [t_mo setValue:question_id forKey:@"question_id"];
            [tag_sets addObject:t_mo];
        }
    }
    
    return tag_sets;
}

- (NSDictionary *)questionDataFromManagedObject:(NSManagedObject *)mo update:(BOOL)update {
    
    //----------------
    // QUESTION OBJECT
    //----------------
    NSString *question_id = [self stringValue: [mo valueForKey:@"id"] ];
    NSString *points = [self stringValue: [mo valueForKey:@"points"] ];
    NSString *question_text = [self stringValue: [mo valueForKey:@"question_text"] ];
    NSString *question_type_id = [self stringValue: [mo valueForKey:@"question_type_id"] ];
    NSString *proficiency_level_id = [self stringValue: [mo valueForKey:@"proficiency_level_id"] ];
    NSString *correct_answer_feedback = [self stringValue: [mo valueForKey:@"correct_answer_feedback"] ];
    NSString *learning_skills_id = [self stringValue: [mo valueForKey:@"learning_skills_id"] ];
    NSString *wrong_answer_feedback = [self stringValue: [mo valueForKey:@"wrong_answer_feedback"] ];
    NSString *general_feedback = [self stringValue: [mo valueForKey:@"general_feedback"] ];
    NSString *is_public = [self stringValue: [mo valueForKey:@"is_public"] ];
    NSString *question_parent_id = [self stringValue: [mo valueForKey:@"question_parent_id"] ];
    
    NSString *name = [self stringValue: [mo valueForKey:@"name"] ];
    
    NSMutableDictionary *question_main_data = [@{
                                                 @"points": points,
                                                 @"question_text": question_text,
                                                 @"question_type_id": question_type_id,
                                                 @"proficiency_level_id": proficiency_level_id,
                                                 @"difficulty_level_id": proficiency_level_id,// BUG FIX
                                                 @"correct_answer_feedback": correct_answer_feedback,
                                                 @"learning_skills_id": learning_skills_id,
                                                 @"wrong_answer_feedback": wrong_answer_feedback,
                                                 @"general_feedback": general_feedback,
                                                 @"is_public": is_public,
                                                 @"question_parent_id": question_parent_id,
                                                 @"name": name
                                                 } mutableCopy];
    if (update) {
        question_main_data[@"id"] = question_id;
    }
    
//    NSDictionary *question_main_data = @{@"id": question_id,
//                                         @"points": points,
//                                         @"question_text": question_text,
//                                         @"question_type_id": question_type_id,
//                                         @"proficiency_level_id": proficiency_level_id,
//                                         @"correct_answer_feedback": correct_answer_feedback,
//                                         @"learning_skills_id": learning_skills_id,
//                                         @"wrong_answer_feedback": wrong_answer_feedback,
//                                         @"general_feedback": general_feedback,
//                                         @"is_public": is_public,
//                                         @"question_parent_id": question_parent_id,
//                                         @"name": name};
    
    //----------------
    // CHOICE DATA
    //----------------
    NSSet *choice_sets = [mo valueForKey:@"choices"];
    NSArray *choices = [choice_sets allObjects];
    NSMutableArray *choice_list = [NSMutableArray array];
    
    NSLog(@"choices: %@", choices);
    NSLog(@"choices.count: %lu", (unsigned long)choices.count);
    
    // True or False and Multiple Choice
    if ([question_type_id isEqualToString:@"1"] || [question_type_id isEqualToString:@"3"]) {
        if (choices.count > 1) {
            NSLog(@"VALID NUMBER OF CHOICES");
            int countCorrect = 0;
            
            for (NSManagedObject *cmo in choices) {
                NSString *is_correct = [self stringValue: [cmo valueForKey:@"is_correct"]];
                
                if ([is_correct isEqualToString:@"100"]) {
                    countCorrect++;
                }
            }
            
            if (countCorrect != 1) {
                NSLog(@"NO CORRECT ANSWER OR CORRECT ANSWERS ARE MORE THAN 1");
                return nil;
            }
        }
        else {
            NSLog(@"NOT ENOUGH CHOICES");
            return nil;
        }
    }
    
    NSLog(@"NAKALABAS NA AKO!!!");
    
    for (NSManagedObject *cmo in choices) {
        NSString *text = [self stringValue: [cmo valueForKey:@"text"] ];
        NSString *suggestive_feedback = [self stringValue: [cmo valueForKey:@"suggestive_feedback"] ];
        NSString *order_number = [self stringValue: [cmo valueForKey:@"order_number"] ];
        NSString *is_correct = [self stringValue: [cmo valueForKey:@"is_correct"] ];
        is_correct = [is_correct isEqualToString:@"100"] ? @"1" : @"0";
        NSDictionary *item = @{ @"text":text,
                                @"suggestive_feedback":suggestive_feedback,
                                @"order_number":order_number,
                                @"is_correct":is_correct };
        [choice_list addObject:item];
    }
    
    //----------------
    // SUBS DATA
    //----------------
    NSArray *subs = [NSArray array];
    
    //----------------
    // TAGS DATA
    //----------------
    NSSet *tag_sets = [mo valueForKey:@"tags"];
    NSArray *tags = [tag_sets allObjects];
    NSMutableArray *tag_list = [NSMutableArray array];
    for (NSManagedObject *tmo in tags) {
        NSString *tag_name = [self stringValue: [tmo valueForKey:@"tag"] ];
        NSDictionary *item = @{ @"tag":tag_name };
        [tag_list addObject:item];
    }

    //----------------
    // IMAGE DATA
    //----------------
    
    NSString *image_one = [self stringValue: [mo valueForKey:@"image_one"] ];
    NSString *image_two = [self stringValue: [mo valueForKey:@"image_two"] ];
    NSString *image_three = [self stringValue: [mo valueForKey:@"image_three"] ];
    NSString *image_four = [self stringValue: [mo valueForKey:@"image_four"] ];
//    NSDictionary *image_data1 = @{@"index":@"1",@"image":[self urlEncode:image_one]};
//    NSDictionary *image_data2 = @{@"index":@"2",@"image":[self urlEncode:image_two]};
//    NSDictionary *image_data3 = @{@"index":@"3",@"image":[self urlEncode:image_three]};
//    NSDictionary *image_data4 = @{@"index":@"4",@"image":[self urlEncode:image_four]};
    NSDictionary *image_data1 = @{@"index":@"1",@"image":image_one};
    NSDictionary *image_data2 = @{@"index":@"2",@"image":image_two};
    NSDictionary *image_data3 = @{@"index":@"3",@"image":image_three};
    NSDictionary *image_data4 = @{@"index":@"4",@"image":image_four};
    NSArray *question_images = @[image_data1, image_data2, image_data3, image_data4];
    
    //----------------
    // QUESTION DATA
    //----------------
    NSDictionary *question_object = @{@"question":question_main_data,
                                      @"choices":choice_list,
                                      @"subs":subs,
                                      @"tags":tag_list,
                                      @"question_images":question_images};
    return question_object;
}

- (NSDictionary *)questionDataFromManagedObject:(NSManagedObject *)mo index:(NSInteger)index update:(BOOL)update {
    
    NSInteger increment = index + 1;
    NSString *increment_string = [NSString stringWithFormat:@"%@", @(increment) ];
    NSString *question_key = [NSString stringWithFormat:@"question%@_", increment_string];
    NSString *question_choice_key = [NSString stringWithFormat:@"%@choice_image", question_key ];
    
    //----------------
    // QUESTION OBJECT
    //----------------
    NSString *question_id = [self stringValue: [mo valueForKey:@"id"] ];
    NSString *points = [self stringValue: [mo valueForKey:@"points"] ];
    NSString *question_text = [self stringValue: [mo valueForKey:@"question_text"] ];
    NSString *question_type_id = [self stringValue: [mo valueForKey:@"question_type_id"] ];
    NSString *proficiency_level_id = [self stringValue: [mo valueForKey:@"proficiency_level_id"] ];
    NSString *correct_answer_feedback = [self stringValue: [mo valueForKey:@"correct_answer_feedback"] ];
    NSString *learning_skills_id = [self stringValue: [mo valueForKey:@"learning_skills_id"] ];
    NSString *wrong_answer_feedback = [self stringValue: [mo valueForKey:@"wrong_answer_feedback"] ];
    NSString *general_feedback = [self stringValue: [mo valueForKey:@"general_feedback"] ];
    NSString *is_public = [self stringValue: [mo valueForKey:@"is_public"] ];
    NSString *question_parent_id = [self stringValue: [mo valueForKey:@"question_parent_id"] ];
    
    NSString *name = [self stringValue: [mo valueForKey:@"name"] ];
    NSString *is_case_sensitive = [self stringValue:[mo valueForKey:@"is_case_sensitive"]];
    
    //-------------------
    // INTEGRATION ITEMS
    //-------------------
    NSString *package_id = @"0";
    NSString *competency_code = [self stringValue: [mo valueForKey:@"competency_code"] ];
    NSString *package_type_id = [self stringValue: [mo valueForKey:@"package_type_id"] ];
    NSString *course_id = [self stringValue: [mo valueForKey:@"course_id"] ];
    
    //-------------------
    // DELETE IMAGE PARSING
    //-------------------
    NSString *token_string = [self stringValue: [mo valueForKey:@"delete_images"] ];
    NSArray *delete_images = [token_string componentsSeparatedByString:@"|"];
    
    delete_images = [delete_images valueForKeyPath:@"@distinctUnionOfObjects.self"];
    
    NSLog(@"ARRAY >%@<", delete_images);
    
    NSMutableDictionary *question_main_data = [@{
                                                 @"points": points,
                                                 @"question_text": question_text,
                                                 @"question_type_id": question_type_id,
                                                 @"package_id": package_id,
                                                 @"difficulty_level_id": proficiency_level_id,// BUG FIX
                                                 @"correct_answer_feedback": correct_answer_feedback,
                                                 @"learning_skills_id": learning_skills_id,
                                                 @"wrong_answer_feedback": wrong_answer_feedback,
                                                 @"general_feedback": general_feedback,
                                                 @"is_public": is_public,
                                                 @"question_parent_id": question_parent_id,
                                                 @"name": name,
                                                 @"competency_code": competency_code,
                                                 @"package_type_id": package_type_id,
                                                 @"course_id": course_id,
                                                 @"is_case_sensitive":is_case_sensitive
                                                 } mutableCopy];
    
    if (token_string.length > 0) {
        question_main_data[@"delete_images"] = delete_images;
    }
    
    if (update) {
        question_main_data[@"id"] = question_id;
    }
    
    //----------------
    // CHOICE DATA
    //----------------
    NSSet *choice_sets = [mo valueForKey:@"choices"];
    NSArray *choices = [choice_sets allObjects];
    NSMutableArray *choice_list = [NSMutableArray array];
    NSMutableDictionary *choice_image_meta = [NSMutableDictionary dictionary];
    
    // Fill in the blanks and Identification
    if ([question_type_id isEqualToString:@"2"] || [question_type_id isEqualToString:@"10"]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"question_id == %@", question_id];
        choices = [self getObjectsForEntity:kChoiceItemEntity predicate:predicate];
    }
    
    // True or False and Multiple Choice
    if ([question_type_id isEqualToString:@"1"] || [question_type_id isEqualToString:@"3"]) {
        if (choices.count > 1) {
            NSLog(@"VALID NUMBER OF CHOICES");
            int countCorrect = 0;
            
            for (NSManagedObject *cmo in choices) {
                NSString *is_correct = [self stringValue: [cmo valueForKey:@"is_correct"]];
                
                if ([is_correct isEqualToString:@"100"]) {
                    countCorrect++;
                }
            }
            
            if (countCorrect < 1) {
                NSLog(@"NO CORRECT ANSWER OR CORRECT ANSWERS ARE MORE THAN 1");
                return nil;
            }
        }
        else {
            NSLog(@"NOT ENOUGH CHOICES");
            return nil;
        }
    }
    
    NSLog(@"NAKALABAS NA AKO!!!");
    
    for (NSInteger i = 0; i < [choices count]; i++) {
        
        NSInteger inc = i + 1;
        NSString *increment_choice_string = [NSString stringWithFormat:@"%@%@", question_choice_key, @(inc) ];
        
        NSManagedObject *cmo = (NSManagedObject *)choices[i];
        
        NSString *choice_id = [self stringValue: [cmo valueForKey:@"id"] ];
        NSString *text = [self stringValue: [cmo valueForKey:@"text"] ];
        NSString *suggestive_feedback = [self stringValue: [cmo valueForKey:@"suggestive_feedback"] ];
        NSString *order_number = [self stringValue: [cmo valueForKey:@"order_number"] ];
        NSString *is_correct = [self stringValue: [cmo valueForKey:@"is_correct"] ];
        is_correct = [is_correct isEqualToString:@"100"] ? @"1" : @"0";
        NSString *choice_image = [self stringValue: [cmo valueForKey:@"choice_image"] ];
        NSString *delete_image = (choice_image.length > 0) ? @"0" : @"1";
        
        NSMutableDictionary *item = [@{@"text":text,
                                       @"suggestive_feedback":suggestive_feedback,
                                       @"order_number":order_number,
                                       @"is_correct":is_correct,
                                       @"delete_image":delete_image} mutableCopy];
        
        if ((update == YES) && (![choice_id containsString:@"-"])) {
            item[@"id"] = choice_id;
        }
        
        NSLog(@"items [%@]", item);
        
        /*
         "text": "Choice ACME Ya",
         "suggestive_feedback": "suggestive feedback ACME Ya",
         "is_correct": "0",
         "order_number": "1"
         */
        [choice_list addObject:item];
        
        NSData *choice_image_data = [cmo valueForKey:@"choice_image_data"];
        if (choice_image_data != nil) {
            if ([delete_image isEqualToString:@"0"]) {
                [choice_image_meta setObject:choice_image_data forKey:increment_choice_string];
            }
        }
    }
    
    //----------------
    // SUBS DATA
    //----------------
    //    NSArray *subs = [NSArray array];
    
    //----------------
    // TAGS DATA
    //----------------
    NSSet *tag_sets = [mo valueForKey:@"tags"];
    NSArray *tags = [tag_sets allObjects];
    NSMutableArray *tag_list = [NSMutableArray array];
    for (NSManagedObject *tmo in tags) {
        NSString *tag_name = [self stringValue: [tmo valueForKey:@"tag"] ];
        NSDictionary *item = @{ @"tag":tag_name };
        [tag_list addObject:item];
    }
    
    //----------------
    // IMAGE DATA
    //----------------
    NSData *image_one_data = [mo valueForKey:@"image_one_data"];
    NSData *image_two_data = [mo valueForKey:@"image_two_data"];
    NSData *image_three_data = [mo valueForKey:@"image_three_data"];
    NSData *image_four_data = [mo valueForKey:@"image_four_data"];
    NSString *image_key_one = [NSString stringWithFormat:@"%@image1",question_key];
    NSString *image_key_two = [NSString stringWithFormat:@"%@image2",question_key];
    NSString *image_key_three = [NSString stringWithFormat:@"%@image3",question_key];
    NSString *image_key_four = [NSString stringWithFormat:@"%@image4",question_key];
    
    ////-----------------
    //// CHECK IF DELETED
    ////-----------------
    for (NSString *deleteIndex in delete_images) {
        if ([deleteIndex  isEqualToString:@"1"]) {
            image_one_data = nil;
        }
        if ([deleteIndex  isEqualToString:@"2"]) {
            image_two_data = nil;
        }
        if ([deleteIndex  isEqualToString:@"3"]) {
            image_three_data = nil;
        }
        if ([deleteIndex  isEqualToString:@"4"]) {
            image_four_data = nil;
        }
    }
    
    NSMutableDictionary *question_images = [NSMutableDictionary dictionary];
    if (image_one_data != nil) {
        [question_images setObject:image_one_data forKey:image_key_one];
    }
    
    if (image_two_data != nil) {
        [question_images setObject:image_two_data forKey:image_key_two];
    }
    
    if (image_three_data != nil) {
        [question_images setObject:image_three_data forKey:image_key_three];
    }
    
    if (image_four_data != nil) {
        [question_images setObject:image_four_data forKey:image_key_four];
    }
    
    //----------------
    // QUESTION DATA
    //----------------
    //    NSDictionary *question_object = @{@"question":question_main_data,
    //                                      @"choices":choice_list,
    //                                      @"subs":subs,
    //                                      @"tags":tag_list};
    
    NSDictionary *question_object = @{@"question":question_main_data,
                                      @"choices":choice_list,
                                      @"tags":tag_list};
    
    NSMutableDictionary *user_data = [NSMutableDictionary dictionary];
    [user_data setObject:question_object forKey:@"question_data"];
    [user_data setObject:question_images forKey:@"question_images"];
    
    if ([choices count] > 0) {
        [user_data setObject:choice_image_meta forKey:@"question_choice_images"];
    }
    
    //    NSLog(@"GENERATE : %@", user_data);
    
    return user_data;
}

- (NSDictionary *)questionDataFromManagedObject:(NSManagedObject *)mo update:(BOOL)update withTags:(NSSet *)tagSet {
    
    //----------------
    // QUESTION OBJECT
    //----------------
    NSString *question_id = [self stringValue: [mo valueForKey:@"id"] ];
    NSString *points = [self stringValue: [mo valueForKey:@"points"] ];
    NSString *question_text = [self stringValue: [mo valueForKey:@"question_text"] ];
    NSString *question_type_id = [self stringValue: [mo valueForKey:@"question_type_id"] ];
    NSString *proficiency_level_id = [self stringValue: [mo valueForKey:@"proficiency_level_id"] ];
    NSString *correct_answer_feedback = [self stringValue: [mo valueForKey:@"correct_answer_feedback"] ];
    NSString *learning_skills_id = [self stringValue: [mo valueForKey:@"learning_skills_id"] ];
    NSString *wrong_answer_feedback = [self stringValue: [mo valueForKey:@"wrong_answer_feedback"] ];
    NSString *general_feedback = [self stringValue: [mo valueForKey:@"general_feedback"] ];
    NSString *is_public = [self stringValue: [mo valueForKey:@"is_public"] ];
    NSString *question_parent_id = [self stringValue: [mo valueForKey:@"question_parent_id"] ];
    NSString *name = [self stringValue: [mo valueForKey:@"name"] ];
    NSString *package_type_id = [self stringValue: [mo valueForKey:@"package_type_id"] ];
    
    NSMutableDictionary *question_main_data = [@{
                                                 @"points": points,
                                                 @"question_text": question_text,
                                                 @"question_type_id": question_type_id,
                                                 @"proficiency_level_id": proficiency_level_id,
                                                 @"difficulty_level_id": proficiency_level_id,// BUG FIX
                                                 @"correct_answer_feedback": correct_answer_feedback,
                                                 @"learning_skills_id": learning_skills_id,
                                                 @"wrong_answer_feedback": wrong_answer_feedback,
                                                 @"general_feedback": general_feedback,
                                                 @"is_public": is_public,
                                                 @"question_parent_id": question_parent_id,
                                                 @"name": name,
                                                 @"package_type_id" : package_type_id
                                                 } mutableCopy];
    if (update) {
        question_main_data[@"id"] = question_id;
    }
    
    //----------------
    // CHOICE DATA
    //----------------
    NSSet *choice_sets = [mo valueForKey:@"choices"];
    NSArray *choices = [choice_sets allObjects];
    NSMutableArray *choice_list = [NSMutableArray array];
    
    NSLog(@"choices: %@", choices);
    NSLog(@"choices.count: %lu", (unsigned long)choices.count);
    
    // True or False and Multiple Choice
    if ([question_type_id isEqualToString:@"1"] || [question_type_id isEqualToString:@"3"]) {
        if (choices.count > 1) {
            NSLog(@"VALID NUMBER OF CHOICES");
            int countCorrect = 0;
            
            for (NSManagedObject *cmo in choices) {
                NSString *is_correct = [self stringValue: [cmo valueForKey:@"is_correct"]];
                
                if ([is_correct isEqualToString:@"100"]) {
                    countCorrect++;
                }
            }
            
            if (countCorrect != 1) {
                NSLog(@"NO CORRECT ANSWER OR CORRECT ANSWERS ARE MORE THAN 1");
                return nil;
            }
        }
        else {
            NSLog(@"NOT ENOUGH CHOICES");
            return nil;
        }
    }
    
    NSLog(@"NAKALABAS NA AKO!!!");
    
    for (NSManagedObject *cmo in choices) {
        NSString *text = [self stringValue: [cmo valueForKey:@"text"] ];
        NSString *suggestive_feedback = [self stringValue: [cmo valueForKey:@"suggestive_feedback"] ];
        NSString *order_number = [self stringValue: [cmo valueForKey:@"order_number"] ];
        NSString *is_correct = [self stringValue: [cmo valueForKey:@"is_correct"] ];
        is_correct = [is_correct isEqualToString:@"100"] ? @"1" : @"0";
        NSDictionary *item = @{ @"text":text,
                                @"suggestive_feedback":suggestive_feedback,
                                @"order_number":order_number,
                                @"is_correct":is_correct };
        [choice_list addObject:item];
    }
    
    //----------------
    // SUBS DATA
    //----------------
    NSArray *subs = [NSArray array];
    
    //----------------
    // TAGS DATA
    //----------------
    NSArray *tags = [tagSet allObjects];
    NSMutableArray *tag_list = [NSMutableArray array];
    for (NSManagedObject *tmo in tags) {
        NSString *tag_name = [self stringValue: [tmo valueForKey:@"tag"] ];
        NSDictionary *item = @{ @"tag":tag_name };
        [tag_list addObject:item];
    }
    
    
    //----------------
    // QUESTION DATA
    //----------------
    NSDictionary *question_object = @{@"question":question_main_data,
                                      @"choices":choice_list,
                                      @"subs":subs,
                                      @"tags":tag_list };
    return question_object;
}

- (void)requestUpdateDetailsForQuestion:(NSManagedObject *)mo doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *user_id = [self stringValue: [mo valueForKey:@"user_id"] ];
    NSDictionary *dictionary_body = [self questionDataFromManagedObject:mo update:YES];
    NSArray *body = nil;
    if (dictionary_body != nil) {
        body = @[ dictionary_body ];
    }
    
    if (body != nil) {
        NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionUpdate, user_id] ];
        NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:body];
        NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                     completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
          {
              if (error) {
                  NSLog(@"error %@", [error localizedDescription]);
              }
              
              if (!error) {
                  NSDictionary *dictionary = [self parseResponseData:responsedata];
                  if (dictionary) {
                      NSLog(@"response data : %@", dictionary);
                  }
                  
                  if (doneBlock) {
                      doneBlock(YES);
                  }
                  
              }//end
          }];
        
        [task resume];
    }
    else {
        if (doneBlock) {
            doneBlock(NO);
        }
    }
}

- (void)requestNewQuestionForContext:(NSManagedObjectContext *)context doneBlock:(TestGuruDoneBlock)doneBlock {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    NSFetchRequest *fr = [NSFetchRequest fetchRequestWithEntityName:kQuestionEntity];
    NSError *error = nil;
    NSManagedObjectContext *ctx = context;
    NSArray *items = [ctx executeFetchRequest:fr error:&error];
    NSMutableArray *questions = [NSMutableArray array];
    
    if (!error) {
        for (NSManagedObject *mo in items) {
            NSDictionary *data =  [self questionDataFromManagedObject:mo update:NO];
            
            if (data != nil) {
                [questions addObject:data];
            }
            else {
                questions = nil;
            }
        }
    }
    
    if (questions != nil) {
        NSLog(@"questions POST : %@", questions);
        NSArray *body = [NSArray arrayWithArray:questions];
        NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionNew, user_id] ];
        NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:body];
        NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                     completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
          {
              if (error) {
                  NSLog(@"error %@", [error localizedDescription]);
              }
              
              if (!error) {
                  NSDictionary *dictionary = [self parseResponseData:responsedata];
                  if (dictionary) {
                      NSLog(@"response data : %@", dictionary);
                  }
                  
                  if (doneBlock) {
                      doneBlock(YES);
                  }
                  
              }//end
          }];
        
        [task resume];
    }
    else
    {
        if (doneBlock) {
            doneBlock(NO);
        }
    }
}

- (void)requestNewQuestionsWithHeaderData:(NSDictionary *)headerData doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSManagedObjectContext *ctx = _workerContext;
    
    NSString *proficiency_level_id = [self stringValue:[headerData valueForKey:@"proficiency_level_id"]];
    NSString *proficiencyLevelName = [self stringValue:[headerData valueForKey:@"proficiencyLevelName"]];
//    NSLog(@"DIFFICULTY LEVEL : %@", proficiency_level_id);
//    NSLog(@"DIFFICULTY LEVEL NAME : %@", proficiencyLevelName);
    
    //LEARNING SKILL
    NSString *learning_skills_id = [self stringValue:[headerData valueForKey:@"learning_skills_id"]];
    NSString *learningSkillsName = [self stringValue:[headerData valueForKey:@"learningSkillsName"]];
//    NSLog(@"LEARNING SKILL : %@", learning_skills_id);
//    NSLog(@"LEARNING SKILL NAME: %@", learningSkillsName);
    
    //FEEDBACK
    NSString *general_feedback = [self stringValue:[headerData valueForKey:@"general_feedback"]];
    NSString *correct_answer_feedback = [self stringValue:[headerData valueForKey:@"correct_answer_feedback"]];
    NSString *wrong_answer_feedback = [self stringValue:[headerData valueForKey:@"wrong_answer_feedback"]];
//    NSLog(@"GENERAL FEEDBACK : %@", general_feedback);
//    NSLog(@"CORRECT FEEDBACK : %@", correct_answer_feedback);
//    NSLog(@"WRONG FEEDBACK : %@", wrong_answer_feedback);
    
    //PACKAGE TYPE ID
    NSString *package_type_id = [self stringValue:[headerData valueForKey:@"package_type_id"]];
//    NSLog(@"PACKAGE TYPE ID : %@", package_type_id);
    
    //IS PUBLIC
    NSString *is_public = [self stringValue:[headerData valueForKey:@"is_public"]];
//    NSLog(@"IS PUBLIC : %@", is_public);
    
    NSSet *tags = [NSSet setWithSet:[headerData valueForKey:@"tags"]];
    
    NSString *entityName = kQuestionEntity;

//    //THIS IS SHIT!!!
//    //PERFORM BATCH UPDATE
//    NSBatchUpdateRequest *batchRequest = [NSBatchUpdateRequest batchUpdateRequestWithEntityName:entityName];
//    batchRequest.propertiesToUpdate = properties;
//    batchRequest.resultType = NSUpdatedObjectsCountResultType;
//    NSError *error = nil;
//    NSBatchUpdateResult *batchResult = (NSBatchUpdateResult *)[ctx executeRequest:batchRequest error:&error];
//    if (error) {
//        NSLog(@"%s ERROR %@", __PRETTY_FUNCTION__, [error localizedDescription]);
//    }
//    NSLog(@"%@ objects updated", batchResult.result);
//    //PERFORM FETCH OF QUESTIONS
    
    // PERFORM FETCH OF QUESTION ITEMS
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entityName];
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"%s ERROR %@", __PRETTY_FUNCTION__, [error localizedDescription]);
    }
    
    NSMutableArray *questions = [NSMutableArray array];
    if ( (items != nil) && ([items count] > 0) ) {
        for (NSManagedObject *mo in items) {
            [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];
            [mo setValue:proficiencyLevelName forKey:@"proficiencyLevelName"];
            [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
            [mo setValue:learningSkillsName forKey:@"learningSkillsName"];
            [mo setValue:general_feedback forKey:@"general_feedback"];
            [mo setValue:correct_answer_feedback forKey:@"correct_answer_feedback"];
            [mo setValue:wrong_answer_feedback forKey:@"wrong_answer_feedback"];
            [mo setValue:package_type_id forKey:@"package_type_id"];
            [mo setValue:is_public forKey:@"is_public"];
            [mo setValue:tags forKey:@"tags"];
            
            NSDictionary *data = [self questionDataFromManagedObject:mo update:NO];
            if (data != nil) {
                [questions addObject:data];
            }
        }
        
        [self saveTreeContext:ctx];
    }
    //POST OPERATION
    if (questions != nil) {
        NSLog(@"questions POST : %@", questions);
        
        AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
        NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
        NSArray *body = [NSArray arrayWithArray:questions];
        NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionNew, user_id] ];
        NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:body];
        NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                     completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
          {
              if (responsedata != nil) {
                  NSDictionary *dictionary = [self parseResponseData:responsedata];
                  if (dictionary) {
                      NSLog(@"%s ---> response data : %@", __PRETTY_FUNCTION__, dictionary);
                      if (doneBlock) {
                          [self requestQuestionListForUser:user_id doneBlock:^(BOOL status) {
                              if (status == YES) {
                                  doneBlock(YES);
                              }
                          }];
                      }
                  }
              }//end
          }];
        
        [task resume];
    }
    else {
        if (doneBlock) {
            doneBlock(NO);
        }
    }
    
}

- (void)setDictionary:(NSDictionary *)dictionary atForm:(NSMutableDictionary *)form {
    
    if (dictionary != nil) {
        NSArray *keys = [dictionary allKeys];
        for (NSString *k in keys) {
            [form setObject:dictionary[k] forKey:k];
        }
    }
}

- (void)requestUpdateQuestionsWithDoneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSManagedObjectContext *ctx = _workerContext;
    
    NSString *entityName = kQuestionEntity;
    
    // PERFORM FETCH OF QUESTION ITEMS
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entityName];
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"%s ERROR %@", __PRETTY_FUNCTION__, [error localizedDescription]);
    }
    
    NSMutableArray *questions = [NSMutableArray array];
    if ( (items != nil) && ([items count] > 0) ) {
        for (NSInteger i = 0; i < items.count; i++ ) {
            NSManagedObject *mo = (NSManagedObject *)items[i];
            NSDictionary *meta = [self questionDataFromManagedObject:mo index:i update:NO];
            // DATA
            NSDictionary *question_data = meta[@"question_data"];
            if (question_data != nil) {
                [questions addObject:question_data];
            }
        }
    }
    
    //POST OPERATION
    if (questions != nil) {
        NSString *user_id = [self loginUser];
        NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointTestGuruQuestionUpdate, user_id]];
        NSLog(@"path : %@", query);
        NSArray *body = [NSArray arrayWithArray:questions];
        NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:[NSURL URLWithString:query] body:body];
        NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                     completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
        {
            if (responsedata != nil) {
                NSDictionary *dictionary = [self parseResponseData:responsedata];
                if (dictionary) {
                    NSLog(@"%s ---> response data : %@", __PRETTY_FUNCTION__, dictionary);
                    if (doneBlock) {
                        [self requestQuestionListForUser:user_id doneBlock:^(BOOL status) {
                            if (status == YES) {
                                doneBlock(YES);
                            }
                        }];
                    }
                }
            }//end
        }];
        
        [task resume];
        
    } else {
        if (doneBlock) {
            doneBlock(NO);
        }
    }
}

- (void)postQuestionsObjectForEditMode:(BOOL)isEditMode withDoneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSManagedObjectContext *ctx = _workerContext;
    
    NSString *entityName = kQuestionEntity;
    
    // PERFORM FETCH OF QUESTION ITEMS
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entityName];
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"%s ERROR %@", __PRETTY_FUNCTION__, [error localizedDescription]);
    }
    
    NSMutableArray *questions = [NSMutableArray array];
    NSMutableDictionary *image_files = [NSMutableDictionary dictionary];
    
    if ( (items != nil) && ([items count] > 0) ) {
        
        for (NSInteger i = 0; i < items.count; i++ ) {
            NSManagedObject *mo = (NSManagedObject *)items[i];
            
            NSDictionary *meta = [self questionDataFromManagedObject:mo index:i update:isEditMode];
            
            // DATA
            NSDictionary *question_data = meta[@"question_data"];
            if (question_data != nil) {
                [questions addObject:question_data];
            }
            
            // QUESTION IMAGES
            NSDictionary *question_images = meta[@"question_images"];
            [self setDictionary:question_images atForm:image_files];
            
            // CHOICES IMAGES
            NSDictionary *choices_images = meta[@"question_choice_images"];
            [self setDictionary:choices_images atForm:image_files];
        }
        
    }
    
    //POST OPERATION
    if (questions != nil) {
        NSString *user_id = [self loginUser];
        NSString *end_point = (isEditMode == NO) ? kEndPointTestGuruQuestionNewV2 : kEndPointTestGuruQuestionUpdateV2;
        NSString *query = [Utils buildUrl:[NSString stringWithFormat:end_point, user_id]];
        NSLog(@"path : %@", query);
        
        // configure the request
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
        [request setHTTPMethod:@"POST"];
        
        // boundary
        NSString *boundary = [self generateBoundaryString];
        NSLog(@"initialize boundary : %@", boundary);
        
        // set content type
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSString *question_string_object = [self jsonStringFromObject:questions];
        NSData *httpBody = [self generateMultiPartWithBoundary:boundary
                                                          data:question_string_object
                                                        images:image_files];
        [request setHTTPBody:httpBody];
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        [config setAllowsCellularAccess:YES];//USE CELLULAR DATA
        
        NSURLSession *dataSession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
        NSURLSessionTask *task = [dataSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (error) {
                NSLog(@"error %@", [error localizedDescription]);
            }
            
            if (!error) {
                NSDictionary *dictionary = [self parseResponseData:data];
                if (dictionary) {
                    NSLog(@"response data : %@", dictionary);
                    if (doneBlock) {
                        doneBlock(YES);
                    }
                }
            }//end
        }];
        [task resume];
        
    } else {
        if (doneBlock) {
            doneBlock(NO);
        }
    }
}

- (void)requestUpdateDetailsForQuestion:(NSManagedObject *)object headerData:(NSDictionary *)headerData doneBlock:(TestGuruDoneBlock)doneBlock {
    
    //QUESTION ID
    NSString *question_id = [self stringValue: [object valueForKey:@"id"] ];
    
    //PREDICATE
    NSPredicate *predicate = [self predicateForKeyPath:@"id" andValue:question_id];
    NSManagedObject *mo = [self getEntity:kQuestionEntity predicate:predicate];
    
    NSString *proficiency_level_id = [self stringValue:[headerData valueForKey:@"proficiency_level_id"]];
    NSString *proficiencyLevelName = [self stringValue:[headerData valueForKey:@"proficiencyLevelName"]];
    NSString *learning_skills_id = [self stringValue:[headerData valueForKey:@"learning_skills_id"]];
    NSString *learningSkillsName = [self stringValue:[headerData valueForKey:@"learningSkillsName"]];
    NSString *general_feedback = [self stringValue:[headerData valueForKey:@"general_feedback"]];
    NSString *correct_answer_feedback = [self stringValue:[headerData valueForKey:@"correct_answer_feedback"]];
    NSString *wrong_answer_feedback = [self stringValue:[headerData valueForKey:@"wrong_answer_feedback"]];
    NSString *package_type_id = [self stringValue:[headerData valueForKey:@"package_type_id"]];
    NSString *is_public = [self stringValue:[headerData valueForKey:@"is_public"]];
    
    NSSet *tags = [NSSet setWithSet:[headerData valueForKey:@"tags"]];
    
    [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];
    [mo setValue:proficiencyLevelName forKey:@"proficiencyLevelName"];
    [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
    [mo setValue:learningSkillsName forKey:@"learningSkillsName"];
    [mo setValue:general_feedback forKey:@"general_feedback"];
    [mo setValue:correct_answer_feedback forKey:@"correct_answer_feedback"];
    [mo setValue:wrong_answer_feedback forKey:@"wrong_answer_feedback"];
    [mo setValue:package_type_id forKey:@"package_type_id"];
    [mo setValue:is_public forKey:@"is_public"];
    
    [mo setValue:tags forKey:@"tags"];
    
    [self saveTreeContext:mo.managedObjectContext];
    
    NSMutableArray *questions = [NSMutableArray array];
    for (NSString *key in [mo.entity.propertiesByName allKeys]) {
        if ( ![key isEqualToString:@"tags"] || ![key isEqualToString:@"choices"] ) {
            NSString *val = [mo valueForKey:key];
            NSLog(@"----- < %@ > = [%@]", key, val );
        }
    }
    
    NSDictionary *data =  [self questionDataFromManagedObject:mo update:YES withTags:tags];
    if (data != nil) {
        [questions addObject:data];
    } else {
        questions = nil;
    }
    
    //POST OPERATION
    if (questions != nil) {
        NSLog(@"questions POST : %@", questions);
        
        AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
        NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
        NSArray *body = [NSArray arrayWithArray:questions];
        NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionUpdate, user_id] ];
        NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:body];
        NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                     completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
          {
              if (responsedata != nil) {
                  NSDictionary *dictionary = [self parseResponseData:responsedata];
                  if (dictionary) {
                      NSLog(@"%s ---> response data : %@", __PRETTY_FUNCTION__, dictionary);
                      if (doneBlock) {
//                          [self requestDetailsForQuestion:<#(NSManagedObject *)#> withNewEntity:<#(NSString *)#> doneBlock:<#^(BOOL status)doneBlock#>]
//                          [self requestQuestionListForUser:user_id doneBlock:^(BOOL status) {
//                              if (status == YES) {
                                  doneBlock(YES);
//                              }
//                          }];
                      }
                  }
              }//end
          }];
        
        [task resume];
    }
    else {
        if (doneBlock) {
            doneBlock(NO);
        }
    }
}

//fix
//- (void)updateEdittedQuestion:(NSManagedObject *)object withNewEntity:(NSString *)entity doneBlock:(TestGuruDoneBlock)doneBlock {
//    
//    NSString *user_id = [self stringValue:[object valueForKey:@"user_id"] ];
//    NSString *question_id = [self stringValue:[object valueForKey:@"id"] ];
//    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionDetails, user_id, question_id] ];
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
//    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
//                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
//                                  {
//                                      if (error) {
//                                          NSLog(@"error %@", [error localizedDescription]);
//                                      }
//                                      
//                                      if (!error) {
//                                          
//                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
//                                          if (dictionary[@"records"] != nil ) {
//                                              
//                                              NSDictionary *records = dictionary[@"records"];
//                                              
//                                              //                  NSLog(@"records : %@", records);
//                                              if (records.count > 0) {
//                                                  
//                                                  NSManagedObjectContext *ctx = _workerContext;
//                                                  
//                                                  [ctx performBlock:^{
//                                                      
//                                                      BOOL cleared = [self clearDataForEntity:entity withPredicate:nil context:ctx];
//                                                      
//                                                      if (cleared) {
//                                                          
//                                                          NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:entity
//                                                                                                              inManagedObjectContext:ctx];
//                                                          
//                                                          if ([self isArrayObject: records[@"question"] ]) {
//                                                              NSDictionary *question = [records[@"question"] lastObject];// DICTIONARY
//                                                              [self updateQuestionsWithData:question managedObject:mo];
//                                                          }
//                                                          
//                                                          if ([self isArrayObject: records[@"choices"] ]) {
//                                                              NSArray *choices = records[@"choices"];
//                                                              [self processChoices:choices managedObject:mo];
//                                                          }
//                                                          
//                                                          //                          if ( [self isArrayObject: records[@"subs"] ] ) {
//                                                          //                              NSArray *subs = records[@"subs"];
//                                                          //                              NSLog(@"subs : %@", subs);
//                                                          ////                              [self processSubs:subs managedObject:mo];
//                                                          //                          }
//                                                          
//                                                          if ( [self isArrayObject: records[@"tags"] ] ) {
//                                                              NSArray *tags = records[@"tags"];
//                                                              [self processTags:tags managedObject:mo];
//                                                          }
//                                                          
//                                                          [self saveTreeContext:ctx];
//                                                          
//                                                          if (doneBlock) {
//                                                              doneBlock(YES);
//                                                          }
//                                                          
//                                                          
//                                                      }
//                                                      
//                                                  }];
//                                              }
//                                          }
//                                      }//end
//                                      
//                                  }];
//    
//    [task resume];
//}


- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
   didSendBodyData:(int64_t)bytesSent
    totalBytesSent:(int64_t)totalBytesSent
totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend {
    
    CGFloat progress = (double)totalBytesSent / (double)totalBytesExpectedToSend;
    NSLog(@"upload progress : %f", progress);
    
    if (self.progressBlock) {
        self.progressBlock(progress);
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    
    if (self.doneProgressBlock) {
        self.doneProgressBlock(@"TRUE");
    }
}

- (void)requestUploadForUserID:(NSString *)userid files:(NSDictionary *)object doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointTestGuruImageUploadForUserID, userid]];
    NSLog(@"path : %@", query);
    NSLog(@"file : %@", object);
    
    // configure the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // boundary
    NSString *boundary = [self generateBoundaryString];
    
    // set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create body
    NSData *httpBody = [self createBodyWithBoundary:boundary files:object];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    [config setAllowsCellularAccess:YES];//USE CELLULAR DATA
    NSURLSession *uploadsession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    NSURLSessionUploadTask *uploadTask = [uploadsession uploadTaskWithRequest:request fromData:httpBody];
    [uploadTask resume];//START upload
    
    self.doneProgressBlock = ^(NSString *status) {
        NSLog(@"TASKS COMPLETE...");
        if ([status isEqualToString:@"TRUE"]) {
            if (doneBlock) {
                doneBlock(YES);
            }
        }
    };
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary
                             files:(NSDictionary *)parameters
{
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *key, id value, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        //        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSArray *files = (NSArray *)value;
        if (files.count > 0) {
            for (NSUInteger i = 0; i < files.count; i++) {
                NSDictionary *fileObject = (NSDictionary *)files[i];
                if (fileObject) {
                    
                    NSString *fieldname = [NSString stringWithFormat:@"file%lu",(unsigned long)i];
                    NSString *filename = [NSString stringWithFormat:@"%@", fileObject[@"filename"] ];
                    NSData *data = [NSData dataWithData: fileObject[@"filedata"] ];
                    NSString *mimetype  = [NSString stringWithFormat:@"%@", fileObject[@"mimetype"] ];
                    
                    //ACTUAL CREATION OF FORM-DATA
                    //base on rfc1867
                    [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    //                    [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldname, filename] dataUsingEncoding:NSUTF8StringEncoding]];
                    [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldname, filename]]];
                    //                    [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
                    [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype]]];
                    [httpBody appendData:data];
                    //                    [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    [httpBody appendData:[self encodeObject:@"\r\n"]];
                    
                }//END->fileObject
                
            }//END->for loop
            
        }//END->files.count
        
    }];
    
    //    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"--%@--\r\n", boundary]]];
    
    return httpBody;
}

- (void)uploadImageFiles:(NSArray *)imageList doneBlock:(TestGuruDoneBlock)doneBlock {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *userid = [NSString stringWithFormat:@"%@", @(account.user.id) ];
    
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointTestGuruImageUploadForUserID, userid]];
    NSLog(@"path : %@", query);
//    NSLog(@"file : %@", imageList);
    
    // configure the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    NSLog(@"request : %@", [[request URL] absoluteString]);

    // boundary
    NSString *boundary = [self generateBoundaryString];
    NSLog(@"initialize boundary : %@", boundary);

    // set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];

    // create body
    NSData *httpBody = [self generateMultiPartFormDataWithBoundary:boundary files:imageList];
    [request setHTTPBody:httpBody];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    [config setAllowsCellularAccess:YES];//USE CELLULAR DATA
    
    NSURLSession *uploadsession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
//    NSURLSessionUploadTask *uploadTask = [uploadsession uploadTaskWithRequest:request fromData:httpBody];
//    [uploadTask resume];//START upload
    
    NSURLSessionDataTask *uploadTask = [uploadsession dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:data];
            if (dictionary) {
                NSLog(@"response data : %@", dictionary);
                
                /*
                 
                 records =     (
                     {
                         fileName = "993f87082bc73046a000b177e0c12fa1.png";
                         fileSize = 4kB;
                         fileType = png;
                         url = "img/uploads/testguru/2/993f87082bc73046a000b177e0c12fa1.png";
                     },
                     {
                         fileName = "f85e9f6a526b10ac688ae3ca5b1f85e2.png";
                         fileSize = 4kB;
                         fileType = png;
                         url = "img/uploads/testguru/2/f85e9f6a526b10ac688ae3ca5b1f85e2.png";
                     },
                     {
                         fileName = "f5fa52cabf48f929a8c4a014f8b777eb.png";
                         fileSize = 4kB;
                         fileType = png;
                         url = "img/uploads/testguru/2/f5fa52cabf48f929a8c4a014f8b777eb.png";
                     },
                     {
                         fileName = "e0eee62ecfb589b4dad7afe10cdc9af4.png";
                         fileSize = 4kB;
                         fileType = png;
                         url = "img/uploads/testguru/2/e0eee62ecfb589b4dad7afe10cdc9af4.png";
                     }
                 );

                 
                 
                 */
                
                
                
            }
            
            if (doneBlock) {
                doneBlock(YES);
            }
            
        }//end
        
    }];
    
    [uploadTask resume];
}

- (void)uploadImageFiles:(NSArray *)imageList dataBlock:(TestGuruDataBlock)dataBlock {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *userid = [NSString stringWithFormat:@"%@", @(account.user.id) ];
    
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointTestGuruImageUploadForUserID, userid]];
    NSLog(@"path : %@", query);
    //    NSLog(@"file : %@", imageList);
    
    // configure the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    NSLog(@"request : %@", [[request URL] absoluteString]);
    
    // boundary
    NSString *boundary = [self generateBoundaryString];
    NSLog(@"initialize boundary : %@", boundary);
    
    // set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create body
    NSData *httpBody = [self generateMultiPartFormDataWithBoundary:boundary files:imageList];
    [request setHTTPBody:httpBody];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    [config setAllowsCellularAccess:YES];//USE CELLULAR DATA
    
    NSURLSession *uploadsession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    //    NSURLSessionUploadTask *uploadTask = [uploadsession uploadTaskWithRequest:request fromData:httpBody];
    //    [uploadTask resume];//START upload
    
    NSURLSessionDataTask *uploadTask = [uploadsession dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
            if (error) {
                NSLog(@"error %@", [error localizedDescription]);
            }
            
            if (!error) {
                NSDictionary *dictionary = [self parseResponseData:data];
                
                if (dictionary[@"records"] != nil ) {
                    
                    NSArray *records = dictionary[@"records"];
                    
                    if (records.count > 0) {
                        
                        NSDictionary *d = [records lastObject];
                        NSString *image_url = [self stringValue:d[@"url"] ];
                        NSDictionary *data = @{@"url":image_url};
                        
                        if (dataBlock) {
                            dataBlock(data);
                        }
                    }
                    
                }
                
            }//end
            
        }];
    
    [uploadTask resume];
}

- (NSData *)generateMultiPartWithBoundary:(NSString *)boundary data:(NSString *)data images:(NSDictionary *)images
{
    
    NSLog(@"boundary : %@", boundary);
    NSLog(@"data : %@", data);
    NSLog(@"images : %@", images);
    
    
    NSMutableData *httpBody = [NSMutableData data];
    
    // DATA
    // add params (all params are strings)
    [httpBody appendData:[self encodeObject:@"\r\n"] ];
    [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"--%@\r\n", boundary]] ];
    [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"data"] ]];
    [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"%@\r\n", data] ]];

    // IMAGES
    // add image data
    if (images != nil) {
        NSString *mimetype = @"image/jpeg";
        for (NSString *key in [images allKeys] ) {
            /*
             
             NSDictionary *d = @{@"name":@"abstract_icon1", @"key":@"abstract_icon1", @"filename":@"abstract_icon1.png", @"data":data};
             
             ========================
             POST /path/to/script.php HTTP/1.0
             Host: example.com
             Content-type: multipart/form-data, boundary=AaB03x
             Content-Length: $requestlen
             
             --AaB03x
             content-disposition: form-data; name="field1"
             
             $field1
             --AaB03x
             content-disposition: form-data; name="field2"
             
             $field2
             --AaB03x
             content-disposition: form-data; name="userfile"; filename="$filename"
             Content-Type: $mimetype
             Content-Transfer-Encoding: binary
             
             $binarydata
             --AaB03x--
             ==========================
             
             */
            
            NSString *name = [NSString stringWithFormat:@"%@", key];
            NSString *filename = [NSString stringWithFormat:@"%@.jpg", key];
            NSData *data = images[key];
            [httpBody appendData:[self encodeObject:@"\r\n"] ];
            [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"--%@\r\n", boundary]] ];
            [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"content-disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", name, filename]] ];
            [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype]] ];
            [httpBody appendData:data];
            [httpBody appendData:[self encodeObject:@"\r\n"] ];
        }
        
        [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"--%@--\r\n", boundary]]];
    }
    
    
    NSString *test_body =  [[NSString alloc] initWithData:httpBody encoding:NSUTF8StringEncoding];
    NSLog(@"-----> form-data : %@", test_body);
    
    
    return httpBody;
}

- (NSData *)generateMultiPartFormDataWithBoundary:(NSString *)boundary files:(NSArray *)files
{
    NSMutableData *httpBody = [NSMutableData data];

    if ([files count] > 0) {
        
        NSString *mimetype = @"image/jpeg";
        
        for (NSDictionary *d in files) {
            
            
//            NSLog(@"data : %@", d);
            
            /*
             
             NSDictionary *d = @{@"name":@"abstract_icon1", @"key":@"abstract_icon1", @"filename":@"abstract_icon1.png", @"data":data};

             ========================
             POST /path/to/script.php HTTP/1.0
             Host: example.com
             Content-type: multipart/form-data, boundary=AaB03x
             Content-Length: $requestlen
             
             --AaB03x
             content-disposition: form-data; name="field1"
             
             $field1
             --AaB03x
             content-disposition: form-data; name="field2"
             
             $field2
             --AaB03x
             content-disposition: form-data; name="userfile"; filename="$filename"
             Content-Type: $mimetype
             Content-Transfer-Encoding: binary
             
             $binarydata
             --AaB03x--
             ==========================
             
             */
            
            NSString *name = d[@"name"];
            NSString *filename = d[@"filename"];
            NSData *data = d[@"data"];
            
            [httpBody appendData:[self encodeObject:@"\r\n"] ];
            [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"--%@\r\n", boundary]] ];
            [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"content-disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", name, filename]] ];
            [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype]] ];
            [httpBody appendData:data];
            [httpBody appendData:[self encodeObject:@"\r\n"] ];
        }

        [httpBody appendData:[self encodeObject:[NSString stringWithFormat:@"--%@--\r\n", boundary]]];
    }
    
    return httpBody;
}

- (NSData *)encodeObject:(NSString *)string {
    return [string dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)generateBoundaryString
{
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
    
    // if supporting iOS versions prior to 6.0, you do something like:
    //
    // // generate boundary string
    // //
    // adapted from http://developer.apple.com/library/ios/#samplecode/SimpleURLConnections
    //
    // CFUUIDRef  uuid;
    // NSString  *uuidStr;
    //
    // uuid = CFUUIDCreate(NULL);
    // assert(uuid != NULL);
    //
    // uuidStr = CFBridgingRelease(CFUUIDCreateString(NULL, uuid));
    // assert(uuidStr != NULL);
    //
    // CFRelease(uuid);
    //
    // return uuidStr;
}

- (void)requestRemoveQuestion:(NSManagedObject *)mo doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *user_id = [self stringValue:[mo valueForKey:@"user_id"] ];
    NSString *question_id = [self stringValue:[mo valueForKey:@"id"] ];
    
    NSPredicate *predicate = [self predicateForKeyPath:@"id" andValue:question_id];
//    [self clearContentsForEntity:kQuestionBankEntity predicate:predicate];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionDeleteV2, question_id, user_id] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
              if (doneBlock) {
                  doneBlock(NO);
              }
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              
              if (dictionary != nil ) {
                      

                  NSDictionary *metaDict = dictionary[@"_meta"];
                  if (metaDict != nil) {
                      NSString *status = metaDict[@"status"];
                      if ([status isEqualToString:@"SUCCESS"]) {
                      NSManagedObjectContext *ctx = self.workerContext;
                      [self clearDataForEntity:kQuestionBankEntity withPredicate:predicate context:ctx];
                          
                      [ctx performBlock:^{
                          [self saveTreeContext:ctx];
                      }];
                      
                      if (doneBlock) {
                          doneBlock(YES);
                      }
                          
                      } else {
                          if (doneBlock) {
                              doneBlock(NO);
                          }
                  }
                  }
//                  NSLog(@"delete data : %@", dictionary);
//                  
//                  NSArray *records = dictionary[@"records"];
//                  
//                  if (records.count > 0) {
//                      
//                      NSLog(@"records : %@", records);
//
//                      NSManagedObjectContext *ctx = self.workerContext;
//                      [ctx performBlock:^{
//                          [self saveTreeContext:ctx];
//                          
//                      }];
//                      
//                      if (doneBlock) {
//                          doneBlock(YES);
//                      }
//                  }
              }
              
          }//end
          
      }];
    
    [task resume];
}

- (NSSet *)insertTags:(NSArray *)tags managedObject:(NSManagedObject *)mo {
    
    NSManagedObjectContext *ctx = mo.managedObjectContext;
    NSSet *sets = [NSSet set];
    
    if (tags.count > 0) {
        
        NSMutableSet *tag_sets = [NSMutableSet set];
        
        for (NSDictionary *t in tags) {
            NSString *tag_id = [self stringValue: t[@"id"] ];
            NSString *tag_name = [self stringValue: t[@"tag"] ];
            
            NSManagedObject *t_mo = [NSEntityDescription insertNewObjectForEntityForName:kTagEntity inManagedObjectContext:ctx];
            [t_mo setValue:tag_id forKey:@"id"];
            [t_mo setValue:tag_name forKey:@"tag"];
            
            [tag_sets addObject:t_mo];
        }
        
        sets = [NSSet setWithSet:tag_sets];
        
        // assign tags
        [mo setValue:sets forKey:@"tags"];
    }

    return sets;
}

- (void)insertNewTagValue:(NSString *)value {
    
    NSCharacterSet *leadingTrailing = [NSCharacterSet whitespaceCharacterSet];
    NSString *normalize_string = [value stringByTrimmingCharactersInSet:leadingTrailing];
    
    NSManagedObjectContext *ctx = _workerContext;
    if (value.length > 0) {
        NSManagedObject *tag_mo = [NSEntityDescription insertNewObjectForEntityForName:kTagValuesEntity inManagedObjectContext:ctx];
        [tag_mo setValue:normalize_string forKey:@"tag"];
        [self saveTreeContext:ctx];
    }
}

- (void)insertNewTagValue:(NSString *)value withQuestionID:(NSString *)question_id doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSCharacterSet *leadingTrailing = [NSCharacterSet whitespaceCharacterSet];
    NSString *normalize_string = [value stringByTrimmingCharactersInSet:leadingTrailing];
    
    NSManagedObjectContext *ctx = _workerContext;
    if (value.length > 0) {
        //NSManagedObject *tag_mo = [NSEntityDescription insertNewObjectForEntityForName:kTagValuesEntity inManagedObjectContext:ctx];
        
        // JCA-11302016
        // To avoid duplicate tag
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tag == %@", normalize_string];
        NSManagedObject *tag_mo = [self getEntity:kTagValuesEntity withPredicate:predicate fromContext:ctx];
        
        [tag_mo setValue:normalize_string forKey:@"tag"];
        [tag_mo setValue:question_id forKey:@"question_id"];
        [self saveTreeContext:ctx];
        
        if (doneBlock) {
            doneBlock(YES);
        }
    }
}

- (void)tagBatchInsert:(NSArray *)tagList doneBlock:(TestGuruDoneBlock)doneBlock {
    
    if (tagList.count > 0) {
        
        NSCharacterSet *leadingTrailing = [NSCharacterSet whitespaceCharacterSet];
        NSManagedObjectContext *ctx = _workerContext;
        [ctx performBlock:^{
            
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kTagValuesEntity];
            NSError *error = nil;
            NSArray *tag_items = [ctx executeFetchRequest:request error:&error];
            if ([tag_items count] > 0) {
                for (NSManagedObject *mo in tag_items) {
                    [ctx deleteObject:mo];
                }
                [self saveTreeContext:ctx];
            }
            for (NSManagedObject *tag in tagList) {
                NSString *value = [NSString stringWithFormat:@"%@", [tag valueForKey:@"tag"]];
                NSString *question_id = [NSString stringWithFormat:@"%@", [tag valueForKey:@"question_id"]];
                NSString *string = [value stringByTrimmingCharactersInSet:leadingTrailing];
                if (string.length > 0) {
                    NSManagedObject *tag_mo = [NSEntityDescription insertNewObjectForEntityForName:kTagValuesEntity
                                                                            inManagedObjectContext:ctx];
                    [tag_mo setValue:question_id forKey:@"question_id"];
                    [tag_mo setValue:string forKey:@"tag"];
                }
            }
            [self saveTreeContext:ctx];
            if (doneBlock) {
                doneBlock(YES);
            }
        }];
    }
}

- (void)removeTagValue:(NSString *)value {
    
    NSCharacterSet *leadingTrailing = [NSCharacterSet whitespaceCharacterSet];
    NSString *normalize_string = [value stringByTrimmingCharactersInSet:leadingTrailing];
    
    NSPredicate *predicate = [self predicateForKeyPath:@"tag" andValue:normalize_string];
    
    NSManagedObjectContext *ctx = _workerContext;
    [self clearDataForEntity:kTagValuesEntity withPredicate:predicate context:ctx];
}

- (void)removeTagValue:(NSString *)value doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSCharacterSet *leadingTrailing = [NSCharacterSet whitespaceCharacterSet];
    NSString *normalize_string = [value stringByTrimmingCharactersInSet:leadingTrailing];
    
    //NSPredicate *predicate = [self predicateForKeyPath:@"tag" andValue:normalize_string];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tag == %@", normalize_string]; // to make it case sensitive
    
    NSManagedObjectContext *ctx = _workerContext;
    BOOL flag = [self clearDataForEntity:kTagValuesEntity withPredicate:predicate context:ctx];
    
    if (doneBlock) {
        doneBlock(flag);
    }
}

- (void)clearTagValuesWithBlock:(TestGuruDoneBlock)doneBlock {
    
    //CLEAR CONTENTS
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTagValuesEntity];
    
    NSManagedObjectContext *ctx = _workerContext;
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        
        [ctx performBlock:^{
            for (NSManagedObject *mo in items) {
                [ctx deleteObject:mo];
            }
            [self saveTreeContext:ctx];
            if (doneBlock) {
                doneBlock(YES);
            }
        }];
    }
}

- (void)updateTagValue:(NSString *)value withNewValue:(NSString *)string context:(NSManagedObjectContext *)context doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSCharacterSet *leadingTrailing = [NSCharacterSet whitespaceCharacterSet];
    NSString *normalize_string = [value stringByTrimmingCharactersInSet:leadingTrailing];
    NSLog(@"old string xxx: %@", normalize_string);
    NSString *normalize_new_string = [string stringByTrimmingCharactersInSet:leadingTrailing];
    NSLog(@"new string xxx: %@", normalize_new_string);
    
    /*
    NSManagedObjectContext *ctx = context;
    [ctx performBlock:^{
        
        //NSPredicate *predicate = [self predicateForKeyPath:@"tag" andValue:normalize_string];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tag == %@", normalize_string]; // to make it case sensitive
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTagValuesEntity];
        [fetchRequest setPredicate:predicate];
        NSError *error = nil;
        NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
        if (error) {
            NSLog(@"error: %@", [error localizedDescription]);
        }

        if ([items count] > 0) {
            NSManagedObject *tag_mo = (NSManagedObject *)[items lastObject];
            [tag_mo setValue:normalize_new_string forKey:@"tag"];
            NSDate *dateObject = [NSDate new];//time stamp
            [tag_mo setValue:dateObject forKey:@"date"];
            [self saveTreeContext:ctx];
            
            if (doneBlock) {
                doneBlock(YES);
            }
        }

    }];
     
     */
    
    // Also handle already existing tag
    // MUST not allow duplicate tags
    NSManagedObjectContext *ctx = context;
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"tag == %@", normalize_new_string];
    NSManagedObject *tagObject = [self getEntity:kTagValuesEntity predicate:filter];
    
    NSLog(@"TAG OBJECT EXISTS?: %@", tagObject);
    
    if (tagObject == nil) {
        [ctx performBlock:^{
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tag == %@", normalize_string]; // to make it case sensitive
            NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTagValuesEntity];
            [fetchRequest setPredicate:predicate];
            
            NSError *error = nil;
            NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
            
            if (error) {
                NSLog(@"error: %@", [error localizedDescription]);
                
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
            else {
                if ([items count] > 0) {
                    NSManagedObject *tag_mo = (NSManagedObject *)[items lastObject];
                    [tag_mo setValue:normalize_new_string forKey:@"tag"];
                    [tag_mo setValue:[NSDate new] forKey:@"date"];
                    
                    [self saveTreeContext:ctx];
                }
                
                if (doneBlock) {
                    doneBlock(YES);
                }
            }
        }];
    }
    else {
        if (doneBlock) {
            doneBlock(NO);
        }
    }
    
}

- (void)updateTagValue:(NSString *)value withNewValue:(NSString *)string doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSCharacterSet *leadingTrailing = [NSCharacterSet whitespaceCharacterSet];
    NSString *normalize_string = [value stringByTrimmingCharactersInSet:leadingTrailing];
    NSLog(@"old string : %@", normalize_string);
    NSString *normalize_new_string = [string stringByTrimmingCharactersInSet:leadingTrailing];
    NSLog(@"new string : %@", normalize_new_string);
    
    NSManagedObjectContext *ctx = _workerContext;
    [ctx performBlock:^{
        NSPredicate *predicate = [self predicateForKeyPath:@"tag" andValue:normalize_string];
        NSManagedObject *mo = [self getEntity:kTagValuesEntity predicate:predicate];
        [mo setValue:normalize_new_string forKey:@"tag"];
        NSDate *dateObject = [NSDate new];//time stamp
        [mo setValue:dateObject forKey:@"date"];
        [self saveTreeContext:ctx];
        
        if (doneBlock) {
            doneBlock(YES);
        }
    }];
}

- (void)insertQuestionWithType:(NSDictionary *)questionTypeData itemCount:(NSString *)count dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSInteger iteration = [count integerValue];
    
    if (iteration > 0) {
        
        NSManagedObjectContext *ctx = _workerContext;
        
        [ctx performBlock:^{
            
            BOOL cleared = [self clearDataForEntity:kQuestionEntity withPredicate:nil context:ctx];
            cleared = [self clearDataForEntity:kChoiceItemEntity withPredicate:nil context:ctx];
            
            if (cleared) {

                //SET DEFAULT VALUES
                NSString *user_id = [self loginUser];

//                NSString *general_feedback = NSLocalizedString(@"Type your general feedback...", nil);
//                NSString *correct_answer_feedback = NSLocalizedString(@"Type your correct answer feedback...", nil);
//                NSString *wrong_answer_feedback = NSLocalizedString(@"Type your wrong answer feedback...", nil);
                
                NSString *general_feedback = @"";
                NSString *correct_answer_feedback = @"";
                NSString *wrong_answer_feedback = @"";
                
//                NSString *tags = @"";
                
                NSString *image_url = @"";
                NSString *is_deleted = @"0";
                NSString *is_public = @"0";
                NSString *points = @"1";
                NSString *question_parent_id = @"0"; //VERY IMPORTANT BUT HARDCODED
                
                NSDictionary *difficultyData = [self fetchDefaultOptionType:kProficiencyEntity];
                NSString *proficiencyLevelName = difficultyData[@"value"];
                NSString *proficiency_level_id = difficultyData[@"id"];
                
                NSDictionary *learningData = [self fetchDefaultOptionType:kSkillEntity];
                NSString *learningSkillsName = learningData[@"value"];
                NSString *learning_skills_id = learningData[@"id"];
                
                NSString *questionTypeName = questionTypeData[@"value"];
                NSString *question_type_id = questionTypeData[@"id"];
                
                NSString *package_type_id = [self stringValue:[self fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID]];
                //[self stringValue: questionTypeData[@"package_type_id"] ];

                NSString *course_id = [self stringValue:[self fetchObjectForKey:kTGQB_SELECTED_COURSE_ID]];
//                [self stringValue: questionTypeData[@"course_id"] ];
                
                //COMPETENCY_CODE
                NSString *competency_code = @""; //IMPORTANT
                
                NSNumber *is_selected = [NSNumber numberWithBool:NO]; // TEST CREATE PURPOSE
                
                //VERY IMPORTANT
                //FOR FIRST INDEX
                NSDictionary *userData = nil;
                
                for (NSInteger i = 0; i < iteration; i++) {
                    
                    NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kQuestionEntity
                                                                        inManagedObjectContext:ctx];
                    /*
                     "question_parent_id": "0", //checked
                     "name": "test1", //checked
                     "points": "1", //checked
                     "question_text": "test1", //checked
                     "image_url": "", //checked
                     "question_type_id": "3", //checked
                     "proficiency_level_id": "1", //checked
                     "correct_answer_feedback": "",
                     "learning_skills_id": "1",
                     "wrong_answer_feedback": "",
                     "general_feedback": "",
                     "is_public": 1
                     */

                    NSString *question_id = [self generateGUID];
                    
//                    NSString *name = [NSString stringWithFormat:@"NEW_QUESTION_NAME_%@", question_id ];
//                    NSString *question_text = [NSString stringWithFormat:@"NEW_QUESTION_TEXT_%@", question_id ];
                    
                    NSString *name = @"";//DEMO
                    NSString *question_text = @"";//DEMO
                    NSString *date_now = [self stringValue: [NSDate date] ];
                    
                    if (i == 0) {
                        //SAVE FIRST INDEX
                        userData = @{@"question_id":question_id};
                    }
                    
                    //QUESTION ID
                    [mo setValue:question_id forKey:@"id"];
                    
                    //PACKAGE TYPE ID
                    [mo setValue:package_type_id forKey:@"package_type_id"];
                    
                    //COURSE ID
                    [mo setValue:course_id forKey:@"course_id"];

                    //COMPETENCY CODE
                    [mo setValue:competency_code forKey:@"competency_code"];
                    
                    [mo setValue:date_now forKey:@"date_created"];
                    [mo setValue:date_now forKey:@"date_modified"];

                    [mo setValue:image_url forKey:@"image_url"];
                    [mo setValue:is_deleted forKey:@"is_deleted"];
                    [mo setValue:is_public forKey:@"is_public"];
                    [mo setValue:points forKey:@"points"];

//                    [mo setValue:tags forKey:@"tags"];
                    [mo setValue:user_id forKey:@"user_id"];

                    [mo setValue:general_feedback forKey:@"general_feedback"];
                    [mo setValue:correct_answer_feedback forKey:@"correct_answer_feedback"];
                    [mo setValue:wrong_answer_feedback forKey:@"wrong_answer_feedback"];
                    
                    [mo setValue:name forKey:@"name"];
                    [mo setValue:question_text forKey:@"question_text"];
                    [mo setValue:question_parent_id forKey:@"question_parent_id"];
                    
                    [mo setValue:questionTypeName forKey:@"questionTypeName"];
                    [mo setValue:question_type_id forKey:@"question_type_id"];
                    
                    [mo setValue:proficiencyLevelName forKey:@"proficiencyLevelName"];
                    [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];

                    [mo setValue:learningSkillsName forKey:@"learningSkillsName"];
                    [mo setValue:learning_skills_id forKey:@"learning_skills_id"];

                    [mo setValue:is_selected forKey:@"is_selected"]; // TEST CREATE PURPOSE DEFAULT VALUE

                    //COMPLETENESS
//                    BOOL state = [self validateQuestionObject:mo];
//                    NSString *is_complete = [NSString stringWithFormat:@"%@", @(state) ];
//                    [mo setValue:is_complete forKey:@"complete"];
                    [self validateQuestionObject:mo];
                    
                    // INSERT TRUE FALSE
                    if ([question_type_id isEqualToString:@"1"]) {
                        [self processChoices:[self generateTrueOrFalseForQuestionID:question_id] managedObject:mo];
                    }
                    
                    // INSERT MULTIPLE CHOICE
                    if ([question_type_id isEqualToString:@"3"]) {
                        [self processChoices:[self generateMultipleChoiceForQuestionID:question_id] managedObject:mo];
                    }
                }
                
                [self saveTreeContext:ctx];
                
                if (dataBlock) {
                    dataBlock(userData);
                }
            }
        }];
    }
}

- (NSDictionary *)fetchDefaultOptionType:(NSString *)type {
    
    
    NSManagedObjectContext *ctx = _workerContext;
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:type];


    NSSortDescriptor *sortByID = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES];
    [request setSortDescriptors:@[sortByID]];
    
    
    
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:request error:&error];
    

    
    if (error) {
        NSLog(@"%s ERROR : %@", __PRETTY_FUNCTION__, [error localizedDescription]);
        return nil;
    }
    
    if (items.count > 0) {
        NSManagedObject *mo = (NSManagedObject *)[items firstObject];
        NSString *type_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"id"] ];
        NSString *type_value = [NSString stringWithFormat:@"%@", [mo valueForKey:@"value"] ];
        [data setValue:type_id forKey:@"id"];
        [data setValue:type_value forKey:@"value"];
    }
    
    return data;
}

- (void)insertQuestionWithItemCount:(NSString *)count
                             object:(NSMutableDictionary *)object
                          doneBlock:(TestGuruDoneBlock)doneBlock {

    NSInteger iteration = [count integerValue];
    
    if ( (iteration > 0) && (object != nil)  ) {
        
        NSManagedObjectContext *ctx = _workerContext;
        
        [ctx performBlock:^{
            
            for (NSInteger i = 0; i < iteration; i++) {
                
                NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kQuestionEntity
                                                                    inManagedObjectContext:ctx];
                /*
                 "question_parent_id": "0", //checked
                 "name": "test1", //checked
                 "points": "1", //checked
                 "question_text": "test1", //checked
                 "image_url": "", //checked
                 "question_type_id": "3", //checked
                 "proficiency_level_id": "1", //checked
                 "correct_answer_feedback": "",
                 "learning_skills_id": "1",
                 "wrong_answer_feedback": "",
                 "general_feedback": "",
                 "is_public": 1
                 */

                NSString *date_now = [self stringValue: [NSDate date] ];
                NSString *question_id = [NSString stringWithFormat:@"%@-%@-%@",
                                         [Utils randomNumber],[Utils randomNumber],[Utils randomNumber]];
                
                [mo setValue:@"0" forKey:@"question_parent_id"];
                [mo setValue:@"100.0" forKey:@"points"];
                [mo setValue:date_now forKey:@"date_created"];
                [mo setValue:date_now forKey:@"date_modified"];
                [mo setValue:@"" forKey:@"image_url"];
                [mo setValue:@"" forKey:@"name"];
                [mo setValue:@"" forKey:@"question_text"];
                [mo setValue:question_id forKey:@"id"];
                
                for (NSString *key in object.allKeys) {
                    
                    if (![key isEqualToString:@"tags"]) {
                        NSString *value = [self stringValue: object[key] ];
                        [mo setValue:value forKey:key];
                    }
                    
                    if ([key isEqualToString:@"tags"]) {
                        [mo setValue:[self insertTags:object[key] managedObject:mo] forKey:key];
                    }
                    
                }

                NSString *question_type_id = [NSString stringWithFormat:@"%@", object[@"question_type_id"] ];
                if ([question_type_id isEqualToString:@"1"]) {
                    [self processChoices:[self generateTrueOrFalseForQuestionID:question_id] managedObject:mo];
                }
            }
            
            [self saveTreeContext:ctx];
            
            if (doneBlock) {
                doneBlock(YES);
            }
            
        }];
    }
}

- (NSArray *)generateTrueOrFalseForQuestionID:(NSString *)questionId {
    
    NSString *true_string = NSLocalizedString(@"true", nil);
    NSString *false_string = NSLocalizedString(@"false", nil);
    
    NSDictionary *true_data = [self buildDefaultChoiceItemString:true_string
                                                      questionID:questionId
                                                          withID:[self generateGUID]
                                                       withOrder:@"0"
                                                       isCorrect:YES];
    
    NSDictionary *false_data = [self buildDefaultChoiceItemString:false_string
                                                       questionID:questionId
                                                           withID:[self generateGUID]
                                                        withOrder:@"1"
                                                        isCorrect:NO];
    NSArray *list = @[true_data,false_data];
    
    NSLog(@"list: %@", list);
    
    return list;
}

- (void)insertChoiceItemWithQuestionID:(NSString *)value {

    NSPredicate *predicate = [self predicateForKeyPath:@"id" andValue:value];
    NSManagedObject *question = [self getEntity:kQuestionEntity predicate:predicate];
    
    NSManagedObjectContext *ctx = question.managedObjectContext;
    [ctx performBlock:^{
        
        NSString *question_id = [NSString stringWithFormat:@"%@", [question valueForKey:@"id"] ];
        
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kChoiceItemEntity];
        fetchRequest.predicate = [self predicateForKeyPath:@"question_id" andValue:question_id];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"order_number" ascending:NO]];
        fetchRequest.fetchLimit = 1;
        
        NSManagedObject *data = [[ctx executeFetchRequest:fetchRequest error:nil] firstObject];
        NSString *index_number = [NSString stringWithFormat:@"%@", [data valueForKey:@"order_number"] ];
        NSInteger increment = [index_number integerValue] + 1;
        NSString *order_number_incremented = [NSString stringWithFormat:@"%ld", (long)increment];
        
        NSDictionary *choice_object = [self buildDefaultChoiceItemString:@""
                                                              questionID:question_id
                                                                  withID:[self generateGUID]
                                                               withOrder:order_number_incremented
                                                               isCorrect:NO];
        
        NSString *date_created = [self stringValue: choice_object[@"date_created"] ];
        NSString *date_modified = [self stringValue: choice_object[@"date_modified"] ];
        NSString *choice_id = [self stringValue: choice_object[@"id"] ];
        NSString *is_correct = [self stringValue: choice_object[@"is_correct"] ];
        NSString *is_deleted = [self stringValue: choice_object[@"is_deleted"] ];
        NSString *order_number = [self stringValue: choice_object[@"order_number"] ];
        NSString *choice_question_id = [self stringValue: choice_object[@"question_id"] ];
        NSString *suggestive_feedback = [self stringValue: choice_object[@"suggestive_feedback"] ];
        NSString *choice_text = [self stringValue: choice_object[@"text"] ];
        
        NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:kChoiceItemEntity inManagedObjectContext:ctx];
        [c_mo setValue:date_created forKey:@"date_created"];
        [c_mo setValue:date_modified forKey:@"date_modified"];
        [c_mo setValue:choice_id forKey:@"id"];
        [c_mo setValue:is_correct forKey:@"is_correct"];
        [c_mo setValue:is_deleted forKey:@"is_deleted"];
        [c_mo setValue:[NSNumber numberWithDouble:[order_number doubleValue]] forKey:@"order_number"];
        [c_mo setValue:choice_question_id forKey:@"question_id"];
        [c_mo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
        [c_mo setValue:choice_text forKey:@"text"];
        [c_mo setValue:question forKey:@"question_choice"];
        
        [self saveTreeContext:ctx];
    }];
}

- (void)insertChoiceItemWithQuestionID:(NSString *)value doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSPredicate *predicate = [self predicateForKeyPath:@"id" andValue:value];
    NSManagedObject *question = [self getEntity:kQuestionEntity predicate:predicate];
    
    NSManagedObjectContext *ctx = question.managedObjectContext;
    [ctx performBlock:^{
        
        NSString *question_id = [NSString stringWithFormat:@"%@", [question valueForKey:@"id"] ];
        
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kChoiceItemEntity];
        fetchRequest.predicate = [self predicateForKeyPath:@"question_id" andValue:question_id];
        fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"order_number" ascending:NO]];
        fetchRequest.fetchLimit = 1;
        
        NSManagedObject *data = [[ctx executeFetchRequest:fetchRequest error:nil] firstObject];
        NSString *index_number = [NSString stringWithFormat:@"%@", [data valueForKey:@"order_number"] ];
        NSInteger increment = [index_number integerValue] + 1;
        NSString *order_number_incremented = [NSString stringWithFormat:@"%ld", (long)increment];
        
        NSDictionary *choice_object = [self buildDefaultChoiceItemString:@""
                                                              questionID:question_id
                                                                  withID:[self generateGUID]
                                                               withOrder:order_number_incremented
                                                               isCorrect:NO];
        
        NSString *date_created = [self stringValue: choice_object[@"date_created"] ];
        NSString *date_modified = [self stringValue: choice_object[@"date_modified"] ];
        NSString *choice_id = [self stringValue: choice_object[@"id"] ];
        NSString *is_correct = [self stringValue: choice_object[@"is_correct"] ];
        NSString *is_deleted = [self stringValue: choice_object[@"is_deleted"] ];
        NSString *order_number = [self stringValue: choice_object[@"order_number"] ];
        NSString *choice_question_id = [self stringValue: choice_object[@"question_id"] ];
        NSString *suggestive_feedback = [self stringValue: choice_object[@"suggestive_feedback"] ];
        NSString *choice_text = [self stringValue: choice_object[@"text"] ];
        
        NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:kChoiceItemEntity inManagedObjectContext:ctx];
        [c_mo setValue:date_created forKey:@"date_created"];
        [c_mo setValue:date_modified forKey:@"date_modified"];
        [c_mo setValue:choice_id forKey:@"id"];
        [c_mo setValue:is_correct forKey:@"is_correct"];
        [c_mo setValue:is_deleted forKey:@"is_deleted"];
        [c_mo setValue:[NSNumber numberWithDouble:[order_number doubleValue]] forKey:@"order_number"];
        [c_mo setValue:choice_question_id forKey:@"question_id"];
        [c_mo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
        [c_mo setValue:choice_text forKey:@"text"];
        [c_mo setValue:question forKey:@"question"];
        
        [self saveTreeContext:ctx];
        
        if (doneBlock) {
            doneBlock(YES);
        }
    }];
}

- (void)saveQuestionChoices:(NSArray *)choices questionid:(NSString *)questionid doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *entity = kQuestionEntity;
    
    NSPredicate *predicate = [self predicateForKeyPath:@"id" andValue:questionid];
    NSManagedObject *mo = [self getEntity:entity predicate:predicate];
    if ( ([choices count] > 0) && (mo != nil) ) {
        NSManagedObjectContext *ctx = _workerContext;
        [ctx performBlock:^{
            NSMutableSet *choice_set = [NSMutableSet set];
            for (NSManagedObject *c in choices) {
                
                /*
                 choice_image
                 choice_image_data binary
                 date_created
                 date_modified
                 id
                 is_correct
                 is_deleted
                 order_number
                 question_id
                 suggestive_feedback
                 tex
                 */
                
                NSString *date_created = [self stringValue: [c valueForKey:@"date_created"] ];
                NSString *date_modified = [self stringValue: [c valueForKey:@"date_modified"] ];
                NSString *choice_id = [self stringValue: [c valueForKey:@"id"] ];
                NSString *is_correct = [self stringValue: [c valueForKey:@"is_correct"] ];
                NSString *is_deleted = [self stringValue: [c valueForKey:@"is_deleted"] ];
                NSString *order_number = [self stringValue: [c valueForKey:@"order_number"] ];
                NSString *choice_question_id = [self stringValue: [c valueForKey:@"question_id"] ];
                NSString *suggestive_feedback = [self stringValue: [c valueForKey:@"suggestive_feedback"] ];
                NSString *choice_text = [self stringValue: [c valueForKey:@"text"] ];
                
                //URL
                NSString *choice_image = [self stringValue: [c valueForKey:@"choice_image"] ];
                
                //BLOB
                NSString *choice_image_data = [c valueForKey:@"choice_image_data"];
                
                NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:kChoiceItemEntity inManagedObjectContext:ctx];
                [c_mo setValue:date_created forKey:@"date_created"];
                [c_mo setValue:date_modified forKey:@"date_modified"];
                [c_mo setValue:choice_id forKey:@"id"];
                [c_mo setValue:is_correct forKey:@"is_correct"];
                [c_mo setValue:is_deleted forKey:@"is_deleted"];
                [c_mo setValue:[NSNumber numberWithDouble:[order_number doubleValue]] forKey:@"order_number"];
                [c_mo setValue:choice_question_id forKey:@"question_id"];
                [c_mo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
                [c_mo setValue:choice_text forKey:@"text"];
                [c_mo setValue:choice_image forKey:@"choice_image"];
                [c_mo setValue:choice_image_data forKey:@"choice_image_data"];
                
                //CREATE NEW CHOICE SETS
                [choice_set addObject:c_mo];
            }
            
            if ([choice_set count] > 0) {
                [mo setValue:choice_set forKey:@"choices"];
                [self saveTreeContext:ctx];
                
                if (doneBlock) {
                    doneBlock(YES);
                }
            }
        }];
    }
}

- (void)deleteChoiceItem:(NSManagedObject *)choice {
    
    NSManagedObjectContext *ctx = choice.managedObjectContext;
    [ctx performBlock:^{
        [ctx deleteObject:choice];
        [self saveTreeContext:ctx];
    }];
}

- (void)deleteChoiceItem:(NSManagedObject *)choice doneBlock:(TestGuruDoneBlock)doneBlock {

    NSManagedObjectContext *ctx = choice.managedObjectContext;
    [ctx performBlockAndWait:^{
        [ctx deleteObject:choice];
        [self saveTreeContext:ctx];
        
        if (doneBlock) {
            doneBlock(YES);
        }
    }];
}

- (NSArray *)generateMultipleChoiceForQuestionID:(NSString *)questionId {
    
//    NSString *choice_string = NSLocalizedString(@"Choice", nil);
    
//    NSString *item_one = [NSString stringWithFormat:@"A. %@", choice_string];
//    NSString *item_two = [NSString stringWithFormat:@"B. %@", choice_string];
//    NSString *item_three = [NSString stringWithFormat:@"C. %@", choice_string];
//    NSString *item_four = [NSString stringWithFormat:@"D. %@", choice_string];
    
    
    NSString *item_one = @"";
    NSString *item_two = @"";
    
    NSDictionary *itemA = [self buildDefaultChoiceItemString:item_one
                                                  questionID:questionId
                                                      withID:[self generateGUID]
                                                   withOrder:@"1"
                                                   isCorrect:YES];
    
    NSDictionary *itemB = [self buildDefaultChoiceItemString:item_two
                                                  questionID:questionId
                                                      withID:[self generateGUID]
                                                   withOrder:@"2"
                                                   isCorrect:NO];
    
//    NSDictionary *itemC = [self buildDefaultChoiceItemString:item_three
//                                                  questionID:questionId
//                                                      withID:[self generateGUID]
//                                                   withOrder:@"3"
//                                                   isCorrect:NO];
//    
//    NSDictionary *itemD = [self buildDefaultChoiceItemString:item_four
//                                                  questionID:questionId
//                                                      withID:[self generateGUID]
//                                                   withOrder:@"4"
//                                                   isCorrect:NO];
    
//    NSArray *list = @[itemA,itemB,itemC,itemD];
    NSArray *list = @[itemA,itemB];
    NSLog(@"list multiple choices: %@", list);
    
    return list;
}

- (NSDictionary *)buildDefaultChoiceItemString:(NSString *)item_string
                                    questionID:(NSString *)questionID
                                        withID:(NSString *)item_id
                                     withOrder:(NSString *)order isCorrect:(BOOL)correct {
    
    NSString *date_created = [self stringValue: [NSDate date] ];
    NSString *date_modified = [self stringValue: [NSDate date] ];
    NSString *choice_id = [NSString stringWithFormat:@"%@", item_id];
    NSString *is_correct = (correct) ? @"100" : @"0";
    NSString *is_deleted = @"0";
    NSString *order_number = [NSString stringWithFormat:@"%@", order];
    NSString *question_id = [NSString stringWithFormat:@"%@", questionID];
    NSString *suggestive_feedback = @"";
    NSString *choice_text = [NSString stringWithFormat:@"%@", item_string];
    
    NSDictionary *c = @{
                            @"date_created":date_created,
                            @"date_modified":date_modified,
                            @"id":choice_id,
                            @"is_correct":is_correct,
                            @"is_deleted":is_deleted,
                            @"order_number":order_number,
                            @"question_id":question_id,
                            @"suggestive_feedback":suggestive_feedback,
                            @"text":choice_text
                        };
    
    return c;
}

- (void)processTestData:(NSDictionary *)d sectionName:(NSString *)sectionName managedObjectContext:(NSManagedObjectContext *)ctx {
    
    NSLog(@"%s data : %@", __PRETTY_FUNCTION__, d);
    
    /*
     "id": "1",
     "name": "hth",
     "description": "egre",
     "quiz_settings_id": "1",
     "user_id": "2",
     "is_graded": "1",
     "date_modified": "2015-12-10 15:10:14",
     "ItemCount": "2"
     
     "id": "1",
     "name": "hth",
     "description": "egre",
     "quiz_settings_id": "1",
     "user_id": "2",
     "is_graded": "1",
     "date_modified": "2015-12-10 15:10:14",
     "item_count": "2"
     
     */
    
    NSString *obj_id = [self stringValue:d[@"id"]];
    NSString *name = [self stringValue:d[@"name"]];
    
    //TEST DESCRIPTION
    NSString *description = [self stringValue:d[@"description"]];
    NSData *attributedData = [description dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *options = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]};
    NSAttributedString *attributted_description = [[NSAttributedString alloc] initWithData:attributedData options:options documentAttributes:nil error:nil];
    description = [attributted_description string];
    
    NSString *quiz_settings_id = [self stringValue:d[@"quiz_settings_id"]];
    NSString *user_id = [self stringValue:d[@"user_id"]];
    NSString *is_graded = [self stringValue:d[@"is_graded"]];
    NSString *date_modified = [self stringValue:d[@"date_modified"]];
    NSString *item_count = [self stringValue:d[@"item_count"]];
    
    NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kTestEntity inManagedObjectContext:ctx];
    
    [mo setValue:obj_id forKey:@"id"];
    [mo setValue:name forKey:@"name"];
    [mo setValue:description forKey:@"desc"];
    [mo setValue:quiz_settings_id forKey:@"quiz_settings_id"];
    [mo setValue:user_id forKey:@"user_id"];
    [mo setValue:is_graded forKey:@"is_graded"];
    [mo setValue:date_modified forKey:@"date_modified"];
    [mo setValue:item_count forKey:@"item_count"];
    
    NSString *search_string = [NSString stringWithFormat:@"%@ %@", name, description];
    [mo setValue:[search_string lowercaseString] forKey:@"search_string"];
}

- (void)requestTestListForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruTestList, userid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSDictionary *meta = dictionary[@"_meta"];
              NSString *metaCount = [self stringValue:meta[@"count"]];
              NSString *metaStatus = meta[@"status"];
              
              NSManagedObjectContext *ctx = self.workerContext;
              
              if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
                  [ctx performBlock:^{
                      [self clearDataForEntity:kTestEntity withPredicate:nil context:ctx];
                  }];
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
                  
                  return;
              }
                  
              if (dictionary[@"records"] != nil ) {
                  NSArray *records = dictionary[@"records"];
                      
                  if (records.count > 0) {
                      // NSManagedObjectContext *ctx = self.workerContext;
                                            
                      [ctx performBlock:^{
                          
                          BOOL status = [self clearDataForEntity:kTestEntity withPredicate:nil context:ctx];
                          
                          if (status) {
                              
                              for (NSDictionary *d in records) {
                                  
                                  NSLog(@"data : %@", d);
                                  
                                  /*
                                   
                                   "id": "32",
                                   "name": "Biology Test",   //CHECKED
                                   "description": "Demo Test 101",   //CHECKED
                                   "quiz_settings_id": "32",   //CHECKED
                                   "user_id": "16",     //CHECKED
                                   "is_graded": "1",
                                   "date_modified": "2015-08-28 08:44:16",
                                   "item_count": "4"
                                   
                                   */
                                  
                                  [self processTestData:d sectionName:@"" managedObjectContext:ctx];
                                  
//                                  NSString *obj_id = [self stringValue:d[@"id"]];
//                                  NSString *name = [self stringValue:d[@"name"]];
//                                  NSString *description = [self stringValue:d[@"description"]];
//                                  NSString *quiz_settings_id = [self stringValue:d[@"quiz_settings_id"]];
//                                  NSString *user_id = [self stringValue:d[@"user_id"]];
//                                  NSString *is_graded = [self stringValue:d[@"is_graded"]];
//                                  NSString *date_modified = [self stringValue:d[@"date_modified"]];
//                                  NSString *item_count = [self stringValue:d[@"item_count"]];
//                                  
//                                  NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kTestEntity inManagedObjectContext:ctx];
//                                  
//                                  [mo setValue:obj_id forKey:@"id"];
//                                  [mo setValue:name forKey:@"name"];
//                                  [mo setValue:description forKey:@"desc"];
//                                  [mo setValue:quiz_settings_id forKey:@"quiz_settings_id"];
//                                  [mo setValue:user_id forKey:@"user_id"];
//                                  [mo setValue:is_graded forKey:@"is_graded"];
//                                  [mo setValue:date_modified forKey:@"date_modified"];
//                                  [mo setValue:item_count forKey:@"item_count"];
//                                  
//                                  NSString *search_string = [NSString stringWithFormat:@"%@ %@", name, description];
//                                  [mo setValue:[search_string lowercaseString] forKey:@"search_string"];
                              }
                              
                              [self saveTreeContext:ctx];
                              
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }
                          
                      }];
                  }
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)requestTestListForTagsForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruTestListByTags, userid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSDictionary *meta = dictionary[@"_meta"];
              NSString *metaCount = [self stringValue:meta[@"count"]];
              NSString *metaStatus = meta[@"status"];
              
              NSManagedObjectContext *ctx = self.workerContext;
              
              if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
                  [ctx performBlock:^{
                      [self clearDataForEntity:kTestEntity withPredicate:nil context:ctx];
                  }];
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
                  
                  return;
              }
              
              if (dictionary[@"records"] != nil ) {
                  NSArray *records = dictionary[@"records"];
                  
                  if (records.count > 0) {
                      // NSManagedObjectContext *ctx = self.workerContext;
                      
                      [ctx performBlock:^{
                          
                          BOOL status = [self clearDataForEntity:kTestEntity withPredicate:nil context:ctx];
                          
                          if (status) {
                              
                              for (NSDictionary *object in records) {
                                  
//                                  NSString *quiz_tag_id = [self stringValue:object[@"quiz_tag_id"]];
                                  NSString *quiz_tag_name = [self stringValue:object[@"quiz_tag_name"]];
                                  
                                  if ( [self isArrayObject:object[@"data"]] ) {
                                      
                                      NSArray *items = object[@"data"];
                                      for (NSDictionary *d in items) {
                                          
                                          NSLog(@"data : %@", d);
                                          [self processTestData:d
                                                    sectionName:quiz_tag_name
                                           managedObjectContext:ctx];
                                      }
                                      
                                  }
                                  
                              }
                              
                              [self saveTreeContext:ctx];
                              
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }
                          
                      }];
                  }
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
              }
          }//end
          
      }];
    
    [task resume];

}

- (void)requestTestListForTestCategoryForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);

    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruTestListByCategory, userid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }

          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSDictionary *meta = dictionary[@"_meta"];
              NSString *metaCount = [self stringValue:meta[@"count"]];
              NSString *metaStatus = meta[@"status"];

              NSManagedObjectContext *ctx = self.workerContext;

              if (dictionary == nil ||
                  dictionary[@"records"] == nil ||
                  [metaCount isEqualToString:@"0"] ||
                  [metaStatus isEqualToString:@"ERROR"]) {

                  [ctx performBlock:^{
                      [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                  }];

                  if (doneBlock) {
                      doneBlock(NO);
                  }

                  return;
              }

              if (dictionary[@"records"] != nil ) {
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {

                      [ctx performBlock:^{

                          BOOL status = [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                          if (status) {

                              for (NSDictionary *object in records) {

//                                  NSString *quiz_category_id = [self stringValue:object[@"quiz_category_id"]];
                                  NSString *quiz_category_name = [self stringValue:object[@"quiz_category_name"]];

                                  if ( [self isArrayObject:object[@"data"]] ) {

                                      NSArray *items = object[@"data"];
                                      for (NSDictionary *d in items) {

                                          NSLog(@"data : %@", d);
                                          [self processTestData:d
                                                    sectionName:quiz_category_name
                                           managedObjectContext:ctx];
                                      }

                                  }

                              }

                              [self saveTreeContext:ctx];

                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }
                          
                      }];
                  }
              }
          }//end
      }];
    
    [task resume];
    
}

- (void)requestTestListForTestTypeForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock {

    NSLog(@"%s", __PRETTY_FUNCTION__);
    
//    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruTestListByTestType, userid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSDictionary *meta = dictionary[@"_meta"];
              NSString *metaCount = [self stringValue:meta[@"count"]];
              NSString *metaStatus = meta[@"status"];
              
              NSManagedObjectContext *ctx = self.workerContext;
              
              if (dictionary == nil ||
                  dictionary[@"records"] == nil ||
                  [metaCount isEqualToString:@"0"] ||
                  [metaStatus isEqualToString:@"ERROR"]) {
                  
                  [ctx performBlock:^{
                      [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                  }];
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
                  
                  return;
              }
              
              if (dictionary[@"records"] != nil ) {
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {
                      
                      [ctx performBlock:^{
                          
                          BOOL status = [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                          if (status) {
                              
                              for (NSDictionary *object in records) {
                                  
//                                  NSString *quiz_type_id = [self stringValue:object[@"quiz_type_id"]];
                                  NSString *quiz_type_name = [self stringValue:object[@"quiz_type_name"]];
                                  
                                  if ( [self isArrayObject:object[@"data"]] ) {
                                      
                                      NSArray *items = object[@"data"];
                                      for (NSDictionary *d in items) {
                                          
                                          NSLog(@"data : %@", d);
                                          [self processTestData:d
                                                    sectionName:quiz_type_name
                                           managedObjectContext:ctx];
                                      }
                                      
                                  }
                                  
                              }
                              
                              [self saveTreeContext:ctx];
                              
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }
                          
                      }];
                      
                      
                  }
                  
              }
          }//end
          
      }];
    
    [task resume];
    
}

- (void)requestTestListForGradeRationgForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    //    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruTestListByGradeRating, userid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSDictionary *meta = dictionary[@"_meta"];
              NSString *metaCount = [self stringValue:meta[@"count"]];
              NSString *metaStatus = meta[@"status"];
              
              NSManagedObjectContext *ctx = self.workerContext;
              
              if (dictionary == nil ||
                  dictionary[@"records"] == nil ||
                  [metaCount isEqualToString:@"0"] ||
                  [metaStatus isEqualToString:@"ERROR"]) {
                  
                  [ctx performBlock:^{
                      [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                  }];
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
                  
                  return;
              }
              
              if (dictionary[@"records"] != nil ) {
                  NSArray *records = dictionary[@"records"];
                  if (records.count > 0) {
                      
                      [ctx performBlock:^{
                          
                          BOOL status = [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                          if (status) {
                              
                              for (NSDictionary *object in records) {
                                  
                                  //                                  NSString *quiz_type_id = [self stringValue:object[@"quiz_type_id"]];
                                  NSString *quiz_type_name = [self stringValue:object[@"quiz_type_name"]];
                                  
                                  if ( [self isArrayObject:object[@"data"]] ) {
                                      
                                      NSArray *items = object[@"data"];
                                      for (NSDictionary *d in items) {
                                          
                                          NSLog(@"data : %@", d);
                                          [self processTestData:d
                                                    sectionName:quiz_type_name
                                           managedObjectContext:ctx];
                                      }
                                      
                                  }
                                  
                              }
                              
                              [self saveTreeContext:ctx];
                              
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }
                          
                      }];
                      
                      
                  }
                  
              }
          }//end
          
      }];
    
    [task resume];
    
}

- (void)requestDetailsForTest:(NSManagedObject *)object doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *test_id = [self stringValue:[object valueForKey:@"id"] ];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruTestDetails, test_id]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {

              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil ) {
                  
                  NSManagedObjectContext *ctx = _workerContext;
                  [ctx performBlock:^{
                      
                      NSDictionary *records = dictionary[@"records"];
                      NSLog(@"THE URL [%@]", url);
                      NSLog(@"recordsxxxxxxxxxx : %@", records);
                      
                      NSArray *record_data = records[@"data"];
                      if (record_data.count > 0) {
                          
                          NSDictionary *d = record_data.lastObject;
                          NSString *allow_review = [self stringValue:d[@"allow_review"]];
                          NSString *attempts = [self stringValue:d[@"attempts"]];
                          NSString *raw_date_close = [self stringValue:d[@"date_close"]];
                          NSString *raw_date_open = [self stringValue:d[@"date_open"]];
                          NSString *description = [self stringValue:d[@"description"]];
                          NSString *is_forced_complete = [self stringValue:d[@"is_forced_complete"]];
                          NSString *is_shuffle_answers = [self stringValue:d[@"is_shuffle_answers"]];
                          NSString *learning_skills_id = [self stringValue:d[@"learning_skills_id"]];
                          NSString *learning_skills_name = [self stringValue:d[@"learning_skills_name"]];
                          NSString *name = [self stringValue:d[@"name"]];
                          NSString *passing_rate = [self stringValue:d[@"passing_rate"]];
                          NSString *password = [self stringValue:d[@"password"]];
                          NSString *proficiency_level_id = [self stringValue:d[@"proficiency_level_id"]];
                          NSString *proficiency_level_name = [self stringValue:d[@"proficiency_level_name"]];
                          
                          if (d[@"difficulty_level_id"] != nil) {
                              proficiency_level_id = [self stringValue:d[@"difficulty_level_id"]];
                          }
                          
                          if (d[@"difficulty_level_name"] != nil) {
                              proficiency_level_name = [self stringValue:d[@"difficulty_level_name"]];
                          }
                          
                          NSString *question_type_name = [self stringValue:d[@"question_type_name"]];
                          NSString *quiz_category_id = [self stringValue:d[@"quiz_category_id"]];
                          NSString *quiz_category_name = [self stringValue:d[@"quiz_category_name"]];
                          NSString *quiz_result_type_id = [self stringValue:d[@"quiz_result_type_id"]];
                          NSString *quiz_result_type_name = [self stringValue:d[@"quiz_result_type_name"]];
                          NSString *quiz_settings_id = [self stringValue:d[@"quiz_settings_id"]];
                          NSString *quiz_shuffling_mode = [self stringValue:d[@"quiz_shuffling_mode"]];
                          NSString *quiz_stage_id = [self stringValue:d[@"quiz_stage_id"]];
                          NSString *quiz_stage_name = [self stringValue:d[@"quiz_stage_name"]];
                          NSString *quiz_type_id = [self stringValue:d[@"quiz_type_id"]];
                          NSString *show_feedbacks = [self stringValue:d[@"show_feedbacks"]];
                          NSString *time_limit = [self stringValue:d[@"time_limit"]];
                          NSString *total_score = [self stringValue:d[@"total_score"]];
                          NSString *user_id = [self stringValue:d[@"user_id"]];
                          
                          NSManagedObject *mo = [self getEntity:kQuizEntity attribute:@"id" parameter:test_id context:ctx];
                          
                          raw_date_open = [self handleBlankDate:raw_date_open];
                          raw_date_close = [self handleBlankDate:raw_date_close];
                          
                          NSString* date_close = [self translateDate:raw_date_close from:@"UTC" to:@"GMT"];
                          NSString* date_open = [self translateDate:raw_date_open from:@"UTC" to:@"GMT"];
                          
                          [mo setValue:test_id forKey:@"id"];
                          [mo setValue:allow_review forKey:@"allow_review"];
                          [mo setValue:attempts forKey:@"attempts"];
                          [mo setValue:date_close forKey:@"date_close"];
                          [mo setValue:date_open forKey:@"date_open"];
                          [mo setValue:description forKey:@"desc"];
                          [mo setValue:is_forced_complete forKey:@"is_forced_complete"];
                          [mo setValue:is_shuffle_answers forKey:@"is_shuffle_answers"];
                          [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
                          [mo setValue:learning_skills_name forKey:@"learning_skills_name"];
                          [mo setValue:name forKey:@"name"];
                          [mo setValue:passing_rate forKey:@"passing_rate"];
                          [mo setValue:password forKey:@"password"];
                          [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];
                          [mo setValue:proficiency_level_name forKey:@"proficiency_level_name"];
                          [mo setValue:question_type_name forKey:@"question_type_name"];
                          [mo setValue:quiz_category_id forKey:@"quiz_category_id"];
                          [mo setValue:quiz_category_name forKey:@"quiz_category_name"];
                          [mo setValue:quiz_result_type_id forKey:@"quiz_result_type_id"];
                          [mo setValue:quiz_result_type_name forKey:@"quiz_result_type_name"];
                          [mo setValue:quiz_settings_id forKey:@"quiz_settings_id"];
                          [mo setValue:quiz_shuffling_mode forKey:@"quiz_shuffling_mode"];
                          [mo setValue:quiz_stage_id forKey:@"quiz_stage_id"];
                          [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
                          [mo setValue:quiz_type_id forKey:@"quiz_type_id"];
                          [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
                          [mo setValue:time_limit forKey:@"time_limit"];
                          [mo setValue:total_score forKey:@"total_score"];
                          [mo setValue:user_id forKey:@"user_id"];
                          
                          if ([self isArrayObject: records[@"questions"] ]) {
                              //PARSE QUESTIONS
                              NSArray *q = [NSArray arrayWithArray: records[@"questions"] ];
                              
                              // SET QUESTIONS
                              [mo setValue:[self processQuiz:mo questions:q] forKey:@"questions"];
                          }
                          
                          [self saveTreeContext:ctx];
                          
//                          [self requestCreateTest:mo doneBlock:nil];
                          
                          if (doneBlock) {
                              doneBlock(YES);
                          }
                          
                      }
                      
                  }];//END -> [ctx performBlock:
                  
              }// END -> (dictionary[@"records"] != nil )
              
          }//ERROR
          
      }];//SESSION
    
    [task resume];
}

- (NSString *)handleBlankDate:(NSString *)date_string {
    
    if ([date_string isEqualToString:@"0000-00-00 00:00:00"]) {
        NSDate *date = [NSDate date];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        //DATE PROCESSING
        NSString *year_template = @"yyyy";
        [formatter setDateFormat:year_template];
        NSString *year = [formatter stringFromDate:date];
        
        date_string = [NSString stringWithFormat:@"%@-01-01 00:00:00", year];
    }
    
    return date_string;
}

- (id)translateDate:(NSString *)msgArrivedDate from:(NSString *)fromTimeZone to:(NSString *)toTimeZone {
    NSDate *date = nil;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    if([fromTimeZone isEqualToString:@"UTC"]) {
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    }
    
    if([fromTimeZone isEqualToString:@"GMT"]) {
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    }
    
    if([msgArrivedDate length] > 0) {
        date = [dateFormatter dateFromString:msgArrivedDate];
    } else {
        date = [NSDate date];
    }
    
    if([toTimeZone isEqualToString:@"UTC"]) {
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    }
    
    if([toTimeZone isEqualToString:@"GMT"]) {
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    }
    
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

- (NSSet *)processQuiz:(NSManagedObject *)object questions:(NSArray *)questions {

    NSMutableSet *set = [NSMutableSet set];
    
    if (questions.count > 0) {
        
        NSManagedObjectContext *ctx = object.managedObjectContext;
        for (NSDictionary *d in questions) {
            
            NSString *obj_id = [self stringValue: d[@"id"] ];
            NSString *image_url = [self stringValue: d[@"image_url"] ];
            NSString *points = [self stringValue: d[@"points"] ];
            NSString *question_id = [self stringValue: d[@"question_id"] ];
            NSString *question_text = [self stringValue: d[@"question_text"] ];
            NSString *question_type_id = [self stringValue: d[@"question_type_id"] ];
            NSString *question_type_name = [self stringValue: d[@"question_type_name"] ];
            NSString *quiz_id = [self stringValue: d[@"quiz_id"] ];
            
            NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kQuizQuestionEntity inManagedObjectContext:ctx];
            [mo setValue:obj_id forKey:@"id"];
            [mo setValue:image_url forKey:@"image_url"];
            [mo setValue:points forKey:@"points"];
            [mo setValue:question_id forKey:@"question_id"];
            [mo setValue:question_text forKey:@"question_text"];
            [mo setValue:question_type_id forKey:@"question_type_id"];
            [mo setValue:question_type_name forKey:@"question_type_name"];
            [mo setValue:quiz_id forKey:@"quiz_id"];
            
            //PARSE CHOICES
            NSLog(@"QUESTION_CHOICES!!! [%@]", d[@"question_choices"]);
            NSString *choice_string = [self stringValue:d[@"question_choices"]];
            NSLog(@"CHOICE STRING!!!! [%@]", choice_string);
            [mo setValue: [self processQuizChoices:mo choices: choice_string ] forKey:@"choices"];
            
            
            NSLog(@"THE CHOICES!!!!!! [%@]", [mo valueForKey:@"choices"]);
            
            
            
            //ADD CHOICE MO
            [set addObject:mo];
        }
    }
    
    
    NSLog(@"HERE IS THE SET [%@]", set);
    
    
    return set;
}

- (NSSet *)processQuizChoices:(NSManagedObject *)object choices:(NSString *)choicestring {

    NSMutableSet *set = [NSMutableSet set];
    
    if (choicestring.length > 0) {
        NSString *choice_string = [self normalizeChoiceString:choicestring encyrpt:NO];
        NSData *choice_data = [choice_string dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *choices = [self parseResponseData:choice_data];
        if (choices.count > 0) {
            
            NSManagedObjectContext *ctx = object.managedObjectContext;
            
            for (NSDictionary *d in choices) {
                
                NSString *choice_id = [self stringValue: d[@"id"] ];
                NSString *date_created = [self stringValue: d[@"date_created"] ];
                NSString *date_modified = [self stringValue: d[@"date_modified"] ];
                NSString *is_correct = [self stringValue: d[@"is_correct"] ];
                NSString *is_deleted = [self stringValue: d[@"is_deleted"] ];
                NSString *order_number = [self stringValue: d[@"order_number"] ];
                NSString *question_id = [self stringValue: d[@"question_id"] ];
                NSString *suggestive_feedback = [self stringValue: d[@"suggestive_feedback"] ];
                NSString *text = [self stringValue: d[@"text"] ];
                
                NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kQuizQuestionChoiceEntity inManagedObjectContext:ctx];
                [mo setValue:choice_id forKey:@"id"];
                [mo setValue:date_created forKey:@"date_created"];
                [mo setValue:date_modified forKey:@"date_modified"];
                [mo setValue:is_correct forKey:@"is_correct"];
                [mo setValue:is_deleted forKey:@"is_deleted"];
                [mo setValue:order_number forKey:@"order_number"];
                [mo setValue:question_id forKey:@"question_id"];
                [mo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
                [mo setValue:text forKey:@"text"];
                
                [set addObject:mo];
            }
            
        }// (choices.count > 0)
    }
    
    return set;
}

- (NSString *)generateGUID {
    
    NSString *guid = [NSString stringWithFormat:@"%@-%@-%@", [Utils randomNumber],[Utils randomNumber],[Utils randomNumber]];
    return guid;
}

- (NSString *)normalizeChoiceString:(NSString *)string encyrpt:(BOOL)encyrpt {
    
    NSString *choice_string = [self stringValue:string];
    
    if (encyrpt == NO) {
        choice_string = [choice_string stringByReplacingOccurrencesOfString:@"_qw_" withString:@"["];
        choice_string = [choice_string stringByReplacingOccurrencesOfString:@"_qe_" withString:@"]"];
        choice_string = [choice_string stringByReplacingOccurrencesOfString:@"_as_" withString:@"{"];
        choice_string = [choice_string stringByReplacingOccurrencesOfString:@"_ad_" withString:@"}"];
    }

    if (encyrpt == YES) {
        choice_string = [choice_string stringByReplacingOccurrencesOfString:@"[" withString:@"_qw_"];
        choice_string = [choice_string stringByReplacingOccurrencesOfString:@"]" withString:@"_qe_"];
        choice_string = [choice_string stringByReplacingOccurrencesOfString:@"{" withString:@"_as_"];
        choice_string = [choice_string stringByReplacingOccurrencesOfString:@"}" withString:@"_ad_"];
    }
    
//    NSLog(@"choice string : %@", choice_string);
    
//    NSData *data = [choice_string dataUsingEncoding:NSUTF8StringEncoding];
    
    return choice_string;
}

- (void)requestPreviewTest:(NSManagedObject *)object doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *user_id = [self stringValue:[object valueForKey:@"user_id"] ];
    NSString *quiz_id = [self stringValue:[object valueForKey:@"id"] ];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruPreviewTest, user_id, quiz_id] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil ) {
                  
                  NSDictionary *records = dictionary[@"records"];
                  
                  NSManagedObjectContext *ctx = _workerContext;
                  [ctx performBlock:^{
                      
                      NSDictionary *d = records[@"data"];
                      
                      NSString *allow_review = [self stringValue:d[@"allow_review"]];
                      NSString *attempts = [self stringValue:d[@"attempts"]];
                      NSString *date_close = [self stringValue:d[@"date_close"]];
                      NSString *date_open = [self stringValue:d[@"date_open"]];
                      NSString *description = [self stringValue:d[@"description"]];
                      NSString *is_forced_complete = [self stringValue:d[@"is_forced_complete"]];
                      NSString *is_shuffle_answers = [self stringValue:d[@"is_shuffle_answers"]];
                      NSString *learning_skills_id = [self stringValue:d[@"learning_skills_id"]];
                      NSString *learning_skills_name = [self stringValue:d[@"learning_skills_name"]];
                      NSString *name = [self stringValue:d[@"name"]];
                      NSString *passing_rate = [self stringValue:d[@"passing_rate"]];
                      NSString *password = [self stringValue:d[@"password"]];
                      NSString *proficiency_level_id = [self stringValue:d[@"proficiency_level_id"]];
                      
                      if (d[@"difficulty_level_id"] != nil) {
                          proficiency_level_id = [self stringValue:d[@"difficulty_level_id"]];
                      }
                      
                      NSString *proficiency_level_name = [self stringValue:d[@"proficiency_level_name"]];
                      NSString *question_type_name = [self stringValue:d[@"question_type_name"]];
                      NSString *quiz_category_id = [self stringValue:d[@"quiz_category_id"]];
                      NSString *quiz_category_name = [self stringValue:d[@"quiz_category_name"]];
                      NSString *quiz_result_type_id = [self stringValue:d[@"quiz_result_type_id"]];
                      NSString *quiz_result_type_name = [self stringValue:d[@"quiz_result_type_name"]];
                      NSString *quiz_settings_id = [self stringValue:d[@"quiz_settings_id"]];
                      NSString *quiz_shuffling_mode = [self stringValue:d[@"quiz_shuffling_mode"]];
                      NSString *quiz_stage_id = [self stringValue:d[@"quiz_stage_id"]];
                      NSString *quiz_stage_name = [self stringValue:d[@"quiz_stage_name"]];
                      NSString *quiz_type_id = [self stringValue:d[@"quiz_type_id"]];
                      NSString *show_feedbacks = [self stringValue:d[@"show_feedbacks"]];
//                      NSString *time_limit = [self stringValue:d[@"time_limit"]];
                      NSString *total_score = [self stringValue:d[@"total_score"]];
                      NSString *user_id = [self stringValue:d[@"user_id"]];
                      
                      NSString *time_limit = [NSString stringWithFormat:@"%@", d[@"time_limit"] ];
                      if ([time_limit isEqualToString:@"(null)"] || [time_limit isEqualToString:@"<null>"] || [time_limit isEqualToString:@"null"]) {
                          time_limit = @"00:00:00";
                      }
//                      NSString *time_limit_seconds = [NSString stringWithFormat:@"%f", [self parseTimeFromString:time_limit] ];
//                      NSString *total_score = [NSString stringWithFormat:@"%@", quiz_data[@"total_score"] ];
//                      NSString *total_items = [NSString stringWithFormat:@"%lu", (unsigned long)questions.count ];
                      
                      NSString *time_remaining = [NSString stringWithFormat:@"%@", records[@"time_remaining"] ];
                      if ([time_limit isEqualToString:@"(null)"] || [time_limit isEqualToString:@"<null>"] || [time_limit isEqualToString:@"null"]) {
                          time_remaining = [NSString stringWithFormat:@"%f", [self parseTimeFromString:time_limit]];
                      }
                      NSManagedObject *mo = [self getEntity:kQuizEntity attribute:@"id" parameter:quiz_id context:ctx];
                      
                      [mo setValue:quiz_id forKey:@"id"];
                      [mo setValue:allow_review forKey:@"allow_review"];
                      [mo setValue:attempts forKey:@"attempts"];
                      [mo setValue:date_close forKey:@"date_close"];
                      [mo setValue:date_open forKey:@"date_open"];
                      [mo setValue:description forKey:@"desc"];
                      [mo setValue:is_forced_complete forKey:@"is_forced_complete"];
                      [mo setValue:is_shuffle_answers forKey:@"is_shuffle_answers"];
                      [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
                      [mo setValue:learning_skills_name forKey:@"learning_skills_name"];
                      [mo setValue:name forKey:@"name"];
                      [mo setValue:passing_rate forKey:@"passing_rate"];
                      [mo setValue:password forKey:@"password"];
                      [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];
                      [mo setValue:proficiency_level_name forKey:@"proficiency_level_name"];
                      [mo setValue:question_type_name forKey:@"question_type_name"];
                      [mo setValue:quiz_category_id forKey:@"quiz_category_id"];
                      [mo setValue:quiz_category_name forKey:@"quiz_category_name"];
                      [mo setValue:quiz_result_type_id forKey:@"quiz_result_type_id"];
                      [mo setValue:quiz_result_type_name forKey:@"quiz_result_type_name"];
                      [mo setValue:quiz_settings_id forKey:@"quiz_settings_id"];
                      [mo setValue:quiz_shuffling_mode forKey:@"quiz_shuffling_mode"];
                      [mo setValue:quiz_stage_id forKey:@"quiz_stage_id"];
                      [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
                      [mo setValue:quiz_type_id forKey:@"quiz_type_id"];
                      [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
                      [mo setValue:time_limit forKey:@"time_limit"];
                      [mo setValue:total_score forKey:@"total_score"];
                      [mo setValue:user_id forKey:@"user_id"];
                      [mo setValue:time_remaining forKey:@"time_remaining"];
                      
                      [mo setValue:time_remaining forKey:@"time_remaining"];
                      
                      if ([self isArrayObject: records[@"questions"] ]) {
                          //PARSE QUESTIONS
                          NSArray *q = [NSArray arrayWithArray: records[@"questions"] ];
                          
                          // SET QUESTIONS
                          [mo setValue:[self processQuiz:mo questions:q] forKey:@"questions"];
                      }
                      
                      [self saveTreeContext:ctx];
                      
                      if (doneBlock) {
                          doneBlock(YES);
                      }
                          
                      
                      
                  }];//END -> [ctx performBlock:
                  
              }// END -> (dictionary[@"records"] != nil )
              
          }//ERROR
          
      }];//SESSION
    
    [task resume];
}


- (void)requestRemoveTest:(NSManagedObject *)mo doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *test_id = [self stringValue:[mo valueForKey:@"id"] ];
    NSPredicate *predicate = [self predicateForKeyPath:@"id" andValue:test_id];
//    [self clearContentsForEntity:kTestEntity predicate:predicate];
    [self clearDataForEntity:kTestEntity withPredicate:predicate context:self.workerContext];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruTestRemove, test_id] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSLog(@"delete data : %@", dictionary);
              
              if (dictionary[@"records"] != nil ) {
                  
                  NSArray *records = dictionary[@"records"];
                  
                  if (records.count > 0) {
                      
//                      NSLog(@"records : %@", records);
                      
                      NSManagedObjectContext *ctx = self.workerContext;
                      [ctx performBlock:^{
                          [self saveTreeContext:ctx];
                          
                      }];
                      
                      if (doneBlock) {
                          doneBlock(YES);
                      }
                      
                  }
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
              }
              
          }//end
          
      }];
    
    [task resume];
}

- (void)requestUpdateTest:(NSManagedObject *)mo newItem:(BOOL)isNew doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *test_id = [self stringValue: [mo valueForKey:@"id" ] ];
    
    /*
     "name": "Science7",
     "description": " short quiz",
     "user_id": "35",
     "quiz_category_id": "1",
     "quiz_type_id": "7",
     "quiz_result_type_id": "1",
     "learning_skills_id": "1",
     "proficiency_level_id": "1",
     "date_open": "2015-05-27 04:30:50",
     "date_close": "2015-05-27 02:12:47",
     "attempts": "1",
     "quiz_shuffling_mode": 1,
     "is_shuffle_answers": 1,
     "total_score": 0,
     "time_limit": "00:15:00",
     "is_forced_complete": 1,
     "quiz_stage_id": "1",
     "show_feedbacks": 1,
     "allow_review": 0,
     
     */

    NSString *user_id = [self stringValue: [mo valueForKey:@"user_id"] ];
    NSString *name = [self stringValue: [mo valueForKey:@"name"] ];
    NSString *description = [self stringValue: [mo valueForKey:@"desc"] ];
    NSString *quiz_category_id = [self stringValue: [mo valueForKey:@"quiz_category_id"] ];
    NSString *quiz_type_id = [self stringValue: [mo valueForKey:@"quiz_type_id"] ];
    NSString *quiz_result_type_id = [self stringValue: [mo valueForKey:@"quiz_result_type_id"] ];
    NSString *learning_skills_id = [self stringValue: [mo valueForKey:@"learning_skills_id"] ];
    NSString *proficiency_level_id = [self stringValue: [mo valueForKey:@"proficiency_level_id"] ];
    NSString *date_open = [self stringValue: [mo valueForKey:@"date_open"] ];
    NSString *date_close = [self stringValue: [mo valueForKey:@"date_close"] ];
    NSString *attempts = [self stringValue: [mo valueForKey:@"attempts"] ];
    NSString *quiz_shuffling_mode = [self stringValue: [mo valueForKey:@"quiz_shuffling_mode"] ];
    NSString *is_shuffle_answers = [self stringValue: [mo valueForKey:@"is_shuffle_answers"] ];
    NSString *total_score = [self stringValue: [mo valueForKey:@"total_score"] ];
    NSString *time_limit = [self stringValue: [mo valueForKey:@"time_limit"] ];
    NSString *is_forced_complete = [self stringValue: [mo valueForKey:@"is_forced_complete"] ];
    NSString *quiz_stage_id = [self stringValue: [mo valueForKey:@"quiz_stage_id"] ];
    NSString *show_feedbacks = [self stringValue: [mo valueForKey:@"show_feedbacks"] ];
    NSString *allow_review = [self stringValue: [mo valueForKey:@"allow_review"] ];
    NSString *passing_rate = [self stringValue: [mo valueForKey:@"passing_rate"] ];
//    NSString *choices = [self stringValue: [mo valueForKey:@"choices"] ];// BUG FIX
    id password = [self stringValue: [mo valueForKey:@"password"] ];
    if ([(NSString *)password isEqualToString:@"<null>"]) {
        password = [NSNull null];
    }
    
    
    NSArray *questions = [NSArray arrayWithArray: [self buildTestQuestionDataFromObject:mo] ];
    
    NSDictionary *postBody = @{
                               @"name": name,
                               @"description": description,
                               @"user_id": user_id,
                               @"quiz_category_id": quiz_category_id,
                               @"quiz_type_id": quiz_type_id,
                               @"quiz_result_type_id": quiz_result_type_id,
                               @"learning_skills_id": learning_skills_id,
                               @"proficiency_level_id": proficiency_level_id,
                               @"difficulty_level_id": proficiency_level_id,//BUG FIX
//                               @"question_choices": choices,//BUG FIX
                               @"date_open": date_open,
                               @"date_close": date_close,
                               @"attempts": attempts,
                               @"quiz_shuffling_mode": quiz_shuffling_mode,
                               @"is_shuffle_answers": is_shuffle_answers,
                               @"total_score": total_score,
                               @"time_limit": time_limit,
                               @"is_forced_complete": is_forced_complete,
                               @"quiz_stage_id": quiz_stage_id,
                               @"show_feedbacks": show_feedbacks,
                               @"allow_review": allow_review,
                               @"password":password,
                               @"passing_rate": passing_rate,
                               @"questions": questions
                               };
    
    NSLog(@"TEST : %@", postBody);
    NSString *createRequest = [NSString stringWithFormat:kEndPointTestGuruTestCreate, user_id];
    NSString *updateRequest = [NSString stringWithFormat:kEndPointTestGuruTestUpdate, test_id];
    NSString *path = (isNew) ? createRequest : updateRequest;
    NSURL *url = [self buildURL:path];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error~!~! %@", [error localizedDescription]);
              if (doneBlock) {
                  doneBlock(NO);
              }
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSLog(@"data : %@", dictionary);
              
              if (dictionary[@"_meta"] != nil ) {
                  
                  NSDictionary *meta = dictionary[@"_meta"];
                  NSString *status = [self stringValue: meta[@"status"] ];
                  
                  BOOL flag = [status isEqualToString:@"SUCCESS"];
                  if (flag) {
//                      [self requestDetailsForTest:mo doneBlock:doneBlock];
                      if (doneBlock) {
                          doneBlock(flag);
                      }

                  }
//                  if (doneBlock) {
//                      doneBlock(flag);
//                  }
              }
              
          }//end
          
      }];
    
    [task resume];
}

- (NSArray *)buildTestQuestionDataFromObject:(NSManagedObject *)object {
    
    NSMutableArray *questions = [NSMutableArray array];
    NSSet *question_set = [object valueForKey:@"questions"];
    NSArray *questions_list = [NSArray arrayWithArray: [question_set allObjects] ];
    if (questions_list.count > 0) {
        for (NSManagedObject *mo in questions_list) {
            
            /*
             "question_id": "48",
             "question_type_id": "3",
             "question_text": "What is the center of an atom called",
             "points": "1.00"
             */

            
            NSString *question_id = [self stringValue: [mo valueForKey:@"question_id"] ];
            NSString *question_type_id = [self stringValue: [mo valueForKey:@"question_type_id"] ];
            NSString *question_text = [self stringValue: [mo valueForKey:@"question_text"] ];
            NSString *points = [self stringValue: [mo valueForKey:@"points"] ];

            /*
            "question_choices": "_qw__as_'id':'144','question_id':'48','text':'nucleus','suggestive_feedback':'','is_correct':'100','order_number':'1','is_deleted':'0','date_created':'2015-05-26 03:59:57','date_modified':'2015-05-26 03:59:57'_ad_,_as_'id':'145','question_id':'48','text':'athenosphere','suggestive_feedback':'','is_correct':'0','order_number':'2','is_deleted':'0','date_created':'2015-05-26 03:59:57','date_modified':'2015-05-26 03:59:57'_ad_,_as_'id':'146','question_id':'48','text':'atom','suggestive_feedback':'','is_correct':'0','order_number':'3','is_deleted':'0','date_created':'2015-05-26 03:59:57','date_modified':'2015-05-26 03:59:57'_ad__qe_",
             */
            
            NSString *question_choices = [self buildTestQuestionChoiceDataFromObject:mo];
            
            
            NSDictionary *data = @{
                                   @"question_id":question_id,
                                   @"question_type_id":question_type_id,
                                   @"question_text":question_text,
                                   @"points":points,
                                   @"question_choices": question_choices };
            
            [questions addObject:data];
        }
    }
    
    return questions;
}

- (NSString *)buildTestQuestionChoiceDataFromObject:(NSManagedObject *)object {

    NSString *choices_string = @"";
    
    NSMutableArray *list = [NSMutableArray array];
    NSSet *choices_set = [object valueForKey:@"choices"];
    NSArray *choice_list = [NSArray arrayWithArray: [choices_set allObjects] ];
    if (choice_list.count > 0) {
        
        for (NSManagedObject *mo in choice_list) {
            
            NSString *choice_id = [self stringValue: [mo valueForKey:@"id"] ];
            NSString *date_created = [self stringValue: [mo valueForKey:@"date_created"] ];
            NSString *date_modified = [self stringValue: [mo valueForKey:@"date_modified"] ];
            NSString *is_correct = [self stringValue: [mo valueForKey:@"is_correct"] ];
            NSString *is_deleted = [self stringValue: [mo valueForKey:@"is_deleted"] ];
            NSString *order_number = [self stringValue: [mo valueForKey:@"order_number"] ];
            NSString *question_id = [self stringValue: [mo valueForKey:@"question_id"] ];
            NSString *suggestive_feedback = [self stringValue: [mo valueForKey:@"suggestive_feedback"] ];
            if ([suggestive_feedback isEqualToString:@"<null>"]) {
                suggestive_feedback = @"";
            }
            
            NSString *text = [self stringValue: [mo valueForKey:@"text"] ];
            
            NSDictionary *dictionary = @{@"id":choice_id,
                                         @"date_created":date_created,
                                         @"date_modified":date_modified,
                                         @"is_correct":is_correct,
                                         @"is_deleted":is_deleted,
                                         @"order_number":order_number,
                                         @"question_id":question_id,
                                         @"suggestive_feedback":suggestive_feedback,
                                         @"text":text };
            [list addObject:dictionary];
        }
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:list options:0 error:nil];
        NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        choices_string = [self normalizeChoiceString:string encyrpt:YES];
    }
    
    return choices_string;
}

- (void)requestDeployTest:(NSDictionary *)parameter doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSLog(@"%s : %@", __PRETTY_FUNCTION__, parameter);

    NSURL *url = [self buildURL:kEndPointTestGuruTestDeploy];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:parameter];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSLog(@"deployed data : %@", dictionary);
              
              if (dictionary[@"_meta"] != nil ) {
                  
                  NSDictionary *meta = dictionary[@"_meta"];
                  NSString *status = [self stringValue: meta[@"status"] ];
                  
                  BOOL flag = [status isEqualToString:@"SUCCESS"];
                  
                  if (doneBlock) {
                      doneBlock(flag);
                  }
              }
              
          }//end
          
      }];
    [task resume];
}

- (void)saveDeployedTestSectionToCoreData:(NSDictionary *)data {
    NSLog(@"Deployed Test to Section: %@", data);
    NSManagedObjectContext *ctx = self.workerContext;
    
    if (data) {
        NSString *qid = data[@"qid"];
        NSArray *sectionList = data[@"cs_ids"];
        
        if (sectionList.count > 0) {
            // NOTE: Enable if deployed test retraction is allowed
            // NSPredicate *predicate = [self predicateForKeyPath:@"qid" andValue:qid];
            
            [ctx performBlock:^{
                // NOTE: Enable if deployed test retraction is allowed
                // [self clearDataForEntity:kDeployedTestEntity withPredicate:predicate context:ctx];
                
                for (NSDictionary *d in sectionList) {
                    NSManagedObject *mo = [self insertNewRecordForEntity:kDeployedTestEntity];
                    NSString *section_id = [self stringValue:d[@"id"]];
                    [mo setValue:qid forKey:@"qid"];
                    [mo setValue:section_id forKey:@"section_id"];
                }
                
                [self saveTreeContext:ctx];
            }];
        }
    }
}

- (void)requestTestSectionListForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruTestSections, userid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              // FIX HERE VIBALHP
              NSDictionary *meta = dictionary[@"_meta"];
              NSString *metaCount = [self stringValue:meta[@"count"]];
              NSString *metaStatus = meta[@"status"];
              
              NSManagedObjectContext *ctx = self.workerContext;
              
              if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
                  [ctx performBlock:^{
                      [self clearDataForEntity:kTestSectionEntity withPredicate:nil context:ctx];
                  }];
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
                  
                  return;
              }
              
              
//              NSLog(@"data : %@", dictionary);
              
              if (dictionary[@"records"] != nil ) {

                  NSArray *records = [NSArray arrayWithArray: dictionary[@"records"] ];

                  if (records.count > 0) {
//                      NSLog(@"records : %@", records);

                      NSManagedObjectContext *ctx = self.workerContext;
                      [ctx performBlock:^{
                          
                          for (NSDictionary *d in records) {
                              /*
                               "course_id" = 5;
                               "course_name" = "Reading 5";
                               details = "<null>";
                               "grade_level_id" = 6;
                               "grade_level_name" = "Grade 5";
                               id = 6;
                               "section_id" = 5;
                               "section_name" = "St. Martin";
                               */
                              
                              NSLog(@"record : %@", d);
                              
                              NSString *id_object = [self stringValue: d[@"id"] ];
                              NSString *course_id = [self stringValue: d[@"course_id"] ];
                              NSString *course_name = [self stringValue: d[@"course_name"] ];
                              NSString *details = [self stringValue: d[@"details"] ];
                              NSString *grade_level_id = [self stringValue: d[@"grade_level_id"] ];
                              NSString *grade_level_name = [self stringValue: d[@"grade_level_name"] ];
                              NSString *section_id = [self stringValue: d[@"section_id"] ];
                              NSString *section_name = [self stringValue: d[@"section_name"] ];
                              
                              NSManagedObject *mo = [self getEntity:kTestSectionEntity attribute:@"id" parameter:id_object context:ctx];
                              
                              [mo setValue:id_object forKey:@"id"];
                              [mo setValue:course_id forKey:@"course_id"];
                              [mo setValue:course_name forKey:@"course_name"];
                              [mo setValue:details forKey:@"details"];
                              [mo setValue:grade_level_id forKey:@"grade_level_id"];
                              [mo setValue:grade_level_name forKey:@"grade_level_name"];
                              [mo setValue:section_id forKey:@"section_id"];
                              [mo setValue:section_name forKey:@"section_name"];
                          }
                          
                          [self saveTreeContext:ctx];
                          if (doneBlock) {
                              doneBlock(YES);
                          }
                      }];
                  }
              }
          }//end
      }];
    
    [task resume];

    
}

- (NSDictionary *)fetchDefaultValueForEntity:(NSString *)entity {
    
    NSDictionary *dictionary = @{@"id":@"",@"value":@""};
    
    NSManagedObjectContext *context = _workerContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        NSManagedObject *mo = [items firstObject];
        
        NSArray *keys = mo.entity.propertiesByName.allKeys;
        
        NSMutableDictionary *data = [NSMutableDictionary dictionary];
        
        for (NSString *key in keys) {
            NSString *value = [self stringValue: [mo valueForKey:key] ];
            [data setValue:value forKey:key];
        }
        
        dictionary = [NSDictionary dictionaryWithDictionary:data];
    }
    
    return dictionary;
}

- (void)insertQuestions:(NSSet *)list testObject:(NSManagedObject *)object doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSLog(@"entity : %@", object.entity.name);
    
    if ( (list.count > 0) && (object != nil) ) {
        
        // GLOBAL SET
        NSMutableSet *list_set = [NSMutableSet set];
        
        // PROCESS EXISTING QUESTION IN THE TEST
        NSSet *question_set = [object valueForKey:@"questions"];
        NSArray *question_object_list = [NSArray arrayWithArray: [question_set allObjects] ];
        if (question_object_list.count > 0) {
            for (NSManagedObject *q in question_object_list ) {
                
                NSLog(@"PASOK DITO SA PROCESS EXISTING QUESTION IN THE TEST");
                
                NSString *object_id = [self stringValue: [q valueForKey:@"id"] ];
                NSString *quiz_id = [self stringValue: [q valueForKey:@"quiz_id"] ];
                NSString *image_url = [self stringValue: [q valueForKey:@"image_url"] ];
                NSString *points = [self stringValue: [q valueForKey:@"points"] ];
                NSString *question_id = [self stringValue: [q valueForKey:@"question_id"] ];
                NSString *question_text = [self stringValue: [q valueForKey:@"question_text"] ];
                NSString *question_type_id = [self stringValue: [q valueForKey:@"question_type_id"] ];
                NSString *question_type_name = [self stringValue: [q valueForKey:@"question_type_name"] ];
                NSString *question_choices = [self choiceStringFromQuestionObject: q ];//CHECKED
                
                NSDictionary *question_object = @{
                                                  @"id":object_id,
                                                  @"quiz_id":quiz_id,
                                                  @"image_url":image_url,
                                                  @"points":points,
                                                  @"question_id":question_id,
                                                  @"question_text":question_text,
                                                  @"question_type_id":question_type_id,
                                                  @"question_type_name":question_type_name,
                                                  @"question_choices":question_choices
                                                  };
                
                [list_set addObject:question_object];
            }
        }
        
        // PROCESS NEW QUESTION SELECTED FROM THE QUESTION BANK
        NSString *quiz_settings_id = [self stringValue:[object valueForKey:@"quiz_settings_id"]];//CHECKED
        NSArray *question_id_list = [NSArray arrayWithArray:[list allObjects]];
        if (question_id_list.count > 0) {
            for (NSString *question_id in question_id_list) {
                
                NSLog(@"PASOK DITO SA PROCESS NEW QUESTION SELECTED FROM THE QUESTION BANK");
                //GET OBJECT WITH FILTER
                NSPredicate *predicate = [self predicateForKeyPath:@"id" andValue:question_id];
                NSManagedObject *mo = [self getEntity:kQuestionBankEntity predicate:predicate];
                
                /*
                 "quiz_id" = 5;
                 "image_url" = "<null>";
                 points = 10;
                 "question_id" = 1;
                 "question_text" = "A ball released in positive gravity will:";
                 "question_type_id" = 3;
                 "question_type_name" = "Multiple Choice";
                 "question_choices" = null;
                 */
                
                NSString *quiz_id = [NSString stringWithFormat:@"%@", quiz_settings_id];
                NSString *image_url = [self stringValue:[mo valueForKey:@"image_url"]]; //CHECKED
                NSString *points = [self stringValue:[mo valueForKey:@"points"]]; //CHECKED
                NSString *question_id = [self stringValue:[mo valueForKey:@"id"]]; //CHECKED
                NSString *question_text = [self stringValue:[mo valueForKey:@"question_text"]]; //CHECKED
                NSString *question_type_id = [self stringValue:[mo valueForKey:@"question_type_id"]]; //CHECKED
                NSString *question_type_name = [self stringValue:[mo valueForKey:@"questionTypeName"]]; //CHECKED
                NSString *question_choices = [self choiceStringFromQuestionObject: mo ];//CHECKED
                
                NSDictionary *question_object = @{
                                                  @"id":@"0",
                                                  @"quiz_id":quiz_id,
                                                  @"image_url":image_url,
                                                  @"points":points,
                                                  @"question_id":question_id,
                                                  @"question_text":question_text,
                                                  @"question_type_id":question_type_id,
                                                  @"question_type_name":question_type_name,
                                                  @"question_choices":question_choices
                                                  };
                
                [list_set addObject:question_object];
            }
        }
        
        
        NSManagedObjectContext *context = object.managedObjectContext;

        [context performBlock:^{
            NSArray *qs = [list_set allObjects];
            [object setValue:[self processQuiz:object questions:qs] forKey:@"questions"];
            [self saveTreeContext:context];
            if (doneBlock) {
                doneBlock(YES);
            }
        }];
    }
    
    if (doneBlock) {
        doneBlock(NO);
    }
    
}

- (NSString *)choiceStringFromQuestionObject:(NSManagedObject *)object  {
    
    NSString *choices_string = @"";
    
    /*
    [
     {
         'id':'144',
         'question_id':'48',
         'text':'nucleus',
         'suggestive_feedback':'',
         'is_correct':'100',
         'order_number':'1',
         'is_deleted':'0',
         'date_created':'2015-05-26 03:59:57',
         'date_modified':'2015-05-26 03:59:57’
     },
     {
         'id':'145',
         'question_id':'48',
         'text':'athenosphere',
         'suggestive_feedback':'',
         'is_correct':'0',
         'order_number':'2',
         'is_deleted':'0',
         'date_created':'2015-05-26 03:59:57',
         'date_modified':'2015-05-26 03:59:57'
     },
     {
         'id':'146',
         'question_id':'48',
         'text':'atom',
         'suggestive_feedback':'',
         'is_correct':'0',
         'order_number':'3',
         'is_deleted':'0',
         'date_created':'2015-05-26 03:59:57',
         'date_modified':'2015-05-26 03:59:57'
     }
     ]
     */

    if (object != nil) {
        NSSet *choice_sets = [object valueForKey:@"choices"];
        
        NSArray *choices = [choice_sets allObjects];
        
        NSMutableArray *list = [NSMutableArray array];
        for (NSManagedObject *c in choices) {
            
            /*
             id
             text
             question_id
             order_number
             suggestive_feedback
             is_correct
             is_deleted
             date_created
             date_modified
            */
 
            NSString *id_object = [self stringValue: [c valueForKey:@"id"] ];
            NSString *text = [self stringValue: [c valueForKey:@"text"] ];
            NSString *question_id = [self stringValue: [c valueForKey:@"question_id"] ];
            NSString *order_number = [self stringValue: [c valueForKey:@"order_number"] ];
            NSString *suggestive_feedback = [self stringValue: [c valueForKey:@"suggestive_feedback"] ];
            NSString *is_correct = [self stringValue: [c valueForKey:@"is_correct"] ];
            NSString *is_deleted = [self stringValue: [c valueForKey:@"is_deleted"] ];
            NSString *date_created = [self stringValue: [c valueForKey:@"date_created"] ];
            NSString *date_modified = [self stringValue: [c valueForKey:@"date_modified"] ];
            
            NSDictionary *item = @{@"id": id_object,
                                   @"text": text,
                                   @"question_id": question_id,
                                   @"order_number": order_number,
                                   @"suggestive_feedback": suggestive_feedback,
                                   @"is_correct": is_correct,
                                   @"is_deleted": is_deleted,
                                   @"date_created": date_created,
                                   @"date_modified": date_modified };
            [list addObject:item];
        }
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:list options:0 error:nil];
        NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        choices_string = [self normalizeChoiceString:string encyrpt:YES];
    }
    
    return choices_string;
}

- (void)removeQuestions:(NSManagedObject *)mo doneBlock:(TestGuruDoneBlock)doneBlock {

    NSString *question_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"question_id"] ];
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:mo.entity.name];
    NSPredicate *predicate = [self predicateForKeyPath:@"question_id" andValue:question_id];
    [fetchRequest setPredicate:predicate];
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        
        for (NSManagedObject *lmo in items) {
            [context deleteObject:lmo];
        }
        
        [self saveTreeContext:context];
        
        if (doneBlock) {
            doneBlock(YES);
        }
    }
}

- (void)removeQuestions:(NSManagedObject *)mo withAttribute:(NSString *)key doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSString *question_id = [NSString stringWithFormat:@"%@", [mo valueForKey:key] ];
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:mo.entity.name];
    NSPredicate *predicate = [self predicateForKeyPath:key andValue:question_id];
    [fetchRequest setPredicate:predicate];
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        
        for (NSManagedObject *lmo in items) {
            [context deleteObject:lmo];
        }
        
        [self saveTreeContext:context];
        
        if (doneBlock) {
            doneBlock(YES);
        }
    }
}

- (void)updateTestObject:(NSManagedObject *)mo data:(NSDictionary *)dictionary newItem:(BOOL)isNew doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSDictionary *d = [NSDictionary dictionaryWithDictionary:dictionary];
    NSString *user_id = [self stringValue:d[@"user_id"]];
    NSString *allow_review = [self stringValue:d[@"allow_review"]];
    NSString *attempts = [self stringValue:d[@"attempts"]];
    NSString *date_close = [self stringValue:d[@"date_close"]];
    NSString *date_open = [self stringValue:d[@"date_open"]];
    NSString *description = [self stringValue:d[@"description"]];
    NSString *is_forced_complete = [self stringValue:d[@"is_forced_complete"]];
    NSString *is_shuffle_answers = [self stringValue:d[@"is_shuffle_answers"]];
    NSString *learning_skills_id = [self stringValue:d[@"learning_skills_id"]];
    NSString *learning_skills_name = [self stringValue:d[@"learning_skills_name"]];
    NSString *name = [self stringValue:d[@"name"]];
    NSString *passing_rate = [self stringValue:d[@"passing_rate"]];
    NSString *proficiency_level_id = [self stringValue:d[@"proficiency_level_id"]];
    
//    if (d[@"difficulty_level_id"] != nil) {
//        proficiency_level_id = [self stringValue:d[@"difficulty_level_id"]];
//    }
    
    NSString *proficiency_level_name = [self stringValue:d[@"proficiency_level_name"]];
    NSString *question_type_name = [self stringValue:d[@"question_type_name"]];
    NSString *quiz_category_id = [self stringValue:d[@"quiz_category_id"]];
    NSString *quiz_category_name = [self stringValue:d[@"quiz_category_name"]];
    NSString *quiz_result_type_id = [self stringValue:d[@"quiz_result_type_id"]];
    NSString *quiz_result_type_name = [self stringValue:d[@"quiz_result_type_name"]];
    NSString *quiz_shuffling_mode = [self stringValue:d[@"quiz_shuffling_mode"]];
    NSString *quiz_stage_id = [self stringValue:d[@"quiz_stage_id"]];
    NSString *quiz_stage_name = [self stringValue:d[@"quiz_stage_name"]];
    NSString *quiz_type_id = [self stringValue:d[@"quiz_type_id"]];
    NSString *show_feedbacks = [self stringValue:d[@"show_feedbacks"]];
    NSString *time_limit = [self stringValue:d[@"time_limit"]];
    
    // UPDATE CORE DATA
    NSManagedObjectContext *ctx = mo.managedObjectContext;
        
    [mo setValue:user_id forKey:@"user_id"];
    [mo setValue:allow_review forKey:@"allow_review"];
    [mo setValue:attempts forKey:@"attempts"];
    [mo setValue:date_close forKey:@"date_close"];
    [mo setValue:date_open forKey:@"date_open"];
    [mo setValue:description forKey:@"desc"];
    [mo setValue:is_forced_complete forKey:@"is_forced_complete"];
    [mo setValue:is_shuffle_answers forKey:@"is_shuffle_answers"];
    [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
    [mo setValue:learning_skills_name forKey:@"learning_skills_name"];
    [mo setValue:name forKey:@"name"];
    [mo setValue:passing_rate forKey:@"passing_rate"];
    [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];
    [mo setValue:proficiency_level_name forKey:@"proficiency_level_name"];
    [mo setValue:question_type_name forKey:@"question_type_name"];
    [mo setValue:quiz_category_id forKey:@"quiz_category_id"];
    [mo setValue:quiz_category_name forKey:@"quiz_category_name"];
    [mo setValue:quiz_result_type_id forKey:@"quiz_result_type_id"];
    [mo setValue:quiz_result_type_name forKey:@"quiz_result_type_name"];
    [mo setValue:quiz_shuffling_mode forKey:@"quiz_shuffling_mode"];
    [mo setValue:quiz_stage_id forKey:@"quiz_stage_id"];
    [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
    [mo setValue:quiz_type_id forKey:@"quiz_type_id"];
    [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
    [mo setValue:time_limit forKey:@"time_limit"];
    
    
//    NSLog(@"ENTITY NAME!!!!! [%@]", mo.entity.name);
//    
//    
//    NSArray *questions = (NSArray *)[[mo valueForKey:@"questions"] allObjects];
//    for (NSManagedObject *q in questions) {
//     
//        NSString *choice_string = [self choiceStringFromQuestionObject:q];
//        NSLog(@"CHOICE_STRING [%@]", choice_string);
//    }
//    
//    
    if (isNew) {
        NSString *total_score = [self stringValue:d[@"total_score"]];
        [mo setValue:total_score forKey:@"total_score"];
    }
    
    
    [self saveTreeContext:ctx];
    
    //SENT TO WIRE
    [self requestUpdateTest:mo newItem:isNew doneBlock:doneBlock];
}

- (void)requestListCountForUserWithID:(NSString *)userid fromEndPoint:(NSString *)endPoint listBlock:(TestGuruListBlock)listBlock {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSURL *url = [self buildURL:[NSString stringWithFormat:endPoint, userid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            
            if (dictionary == nil) {
                if (listBlock) {
                    listBlock (@[]);
                }
            }
            
            if (dictionary[@"records"] != nil ) {
                if (listBlock) {
                    listBlock (dictionary[@"records"]);
                }
            }
        }
        
    }];
    
    [task resume];
}

//- (void)downloadImageFromQuestionID:(NSManagedObject *)mo dataBlock:(ResourceManagerBinaryBlock)dataBlock {
//    
//    if (mo != nil) {
//        //URL PATH CREATION
//        NSString *image_url = [mo valueForKey:@"image_url"];
//        NSString *imageUrlPath = [Utils buildUrl:[NSString stringWithFormat:@"/%@", image_url]];
//        
//        NSURLSession *s = [NSURLSession sharedSession];
//        NSURL *imageURL = [NSURL URLWithString:imageUrlPath];
//        NSURLSessionDownloadTask *dt = [s downloadTaskWithURL:imageURL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
//            
//            if (location) {
//                
//                NSData *imageData = [NSData dataWithContentsOfURL:location];
//                if (imageData != nil) {
//                    if (dataBlock) {
//                        dataBlock(imageData);
//                    }
//                }
//            }
//        }];
//        [dt resume];
//    }
//    
//}

- (NSData *)jsonFile:(NSString *)filename {

    NSString *jsonFilePath = [[NSBundle mainBundle] pathForResource:filename ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:jsonFilePath];
    return jsonData;
}

#pragma mark - Question Bank v2.0 Implementation

- (void)requestCourseList:(TestGuruDoneBlock)doneBlock {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    NSString *limit = [self fetchObjectForKey:kTG_PAGINATION_LIMIT];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruCourseList, user_id, limit, @"1", @""] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
              if (doneBlock) {
                  doneBlock(NO);
              }
          }
          
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil ) {
                  NSDictionary *records = dictionary[@"records"];
                  
                  BOOL flag = [self isArrayObject:records[@"data"]];
                  if (flag) {
                      NSArray *data = records[@"data"];
                      if (data.count > 0) {
                          NSManagedObjectContext *ctx = _workerContext;
                          
                          [self clearDataForEntity:kCourseListEntity withPredicate:nil context:ctx];
                          
                          [ctx performBlock:^{
                              
                              for (NSDictionary *d in data) {
                                  
                                  NSString *course_id = [self stringValue:d[@"course_id"]];
                                  NSString *course_name = [self stringValue:d[@"course_name"]];
                                  NSString *initial = [self stringValue:d[@"initial"]];
                                  NSString *course_description = [self stringValue:d[@"description"]];
                                  NSString *course_code = [self stringValue:d[@"course_code"]];
                                  NSString *course_banner = [self stringValue:d[@"course_banner"]];
                                  NSString *grade_level_id = [self stringValue:d[@"grade_level_id"]];
                                  NSString *school_level_id = [self stringValue:d[@"school_level_id"]];
                                  NSString *cs_id = [self stringValue:d[@"cs_id"]];
                                  NSString *section_id = [self stringValue:d[@"section_id"]];
                                  NSString *schedule = [self stringValue:d[@"schedule"]];
                                  NSString *venue = [self stringValue:d[@"venue"]];
                                  NSString *section_name = [self stringValue:d[@"section_name"]];
                                  NSString *grade_level = [self stringValue:d[@"grade_level"]];
                                  NSString *school_level = [self stringValue:d[@"school_level"]];
                                  NSString *search_string = [NSString stringWithFormat:@"%@%@%@%@%@", course_name,course_description,course_code,grade_level,school_level];
                                  
                                  NSManagedObject *mo = [self getEntity:kCourseListEntity attribute:@"course_id" parameter:course_id context:ctx];
                                  
                                  [mo setValue:course_id forKey:@"course_id"];
                                  [mo setValue:course_name forKey:@"course_name"];
                                  [mo setValue:initial forKey:@"initial"];
                                  [mo setValue:course_description forKey:@"course_description"];
                                  [mo setValue:course_code forKey:@"course_code"];
                                  [mo setValue:course_banner forKey:@"course_banner"];
                                  [mo setValue:grade_level_id forKey:@"grade_level_id"];
                                  [mo setValue:school_level_id forKey:@"school_level_id"];
                                  [mo setValue:cs_id forKey:@"cs_id"];
                                  [mo setValue:section_id forKey:@"section_id"];
                                  [mo setValue:schedule forKey:@"schedule"];
                                  [mo setValue:venue forKey:@"venue"];
                                  [mo setValue:section_name forKey:@"section_name"];
                                  [mo setValue:grade_level forKey:@"grade_level"];
                                  [mo setValue:school_level forKey:@"school_level"];
                                  [mo setValue:search_string forKey:@"search_string"];

                              }
                              
                              [self saveTreeContext:ctx];
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                          }];
                      }
                  } else {
                      if (doneBlock) {
                          doneBlock(NO);
                      }
                  }
                  
              }
          }//end
      }];
    
    [task resume];
}

- (void)requestDashboardStatistics:(TestGuruDataBlock)dataBlock {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    NSString *course_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_COURSE_ID]];
    NSString *package_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID]];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruDashboardStatistics, course_id, package_id, user_id] ];
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary[@"records"] != nil ) {
                                              if (dataBlock) {
                                                  dataBlock( dictionary[@"records"] );
                                              }
                                          }
                                      }//end
                                  }];
    [task resume];
}


//- (void)requestFilteredQuestionBankWithPage:(NSString *)pageNumber
//                                  filterIDs:(NSArray *)arrayOfIDs withPackageID:(NSString *)packageID dataBlock:(TestGuruDataBlock)dataBlock {
//    
//    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
//    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
//    NSDictionary *postBody = [self createQuestionBankFilterBody:arrayOfIDs withPage:pageNumber];
//    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
//    
//    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruFilterQuestionBank, packageID, user_id] ];
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
//        NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
//      {
//          if (error) {
//              NSLog(@"error %@", [error localizedDescription]);
//              if (dataBlock) {
//                  dataBlock(nil);
//          }
//          }
//
//          if (!error) {
//          NSDictionary *dictionary = [self parseResponseData:responsedata];
//              if (dictionary[@"records"] != nil ) {
//                  NSDictionary *records = dictionary[@"records"];
//              NSArray *data = records[@"data"];
//              if (data.count > 0) {
//                      
//                  NSManagedObjectContext *ctx = _workerContext;
//                      
//                      [ctx performBlock:^{
//                          
//                      if ([pageNumber  isEqual:@"1"]) {
//                          [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
//                      }
//                          
//                      for (NSDictionary *d in data) {
//                                  
//                                  NSLog(@"data : %@", d);
//                                  [self processQuestionDataV2:d sectionName:@"" homeurl:homeurl details:NO managedObjectContext:ctx];
//                              }
//                              
//                              [self saveTreeContext:ctx];
//                              
//                              if (dataBlock) {
//                                  NSDictionary *pagingInfo = [self pagingInformation:records[@"pagination"] total:data.count];
//                                  dataBlock(pagingInfo);
//                              }
//                      }];
//                  }
//              }
//      }
//      }];
//        [task resume];
//}

- (void)requestFilteredQuestionBankWithPage:(NSString *)pageNumber withKeyword:(NSString *)search_key
                                  filterIDs:(NSArray *)arrayOfIDs dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSString *encodedSearchKey = [self urlEncode:search_key];
    NSLog(@"ENCODED SEARCH KEY = %@", encodedSearchKey);
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    NSString *limit = [self fetchObjectForKey:kTG_PAGINATION_LIMIT];
    
    NSString *course_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_COURSE_ID]];
    NSString *package_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID]];
    
    NSDictionary *postBody = [self createQuestionBankFilterBody:arrayOfIDs];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruFilterQuestionBankV2, course_id, package_id, user_id, limit, pageNumber, encodedSearchKey] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
        NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
              if (dataBlock) {
                  dataBlock(nil);
          }
          }

          if (!error) {
          NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary[@"records"] != nil ) {
                  NSDictionary *records = dictionary[@"records"];
              NSArray *data = records[@"data"];
                      
                  NSManagedObjectContext *ctx = _workerContext;
                      if ([pageNumber  isEqual:@"1"]) {
                          [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                      }
                          
              if (data.count > 0) {
                       NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];

                                                  [ctx performBlockAndWait:^{
                      for (NSMutableDictionary *d in data) {
                          NSMutableDictionary *question_object = d[@"question"];
                          NSString *course_id_string = [self stringValue:course_id];
                          [question_object setValue:course_id_string forKey:@"course_id"];
                          
                                  NSLog(@"data : %@", d);
                                                          [self processQuestionDataV3:d sectionName:@"" homeurl:homeurl details:NO managedObjectContext:ctx];
                              }
                              
                              [self saveTreeContext:ctx];
                      }];
                  }
                                                if (dataBlock) {
                                  NSDictionary *pagingInfo = [self pagingInformation:records[@"pagination"] total:data.count];
                                  dataBlock(pagingInfo);
                              }
              }
      }
      }];
        [task resume];
}

- (void)requestFilteredAssignQuestionWithPage:(NSString *)pageNumber withKeyword:(NSString *)search_key
                                    filterIDs:(NSArray *)arrayOfIDs dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSManagedObjectContext *ctx = _workerContext;
    if ([pageNumber  isEqual:@"1"]) {
        [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
    }
    
    NSString *encodedSearchKey = [self urlEncode:search_key];
    NSLog(@"ENCODED SEARCH KEY = %@", encodedSearchKey);
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    NSString *limit = [self fetchObjectForKey:kTG_PAGINATION_LIMIT];
    
    NSString *package_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID]];
    NSString *course_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_COURSE_ID]];
    
    NSDictionary *postBody = [self createQuestionBankFilterBody:arrayOfIDs];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruFilterAssignQuestionV3, course_id, package_id, user_id, limit, pageNumber, encodedSearchKey] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          if (dataBlock) {
                                              dataBlock(nil);
                                          }
                                      }
                                      
                                      if (!error) {
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary[@"records"] != nil ) {
                                              NSDictionary *records = dictionary[@"records"];
                                              NSArray *data = records[@"data"];
                                              
                                              if (data.count > 0) {
                                                  NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                                                  
                                                  [ctx performBlockAndWait:^{
                                                      for (NSMutableDictionary *d in data) {
                                                          NSMutableDictionary *question_object = d[@"question"];
                                                          NSString *course_id_string = [self stringValue:course_id];
                                                          [question_object setValue:course_id_string forKey:@"course_id"];
                                                          
                                                          NSLog(@"data : %@", d);
                                                          [self processQuestionDataV3:d sectionName:@"" homeurl:homeurl details:NO managedObjectContext:ctx];
                                                      }
                                                      
                                                      [self saveTreeContext:ctx];
                                                  }];
                                              }
                                              if (dataBlock) {
                                                  NSDictionary *pagingInfo = [self pagingInformation:records[@"pagination"] total:data.count];
                                                  dataBlock(pagingInfo);
                                              }
                                          } else {
                                              if (dataBlock) {
                                                  dataBlock(nil);
                                              }
                                          }
                                      }
                                  }];
    [task resume];
}

- (void)requestFilteredAssignQuestionWithPage:(NSString *)pageNumber
                                  withKeyword:(NSString *)search_key
                                    filterIDs:(NSArray *)arrayOfIDs
                                         sort:(NSDictionary *)sort
                                    dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSManagedObjectContext *ctx = _workerContext;
    if ([pageNumber  isEqual:@"1"]) {
        [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
    }
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    NSString *limit = [self fetchObjectForKey:kTG_PAGINATION_LIMIT];
    
    NSString *course_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_COURSE_ID]];
    NSString *package_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID]];
    NSString *encodedSearchKey = [self urlEncode:search_key];
    
    NSDictionary *postBody = [self createQuestionBankFilterBody:arrayOfIDs];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruFilterAssignQuestionV3, course_id, package_id, user_id, limit, pageNumber, encodedSearchKey]];
    
    if (sort != nil) {
        NSString *sort_by = [self stringValue:sort[@"sort_by"]];
        NSString *view_by = [self stringValue:sort[@"view_by"]];
        NSString *sort_kw = [NSString stringWithFormat:@"%@,%@", sort_by, view_by];
        
        if ([view_by isEqualToString:@""]) {
            sort_kw = sort_by;
        }
        
        if ([sort_by isEqualToString:@""]) {
            sort_kw = view_by;
        }
        
        if (![sort_kw isEqualToString:@","]) {
            url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruFilterAssignQuestionV4, course_id, package_id, user_id, limit, pageNumber, encodedSearchKey, sort_kw]];
        }
        
    }
    
    NSLog(@"url: %@", url);
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            
            if (dictionary[@"records"] != nil ) {
                NSDictionary *records = dictionary[@"records"];
                NSArray *data = records[@"data"];
                
                if (data.count > 0) {
                    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                    
                    [ctx performBlockAndWait:^{
                        NSInteger index = [self getStartingIndexForSorting];
                        NSLog(@"index: %zd", index);
                        
                        for (NSMutableDictionary *d in data) {
                            NSMutableDictionary *question_object = d[@"question"];
                            
                            NSLog(@"BEFORE: %@", question_object);
                            
                            // DYNAMIC SORTING
                            [question_object setObject:@(index) forKey:@"index"];
                            
                            NSLog(@"AFTER: %@", question_object);
                            
                            NSString *course_id_string = [self stringValue:course_id];
                            [question_object setValue:course_id_string forKey:@"course_id"];
                            [self processQuestionDataV3:d sectionName:@"" homeurl:homeurl details:NO managedObjectContext:ctx];
                            
                            index++;
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                }
                if (dataBlock) {
                    NSDictionary *pagingInfo = [self pagingInformation:records[@"pagination"] total:data.count];
                    dataBlock(pagingInfo);
                }
                
            }
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
}

- (NSDictionary *)createQuestionBankFilterBody:(NSArray *)arrayOfIDs {

    NSMutableArray *learningSkillsFilterIDs = [NSMutableArray array];
    NSMutableArray *difficultyLevelFilterIDs = [NSMutableArray array];
    NSMutableArray *questionTypeFilterIDs = [NSMutableArray array];
    
    NSDictionary *post_body;
    
    for (id object in arrayOfIDs) {
        NSString* section_order = [object valueForKey:@"section_order"];
        NSString* filter_id = [object valueForKey:@"type_id"];
        NSLog(@"section_order [%@]", section_order);
        
        if ([section_order  isEqualToString: @"2"]) {
            [learningSkillsFilterIDs addObject:filter_id];
        } else if ([section_order  isEqualToString: @"1"]) {
            [difficultyLevelFilterIDs addObject:filter_id];
        } else {
            [questionTypeFilterIDs addObject:filter_id];
        }
    }
    
    post_body = @{
                  @"question_types":questionTypeFilterIDs,
                  @"difficulty_levels":difficultyLevelFilterIDs,
                  @"learning_skills":learningSkillsFilterIDs
                  };
    
    NSLog(@"post_body >%@<", post_body);
    return post_body;
}

- (void)processQuestionDataV2:(NSDictionary *)d
                  sectionName:(NSString *)sectionName
                      homeurl:(NSString *)homeurl details:(BOOL)enable managedObjectContext:(NSManagedObjectContext *)ctx {
    
    //QUESTION
    /**"question": {
            //            "id": "154",
            //            "user_id": "2",
            //            "question_parent_id": "0",
            //            "question_type_id": "1",
            //            "package_type_id": "4",
            //            "difficulty_level_id": "1",
            //            "learning_skills_id": "4",
            //            "name": "lll",
            //            "question_text": "lll",
            //            "correct_answer_feedback": "",
            //            "wrong_answer_feedback": "",
            //            "general_feedback": "",
            //            "points": "1.00",
            //            "image_url": null,
            //            "is_public": "0",
            //            "is_deleted": "0",
            //            "date_created": "2016-01-19 07:45:18",
            //            "date_modified": "2016-01-19 07:45:18",
            //            "questionTypeName": "True or False",
            //            "learningSkillsName": "Analysis/Synthesis/Evaluation",
            //            "difficultyLevelName": "Easy",
            "question_images": [
                                {
                                    "index": "1",
                                    "image": ""
                                },
                                {
                                    "index": "2",
                                    "image": ""
                                },
                                {
                                    "index": "3",
                                    "image": ""
                                },
                                {
                                    "index": "4",
                                    "image": ""
                                }
                                ],
            "tags": []
        }**/
    
    NSDictionary *qData = d[@"question"];
    
    NSString *obj_id = [self stringValue:qData[@"id"]];
    NSString *user_id = [self stringValue:qData[@"user_id"]];
    NSString *question_parent_id = [self stringValue:qData[@"question_parent_id"]];
    NSString *question_type_id = [self stringValue:qData[@"question_type_id"]];
    NSString *package_type_id = [self stringValue:qData[@"package_type_id"]];
    
    NSString *proficiency_level_id = [self stringValue:qData[@"proficiency_level_id"]];
    if (qData[@"difficulty_level_id"] != nil) {
        proficiency_level_id = [self stringValue:qData[@"difficulty_level_id"]];
    }
    
    NSString *learning_skills_id = [self stringValue:qData[@"learning_skills_id"]];
    
    NSString *name = [self stringValue:qData[@"name"]];
    NSAttributedString *attributted_name = [[NSAttributedString alloc] initWithData:[name dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
    name = [attributted_name string];
    
    NSString *question_text = [self stringValue:qData[@"question_text"]];
    NSLog(@"HTML DAW ITO : %@", question_text);
    NSAttributedString *attributed_question_text = [[NSAttributedString alloc] initWithData:[question_text dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
    question_text = [attributed_question_text string];
    
    NSString *correct_answer_feedback = [self stringValue:qData[@"correct_answer_feedback"]];
    NSString *wrong_answer_feedback = [self stringValue:qData[@"wrong_answer_feedback"]];
    NSString *general_feedback = [self stringValue:qData[@"general_feedback"]];
    NSString *points = [self stringValue:qData[@"points"]];
    NSString *image_url = [NSString stringWithFormat:@"%@/%@", homeurl, [self stringValue:qData[@"image_url"]] ];
    NSString *is_public = [self stringValue:qData[@"is_public"]];
    NSString *is_deleted = [self stringValue:qData[@"is_deleted"]];
    NSString *date_created = [self stringValue:qData[@"date_created"]];
    NSString *date_modified = [self stringValue:qData[@"date_modified"]];
    NSString *questionTypeName = [self stringValue:qData[@"questionTypeName"]];
    NSString *learningSkillsName = [self stringValue:qData[@"learningSkillsName"]];
    NSString *proficiencyLevelName = [self stringValue:qData[@"proficiencyLevelName"]];
    if (qData[@"difficultyLevelName"] != nil) {
        proficiencyLevelName = [self stringValue:qData[@"difficultyLevelName"]];
    }
    
    NSString *image_one = @"";
    NSString *image_two = @"";
    NSString *image_three = @"";
    NSString *image_four = @"";
    
    NSArray *qImages = qData[@"question_images"];
    
    for (NSDictionary *imageDict in qImages) {
        NSString *image_index = [self stringValue:imageDict[@"index"]];
        NSString *image = [self stringValue:imageDict[@"image"]];
        
        if (image.length > 0) {
        if ([image_index isEqual:@"1"]) {
                image_one = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
        }
        if ([image_index isEqual:@"2"]) {
                image_two = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
        }
        if ([image_index isEqual:@"3"]) {
                image_three = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
        }
        if ([image_index isEqual:@"4"]) {
                image_four = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
            }
        }
    }
    
    NSManagedObject *mo = [self getEntity:kQuestionBankEntity attribute:@"id" parameter:obj_id context:ctx];

    [mo setValue:obj_id forKey:@"id"];
    [mo setValue:name forKey:@"name"];
    [mo setValue:correct_answer_feedback forKey:@"correct_answer_feedback"];
    [mo setValue:date_created forKey:@"date_created"];
    [mo setValue:date_modified forKey:@"date_modified"];
    [mo setValue:general_feedback forKey:@"general_feedback"];
    [mo setValue:image_url forKey:@"image_url"];
    [mo setValue:is_deleted forKey:@"is_deleted"];
    [mo setValue:is_public forKey:@"is_public"];
    [mo setValue:learningSkillsName forKey:@"learningSkillsName"];
    [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
    [mo setValue:points forKey:@"points"];
    [mo setValue:proficiencyLevelName forKey:@"proficiencyLevelName"];
    [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];
    [mo setValue:questionTypeName forKey:@"questionTypeName"];
    [mo setValue:question_parent_id forKey:@"question_parent_id"];
    [mo setValue:question_text forKey:@"question_text"];
    [mo setValue:question_type_id forKey:@"question_type_id"];
    //                              [mo setValue:tags forKey:@"tags"];
    [mo setValue:user_id forKey:@"user_id"];
    [mo setValue:wrong_answer_feedback forKey:@"wrong_answer_feedback"];
    
    [mo setValue:package_type_id forKey:@"package_type_id"];
    
    NSString *search_string = [NSString stringWithFormat:@"%@ %@ %@", name, question_text, questionTypeName];
    [mo setValue:[search_string lowercaseString] forKey:@"search_string"];
    
    [mo setValue:image_one forKey:@"image_one"];
    [mo setValue:image_two forKey:@"image_two"];
    [mo setValue:image_three forKey:@"image_three"];
    [mo setValue:image_four forKey:@"image_four"];
    
    
    // ------ TAG PARSING ------
    NSMutableSet *tagSet = [NSMutableSet set];
    NSArray *tagsArray = qData[@"tags"];
    if (tagsArray.count > 0) {
        for (NSInteger i = 0; i < [tagsArray count]; i++) {
            NSString *tag_id = [NSString stringWithFormat:@"%@", @(i) ];
            NSString *tag_value = [NSString stringWithFormat:@"%@", tagsArray[i] ];
            NSManagedObject *tagMo = [NSEntityDescription insertNewObjectForEntityForName:kTagEntity
                                                                   inManagedObjectContext:mo.managedObjectContext];
            [tagMo setValue:tag_id forKey:@"id"];
            [tagMo setValue:tag_value forKey:@"tag"];
            [tagSet addObject:tagMo];
        }
    }
    
    if ( tagSet.count > 0 ) {
        [mo setValue:tagSet forKey:@"tags"];
    }
    // ------ TAG PARSING ------


    // CHOICES
    
    /**"choices": [
                {
//                    "id": "379",
//                    "question_id": "154",
//                    "text": "True",
//                    "suggestive_feedback": "",
//                    "is_correct": "100",
//                    "order_number": "1",
                    "question_choice_images": [],
//                    "is_deleted": "0",
//                    "date_created": "2016-01-19 07:45:18",
//                    "date_modified": "2016-01-19 07:45:18"
                }
     ],**/
    
    NSArray *cArray = d[@"choices"];
    
    NSMutableSet *choice_sets = [NSMutableSet set];
    if (cArray.count > 0) {
        for (NSDictionary *c in cArray) {
            NSString *choice_id = [self stringValue: c[@"id"] ];
            NSString *question_id = [self stringValue: c[@"question_id"] ];
            NSString *choice_text = [self stringValue: c[@"text"] ];
            NSString *suggestive_feedback = [self stringValue: c[@"suggestive_feedback"] ];
            NSString *is_correct = [self stringValue: c[@"is_correct"] ];
            NSString *order_number = [self stringValue: c[@"order_number"] ];
            NSString *is_deleted = [self stringValue: c[@"is_deleted"] ];
            NSString *date_created = [self stringValue: c[@"date_created"] ];
            NSString *date_modified = [self stringValue: c[@"date_modified"] ];
            NSString *question_choice_image = [self stringValue: c[@"question_choice_image"] ];
            
            NSString *choice_image = @"";
            if (question_choice_image.length > 0) {
                if (![question_choice_image  isEqual: @"null"]) {
                    choice_image = [NSString stringWithFormat:@"%@/%@", homeurl, question_choice_image];
            }
            }
            
            NSNumber *order = [NSNumber numberWithDouble:[order_number doubleValue] ];
            
            NSManagedObjectContext *choice_ctx = _workerContext;
            
            NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:kChoiceEntity inManagedObjectContext:choice_ctx];
            
            [c_mo setValue:date_created forKey:@"date_created"];
            [c_mo setValue:date_modified forKey:@"date_modified"];
            [c_mo setValue:choice_id forKey:@"id"];
            [c_mo setValue:is_correct forKey:@"is_correct"];
            [c_mo setValue:is_deleted forKey:@"is_deleted"];
            [c_mo setValue:order forKey:@"order_number"];
            [c_mo setValue:question_id forKey:@"question_id"];
            [c_mo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
            [c_mo setValue:choice_text forKey:@"text"];
            [c_mo setValue:choice_image forKey:@"choice_image"];
            
            [choice_sets addObject:c_mo];
        }
        
        // assign choices
        [mo setValue:[NSSet setWithSet:choice_sets] forKey:@"choices"];
    }
}
    
- (void)processQuestionDataV3:(NSMutableDictionary *)d
                  sectionName:(NSString *)sectionName
                      homeurl:(NSString *)homeurl details:(BOOL)enable managedObjectContext:(NSManagedObjectContext *)ctx {
    
    //QUESTION
    /**"question": {
     //            "id": "154",
     //            "user_id": "2",
     //            "question_parent_id": "0",
     //            "question_type_id": "1",
     //            "package_type_id": "4",
     //            "difficulty_level_id": "1",
     //            "learning_skills_id": "4",
     //            "name": "lll",
     //            "question_text": "lll",
     //            "correct_answer_feedback": "",
     //            "wrong_answer_feedback": "",
     //            "general_feedback": "",
     //            "points": "1.00",
     //            "image_url": null,
     //            "is_public": "0",
     //            "is_deleted": "0",
     //            "date_created": "2016-01-19 07:45:18",
     //            "date_modified": "2016-01-19 07:45:18",
     //            "questionTypeName": "True or False",
     //            "learningSkillsName": "Analysis/Synthesis/Evaluation",
     //            "difficultyLevelName": "Easy",
     "question_images": [
    {
     "index": "1",
     "image": ""
     },
     {
     "index": "2",
     "image": ""
     },
     {
     "index": "3",
     "image": ""
     },
     {
     "index": "4",
     "image": ""
      }
     ],
     "tags": []
     }**/
      
    NSDictionary *qData = d[@"question"];
              
    NSString *obj_id = [self stringValue:qData[@"id"]];
    NSString *user_id = [self stringValue:qData[@"user_id"]];
    NSString *question_parent_id = [self stringValue:qData[@"question_parent_id"]];
    NSString *question_type_id = [self stringValue:qData[@"question_type_id"]];
    NSString *package_type_id = [self stringValue:qData[@"package_type_id"]];
    NSString *package_type_icon = [NSString stringWithFormat:@"%@/%@", homeurl, [self stringValue:qData[@"package_type_icon"]]];
    
    NSString *course_id = [self stringValue:qData[@"course_id"]];
    NSString *proficiency_level_id = [self stringValue:qData[@"difficulty_level_id"]];
              
    NSString *learning_skills_id = [self stringValue:qData[@"learning_skills_id"]];
          
    NSString *name = [self stringValue:qData[@"name"]];
    NSAttributedString *attributted_name = [[NSAttributedString alloc] initWithData:[name dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
    name = [attributted_name string];
                  
    NSString *question_text = [self stringValue:qData[@"question_text"]];
    NSLog(@"HTML DAW ITO : %@", question_text);
//    NSAttributedString *attributed_question_text = [[NSAttributedString alloc] initWithData:[question_text dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
//    question_text = [attributed_question_text string];
                      
    NSString *competency_code = [self stringValue:qData[@"competency_code" ]];
    NSString *correct_answer_feedback = [self stringValue:qData[@"correct_answer_feedback"]];
    NSString *wrong_answer_feedback = [self stringValue:qData[@"wrong_answer_feedback"]];
    NSString *general_feedback = [self stringValue:qData[@"general_feedback"]];
    NSString *points = [self stringValue:qData[@"points"]];
    
    points = [self formatStringNumber:points];
    
    NSString *image_url = [NSString stringWithFormat:@"%@/%@", homeurl, [self stringValue:qData[@"image_url"]] ];
    NSString *is_public = [self stringValue:qData[@"is_public"]];
    NSString *is_deleted = [self stringValue:qData[@"is_deleted"]];
    NSString *date_created = [self stringValue:qData[@"date_created"]];
    NSString *raw_date_modified = [self stringValue:qData[@"date_modified"]];

    raw_date_modified = [self handleBlankDate:raw_date_modified];    
    NSString* date_modified = [self translateDate:raw_date_modified from:@"UTC" to:@"GMT"];
    NSDate *sort_date = [self parseDateFromString:raw_date_modified];
                              
    NSString *formatted_date_modified = [self stringValue:qData[@"formatted_date_modified"]];
                              
    NSString *questionTypeName = [self stringValue:qData[@"question_type_name"]];
    NSString *learningSkillsName = [self stringValue:qData[@"learning_skills_name"]];
    NSString *proficiencyLevelName = [self stringValue:qData[@"difficulty_level_name"]];
    NSString *question_type_icon = [NSString stringWithFormat:@"%@/%@", homeurl, [self stringValue:qData[@"question_type_icon"]]];
    
    NSNumber *is_selected = [NSNumber numberWithBool:NO]; // TEST CREATE PURPOSE
                              
    NSString *image_one = @"";
    NSString *image_two = @"";
    NSString *image_three = @"";
    NSString *image_four = @"";

    NSArray *qImages = qData[@"question_images"];
                          
    for (NSDictionary *imageDict in qImages) {
        NSString *image_index = [self stringValue:imageDict[@"index"]];
        NSString *image = [self stringValue:imageDict[@"image"]];
                          
        if (image.length > 0) {
            if ([image_index isEqual:@"1"]) {
                image_one = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
                          }
            if ([image_index isEqual:@"2"]) {
                image_two = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
                      }
            if ([image_index isEqual:@"3"]) {
                image_three = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
          }
            if ([image_index isEqual:@"4"]) {
                image_four = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
}
                                          }
                                      }
    
    NSManagedObject *mo = [self getEntity:kQuestionBankEntity attribute:@"id" parameter:obj_id context:ctx];
                                                  
    // DYNAMIC SORTING
    NSString *index_string = [self stringValue:qData[@"index"]];
    NSInteger index = [index_string integerValue];
    [mo setValue:@(index) forKey:@"index"];
    
    [mo setValue:is_selected forKey:@"is_selected"]; // TEST CREATE PURPOSE DEFAULT VALUE
    [mo setValue:obj_id forKey:@"id"];
    [mo setValue:name forKey:@"name"];
    [mo setValue:correct_answer_feedback forKey:@"correct_answer_feedback"];
    [mo setValue:date_created forKey:@"date_created"];
    [mo setValue:date_modified forKey:@"date_modified"];
    [mo setValue:formatted_date_modified forKey:@"formatted_date_modified"];
    
    [mo setValue:sort_date forKey:@"sort_date"];
    
    [mo setValue:general_feedback forKey:@"general_feedback"];
    [mo setValue:image_url forKey:@"image_url"];
    [mo setValue:is_deleted forKey:@"is_deleted"];
    [mo setValue:is_public forKey:@"is_public"];
    [mo setValue:learningSkillsName forKey:@"learningSkillsName"];
    [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
    [mo setValue:points forKey:@"points"];
    [mo setValue:proficiencyLevelName forKey:@"proficiencyLevelName"];
    [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];
    [mo setValue:questionTypeName forKey:@"questionTypeName"];
    [mo setValue:question_type_icon forKey:@"question_type_icon"];
    [mo setValue:question_parent_id forKey:@"question_parent_id"];
    [mo setValue:question_text forKey:@"question_text"];
    [mo setValue:competency_code forKey:@"competency_code"];
    [mo setValue:question_type_id forKey:@"question_type_id"];
    //                              [mo setValue:tags forKey:@"tags"];
    [mo setValue:user_id forKey:@"user_id"];
    [mo setValue:wrong_answer_feedback forKey:@"wrong_answer_feedback"];
                                                      
    [mo setValue:package_type_id forKey:@"package_type_id"];
    
    NSString *search_string = [NSString stringWithFormat:@"%@ %@ %@", name, question_text, questionTypeName];
    [mo setValue:[search_string lowercaseString] forKey:@"search_string"];
                                                          
    [mo setValue:image_one forKey:@"image_one"];
    [mo setValue:image_two forKey:@"image_two"];
    [mo setValue:image_three forKey:@"image_three"];
    [mo setValue:image_four forKey:@"image_four"];
    [mo setValue:course_id forKey:@"course_id"];
    [mo setValue:package_type_icon forKey:@"package_type_icon"];
    
    NSString *is_case_sensitive = [self stringValue:qData[@"is_case_sensitive"]];
    [mo setValue:is_case_sensitive forKey:@"is_case_sensitive"];
    
    // ------ TAG PARSING ------
    NSMutableSet *tagSet = [NSMutableSet set];
    NSArray *tagsArray = qData[@"tags"];
    if (tagsArray.count > 0) {
        for (NSInteger i = 0; i < [tagsArray count]; i++) {
            NSString *tag_id = [NSString stringWithFormat:@"%@", @(i) ];
            NSString *tag_value = [NSString stringWithFormat:@"%@", tagsArray[i] ];
            NSManagedObject *tagMo = [NSEntityDescription insertNewObjectForEntityForName:kTagEntity
                                                                   inManagedObjectContext:mo.managedObjectContext];
            [tagMo setValue:tag_id forKey:@"id"];
            [tagMo setValue:tag_value forKey:@"tag"];
            [tagMo setValue:obj_id forKey:@"question_id"];
            [tagSet addObject:tagMo];
                                          }
                                      }
    
    if ( tagSet.count > 0 ) {
        [mo setValue:tagSet forKey:@"tags"];
}
    // ------ TAG PARSING ------

    
    // CHOICES
    
    /**"choices": [
                                  {
     //                    "id": "379",
     //                    "question_id": "154",
     //                    "text": "True",
     //                    "suggestive_feedback": "",
     //                    "is_correct": "100",
     //                    "order_number": "1",
     "question_choice_images": [],
     //                    "is_deleted": "0",
     //                    "date_created": "2016-01-19 07:45:18",
     //                    "date_modified": "2016-01-19 07:45:18"
    }
     ],**/
    
    NSArray *cArray = d[@"choices"];
                                              
    NSMutableSet *choice_sets = [NSMutableSet set];
    if (cArray.count > 0) {
        for (NSDictionary *c in cArray) {
            NSString *choice_id = [self stringValue: c[@"id"] ];
            NSString *question_id = [self stringValue: c[@"question_id"] ];
            NSString *choice_text = [self stringValue: c[@"text"] ];
            NSString *suggestive_feedback = [self stringValue: c[@"suggestive_feedback"] ];
            NSString *is_correct = [self stringValue: c[@"is_correct"] ];
            NSString *order_number = [self stringValue: c[@"order_number"] ];
            NSString *is_deleted = [self stringValue: c[@"is_deleted"] ];
            NSString *date_created = [self stringValue: c[@"date_created"] ];
            NSString *date_modified = [self stringValue: c[@"date_modified"] ];
            NSString *question_choice_image = [self stringValue: c[@"question_choice_image"] ];
                                              
            if ([is_correct isEqualToString:@"1"]) {
                is_correct = @"100";
            }
            
            NSString *choice_image = @"";
            if (question_choice_image.length > 0) {
                if (![question_choice_image  isEqual: @"null"]) {
                    choice_image = [NSString stringWithFormat:@"%@/%@", homeurl, question_choice_image];
                                          }
                                                      }
                                                      
            NSNumber *order = [NSNumber numberWithDouble:[order_number doubleValue] ];
                                                          
            NSManagedObjectContext *choice_ctx = _workerContext;
                                                          
            NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:kChoiceEntity inManagedObjectContext:choice_ctx];
        
            [c_mo setValue:date_created forKey:@"date_created"];
            [c_mo setValue:date_modified forKey:@"date_modified"];
            [c_mo setValue:choice_id forKey:@"id"];
            [c_mo setValue:is_correct forKey:@"is_correct"];
            [c_mo setValue:is_deleted forKey:@"is_deleted"];
            [c_mo setValue:order forKey:@"order_number"];
            [c_mo setValue:question_id forKey:@"question_id"];
            [c_mo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
            [c_mo setValue:choice_text forKey:@"text"];
            [c_mo setValue:choice_image forKey:@"choice_image"];
                                                      
            [choice_sets addObject:c_mo];
                                                      }
                                                      
        // assign choices
        [mo setValue:[NSSet setWithSet:choice_sets] forKey:@"choices"];
}
                                          }
#pragma mark - Question Bank Group by v2.0 Implementation

- (void)updateGroupByCountWithSectionID:(NSString *)section_id doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSManagedObjectContext *ctx = self.workerContext;
    NSManagedObject *mo = [self getEntity:kQuestionGroupByEntity attribute:@"section_id" parameter:section_id context:ctx];
    
    NSString *itemCountString = [self stringValue:[mo valueForKey:@"item_count"]];
    NSInteger itemCountInt = [itemCountString integerValue];
    NSInteger item_count = itemCountInt - 1;
    
    [ctx performBlock:^{
        [mo setValue:[self stringValue:@(item_count)] forKey:@"item_count"];
        [self saveTreeContext:ctx];
        if (doneBlock) {
            doneBlock(YES);
        }
    }];
    
}

- (void)requestHeadersWithLinkType:(NSString *)type withPage:(NSString *)pageNumber dataBlock:(TestGuruDataBlock)dataBlock {
    NSLog(@"%s", __PRETTY_FUNCTION__);

    /** types : tags,difficulty,learningskills,questiontypes, competencies **/
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    NSString *limit = [self fetchObjectForKey:kTG_PAGINATION_LIMIT];
    
    NSString *course_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_COURSE_ID]];
    NSString *package_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID]];
    
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionGroupByV2, type, course_id, package_id, user_id, limit, pageNumber] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          if (dataBlock) {
                                              dataBlock(nil);
                                          }
                                      }
                                      
                                      if (!error) {
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          NSManagedObjectContext *ctx = self.workerContext;
                                          
                                          if ([pageNumber isEqualToString:@"1"]) {
                                                  [self clearDataForEntity:kQuestionGroupByEntity withPredicate:nil context:ctx];
                                          }
                                          
                                          if (dictionary[@"records"] != nil) {
                                              NSDictionary *records = dictionary[@"records"];
                                              NSArray *data = records[@"data"];
                                                  
                                              if (data.count > 0) {
                                                  [ctx performBlockAndWait:^{
                                                      
                                                      for (NSDictionary *object in data) {
                                                          
                                                          NSString *section_header = [self stringValue:object[@"value"]];
                                                          NSString *section_id = [self stringValue:object[@"id"]];
                              NSString *item_count = [self stringValue:object[@"item_count"] ];
                                                          
                                                          if ([type isEqual:@"tags"]) {
                                                              section_header = [self stringValue:object[@"tag"] ];
                              } else if ([type isEqual:@"competencies"]) {
                                  section_header = [self stringValue:object[@"competency_code"] ];
                                    section_id = [self stringValue:object[@"competency_code"] ];
                                              }
                                              
                                                          NSManagedObject *mo = [self getEntity:kQuestionGroupByEntity attribute:@"section_id" parameter:section_id context:ctx];

                                                          // Save data to core data
                                                          [mo setValue:section_header forKey:@"section_header"];
                                                          [mo setValue:section_id forKey:@"section_id"];
                              [mo setValue:item_count forKey:@"item_count"];
//                              [mo setValue:@([section_id integerValue]) forKey:@"sort_order"];
                                                          [mo setValue:[NSNumber numberWithBool:NO] forKey:@"is_collapsed"];
                                                      }
                                                      
                                                      [self saveTreeContext:ctx];
                                                      
                                  }];
                          }
                          
                          if (dataBlock) {
                              NSDictionary *pagingInfo = [self pagingInformation:records[@"pagination"] total:data.count];
                              dataBlock(pagingInfo);
                          }
              }
          }//end
          
      }];
    [task resume];
}

- (void)requestQuestionForLinkType:(NSString *)type withID:(NSString *)type_id withPage:(NSString *)pageNumber withKeyword:(NSString *)search_key withLimit:(NSString *)limit
                                   dataBlock:(TestGuruDataBlock)dataBlock {
    
    //types : tag,difficultylevel,learningskill,type, competencycode
    
    type_id = [self urlEncode:type_id];
    search_key = [self urlEncode:search_key];
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    NSString *course_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_COURSE_ID]];
    NSString *package_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID]];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruQuestionsForIdV2, course_id, package_id, user_id, type, type_id, limit, pageNumber, search_key] ];
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          if (dataBlock) {
                                              dataBlock(nil);
                                          }
                                      }
                                      
                                      if (!error) {
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary[@"records"] != nil ) {
                                              NSDictionary *records = dictionary[@"records"];
                                              NSArray *data = records[@"data"];
                                              if (data.count > 0) {
                                                  
                                                      NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                                                  NSManagedObjectContext *ctx = _workerContext;
                                                      if ([pageNumber  isEqual:@"1"]) {
                                                          [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                                                      }
                                                  [ctx performBlock:^{
                                                      
                                                      for (NSMutableDictionary *d in data) {
                                                          
                                                          NSLog(@"data : %@", d);
                                                          NSMutableDictionary *quesiton_object = d[@"question"];
                                                          NSString *course_id_string = [NSString stringWithFormat:@"%@", course_id];
                                                          [quesiton_object setValue:course_id_string forKey:@"course_id"];
                                                          
                              [self processQuestionDataV3:d sectionName:@"" homeurl:homeurl details:NO managedObjectContext:ctx];
                                                      }
                                                      
                                                      [self saveTreeContext:ctx];
                                                      
                                                  }];
                                              }
                                              
                                              if (dataBlock) {
                                                   NSDictionary *pagingInfo = [self pagingInformation:records[@"pagination"] total:data.count];
                                                    dataBlock(pagingInfo);
                                              }
                                          }
                                      }
                                  }];
    [task resume];
}
    
- (void)updateGroupByObject:(NSManagedObject *)mo withPrevious:(NSManagedObject *)prevObject {
    NSManagedObject *finalObject;
    NSNumber *is_collapsed;
    
    if (prevObject != nil) {
        is_collapsed = [NSNumber numberWithBool:NO];
        finalObject = prevObject;
    }
    
    if (mo !=nil) {
        is_collapsed = [NSNumber numberWithBool:YES];
        finalObject = mo;
    }
    
    NSManagedObjectContext *ctx = finalObject.managedObjectContext;
    [ctx performBlockAndWait:^{
        [finalObject setValue:is_collapsed forKey:@"is_collapsed"];
        [self saveTreeContext:ctx];
    }];
}


//Helpers for question bank
- (NSDictionary *)pagingInformation:(NSDictionary *)data total:(NSInteger)total_parsed {
    NSDictionary *pagingData = @{
                                 @"current_page":[self stringValue:data[@"current_page"]],
                                 @"total_filtered":[self stringValue:data[@"total_filtered"]],
                                 @"total_items":[self stringValue:data[@"total_items"]],
                                 @"total_pages":[self stringValue:data[@"total_pages"]],
                                 @"total_parsed":[NSString stringWithFormat:@"%ld",(long)total_parsed]
                                 };
    return pagingData;
}

#pragma mark - Test Bank v2.0 Implementation

- (void)requestTestProperty:(NSString *)entityName endPoint:(NSString *)endPoint doneBlock:(TestGuruDoneBlock)doneBlock {
    NSURL *url = [self buildURL:endPoint];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }

        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            NSManagedObjectContext *ctx = self.workerContext;
    
            [ctx performBlock:^{
                [self clearDataForEntity:entityName withPredicate:nil context:ctx];
            }];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"test property records: %@", records);
                
                if (records.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d in records) {
                            // Parse data
                            NSString *property_id = [self stringValue:d[@"id"]];
                            NSString *value = [self stringValue:d[@"value"]];
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:entityName attribute:@"id" parameter:property_id context:ctx];
                            
                            // Save data to core data
                            [mo setValue:property_id forKey:@"id"];
                            [mo setValue:value forKey:@"value"];
                        }
                        
                        [self saveTreeContext:ctx];
                        
                        if (doneBlock) {
                            doneBlock(YES);
                        }
                    }];
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestCreateTest:(NSDictionary *)postBody testid:(NSString *)testid isNew:(BOOL)isNew doneBlock:(TestGuruDoneBlock)doneBlock {
    // Just temporary for api v1.0
    NSString *userid = postBody[@"user_id"];

    NSString *createRequest = [NSString stringWithFormat:kEndPointTestCreate, userid];
    NSString *updateRequest = [NSString stringWithFormat:kEndPointTestUpdate, testid];
    NSString *path = (isNew) ? createRequest : updateRequest;
    
    NSURL *url = [self buildURL:path];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
                
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
                        
            if (doneBlock) {
                doneBlock(NO);
            }
        }

        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"created or updated test: %@", records);
                
                if (doneBlock) {
                    doneBlock(YES);
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }

    }];
    
    [task resume];
}

- (NSManagedObject *)getObjectForEntity:(NSString *)entity withSortDescriptor:(NSString *)descriptor andPredicate:(NSPredicate *)predicate {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"entity : %@", entity);
    NSLog(@"predicate : %@", predicate);
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:descriptor ascending:YES selector:@selector(localizedStandardCompare:)];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    NSError *error = nil;
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items firstObject];
    }
    
    return nil;
}

- (void)requestTestDetail:(NSManagedObject *)testObject doneBlock:(TestGuruDoneBlock)doneBlock {
    NSString *test_id = [self stringValue:[testObject valueForKey:@"id"]];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruTestDetails, test_id]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            NSManagedObjectContext *ctx = self.workerContext;
            
            [ctx performBlock:^{
                [self clearDataForEntity:kTestDetailEntity withPredicate:nil context:ctx];
                [self clearDataForEntity:kTestQuestionEntity withPredicate:nil context:ctx];
                [self clearDataForEntity:kTestQuestionChoiceEntity withPredicate:nil context:ctx];
            }];
            
            if (isOkayToParse) {
                NSDictionary *records = dictionary[@"records"];
                NSArray *data = records[@"data"];
                NSLog(@"test detail records: %@", records);
                NSLog(@"test detail record data: %@", data);
                
                if ([self isArrayObject:data] && data.count > 0) {
                    [ctx performBlock:^{
                        // Get last object from dictionary test data
                        NSDictionary *d = data.lastObject;
                        
                        // Parse data
                        NSString *quiz_settings_id = [self stringValue:d[@"quiz_settings_id"]];
                        NSString *user_id = [self stringValue:d[@"user_id"]];
                        NSString *name = [self stringValue:d[@"name"]];
                        NSString *description = [self stringValue:d[@"description"]];
                        NSString *quiz_category_id = [self stringValue:d[@"quiz_category_id"]];
                        NSString *quiz_category_name = [self stringValue:d[@"quiz_category_name"]];
                        NSString *quiz_type_id = [self stringValue:d[@"quiz_type_id"]];
                        NSString *question_type_name = [self stringValue:d[@"question_type_name"]];
                        NSString *quiz_result_type_id = [self stringValue:d[@"quiz_result_type_id"]];
                        NSString *quiz_result_type_name = [self stringValue:d[@"quiz_result_type_name"]];
                        NSString *learning_skills_id = [self stringValue:d[@"learning_skills_id"]];
                        NSString *learning_skills_name = [self stringValue:d[@"learning_skills_name"]];
                        NSString *difficulty_level_id = [self stringValue:d[@"difficulty_level_id"]];
                        NSString *difficulty_level_name = [self stringValue:d[@"difficulty_level_name"]];
                        NSString *date_open = [self stringValue:d[@"date_open"]];
                        NSString *date_close = [self stringValue:d[@"date_close"]];
                        NSString *attempts = [self stringValue:d[@"attempts"]];
                        NSString *quiz_shuffling_mode = [self stringValue:d[@"quiz_shuffling_mode"]];
                        NSString *is_shuffle_answers = [self stringValue:d[@"is_shuffle_answers"]];
                        NSString *total_score = [self stringValue:d[@"total_score"]];
                        NSString *time_limit = [self stringValue:d[@"time_limit"]];
                        NSString *is_forced_complete = [self stringValue:d[@"is_forced_complete"]];
                        NSString *password = [self stringValue:d[@"password"]];
                        NSString *quiz_stage_id = [self stringValue:d[@"quiz_stage_id"]];
                        NSString *show_feedbacks = [self stringValue:d[@"show_feedbacks"]];
                        NSString *allow_review = [self stringValue:d[@"allow_review"]];
                        NSString *passing_rate = [self stringValue:d[@"passing_rate"]];
                        NSString *quiz_stage_name = [self stringValue:d[@"quiz_stage_name"]];
                        
                        // Create managed object
                        NSManagedObject *mo = [self getEntity:kTestDetailEntity attribute:@"id" parameter:test_id context:ctx];
                        
                        // Save to core data
                        [mo setValue:test_id forKey:@"id"];
                        [mo setValue:quiz_settings_id forKey:@"quiz_settings_id"];
                        [mo setValue:user_id forKey:@"user_id"];
                        [mo setValue:name forKey:@"name"];
                        [mo setValue:description forKey:@"test_description"];
                        [mo setValue:quiz_category_id forKey:@"quiz_category_id"];
                        [mo setValue:quiz_category_name forKey:@"quiz_category_name"];
                        [mo setValue:quiz_type_id forKey:@"quiz_type_id"];
                        [mo setValue:question_type_name forKey:@"question_type_name"];
                        [mo setValue:quiz_result_type_id forKey:@"quiz_result_type_id"];
                        [mo setValue:quiz_result_type_name forKey:@"quiz_result_type_name"];
                        [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
                        [mo setValue:learning_skills_name forKey:@"learning_skills_name"];
                        [mo setValue:difficulty_level_id forKey:@"difficulty_level_id"];
                        [mo setValue:difficulty_level_name forKey:@"difficulty_level_name"];
                        [mo setValue:date_open forKey:@"date_open"];
                        [mo setValue:date_close forKey:@"date_close"];
                        [mo setValue:attempts forKey:@"attempts"];
                        [mo setValue:quiz_shuffling_mode forKey:@"quiz_shuffling_mode"];
                        [mo setValue:is_shuffle_answers forKey:@"is_shuffle_answers"];
                        [mo setValue:total_score forKey:@"total_score"];
                        [mo setValue:time_limit forKey:@"time_limit"];
                        [mo setValue:is_forced_complete forKey:@"is_forced_complete"];
                        [mo setValue:password forKey:@"password"];
                        [mo setValue:quiz_stage_id forKey:@"quiz_stage_id"];
                        [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
                        [mo setValue:allow_review forKey:@"allow_review"];
                        [mo setValue:passing_rate forKey:@"passing_rate"];
                        [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
                        
                        if ([self isArrayObject:records[@"questions"]]) {
                            // Parse questions and save to core data
                            NSArray *questions = [NSArray arrayWithArray:records[@"questions"]];
                            [mo setValue:[self processTestQuestions:mo questions:questions] forKey:@"questions"];
                        }
                        
                        [self saveTreeContext:ctx];
                        
                        if (doneBlock) {
                            doneBlock(YES);
                        }
                    }];
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    [task resume];
}

- (NSSet *)processTestQuestions:(NSManagedObject *)object questions:(NSArray *)questions {
    NSMutableSet *set = [NSMutableSet set];
    
    if (questions.count > 0) {
        NSManagedObjectContext *ctx = object.managedObjectContext;
        
        for (NSDictionary *d in questions) {
            // Parse data
            NSString *obj_id = [self stringValue:d[@"id"]];
            NSString *image_url = [self stringValue:d[@"image_url"]];
            NSString *points = [self stringValue:d[@"points"]];
            NSString *question_id = [self stringValue:d[@"question_id"]];
            NSString *question_text = [self stringValue:d[@"question_text"]];
            NSString *question_type_id = [self stringValue:d[@"question_type_id"]];
            NSString *question_type_name = [self stringValue:d[@"question_type_name"]];
            NSString *quiz_id = [self stringValue:d[@"quiz_id"]];
            
            // Create managed object
            NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kTestQuestionEntity inManagedObjectContext:ctx];
            
            // Save to core data
            [mo setValue:obj_id forKey:@"id"];
            [mo setValue:image_url forKey:@"image_url"];
            [mo setValue:points forKey:@"points"];
            [mo setValue:question_id forKey:@"question_id"];
            [mo setValue:question_text forKey:@"question_text"];
            [mo setValue:question_type_id forKey:@"question_type_id"];
            [mo setValue:question_type_name forKey:@"question_type_name"];
            [mo setValue:quiz_id forKey:@"quiz_id"];
            
            // Parse question choices
            NSString *choice_string = [self stringValue:d[@"question_choices"]];
            
            // Save to core data
            [mo setValue:[self processTestQuestionChoices:mo choices:choice_string] forKey:@"choices"];
            
            // Add question mo to set
            [set addObject:mo];
        }
    }

    return set;
}

- (NSSet *)processTestQuestionChoices:(NSManagedObject *)object choices:(NSString *)choicestring {
    NSMutableSet *set = [NSMutableSet set];
    
    if (choicestring.length > 0) {
        NSString *choice_string = [self normalizeChoiceString:choicestring encyrpt:NO];
        NSData *choice_data = [choice_string dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *choices = [self parseResponseData:choice_data];
        
        if (choices.count > 0) {
            NSManagedObjectContext *ctx = object.managedObjectContext;
            
            for (NSDictionary *d in choices) {
                // Parse data
                NSString *choice_id = [self stringValue:d[@"id"]];
                NSString *date_created = [self stringValue:d[@"date_created"]];
                NSString *date_modified = [self stringValue:d[@"date_modified"]];
                NSString *is_correct = [self stringValue:d[@"is_correct"]];
                NSString *is_deleted = [self stringValue:d[@"is_deleted"]];
                NSString *order_number = [self stringValue:d[@"order_number"]];
                NSString *question_id = [self stringValue:d[@"question_id"]];
                NSString *suggestive_feedback = [self stringValue:d[@"suggestive_feedback"]];
                NSString *text = [self stringValue:d[@"text"]];
                
                // Create managed object
                NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kTestQuestionChoiceEntity inManagedObjectContext:ctx];
                
                // Save to core data
                [mo setValue:choice_id forKey:@"id"];
                [mo setValue:date_created forKey:@"date_created"];
                [mo setValue:date_modified forKey:@"date_modified"];
                [mo setValue:is_correct forKey:@"is_correct"];
                [mo setValue:is_deleted forKey:@"is_deleted"];
                [mo setValue:order_number forKey:@"order_number"];
                [mo setValue:question_id forKey:@"question_id"];
                [mo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
                [mo setValue:text forKey:@"text"];
                
                // Add choice mo to set
                [set addObject:mo];
            }
        }
    }
    
    return set;
}

- (void)requestDeleteTest:(NSString *)testid doneBlock:(TestGuruDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestDelete, testid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
    
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            NSManagedObjectContext *ctx = self.workerContext;
            
            [ctx performBlock:^{
                NSPredicate *predicate = [self predicateForKeyPath:@"id" andValue:testid];
                [self clearDataForEntity:kTestEntity withPredicate:predicate context:self.workerContext];
            }];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"deleted test records : %@", records);
                
                if (records.count > 0) {
                    if (doneBlock) {
                        doneBlock(YES);
                    }
                }
                else {
                    if (doneBlock) {
                        doneBlock(NO);
                    }
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)insertQuestionToTest:(NSArray *)questions testid:(NSString *)testid {
    if (questions.count > 0) {
        for (NSManagedObject *qmo in questions) {
            NSManagedObjectContext *ctx = qmo.managedObjectContext;
            
            // Parse data
            NSString *image_url = [self stringValue:[qmo valueForKey:@"image_url"]];
            NSString *points = [self stringValue:[qmo valueForKey:@"points"]];
            NSString *question_id = [self stringValue:[qmo valueForKey:@"id"]];
            NSString *question_text = [self stringValue:[qmo valueForKey:@"question_text"]];
            NSString *question_type_id = [self stringValue:[qmo valueForKey:@"question_type_id"]];
            NSString *question_type_name = [self stringValue:[qmo valueForKey:@"questionTypeName"]];
            
            // Create managed object
            NSManagedObject *tqmo = [NSEntityDescription insertNewObjectForEntityForName:kTestQuestionEntity inManagedObjectContext:ctx];
            
            // Save to core data
            [tqmo setValue:image_url forKey:@"image_url"];
            [tqmo setValue:points forKey:@"points"];
            [tqmo setValue:question_id forKey:@"question_id"];
            [tqmo setValue:question_text forKey:@"question_text"];
            [tqmo setValue:question_type_id forKey:@"question_type_id"];
            [tqmo setValue:question_type_name forKey:@"question_type_name"];
            [tqmo setValue:testid forKey:@"quiz_id"];
            
            // Parse choices and save to core data
            NSArray *choices = [qmo valueForKey:@"choices"];
            [tqmo setValue:[self insertChoiceToQuestion:choices object:tqmo] forKey:@"choices"];
            
            [self saveTreeContext:ctx];
        }
    }
}

- (NSSet *)insertChoiceToQuestion:(NSArray *)choices object:(NSManagedObject *)object {
    NSManagedObjectContext *ctx = object.managedObjectContext;
    NSMutableSet *set = [NSMutableSet set];
    
    if (choices.count > 0) {
        for (NSManagedObject *mo in choices) {
            NSString *date_created = [self stringValue:[mo valueForKey:@"date_created"]];
            NSString *date_modified = [self stringValue:[mo valueForKey:@"date_modified"]];
            NSString *obj_id = [self stringValue:[mo valueForKey:@"id"]];
            NSString *is_correct = [self stringValue:[mo valueForKey:@"is_correct"]];
            NSString *is_deleted = [self stringValue:[mo valueForKey:@"is_deleted"]];
            NSString *order_number = [self stringValue:[mo valueForKey:@"order_number"]];
            NSString *question_id = [self stringValue:[mo valueForKey:@"question_id"]];
            NSString *suggestive_feedback = [self stringValue:[mo valueForKey:@"suggestive_feedback"]];
            NSString *text = [self stringValue:[mo valueForKey:@"text"]];
            
            // Create managed object
            NSManagedObject *cmo = [NSEntityDescription insertNewObjectForEntityForName:kTestQuestionChoiceEntity inManagedObjectContext:ctx];
            
            // Save to core data
            [cmo setValue:date_created forKey:@"date_created"];
            [cmo setValue:date_modified forKey:@"date_modified"];
            [cmo setValue:obj_id forKey:@"id"];
            [cmo setValue:is_correct forKey:@"is_correct"];
            [cmo setValue:is_deleted forKey:@"is_deleted"];
            [cmo setValue:order_number forKey:@"order_number"];
            [cmo setValue:question_id forKey:@"question_id"];
            [cmo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
            [cmo setValue:text forKey:@"text"];
            
            // Add to set
            [set addObject:cmo];
        }
    }
    
    return set;
}

- (BOOL)removeQuestionFromTest:(NSManagedObject *)questionObject {
    if (questionObject != nil) {
        NSString *entity1 = kTestQuestionEntity;
        NSString *entity2 = kTestQuestionChoiceEntity;
        
        NSString *question_id = [self stringValue:[questionObject valueForKey:@"question_id"]];
        NSPredicate *predicate = [self predicateForKeyPath:@"question_id" andValue:question_id];
        
        BOOL isQuestionDeleted = [self clearDataForEntity:entity1 withPredicate:predicate context:questionObject.managedObjectContext];
        BOOL areChoicesDeleted = [self clearDataForEntity:entity2 withPredicate:predicate context:questionObject.managedObjectContext];
        
        if (isQuestionDeleted || areChoicesDeleted) {
            return YES;
        }
    }

    return NO;
}

- (void)clearTestBankRelatedDataFromCoreData {
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kTestDetailEntity withPredicate:nil context:ctx];
    [self clearDataForEntity:kTestQuestionEntity withPredicate:nil context:ctx];
    [self clearDataForEntity:kTestQuestionChoiceEntity withPredicate:nil context:ctx];
}

- (void)createTestSortOptionDefaultSettingsForUser:(NSString *)user_id {
    NSManagedObjectContext *ctx = self.workerContext;
    
    NSPredicate *predicate = [self predicateForKeyPath:@"user_id" andValue:user_id];
    NSArray *sortOptions = [self getObjectsForEntity:kTestSortOptionEntity predicate:predicate context:ctx];
    
    if (sortOptions.count == 0) {
        NSArray *defaultSettings = @[@{@"sort_type_id":@"1",
                                       @"sort_type_name":@"Graded",
                                       @"is_selected":@"1"},
                                     @{@"sort_type_id":@"2",
                                       @"sort_type_name":@"Non-Graded",
                                       @"is_selected":@"0"}
                       ];
    
        for (NSDictionary *d in defaultSettings) {
            NSString *sort_type_id = [self stringValue:d[@"sort_type_id"]];
            NSString *sort_type_name = [self stringValue:d[@"sort_type_name"]];
            NSString *is_selected = [self stringValue:d[@"is_selected"]];
            
            NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kTestSortOptionEntity inManagedObjectContext:ctx];
            
            [mo setValue:user_id forKey:@"user_id"];
            [mo setValue:sort_type_id forKey:@"sort_type_id"];
            [mo setValue:sort_type_name forKey:@"sort_type_name"];
            [mo setValue:is_selected forKey:@"is_selected"];
            
            [self saveTreeContext:ctx];
        }
    }
}

- (void)updateTestSortOptionSettings:(NSManagedObject *)selectedSortTypeObject {
    NSManagedObjectContext *ctx = selectedSortTypeObject.managedObjectContext;
    
    [ctx performBlockAndWait:^{
        [selectedSortTypeObject setValue:@"1" forKey:@"is_selected"];
        [self saveTreeContext:ctx];
    }];
}

#pragma mark - Test Bank v2.2 Implementation

- (void)requestPaginatedTestListForCourse:(NSString *)course_id andUser:(NSString *)user_id withParametersForPagination:(NSDictionary *)parameters dataBlock:(TestGuruDataBlock)dataBlock {
    
    // Pagination Parameters
    NSString *package_id = [self stringValue:parameters[@"package_id"]];
    NSString *count_limit = [self stringValue:parameters[@"count_limit"]];
    NSString *page_number = [self stringValue:parameters[@"page_number"]];
    NSString *search_keyw = [self stringValue:parameters[@"search_keyw"]];
    NSString *sorted_by = [self stringValue:parameters[@"sorted_by"]];
    NSString *test_filter = [self stringValue:parameters[@"test_filter"]];
    NSString *enconded_search_keyw = [self urlEncode:search_keyw];
    
    NSManagedObjectContext *ctx = self.workerContext;
    if ([page_number isEqualToString:@"1"]) {
            [self clearDataForEntity:kTestEntity withPredicate:nil context:ctx];
    }
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTBPaginatedTestListNoFilter, course_id, package_id, user_id, count_limit, page_number, enconded_search_keyw, sorted_by]];
    
    // Graded or Non-Graded
    if ([test_filter isEqualToString:@"1"] || [test_filter isEqualToString:@"0"]) {
        url = [self buildURL:[NSString stringWithFormat:kEndPointTGTBPaginatedTestListComplete, course_id, package_id, user_id, count_limit, page_number, enconded_search_keyw, sorted_by, test_filter]];
    }
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];

            if (isOkayToParse) {
                NSDictionary *records = dictionary[@"records"];
                NSDictionary *pagination = records[@"pagination"];
                NSArray *data = records[@"data"];
                
                NSLog(@"test detail records: %@", records);
                NSLog(@"test detail records pagination: %@", data);
                NSLog(@"test detail records data: %@", data);
                
                if ([self isArrayObject:data] && data.count > 0) {
                    [ctx performBlock:^{
                        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:NO];
                        NSManagedObject *lastObject = [self getFirstEntity:kTestEntity predicate:nil sortDescriptor:descriptor];
                        NSString *stringIndex = [self stringValue:[lastObject valueForKey:@"index"]];
                        NSInteger indexCounter = [stringIndex isEqualToString:@""] ? 0 : [stringIndex integerValue] + 1;
                        
                        for (NSDictionary *d in data) {
                            // Parse data
                            NSString *test_id = [self stringValue:d[@"id"]];
                            NSString *quiz_settings_id = [self stringValue:d[@"quiz_settings_id"]];
                            NSString *user_id = [self stringValue:d[@"user_id"]];
                            NSString *name = [self stringValue:d[@"name"]];
                            NSString *general_instruction = [self stringValue:d[@"general_instruction"]];
                            NSString *description = [self stringValue:d[@"test_description"]];
                            NSString *quiz_category_id = [self stringValue:d[@"quiz_category_id"]];
                            NSString *quiz_category_name = [self stringValue:d[@"quiz_category_name"]];
                            NSString *difficulty_level_id = [self stringValue:d[@"difficulty_level_id"]];
                            NSString *formatted_date_open = [self stringValue:d[@"formatted_date_open"]];
                            NSString *date_open = [self stringValue:d[@"date_open"]];
                            NSString *formatted_date_close = [self stringValue:d[@"formatted_date_close"]];
                            NSString *date_close = [self stringValue:d[@"date_close"]];
                            NSString *attempts = [self stringValue:d[@"attempts"]];
                            NSString *shuffle_questions = [self stringValue:d[@"shuffle_questions"]];
                            NSString *shuffle_choices = [self stringValue:d[@"shuffle_choices"]];
                            NSString *show_score = [self stringValue:d[@"show_score"]];
                            NSString *show_result = [self stringValue:d[@"show_result"]];
                            NSString *show_correct_answer = [self stringValue:d[@"show_correct_answer"]];
                            NSString *total_score = [self stringValue:d[@"total_score"]];
                            NSString *time_limit = [self stringValue:d[@"time_limit"]];
                            NSString *is_forced_submit = [self stringValue:d[@"is_forced_submit"]];
                            NSString *password = [self stringValue:d[@"password"]];
                            NSString *is_graded = [self stringValue:d[@"is_graded"]];
                            NSString *show_feedbacks = [self stringValue:d[@"show_feedbacks"]];
                            NSString *allow_review = [self stringValue:d[@"allow_review"]];
                            NSString *passing_score = [self stringValue:d[@"passing_score"]];
                            NSString *is_expired = [self stringValue:d[@"is_expired"]];
                            NSString *quiz_stage_name = [self stringValue:d[@"quiz_stage_name"]];
                            NSString *item_count = [self stringValue:d[@"item_count"]];
                            NSString *date_created = [self stringValue:d[@"date_created"]];
                            NSString *formatted_date = [self stringValue:d[@"formatted_date"]];
                            NSString *no_expiry = [self stringValue:d[@"no_expiry"]];
                            NSString *date_modified = [self stringValue:d[@"date_modified"]];
                            
                            NSString *is_approved = [self stringValue:d[@"is_approved"]];
                            NSString *course_period_id = [self stringValue:d[@"course_period_id"]];
                            NSString *quiz_type_id = [self stringValue:d[@"quiz_type_id"]];
                            
                            total_score = [self formatStringNumber:total_score];
                            passing_score = [self formatStringNumber:passing_score];
                            
                            // Temporary
                            // No expiry not yet working
                            if ([date_close isEqualToString:@"0000-00-00 00:00:00"] || [date_close isEqualToString:@""]) {
                                no_expiry = @"1";
                            }
                            
                            date_open = [self autoCorrectDateString:date_open];
                            date_close = [self autoCorrectDateString:date_close];
                            date_created = [self autoCorrectDateString:date_created];
                            
                            NSString *sort_date_created_string = [self translateDate:date_created from:@"UTC" to:@"GMT"];
                            NSDate *sort_date_created = [self parseDateFromString:sort_date_created_string];
                                                        
                            if ([formatted_date_open isEqualToString:@""]) {
                                formatted_date_open = date_open;
                            }
                            
                            if ([formatted_date_close isEqualToString:@""]) {
                                formatted_date_close = date_close;
                            }
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:kTestEntity attribute:@"id" parameter:test_id context:ctx];
                            
                            // Save to core data
                            [mo setValue:test_id forKey:@"id"];
                            [mo setValue:quiz_settings_id forKey:@"quiz_settings_id"];
                            [mo setValue:user_id forKey:@"user_id"];
                            [mo setValue:name forKey:@"name"];
                            [mo setValue:general_instruction forKey:@"general_instruction"];
                            [mo setValue:description forKey:@"test_description"];
                            [mo setValue:quiz_category_id forKey:@"quiz_category_id"];
                            [mo setValue:quiz_category_name forKey:@"quiz_category_name"];
                            [mo setValue:difficulty_level_id forKey:@"difficulty_level_id"];
                            [mo setValue:formatted_date_open forKey:@"formatted_date_open"];
                            [mo setValue:date_open forKey:@"date_open"];
                            [mo setValue:formatted_date_close forKey:@"formatted_date_close"];
                            [mo setValue:date_close forKey:@"date_close"];
                            [mo setValue:attempts forKey:@"attempts"];
                            [mo setValue:shuffle_questions forKey:@"shuffle_questions"];
                            [mo setValue:shuffle_choices forKey:@"shuffle_choices"];
                            [mo setValue:show_score forKey:@"show_score"];
                            [mo setValue:show_result forKey:@"show_result"];
                            [mo setValue:show_correct_answer forKey:@"show_correct_answer"];
                            [mo setValue:total_score forKey:@"total_score"];
                            [mo setValue:time_limit forKey:@"time_limit"];
                            [mo setValue:is_forced_submit forKey:@"is_forced_submit"];
                            [mo setValue:password forKey:@"password"];
                            [mo setValue:is_graded forKey:@"is_graded"];
                            [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
                            [mo setValue:allow_review forKey:@"allow_review"];
                            [mo setValue:passing_score forKey:@"passing_score"];
                            [mo setValue:is_expired forKey:@"is_expired"];
                            [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
                            [mo setValue:item_count forKey:@"item_count"];
                            [mo setValue:date_created forKey:@"date_created"];
                            [mo setValue:formatted_date forKey:@"formatted_date"];
                            [mo setValue:no_expiry forKey:@"no_expiry"];
                            [mo setValue:date_modified forKey:@"date_modified"];
                            [mo setValue:sort_date_created forKey:@"sort_date_created"];
                            [mo setValue:@(indexCounter) forKey:@"index"];
                            
                            [mo setValue:is_approved forKey:@"is_approved"];
                            [mo setValue:course_period_id forKey:@"course_period_id"];
                            [mo setValue:quiz_type_id forKey:@"quiz_type_id"];
                            
                            indexCounter++;
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                }
                
                if (dataBlock) {
                    dataBlock(pagination);
                }
            }
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestTestDetailsForTest:(NSString *)testid doneBlock:(TestGuruDoneBlock)doneBlock {
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kTestQuestionItemEntity withPredicate:nil context:ctx];
    [self clearDataForEntity:kTestQuestionItemChoiceEntity withPredicate:nil context:ctx];
    [self clearDataForEntity:kTestQuestionItemImageEntity withPredicate:nil context:ctx];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTBTestDetails, testid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSDictionary *records = dictionary[@"records"];
                NSLog(@"test detail records: %@", records);
                
                if ([self isDictionaryObject:records] && records.count > 0) {
                    [ctx performBlockAndWait:^{
                        
                        // Parse data
                        NSString *test_id = [self stringValue:records[@"id"]];
                        NSString *quiz_settings_id = [self stringValue:records[@"quiz_settings_id"]];
                        NSString *user_id = [self stringValue:records[@"user_id"]];
                        NSString *name = [self stringValue:records[@"name"]];
                        NSString *general_instruction = [self stringValue:records[@"general_instruction"]];
                        NSString *description = [self stringValue:records[@"test_description"]];
                        NSString *quiz_category_id = [self stringValue:records[@"quiz_category_id"]];
                        NSString *quiz_category_name = [self stringValue:records[@"quiz_category_name"]];
                        NSString *difficulty_level_id = [self stringValue:records[@"difficulty_level_id"]];
                        NSString *formatted_date_open = [self stringValue:records[@"formatted_date_open"]];
                        NSString *date_open = [self stringValue:records[@"date_open"]];
                        NSString *formatted_date_close = [self stringValue:records[@"formatted_date_close"]];
                        NSString *date_close = [self stringValue:records[@"date_close"]];
                        NSString *attempts = [self stringValue:records[@"attempts"]];
                        NSString *shuffle_questions = [self stringValue:records[@"shuffle_questions"]];
                        NSString *shuffle_choices = [self stringValue:records[@"shuffle_choices"]];
                        NSString *show_score = [self stringValue:records[@"show_score"]];
                        NSString *show_result = [self stringValue:records[@"show_result"]];
                        NSString *show_correct_answer = [self stringValue:records[@"show_correct_answer"]];
                        NSString *total_score = [self stringValue:records[@"total_score"]];
                        NSString *time_limit = [self stringValue:records[@"time_limit"]];
                        NSString *is_forced_submit = [self stringValue:records[@"is_forced_submit"]];
                        NSString *password = [self stringValue:records[@"password"]];
                        NSString *is_graded = [self stringValue:records[@"is_graded"]];
                        NSString *show_feedbacks = [self stringValue:records[@"show_feedbacks"]];
                        NSString *allow_review = [self stringValue:records[@"allow_review"]];
                        NSString *passing_score = [self stringValue:records[@"passing_score"]];
                        NSString *is_expired = [self stringValue:records[@"is_expired"]];
                        NSString *quiz_stage_name = [self stringValue:records[@"quiz_stage_name"]];
                        NSString *item_count = [self stringValue:records[@"item_count"]];
                        NSString *date_created = [self stringValue:records[@"date_created"]];
                        NSString *formatted_date = [self stringValue:records[@"formatted_date"]];
                        NSString *no_expiry = [self stringValue:records[@"no_expiry"]];
                        
                        // ACADEMIC TERM AND ASSESSMENT TYPE
                        NSString *is_approved = [self stringValue:records[@"is_approved"]];
                        NSString *academic_term_id = [self stringValue:records[@"course_period_id"]];
                        NSString *assessment_type_id = [self stringValue:records[@"quiz_type_id"]];
                        
                        total_score = [self formatStringNumber:total_score];
                        passing_score = [self formatStringNumber:passing_score];
                        
                        // Temporary
                        // No expiry not yet working
                        if ([date_close isEqualToString:@"0000-00-00 00:00:00"] || [date_close isEqualToString:@""]) {
                            no_expiry = @"1";
                        }
                        
                        date_open = [self autoCorrectDateString:date_open];
                        date_close = [self autoCorrectDateString:date_close];
                        date_created = [self autoCorrectDateString:date_created];
                        
                        NSString *sort_date_created_string = [self translateDate:date_created from:@"UTC" to:@"GMT"];
                        NSDate *sort_date_created = [self parseDateFromString:sort_date_created_string];
                        
                        if ([formatted_date_open isEqualToString:@""]) {
                            formatted_date_open = date_open;
                        }
                        
                        if ([formatted_date_close isEqualToString:@""]) {
                            formatted_date_close = date_close;
                        }
                        
                        // Create managed object
                        NSManagedObject *mo = [self getEntity:kTestInformationEntity attribute:@"id" parameter:test_id context:ctx];
                        
                        // Save to core data
                        [mo setValue:test_id forKey:@"id"];
                        [mo setValue:quiz_settings_id forKey:@"quiz_settings_id"];
                        [mo setValue:user_id forKey:@"user_id"];
                        [mo setValue:name forKey:@"name"];
                        [mo setValue:general_instruction forKey:@"general_instruction"];
                        [mo setValue:description forKey:@"test_description"];
                        [mo setValue:quiz_category_id forKey:@"quiz_category_id"];
                        [mo setValue:quiz_category_name forKey:@"quiz_category_name"];
                        [mo setValue:difficulty_level_id forKey:@"difficulty_level_id"];
                        [mo setValue:formatted_date_open forKey:@"formatted_date_open"];
                        [mo setValue:date_open forKey:@"date_open"];
                        [mo setValue:formatted_date_close forKey:@"formatted_date_close"];
                        [mo setValue:date_close forKey:@"date_close"];
                        [mo setValue:attempts forKey:@"attempts"];
                        [mo setValue:shuffle_questions forKey:@"shuffle_questions"];
                        [mo setValue:shuffle_choices forKey:@"shuffle_choices"];
                        [mo setValue:show_score forKey:@"show_score"];
                        [mo setValue:show_result forKey:@"show_result"];
                        [mo setValue:show_correct_answer forKey:@"show_correct_answer"];
                        [mo setValue:total_score forKey:@"total_score"];
                        [mo setValue:time_limit forKey:@"time_limit"];
                        [mo setValue:is_forced_submit forKey:@"is_forced_submit"];
                        [mo setValue:password forKey:@"password"];
                        [mo setValue:is_graded forKey:@"is_graded"];
                        [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
                        [mo setValue:allow_review forKey:@"allow_review"];
                        [mo setValue:passing_score forKey:@"passing_score"];
                        [mo setValue:is_expired forKey:@"is_expired"];
                        [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
                        [mo setValue:item_count forKey:@"item_count"];
                        [mo setValue:date_created forKey:@"date_created"];
                        [mo setValue:formatted_date forKey:@"formatted_date"];
                        [mo setValue:no_expiry forKey:@"no_expiry"];
                        [mo setValue:sort_date_created forKey:@"sort_date_created"];
                        
                        // ACADEMIC TERM AND ASSESSMENT TYPE
                        [mo setValue:is_approved forKey:@"is_approved"];
                        [mo setValue:academic_term_id forKey:@"academic_term_id"];
                        [mo setValue:assessment_type_id forKey:@"assessment_type_id"];
                        
                        NSPredicate *academic_term_predicate = [self predicateForKeyPath:@"id" andValue:academic_term_id];
                        NSManagedObject *academic_term_mo = [self getFirstEntity:kTestAcademicTermEntity predicate:academic_term_predicate sortDescriptor:nil];
                        NSString *academic_term_name = [self stringValue:[academic_term_mo valueForKey:@"name"]];
                        [mo setValue:academic_term_name forKey:@"academic_term_name"];
                        
                        NSPredicate *assessment_type_predicate = [self predicateForKeyPath:@"component_id" andValue:assessment_type_id];
                        NSManagedObject *assessment_type_mo = [self getFirstEntity:kTestAssessmentTypeEntity predicate:assessment_type_predicate sortDescriptor:nil];
                        NSString *assessment_type_name = [self stringValue:[assessment_type_mo valueForKey:@"component_name"]];
                        [mo setValue:assessment_type_name forKey:@"assessment_type_name"];
                        
                        // Parse questions and save to core data
                        if ([self isArrayObject:records[@"question_items"]]) {
                            NSArray *question_items = [NSArray arrayWithArray:records[@"question_items"]];
                            [mo setValue:[self processTestQuestionItems:mo questions:question_items] forKey:@"question_items"];
                        }
                
                        [self saveTreeContext:ctx];
                    }];
                
                if (doneBlock) {
                    doneBlock(YES);
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    [task resume];
}

- (NSSet *)processTestQuestionItems:(NSManagedObject *)object questions:(NSArray *)questions {
    NSMutableSet *set = [NSMutableSet set];
    NSInteger counter = 0;
    
    if (questions.count > 0) {
        NSManagedObjectContext *ctx = object.managedObjectContext;
        NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer]];
        
        for (NSDictionary *d in questions) {
            // Parse data
            NSString *obj_id = [self stringValue:d[@"id"]];
            NSString *quiz_id = [self stringValue:d[@"quiz_id"]];
            NSString *question_course_id = [self stringValue:d[@"question_course_id"]];
            NSString *question_id = [self stringValue:d[@"question_id"]];
            NSString *question_type_id = [self stringValue:d[@"question_type_id"]];
            NSString *question_type_name = [self stringValue:d[@"question_type_name"]];
            NSString *question_name = [self stringValue:d[@"question_name"]];
            NSString *question_text = [self stringValue:d[@"question_text"]];
            NSString *points = [self stringValue:d[@"points"]];
            NSString *is_deleted = [self stringValue:d[@"is_deleted"]];
            NSString *date_created = [self stringValue:d[@"date_created"]];
            NSString *formatted_date_created = [self stringValue:d[@"formatted_date_created"]];
            NSString *date_modified = [self stringValue:d[@"date_modified"]];
            NSString *formatted_date_modified = [self stringValue:d[@"formatted_date_modified"]];
            NSString *question_difficulty_level_id = [self stringValue:d[@"question_difficulty_level_id"]];
            NSString *question_difficulty_level_name = [self stringValue:d[@"question_difficulty_level_name"]];
            NSString *question_learning_skills_id = [self stringValue:d[@"question_learning_skills_id"]];
            NSString *question_learning_skills_name = [self stringValue:d[@"question_learning_skills_name"]];
            NSString *question_package_type_id = [self stringValue:d[@"question_package_type_id"]];
            NSString *question_package_type_name = [self stringValue:d[@"question_package_type_name"]];
            NSString *question_package_type_icon = [self stringValue:d[@"question_package_type_icon"]];
            
            date_created = [self autoCorrectDateString:date_created];
            date_modified = [self autoCorrectDateString:date_modified];
            
            NSString *sort_date_modified_string = [self translateDate:date_modified from:@"UTC" to:@"GMT"];
            NSDate *sort_date_modified = [self parseDateFromString:sort_date_modified_string];
            
            points = [self formatStringNumber:points];

            NSString *package_icon = @"";
            if (question_package_type_icon.length > 0) {
                package_icon = [NSString stringWithFormat:@"%@/%@", homeurl, question_package_type_icon];
            }
            
            NSString *search_string = [NSString stringWithFormat:@"%@ %@", question_name, question_text];

            // Create managed object
            NSManagedObject *mo = [self getEntity:kTestQuestionItemEntity predicate:[self predicateForKeyPath:@"question_id" andValue:question_id] context:ctx];
            
            if (mo == nil) {
                mo = [NSEntityDescription insertNewObjectForEntityForName:kTestQuestionItemEntity inManagedObjectContext:ctx];
            }
            
            // Save to core data
            [mo setValue:obj_id forKey:@"id"];
            [mo setValue:quiz_id forKey:@"quiz_id"];
            [mo setValue:question_course_id forKey:@"question_course_id"];
            [mo setValue:question_id forKey:@"question_id"];
            [mo setValue:question_type_id forKey:@"question_type_id"];
            [mo setValue:question_type_name forKey:@"question_type_name"];
            [mo setValue:question_name forKey:@"question_name"];
            [mo setValue:question_text forKey:@"question_text"];
            [mo setValue:points forKey:@"points"];
            [mo setValue:is_deleted forKey:@"is_deleted"];
            [mo setValue:date_created forKey:@"date_created"];
            [mo setValue:formatted_date_created forKey:@"formatted_date_created"];
            [mo setValue:date_modified forKey:@"date_modified"];
            [mo setValue:formatted_date_modified forKey:@"formatted_date_modified"];
            [mo setValue:sort_date_modified forKey:@"sort_date"];
            [mo setValue:question_difficulty_level_id forKey:@"question_difficulty_level_id"];
            [mo setValue:question_difficulty_level_name forKey:@"question_difficulty_level_name"];
            [mo setValue:question_learning_skills_id forKey:@"question_learning_skills_id"];
            [mo setValue:question_learning_skills_name forKey:@"question_learning_skills_name"];
            [mo setValue:question_package_type_id forKey:@"question_package_type_id"];
            [mo setValue:question_package_type_name forKey:@"question_package_type_name"];
            [mo setValue:package_icon forKey:@"question_package_type_icon"];
            [mo setValue:search_string forKey:@"search_string"];
            [mo setValue:@(counter) forKey:@"sort_id"];
            
            counter++;
 
            if ([self isArrayObject:d[@"question_choices"]]) {
                // Save to core data
                [mo setValue:[self processTestQuestionItemChoices:mo choices:d[@"question_choices"]] forKey:@"question_choices"];
            }
            
            if ([self isArrayObject:d[@"question_images"]]) {
                // Save to core data
                [mo setValue:[self processTestQuestionItemImages:mo images:d[@"question_images"]] forKey:@"question_images"];
            }
            
            // Add question mo to set
            [set addObject:mo];
        }
    }
    
    return set;
}

- (NSSet *)processTestQuestionItemChoices:(NSManagedObject *)object choices:(NSArray *)choices {
    NSMutableSet *set = [NSMutableSet set];

    if (choices.count > 0) {
        NSManagedObjectContext *ctx = object.managedObjectContext;
        NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer]];
        
        for (NSDictionary *d in choices) {
            // Parse data
            NSString *choice_id = [self stringValue:d[@"id"]];
            NSString *question_id = [self stringValue:d[@"question_id"]];
            NSString *text = [self stringValue:d[@"text"]];
            NSString *suggestive_feedback = [self stringValue:d[@"suggestive_feedback"]];
            NSString *is_correct = [self stringValue:d[@"is_correct"]];
            NSString *order_number = [self stringValue:d[@"order_number"]];
            NSString *is_deleted = [self stringValue:d[@"is_deleted"]];
            NSString *date_created = [self stringValue:d[@"date_created"]];
            NSString *date_modified = [self stringValue:d[@"date_modified"]];
            NSString *question_choice_image = [self stringValue:d[@"question_choice_image"]];
            
            NSString *choice_image = @"";
            if (question_choice_image.length > 0) {
                choice_image = [NSString stringWithFormat:@"%@/%@", homeurl, question_choice_image];
            }
            
            date_created = [self autoCorrectDateString:date_created];
            date_modified = [self autoCorrectDateString:date_modified];
            
            // Create managed object
            NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kTestQuestionItemChoiceEntity inManagedObjectContext:ctx];
            
            // Save to core data
            [mo setValue:choice_id forKey:@"id"];
            [mo setValue:question_id forKey:@"question_id"];
            [mo setValue:text forKey:@"text"];
            [mo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
            [mo setValue:is_correct forKey:@"is_correct"];
            [mo setValue:order_number forKey:@"order_number"];
            [mo setValue:is_deleted forKey:@"is_deleted"];
            [mo setValue:date_created forKey:@"date_created"];
            [mo setValue:date_modified forKey:@"date_modified"];
            [mo setValue:choice_image forKey:@"choice_image"];
            
            // Add choice mo to set
            [set addObject:mo];
        }
    }
    
    return set;
}

- (NSSet *)processTestQuestionItemImages:(NSManagedObject *)object images:(NSArray *)images {
    NSMutableSet *set = [NSMutableSet set];
    
    if (images.count > 0) {
        NSManagedObjectContext *ctx = object.managedObjectContext;
        NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer]];
        for (NSDictionary *d in images) {
            // Parse data
            NSString *index = [self stringValue:d[@"index"]];
            NSString *image = [self stringValue:d[@"image"]];
            
            if (image.length > 0) {
                image = [NSString stringWithFormat:@"%@/%@", homeurl, image];
                
                // Create managed object
                NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kTestQuestionItemImageEntity inManagedObjectContext:ctx];
                
                // Save to core data
                [mo setValue:index forKey:@"index"];
                [mo setValue:image forKey:@"image"];
                
                // Add choice mo to set
                [set addObject:mo];
            }
        }
    }
    
    return set;
}

- (void)requestPaginatedTestGradingTypeListForUser:(NSString *)user_id withParametersForPagination:(NSDictionary *)parameters dataBlock:(TestGuruDataBlock)dataBlock {
    // Pagination Parameters
    NSString *count_limit = parameters[@"count_limit"];
    NSString *page_number = parameters[@"page_number"];
    NSString *search_keyw = parameters[@"search_keyw"];
    NSString *enconded_search_keyw = [self urlEncode:search_keyw];
    
    NSManagedObjectContext *ctx = self.workerContext;
    
    if ([page_number isEqualToString:@"1"]) {
        [self clearDataForEntity:kTestGradingTypeEntity withPredicate:nil context:ctx];
    }
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTBPaginatedTestGradingTypeList, user_id, count_limit, page_number, enconded_search_keyw]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSDictionary *records = dictionary[@"records"];
                NSDictionary *pagination = records[@"pagination"];
                NSArray *data = records[@"data"];
                
                NSLog(@"test detail records: %@", records);
                NSLog(@"test detail records pagination: %@", data);
                NSLog(@"test detail records data: %@", data);
                
                if ([self isArrayObject:data] && data.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d in data) {
                            // Parse data
                            NSString *name = [self stringValue:d[@"name"]];
                            NSString *is_graded = [self stringValue:d[@"is_graded"]];
                            NSString *item_count = [self stringValue:d[@"item_count"]];
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:kTestGradingTypeEntity attribute:@"name" parameter:name context:ctx];
                            
                            // Save to core data
                            [mo setValue:user_id forKey:@"user_id"];
                            [mo setValue:name forKey:@"name"];
                            [mo setValue:is_graded forKey:@"is_graded"];
                            [mo setValue:item_count forKey:@"item_count"];
                        }
                        
                       [self saveTreeContext:ctx];
                    }];
                }
                
                if (dataBlock) {
                    dataBlock(pagination);
                }
            }
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestPaginatedAcademicTermsForParameters:(NSDictionary *)parameters dataBlock:(TestGuruDataBlock)dataBlock {
    NSManagedObjectContext *ctx = self.workerContext;
    
    NSString *current_page = [self stringValue:parameters[@"current_page"]];
    NSString *limit = [self stringValue:parameters[@"limit"]];
    NSString *search_keyword = [self urlEncode:[self stringValue:parameters[@"search_keyword"]]];
    
    if ([current_page isEqualToString:@"1"]) {
        [self clearDataForEntity:kTestAcademicTermEntity withPredicate:nil context:ctx];
    }
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTBPaginatedAcademicTerms, current_page, limit, search_keyword]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSDictionary *records = dictionary[@"records"];
                NSDictionary *pagination = records[@"pagination"];
                NSArray *data = records[@"data"];
                
                NSLog(@"test academic terms records: %@", records);
                
                if ([self isArrayObject:data] && data.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d in data) {
                            NSString *term_id = [self stringValue:d[@"id"]];
                            NSString *name = [self stringValue:d[@"name"]];
                            NSString *date_start = [self stringValue:d[@"date_start"]];
                            NSString *date_end = [self stringValue:d[@"date_end"]];
                            NSString *locked = [self stringValue:d[@"locked"]];
                           
                            NSManagedObject *mo = [self getEntity:kTestAcademicTermEntity attribute:@"id" parameter:term_id context:ctx];
                            
                            [mo setValue:@(term_id.integerValue) forKey:@"id"];
                            [mo setValue:name forKey:@"name"];
                            [mo setValue:date_start forKey:@"date_start"];
                            [mo setValue:date_end forKey:@"date_end"];
                            [mo setValue:locked forKey:@"locked"];
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                }
                
                if (dataBlock) {
                    dataBlock(pagination);
                }
            }
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];    
}

- (void)requestTestResulTypeList:(TestGuruDoneBlock)doneBlock {
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kTestResultTypeEntity withPredicate:nil context:ctx];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTBResultTypeList]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"test result types records: %@", records);
                
                if ([self isArrayObject:records] && records.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d in records) {
                            NSString *type_id = [self stringValue:d[@"id"]];
                            NSString *name = [self stringValue:d[@"name"]];
                            
                            NSManagedObject *mo = [self getEntity:kTestResultTypeEntity attribute:@"id" parameter:type_id context:ctx];
                            
                            [mo setValue:@(type_id.integerValue) forKey:@"id"];
                            [mo setValue:name forKey:@"name"];
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                }
                
                if (doneBlock) {
                    doneBlock(YES);
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestTestAssessmentTypeListForCourse:(NSString *)courseid isForTestGuru:(BOOL)isForTestGuru isForApproval:(BOOL)isForApproval doneBlock:(TestGuruDoneBlock)doneBlock {
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kTestAssessmentTypeEntity withPredicate:nil context:ctx];
    
    NSString *testGuru = isForTestGuru ? @"1" : @"";
    NSString *approval = isForApproval ? @"1" : @"";
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTBAssessmentTypeListForCourse, courseid, testGuru, approval]];
    NSLog(@"assessment type list : %@", url.absoluteString);
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {

        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"test result types records: %@", records);
                
                if ([self isArrayObject:records] && records.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d in records) {
                            NSString *assessment_type_id = [self stringValue:d[@"id"]];
                            NSString *course_id = [self stringValue:d[@"course_id"]];
                            NSString *component_id = [self stringValue:d[@"component_id"]];
                            NSString *component_name = [self stringValue:d[@"component_name"]];
                            NSString *percentage = [self stringValue:d[@"percentage"]];
                            NSString *for_testguru = [self stringValue:d[@"for_testguru"]];
                            
                            NSManagedObject *mo = [self getEntity:kTestAssessmentTypeEntity attribute:@"id" parameter:assessment_type_id context:ctx];
                            
                            [mo setValue:@(assessment_type_id.integerValue) forKey:@"id"];
                            [mo setValue:course_id forKey:@"course_id"];
                            [mo setValue:component_id forKey:@"component_id"];
                            [mo setValue:component_name forKey:@"component_name"];
                            [mo setValue:percentage forKey:@"percentage"];
                            [mo setValue:for_testguru forKey:@"for_testguru"];
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                }
                
                if (doneBlock) {
                    doneBlock(YES);
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestCourseSectionListForUser:(NSString *)user_id test_id:(NSString *)test_id isForDeployment:(BOOL)isForDeployment doneBlock:(TestGuruDoneBlock)doneBlock {
    NSString *cs_id = [self fetchObjectForKey:kTGQB_SELECTED_COURSE_ID];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTBCourseSectionList, user_id, test_id, cs_id]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            NSManagedObjectContext *ctx = self.workerContext;
                [self clearDataForEntity:kCourseSectionListEntity withPredicate:nil context:ctx];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"course section records: %@", records);
                
                if (records.count > 0) {
                    [ctx performBlockAndWait:^{
                        for (NSDictionary *d in records) {
                            // Parse data
                            NSString *course_section_id = [self stringValue:d[@"course_section_id"]];
                            NSString *section_id = [self stringValue:d[@"section_id"]];
                            NSString *section_name = [self stringValue:d[@"section_name"]];
                            NSString *is_deployed = (isForDeployment) ? [self stringValue:d[@"is_deployed"]] : @"0";
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:kCourseSectionListEntity attribute:@"course_section_id" parameter:course_section_id context:ctx];
                            
                            // Save data to core data
                            [mo setValue:user_id forKey:@"user_id"];
                            [mo setValue:course_section_id forKey:@"course_section_id"];
                            [mo setValue:section_id forKey:@"section_id"];
                            [mo setValue:section_name forKey:@"section_name"];
                            [mo setValue:is_deployed forKey:@"is_deployed"];
                        }
                        
                        [self saveTreeContext:ctx];
                        
                        if (doneBlock) {
                            doneBlock(YES);
                        }
                    }];
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestDeployForTest:(NSString *)testid sections:(NSArray *)sections doneBlock:(TestGuruDoneBlock)doneBlock {
    NSDictionary *postBody = @{@"course_section_ids":sections};
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTBDeployTest, testid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }

        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"deployed test records: %@", records);
                
                if (records.count > 0) {
                    if (doneBlock) {
                        doneBlock(YES);
                    }
                }
                else {
                    if (doneBlock) {
                        doneBlock(NO);
                    }
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestDeleteForTest:(NSString *)testid doneBlock:(TestGuruDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTBDeleteTest, testid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            NSManagedObjectContext *ctx = self.workerContext;
            
            [ctx performBlock:^{
                NSPredicate *predicate = [self predicateForKeyPath:@"id" andValue:testid];
                [self clearDataForEntity:kTestEntity withPredicate:predicate context:self.workerContext];
            }];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"deleted test records : %@", records);
                
                if (records.count > 0) {
                    if (doneBlock) {
                        doneBlock(YES);
                    }
                }
                else {
                    if (doneBlock) {
                        doneBlock(NO);
                    }
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestGradingTypes:(NSString *)pageNumber dataBlock:(TestGuruDataBlock)dataBlock {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    /** types : tags,difficulty,learningskills,questiontypes, competencies **/
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
//    NSString *course_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_COURSE_ID]];
//    NSString *package_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID]];
    
    NSString *limit = [self fetchObjectForKey:kTG_PAGINATION_LIMIT];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTBGroupBy, user_id, limit, pageNumber] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          if (dataBlock) {
                                              dataBlock(nil);
                                          }
                                      }
                                      
                                      if (!error) {
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          NSManagedObjectContext *ctx = self.workerContext;
                                          
                                          if ([pageNumber isEqualToString:@"1"]) {
                                              [self clearDataForEntity:kQuestionGroupByEntity withPredicate:nil context:ctx];
                                          }
                                          
                                          if (dictionary[@"records"] != nil) {
                                              NSDictionary *records = dictionary[@"records"];
                                              NSArray *data = records[@"data"];
                                              
                                              if (data.count > 0) {
                                                  [ctx performBlock:^{
                                                      
                                                      for (NSDictionary *object in data) {
                                                          
                                                          NSString *section_header = [self stringValue:object[@"name"]];
                                                          NSString *section_id = [self stringValue:object[@"is_graded"]];
                                                          NSString *item_count = [self stringValue:object[@"item_count"] ];
                                                          
                                                          NSManagedObject *mo = [self getEntity:kQuestionGroupByEntity attribute:@"section_id" parameter:section_id context:ctx];
                                                          
                                                          // Save data to core data
                                                          [mo setValue:section_header forKey:@"section_header"];
                                                          [mo setValue:section_id forKey:@"section_id"];
                                                          [mo setValue:item_count forKey:@"item_count"];
                                                          //                              [mo setValue:@([section_id integerValue]) forKey:@"sort_order"];
                                                          [mo setValue:[NSNumber numberWithBool:NO] forKey:@"is_collapsed"];
                                                      }
                                                      
                                                      [self saveTreeContext:ctx];
                                                      
                                                  }];
                                              }
                                              
                                              if (dataBlock) {
                                                  NSDictionary *pagingInfo = [self pagingInformation:records[@"pagination"] total:data.count];
                                                  dataBlock(pagingInfo);
                                              }
                                          }
                                      }//end
                                      
                                  }];
    [task resume];
}

- (void)insertNewTestWitContentBlock:(TestGuruContent)contentBlock {
 
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:kServerDateFormat];
    
    NSManagedObjectContext *ctx = self.workerContext;
    
    [self clearDataForEntity:kTestInformationEntity withPredicate:nil context:ctx];
    [self clearDataForEntity:kTestQuestionItemEntity withPredicate:nil context:ctx];
    [self clearDataForEntity:kQuestionEntity withPredicate:nil context:ctx];
    //TODO
//    [self clearDataForEntity:kTestQuestionItemChoiceEntity withPredicate:nil context:ctx];
//    [self clearDataForEntity:kTestQuestionItemImageEntity withPredicate:nil context:ctx];
    
    NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kTestInformationEntity inManagedObjectContext:ctx];
    if (mo != nil) {
        
        /*
        "allow_review" = 0;
        attempts = 1;
        "date_close" = "2016-02-01 00:00:00";
        "date_created" = "2016-03-04 05:14:59";
        "date_open" = "2016-02-01 00:00:00";
        description = "";
        "difficulty_level_id" = 0;
        "formatted_date_close" = "February 01 2016";
        "formatted_date_open" = "February 01 2016";
        "general_instruction" = "test 20";
        id = 448;
        "is_expired" = 1;
        "is_forced_submit" = 0;
        "is_graded" = 1;
        "item_count" = 2;
        name = "Test 20 save";
        "passing_score" = 50;
        password = "<null>";
        "quiz_category_id" = 1;
        "quiz_category_name" = Test;
        "quiz_settings_id" = 467;
        "quiz_stage_name" = Graded;
        "show_correct_answer" = 0;
        "show_feedbacks" = 0;
        "show_result" = 0;
        "show_score" = 1;
        "shuffle_choices" = 1;
        "shuffle_questions" = 1;
        "time_limit" = "00:15:00";
        "total_score" = "6.00";
        "user_id" = 2;
         */
        
        //SAMPLE DATA
        NSString *guid_text = [self generateGUID];
        NSDate *dateObject = [NSDate date];
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSCalendarUnit units = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
        NSDateComponents *components = [calendar components:units fromDate:dateObject];
        components.day = components.day + 1;//increment
        NSDate *endDate = [calendar dateFromComponents:components];
        
        
        //START DATE
        NSString *date_now = [df stringFromDate:dateObject];
        NSString *date_now_formatted = [self stringLongStyleForDate:dateObject];
        
        //END DATE
        NSString *date_end = [df stringFromDate:endDate];
        NSString *date_end_formatted = [self stringLongStyleForDate:endDate];
        
        NSDictionary *difficultyData = [self fetchDefaultOptionType:kProficiencyEntity];
        NSString *proficiency_level_id = difficultyData[@"id"];

        //USER DATA
        NSString *test_id = guid_text;//ID
        NSString *user_id = [self loginUser];
        NSString *name = @"";
        NSString *general_instruction = @"";
        NSString *test_description = guid_text;  //TEST_DESCRIPTION
        NSString *password = @"";//@"password";//default
        
        NSString *allow_review = @"0";
        NSString *attempts = @"1";
        NSString *is_expired = @"1";
        NSString *is_forced_submit = @"0";
        NSString *is_graded = @"1";
        NSString *item_count = @"0";
        NSString *passing_score = @"0";

        NSString *show_correct_answer = @"0";//default
        NSString *show_feedbacks = @"0";//default
        NSString *show_result = @"0";//default
        NSString *show_score = @"1";//default
        NSString *shuffle_choices = @"1";//default
        NSString *shuffle_questions = @"1";//default
        
        //TODO: WHY IS THIS HARD CODED IN THE BACKEND
        NSString *time_limit = @"00:15:00";//default
        NSString *quiz_category_id = @"1";
        NSString *quiz_category_name = @"Test";
//        NSString *quiz_settings_id = @"467";
        NSString *quiz_stage_name = @"Graded";
        NSString *total_score = @"0";
        NSString *difficulty_level_id = proficiency_level_id;
        
        //DATE PROPERTIES
        NSString *date_created = date_now;
        NSString *date_open = date_now;
        NSString *date_close = date_end;
        NSString *formatted_date_open = date_now_formatted;
        NSString *formatted_date_close = date_end_formatted;
        
        //ACADEMIC TERM (COURSE PERIOD)
        NSSortDescriptor *academic_term_descriptor = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES];
        NSManagedObject *academic_term_mo = [self getFirstEntity:kTestAcademicTermEntity predicate:nil sortDescriptor:academic_term_descriptor];
        NSString *academic_term_id = [self stringValue:[academic_term_mo valueForKey:@"id"]];
        NSString *academic_term_name = [self stringValue:[academic_term_mo valueForKey:@"name"]];
        
        //ASSESSMENT TYPE
        NSSortDescriptor *assessment_type_descriptor = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES];
        NSManagedObject *assessment_type_mo = [self getFirstEntity:kTestAssessmentTypeEntity predicate:nil sortDescriptor:assessment_type_descriptor];
        NSString *assessment_type_id = [self stringValue:[assessment_type_mo valueForKey:@"component_id"]];
        NSString *assessment_type_name = [self stringValue:[assessment_type_mo valueForKey:@"component_name"]];
        
        //CORE DATA VALUES
        [mo setValue:test_id forKey:@"id"];
        [mo setValue:user_id forKey:@"user_id"];
        [mo setValue:name forKey:@"name"];
        [mo setValue:general_instruction forKey:@"general_instruction"];
        [mo setValue:test_description forKey:@"test_description"];
        [mo setValue:password forKey:@"password"];
        
        [mo setValue:allow_review forKey:@"allow_review"];
        [mo setValue:attempts forKey:@"attempts"];
        [mo setValue:is_expired forKey:@"is_expired"];
        [mo setValue:is_forced_submit forKey:@"is_forced_submit"];
        [mo setValue:is_graded forKey:@"is_graded"];
        [mo setValue:item_count forKey:@"item_count"];
        [mo setValue:passing_score forKey:@"passing_score"];
        
        [mo setValue:show_correct_answer forKey:@"show_correct_answer"];
        [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
        [mo setValue:show_result forKey:@"show_result"];
        [mo setValue:show_score forKey:@"show_score"];
        [mo setValue:shuffle_choices forKey:@"shuffle_choices"];
        [mo setValue:shuffle_questions forKey:@"shuffle_questions"];
        
        [mo setValue:time_limit forKey:@"time_limit"];
        [mo setValue:quiz_category_id forKey:@"quiz_category_id"];
        [mo setValue:quiz_category_name forKey:@"quiz_category_name"];

        [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
        [mo setValue:total_score forKey:@"total_score"];
        [mo setValue:difficulty_level_id forKey:@"difficulty_level_id"];
        
        [mo setValue:date_close forKey:@"date_close"];
        [mo setValue:date_created forKey:@"date_created"];
        [mo setValue:date_open forKey:@"date_open"];
        [mo setValue:formatted_date_close forKey:@"formatted_date_close"];
        [mo setValue:formatted_date_open forKey:@"formatted_date_open"];
        
        [mo setValue:academic_term_id forKey:@"academic_term_id"];
        [mo setValue:academic_term_name forKey:@"academic_term_name"];
        [mo setValue:assessment_type_id forKey:@"assessment_type_id"];
        [mo setValue:assessment_type_name forKey:@"assessment_type_name"];
        
        [self saveTreeContext:ctx];
        
        if (contentBlock) {
            contentBlock([NSString stringWithFormat:@"%@",guid_text]);
        }
    }
}

- (void)updateObject:(NSManagedObject *)mo withDetails:(NSDictionary *)dictionary {
    
    NSManagedObjectContext *ctx = mo.managedObjectContext;
    if (mo != nil) {
        
//        [ctx performBlock:^{
            for (NSString *key in [dictionary allKeys]) {
                id value = dictionary[key];
                [mo setValue:value forKey:key];
            }
            
            [self saveTreeContext:ctx];
//        }];
    }
}


- (void)postTestWithUserData:(NSDictionary *)dictionary edit:(BOOL)mode doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSLog(@"USER DATA : %@", dictionary);
    
    // USER ID
    NSString *userid = [self loginUser];
    
    // TEST ID
    NSString *testid = [NSString stringWithFormat:@"%@", dictionary[@"id"] ];
    
    //FETCH REQUEST
    NSPredicate *predicate = [self predicateForKeyPath:@"id" andValue:testid];
    NSManagedObject *mo = [self getEntity:kTestInformationEntity predicate:predicate];
    
    // BUILD REQUEST
    NSString *createRequest = [NSString stringWithFormat:kEndPointTGTBCreateTest, userid];
    NSString *updateRequest = [NSString stringWithFormat:kEndPointTGTBUpdateTest, userid, testid];
    
    NSString *path = (mode == NO) ? createRequest : updateRequest;
    NSURL *url = [self buildURL:path];
    
    // GENERATE POST BODY
    NSDictionary *postBody = [self generateTestData:mo editMode:mode];
    
    if (postBody != nil) {
        
        NSLog(@"JSON BODY : %@", postBody);
        
        NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
        NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                     completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
        {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            if (dictionary) {
                NSLog(@"dictionary : %@", dictionary);
                if (doneBlock) {
                    doneBlock(YES);
                }
            }
        }];
        
        [task resume];
    }
}

- (void)updateTest:(NSString *)entity predicate:(NSPredicate *)predicate details:(NSDictionary *)details {
    
    NSManagedObject *mo = [self getEntity:entity predicate:predicate];
    NSManagedObjectContext *ctx = mo.managedObjectContext;
    if (mo != nil) {
        NSArray *keys = [details allKeys];
        for (NSString *k in keys) {
            [mo setValue:details[k] forKey:k];
        }
        
        [self saveTreeContext:ctx];
    }
}

- (void)assignTestListWithDoneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSNumber *selected_state = [NSNumber numberWithBool:YES];
    NSArray *items = [self getObjectsForEntity:kQuestionBankEntity
                                     predicate:[self predicateForKeyPath:@"is_selected" object:selected_state]];
    
    NSLog(@"ITEM COUNT : %@", items);
    
    if ([items count] > 0) {
        
        for (NSManagedObject *object in items) {
            
            NSManagedObjectContext *ctx = _workerContext;
            
            NSString *entity_type = kQuestionEntity;
            NSString *question_id_string = [object valueForKey:@"id"];
            NSLog(@"QUESTION ID : %@", question_id_string);
            
            NSManagedObject *mo = [self getEntity:entity_type
                                        predicate:[self predicateForKeyPath:@"id" object:question_id_string]
                                          context:ctx];
            if (mo == nil) {
                mo = [NSEntityDescription insertNewObjectForEntityForName:entity_type
                                                   inManagedObjectContext:ctx];
            }
            
            NSArray *keys = object.entity.propertiesByName.allKeys;
                
            for (NSString *key in keys) {
                
                if ([object valueForKey:key] != nil) {
                    
                    id value = [object valueForKey:key];
                    
                    //RESET IS_SELECTED AT QUESTION ENTITY
                    if ([key isEqualToString:@"is_selected"]) {
                        value = [NSNumber numberWithBool:NO];
                    }
                    
                    //////////////////////////////////////////////
                    // ---------- STRING PROCESSING  ----------///
                    //////////////////////////////////////////////
                    if (![key isEqualToString:@"choices"]) {
                        [mo setValue:value forKey:key];
                    }
                    
                    //////////////////////////////////////////////
                    // ---------- CHOICE PROCESSING  ----------///
                    //////////////////////////////////////////////
                    if ([key isEqualToString:@"choices"]) {
                        NSLog(@"VALUE TYPE [%@]", [value class]);
                        
                        NSSet *choices = (NSSet *)[object valueForKey:key];
                        NSArray *choices_array = [choices allObjects];
                        
                        // CHOICE ITEM SET CONTAINER
                        NSMutableSet *mutable_set = [NSMutableSet set];
                        
                        // LOOP THRU CHOICE ENTITY VALUES
                        for (NSManagedObject *c_mo in choices_array) {
                            // CHOICE KEY LOOKUPS
                            NSArray *c_keys = [c_mo.entity.propertiesByName allKeys];
                            
                            // CHOICE ITEM CREATION
                            NSManagedObject *choice_item = [NSEntityDescription insertNewObjectForEntityForName:kChoiceItemEntity
                                                                                         inManagedObjectContext:c_mo.managedObjectContext];
                            // VALUE COPYING CHOICE -> CHOICE ITEM
                            for (NSString *j_key in c_keys) {
                                if (![j_key isEqualToString:@"question"]) {
                                    id j_value = [c_mo valueForKey:j_key];
                                    [choice_item setValue:j_value forKey:j_key];
                                }
                            }
                            // ADD THE NEW CHOICE ITEM MO
                            [mutable_set addObject:choice_item];
                        }
                        
                        [mo setValue:[NSSet setWithSet:mutable_set] forKey:@"choices"];
                    }
                    
                } // ----> END
                
            }// ----> LOOP END
            
            [self saveTreeContext:ctx];
        }
        
        
        
        if (doneBlock) {
            doneBlock(YES);
        }
    }
}

- (void)assignTestListWithSet:(NSMutableSet *)setOfDicts doneBlock:(TestGuruDoneBlock)doneBlock {
    
//    NSNumber *selected_state = [NSNumber numberWithBool:YES];
//    NSArray *items = [self getObjectsForEntity:kQuestionBankEntity
//                                     predicate:[self predicateForKeyPath:@"is_selected" object:selected_state]];
//    
//    NSLog(@"ITEM COUNT : %@", items);
    
    NSManagedObjectContext *ctx = _workerContext;
    
    NSMutableSet *newQuestions = [self getAllNewQuestionsFromSet:setOfDicts];
    NSMutableSet *removedQuestions = [self getAllRemovedQuestionsFromSet:setOfDicts];
    
    NSLog(@"NEW QUESTIONS [%lu]", (unsigned long)newQuestions.count);
    if ([newQuestions count] > 0) {
        
        for (NSDictionary *dict in newQuestions) {
            
            NSString *entity_type = kQuestionEntity;
            NSString *question_id_string = [dict valueForKey:@"id"];
            NSLog(@"QUESTION ID : %@", question_id_string);
            
            NSManagedObject *mo = [self getEntity:entity_type
                                        predicate:[self predicateForKeyPath:@"id" object:question_id_string]
                                          context:ctx];
            if (mo == nil) {
                mo = [NSEntityDescription insertNewObjectForEntityForName:entity_type
                                                   inManagedObjectContext:ctx];
            }
            
            NSArray *keys = [dict allKeys];//object.entity.propertiesByName.allKeys;
                
            for (NSString *key in keys) {
                
                if (([dict valueForKey:key] != nil) && !([[dict valueForKey:key] isKindOfClass:[NSNull class]])) {
                    
                    id value = [dict valueForKey:key];

                    //RESET IS_SELECTED AT QUESTION ENTITY
                    if ([key isEqualToString:@"is_selected"]) {
                        value = [NSNumber numberWithBool:NO];
                    }
                    
                    //////////////////////////////////////////////
                    // ---------- STRING PROCESSING  ----------///
                    //////////////////////////////////////////////
                    if (![key isEqualToString:@"choices"] && ![key isEqualToString:@"tags"]) {
                        [mo setValue:value forKey:key];
                    }
                    
                    //////////////////////////////////////////////
                    // ---------- CHOICE PROCESSING  ----------///
                    //////////////////////////////////////////////
                    if ([key isEqualToString:@"choices"]) {
                        NSLog(@"VALUE TYPE [%@]", [value class]);
                        
                        NSSet *choices = (NSSet *)[dict valueForKey:key];
                        NSArray *choices_array = [choices allObjects];
                        
                        // CHOICE ITEM SET CONTAINER
                        NSMutableSet *mutable_set = [NSMutableSet set];
                        
                        // LOOP THRU CHOICE ENTITY VALUES
                        for (NSManagedObject *c_mo in choices_array) {
                            // CHOICE KEY LOOKUPS
                            NSArray *c_keys = [c_mo.entity.propertiesByName allKeys];
                            
                            // CHOICE ITEM CREATION
                            NSManagedObject *choice_item = [NSEntityDescription insertNewObjectForEntityForName:kChoiceItemEntity inManagedObjectContext:ctx];
                            // VALUE COPYING CHOICE -> CHOICE ITEM
                            for (NSString *j_key in c_keys) {
                                if (![j_key isEqualToString:@"question"]) {
                                    id j_value = [c_mo valueForKey:j_key];
                                    [choice_item setValue:j_value forKey:j_key];
                                }
                            }
                            // ADD THE NEW CHOICE ITEM MO
                            [mutable_set addObject:choice_item];
                        }
                        
                        [mo setValue:[NSSet setWithSet:mutable_set] forKey:@"choices"];
                    }
                    
                } // ----> END
                
            }// ----> LOOP END
            
            [self saveTreeContext:ctx];
        }
////        if (doneBlock) {
////            doneBlock(YES);
////        }
//    } else {
//        [self clearDataForEntity:kQuestionEntity withPredicate:nil context:ctx];
//        
//        if (doneBlock) {
//            doneBlock(YES);
//        }
    }
    
    
    if (removedQuestions.count > 0) {
        for (NSManagedObject *mo in removedQuestions) {
            NSString *question_id_string = [NSString stringWithFormat:@"%@", [mo valueForKey:@"id"]];
            NSPredicate *predicate = [self predicateForKeyPath:@"id" object:question_id_string];
            [self clearDataForEntity:kQuestionEntity withPredicate:predicate context:ctx];
        }
    }

    
    if (doneBlock) {
        doneBlock(YES);
    }
}

/**
Accepts set of Dictionaries




*/

- (NSMutableSet *)getAllNewQuestionsFromSet:(NSMutableSet *)set {
    
    NSMutableSet *questionsToAdd = [NSMutableSet set];
    NSArray *assignedQuestions = [self getObjectsForEntity:kQuestionEntity predicate:nil];
    
    for (NSDictionary *dict in set) {
        NSString *question_id_string = [NSString stringWithFormat:@"%@", [dict valueForKey:@"id"]];
        NSPredicate *predicate = [self predicateForKeyPath:@"id" object:question_id_string];
        
        NSArray *newQuestions = [assignedQuestions filteredArrayUsingPredicate:predicate];
        
        if (newQuestions.count == 0) {
            [questionsToAdd addObject:dict];
        }
    }
    
    return questionsToAdd;
}

- (NSMutableSet *)getAllRemovedQuestionsFromSet:(NSMutableSet *)set {
    NSMutableSet *questionsToRemove = [NSMutableSet set];
    NSArray *assignedQuestions = [self getObjectsForEntity:kQuestionEntity predicate:nil];
    
    for (NSManagedObject *mo in assignedQuestions) {
        NSString *question_id_string = [NSString stringWithFormat:@"%@", [mo valueForKey:@"id"]];
        NSPredicate *predicate = [self predicateForKeyPath:@"id" object:question_id_string];
        
        NSArray *removedQuestion = [[set filteredSetUsingPredicate:predicate] allObjects];
        
        if (removedQuestion.count == 0) {
            [questionsToRemove addObject:mo];
        }
    }
    
    return questionsToRemove;
}

- (void)copyQuestionItemsFromTestInformation:(NSManagedObject *)testObject doneBlock:(TestGuruDoneBlock)doneBlock {
    
    if (testObject != nil) {
        
        NSArray *items = [[testObject valueForKey:@"question_items"] allObjects];
        
        if ([items count] > 0) {
            
            NSManagedObjectContext *ctx = _workerContext;
            
            [self clearDataForEntity:kQuestionEntity withPredicate:nil context:ctx];
            
            [ctx performBlock:^{
                
                for (NSManagedObject *object in items) {
                    
                    
                    NSString *entity_type = kQuestionEntity;
                    
                    //                    NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:entity_type
                    //                                                                        inManagedObjectContext:ctx];
                    /*
                     date_created X
                     date_modified X
                     id ——-
                     is_deleted X
                     points X
                     question_course_id ———
                     question_id ==== id
                     question_name ==== name
                     question_text X
                     question_type_id X
                     question_type_name ==== questionTypeName
                     quiz_id ==== testInformation
                     sort_date_modified ———
                     */
                    
                    NSString *question_id = [object valueForKey:@"question_id"]; //ID
                    NSString *name = [object valueForKey:@"question_name"];
                    NSString *question_text = [object valueForKey:@"question_text"];
                    NSString *question_type_id = [object valueForKey:@"question_type_id"];
                    NSString *questionTypeName = [object valueForKey:@"question_type_name"];
                    NSString *date_created = [object valueForKey:@"date_created"];
                    NSString *date_modified = [object valueForKey:@"date_modified"];
                    NSString *formatted_date_modified = [object valueForKey:@"formatted_date_modified"];
                    NSString *is_deleted = [object valueForKey:@"is_deleted"];
                    NSString *points = [object valueForKey:@"points"];
                    NSString *proficiency_level_id = [object valueForKey:@"question_difficulty_level_id"];
                    NSString *search_string = [object valueForKey:@"search_string"];
                    NSString *question_package_type_icon = [object valueForKey:@"question_package_type_icon"];
                    
                    NSManagedObject *mo = [self getEntity:entity_type predicate:[self predicateForKeyPath:@"id" andValue:question_id] context:ctx];
                    
                    if (mo == nil) {
                        mo = [NSEntityDescription insertNewObjectForEntityForName:entity_type inManagedObjectContext:ctx];
                    }
                    
                    [mo setValue:question_id forKey:@"id"];
                    [mo setValue:name forKey:@"name"];
                    [mo setValue:question_text forKey:@"question_text"];
                    [mo setValue:question_type_id forKey:@"question_type_id"];
                    [mo setValue:questionTypeName forKey:@"questionTypeName"];
                    [mo setValue:date_created forKey:@"date_created"];
                    [mo setValue:date_modified forKey:@"date_modified"];
                    [mo setValue:formatted_date_modified forKey:@"formatted_date_modified"];
                    [mo setValue:is_deleted forKey:@"is_deleted"];
                    [mo setValue:points forKey:@"points"];
                    [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];
                    [mo setValue:search_string forKey:@"search_string"];
                    [mo setValue:question_package_type_icon forKey:@"package_type_icon"];
                    
                    // QUESTION IMAGES DEEP COPY
                    NSSet *question_images = [object valueForKey:@"question_images"];
                    
                    NSString *image_one = @"";
                    NSString *image_two = @"";
                    NSString *image_three = @"";
                    NSString *image_four = @"";
                    
                    for (NSManagedObject *image_mo in question_images) {
                        NSString *image_index = [image_mo valueForKey:@"index"];
                        NSString *image = [image_mo valueForKey:@"image"];
                        
                        if (image.length > 0) {
                            if ([image_index isEqual:@"1"]) {
                                image_one = [NSString stringWithFormat:@"%@", image ];
                            }
                            if ([image_index isEqual:@"2"]) {
                                image_two = [NSString stringWithFormat:@"%@", image ];
                            }
                            if ([image_index isEqual:@"3"]) {
                                image_three = [NSString stringWithFormat:@"%@", image ];
                            }
                            if ([image_index isEqual:@"4"]) {
                                image_four = [NSString stringWithFormat:@"%@", image ];
                            }
                        }
                    }
                    
                    [mo setValue:image_one forKey:@"image_one"];
                    [mo setValue:image_two forKey:@"image_two"];
                    [mo setValue:image_three forKey:@"image_three"];
                    [mo setValue:image_four forKey:@"image_four"];
                    
                    
                    // Question choices
                    NSSet *choices = [object valueForKey:@"question_choices"];
                    NSArray *choices_array = [choices allObjects];
                    
                    // CHOICE ITEM SET CONTAINER
                    NSMutableSet *mutable_set = [NSMutableSet set];
                    
                    // LOOP THRU CHOICE ENTITY VALUES
                    for (NSManagedObject *c_mo in choices_array) {
                        // CHOICE KEY LOOKUPS
                        NSArray *c_keys = [c_mo.entity.propertiesByName allKeys];
                        
                        // CHOICE ITEM CREATION
                        NSManagedObject *choice_item = [NSEntityDescription insertNewObjectForEntityForName:kChoiceItemEntity inManagedObjectContext:ctx];
                        // VALUE COPYING CHOICE -> CHOICE ITEM
                        for (NSString *j_key in c_keys) {
                            if (![j_key isEqualToString:@"question_item"]) {
                                id j_value = [c_mo valueForKey:j_key];
                                
                                if ([j_key isEqualToString:@"order_number"]) {
                                    j_value = [NSNumber numberWithDouble:[j_value doubleValue] ];
                                }
                                
                                [choice_item setValue:j_value forKey:j_key];
                            }
                        }
                        // ADD THE NEW CHOICE ITEM MO
                        [mutable_set addObject:choice_item];
                    }
                    
                    [mo setValue:[NSSet setWithSet:mutable_set] forKey:@"choices"];
                    
                    // DEFAULT VALUE for test create purpose
                    NSNumber *is_selected = [NSNumber numberWithBool:NO];
                    [mo setValue:is_selected forKey:@"is_selected"];
                }
                
                [self saveTreeContext:ctx];
                
                if (doneBlock) {
                    doneBlock(YES);
                }
                
            }];
        }
    }
}

/////// TEST BANK v2.3 ///////
- (void)requestPDFdata:(NSDictionary *)info process:(TestGuruDataBlock)dataBlock {

    //User ID
    NSString *user_id = [self loginUser];
    
    //Course Section ID
    NSString *course_id = [self stringValue:[self fetchObjectForKey:kTGQB_SELECTED_COURSE_ID]];
    
    //Quiz ID
    NSString *quiz_id = [self stringValue: info[@"qid"] ];

    //Section IDs
    NSArray *cs_ids = [NSArray arrayWithArray: info[@"cs_ids"] ];
    NSDictionary *parameters = @{@"course_section_ids": cs_ids};
    
    NSString *email_flag = [self stringValue: info[@"email"] ];
    NSString *playlist_flag = [self stringValue: info[@"playlist"] ];
    
    NSString *saveToPDF = [NSString stringWithFormat:kEndPointTGTBSaveToPDF, user_id, course_id, quiz_id];
    saveToPDF = [NSString stringWithFormat:@"%@?email=%@", saveToPDF, email_flag]; // DEFAULT URL ENCODED FOR EMAIL
    
    /*
     NOTE: for iOS app zip files are not viewable in the playlist module, there is no file explorer, in the file
     which is a known limitation.
     */
    if ([playlist_flag isEqualToString:@"1"]) {
        saveToPDF = [NSString stringWithFormat:@"%@&playlist=%@", saveToPDF, playlist_flag];
    }
    
    NSURL *url = [self buildURL:saveToPDF];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:parameters];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              if (dictionary != nil) {
                  
                  /*
                   {
                       "_meta": {
                           "status": "SUCCESS",
                           "count": 1
                       },
                       "records": {
                           "process_id": "4307"
                       }
                   }
                   */

                  NSLog(@"data : %@", dictionary);
                  if (dictionary[@"records"] != nil ) {
                      NSDictionary *records = dictionary[@"records"];
                      NSString *process_id = [self stringValue: records[@"process_id"] ];
                      NSDictionary *data = @{@"process_id": process_id};
                      if (dataBlock) {
                          dataBlock(data);
                      }
                  }
              }
          }//end
          
      }];
    
    [task resume];
}

- (void)requestFilteredQuestionBankWithPage:(NSString *)pageNumber
                                withKeyword:(NSString *)search_key
                                  filterIDs:(NSArray *)arrayOfIDs
                                       sort:(NSDictionary *)sort
                                  dataBlock:(TestGuruDataBlock)dataBlock {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    NSString *limit = [self fetchObjectForKey:kTG_PAGINATION_LIMIT];
    
    NSString *course_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_COURSE_ID]];
    NSString *package_id = [NSString stringWithFormat:@"%@",[self fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID]];
    NSString *encodedSearchKey = [self urlEncode:search_key];
    
    NSDictionary *postBody = [self createQuestionBankFilterBody:arrayOfIDs];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruFilterQuestionBankV2, course_id, package_id, user_id, limit, pageNumber, encodedSearchKey]];
    
    if (sort != nil) {
        NSString *sort_by = [self stringValue:sort[@"sort_by"]];
        NSString *view_by = [self stringValue:sort[@"view_by"]];
        NSString *sort_kw = [NSString stringWithFormat:@"%@,%@", sort_by, view_by];
        
        if ([view_by isEqualToString:@""]) {
            sort_kw = sort_by;
        }
        
        if ([sort_by isEqualToString:@""]) {
            sort_kw = view_by;
        }
        
        if (![sort_kw isEqualToString:@","]) {
            url = [self buildURL:[NSString stringWithFormat:kEndPointTestGuruFilterQuestionBankV3, course_id, package_id, user_id, limit, pageNumber, encodedSearchKey, sort_kw]];
        }
        
    }
    
    NSLog(@"url: %@", url);
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            
            if (dictionary[@"records"] != nil ) {
                NSDictionary *records = dictionary[@"records"];
                NSArray *data = records[@"data"];
                NSManagedObjectContext *ctx = _workerContext;
                
                if ([pageNumber  isEqual:@"1"]) {
                    [self clearDataForEntity:kQuestionBankEntity withPredicate:nil context:ctx];
                }
                
                if (data.count > 0) {
                    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                
                    [ctx performBlockAndWait:^{
                        NSInteger index = [self getStartingIndexForSorting];
                        NSLog(@"index: %zd", index);
                        
                        for (NSMutableDictionary *d in data) {
                            NSMutableDictionary *question_object = d[@"question"];
                            
                            NSLog(@"BEFORE: %@", question_object);
                            
                            // DYNAMIC SORTING
                            [question_object setObject:@(index) forKey:@"index"];
                            
                            NSLog(@"AFTER: %@", question_object);
                            
                            NSString *course_id_string = [self stringValue:course_id];
                            [question_object setValue:course_id_string forKey:@"course_id"];
                            [self processQuestionDataV3:d sectionName:@"" homeurl:homeurl details:NO managedObjectContext:ctx];
                            
                            index++;
                        }
                        
                        [self saveTreeContext:ctx];
                        
                    }];
                }
                if (dataBlock) {
                    NSDictionary *pagingInfo = [self pagingInformation:records[@"pagination"] total:data.count];
                    dataBlock(pagingInfo);
                }
            } else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
}

- (NSInteger)getStartingIndexForSorting {
    NSArray *records = [self getObjectsForEntity:kQuestionBankEntity predicate:nil];
    
    if (records != nil) {
        return records.count;
    }
    
    return 0;
}

- (void)updateObjectsFromEntity:(NSString *)entity details:(NSDictionary *)dictionary predicate:(NSPredicate *)predicate {
    
    NSArray *arrayOfObjects = [self getObjectsForEntity:entity predicate:predicate];
    
    if (arrayOfObjects.count > 0) {
        for (NSManagedObject *mo in arrayOfObjects) {
            [self updateObject:mo withDetails:dictionary];
        }
    }
}

- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    NSManagedObjectContext *ctx = _workerContext;
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return items;
    }
    
    return nil;
}

- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate sortDescriptor:(NSSortDescriptor *)sortDescriptor {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    NSManagedObjectContext *ctx = _workerContext;
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return items;
    }
    
    return nil;
}

- (NSDictionary *)generateTestData:(NSManagedObject *)mo editMode:(BOOL)mode {

    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:kServerDateFormat];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    // SAMPLE JSON PAYLOAD
    
    /*
    "name": "nem test1211112",
    "general_instruction": "asd123adsda",
    "attempts": 10,
    "time_limit": "12:31",
    "date_open": "2001-12-31",
    "date_close": "2001-12-31",
    "passing_rate": "100",
    "is_graded": 1,
    "is_shuffle_answers": 0,
    "is_shuffle_questions": 0,
    "is_forced_complete": 1,
    "show_score": 0,
    "show_result": 0,
    "show_correct_answers": 0,
    "show_feedbacks": 1,
    "password": null,
    "question_ids": ["378", "379"],
    "total_score": 100,
    "allow_review": 1
     */
    
    if (mo != nil) {
        NSString *name = [self stringValue: [mo valueForKey:@"name"] ];
        NSString *general_instruction = [self stringValue: [mo valueForKey:@"general_instruction"] ];
        NSString *attempts = [self stringValue: [mo valueForKey:@"attempts"] ]; //NUMBER
        NSString *time_limit = [self stringValue: [mo valueForKey:@"time_limit"] ];
        
        // NSString *date_open = [self valueTransformDateString: [mo valueForKey:@"date_open"] formatter:df];
        // NSString *date_close = [self valueTransformDateString: [mo valueForKey:@"date_close"] formatter:df];
        
        NSString *date_open = [self stringValue:[mo valueForKey:@"date_open"]];
        NSString *date_close = [self stringValue:[mo valueForKey:@"date_close"]];
        
        //NOTE: VERY IMPORTANT
        // SERVER SIDE PARSES PASSING RATE
        // PASSING SCORE IS JUST FOR DISPLAY PURPOSES
        NSString *passing_rate = [self stringValue: [mo valueForKey:@"passing_score"] ]; //NUMBER
        
        
        NSString *is_graded = [self stringValue: [mo valueForKey:@"is_graded"] ]; //NUMBER
        
        NSString *course_id = [self stringValue: [self fetchObjectForKey:kTGQB_SELECTED_COURSE_ID] ]; // STRING
        
        //NOTE: VERY IMPORTANT
        // SERVER SIDE PARSES is_shuffle_answers
        // SHUFFLE CHOICES shuffle_choices
        NSString *is_shuffle_answers = [self stringValue: [mo valueForKey:@"shuffle_choices"] ]; //NUMBER
        
        //NOTE: VERY IMPORTANT
        // SERVER SIDE PARSES is_shuffle_questions
        // SHUFFLE QUESTIONS shuffle_questions
        NSString *is_shuffle_questions = [self stringValue: [mo valueForKey:@"shuffle_questions"] ]; //NUMBER
        
        //NOTE: VERY IMPORTANT
        // SERVER SIDE PARSES is_forced_complete
        // FORCE SUBMIT is_forced_submit
        NSString *is_forced_complete = [self stringValue: [mo valueForKey:@"is_forced_submit"] ]; //NUMBER
        
        NSString *show_score = [self stringValue: [mo valueForKey:@"show_score"] ]; //NUMBER
        NSString *show_result = [self stringValue: [mo valueForKey:@"show_result"] ]; //NUMBER
        
        //NOTE: VERY IMPORTANT
        // SERVER SIDE PARSES show_correct_answers
        // FORCE SUBMIT show_correct_answer
        NSString *show_correct_answers = [self stringValue: [mo valueForKey:@"show_correct_answer"] ]; //NUMBER
        NSString *show_feedbacks = [self stringValue: [mo valueForKey:@"show_feedbacks"] ]; //NUMBER
        NSString *password = [self stringValue: [mo valueForKey:@"password"] ];
//        NSString *total_score = [self stringValue: [mo valueForKey:@"total_score"] ]; //NUMBER
        NSString *allow_review = [self stringValue: [mo valueForKey:@"allow_review"] ]; //NUMBER
        
        NSString *course_period_id = [self stringValue: [mo valueForKey:@"academic_term_id"] ];
        NSString *quiz_type_id = [self stringValue: [mo valueForKey:@"assessment_type_id"] ]; //NUMBER
        
        //NSARRAY
        //"question_ids": ["378", "379"],
        NSMutableArray *question_ids = [NSMutableArray array];

        NSString *entity_name = kQuestionEntity;
        
        //BUILD
        NSManagedObjectContext *ctx = _workerContext;
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity_name];
        [request setResultType:NSDictionaryResultType];
        [request setReturnsDistinctResults:YES];
        [request setPropertiesToFetch:@[@"id"]];
        NSArray *items = [ctx executeFetchRequest:request error:nil];
        
        for (NSDictionary *d in items) {
            NSString *string_id = [self stringValue: d[@"id"] ];
            [question_ids addObject:string_id];
        }
        
        data[@"name"] = name;
        data[@"general_instruction"] = general_instruction;
        data[@"attempts"] = [self valueTransformString:attempts];
        data[@"time_limit"] = time_limit;
        data[@"date_open"] = date_open;
        data[@"date_close"] = date_close;
        data[@"passing_rate"] = [self formatStringNumber:passing_rate];
        data[@"is_graded"] = [self valueTransformString:is_graded];
        data[@"is_shuffle_answers"] = [self valueTransformString:is_shuffle_answers];
        data[@"is_shuffle_questions"] = [self valueTransformString:is_shuffle_questions];
        data[@"is_forced_complete"] = [self valueTransformString:is_forced_complete];
        data[@"show_score"] = [self valueTransformString:show_score];
        data[@"show_result"] = [self valueTransformString:show_result];
        data[@"show_correct_answers"] = [self valueTransformString:show_correct_answers];
        data[@"show_feedbacks"] = [self valueTransformString:show_feedbacks];
        data[@"password"] = password;
//        data[@"total_score"] = [self valueTransformString:total_score];
        data[@"allow_review"] = [self valueTransformString:allow_review];
        data[@"question_ids"] = [NSArray arrayWithArray:question_ids];
        data[@"course_id"] = course_id;
        data[@"course_period_id"] = [self valueTransformString:course_period_id];
        data[@"quiz_type_id"] = [self valueTransformString:quiz_type_id];
        data[@"result_type_id"] = @"3";
    }
    NSLog(@"----------------------------> HELLO WORLD DATA : %@", data);
    
    return [NSDictionary dictionaryWithDictionary:data];
}

- (NSString *)valueTransformDateString:(NSString *)value formatter:(NSDateFormatter *)df {

    NSString *string = [self stringValue: value ];
    
    NSDate *date_object = [df dateFromString:string];
    NSString *date_object_string = [NSString stringWithFormat:@"%@", [date_object description] ];
    
    NSLog(@"OBJECT DATE : %@", date_object_string );
    NSArray *items = [date_object_string componentsSeparatedByString:@" "];
    NSLog(@"items : %@", items);

    if ([items count] > 1) {
        NSString *yearMonthDay = [NSString stringWithFormat:@"%@", items[0] ];
        NSString *hourMinSec = [NSString stringWithFormat:@"%@", items[1] ];
        return [NSString stringWithFormat:@"%@ %@", yearMonthDay, hourMinSec];
    }
    
    return [self dateDefaultValue];
}

- (NSString *)dateDefaultValue {
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *year = [formatter stringFromDate:date];
    NSString *formatted_date = [NSString stringWithFormat:@"%@-01-01 00:00:00", year ];
    
    return formatted_date;
}

- (NSNumber *)valueTransformString:(NSString *)string {
    
    NSInteger value = [string integerValue];
    NSNumber *number = [NSNumber numberWithInteger:value];
    
    return number;
}

- (NSString *)stringLongStyleForDate:(NSDate *)date {
    return [NSDateFormatter localizedStringFromDate:date
                                          dateStyle:NSDateFormatterLongStyle
                                          timeStyle:NSDateFormatterNoStyle];
}

- (NSString *)stringDate:(NSDate *)date {
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [self.formatter setTimeZone:gmt];
    NSString *template = @"yyyy-MM-dd hh:mm:ss";
    [self.formatter setDateFormat:template];
    NSString *date_string = [self.formatter stringFromDate:date];
    
    return date_string;
}

#pragma mark - Parsing Helpers

- (BOOL)isItOkayToParseUsingThisResponseData:(NSDictionary *)response {
    NSLog(@"response: %@", response);
    
    if (response != nil) {
        NSDictionary *parsedmeta = response[@"_meta"];
        
        if (parsedmeta != nil) {
            if ([self doesKeyExistInDictionary:parsedmeta key:@"status"]) {
                NSString *status = [self stringValue:parsedmeta[@"status"]];
                
                if ([status isEqualToString:@"SUCCESS"]) {
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

- (BOOL)doesKeyExistInDictionary:(NSDictionary *)d key:(NSString *)key {
    NSArray *keys = [d allKeys];
    
    for (NSString *k in keys) {
        if ([k isEqualToString:key]) {
            return YES;
        }
    }
    
    return NO;
}

- (NSString *)formatStringNumber:(NSString *)stringNumber {
    CGFloat floatNumber = [stringNumber floatValue];
    NSInteger intNumber = (NSInteger)floatNumber;
    
    if (floatNumber == intNumber) {
        stringNumber = [NSString stringWithFormat:@"%zd", intNumber];
    }
    else {
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        formatter.maximumFractionDigits = 2;
        formatter.roundingMode = NSNumberFormatterRoundUp;
        
        stringNumber = [formatter stringFromNumber:[NSNumber numberWithFloat:floatNumber]];
    }
    
    return stringNumber;
}

- (NSString *)autoCorrectDateString:(NSString *)dateString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    dateFormatter.dateFormat = kServerDateFormat;
    
    return [self valueTransformDateString:dateString formatter:dateFormatter];
}

#pragma mark - Create/Edit Question Helper

- (void)deepCopyManagedObject:(NSManagedObject *)object objectBlock:(TestGuruManagedObjectBlock)objectBlock {

    if (object != nil) {

        NSManagedObjectContext *ctx = _workerContext;
        
        NSString *questionEntity = kQuestionEntity;
        
        BOOL cleared = [self clearDataForEntity:questionEntity withPredicate:nil context:ctx];
        cleared = [self clearDataForEntity:kChoiceItemEntity withPredicate:nil context:ctx];

        if (cleared) {
            NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:questionEntity
                                                                inManagedObjectContext:object.managedObjectContext];
            
            NSArray *keys = object.entity.propertiesByName.allKeys;
            
            for (NSString *key in keys) {

                if ([object valueForKey:key] != nil) {
                    
                    id value = [object valueForKey:key];
                    
                    //////////////////////////////////////////////
                    // ---------- STRING PROCESSING  ----------///
                    //////////////////////////////////////////////
                    if (![key isEqualToString:@"choices"]) {
                        [mo setValue:value forKey:key];
                    }
                    
                    
                    //////////////////////////////////////////////
                    // ---------- CHOICE PROCESSING  ----------///
                    //////////////////////////////////////////////
                    if ([key isEqualToString:@"choices"]) {
                        NSLog(@"VALUE TYPE [%@]", [value class]);
                        
                        NSSet *choices = (NSSet *)[object valueForKey:key];
                        NSArray *choices_array = [choices allObjects];
                        
                        // CHOICE ITEM SET CONTAINER
                        NSMutableSet *mutable_set = [NSMutableSet set];
                        
                        // LOOP THRU CHOICE ENTITY VALUES
                        for (NSManagedObject *c_mo in choices_array) {
                            // CHOICE KEY LOOKUPS
                            NSArray *c_keys = [c_mo.entity.propertiesByName allKeys];
                            
                            // CHOICE ITEM CREATION
                            NSManagedObject *choice_item = [NSEntityDescription insertNewObjectForEntityForName:kChoiceItemEntity
                                                                                         inManagedObjectContext:c_mo.managedObjectContext];
                            // VALUE COPYING CHOICE -> CHOICE ITEM
                            for (NSString *j_key in c_keys) {
                                if (![j_key isEqualToString:@"question"]) {
                                    id j_value = [c_mo valueForKey:j_key];
                                    [choice_item setValue:j_value forKey:j_key];
                                }
                            }
                            // ADD THE NEW CHOICE ITEM MO
                            [mutable_set addObject:choice_item];
                        }
                        
                        [mo setValue:[NSSet setWithSet:mutable_set] forKey:@"choices"];
                    }
                    
                } // ----> END
            }
            
            [self saveTreeContext:ctx];
            
            if (objectBlock) {
                objectBlock(mo);
            }
        }
    }
}

- (NSString *)urlEncode:(id<NSObject>)value
{
    //make sure param is a string
    if ([value isKindOfClass:[NSNumber class]]) {
        value = [(NSNumber*)value stringValue];
    }
    
    NSAssert([value isKindOfClass:[NSString class]], @"request parameters can be only of NSString or NSNumber classes. '%@' is of class %@.", value, [value class]);
    
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                 NULL,
                                                                                 (__bridge CFStringRef) value,
                                                                                 NULL,
                                                                                 (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                 kCFStringEncodingUTF8));
}

#pragma mark - User Standard Defaults Helper Functions

- (void)saveObject:(id)object forKey:(NSString *)key {
    NSLog(@"%s [%@] [%@]", __PRETTY_FUNCTION__, object, key);
    [self.userDefaults setObject:object forKey:key];
    [self.userDefaults synchronize];
}

- (id)fetchObjectForKey:(NSString *)key {
    id object = [self.userDefaults objectForKey:key];
    return object;
}

#pragma mark - Test Management Related Methods

- (void)requestPaginatedTestListForCoordinator:(NSString *)coordinator paginationParameters:(NSDictionary *)parameters dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSString *user_id = coordinator;
    NSString *current_page = [self stringValue:parameters[@"current_page"]];
    NSString *limit = [self stringValue:parameters[@"limit"]];
    NSString *course_id = [self stringValue:parameters[@"course_id"]];
    NSString *is_approved = [self stringValue:parameters[@"is_approved"]];
    NSString *search_keyword = [self urlEncode:[self stringValue:parameters[@"search_keyword"]]];
    
    NSManagedObjectContext *ctx = self.workerContext;
    
    if ([current_page isEqualToString:@"1"]) {
        [self clearDataForEntity:kCoordinatorTestEntity withPredicate:nil context:ctx];
    }
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTMCoordinatorTestList, user_id, current_page, limit, course_id, is_approved, search_keyword]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSDictionary *records = dictionary[@"records"];
                NSDictionary *pagination = records[@"pagination"];
                NSArray *data = records[@"data"];
                
                NSLog(@"test detail records: %@", records);
                NSLog(@"test detail records pagination: %@", data);
                NSLog(@"test detail records data: %@", data);
                
                if ([self isArrayObject:data] && data.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d in data) {
                            NSString *test_id = [self stringValue:d[@"id"]];
                            NSString *quiz_settings_id = [self stringValue:d[@"quiz_settings_id"]];
                            NSString *user_id = [self stringValue:d[@"user_id"]];
                            NSString *last_name = [self stringValue:d[@"last_name"]];
                            NSString *first_name = [self stringValue:d[@"first_name"]];
                            NSString *name = [self stringValue:d[@"name"]];
                            NSString *course_name = [self stringValue:d[@"course_name"]];
                            NSString *course_id = [self stringValue:d[@"course_id"]];
                            NSString *general_instruction = [self stringValue:d[@"general_instruction"]];
                            NSString *description = [self stringValue:d[@"description"]];
                            NSString *quiz_category_id = [self stringValue:d[@"quiz_category_id"]];
                            NSString *quiz_category_name = [self stringValue:d[@"quiz_category_name"]];
                            NSString *course_period_id = [self stringValue:d[@"course_period_id"]];
                            NSString *quiz_type_id = [self stringValue:d[@"quiz_type_id"]];
                            NSString *result_type_id = [self stringValue:d[@"result_type_id"]];
                            NSString *difficulty_level_id = [self stringValue:d[@"difficulty_level_id"]];
                            NSString *formatted_date_open = [self stringValue:d[@"formatted_date_open"]];
                            NSString *date_open = [self stringValue:d[@"date_open"]];
                            NSString *formatted_date_close = [self stringValue:d[@"formatted_date_close"]];
                            NSString *date_close = [self stringValue:d[@"date_close"]];
                            NSString *no_expiry = [self stringValue:d[@"no_expiry"]];
                            NSString *attempts = [self stringValue:d[@"attempts"]];
                            NSString *shuffle_questions = [self stringValue:d[@"shuffle_questions"]];
                            NSString *shuffle_choices = [self stringValue:d[@"shuffle_choices"]];
                            NSString *show_score = [self stringValue:d[@"show_score"]];
                            NSString *show_result = [self stringValue:d[@"show_result"]];
                            NSString *show_correct_answer = [self stringValue:d[@"show_correct_answer"]];
                            NSString *total_score = [self stringValue:d[@"total_score"]];
                            NSString *time_limit = [self stringValue:d[@"time_limit"]];
                            NSString *is_forced_submit = [self stringValue:d[@"is_forced_submit"]];
                            NSString *password = [self stringValue:d[@"password"]];
                            NSString *is_approved = [self stringValue:d[@"is_approved"]];
                            NSString *is_graded = [self stringValue:d[@"is_graded"]];
                            NSString *show_feedbacks = [self stringValue:d[@"show_feedbacks"]];
                            NSString *allow_review = [self stringValue:d[@"allow_review"]];
                            NSString *passing_score = [self stringValue:d[@"passing_score"]];
                            NSString *is_expired = [self stringValue:d[@"is_expired"]];
                            NSString *quiz_stage_name = [self stringValue:d[@"quiz_stage_name"]];
                            NSString *item_count = [self stringValue:d[@"item_count"]];
                            NSString *date_modified = [self stringValue:d[@"date_modified"]];
                            NSString *date_created = [self stringValue:d[@"date_created"]];
                            NSString *formatted_date = [self stringValue:d[@"formatted_date"]];
                            
                            NSManagedObject *mo = [self getEntity:kCoordinatorTestEntity attribute:@"id" parameter:test_id context:ctx];
                            
                            [mo setValue:@([test_id integerValue]) forKey:@"id"];
                            [mo setValue:quiz_settings_id forKey:@"quiz_settings_id"];
                            [mo setValue:user_id forKey:@"user_id"];
                            [mo setValue:last_name forKey:@"last_name"];
                            [mo setValue:first_name forKey:@"first_name"];
                            [mo setValue:name forKey:@"name"];
                            [mo setValue:course_name forKey:@"course_name"];
                            [mo setValue:course_id forKey:@"course_id"];
                            [mo setValue:general_instruction forKey:@"general_instruction"];
                            [mo setValue:description forKey:@"test_description"];
                            [mo setValue:quiz_category_id forKey:@"quiz_category_id"];
                            [mo setValue:quiz_category_name forKey:@"quiz_category_name"];
                            [mo setValue:course_period_id forKey:@"course_period_id"];
                            [mo setValue:quiz_type_id forKey:@"quiz_type_id"];
                            [mo setValue:result_type_id forKey:@"result_type_id"];
                            [mo setValue:difficulty_level_id forKey:@"difficulty_level_id"];
                            [mo setValue:formatted_date_open forKey:@"formatted_date_open"];
                            [mo setValue:date_open forKey:@"date_open"];
                            [mo setValue:formatted_date_close forKey:@"formatted_date_close"];
                            [mo setValue:date_close forKey:@"date_close"];
                            [mo setValue:no_expiry forKey:@"no_expiry"];
                            [mo setValue:attempts forKey:@"attempts"];
                            [mo setValue:shuffle_questions forKey:@"shuffle_questions"];
                            [mo setValue:shuffle_choices forKey:@"shuffle_choices"];
                            [mo setValue:show_score forKey:@"show_score"];
                            [mo setValue:show_result forKey:@"show_result"];
                            [mo setValue:show_correct_answer forKey:@"show_correct_answer"];
                            [mo setValue:total_score forKey:@"total_score"];
                            [mo setValue:time_limit forKey:@"time_limit"];
                            [mo setValue:is_forced_submit forKey:@"is_forced_submit"];
                            [mo setValue:password forKey:@"password"];
                            [mo setValue:is_approved forKey:@"is_approved"];
                            [mo setValue:is_graded forKey:@"is_graded"];
                            [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
                            [mo setValue:allow_review forKey:@"allow_review"];
                            [mo setValue:passing_score forKey:@"passing_score"];
                            [mo setValue:is_expired forKey:@"is_expired"];
                            [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
                            [mo setValue:item_count forKey:@"item_count"];
                            [mo setValue:date_modified forKey:@"date_modified"];
                            [mo setValue:date_created forKey:@"date_created"];
                            [mo setValue:formatted_date forKey:@"formatted_date"];
                            
                            NSDate *sort_date_modified = [self parseDateFromString:date_modified];
                            [mo setValue:sort_date_modified forKey:@"sort_date_modified"];
                            
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                }
                
                if (dataBlock) {
                    NSString *current_page = [self stringValue:pagination[@"current_page"]];
                    NSString *total_filtered = [self stringValue:pagination[@"total_filtered"]];
                    NSString *total_items = [self stringValue:pagination[@"total_items"]];
                    NSString *total_pages = [self stringValue:pagination[@"total_pages"]];
                    
                    NSDictionary *data = @{@"current_page": @([current_page integerValue]),
                                           @"total_filtered": @([total_filtered integerValue]),
                                           @"total_items": @([total_items integerValue]),
                                           @"total_pages": @([total_pages integerValue])};
                    dataBlock(data);
                }
            }
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
    
}

- (void)requestCourseListForCoordinatorID:(NSString *)coordinator doneBlock:(TestGuruDoneBlock)doneBlock {
    
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kCoordinatorCourseEntity withPredicate:nil context:ctx];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTMCourseListing, coordinator]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"course records: %@", records);
                
                if (records.count > 0) {
                    
                    [ctx performBlock:^{
                        
                        for (NSDictionary *d in records) {
                            
                            /*
                            {
                                category_id: "1",
                                category_name: "Mathematics",
                                courses: [
                                                {
                                                    id: "3",
                                                    name: "Mathematics",
                                                    initial: "Math",
                                                    description: "",
                                                    course_category_id: "1",
                                                    is_coordinated: 1
                                                }
                                          ]
                            }
                             */
                            
                            
                            NSString *course_id = [self stringValue:d[@"id"]];
                            NSString *name = [self stringValue:d[@"name"]];
                            NSString *initial = [self stringValue:d[@"initial"]];
                            NSString *description = [self stringValue:d[@"description"]];
                            NSString *course_code = [self stringValue:d[@"course_code"]];
                            
                            NSManagedObject *mo = [self getEntity:kCoordinatorCourseEntity attribute:@"id" parameter:course_id context:ctx];
                            
                            [mo setValue:@([course_id integerValue]) forKey:@"id"];
                            [mo setValue:name forKey:@"name"];
                            [mo setValue:initial forKey:@"initial"];
                            [mo setValue:description forKey:@"course_description"];
                            [mo setValue:course_code forKey:@"course_code"];
                        }
                        
                        NSString *course_id = @"-1";
                        NSString *name = NSLocalizedString(@"All Course", "");    // unchecked
                        NSString *initial = @"";
                        NSString *description = @"";
                        NSString *course_code = @"";
                        
                        NSManagedObject *mo = [self getEntity:kCoordinatorCourseEntity attribute:@"id" parameter:course_id context:ctx];
                        
                        [mo setValue:@([course_id integerValue]) forKey:@"id"];
                        [mo setValue:name forKey:@"name"];
                        [mo setValue:initial forKey:@"initial"];
                        [mo setValue:description forKey:@"course_description"];
                        [mo setValue:course_code forKey:@"course_code"];
                        
                        [self saveTreeContext:ctx];
                    }];
                }
            }
            
            if (doneBlock) {
                doneBlock(isOkayToParse);
            }
        }
    }];
    
    [task resume];
}

- (void)requestCourseListForCoordinator:(NSString *)coordinator doneBlock:(TestGuruDoneBlock)doneBlock {
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kCoordinatorCourseEntity withPredicate:nil context:ctx];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTMCoordinatorCourseList, coordinator]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"course records: %@", records);
                
                if (records.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d in records) {
                            NSString *course_id = [self stringValue:d[@"id"]];
                            NSString *name = [self stringValue:d[@"name"]];
                            NSString *initial = [self stringValue:d[@"initial"]];
                            NSString *description = [self stringValue:d[@"description"]];
                            NSString *course_code = [self stringValue:d[@"course_code"]];
                           
                            NSManagedObject *mo = [self getEntity:kCoordinatorCourseEntity attribute:@"id" parameter:course_id context:ctx];
                            
                            [mo setValue:@([course_id integerValue]) forKey:@"id"];
                            [mo setValue:name forKey:@"name"];
                            [mo setValue:initial forKey:@"initial"];
                            [mo setValue:description forKey:@"course_description"];
                            [mo setValue:course_code forKey:@"course_code"];
                            [mo setValue:name forKey:@"sorter"];
                        }
                        
                        NSString *course_id = @"-1";
                        NSString *name = NSLocalizedString(@"All Course", "");    // unchecked
                        NSString *initial = @"";
                        NSString *description = @"";
                        NSString *course_code = @"";
                        
                        NSManagedObject *mo = [self getEntity:kCoordinatorCourseEntity attribute:@"id" parameter:course_id context:ctx];
                        
                        [mo setValue:@([course_id integerValue]) forKey:@"id"];
                        [mo setValue:name forKey:@"name"];
                        [mo setValue:initial forKey:@"initial"];
                        [mo setValue:description forKey:@"course_description"];
                        [mo setValue:course_code forKey:@"course_code"];
                        [mo setValue:@"AAAAAAAAAA" forKey:@"sorter"];
                        
                        [self saveTreeContext:ctx];
                    }];
                }
            }
        
            if (doneBlock) {
                doneBlock(isOkayToParse);
            }
        }
    }];
    
    [task resume];
}

- (void)requestChangeTestStatus:(NSString *)status body:(NSDictionary *)body doneBlock:(TestGuruDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTMChangeTestStatus]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:body];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSArray *test = body[@"quiz_ids"];
                NSLog(@"test status updated: %@", records);
                
                NSManagedObjectContext *ctx = self.workerContext;
                
                [ctx performBlock:^{
                    for (NSDictionary *d in test) {
                        NSString *test_id = [self stringValue:d[@"id"]];
                        NSString *status = [self stringValue:d[@"status"]];
                        
                        NSManagedObject *mo = [self getEntity:kCoordinatorTestEntity attribute:@"id" parameter:test_id context:ctx];
                        
                        [mo setValue:@([test_id integerValue]) forKey:@"id"];
                        [mo setValue:status forKey:@"is_approved"];
                    }
                    
                    [self saveTreeContext:ctx];
                }];
            }
            
            if (doneBlock) {
                doneBlock(isOkayToParse);
            }
        }
    }];
    
    [task resume];
}

- (BOOL)isCoordinator {
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *is_coordinator = [self stringValue:account.user.is_coordinator];
    return [is_coordinator isEqualToString:@"1"] ? true : false;
}

- (void)requestCommentsForTestID:(NSString *)test_id forPageNumber:(NSString *)pageNumber withSearchText:(NSString *)search_keyword dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSString *user_id = [self loginUser];
    
    NSString *limit = @"100";//[self fetchObjectForKey:kTG_PAGINATION_LIMIT];
    
    NSManagedObjectContext *ctx = self.workerContext;
    
    if ([pageNumber isEqualToString:@"1"]) {
        [self clearDataForEntity:kTestCommentEntity withPredicate:nil context:ctx];
    }
    ///vsmart-rest-dev/v2/quizzes/list/comments/user/%@/test/%@?current_page=%@&limit=%@&search_keyword=%@
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointTGTMTestComments, user_id, test_id, pageNumber, limit, search_keyword]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
//                dataBlock(@{@"error":[error localizedDescription]});
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSDictionary *records = dictionary[@"records"];
                NSDictionary *pagination = records[@"pagination"];
                NSArray *data = records[@"data"];
                
                NSLog(@"test detail records: %@", records);
                NSLog(@"test detail records pagination: %@", data);
                NSLog(@"test detail records data: %@", data);
                
                NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer]];
                
                if (data != nil) {
                    [ctx performBlock:^{
                        NSInteger index = 0;
                        for (NSDictionary *d in data) {
                            NSString *comment_id = [self stringValue:d[@"id"]];
                            NSString *user_id = [self stringValue:d[@"user_id"]];
                            NSString *quiz_id = [self stringValue:d[@"quiz_id"]];
                            NSString *comment = [self stringValue:d[@"comment"]];
                            NSString *formatted_date_created = [self stringValue:d[@"formatted_date_created"]];
                            NSString *formatted_date_modified = [self stringValue:d[@"formatted_date_modified"]];
                            NSString *name = [self stringValue:d[@"name"]];
                            NSString *avatar = [self stringValue:d[@"avatar"]];
                            avatar = [NSString stringWithFormat:@"%@%@", homeurl, avatar];//homeurl + avatar;

                            NSManagedObject *mo = [self getEntity:kTestCommentEntity attribute:@"comment_id" parameter:comment_id context:ctx];

                            [mo setValue:comment_id forKey:@"comment_id"];
                            [mo setValue:user_id forKey:@"user_id"];
                            [mo setValue:quiz_id forKey:@"quiz_id"];
                            [mo setValue:comment forKey:@"comment"];
                            [mo setValue:formatted_date_created forKey:@"formatted_date_created"];
                            [mo setValue:formatted_date_modified forKey:@"formatted_date_modified"];
                            [mo setValue:name forKey:@"name"];
                            [mo setValue:avatar forKey:@"avatar"];
                            [mo setValue:@(index) forKey:@"index"];
                            index++;
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                }
                
                if (dataBlock) {
                    NSString *current_page = [self stringValue:pagination[@"current_page"]];
                    NSString *total_filtered = [self stringValue:pagination[@"total_filtered"]];
                    NSString *total_items = [self stringValue:pagination[@"total_items"]];
                    NSString *total_pages = [self stringValue:pagination[@"total_pages"]];
                    
                    NSDictionary *data = @{@"current_page": @([current_page integerValue]),
                                           @"total_filtered": @([total_filtered integerValue]),
                                           @"total_items": @([total_items integerValue]),
                                           @"total_pages": @([total_pages integerValue])};
                    dataBlock(data);
                }
            }
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
    
}

- (void)postComment:(NSString *)comment forTestID:(NSString *)test_id dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSString *user_id = [self loginUser];
    
    NSDictionary *commentDict = @{@"user_id":user_id,
                                  @"quiz_id":test_id,
                                  @"comment":comment};

    NSURL *url = [self buildURL:kEndPointTGTMPostTestComment];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:commentDict];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            if (dataBlock) {
                dataBlock(@{@"error":[error localizedDescription]});
//                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                
                
                if (dataBlock) {
                    dataBlock(nil);
                }
                
            } else {
                
                if (dataBlock) {
                    dataBlock(@{@"error":dictionary[@"records"][@"user_message"]});
                }
            }
        }
    }];
    
    [task resume];
}

- (void)editComment:(NSString *)comment forCommentID:(NSString *)comment_id dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSDictionary *commentDict = @{@"comment_id":comment_id,
                                  @"comment":comment};
    
    NSURL *url = [self buildURL:kEndPointTGTMEditComment];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:commentDict];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            if (dataBlock) {
                dataBlock(@{@"error":[error localizedDescription]});
                //                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                
                NSManagedObjectContext *ctx = self.workerContext;
                NSManagedObject *mo = [self getEntity:kTestCommentEntity attribute:@"comment_id" parameter:comment_id context:ctx];
                
                [ctx performBlockAndWait:^{
                    
                    [mo setValue:comment forKey:@"comment"];
                    [self saveTreeContext:ctx];
                }];
                
                if (dataBlock) {
                    dataBlock(nil);
                }
                
            } else {
                
                if (dataBlock) {
                    dataBlock(@{@"error":dictionary[@"records"][@"user_message"]});
                }
            }
        }
    }];
    
    [task resume];
}


- (void)deleteComment:(NSString *)comment_id dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSDictionary *commentDict = @{@"comment_id":comment_id};
    
    NSURL *url = [self buildURL:kEndPointTGTMDeleteComment];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:commentDict];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            if (dataBlock) {
                dataBlock(@{@"error":[error localizedDescription]});
                //                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                
                NSManagedObjectContext *ctx = self.workerContext;
                NSPredicate *predicate = [self predicateForKeyPath:@"comment_id" andValue:comment_id];
                [self clearDataForEntity:kTestCommentEntity withPredicate:predicate context:ctx];
                
                
                if (dataBlock) {
                    dataBlock(nil);
                }
                
            } else {
                
                if (dataBlock) {
                    dataBlock(@{@"error":dictionary[@"records"][@"user_message"]});
                }
            }
        }
    }];
    
    [task resume];
    
}

#pragma mark - Fill in the Blanks and Identification

- (NSManagedObject *)getEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate fromContext:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
}

- (void)answerBatchInsert:(NSArray *)answerList doneBlock:(TestGuruDoneBlock)doneBlock {
    [self clearContentsForEntity:kSplittedAnswerEntity predicate:nil];
    
    if (answerList.count > 0) {
        NSManagedObjectContext *ctx = _workerContext;
        
        [ctx performBlock:^{
            for (NSManagedObject *answer in answerList) {
                NSString *question_id = [NSString stringWithFormat:@"%@", [answer valueForKey:@"question_id"]];
                NSString *text = [NSString stringWithFormat:@"%@", [answer valueForKey:@"text"]];
                NSArray *splitted_answers = [text componentsSeparatedByString:@","];
                
                if (![text isEqualToString:@""]) {
                    for (NSString *sa in splitted_answers) {
                        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"answer == %@", sa];
                        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"question_id == %@", question_id];
                        NSCompoundPredicate *predicate3 = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
                        NSManagedObject *mo = [self getEntity:kSplittedAnswerEntity withPredicate:predicate3 fromContext:ctx];
                        
                        [mo setValue:question_id forKey:@"question_id"];
                        [mo setValue:sa forKey:@"answer"];
                        [mo setValue:[NSDate new] forKey:@"time_stamp"];
                    }
                }
            }
            
            [self saveTreeContext:ctx];
            
            if (doneBlock) {
                doneBlock(YES);
            }
        }];
    }
    else {
        if (doneBlock) {
            doneBlock(NO);
        }
    }
}

- (void)insertNewAnswer:(NSString *)answer withQuestionID:(NSString *)questionID doneBlock:(TestGuruDoneBlock)doneBlock {
    NSManagedObjectContext *ctx = _workerContext;
    
    if (answer.length > 0) {
        [ctx performBlock:^{
            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"answer == %@", answer];
            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"question_id == %@", questionID];
            NSCompoundPredicate *predicate3 = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
            NSManagedObject *mo = [self getEntity:kSplittedAnswerEntity withPredicate:predicate3 fromContext:ctx];
            
            [mo setValue:answer forKey:@"answer"];
            [mo setValue:questionID forKey:@"question_id"];
            [mo setValue:[NSDate new] forKey:@"time_stamp"];
            
            [self saveTreeContext:ctx];
            
            if (doneBlock) {
                doneBlock(YES);
            }
        }];
    }
    else {
        if (doneBlock) {
            doneBlock(NO);
        }
    }
}

- (void)updateAnswer:(NSString *)oldAnswer withNewAnswer:(NSString *)newAnswer withQuestionID:(NSString *)questionID doneBlock:(TestGuruDoneBlock)doneBlock {
    NSManagedObjectContext *ctx = _workerContext;
    
    if (newAnswer.length > 0) {
        [ctx performBlock:^{
            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"answer == %@", oldAnswer];
            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"question_id == %@", questionID];
            NSCompoundPredicate *predicate3 = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
            NSManagedObject *mo = [self getEntity:kSplittedAnswerEntity withPredicate:predicate3 fromContext:ctx];
            [mo setValue:newAnswer forKey:@"answer"];
            [mo setValue:[NSDate new] forKey:@"time_stamp"];
            
            [self saveTreeContext:ctx];
            
            if (doneBlock) {
                doneBlock(YES);
            }
        }];
    }
    else {
        if (doneBlock) {
            doneBlock(NO);
        }
    }
}

- (void)removeAnswer:(NSString *)answer withQuestionID:(NSString *)questionID doneBlock:(TestGuruDoneBlock)doneBlock {
    NSManagedObjectContext *ctx = _workerContext;
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"answer == %@", answer];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"question_id == %@", questionID];
    NSCompoundPredicate *predicate3 = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    BOOL flag = [self clearDataForEntity:kSplittedAnswerEntity withPredicate:predicate3 context:ctx];
    
    if (doneBlock) {
        doneBlock(flag);
    }
}

- (NSString *)concatenateAnswers:(NSArray *)answers {
    NSMutableString *concatenatedString = [NSMutableString stringWithFormat:@"%@", @""];
    NSInteger counter = 1;
    
    if (answers.count > 0) {
        for (NSManagedObject *mo in answers) {
            NSString *answer = [NSString stringWithFormat:@"%@", [mo valueForKey:@"answer"]];
            NSString *separator = (counter < answers.count) ? @"," : @"";
            [concatenatedString appendString:[NSString stringWithFormat:@"%@%@", answer, separator]];
            counter++;
        }
    }
    
    return concatenatedString;
}

- (void)updateChoiceItemWithQuestionID:(NSString *)questionID andData:(NSDictionary *)data {
    NSManagedObjectContext *ctx = _workerContext;
    NSManagedObject *mo = [self getEntity:kChoiceItemEntity attribute:@"question_id" parameter:questionID context:ctx];
    
    double order_number = 1;
    NSString *choice_image = @"";
    NSString *date_created = @"";
    NSString *date_modified = @"";
    NSString *is_correct = @"1";
    NSString *is_deleted = @"0";
    NSString *question_id = questionID;
    NSString *suggestive_feedback = @"";
    NSString *text = [self stringValue:data[@"text"]];
    
    NSDictionary *newData = @{@"order_number": @(order_number),
                              @"choice_image": choice_image,
                              @"date_created": date_created,
                              @"date_modified": date_modified,
                              @"is_correct": is_correct,
                              @"is_deleted": is_deleted,
                              @"question_id": question_id,
                              @"suggestive_feedback": suggestive_feedback,
                              @"text": text};
    
    if (mo != nil) {
        NSArray *keys = [newData allKeys];
        
        for (NSString *k in keys) {
            [mo setValue:newData[k] forKey:k];
        }
        
        NSManagedObject *question = [self getEntity:kQuestionEntity attribute:@"id" parameter:questionID context:ctx];
        [question setValue:[NSSet setWithObject:mo] forKey:@"choices"];
        
        [self saveTreeContext:ctx];
        [self validateCompletionOfQuestionWithChoiceObject:mo];
    }
}

- (void)validateCompletionOfQuestionWithChoiceObject:(NSManagedObject *)choice {
    if (choice != nil) {
        NSString *choice_text = [self stringValue:[choice valueForKey:@"text"]];
        NSString *question_id = [self stringValue:[choice valueForKey:@"question_id"]];
        NSPredicate *predicate = [self predicateForKeyPath:@"id" object:question_id];
        NSManagedObject *question = [self getEntity:kQuestionEntity predicate:predicate];
        
        if (question != nil) {
            NSString *question_title = [self stringValue: [question valueForKey:@"name"]];
            NSString *question_description = [self stringValue: [question valueForKey:@"question_text"]];
            NSString *question_points = [self stringValue: [question valueForKey:@"points"]];
            NSString *proficiency_level_id = [self stringValue: [question valueForKey:@"proficiency_level_id"]];
            NSString *learning_skills_id = [self stringValue: [question valueForKey:@"learning_skills_id"]];
            
            BOOL isComplete = NO;
            
            if ((question_title.length > 0) &&
                (question_description.length > 0) &&
                (question_points.length > 0) &&
                (proficiency_level_id.length > 0) &&
                (learning_skills_id.length > 0) &&
                (choice_text.length > 0)) {
                isComplete = YES;
            }
            
            // FILL IN THE BLANKS ONLY
            NSString *question_type_id = [self stringValue: [question valueForKey:@"question_type_id"]];
            if ([question_type_id isEqualToString:@"2"]) {
                if (![question_description containsString:@"-blank-"]) {
                    isComplete = NO;
                }
            }
            
            NSString *flag = [NSString stringWithFormat:@"%@", @(isComplete)];
            [question setValue:flag forKey:@"complete"];
            [self saveTreeContext:_workerContext];
            
            NSNotificationCenter *notification = [NSNotificationCenter defaultCenter];
            [notification postNotificationName:kNotificationQuestionUpdate object:flag];
        }
    }
    else {
        NSString *flag = [NSString stringWithFormat:@"%@", @(NO)];
        NSNotificationCenter *notification = [NSNotificationCenter defaultCenter];
        [notification postNotificationName:kNotificationQuestionUpdate object:flag];
    }
}

@end
