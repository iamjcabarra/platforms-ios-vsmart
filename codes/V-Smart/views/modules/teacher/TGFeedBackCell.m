//
//  TGFeedBackCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 08/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TGFeedBackCell.h"
#import "TGFeedBackItem.h"
#import "HMSegmentedControl.h"
#import "TestGuruDataManager.h"
#import "Masonry.h"
#import "MainHeader.h"

@interface TGFeedBackCell()

<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,
NSFetchedResultsControllerDelegate, UITextViewDelegate, TGFeedBackItemDelegate> {
    NSMutableDictionary *_objectChanges;
    NSMutableDictionary *_sectionChanges;
}

@property (strong, nonatomic) HMSegmentedControl *segmented;

@property (strong, nonatomic) IBOutlet UIView *segmentedContainer;
@property (strong, nonatomic) IBOutlet UIView *controlContainer;
@property (strong, nonatomic) IBOutlet UICollectionView *collection;
@property (strong, nonatomic) IBOutlet UILabel *typeLabel;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) TestGuruDataManager *tm;

@end

@implementation TGFeedBackCell

static NSString *kCellIdentifier = @"tg_feedback_cell_identifier";

- (void)awakeFromNib {
    // Initialization code
    self.tm = [TestGuruDataManager sharedInstance];
    self.typeLabel.text = NSLocalizedString(@"Feedback Messages", nil);
    [self applyShadowToView:self.controlContainer];
    [self setupSegmentedControls];
}

- (void)applyShadowToView:(UIView *)object {
    
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.5f;
    
//    layer.borderColor = [[UIColor redColor] CGColor];
//    layer.borderWidth = 0.5f;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // RESIZE ITEM SIZE AND INVALIDATE FLOWLAYOUT
    [self resizeContents];
}

- (void)setupSegmentedControls {
    
    CGRect controlFrame = self.segmentedContainer.frame;
    
    NSString *generalLabel = NSLocalizedString(@"General", nil);
    NSString *correctLabel = NSLocalizedString(@"Correct Answer", nil);
    NSString *wrongLabel = NSLocalizedString(@"Wrong Answer", nil);
    
    HMSegmentedControl *sc = [[HMSegmentedControl alloc] initWithFrame:controlFrame];
    sc.sectionTitles = @[generalLabel, correctLabel, wrongLabel];
    sc.userDraggable = YES;
    sc.selectedSegmentIndex = 0;
    sc.selectionIndicatorHeight = 0.0f;
    
    UIColor *bg = [UIColor lightGrayColor];
//    UIColor *bg = UIColorFromHex(0x0080FF);
    
    sc.backgroundColor = bg;

    UIColor *fontColor = UIColorFromHex(0x3498db);
    sc.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor],
                                NSFontAttributeName : FONT_NEUE(19) };

    sc.selectedTitleTextAttributes = @{ NSForegroundColorAttributeName : fontColor };
    sc.selectionIndicatorColor = [UIColor whiteColor];
    sc.selectionStyle = HMSegmentedControlSelectionStyleBox;
    sc.selectionIndicatorBoxOpacity = 1.0f;
    sc.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationUp;
    self.segmented = sc;
    
    [self.segmentedContainer setBackgroundColor:bg];
    [self.segmentedContainer addSubview:self.segmented];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(2, 2, 0, 2);
    [self.segmented mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.segmentedContainer).with.insets(padding);
    }];
    
    __weak typeof(self) wo = self;
    [self.segmented setIndexChangeBlock:^(NSInteger index) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo scollTextViewAtIndexPath:indexPath];
        });
    }];
}

- (void)scollTextViewAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.collection scrollToItemAtIndexPath:indexPath
                            atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                    animated:YES];
    [self resizeContents];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell indexPath:indexPath];
    return cell;
}

- (void)configureCell:(UICollectionViewCell *)c indexPath:(NSIndexPath *)indexPath {
    
    TGFeedBackItem *cell = (TGFeedBackItem *)c;
    cell.indexObject = indexPath;
    cell.delegate = self;
    
    NSInteger row = indexPath.row;
    
    if (row == 0) {
        cell.textView.text = self.generalFeedback;
        cell.feedbackPlaceholder.text = NSLocalizedString(@"Type your general feedback...", nil);
        if (cell.textView.text.length > 0 ) {
            cell.feedbackPlaceholder.hidden = YES;
        }
    }
    
    if (row == 1) {
        cell.textView.text = self.correctAnswerFeedback;
        cell.feedbackPlaceholder.text = NSLocalizedString(@"Type your correct answer feedback...", nil);
        if (cell.textView.text.length > 0) {
            cell.feedbackPlaceholder.hidden = YES;
        }
    }
    
    if (row == 2) {
        cell.textView.text = self.wrongAnswerFeedback;
        cell.feedbackPlaceholder.text = NSLocalizedString(@"Type your wrong answer feedback...", nil);
        if (cell.textView.text.length > 0) {
            cell.feedbackPlaceholder.hidden = YES;
        }
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = collectionView.frame.size.width;
    CGFloat height = collectionView.frame.size.height;
    
    return CGSizeMake(width, height);
}

- (void)resizeContents {
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collection.collectionViewLayout;

    CGFloat width = self.collection.bounds.size.width;
    CGFloat height = self.collection.bounds.size.height;
    
    layout.itemSize = CGSizeMake(width, height);
    [layout invalidateLayout];
}

#pragma mark - TGFeedBackItemDelegate

- (void)updatedText:(NSString *)text withIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger row = indexPath.row;
    
    if (row == 0) {
        self.generalFeedback = [NSString stringWithFormat:@"%@", text];
    }
    
    if (row == 1) {
        self.correctAnswerFeedback = [NSString stringWithFormat:@"%@", text];
    }
    
    if (row == 2) {
        self.wrongAnswerFeedback = [NSString stringWithFormat:@"%@", text];
    }
}

@end
