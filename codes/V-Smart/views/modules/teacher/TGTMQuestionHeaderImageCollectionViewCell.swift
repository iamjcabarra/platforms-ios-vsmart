//
//  TGTMQuestionHeaderImageCollectionViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 28/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class TGTMQuestionHeaderImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var questionImage: UIImageView!
    
}
