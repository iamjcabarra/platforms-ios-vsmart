//
//  TestGuruDynamicHeader.h
//  V-Smart
//
//  Created by Ryan Migallos on 23/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TestGuruDynamicHeaderDelegate <NSObject>
@required
- (void)didSelectButtonAction:(NSString *)actionType;
@optional
- (void)filterOptionCount:(NSUInteger)count;
@end

@interface TestGuruDynamicHeader : UIViewController

@property (weak, nonatomic) id <TestGuruDynamicHeaderDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *controlBar;

@end
