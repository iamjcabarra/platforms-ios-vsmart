//
//  TestAssignQuestionsView.m
//  V-Smart
//
//  Created by Julius Abarra on 28/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestAssignQuestionsView.h"
#import "TestAssignQuestionsCell.h"
#import "TestGuruDataManager.h"
#import "HUD.h"

@interface TestAssignQuestionsView () <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) TestGuruDataManager *tgdm;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) UIRefreshControl *tableRefreshControl;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UIButton *selectAllButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelSearchButton;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) IBOutlet UITextField *searchText;

@property (strong, nonatomic) NSString *kQuestionCellIdentifier;
@property (strong, nonatomic) NSString *userid;

@property (strong, nonatomic) NSMutableArray *preAssignedQuestions;
@property (strong, nonatomic) NSMutableArray *temSelectedQuestions;

@end

@implementation TestAssignQuestionsView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.userid = [NSString stringWithFormat:@"%d", account.user.id];
    
    // Use test guru data manager
    self.tgdm = [TestGuruDataManager sharedInstance];
    self.managedObjectContext = self.tgdm.mainContext;
    
    // Set protocols for table view
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Allow selections to table view
    self.tableView.allowsSelection = YES;
    self.tableView.allowsMultipleSelection = YES;
    
    self.tableView.rowHeight = 50.f;
    
    // Set cell identifier
    self.kQuestionCellIdentifier = @"questionCellIdentifier";
    
    // Create a selector for refresh action
    SEL refreshAction = @selector(listAllQuestions);
    
    // Implement refresh controller
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    // Search text action
    [self.searchText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.searchText.placeholder = [self localizeString:@"Search Question"]; // unchecked
    
    // Localize strings
    [self setUpStringLocalization];
    
    // Customize buttons
    [self customizeButtons];
    
    self.temSelectedQuestions = [NSMutableArray array];
    
    // Get ids of pre-assigned questions
    [self processPreAssignedQuestionIDList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // To avoid border cutting using grouped table view
    self.tableView.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Buttons 

- (void)customizeButtons {
    // Titles
    NSString *selectAllTitle = [self localizeString:@"Select All"];
    NSString *cancelTitle = [self localizeString:@"Cancel"];
    NSString *doneTitle = [self localizeString:@"Done"];
    
    [self.selectAllButton setTitle:selectAllTitle forState:UIControlStateNormal];
    [self.cancelSearchButton setTitle:cancelTitle forState:UIControlStateNormal];
    [self.doneButton setTitle:doneTitle forState:UIControlStateNormal];
    
    // Actions
    [self.closeButton addTarget:self action:@selector(closeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.selectAllButton addTarget:self action:@selector(selectAllButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelSearchButton addTarget:self action:@selector(cancelSearchButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.doneButton addTarget:self action:@selector(doneButtonAction:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Questions Tracking Helpers

- (void)processPreAssignedQuestionIDList {
    self.preAssignedQuestions = [NSMutableArray array];

    for (NSManagedObject *mo in self.assignedQuestions) {
        NSString *question_id = [mo valueForKey:@"question_id"];
        [self.preAssignedQuestions addObject:question_id];
    }
}

- (void)updateDoneButtonState {
    if (self.temSelectedQuestions.count > 0) {
        self.doneButton.enabled = YES;
        [self.doneButton setBackgroundColor:UIColorFromHex(0x6BBE65)];
    }
    else {
        self.doneButton.enabled = NO;
        [self.doneButton setBackgroundColor:[UIColor lightGrayColor]];
    }
}

- (BOOL)isQuestionAssigned:(id)object {
    NSSet *set = [NSSet setWithArray:self.preAssignedQuestions];
    
    if ([set containsObject:object]) {
        return YES;
    }
    
    return NO;
}

- (BOOL)isQuestionSelected:(id)object {
    NSSet *set = [NSSet setWithArray:self.temSelectedQuestions];
    
    if ([set containsObject:object]) {
        return YES;
    }
    
    return NO;
}

#pragma mark - List All Questions

- (void)listAllQuestions {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", [self localizeString:@"Loading"]];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    __weak typeof(self) wo = self;
    [self.tgdm requestAssignQuestionListForUser:self.userid enableChoice:YES doneBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo.tableRefreshControl endRefreshing];
                [wo reloadFetchedResultsController];
                [wo.tableView reloadData];
                [HUD hideUIBlockingIndicator];
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [wo.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            });
        }
    }];
}

#pragma mark - String Localization

- (void)setUpStringLocalization {
    NSString *title = [self localizeString:@"Assign Questions"];
    NSString *selectAllString = [self localizeString:@"Select All"];
    NSString *cancelSearchString = [self localizeString:@"Cancel"];
    NSString *doneString = [self localizeString:@"Done"];
    
    self.titleLabel.text = title;
    [self.selectAllButton setTitle:selectAllString forState:UIControlStateNormal];
    [self.selectAllButton setTitle:selectAllString forState:UIControlStateHighlighted];
    [self.cancelSearchButton setTitle:cancelSearchString forState:UIControlStateNormal];
    [self.cancelSearchButton setTitle:cancelSearchString forState:UIControlStateHighlighted];
    [self.doneButton setTitle:doneString forState:UIControlStateNormal];
    [self.doneButton setTitle:doneString forState:UIControlStateHighlighted];
}

- (NSString *)localizeString:(NSString *)string {
    NSString *localized = NSLocalizedString(string, nil);
    return localized;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TestAssignQuestionsCell *questionCell = [tableView dequeueReusableCellWithIdentifier:self.kQuestionCellIdentifier forIndexPath:indexPath];
    questionCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self configureCell:questionCell atIndexPath:indexPath];
    return questionCell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TestAssignQuestionsCell *questionCell = (TestAssignQuestionsCell *)cell;
    [self configureCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(TestAssignQuestionsCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isAssigned = [self isQuestionAssigned:[NSString stringWithFormat:@"%@", [mo valueForKey:@"id"]]];
    
    if (!isAssigned) {
        NSString *questionName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"name"]];
        NSString *questionType = [NSString stringWithFormat:@"%@", [mo valueForKey:@"questionTypeName"]];
        NSString *questionPoints = [NSString stringWithFormat:@"%.2f", [[mo valueForKey:@"points"] floatValue]];
        
        cell.questionNameLabel.text = questionName;
        cell.questionTypeLabel.text = questionType;
        cell.questionPointsLabel.text = questionPoints;
        
        NSString *pointTitle = NSLocalizedString(@"Point", nil);
        
        if ([questionPoints floatValue] > 1.0) {
            pointTitle = NSLocalizedString(@"Points", nil);
        }
        
        cell.questionPointsTitleLabel.text = pointTitle;
        
        BOOL isSelected = [self isQuestionSelected:mo];
        UIImage *buttonImage = [UIImage imageNamed:@"check_icn48.png"];
        
        if (isSelected) {
            buttonImage = [UIImage imageNamed:@"check_icn_active48.png"];
        }

        [cell.checkBoxButton setImage:buttonImage forState:UIControlStateNormal];
    }
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TestAssignQuestionsCell *questionCell = (TestAssignQuestionsCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self.temSelectedQuestions addObject:mo];
    [self updateDoneButtonState];
    
    UIImage *buttonImage = [UIImage imageNamed:@"check_icn_active48.png"];
    [questionCell.checkBoxButton setImage:buttonImage forState:UIControlStateNormal];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    TestAssignQuestionsCell *questionCell = (TestAssignQuestionsCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self.temSelectedQuestions removeObject:mo];
    [self updateDoneButtonState];
    
    UIImage *buttonImage = [UIImage imageNamed:@"check_icn48.png"];
    [questionCell.checkBoxButton setImage:buttonImage forState:UIControlStateNormal];
}

#pragma mark - Actions for Buttons

- (void)closeButtonAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)cancelSearchButtonAction:(id)sender {
    self.searchText.text = @"";
    [self reloadFetchedResultsController];
    [self.tableView reloadData];
}

- (void)selectAllButtonAction:(id)sender {
    
}

- (void)doneButtonAction:(id)sender {
    [self.delegate selectedQuestions:self.temSelectedQuestions];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)textFieldDidChange:(id)sender {
    [self reloadFetchedResultsController];
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionBankEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSCompoundPredicate *cp;
    
    NSPredicate *predicate1 = [self predicateForKeyPath:@"is_public" value:@"1"];
    NSPredicate *predicate2 = [self predicateForKeyPath:@"user_id" value:self.userid];
    NSPredicate *predicate3 = [self predicateForKeyPathContains:@"search_string" value:self.searchText.text];
    NSPredicate *predicate4 = [self predicateForKeyPath:@"question_type_id" value:@"3"]; // Just temporary
    
    if ([self.searchText.text isEqualToString:@""]) {
        cp = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2, predicate4]];
    }
    else {
        cp = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2, predicate3, predicate4]];
    }
    
    [fetchRequest setPredicate:cp];
    
    NSSortDescriptor *dateModified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:YES];
    [fetchRequest setSortDescriptors:@[dateModified]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - Reloading of Results

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

@end
