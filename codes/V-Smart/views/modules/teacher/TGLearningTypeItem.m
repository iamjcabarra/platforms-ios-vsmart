//
//  TGLearningTypeItem.m
//  V-Smart
//
//  Created by Ryan Migallos on 08/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TGLearningTypeItem.h"
#import "MainHeader.h"

typedef NS_ENUM(NSInteger, TGOptionTypeStyle) {
    TGOptionTypeStyleValue1,
    TGOptionTypeStyleValue2,
    TGOptionTypeStyleValue3,
    TGOptionTypeStyleDefault,
};

@interface TGLearningTypeItem()

@property (strong, nonatomic) IBOutlet UIView *customBackgroundView;

@end

@implementation TGLearningTypeItem

- (void)awakeFromNib {
    // Initialization code
    [self showHighlight:NO];
}

- (void)showHighlight:(BOOL)enable {
    
    UIColor *labelBackgroundColor = UIColorFromHex(0xDFEAF2);
    UIColor *labelTextColor = UIColorFromHex(0x0080FF);
    UIColor *typeBackgroundColor = UIColorFromHex(0x0080FF);
    
    if (enable) {
        
        TGOptionTypeStyle optionStyle = [self typeForTitle:self.titleLabel.text];
        UIColor *selectedColor = [self colorForType:optionStyle];
        
        labelTextColor = [UIColor whiteColor];
        labelBackgroundColor = selectedColor;
        typeBackgroundColor = selectedColor;
    }
    
    self.titleLabel.textColor = labelTextColor;
    self.titleLabel.backgroundColor = labelBackgroundColor;
    self.customBackgroundView.backgroundColor = typeBackgroundColor;
    
    //FORCE TO REDRAW
    [self setNeedsDisplay];
}

- (UIColor *)colorForType:(TGOptionTypeStyle)option {
    
    UIColor *selectedColor = UIColorFromHex(0x0080FF);
    
    UIColor *easyColor = UIColorFromHex(0x68BF61);
    UIColor *moderateColor = UIColorFromHex(0xFFCD02);
    UIColor *difficultColor = UIColorFromHex(0xF8931F);
    
    if (option == TGOptionTypeStyleValue1) {
        selectedColor = easyColor;
    }
    
    if (option == TGOptionTypeStyleValue2) {
        selectedColor = moderateColor;
    }
    
    if (option == TGOptionTypeStyleValue3) {
        selectedColor = difficultColor;
    }
    
    return selectedColor;
}

- (TGOptionTypeStyle)typeForTitle:(NSString *)title_string {
    
    title_string = [title_string lowercaseString];
    
    TGOptionTypeStyle option_style = TGOptionTypeStyleDefault;
    
    if ([title_string isEqualToString:@"easy"]) {
        option_style = TGOptionTypeStyleValue1;
    }
    
    if ([title_string isEqualToString:@"moderate"]) {
        option_style = TGOptionTypeStyleValue2;
    }
    
    if ([title_string isEqualToString:@"difficult"]) {
        option_style = TGOptionTypeStyleValue3;
    }
    
    return option_style;
}

@end
