//
//  TGTMQuestionContentWithoutChoiceTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 27/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class TGTMQuestionContentWithoutChoiceTableViewCell: UITableViewCell {
    
    @IBOutlet var questionAnswerTextView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
