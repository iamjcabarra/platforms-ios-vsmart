//
//  TBSectionItemCell.m
//  V-Smart
//
//  Created by Julius Abarra on 12/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TBSectionItemCell.h"

@implementation TBSectionItemCell

- (void)awakeFromNib {
    self.sectionStateImage.image = [UIImage imageNamed:@"vibe_check_box_blank_75px.png"];
}

- (void)prepareForReuse {
    [self showSelection:NO];
    [super prepareForReuse];
}

- (void)showSelection:(BOOL)selection {
    UIImage *selected = [UIImage imageNamed:@"vibe_check_box_75px.png"];
    UIImage *unselected = [UIImage imageNamed:@"vibe_check_box_blank_75px.png"];
    
    self.sectionStateImage.image = selection ? selected : unselected;
}

@end
