//
//  TGQBChoicesView.m
//  V-Smart
//
//  Created by Ryan Migallos on 15/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGQBChoicesView.h"
#import "TGGenericChoiceItem.h"
#import "TestGuruDataManager.h"
#import <CoreGraphics/CoreGraphics.h>
#import "UIImageView+WebCache.h"
#import "MainHeader.h"

@interface TGQBChoicesView() <NSFetchedResultsControllerDelegate, TGGenericChoiceItemDelegate>

@property (strong, nonatomic) IBOutlet UILabel *typeLabel;
@property (strong, nonatomic) IBOutlet UITableView *customTable;
@property (strong, nonatomic) UIButton *addButton;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) NSString *question_id;
@property (strong, nonatomic) NSString *question_type_id;
@property (strong, nonatomic) NSManagedObject *mo;

@end

@implementation TGQBChoicesView

static NSString *kGenericIdentifier  = @"tg_generic_choice_identifier";

- (void)awakeFromNib {
    // Initialization code
    
    self.tm = [TestGuruDataManager sharedInstance];
    
    NSString *choices_title = NSLocalizedString(@"Choices", nil);
    self.typeLabel.text = [choices_title uppercaseString];
    
    // Choice
    UINib *genericNib = [UINib nibWithNibName:@"TGGenericChoiceItem" bundle:nil];
    [self.customTable registerNib:genericNib forCellReuseIdentifier:kGenericIdentifier];
}

- (void)tableScrollEnable:(BOOL)enable {
    
    self.customTable.scrollEnabled = enable;
    self.customTable.alwaysBounceVertical = enable;
}

- (void)setupTableViewComponent:(NSManagedObject *)object {
    
    //    NSString *question_type_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"question_type_id"] ];
    self.question_type_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"question_type_id"] ];
    
    //TRUE OR FALSE
    if ([self.question_type_id isEqualToString:@"1"]) {
        [self tableScrollEnable:NO];
    }
    
    //MULTIPLE CHOICE
    if ([self.question_type_id isEqualToString:@"3"]) {
        [self tableScrollEnable:YES];
        [self setupAddButtonComponent];
    }
    
    [self.customTable reloadData]; // fix-06102016
}

- (void)setupAddButtonComponent {
    
    self.addButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.addButton.frame = CGRectMake(0, 0, self.customTable.frame.size.width, 44);
    self.addButton.frame = CGRectMake(7, 0, self.customTable.frame.size.width, 30);
    self.addButton.backgroundColor = UIColorFromHex(0x0080FF);
    
//    CALayer *layer = self.addButton.layer;
//    layer.cornerRadius = 5;
    
    //    NSString *string = NSLocalizedString(@"Add", nil);
    NSString *string = [NSString stringWithFormat:@"+ %@", NSLocalizedString(@"Add Choice", nil)];
    [self.addButton setTitle:string forState:UIControlStateNormal];
    [self.addButton setTitle:string forState:UIControlStateHighlighted];
    [self.addButton setTitle:string forState:UIControlStateSelected];
    
    [self.addButton addTarget:self action:@selector(addChoiceItemAction:) forControlEvents:UIControlEventTouchUpInside];
    self.customTable.tableFooterView = self.addButton;
    
    //[self.customTable reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setObjectData:(NSManagedObject *)object {
    
    if (object != nil) {
        self.mo = object;
        self.question_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"id"] ];
        _fetchedResultsController = nil;
        NSError * error = nil;
        BOOL state = [self.fetchedResultsController performFetch:&error];
        if (state) {
            [self setupTableViewComponent:object];
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.customTable = tableView;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kGenericIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

//- (void)configureCell:(UITableViewCell *)object atIndexPath:(NSIndexPath *)indexPath {
//
//    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//
//    TGGenericChoiceItem *cell = (TGGenericChoiceItem *)object;
//    cell.contentView.backgroundColor = self.customTable.backgroundColor;
//
//    NSString *choice_text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"text"] ];
//    NSString *is_correct = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_correct"] ];
//
//    cell.checkImageView.image = [self checkImageForString:is_correct];
//    cell.deleteButton.hidden = self.isTrashHidden;
//
//    NSString *url_str = [NSString stringWithFormat:@"%@", [mo valueForKey:@"choice_image"] ];
//    NSURL *url = [NSURL URLWithString:url_str];
//
//    cell.delegate = self;
//    cell.customTextField.text = choice_text;
//    [cell.uploadImageView sd_setImageWithURL:url];
//
//    cell.mo = mo;
//}

- (void)configureCell:(UITableViewCell *)object atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    TGGenericChoiceItem *cell = (TGGenericChoiceItem *)object;
    cell.contentView.backgroundColor = self.customTable.backgroundColor;
    
    NSString *choice_text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"text"] ];
    NSString *is_correct = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_correct"] ];
    NSData *choice_image_data = [NSData dataWithData:[mo valueForKey:@"choice_image_data"] ];
    
    cell.checkImageView.image = [self checkImageForString:is_correct];
    cell.deleteButton.hidden = self.isTrashHidden;
    
    NSString *url_str = [NSString stringWithFormat:@"%@", [mo valueForKey:@"choice_image"] ];
    NSURL *url = [NSURL URLWithString:url_str];
    
    cell.delegate = self;
    
    cell.customTextField.text = choice_text;
    cell.customTextField.layer.borderColor = UIColorFromHex(0x3498DB).CGColor;
    cell.customTextField.layer.borderWidth = 2.0;
    
    if ([self isNotEmpty:url_str]) {
        if ([[url scheme] isEqualToString:@"assets-library"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.uploadImageView.image = [UIImage imageWithData:choice_image_data];
            });
        } else {
            [cell.uploadImageView sd_setImageWithURL:url];
        }
//        cell.removeChoiceImageButton.hidden = NO;
        
        // JCA-02-17-2017
        // VIT#2564
        // Disable remove choice image for True or False type of question
        cell.removeChoiceImageButton.hidden = [self.question_type_id isEqualToString:@"1"] ? YES : NO;
        
    } else {
        cell.removeChoiceImageButton.hidden = YES;
        cell.uploadImageView.image = [UIImage imageNamed:@"icn_no_image.png"];
    }
    
    cell.mo = mo;
    
    // JCA-02-17-2017
    // VIT#2564
    // Disable upload choice image for True or False type of question
    cell.uploadImageView.hidden = [self.question_type_id isEqualToString:@"1"] ? YES : NO;
    cell.uploadButton.hidden = [self.question_type_id isEqualToString:@"1"] ? YES : NO;
}

- (BOOL)isNotEmpty:(NSString *)string {
    BOOL isNotEmpty = NO;
    
    string = [string stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"<null>" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"null" withString:@""];
    
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceCharacterSet];
    string = [string stringByTrimmingCharactersInSet:whiteSpace];
    
    if (string.length > 0) {
        isNotEmpty = YES;
    }
    
    return isNotEmpty;
}

- (UIImage *)checkImageForString:(NSString *)string {
    
    BOOL flag = [self isCorrectSelection:string];
//    NSString *imageName = (flag) ? @"check_icn_active150" : @"check_icn150";
    NSString *imageName = (flag) ? @"check_icn_active48.png" : @"check_icn48";
    return [UIImage imageNamed:imageName];
}

- (BOOL)isCorrectSelection:(NSString *)value {
    return [value isEqualToString:@"100"];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    BOOL isAscending = YES;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kChoiceItemEntity];
    
    NSPredicate *p1 = [NSPredicate predicateWithFormat:@"question = %@", self.mo];
    NSPredicate *p2 = [self.tm predicateForKeyPath:@"question_id" andValue:self.question_id];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[p1,p2]];
    [fetchRequest setPredicate:predicate];
    
    
    //TODO
    //    [fetchRequest setResultType:NSDictionaryResultType];
    //    [fetchRequest setReturnsDistinctResults:YES];
    //    [fetchRequest setPropertiesToFetch:fetchRequest.entity.properties];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *order_number = [NSSortDescriptor sortDescriptorWithKey:@"order_number" ascending:isAscending];
    [fetchRequest setSortDescriptors:@[order_number]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.customTable beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    UITableView *tableView = self.customTable;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.customTable;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.customTable endUpdates];
}

- (NSPredicate *)predicateForQuestionObject:(NSManagedObject *)object {
    
    NSString *choice_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"id"] ];
    NSString *order_number = [NSString stringWithFormat:@"%@", [object valueForKey:@"order_number"] ];
    NSString *questionID = [NSString stringWithFormat:@"%@", [object valueForKey:@"question_id"] ];
    
    NSPredicate *p1 = [self.tm predicateForKeyPath:@"id" andValue:choice_id];
    NSPredicate *p2 = [self.tm predicateForKeyPath:@"order_number" andValue:order_number];
    NSPredicate *p3 = [self.tm predicateForKeyPath:@"question_id" andValue:questionID];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[p1,p2,p3]];
    
    return predicate;
}

/////////////////////////////////////////////////////////////////////////////
// ------------------------- Generic Choice Item ------------------------- //
/////////////////////////////////////////////////////////////////////////////

#pragma mark - TGGenericChoiceItemDelegate

- (void)updatedText:(NSString *)text withManagedObject:(NSManagedObject *)object {
    
    NSPredicate *predicate = [self predicateForQuestionObject:object];
    [self.tm updateQuestionChoiceText:text withPredicate:predicate];
}

- (void)didPressChoiceAction:(TGGenericChoiceAction)action withManagedObject:(NSManagedObject *)object {
    
    if (action == TGGenericChoiceCheck) {
        
        NSPredicate *predicate = [self predicateForQuestionObject:object];
        NSLog(@"predicate object : %@", predicate);
        NSString *is_correct = [NSString stringWithFormat:@"%@", [object valueForKey:@"is_correct"] ];
        BOOL flag = [self isCorrectSelection:is_correct];
        
        //TRUE OR FALSE
        if ([self.question_type_id isEqualToString:@"1"]) {
            if (flag == NO) {
                NSDictionary *userData = @{@"is_correct":@"100"};
                NSString *questionid = [NSString stringWithFormat:@"%@", [object valueForKey:@"question_id"] ];
                [self.tm updateChoiceItemForQuestion:questionid updateDetails:userData predicate:predicate];
            }
        }
        
        //MULTIPLE CHOICE
        if ([self.question_type_id isEqualToString:@"3"]) {
            is_correct = (flag) ? @"0" : @"100";
            NSDictionary *userData = @{@"is_correct":is_correct};
            [self.tm updateEntity:kChoiceItemEntity details:userData predicate:predicate];
        }
        
    }
    
    if (action == TGGenericChoiceDelete) {
        [self deleteChoiceItem:object];
    }
}

- (void)didPerformChoiceImageAction:(TGGenericChoiceAction)action withImageData:(NSData *)imageData withStringURL:(NSString *)stringUrl withManagedObject:(NSManagedObject *)object {
    
    if (action == TGGenericChoiceUpload) {
        NSString *choice_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"id"] ];
        NSString *question_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"question_id"] ];
        NSPredicate *p1 = [self.tm predicateForKeyPath:@"id" andValue:choice_id];
        NSPredicate *p2 = [self.tm predicateForKeyPath:@"question_id" andValue:question_id];
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[p1,p2]];
        NSMutableDictionary *userData = [NSMutableDictionary dictionary];
        
        if (imageData == nil) {
            [userData setValue:@"" forKey:@"choice_image"];
        } else {
            [userData setValue:stringUrl forKey:@"choice_image"];
            [userData setValue:imageData forKey:@"choice_image_data"];
        }
        
        [self.tm updateQuestion:kChoiceItemEntity predicate:predicate details:userData];
    }
}

/////////////////////////////////////////////////////////////////////////////
// ------------------------- Generic Choice Item ------------------------- //
/////////////////////////////////////////////////////////////////////////////

- (void)addChoiceItemAction:(UIButton *)sender {
    __weak typeof(self) wo = self;
    [self.tm insertChoiceItemWithQuestionID:self.question_id doneBlock:^(BOOL status) {
        
        NSArray *choices = wo.fetchedResultsController.fetchedObjects;
        if (choices.count >= 3) {
            wo.isTrashHidden = NO;
        }
        [self.tm saveQuestionChoices:choices questionid:self.question_id doneBlock:^(BOOL status) {
            if (status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSArray *paths = [wo.customTable indexPathsForVisibleRows];
                    [wo.customTable reloadRowsAtIndexPaths:paths
                                          withRowAnimation:UITableViewRowAnimationFade];
                    [wo notifyParentViewForTableLayoutForInsertOperation:YES];
                });
            }
        }];
    }];
}

- (void)deleteChoiceItem:(NSManagedObject *)object {
    
    __weak typeof(self) wo = self;
    [self.tm deleteChoiceItem:object doneBlock:^(BOOL status) {
        
        NSArray *choices = [NSArray arrayWithArray:wo.fetchedResultsController.fetchedObjects];
        if (choices.count == 2) {
            wo.isTrashHidden = YES;
        }
        [self.tm saveQuestionChoices:choices questionid:self.question_id doneBlock:^(BOOL status) {
            if (status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSArray *paths = [wo.customTable indexPathsForVisibleRows];
                    [wo.customTable reloadRowsAtIndexPaths:paths
                                          withRowAnimation:UITableViewRowAnimationFade];
                    [wo notifyParentViewForTableLayoutForInsertOperation:NO];
                });
            }
        }];
    }];
}

- (void)notifyParentViewForTableLayoutForInsertOperation:(BOOL)flag {
    
    CGFloat table_height = [self calculateTableHeightForInsert:flag];
    if ([(NSObject *)self.delegate respondsToSelector:@selector(expandTableWithHeight:)]) {
        [self.delegate expandTableWithHeight:table_height];
    }
}

- (CGFloat)calculateTableHeightForInsert:(BOOL)flag {
    
    CGFloat height = self.customTable.contentSize.height;
    
    //INSERT OPERATION PADDING => 90.0
    //DELETE OPERATION PADDING => 0.0
    
    CGFloat padding = (flag) ? 90.0 : 0.0;//adjust for custom padding
    CGFloat table_height = height + padding;
    
    return table_height;
}

@end
