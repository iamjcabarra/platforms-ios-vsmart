//
//  TBTestFilterOptionView.h
//  V-Smart
//
//  Created by Julius Abarra on 02/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

@protocol TBTestFilterOptionViewDelegate <NSObject>

@required
- (void)selectedTestFilterOption:(NSDictionary *)optionDetail;

@end

#import <UIKit/UIKit.h>

@interface TBTestFilterOptionView : UIViewController

@property (weak, nonatomic) id <TBTestFilterOptionViewDelegate> delegate;

@end
