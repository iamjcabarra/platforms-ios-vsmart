//
//  TGTMStatusTableViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 23/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol TGTMStatusTableViewControllerDelegate: class {
    func selectedStatusObject(_ object: [String: String])
}

class TGTMStatusTableViewController: UITableViewController {
    
    weak var delegate: TGTMStatusTableViewControllerDelegate?
    fileprivate var statusList: [[String: String]] = []
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let statusA = NSLocalizedString("All Status", comment: "")
        let statusB = NSLocalizedString("Pending", comment: "")
        let statusC = NSLocalizedString("Approved", comment: "")
        let statusD = NSLocalizedString("Disapproved", comment: "")
        
        self.statusList = [["id": "", "name": statusA], ["id": "0", "name": statusB], ["id": "1", "name": statusC], ["id": "2", "name": statusD]]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table View Data Source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.statusList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        let status = self.statusList[(indexPath as NSIndexPath).row]
        let name = status["name"]
        cell.textLabel?.text = name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let status = self.statusList[(indexPath as NSIndexPath).row]
        let id: String! = status["id"]
        let name: String! = status["name"]
        let statusObject: [String: String] = ["id": id, "name": name]
        
        self.dismiss(animated: true, completion: {
            self.delegate?.selectedStatusObject(statusObject)
        })
    }

}
