//
//  QuestionSamplerView.h
//  V-Smart
//
//  Created by Ryan Migallos on 29/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuestionSamplerDelegate <NSObject>
@optional
- (void)didFinishSaveInformation:(BOOL)state;
- (void)didSelectEditIndex:(NSIndexPath *)indexPath withEditObject:(NSManagedObject *)object;
@end

@interface QuestionSamplerView : UIViewController

@property (weak, nonatomic) id <QuestionSamplerDelegate> delegate;
@property (strong, nonatomic) NSManagedObject *question;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (strong, nonatomic) NSString *entityObject;

@property (strong, nonatomic) NSArray *sortDescriptors;
@property (assign, nonatomic) BOOL isComplete;

@property (assign, nonatomic) BOOL isTestPreview;

- (void)closeButtonHidden;

@end
