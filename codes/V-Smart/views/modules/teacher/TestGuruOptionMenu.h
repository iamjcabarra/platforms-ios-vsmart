//
//  TestGuruOptionMenu.h
//  V-Smart
//
//  Created by Ryan Migallos on 8/10/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OptionMenuDelegate <NSObject>
@required
- (void)selectedOptionMenuForEntity:(NSString *)entity withObject:(NSManagedObject *)object;
@end

@interface TestGuruOptionMenu : UITableViewController
@property (nonatomic, strong) NSString *entity;
@property (nonatomic, weak) id <OptionMenuDelegate> delegate;
@end
