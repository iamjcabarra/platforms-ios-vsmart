//
//  TBSecondDetailViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 03/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBSecondDetailViewController : UIViewController

@property (strong, nonatomic) NSManagedObject *testObject;

@end
