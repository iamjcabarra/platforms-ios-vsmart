//
//  TestBankDeployController.m
//  V-Smart
//
//  Created by Ryan Migallos on 9/26/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestBankDeployController.h"
#import "TestGuruDataManager.h"
#import "AccountInfo.h"
#import "Utils.h"
#import "VSmartValues.h"

@interface TestBankDeployController () <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate> {
    
    NSManagedObject *mo_selected;
    UITableView *tableView_active;
}

@property (nonatomic, strong) TestGuruDataManager *tm;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UIButton *deployButton;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSString *user_id;

@property (nonatomic, strong) NSMutableSet *course_section_list;

@property (nonatomic, strong) NSArray *deployedTestSectionObjects;
@property (nonatomic, strong) NSMutableArray *deployedTestSections;

@end

@implementation TestBankDeployController

static NSString *kCellIdentifier = @"test_deploy_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    self.labelTitle.text = NSLocalizedString(@"Select Section", nil);
    [self setupButtons];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.allowsSelection = YES;
    self.tableView.allowsMultipleSelection = YES;
    
    self.tm = [TestGuruDataManager sharedInstance];
    [self.tm requestTestSectionListForUser:self.user_id doneBlock:^(BOOL status) {
        //do nothing
    }];
    
    // Access recently selected sections where test has been deployed from core data
    NSString *qid = [NSString stringWithFormat:@"%@", [self.quiz_mo valueForKey:@"id"]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"qid = %@", qid];
    self.deployedTestSectionObjects = [self.tm getObjectsForEntity:kDeployedTestEntity predicate:predicate context:self.tm.mainContext];
    
    if (self.deployedTestSectionObjects.count > 0) {
        self.deployedTestSections = [[NSMutableArray alloc] init];
        
        // Populate array with sections where test has been deployed
        for (NSManagedObject *mo in self.deployedTestSectionObjects) {
            NSString *section_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_id"]];
            [self.deployedTestSections addObject:section_id];
        }
    }
}

- (void)setupButtons {
    
    NSString *closeText = NSLocalizedString(@"Close", nil);
    [self.closeButton setTitle:closeText forState:UIControlStateNormal];
    [self.closeButton setTitle:closeText forState:UIControlStateHighlighted];
    
    NSString *deployText = NSLocalizedString(@"Deploy", nil);
    [self.deployButton setTitle:deployText forState:UIControlStateNormal];
    [self.deployButton setTitle:deployText forState:UIControlStateHighlighted];
    
    [self.closeButton addTarget:self action:@selector(closeActionButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.deployButton addTarget:self action:@selector(deployActionButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)closeActionButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)deployActionButton:(id)sender {

    self.course_section_list = [NSMutableSet set];
    NSArray *selection_list = [self.tableView indexPathsForSelectedRows];
    for (NSIndexPath *indexPath in selection_list) {
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSString *course_section_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"id"] ];
        NSDictionary *d = @{@"id":course_section_id};
        [self.course_section_list addObject:d];
    }
    
    if (self.quiz_mo) {
        __weak typeof(self) wo = self;
        NSString *test_name = [NSString stringWithFormat:@"%@", [self.quiz_mo valueForKey:@"name"]];
        NSString *qid = [NSString stringWithFormat:@"%@", [self.quiz_mo valueForKey:@"id"] ];
        NSArray *cs_ids = [NSArray arrayWithArray: [self.course_section_list allObjects] ];
        NSDictionary *body = @{@"qid":qid, @"cs_ids":cs_ids};
        [self.tm requestDeployTest:body doneBlock:^(BOOL status) {
            //do nothing
            if (status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString *boxTitle = NSLocalizedString(@"Deploy", nil);
                    AlertWithMessageAndDelegate(boxTitle, test_name, wo);
        
                    // Save deployed test and section where this test is deployed to core data
                    // NOTE: body contains with test id with course section ids only (1-M Relationship)
                    [wo.tm saveDeployedTestSectionToCoreData:body];
                });
            }
        }];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView_active = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];

    /*
     "course_id" = 5;
     "course_name" = "Reading 5";
     details = "<null>";
     "grade_level_id" = 6;
     "grade_level_name" = "Grade 5";
     id = 6;
     "section_id" = 5;
     "section_name" = "St. Martin";
     */
    
    BOOL hasSelected = NO;
    
    NSString *section_id = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"id"]];
    NSString *course_name = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"course_name"] ];
    NSString *section_name = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"section_name"] ];
    NSString *text = [NSString stringWithFormat:@"%@ %@", course_name, section_name];
    
    if (self.deployedTestSections.count > 0) {
        for (int i = 0; i < self.deployedTestSections.count; i++) {
            NSString *selectedSection = [NSString stringWithFormat:@"%@", self.deployedTestSections[i]];
            
            // Check if section has already been selected before for deployment
            if ([selectedSection isEqualToString:section_id]) {
                hasSelected = YES;
                break;
            }
        }
    }
    
    // If section has already been selected, then cell must be set to selected state
    if (hasSelected) {
        [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        
        // NOTE: Disable if deployed test retraction is allowed
        // If yes, additional api call implementation for retraction is needed
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.backgroundColor = [UIColor lightGrayColor];
    }
    
    cell.textLabel.text = text;
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *context = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTestSectionEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"course_name" ascending:YES];
    [fetchRequest setSortDescriptors:@[date_modified]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:context
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
