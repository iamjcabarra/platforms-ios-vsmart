//
//  TBTestOptionsSegmentView.m
//  TestBankVersion2.1
//
//  Created by Julius Abarra on 06/02/2016.
//  Copyright © 2016 Vibe Technologies, Inc. All rights reserved.
//

#import "TBTestOptionsSegmentView.h"
#import "TBTestOptionInfoView.h"
#import "TBClassHelper.h"
#import "TBContentManager.h"

@interface TBTestOptionsSegmentView () <UITextFieldDelegate>

@property (strong, nonatomic) TBClassHelper *classHelper;
@property (strong, nonatomic) TBContentManager *tbcm;

@property (strong, nonatomic) TBTestOptionInfoView *optionInfoView;
@property (strong, nonatomic) UIPopoverController *optionInfoPopOverController;

@property (strong, nonatomic) IBOutlet UILabel *shuffleChoicesLabel;
@property (strong, nonatomic) IBOutlet UILabel *shuffleQuestionsLabel;
@property (strong, nonatomic) IBOutlet UILabel *forcedSubmitLabel;
@property (strong, nonatomic) IBOutlet UILabel *showScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *showResultLabel;
@property (strong, nonatomic) IBOutlet UILabel *showResultWCALabel;
@property (strong, nonatomic) IBOutlet UILabel *showFeedbacksLabel;
@property (strong, nonatomic) IBOutlet UILabel *setPasswordLabel;

@property (strong, nonatomic) IBOutlet UISwitch *shuffleChoicesSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *shuffleQuestionsSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *forcedSubmitSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *showScoreSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *showResultSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *showResultWCASwitch;
@property (strong, nonatomic) IBOutlet UISwitch *showFeedbacksSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *setPasswordSwitch;

@property (strong, nonatomic) IBOutlet UIButton *shuffleChoicesInfoButton;
@property (strong, nonatomic) IBOutlet UIButton *shuffleQuestionsInfoButton;
@property (strong, nonatomic) IBOutlet UIButton *forcedSubmitInfoButton;
@property (strong, nonatomic) IBOutlet UIButton *showScoreInfoButton;
@property (strong, nonatomic) IBOutlet UIButton *showResultInfoButton;
@property (strong, nonatomic) IBOutlet UIButton *showResultWCAInfoButton;
@property (strong, nonatomic) IBOutlet UIButton *showFeedbacksInfoButton;
@property (strong, nonatomic) IBOutlet UIButton *setPasswordInfoButton;

@property (strong, nonatomic) IBOutlet UITextField *setPasswordTextField;

@property (assign, nonatomic) int actionType;

@end

@implementation TBTestOptionsSegmentView

#define kBorderWidth 1.0f
#define kRightBorderColor UIColorFromHex(0x3498DB).CGColor
#define kWrongBorderColor UIColorFromHex(0xFF6666).CGColor

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Content Manager
    self.tbcm = [TBContentManager sharedInstance];
    self.actionType = [self.tbcm.actionType intValue];
    
    // Customize UI Objects
    [self customizeLabels];
    [self customizeSwithes];
    [self customizeButtons];
    [self customizeTextFields];
    
    // Default Values
    [self setUpTestOptionsDefaultValues];
    
    // Invalidate Contents
    [self invalidateTestOptions];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom UI Objects

- (void)customizeLabels {
    // Localize String
    self.shuffleChoicesLabel.text = [self.classHelper localizeString:@"Shuffle Choices"];
    self.shuffleQuestionsLabel.text = [self.classHelper localizeString:@"Shuffle Questions"];
    self.forcedSubmitLabel.text = [self.classHelper localizeString:@"Forced Submit"];
    self.showScoreLabel.text = [self.classHelper localizeString:@"Show Score"];
    self.showResultLabel.text = [self.classHelper localizeString:@"Show Result"];
    self.showResultWCALabel.text = [self.classHelper localizeString:@"Show Result with Correct Answer"];
    self.showFeedbacksLabel.text = [self.classHelper localizeString:@"Show Feedback Messages"];
    self.setPasswordLabel.text = [self.classHelper localizeString:@"Set Password"];
}

- (void)customizeSwithes {
    // Switch Tag
    self.setPasswordSwitch.tag = 100;
    
    // Action for Switch
    [self.shuffleChoicesSwitch addTarget:self
                                  action:@selector(changeSwitchState:)
                        forControlEvents:UIControlEventValueChanged];
    
    [self.shuffleQuestionsSwitch addTarget:self
                                    action:@selector(changeSwitchState:)
                          forControlEvents:UIControlEventValueChanged];
    
    [self.forcedSubmitSwitch addTarget:self
                                action:@selector(changeSwitchState:)
                      forControlEvents:UIControlEventValueChanged];
    
    [self.showScoreSwitch addTarget:self
                             action:@selector(changeSwitchState:)
                   forControlEvents:UIControlEventValueChanged];
    
    [self.showResultSwitch addTarget:self
                              action:@selector(changeSwitchState:)
                    forControlEvents:UIControlEventValueChanged];
    
    [self.showResultWCASwitch addTarget:self
                                 action:@selector(changeSwitchState:)
                       forControlEvents:UIControlEventValueChanged];
    
    [self.showFeedbacksSwitch addTarget:self
                                 action:@selector(changeSwitchState:)
                       forControlEvents:UIControlEventValueChanged];
    
    [self.setPasswordSwitch addTarget:self
                               action:@selector(changeSwitchState:)
                     forControlEvents:UIControlEventValueChanged];
}

- (void)customizeButtons {
    // Button Tag
    self.shuffleChoicesInfoButton.tag = 100;
    self.shuffleQuestionsInfoButton.tag = 200;
    self.forcedSubmitInfoButton.tag = 300;
    self.showScoreInfoButton.tag = 400;
    self.showResultInfoButton.tag = 500;
    self.showResultWCAInfoButton.tag = 600;
    self.showFeedbacksInfoButton.tag = 700;
    self.setPasswordInfoButton.tag = 800;
    
    // Action for Button
    [self.shuffleChoicesInfoButton addTarget:self
                                      action:@selector(showPopOverOptionInfo:)
                            forControlEvents:UIControlEventTouchUpInside];
    
    [self.shuffleQuestionsInfoButton addTarget:self
                                        action:@selector(showPopOverOptionInfo:)
                              forControlEvents:UIControlEventTouchUpInside];
    
    [self.forcedSubmitInfoButton addTarget:self
                                    action:@selector(showPopOverOptionInfo:)
                          forControlEvents:UIControlEventTouchUpInside];
    
    [self.showScoreInfoButton addTarget:self
                                 action:@selector(showPopOverOptionInfo:)
                       forControlEvents:UIControlEventTouchUpInside];
    
    [self.showResultInfoButton addTarget:self
                                  action:@selector(showPopOverOptionInfo:)
                        forControlEvents:UIControlEventTouchUpInside];
    
    [self.showResultWCAInfoButton addTarget:self
                                     action:@selector(showPopOverOptionInfo:)
                           forControlEvents:UIControlEventTouchUpInside];
    
    [self.showFeedbacksInfoButton addTarget:self
                                     action:@selector(showPopOverOptionInfo:)
                           forControlEvents:UIControlEventTouchUpInside];
    
    [self.setPasswordInfoButton addTarget:self
                                   action:@selector(showPopOverOptionInfo:)
                         forControlEvents:UIControlEventTouchUpInside];
}

- (void)customizeTextFields {
    NSString *password = [self.classHelper localizeString:@"Enter password of this test here"];
    self.setPasswordTextField.placeholder = [NSString stringWithFormat:@"%@...", password];
    self.setPasswordTextField.delegate = self;
    
    [self.setPasswordTextField addTarget:self
                                  action:@selector(textFieldDidChange:)
                        forControlEvents:UIControlEventEditingChanged];
    
    self.setPasswordTextField.layer.borderWidth = kBorderWidth;
}

#pragma mark - Options Default Values

- (void)setUpTestOptionsDefaultValues {
    BOOL isTestOptionsNotEmpty = self.tbcm.testOptions.count > 0;
    
    if (isTestOptionsNotEmpty) {
        self.shuffleChoicesSwitch.on = [[self.tbcm.testOptions objectForKey:@"shuffle_choices"] boolValue];
        self.shuffleQuestionsSwitch.on = [[self.tbcm.testOptions objectForKey:@"shuffle_questions"] boolValue];
        self.forcedSubmitSwitch.on = [[self.tbcm.testOptions objectForKey:@"forced_submit"] boolValue];
        self.showScoreSwitch.on = [[self.tbcm.testOptions objectForKey:@"show_score"] boolValue];
        self.showResultSwitch.on = [[self.tbcm.testOptions objectForKey:@"show_result"] boolValue];
        self.showResultWCASwitch.on = [[self.tbcm.testOptions objectForKey:@"show_result_with_correct_answer"] boolValue];
        self.showFeedbacksSwitch.on = [[self.tbcm.testOptions objectForKey:@"show_feedback_messages"] boolValue];
        self.setPasswordSwitch.on = [[self.tbcm.testOptions objectForKey:@"set_password"] boolValue];
        
        if (self.setPasswordSwitch.on) {
            self.setPasswordTextField.hidden = NO;
            self.setPasswordTextField.text = [self.tbcm.testOptions objectForKey:@"password"];
        }
        else {
            self.setPasswordTextField.hidden = YES;
        }
        
        [self invalidateTestOptions];
    }
}

#pragma mark - Switch Action

- (void)changeSwitchState:(id)sender {
    UISwitch *theSwitch = (UISwitch *)sender;
    BOOL isOn = [theSwitch isOn];
    
    if (isOn) {
        if (theSwitch.tag == 100) {
            self.setPasswordTextField.hidden = NO;
        }
    }
    else {
        if (theSwitch.tag == 100) {
            self.setPasswordTextField.hidden = YES;
        }
    }
    
    [self updateContentManager];
}

#pragma mark - Button Action

- (void)showPopOverOptionInfo:(id)sender {
    UIButton *button = (UIButton *)sender;
    NSInteger tag = button.tag;
    
    // Create Option Info Controller
    self.optionInfoView = [[TBTestOptionInfoView alloc] initWithNibName:@"TBTestOptionInfoView" bundle:nil];
    
    // Create Popover Controller
    self.optionInfoPopOverController = [[UIPopoverController alloc] initWithContentViewController:self.optionInfoView];
    self.optionInfoPopOverController.popoverContentSize = CGSizeMake(600.0f, 100.0f);
    
    // Render Popover
    [self.optionInfoPopOverController presentPopoverFromRect:button.bounds
                                                      inView:button
                                    permittedArrowDirections:UIPopoverArrowDirectionDown
                                                    animated:YES];
    
    // Option Contents
    NSString *title = @"";
    NSString *body = @"";
    
    switch (tag) {
        case 100:
            title = [self.classHelper localizeString:@"Shuffle Choices"];
            body = @"";
            break;
        case 200:
            title = [self.classHelper localizeString:@"Shuffle Questions"];
            body = @"";
            break;
        case 300:
            title = [self.classHelper localizeString:@"Forced Submit"];
            body = @"";
            break;
        case 400:
            title = [self.classHelper localizeString:@"Show Score"];
            body = @"";
            break;
        case 500:
            title = [self.classHelper localizeString:@"Show Result"];
            body = @"";
            break;
        case 600:
            title = [self.classHelper localizeString:@"Show Result with Correct Answer"];
            body = @"";
            break;
        case 700:
            title = [self.classHelper localizeString:@"Show Feedback Messages"];
            body = @"";
            break;
        case 800:
            title = [self.classHelper localizeString:@"Set Password"];
            body = @"";
            break;
        default:
            break;
    }
    
    [self.optionInfoView setTestOptionInfoWithTitle:title andBody:body];
}

#pragma mark - Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self invalidateTestOptions];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // if backspace
    if ([string length] == 0) {
        return YES;
    }
    
    // limit to only up to maximum allowable number of characters
    if ([textField.text length] >= [kTestMaxPasswordChar intValue]) {
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidChange:(id)sender {
    [self invalidateTestOptions];
    [self updateContentManager];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self invalidateTestOptions];
    [self updateContentManager];
}

- (void)invalidateTestOptions {
    if ([self.setPasswordTextField.text isEqualToString:@""]) {
        self.setPasswordTextField.layer.borderColor = kWrongBorderColor;
    }
    else {
        self.setPasswordTextField.layer.borderColor = kRightBorderColor;
    }
}

#pragma mark - Update Content Manager

- (void)updateContentManager {
    NSString *shuffle_choices = (self.shuffleChoicesSwitch.on) ? @"1": @"0";
    NSString *shuffle_questions = (self.shuffleQuestionsSwitch.on) ? @"1": @"0";
    NSString *forced_submit = (self.forcedSubmitSwitch.on) ? @"1": @"0";
    NSString *show_score = (self.showScoreSwitch.on) ? @"1": @"0";
    NSString *show_result = (self.showResultSwitch.on) ? @"1": @"0";
    NSString *show_result_with_correct_answer = (self.showResultWCASwitch.on) ? @"1": @"0";
    NSString *show_feedback_messages = (self.showFeedbacksSwitch.on) ? @"1": @"0";
    NSString *set_password = (self.setPasswordSwitch.on) ? @"1": @"0";
    NSString *password = self.setPasswordTextField.text;
    
    [self.tbcm.testOptions setValue:shuffle_choices forKey:@"shuffle_choices"];
    [self.tbcm.testOptions setValue:shuffle_questions forKey:@"shuffle_questions"];
    [self.tbcm.testOptions setValue:forced_submit forKey:@"forced_submit"];
    [self.tbcm.testOptions setValue:show_score forKey:@"show_score"];
    [self.tbcm.testOptions setValue:show_result forKey:@"show_result"];
    [self.tbcm.testOptions setValue:show_result_with_correct_answer forKey:@"show_result_with_correct_answer"];
    [self.tbcm.testOptions setValue:show_feedback_messages forKey:@"show_feedback_messages"];
    [self.tbcm.testOptions setValue:set_password forKey:@"set_password"];
    [self.tbcm.testOptions setValue:password forKey:@"password"];
}

@end
