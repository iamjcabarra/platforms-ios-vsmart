//
//  QuizHeader.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/9/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuizHeader : UIView

@property (strong, nonatomic) IBOutlet UIButton *buttonAssignQuestion;

- (void)displayData:(NSManagedObject *)object;
- (void)displayDefaultValues;
- (NSMutableDictionary *)getHeaderValues;

@end
