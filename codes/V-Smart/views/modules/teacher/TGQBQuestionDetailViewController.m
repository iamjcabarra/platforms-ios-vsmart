//
//  TGQBQuestionDetailViewController.m
//  V-Smart
//
//  Created by Ryan Migallos on 10/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGQBQuestionDetailViewController.h"
#import "TGQBQuestionItemDropDownMenu.h"
#import "TGQBQuestionContentViewController.h"
#import "TestGuruDataManager.h"
#import "TestGuruConstants.h"
#import "QuestionSamplerView.h"
#import "MainHeader.h"
#import "HUD.h"
#import "UIImageView+WebCache.h"

#import "V_Smart-Swift.h" //SWIFT 2.1.1 IMPLEMENTATION

@interface TGQBQuestionDetailViewController () <TGQBQuestionItemDropDownDelegate, QuestionSamplerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *packageImageView;
@property (strong, nonatomic) IBOutlet UILabel *packageTypeName;

@property (strong, nonatomic) IBOutlet UILabel *questionItemTitle;
@property (strong, nonatomic) IBOutlet TGQBQuestionItemDropDownMenu *dropDown;

@property (strong, nonatomic) IBOutlet UIButton *saveCloseButton;
@property (strong, nonatomic) IBOutlet UIButton *previewButton;

@property (strong, nonatomic) IBOutlet UIButton *nextButton;
@property (strong, nonatomic) IBOutlet UIButton *prevButton;

@property (strong, nonatomic) TGQBQuestionContentViewController *content;
@property (strong, nonatomic) TestGuruDataManager *tm;

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

//NAVIGATION BUTTON
@property (strong, nonatomic) UIBarButtonItem *leftButton;
@property (strong, nonatomic) UIBarButtonItem *rightButton;

// QUESTION CYCLE MANAGER
@property (strong, nonatomic) NSArray *objectList;
@property (assign, nonatomic) NSInteger currentIndex;
@property (assign, nonatomic) NSInteger currentCount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerTopConstraint;

@property (assign, nonatomic) BOOL isComplete;
@end

@implementation TGQBQuestionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.dropDown.delegate = self;
    self.tm = [TestGuruDataManager sharedInstance];
    
    if (self.editMode == NO) {
        self.currentIndex = 0;
        self.headerTopConstraint.constant = 0;
    }
    
    if (self.editMode == YES) {
        self.headerTopConstraint.constant = -38;
    }
    
    self.currentCount = [self.tm fetchCountForEntity:kQuestionEntity];
    self.nextButton.hidden = (self.currentCount == 1);
    self.prevButton.hidden = YES;//(self.currentCount == 1);
    
    NSString *create_string = NSLocalizedString(@"Create Question", nil);
    NSString *edit_string = NSLocalizedString(@"Edit Question", nil);
    NSString *title_string = (self.editMode == NO) ? create_string : edit_string;
    self.title = [NSString stringWithFormat:@"%@", title_string];
    
    [self customizeNavigationController];
    [self applyShadowToView:self.dropDown];
    [self applyShadowToView:self.packageImageView];
    [self setupPackageIdentifier];
    [self setupNotification];

    [self.nextButton addTarget:self action:@selector(nextButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.prevButton addTarget:self action:@selector(prevButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self applyShadowToView:self.prevButton];
    [self applyShadowToView:self.nextButton];
    
    [self.saveCloseButton addTarget:self action:@selector(saveCloseButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.previewButton addTarget:self action:@selector(previewButtonAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setQuestionObject:(NSManagedObject *)managedObject {
    
    if (managedObject != nil) {
        self.mo = managedObject;
//        BOOL flag = [self validateQuestionObject:self.mo];
//        NSString *is_complete = (flag) ? @"1" : @"0";
        
        NSString *is_complete = [self.tm stringValue:[managedObject valueForKey:@"complete"]];
        [self validateCompleteStateButton:is_complete];
        [self validateSaveAndClose];
    }
}

- (BOOL)validateQuestionObject:(NSManagedObject *)mo {
    
    NSString *question_title = [NSString stringWithFormat:@"%@", [mo valueForKey:@"name"] ];
    NSString *question_description = [NSString stringWithFormat:@"%@", [mo valueForKey:@"question_text"] ];
    NSString *question_points = [NSString stringWithFormat:@"%@", [mo valueForKey:@"points"] ];
    NSString *proficiency_level_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"proficiency_level_id"] ];
    NSString *learning_skills_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"learning_skills_id"] ];
    
    BOOL isComplete = NO;
    
    if ( (question_title.length > 0) &&
        (question_description.length > 0) &&
        (question_points.length > 0) &&
        (proficiency_level_id.length > 0) &&
        (learning_skills_id.length > 0) ) {
        isComplete = YES;
    }
    
    return isComplete;
}

- (void)nextButtonAction:(UIButton *)button {
//    NSInteger count = [self.tm fetchCountForEntity:kQuestionEntity];
//    self.nextButton.hidden = NO;
//    self.prevButton.hidden = NO;
//    
//    NSInteger index = self.currentIndex;
//    index = index + 1; //increment
//    count = count - 1;
//    
//    NSLog(@"count [%ld]\ncurrentIndex[%ld]\nindex[%ld]", (long)count,(long)self.currentIndex,(long)index);
//    
//    if (index == count) {
//        self.nextButton.hidden = YES;
//        index = count;
//    }
//    self.currentIndex = index;
//    self.currentCount = count;
//    
//    [self traverseWithIndex:index count:count];
    
    NSInteger index = self.currentIndex;
    index = index + 1; //increment
    
    [self traverseWithIndex:index];
}

- (void)prevButtonAction:(UIButton *)button {
//    NSInteger count = [self.tm fetchCountForEntity:kQuestionEntity];
//    self.nextButton.hidden = NO;
//    self.prevButton.hidden = NO;
//    
//    NSInteger index = self.currentIndex;
//    index = index - 1; //decrement
//    
//    NSLog(@"count [%ld]\ncurrentIndex[%ld]\nindex[%ld]", (long)count,(long)self.currentIndex,(long)index);
////    if (index <= 0) {
////    if (index == 0) {
////        self.prevButton.hidden = YES;
////        index = 0;
////    }
//    self.currentIndex = index;
//    self.currentCount = count;
//    
//    [self traverseWithIndex:index count:count];
    
    NSInteger index = self.currentIndex;
    index = index - 1; //decrement
    
    [self traverseWithIndex:index];
    }
    
- (void)traverseWithIndex:(NSInteger)index { //count:(NSInteger)count {
    self.prevButton.enabled = NO;
    self.nextButton.enabled = NO;
    [self performSelector:@selector(activateButtons) withObject:nil afterDelay:1.0];
    
//    __block NSInteger listCount;
//    __weak typeof(self) wo = self;
//    [self.tm fetchQuestionList:^(NSArray *list) {
//        if (list != nil) {
//            wo.objectList = [NSArray arrayWithArray:list];
//            listCount = [wo.objectList count];
//            if ( (index >= 0) && (index < listCount)) {
//                NSManagedObject *question_mo = wo.objectList[index];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [wo setQuestionObject:question_mo];
//                    [wo.content setObjectData:question_mo];
//                });
//            }
//        }
//    }];
    
//    __weak typeof(self) wo = self;
    __block NSInteger listCount = [self.tm fetchCountForEntity:kQuestionEntity];
    
//    [self.tm fetchQuestionList:^(NSArray *list) {
        if (listCount > 0) {
            if ( (index >= 0) && (index < listCount)) {
//                dispatch_async(dispatch_get_main_queue(), ^{
                    NSNotificationCenter *notif = [NSNotificationCenter defaultCenter];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
                    [notif postNotificationName:kNotificationQuestionEditFromPreview object:indexPath];
//                });
            }
}
//    }];

    NSLog(@"INDEX [%ld], LISTCOUNT [%ld]", (long)index, (long)listCount);
    [self setUpData:index withListCount:listCount];
}
    
- (void)setUpData:(NSInteger)index withListCount:(NSInteger)listCount {
    BOOL prevHidden = NO;
    BOOL nextHidden = NO;
                
    if (index == 0) {
        prevHidden = YES;
            }
    
    if (index == (listCount - 1)) {
        nextHidden = YES;
        }
    
    if (listCount == 1) {
        nextHidden = YES;
        prevHidden = YES;
    }
    
    self.nextButton.hidden = nextHidden;
    self.prevButton.hidden = prevHidden;
    self.currentIndex = index;
    self.currentCount = listCount;
}

- (void)activateButtons {
    self.prevButton.enabled = YES;
    self.nextButton.enabled = YES;
}

- (void)cancelButtonAction:(id)sender {
    
    NSString *alertMessage = NSLocalizedString(@"Are you sure you want to leave this page, your changes will not be applied?", nil);
    
    NSString *noButtonTitle = NSLocalizedString(@"No", nil);
    NSString *yesButtonTitle = NSLocalizedString(@"Yes", nil);
    
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:@""
                                                                          message:alertMessage
                                                                   preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:yesButtonTitle
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         NSLog(@"YES action");
                                                         [self cleanScratchPad];
                                                     }];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:noButtonTitle
                                                        style:UIAlertActionStyleCancel
                                                     handler:nil];
    [alertControl addAction:noAction];
    [alertControl addAction:yesAction];
    
    [self presentViewController:alertControl animated:YES completion:nil];
}

- (void)cleanScratchPad {
    
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    BOOL flag = [self.tm clearContentsForEntity:kQuestionEntity predicate:nil];
    if (flag) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - NSNotification Center Capabilities

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self teardownNotification];
    [self.dropDown teardownNotificationForItemList];
}

- (void)setupNotification {
    SEL updateNotifAction = @selector(questionUpdateNotification:);
    SEL deleteNotifAction = @selector(questionDeleteNotification:);
    
    NSNotificationCenter *notif = [NSNotificationCenter defaultCenter];
    [notif addObserver:self selector:updateNotifAction name:kNotificationQuestionUpdate object:nil];
    [notif addObserver:self selector:deleteNotifAction name:kNotificationQuestionDelete object:nil];
}

- (void)teardownNotification {
    NSNotificationCenter *notif = [NSNotificationCenter defaultCenter];
    [notif removeObserver:self name:kNotificationQuestionUpdate object:nil];
    [notif removeObserver:self name:kNotificationQuestionDelete object:nil];
}

- (void)questionUpdateNotification:(NSNotification *)notification {
    
    NSString *complete = [NSString stringWithFormat:@"%@", [notification object] ];
    NSLog(@"%s --- complete state : %@", __PRETTY_FUNCTION__, complete);
    [self validateCompleteStateButton:complete];
    [self validateSaveAndClose];
}

- (void)questionDeleteNotification:(NSNotification *)notification {
    
    NSString *notif_question_id = [NSString stringWithFormat:@"%@", [notification object] ];
    
    self.currentCount = [self.tm fetchCountForEntity:kQuestionEntity];
    self.nextButton.hidden = (self.currentCount == 1);
    self.prevButton.hidden = (self.currentCount == 1);
    NSInteger index = self.currentIndex;

    NSLog(@"---------------------------------------------");
    NSLog(@"notif_question_id : %@", notif_question_id);
    NSLog(@"NUMBER OF COUNT : %@", @(self.currentCount) );
    NSLog(@"OLD INDEX : %@", @(index) );
//    if ( (index >= 0) && (index < self.currentCount) ) {
//        
//        --index;
//        if (index < 0) {
//            index = 0;
//        }
//        
//        self.currentIndex = index;
//        [self traverseWithIndex:index count:self.currentCount];
//    }
        
    if (index == self.currentCount) {
        index = index - 1;
        [self traverseWithIndex:index];
        return;
        }
        
    if (index < self.currentCount) {
        [self traverseWithIndex:index];
    }
    
}

- (void)validateSaveAndClose {
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wo.tm checkCompleteStates:^(NSInteger number) {
            wo.isComplete = (number == 0);
            wo.saveCloseButton.backgroundColor = (number == 0) ? UIColorFromHex(0x68BF61) : [UIColor lightGrayColor];
            wo.saveCloseButton.userInteractionEnabled = (number == 0);
            
        }];
    });
}

- (void)validateCompleteStateButton:(NSString *)complete {
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wo.dropDown displayCheckState:complete];
    });
}

- (void)setupPackageIdentifier {
    
//    NSString *package_type_name = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_NAME]];
////    NSData *package_type_image_data = [NSData dataWithData:[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_DATA] ];
//     NSString *package_type_image_url = [self.tm stringValue:[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_URL]];
//    
//    __weak typeof(self) wo = self;
//    dispatch_async(dispatch_get_main_queue(), ^{
////        wo.packageImageView.image = [UIImage imageWithData:package_type_image_data];
//        [wo.packageImageView sd_setImageWithURL:[[NSURL alloc] initWithString:package_type_image_url]];
//        wo.packageTypeName.text = [package_type_name uppercaseString];
//    });
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *package_type_name = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_NAME]];
        NSString *package_type_image_url = [self.tm stringValue:[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_URL]];
        [wo.packageImageView sd_setImageWithURL:[NSURL URLWithString:package_type_image_url]];
        wo.packageTypeName.text = [package_type_name uppercaseString];
    });
}

- (void)customizeNavigationController {
    
    UIColor *color = UIColorFromHex(0xFE9A3D);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    } else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [self setupNavigationButtons];
}

- (void)setupNavigationButtons {
    
    self.navigationItem.hidesBackButton = YES;
    
    SEL cancelAction = @selector(cancelButtonAction:);
    
    if (self.leftButton == nil) {
        NSString *course_name = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTGQB_SELECTED_COURSE_NAME]];
//        self.leftButton = [self generateButtonWithTitle:course_name action:cancelAction];
//        self.navigationItem.leftBarButtonItem = self.leftButton;
        UIButton *b = [UIButton buttonWithType:UIButtonTypeSystem];
        [b asBackButtonWithTitle:course_name color:[UIColor whiteColor]];
        [b addTarget:self action:cancelAction forControlEvents:UIControlEventTouchUpInside];
        self.leftButton = [[UIBarButtonItem alloc] init];
        self.leftButton.customView = b;
        self.navigationItem.leftBarButtonItem = self.leftButton;
    }

    if (self.editMode == YES) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    if (self.editMode == NO) {
        if (self.rightButton == nil) {
            NSString *cancel_string = NSLocalizedString(@"Cancel", nil);
            self.rightButton = [self generateButtonWithTitle:cancel_string action:cancelAction];
//    UIImage * btBack_30 = [[UIImage imageNamed:@"search_bg_white"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13, 0, 5)];
//    [cancel setBackButtonBackgroundImage:btBack_30 forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
            self.navigationItem.rightBarButtonItem = self.rightButton;
        }
    }
}

- (UIBarButtonItem *)generateButtonWithTitle:(NSString *)title action:(SEL)action {
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:title
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:action];
    return button;
}

- (UIBarButtonItem *)generateButtonWithImage:(NSString *)imageName action:(SEL)action {
    
    UIImage *trashImage = [UIImage imageNamed:imageName];
    UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
    [b setImage:trashImage forState:UIControlStateNormal];
    [b setImage:trashImage forState:UIControlStateHighlighted];
    [b setFrame:CGRectMake(0, 0, 26, 26)];
    [b addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:b];
    
    return button;
}

- (void)applyShadowToView:(UIView *)object {
    
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.5f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TGQBQuestionItemDropDownDelegate

- (void)didFinishSelectingItem:(NSString *)string withObject:(NSManagedObject *)object withIndexPath:(NSIndexPath *)indexPath {
    
    self.questionItemTitle.text = [NSString stringWithFormat:@"%@", string];
    
    NSString *question_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"id"] ];
    NSString *package_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"package_type_id"] ];
    NSString *course_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"course_id"] ];
    
    NSLog(@"FROM DELEGATE");
    NSLog(@"question id : %@", question_id);
    NSLog(@"package id : %@", package_id);
    NSLog(@"course id : %@", course_id);
    
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:question_id];
    NSManagedObject *question_mo = [self.tm getEntity:kQuestionEntity predicate:predicate];
    
    self.selectedIndexPath = indexPath;
//    [self traverseWithIndex:indexPath.row count:0];
    
    
    [self setQuestionObject:question_mo];
    [self.content setObjectData:question_mo];
    
    NSInteger listCount = [self.tm fetchCountForEntity:kQuestionEntity];
    [self setUpData:indexPath.row withListCount:listCount];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"tgqb_content_view_embed"]) {
        self.content = (TGQBQuestionContentViewController *)[segue destinationViewController];
        self.content.title = @"HELLO WORLD";
        [self.content setObjectData:self.mo];
    }
    
    if ([segue.identifier isEqualToString:@"SAMPLE_QUESTION_VIEW"]) {
        QuestionSamplerView *qsv = (QuestionSamplerView *)[segue destinationViewController];
        qsv.question = self.mo;
        qsv.delegate = self;
        qsv.indexPath = self.selectedIndexPath;
        qsv.entityObject = kQuestionEntity;
        qsv.sortDescriptors = [self createPreviewSortDescriptor];
        qsv.isComplete = self.isComplete;
    }
}

- (NSArray *)createPreviewSortDescriptor {
    NSSortDescriptor *sortType = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:YES];
    return @[sortType];
}

- (void)previewButtonAction:(UIButton *)sender {
    NSLog(@"-------> %s", __PRETTY_FUNCTION__);
    
    [self performSegueWithIdentifier:@"SAMPLE_QUESTION_VIEW" sender:self];
}

- (void)saveCloseButtonAction:(UIButton *)sender {
    NSLog(@"-------> %s", __PRETTY_FUNCTION__);
    [self saveAllComponents];
}

- (void)saveAllComponents {
    [HUD showUIBlockingIndicatorWithText:NSLocalizedString(@"Uploading", nil)];

    [self.tm postQuestionsObjectForEditMode:self.editMode withDoneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self displayAlertForStatus:status editMode:self.editMode];
            [HUD hideUIBlockingIndicator];
        });
    }];
}

- (void)displayAlertForStatus:(BOOL)status editMode:(BOOL)editMode {
    
    //    NSLog(@"---------------------> STATUS : %@", (status) ? @"YES" : @"NO" );
    NSString *alert_title = @"";
    NSString *positive_response = NSLocalizedString(@"Okay", nil);// uncheck
    
    //CREATE STRING
    NSString *create_success = NSLocalizedString(@"Successfully created Question/s", nil);
    NSString *create_failed = NSLocalizedString(@"Failed to create Question/s", nil);
    
    NSString *edit_success = NSLocalizedString(@"Successfully updated Question", nil);
    NSString *edit_failed = NSLocalizedString(@"Failed to update Question", nil);
    
    NSString *success = (editMode == NO) ? create_success : edit_success;
    NSString *failed = (editMode == NO) ? create_failed : edit_failed;
    NSString *alert_message = (status) ? success : failed;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:alert_title
                                                                   message:alert_message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:positive_response style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
        if (status) {
            
            [self.navigationController popViewControllerAnimated:NO];
            
//            if (editMode == NO) {
                SEL delegateAction = @selector(didFinishCreateOperation:);
                if ([(NSObject *)self.delegate respondsToSelector:delegateAction]) {
                    [self.delegate didFinishCreateOperation:YES];
                }
//            }//CREATE OPERATION
        }
        
    }];
    
    [alert addAction:confirmAction];
    [self presentViewController:alert animated:YES completion:nil];
}

///////////////////////////
//Question Sampler Delegate
///////////////////////////
- (void)didSelectEditIndex:(NSIndexPath *)indexPath withEditObject:(NSManagedObject *)object {
    NSNotificationCenter *notif = [NSNotificationCenter defaultCenter];
    [notif postNotificationName:kNotificationQuestionEditFromPreview object:indexPath];
}

- (void)didFinishSaveInformation:(BOOL)state {
    [self.tm postQuestionsObjectForEditMode:self.editMode withDoneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self displayAlertForStatus:status editMode:self.editMode];
        });
    }];
}

@end
