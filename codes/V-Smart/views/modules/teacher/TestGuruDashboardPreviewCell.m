//
//  TestGuruDashboardPreviewCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 22/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruDashboardPreviewCell.h"

@implementation TestGuruDashboardPreviewCell

- (void)awakeFromNib {
    
    CALayer *imageLayer = self.containerView.layer;
    imageLayer.shadowColor = [UIColor lightGrayColor].CGColor;
    imageLayer.shadowOffset = CGSizeMake(2.0,2.0);
    imageLayer.shadowRadius = 5.0f;
    imageLayer.shadowOpacity = 0.9f;
}

@end
