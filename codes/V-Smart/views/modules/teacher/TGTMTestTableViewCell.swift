//
//  TGTMTestTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 22/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
import QuartzCore

class TGTMTestTableViewCell: MGSwipeTableCell {
    
    @IBOutlet var testNameLabel: UILabel!
    @IBOutlet var courseNameLabel: UILabel!
    @IBOutlet var teacherNameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var itemLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func updateStatus(_ status: String) {
        if status == "0" {
            self.statusLabel.text = NSLocalizedString("Pending", comment: "")
            self.statusLabel.backgroundColor = UIColor(rgba: "#FFCC00")
        }
        else if status == "1" {
            self.statusLabel.text = NSLocalizedString("Approved", comment: "")
            self.statusLabel.backgroundColor = UIColor(rgba: "#4CD964")
        }
        else {
            self.statusLabel.text = NSLocalizedString("Disapproved", comment: "")
            self.statusLabel.backgroundColor = UIColor(rgba: "#FF3B30")
        }
    }

}
