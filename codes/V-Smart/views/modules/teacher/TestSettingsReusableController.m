//
//  TestSettingsReusableController.m
//  V-Smart
//
//  Created by Julius Abarra on 15/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestSettingsReusableController.h"
#import "TestGuruDataManager.h"

@interface TestSettingsReusableController () <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) TestGuruDataManager *tgdm;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) UIRefreshControl *tableRefreshControl;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSString *entityName;
@property (strong, nonatomic) NSString *endPoint;

@end

static NSString *kCellIdentifier = @"PropertyItemCell";

@implementation TestSettingsReusableController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tgdm = [TestGuruDataManager sharedInstance];
    self.managedObjectContext = self.tgdm.mainContext;
    
    // Category
    if (self.selectedTestProperty == 100) {
        self.entityName = @"TestCategory";
        self.endPoint = kEndPointTestCategoryList;
    }
    // Resutl type
    if (self.selectedTestProperty == 200) {
        self.entityName = @"TestResultType";
        self.endPoint = kEndPointTestResultTypeList;
    }
    // Test type
    if (self.selectedTestProperty == 300) {
        self.entityName = @"TestType";
        self.endPoint = kEndPointTestTypeList;
    }
    // Level of difficulty
    if (self.selectedTestProperty == 400) {
        self.entityName = @"TestDifficultyLevel";
        self.endPoint = kEndPointTestDifficultyLevelList;
    }
    // Learning skill
    if (self.selectedTestProperty == 500) {
        self.entityName = @"TestLearningSkill";
        self.endPoint = kEndPointTestLearningSkillList;
    }
    
    [self listTestPropertyItems];
    
    // Pull to refresh
    SEL refreshAction = @selector(listTestPropertyItems);
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - List Test Property Items

- (void)listTestPropertyItems {
    if (self.entityName != nil && ![self.endPoint isEqualToString:@""]) {
        __weak typeof(self) wo = self;
        [self.tgdm requestTestProperty:self.entityName endPoint:self.endPoint doneBlock:^(BOOL status) {
            if (status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [wo.tableRefreshControl endRefreshing];
                    [wo reloadFetchedResultsController];
                    [wo.tableView reloadData];
                });
            }
        }];
    }
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];
    }
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *testPropertyValue = [self stringValue:[mo valueForKey:@"value"]];
    
    cell.textLabel.text = testPropertyValue;
    return cell;
}

#pragma mark - Table Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *testPropertyID = [NSString stringWithFormat:@"%@", [mo valueForKey:@"id"]];
    NSString *testPropertyValue = [NSString stringWithFormat:@"%@", [mo valueForKey:@"value"]];
    NSDictionary *d = @{@"id":testPropertyID, @"value":testPropertyValue};
    
    [self.delegate selectedTestProperty:d];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Get String Value

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:self.entityName];
    [fetchRequest setFetchBatchSize:10];
    
    NSSortDescriptor *sortByid = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortByid]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - Reload Fetched Results Controller

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

@end