//
//  QuestionTypeListItemCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 29/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "QuestionTypeListItemCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation QuestionTypeListItemCell

- (void)awakeFromNib {
    // Initialization code

    CALayer *imageLayer = self.questionImageView.layer;
    
    imageLayer.shadowColor = [UIColor lightGrayColor].CGColor;
    imageLayer.shadowOffset = CGSizeMake(2.0,2.0);
    imageLayer.shadowRadius = 5.0f;
    imageLayer.shadowOpacity = 0.9f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
