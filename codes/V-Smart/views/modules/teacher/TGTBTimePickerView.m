//
//  TGTBTimePickerView.m
//  V-Smart
//
//  Created by Julius Abarra on 14/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGTBTimePickerView.h"
#import "TBClassHelper.h"

@interface TGTBTimePickerView () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) TBClassHelper *classHelper;

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;

@property (strong, nonatomic) IBOutlet UILabel *pickerViewTitle;
@property (strong, nonatomic) IBOutlet UILabel *hrsLabel;
@property (strong, nonatomic) IBOutlet UILabel *minLabel;
@property (strong, nonatomic) IBOutlet UILabel *secLabel;

@property (strong, nonatomic) IBOutlet UIButton *doneButton;

@property (strong, nonatomic) NSMutableArray *hrsList;
@property (strong, nonatomic) NSMutableArray *minList;
@property (strong, nonatomic) NSMutableArray *secList;

@property (strong, nonatomic) NSString *prevMin;
@property (strong, nonatomic) NSString *prevSec;
@property (strong, nonatomic) NSString *timeToReturn;

@end

@implementation TGTBTimePickerView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Title
    self.pickerViewTitle.text = [self.classHelper localizeString:@"Set Time Limit"];
    
    // Background View
    self.backgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.backgroundView.layer.borderWidth = 1.0f;
    
    // Initialize Time Limit
    self.timeToReturn = @"00:15:00";
    
    // Time Collections
    self.hrsList = [NSMutableArray array];
    self.minList = [NSMutableArray array];
    self.secList = [NSMutableArray array];
    
    for (int i = 0; i < 60; i++) {
        NSString *stringValue = (i < 10) ? [NSString stringWithFormat:@"0%d", i] : [NSString stringWithFormat:@"%d", i];
        
        if (i < 25) {
            [self.hrsList addObject:stringValue];
        }
        
        [self.minList addObject:stringValue];
        [self.secList addObject:stringValue];
    }

    // Initialize Time Components
    self.hrsLabel.text = @"00";
    self.minLabel.text = @"00";
    self.secLabel.text = @"00";
    
    // Initialize Flags
    self.prevMin = @"00";
    self.prevSec = @"00";
    
    // Done Button Action
    [self.doneButton addTarget:self
                        action:@selector(selectTimeAction:)
              forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Picker View Data Source

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView; {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch (component) {
        case 0:
            return self.hrsList.count;
            break;
        case 1:
            return self.minList.count;
            break;
        case 2:
            return self.secList.count;
            break;
        default:
            break;
    }
    
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    switch (component) {
        case 0:
            return [self.hrsList objectAtIndex:row];
            break;
        case 1:
            return [self.minList objectAtIndex:row];
            break;
        case 2:
            return [self.secList objectAtIndex:row];
            break;
        default:
            break;
    }
    
    return nil;
}

#pragma mark - Picker View Delegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    switch (component) {
        case 0:
            self.hrsLabel.text = [self.hrsList objectAtIndex:row];
            
            if (row == 24) {
                self.minLabel.text = @"00";
                self.secLabel.text = @"00";
            }
            else {
                self.minLabel.text = self.prevMin;
                self.secLabel.text = self.prevSec;
            }
            
            break;
        case 1:
            self.prevMin = [self.minList objectAtIndex:row];
            self.minLabel.text = ([self.hrsLabel.text isEqualToString:@"24"]) ? @"00" : self.prevMin;
            break;
        case 2:
            self.prevSec = [self.secList objectAtIndex:row];
            self.secLabel.text = ([self.hrsLabel.text isEqualToString:@"24"]) ? @"00" : self.prevSec;
            break;
        default:
            break;
    }
}

#pragma mark - Done Button Action

- (void)selectTimeAction:(id)sender {
    self.timeToReturn = [NSString stringWithFormat:@"%@:%@:%@", self.hrsLabel.text, self.minLabel.text, self.secLabel.text];
    [self.delegate selectedTime:self.timeToReturn];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
