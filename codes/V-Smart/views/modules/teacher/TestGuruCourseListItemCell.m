//
//  TestGuruCourseListItemCell.m
//  V-Smart
//
//  Created by Carmelito Bayarcal on 09/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestGuruCourseListItemCell.h"

@implementation TestGuruCourseListItemCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)layoutSubviews {
    [super layoutSubviews];
//    CGFloat viewWidth = self.contentView.frame.size.width;
//    CGFloat dividedWidth = viewWidth/4;
//    
//    self.courseNameWidth.constant = dividedWidth;
//    self.courseCodeWidth.constant = dividedWidth;
//    self.courseDescriptionWidth.constant = dividedWidth;
//    
//    NSLog(@"VIEW WIDTH!!!! [%f]", viewWidth);
//    http://blog.projectrhinestone.org/enable-justified-alignment-for-multi-line-uilabel-instance/
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    paragraphStyle.alignment = NSTextAlignmentJustified;
//    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:17.f],
//                                 NSBaselineOffsetAttributeName: @0,
//                                 NSParagraphStyleAttributeName: paragraphStyle};
//    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:myText
//                                                                         attributes:attributes];
//    myLabel.attributedText = attributedText;
    
    
    self.courseDescription.textAlignment = NSTextAlignmentJustified;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
