//
//  TGTBSidePanelReusableView.swift
//  V-Smart
//
//  Created by Ryan Migallos on 30/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class TGTBSidePanelReusableView: UICollectionReusableView {
    
    @IBOutlet var customTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
