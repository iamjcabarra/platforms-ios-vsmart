//
//  TGQBChoicesView.h
//  V-Smart
//
//  Created by Ryan Migallos on 15/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGQBChoicesViewDelegate <NSObject>
@optional
- (void)expandTableWithHeight:(CGFloat)customHeight;
@end
@interface TGQBChoicesView : UITableViewCell

@property (weak, nonatomic) id <TGQBChoicesViewDelegate> delegate;
@property (assign, nonatomic) BOOL isTrashHidden;

- (void)setObjectData:(NSManagedObject *)object;

@end
