//
//  TGQBQuestionBasicData.h
//  V-Smart
//
//  Created by Ryan Migallos on 12/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGQBQuestionBasicData : UITableViewCell

// Entry Fields
@property (strong, nonatomic) IBOutlet UITextField *titleField;
@property (strong, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (strong, nonatomic) IBOutlet UITextField *pointsField;
@property (strong, nonatomic) IBOutlet UILabel *pointsIndicator;

- (void)setObjectData:(NSManagedObject *)object;

@end
