//
//  TGTBCreateTestSegmentView.h
//  V-Smart
//
//  Created by Julius Abarra on 07/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestGuruConstants.h"
@interface TGTBCreateTestSegmentView : UIViewController

@property (strong, nonatomic) NSManagedObject *testObject;
- (void)swapEmbeddedViews:(NSString *)segueIdentifier;
@property (assign, nonatomic) TGTBCrudActionType crudActionType;

@end
