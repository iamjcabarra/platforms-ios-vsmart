//
//  TestGuruQuestionDetailsViewController.m
//  V-Smart
//
//  Created by Ryan Migallos on 05/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruQuestionDetailsViewController.h"
#import "TestGuruCollapsibleHeaderView.h"
#import "TestGuruOptionCell.h"
#import "TestGuruFeedbackCell.h"
#import "TestGuruTagCell.h"
#import "TGDetailWithChoiceCell.h"
#import "TGDetailWithTextAreaCell.h"

@interface TestGuruQuestionDetailsViewController ()

@property (strong, nonatomic) NSMutableDictionary *treeStructure;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *items;

@end

@implementation TestGuruQuestionDetailsViewController

static NSString *kPropertyIdentifier = @"cell_question_property";
static NSString *kDetailIdentifier = @"cell_question_detail";
static NSString *kSectionIdentifier = @"section_header_identifier";

static NSString *kBasicCellIdentifier = @"basic_cell_prototype_identifier";
static NSString *kCellOptionIdentifier = @"table_cell_option_identifier";
static NSString *kCellFeedbackIdentifier = @"table_cell_feedback_identifier";
static NSString *kCellTagShareIdentifier = @"table_cell_tag_identifier";
static NSString *kCellUserChoiceDetailIdentifier = @"table_cell_user_choice_identifier";
static NSString *kCellUserTextDetailIdentifier = @"table_cell_user_textarea_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = NSLocalizedString(@"Create Questions", nil);
    [self setupRightBarButton];
    if (self.editMode) {
//        self.title
    }
    
    // TABLE HEADER
    UINib *cn = [UINib nibWithNibName:@"TestGuruCollapsibleHeaderView" bundle:nil];
    [self.tableView registerNib:cn forHeaderFooterViewReuseIdentifier:kSectionIdentifier];
    
    // REGISTER OPTION TYPE CELL
    UINib *on = [UINib nibWithNibName:@"TestGuruOptionCell" bundle:nil];
    [self.tableView registerNib:on forCellReuseIdentifier:kCellOptionIdentifier];
    
    // REGISTER FEEDBACK CELL
    UINib *fn = [UINib nibWithNibName:@"TestGuruFeedbackCell" bundle:nil];
    [self.tableView registerNib:fn forCellReuseIdentifier:kCellFeedbackIdentifier];
    
    // TAG SHARE CELL
    UINib *tsn = [UINib nibWithNibName:@"TestGuruTagCell" bundle:nil];
    [self.tableView registerNib:tsn forCellReuseIdentifier:kCellTagShareIdentifier];

    // CHOICE CELL
    UINib *dn = [UINib nibWithNibName:@"TGDetailWithChoiceCell" bundle:nil];
    [self.tableView registerNib:dn forCellReuseIdentifier:kCellUserChoiceDetailIdentifier];
    
    // TEXT CELL
    UINib *tn = [UINib nibWithNibName:@"TGDetailWithTextAreaCell" bundle:nil];
    [self.tableView registerNib:tn forCellReuseIdentifier:kCellUserTextDetailIdentifier];
    
    [self loadInitialContents];
}

- (void)setupRightBarButton {
    
    NSString *cancelTitle = NSLocalizedString(@"Cancel", nil);
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithTitle:cancelTitle
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:@selector(cancelButtonAction:)];
    self.navigationItem.rightBarButtonItem = cancel;
}

- (void)cancelButtonAction:(id)sender {
    
    NSString *alertTitle = NSLocalizedString(@"Save Changes", nil);
    NSString *alertMessage = NSLocalizedString(@"Do you want to save your changes?", nil);
    
    NSString *noButtonTitle = NSLocalizedString(@"No", nil);
    NSString *yesButtonTitle = NSLocalizedString(@"Yes", nil);
    
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:alertTitle
                                                                          message:alertMessage
                                                                   preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:noButtonTitle
                                                       style:UIAlertActionStyleCancel
                                                     handler:^(UIAlertAction *action) {
                                                         NSLog(@"NO action");
                                                     }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:yesButtonTitle
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          NSLog(@"YES action");
                                                      }];
    
    [alertControl addAction:noAction];
    [alertControl addAction:yesAction];

    [self presentViewController:alertControl animated:YES completion:nil];
}

- (void)displayContentFromObject:(NSManagedObject *)mo {
    
    if (mo != nil) {
        
        NSLog(@"--------> entity object : %@", mo.entity.name);
        NSString *question_type_name = [mo valueForKey:@"questionTypeName"];
        NSLog(@"question type name : %@", question_type_name);
        NSString *question_type_id = [mo valueForKey:@"question_type_id"];
        NSLog(@"question type id : %@", question_type_id);
        
        NSString *learning_skill_name = [mo valueForKey:@"learningSkillsName"];
        NSLog(@"learning skill name : %@", learning_skill_name);
        NSString *learning_skills_id = [mo valueForKey:@"learning_skills_id"];
        NSLog(@"learning skills id : %@", learning_skills_id);
        
        NSString *proficiency_level_name = [mo valueForKey:@"proficiencyLevelName"];
        NSLog(@"proficiency level name : %@", proficiency_level_name);
        NSString *proficiency_level_id = [mo valueForKey:@"proficiency_level_id"];
        NSLog(@"proficiency level id : %@", proficiency_level_id);
    }
    
}

- (void)loadInitialContents {

    NSArray *question_types = @[
                                
                                @{@"id":@"0",
                                  @"value":@"EASY",
                                  @"selected":@(YES)
                                  },
                                
                                @{@"id":@"1",
                                  @"value":@"MODERATE",
                                  @"selected":@(NO)
                                  },

                                @{@"id":@"2",
                                  @"value":@"DIFFICULT",
                                  @"selected":@(NO)
                                  }
                                
                                ];
    
    NSArray *skill_types = @[
                                @{@"id":@"0",
                                  @"value":@"APPLICATION",
                                  @"selected":@(YES)
                                  },
                                
                                @{@"id":@"1",
                                  @"value":@"COMPREHENSION",
                                  @"selected":@(NO)
                                  },
                                
                                @{@"id":@"2",
                                  @"value":@"KNOWLEDGE",
                                  @"selected":@(NO)
                                  },
                                
                                @{@"id":@"3",
                                  @"value":@"ANALYSIS",
                                  @"selected":@(NO)
                                  }
                                
                                ];
    
    NSArray *feedback_values = @[
                                 
                             @{@"id":@"0",
                               @"value":@"You have taken the exam"},
                             
                             @{@"id":@"1",
                               @"value":@"You pass the exam"},
                             
                             @{@"id":@"2",
                               @"value":@"You failed the exam"}
                             
                             ];
    
    NSArray *tags_shared = @[
                             @{@"tags":@[@"hello, world", @"test", @"guru", @"details"]},
                             @{@"shared":[NSNumber numberWithBool:NO]}
                             ];
    
    
    NSString *feedback_title_string = @"Feedback";
    
    NSArray *property_rows = @[
                                    @{@"sequence":@"0",
                                      @"type":kCellOptionIdentifier,
                                      @"height":@(75.0f),
                                      @"title":@"Question Types",
                                      @"options":question_types},

                                    @{@"sequence":@"1",
                                      @"type":kCellOptionIdentifier,
                                      @"height":@(75.0f),
                                      @"title":@"Learning Skill",
                                      @"options":skill_types},

                                    @{@"sequence":@"2",
                                      @"type":kCellFeedbackIdentifier,
                                      @"height":@(180.0f),
                                      @"title":@"Feedback",
                                      @"options":feedback_values},

                                    @{@"sequence":@"3",
                                      @"type":kCellTagShareIdentifier,
                                      @"height":@(75.0f),
                                      @"title":@"Tags",
                                      @"options":tags_shared},
                                    
                                ];
    
    NSArray *detail_rows = @[
                               @{@"sequence":@"0",
                                 @"type":kCellUserChoiceDetailIdentifier,
                                 @"height":@(390.0f),
                                 @"title":@"Question Types",
                                 @"options":@[@"EASY",@"MODERATE",@"DIFFICULT"]},
                               
                               @{@"sequence":@"1",
                                 @"type":kCellUserTextDetailIdentifier,
                                 @"height":@(245.0f),
                                 @"title":@"Question Types",
                                 @"options":@[@"EASY",@"MODERATE",@"DIFFICULT"]}
                               
                               ];
    self.items = @[
                    @{@"title": @"Question Properties",
                      @"type": @"property",
                      @"rows": property_rows},
                   
                    @{@"title": @"Question Details",
                      @"type": @"detail",
                      @"rows": detail_rows}
                ];
    
    [self setupContents:self.items];
}

- (void)setupContents:(NSArray *)list {
    
    if (list != nil) {
        
        if (self.treeStructure == nil) {
            self.treeStructure = [NSMutableDictionary dictionary];
        }
        
        for (NSUInteger i = 0; i < [list count]; i++) {
            NSString *section_key = [NSString stringWithFormat:@"%lu", (unsigned long)i];
            NSDictionary *data = (NSDictionary *)list[i];
            NSArray *item_list = (NSArray *)data[@"rows"];
            NSNumber *item_list_count = [NSNumber numberWithInteger:[item_list count]];
            [self.treeStructure setValue:item_list_count forKey:section_key];
        }
        
        NSLog(@"tree source : %@", self.treeStructure);
        
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger section_count = 0;
    if (self.treeStructure != nil) {
        NSArray *sections = [self.treeStructure allKeys];
        section_count = [sections count];
    }
    
    return section_count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger row_count = 0;
    
    if (self.treeStructure != nil) {
        NSString *section_key = [NSString stringWithFormat:@"%ld", (long)section];
        if ([self.treeStructure valueForKey:section_key] != nil) {
            NSNumber *tree_item_count = (NSNumber *)[self.treeStructure valueForKey:section_key];
            row_count = [tree_item_count integerValue];
        }
    }
    
    return row_count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *object = [self objectForIndexPath:indexPath];
    NSString *identifier = [NSString stringWithFormat:@"%@", object[@"type"] ];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];

    if ([identifier isEqualToString:kCellOptionIdentifier]) {
        TestGuruOptionCell *optionCell = (TestGuruOptionCell *)cell;
        [optionCell displayData:object];
        return optionCell;
    }
    
    if ([identifier isEqualToString:kCellFeedbackIdentifier]) {
        TestGuruFeedbackCell *feedbackCell = (TestGuruFeedbackCell *)cell;
        [feedbackCell displayData:object];
        return feedbackCell;
    }

    if ([identifier isEqualToString:kCellTagShareIdentifier]) {
        TestGuruTagCell *tagCell = (TestGuruTagCell *)cell;
        [tagCell displayData:object];
        return tagCell;
    }

    if ([identifier isEqualToString:kCellUserChoiceDetailIdentifier]) {
        TGDetailWithChoiceCell *choiceCell = (TGDetailWithChoiceCell *)cell;
        return choiceCell;
    }

    if ([identifier isEqualToString:kCellUserTextDetailIdentifier]) {
        TGDetailWithTextAreaCell *textAreaCell = (TGDetailWithTextAreaCell *)cell;
        return textAreaCell;
    }
    
    return cell;
}

- (NSDictionary *)objectForIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    
    NSDictionary *data = self.items[section];
    NSArray *item_list = (NSArray *)data[@"rows"];
    
    NSDictionary *object = (NSDictionary *)item_list[row];
    
    return object;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = 100.0f;
    
    NSDictionary *object = [self objectForIndexPath:indexPath];
    NSNumber *height_value = (NSNumber *)object[@"height"];

    
    if (height_value != nil) {
        return [height_value floatValue];
    }
    
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    TestGuruCollapsibleHeaderView *header = (TestGuruCollapsibleHeaderView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:kSectionIdentifier];
    header.section = section;
    header.indicatorImage.hidden = YES; //TODO
    
    if (self.treeStructure != nil) {
        NSDictionary *data = self.items[section];
        NSString *title_header = [NSString stringWithFormat:@"%@", data[@"title"] ];
        header.titleLabel.text = title_header;
        [header.button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return header;
}

- (NSIndexPath *)indexPathForButton:(UIButton *)button {
    
    UIView *v = [button superview];
    while (![v isKindOfClass:[TestGuruCollapsibleHeaderView class]]) {
        v = [v superview];
    }
    TestGuruCollapsibleHeaderView *custom = (TestGuruCollapsibleHeaderView *)v;
    
    NSInteger section = custom.section;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
    
    NSLog(@"section: %ld row : %ld", (long)indexPath.section, (long)indexPath.row);
    
    return indexPath;
}

- (void)buttonAction:(UIButton *)sender {
    
    NSIndexPath *indexPath = [self indexPathForButton:sender];
    
    // UPDATE DATA SOURCE
    NSUInteger section = (NSUInteger)indexPath.section;
    NSString *section_key = [NSString stringWithFormat:@"%lu", (unsigned long)section];
    
    BOOL flag = ([self.treeStructure valueForKey:section_key] != nil);
    if (flag) {
        
        //RETRIEVE TABLE ROWS
        NSNumber *tree_item_count = (NSNumber *)[self.treeStructure valueForKey:section_key];
        
        //CALCULATE NEW ROW COUNT
        NSDictionary *data = self.items[section];
        NSArray *item_total_list = (NSArray *)data[@"rows"];
        
        //NEW ROW COUNT
        NSUInteger count = [tree_item_count integerValue];
        NSInteger item_total_row_count = [item_total_list count];
        NSInteger calculated_row_count = (count > 0) ? 0 : item_total_row_count;
        
        //SAVE NEW ROW COUNT
        [self.treeStructure setObject:[NSNumber numberWithInteger:calculated_row_count] forKey:section_key];
        
        //CREATE INDEX PATH CONTAINERS
        NSMutableOrderedSet *orderedSet = [NSMutableOrderedSet orderedSet];
        
        NSInteger increment = item_total_row_count;
        for (NSInteger i = 0; i < increment; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
            [orderedSet addObject:indexPath];
        }
        
        BOOL expanded_state = (calculated_row_count > 0);
        
        NSArray *indexes = [orderedSet array];
        UITableView *tv = self.tableView;
        
        //INSERT
        if ( expanded_state == YES) {
            NSLog(@"INSERTING ROWS...");
            [tv beginUpdates];
            [tv insertRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationTop];
            [tv endUpdates];
        }
        
        //DELETE
        if ( expanded_state == NO ) {
            NSLog(@"REMOVING ROWS...");
            [tv beginUpdates];
            [tv deleteRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationTop];
            [tv endUpdates];
        }
    }
}

@end
