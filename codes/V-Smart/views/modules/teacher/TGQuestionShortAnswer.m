//
//  TGQuestionShortAnswer.m
//  V-Smart
//
//  Created by Ryan Migallos on 10/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TGQuestionShortAnswer.h"
#import "TestGuruDataManager.h"

typedef NS_ENUM (NSInteger, TGQuestionImageOptionValue) {
    TGQuestionImageOptionValueDefault,
    TGQuestionImageOptionValue1,
    TGQuestionImageOptionValue2,
    TGQuestionImageOptionValue3,
    TGQuestionImageOptionValue4,
};

@interface TGQuestionShortAnswer() <UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, UIImagePickerControllerDelegate>

// Localization Items
@property (strong, nonatomic) IBOutlet UILabel *titleIndicator;
@property (strong, nonatomic) IBOutlet UILabel *descriptionIndicator;
@property (strong, nonatomic) IBOutlet UILabel *pointsIndicator;
@property (strong, nonatomic) IBOutlet UILabel *imageIndicator;

// Text View Items
@property (strong, nonatomic) IBOutlet UIView *textViewContainer;
@property (strong, nonatomic) IBOutlet UILabel *descriptionPlaceholder;

// Image Placeholders
@property (strong, nonatomic) IBOutlet UIImageView *imageViewOne;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewThree;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewFour;

// Image Buttons
@property (strong, nonatomic) IBOutlet UIButton *buttonOne;
@property (strong, nonatomic) IBOutlet UIButton *buttonTwo;
@property (strong, nonatomic) IBOutlet UIButton *buttonThree;
@property (strong, nonatomic) IBOutlet UIButton *buttonFour;

@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) NSString *question_id;
@property (strong, nonatomic) NSManagedObject *mo;

@property (strong, nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) UIImagePickerController *imagePicker;

@property (assign, nonatomic) TGQuestionImageOptionValue questionImageOption;

@end

@implementation TGQuestionShortAnswer

- (void)awakeFromNib {
    
    self.tm = [TestGuruDataManager sharedInstance];
    // Initialization code
    
    self.questionImageOption = TGQuestionImageOptionValueDefault;
    
    NSString *title_string = NSLocalizedString(@"Title", nil);
    self.titleIndicator.text = [title_string uppercaseString];
    
    NSString *question_string = NSLocalizedString(@"Question", nil);
    self.descriptionIndicator.text = [question_string uppercaseString];
    
    NSString *points_string = NSLocalizedString(@"Points", nil);
    self.pointsIndicator.text = [points_string uppercaseString];
    
    self.titleField.placeholder = NSLocalizedString(@"Please enter a title", nil);
    
    
    [self applyShadowToView:self.customBackground];
    
    //TESTING
    [self setupButtonTest];
    [self setupImagePickerControl];
}

- (void)applyShadowToView:(UIView *)object {
    
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.5f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setObjectData:(NSManagedObject *)object {
    
    self.mo = object;
    self.question_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"id"] ];
    
    // Hide place holder
    if (self.descriptionLabel.text.length > 0) {
        self.descriptionPlaceholder.hidden = YES;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // verify max length has not been exceeded
    NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString *originalText = [NSString stringWithFormat:@"%@", textField.text];
    
    if ( textField == self.pointsField ) {
        
        // allow backspace
        if (!string.length)
        {
            return YES;
        }
        
        NSCharacterSet *numberSet = [NSCharacterSet decimalDigitCharacterSet];
        if ([string rangeOfCharacterFromSet:[numberSet invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        
        if (updatedText.length > 3) // 4 was chosen for SSN verification
        {
            // suppress the max length message only when the user is typing
            // easy: pasted data has a length greater than 1; who copy/pastes one character?
            if (string.length > 1)
            {
                // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
            }
            return NO;
        }
        
        if (self.mo != nil) {
            
            //QUESTION POINTS STORE TO CORE DATA
            [self updateDetailsWithValue:originalText andKey:@"points"];
        }
        
    }
    
    if ( textField == self.titleField ) {
        
        if (self.mo != nil) {
            
            //QUESTION TITLE STORE TO CORE DATA
            [self updateDetailsWithValue:originalText andKey:@"name"];
        }
    }
    
    return YES;
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    
    NSString *updatedText = [NSString stringWithFormat:@"%@", textView.text];
    if (self.mo != nil) {
        //QUESTION DESCRIPTION STORE TO CORE DATA
        [self updateDetailsWithValue:updatedText andKey:@"question_text"];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.descriptionPlaceholder.hidden = YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSString *updated_string = [NSString stringWithFormat:@"%@", textView.text];
    if (updated_string.length > 1) {
        self.descriptionPlaceholder.hidden = YES;
    }
    
    return YES;
}

- (void)updateDetailsWithValue:(NSString *)value andKey:(NSString *)key {
    
    NSString *entity = kQuestionEntity;
    
    NSDictionary *userData = @{key:value};
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:self.question_id];
    [self.tm updateEntity:entity details:userData predicate:predicate];
}

////////////////////////////////////////////////////////////////
// ------------- IMAGE UPLOADING IMPLEMENTATION ------------- //
////////////////////////////////////////////////////////////////

- (void)setupImagePickerControl {
    
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = YES;
    self.imagePicker.modalPresentationStyle = UIModalPresentationPopover;
    self.popover = [[UIPopoverController alloc] initWithContentViewController:self.imagePicker];
}

- (void)setupButtonTest {
    
    SEL action = @selector(showImagePickerAction:);
    //    SEL action = @selector(imageTestUploadAction:);
    
    [self.buttonOne addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [self.buttonTwo addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [self.buttonThree addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [self.buttonFour addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    //    self.imageViewOne.image = [UIImage imageNamed:@"abstract_icon1.png"];
    //    self.imageViewTwo.image = [UIImage imageNamed:@"abstract_icon2.png"];
    //    self.imageViewThree.image = [UIImage imageNamed:@"abstract_icon3.png"];
    //    self.imageViewFour.image = [UIImage imageNamed:@"abstract_icon4.png"];
}

- (void)showImagePickerAction:(UIButton *)sender {
    
    UIButton *b = sender;
    
    if (b == self.buttonOne) {
        self.questionImageOption = TGQuestionImageOptionValue1;
    }
    
    if (b == self.buttonTwo) {
        self.questionImageOption = TGQuestionImageOptionValue2;
    }
    
    if (b == self.buttonThree) {
        self.questionImageOption = TGQuestionImageOptionValue3;
    }
    
    if (b == self.buttonFour) {
        self.questionImageOption = TGQuestionImageOptionValue4;
    }
    
    [self.popover presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    NSString *key_label = @"1";
    
    if (self.questionImageOption == TGQuestionImageOptionValue1) {
        self.imageViewOne.image = chosenImage;
        key_label = @"1";
    }
    
    if (self.questionImageOption == TGQuestionImageOptionValue2) {
        self.imageViewTwo.image = chosenImage;
        key_label = @"2";
    }
    
    if (self.questionImageOption == TGQuestionImageOptionValue3) {
        self.imageViewThree.image = chosenImage;
        key_label = @"3";
    }
    
    if (self.questionImageOption == TGQuestionImageOptionValue4) {
        self.imageViewFour.image = chosenImage;
        key_label = @"4";
    }
    
    NSString *name = [NSString stringWithFormat:@"%@_%@", self.question_id, key_label];
    NSString *key = [NSString stringWithFormat:@"%@_%@", self.question_id, key_label];
    NSString *filename = [NSString stringWithFormat:@"%@_%@.jpg", self.question_id, key_label];
    CGFloat compression = 0.6f;
    NSData *image_data = UIImageJPEGRepresentation(chosenImage, compression);
    NSDictionary *dictionary = @{@"name":name, @"key":key, @"filename":filename, @"data":image_data};
    NSArray *list = @[dictionary];

    [self.tm uploadImageFiles:list dataBlock:^(NSDictionary *data) {
        if (data) {
            NSString *url = [NSString stringWithFormat:@"%@", data[@"url"] ];
            NSString *mo_key = [self keyForLabel:key_label];
            NSDictionary *mo_data = @{mo_key:url};
            NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:self.question_id];
            [self.tm updateQuestion:kQuestionEntity
                          predicate:predicate
                            details:mo_data];
        }
    }];
    
    [self.popover dismissPopoverAnimated:YES];
}

- (NSString *)keyForLabel:(NSString *)label {
    
    NSString *key = @"image_one";
    
    if ([label isEqualToString:@"1"]) {
        key = @"image_one";
    }
    
    if ([label isEqualToString:@"2"]) {
        key = @"image_two";
    }
    
    if ([label isEqualToString:@"3"]) {
        key = @"image_three";
    }

    if ([label isEqualToString:@"4"]) {
        key = @"image_four";
    }
    
    return key;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    self.questionImageOption = TGQuestionImageOptionValueDefault;
}

- (void)imageTestUploadAction:(UIButton *)button {
    
    NSLog(@"-----> %s", __PRETTY_FUNCTION__);
    
    NSDictionary *d1 = [self generateImageDataWithName:@"abstract_icon1" key:@"abstract_icon1" filename:@"abstract_icon1.png"];
    NSDictionary *d2 = [self generateImageDataWithName:@"abstract_icon2" key:@"abstract_icon2" filename:@"abstract_icon2.png"];
    NSDictionary *d3 = [self generateImageDataWithName:@"abstract_icon3" key:@"abstract_icon3" filename:@"abstract_icon3.png"];
    NSDictionary *d4 = [self generateImageDataWithName:@"abstract_icon4" key:@"abstract_icon4" filename:@"abstract_icon4.png"];
    NSArray *list = @[d1,d2,d3,d4];
    
    [self.tm uploadImageFiles:list doneBlock:^(BOOL status) {
        //TODO
    }];
}

- (NSDictionary *)generateImageDataWithName:(NSString *)name key:(NSString *)key filename:(NSString *)filename {
    
    UIImage *image = [UIImage imageNamed:filename];
    CGFloat compression = 0.6f;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    NSDictionary *dictionary = @{@"name":name, @"key":key, @"filename":filename, @"data":data};
    return dictionary;
}

@end
