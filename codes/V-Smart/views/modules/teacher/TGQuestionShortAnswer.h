//
//  TGQuestionShortAnswer.h
//  V-Smart
//
//  Created by Ryan Migallos on 10/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGQuestionShortAnswer : UITableViewCell

// Entry Fields
@property (strong, nonatomic) IBOutlet UITextField *titleField;
@property (strong, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (strong, nonatomic) IBOutlet UITextField *pointsField;
@property (strong, nonatomic) IBOutlet UIButton *eraseButton;
@property (strong, nonatomic) IBOutlet UIView *customBackground;

- (void)setObjectData:(NSManagedObject *)object;

@end
