//
//  TBNewTestItemCell.m
//  V-Smart
//
//  Created by Julius Abarra on 01/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TBNewTestItemCell.h"

@interface TBNewTestItemCell ()

@property (strong, nonatomic) IBOutlet UILabel *pointsLabel;
@property (strong, nonatomic) IBOutlet UILabel *itemsLabel;

@end

@implementation TBNewTestItemCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.testStatus.hidden = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)updatePointsLabel:(NSString *)points {
    NSString *label = NSLocalizedString(@"Point", nil);
    
    if ([points floatValue] > 1) {
        label = NSLocalizedString(@"Points", nil);
    }
    
    self.pointsLabel.text = label;
}

- (void)updateItemsLabel:(NSString *)items {
    NSString *label = NSLocalizedString(@"Item", nil);
    
    if ([items floatValue] > 1) {
        label = NSLocalizedString(@"Items", nil);
    }
    
    self.itemsLabel.text = label;
}

@end
