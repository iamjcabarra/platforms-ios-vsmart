//
//  TBDetailViewController.m
//  TestBankVersion2.1
//
//  Created by Julius Abarra on 06/02/2016.
//  Copyright © 2016 Vibe Technologies, Inc. All rights reserved.
//

#import "TBDetailViewController.h"
#import "TBDetailViewSegmentController.h"
#import "TBTestAvailableQuestionsView.h"
#import "TBQuestionItemCell.h"
#import "TBClassHelper.h"
#import "TBContentManager.h"
#import "TestGuruDataManager.h"
#import "UIImageView+WebCache.h"

#import "V_Smart-Swift.h" //SWIFT 2.1.1 IMPLEMENTATION

////////////// STORY BOARD SEGUES CONSTANTS //////////////
static NSString *kTGTB_TESTPLAYINFO_VIEW = @"TGTB_TESTPLAYINFO_VIEW";

////////////// NSSTRING HTML CATEGORY IMPLEMENTATION //////////////

@interface NSString (HtmlCustomCheck)
- (BOOL)containsHTML;
@end

@implementation NSString (HtmlCustomCheck)

- (BOOL)containsHTML {
    NSString *stringcopy = [self copy];
    NSString *expression = @"<[^>]+>";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:stringcopy
                                                        options:0
                                                          range:NSMakeRange(0, [stringcopy length])];
    if (numberOfMatches == 1) {
        return YES;
    }
    
    return NO;
}

@end

////////////// NSSTRING HTML CATEGORY IMPLEMENTATION //////////////

@interface TBDetailViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) TBClassHelper *classHelper;
@property (strong, nonatomic) TBContentManager *tbcm;
@property (strong, nonatomic) TestGuruDataManager *tgdm;

@property (strong, nonatomic) TBDetailViewSegmentController *segmentViewController;
@property (strong, nonatomic) TGTBTestPlayLandingView *testPlayLanding; //SWIFT 2.1.1 IMPLEMENTATION

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (strong, nonatomic) IBOutlet UILabel *sectionALabel;
@property (strong, nonatomic) IBOutlet UILabel *sectionBLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionTableMessage;

@property (strong, nonatomic) IBOutlet UIButton *collapseButton;
@property (strong, nonatomic) IBOutlet UIButton *addQuestionButton;
@property (strong, nonatomic) IBOutlet UIButton *saveAndCloseButton;
@property (strong, nonatomic) IBOutlet UIButton *previewButton;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *collapsibleViewHeight;
@property (strong, nonatomic) IBOutlet UITableView *questionTable;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (assign, nonatomic) BOOL isSectionACollapsed;


//@property (strong, nonatomic) TGTBTestPlayInfoView *testing; //SWIFT 2.1.1 IMPLEMENTATION

@end

@implementation TBDetailViewController

static CGFloat kZeroContainerHeight = 000.0f;
static CGFloat kWithContainerHeight = 450.0f;

static NSString *kQuestionCellIdentifer = @"questionCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Content Manager
    self.tbcm = [TBContentManager sharedInstance];
    [self.tbcm recreateObjects];
    self.tbcm.actionType = [NSNumber numberWithInt:self.actionType];
    
    // Test Guru Data Manager
    self.tgdm = [TestGuruDataManager sharedInstance];
    
    // Navigation View
    [self setUpNavigationView];
    
    // Default View
    [self setUpDefaultViewForTestDetail];
    
    // Action for Segmented Control
    [self.segmentedControl addTarget:self
                              action:@selector(segmentedControlAction:)
                    forControlEvents:UIControlEventValueChanged];
    
    // Action for Preview Control //SWIFT 2.1.1 IMPLEMENTATION
    [self.previewButton addTarget:self
                           action:@selector(previewButtonAction:)
                 forControlEvents:UIControlEventTouchUpInside];
    
    // Test Contents
    [self configureTestContents];
    [self listTestQuestionData];
    
    // Selected Question View
    [self setUpSelectedQuestionsView];
    
}

//SWIFT 2.1.1 IMPLEMENTATION
- (void)previewButtonAction:(UIButton *)button {
    [self performSegueWithIdentifier:kTGTB_TESTPLAYINFO_VIEW sender:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.tbcm destroyContents];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation View

- (void)setUpNavigationView {
    NSString *title = @"";
    
    if (self.actionType == 1) {
        title = [self.classHelper localizeString:@"Create Test"];
    }
    
    if (self.actionType == 2) {
        title = [self.classHelper localizeString:@"Edit Test"];
    }
    
    self.title = title;
    
    // Back Bar Button
    [self setUpCustomBackBarButton];
    
    // Cancel Bar Button
    [self setUpCustomCancelBarButton];
}

- (void)setUpCustomBackBarButton {
    NSString *title = [NSString stringWithFormat:@"< %@", self.customBackBarButtonTitle];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 200, 42);
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:UIColorFromHex(0X3083FB) forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button addTarget:self action:@selector(backBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
}

- (void)setUpCustomCancelBarButton {
    NSString *title = [self.classHelper localizeString:@"Cancel"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 200, 42);
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:UIColorFromHex(0X3083FB) forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [button addTarget:self action:@selector(backBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = customBarItem;
}

- (void)backBarButtonAction:(id)sender {
    // Just Temporary
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Default View Settings

- (void)setUpDefaultViewForTestDetail {
    // Section Title
    NSString *sectionATitle = [[self.classHelper localizeString:@"Test Info"] uppercaseString];
    NSString *sectionBTitle = [[self.classHelper localizeString:@"Selected Questions"] uppercaseString];
    self.sectionALabel.text = [NSString stringWithFormat:@"   %@", sectionATitle];
    self.sectionBLabel.text = [NSString stringWithFormat:@"   %@", sectionBTitle];
    
    // Question Table Message
    NSString *tableMessage = [self.classHelper localizeString:@"You cannot save this test without at least one question assigned to it."];
    self.questionTableMessage.text = tableMessage;
    
    // Button Title
    NSString *saveAndCloseTitle = [self.classHelper localizeString:@"Save & Close"];
    [self.saveAndCloseButton setTitle:saveAndCloseTitle forState:UIControlStateNormal];
    [self.saveAndCloseButton setTitle:saveAndCloseTitle forState:UIControlStateHighlighted];
    
    NSString *previewTitle = [self.classHelper localizeString:@"Preview"];
    [self.previewButton setTitle:previewTitle forState:UIControlStateNormal];
    [self.previewButton setTitle:previewTitle forState:UIControlStateHighlighted];
    
    // Button Action
    [self.collapseButton addTarget:self
                            action:@selector(collapseSectionAView:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    [self.addQuestionButton addTarget:self
                               action:@selector(showAvailableQuestions:)
                     forControlEvents:UIControlEventTouchUpInside];
    
    // Test Information Section
    self.isSectionACollapsed = YES;
    
    // Update Test Detail View
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        wo.collapsibleViewHeight.constant = kWithContainerHeight;
        [wo.collapseButton setBackgroundImage:[UIImage imageNamed:@"icn_up_arrow"] forState:UIControlStateNormal];
        [self.view setNeedsUpdateConstraints];
        [self.view layoutIfNeeded];
    });
    
    // Segmented Control Title
    NSString *detailsTitle = [[self.classHelper localizeString:@"Details"] uppercaseString];
    NSString *optionsTitle = [[self.classHelper localizeString:@"Options"] uppercaseString];
    [self.segmentedControl setTitle:detailsTitle forSegmentAtIndex:0];
    [self.segmentedControl setTitle:optionsTitle forSegmentAtIndex:1];
}

#pragma mark - Button Action

- (void)collapseSectionAView:(id)sender {
    NSString *imageString;
    
    // Test Information Section
    if (self.isSectionACollapsed) {
        imageString = @"icn_down_arrow";
        self.collapsibleViewHeight.constant = kZeroContainerHeight;
        self.isSectionACollapsed = NO;
    }
    else {
        imageString = @"icn_up_arrow";
        self.collapsibleViewHeight.constant = kWithContainerHeight;
        self.isSectionACollapsed = YES;
    }
    
    // Update Test Detail View
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.5f
                          delay:0.0f
         usingSpringWithDamping:1.0f
          initialSpringVelocity:0.5f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.collapseButton setBackgroundImage:[UIImage imageNamed:imageString] forState:UIControlStateNormal];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

- (void)showAvailableQuestions:(id)sender {
    [self performSegueWithIdentifier:@"showAvailableQuestions" sender:nil];
}

#pragma mark - Test Info Segment Views

- (void)segmentedControlAction:(id)sender {
    // Test Details
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        __weak typeof(self) wo = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.segmentViewController swapViews:@"embedTestDetails"];
        });
    }
    // Test Options
    if (self.segmentedControl.selectedSegmentIndex == 1) {
        __weak typeof(self) wo = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.segmentViewController swapViews:@"embedTestOptions"];
        });
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Segment View
    if ([segue.identifier isEqualToString:@"embedSegmentView"]) {
        self.segmentViewController = [segue destinationViewController];
    }
    
    // Available Questions
    if ([segue.identifier isEqualToString:@"showAvailableQuestions"]) {
        TBTestAvailableQuestionsView *availableQuestions = (TBTestAvailableQuestionsView *)[segue destinationViewController];
    }
    
        //SWIFT 2.1.1 IMPLEMENTATION
    if ([segue.identifier isEqualToString:kTGTB_TESTPLAYINFO_VIEW]) {
        self.testPlayLanding = (TGTBTestPlayLandingView *)[segue destinationViewController];
        self.testPlayLanding.testObject = self.testObject;
    }

}

#pragma mark - Selected Questions View

- (void)setUpSelectedQuestionsView {
    // Set Protocols
    self.questionTable.dataSource = self;
    self.questionTable.delegate = self;
    
    // Allow Selection
    self.questionTable.allowsSelection = YES;
    self.questionTable.allowsMultipleSelection = NO;
    
    // Default Height
    self.questionTable.rowHeight = 215.0f;

    // Resusable Cell
    UINib *questionItemCellNib = [UINib nibWithNibName:@"TBQuestionItemCell" bundle:nil];
    [self.questionTable registerNib:questionItemCellNib forCellReuseIdentifier:kQuestionCellIdentifer];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    return count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo name];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.questionTable = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kQuestionCellIdentifer forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TBQuestionItemCell *questionCell = (TBQuestionItemCell *)cell;
    [self configureQuestionCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(TBQuestionItemCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    NSString *name = [mo valueForKey:@"name"];
    NSString *date_modified = [mo valueForKey:@"date_modified"];
    NSString *date_string = [self.classHelper transformDateString:date_modified
                                                              fromFormat:@"yyyy-MM-dd HH:mm:ss"
                                                                toFormat:@"EEE. MMM. dd, yyyy hh:mm a"];
    
    NSString *question_text = [mo valueForKey:@"question_text"];
    NSString *questionTypeName = [mo valueForKey:@"questionTypeName"];
    NSString *question_type_id = [mo valueForKey:@"question_type_id"];
    NSString *proficiency_level_id = [mo valueForKey:@"proficiency_level_id"];
    NSString *proficiency_name = [mo valueForKey:@"proficiencyLevelName"];
    
    cell.questionImage.image = nil;
    
    NSString *firstImage = [self getFirstImage:mo];
    if (firstImage.length > 0) {
        [cell.questionImage sd_setImageWithURL:[NSURL URLWithString:firstImage] ];
    }
    
    cell.questionTitleLabel.text = name;
    cell.dateLabel.text = [date_string uppercaseString];
    cell.questionTextLabel.text = question_text;
    cell.questionTextLabel.hidden = YES;
    
    if ([question_text containsHTML]) {
        NSData *htmlData = [question_text dataUsingEncoding:NSUnicodeStringEncoding];
        NSDictionary *options = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType};
        NSAttributedString *attributed_string = [[NSAttributedString alloc] initWithData:htmlData
                                                                                 options:options
                                                                      documentAttributes:nil
                                                                                   error:nil];
        cell.questionTextLabel.attributedText = attributed_string;
    }
    
    [cell loadWebViewWithContents:question_text];
    
    cell.questionTypeLabel.text = questionTypeName;
    cell.questionDifficultyLabel.text = proficiency_name;
    
    [cell updateQuestionTypeView:question_type_id];
    [cell updateDifficultyLevelView:proficiency_level_id];
}

- (NSString *)getFirstImage:(NSManagedObject *)mo {
    if (mo != nil) {
        NSString *image_one = [mo valueForKey:@"image_one"];
        if (image_one.length > 0) {
            return image_one;
        }
        
        NSString *image_two = [mo valueForKey:@"image_two"];
        if (image_two.length > 0) {
            return image_two;
        }
        
        NSString *image_three = [mo valueForKey:@"image_three"];
        if (image_three.length > 0) {
            return image_three;
        }
        
        NSString *image_four = [mo valueForKey:@"image_four"];
        if (image_four.length > 0) {
            return image_four;
        }
    }

    return @"";
}

#pragma mark - Table View Delegate

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [self initiateDelete:mo];
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *actionString = [self.classHelper localizeString:@"Remove"];
    return actionString;
}

- (void)initiateDelete:(NSManagedObject *)mo {
    NSString *avTitle = [[self.classHelper localizeString:@"Remove"] uppercaseString];
    NSString *message = [self.classHelper localizeString:@"Are you sure you want to remove this question from this test?"];
    NSString *posResp = [self.classHelper localizeString:@"Yes"];
    NSString *negResp = [self.classHelper localizeString:@"No"];
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:avTitle
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *posAlertAction = [UIAlertAction actionWithTitle:posResp
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               [self.tgdm.mainContext deleteObject:mo];
                                                               [self reloadFetchedResultsController];
                                                           }];
    
    UIAlertAction *negAlertAction = [UIAlertAction actionWithTitle:negResp
                                                             style:UIAlertActionStyleDefault
                                                           handler:nil];
    [alertController addAction:posAlertAction];
    [alertController addAction:negAlertAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    BOOL isAscending = NO;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tgdm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionBankEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:isAscending];
    [fetchRequest setSortDescriptors:@[date_modified]];
   
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.questionTable beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.questionTable insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.questionTable deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.questionTable;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.questionTable endUpdates];
}

#pragma mark - Reloading of Results

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.questionTable reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

#pragma mark - Content Manager Configuration

- (void)configureTestContents {
    // Create Test
    if (self.actionType == 1) {
        // Test Details
        NSString *max_attempts = [self.classHelper stringValue:kTestMinAttempts];
        NSString *time_limit = [self.classHelper stringValue:kTestMinTimeLimit];
        NSString *start_date = [self.classHelper getDateTodayWithFormat:@"EEE, dd MMM yyyy, hh:mm a"];
        NSString *end_date = [self.classHelper addDayToDate:[NSDate date]
                                               numberOfDays:1
                                             formatToReturn:@"EEE, dd MMM yyyy, hh:mm a"];
        NSString *has_passing_score = [self.classHelper stringValue:kTestHasPassingScore];
        NSString *no_expiry = [self.classHelper stringValue:kTestHasExpiry];
        NSString *stage_id = [self.classHelper stringValue:kTestIsGraded];
        
        [self.tbcm.testDetails setValue:@"" forKey:@"title"];
        [self.tbcm.testDetails setValue:@"" forKey:@"general_instructions"];
        [self.tbcm.testDetails setValue:max_attempts forKey:@"max_attempts"];
        [self.tbcm.testDetails setValue:time_limit forKey:@"time_limit"];
        [self.tbcm.testDetails setValue:start_date forKey:@"start_date"];
        [self.tbcm.testDetails setValue:end_date forKey:@"end_date"];
        [self.tbcm.testDetails setValue:has_passing_score forKey:@"has_passing_score"];
        [self.tbcm.testDetails setValue:@"" forKey:@"passing_score"];
        [self.tbcm.testDetails setValue:no_expiry forKey:@"no_expiry"];
        [self.tbcm.testDetails setValue:stage_id forKey:@"stage_id"];
        
        // Test Options
        NSString *shuffle_choices = kTestShuffleChoices;
        NSString *shuffle_questions = kTestShuffleQuestions;
        NSString *forced_submit = kTestForcedSubmit;
        NSString *show_score = kTestShowScore;
        NSString *show_result = kTestShowResult;
        NSString *show_result_with_correct_answer = kTestShowResultWCA;
        NSString *show_feedback_messages = kTestShowFeedbacks;
        NSString *set_password = kTestSetPassword;
        
        [self.tbcm.testOptions setValue:shuffle_choices forKey:@"shuffle_choices"];
        [self.tbcm.testOptions setValue:shuffle_questions forKey:@"shuffle_questions"];
        [self.tbcm.testOptions setValue:forced_submit forKey:@"forced_submit"];
        [self.tbcm.testOptions setValue:show_score forKey:@"show_score"];
        [self.tbcm.testOptions setValue:show_result forKey:@"show_result"];
        [self.tbcm.testOptions setValue:show_result_with_correct_answer forKey:@"show_result_with_correct_answer"];
        [self.tbcm.testOptions setValue:show_feedback_messages forKey:@"show_feedback_messages"];
        [self.tbcm.testOptions setValue:set_password forKey:@"set_password"];
        [self.tbcm.testOptions setValue:@"" forKey:@"password"];
    }
    
    // Edit Test
    if (self.actionType == 2) {
        
    }
}

- (void)listTestQuestionData {
    // Create managed object
    NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kQuestionBankEntity inManagedObjectContext:self.tgdm.mainContext];
    
    // Save to core data
    [mo setValue:@"name" forKey:@"name"];
    [mo setValue:@"2016-02-10 12:00:00" forKey:@"date_modified"];
    [mo setValue:@"Question" forKey:@"question_text"];
    [mo setValue:@"Multiple Choice" forKey:@"questionTypeName"];
    [mo setValue:@"1" forKey:@"question_type_id"];
    [mo setValue:@"1" forKey:@"proficiency_level_id"];
    [mo setValue:@"Easy" forKey:@"proficiencyLevelName"];
    [mo setValue:@"hello" forKey:@"image_one"];
    [mo setValue:@"10" forKey:@"points"];
    
    [self saveTreeContext:self.tgdm.mainContext];
}

- (void)saveTreeContext:(NSManagedObjectContext *)context {
    if (!context) {
        return;
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"error saving: %@", error);
    }
    
    if (context.parentContext) {
        [self saveTreeContext:context.parentContext];
    }
}

@end
