//
//  QuestionSamplerPageHeader.m
//  V-Smart
//
//  Created by Ryan Migallos on 03/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "QuestionSamplerPageHeader.h"

@interface QuestionSamplerPageHeader()

@property (strong, nonatomic) IBOutlet UILabel *questionTitle;
@property (strong, nonatomic) IBOutlet UILabel *questionType;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIView *difficultyView;
@property (strong, nonatomic) IBOutlet UILabel *pageLabel;

@end

@implementation QuestionSamplerPageHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)displayObject:(NSManagedObject *)object withIndexRow:(NSInteger)indexRow withTotalRow:(NSInteger)totalRow {
    
    NSManagedObject *mo = object;
    
    NSString *proficiency_level_id = [mo valueForKey:@"proficiency_level_id"];
    NSString *question_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"name"] ];
    NSString *question_type_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"questionTypeName"] ];
    NSString *question_type_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"question_type_id"] ];

    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        wo.questionTitle.text = question_name;
        wo.questionType.text = question_type_name;
        wo.pageLabel.text = [NSString stringWithFormat:@"%ld / %ld", (long)indexRow + 1, (long)totalRow];
        [wo setQuestionTypeID:question_type_id];
        [wo setupDifficultyView:proficiency_level_id];
    });
}

- (void)setupDifficultyView:(NSString *)proficiency_level_id {
        UIColor *easyColor = UIColorFromHex(0x68BF61);
        UIColor *moderateColor = UIColorFromHex(0xFFCD02);
        UIColor *difficultColor = UIColorFromHex(0xF8931F);
    
        UIColor *color = [UIColor whiteColor];
    
        //EASY
        if ([proficiency_level_id isEqualToString:@"1"]) {
            color = easyColor;
        }
    
        //MODERATE
        if ([proficiency_level_id isEqualToString:@"2"]) {
            color = moderateColor;
        }
    
        //DIFFICULT
        if ([proficiency_level_id isEqualToString:@"3"]) {
            color = difficultColor;
        }

        self.difficultyView.backgroundColor = color;
}

- (void)setQuestionTypeID:(NSString *)question_type_id {
    
    self.imageView.image = nil;
    
    NSString *image_string = @"icon_true_or_false_150px";
    
    //ESSAY
    if ([question_type_id isEqualToString:@"6"]) {
        image_string = @"icon_essay_150px";
    }
    
    //MULTIPLE CHOICE
    if ([question_type_id isEqualToString:@"3"]) {
        image_string = @"icon_multiple_choice_150px";
    }
    
    //SHORT ANSWER
    if ([question_type_id isEqualToString:@"9"]) {
        image_string = @"icon_short_answer_150px";
    }
    
    //TRUE OR FALSE
    if ([question_type_id isEqualToString:@"1"]) {
        image_string = @"icon_true_or_false_150px";
    }
    
    self.imageView.image = [UIImage imageNamed:image_string];
}

@end
