//
//  DeployedTestItemCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/28/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeployedTestItemCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *testName;
@property (strong, nonatomic) IBOutlet UILabel *testDescription;
@property (strong, nonatomic) IBOutlet UILabel *testCount;

@end
