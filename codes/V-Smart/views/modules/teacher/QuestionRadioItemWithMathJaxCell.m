//
//  QuestionRadioItemWithMathJaxCell.m
//  V-Smart
//
//  Created by Carmelito Bayarcal on 01/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "QuestionRadioItemWithMathJaxCell.h"
#import "TestGuruDataManager.h"

@implementation QuestionRadioItemWithMathJaxCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.webView.delegate = self;
    
    self.webView.scrollView.scrollEnabled = NO;
    self.webView.scrollView.scrollsToTop = NO;
    
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView setOpaque:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)reconfigureWebViewForTestResults:(BOOL)reconfigure {
    if (reconfigure) {
        self.webView.scrollView.scrollEnabled = YES;
        self.webView.scrollView.scrollsToTop = YES;
        self.webViewHeight.constant = 150.0f;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self layoutIfNeeded];
        });
    }
}

- (void)loadWebViewWithContents:(NSString *)string {
    
    
    NSString *htmlStartContent = @"<!DOCTYPE html><html><head><meta name='viewport' content='initial-scale=1.5'/><style> p{ font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: 500; font-size: 20px; }</style><meta charset='UTF-8'></head><body><script type='text/javascript'>window.onload = function() { window.location.href = 'ready://' + document.body.offsetHeight;}</script><p>";
    
    NSString *htmlEndContent = @"</p></body></html>";
    
    NSString *html_string = [NSString stringWithFormat:@"%@%@%@", htmlStartContent, string ,htmlEndContent];
    
    //    myWebView.loadHTMLString(myHTML, baseURL:nil)
    [self.webView loadHTMLString:html_string baseURL:nil];
}

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
//    NSURL *url = [request URL];
//    if (navigationType == UIWebViewNavigationTypeOther) {
//        if ([[url scheme] isEqualToString:@"ready"]) {
//            float contentHeight = [[url host] floatValue];
//            //                        CGRect fr = webView.frame;
//            //                        fr.size = CGSizeMake(webView.frame.size.width, contentHeight);
//            //                        webView.frame = fr;
//            CGFloat heightConstant = self.webViewHeight.constant;
//            
//            self.webViewHeight.constant = contentHeight + 40;
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [self layoutIfNeeded];
//            });
//            
//            if (heightConstant != contentHeight + 40) {
//                if ([(NSObject *)self.delegate respondsToSelector:@selector(didFinishloadingChoice:)]) {
//                    [self.delegate didFinishloadingChoice:self.indexPath];
//                }
//            }
//            
//            return NO;
//        }
//    }
//    return YES;
    
    if (self.isForTestViewResults == NO) {
        NSURL *url = [request URL];
        if (navigationType == UIWebViewNavigationTypeOther) {
            if ([[url scheme] isEqualToString:@"ready"]) {
                float contentHeight = [[url host] floatValue];
                CGFloat heightConstant = self.webViewHeight.constant;
                
                self.webViewHeight.constant = contentHeight + 40;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self layoutIfNeeded];
                });
                
                if (heightConstant != contentHeight + 40) {
                    if ([(NSObject *)self.delegate respondsToSelector:@selector(didFinishloadingChoice:)]) {
                        [self.delegate didFinishloadingChoice:self.indexPath];
                    }
                }
                
                return NO;
            }
        }
    }
    
    return YES;
}

-(void)prepareForReuse {
    [super prepareForReuse];
    //    self.webViewHeight.constant = 18;
    //    [self layoutIfNeeded];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    NSString *fontString = [[NSUserDefaults standardUserDefaults] objectForKey:kCP_SELECTED_FONT_SIZE];

    if (fontString == nil) {
        fontString = @"20";
    }
    
    CGFloat fontSize = fontString.floatValue;
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%f%%'", fontSize*5];
    [self.webView stringByEvaluatingJavaScriptFromString:jsString];
    
    [webView stopLoading];
    webView = nil;
}

@end
