//
//  TGQuestionEssay.m
//  V-Smart
//
//  Created by Ryan Migallos on 10/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TGQuestionEssay.h"
#import "TestGuruDataManager.h"

@interface TGQuestionEssay() <UITextFieldDelegate, UITextViewDelegate>

// Localization Items
@property (strong, nonatomic) IBOutlet UILabel *titleIndicator;
@property (strong, nonatomic) IBOutlet UILabel *descriptionIndicator;
@property (strong, nonatomic) IBOutlet UILabel *pointsIndicator;
@property (strong, nonatomic) IBOutlet UILabel *imageIndicator;

// Text View Items
@property (strong, nonatomic) IBOutlet UIView *textViewContainer;
@property (strong, nonatomic) IBOutlet UILabel *descriptionPlaceholder;

// Image Placeholders
@property (strong, nonatomic) IBOutlet UIImageView *imageViewOne;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewThree;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewFour;

// Image Buttons
@property (strong, nonatomic) IBOutlet UIButton *buttonOne;
@property (strong, nonatomic) IBOutlet UIButton *buttonTwo;
@property (strong, nonatomic) IBOutlet UIButton *buttonThree;
@property (strong, nonatomic) IBOutlet UIButton *buttonFour;

@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) NSString *question_id;
@property (strong, nonatomic) NSManagedObject *mo;

@end

@implementation TGQuestionEssay

- (void)awakeFromNib {
    
    self.tm = [TestGuruDataManager sharedInstance];
    // Initialization code
    
    NSString *title_string = NSLocalizedString(@"Title", nil);
    self.titleIndicator.text = [title_string uppercaseString];
    
    NSString *question_string = NSLocalizedString(@"Question", nil);
    self.descriptionIndicator.text = [question_string uppercaseString];
    
    NSString *points_string = NSLocalizedString(@"Points", nil);
    self.pointsIndicator.text = [points_string uppercaseString];
    
    self.titleField.placeholder = NSLocalizedString(@"Please enter a title", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setObjectData:(NSManagedObject *)object {
    
    self.mo = object;
    self.question_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"id"] ];
    
    // Hide place holder
    if (self.descriptionLabel.text.length > 0) {
        self.descriptionPlaceholder.hidden = YES;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // verify max length has not been exceeded
    NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString *originalText = [NSString stringWithFormat:@"%@", textField.text];
    
    if ( textField == self.pointsField ) {
        
        // allow backspace
        if (!string.length)
        {
            return YES;
        }
        
        NSCharacterSet *numberSet = [NSCharacterSet decimalDigitCharacterSet];
        if ([string rangeOfCharacterFromSet:[numberSet invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        
        if (updatedText.length > 3) // 4 was chosen for SSN verification
        {
            // suppress the max length message only when the user is typing
            // easy: pasted data has a length greater than 1; who copy/pastes one character?
            if (string.length > 1)
            {
                // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
            }
            return NO;
        }
        
        if (self.mo != nil) {
            
            //QUESTION POINTS STORE TO CORE DATA
            [self updateDetailsWithValue:originalText andKey:@"points"];
        }
        
    }
    
    if ( textField == self.titleField ) {
        
        if (self.mo != nil) {
            
            //QUESTION TITLE STORE TO CORE DATA
            [self updateDetailsWithValue:originalText andKey:@"name"];
        }
    }
    
    return YES;
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    
    NSString *updatedText = [NSString stringWithFormat:@"%@", textView.text];
    if (self.mo != nil) {
        //QUESTION DESCRIPTION STORE TO CORE DATA
        [self updateDetailsWithValue:updatedText andKey:@"question_text"];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.descriptionPlaceholder.hidden = YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {

    NSString *updated_string = [NSString stringWithFormat:@"%@", textView.text];
    if (updated_string.length > 1) {
        self.descriptionPlaceholder.hidden = YES;
    }
    
    return YES;
}

- (void)updateDetailsWithValue:(NSString *)value andKey:(NSString *)key {
    
    NSString *entity = kQuestionEntity;
    
    NSDictionary *userData = @{key:value};
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:self.question_id];
    [self.tm updateEntity:entity details:userData predicate:predicate];
}

@end
