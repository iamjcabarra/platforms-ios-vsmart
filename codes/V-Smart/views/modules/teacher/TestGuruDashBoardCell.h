//
//  TestGuruDashBoardCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 7/20/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruDashBoardCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *testModuleImageView;
@property (strong, nonatomic) IBOutlet UILabel *testModuleTitle;

@end
