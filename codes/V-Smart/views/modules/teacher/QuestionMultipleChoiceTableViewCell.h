//
//  QuestionMultipleChoiceTableViewCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 7/30/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionMultipleChoiceTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextField *questionField;
@property (strong, nonatomic) IBOutlet UITextField *questionPointsField;
@property (strong, nonatomic) IBOutlet UITextView *questionTextArea;
@property (strong, nonatomic) IBOutlet UIButton *eraseButton;
@property (assign, nonatomic) BOOL isEditMode;
@property (assign, nonatomic) BOOL isCreateAction;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalContraintMultipleChoice;
@property (weak, nonatomic) IBOutlet UIImageView *questionImageView;

- (void)choicesForObject:(NSManagedObject *)mo;
- (NSManagedObject *)saveContents:(NSDictionary *)object;

@end
