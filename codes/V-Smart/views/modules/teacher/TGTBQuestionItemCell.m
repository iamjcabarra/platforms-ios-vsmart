//
//  TGTBQuestionItemCell.m
//  V-Smart
//
//  Created by Julius Abarra on 08/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGTBQuestionItemCell.h"

@interface TGTBQuestionItemCell () <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIView *difficultyView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *checkBoxLeadingConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *webViewLeadingConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *questionDescriptionConstraint;

@end

@implementation TGTBQuestionItemCell

- (void)awakeFromNib {
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView setOpaque:NO];
    self.webView.delegate = self;
    self.webView.scrollView.scrollEnabled = NO;
}

- (void)loadWebViewWithContent:(NSString *)content {
    NSString *htmlStartContent = @"<!DOCTYPE html><html><head><meta name='viewport' content='initial-scale=1.5'/><style> p{font-family:'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight:150; font-size:20px; text-align:justify}</style><meta charset='UTF-8'></head><body><script type='text/javascript'>window.onload=function(){window.location.href='ready://'+document.body.offsetHeight;}</script><p>";
    NSString *htmlEndContent = @"</p></body></html>";
    NSString *htmlString = [NSString stringWithFormat:@"%@%@%@", htmlStartContent, content, htmlEndContent];
    [self.webView loadHTMLString:htmlString baseURL:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [webView stopLoading];
    webView = nil;
}

- (void)updateDifficultyLevelView:(NSString *)levelID {
    UIColor *color = UIColorFromHex(0x68BF61);
    
    // Easy
    if ([levelID isEqualToString:@"1"]) {
        color = UIColorFromHex(0x68BF61);
    }
    
    // Moderate
    if ([levelID isEqualToString:@"2"]) {
        color = UIColorFromHex(0xFFCD02);
    }
    
    // Difficult
    if ([levelID isEqualToString:@"3"]) {
        color = UIColorFromHex(0xF8931F);
    }
    
    self.difficultyView.backgroundColor = color;
    self.containerView.backgroundColor = color;
}

- (void)updatePackageImage:(NSString *)packageID {
    NSString *imageString = @"icn_user_defined.png";
    
    // Free
    if ([packageID isEqualToString:@"1"]) {
        imageString = @"free-50px.png";
    }
    
    // Premium
    if ([packageID isEqualToString:@"2"]) {
        imageString = @"premium-50px.png";
    }
    
    // User-Defined
    if ([packageID isEqualToString:@"3"]) {
        imageString = @"user_define-50px.png";
    }
    
    self.packageTypeImage.image = [UIImage imageNamed:imageString];
}

- (void)shouldHideCheckbox:(BOOL)hide {
    if (hide) {
        self.checkBoxButton.hidden = YES;
        self.checkBoxLeadingConstraint.constant = -15;
        self.webViewLeadingConstraint.constant = 20;
        self.questionDescriptionConstraint.constant = 20;
    }
    else {
        self.checkBoxButton.hidden = NO;
        self.checkBoxLeadingConstraint.constant = 13;
        self.webViewLeadingConstraint.constant = 41;
        self.questionDescriptionConstraint.constant = 41;
    }
}

- (void)updatePoints:(NSString *)points {
    NSString *pointString = NSLocalizedString(@"Points", nil);
    CGFloat pointInt = [points floatValue];
    
    if (pointInt <= 1.0) {
        pointString = NSLocalizedString(@"Point", nil);
    }
    
    self.questionPointLabel.text = pointString;
    self.questionPointValue.text = points;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end