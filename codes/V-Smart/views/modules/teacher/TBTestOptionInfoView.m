//
//  TBTestOptionInfoView.m
//  V-Smart
//
//  Created by Julius Abarra on 15/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TBTestOptionInfoView.h"

@interface TBTestOptionInfoView ()


@property (strong, nonatomic) IBOutlet UILabel *optionTitleLabel;
@property (strong, nonatomic) IBOutlet UITextView *optionInfoTextView;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;

@end

@implementation TBTestOptionInfoView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.closeButton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)closeAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)setTestOptionInfoWithTitle:(NSString *)title andBody:(NSString *)body {
    self.optionTitleLabel.text = title;
    self.optionInfoTextView.text = body;
}

@end
