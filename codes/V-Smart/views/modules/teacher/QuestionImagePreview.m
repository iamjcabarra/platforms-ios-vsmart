//
//  QuestionImagePreview.m
//  V-Smart
//
//  Created by Carmelito Bayarcal on 04/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "QuestionImagePreview.h"
#import "UIImageView+WebCache.h"

@interface QuestionImagePreview () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *questionImage;
@property (weak, nonatomic) IBOutlet UILabel *questionText;
@property (weak, nonatomic) IBOutlet UIButton *previousButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property (assign, nonatomic) NSInteger arrayCount;
@property (strong, nonatomic) NSOrderedSet *array;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation QuestionImagePreview

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.previousButton addTarget:self action:@selector(previousButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.nextButton addTarget:self action:@selector(nextButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
//    NSString *type = [NSString stringWithFormat:@"%@", self.previewData[@"type"] ];
    NSOrderedSet *imageArray = [NSOrderedSet orderedSetWithOrderedSet:self.previewData[@"content"] ];
    self.array = imageArray;
    self.arrayCount = imageArray.count;
    
    if (self.arrayCount == 1) {
        self.previousButton.hidden = YES;
        self.nextButton.hidden = YES;
    } else {
        [self setUpButtons];
    }
    
    [self setUpPreview:self.index];
    
    self.scrollView.delegate = self;
    self.scrollView.minimumZoomScale = 1.0;
    self.scrollView.maximumZoomScale = 5.0;

//    if ([type isEqualToString:@"question"]) {
//        
//        NSLog(@"imageArray [%@]", imageArray);
//        NSLog(@"SELF.INDEX [%ld]", (long)self.index);
//        
//        NSInteger i = self.index - 1;
//        NSDictionary *imageDict = imageArray[i];
//        
//        NSURL *imageURL = [NSURL URLWithString:imageDict[@"data"]];
//        
//        self.questionText.hidden = YES;
//        [self.questionImage setImageWithURL:imageURL];
//    } else {
//        NSLog(@"IMAGE ARRAY %@", imageArray);
//        NSLog(@"SELF.INDEX [%ld]", (long)self.index);
//        
//        NSInteger i = self.index - 1;
//        NSDictionary *imageDict = imageArray[i];
//        
//        NSURL *imageURL = [NSURL URLWithString:imageDict[@"choice_image"]];
//        
//        [self.questionImage setImageWithURL:imageURL];
//        self.questionText.hidden = NO;
//        self.questionText.text = [NSString stringWithFormat:@"%@", imageDict[@"choice_title"]];
//    }
}

- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.questionImage;
}

- (void)setUpButtons {
    NSInteger index_dup = self.index + 1;
    NSLog(@"INDEX DUP %ld", (long)index_dup);
    NSLog(@"ARRAY COUNT %ld", (long)self.arrayCount);
    if (index_dup <= self.arrayCount) {
        self.nextButton.hidden = NO;
    } else {
        self.nextButton.hidden = YES;
    }
    
    if ((index_dup - 1) != 1) {
        self.previousButton.hidden = NO;
    } else {
        self.previousButton.hidden = YES;
    }
}

- (void)previousButtonAction:(UIButton *)sender {
    self.index --;
    [self setUpPreview:self.index];
    [self setUpButtons];
}

- (void)nextButtonAction:(UIButton *)sender {
    self.index ++;
    [self setUpPreview:self.index];
    [self setUpButtons];
}

- (void)setUpPreview:(NSInteger)index {
    NSInteger i = index - 1;
    
    NSDictionary *imageDict = self.array[i];
    
    NSURL *imageURL = [NSURL URLWithString:imageDict[@"data"]];
    
    
    if ([[imageURL scheme] isEqualToString:@"assets-library"]) {
        NSData *image_data = [NSData dataWithData:imageDict[@"image_data"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.questionImage.image = [UIImage imageWithData:image_data];
        });
    } else {
        [self.questionImage sd_setImageWithURL:imageURL];
    }
    
    NSString *choice_title = imageDict[@"choice_title"];
    
    self.questionText.hidden = YES;
    if (choice_title != nil) {
        self.questionText.hidden = YES;
        self.questionText.text = [NSString stringWithFormat:@"%@", imageDict[@"choice_title"]];
    }
}

- (IBAction)buttonCloseAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
