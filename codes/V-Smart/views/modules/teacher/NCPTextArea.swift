//
//  NCPTextArea.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 27/10/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class NCPTextField: UITextField {
    @IBInspectable public var PasteEnable: Bool = true
    @IBInspectable public var CopyEnabled: Bool = true
    @IBInspectable public var CutEnabled: Bool = true
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return PasteEnable
        }
        if action == #selector(UIResponderStandardEditActions.copy(_:)) {
            return CopyEnabled
        }
        if action == #selector(UIResponderStandardEditActions.cut(_:)) {
            return CutEnabled
        }
        
        return super.canPerformAction(action, withSender: sender)
    }
}

@IBDesignable
class NCPTextView: UITextView {
    @IBInspectable public var PasteEnable: Bool = true
    @IBInspectable public var CopyEnabled: Bool = true
    @IBInspectable public var CutEnabled: Bool = true
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return PasteEnable
        }
        if action == #selector(UIResponderStandardEditActions.copy(_:)) {
            return CopyEnabled
        }
        if action == #selector(UIResponderStandardEditActions.cut(_:)) {
            return CutEnabled
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
