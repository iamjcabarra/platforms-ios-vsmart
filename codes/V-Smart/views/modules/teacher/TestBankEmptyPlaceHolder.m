//
//  TestBankEmptyPlaceHolder.m
//  V-Smart
//
//  Created by Julius Abarra on 27/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestBankEmptyPlaceHolder.h"

@interface TestBankEmptyPlaceHolder ()

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UIButton *createButton;

@end

@implementation TestBankEmptyPlaceHolder

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.imageView.image = [UIImage imageNamed:@"test_placeholder"];
    
    NSString *buttonTitle = NSLocalizedString(@"Create", nil);
    [self.createButton setTitle:buttonTitle forState:UIControlStateNormal];
    [self.createButton setTitle:buttonTitle forState:UIControlStateHighlighted];
    [self.createButton addTarget:self action:@selector(createActionForButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)createActionForButton:(id)sender {
    if ([(NSObject*)self.delegate respondsToSelector:@selector(didFinishSelectingCreateAction)] ) {
        [self.delegate didFinishSelectingCreateAction];
    }
}
- (void)showEmptyViewWithMessage:(NSString *)message shouldHideCreateButton:(BOOL)hideCreateButton {
    if ([message isEqualToString:@""]) {
        message = NSLocalizedString(@"No available test yet. Create one today.", nil);
    }
    
    self.messageLabel.text = message;
    self.createButton.hidden = hideCreateButton;
}

@end
