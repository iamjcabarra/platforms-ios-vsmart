//
//  TGTMTestCommentViewController.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 28/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol TGTMTestCommentViewDelegate: Dimmable {
    func reloadList()
}

class TGTMTestCommentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, TGTMTestCommentViewDelegate {
    
    var dimView: UIView = UIView()

    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet fileprivate var tableView: UITableView!
    var test_id : String!
    
    fileprivate lazy var tgmDataManager: TestGuruDataManager = {
        let tgdm = TestGuruDataManager.sharedInstance()
        return tgdm!
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Test Comments"
        
        self.tableView.estimatedRowHeight = 300
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.commentButton.addTarget(self, action: #selector(self.commentButtonAction(_:)), for: .touchUpInside)
        
        self.tgmDataManager.requestComments(forTestID: test_id, forPageNumber: "1", withSearchText: "") { (dict) in
            
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.commentButton.scaleAnimate(0)
    }
    
    func commentButtonAction(_ b: UIButton) {
        self.performSegue(withIdentifier: "SHOW_TESTGURU_COMMENT_CREATE", sender: nil)
    }
    
    func reloadList() {
        self.tgmDataManager.requestComments(forTestID: test_id, forPageNumber: "1", withSearchText: "") { (dict) in
            
        }
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            emptyView.isHidden = false
            return 0
        }
        
        if sectionCount == 0 {
            emptyView.isHidden = false
        } else {
            emptyView.isHidden = true
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        if sectionData.numberOfObjects == 0 {
            emptyView.isHidden = false
        } else {
            emptyView.isHidden = true
        }
        
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "test_comment_cell_identifier", for: indexPath) as! TGTMTestCommentCell
        self.configureCell(cell, atIndexPath: indexPath)
        
        return cell
    }
    
    func configureCell(_ cell: TGTMTestCommentCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        
        let name: String! = mo.value(forKey: "name") as! String
        let comment: String! = mo.value(forKey: "comment") as! String
        let avatar: String! = mo.value(forKey: "avatar") as! String
        let formatted_date_modified: String! = mo.value(forKey: "formatted_date_modified") as! String
        let comment_id: String! = mo.value(forKey: "comment_id") as! String
        let c_user_id: String! = mo.value(forKey: "user_id") as! String
        
        self.configureSwipeableButtonForCell(cell, comment_id: comment_id, comment_userID: c_user_id, comment: comment)
        
        cell.nameLabel.text = name
        cell.commentLabel.text = comment
        cell.dateLabel.text = formatted_date_modified
        cell.avatarImageView.clipsToBounds = true
        
        cell.selectionStyle = .none
        
        guard let url = URL(string: avatar) else {
            return
        }
        
        cell.avatarImageView.sd_setImage(with: url)
    }
    
    fileprivate func configureSwipeableButtonForCell(_ cell: TGTMTestCommentCell, comment_id: String, comment_userID: String, comment: String) {
        // Approve Action
        
        let titleA = NSLocalizedString("Edit", comment: "")
        let imageA = UIImage(named:"edit_white.png")
        let colorA = UIColor(rgba: "#2196f3")
        
        let buttonA: MGSwipeButton! = MGSwipeButton(title: titleA, icon: imageA, backgroundColor: colorA, callback: {
            (sender) -> Bool in
            DispatchQueue.main.async(execute: {
                let dict: [String:String] = ["comment_id": comment_id,
                            "comment": comment]
                self.performSegue(withIdentifier: "SHOW_TESTGURU_COMMENT_CREATE", sender: dict)
            })
            
            return true
        })
        
        buttonA.setTitleColor(UIColor.white, for: UIControlState())
        buttonA.centerIconOverText()
        
        // Disapprove Action
        
        let titleB = NSLocalizedString("Delete", comment: "")
        let imageB = UIImage(named:"trashcan_white.png")
        let colorB = UIColor(rgba: "#FF3B30")
        
        let buttonB: MGSwipeButton! = MGSwipeButton(title: titleB, icon: imageB, backgroundColor: colorB, callback: {
            (sender) -> Bool in
            
            let alert = UIAlertController(title: NSLocalizedString("Delete", comment: ""), message: NSLocalizedString("Are you sure you want to delete this comment?", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (alert) in
                HUD.showUIBlockingIndicator(withText: NSLocalizedString("Deleting", comment: ""))
                self.tgmDataManager.deleteComment(comment_id, dataBlock: { (dict) in
                    DispatchQueue.main.async(execute: { 
                        HUD.hideUIBlockingIndicator()
                    })
                    if dict == nil {
                        
                    }else {
                        let errorMessage: String! = dict!["error"] as! String
                        self.displayAlert(withTitle: "", withMessage: errorMessage)
                    }
                })
            }))

            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
            
            return true
        })
        
        buttonB.setTitleColor(UIColor.white, for: UIControlState())
        buttonB.centerIconOverText()
        
        let user_id = self.tgmDataManager.loginUser()
        
        if comment_userID == user_id {
          cell.rightButtons = [buttonB, buttonA]
            cell.rightSwipeSettings.transition = MGSwipeTransition.rotate3D
        }
        
        //cell.updateStatus(testStatus)
    }

    func displayAlert(withTitle title: String, withMessage message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SHOW_TESTGURU_COMMENT_CREATE" {
            let cc = segue.destination as! TGTMTestCommentCreateViewController
            
            cc.test_id = test_id
            cc.delegate = self
            
            guard let dict = sender as? [String:String] else {
                return
            }
            
            cc.comment_id = dict["comment_id"]! as String
            cc.comment = dict["comment"]! as String
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Fetched Results Controller
    
    var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {

        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = self.tgmDataManager.mainContext
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kTestCommentEntity)
        //let fetchRequest = NSFetchRequest(entityName: kTestCommentEntity)
        fetchRequest.fetchBatchSize = 20
        
//        let predicate = NSComparisonPredicate(keyPath: "is_approved", withValue: self.statusID, isExact: true)
//        fetchRequest.predicate = predicate
        
        let sortDescriptor = NSSortDescriptor(key: "index", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.tableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! TGTMTestCommentCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }

}
