//
//  QuestionSamplerView.m
//  V-Smart
//
//  Created by Ryan Migallos on 29/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "QuestionSamplerView.h"
#import "QuestionSamplerPage.h"
#import "TestGuruDataManager.h"
#import "QuestionImagePreview.h"
#import "QuestionSamplerPageHeader.h"

@interface QuestionSamplerView ()
<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, NSFetchedResultsControllerDelegate, QuestionSamplerPageDelegate> {
    NSMutableDictionary *_objectChanges;
    NSMutableDictionary *_sectionChanges;
    dispatch_queue_t queue;
}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) NSMutableDictionary *activeDownload;
@property (assign, nonatomic) BOOL doneScrolling;

@property (strong, nonatomic) NSDictionary *previewData;
@property (assign, nonatomic) NSInteger index;
@property (assign, nonatomic) NSInteger totalItems;

@property (strong, nonatomic) IBOutlet UIButton *customCloseButton;

@property (weak, nonatomic) IBOutlet UIButton *soloEditButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIView *saveAndEditView;

@end

@implementation QuestionSamplerView

static NSString *kSamplerPageIdentifier = @"sample_question_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tm = [TestGuruDataManager sharedInstance];
    queue = dispatch_queue_create("com.vibetech.question.PREVIEW",DISPATCH_QUEUE_SERIAL);
    
    [self setupButtonsInPreview];
    
    if ([[[UIDevice currentDevice] systemVersion] compare:@"10.0" options:NSNumericSearch] != NSOrderedAscending)
    {
        self.collectionView.prefetchingEnabled = NO;
    }
    
    
    //BY DEFAULT
//    self.buttonSave.hidden = ([self.entityObject isEqualToString:kQuestionEntity]) ? NO : YES;
//    [self.buttonSave addTarget:self action:@selector(previewButtonSaveAction:) forControlEvents:UIControlEventTouchUpInside];
}

//- (void)previewButtonSaveAction:(UIButton *)sender {
//    
//    NSLog(@"%s", __PRETTY_FUNCTION__);
//    
//    if ([(NSObject*)self.delegate respondsToSelector:@selector(didFinishSaveInformation:)] ) {
//        [self.delegate didFinishSaveInformation:YES];
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }
//}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    if (self.indexPath != nil) {
        [self.collectionView scrollToItemAtIndexPath:self.indexPath
                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                            animated:NO];
        [self.collectionView reloadData];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)unwindFromConfirmationForm:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0f;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    CGFloat height = collectionView.frame.size.height;
    CGFloat width = collectionView.frame.size.width;
    return CGSizeMake(width, height);
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    NSInteger count = [[self.fetchedResultsController sections] count];
    
    return count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    self.totalItems = [sectionInfo numberOfObjects];
    return self.totalItems;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    QuestionSamplerPage *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kSamplerPageIdentifier forIndexPath:indexPath];
    
    [cell.headerView displayObject:mo withIndexRow:indexPath.row withTotalRow:self.totalItems];
    [cell displayObject:mo];
    
    NSString *package_type_id = [self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID];
    
    cell.separatorView.hidden = ([package_type_id isEqualToString:@"1"]);

    cell.delegate = self;
    
    return cell;
}

- (void)saveButtonAction:(id)sender {
    if ([(NSObject*)self.delegate respondsToSelector:@selector(didFinishSaveInformation:)] ) {
        [self.delegate didFinishSaveInformation:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)editButtonAction:(id)sender {
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.collectionView];
    NSIndexPath *indexPath = [[self.collectionView indexPathsForVisibleItems] lastObject];
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    BOOL checkDelegate = [self.delegate isKindOfClass: NSClassFromString(@"TGQBQuestionDetailViewController")];
    
    if (checkDelegate) {
        if ([(NSObject*)self.delegate respondsToSelector:@selector(didSelectEditIndex:withEditObject:)] ) {
            [self.delegate didSelectEditIndex:indexPath withEditObject:nil];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
    [self.tm deepCopyManagedObject:mo objectBlock:^(NSManagedObject *object) {
        if ([(NSObject*)self.delegate respondsToSelector:@selector(didSelectEditIndex:withEditObject:)] ) {
            [self.delegate didSelectEditIndex:indexPath withEditObject:object];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}
}


- (void)setupButtonsInPreview {

    BOOL soloEditIsHidden = YES;
    BOOL saveAndEditIsHidden = YES;
    
    if (!self.isTestPreview) {
        if (self.entityObject == kQuestionBankEntity) {
            [self.soloEditButton setTitle:NSLocalizedString(@"Edit", nil) forState:UIControlStateNormal];
            [self.soloEditButton addTarget:self action:@selector(editButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            soloEditIsHidden = NO;
            saveAndEditIsHidden = YES;
        } else {
            [self.editButton setTitle:NSLocalizedString(@"Edit", nil) forState:UIControlStateNormal];
            [self.editButton addTarget:self action:@selector(editButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            self.saveButton.enabled = self.isComplete;
            self.saveButton.backgroundColor = (self.isComplete) ? UIColorFromHex(0x68BF61) : [UIColor lightGrayColor];
            
            [self.saveButton setTitle:NSLocalizedString(@"Save & Close", nil) forState:UIControlStateNormal];
            [self.saveButton addTarget:self action:@selector(saveButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            soloEditIsHidden = YES;
            saveAndEditIsHidden = NO;
        }
    } else {
        saveAndEditIsHidden = YES;
        soloEditIsHidden = YES;
    }
    
    NSString *package_type_id = [self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID];
    if ([package_type_id isEqualToString:@"1"]) {
        soloEditIsHidden = YES;
        saveAndEditIsHidden = YES;
    }
    
    self.soloEditButton.hidden = soloEditIsHidden;
    self.saveAndEditView.hidden = saveAndEditIsHidden;

}

- (void)transitionWithData:(NSDictionary *)previewData index:(NSInteger)index segueID:(NSString *)segueIdentifier {
    self.previewData = [NSDictionary dictionaryWithDictionary:previewData];
    self.index = index;
    [self performSegueWithIdentifier:segueIdentifier sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SAMPLE_QUESTION_IMAGE_VIEW"]) {
        QuestionImagePreview *imagePreview = (QuestionImagePreview *)[segue destinationViewController];
        imagePreview.previewData = self.previewData;
        imagePreview.index = self.index;
    }
}

- (void)downloadData:(NSManagedObject *)mo indexPath:(NSIndexPath *)indexPath {

    if (self.activeDownload == nil) {
        self.activeDownload = [NSMutableDictionary dictionary];
    }

    NSIndexPath *index = [self.activeDownload objectForKey:indexPath];
    if (index == nil) {
        __weak typeof(self) wo = self;
        [self.activeDownload setObject:indexPath forKey:indexPath];
        dispatch_async(queue, ^{
            
            if ([self.entityObject isEqualToString:kQuestionBankEntity]) {
                [wo.tm requestDetailsForQuestion:mo doneBlock:nil];
            }
            
            if ([self.entityObject isEqualToString:kQuestionEntity]) {
                [wo.tm requestDetailsForQuestionPreview:mo withNewEntity:kQuestionEntity doneBlock:nil];
            }
            
        });
    }
}

#pragma mark <NSFetchedResultsControllerDelegate>

- (NSFetchedResultsController *)fetchedResultsController
{
//    BOOL isAscending = NO;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;

//    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionBankEntity];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:self.entityObject];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Edit the sort key as appropriate.
//    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"sort_date" ascending:isAscending];
//    NSSortDescriptor *question_text = [NSSortDescriptor sortDescriptorWithKey:@"question_text" ascending:isAscending];
//    NSSortDescriptor *question_id = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:isAscending];
    [fetchRequest setSortDescriptors:self.sortDescriptors];

    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    _objectChanges = [NSMutableDictionary dictionary];
    _sectionChanges = [NSMutableDictionary dictionary];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    if (type == NSFetchedResultsChangeInsert || type == NSFetchedResultsChangeDelete) {
        NSMutableIndexSet *changeSet = _sectionChanges[@(type)];
        if (changeSet != nil) {
            [changeSet addIndex:sectionIndex];
        } else {
            _sectionChanges[@(type)] = [[NSMutableIndexSet alloc] initWithIndex:sectionIndex];
        }
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    NSMutableArray *changeSet = _objectChanges[@(type)];
    if (changeSet == nil) {
        changeSet = [[NSMutableArray alloc] init];
        _objectChanges[@(type)] = changeSet;
    }
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [changeSet addObject:newIndexPath];
            break;
        case NSFetchedResultsChangeDelete:
            [changeSet addObject:indexPath];
            break;
        case NSFetchedResultsChangeUpdate:
            [changeSet addObject:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [changeSet addObject:@[indexPath, newIndexPath]];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    NSMutableArray *moves = _objectChanges[@(NSFetchedResultsChangeMove)];
    if (moves.count > 0) {
        NSMutableArray *updatedMoves = [[NSMutableArray alloc] initWithCapacity:moves.count];
        
        NSMutableIndexSet *insertSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        NSMutableIndexSet *deleteSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        for (NSArray *move in moves) {
            NSIndexPath *fromIP = move[0];
            NSIndexPath *toIP = move[1];
            
            if ([deleteSections containsIndex:fromIP.section]) {
                if (![insertSections containsIndex:toIP.section]) {
                    NSMutableArray *changeSet = _objectChanges[@(NSFetchedResultsChangeInsert)];
                    if (changeSet == nil) {
                        changeSet = [[NSMutableArray alloc] initWithObjects:toIP, nil];
                        _objectChanges[@(NSFetchedResultsChangeInsert)] = changeSet;
                    } else {
                        [changeSet addObject:toIP];
                    }
                }
            } else if ([insertSections containsIndex:toIP.section]) {
                NSMutableArray *changeSet = _objectChanges[@(NSFetchedResultsChangeDelete)];
                if (changeSet == nil) {
                    changeSet = [[NSMutableArray alloc] initWithObjects:fromIP, nil];
                    _objectChanges[@(NSFetchedResultsChangeDelete)] = changeSet;
                } else {
                    [changeSet addObject:fromIP];
                }
            } else {
                [updatedMoves addObject:move];
            }
        }
        
        if (updatedMoves.count > 0) {
            _objectChanges[@(NSFetchedResultsChangeMove)] = updatedMoves;
        } else {
            [_objectChanges removeObjectForKey:@(NSFetchedResultsChangeMove)];
        }
    }
    
    NSMutableArray *deletes = _objectChanges[@(NSFetchedResultsChangeDelete)];
    if (deletes.count > 0) {
        NSMutableIndexSet *deletedSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        [deletes filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSIndexPath *evaluatedObject, NSDictionary *bindings) {
            return ![deletedSections containsIndex:evaluatedObject.section];
        }]];
    }
    
    NSMutableArray *inserts = _objectChanges[@(NSFetchedResultsChangeInsert)];
    if (inserts.count > 0) {
        NSMutableIndexSet *insertedSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        [inserts filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSIndexPath *evaluatedObject, NSDictionary *bindings) {
            return ![insertedSections containsIndex:evaluatedObject.section];
        }]];
    }
    
    UICollectionView *cv = self.collectionView;
    
    [cv performBatchUpdates:^{
        NSIndexSet *deletedSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        if (deletedSections.count > 0) {
            [cv deleteSections:deletedSections];
        }
        
        NSIndexSet *insertedSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        if (insertedSections.count > 0) {
            [cv insertSections:insertedSections];
        }
        
        NSArray *deletedItems = _objectChanges[@(NSFetchedResultsChangeDelete)];
        if (deletedItems.count > 0) {
            [cv deleteItemsAtIndexPaths:deletedItems];
        }
        
        NSArray *insertedItems = _objectChanges[@(NSFetchedResultsChangeInsert)];
        if (insertedItems.count > 0) {
            [cv insertItemsAtIndexPaths:insertedItems];
        }
        
        NSArray *reloadItems = _objectChanges[@(NSFetchedResultsChangeUpdate)];
        if (reloadItems.count > 0) {
            [cv reloadItemsAtIndexPaths:reloadItems];
        }
        
        NSArray *moveItems = _objectChanges[@(NSFetchedResultsChangeMove)];
        for (NSArray *paths in moveItems) {
            [cv moveItemAtIndexPath:paths[0] toIndexPath:paths[1]];
        }
    } completion:nil];
    
    _objectChanges = nil;
    _sectionChanges = nil;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [self.collectionView performBatchUpdates:nil completion:nil];
}

- (void)closeButtonHidden {
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        wo.customCloseButton.hidden = YES;
    });
    

}

@end
