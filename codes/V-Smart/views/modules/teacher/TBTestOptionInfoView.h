//
//  TBTestOptionInfoView.h
//  V-Smart
//
//  Created by Julius Abarra on 15/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBTestOptionInfoView : UIViewController

- (void)setTestOptionInfoWithTitle:(NSString *)title andBody:(NSString *)body;

@end
