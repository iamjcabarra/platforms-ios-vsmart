//
//  ChoiceItemCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 8/3/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChoiceItemDelegate <NSObject>
@required
- (void) updatedChoiceItemTitle:(NSString *)title point:(CGPoint)point;
@end

@interface ChoiceItemCell : UITableViewCell

@property (nonatomic, weak) id<ChoiceItemDelegate> delegate;
@property (nonatomic, strong) IBOutlet UITextField *choiceLabel;
@property (nonatomic, strong) IBOutlet UIButton *eraseButton;
@property (nonatomic, strong) IBOutlet UIButton *checkButton;

@end
