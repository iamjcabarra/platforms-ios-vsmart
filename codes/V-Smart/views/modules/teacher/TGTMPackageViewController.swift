//
//  TGTMPackageViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 22/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class TGTMPackageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {
    
    @IBOutlet fileprivate var tableView: UITableView!
    @IBOutlet fileprivate var emptyPlaceholderView: UIView!
    @IBOutlet fileprivate var emptyPlaceholderLabel: UILabel!
    
    fileprivate var isCoordinator = false
    fileprivate var tableRefreshControl: UIRefreshControl!
    
    fileprivate let kCellIdentifier = "package_cell_identifier"
    fileprivate let kTGTMTestListViewSegueIdentifier = "SHOW_TGTM_TEST_LIST_VIEW"
    fileprivate let kCourseListViewSegueIdentifier = "COURSE_TEST_GURU_MODULE"
    
    fileprivate var testListView: TGTMTestViewController!
    fileprivate var courseListView: TGCourseListController!
    
    // MARK: - Data Manager
    
    fileprivate lazy var tgmDataManager: TestGuruDataManager = {
        let tgdm = TestGuruDataManager.sharedInstance()
        return tgdm!
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 150.0;
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.isCoordinator = self.tgmDataManager.isCoordinator()
        self.loadPackageListView(isRefreshing: false)
        
        let refreshAction = #selector(self.refreshAction(_:))
        self.tableRefreshControl = UIRefreshControl()
        self.tableRefreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: ""))
        self.tableRefreshControl.addTarget(self, action: refreshAction, for: .valueChanged)
        self.tableView.addSubview(self.tableRefreshControl)
        
        self.tgmDataManager.requestPaginationSettings { (success) in }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.title = NSLocalizedString("Test Guru", comment: "")
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "#343434")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Package List View
    
    fileprivate func loadPackageListView(isRefreshing refresh: Bool) {
        if !refresh {
            let message = "\(NSLocalizedString("Loading", comment: ""))..."
            self.view.makeToastActivity(message: message)
            self.view.isUserInteractionEnabled = false
        }
        
        self.tgmDataManager.requestPackageTypes { (success) in
            DispatchQueue.main.async(execute: {
                refresh ? self.tableRefreshControl.endRefreshing() : self.view.hideToastActivity()
                self.view.isUserInteractionEnabled = true
                self.shouldShowEmptyPlaceholderView(!success)
            })
        }
    }
    
    func refreshAction(_ sender: UIRefreshControl?) {
        self.loadPackageListView(isRefreshing: true)
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kCellIdentifier, for: indexPath) as! TGTMPackageTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        cell.layer.borderWidth = 10
        cell.layer.borderColor = UIColor(rgba: "#ECEFF1").cgColor
        return cell
    }
    
    func configureCell(_ cell: TGTMPackageTableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath) 
        let id = self.tgmDataManager.stringValue(mo.value(forKey: "id"))
        let value = self.tgmDataManager.stringValue(mo.value(forKey: "value"))
        let image_url = self.tgmDataManager.stringValue(mo.value(forKey: "image_url"))
        let description = self.tgmDataManager.stringValue(mo.value(forKey: "package_description"))
        
        if id == "10000" {
            cell.packageImage.image = UIImage(named: image_url!)
        }
        else {
            cell.packageImage.sd_setImage(with: URL(string: image_url!), completed: { (image, error, type, url) in
                if image == nil || error != nil { cell.packageImage.image = UIImage(named: "unknown-file-03.png") }
            })
        }
        
        cell.packageTitleLabel.text = value
        cell.packageDescriptionLabel.text = description
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        let id = self.tgmDataManager.stringValue(mo.value(forKey: "id"))
        let value = self.tgmDataManager.stringValue(mo.value(forKey: "value"))
        let image_url = self.tgmDataManager.stringValue(mo.value(forKey: "image_url"))
        
        self.tgmDataManager.save(id, forKey: kTGQB_SELECTED_PACKAGE_ID)
        self.tgmDataManager.save(value, forKey: kTGQB_SELECTED_PACKAGE_NAME)
        self.tgmDataManager.save(image_url, forKey: kTGQB_SELECTED_PACKAGE_IMAGE_URL)
        
        DispatchQueue.main.async(execute: {
            let segueIdentifier = id == "10000" ? self.kTGTMTestListViewSegueIdentifier : self.kCourseListViewSegueIdentifier
            self.performSegue(withIdentifier: segueIdentifier, sender: nil)
            tableView.deselectRow(at: indexPath, animated: true)
        })
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kPackageTypeEntity)
        
        let ctx = self.tgmDataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: kPackageTypeEntity)
        fetchRequest.fetchBatchSize = 20
        
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.tableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! TGTMPackageTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == self.kTGTMTestListViewSegueIdentifier {
            self.testListView = segue.destination as? TGTMTestViewController
        }
        
        if segue.identifier == self.kCourseListViewSegueIdentifier {
            self.courseListView = segue.destination as? TGCourseListController
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }
    
    // MARK: - Empty Placeholder View
    
    fileprivate func shouldShowEmptyPlaceholderView(_ show: Bool) {
        if (show) {
            let message = NSLocalizedString("No available data.", comment: "")
            self.emptyPlaceholderLabel.text = message
        }
        
        self.emptyPlaceholderView.isHidden = !show
    }

}
