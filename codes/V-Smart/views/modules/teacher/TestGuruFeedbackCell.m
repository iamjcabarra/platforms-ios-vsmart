//
//  TestGuruFeedbackCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 10/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruFeedbackCell.h"
#import "TestGuruFeedbackTextCell.h"

@interface TestGuruFeedbackCell() <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *typeLabel;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmented;
@property (strong, nonatomic) IBOutlet UICollectionView *collection;

@property (strong, nonatomic) NSArray *items;

@end

@implementation TestGuruFeedbackCell

static NSString *kCellID = @"feedback_textview_cell_identifier";
static CGFloat kHeight = 105.0f;

- (void)awakeFromNib {
    // Initialization code
    self.items = @[@"Text View 1",@"Text View 2",@"Text View 3"];
    
    // Test Guru Option Type Cell
    UINib *textNib = [UINib nibWithNibName:@"TestGuruFeedbackTextCell" bundle:nil];
    [self.collection registerNib:textNib forCellWithReuseIdentifier:kCellID];
    
    SEL action = @selector(scrollAction:);
    [self.segmented addTarget:self action:action forControlEvents:UIControlEventValueChanged];
}

- (void)displayData:(NSDictionary *)data {
    
    if (data != nil) {
        NSString *title_string = data[@"title"];
        NSArray *options = data[@"options"];
        
        NSMutableOrderedSet *set = [NSMutableOrderedSet orderedSet];
        for (NSDictionary *d in options) {
            
            NSString *value_string = [NSString stringWithFormat:@"%@", d[@"value"] ];
            [set addObject:value_string];
        }
        
        self.typeLabel.text = title_string;
        self.items = [set array];
        [self.collection reloadData];
    }
}

- (void)scrollAction:(UISegmentedControl *)control {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    NSInteger selected_index = self.segmented.selectedSegmentIndex;
    
    if ([self.items count] > 0) {
        indexPath = [NSIndexPath indexPathForRow:selected_index inSection:0];
    }
    
    [self scollTextViewAtIndexPath:indexPath];
}

- (void)scollTextViewAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.collection scrollToItemAtIndexPath:indexPath
                            atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                    animated:YES];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.items count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger row = indexPath.row;
    
    TestGuruFeedbackTextCell *cell = (TestGuruFeedbackTextCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kCellID
                                                                                                           forIndexPath:indexPath];
    
    NSString *text = self.items[row];
    
    cell.textView.text = text;
    
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0f;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = self.collection.bounds.size.width;
    return CGSizeMake(width, kHeight);
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [self.collection performBatchUpdates:nil completion:nil];
}

@end
