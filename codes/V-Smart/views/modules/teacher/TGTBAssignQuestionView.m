//
//  TGTBAssignQuestionView.m
//  V-Smart
//
//  Created by Carmelito Bayarcal on 09/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGTBAssignQuestionView.h"
#import "TestGuruDataManager.h"
#import "TGTBQuestionItemCell.h"
#import "TestGuruDynamicHeader.h"
#import "TestGuruSortHeader.h"
#import "TGTBTestQuestionView.h"
#import "QuestionSamplerView.h"
#import "HUD.h"
#import "UIImageView+WebCache.h"

@interface TGTBAssignQuestionView () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, TestGuruDynamicHeaderDelegate, TestGuruSortHeaderDelegate, UISearchBarDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) TestGuruDataManager *tm;

@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *checkAllButton;
@property (weak, nonatomic) IBOutlet UILabel *selectAllLabel;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (weak, nonatomic) IBOutlet UIButton *sortButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *selectedStatsLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *assignButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sortVerticalConstraint;
@property (weak, nonatomic) IBOutlet UIButton *loadMoreButton;
@property (strong, nonatomic) IBOutlet UIView *tableFootView;
@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;

@property (strong, nonatomic) NSDateFormatter *formatter;
@property (strong, nonatomic) NSDateFormatter *dateToStringFormatter;

@property (strong, nonatomic) NSArray *filterIDs;
@property (assign, nonatomic) BOOL isProcessing;
@property (strong, nonatomic) NSArray *sortDescriptors;
@property (strong, nonatomic) NSDictionary *sortData;

@property (assign, nonatomic) BOOL sortOrderAscending;

@property (strong, nonatomic) NSString *current_page;
@property (strong, nonatomic) NSString *total_filtered;
//@property (strong, nonatomic) NSString *total_items;
@property (strong, nonatomic) NSString *total_pages;
@property (strong, nonatomic) NSString *total_parsed;

@property (strong, nonatomic) NSMutableSet *selectedObjects;
@property (strong, nonatomic) NSMutableSet *deselectedObjects;

@end

static NSString *kQuestionTypeCellIdentifier = @"questionCellIdentifier";

@implementation TGTBAssignQuestionView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tm = [TestGuruDataManager sharedInstance];
    self.searchBar.delegate = self;
    [self.closeButton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UINib *questionItemCellNib = [UINib nibWithNibName:@"TGTBQuestionItemCell" bundle:nil];
    [self.tableView registerNib:questionItemCellNib forCellReuseIdentifier:kQuestionTypeCellIdentifier];
    
    self.formatter = [[NSDateFormatter alloc] init];
    [self.formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [self.formatter setTimeZone:[NSTimeZone localTimeZone]];
    
    self.dateToStringFormatter = [[NSDateFormatter alloc] init];
    [self.dateToStringFormatter setDateFormat:@"EEE, MMM dd, yyyy hh:mm aa"];
    [self.dateToStringFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    self.sortOrderAscending = NO;
    [self setUpSortDescriptors];
    
    self.tableView.tableFooterView = self.tableFootView;
    
    [self.checkAllButton addTarget:self action:@selector(selectAllAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.filterButton addTarget:self action:@selector(showFilterAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.loadMoreButton addTarget:self action:@selector(loadMoreAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.assignButton addTarget:self action:@selector(assignButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.selectedObjects = [NSMutableSet setWithSet:self.setOfAssignedObjects];
    
    [self filterQuestionsPageOne];
    
    [self updateSelectedStatistics];
}

- (void)setUpSortDescriptors {
    //NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"sort_date" ascending:self.sortOrderAscending];
    //NSSortDescriptor *question_text = [NSSortDescriptor sortDescriptorWithKey:@"question_text" ascending:self.sortOrderAscending];
    //NSSortDescriptor *question_id = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:self.sortOrderAscending];
    
    //self.sortDescriptors = @[date_modified,question_text,question_id];
    
    NSSortDescriptor *index = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"sort_date" ascending:self.sortOrderAscending];
    NSSortDescriptor *question_text = [NSSortDescriptor sortDescriptorWithKey:@"question_text" ascending:self.sortOrderAscending];
    NSSortDescriptor *question_id = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:self.sortOrderAscending];
    
    self.sortDescriptors = @[index, date_modified,question_text,question_id];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([(NSObject*)self.delegate respondsToSelector:@selector(didFinishAssigning:)]) {
        [self.delegate didFinishAssigning:YES];
    }
}


- (NSManagedObject *)checkArrayOfObjects:(NSMutableSet *)set contains:(NSManagedObject *) mo {
    NSManagedObject *finalObject = nil;
    
    NSString *question_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"id"] ];
    
    NSPredicate *questionIDPredicate = [NSPredicate predicateWithFormat:@"id == %@", question_id];
    //    NSCompoundPredicate = [NSCompoundPredicate a]
    NSCompoundPredicate *predicates = [NSCompoundPredicate andPredicateWithSubpredicates:@[questionIDPredicate]];
    
    NSArray *filteredArray = [[set filteredSetUsingPredicate:predicates] allObjects];
    finalObject =  (filteredArray.count > 0) ? filteredArray.firstObject : nil;
    
    return finalObject;
}


- (void)updateSelectedStatistics {
    
    CGFloat totalPoints = 0;
    NSInteger count = self.selectedObjects.count;
    for (NSDictionary *dict in self.selectedObjects) {
        NSString *points = [NSString stringWithFormat:@"%@", [dict valueForKey:@"points"]];
        totalPoints = totalPoints + [points floatValue];
    }
    
    NSString *totalPointsString = [NSString stringWithFormat:@"%.02f", totalPoints];
    totalPointsString = [self.tm formatStringNumber:totalPointsString];
    
    self.selectedStatsLabel.text = [NSString stringWithFormat:@"%@ (%ld items / %@ points)", NSLocalizedString(@"Selected Questions", nil), (long)count, totalPointsString];
    
    BOOL allowAssign = NO;
    if (count > 0) {
        allowAssign = YES;
    }
    
    self.assignButton.userInteractionEnabled = allowAssign;
    self.assignButton.backgroundColor = (allowAssign) ? UIColorFromHex(0x68bf61) : [UIColor lightGrayColor];
}


- (void)addToSelected:(NSManagedObject *)mo {
    
    NSArray *keys = mo.entity.propertiesByName.allKeys;
    NSDictionary *dict = [mo dictionaryWithValuesForKeys:keys];
    
    NSLog(@"MO! [%@]", mo);
    
    if (![self checkArrayOfObjects:self.selectedObjects contains:mo]) {
        [self.selectedObjects addObject:dict];
    }
    
    NSManagedObject *object = [self checkArrayOfObjects:self.deselectedObjects contains:mo];
    if (object != nil) {
        [self.deselectedObjects removeObject:object];
    }
    
    [self updateSelectedStatistics];
}

- (void)removeFromSelected:(NSManagedObject *)mo {
    
    NSArray *keys = mo.entity.propertiesByName.allKeys;
    NSDictionary *dict = [mo dictionaryWithValuesForKeys:keys];
    
    if (![self checkArrayOfObjects:self.deselectedObjects contains:mo]) {
        [self.deselectedObjects addObject:dict];
    }
    
    NSManagedObject *object = [self checkArrayOfObjects:self.selectedObjects contains:mo];
    if (object != nil) {
        [self.selectedObjects removeObject:object];
    }
    
    [self updateSelectedStatistics];
}


- (void)selectAllAssigned {
    NSDictionary *userDetails = @{@"is_selected":[NSNumber numberWithBool:YES]};
    
    NSSet *setObjects = [NSSet setWithSet:self.selectedObjects];
    for (NSDictionary *dict in setObjects) {
        NSString *question_id = [NSString stringWithFormat:@"%@", [dict valueForKey:@"id"] ];
        NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" object:question_id];
        [self.tm updateTest:kQuestionBankEntity predicate:predicate details:userDetails]; // meantime fix
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)assignButtonAction:(UIButton *)button {
    __weak typeof(self) wo = self;
    //    [self.tm assignTestListWithDoneBlock:^(BOOL status) {
    NSString *indicatorStr = NSLocalizedString(@"Loading", nil);
    [HUD showUIBlockingIndicatorWithText:indicatorStr];
    
    [self.tm assignTestListWithSet:self.selectedObjects doneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo dismissViewControllerAnimated:YES completion:nil];
            [HUD hideUIBlockingIndicator];
        });
    }];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)sortButtonAction:(UIButton *)sender {
    [self animateDrawerNavigationWithType:@"SORT"];
}

- (void)closeAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)selectAllAction:(UIButton *)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSNumber *originalState = [NSNumber numberWithBool:sender.selected];
    BOOL newBool = (sender.selected) ? NO : YES;
    NSNumber *newState = [NSNumber numberWithBool:newBool];
    
    NSString *indicatorStr = NSLocalizedString(@"Loading", nil);
    [HUD showUIBlockingIndicatorWithText:indicatorStr];
    
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"is_selected" object:originalState];
    
    sender.selected = newBool;
    NSDictionary *userDetails = @{@"is_selected":newState};
    [self.tm updateObjectsFromEntity:kQuestionBankEntity details:userDetails predicate:predicate];
    
    
    NSPredicate *predicateCurrent = [self.tm predicateForKeyPath:@"is_selected" object:newState];
    NSArray *arrayObjects = [self.tm getObjectsForEntity:kQuestionBankEntity predicate:predicateCurrent];
    for (NSManagedObject *mo in arrayObjects) {
        //        NSManagedObject *object = [NSManagedObject]
        if (sender.selected == YES) {
            [self addToSelected:mo];
        } else {
            [self removeFromSelected:mo];
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [HUD hideUIBlockingIndicator];
    });
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    
    

    
    //    [self setupView:count];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kQuestionTypeCellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TGTBQuestionItemCell *questionCell = (TGTBQuestionItemCell *)cell;
    [self configureQuestionCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(TGTBQuestionItemCell *)cell managedObject:(NSManagedObject *)mo
            objectAtIndexPath:(NSIndexPath *)indexPath {
    [cell shouldHideCheckbox:NO];
    
    NSString *name = [mo valueForKey:@"name"];
    NSString *formatted_date_modified = [mo valueForKey:@"formatted_date_modified"];
    NSString *questionTypeName = [mo valueForKey:@"questionTypeName"];
    NSString *proficiency_level_id = [mo valueForKey:@"proficiency_level_id"];
    NSString *points = [mo valueForKey:@"points"];
    NSString *package_type_icon = [mo valueForKey:@"package_type_icon"];
    
    cell.questionTitleLabel.text = name;
    cell.questionTypeLabel.text = [questionTypeName uppercaseString];
    cell.questionDateLabel.text = formatted_date_modified;
    
    NSString *question_text = [mo valueForKey:@"question_text"];
    question_text = [question_text stringByReplacingOccurrencesOfString:@"-blank-" withString:@"_______________"];
    [cell loadWebViewWithContent:question_text];
    
    [cell updateDifficultyLevelView:proficiency_level_id];
    [cell updatePoints:points];
    
    [cell.checkBoxButton addTarget:self action:@selector(toggleCheckBoxButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.packageTypeImage sd_setImageWithURL:[NSURL URLWithString:package_type_icon]];
    
    BOOL is_selected = [(NSNumber *)[mo valueForKey:@"is_selected"] boolValue];
    cell.checkBoxButton.selected = is_selected;
    cell.detailView.backgroundColor = (is_selected) ? UIColorFromHex(0xcdecd2) : [UIColor whiteColor];
    cell.questionDescriptionLabel.backgroundColor = (is_selected) ? UIColorFromHex(0xcdecd2) : [UIColor clearColor];
}

- (void)toggleCheckBoxButton:(UIButton *)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *question_id = [mo valueForKey:@"id"];
    
    sender.selected = (sender.selected) ? NO : YES;
    NSDictionary *userDetails = @{@"is_selected":[NSNumber numberWithBool:sender.selected]};
    
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" object:question_id];
    [self.tm updateTest:kQuestionBankEntity predicate:predicate details:userDetails]; // meantime fix
    
    if (sender.selected == YES) {
        [self addToSelected:mo];
    } else {
        [self removeFromSelected:mo];
    }
    
    //    [self.tm updateObject:mo withDetails:userDetails];// weird bug
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"SAMPLE_QUESTION_VIEW" sender:self];
}



#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 185;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"testguru_dynamic_filter_header"]) {
        TestGuruDynamicHeader *dynamicHeader = (TestGuruDynamicHeader *)[segue destinationViewController];
        dynamicHeader.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"SAMPLE_QUESTION_VIEW"]) {
        QuestionSamplerView *sampler = (QuestionSamplerView *)[segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        sampler.indexPath = indexPath;
        sampler.entityObject = kQuestionBankEntity;
        sampler.sortDescriptors = self.sortDescriptors;
        sampler.isTestPreview = YES;
    }
    
    if ([segue.identifier isEqualToString:@"testguru_sort_header"]) {
        TestGuruSortHeader *sortHeader = (TestGuruSortHeader *)[segue destinationViewController];
        sortHeader.delegate = self;
        //        [sortHeader.view layoutIfNeeded];
    }
}


#pragma mark - Search Bar delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self initiateFilter:@"1"];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([self.searchBar.text isEqualToString:@""]) {
        [self initiateFilter:@"1"];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - Fetched results controller

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionBankEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    [fetchRequest setSortDescriptors:self.sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    
    NSString *section_name = nil; //(self.isGroupBy) ? @"section_name" : nil;
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:section_name
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void)sortDidSelectButtonAction:(NSString *)actionType withSortData:(NSDictionary *)sortData {
    if ([actionType isEqualToString:@"APPLY"]) {
        if (sortData) {
            self.sortButton.selected = YES;
            self.sortData = sortData;
            
            // DYNAMIC SORTING
            NSSortDescriptor *index = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
            self.sortDescriptors = @[index];
            
            [self filterQuestionsPageOne];
        }
        else {
            self.sortButton.selected = NO;
        }
    }
    
    BOOL hasSort = YES;
    BOOL isViewBy = [sortData objectForKey:@"view_by"];
    
    if ([sortData count] == 1 && isViewBy) {
        hasSort = NO;
    }
    
    UIFont *font = (hasSort) ? [UIFont fontWithName:@"Helvetica-Bold" size:18] : [UIFont fontWithName:@"Helvetica" size:17];
    self.sortButton.titleLabel.font = font;
    
    self.sortVerticalConstraint.constant = -130;
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}

/// FILTER DRAWER DELEGEATE
- (void)didSelectButtonAction:(NSString *)actionType {
    NSLog(@"HELLO %@", actionType);
    self.verticalConstraint.constant = -250;
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
    
    
    if ([actionType isEqualToString:@"APPLY"]) {
        [self filterQuestionsPageOne];
    }
}


- (void)filterOptionCount:(NSUInteger)count {
    
}


- (void)showFilterAction:(UIButton *)sender {
    
    NSLog(@"%s -----> ", __PRETTY_FUNCTION__);
    
    [self animateDrawerNavigationWithType:@"FILTER"];
}
- (void)animateDrawerNavigationWithType:(NSString *)type {
    
    if ([type isEqualToString:@"FILTER"]) {
        self.verticalConstraint.constant = (self.verticalConstraint.constant == -250) ? 0 : -250;
        self.sortVerticalConstraint.constant = -130;
    } else {
        self.verticalConstraint.constant = -250;
        self.sortVerticalConstraint.constant = (self.sortVerticalConstraint.constant == -130) ? 0 : -130;
    }
    
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)filterQuestionsPageOne {
    self.filterIDs = [self.tm filterOptionsSettedTo:YES];
    NSString *nextPageStr = [NSString stringWithFormat:@"%@", @1];
    [self initiateFilter:nextPageStr];
}

- (void)initiateFilter:(NSString *)page {
    if (!self.isProcessing) {
        NSString *indicatorStr = NSLocalizedString(@"Loading", nil);
        [HUD showUIBlockingIndicatorWithText:indicatorStr];
        self.isProcessing = YES;
        self.loadMoreButton.hidden = YES;
        
//        [self.tm requestFilteredQuestionBankWithPage:page withKeyword:self.searchBar.text filterIDs:self.filterIDs sort:self.sortData dataBlock:^(NSDictionary *data)
        
        [self.tm requestFilteredAssignQuestionWithPage:page withKeyword:self.searchBar.text filterIDs:self.filterIDs sort:self.sortData dataBlock:^(NSDictionary *data) {
            if (data != nil) {
                self.current_page = data[@"current_page"];
                self.total_filtered = data[@"total_filtered"];
                //                self.total_items = data[@"total_items"];
                self.total_parsed = data[@"total_parsed"];
                self.total_pages = data[@"total_pages"];
            }
            
            //[self selectAllAssigned];
            
            //self.loadMoreButton.enabled = YES;
            //[self updateInterface];
            //self.isProcessing = NO;
            
            //__weak typeof(self) wo = self;
            //dispatch_async(dispatch_get_main_queue(), ^{
                //[wo reloadFetchedResultsController];
            //});
            
            __weak typeof(self) wo = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo selectAllAssigned];
    
                wo.loadMoreButton.enabled = YES;
                [wo updateInterface];
                wo.isProcessing = NO;
                
                [wo reloadFetchedResultsController];
            });
        }];
    }
}

- (void)updateInterface {
    NSInteger totalPages = [self.total_pages integerValue];
    NSInteger currentPage = [self.current_page integerValue];
    BOOL isHidden = YES;
    
    if (totalPages > currentPage) {
        isHidden = NO;
    } else {
        isHidden = YES;
    }
    
    NSInteger filteredInt = [self.total_filtered integerValue];
    
    NSString *questionString = (filteredInt > 1) ? NSLocalizedString(@"Questions", nil): NSLocalizedString(@"Question", nil);
    
    BOOL emptyViewHidden = (filteredInt == 0) ? NO : YES;
    NSString *emptyMessage = (self.searchBar.text.length > 0) ? [NSString stringWithFormat:@"%@ '%@'", NSLocalizedString(@"No results found for", nil), self.searchBar.text] : NSLocalizedString(@"No questions available yet", nil);
    
    if ([self.total_filtered isEqualToString:@"(null)"]) {
        self.total_filtered = @"0";
    }
   
    NSString *countString = [NSString stringWithFormat:@"%@ (%@)", questionString, self.total_filtered];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.loadMoreButton.hidden = isHidden;
         self.emptyView.hidden = emptyViewHidden;
        self.emptyLabel.text = emptyMessage;
        [HUD hideUIBlockingIndicator];
        self.questionCountLabel.text = countString;
    });
}

- (void)loadMoreAction:(UIButton *)sender {
    
    NSInteger totalPages = [self.total_pages integerValue];
    NSInteger currentPage = [self.current_page integerValue];
    
    NSString *nextPageStr = [NSString stringWithFormat:@"%ld", (long)currentPage+1];
    
    if (totalPages > currentPage) {
        sender.enabled = NO;
        [self initiateFilter:nextPageStr];
    }
}

@end
