//
//  TGLearningTypeCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 08/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGLearningTypeCell : UITableViewCell

@property (strong, nonatomic) NSString *type_id;
@property (strong, nonatomic) NSString *type_value;

@end
