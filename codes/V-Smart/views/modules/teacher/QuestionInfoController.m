//
//  QuestionInfoController.m
//  V-Smart
//
//  Created by Ryan Migallos on 7/30/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "QuestionInfoController.h"
#import "QuestionHeader.h"
#import "TestGuruDataManager.h"
#import "QuestionEssayTableViewCell.h"
#import "QuestionShortAnswerTableViewCell.h"
#import "QuestionTrueFalseTableViewCell.h"
#import "QuestionMultipleChoiceTableViewCell.h"

@interface QuestionInfoController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>


@property (strong, nonatomic) IBOutlet QuestionHeader *headerView;

@property (strong, nonatomic) NSString *questionTypeName;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *points;
@property (strong, nonatomic) NSString *question_text;
@property (strong, nonatomic) NSString *tags;
@property (strong, nonatomic) NSString *image_url;

@property (nonatomic, strong) TestGuruDataManager *tm;

@end

@implementation QuestionInfoController

static NSString *kEssayCellIdentifier = @"essay_cell_identifier";
static NSString *kShortAnswerCellIdentifier = @"shortanswer_cell_identifier";
static NSString *kTrueFalseCellIdentifier = @"truefalse_cell_identifier";
static NSString *kMultipleChoiceCellIdentifier = @"multiplechoice_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = NO;
    
    // Setup Right Bar Button
    [self setupRightBarButton];
    
    // Register Multiple Cell Types
    [self registerCellObjects];

    // Render Question Data
    [self displayQuestionData];
    
    // Test Guru Data Manager
    self.tm = [TestGuruDataManager sharedInstance];
    
    // It does not allow scrolling of table view
    // self.tableView.userInteractionEnabled = _isEditMode;
    
    self.tableView.userInteractionEnabled = YES;
    
    // If read-only
    if (!_isEditMode) {
        self.headerView.userInteractionEnabled = NO;
    }
}

- (void)setupRightBarButton {
    
//    NSString *saveTitle = (_isEditMode)  ?  NSLocalizedString(@"Save", nil) : NSLocalizedString(@"Read Only", nil);
    NSString *saveTitle = (_isEditMode)  ?  NSLocalizedString(@"Update", nil) : NSLocalizedString(@"Read Only", nil);
    SEL saveAction = @selector(saveButtonAction:);
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:saveTitle style:UIBarButtonItemStylePlain target:self action:saveAction];
    self.navigationItem.rightBarButtonItem = saveButton;
    self.navigationItem.rightBarButtonItem.enabled = _isEditMode;
}

- (void)registerCellObjects {
    
    // Essay
    UINib *essayNib = [UINib nibWithNibName:@"QuestionEssayTableViewCell" bundle:nil];
    [self.tableView registerNib:essayNib forCellReuseIdentifier:kEssayCellIdentifier];
    
    // ShortAnswer
    UINib *shortAnswerNib = [UINib nibWithNibName:@"QuestionShortAnswerTableViewCell" bundle:nil];
    [self.tableView registerNib:shortAnswerNib forCellReuseIdentifier:kShortAnswerCellIdentifier];
    
    // True False
    UINib *trueFalseNib = [UINib nibWithNibName:@"QuestionTrueFalseTableViewCell" bundle:nil];
    [self.tableView registerNib:trueFalseNib forCellReuseIdentifier:kTrueFalseCellIdentifier];
    
    // Multiple Choice
    UINib *multipleChoiceNib = [UINib nibWithNibName:@"QuestionMultipleChoiceTableViewCell" bundle:nil];
    [self.tableView registerNib:multipleChoiceNib forCellReuseIdentifier:kMultipleChoiceCellIdentifier];
}

- (void)displayQuestionData {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.headerView setValuesWithManagedObject:self.question];
    });
    
    self.questionTypeName = [NSString stringWithFormat:@"%@", [self.question valueForKey:@"questionTypeName"] ];
    self.name = [NSString stringWithFormat:@"%@", [self.question valueForKey:@"name"] ];
    self.points = [NSString stringWithFormat:@"%@", [self.question valueForKey:@"points"] ];
    self.question_text = [NSString stringWithFormat:@"%@", [self.question valueForKey:@"question_text"] ];
    self.image_url = [NSString stringWithFormat:@"%@", [self.question valueForKey:@"image_url"] ];
    
    
    self.title = self.name;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (void)addObject:(NSManagedObject *)mo inArray:(NSMutableArray *)array {
    if (mo != nil) {
        [array addObject:mo];
    }
}

- (void)saveButtonAction:(id)sender {
//    NSLog(@"%s", __PRETTY_FUNCTION__);
//        
//    NSDictionary *data = [self.headerView getValues];
//    NSLog(@"data : %@", data);
//
//    NSArray *paths = [self.tableView indexPathsForVisibleRows];
//    NSString *cell_identifier = [self cellIdentifierForObject:self.question];
//    NSLog(@"cell identifier : %@", cell_identifier);
//    
//    NSMutableArray *list_data = [NSMutableArray array];
//    for (NSIndexPath *idx in paths) {
//        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:idx];
//        
//        if ([cell_identifier isEqualToString:kTrueFalseCellIdentifier]) {
//            //True or False
//            QuestionTrueFalseTableViewCell *tfc = (QuestionTrueFalseTableViewCell *)cell;
//            [self addObject:[tfc saveContents:data] inArray:list_data];
//        }
//        
//        if ([cell_identifier isEqualToString:kShortAnswerCellIdentifier]) {
//            //Fill in the Blanks (Short Answer)
//            QuestionShortAnswerTableViewCell *sac = (QuestionShortAnswerTableViewCell *)cell;
//            [self addObject:[sac saveContents:data] inArray:list_data];
//        }
//        
//        if ([cell_identifier isEqualToString:kMultipleChoiceCellIdentifier]) {
//            //Multiple Choice
//            QuestionMultipleChoiceTableViewCell *mcc = (QuestionMultipleChoiceTableViewCell *)cell;
//            [self addObject:[mcc saveContents:data] inArray:list_data];
//        }
//        
//        if ([cell_identifier isEqualToString:kEssayCellIdentifier]) {
//            //Essay
//            QuestionEssayTableViewCell *etc = (QuestionEssayTableViewCell *)cell;
//            [self addObject:[etc saveContents:data] inArray:list_data];
//        }
//    }
//    
//    if (list_data.count > 0) {
//        __weak typeof(self) wo = self;
//        NSManagedObject *mo = [list_data lastObject];
//        [self.tm requestUpdateDetailsForQuestion:mo doneBlock:^(BOOL status) {
//            if (status) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [wo.navigationController popViewControllerAnimated:YES];
//            });
//            }
//            else {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    NSString *avTitle = NSLocalizedString(@"Update Error", nil);
//                    NSString *butOkay = NSLocalizedString(@"Okay", nil);
//                    NSString *message = NSLocalizedString(@"Incomplete Question", nil);
//                    
//                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:avTitle
//                                                                 message:message
//                                                                delegate:wo
//                                                       cancelButtonTitle:butOkay
//                                                       otherButtonTitles:nil];
//                    [av show];
//                });
//            }
//        }];
//    }
    
    // ENHANCEMENT#279:
    [self showConfirmationMessage];
}

// ENHANCEMENT#279:
// Confirm the user if he/she really wants to update selected question
- (void)showConfirmationMessage {
    NSString *title = NSLocalizedString(@"Update Question", nil);
    NSString *message = NSLocalizedString(@"Do you really want to update this question?", nil);
    NSString *yTitle = NSLocalizedString(@"Yes", nil);
    NSString *nTitle = NSLocalizedString(@"No", nil);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:nTitle
                                          otherButtonTitles:yTitle, nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // If yes
    if (buttonIndex == 1) {
        NSLog(@"%s", __PRETTY_FUNCTION__);
        
        NSDictionary *data = [self.headerView getValues];
        NSLog(@"data : %@", data);
        
        NSArray *paths = [self.tableView indexPathsForVisibleRows];
        NSString *cell_identifier = [self cellIdentifierForObject:self.question];
        NSLog(@"cell identifier : %@", cell_identifier);
        
        NSMutableArray *list_data = [NSMutableArray array];
        for (NSIndexPath *idx in paths) {
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:idx];
            
            if ([cell_identifier isEqualToString:kTrueFalseCellIdentifier]) {
                //True or False
                QuestionTrueFalseTableViewCell *tfc = (QuestionTrueFalseTableViewCell *)cell;
                [self addObject:[tfc saveContents:data] inArray:list_data];
            }
            
            if ([cell_identifier isEqualToString:kShortAnswerCellIdentifier]) {
                //Fill in the Blanks (Short Answer)
                QuestionShortAnswerTableViewCell *sac = (QuestionShortAnswerTableViewCell *)cell;
                [self addObject:[sac saveContents:data] inArray:list_data];
            }
            
            if ([cell_identifier isEqualToString:kMultipleChoiceCellIdentifier]) {
                //Multiple Choice
                QuestionMultipleChoiceTableViewCell *mcc = (QuestionMultipleChoiceTableViewCell *)cell;
                [self addObject:[mcc saveContents:data] inArray:list_data];
            }
            
            if ([cell_identifier isEqualToString:kEssayCellIdentifier]) {
                //Essay
                QuestionEssayTableViewCell *etc = (QuestionEssayTableViewCell *)cell;
                [self addObject:[etc saveContents:data] inArray:list_data];
            }
        }
        
        if (list_data.count > 0) {
            __weak typeof(self) wo = self;
            NSManagedObject *mo = [list_data lastObject];
            [self.tm requestUpdateDetailsForQuestion:mo doneBlock:^(BOOL status) {
                if (status) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // ENHANCEMENT#285
                        // Echo message after successfullly creating a new question
                        NSString *butOkay = NSLocalizedString(@"Okay", nil);
                        NSString *message = NSLocalizedString(@"Question has been successfully updated.", nil);
                        
                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                                     message:message
                                                                    delegate:wo
                                                           cancelButtonTitle:butOkay
                                                           otherButtonTitles:nil];
                        [av show];
                        
                        [wo.navigationController popViewControllerAnimated:YES];
                    });
                }
                else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *avTitle = NSLocalizedString(@"Update Error", nil);
                        NSString *butOkay = NSLocalizedString(@"Okay", nil);
                        NSString *message = NSLocalizedString(@"There must be at least 2 choices for each question, one of which must be chosen as the correct answer.", nil);
                        
                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:avTitle
                                                                     message:message
                                                                    delegate:wo
                                                           cancelButtonTitle:butOkay
                                                           otherButtonTitles:nil];
                        [av show];
                    });
                }
            }];
        }
    }
}

- (NSString *)cellIdentifierForObject:(NSManagedObject *)mo {
    
    NSString *type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"question_type_id"] ];
//    NSLog(@"cell question type : %@", type);
    
    NSString *cell_identifier = kShortAnswerCellIdentifier;
    
    if ([type isEqualToString:@"1"]) {
        //True or False
        cell_identifier = kTrueFalseCellIdentifier;
    }
    
    if ([type isEqualToString:@"2"]) {
        //Fill in the Blanks (Short Answer)
        cell_identifier = kShortAnswerCellIdentifier;
    }
    
    if ([type isEqualToString:@"3"]) {
        //Multiple Choice
        cell_identifier = kMultipleChoiceCellIdentifier;
    }
    
    if ([type isEqualToString:@"6"]) {
        //Essay
        cell_identifier = kEssayCellIdentifier;
    }
    
    return cell_identifier;
}

- (NSSet *)choicesFromQuestion:(NSManagedObject *)mo {
    
    NSSet *choices = [NSSet setWithSet: [mo valueForKey:@"choices"] ];
    return choices;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cell_identifier = [self cellIdentifierForObject:self.question];
    
    NSString *image_url = [NSString stringWithFormat:@"%@", [self.question valueForKey:@"image_url"] ];
    
    NSLog(@"Image url! [%@]", image_url);
    
    BOOL imageIsPresent = NO;
    
    if ([image_url isEqualToString:@""] || [image_url isEqualToString:@"<null>"] || [image_url isEqualToString:@"(null)"]) {
        imageIsPresent = NO;
    } else {
        imageIsPresent = YES;
    }
    
    if ([cell_identifier isEqualToString:kTrueFalseCellIdentifier]) {
        //True or False
        
        if (imageIsPresent) {
            return 605.0f;
        }
        
        return 455.0f;
    }
    
    if ([cell_identifier isEqualToString:kShortAnswerCellIdentifier]) {
        //Fill in the Blanks (Short Answer)
        if (imageIsPresent) {
            return 450.0f;
        }
        
        return 300.0f;
    }
    
    if ([cell_identifier isEqualToString:kMultipleChoiceCellIdentifier]) {
        //Multiple Choice
        if (imageIsPresent) {
            return 700.0f;
        }
        return 550.0f;
    }
    
    if ([cell_identifier isEqualToString:kEssayCellIdentifier]) {
        //Essay
        if (imageIsPresent) {
            return 450.0f;
        }
        return 300.0f;
    }
    
    
    return 300;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cell_identifier = [self cellIdentifierForObject:self.question];

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier forIndexPath:indexPath];
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
//    // If read-only
//    if (!_isEditMode) {
//        // cell.userInteractionEnabled = NO;
//    }
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cell_identifier = [self cellIdentifierForObject:self.question];
    __weak typeof(self) wo = self;
    
    if ([cell_identifier isEqualToString:kTrueFalseCellIdentifier]) {
        //True or False
        QuestionTrueFalseTableViewCell *tfc = (QuestionTrueFalseTableViewCell *)cell;
        tfc.questionField.text = self.name;
        tfc.questionPointsField.text = self.points;
        tfc.questionTextArea.text = self.question_text;
        tfc.eraseButton.hidden = YES;
        
        // If read-only
        if (!_isEditMode) {
            tfc.questionField.userInteractionEnabled = NO;
            tfc.questionPointsField.userInteractionEnabled = NO;
            tfc.questionTextArea.editable = NO;
            tfc.questionTextArea.selectable = NO;
            tfc.isEditMode = _isEditMode;
        }
        
        [tfc choicesForObject:self.question];
        
        if ([self.image_url isEqualToString:@""] || [self.image_url isEqualToString:@"<null>"] || [self.image_url isEqualToString:@"(null)"]) {
            tfc.verticalConstraintTrueOrFalse.constant = 8;
            tfc.questionImageView.hidden = YES;
            
        } else {
            tfc.verticalConstraintTrueOrFalse.constant = 150;
            tfc.questionImageView.hidden = NO;
            
            if ([self.question valueForKey:@"image_data"] == nil) {
                [self.tm downloadImageFromQuestionObject:self.question binaryBlock:^(NSData *binary) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        tfc.questionImageView.image = [UIImage imageWithData:binary];
                    });
                }];
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    tfc.questionImageView.image = [UIImage imageWithData:[wo.question valueForKey:@"image_data"] ];
                });
            }
        }
    }
    
    if ([cell_identifier isEqualToString:kShortAnswerCellIdentifier]) {
        //Fill in the Blanks (Short Answer)
        QuestionShortAnswerTableViewCell *sac = (QuestionShortAnswerTableViewCell *)cell;
        sac.questionField.text = self.name;
        sac.questionPointsField.text = self.points;
        sac.questionTextArea.text = self.question_text;
        sac.eraseButton.hidden = YES;
        sac.question = self.question;
        
        // If read-only
        if (!_isEditMode) {
            sac.questionField.userInteractionEnabled = NO;
            sac.questionPointsField.userInteractionEnabled = NO;
            sac.questionTextArea.editable = NO;
            sac.questionTextArea.selectable = NO;
        }
    }
    
    if ([cell_identifier isEqualToString:kMultipleChoiceCellIdentifier]) {
        //Multiple Choice
        QuestionMultipleChoiceTableViewCell *mcc = (QuestionMultipleChoiceTableViewCell *)cell;
        mcc.questionField.text = self.name;
        mcc.questionPointsField.text = self.points;
        mcc.questionTextArea.text = self.question_text;
        mcc.eraseButton.hidden = YES;
        
        // If read-only
        if (!_isEditMode) {
            mcc.questionField.userInteractionEnabled = NO;
            mcc.questionPointsField.userInteractionEnabled = NO;
            mcc.questionTextArea.editable = NO;
            mcc.questionTextArea.selectable = NO;
            mcc.isEditMode = _isEditMode;
        }
        
        [mcc choicesForObject:self.question];
        
        if ([self.image_url isEqualToString:@""] || [self.image_url isEqualToString:@"<null>"] || [self.image_url isEqualToString:@"(null)"]) {
            mcc.verticalContraintMultipleChoice.constant = 8;
            mcc.questionImageView.hidden = YES;
            
        } else {
            mcc.verticalContraintMultipleChoice.constant = 150;
            mcc.questionImageView.hidden = NO;
            
            if ([self.question valueForKey:@"image_data"] == nil) {
                [self.tm downloadImageFromQuestionObject:self.question binaryBlock:^(NSData *binary) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        mcc.questionImageView.image = [UIImage imageWithData:binary];
                    });
                }];
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    mcc.questionImageView.image = [UIImage imageWithData:[wo.question valueForKey:@"image_data"] ];
                });
            }
        }
    }
    
    if ([cell_identifier isEqualToString:kEssayCellIdentifier]) {
        //Essay
        QuestionEssayTableViewCell *etc = (QuestionEssayTableViewCell *)cell;
        etc.questionField.text = self.name;
        etc.questionPointsField.text = self.points;
        etc.questionTextArea.text = self.question_text;
        etc.eraseButton.hidden = YES;
        etc.question = self.question;
        
        // If read-only
        if (!_isEditMode) {
            etc.questionField.userInteractionEnabled = NO;
            etc.questionPointsField.userInteractionEnabled = NO;
            etc.questionTextArea.editable = NO;
            etc.questionTextArea.selectable = NO;
        }
    }
}

@end
