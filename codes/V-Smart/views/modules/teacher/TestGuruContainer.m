//
//  TestGuruContainer.m
//  V-Smart
//
//  Created by Ryan Migallos on 7/20/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruContainer.h"
#import "AppDelegate.h"
#import "VSmartMacros.h"
#import "VSmartValues.h"
#import "ResourceManager.h"

@interface TestGuruContainer ()

//container view property
@property (nonatomic, strong) UIViewController *cv;

@end

@implementation TestGuruContainer

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupChildViewController];
    [self layoutNotifications];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationHideProfileView object:self];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        dispatch_queue_t queue = dispatch_queue_create("com.vsmartchool.testguru.ACTIVITY",DISPATCH_QUEUE_SERIAL);
        dispatch_async(queue, ^{
            ResourceManager *rm = [AppDelegate resourceInstance];
            NSString *details = @"Left the TestGuru module in Apple iPad";
            [rm requestLogActivityWithModuleType:@"7" details:details];
        });
    }
    
    [super viewWillDisappear:animated];
}

- (void)setupChildViewController {
    
//    NSString *storyboardName = @"TestGuruStoryboard"; V2.1
//    NSString *storyboardName = @"TestGuruV2";
    
    
//    NSString *storyboardName = @"TestGuruV2JA";
    
    // BACKWARD COMPATIBILITY IMPLEMENTATION
    CGFloat version = [[Utils getServerInstanceVersion] floatValue];
    
    NSLog(@"TEST GURU SERVER VERSION : %@", @(version) );
    
    NSString *storyboardName = (version < VSMART_SERVER_MAX_VER) ? @"TestGuruV2JA" : @"TestGuruV2CB";
    
//    NSString *storyboardName = @"TestGuruV2CB"; // V2.5
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    self.cv = [sb instantiateInitialViewController];
    [self initiateCustomLayoutFor:self.cv];
    self.cv.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.cv.view.autoresizesSubviews = YES;
    
    [self addChildViewController:self.cv];
    [self.view addSubview:self.cv.view];
    [self.cv didMoveToParentViewController:self];
    [self.view sendSubviewToBack:self.cv.view];
}

- (void)layoutNotifications {
    VS_NCADD(kNotificationProfileHeight, @selector(resizeContainerView:))
}

- (NSString *) dashboardName {
    NSString *moduleName = NSLocalizedString(@"Test Guru", nil);
    return moduleName;
}

#pragma mark - Post Notification Events
- (void)resizeContainerView:(NSNotification *)notification {
    
    VLog(@"Received: kNotificationProfileHeight");
    [UIView animateWithDuration:0.45 animations:^{
        [self initiateCustomLayoutFor:self.cv];
        [self.cv.view setNeedsLayout];
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            [super adjustProfileHeight];
        }
        
        [super showOrHideMiniAvatar];
    }];
}

- (void)initiateCustomLayoutFor:(UIViewController *)viewcontroller {
    
    float profileY = [super profileHeight];
    float headerDecrement = ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    float gridHeight = self.view.frame.size.height - headerDecrement;
    float gridY = [super headerSize].size.height + profileY;
    CGRect customFrame = CGRectMake(0, gridY, self.view.frame.size.width, gridHeight);
    
    [viewcontroller.view setFrame:customFrame];
}

@end

