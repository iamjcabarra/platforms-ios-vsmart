//
//  ChoiceItemCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 8/3/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "ChoiceItemCell.h"

@interface ChoiceItemCell() <UITextFieldDelegate>


@end

@implementation ChoiceItemCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UITableView *)getTableView {
    
    id view = [self superview];
    
    while (view && [view isKindOfClass:[UITableView class]] == NO) {
        view = [view superview];
    }
    
    return (UITableView *)view;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSString *title = [NSString stringWithFormat:@"%@", textField.text ];
    NSMutableString *text_data = [NSMutableString stringWithString:title];
    [text_data replaceCharactersInRange:range withString:string];
    title = [NSString stringWithString:text_data];
    
    if ( [self.delegate respondsToSelector:@selector(updatedChoiceItemTitle:point:)] ) {
        CGPoint point = [self convertPoint:CGPointZero toView:[self getTableView]];
        [self.delegate updatedChoiceItemTitle:title point:point];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSString *title = [NSString stringWithFormat:@"%@", textField.text ];
    if ( [self.delegate respondsToSelector:@selector(updatedChoiceItemTitle:point:)] ) {
        CGPoint point = [self convertPoint:CGPointZero toView:[self getTableView]];
        [self.delegate updatedChoiceItemTitle:title point:point];
        [textField resignFirstResponder];
    }
    
    return YES;
}

@end
