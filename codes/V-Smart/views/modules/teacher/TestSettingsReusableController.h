//
//  TestSettingsReusableController.h
//  V-Smart
//
//  Created by Julius Abarra on 15/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

@protocol TestSettingsTestPropertyPickerDelegate <NSObject>

@required
- (void)selectedTestProperty:(NSDictionary *)d;

@end

#import <UIKit/UIKit.h>

@interface TestSettingsReusableController : UIViewController

@property (weak, nonatomic) id <TestSettingsTestPropertyPickerDelegate> delegate;
@property (assign, nonatomic) NSInteger selectedTestProperty;

@end
