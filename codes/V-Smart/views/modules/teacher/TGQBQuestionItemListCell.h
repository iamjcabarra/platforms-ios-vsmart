//
//  TGQBQuestionItemListCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 10/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGQBQuestionItemListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *checkImage;
@property (strong, nonatomic) IBOutlet UILabel *questionTitle;
@end
