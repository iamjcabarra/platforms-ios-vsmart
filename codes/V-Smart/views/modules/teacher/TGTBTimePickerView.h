//
//  TGTBTimePickerView.h
//  V-Smart
//
//  Created by Julius Abarra on 14/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

@protocol TGTBTimePickerView <NSObject>

@required
- (void)selectedTime:(NSString *)timeString;

@end

#import <UIKit/UIKit.h>

@interface TGTBTimePickerView : UIViewController

@property (weak, nonatomic) id <TGTBTimePickerView> delegate;

@end
