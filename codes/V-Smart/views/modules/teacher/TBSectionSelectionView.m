//
//  TBSectionSelectionView.m
//  V-Smart
//
//  Created by Julius Abarra on 12/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TBSectionSelectionView.h"
#import "TBSectionItemCell.h"
#import "TBClassHelper.h"
#import "TestGuruDataManager.h"
#import "TGTBTestActionConstants.h"
#import "HUD.h"
#import "V_Smart-Swift.h"

//// BUTTON CATEGORY ////
@interface UIButton (CUSTOM)
- (void)setColor:(UIColor *)color forState:(UIControlState)state;
@end

@implementation UIButton (CUSTOM)
- (void)setColor:(UIColor *)color forState:(UIControlState)state {
    
    UIView *colorView = [[UIView alloc] initWithFrame:self.frame];
    colorView.backgroundColor = color;
    UIGraphicsBeginImageContext(colorView.bounds.size);
    [colorView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:state];
}
@end
//// BUTTON CATEGORY ////

@interface TBSectionSelectionView () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) TBClassHelper *classHelper;
@property (strong, nonatomic) TestGuruDataManager *tgdm;

@property (strong, nonatomic) IBOutlet UILabel *optionTypeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *optionTypeIcon;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *processButton;

@property (strong, nonatomic) IBOutlet UIButton *enablePlaylist;
@property (strong, nonatomic) IBOutlet UILabel *playlistTitle;

@property (strong, nonatomic) NSArray *sectionData;
@property (strong, nonatomic) NSMutableSet *selectedSections;
@property (strong, nonatomic) NSMutableSet *selectedIndexPaths;

@property (assign, nonatomic) BOOL sendMail;
@property (assign, nonatomic) BOOL sendPlaylist;

@end

@implementation TBSectionSelectionView

static NSString *kSectionCellIdentifier = @"sectionCellIdentifier";
static CGFloat kCellHeight = 75.0f;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Test Guru Data Manager
    self.tgdm = [TestGuruDataManager sharedInstance];
    
    // Default View
    [self setUpDefaultViewForSectionSelection];
    
    // List Section
    [self listAllSection];
    
    // Collection View Protocols
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    // Allow Sections in Collection View
    self.collectionView.allowsSelection = YES;
    self.collectionView.allowsMultipleSelection = YES;
    
    self.selectedSections = [NSMutableSet set];
    self.selectedIndexPaths = [NSMutableSet set];
    
    // Localization
    self.playlistTitle.text = @"Playlists";
    [self.playlistTitle localize];
    
    [self.enablePlaylist addTarget:self action:@selector(checkPlaylistButton:) forControlEvents:UIControlEventTouchUpInside];

    // Option
    self.sendMail = YES;
    self.sendPlaylist = NO;
    
    if (self.testSwipeButtonActionType == TGTBSwipeButtonActionDeploy) {
        self.enablePlaylist.hidden = YES;
        self.playlistTitle.hidden = YES;
    }
    
    // Disable Playlist for now
    self.enablePlaylist.hidden = true;
    self.playlistTitle.hidden = true;
}

- (void)checkPlaylistButton:(UIButton *)button {
    
    BOOL flag = (button.selected) ? NO : YES;
    button.selected = flag;
    self.sendPlaylist = flag;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Default View Settings

- (void)setUpDefaultViewForSectionSelection {
    // Title
    NSString *title = [self.classHelper localizeString:@"Select Section"];
    self.optionTypeLabel.text = title;
    
    // Icon
    NSString *imageString = @"";
    NSString *processTitle = @"";
    
    // Deploy Test
    if (self.testSwipeButtonActionType == TGTBSwipeButtonActionDeploy) {
        imageString = @"deploy52px.png";
        processTitle = [self.classHelper localizeString:@"Deploy"];
    }
    
    // Save to PDF
    if (self.testSwipeButtonActionType == TGTBSwipeButtonActionSaveAsPDF) {
        imageString = @"pdf52px.png";
        processTitle = [self.classHelper localizeString:@"Generate"];
    }
    
    self.optionTypeIcon.image = [UIImage imageNamed:imageString];
    
    // Cancel Button
    NSString *cancelTitle = [self.classHelper localizeString:@"Cancel"];
    [self.cancelButton setTitle:cancelTitle forState:UIControlStateNormal];
    [self.cancelButton setTitle:cancelTitle forState:UIControlStateHighlighted];
    
    [self.cancelButton addTarget:self
                          action:@selector(cancelSectionSelection:)
                forControlEvents:UIControlEventTouchUpInside];
    
    // Process Button
    [self.processButton setTitle:processTitle forState:UIControlStateDisabled];
    [self.processButton setTitle:processTitle forState:UIControlStateNormal];
    [self.processButton setTitle:processTitle forState:UIControlStateHighlighted];
    [self.processButton setTitle:processTitle forState:UIControlStateDisabled];
    
    [self.processButton addTarget:self
                           action:@selector(processButtonAction:)
                 forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - List Section

- (void)listAllSection {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", [self.classHelper localizeString:@"Please wait"]];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    NSString *userid = [self.tgdm loginUser];
    __weak typeof(self) wo = self;
    
    BOOL isForDeployment = (self.testSwipeButtonActionType == TGTBSwipeButtonActionDeploy);
    
    NSString *test_id = [self.testObject valueForKey:@"id"];
    [self.tgdm requestCourseSectionListForUser:userid test_id:test_id isForDeployment:isForDeployment doneBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSPredicate *predicate = [wo predicateForKeyPath:@"user_id" value:userid];
                wo.sectionData = [wo.tgdm getObjectsForEntity:kCourseSectionListEntity
                                                    predicate:predicate
                                                      context:wo.tgdm.mainContext];
                [wo.collectionView reloadData];
                [wo updateProcessButtonState];
            });
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD hideUIBlockingIndicator];
        });
    }];
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

#pragma mark - Button Action

- (void)cancelSectionSelection:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)processButtonAction:(id)sender {
    // Deploy Test
    if (self.testSwipeButtonActionType == TGTBSwipeButtonActionDeploy) {
        NSString *indicatorString = [NSString stringWithFormat:@"%@...", [self.classHelper localizeString:@"Deploying"]];
        [HUD showUIBlockingIndicatorWithText:indicatorString];
        
        __weak typeof(self) wo = self;
        NSString *testid = [self.testObject valueForKey:@"id"];
        NSArray *sections = [NSArray arrayWithArray:[self.selectedSections allObjects]];
        
        [self.tgdm requestDeployForTest:testid sections:sections doneBlock:^(BOOL status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [wo showConfirmationMessageForAction:TGTBSwipeButtonActionDeploy status:status];
            });
        }];
    }
    
    if (self.testSwipeButtonActionType == TGTBSwipeButtonActionSaveAsPDF) {
        
        __weak typeof(self) wo = self;
        NSString *testid = [self.testObject valueForKey:@"id"];
        NSArray *sections = [NSArray arrayWithArray:[self.selectedSections allObjects]];
        
        
        NSString *email_flag = (self.sendMail) ? @"1" : @"0";
        NSString *playlist_flag = (self.sendPlaylist) ? @"1" : @"0";
        NSDictionary *info = @{@"qid":testid,@"cs_ids":sections,@"email":email_flag,@"playlist":playlist_flag};
        [self.tgdm requestPDFdata:info process:^(NSDictionary *data) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
//                [wo dismissViewControllerAnimated:YES completion:nil];
                [wo dismissViewControllerAnimated:YES completion:^{
            if (data != nil) {
                NSString *process_id = [NSString stringWithFormat:@"%@", data[@"process_id"]];
                if ([(NSObject *)wo.delegate respondsToSelector:@selector(didFinishGeneratingPDFprocess:testID:email:playlist:)]) {
                    [wo.delegate didFinishGeneratingPDFprocess:process_id testID:testid email:self.sendMail playlist:self.sendPlaylist];
                }
            }
        }];
            });
            
//            if (data != nil) {
//                NSString *process_id = [NSString stringWithFormat:@"%@", data[@"process_id"]];
//                if ([(NSObject *)wo.delegate respondsToSelector:@selector(didFinishGeneratingPDFprocess:testID:email:playlist:)]) {
//                    [wo.delegate didFinishGeneratingPDFprocess:process_id testID:testid email:self.sendMail playlist:self.sendPlaylist];
//                }
//            }
        }];
    }
}

#pragma mark - Confirmation Message for Action

- (void)showConfirmationMessageForAction:(int)action status:(BOOL)status {
    NSString *testTitle = [self.testObject valueForKey:@"name"];
    NSString *avTitle = @"";
    NSString *message = @"";
    NSString *butOkay = [self.classHelper localizeString:@"Okay"];
    
    if (status) {
        // Deploy Test
        if (action == TGTBSwipeButtonActionDeploy) {
            avTitle = [[self.classHelper localizeString:@"Deploy"] uppercaseString];
            message = [self.classHelper localizeString:@"You successfully deployed"];
            message = [NSString stringWithFormat:@"%@ %@.", message, testTitle];
        }
    }
    else {
        // Deploy Test
        if (action == TGTBSwipeButtonActionDeploy) {
            avTitle = [[self.classHelper localizeString:@"Deploy"] uppercaseString];
            message = [self.classHelper localizeString:@"was not deployed successfully"];
            message = [NSString stringWithFormat:@"%@ %@.", testTitle, message];
        }
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:avTitle
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *theAction = [UIAlertAction actionWithTitle:butOkay
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          if (status) {
                                                              [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                                                          }
                                                      }];
    [alert addAction:theAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Collection View Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.sectionData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TBSectionItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kSectionCellIdentifier forIndexPath:indexPath];

    NSManagedObject *mo = self.sectionData[indexPath.row];
    NSString *section_name = [mo valueForKey:@"section_name"];
    NSString *is_deployed = [mo valueForKey:@"is_deployed"];
    NSString *section_id = [mo valueForKey:@"course_section_id"];
    
    if ([is_deployed isEqualToString:@"1"]) {
        [self.selectedSections addObject:[NSNumber numberWithInteger:[section_id integerValue]]];
        [self.selectedIndexPaths addObject:indexPath];
        [self updateProcessButtonState];
    }
    
    cell.sectionNameLabel.text = section_name;
    cell.sectionNameLabel.lineBreakMode = NSLineBreakByCharWrapping;
    cell.sectionNameLabel.textAlignment = NSTextAlignmentJustified;
    cell.sectionNameLabel.numberOfLines = 0;
    [cell.sectionNameLabel sizeToFit];
    
    if (self.selectedIndexPaths) {
        [cell showSelection:[self.selectedIndexPaths containsObject:indexPath]];
    }

    return cell;
}

#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionView *)cv didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = self.sectionData[indexPath.row];
    NSString *is_deployed = [mo valueForKey:@"is_deployed"];
    
    if ([is_deployed isEqualToString:@"0"]) {
        
    TBSectionItemCell *cell = (TBSectionItemCell *)[cv cellForItemAtIndexPath:indexPath];
    [cell showSelection:![self.selectedIndexPaths containsObject:indexPath]];
    
    NSString *section_id = [mo valueForKey:@"course_section_id"];
    
    if (![self.selectedSections containsObject:section_id]) {
        [self.selectedSections addObject:[NSNumber numberWithInteger:[section_id integerValue]]];
        } else {
            [self.selectedSections removeObject:[NSNumber numberWithInteger:[section_id integerValue]]];
    }
    
    if (![self.selectedIndexPaths containsObject:indexPath]) {
        [self.selectedIndexPaths addObject:indexPath];
    }
    
    [self updateProcessButtonState];
        
    }
}

- (void)collectionView:(UICollectionView *)cv didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = self.sectionData[indexPath.row];
    NSString *is_deployed = [mo valueForKey:@"is_deployed"];
    
    if ([is_deployed isEqualToString:@"0"]) {
    TBSectionItemCell *cell = (TBSectionItemCell *)[cv cellForItemAtIndexPath:indexPath];
    [cell showSelection:NO];
    
    NSString *section_id =  [mo valueForKey:@"course_section_id"];
    
    if ([self.selectedSections containsObject:section_id]) {
        [self.selectedSections addObject:[NSNumber numberWithInteger:[section_id integerValue]]];
        } else {
            [self.selectedSections removeObject:[NSNumber numberWithInteger:[section_id integerValue]]];
    }
    
    if ([self.selectedIndexPaths containsObject:indexPath]) {
        [self.selectedIndexPaths removeObject:indexPath];
    }
    
    [self updateProcessButtonState];
}
}

#pragma mark - Collection View FlowLayout Delegate

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 20;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = (collectionView.frame.size.width / 2) - 10;
    return CGSizeMake(width, kCellHeight);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
   return CGSizeMake(0, 0);
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.collectionView performBatchUpdates:nil completion:nil];
}

#pragma mark - Update Process Button State

- (void)updateProcessButtonState {
    BOOL enable = (self.selectedSections.count > 0) ? YES : NO;
    self.processButton.enabled = enable;
    
    UIColor *color = (enable) ? UIColorFromHex(0x00B101) :  UIColorFromHex(0x555555);
    [self.processButton setColor:color forState:UIControlStateNormal];
}

@end
