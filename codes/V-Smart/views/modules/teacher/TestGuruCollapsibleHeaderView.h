//
//  TestGuruCollapsibleHeaderView.h
//  V-Smart
//
//  Created by Ryan Migallos on 05/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface TestGuruCollapsibleHeaderView : UITableViewHeaderFooterView

@property (strong, nonatomic) IBOutlet UIView *background;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *indicatorImage;
@property (strong, nonatomic) IBOutlet UIButton *button;
@property (assign, nonatomic) NSInteger section;

@end
