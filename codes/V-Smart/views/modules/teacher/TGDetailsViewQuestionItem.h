//
//  TGDetailsViewQuestionItem.h
//  V-Smart
//
//  Created by Ryan Migallos on 05/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGDetailsViewQuestionItem : UITableViewController

@property (assign, nonatomic) BOOL editMode;

@end
