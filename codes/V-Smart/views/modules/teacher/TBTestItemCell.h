//
//  TBTestItemCell.h
//  V-Smart
//
//  Created by Julius Abarra on 10/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"

@interface TBTestItemCell : MGSwipeTableCell

@property (strong, nonatomic) IBOutlet UILabel *testTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *testPointLabel;
@property (strong, nonatomic) IBOutlet UILabel *testActualPointLabel;
@property (strong, nonatomic) IBOutlet UILabel *testStageTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionActualCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *testDateModifiedLabel;
@property (strong, nonatomic) IBOutlet UILabel *testStartDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *testActualStartDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *testEndDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *testActualEndDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *testAttemptsLabel;
@property (strong, nonatomic) IBOutlet UILabel *testActualAttemptsLabel;
@property (strong, nonatomic) IBOutlet UILabel *testTimeLimitLabel;
@property (strong, nonatomic) IBOutlet UILabel *testActualTimeLimitLabel;

@property (strong, nonatomic) IBOutlet UIButton *moreActionButton;

- (void)updateTestStageTypeView:(NSString *)stageTypeID;

@end
