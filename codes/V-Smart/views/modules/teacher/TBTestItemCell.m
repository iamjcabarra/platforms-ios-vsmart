//
//  TBTestItemCell.m
//  V-Smart
//
//  Created by Julius Abarra on 10/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TBTestItemCell.h"
#import <QuartzCore/QuartzCore.h>

@interface TBTestItemCell ()

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIView *stageTypeView;

@end

@implementation TBTestItemCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)updateTestStageTypeView:(NSString *)stageTypeID {
    UIColor *color = [UIColor whiteColor];
    NSString *name = @"";
    
    // Non-Graded
    if ([stageTypeID isEqualToString:@"0"]) {
        color = UIColorFromHex(0x980305);
        name = NSLocalizedString(@"Non-Graded", nil);
    }
    
    // Graded
    if ([stageTypeID isEqualToString:@"1"]) {
        color = UIColorFromHex(0x167C80);
        name = NSLocalizedString(@"Graded", nil);
    }
    
    self.containerView.backgroundColor = color;
    self.stageTypeView.backgroundColor = color;
    
    self.testStageTypeLabel.backgroundColor = color;
    self.testStageTypeLabel.text = name;
}

@end
