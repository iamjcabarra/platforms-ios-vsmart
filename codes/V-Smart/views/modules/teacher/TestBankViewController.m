//
//  TestBankViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 07/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//
#import <MessageUI/MessageUI.h>
#import "TestBankViewController.h"
#import "TBNewTestItemCell.h"
#import "TestBankEmptyPlaceHolder.h"
#import "TBTestFilterOptionView.h"
#import "TBSectionSelectionView.h"
#import "TGTBTestDetailView.h"
#import "TGTBCreateTestView.h"
#import "TGTBTestActionConstants.h"
#import "TBClassHelper.h"
#import "TestGuruDataManager.h"
#import "TestGuruConstants.h"
#import "HUD.h"
#import "AppDelegate.h"
#import "ResourceManager.h"
#import "UIColor-Expanded.h"
#import "V_Smart-Swift.h" //SWIFT 2.1.1 IMPLEMENTATION

@interface TestBankViewController ()
<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate, MFMailComposeViewControllerDelegate,
TBSectionSelectionDelegate, TBTestFilterOptionViewDelegate, TestBankEmptyPlaceHolderDelegate, MGSwipeTableCellDelegate>

@property (strong, nonatomic) TBClassHelper *classHelper;
@property (strong, nonatomic) TestGuruDataManager *tgdm;

@property (strong, nonatomic) IBOutlet TestBankEmptyPlaceHolder *emptyView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITextField *filterOptionTextField;
@property (strong, nonatomic) IBOutlet UIButton *filterOptionButton;
@property (strong, nonatomic) IBOutlet UISearchBar *testSearchBar;
@property (strong, nonatomic) IBOutlet UIButton *testSortButton;
@property (strong, nonatomic) IBOutlet UIButton *loadMoreButton;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObject *selectedTestObject;

@property (strong, nonatomic) TBTestFilterOptionView *dropDownTestFilterOptionView;
//@property (strong, nonatomic) UIPopoverController *popOverController;

@property (strong, nonatomic) NSString *viewTitle;
@property (strong, nonatomic) NSString *temporaryFilterOptionValue;
@property (strong, nonatomic) NSString *temporarySearchKeyword;
@property (strong, nonatomic) NSString *testIDNew;

@property (assign, nonatomic) BOOL isViewByNewest;
@property (assign, nonatomic) BOOL isVersion25;

@property (assign, nonatomic) int crudActionType;
@property (assign, nonatomic) TGTBSwipeButtonActionType swipeButtonActionType;

@property (assign, nonatomic) NSInteger currentPageNumber;
@property (assign, nonatomic) NSInteger totalNumberOfTest;
@property (assign, nonatomic) NSInteger totalNumberOfFilteredTest;
@property (assign, nonatomic) NSInteger totalNumberOfPage;

@end

@implementation TestBankViewController

static NSString *kTestCellIdentifier = @"testItemCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Test Guru Data Manager
    self.tgdm = [TestGuruDataManager sharedInstance];
    
    // Initialize Flag for Pagination Request
    self.currentPageNumber = 0;
    self.totalNumberOfTest = 0;
    self.totalNumberOfFilteredTest = 0;
    self.totalNumberOfPage = 0;
    
    // Test List View
    self.temporarySearchKeyword = @"";
    [self setUpTestListTableView];
    
    // Filter Test
    self.temporaryFilterOptionValue = @"*";
    self.filterOptionTextField.text = [[self.classHelper localizeString:@"All"] uppercaseString];
    
    [self.filterOptionButton addTarget:self
                                action:@selector(testFilterButtonAction:)
                      forControlEvents:UIControlEventTouchUpInside];
    
    // Search Test
    self.testSearchBar.delegate = self;
    self.testSearchBar.placeholder = [self.classHelper localizeString:@"Search"];
    
    // Sort Test
    self.isViewByNewest = YES;
    [self.testSortButton addTarget:self
                            action:@selector(testSortButtonAction:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    // Load More
    self.loadMoreButton.hidden = YES;
    [self.loadMoreButton addTarget:self
                            action:@selector(loadMoreButtonAction:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    // Empty View
    [self setupBlankPage];
    [self showEmptyViewPlaceHolder:NO];
    
    CGFloat version = [[Utils getServerInstanceVersion] floatValue];
    
    NSLog(@"TEST GURU SERVER VERSION : %@", @(version) );
    
    self.isVersion25 = (version >= VSMART_SERVER_MAX_VER);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self customizeNavigationController];
    
    NSDictionary *parameters = [self getParametersForTestPaginationIsReset:YES shouldLoadNextPage:NO];
    [self listPaginatedTestForParameters:parameters];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Paginated Test List

- (NSDictionary *)getParametersForTestPaginationIsReset:(BOOL)isReset shouldLoadNextPage:(BOOL)nextPage {
    NSString *packageID = [self.tgdm stringValue:[self.tgdm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID]];
    NSString *paginatedTestCountLimit = [self.tgdm stringValue:[self.tgdm fetchObjectForKey:kTG_PAGINATION_LIMIT]];
    NSString *pageNumberString = @"1";
    NSString *sortedBy = [NSString stringWithFormat:@"%@date_modified", (self.isViewByNewest) ? @"-" : @""];
    
    if (!isReset) {
        NSInteger pageNumber = nextPage ? self.currentPageNumber + 1 : self.currentPageNumber;
        pageNumberString = [NSString stringWithFormat:@"%zd", pageNumber];
    }
    
    self.temporarySearchKeyword = [self.classHelper stringValue:self.temporarySearchKeyword];
    self.temporaryFilterOptionValue = [self.classHelper stringValue:self.temporaryFilterOptionValue];
    
    NSDictionary *parameters = @{@"package_id":packageID,
                                 @"count_limit":paginatedTestCountLimit,
                                 @"page_number":pageNumberString,
                                 @"search_keyw":self.temporarySearchKeyword,
                                 @"test_filter":self.temporaryFilterOptionValue,
                                 @"sorted_by":sortedBy
                                 };
    return parameters;
}

- (void)listPaginatedTestForParameters:(NSDictionary *)parameters {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", [self.classHelper localizeString:@"Loading"]];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    self.loadMoreButton.hidden = YES;
    
    NSString *courseid = [NSString stringWithFormat:@"%@", [self.tgdm fetchObjectForKey:kTGQB_SELECTED_COURSE_ID]];
    NSString *userid = [self.tgdm loginUser];
    
    __weak typeof(self) wo = self;
    
    [self.tgdm requestPaginatedTestListForCourse:courseid andUser:userid withParametersForPagination:parameters dataBlock:^(NSDictionary *data) {
        if (data != nil) {
            self.currentPageNumber = [data[@"current_page"] integerValue];
            self.totalNumberOfTest = [data[@"total_items"] integerValue];
            self.totalNumberOfFilteredTest = [data[@"total_filtered"] integerValue];
            self.totalNumberOfPage = [data[@"total_pages"] integerValue];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo reloadFetchedResultsController];
            [wo updateTotalTestCountTitle:wo.totalNumberOfTest];
            
            BOOL hideButton = (wo.totalNumberOfPage > wo.currentPageNumber) ? NO : YES;
            wo.loadMoreButton.hidden = hideButton;
            
            BOOL shouldShow = (wo.totalNumberOfFilteredTest == 0) ? YES :  NO;
            [wo showEmptyViewPlaceHolder:shouldShow];
            
            [HUD hideUIBlockingIndicator];
        });
    }];
}

#pragma mark - Test List View

- (void)setUpTestListTableView {
    // Set Protocols
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Allow Selection
    self.tableView.allowsSelection = YES;
    self.tableView.allowsMultipleSelection = NO;
    
    // Default Height
    self.tableView.rowHeight = 125.0f;
    
    // Resusable Cell
    UINib *testItemCellNib = [UINib nibWithNibName:@"TBNewTestItemCell" bundle:nil];
    [self.tableView registerNib:testItemCellNib forCellReuseIdentifier:kTestCellIdentifier];
}

#pragma mark - Test Filter Action

- (void)testFilterButtonAction:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    // Create Test Filter Reusable Controller
    self.dropDownTestFilterOptionView = [[TBTestFilterOptionView alloc] initWithNibName:@"TBTestFilterOptionView" bundle:nil];
    self.dropDownTestFilterOptionView.delegate = self;
    
    //// Create Test Filter Popover
    //self.popOverController = [[UIPopoverController alloc] initWithContentViewController:self.dropDownTestFilterOptionView];
    //self.popOverController.popoverContentSize = CGSizeMake(175.0f, 150.0f);
    //
    //// Render Popover
    //CGRect pRect = CGRectMake(button.bounds.origin.x, button.bounds.origin.y + 112, button.width, button.height);
    //[self.popOverController presentPopoverFromRect:pRect
    //                                        inView:button
    //                      permittedArrowDirections:0
    //                                      animated:YES];
    
    self.dropDownTestFilterOptionView.modalPresentationStyle = UIModalPresentationPopover;
    self.dropDownTestFilterOptionView.preferredContentSize = CGSizeMake(175.0f, 150.0f);
    self.dropDownTestFilterOptionView.popoverPresentationController.permittedArrowDirections = 0;
    self.dropDownTestFilterOptionView.popoverPresentationController.sourceView = button;
    self.dropDownTestFilterOptionView.popoverPresentationController.sourceRect = CGRectMake(button.bounds.origin.x, button.bounds.origin.y + 112, button.width, button.height);
    [self presentViewController:self.dropDownTestFilterOptionView animated:true completion:nil];
}

#pragma mark - Test Filter Delegate

- (void)selectedTestFilterOption:(NSDictionary *)optionDetail {
    NSString *testFilterOption = optionDetail[@"option"];
    NSString *testFilterOptionValue = optionDetail[@"value"];
    
    self.filterOptionTextField.text = testFilterOption;
    self.temporaryFilterOptionValue = testFilterOptionValue;
    
    NSDictionary *parameters = [self getParametersForTestPaginationIsReset:YES shouldLoadNextPage:NO];
    [self listPaginatedTestForParameters:parameters];
}

#pragma mark - Search Bar Delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.temporarySearchKeyword = searchBar.text;
    NSDictionary *parameters = [self getParametersForTestPaginationIsReset:YES shouldLoadNextPage:NO];
    [self listPaginatedTestForParameters:parameters];
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText isEqualToString:@""]) {
        self.temporarySearchKeyword = searchText;
        NSDictionary *parameters = [self getParametersForTestPaginationIsReset:YES shouldLoadNextPage:NO];
        [self listPaginatedTestForParameters:parameters];
    }
}

#pragma mark - Test Sort Action

- (void)testSortButtonAction:(id)sender {
    self.isViewByNewest = (self.isViewByNewest) ? NO : YES;
    
    NSDictionary *parameters = [self getParametersForTestPaginationIsReset:YES shouldLoadNextPage:NO];
    [self listPaginatedTestForParameters:parameters];
}

#pragma mark - Load More Action

- (void)loadMoreButtonAction:(id)sender {
    NSDictionary *parameters = [self getParametersForTestPaginationIsReset:NO shouldLoadNextPage:YES];
    [self listPaginatedTestForParameters:parameters];
}

#pragma mark - Navigation Bar

- (void)customizeNavigationController {
    UIColor *color = UIColorFromHex(0xF69C42);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    }
    else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

- (UIButton *)buttonWithName:(NSString *)title position:(CGFloat)position action:(SEL)action  {
    UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
    b.frame = CGRectMake(position, 0, 80, 44);
    b.showsTouchWhenHighlighted = YES;
    
    [b setTitle:title forState:UIControlStateNormal];
    [b setTitle:title forState:UIControlStateSelected];
    
    UIColor *normalColor = [UIColor darkGrayColor];
    [b setTitleColor:normalColor forState:UIControlStateNormal];
    UIColor *selectedColor = [UIColor blueColor];
    [b setTitleColor:selectedColor forState:UIControlStateSelected];
    [b addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    return b;
}

- (void)updateTotalTestCountTitle:(NSInteger)count {
    NSString *testCountTitleA = [self.classHelper localizeString:@"All Test"];
    NSString *testCountTitleB = [self.classHelper localizeString:@"All Tests"];
    NSString *testCountTitleC = (self.totalNumberOfTest > 1) ? testCountTitleB : testCountTitleA;
    
    NSString *countTitle = [NSString stringWithFormat:@"%@ (%zd)", testCountTitleC, count];
    self.title = countTitle;
    
    // Custom Back Bar Button Title
    self.viewTitle = self.title;
}

#pragma mark - Empty View Place Holder

- (void)setupBlankPage {
    self.emptyView.delegate = self;
}

- (void)showEmptyViewPlaceHolder:(BOOL)shouldShow {
    if (shouldShow) {
        self.emptyView.hidden = NO;
        self.tableView.hidden = YES;
        
        BOOL hideCreateButton = (self.totalNumberOfTest > 0) ? YES: NO;
        NSString *evpMessage = @"";
        
        if (self.totalNumberOfTest > 0) {
            evpMessage = [self.classHelper localizeString:@"No results found for"];
            evpMessage = [NSString stringWithFormat:@"%@ \"%@\"", evpMessage, self.testSearchBar.text];
            
            if ([self.testSearchBar.text isEqualToString:@""]) {
                evpMessage = [self.classHelper localizeString:@"No results found."];
            }
        }
        
        [self.emptyView showEmptyViewWithMessage:evpMessage shouldHideCreateButton:hideCreateButton];
    }
    else {
        self.emptyView.hidden = YES;
        self.tableView.hidden = NO;
    }
}

#pragma mark - Empty Place Holder Delegate

- (void)didFinishSelectingCreateAction {
    self.crudActionType = TGTBCrudActionTypeCreate;
    
    __weak typeof(self) wo = self;
    [self.tgdm insertNewTestWitContentBlock:^(NSString *test_id) {
        if ((test_id != nil) && (test_id.length > 0)) {
            dispatch_async(dispatch_get_main_queue(), ^{
                wo.testIDNew = [NSString stringWithFormat:@"%@", test_id];
                [wo performSegueWithIdentifier:@"showCreateTestView" sender:self];
            });
        }
    }];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView = tableView;
    
    TBNewTestItemCell *cell = [tableView dequeueReusableCellWithIdentifier:kTestCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TBNewTestItemCell *testCell = (TBNewTestItemCell *)cell;
    [self configureQuestionCell:testCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(TBNewTestItemCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    NSString *name = [mo valueForKey:@"name"];
    NSString *formatted_date_open = [[mo valueForKey:@"formatted_date_open"] uppercaseString];
    NSString *formatted_date_close = [[mo valueForKey:@"formatted_date_close"] uppercaseString];
    NSString *no_expiry = [mo valueForKey:@"no_expiry"];
    NSString *item_string = [NSString stringWithFormat:@"%@", [mo valueForKey:@"item_count"]];
    NSString *point_string = [NSString stringWithFormat:@"%@", [mo valueForKey:@"total_score"]];
    NSString *is_graded = [mo valueForKey:@"is_graded"];
    NSString *is_approved = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_approved"] ];
    
    cell.testStatus.hidden = (self.isVersion25) ? NO : YES;
    [self updateLabel:cell.testStatus status:is_approved];
    
    NSString *image_string = [is_graded isEqualToString:@"1"] ? @"graded.png" : @"non-graded.png";
    
    if ([no_expiry isEqualToString:@"1"]) {
        formatted_date_close = [self.classHelper localizeString:@"No Expiration"];
    }
    
    NSString *start_date_string = NSLocalizedString(@"Start Date", nil);
    NSString *end_date_string = NSLocalizedString(@"End Date", nil);
    
    cell.testTitle.text = name;
    cell.testStartDateLabel.text = [NSString stringWithFormat:@"%@: %@", start_date_string, formatted_date_open];
    cell.testEndDateLabel.text = [NSString stringWithFormat:@"%@: %@", end_date_string, formatted_date_close];
    cell.testStageTypeImage.image = [UIImage imageNamed:image_string];
    
    cell.itemsValueLabel.text = [NSString stringWithFormat:@"%@", item_string];
    [cell updateItemsLabel:item_string];
    
    cell.pointsValueLabel.text = [NSString stringWithFormat:@"%@", point_string];
    [cell updatePointsLabel:point_string];
    
    cell.delegate = self;
    cell.allowsMultipleSwipe = NO;
    cell.allowsButtonsWithDifferentWidth = NO;
    cell.allowsSwipeWhenTappingButtons = YES;
    cell.preservesSelectionStatus = NO;
    
    cell.lockImage.hidden = YES;
}

- (void) updateLabel:(UILabel *)label status:(NSString *)status {
    
    if ([status isEqualToString:@"0"]) {
        //PENDING
        label.text = NSLocalizedString(@"Pending", comment: nil);
        label.backgroundColor = UIColorFromHex(0xFFCC00);
    }
    
    if ([status isEqualToString:@"1"]) {
        //GREEN
        label.text = NSLocalizedString(@"Approved", comment: nil);
        label.backgroundColor = UIColorFromHex(0x4CD964);
    }
    
    if ([status isEqualToString:@""] || [status isEqualToString:@"2"]) {
        //RED
        label.text = NSLocalizedString(@"Disapproved", comment: nil);
        label.backgroundColor = UIColorFromHex(0xFF3B30);
    }
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedTestObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self prepareTestDetails:self.selectedTestObject forSegue:@"showTestDetailView"];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)prepareTestDetails:(NSManagedObject *)testObject forSegue:(NSString *)segueIdentifier {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", [self.classHelper localizeString:@"Please wait"]];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    NSString *testid = [testObject valueForKey:@"id"];
    __weak typeof(self) wo = self;
    
    [self.tgdm requestTestDetailsForTest:testid doneBlock:^(BOOL status) {
        if (status) {
            NSPredicate *predicate = [self.tgdm predicateForKeyPath:@"id" andValue:testid];
            self.selectedTestObject = [self.tgdm getEntity:kTestInformationEntity predicate:predicate];
            
            [self.tgdm copyQuestionItemsFromTestInformation:self.selectedTestObject doneBlock:^(BOOL status) {
                if (status) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [wo performSegueWithIdentifier:segueIdentifier sender:nil];
                    });
                }
            }];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD hideUIBlockingIndicator];
        });
    }];
}

#pragma mark - Swipe Table View Cell Delegate

- (BOOL)swipeTableCell:(MGSwipeTableCell*)cell canSwipe:(MGSwipeDirection)direction {
    if (direction == MGSwipeDirectionRightToLeft) {
        return YES;
    }
    
    return NO;
}

- (NSArray *)swipeTableCell:(MGSwipeTableCell *)cell swipeButtonsForDirection:(MGSwipeDirection)direction swipeSettings:(MGSwipeSettings *)swipeSettings expansionSettings:(MGSwipeExpansionSettings *)expansionSettings {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    self.selectedTestObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if (direction == MGSwipeDirectionRightToLeft) {
        swipeSettings.transition = MGSwipeTransitionRotate3D;
        swipeSettings.threshold = 0.5f;
        swipeSettings.offset = 1.0f;
        swipeSettings.showAnimation.duration = 0.3f;
        swipeSettings.showAnimation.easingFunction = MGSwipeEasingFunctionLinear;
        swipeSettings.hideAnimation.duration = 0.3f;
        swipeSettings.hideAnimation.easingFunction = MGSwipeEasingFunctionLinear;
        swipeSettings.keepButtonsSwiped = YES;
        
        UIColor *buttonBGColor = [UIColor whiteColor];
        UIEdgeInsets insets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
        CGFloat buttonWidth = cell.frame.size.width / 4.0f - 1.0f;
        
        __weak typeof(self) wo = self;
        
        NSString *buttonTitle1 = [self.classHelper localizeString:@"Delete"];
        MGSwipeButton *button1 = [MGSwipeButton buttonWithTitle:buttonTitle1
                                                           icon:[UIImage imageNamed:@"delete52px.png"]
                                                backgroundColor:buttonBGColor
                                                         insets:insets
                                                       callback:^BOOL(MGSwipeTableCell *sender) {
                                                           [self deleteTestAction:sender];
                                                           return NO;
                                                       }];
        
        button1.buttonWidth = buttonWidth;
        [button1 centerIconOverTextWithSpacing:20.0f];
        
        NSString *buttonTitle2 = [self.classHelper localizeString:@"Edit"];
        MGSwipeButton *button2 = [MGSwipeButton buttonWithTitle:buttonTitle2
                                                           icon:[UIImage imageNamed:@"edit52px.png"]
                                                backgroundColor:buttonBGColor
                                                         insets:insets
                                                       callback:^BOOL(MGSwipeTableCell *sender) {
                                                           self.crudActionType = TGTBCrudActionTypeEdit;
                                                           [self prepareTestDetails:self.selectedTestObject forSegue:@"showCreateTestView"];
                                                           
                                                           return NO;
                                                       }];
        
        button2.buttonWidth = buttonWidth;
        [button2 centerIconOverTextWithSpacing:20.0f];
        
        NSString *buttonTitle3 = [self.classHelper localizeString:@"Save to PDF"];
        MGSwipeButton *button3 = [MGSwipeButton buttonWithTitle:buttonTitle3
                                                           icon:[UIImage imageNamed:@"pdf52px.png"]
                                                backgroundColor:buttonBGColor
                                                         insets:insets
                                                       callback:^BOOL(MGSwipeTableCell *sender) {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               wo.swipeButtonActionType = TGTBSwipeButtonActionSaveAsPDF;
                                                               [wo performSegueWithIdentifier:@"showSectionSelectionView" sender:nil];
                                                           });
                                                           
                                                           return NO;
                                                       }];
        
        button3.buttonWidth = buttonWidth;
        [button3 centerIconOverTextWithSpacing:20.0f];
        
        NSString *buttonTitle4 = [self.classHelper localizeString:@"Deploy"];
        MGSwipeButton *button4 = [MGSwipeButton buttonWithTitle:buttonTitle4
                                                           icon:[UIImage imageNamed:@"deploy52px.png"]
                                                backgroundColor:buttonBGColor
                                                         insets:insets
                                                       callback:^BOOL(MGSwipeTableCell *sender) {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               wo.swipeButtonActionType = TGTBSwipeButtonActionDeploy;
                                                               [wo performSegueWithIdentifier:@"showSectionSelectionView" sender:nil];
                                                           });
                                                           
                                                           return NO;
                                                       }];
        
        button4.buttonWidth = buttonWidth;
        [button4 centerIconOverTextWithSpacing:20.0f];
        
        NSString *is_approved = [self.tgdm stringValue:[self.selectedTestObject valueForKey:@"is_approved"]];
        
       
        if (self.isVersion25 == NO) {
            is_approved = @"1";
        }
        
        return [is_approved isEqualToString:@"1"] ? @[button1, button2, button3, button4] : @[button1, button2];
    }
    
    return nil;
}

//- (void)swipeTableCell:(MGSwipeTableCell*)cell didChangeSwipeState:(MGSwipeState)state gestureIsActive:(BOOL)gestureIsActive {
//    if (state == MGSwipeStateSwipingRightToLeft) {
//        cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        cell.layer.borderWidth = 1.0f;
//        cell.layer.shadowOpacity = 1.0f;
//        cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:cell.bounds].CGPath;
//        cell.layer.masksToBounds = NO;
//    }
//    else {
//        cell.layer.borderColor = [UIColor clearColor].CGColor;
//        cell.layer.borderWidth = 0.0f;
//        cell.layer.shadowOpacity = 0.0f;
//        cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:cell.bounds].CGPath;;
//        cell.layer.masksToBounds = YES;
//    }
//}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:self.tableView.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

#pragma mark - Test Delete Action

- (void)deleteTestAction:(id)sender {
    NSString *avTitle = [[self.classHelper localizeString:@"Delete"] uppercaseString];
    NSString *message = [self.classHelper localizeString:@"Are you sure you want to delete this test?"];
    NSString *butPosR = [self.classHelper localizeString:@"Yes"];
    NSString *butNegR = [self.classHelper localizeString:@"No"];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:avTitle
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *posAction = [UIAlertAction actionWithTitle:butPosR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          NSString *testid = [self.selectedTestObject valueForKey:@"id"];
                                                          [self processDeleteForTest:testid];
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                      }];
    
    UIAlertAction *negAction = [UIAlertAction actionWithTitle:butNegR
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                      }];
    [alert addAction:posAction];
    [alert addAction:negAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)processDeleteForTest:(NSString *)testid {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", [self.classHelper localizeString:@"Deleting"]];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    __weak typeof(self) wo = self;
    [self.tgdm requestDeleteForTest:testid doneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD hideUIBlockingIndicator];
            [wo showConfirmationMessageForDeleteAction:status];
        });
    }];
}

- (void)showConfirmationMessageForDeleteAction:(BOOL)status {
    NSString *avTitle = [[self.classHelper localizeString:@"Delete"] uppercaseString];
    NSString *butOkay = [self.classHelper localizeString:@"Okay"];
    NSString *message = @"";
    
    if (status) {
        message = [self.classHelper localizeString:@"Test successfully deleted."];
        NSDictionary *parameters = [self getParametersForTestPaginationIsReset:NO shouldLoadNextPage:NO];
        [self listPaginatedTestForParameters:parameters];
    }
    else {
        message = [self.classHelper localizeString:@"Test Bank is failed to delete test."];
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:avTitle
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *theAction = [UIAlertAction actionWithTitle:butOkay
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                      }];
    [alert addAction:theAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tgdm.mainContext;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTestEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSSortDescriptor *index = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
    [fetchRequest setSortDescriptors:@[index]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - Reload Fetched Results

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error: %@", [err localizedDescription]);
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Test Detail View
    if ([segue.identifier isEqualToString:@"showTestDetailView"]) {
        TGTBTestDetailView *testDetailView = (TGTBTestDetailView *)[segue destinationViewController];
        testDetailView.testObject = self.selectedTestObject;
        [self hideBackButtonTitle];
    }
    
    // Create or Edit Test View
    if ([segue.identifier isEqualToString:@"showCreateTestView"]) {
        if (self.crudActionType == TGTBCrudActionTypeCreate) {
            if (self.testIDNew.length > 0) {
                NSPredicate *predicate = [self.tgdm predicateForKeyPath:@"id" andValue:self.testIDNew];
                NSManagedObject *object = [self.tgdm getEntity:kTestInformationEntity predicate:predicate];
                
                if (object != nil) {
                    self.selectedTestObject = object;
                    TGTBCreateTestView *createTestView = (TGTBCreateTestView *)[segue destinationViewController];
                    createTestView.customBackBarButtonTitle = self.viewTitle;
                    createTestView.testObject = object;
                    createTestView.crudActionType = self.crudActionType;
                }
            }
        }
        
        if (self.crudActionType == TGTBCrudActionTypeEdit) {
            TGTBCreateTestView *createTestView = (TGTBCreateTestView *)[segue destinationViewController];
            createTestView.customBackBarButtonTitle = self.viewTitle;
            createTestView.testObject = self.selectedTestObject;
            createTestView.crudActionType = self.crudActionType;
        }
    }
    
    // Section Selection View
    if ([segue.identifier isEqualToString:@"showSectionSelectionView"]) {
        TBSectionSelectionView *sectionView = (TBSectionSelectionView *)[segue destinationViewController];
        sectionView.testSwipeButtonActionType = self.swipeButtonActionType;
        sectionView.delegate = self;
        sectionView.testObject = self.selectedTestObject;
    }
}

- (void)hideBackButtonTitle {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

#pragma mark - TBSectionSelectionDelegate

- (void)didFinishGeneratingPDFprocess:(NSString *)processid testID:(NSString *)testid email:(BOOL)status playlist:(BOOL)flag {
    
    if (processid != nil ) {
        
        NSString *process_id = [NSString stringWithFormat:@"%@", processid];
        
        NSDictionary *info = @{@"process":process_id,
                               @"email":@(YES),
                               @"playlist":@(flag),
                               @"testid": testid
                               };
        
        NSLog(@"procesxs id : %@", process_id);
        __weak typeof(self) wo = self;
        
        
//            [ProgressHUD show:@"Generating PDF" Interaction:NO];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD showUIBlockingIndicatorWithText:@"Generating PDF"];
        });
        ResourceManager *r = (ResourceManager *)[AppDelegate resourceInstance];
        [r socketEvent:@"process_id_status" info:info dataBlock:^(NSDictionary *data) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
            });
            
            if (data != nil) {
                NSString *downloadlink = [NSString stringWithFormat:@"%@", data[@"link"] ];
                NSLog(@"DOWNLOAD PDF LINK : %@", downloadlink);
                //EMAIL
                if (status == YES) {
                    NSLog(@"initiate email");
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [wo sendEmail:downloadlink];
                    });
                }
            }
        }];
    }
}

- (void)sendEmail:(NSString *)body {
    
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
        emailComposer.mailComposeDelegate = self;
        [emailComposer setSubject:@"Test Papers"];
        NSString *filename = [NSString stringWithFormat:@"%@", [body lastPathComponent] ];
        NSString *message = [NSString stringWithFormat:@"<p>Link : <a href='%@'>%@</a></p>", body, filename];
        [emailComposer setMessageBody:message isHTML:YES];
        
        [self presentViewController:emailComposer animated:YES completion:^{
            NSLog(@"presented email composer...");
        }];
        
    } else {
        NSLog(@"no email capabilities...");
        NSString *message = @"Please enable default email";
        AlertWithMessageAndDelegate(@"Save to PDF", message, self);
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    /*
     MFMailComposeResultCancelled,
     MFMailComposeResultSaved,
     MFMailComposeResultSent,
     MFMailComposeResultFailed
     */
    
    if (result == MFMailComposeResultFailed) {
        NSLog(@"faild composing email...");
    }
    
    if (error) {
        NSLog(@"email error: %@", [error localizedDescription]);
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
