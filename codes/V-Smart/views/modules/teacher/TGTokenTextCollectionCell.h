//
//  TGTokenTextCollectionCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 06/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGTokenTextCollectionCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *tokenText;
@property (strong, nonatomic) IBOutlet UIImageView *tokenImageView;
@property (strong, nonatomic) IBOutlet UIButton *tokenDeleteButton;

@end
