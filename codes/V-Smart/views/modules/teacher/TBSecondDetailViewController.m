//
//  TBSecondDetailViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 03/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "TBSecondDetailViewController.h"
#import "TBDetailViewController.h"
#import "TBQuestionItemCell.h"
#import "TBSectionSelectionView.h"
#import "TBClassHelper.h"
#import "TestGuruDataManager.h"
#import "UIImageView+WebCache.h"

////////////// NSSTRING HTML CATEGORY IMPLEMENTATION //////////////

@interface NSString (HtmlCustomCheck)
- (BOOL)containsHTML;
@end

@implementation NSString (HtmlCustomCheck)

- (BOOL)containsHTML {
    NSString *stringcopy = [self copy];
    NSString *expression = @"<[^>]+>";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:stringcopy
                                                        options:0
                                                          range:NSMakeRange(0, [stringcopy length])];
    if (numberOfMatches == 1) {
        return YES;
    }
    
    return NO;
}

@end

////////////// NSSTRING HTML CATEGORY IMPLEMENTATION //////////////

@interface TBSecondDetailViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate>

@property (strong, nonatomic) TBClassHelper *classHelper;
@property (strong, nonatomic) TestGuruDataManager *tgdm;

@property (strong, nonatomic) NSManagedObject *testDetail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) IBOutlet UIView *testInfoView;
@property (strong, nonatomic) IBOutlet UILabel *testTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *generalInstructionsLabel;
@property (strong, nonatomic) IBOutlet UITextView *generalInstructionsValueView;

@property (strong, nonatomic) IBOutlet UILabel *maxAttemptsLabel;
@property (strong, nonatomic) IBOutlet UILabel *maxAttemptsValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *startDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *startDateValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *timeLimitLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLimitValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *endDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *endDateValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *passingScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *passingScoreValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalPointsLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalPointsValueLabel;

@property (strong, nonatomic) IBOutlet UIButton *previewButton;

@property (strong, nonatomic) IBOutlet UILabel *assignedQuestionsLabel;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIButton *sortButton;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIButton *deployButton;
@property (strong, nonatomic) IBOutlet UIButton *saveToPDFButton;
@property (strong, nonatomic) IBOutlet UIButton *editButton;

@property (strong, nonatomic) IBOutlet UILabel *deployLabel;
@property (strong, nonatomic) IBOutlet UILabel *saveToPDFLabel;
@property (strong, nonatomic) IBOutlet UILabel *editLabel;

@property (strong, nonatomic) NSString *temporarySearchKeyword;

@property (assign, nonatomic) int actionType;
@property (assign, nonatomic) BOOL isViewByNewest;

@end

@implementation TBSecondDetailViewController

static NSString *kQuestionCellIdentifer = @"questionCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Test Guru Data Manager
    self.tgdm = [TestGuruDataManager sharedInstance];
    
    self.testInfoView.layer.borderColor = [UIColor grayColor].CGColor;
    self.testInfoView.layer.borderWidth = 1.0f;
    
    // Just Temporary
    [self loadSelectedTestHeaderInformation];
    
    // Question Table View
    [self setUpSelectedQuestionsView];
    
    // Search Question
    self.temporarySearchKeyword = @"";
    self.searchBar.delegate = self;
    self.searchBar.placeholder = [self.classHelper localizeString:@"Search"];
    
    // Sort Question
    self.isViewByNewest = NO;
    [self.sortButton addTarget:self
                        action:@selector(sortQuestionButtonAction:)
              forControlEvents:UIControlEventTouchUpInside];
    
    
    // Test Action Option
    [self.deployButton addTarget:self
                          action:@selector(deployButtonAction:)
                forControlEvents:UIControlEventTouchUpInside];
    
    [self.saveToPDFButton addTarget:self
                             action:@selector(saveToPDFButtonAction:)
                   forControlEvents:UIControlEventTouchUpInside];
    
    [self.editButton addTarget:self
                        action:@selector(editButtonAction:)
              forControlEvents:UIControlEventTouchUpInside];
    
    [self.generalInstructionsValueView setFont:[UIFont systemFontOfSize:15]];
    
    // Just Temporary
    self.title = @"Test Details";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Test Header View

- (void)loadSelectedTestHeaderInformation {
    self.testTitleLabel.text = [NSString stringWithFormat:@"   %@", [self.testObject valueForKey:@"name"]];
    self.generalInstructionsValueView.text = [self.testObject valueForKey:@"general_instruction"];
    self.maxAttemptsValueLabel.text = [self.testObject valueForKey:@"attempts"];
    self.timeLimitValueLabel.text = [self.testObject valueForKey:@"time_limit"];
    self.passingScoreValueLabel.text = [self.testObject valueForKey:@"passing_score"];
    
    NSString *date_open = [self.testObject valueForKey:@"date_open"];
    NSString *date_close = [self.testObject valueForKey:@"date_close"];
    
    NSString *formatted_date_open = [self.classHelper transformDateString:date_open
                                                               fromFormat:@"yyyy-MM-dd HH:mm:ss"
                                                                 toFormat:@"EEE. MMM. dd, yyyy hh:mm a"];
    
    NSString *formatted_date_close = [self.classHelper transformDateString:date_close
                                                                fromFormat:@"yyyy-MM-dd HH:mm:ss"
                                                                  toFormat:@"EEE. MMM. dd, yyyy hh:mm a"];
    self.startDateValueLabel.text = formatted_date_open;
    self.endDateValueLabel.text = formatted_date_close;
}

#pragma mark - Search Bar Delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.temporarySearchKeyword = searchBar.text;
    [self reloadFetchedResultsController];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.temporarySearchKeyword = searchText;
    [self reloadFetchedResultsController];
}

#pragma mark - Sort Action

- (void)sortQuestionButtonAction:(id)sender {
    self.isViewByNewest = (self.isViewByNewest) ? NO : YES;
    [self reloadFetchedResultsController];
}

#pragma mark - Button Action

- (void)deployButtonAction:(id)sender {
    self.actionType = 1;
    [self performSegueWithIdentifier:@"showSectionSelectionView" sender:nil];
}

- (void)saveToPDFButtonAction:(id)sender {
    self.actionType = 2;
    [self performSegueWithIdentifier:@"showSectionSelectionView" sender:nil];
}

- (void)editButtonAction:(id)sender {
    [self performSegueWithIdentifier:@"showTestDetail" sender:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Section Selection View
    if ([segue.identifier isEqualToString:@"showSectionSelectionView"]) {
        TBSectionSelectionView *sectionView = (TBSectionSelectionView *)[segue destinationViewController];
        sectionView.testSwipeButtonActionType = self.actionType;
        sectionView.testObject = self.testObject;
    }
    
    // Edit Test View
    if ([segue.identifier isEqualToString:@"showTestDetail"]) {
        TBDetailViewController *firstDetailView = (TBDetailViewController *)[segue destinationViewController];
        firstDetailView.customBackBarButtonTitle = self.title;
        firstDetailView.actionType = 2;
    }
}

#pragma mark - Assigned Questions View

- (void)setUpSelectedQuestionsView {
    // Set Protocols
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Allow Selection
    self.tableView.allowsSelection = YES;
    self.tableView.allowsMultipleSelection = NO;
    
    // Default Height
    self.tableView.rowHeight = 215.0f;
    
    // Resusable Cell
    UINib *questionItemCellNib = [UINib nibWithNibName:@"TBQuestionItemCell" bundle:nil];
    [self.tableView registerNib:questionItemCellNib forCellReuseIdentifier:kQuestionCellIdentifer];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    return count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo name];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kQuestionCellIdentifer forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TBQuestionItemCell *questionCell = (TBQuestionItemCell *)cell;
    [self configureQuestionCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(TBQuestionItemCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    NSString *question_name = [mo valueForKey:@"question_name"];
    NSString *date_modified = [mo valueForKey:@"date_modified"];
    NSString *date_string = [self.classHelper transformDateString:date_modified
                                                       fromFormat:@"yyyy-MM-dd HH:mm:ss"
                                                         toFormat:@"EEE. MMM. dd, yyyy hh:mm a"];
    
    NSString *question_text = [mo valueForKey:@"question_text"];
    NSString *question_type_name = [mo valueForKey:@"question_type_name"];
    NSString *question_type_id = [mo valueForKey:@"question_type_id"];
    
    // Not included in API
    // Hard Code
    NSString *proficiency_level_id = @"1";
    NSString *proficiency_name = @"Easy";
    
    cell.questionImage.image = nil;
    
    NSString *firstImage = [self getFirstImage:mo];
    if (firstImage.length > 0) {
        [cell.questionImage sd_setImageWithURL:[NSURL URLWithString:firstImage]];
    }
    
    cell.questionTitleLabel.text = question_name;
    cell.dateLabel.text = [date_string uppercaseString];
    cell.questionTextLabel.text = question_text;
    cell.questionTextLabel.hidden = YES;
    
    if ([question_text containsHTML]) {
        NSData *htmlData = [question_text dataUsingEncoding:NSUnicodeStringEncoding];
        NSDictionary *options = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType};
        NSAttributedString *attributed_string = [[NSAttributedString alloc] initWithData:htmlData
                                                                                 options:options
                                                                      documentAttributes:nil
                                                                                   error:nil];
        cell.questionTextLabel.attributedText = attributed_string;
    [cell loadWebViewWithContents:question_text];
    }
    
    cell.questionTypeLabel.text = question_type_name;
    cell.questionDifficultyLabel.text = proficiency_name;
    
    [cell updateQuestionTypeView:question_type_id];
    [cell updateDifficultyLevelView:proficiency_level_id];
}

- (NSString *)getFirstImage:(NSManagedObject *)mo {
    NSSet *images = [mo valueForKey:@"question_images"];
    NSSortDescriptor *indexDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
    NSArray *sortedImages = [images sortedArrayUsingDescriptors:[NSArray arrayWithObject:indexDescriptor]];
    
    for (NSManagedObject *mo in sortedImages) {
        NSString *imageURL = [mo valueForKey:@"image"];
        
        if (imageURL.length > 0) {
            return imageURL;
        }
    }
    
    return @"";
}

#pragma mark - Table View Delegate

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [self initiateDelete:mo];
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *actionString = [self.classHelper localizeString:@"Remove"];
    return actionString;
}

- (void)initiateDelete:(NSManagedObject *)mo {
    NSString *avTitle = [[self.classHelper localizeString:@"Remove"] uppercaseString];
    NSString *message = [self.classHelper localizeString:@"Are you sure you want to remove this question from this test?"];
    NSString *posResp = [self.classHelper localizeString:@"Yes"];
    NSString *negResp = [self.classHelper localizeString:@"No"];
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:avTitle
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *posAlertAction = [UIAlertAction actionWithTitle:posResp
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               [self.tgdm.mainContext deleteObject:mo];
                                                               [self reloadFetchedResultsController];
                                                           }];
    
    UIAlertAction *negAlertAction = [UIAlertAction actionWithTitle:negResp
                                                             style:UIAlertActionStyleDefault
                                                           handler:nil];
    [alertController addAction:posAlertAction];
    [alertController addAction:negAlertAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tgdm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTestQuestionItemEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSPredicate *predicate1 = [self predicateForKeyPath:@"quiz_id" value:[self.testObject valueForKey:@"id"]];
    NSPredicate *predicate2 = [self predicateForKeyPathContains:@"question_name" value:self.temporarySearchKeyword];
    NSCompoundPredicate *cp = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1]];
    
    if (![self.searchBar.text isEqualToString:@""]) {
        cp = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    }
    
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:self.isViewByNewest];
    [fetchRequest setSortDescriptors:@[date_modified]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - Reloading of Results

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

@end
