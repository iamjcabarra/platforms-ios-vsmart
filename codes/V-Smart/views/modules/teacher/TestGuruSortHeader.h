//
//  TestGuruSortHeader.h
//  V-Smart
//
//  Created by Ryan Migallos on 26/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TestGuruSortHeaderDelegate <NSObject>
@optional
- (void)sortDidSelectButtonAction:(NSString *)actionType withSortData:(NSDictionary *)sortData;
@optional
- (void)sortDidSelectButtonAction:(NSString *)actionType withSortDescriptors:(NSMutableArray *)sortDescriptors;
@optional
- (void)sortOptionCount:(NSUInteger)count;
@end

@interface TestGuruSortHeader : UIViewController

@property (weak, nonatomic) id <TestGuruSortHeaderDelegate> delegate;

@end
