//
//  TGTMCourseTableViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 23/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol TGTMCourseTableViewControllerDelegate: class {
    func selectedCourseObject(_ object: [String: AnyObject])
}

class TGTMCourseTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    weak var delegate: TGTMCourseTableViewControllerDelegate?
    
    // MARK: - Data Manager
    
    fileprivate lazy var tgmDataManager: TestGuruDataManager = {
        let tgdm = TestGuruDataManager.sharedInstance()
        return tgdm!
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadCourseList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - List Course
    
    func loadCourseList() {
        let coordinator = self.tgmDataManager.loginUser()
        self.tgmDataManager.requestCourseList(forCoordinator: coordinator) { (success) in }
//        self.tgmDataManager.requestCourseList(forCoordinatorID: coordinator) { (success) in }
    }
    
    // MARK: - Table View Data Source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: UITableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath) 
        cell.textLabel?.text = mo.value(forKey: "name") as? String
    }
    
    // MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        let id = mo.value(forKey: "id") as! Int
        let name = mo.value(forKey: "name") as! String
        let courseObject: [String : AnyObject] = ["id": id as AnyObject, "name": name as AnyObject]
        
        self.dismiss(animated: true, completion: {
            self.delegate?.selectedCourseObject(courseObject)
        })
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kCoordinatorCourseEntity)
        
        let ctx = self.tgmDataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: kCoordinatorCourseEntity)
        fetchRequest.fetchBatchSize = 20
        
        let sortDescriptor = NSSortDescriptor.init(key: "sorter", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.tableView.cellForRow(at: indexPath) {
                    self.configureCell(cell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
}

