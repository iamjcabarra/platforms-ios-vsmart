//
//  QuestionImageRadioItemWithMathJaxCell.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 01/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuestionImageRadioItemWithMathJaxCellDelegate <NSObject>
@optional
- (void)didFinishloadingChoice:(NSIndexPath *)indexPath;
@end

@interface QuestionImageRadioItemWithMathJaxCell : UITableViewCell <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *radioButton;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIImageView *choiceImage;
@property (weak, nonatomic) IBOutlet UIButton *choiceImageButton;

@property (weak, nonatomic) NSIndexPath *indexPath;

@property (assign, nonatomic) BOOL isForTestViewResults;

@property (weak, nonatomic) id <QuestionImageRadioItemWithMathJaxCellDelegate> delegate;

- (void)loadWebViewWithContents:(NSString *)string;
- (void)reconfigureWebViewForTestResults:(BOOL)reconfigure;

@end
