//
//  TGQuestionDetailsView.m
//  V-Smart
//
//  Created by Ryan Migallos on 07/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TGQuestionDetailsView.h"
#import "TestGuruDataManager.h"
#import "TGCollapsibleHeaderView.h"
#import "TGDetailsViewQuestionItem.h"
#import "QuestionSamplerView.h"
#import "HUD.h"

@interface TGQuestionDetailsView () <QuestionSamplerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) TestGuruDataManager *tm;


//QUESTION PROPERTY
@property (strong, nonatomic) IBOutlet UILabel *headerPropertyLabel;
@property (strong, nonatomic) IBOutlet UIButton *headerPropertyButton;
@property (strong, nonatomic) IBOutlet UIImageView *headerPropertyImage;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *headerPropertyHeight;

//QUESTION DETAILS
@property (strong, nonatomic) IBOutlet UILabel *headerDetailsLabel;
@property (strong, nonatomic) IBOutlet UIButton *headerDetailsButton;

//SAVE BUTTONS
@property (strong, nonatomic) IBOutlet UIButton *buttonSaveClose;
@property (strong, nonatomic) IBOutlet UIButton *buttonSavePreview;

//NAVIGATION BUTTON
@property (strong, nonatomic) UIBarButtonItem *leftButton;
@property (strong, nonatomic) UIBarButtonItem *rightButton;

@property (strong, nonatomic) TGCollapsibleHeaderView *headerView;
@property (strong, nonatomic) TGDetailsViewQuestionItem *listView;

@property (strong, nonatomic) MBProgressHUD *hud;

@end

@implementation TGQuestionDetailsView

static CGFloat kZeroHeight = 0.0f;
static CGFloat kNormalHeight = 333.0f;
static NSString *kCellIdentifier = @"cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tm = [TestGuruDataManager sharedInstance];
    
    // Do any additional setup after loading the view.
    [self setupNavigationButtons];
    [self setupQuestionPropertyComponent];
    [self setupLocalization];
    [self loadValues];
    
    [self.buttonSaveClose addTarget:self
                             action:@selector(saveAndCloseAction:)
                   forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.buttonSavePreview addTarget:self
                               action:@selector(saveAndPreviewAction:)
                     forControlEvents:UIControlEventTouchUpInside];
    
//    self.buttonSaveClose.userInteractionEnabled = NO;
//    self.buttonSavePreview.userInteractionEnabled = NO;
    
    
    if (self.hud == nil) {
        self.hud = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:self.hud];
    }
}

- (void)saveAndPreviewAction:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"SAVE_AND_PREVIEW_QUESTIONS" sender:self];
}

- (void)didFinishSaveInformation:(BOOL)state {
    
    [self saveAllComponents];
}

- (void)saveAndCloseAction:(UIButton *)sender {
    
    [self saveAllComponents];
}

- (void)saveAllComponents {
        
    NSDictionary *headerData = [self.headerView getUserValues];
    NSLog(@"HEADER DATA : %@", headerData);
    
    __weak typeof(self) wo = self;
    if (self.editMode == NO) {
        [self.tm requestNewQuestionsWithHeaderData:headerData doneBlock:^(BOOL status) {
            if (status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // ENHANCEMENT#285
                    // Echo message after successfullly creating a new question
                    NSString *butOkay = NSLocalizedString(@"Okay", nil);
                    NSString *message = NSLocalizedString(@"A new question has been successfully created.", nil);
                    
                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                                 message:message
                                                                delegate:wo
                                                       cancelButtonTitle:butOkay
                                                       otherButtonTitles:nil];
                    [av show];
                    

                    [self.navigationController popViewControllerAnimated:NO];
                    
                    if ([(NSObject*)wo.delegate respondsToSelector:@selector(didFinishCreateOperation:)] ) {
                        [wo.delegate didFinishCreateOperation:YES];
                    }

                });
            } else {
                [self saveOperationAlertError:wo];
            }
        }];
    }
    
    if (self.editMode == YES) {
        [self.tm requestUpdateDetailsForQuestion:self.mo headerData:headerData doneBlock:^(BOOL status) {
            if (status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // ENHANCEMENT#285
                    // Echo message after successfullly creating a new question
                    NSString *butOkay = NSLocalizedString(@"Okay", nil);
                    NSString *message = NSLocalizedString(@"Question has been successfully updated.", nil);
                    
                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                                 message:message
                                                                delegate:wo
                                                       cancelButtonTitle:butOkay
                                                       otherButtonTitles:nil];
                    [av show];
                    
                    if ([(NSObject*)wo.delegate respondsToSelector:@selector(didFinishCreateOperation:)] ) {
                        [wo.delegate didFinishCreateOperation:YES];
                    }
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                });
            } else {
                [self saveOperationAlertError:wo];
            }
        }];
    }
}

- (void)saveOperationAlertSuccess:(id)object {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *avTitle = NSLocalizedString(@"Save Error", nil);
        NSString *butOkay = NSLocalizedString(@"Okay", nil);
        NSString *message = NSLocalizedString(@"There must be at least 2 choices for each question, one of which must be chosen as the correct answer.", nil);
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:avTitle
                                                     message:message
                                                    delegate:object
                                           cancelButtonTitle:butOkay
                                           otherButtonTitles:nil];
        [av show];
    });
}

- (void)saveOperationAlertError:(id)object {

    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *avTitle = NSLocalizedString(@"Save Error", nil);
        NSString *butOkay = NSLocalizedString(@"Okay", nil);
        NSString *message = NSLocalizedString(@"There must be at least 2 choices for each question, one of which must be chosen as the correct answer.", nil);
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:avTitle
                                                     message:message
                                                    delegate:object
                                           cancelButtonTitle:butOkay
                                           otherButtonTitles:nil];
        [av show];
    });
}

//- (void)removeCreated

- (void)loadValues {
    
    if (self.editMode == YES) {
        [self loadUserValues];
    }
    
    if (self.editMode == NO) {
        [self loadDefaultUserValues];
    }
}

- (void)loadUserValues {
    
    if (self.mo != nil) {
        NSString *question_name = [self.mo valueForKey:@"name"];
        NSLog(@"QUESTION NAME ------------> %@", question_name);
    }
}

- (void)loadDefaultUserValues {
    
    if (self.mo != nil) {
        NSString *question_name = [self.mo valueForKey:@"name"];
        NSLog(@"QUESTION NAME ------------> %@", question_name);
    }
    
}

- (void)setupLocalization {
    
    NSString *edit_title_string = NSLocalizedString(@"Edit Questions", nil);
    NSString *create_title_string = NSLocalizedString(@"Create Questions", nil);
    self.title = (self.editMode) ?  edit_title_string : create_title_string;
    
    self.headerPropertyLabel.text = NSLocalizedString(@"Question Properties", nil);
    self.headerDetailsLabel.text = NSLocalizedString(@"Question Details", nil);
    
    NSString *saveCloseString = NSLocalizedString(@"Save and Close", nil);
    [self.buttonSaveClose setTitle:saveCloseString forState:UIControlStateNormal];
    [self.buttonSaveClose setTitle:saveCloseString forState:UIControlStateHighlighted];

    NSString *savePreviewString = NSLocalizedString(@"Preview", nil);
    [self.buttonSavePreview setTitle:savePreviewString forState:UIControlStateNormal];
    [self.buttonSavePreview setTitle:savePreviewString forState:UIControlStateHighlighted];
}

- (void)setupQuestionPropertyComponent {

    //DEFAULT
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        CGFloat height = (self.editMode) ? kZeroHeight : kNormalHeight;
        NSString *image_name = (height == kZeroHeight) ? @"icn_up_arrow" : @"icn_down_arrow";
        wo.headerPropertyImage.image = [UIImage imageNamed:image_name];
        wo.headerPropertyHeight.constant = height;
        [self.view setNeedsUpdateConstraints];
        [self.view layoutIfNeeded];
    });
    [self.headerPropertyButton addTarget:self
                                  action:@selector(headerButtonAction:)
                        forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupNavigationButtons {
    
    self.navigationItem.hidesBackButton = YES;
    SEL cancelAction = @selector(cancelButtonAction:);
    SEL deleteAction = @selector(deleteButtonAction:);
    
    if (self.leftButton == nil) {
        NSString *dashboard_title = NSLocalizedString(@"Dashboard", nil);
        NSString *leftButton_title = [NSString stringWithFormat:@"%@", dashboard_title];
        self.leftButton = [self generateButtonWithTitle:leftButton_title action:cancelAction];
        self.navigationItem.leftBarButtonItem = self.leftButton;
    }
    
    if (self.rightButton == nil) {
        NSString *image_string = @"icon_question_remove";
        NSString *cancel_string = NSLocalizedString(@"Cancel", nil);
        self.rightButton = (self.editMode) ? [self generateButtonWithImage:image_string action:deleteAction] : [self generateButtonWithTitle:cancel_string action:cancelAction];
        self.navigationItem.rightBarButtonItem = self.rightButton;
    }
}

- (UIBarButtonItem *)generateButtonWithTitle:(NSString *)title action:(SEL)action {
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:title
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:action];
    return button;
}

- (UIBarButtonItem *)generateButtonWithImage:(NSString *)imageName action:(SEL)action {
    
    UIImage *trashImage = [UIImage imageNamed:imageName];
    UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
    [b setImage:trashImage forState:UIControlStateNormal];
    [b setImage:trashImage forState:UIControlStateHighlighted];
    [b setFrame:CGRectMake(0, 0, 26, 26)];
    [b addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
//    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithImage:trashImage
//                                                               style:UIBarButtonItemStylePlain
//                                                              target:self
//                                                              action:action];
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:b];
    
    return button;
}

- (void)cancelButtonAction:(id)sender {
    
    NSString *alertTitle = NSLocalizedString(@"Save Changes", nil);
    NSString *alertMessage = NSLocalizedString(@"Do you want to save your changes?", nil);
    
    NSString *noButtonTitle = NSLocalizedString(@"No", nil);
    NSString *yesButtonTitle = NSLocalizedString(@"Yes", nil);
    
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:alertTitle
                                                                          message:alertMessage
                                                                   preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:yesButtonTitle
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction *action) {
                                                         NSLog(@"YES action");
                                                         [self saveAllComponents];
                                                     }];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:noButtonTitle
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         NSLog(@"NO action");
                                                         
                                                         if (self.editMode == YES) {
                                                             [self.navigationController popViewControllerAnimated:YES];
                                                         }
                                                         
                                                         if (self.editMode == NO) {
                                                             [self cleanScratchPad];
                                                         }
                                                     }];
    [alertControl addAction:yesAction];
    [alertControl addAction:noAction];
    
    [self presentViewController:alertControl animated:YES completion:nil];
}

- (void)cleanScratchPad {
    
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    BOOL flag = [self.tm clearContentsForEntity:kQuestionEntity predicate:nil];
    if (flag) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)deleteButtonAction:(id)sender {
    
    NSString *alertTitle = NSLocalizedString(@"Delete", nil);
    NSString *alertMessage = NSLocalizedString(@"Are you sure you want to delete", nil);
    NSString *yesButtonTitle = NSLocalizedString(@"Yes", nil);
    NSString *noButtonTitle = NSLocalizedString(@"No", nil);
    
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:alertTitle
                                                                          message:alertMessage
                                                                   preferredStyle:UIAlertControllerStyleAlert];
    __weak typeof(self) wo = self;
    UIAlertAction *yesAlertAction = [UIAlertAction actionWithTitle:yesButtonTitle
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   SEL actionDelete = @selector(initiateDeleteOperation);
                                                                   [wo.hud showWhileExecuting:actionDelete
                                                                                       onTarget:wo
                                                                                     withObject:nil
                                                                                       animated:YES];
                                                               });
                                                               
                                                           }];

    UIAlertAction *noAlertAction = [UIAlertAction actionWithTitle:noButtonTitle
                                                            style:UIAlertActionStyleDefault
                                                          handler:nil];
    [alertControl addAction:yesAlertAction];
    [alertControl addAction:noAlertAction];
    
    [self presentViewController:alertControl animated:YES completion:nil];
}

- (void)initiateDeleteOperation {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    
    __weak typeof(self) wo = self;
    [self.tm requestRemoveQuestion:self.mo doneBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo.navigationController popViewControllerAnimated:YES];
            });
        }
    }];
}

- (void)headerButtonAction:(id)sender {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    CGFloat height = self.headerPropertyHeight.constant;
    height = (height == kZeroHeight) ? kNormalHeight : kZeroHeight;
    NSString *image_name = (height == kZeroHeight) ? @"icn_up_arrow" : @"icn_down_arrow";
    UIImage *imageSymbol = [UIImage imageNamed:image_name];
    
    self.headerPropertyHeight.constant = height;
    
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.5f
                          delay:0.0f
         usingSpringWithDamping:1.0f
          initialSpringVelocity:0.5f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.headerPropertyImage.image = imageSymbol;
                        [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"embed_tg_collapsible_header_view"]) {
        self.headerView = (TGCollapsibleHeaderView *)[segue destinationViewController];
//        self.headerView.package_type_id = [NSString stringWithFormat:@"%@", self.package_type_id];
        self.headerView.mo = self.mo;
    }

    if ([segue.identifier isEqualToString:@"embed_tg_table_control_view"]) {
        self.listView = (TGDetailsViewQuestionItem *)[segue destinationViewController];
    }
    
    if ([segue.identifier isEqualToString:@"SAVE_AND_PREVIEW_QUESTIONS"]) {
        QuestionSamplerView *sampler = (QuestionSamplerView *)[segue destinationViewController];
        sampler.delegate = self;
        sampler.indexPath = nil;
        sampler.entityObject = kQuestionEntity;
    }
}

@end
