//
//  TestContentManager.m
//  V-Smart
//
//  Created by Julius Abarra on 18/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestContentManager.h"

@implementation TestContentManager

#pragma mark - Singleton Methods

+ (id)sharedInstance {
    static TestContentManager *sharedInstance = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init {
    
    if (self = [super init]) {
        self.testMainInfo = [NSMutableDictionary dictionary];
        self.testSettings = [NSMutableDictionary dictionary];
        self.testAssignedQuestions = [NSArray array];
        self.actionType = 0;
        self.testid = @"";
    }
    
    return self;
}

- (void)clearEntries {
    self.testMainInfo = nil;
    self.testSettings = nil;
    self.testAssignedQuestions = nil;
    self.actionType = 0;
    self.testid = nil;
}

- (void)recreateObjects {
    self.testMainInfo = [NSMutableDictionary dictionary];
    self.testSettings = [NSMutableDictionary dictionary];
    self.testAssignedQuestions = [NSArray array];
    self.actionType = 0;
    self.testid = @"";
}

@end
