//
//  TGQBCompetencyHistoryViewController.m
//  V-Smart
//
//  Created by Ryan Migallos on 18/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGQBCompetencyHistoryViewController.h"
#import "TGQBCompetencyHistoryCell.h"
#import "TestGuruDataManager.h"

@interface TGQBCompetencyHistoryViewController () <NSFetchedResultsControllerDelegate>

// HEADERS
@property (strong, nonatomic) IBOutlet UILabel *curriculumHeader;
@property (strong, nonatomic) IBOutlet UILabel *periodHeader;
@property (strong, nonatomic) IBOutlet UILabel *codeHeader;
@property (strong, nonatomic) IBOutlet UILabel *learningCompetencyHeader;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) NSArray *items;

@end

@implementation TGQBCompetencyHistoryViewController

static NSString *kCellIdentifier = @"tgqb_competency_history_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tm = [TestGuruDataManager sharedInstance];
    
    NSMutableArray *list = [NSMutableArray array];
    NSInteger count = 10;
    for (NSInteger i = 0; i < count; i++) {
        NSString *text = [NSString stringWithFormat:@"%@", @(i) ];
        [list addObject:text];
    }
    self.items = [NSArray arrayWithArray:list];
    [self.tableView reloadData];
    
    [self applyShadowToView:self.curriculumHeader];
    [self applyShadowToView:self.periodHeader];
    [self applyShadowToView:self.codeHeader];
    [self applyShadowToView:self.learningCompetencyHeader];
}

- (void)applyShadowToView:(UIView *)object {
    
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.5f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)object atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TGQBCompetencyHistoryCell *cell = (TGQBCompetencyHistoryCell *)object;
    
    /*
        curriculum_id : 16
        curriculum_title : SCIENCE 8 DepEd Curriculum Guide

        period_id: 59
        period_name: First Quarter

        code: S8FE-Ia-15
        competency: The Learners should be able to investigate the relationship between the amount of force applied and the mass of the object to the amount of change in the object’s motion
     */
    
    NSString *curriculum_title = [NSString stringWithFormat:@"%@", [mo valueForKey:@"curriculum_title"] ];
    NSString *period_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"period_name"] ];
    NSString *code_string = [NSString stringWithFormat:@"%@", [mo valueForKey:@"code"] ];
    NSString *competency = [NSString stringWithFormat:@"%@", [mo valueForKey:@"competency"] ];
    
    cell.curriculumField.text = curriculum_title;
    cell.periodField.text = period_name;
    cell.codeField.text = code_string;
    cell.learningCompetencyField.text = competency;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if ([(NSObject *)self.delegate respondsToSelector:@selector(didFinishSelectingHistoricalData:)]) {
        [self.delegate didFinishSelectingHistoricalData:mo];
    }
    
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    BOOL isAscending = NO;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCompetencyHistoryEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *order_number = [NSSortDescriptor sortDescriptorWithKey:@"date_created" ascending:isAscending];
    [fetchRequest setSortDescriptors:@[order_number]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    
    UITableView *tv = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tv insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tv deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tv cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tv deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tv insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
