//
//  TBTestDetailsSegmentView.m
//  TestBankVersion2.1
//
//  Created by Julius Abarra on 06/02/2016.
//  Copyright © 2016 Vibe Technologies, Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "TBTestDetailsSegmentView.h"
#import "TestDatePickerController.h"
#import "TestTimePickerController.h"
#import "TBClassHelper.h"
#import "TBContentManager.h"

@interface TBTestDetailsSegmentView () <UITextFieldDelegate, UITextViewDelegate, TestDatePickerDelegate, TestTimePickerController>

@property (strong, nonatomic) TBClassHelper *classHelper;
@property (strong, nonatomic) TBContentManager *tbcm;

@property (strong, nonatomic) TestDatePickerController *datePickerView;
@property (strong, nonatomic) TestTimePickerController *timePickerView;

@property (strong, nonatomic) UIPopoverController *datePickerPopOverController;
@property (strong, nonatomic) UIPopoverController *timePickerPopOverController;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *instructionsLabel;
@property (strong, nonatomic) IBOutlet UILabel *attemptsLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLimitLabel;
@property (strong, nonatomic) IBOutlet UILabel *passingScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *startDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *endDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *noExpiryLabel;

@property (strong, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) IBOutlet UITextView *instructionsTextView;
@property (strong, nonatomic) IBOutlet UITextField *attemtpsTextField;
@property (strong, nonatomic) IBOutlet UITextField *timeLimitTextField;
@property (strong, nonatomic) IBOutlet UITextField *passingScoreTextField;
@property (strong, nonatomic) IBOutlet UITextField *startDateTextField;
@property (strong, nonatomic) IBOutlet UITextField *endDateTextField;

@property (strong, nonatomic) IBOutlet UIImageView *withScoreImage;
@property (strong, nonatomic) IBOutlet UIImageView *hasExpiryImage;
@property (strong, nonatomic) IBOutlet UIImageView *yesGradedImage;

@property (strong, nonatomic) IBOutlet UIButton *timeLimitButton;
@property (strong, nonatomic) IBOutlet UIButton *startDateButton;
@property (strong, nonatomic) IBOutlet UIButton *endDateButton;
@property (strong, nonatomic) IBOutlet UIButton *yesGradedButton;

@property (strong, nonatomic) UIImage *notImage;
@property (strong, nonatomic) UIImage *yesImage;

@property (assign, nonatomic) int actionType;
@property (assign, nonatomic) int tempAttempts;
@property (assign, nonatomic) int tempPassingScore;
@property (assign, nonatomic) int totalQuestionPoints;

@property (strong, nonatomic) NSString *tempEndDate;

@property (assign, nonatomic) BOOL hasPassingScore;
@property (assign, nonatomic) BOOL hasExpiry;
@property (assign, nonatomic) BOOL yesGraded;

@property (assign, nonatomic) NSInteger timeFrameButtonTag;

@end

@implementation TBTestDetailsSegmentView

#define kBorderWidth 1.0f
#define kRightBorderColor UIColorFromHex(0x3498DB).CGColor
#define kWrongBorderColor UIColorFromHex(0xFF6666).CGColor
#define kEditableTBGColor UIColorFromHex(0xFFFFFF)
#define kDisabledTBGColor UIColorFromHex(0xAAAAAA)

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Content Manager
    self.tbcm = [TBContentManager sharedInstance];
    self.actionType = [self.tbcm.actionType intValue];
    
    // Customize UI Objects
    [self customizeLabels];
    [self customizeTextFields];
    [self customizeImages];
    [self customizeButtons];
    
    // Just Temporary
    self.totalQuestionPoints = 100;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Default Test Values
    [self setUpTestDetailsDefaultValues];
    
    // Contents Invalidation
    [self invalidateTestDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom UI Objects

- (void)customizeLabels {
    // Localize String
    self.titleLabel.text = [[self.classHelper localizeString:@"Title"] uppercaseString];
    self.instructionsLabel.text = [[self.classHelper localizeString:@"General Instructions"] uppercaseString];
    self.attemptsLabel.text = [[self.classHelper localizeString:@"Max. Attempts"] uppercaseString];
    self.timeLimitLabel.text = [[self.classHelper localizeString:@"Time Limit"] uppercaseString];
    self.passingScoreLabel.text = [[self.classHelper localizeString:@"Passing Score"] uppercaseString];
    self.startDateLabel.text = [[self.classHelper localizeString:@"Start Date"] uppercaseString];
    self.endDateLabel.text = [[self.classHelper localizeString:@"End Date"] uppercaseString];
    self.noExpiryLabel.text = [self.classHelper localizeString:@"No Expiration Date"];
}

- (void)customizeTextFields {
    // Place Holder
    NSString *title = [self.classHelper localizeString:@"Enter title of test here"];
    self.titleTextField.placeholder = [NSString stringWithFormat:@"%@...", title];
    
    // Text Field Tag
    self.titleTextField.tag = 100;
    self.instructionsTextView.tag = 200;
    self.attemtpsTextField.tag = 300;
    self.timeLimitTextField.tag = 400;
    self.passingScoreTextField.tag = 500;
    self.startDateTextField.tag = 600;
    self.endDateTextField.tag = 700;
    
    // Text Field Delegate
    self.titleTextField.delegate = self;
    self.instructionsTextView.delegate = self;
    self.attemtpsTextField.delegate = self;
    self.timeLimitTextField.delegate = self;
    self.passingScoreTextField.delegate = self;
    self.startDateTextField.delegate = self;
    self.endDateTextField.delegate = self;
    
    // Text Field Border Width
    self.titleTextField.layer.borderWidth = kBorderWidth;
    self.instructionsTextView.layer.borderWidth = kBorderWidth;
    self.attemtpsTextField.layer.borderWidth = kBorderWidth;
    self.timeLimitTextField.layer.borderWidth = kBorderWidth;
    self.passingScoreTextField.layer.borderWidth = kBorderWidth;
    self.startDateTextField.layer.borderWidth = kBorderWidth;
    self.endDateTextField.layer.borderWidth = kBorderWidth;
    
    // Action for Interactive Text Field
    [self.titleTextField addTarget:self
                            action:@selector(textFieldDidChange:)
                  forControlEvents:UIControlEventEditingChanged];
    
    [self.attemtpsTextField addTarget:self
                               action:@selector(textFieldDidChange:)
                     forControlEvents:UIControlEventEditingChanged];
    
    [self.passingScoreTextField addTarget:self
                                   action:@selector(textFieldDidChange:)
                         forControlEvents:UIControlEventEditingChanged];
}

- (void)customizeImages {
    // Reusable Image
    self.notImage = [UIImage imageNamed:@"check_icn48.png"];
    self.yesImage = [UIImage imageNamed:@"check_icn_active48.png"];
    
    // Tap Recognizer for Image View
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapDetected:)];
    singleTap1.numberOfTapsRequired = 1;
    
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapDetected:)];
    singleTap2.numberOfTapsRequired = 1;
    
    UITapGestureRecognizer *singleTap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapDetected:)];
    singleTap3.numberOfTapsRequired = 1;
    
    // Image View Tag
    self.withScoreImage.tag = 100;
    self.hasExpiryImage.tag = 200;
    self.yesGradedImage.tag = 300;
    
    // Enable User Interaction
    [self.withScoreImage setUserInteractionEnabled:YES];
    [self.hasExpiryImage setUserInteractionEnabled:YES];
    [self.yesGradedImage setUserInteractionEnabled:YES];
    
    // Allow Multiple Touch
    [self.withScoreImage setMultipleTouchEnabled:YES];
    [self.hasExpiryImage setMultipleTouchEnabled:YES];
    [self.yesGradedImage setMultipleTouchEnabled:YES];
    
    // Add Tap Recognizer
    [self.withScoreImage addGestureRecognizer:singleTap1];
    [self.hasExpiryImage addGestureRecognizer:singleTap2];
    [self.yesGradedImage addGestureRecognizer:singleTap3];
}

- (void)customizeButtons {
    // Button Title
    NSString *yesGraded = [[self.classHelper localizeString:@"Test is Graded"] uppercaseString];
    [self.yesGradedButton setTitle:yesGraded forState:UIControlStateNormal];
    [self.yesGradedButton setTitle:yesGraded forState:UIControlStateHighlighted];
    
    // Button Tag
    self.startDateButton.tag = 100;
    self.endDateButton.tag = 200;
    
    // Action for Button
    [self.timeLimitButton addTarget:self
                             action:@selector(showPopOverTimePicker:)
                   forControlEvents:UIControlEventTouchUpInside];
    
    [self.startDateButton addTarget:self
                             action:@selector(showPopOverDatePicker:)
                   forControlEvents:UIControlEventTouchUpInside];
    
    [self.endDateButton addTarget:self
                           action:@selector(showPopOverDatePicker:)
                 forControlEvents:UIControlEventTouchUpInside];
    
    [self.yesGradedButton addTarget:self
                             action:@selector(changeStageTypeState:)
                   forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Details Default Values

- (void)setUpTestDetailsDefaultValues {
    BOOL isTestDetailsNotEmpty = self.tbcm.testDetails.count > 0;
    
    if (isTestDetailsNotEmpty) {
        self.titleTextField.text = [self.tbcm.testDetails objectForKey:@"title"];
        self.instructionsTextView.text = [self.tbcm.testDetails objectForKey:@"general_instructions"];
        self.attemtpsTextField.text = [self.tbcm.testDetails objectForKey:@"max_attempts"];
        self.timeLimitTextField.text = [self.tbcm.testDetails objectForKey:@"time_limit"];
        self.passingScoreTextField.text = [self.tbcm.testDetails objectForKey:@"passing_score"];
        self.startDateTextField.text = [self.tbcm.testDetails objectForKey:@"start_date"];
        self.endDateTextField.text = [self.tbcm.testDetails objectForKey:@"end_date"];
        
        // Intermediate
        self.tempAttempts = [self.attemtpsTextField.text intValue];
        self.tempPassingScore = [self.passingScoreTextField.text intValue];
        self.tempEndDate = self.endDateTextField.text;

        // Passing Score
        self.hasPassingScore = [[self.tbcm.testDetails objectForKey:@"has_passing_score"] boolValue];
        [self changePassingScoreState];
        
        // Expiration
        self.hasExpiry = ![[self.tbcm.testDetails objectForKey:@"no_expiry"] boolValue];
        [self changeExpirationState];
        
        // Stage Type
        self.yesGraded = [[self.tbcm.testDetails objectForKey:@"stage_id"] boolValue];
        [self changeStageTypeState];
    }
}

#pragma mark - Tap Recognizer Implementation

- (void)imageTapDetected:(UITapGestureRecognizer *)tapGesture {
    [self.view endEditing:YES];
    NSInteger tag = tapGesture.view.tag;
    
    // Passing Score
    if (tag == 100) {
        self.hasPassingScore = !self.hasPassingScore;
        [self changePassingScoreState];
        [self.passingScoreTextField becomeFirstResponder];
    }
    
    // End Date
    if (tag == 200) {
        self.hasExpiry = !self.hasExpiry;
        [self changeExpirationState];
    }
    
    // Stage Type
    if (tag == 300) {
        self.yesGraded = !self.yesGraded;
        [self changeStageTypeState];
    }
    
    [self updateContentManager];
}

#pragma mark - Update Checkbox State

- (void)changePassingScoreState {
    NSString *scoreString = @"";
    if (self.tempPassingScore > 0) {
        scoreString = [NSString stringWithFormat:@"%d", self.tempPassingScore];
    }
    
    if (self.hasPassingScore) {
        self.hasPassingScore = YES;
        self.passingScoreTextField.text = scoreString;
        self.passingScoreTextField.enabled = YES;
        self.passingScoreTextField.backgroundColor = kEditableTBGColor;
        self.withScoreImage.image = self.yesImage;
    }
    else {
        self.hasPassingScore = NO;
        self.passingScoreTextField.text = scoreString;
        self.passingScoreTextField.enabled = NO;
        self.passingScoreTextField.layer.borderColor = kRightBorderColor;
        self.passingScoreTextField.backgroundColor = kDisabledTBGColor;
        self.withScoreImage.image = self.notImage;
    }
}

- (void)changeExpirationState {
    if (self.hasExpiry) {
        self.hasExpiry = YES;
        self.endDateTextField.text = self.tempEndDate;
        self.endDateTextField.enabled = YES;
        self.endDateTextField.backgroundColor = kEditableTBGColor;
        self.hasExpiryImage.image = self.notImage;
        self.endDateButton.enabled = YES;
    }
    else {
        self.hasExpiry = NO;
        self.endDateTextField.text = self.tempEndDate;
        self.endDateTextField.enabled = NO;
        self.endDateTextField.layer.borderColor = kRightBorderColor;
        self.endDateTextField.backgroundColor = kDisabledTBGColor;
        self.hasExpiryImage.image = self.yesImage;
        self.endDateButton.enabled = NO;
    }
}

- (void)changeStageTypeState {
    if (self.yesGraded) {
        self.yesGraded = YES;
        [self.yesGradedButton setBackgroundColor:UIColorFromHex(0x3498DB)];
        self.yesGradedImage.image = self.yesImage;
    }
    else {
        self.yesGraded = NO;
        [self.yesGradedButton setBackgroundColor:UIColorFromHex(0xAAAAAA)];
        self.yesGradedImage.image = self.notImage;
    }
}

- (void)changeStageTypeState:(id)sender {
    self.yesGraded = !self.yesGraded;
    
    if (self.yesGraded) {
        self.yesGraded = YES;
        [self.yesGradedButton setBackgroundColor:UIColorFromHex(0x3498DB)];
        self.yesGradedImage.image = self.yesImage;
    }
    else {
        self.yesGraded = NO;
        [self.yesGradedButton setBackgroundColor:UIColorFromHex(0xAAAAAA)];
        self.yesGradedImage.image = self.notImage;
    }
    
    [self updateContentManager];
}

#pragma mark - Date and Time Picker Popover

- (void)showPopOverDatePicker:(id)sender {
    [self.view endEditing:YES];
    
    UIButton *button = (UIButton *)sender;
    self.timeFrameButtonTag = button.tag;
    
    // Create Date Picker Controller
    self.datePickerView = [[TestDatePickerController alloc] initWithNibName:@"TestDatePickerView" bundle:nil];
    self.datePickerView.delegate = self;
    
    // Create Popover Controller
    self.datePickerPopOverController = [[UIPopoverController alloc] initWithContentViewController:self.datePickerView];
    self.datePickerPopOverController.popoverContentSize = CGSizeMake(320.0f, 235.0f);
    
    // Render Popover
    [self.datePickerPopOverController presentPopoverFromRect:button.bounds
                                                      inView:button
                                    permittedArrowDirections:UIPopoverArrowDirectionDown
                                                    animated:YES];
}

- (void)showPopOverTimePicker:(id)sender {
    [self.view endEditing:YES];
    
    UIButton *button = (UIButton *)sender;
    
    // Create Time Picker Controller
    self.timePickerView = [[TestTimePickerController alloc] initWithNibName:@"TestTimePickerView" bundle:nil];
    self.timePickerView.delegate = self;
    
    // Create Popover Controller
    self.timePickerPopOverController = [[UIPopoverController alloc] initWithContentViewController:self.timePickerView];
    self.timePickerPopOverController.popoverContentSize = CGSizeMake(320.0f, 235.0f);
    
    // Render Popover
    [self.timePickerPopOverController presentPopoverFromRect:button.bounds
                                                      inView:button
                                    permittedArrowDirections:UIPopoverArrowDirectionDown
                                                    animated:YES];
}

#pragma mark - Date and Time Picker Delegate

- (void)selectedDate:(NSString *)dateString {
    // Start Date
    if (self.timeFrameButtonTag == 100) {
        self.startDateTextField.text = dateString;
    }
    
    // End Date
    if (self.timeFrameButtonTag == 200) {
        self.endDateTextField.text = dateString;
    }
    
    [self correctTestSchedule];
    [self invalidateTestDetails];
    [self updateContentManager];
}

- (void)selectedTime:(NSString *)timeString {
    self.timeLimitTextField.text = timeString;
    [self correctTestSchedule];
    [self invalidateTestDetails];
    [self updateContentManager];
}

#pragma mark - Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self invalidateTestDetails];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSInteger textFieldTag = textField.tag;
    
    // Title
    if (textFieldTag == 100) {
        
        // if backspace
        if ([string length] == 0) {
            return YES;
        }
        
        // limit to only up to maximum allowable number of characters
        if ([textField.text length] >= [kTestMaxTitleChar intValue]) {
            return NO;
        }
    }
    
    // Attempts and Passing Score
    if (textFieldTag == 300 || textFieldTag == 500) {
        
        // if backspace
        if ([string length] == 0) {
            return YES;
        }
        
        // limit to only numeric characters
        NSCharacterSet *numberCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; ++i) {
            unichar c = [string characterAtIndex:i];
            
            if (![numberCharSet characterIsMember:c]) {
                return NO;
            }
        }
        
        // limit to only up to maximum allowable value
        NSString *newText;
        newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if (textFieldTag == 300) {
            if ([newText intValue] > [kTestMaxAttempts intValue]) {
                return NO;
            }
        }
        
        if (textFieldTag == 500) {
            if ([newText intValue] > self.totalQuestionPoints) {
                return NO;
            }
        }
    }
    
    return YES;
}

- (void)textFieldDidChange:(id)sender {
    UITextField *textField = (UITextField *)sender;
    NSInteger textFieldTag = textField.tag;
    int intValue = [textField.text intValue];
    
    // Title
    if (textFieldTag == 100) {
        if ([textField.text isEqualToString:@""]) {
            textField.layer.borderColor = kWrongBorderColor;
        }
        else {
            textField.layer.borderColor = kRightBorderColor;
        }
    }
    
    // Attempts
    if (textFieldTag == 300) {
        if ([textField.text isEqualToString:@""] || intValue < [kTestMinAttempts intValue]) {
            textField.layer.borderColor = kWrongBorderColor;
        }
        else {
            textField.layer.borderColor = kRightBorderColor;
        }
    }
    
    // Passing Score
    if (textFieldTag == 500) {
        if ([textField.text isEqualToString:@""] || intValue <= 0 || intValue > self.totalQuestionPoints) {
            textField.layer.borderColor = kWrongBorderColor;
        }
        else {
            textField.layer.borderColor = kRightBorderColor;
        }
    }
    
    [self updateContentManager];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSInteger textFieldTag = textField.tag;
    int intValue = [textField.text intValue];
    
    // Attempts
    if (textFieldTag == 300) {
        int attempts = [self.tbcm updateTestAttempt:intValue lastAcceptableTestAttempt:self.tempAttempts];
        textField.text = [NSString stringWithFormat:@"%d", attempts];
        textField.layer.borderColor = kRightBorderColor;
        self.tempAttempts = [textField.text intValue];
    }
    
    // Passing Score
    if (textFieldTag == 500 && self.hasPassingScore) {
        int passingScore = [self.tbcm updateTestScore:intValue
                              lastAcceptableTestScore:self.tempPassingScore
                                  totalQuestionPoints:self.totalQuestionPoints];
        
        textField.text = [NSString stringWithFormat:@"%d", passingScore];
        textField.layer.borderColor = kRightBorderColor;
        self.tempPassingScore = [textField.text intValue];
    }
    
    [self invalidateTestDetails];
    [self updateContentManager];
}

#pragma mark - Text View Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self invalidateTestDetails];
}

- (void)textViewDidChange:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.layer.borderColor = kWrongBorderColor;
    }
    else {
        textView.layer.borderColor = kRightBorderColor;
    }
    
    [self updateContentManager];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.layer.borderColor = kWrongBorderColor;
    }
    else {
        textView.layer.borderColor = kRightBorderColor;
    }
    
    [self invalidateTestDetails];
    [self updateContentManager];
}

#pragma mark - Test Schedule Correction

- (void)correctTestSchedule {
    // Get dates difference
    double dateRangeSeconds = [self computeTimeDifferenceInSecondsFromDate:self.startDateTextField.text
                                                                fromFormat:@"EEE, dd MMM yyyy, hh:mm a"
                                                                    toDate:self.endDateTextField.text
                                                                  toFormat:@"yyyy-MM-dd HH:mm:ss"];
    // Convert time limit to seconds
    double timeLimitSeconds = [self.classHelper convertTimeStringToSeconds:self.timeLimitTextField.text];
    
    // If end date is less than the start date
    if (dateRangeSeconds < 0) {
        NSDate *startDate = [self.classHelper convertStringToDate:self.startDateTextField.text
                                                           format:@"EEE, dd MMM yyyy, hh:mm a"];
        
        NSString *endDate = [self.classHelper addDayToDate:startDate
                                              numberOfDays:1
                                            formatToReturn:@"EEE, dd MMM yyyy, hh:mm a"];
        self.endDateTextField.text = endDate;
    }
    else {
        // If time limit is more than the difference of dates
        if (timeLimitSeconds > dateRangeSeconds) {
            NSDate *endDate = [self.classHelper convertStringToDate:self.endDateTextField.text format:@"EEE, dd MMM yyyy, hh:mm a"];
            endDate = [endDate dateByAddingTimeInterval:(timeLimitSeconds - dateRangeSeconds)];
            
            NSString *endDateString = [self.classHelper convertDateToString:endDate
                                                                     format:@"EEE, dd MMM yyyy, hh:mm a"];
            self.endDateTextField.text = endDateString;
        }
    }
    
    self.tempEndDate = self.endDateTextField.text;
}

- (double)computeTimeDifferenceInSecondsFromDate:(NSString *)fDate
                                      fromFormat:(NSString *)fromFormat
                                          toDate:(NSString *)tDate
                                        toFormat:(NSString*)toFormat {
    
    NSString *fDateString = [self.classHelper transformDateString:fDate
                                                       fromFormat:fromFormat
                                                         toFormat:toFormat];
    
    NSString *tDateString = [self.classHelper transformDateString:tDate
                                                       fromFormat:fromFormat
                                                         toFormat:toFormat];
    
    if (!([fDateString isEqualToString:@""] && [tDateString isEqualToString:@""])) {
        NSDate *fDateObject = [self.classHelper convertStringToDate:fDateString format:toFormat];
        NSDate *tDateObject = [self.classHelper convertStringToDate:tDateString format:toFormat];
        
        if (fDateObject != nil && tDateObject != nil) {
            NSTimeInterval secondsDifference = [tDateObject timeIntervalSinceDate:fDateObject];
            return secondsDifference;
        }
    }
    
    return 0;
}

#pragma mark - Test Details Invalidation

- (void)invalidateTestDetails {
    // Title
    if ([self.titleTextField.text isEqualToString:@""]) {
        self.titleTextField.layer.borderColor = kWrongBorderColor;
    }
    else {
        self.titleTextField.layer.borderColor = kRightBorderColor;
    }
    
    // Instruction
    if ([self.instructionsTextView.text isEqualToString:@""]) {
        self.instructionsTextView.layer.borderColor = kWrongBorderColor;
    }
    else {
        self.instructionsTextView.layer.borderColor = kRightBorderColor;
    }
    
    // Attempts
    int minAttempts = [kTestMinAttempts intValue];
    int maxAttempts = [kTestMaxAttempts intValue];
    int actAttempts = [self.attemtpsTextField.text intValue];
    
    if (actAttempts < minAttempts || actAttempts > maxAttempts) {
        self.attemtpsTextField.layer.borderColor = kWrongBorderColor;
    }
    else {
        self.attemtpsTextField.layer.borderColor = kRightBorderColor;
        self.tempAttempts = actAttempts;
    }
    
    // Time Limit
    double minTimeLimit = [self.classHelper convertTimeStringToSeconds:kTestMinTimeLimit];
    double maxTimeLimit = [self.classHelper convertTimeStringToSeconds:kTestMaxTimeLimit];
    double actTimeLimit = [self.classHelper convertTimeStringToSeconds:self.timeLimitTextField.text];
    
    if (actTimeLimit < minTimeLimit || actTimeLimit > maxTimeLimit) {
        self.timeLimitTextField.layer.borderColor = kWrongBorderColor;
    }
    else {
        self.timeLimitTextField.layer.borderColor = kRightBorderColor;
    }
    
    // Passing Score
    if (self.hasPassingScore) {
        int passingScore = [self.passingScoreTextField.text intValue];
        
        if (passingScore <= 0 || passingScore > self.totalQuestionPoints) {
            self.passingScoreTextField.layer.borderColor = kWrongBorderColor;
        }
        else {
            self.passingScoreTextField.layer.borderColor = kRightBorderColor;
            self.tempPassingScore = passingScore;
        }
    }
    else {
        self.passingScoreTextField.layer.borderColor = kRightBorderColor;
    }
    
    // Start Date
    if ([self.startDateTextField.text isEqualToString:@""]) {
        self.startDateTextField.layer.borderColor = kWrongBorderColor;
    }
    else {
        self.startDateTextField.layer.borderColor = kRightBorderColor;
    }
    
    // End Date
    if (self.hasExpiry) {
        double timeLimit = [self.classHelper convertTimeStringToSeconds:self.timeLimitTextField.text];
        double dateRange = [self computeTimeDifferenceInSecondsFromDate:self.startDateTextField.text
                                                             fromFormat:@"EEE, dd MMM yyyy, hh:mm a"
                                                                 toDate:self.endDateTextField.text
                                                               toFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        if (timeLimit > dateRange || dateRange < 0) {
            self.endDateTextField.layer.borderColor = kWrongBorderColor;
        }
        else {
            self.endDateTextField.layer.borderColor = kRightBorderColor;
            self.tempEndDate = self.endDateTextField.text;
        }
    }
    else {
        self.endDateTextField.layer.borderColor = kRightBorderColor;
    }
}

#pragma mark - Update Content Manager

- (void)updateContentManager {
    NSString *title = self.titleTextField.text;
    NSString *general_instructions = self.instructionsTextView.text;
    NSString *max_attempts = self.attemtpsTextField.text;
    NSString *time_limit = self.timeLimitTextField.text;
    NSString *start_date = self.startDateTextField.text;
    NSString *end_date = self.endDateTextField.text;
    NSString *has_passing_score = (self.hasPassingScore) ? @"1" : @"0";
    NSString *passing_score = self.passingScoreTextField.text;
    NSString *no_expiry = (self.hasExpiry) ? @"0" : @"1";
    NSString *stage_id = (self.yesGraded) ? @"1" : @"0";
    
    [self.tbcm.testDetails setValue:title forKey:@"title"];
    [self.tbcm.testDetails setValue:general_instructions forKey:@"general_instructions"];
    [self.tbcm.testDetails setValue:max_attempts forKey:@"max_attempts"];
    [self.tbcm.testDetails setValue:time_limit forKey:@"time_limit"];
    [self.tbcm.testDetails setValue:start_date forKey:@"start_date"];
    [self.tbcm.testDetails setValue:end_date forKey:@"end_date"];
    [self.tbcm.testDetails setValue:has_passing_score forKey:@"has_passing_score"];
    [self.tbcm.testDetails setValue:passing_score forKey:@"passing_score"];
    [self.tbcm.testDetails setValue:no_expiry forKey:@"no_expiry"];
    [self.tbcm.testDetails setValue:stage_id forKey:@"stage_id"];
}

@end
