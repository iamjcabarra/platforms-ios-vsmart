//
//  TestBankSortHeader.m
//  V-Smart
//
//  Created by Julius Abarra on 11/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestBankSortHeader.h"
#import "TestBankSortHeaderCell.h"
#import "TestGuruDataManager.h"

//// BUTTON CATEGORY ////

@interface UIButton (CUSTOM)
- (void)setColor:(UIColor *)color forState:(UIControlState)state;
@end

@implementation UIButton (CUSTOM)

- (void)setColor:(UIColor *)color forState:(UIControlState)state {
    UIView *colorView = [[UIView alloc] initWithFrame:self.frame];
    colorView.backgroundColor = color;
    UIGraphicsBeginImageContext(colorView.bounds.size);
    [colorView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:state];
}

@end

//// BUTTON CATEGORY ////

@interface TestBankSortHeader () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) TestGuruDataManager *tgdm;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UILabel *sortByLabel;
@property (strong, nonatomic) IBOutlet UILabel *viewByLabel;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) IBOutlet UIButton *viewByButton;
@property (strong, nonatomic) IBOutlet UIButton *arrowButton;
@property (strong, nonatomic) IBOutlet UIButton *applyButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *resetButton;

@property (strong, nonatomic) UIImage *ascImage;
@property (strong, nonatomic) UIImage *desImage;

@property (strong, nonatomic) NSArray *items;
@property (strong, nonatomic) NSManagedObject *selectedSortTypeObject;

@end

@implementation TestBankSortHeader

static NSString * const kCellIdentifier = @"sortHeaderCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Test Guru Data Manager
    self.tgdm = [TestGuruDataManager sharedInstance];
    self.managedObjectContext = self.tgdm.mainContext;
    
    // Collection View
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    // Allow Sections in Collection View
    self.collectionView.allowsSelection = YES;
    self.collectionView.allowsMultipleSelection = NO;
    
    // Sort Settins
    [self loadTestSortSettings];
    
    // Custom Button
    [self setupButtonControls];
    
    // Sort Option Label
    self.viewByLabel.text = [NSLocalizedString(@"View By", nil) uppercaseString];
    self.sortByLabel.text = [NSLocalizedString(@"Sort By", nil) uppercaseString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Sort Settings

- (void)loadTestSortSettings {
    NSPredicate *predicate = [self predicateForKeyPath:@"user_id" value:[self.tgdm loginUser]];
    self.items = [self.tgdm getObjectsForEntity:kTestSortOptionEntity
                                      predicate:predicate
                                        context:self.managedObjectContext];
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

#pragma mark - Button Controls

- (void)setupButtonControls {
    // Button Title
    [self title:@"Apply" forButton:self.applyButton];
    [self title:@"Cancel" forButton:self.cancelButton];
    [self title:@"Reset" forButton:self.resetButton];
    
    // Button Tag
    self.applyButton.tag = 100;
    self.cancelButton.tag = 200;
    self.resetButton.tag = 300;
    
    // Button Action
    [self.applyButton addTarget:self
                         action:@selector(buttonAction:)
               forControlEvents:UIControlEventTouchUpInside];
    
    [self.cancelButton addTarget:self
                          action:@selector(buttonAction:)
                forControlEvents:UIControlEventTouchUpInside];
    
    [self.resetButton addTarget:self
                         action:@selector(buttonAction:)
               forControlEvents:UIControlEventTouchUpInside];
    
    // View By Button
    NSString *newestTitle = NSLocalizedString(@"Newest to Oldest", nil);
    NSString *oldestTitle = NSLocalizedString(@"Oldest to Newest", nil);
    [self.viewByButton setTitle:newestTitle forState:UIControlStateNormal];
    [self.viewByButton setTitle:oldestTitle forState:UIControlStateSelected];
    
    UIColor *inActiveText = UIColorFromHex(0x007AFF);
    UIColor *activeText = [UIColor whiteColor];
    [self.viewByButton setTitleColor:inActiveText forState:UIControlStateNormal];
    [self.viewByButton setTitleColor:activeText forState:UIControlStateSelected];
    
    UIColor *active = UIColorFromHex(0x007AFF);
    UIColor *inActive = UIColorFromHex(0xDFEAF2);
    [self.viewByButton setColor:inActive forState:UIControlStateNormal];
    [self.viewByButton setColor:active forState:UIControlStateSelected];
    
    CALayer *layer = self.viewByButton.layer;
    layer.borderWidth = 1.0f;
    layer.borderColor = active.CGColor;
    [self.viewByButton setTintColor:[UIColor clearColor]];
    
    // View By Button Action
    [self.viewByButton addTarget:self
                          action:@selector(viewByButtonAction:)
                forControlEvents:UIControlEventTouchUpInside];
    
    // View By Arrow
    self.ascImage = [UIImage imageNamed:@"icn_up_arrow"];
    self.desImage = [UIImage imageNamed:@"icn_down_arrow"];
    [self.arrowButton setImage:self.ascImage forState:UIControlStateNormal];
    [self.arrowButton setImage:self.desImage forState:UIControlStateSelected];
    self.arrowButton.userInteractionEnabled = NO;
}

#pragma mark - Button Action

- (void)viewByButtonAction:(UIButton *)button {
    BOOL state = button.selected;
    button.selected = (state) ? NO : YES;
    self.arrowButton.selected = (self.arrowButton.selected) ? NO : YES;
}

- (void)buttonAction:(UIButton *)sender {
    UIButton *button = (UIButton *)sender;
    NSInteger tag = button.tag;
    NSString *actionType = @"";
    
    if (tag == 100) {
        actionType = @"APPLY";
    }
    
    if (tag == 200) {
        actionType = @"CANCEL";
    }
    
    if (tag == 300) {
        actionType = @"RESET";
    }
    
    if ([(NSObject *)self.delegate respondsToSelector:@selector(sortDidSelectButtonAction:sortType:isOldest:)]) {
        [self.delegate sortDidSelectButtonAction:actionType sortType:self.selectedSortTypeObject isOldest:self.viewByButton.selected];
    }
}

- (void)title:(NSString *)string forButton:(UIButton *)button {
    NSString *title = [NSLocalizedString(string, nil) uppercaseString];
    [button setTitle:title forState:UIControlStateNormal];
}

#pragma mark - Collection View Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TestBankSortHeaderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    NSManagedObject *mo = self.items[indexPath.row];
    
    NSString *title = [NSString stringWithFormat:@"%@", [mo valueForKey:@"sort_type_name"]];
    cell.sortByLabel.text = title;
    
    NSString *selectedString = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_selected"]];
    BOOL selected = [selectedString boolValue];
    [cell showState:selected];
    
    return cell;
}

#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionView *)cv didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    TestBankSortHeaderCell *cell = (TestBankSortHeaderCell *)[cv cellForItemAtIndexPath:indexPath];
    [cell showState:cell.selected];
    self.selectedSortTypeObject = self.items[indexPath.row];
}

- (void)collectionView:(UICollectionView *)cv didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    TestBankSortHeaderCell *cell = (TestBankSortHeaderCell *)[cv cellForItemAtIndexPath:indexPath];
    [cell showState:cell.selected];
}

#pragma mark - Collection View Delegate FlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

@end