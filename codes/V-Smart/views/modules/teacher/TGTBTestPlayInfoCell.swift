//
//  TGTBTestPlayInfoCell.swift
//  V-Smart
//
//  Created by Ryan Migallos on 03/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class TGTBTestPlayInfoCell: UITableViewCell {

    @IBOutlet var itemViewImage: UIImageView!
    @IBOutlet var itemTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        itemViewImage.customShadow(5.0)
//        itemTextLabel.customShadow(5.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
