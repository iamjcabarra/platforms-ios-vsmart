//
//  TestBankQuestionItemCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestBankQuestionItemCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *questionName;
@property (strong, nonatomic) IBOutlet UILabel *questionType;
@property (weak, nonatomic) IBOutlet UILabel *noChoiceLabel;

@end
