//
//  TGTBCreateTestView.m
//  V-Smart
//
//  Created by Julius Abarra on 07/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGTBCreateTestView.h"
#import "TGTBCreateTestSegmentView.h"
#import "TBClassHelper.h"
#import "TestGuruDataManager.h"
#import "TGTBTestActionConstants.h"
#import "HUD.h"
#import "TestGuruDataManager+Course.h"
#import "V_Smart-Swift.h" //SWIFT 2.1.1 IMPLEMENTATION
#import "UIImageView+WebCache.h"

@interface TGTBCreateTestView () <TGTBTestPlayLandingDelegate, TGTBQuestionPreviewDelegate>

@property (strong, nonatomic) TBClassHelper *classHelper;
@property (strong, nonatomic) TestGuruDataManager *tgdm;

@property (strong, nonatomic) TGTBCreateTestSegmentView *segmentView;

@property (strong, nonatomic) IBOutlet UIImageView *packageTypeImage;
@property (strong, nonatomic) IBOutlet UILabel *packageTypeLabel;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) IBOutlet UIButton *saveAndCloseButton;
@property (strong, nonatomic) IBOutlet UIButton *previewButton;

@property (strong, nonatomic) NSString *test_id;

@property (strong, nonatomic) NSNotificationCenter *notif;

@property (strong, nonatomic) TestGuruDataManager *tm;

@end

@implementation TGTBCreateTestView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tm = [TestGuruDataManager sharedInstance];
    
    self.notif = [NSNotificationCenter defaultCenter];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    if (self.testObject != nil) {
        self.test_id = [NSString stringWithFormat:@"%@", [self.testObject valueForKey:@"id"] ];
    }
    
    // Test Guru Data Manager
    self.tgdm = [TestGuruDataManager sharedInstance];
    
    // Segmented Control
    [self customizeSegmentedControl];
    
    // Default Values
    self.segmentedControl.selectedSegmentIndex = 0;
    [self previewButtonVisibilityForSegmentedIndex:self.segmentedControl.selectedSegmentIndex];
    
    [self.segmentedControl addTarget:self
                              action:@selector(segmentedControlAction:)
                    forControlEvents:UIControlEventValueChanged];
    
    [self.previewButton addTarget:self
                           action:@selector(showPreviewQuestions:)
                 forControlEvents:UIControlEventTouchUpInside];
    
    [self.saveAndCloseButton addTarget:self
                                action:@selector(saveTestOperation:)
                      forControlEvents:UIControlEventTouchUpInside];
    
    [self validateSaveAndClose];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setUpCustomNavigationView];
    [self setupNotification];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self setUpCreateTestHeaderView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)showPreviewQuestions:(UIButton *)button {
    [self performSegueWithIdentifier:@"showTestPreview" sender:self];
}

- (void)validateSaveAndClose {
    BOOL testInfoComplete = NO;
    BOOL testComplete = NO;
    
    NSManagedObject *t_mo = self.testObject;
    
    NSString *name = [t_mo valueForKey:@"name"];
    NSString *general_instruction = [t_mo valueForKey:@"general_instruction"];
    NSString *attempts = [t_mo valueForKey:@"attempts"];
    NSString *passing_score = [t_mo valueForKey:@"passing_score"];
    NSString *time_limit = [t_mo valueForKey:@"time_limit"];
    NSString *date_open = [t_mo valueForKey:@"date_open"];
    NSString *date_close = [t_mo valueForKey:@"date_close"];
    
    NSLog(@"iTO [%@]", general_instruction);
    if ((name.length > 0) && (general_instruction.length > 0) && (attempts.length > 0) && (passing_score.length > 0) && (time_limit.length > 0) && (date_open.length > 0) && (date_close.length > 0)) {
        testInfoComplete = YES;
    }
    
    NSInteger count = [self.tm fetchCountForEntity:kQuestionEntity];
    if ((count > 0) && (testInfoComplete == YES)) {
        testComplete = YES;
    }
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        wo.saveAndCloseButton.backgroundColor = (testComplete) ? UIColorFromHex(0x68bf61) : [UIColor lightGrayColor];
        wo.saveAndCloseButton.userInteractionEnabled = (testComplete);
        wo.previewButton.userInteractionEnabled = (testComplete);
        wo.previewButton.backgroundColor = (testComplete) ? UIColorFromHex(0x4274b9) : [UIColor lightGrayColor];
    });
}

#pragma mark - NSNotification Center Capabilities

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self teardownNotification];
}

- (void)setupNotification {
    SEL updateNotifAction = @selector(testCreateVerification:);
    [self.notif addObserver:self selector:updateNotifAction name:kNotificationTestCreateVerification object:nil];
}

- (void)teardownNotification {
    [self.notif removeObserver:self name:kNotificationTestCreateVerification object:nil];
}

- (void)testCreateVerification:(NSNotification *)notification {
    [self validateSaveAndClose];
}

- (void)saveTestOperation:(UIButton *)button {
    BOOL edit = (self.crudActionType == TGTBCrudActionTypeEdit) ? YES : NO;
    NSString *indicatorString = (edit) ? [self.classHelper localizeString:@"Updating"] : [self.classHelper localizeString:@"Saving"];
    indicatorString = [NSString stringWithFormat:@"%@...", indicatorString];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    NSDictionary *user_data = @{@"id":self.test_id};
    __weak typeof(self) wo = self;
    
    [self.tgdm postTestWithUserData:user_data edit:edit doneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD hideUIBlockingIndicator];
            [wo showAlertAction:wo.crudActionType isOperationSuccessful:status];
        });
    }];
}

- (void)showAlertAction:(NSInteger)crudActionType isOperationSuccessful:(BOOL)success {
    NSString *avTitleA = [self.classHelper localizeString:@"Create Test"];
    NSString *avTitleB = [self.classHelper localizeString:@"Edit Test"];
    NSString *avTitleC = (self.crudActionType == TGTBCrudActionTypeCreate) ? avTitleA : avTitleB;
    
    NSString *butOkay = NSLocalizedString(@"Okay", nil);
    NSString *message = @"";
    
    if (success) {
        if (crudActionType == TGTBCrudActionTypeCreate) {
            message = NSLocalizedString(@"Test successfully created.", nil);
        }
        if (crudActionType == TGTBCrudActionTypeEdit) {
            message = NSLocalizedString(@"Test successfully updated.", nil);
        }
    }
    else {
        if (crudActionType == TGTBCrudActionTypeCreate) {
            message = NSLocalizedString(@"Test Bank is failed to create test.", nil);
        }
        if (crudActionType == TGTBCrudActionTypeEdit) {
            message = NSLocalizedString(@"Test Bank is failed to update test.", nil);
        }
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:avTitleC
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *theAction = [UIAlertAction actionWithTitle:butOkay
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          __weak typeof (self) wo = self;
                                                          dispatch_async (dispatch_get_main_queue(), ^{
                                                              if (wo.isFromTestDetailsView) {
                                                                  [wo.delegate willReloadUpdatedTestDetailsView:YES];
                                                              }
                                                              
                                                              [wo.navigationController popViewControllerAnimated:YES];
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                          });
                                                      }];
    [alert addAction:theAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Custom Navigation View

- (void)setUpCustomNavigationView {
    UIColor *color = UIColorFromHex(0xF69C42);
    
    if (self.crudActionType == TGTBCrudActionTypeEdit) {
        color = UIColorFromHex(0x17A555);
    }
    
    self.navigationController.navigationBar.barTintColor = color;
    self.navigationController.navigationBar.translucent = NO;
    self.segmentedControl.tintColor = color;
    
    NSString *title = @"";
    
    if (self.crudActionType == TGTBCrudActionTypeCreate) {
        title = [self.classHelper localizeString:@"Create Test"];
    }
    
    if (self.crudActionType == TGTBCrudActionTypeEdit) {
        title = [self.classHelper localizeString:@"Edit Test"];
    }
    
    self.title = title;
    
    // Return Bar Button
    [self setUpCustomReturnBarButton];
    
    // Cancel Bar Button
    [self setUpCustomCancelBarButton];
}

- (void)setUpCustomReturnBarButton {
    NSString *title = [NSString stringWithFormat:@"%@", self.customBackBarButtonTitle];
    
    UIButton *b = [UIButton buttonWithType:UIButtonTypeSystem];
    [b asBackButtonWithTitle:title color:[UIColor whiteColor]];
    [b addTarget:self action:@selector(returnBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] init];
    leftBarItem.customView = b;
    self.navigationItem.leftBarButtonItem = leftBarItem;
}

- (void)setUpCustomCancelBarButton {
    NSString *title = [self.classHelper localizeString:@"Cancel"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 200, 42);
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [button addTarget:self action:@selector(returnBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = customBarItem;
}

- (void)returnBarButtonAction:(id)sender {
    NSString *alertMessage = NSLocalizedString(@"Are you sure you want to leave this page, your changes will not be applied?", nil);
    
    NSString *noButtonTitle = NSLocalizedString(@"No", nil);
    NSString *yesButtonTitle = NSLocalizedString(@"Yes", nil);
    
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:@""
                                                                          message:alertMessage
                                                                   preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:noButtonTitle
                                                       style:UIAlertActionStyleCancel
                                                     handler:nil];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:yesButtonTitle
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [self.navigationController popViewControllerAnimated:YES];
                                                      }];
    
    [alertControl addAction:noAction];
    [alertControl addAction:yesAction];
    
    [self presentViewController:alertControl animated:YES completion:nil];
}

#pragma mark - Create Test Header View

- (void)setUpCreateTestHeaderView {
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *packageTypeName = [NSString stringWithFormat:@"%@", [self.tgdm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_NAME]];
        NSString *packageTypeImage = [self.tgdm stringValue:[self.tgdm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_URL]];
        wo.packageTypeLabel.text = [packageTypeName uppercaseString];
        [wo.packageTypeImage sd_setImageWithURL:[NSURL URLWithString:packageTypeImage]];
    });
}

#pragma mark - Custom Segmented Control

- (void)customizeSegmentedControl {
    NSString *segmentATitle = [self.classHelper localizeString:@"Main"];
    [self.segmentedControl setTitle:segmentATitle forSegmentAtIndex:0];
    
    NSString *segmentBTitle = [self.classHelper localizeString:@"Settings"];
    [self.segmentedControl setTitle:segmentBTitle forSegmentAtIndex:1];
    
    NSString *segmentCTitle = [self.classHelper localizeString:@"Assigned Questions"];
    [self.segmentedControl setTitle:segmentCTitle forSegmentAtIndex:2];
}

#pragma mark - Create Test Segment Views

- (void)segmentedControlAction:(id)sender {
    __weak typeof(self) wo = self;
    
    UISegmentedControl *sc = (UISegmentedControl *)sender;
    NSInteger index = sc.selectedSegmentIndex;
    
    // Test Main Info View
    if (index == 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.segmentView swapEmbeddedViews:@"embedTestMainInfo"];
            [wo previewButtonVisibilityForSegmentedIndex:index];
        });
    }
    // Test Settings View
    if (index == 1) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.segmentView swapEmbeddedViews:@"embedTestSettings"];
            [wo previewButtonVisibilityForSegmentedIndex:index];
        });
    }
    // Test Question View
    if (index== 2) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.segmentView swapEmbeddedViews:@"embedTestQuestion"];
            [wo previewButtonVisibilityForSegmentedIndex:index];
        });
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedCreateTestSegmentView"]) {
        self.segmentView = (TGTBCreateTestSegmentView *)[segue destinationViewController];
        if (self.testObject != nil) {
            self.segmentView.testObject = self.testObject;
            self.segmentView.crudActionType = self.crudActionType;
        }
    }
    
    if ([segue.identifier isEqualToString:@"showTestPreview"]) {
        TGTBTestPlayLandingView *plv = (TGTBTestPlayLandingView *)[segue destinationViewController];
        plv.testObject = self.testObject;
        plv.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"TGTB_QUESTION_SAMPLER_VIEW_2"]) {
        TGTBQuestionPreview *qtv = (TGTBQuestionPreview *)[segue destinationViewController];
        qtv.title = self.title;
        qtv.isEditingMode = YES;
        qtv.delegate = self;
    }
}

- (void)previewButtonVisibilityForSegmentedIndex:(NSInteger)index {
    self.previewButton.hidden = YES;
    
    if (index == 2) {
        self.previewButton.hidden = NO;
    }
}

- (void)didViewQuestions {
    [self performSegueWithIdentifier:@"TGTB_QUESTION_SAMPLER_VIEW_2" sender:self];
}

- (void)didPressSave {
    [self saveTestOperation:self.saveAndCloseButton];
}

@end
