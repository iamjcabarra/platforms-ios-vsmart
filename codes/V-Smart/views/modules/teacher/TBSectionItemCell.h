//
//  TBSectionItemCell.h
//  V-Smart
//
//  Created by Julius Abarra on 12/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBSectionItemCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *sectionStateImage;
@property (strong, nonatomic) IBOutlet UILabel *sectionNameLabel;

- (void)showSelection:(BOOL)selection;

@end
