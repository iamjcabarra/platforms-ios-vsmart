//
//  TGTBCreateTestView.h
//  V-Smart
//
//  Created by Julius Abarra on 07/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestGuruConstants.h"

@protocol TGTBCreateTestViewDelegate <NSObject>
@required
- (void)willReloadUpdatedTestDetailsView:(BOOL)willReload;
@end

@interface TGTBCreateTestView : UIViewController

@property (weak, nonatomic) id <TGTBCreateTestViewDelegate> delegate;

@property (strong, nonatomic) NSString *customBackBarButtonTitle;
@property (strong, nonatomic) NSManagedObject *testObject;
@property (assign, nonatomic) TGTBCrudActionType crudActionType;
@property (assign, nonatomic) BOOL isFromTestDetailsView;

@end
