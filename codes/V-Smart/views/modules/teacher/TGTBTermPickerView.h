//
//  TGTBTermPickerView.h
//  V-Smart
//
//  Created by Julius Abarra on 16/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

@protocol TGTBTermPickerViewDelegate <NSObject>

@required
- (void)selectedTerm:(NSDictionary *)termObject;

@end

#import <UIKit/UIKit.h>

@interface TGTBTermPickerView : UITableViewController

@property (weak, nonatomic) id <TGTBTermPickerViewDelegate> delegate;

@end
