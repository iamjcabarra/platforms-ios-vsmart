//
//  TestGuruFeedbackTextCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 13/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruFeedbackTextCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UITextView *textView;

@end
