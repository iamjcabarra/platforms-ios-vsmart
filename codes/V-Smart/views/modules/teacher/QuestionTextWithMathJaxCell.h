//
//  QuestionTextWithMathJaxCell.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 01/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuestionTextWithMathJaxCellDelegate <NSObject>
@optional
- (void)didFinishloading:(NSIndexPath *)indexPath;

@optional
- (void)didInputTextInBlank:(NSString *)inputText;
@end

@interface QuestionTextWithMathJaxCell : UITableViewCell <UIWebViewDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeight;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;

@property (weak, nonatomic) id <QuestionTextWithMathJaxCellDelegate> delegate;
@property (weak, nonatomic) NSIndexPath *indexPath;

@property (assign, nonatomic) BOOL isForViewTestResults;

- (void)loadWebViewWithContents:(NSString *)string;
- (void)reconfigureWebViewForTestResults:(BOOL)reconfigure;

@end
