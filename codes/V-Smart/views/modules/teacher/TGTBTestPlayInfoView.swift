//
//  TGTBTestPlayInfoView.swift
//  V-Smart
//
//  Created by Ryan Migallos on 02/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol TGTBTestPlayInfoDelegate: class {
    func didClickViewQuestions(_ flag:Bool)
}

class TGTBTestPlayInfoView: UITableViewController {

    // MARK: - Delegate Property
    weak var delegate:TGTBTestPlayInfoDelegate?

    var mo: NSManagedObject?
    
    // MARK: - Storyboard Outlets
    
    // HEADER
    @IBOutlet var packageTypeView: UIImageView!
    @IBOutlet var packageTypeLabel: UILabel!
    @IBOutlet var gradeLevelLabel: UILabel!
    
    // COURSE
    @IBOutlet var courseTitleLabel: UILabel!
    
    // INSTRUCTION
    @IBOutlet var instructionTitleLabel: UILabel!
    @IBOutlet var instructionDescriptionLabel: UILabel!
    
    // CELL TYPES
    @IBOutlet var examCount: TGTBTestPlayInfoCell!
    @IBOutlet var examTimeLimit: TGTBTestPlayInfoCell!
    @IBOutlet var examQuestion: TGTBTestPlayInfoCell!
    @IBOutlet var examScore: TGTBTestPlayInfoCell!
    
    // BUTTON
    @IBOutlet var viewQuestionButton: UIButton!
    
    // MARK: - Shared Data Manager
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tm = TestGuruDataManager.sharedInstance()
        return tm!
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadUserData()
        setupPackageIdentifier()
        customizedSubViews()
        
        let viewAction = #selector( self.viewButtonAction(_:) )
        viewQuestionButton.addTarget(self, action: viewAction, for:.touchUpInside)
    }
    
    func loadUserData() {
        
        if mo == nil {
            let objects = dataManager.fetchObjects(forEntity: kTestInformationEntity) as! [NSManagedObject]
            mo = objects.first
        }
        
//        let sample_instruction = "This exam contains 50 multiple choice questions, each worth 2 points. Circle the correct response for each question. Make sure that your answer is clearly marked. You will not receive partial credit for any work done. The standard normal table appears on Page 13 of this exam. This is a closed-book, closed notes examination. You may use a calculator if you wish. However, cell phones are not permitted for use in any way. Any discussion or otherwise inappropriate communication between examinees, as well as the appearance of any unnecessary material or cell-phone usage, will be dealt with severely. Violations may result in an “F” for this exam, “F” for the class, suspension, or expulsion. This exam is worth a total of 100 points. Print your name at the top of this page in the upper right hand corner. Good Luck!!"
        
        
        //COURSE TITLE
        courseTitleLabel.text = dataManager.fetchObject(forKey: kTGQB_SELECTED_COURSE_NAME) as? String
        
        //GENERAL INSTRUCTIONS
        let sample_instruction = (mo!.value(forKey: "general_instruction")! as AnyObject).description
        instructionDescriptionLabel.text = sample_instruction
        
//        let set : NSSet = mo!.valueForKey("question_items") as! NSSet
        let item_count = "\(self.dataManager.fetchCount(forEntity: kQuestionEntity))"
        
        /*
         This test contains ___ question
         NOTE: question(s) <= dynamic
         */
        
        let highlightColor = UIColor.black
        
        //QUESTION COUNT
//        let item_count = mo!.valueForKey("item_count")!.description
        let question_string = ( Int(item_count) > 1 ) ? "questions" : "question"
        let item_count_string = "This Test Contains \(item_count) \(question_string)."
        examQuestion.itemTextLabel.text = item_count_string
        examQuestion.itemTextLabel.highlightString(item_count, size: 21, color: highlightColor)
        
        /*
         You have ___ minutes to finish this test
         NOTE: minutes | minute <= dynamic
         */
        
        //TIME LIMIT
        let time_limit = (mo!.value(forKey: "time_limit")! as AnyObject).description
        let time_limit_string = "You have \(time_limit!) to finish this test."
        examTimeLimit.itemTextLabel.text = time_limit_string
        examTimeLimit.itemTextLabel.highlightString(time_limit, size: 21, color: highlightColor)
        
        /*
         The passing score of this test is ____
         NOTE: There is NO passing score for this test
         */
        
        //EXAM SCORE
        let total_score = (mo!.value(forKey: "passing_score")! as AnyObject).description // FIX IN PASSING SCORE
        let passing_score = ( Int(total_score!)! > 0 ) ? total_score : "NO"
        let with_passing_score_string = "The Passing score of this test is \(passing_score!)."
        let without_passing_score_string = "There is \(passing_score!) passing score for this test."
        examScore.itemTextLabel.text = ( Int(total_score!)! > 0 ) ? with_passing_score_string : without_passing_score_string
        examScore.itemTextLabel.highlightString(passing_score, size: 21, color: highlightColor )
        
        /*
         You can take this test ___ more time
         NOTE: There is NO passing score for this test
         */
        
        //EXAM COUNT
        let number_attempts = (mo!.value(forKey: "attempts")! as AnyObject).description
        let time_message_string = ( Int(number_attempts!)! > 1 ) ? "times" : "time"
        let attempt_string = "\(NSLocalizedString("You can take this test", comment: "")) \(number_attempts!) \(NSLocalizedString("more", comment: "")) \(NSLocalizedString(time_message_string, comment: ""))."
        examCount.itemTextLabel.text = attempt_string
        examCount.itemTextLabel.highlightString(number_attempts, size: 21, color: highlightColor )
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func customizedSubViews() {
        packageTypeView.customShadow(5.0)
        packageTypeLabel.customShadow(5.0)
        courseTitleLabel.customShadow(5.0)
        courseTitleLabel.customShadow(5.0)
        instructionTitleLabel.customShadow(5.0)
        instructionDescriptionLabel.customShadow(5.0)
        
        gradeLevelLabel.isHidden = true;
    }

    fileprivate func setupPackageIdentifier() {
        
//        let package_type_name = {
//            dataManager.fetchObjectForKey(kTGQB_SELECTED_PACKAGE_NAME) as! String
//        }()
//        
//        let package_type_image_data = {
//            dataManager.fetchObjectForKey(kTGQB_SELECTED_PACKAGE_IMAGE_DATA) as! NSData
//        }()
//        
//        packageTypeLabel.text = package_type_name
//        packageTypeView.image = UIImage(data: package_type_image_data)
        
        let package_type_name = {
            dataManager.fetchObject(forKey: kTGQB_SELECTED_PACKAGE_NAME) as! String
        }()
        
        let package_type_image_url = {
            dataManager.fetchObject(forKey: kTGQB_SELECTED_PACKAGE_IMAGE_URL) as! NSString
        }()
        
        packageTypeLabel.text = package_type_name
        packageTypeView.sd_setImage(with: URL(string: package_type_image_url as String))

        //TODO
        let grade_level_name = "Rose - Grade 7"
        gradeLevelLabel.text = grade_level_name
    }
    
    func viewButtonAction(_ sender:UIButton!) {
        print("view button action press")
        delegate?.didClickViewQuestions(true)
    }
    
}
