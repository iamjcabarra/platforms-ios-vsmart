//
//  TestGuruDynamicReusableView.h
//  V-Smart
//
//  Created by Ryan Migallos on 23/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruDynamicReusableView : UICollectionReusableView

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
