//
//  TGQBQuestionItemList.m
//  V-Smart
//
//  Created by Ryan Migallos on 10/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGQBQuestionItemList.h"
#import "TGQBQuestionItemListCell.h"
#import "TestGuruDataManager.h"

@interface TGQBQuestionItemList () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) TestGuruDataManager *tm;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) NSString *search_string;
@property (assign, nonatomic) BOOL search_active;

@end

@implementation TGQBQuestionItemList

static NSString *kCellID = @"tgqb_question_item_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.search_active = NO;
    self.search_string = @"";
    
    self.tm = [TestGuruDataManager sharedInstance];
    
//    [self setupNotification];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    [self teardownNotification];
}

- (void)setupNotification {
    SEL notifAction = @selector(questionUpdateNotification:);
    
    NSNotificationCenter *notif = [NSNotificationCenter defaultCenter];
    
    [notif addObserver:self
                                             selector:notifAction
                                                 name:kNotificationQuestionUpdate
                                               object:nil];
    
//    SEL notifEditAction = @selector(notifEditAction:);
    [notif addObserver:self
              selector:@selector(notifEditAction:)
                  name:kNotificationQuestionEditFromPreview
                object:nil];
}

- (void)teardownNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)questionUpdateNotification:(NSNotification *)notification {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);

    
    [self reloadFetchedResultsController];
    
//    NSError *error = nil;
//    [self.fetchedResultsController performFetch:&error];
//    [self.tableView reloadData];
}

- (void)notifEditAction:(NSNotification *)notification {
    NSIndexPath *indexPath = (NSIndexPath *)[notification object];
    [self performDelegateOperationWithIndexPath:indexPath];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)generateItemString:(NSInteger)row {
    
    NSString *item_string = NSLocalizedString(@"Question Item", nil);
    NSInteger index = row + 1;
    NSString *item_row = [NSString stringWithFormat:@"%ld", (long)index ];
    
    return [NSString stringWithFormat:@"%@ # %@", item_string, item_row];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TGQBQuestionItemListCell *cell = (TGQBQuestionItemListCell *)[tableView dequeueReusableCellWithIdentifier:kCellID
                                                                                                 forIndexPath:indexPath];
    // Configure the cell...
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *complete = [NSString stringWithFormat:@"%@", [mo valueForKey:@"complete"] ];
    
    NSString *image_string = ([complete isEqualToString:@"1"]) ? @"check_icn_active150" : @"check_icn150.png";
    cell.checkImage.image = [UIImage imageNamed:image_string];
    cell.questionTitle.text = [self generateItemString:indexPath.row];
    
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performDelegateOperationWithIndexPath:indexPath];
}

- (void)performDelegateOperationWithIndexPath:(NSIndexPath *)indexPath {
    
    [self reloadFetchedResultsController];
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *item_string = [self generateItemString:indexPath.row];
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if ([(NSObject *)self.delegate respondsToSelector:@selector(selectedMenuItem:withObject:withIndexPath:)]) {
        [self.delegate selectedMenuItem:item_string withObject:mo withIndexPath:indexPath];
    }
}

#pragma mark - Fetched results controller

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortType = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:NO];
    [fetchRequest setSortDescriptors:@[sortType]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

@end
