//
//  UILabelCustomizeExtension.swift
//  V-Smart
//
//  Created by Ryan Migallos on 22/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

extension UILabel {
    
    func highlightString(_ pattern:String!, size:CGFloat, color:UIColor!) {
        
        // NSMutableAttributedString
        let mutableAttributedString = NSMutableAttributedString(attributedString: self.attributedText!)
        
        // Find Range of Pattern
        let range = rangeFromString(mutableAttributedString.string, pattern: pattern)
        
        // Pattern Found
        if range.location != NSNotFound {
            
            let currentAttributesDict = mutableAttributedString.attributes(at: range.location, effectiveRange: nil) as [String : AnyObject]!
            let currentFont = currentAttributesDict?[NSFontAttributeName];
            let fontDescriptor = currentFont!.fontDescriptor
            let changedFontDescriptor = fontDescriptor?.withSymbolicTraits(.traitBold)
            let updatedFont = UIFont(descriptor: changedFontDescriptor!, size: size)
            let updatedAttributes = [NSFontAttributeName : updatedFont, NSForegroundColorAttributeName : color] as [String : Any]
            
            mutableAttributedString.beginEditing()
            mutableAttributedString.setAttributes(updatedAttributes, range: range)
            mutableAttributedString.endEditing()
            
            self.attributedText = mutableAttributedString
        }
    }
    
    fileprivate func rangeFromString(_ string:String,pattern:String) -> NSRange {
        let new_pattern: String = (pattern.characters.count == 0) ? " " : pattern
        let new_string: String = (string.characters.count == 0) ? " " : string
        let regex = try! NSRegularExpression(pattern: new_pattern, options: [.caseInsensitive])
        let range = NSMakeRange(0, new_string.characters.count)
        let matchRange = regex.rangeOfFirstMatch(in: string, options: .reportProgress, range: range)
        return matchRange
    }
    
    func localize() {
        let labelText = self.text
        self.text = NSLocalizedString(labelText!, comment: "")
    }
    
    func setHTMLFromString(text: String) {
        let modifiedFont: String! = "<span style=\"font-family: \(self.font!.fontName); font-size: \(self.font!.pointSize)\">\(text)</span>"
        
//        let attrStr = try! NSAttributedString(
//            data: modifiedFont.data(using: String.Encoding.utf8, allowLossyConversion: true)!,
//            options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8],
//            documentAttributes: nil)
        
        let data = modifiedFont.data(using: String.Encoding.utf8, allowLossyConversion: true)
        
            let attrStr = try! NSAttributedString(data: data!,
                                             options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue],
                                             documentAttributes: nil)
        
        self.attributedText = attrStr
    }
}
