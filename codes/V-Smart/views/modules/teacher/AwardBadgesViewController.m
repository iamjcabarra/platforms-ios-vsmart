//
//  AwardBadgesViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 9/9/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "AwardBadgesViewController.h"
#import "GMGridView.h"

@interface AwardBadgesViewController ()<GMGridViewDataSource, GMGridViewSortingDelegate, GMGridViewActionDelegate>
{
    __gm_weak GMGridView *_gmGridView;
    
    NSMutableArray *_data;
    __gm_weak NSMutableArray *_currentData;
}
@end

@implementation AwardBadgesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    _gmGridView.mainSuperView = self.navigationController.view;
    
	// Do any additional setup after loading the view.
    [self _setupNotifications];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
    
    [self setupData];
    [self setupGrid];
}

-(void) setupData {
    
    //NSDictionary *item = @{@"module_name": @"Textbooks", @"image": @"icn_textbooks"};
    
    NSArray *modules = [self _buildBadges];
    
    _data = [[NSMutableArray alloc] initWithArray:modules];
    _currentData = _data;
}

-(void) setupGrid {
    VLog(@"setupGrid");
    
    self.view.backgroundColor = UIColorFromHex(0xeeeeee);
    
    float spacing = 1.2;
    float insets = 2.966102;
    
    //GMGridView *gmGridView = [[GMGridView alloc] initWithFrame:CGRectMake(0, 310.0, self.dashboardView.bounds.size.width, self.dashboardView.bounds.size.height)];
    
    float profileY = [super profileHeight];
    float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    //float gridHeight = 638 + [super profileSize].size.height;
    
    float gridY = [super headerSize].size.height + profileY;
    
    GMGridView *gmGridView = [[GMGridView alloc] initWithFrame:CGRectMake(0, gridY, self.view.bounds.size.width, gridHeight + 44)];
    
    //VLog(@"Size: %@", NSStringFromCGSize(self.dashboardView.bounds.size));
    gmGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    gmGridView.backgroundColor = [UIColor clearColor];
    gmGridView.scrollEnabled = YES;
    [self.view addSubview:gmGridView];
    [self.view sendSubviewToBack:gmGridView];
    _gmGridView = gmGridView;
    
    _gmGridView.style = GMGridViewStyleSwap;
    _gmGridView.itemSpacing = spacing;
    _gmGridView.minEdgeInsets = UIEdgeInsetsMake(insets, insets, insets, insets);
    _gmGridView.centerGrid = NO;
    _gmGridView.actionDelegate = self;
    _gmGridView.sortingDelegate = self;
    _gmGridView.dataSource = self;
}

- (NSArray *) _buildBadges {
    NSArray *currentModule = nil;
    
    NSArray *modules = @[@{@"module_name": @"Writer Whiz", @"image": @"ic_WriterWhiz"},
                         @{@"module_name": @"English Whiz", @"image": @"icn_EnglishWhiz"},
                         @{@"module_name": @"Environmentalist", @"image": @"icn_environmentalist"},
                         @{@"module_name": @"Filipino Whiz", @"image": @"icn_FilipinoWhiz"},
                         @{@"module_name": @"Math Whiz", @"image": @"icn_MathWhiz"},
                         @{@"module_name": @"Musician", @"image": @"icn_musician"},
                         @{@"module_name": @"MVP", @"image": @"icn_MVP"},
                         @{@"module_name": @"Science Whiz", @"image": @"icn_scienceWhiz"},
                         @{@"module_name": @"Techie Geek", @"image": @"icn_TechieGeek"},
                         @{@"module_name": @"The Artist", @"image": @"icn_theArtist"},
                         @{@"module_name": @"The Dancer", @"image": @"icn_theDancer"},
                         @{@"module_name": @"The Songbird", @"image": @"icn_theSongbird"}];
    
    currentModule = [NSArray arrayWithArray:modules];
    return currentModule;
}

-(void) _setupNotifications {
    VS_NCADD(kNotificationProfileHeight, @selector(_refreshController:))
}

- (NSString *) dashboardName {
    
    NSString *moduleName = NSLocalizedString(@"Award Badges", nil);
    return moduleName;
    
//    return kModuleAwardBadges;
}

#pragma mark - Post Notification Events
-(void) _refreshController: (NSNotification *) notification {
    VLog(@"Received: kNotificationProfileHeight");
    
//    [UIView animateWithDuration:0.45 animations:^{
//        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
//            [super adjustProfileHeight];
//        }
//        [super showOrHideMiniAvatar];
//    } completion:^(BOOL finished) {
//        
//    }];
    
    [UIView animateWithDuration:0.45 animations:^{
        float profileY = [super profileHeight];
        float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
        
        float gridY = [super headerSize].size.height + profileY;
        [_gmGridView setFrame:CGRectMake(0, gridY, self.view.bounds.size.width, gridHeight + 44)];
        [_gmGridView setNeedsLayout];
        
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            CGRect frame = self.baseProfileView.frame;
            frame.origin.y = - (218.0f - 92.0f);
            
            if (profileY == 0.0) {
                [self.baseProfileView setFrame:frame];
            } else {
                [self.baseProfileView setFrame:CGRectMake(0, 92.0f, self.view.frame.size.width, 218.0)];
            }
        }
        
    } completion:^(BOOL finished) {
        [super showOrHideMiniAvatar];
    }];
}

//////////////////////////////////////////////////////////////
#pragma mark GMGridViewDataSource
//////////////////////////////////////////////////////////////

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return [_currentData count];
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    //return CGSizeMake(253, 220);
    return CGSizeMake(253, 214);
}

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    //NSLog(@"Creating view indx %d", index);
    
    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    //VLog(@"Grid Size: %@", NSStringFromCGSize(size));
    GMGridViewCell *cell = [gridView dequeueReusableCell];
    NSDictionary *module = [_currentData objectAtIndex:index];
    
    UIImage *moduleImage = [UIImage imageNamed:[module valueForKey:@"image"]];
    NSString *moduleName = [module valueForKey:@"module_name"];
    
    if (!cell) {
        cell = [[GMGridViewCell alloc] init];
        //cell.deleteButtonIcon = [UIImage imageNamed:@"close_x.png"];
        //cell.deleteButtonOffset = CGPointMake(-15, -15);
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        view.backgroundColor = UIColorFromHex(0xffffff);
        view.layer.masksToBounds = NO;
        view.layer.cornerRadius = 2;
        
        cell.contentView = view;
    }
    
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 253, 214)];
    imageView.contentMode = UIViewContentModeCenter;
    [imageView setImage:moduleImage];
    
    [cell.contentView addSubview:imageView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:cell.contentView.bounds];
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    label.text = moduleName;
    label.font = [UIFont fontWithName:@"HelveticaNeue-Regular" size:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = UIColorFromHex(0x575757);
    [cell.contentView addSubview:label];
    
    CGRect labelPosition = label.frame;
    labelPosition.origin.y = 80;
    label.frame = labelPosition;
    
    return cell;
}


- (BOOL)GMGridView:(GMGridView *)gridView canDeleteItemAtIndex:(NSInteger)index
{
    return NO; //index % 2 == 0;
}

//////////////////////////////////////////////////////////////
#pragma mark GMGridViewActionDelegate
//////////////////////////////////////////////////////////////

-(void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    GMGridViewCell *cell = [gridView cellForItemAtIndex:position];
    UIGraphicsBeginImageContext(cell.bounds.size);
    [cell.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // 2.
    // Add the UIImageView inside the UIButton
    UIImageView *zoomView = [[UIImageView alloc] initWithFrame:cell.bounds];
    zoomView.image = img;
    [cell addSubview:zoomView];
    [cell bringSubviewToFront:zoomView];
    
//    NSDictionary *module = [_currentData objectAtIndex:position];
    
    [UIView animateWithDuration:0.35
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:(void (^)(void)) ^{
                         zoomView.alpha = 0.0;
                         zoomView.transform = CGAffineTransformMakeScale(2, 2);
                     }
                     completion:^(BOOL finished){
                         zoomView.transform = CGAffineTransformIdentity;
                         [zoomView removeFromSuperview];
                         
//                         NSString *moduleTitle = [module objectForKey:@"module_name"];
                         //[self pushToModule:moduleTitle];
                     }];
}

-(void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    NSLog(@"Tap on empty space");
}

-(void)GMGridView:(GMGridView *)gridView processDeleteActionForItemAtIndex:(NSInteger)index
{
    
}

//////////////////////////////////////////////////////////////
#pragma mark GMGridViewSortingDelegate
//////////////////////////////////////////////////////////////

-(void)GMGridView:(GMGridView *)gridView didStartMovingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = UIColorFromHex(0x2eaee2);
                         cell.contentView.layer.shadowOpacity = 0.7;
                     }
                     completion:nil
     ];
}

-(void)GMGridView:(GMGridView *)gridView didEndMovingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = [UIColor whiteColor];
                         cell.contentView.layer.shadowOpacity = 0;
                     }
                     completion:nil
     ];
}

- (BOOL)GMGridView:(GMGridView *)gridView shouldAllowShakingBehaviorWhenMovingCell:(GMGridViewCell *)cell atIndex:(NSInteger)index
{
    return YES;
}

-(void)GMGridView:(GMGridView *)gridView moveItemAtIndex:(NSInteger)oldIndex toIndex:(NSInteger)newIndex
{
    NSObject *object = [_currentData objectAtIndex:oldIndex];
    [_currentData removeObject:object];
    [_currentData insertObject:object atIndex:newIndex];
}

-(void)GMGridView:(GMGridView *)gridView exchangeItemAtIndex:(NSInteger)index1 withItemAtIndex:(NSInteger)index2
{
    [_currentData exchangeObjectAtIndex:index1 withObjectAtIndex:index2];
}

-(void)viewDidUnload
{
    [super viewDidUnload];
    _gmGridView = nil;
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
