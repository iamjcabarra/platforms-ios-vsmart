//
//  TestTimePickerController.h
//  V-Smart
//
//  Created by Julius Abarra on 15/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

@protocol TestTimePickerController <NSObject>

@required
- (void)selectedTime:(NSString *)timeString;

@end

#import <UIKit/UIKit.h>

@interface TestTimePickerController : UIViewController

@property (weak, nonatomic) id <TestTimePickerController> delegate;

@end
