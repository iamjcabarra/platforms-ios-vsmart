//
//  QuestionBankCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 7/28/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "QuestionBankCell.h"
//#import <QuartzCore/QuartzCore.h>

@interface QuestionBankCell()

@property (strong, nonatomic) IBOutlet UIView *containerView;

@end

@implementation QuestionBankCell

- (void)awakeFromNib {
    // Initialization code
    
//    CALayer *layer = self.containerView.layer;
//    layer.cornerRadius = 5.0f;
//    layer.borderColor = [UIColor lightGrayColor].CGColor;
//    layer.shadowColor = [UIColor blackColor].CGColor;
//    layer.shadowOffset = CGSizeMake(0,2);
//    layer.shadowRadius = 2;
//    layer.shadowOpacity = 0.7f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
