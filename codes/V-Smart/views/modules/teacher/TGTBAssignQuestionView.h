//
//  TGTBAssignQuestionView.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 09/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGTBAssignQuestionDelegate <NSObject>
@optional
- (void)didFinishAssigning:(BOOL)status;
@end



@interface TGTBAssignQuestionView : UIViewController

@property (strong, nonatomic) NSMutableSet *setOfAssignedObjects;
@property (weak, nonatomic) id <TGTBAssignQuestionDelegate> delegate;

@end
