//
//  TestGuruTestItemCell.m
//  V-Smart
//
//  Created by Julius Abarra on 08/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestGuruTestItemCell.h"
#import <QuartzCore/QuartzCore.h>

@interface TestGuruTestItemCell ()

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIView *difficultyView;
@property (strong, nonatomic) IBOutlet UIView *publicView;

@end

@implementation TestGuruTestItemCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setTestGradedAndTestType:(NSString *)is_graded_id {
    NSString *stage = @"";
    
    // NON-GRADED
    if ([is_graded_id isEqualToString:@"0"]) {
        stage = NSLocalizedString(@"Non-Graded", nil);    // uncheked
    }
    
    // GRADED
    if ([is_graded_id isEqualToString:@"1"]) {
        stage = NSLocalizedString(@"Graded", nil);        // uncheked
    }
    
//    self.testGradedAndTestTypeLabel.text = [stage uppercaseString];
}

- (void)setDifficultyLevel:(NSString *)difficulty_level_id {
    UIColor *easyColor = UIColorFromHex(0x68BF61);
    UIColor *moderateColor = UIColorFromHex(0xFFCD02);
    UIColor *difficultColor = UIColorFromHex(0xF8931F);
    
    UIColor *color = [UIColor whiteColor];
    
    // EASY
    if ([difficulty_level_id isEqualToString:@"1"]) {
        color = easyColor;
    }
    
    // MODERATE
    if ([difficulty_level_id isEqualToString:@"2"]) {
        color = moderateColor;
    }
    
    // DIFFICULT
    if ([difficulty_level_id isEqualToString:@"3"]) {
        color = difficultColor;
    }
    
    self.difficultyView.backgroundColor = color;
    self.containerView.backgroundColor = color;
}

@end
