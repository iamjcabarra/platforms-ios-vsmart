//
//  TestGuruCourseListView.m
//  V-Smart
//
//  Created by Carmelito Bayarcal on 09/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestGuruCourseListView.h"
#import "TestGuruCourseListItemCell.h"
#import "TestGuruDataManager.h"
#import "TestGuruSpringboard.h"
#import "TestGuruConstants.h"
#import "CPCourseCellItem.h"
#import "UIImageView+WebCache.h"

@interface TestGuruCourseListView () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate>

@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *packageImage;
@property (weak, nonatomic) IBOutlet UILabel *packageName;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (assign, nonatomic) BOOL isVersion25;
@property (assign, nonatomic) BOOL searchBarActive;

@end

@implementation TestGuruCourseListView
static NSString *kCourseCellIdentifier = @"courseCellIdentifier";
static NSString *kQuestionCourseIdentifier = @"tg_course_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tm = [TestGuruDataManager sharedInstance];
    
    self.searchBar.delegate = self;
    self.searchBar.placeholder = NSLocalizedString(@"Search", nil);
    self.title = NSLocalizedString(@"Course List", nil);
    
    // Resusable Cell
    UINib *courseCellItemNib = [UINib nibWithNibName:@"CPCourseCellItem" bundle:nil];
    [self.tableView registerNib:courseCellItemNib forCellReuseIdentifier:kCourseCellIdentifier];
    
    
    CGFloat version = [[Utils getServerInstanceVersion] floatValue];
    
    self.isVersion25 = (version >= VSMART_SERVER_MAX_VER);
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *package_type_name = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_NAME]];
        NSString *package_type_image_url = [self.tm stringValue:[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_URL]];
        [wo.packageImage sd_setImageWithURL:[NSURL URLWithString:package_type_image_url]];
        wo.packageName.text = [package_type_name uppercaseString];
    });
    
    [self customizeNavigationController];
    self.tableView.userInteractionEnabled = YES;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    //    NSString *package_type_name = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_NAME]];
    ////    NSData *package_type_image_data = [NSData dataWithData:[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_DATA] ];
    //    NSString *package_type_image_url = [self.tm stringValue:[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_URL]];
    //
    //    __weak typeof(self) wo = self;
    //    dispatch_async(dispatch_get_main_queue(), ^{
    ////        wo.packageImage.image = [UIImage imageWithData:package_type_image_data];
    //        [wo.packageImage sd_setImageWithURL:[[NSURL alloc] initWithString:package_type_image_url]];
    //        wo.packageName.text = [package_type_name uppercaseString];
    //    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self reloadFetchedResultsController];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self.searchBarActive = NO;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    self.searchBarActive = YES;
}

- (void)customizeNavigationController {
    
    UIColor *color = UIColorFromHex(0xF69C42);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    } else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.tableView = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCourseCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    CPCourseCellItem *courseCell = (CPCourseCellItem *)cell;
    
    NSString *course_name = [mo valueForKey:@"course_name"];
    NSString *section_name = [mo valueForKey:@"section_name"];
    NSString *grade_level = [mo valueForKey:@"grade_level"];
    NSString *schedule = [mo valueForKey:@"schedule"];
    
    
    courseCell.courseSectionLabel.hidden = (self.isVersion25 == YES) ? NO : YES;
    
    courseCell.courseNameLabel.text = course_name;
    courseCell.courseSectionLabel.text = [NSString stringWithFormat:@"%@ - %@", section_name, grade_level];
    courseCell.courseScheduleLabel.text = schedule;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *course_id = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"course_id"] ];
    NSString *course_name = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"course_name"] ];
    
    [self.tm saveObject:course_id forKey:kTGQB_SELECTED_COURSE_ID];
    [self.tm saveObject:course_name forKey:kTGQB_SELECTED_COURSE_NAME];
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wo performSegueWithIdentifier:@"SPRINGBOARD_TEST_GURU_MODULE" sender:self];
    });
}

- (void)justifyLabel:(UILabel *)label string:(NSString *)string {
    NSMutableParagraphStyle *paragraphStyles = [[NSMutableParagraphStyle alloc] init];
    paragraphStyles.alignment = NSTextAlignmentJustified;
    paragraphStyles.firstLineHeadIndent = 1.0f;
    NSDictionary *attributes = @{NSParagraphStyleAttributeName:paragraphStyles};
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:string attributes:attributes];
    label.attributedText = attributedString;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [[self.tableView indexPathsForSelectedRows] lastObject];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.tableView.userInteractionEnabled = YES;
}

#pragma mark - Fetched results controller

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (NSFetchedResultsController *)fetchedResultsController
{
    BOOL isAscending = NO;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCourseListEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    NSPredicate *predicate;
    
    if (self.searchBarActive) {
        if (![self.searchBar.text isEqual:@""]) {
            NSLog(@"SEARCHBAR.TEXT [%@]", self.searchBar.text);
            predicate = [self predicateForKeyPathContains:@"search_string" value:self.searchBar.text];
            [fetchRequest setPredicate:predicate];
        }
    }
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *course_id = [NSSortDescriptor sortDescriptorWithKey:@"course_id" ascending:isAscending];
    [fetchRequest setSortDescriptors:@[course_id]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
