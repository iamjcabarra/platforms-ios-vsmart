//
//  TGDetailsViewQuestionItem.m
//  V-Smart
//
//  Created by Ryan Migallos on 05/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGDetailsViewQuestionItem.h"
#import "TestGuruDataManager.h"
#import "TGQuestionShortAnswer.h"
#import "TGQuestionEssay.h"
#import "TGQuestionTrueFalse.h"
#import "TGQuestionMultipleChoice.h"
#import "MainHeader.h"

@interface TGDetailsViewQuestionItem () <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) TestGuruDataManager *tm;

@end

@implementation TGDetailsViewQuestionItem

static NSString *kEssayCellID = @"tg_essay_cell_identifier";
static NSString *kShortAnswerCellID = @"tg_short_answer_cell_identifier";
static NSString *kTrueFalseCellID = @"tg_true_false_cell_identifier";
static NSString *kMultipleChoiceCellID = @"tg_multiple_choice_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tm = [TestGuruDataManager sharedInstance];
    
    // ESSAY CELL
    UINib *essayNib = [UINib nibWithNibName:@"TGQuestionEssay" bundle:nil];
    [self.tableView registerNib:essayNib forCellReuseIdentifier:kEssayCellID];

    UINib *shortNib = [UINib nibWithNibName:@"TGQuestionShortAnswer" bundle:nil];
    [self.tableView registerNib:shortNib forCellReuseIdentifier:kShortAnswerCellID];

    UINib *trueFalseNib = [UINib nibWithNibName:@"TGQuestionTrueFalse" bundle:nil];
    [self.tableView registerNib:trueFalseNib forCellReuseIdentifier:kTrueFalseCellID];

    UINib *multipleChoiceNib = [UINib nibWithNibName:@"TGQuestionMultipleChoice" bundle:nil];
    [self.tableView registerNib:multipleChoiceNib forCellReuseIdentifier:kMultipleChoiceCellID];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger count = [[self.fetchedResultsController sections] count];
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *cell_identifier = [self cellIdentifierForObject:mo];
    
    return [self heightForIdentifier:cell_identifier];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.tableView = tableView;
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *cell_identifier = [self cellIdentifierForObject:mo];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIColor *color = [UIColor whiteColor];
    if( [indexPath row] % 2) {
        color = UIColorFromHexWithAlpha(0xADDFFF, 0.9);
        
    }
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *cell_identifier = [self cellIdentifierForObject:mo];
    
    if ([cell_identifier isEqualToString:kTrueFalseCellID]) {
        //True or False
        TGQuestionTrueFalse *trueFalse = (TGQuestionTrueFalse *)cell;
        trueFalse.table.backgroundColor = color;
        trueFalse.customBackground.backgroundColor = color;
    }
    
    if ([cell_identifier isEqualToString:kShortAnswerCellID]) {
        //Fill in the Blanks (Short Answer)
        TGQuestionShortAnswer *shortAnswer = (TGQuestionShortAnswer *)cell;
        shortAnswer.customBackground.backgroundColor = color;
    }
    
    if ([cell_identifier isEqualToString:kMultipleChoiceCellID]) {
        //Multiple Choice
        TGQuestionMultipleChoice *multipleChoice = (TGQuestionMultipleChoice *)cell;
        multipleChoice.table.backgroundColor = color;
        multipleChoice.customBackground.backgroundColor = color;
    }
    
    if ([cell_identifier isEqualToString:kEssayCellID]) {
        //Essay
        TGQuestionEssay *essay = (TGQuestionEssay *)cell;
        essay.customBackground.backgroundColor = color;
    }
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *cell_identifier = [self cellIdentifierForObject:mo];
    
    NSString *name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"name"] ];
    NSString *question_text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"question_text"] ];
    NSString *points = [NSString stringWithFormat:@"%@", [mo valueForKey:@"points"] ];
    
    BOOL hidden = self.editMode;
    NSInteger rowCount = [self.tableView numberOfRowsInSection:indexPath.section];
    if (rowCount == 1) {
        hidden = YES;
    }
    
    if ([cell_identifier isEqualToString:kTrueFalseCellID]) {
        //True or False
        TGQuestionTrueFalse *trueFalse = (TGQuestionTrueFalse *)cell;
        trueFalse.titleField.text = name;
        trueFalse.descriptionLabel.text = question_text;
        trueFalse.pointsField.text = points;
        trueFalse.eraseButton.hidden = hidden;
        [trueFalse setObjectData:mo];
        [trueFalse.eraseButton addTarget:self action:@selector(eraseQuestionObjectAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if ([cell_identifier isEqualToString:kShortAnswerCellID]) {
        //Fill in the Blanks (Short Answer)
        TGQuestionShortAnswer *shortAnswer = (TGQuestionShortAnswer *)cell;
        shortAnswer.titleField.text = name;
        shortAnswer.descriptionLabel.text = question_text;
        shortAnswer.pointsField.text = points;
        shortAnswer.eraseButton.hidden = hidden;
        [shortAnswer setObjectData:mo];
        [shortAnswer.eraseButton addTarget:self action:@selector(eraseQuestionObjectAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if ([cell_identifier isEqualToString:kMultipleChoiceCellID]) {
        //Multiple Choice
        TGQuestionMultipleChoice *multipleChoice = (TGQuestionMultipleChoice *)cell;
        multipleChoice.titleField.text = name;
        multipleChoice.descriptionLabel.text = question_text;
        multipleChoice.pointsField.text = points;
        multipleChoice.eraseButton.hidden = hidden;
        multipleChoice.isTrashHidden = [self isVisibleStateForObject:mo];
        [multipleChoice setObjectData:mo];
        [multipleChoice.eraseButton addTarget:self action:@selector(eraseQuestionObjectAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if ([cell_identifier isEqualToString:kEssayCellID]) {
        //Essay
        TGQuestionEssay *essay = (TGQuestionEssay *)cell;
        essay.titleField.text = name;
        essay.descriptionLabel.text = question_text;
        essay.pointsField.text = points;
        essay.eraseButton.hidden = hidden;
        [essay setObjectData:mo];
        [essay.eraseButton addTarget:self action:@selector(eraseQuestionObjectAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
}

- (BOOL)isVisibleStateForObject:(NSManagedObject *)object {
    
    BOOL state = NO;
    if (object != nil) {
        
        NSSet *choice_set = [object valueForKey:@"choices"];
        NSInteger count = [choice_set count];
        state = (count == 2) ? YES : NO;
    }
    
    return state;
}

- (NSManagedObject *)managedObjectFromButtonAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (void)eraseQuestionObjectAction:(UIButton *)sender {
    
    __weak typeof(self) wo = self;
    
    NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
    [self.tm removeQuestions:mo withAttribute:@"id" doneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.tableView reloadData];
        });
    }];
}

- (NSString *)identifierForQuestionTypeID:(NSString *)question_type_id {
    
    NSString *identifier = @"cell_identifier";
    
    //ESSAY
    if ([question_type_id isEqualToString:@"6"]) {
        
    }
    
    //MULTIPLE CHOICE
    if ([question_type_id isEqualToString:@"3"]) {
        
    }
    
    //SHORT ANSWER
    if ([question_type_id isEqualToString:@"9"]) {
        
    }
    
    //TRUE OR FALSE
    if ([question_type_id isEqualToString:@"1"]) {
        
    }
    
    return identifier;
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    BOOL isAscending = NO;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:isAscending];
    [fetchRequest setSortDescriptors:@[date_modified]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (NSString *)cellIdentifierForObject:(NSManagedObject *)mo {
    
    NSString *type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"question_type_id"] ];
    NSLog(@"cell question type : %@", type);
    
    NSString *cell_identifier = kShortAnswerCellID;
    
    /*
     {
     "id": "1",
     "value": "True or False"
     },
     {
     "id": "9",
     "value": "Short Answer"
     },
     {
     "id": "3",
     "value": "Multiple Choice"
     },
     {
     "id": "6",
     "value": "Essay"
     }
     */
    
    if ([type isEqualToString:@"1"]) {
        //True or False
        cell_identifier = kTrueFalseCellID;
    }
    
    if ([type isEqualToString:@"2"] || [type isEqualToString:@"9"]) {
        //Fill in the Blanks (Short Answer)
        cell_identifier = kShortAnswerCellID;
    }
    
    if ([type isEqualToString:@"3"]) {
        //Multiple Choice
        cell_identifier = kMultipleChoiceCellID;
    }
    
    if ([type isEqualToString:@"6"]) {
        //Essay
        cell_identifier = kEssayCellID;
    }
    
    return cell_identifier;
}

- (CGFloat)heightForIdentifier:(NSString *)cellIdentifier {
    
    CGFloat height = 300.0f;
    
    if ([cellIdentifier isEqualToString:kTrueFalseCellID]) {
        //True or False
        height = 430.0f;
    }
    
    if ([cellIdentifier isEqualToString:kShortAnswerCellID]) {
        //Fill in the Blanks (Short Answer)
        height = 300.0f;
    }
    
    if ([cellIdentifier isEqualToString:kMultipleChoiceCellID]) {
        //Multiple Choice
        height = 550.0f;
    }
    
    if ([cellIdentifier isEqualToString:kEssayCellID]) {
        //Essay
        height = 300.0f;
    }
    
    return height;
}

@end
