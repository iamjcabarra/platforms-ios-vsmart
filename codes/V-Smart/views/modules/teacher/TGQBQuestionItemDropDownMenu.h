//
//  TGQBQuestionItemDropDownMenu.h
//  V-Smart
//
//  Created by Ryan Migallos on 10/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGQBQuestionItemDropDownDelegate <NSObject>
@optional
- (void)didFinishSelectingItem:(NSString *)string withObject:(NSManagedObject *)object withIndexPath:(NSIndexPath *)indexPath;
@end

@interface TGQBQuestionItemDropDownMenu : UIView
@property (nonatomic, weak) id <TGQBQuestionItemDropDownDelegate> delegate;
- (void)displayCheckState:(NSString *)complete;
- (void)teardownNotificationForItemList;

@end
