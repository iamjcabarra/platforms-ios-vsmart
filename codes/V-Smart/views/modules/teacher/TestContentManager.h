//
//  TestContentManager.h
//  V-Smart
//
//  Created by Julius Abarra on 18/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TestContentManager : NSObject

/////// PUBLIC PROPERTIES //////
@property (strong, nonatomic) NSMutableDictionary *testMainInfo;
@property (strong, nonatomic) NSMutableDictionary *testSettings;
@property (strong, nonatomic) NSArray *testAssignedQuestions;

@property (assign, nonatomic) int actionType;
@property (strong, nonatomic) NSString *testid;

/////// PUBLIC SINGLETON METHODS //////
+ (id)sharedInstance;
- (void)clearEntries;
- (void)recreateObjects;

@end
