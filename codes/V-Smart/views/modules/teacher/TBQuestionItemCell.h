//
//  TBQuestionItemCell.h
//  V-Smart
//
//  Created by Julius Abarra on 10/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBQuestionItemCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionDifficultyLabel;
@property (strong, nonatomic) IBOutlet UIImageView *questionImage;

- (void)updateQuestionTypeView:(NSString *)questionTypeID;
- (void)updateDifficultyLevelView:(NSString *)difficultyLevelID;
- (void)loadWebViewWithContents:(NSString *)string;

@end
