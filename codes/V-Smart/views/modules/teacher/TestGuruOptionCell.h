//
//  TestGuruOptionCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 10/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruOptionCell : UITableViewCell

@property (strong, nonatomic) NSString *type;

- (void)displayData:(NSDictionary *)data;
- (NSDictionary *)getOptionData;

@end
