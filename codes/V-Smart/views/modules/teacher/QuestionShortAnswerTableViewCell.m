//
//  QuestionShortAnswerTableViewCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 7/30/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "QuestionShortAnswerTableViewCell.h"

@interface QuestionShortAnswerTableViewCell() <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UILabel *questionTypeTitle;
@property (strong, nonatomic) IBOutlet UILabel *questionTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionPointLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;

@end

@implementation QuestionShortAnswerTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.questionTypeTitle.text = NSLocalizedString(@"Short Answer", nil);
    self.questionTitleLabel.text = NSLocalizedString(@"Title", nil);
    self.questionPointLabel.text = NSLocalizedString(@"Points", nil);
    self.questionLabel.text = NSLocalizedString(@"Question", nil);
    self.questionPointsField.text = @"1.0";
    self.questionPointsField.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSManagedObject *)saveContents:(NSDictionary *)object {
    
    if ( self.question != nil ) {
        
        BOOL save_status = YES;
        
        NSString *name = [self normalizeData: self.questionField.text ];
        NSString *points = [self normalizeData: self.questionPointsField.text ];
        NSString *question_text = [self normalizeData: self.questionTextArea.text ];
        if ( (name.length <= 0) || (points.length <= 0) || (question_text.length <= 0)) {
            if (points.length <= 0) {
                self.questionPointsField.text = @"1.0";
            }
            save_status = NO;
        }
        
        
        if (save_status) {
            
            NSMutableDictionary *data = [NSMutableDictionary dictionary];
            for (NSString *key in [object allKeys]) {
                
                if ( ![key isEqualToString:@"tags"] ) {
                    [data setValue:[object objectForKey:key] forKey:key];
                }
                
                if ( [key isEqualToString:@"tags"] ) {
                    NSMutableArray *array = [object objectForKey:key];
                    [data setValue:[self buildTags:array managedObject:self.question] forKey:key];
                }

            }
            data[@"name"] = name;
            data[@"points"] = points;
            data[@"question_text"] = question_text;
            
            NSManagedObjectContext *ctx = self.question.managedObjectContext;
            for (NSString *key in [data allKeys] ) {
                [self.question setValue:[data objectForKey:key] forKey:key];
            }
            [self saveTreeContext:ctx];
            
            return self.question;
            
        } else {
            
            NSString *message = NSLocalizedString(@"Please fill in all required fields for question", nil);
            NSString *ok = NSLocalizedString(@"Okay", nil);
            NSString *title_alert = NSLocalizedString(@"Save Error", nil);
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:title_alert
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:ok
                                               otherButtonTitles:nil];
            [av show];
        }
    }
    
    return nil;
}

- (void)saveTreeContext:(NSManagedObjectContext *)context {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    if (!context) {
        return;
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"ERROR saving: %@", error);
    }
    
    if (context.parentContext) {
        [self saveTreeContext:context.parentContext];
    }
}

- (NSString *)normalizeData:(NSString *)string {
    
    NSString *text = @"";
    
    if (string.length > 0) {
        
        NSString *value = [NSString stringWithFormat:@"%@", string ];
        text = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    
    return text;
}

- (NSSet *)buildTags:(NSArray *)tags managedObject:(NSManagedObject *)mo {
    return [self insertTags:tags managedObject:mo];
}

- (NSSet *)insertTags:(NSArray *)tags managedObject:(NSManagedObject *)mo {
    
    NSManagedObjectContext *ctx = mo.managedObjectContext;
    NSSet *sets = [NSSet set];
    
    if (tags.count > 0) {
        
        NSMutableSet *tag_sets = [NSMutableSet set];
        
        for (NSDictionary *t in tags) {
            NSString *tag_id = [self normalizeData: t[@"id"] ];
            NSString *tag_name = [self normalizeData: t[@"tag"] ];
            
            NSManagedObject *t_mo = [NSEntityDescription insertNewObjectForEntityForName:@"Tag" inManagedObjectContext:ctx];
            [t_mo setValue:tag_id forKey:@"id"];
            [t_mo setValue:tag_name forKey:@"tag"];
            
            [tag_sets addObject:t_mo];
        }
        
        sets = [NSSet setWithSet:tag_sets];
        
        // assign tags
        [mo setValue:sets forKey:@"tags"];
    }
    
    return sets;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ( textField == self.questionPointsField ) {
        
        // allow backspace
        if (!string.length)
        {
            return YES;
        }
        
        NSCharacterSet *numberSet = [NSCharacterSet decimalDigitCharacterSet];
        if ([string rangeOfCharacterFromSet:[numberSet invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        
        // verify max length has not been exceeded
        NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if (updatedText.length > 3) // 4 was chosen for SSN verification
        {
            // suppress the max length message only when the user is typing
            // easy: pasted data has a length greater than 1; who copy/pastes one character?
            if (string.length > 1)
            {
                // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
            }
            return NO;
        }
        
        //        NSArray *sep = [updatedText componentsSeparatedByString:@"."];
        //        if([sep count] >= 2) {
        //            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
        //            return !([sepStr length]>1);
        //        }
    }
    return YES;
}

@end
