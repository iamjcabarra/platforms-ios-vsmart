//
//  TGTMTestViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 22/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class TGTMTestViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, TGTMCourseTableViewControllerDelegate, TGTMStatusTableViewControllerDelegate, UISearchBarDelegate {

    @IBOutlet fileprivate var emptyPlaceholderView: UIView!
    @IBOutlet fileprivate var emptyPlaceholderLabel: UILabel!
    @IBOutlet fileprivate var courseTextField: UITextField!
    @IBOutlet fileprivate var courseButton: UIButton!
    @IBOutlet fileprivate var statusTextField: UITextField!
    @IBOutlet fileprivate var statusButton: UIButton!
    @IBOutlet fileprivate var searchBar: UISearchBar!
    @IBOutlet fileprivate var loadMoreButton: UIButton!
    @IBOutlet fileprivate var tableView: UITableView!
    
    fileprivate var tableRefreshControl: UIRefreshControl!
    fileprivate var courseSelectionPopup: TGTMCourseTableViewController!
    fileprivate var statusSelectionPopup: TGTMStatusTableViewController!
    
    fileprivate var currentPageNumber = 1
    fileprivate var totalNumberOfFilteredTest = 0
    fileprivate var totalNumberOfTest = 0
    fileprivate var totalNumberOfPage = 0
    
    fileprivate var courseID = ""
    fileprivate var statusID = ""
    fileprivate var searchKeyword = ""
    
    fileprivate var dateHelper = DateHelper()
    
    fileprivate let kTestDetailsViewSegueIdentifier = "SHOW_TGTM_TEST_DETAILS_VIEW"
    
    // MARK: - Data Manager
    
    fileprivate lazy var tgmDataManager: TestGuruDataManager = {
        let tgdm = TestGuruDataManager.sharedInstance()
        return tgdm!
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.courseTextField.text = NSLocalizedString("All Course", comment: "")
        self.statusTextField.text = NSLocalizedString("All Status", comment: "")
        self.searchBar.placeholder = NSLocalizedString("Search", comment: "")
        self.searchBar.delegate = self
        
        let courseButtonAction = #selector(self.courseButtonAction(_:))
        self.courseButton.addTarget(self, action: courseButtonAction, for: .touchUpInside)
        
        let statusButtonAction = #selector(self.statusButtonAction(_:))
        self.statusButton.addTarget(self, action: statusButtonAction, for: .touchUpInside)
        
        let loadMoreButtonAction = #selector(self.loadMoreButtonAction(_:))
        self.loadMoreButton.addTarget(self, action: loadMoreButtonAction, for: .touchUpInside)
        
        let loadMoreText = NSLocalizedString("Load More", comment: "")
        self.loadMoreButton.setTitle(loadMoreText, for: UIControlState())
        self.loadMoreButton.setTitle(loadMoreText, for: .highlighted)
        self.loadMoreButton.setTitle(loadMoreText, for: .selected)
        
        let refreshTestListAction = #selector(self.refreshTestListAction(_:))
        self.tableRefreshControl = UIRefreshControl.init()
         self.tableRefreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: ""))
        self.tableRefreshControl.addTarget(self, action: refreshTestListAction, for: .valueChanged)
        self.tableView.addSubview(self.tableRefreshControl)
        
        self.shouldShowEmptyPlaceholder(false)
        
        let parameters = self.builtParametersForPagination(isReset: true, nextPage: false)
        self.loadPaginatedTestListForParameters(parameters, isRefreshing: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.title = NSLocalizedString("Test Management", comment: "")
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "#343434")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Paginated Test List
    
    fileprivate func loadPaginatedTestListForParameters(_ parameters: [String: String], isRefreshing: Bool) {
        self.loadMoreButton.isHidden = true
        
        if !isRefreshing {
            let message = "\(NSLocalizedString("Loading", comment: ""))..."
            self.view.makeToastActivity(message: message)
            self.view.isUserInteractionEnabled = false
        }
        
        let coordinator = self.tgmDataManager.loginUser()
        
        self.tgmDataManager.requestPaginatedTestList(forCoordinator: coordinator, paginationParameters: parameters) { (dataBlock) in
            if let data = dataBlock as? [String: Int] {
                self.currentPageNumber = data["current_page"]!
                self.totalNumberOfFilteredTest = data["total_filtered"]!
                self.totalNumberOfTest = data["total_items"]!
                self.totalNumberOfPage = data["total_pages"]!
                
                DispatchQueue.main.async(execute: {
                    self.title = NSLocalizedString("Test Management", comment: "") + " (\(self.totalNumberOfTest))"
                    isRefreshing ? self.tableRefreshControl.endRefreshing() : self.view.hideToastActivity()
                    self.view.isUserInteractionEnabled = true
                    self.loadMoreButton.isHidden = self.totalNumberOfPage > self.currentPageNumber ? false : true
                    self.shouldShowEmptyPlaceholder(self.totalNumberOfFilteredTest < 1 ? true : false)
                    self.reloadFetchedResultsController()
                })
            }
            else {
                DispatchQueue.main.async(execute: {
                    isRefreshing ? self.tableRefreshControl.endRefreshing() : self.view.hideToastActivity()
                    self.view.isUserInteractionEnabled = true
                    self.loadMoreButton.isHidden = true
                    self.shouldShowEmptyPlaceholder(true)
                    self.reloadFetchedResultsController()
                })
            }
        }
    }
    
    fileprivate func builtParametersForPagination(isReset reset: Bool, nextPage: Bool) -> [String: String] {
        var limit = self.tgmDataManager.stringValue(self.tgmDataManager.fetchObject(forKey: kTG_PAGINATION_LIMIT))
        var newPageNumber = "1"
        if Int(limit!) < 1 { limit = "10" }
        if !reset { newPageNumber = nextPage ? "\(self.currentPageNumber + 1)": "\(self.currentPageNumber)" }
        return ["current_page": newPageNumber, "limit": limit!, "course_id": self.courseID, "is_approved": self.statusID, "search_keyword": self.searchKeyword]
    }
    
    // MARK: - Course Selection Modal Popup
    
    func courseButtonAction(_ sender: UIButton) {
        self.courseSelectionPopup = TGTMCourseTableViewController(nibName: "TGTMCourseTableViewController", bundle: nil)
        self.courseSelectionPopup.delegate = self
        
        self.courseSelectionPopup.modalPresentationStyle = .popover
        self.courseSelectionPopup.preferredContentSize = CGSize(width: self.courseTextField.frame.width, height: 200.0)
        self.courseSelectionPopup.popoverPresentationController?.permittedArrowDirections = .up
        self.courseSelectionPopup.popoverPresentationController?.sourceView = sender
        self.courseSelectionPopup.popoverPresentationController?.sourceRect = sender.bounds
        self.present(self.courseSelectionPopup, animated: true, completion: nil)
    }
    
    func selectedCourseObject(_ object: [String: AnyObject]) {
        if let courseID = object["id"] as? Int, let name = object["name"] as? String {
            self.courseID = courseID == -1 ? "" : "\(courseID)"
            self.courseTextField.text = name
            
            let parameters = self.builtParametersForPagination(isReset: true, nextPage: false)
            self.loadPaginatedTestListForParameters(parameters, isRefreshing: false)
        }
    }
    
    // MARK: - Status Selection Modal Popup
    
    func statusButtonAction(_ sender: UIButton) {
        self.statusSelectionPopup = TGTMStatusTableViewController(nibName: "TGTMStatusTableViewController", bundle: nil)
        self.statusSelectionPopup.delegate = self
        
        self.statusSelectionPopup.modalPresentationStyle = .popover
        self.statusSelectionPopup.preferredContentSize = CGSize(width: self.statusTextField.frame.width, height: 200.0)
        self.statusSelectionPopup.popoverPresentationController?.permittedArrowDirections = .up
        self.statusSelectionPopup.popoverPresentationController?.sourceView = sender
        self.statusSelectionPopup.popoverPresentationController?.sourceRect = sender.bounds
        self.present(self.statusSelectionPopup, animated: true, completion: nil)
    }
    
    func selectedStatusObject(_ object: [String: String]) {
        if let statusID = object["id"], let name = object["name"] {
            self.statusID = statusID
            self.statusTextField.text = name
            
            let parameters = self.builtParametersForPagination(isReset: true, nextPage: false)
            self.loadPaginatedTestListForParameters(parameters, isRefreshing: false)
        }
    }
    
    // MARK: - Search Bar Delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText == "") {
            self.searchKeyword = searchText
            let parameters = self.builtParametersForPagination(isReset: true, nextPage: false)
            self.loadPaginatedTestListForParameters(parameters, isRefreshing: false)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchKeyword = searchBar.text!
        let parameters = self.builtParametersForPagination(isReset: true, nextPage: false)
        self.loadPaginatedTestListForParameters(parameters, isRefreshing: false)
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = self.searchKeyword
    }
    
    // MARK: - Button Event Handlers
    
    func loadMoreButtonAction(_ sender: UIButton) {
        let parameters = self.builtParametersForPagination(isReset: false, nextPage: true)
        self.loadPaginatedTestListForParameters(parameters, isRefreshing: false)
    }
    
    func refreshTestListAction(_ sender: UIRefreshControl) {
        let parameters = self.builtParametersForPagination(isReset: true, nextPage: false)
        self.loadPaginatedTestListForParameters(parameters, isRefreshing: true)
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "test_cell_identifier", for: indexPath) as! TGTMTestTableViewCell
        cell.selectionStyle = .none
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: TGTMTestTableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath) 
        
        let name = mo.value(forKey: "name") as! String
        let test_name_label = NSLocalizedString("Test Name", comment: "")
        let actual_test_name = "\(test_name_label): \(name)"
        cell.testNameLabel.attributedText = self.attributeString(actual_test_name, location: 0, length: test_name_label.characters.count + 1)
        
        let course_name = mo.value(forKey: "course_name") as! String
        let course_name_label = NSLocalizedString("Course", comment: "")
        let actual_course_name = "\(course_name_label): \(course_name)"
        cell.courseNameLabel.attributedText = self.attributeString(actual_course_name, location: 0, length: course_name_label.characters.count + 1)

        let last_name = mo.value(forKey: "last_name") as! String
        let first_name = mo.value(forKey: "first_name") as! String
        let teacher_name_label = NSLocalizedString("Teacher", comment: "")
        let actual_teacher_name = "\(teacher_name_label): \(last_name), \(first_name)"
        cell.teacherNameLabel.attributedText = self.attributeString(actual_teacher_name, location: 0, length: teacher_name_label.characters.count + 1)
        
        let date_created = mo.value(forKey: "date_created") as! String
        let formatted_date_created = self.dateHelper.dateString(date_created,
                                                                fromFormat: "yyyy-MM-dd HH:mm:ss",
                                                                toFormat: "EEE, MMM dd, yyyy hh:mm a",
                                                                fromTimeZone: "UTC",
                                                                setToLocalTimeZone: true)
        let date_label = NSLocalizedString("Date", comment: "")
        let actual_date = "\(date_label): \(formatted_date_created)"
        cell.dateLabel.attributedText = self.attributeString(actual_date, location: 0, length: date_label.characters.count + 1)
        
        let item_count = mo.value(forKey: "item_count") as! String
        let item_count_label = Int(item_count) > 1 ? NSLocalizedString("Items", comment: "") : NSLocalizedString("Item", comment: "")
        let actual_item_count = "\(item_count_label): \(item_count)"
        cell.itemLabel.attributedText = self.attributeString(actual_item_count, location: 0, length: item_count_label.characters.count + 1)
        
        let id = mo.value(forKey: "id") as! Int
        let status = mo.value(forKey: "is_approved") as! String
        self.configureSwipeableButtonForCell(cell, testID: "\(id)", testStatus: status)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = (indexPath as NSIndexPath).row % 2 == 0 ? UIColor(rgba: "#ECEFF1") : UIColor.white
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        let id: String = self.tgmDataManager.stringValue(mo.value(forKey: "id"))
        let name: String = self.tgmDataManager.stringValue(mo.value(forKey: "name"))
        let is_approved: String = self.tgmDataManager.stringValue(mo.value(forKey: "is_approved"))
        
        let object: [String:String] = ["id": id, "name": name, "is_approved": is_approved]
        
        let message = "\(NSLocalizedString("Please wait", comment: ""))..."
        self.view.makeToastActivity(message: message)
        self.view.isUserInteractionEnabled = false
    
        self.tgmDataManager.requestTestDetails(forTest: id) { (success) in
            if success {
                DispatchQueue.main.async(execute: {
                    self.view.hideToastActivity()
                    self.view.isUserInteractionEnabled = true
                    self.performSegue(withIdentifier: self.kTestDetailsViewSegueIdentifier, sender: object)
                    tableView.deselectRow(at: indexPath, animated: true)
                })
            }
            else {
                DispatchQueue.main.async(execute: {
                    self.view.hideToastActivity()
                    self.view.isUserInteractionEnabled = true
                    let message = NSLocalizedString("There was an error accessing this test. Please try again later.", comment: "")
                    self.view.makeToast(message: message, duration: 2.0, position: "Center" as AnyObject)
                })
            }
        }
    }
    
    // MARK: - Swipeable Buttons Configuration
    
    fileprivate func configureSwipeableButtonForCell(_ cell: TGTMTestTableViewCell, testID: String, testStatus: String) {
        
        // Approve Action
        let titleA = NSLocalizedString("Approve", comment: "")
        let imageA = UIImage(named:"approve-50px.png")
        let colorA = UIColor(rgba: "#4CD964")
        
        let user_id: String = self.tgmDataManager.loginUser()
        
        let buttonA: MGSwipeButton! = MGSwipeButton(title: titleA, icon: imageA, backgroundColor: colorA, callback: {
            (sender) -> Bool in
            
            let body = ["quiz_ids": [["id": testID, "status": "1", "user_id":user_id]]]
            self.tgmDataManager.requestChangeTestStatus(testStatus, body: body, doneBlock: { (success) in
                DispatchQueue.main.async(execute: {
                    self.reloadFetchedResultsController()
                    let messageA = NSLocalizedString("Approving of test was successful.", comment: "")
                    let messageB = NSLocalizedString("Approving of test was unsuccessful.", comment: "")
                    self.view.makeToast(message: success ? messageA : messageB, duration: 2.0, position: "Center" as AnyObject)
                    self.reloadFetchedResultsController()
                })
            })
            
            return true
        })
        
        buttonA.setTitleColor(UIColor.white, for: UIControlState())
        buttonA.centerIconOverText()
        
        // Disapprove Action
        let titleB = NSLocalizedString("Disapprove", comment: "")
        let imageB = UIImage(named:"disapprove-50px.png")
        let colorB = UIColor(rgba: "#FF3B30")
        
        let buttonB: MGSwipeButton! = MGSwipeButton(title: titleB, icon: imageB, backgroundColor: colorB, callback: {
            (sender) -> Bool in
            
            let body = ["quiz_ids": [["id": testID, "status": "2", "user_id":user_id]]]
            self.tgmDataManager.requestChangeTestStatus(testStatus, body: body, doneBlock: { (success) in
                DispatchQueue.main.async(execute: {
                    let messageA = NSLocalizedString("Disapproving of test was successful.", comment: "")
                    let messageB = NSLocalizedString("Disapproving of test was unsuccessful.", comment: "")
                    self.view.makeToast(message: success ? messageA : messageB, duration: 2.0, position: "Center" as AnyObject)
                    self.reloadFetchedResultsController()
                })
            })
            
            return true
        })
        
        buttonB.setTitleColor(UIColor.white, for: UIControlState())
        buttonB.centerIconOverText()
        
        var buttons: [MGSwipeButton]? = nil
        
        if testStatus == "0" {
            buttons = [buttonB, buttonA]
        }
        
        if testStatus == "2" {
            buttons = [buttonA]
        }
        
        cell.rightButtons = buttons
        cell.rightSwipeSettings.transition = MGSwipeTransition.rotate3D
        cell.updateStatus(testStatus)
    }
    
    // MARK: - Add Attribute to String
    
    func attributeString(_ string: String, location: Int, length: Int) -> NSMutableAttributedString {
        var mutableString = NSMutableAttributedString()
        mutableString = NSMutableAttributedString(string: string, attributes: nil)
        mutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(rgba: "#7F7F7F"), range: NSRange(location: location, length: length))
        mutableString.addAttributes([NSFontAttributeName:UIFont.systemFont(ofSize: 17.0)], range: NSRange(location: location, length: length))
        return mutableString
    }
    
    // MARK: - Empty Placeholder View
    
    fileprivate func shouldShowEmptyPlaceholder(_ show: Bool) {
        self.emptyPlaceholderView.isHidden = !show
        
        if show {
            var message = ""
            
            if self.searchKeyword != "" {
                message = "\(NSLocalizedString("No results found for", comment: "")) \"\(self.searchKeyword)\"."
            }
            else if self.courseID != "" || self.statusID != "" {
                message = NSLocalizedString("No results found.", comment: "")
            }
            else {
                message = NSLocalizedString("No available test.", comment: "")
            }
            
            self.emptyPlaceholderLabel.text = message
        }
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kCoordinatorTestEntity)
        
        let ctx = self.tgmDataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: kCoordinatorTestEntity)
        fetchRequest.fetchBatchSize = 20
        
        if self.statusID != "" {
            let predicate = NSComparisonPredicate(keyPath: "is_approved", withValue: self.statusID, isExact: true)
            fetchRequest.predicate = predicate
        }
        
        let sortDescriptor = NSSortDescriptor(key: "sort_date_modified", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.tableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! TGTMTestTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.kTestDetailsViewSegueIdentifier {
            let detailsView = segue.destination as! TGTMTestDetailsViewController
            if let object = sender as? [String: String] {
                detailsView.testID = object["id"]!
                detailsView.testName = object["name"]!
                detailsView.testStatus = object["is_approved"]!
            }
        }
    }

}
