//
//  TBDetailTableViewCell.m
//  TestBankVersion2.1
//
//  Created by Julius Abarra on 06/02/2016.
//  Copyright © 2016 Vibe Technologies, Inc. All rights reserved.
//

#import "TBDetailTableViewCell.h"

@implementation TBDetailTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
