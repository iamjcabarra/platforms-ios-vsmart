//
//  TGPackageCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 17/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGPackageCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *packageName;
@property (strong, nonatomic) IBOutlet UIImageView *packageImageView;

@end
