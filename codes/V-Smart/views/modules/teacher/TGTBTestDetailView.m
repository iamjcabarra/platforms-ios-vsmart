//
//  TGTBTestDetailView.m
//  V-Smart
//
//  Created by Julius Abarra on 07/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "TGTBTestDetailView.h"
#import "TGTBQuestionItemCell.h"
#import "TBSectionSelectionView.h"
#import "TGTBCreateTestView.h"
#import "QuestionSamplerView.h"
#import "TGTBTestActionConstants.h"
#import "TBClassHelper.h"
#import "TestGuruDataManager.h"
#import "UIImageView+WebCache.h"
#import "V_Smart-Swift.h"
#import "HUD.h"

////////////// NSSTRING HTML CATEGORY IMPLEMENTATION //////////////

@interface NSString (HtmlCustomCheck)
- (BOOL)containsHTML;
@end

@implementation NSString (HtmlCustomCheck)

- (BOOL)containsHTML {
    NSString *stringcopy = [self copy];
    NSString *expression = @"<[^>]+>";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:stringcopy
                                                        options:0
                                                          range:NSMakeRange(0, [stringcopy length])];
    
    if (numberOfMatches > 0) {
        return YES;
    }
    
    return NO;
}

@end

////////////// NSSTRING HTML CATEGORY IMPLEMENTATION //////////////

@interface TGTBTestDetailView () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate, TGTBCreateTestViewDelegate, TGTBTestPlayLandingDelegate, TGTBQuestionPreviewDelegate>

@property (strong, nonatomic) TBClassHelper *classHelper;
@property (strong, nonatomic) TestGuruDataManager *tgdm;

@property (strong, nonatomic) NSManagedObject *testDetail;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) TGTBTestPlayLandingView *testPlayLanding;

@property (strong, nonatomic) IBOutlet UIView *emptyView;
@property (strong, nonatomic) IBOutlet UILabel *emptyMessageLabel;

@property (strong, nonatomic) IBOutlet UIView *testInfoView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *testInfoViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@property (strong, nonatomic) IBOutlet UILabel *generalInstructionsLabel;
@property (strong, nonatomic) IBOutlet UILabel *generalInstructionsValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *attemptsLabel;
@property (strong, nonatomic) IBOutlet UILabel *attemptsValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *startDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *startDateValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *timeLimitLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLimitValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *endDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *endDateValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *passingScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *passingScoreValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalPointsLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalPointsValueLabel;

@property (strong, nonatomic) IBOutlet UIButton *previewButton;

@property (strong, nonatomic) IBOutlet UILabel *assignedQuestionsLabel;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIButton *sortButton;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIView *deployView;
@property (strong, nonatomic) IBOutlet UIView *saveToPDFView;
@property (strong, nonatomic) IBOutlet UIView *editView;
@property (strong, nonatomic) IBOutlet UIView *commentView;

@property (strong, nonatomic) IBOutlet UIButton *deployButton;
@property (strong, nonatomic) IBOutlet UIButton *saveToPDFButton;
@property (strong, nonatomic) IBOutlet UIButton *editButton;
@property (strong, nonatomic) IBOutlet UIButton *commentButton;

@property (strong, nonatomic) IBOutlet UILabel *deployLabel;
@property (strong, nonatomic) IBOutlet UILabel *saveToPDFLabel;
@property (strong, nonatomic) IBOutlet UILabel *editLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentLabel;

@property (assign, nonatomic) int actionType;
@property (strong, nonatomic) NSArray *sortDescriptors;
@property (assign, nonatomic) BOOL isViewByNewest;

@end

@implementation TGTBTestDetailView

static NSString *kQuestionCellIdentifer = @"questionCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Test Guru Data Manager
    self.tgdm = [TestGuruDataManager sharedInstance];
    
    // Localize Strings
    [self setUpStringLocalization];
    
    // Test Information
    [self.testInfoView customShadow:5.0f];
    [self loadSelectedTestHeaderInformation];
    [self updateTestViewWithDynamicHeight];
    
    // Question Table View
    [self setUpSelectedQuestionsView];
    
    // Search Question
    self.searchBar.delegate = self;
    self.searchBar.placeholder = [self.classHelper localizeString:@"Search"];
    
    // Sort Question
    self.isViewByNewest = NO;
    [self.sortButton addTarget:self
                        action:@selector(sortQuestionButtonAction:)
              forControlEvents:UIControlEventTouchUpInside];
    
    // Action Button
    [self.previewButton addTarget:self
                           action:@selector(previewButtonAction:)
                 forControlEvents:UIControlEventTouchUpInside];
    
    [self.deployButton addTarget:self
                          action:@selector(deployButtonAction:)
                forControlEvents:UIControlEventTouchUpInside];
    
    [self.saveToPDFButton addTarget:self
                             action:@selector(saveToPDFButtonAction:)
                   forControlEvents:UIControlEventTouchUpInside];
    
    [self.editButton addTarget:self
                        action:@selector(editButtonAction:)
              forControlEvents:UIControlEventTouchUpInside];
    
    [self.commentButton addTarget:self
                           action:@selector(commentButtonAction:)
                 forControlEvents:UIControlEventTouchUpInside];
    
    // Hide Save to PDF View
    self.saveToPDFView.hidden = YES;
    
    NSString *is_approved = [self.tgdm stringValue:[self.testObject valueForKey:@"is_approved"]];
    if ([is_approved isEqual: @"1"] == NO) {
        self.deployView.hidden = YES;
    }
    
    
    [self updateQuestionCountLabel];
    [self setUpSortDescriptors];
    
    // Empty View
    self.emptyView.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self customizeNavigationController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar

- (void)customizeNavigationController {
    UIColor *color = UIColorFromHex(0xF69C42);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    }
    else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

#pragma mark - Test Header View

- (void)setUpStringLocalization {
    NSString *generalInstructionsLabel = [[self.classHelper localizeString:@"General Instructions"] uppercaseString];
    NSString *attemptsLabel = [[self.classHelper localizeString:@"Attempt"] uppercaseString];
    NSString *timeLimitLabel = [[self.classHelper localizeString:@"Time Limit"] uppercaseString];
    NSString *passingScoreLabel = [[self.classHelper localizeString:@"Passing Score"] uppercaseString];
    NSString *startDateLabel = [[self.classHelper localizeString:@"Start Date"] uppercaseString];
    NSString *endDateLabel = [[self.classHelper localizeString:@"End Date"] uppercaseString];
    NSString *totalPointsLabel = [[self.classHelper localizeString:@"Total Points"] uppercaseString];
    NSString *assignedQuestionsLabel = [[self.classHelper localizeString:@"Assigned Questions"] uppercaseString];
    NSString *deployLabel = [self.classHelper localizeString:@"Deploy"];
    NSString *saveToPDFLabel = [self.classHelper localizeString:@"Save to PDF"];
    NSString *editLabel = [self.classHelper localizeString:@"Edit"];
    NSString *commentLabel = [self.classHelper localizeString:@"Comment"];
    
    self.generalInstructionsLabel.text = generalInstructionsLabel;
    self.attemptsLabel.text = attemptsLabel;
    self.timeLimitLabel.text = timeLimitLabel;
    self.passingScoreLabel.text = passingScoreLabel;
    self.startDateLabel.text = startDateLabel;
    self.endDateLabel.text = endDateLabel;
    self.totalPointsLabel.text = totalPointsLabel;
    self.assignedQuestionsLabel.text = assignedQuestionsLabel;
    self.deployLabel.text = deployLabel;
    self.saveToPDFLabel.text = saveToPDFLabel;
    self.editLabel.text= editLabel;
    self.commentLabel.text = commentLabel;
}

- (void)loadSelectedTestHeaderInformation {
    // Title
    self.title = [self.testObject valueForKey:@"name"];
    
    // General Instructions
    NSString *instructions = [self.testObject valueForKey:@"general_instruction"];
    [self.classHelper justifyLabel:self.generalInstructionsValueLabel string:instructions];
    self.generalInstructionsValueLabel.numberOfLines = 0;
    
    // Attempts
    NSString *attempts = [self.testObject valueForKey:@"attempts"];
    self.attemptsValueLabel.text = attempts;
    [self updateAttemptCountLabel:[attempts integerValue]];
    
    // Time Limit
    self.timeLimitValueLabel.text = [self.testObject valueForKey:@"time_limit"];
    
    // Passing Score
    NSString *passingScore = [self.testObject valueForKey:@"passing_score"];
    self.passingScoreValueLabel.text = passingScore;
    
    // Start Date
    NSString *formattedDateStart = [self.testObject valueForKey:@"formatted_date_open"];
    self.startDateValueLabel.text = formattedDateStart;
    
    // End Date
    NSString *formattedDateEnd = [self.testObject valueForKey:@"formatted_date_close"];
    BOOL noExpiry = [[self.testObject valueForKey:@"no_expiry"] isEqualToString:@"1"];
    BOOL isExpired = [[self.testObject valueForKey:@"is_expired"] boolValue];
    
    if (noExpiry) {
        formattedDateEnd = [self.classHelper localizeString:@"No Expiration"];
    }
    
    self.endDateValueLabel.text = formattedDateEnd;
    [self.endDateValueLabel setTextColor:[UIColor blackColor]];
    
    if (isExpired && !noExpiry) {
        [self.endDateValueLabel setTextColor:[UIColor redColor]];
    }
    
    // Total Points
    NSString *totalPoints = [self.testObject valueForKey:@"total_score"];
    self.totalPointsValueLabel.text = totalPoints;
}

- (void)updateAttemptCountLabel:(NSInteger)count {
    NSString *text = [[self.classHelper localizeString:@"Attempt"] uppercaseString];
    
    if (count > 1) {
        text = [[self.classHelper localizeString:@"Attempts"] uppercaseString];
    }
    
    self.attemptsLabel.text = text;
}

- (void)updateQuestionCountLabel {
    NSString *assignedQuestionsLabel = [[self.classHelper localizeString:@"Assigned Question"] uppercaseString];
    NSInteger assignedQuestionsCount = [self.tgdm fetchCountForEntity:kTestQuestionItemEntity];
    
    if (assignedQuestionsCount > 1) {
        assignedQuestionsLabel = [[self.classHelper localizeString:@"Assigned Questions"] uppercaseString];
    }
    
    assignedQuestionsLabel = [NSString stringWithFormat:@"%@ (%zd)", assignedQuestionsLabel, assignedQuestionsCount];
    self.assignedQuestionsLabel.text = assignedQuestionsLabel;
}

- (void)updateTestViewWithDynamicHeight {
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.25f animations:^{
            CGFloat additionalHeight = [wo.classHelper getHeightOfLabel:wo.generalInstructionsValueLabel];
            wo.testInfoViewHeight.constant = 208.0f + additionalHeight;
            wo.contentViewHeight.constant = wo.view.frame.size.height + additionalHeight;
            [wo.view layoutIfNeeded];
        }];
    });
}

#pragma mark - Create Test View Delegate

- (void)willReloadUpdatedTestDetailsView:(BOOL)willReload {
    if (willReload) {
        NSString *testid = [self.testObject valueForKey:@"id"];
        __weak typeof(self) wo = self;
        
        [self.tgdm requestTestDetailsForTest:testid doneBlock:^(BOOL status) {
            if (status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [wo loadSelectedTestHeaderInformation];
                    [wo updateTestViewWithDynamicHeight];
                    [wo reloadFetchedResultsController];
                    [wo updateQuestionCountLabel];
                });
            }
        }];
    }
}

#pragma mark - Search Bar Delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self reloadFetchedResultsController];
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self reloadFetchedResultsController];
}

#pragma mark - Button Action

- (void)sortQuestionButtonAction:(id)sender {
    self.isViewByNewest = (self.isViewByNewest) ? NO : YES;
    [self setUpSortDescriptors];
    [self reloadFetchedResultsController];
}

- (void)previewButtonAction:(UIButton *)button {
    [self performSegueWithIdentifier:@"showTestPreview" sender:self];
}

- (void)deployButtonAction:(id)sender {
    self.actionType = TGTBSwipeButtonActionDeploy;
    [self performSegueWithIdentifier:@"showSectionSelectionView" sender:nil];
}

- (void)saveToPDFButtonAction:(id)sender {
    self.actionType = TGTBSwipeButtonActionSaveAsPDF;
    [self performSegueWithIdentifier:@"showSectionSelectionView" sender:nil];
}

- (void)editButtonAction:(id)sender {
    self.actionType = TGTBCrudActionTypeEdit;
    
    NSString *testid = [NSString stringWithFormat:@"%@", [self.testObject valueForKey:@"id"]];
    NSPredicate *predicate = [self.tgdm predicateForKeyPath:@"id" andValue:testid];
    NSManagedObject *object = [self.tgdm getEntity:kTestInformationEntity predicate:predicate];
    
    __weak typeof(self) wo = self;
    
    [self.tgdm copyQuestionItemsFromTestInformation:object doneBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo performSegueWithIdentifier:@"showCreateTestView" sender:nil];
            });
        }
    }];
}

- (void)commentButtonAction:(id)sender {
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
         [wo performSegueWithIdentifier:@"SHOW_TGTM_TEST_COMMENT_VIEW" sender:self];
    });
}

#pragma mark - Sort Descriptors

- (void)setUpSortDescriptors {
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"sort_date" ascending:self.isViewByNewest];
    NSSortDescriptor *question_text = [NSSortDescriptor sortDescriptorWithKey:@"question_text" ascending:self.isViewByNewest];
    NSSortDescriptor *question_id = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:self.isViewByNewest];
    
    self.sortDescriptors = @[date_modified,question_text,question_id];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Section Selection View
    if ([segue.identifier isEqualToString:@"showSectionSelectionView"]) {
        TBSectionSelectionView *sectionView = (TBSectionSelectionView *)[segue destinationViewController];
        sectionView.testSwipeButtonActionType = self.actionType;
        sectionView.testObject = self.testObject;
    }
    
    // Test Preview
    if ([segue.identifier isEqualToString:@"showTestPreview"]) {
        self.testPlayLanding = (TGTBTestPlayLandingView *)[segue destinationViewController];
        self.testPlayLanding.testObject = self.testObject;
        self.testPlayLanding.delegate = self;
        [self hideBackButtonTitle];
    }
    
    // Edit Test View
    if ([segue.identifier isEqualToString:@"showCreateTestView"]) {
        TGTBCreateTestView *createTestView = (TGTBCreateTestView *)[segue destinationViewController];
        createTestView.customBackBarButtonTitle = self.title;
        createTestView.testObject = self.testObject;
        createTestView.crudActionType = self.actionType;
        createTestView.isFromTestDetailsView = YES;
        createTestView.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"SAMPLE_QUESTION_VIEW_SEGUE"]) {
        QuestionSamplerView *sampler = (QuestionSamplerView *)[segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        sampler.indexPath = indexPath;
        sampler.entityObject = kQuestionEntity;
        sampler.sortDescriptors = self.sortDescriptors;
        sampler.isTestPreview = YES;
    }
    
    if ([segue.identifier isEqualToString:@"TGTB_QUESTION_SAMPLER_VIEW_2"]) {
        TGTBQuestionPreview *qtv = (TGTBQuestionPreview *)[segue destinationViewController];
        qtv.title = self.title;
        qtv.isEditingMode = NO;
        qtv.testObject = self.testObject;
        qtv.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"SHOW_TGTM_TEST_COMMENT_VIEW"]) {
        TGTMTestCommentViewController *commentView = (TGTMTestCommentViewController *)[segue destinationViewController];
        commentView.test_id = [self.tgdm stringValue:[self.testObject valueForKey:@"id"]];
    }
}

#pragma mark - Assigned Questions View

- (void)setUpSelectedQuestionsView {
    // Set Protocols
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Allow Selection
    self.tableView.allowsSelection = YES;
    self.tableView.allowsMultipleSelection = NO;
    
    // Table Row Height
    self.tableView.rowHeight = 180.0f;
    
    // Resusable Cell
    UINib *questionItemCellNib = [UINib nibWithNibName:@"TGTBQuestionItemCell" bundle:nil];
    [self.tableView registerNib:questionItemCellNib forCellReuseIdentifier:kQuestionCellIdentifer];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    [self shouldShowEmptyView:(count > 0) ? NO: YES];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kQuestionCellIdentifer forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TGTBQuestionItemCell *questionCell = (TGTBQuestionItemCell *)cell;
    [self configureQuestionCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(TGTBQuestionItemCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    [cell shouldHideCheckbox:YES];
    
    NSString *question_name = [mo valueForKey:@"question_name"];
    NSString *question_type_name = [mo valueForKey:@"question_type_name"];
    NSString *formatted_date_modified = [mo valueForKey:@"formatted_date_modified"];
    
    cell.questionTitleLabel.text = question_name;
    cell.questionTypeLabel.text = question_type_name;
    cell.questionDateLabel.text = [formatted_date_modified uppercaseString];
    
    NSString *package_type_icon = [mo valueForKey:@"question_package_type_icon"];
    [cell.packageTypeImage sd_setImageWithURL:[NSURL URLWithString:package_type_icon]];
    
    NSString *question_text = [mo valueForKey:@"question_text"];
    question_text = [question_text stringByReplacingOccurrencesOfString:@"-blank-" withString:@"_______________"];
    [cell loadWebViewWithContent:question_text];
    
    NSString *question_difficulty_level_id = [mo valueForKey:@"question_difficulty_level_id"];
    [cell updateDifficultyLevelView:question_difficulty_level_id];
    
    NSString *points = [mo valueForKey:@"points"];
    [cell updatePoints:points];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"SAMPLE_QUESTION_VIEW_SEGUE" sender:self];
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tgdm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTestQuestionItemEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSPredicate *predicate1 = [self predicateForKeyPath:@"quiz_id" value:[self.testObject valueForKey:@"id"]];
    NSPredicate *predicate2 = [self predicateForKeyPathContains:@"search_string" value:self.searchBar.text];
    NSCompoundPredicate *cp = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1]];
    
    if (![self.searchBar.text isEqualToString:@""]) {
        cp = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    }
    
    [fetchRequest setPredicate:cp];
    [fetchRequest setSortDescriptors:self.sortDescriptors];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self updateTestViewWithDynamicHeight];
}

#pragma mark - Reloading of Results

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (void)hideBackButtonTitle {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)didViewQuestions {
    [self performSegueWithIdentifier:@"TGTB_QUESTION_SAMPLER_VIEW_2" sender:self];
}

- (void)didPressEdit {
    [self editButtonAction:self.editButton];
}

#pragma mark - Custom Empty View

- (void)shouldShowEmptyView:(BOOL)show {
    if (show) {
        NSString *message = NSLocalizedString(@"No available assigned question yet.", nil);
        
        if (![self.searchBar.text isEqualToString:@""]) {
            message = NSLocalizedString(@"No results found for", nil);
            message =[NSString stringWithFormat:@"%@ \"%@\"", message, self.searchBar.text];
        }
        
        self.emptyMessageLabel.text = message;
    }
    
    self.emptyView.hidden = !show;
    self.tableView.hidden = show;
}

@end
