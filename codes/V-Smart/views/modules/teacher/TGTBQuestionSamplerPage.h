//
//  TGTBQuestionSamplerPage.h
//  V-Smart
//
//  Created by Ryan Migallos on 17/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionSamplerPageHeader.h"
#import <AssetsLibrary/AssetsLibrary.h>

@protocol TGTBQuestionSamplerPageDelegate <NSObject>
@required
- (void)transitionWithData:(NSDictionary *)previewData index:(NSInteger)index segueID:(NSString *)segueIdentifier;
@end

@interface TGTBQuestionSamplerPage : UICollectionViewCell

- (void)displayObject:(NSManagedObject *)object;


@property (nonatomic, weak) id <TGTBQuestionSamplerPageDelegate> delegate;

@property (weak, nonatomic) IBOutlet QuestionSamplerPageHeader *headerView;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *soloEditButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIView *saveEditView;

@end
