//
//  TGFeedBackItem.m
//  V-Smart
//
//  Created by Ryan Migallos on 09/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TGFeedBackItem.h"

@interface TGFeedBackItem() <UITextViewDelegate>

@end

@implementation TGFeedBackItem

- (void)awakeFromNib {
    // Initialization code
    self.textView.text = @"";
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.feedbackPlaceholder.hidden = YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    // verify max length has not been exceeded
    NSString *updated_string = [textView.text stringByReplacingCharactersInRange:range withString:text];
//    NSString *updated_string = [NSString stringWithFormat:@"%@", textView.text];
    if ([(NSObject*)self.delegate respondsToSelector:@selector(updatedText:withIndexPath:)] ) {
        [self.delegate updatedText:updated_string withIndexPath:self.indexObject];
    }
    
    if (updated_string != nil) {
        if (updated_string.length > 1) {
            self.feedbackPlaceholder.hidden = YES;
        }
    }
    
    return YES;
}

@end
