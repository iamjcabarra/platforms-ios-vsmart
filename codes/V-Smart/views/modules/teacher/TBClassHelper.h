//
//  TBClassHelper.h
//  TestBankVersion2.1
//
//  Created by Julius Abarra on 06/02/2016.
//  Copyright © 2016 Vibe Technologies, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TBClassHelper : NSObject

- (NSString *)localizeString:(NSString *)string;
- (NSString *)stringValue:(id)object;
- (void)justifyLabel:(UILabel *)label string:(NSString *)string;
- (CGFloat)getHeightOfLabel:(UILabel*)label;

- (NSString *)getDateTodayWithFormat:(NSString *)format;
- (NSString *)transformDateString:(NSString *)string fromFormat:(NSString *)fromFormat toFormat:(NSString *)toFormat;
- (NSString *)addDayToDate:(NSDate *)date numberOfDays:(int)days formatToReturn:(NSString *)format;
- (NSString *)convertDateToString:(NSDate *)date format:(NSString *)format;
- (NSDate *)convertStringToDate:(NSString *)dateString format:(NSString *)format;
- (double)computeTimeDifferenceInSecondsFromDate:(NSString *)fromDate
                                          toDate:(NSString *)toDate
                                      fromFormat:(NSString *)fromFormat
                                        toFormat:(NSString *)toFormat;
- (double)convertTimeStringToSeconds:(NSString *)timeString;

@end
