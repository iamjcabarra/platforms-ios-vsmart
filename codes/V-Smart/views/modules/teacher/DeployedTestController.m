//
//  DeployedTestController.m
//  V-Smart
//
//  Created by Ryan Migallos on 9/28/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "DeployedTestController.h"
#import "DeployedTestItemCell.h"
#import "DeployedTestInformation.h"
#import "DeployedTestPreview.h"
#import "TestGuruDataManager.h"
#import "ResourceManager.h"
#import "AccountInfo.h"
#import "Utils.h"
#import "VSmartValues.h"
#import "AppDelegate.h"

#define ResultsTableView self.searchResultsTableViewController.tableView

@interface DeployedTestController ()
<NSFetchedResultsControllerDelegate, UISearchResultsUpdating, UISearchControllerDelegate,
UITableViewDataSource, UITableViewDelegate, DeployedTestInformationDelegate> {
    NSManagedObject *mo_selected;
    UITableView *tableView_active;
}

@property (nonatomic, strong) TestGuruDataManager *tm;
@property (nonatomic, strong) ResourceManager *rm;

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) UIRefreshControl *tableRefreshControl;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) UITableViewController *searchResultsTableViewController;
@property (nonatomic, strong) NSArray *results;
@property (nonatomic, assign) BOOL isAscending;

@property (nonatomic, strong) NSString *user_id;

@end

@implementation DeployedTestController

static NSString *kCellIdentifier = @"deploy_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    self.title = NSLocalizedString(@"Tests", nil);
    // Uncomment the following line to preserve selection between presentations.
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.isAscending = NO;
    
    self.tm = [TestGuruDataManager sharedInstance];
    self.rm = [AppDelegate resourceInstance];
    
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    SEL refreshAction = @selector(listAllTest);
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    [self listAllTest];
    [self setupRightBarButton];
    [self setupSearchCapabilities];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupRightBarButton {
    
    UIImage *sortImage = [UIImage imageNamed:@"sort_258x258"];
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sortButton.frame = CGRectMake(0, 0, 44, 44);
    sortButton.showsTouchWhenHighlighted = YES;
    [sortButton setImage:sortImage forState:UIControlStateNormal];
    [sortButton setImage:sortImage forState:UIControlStateHighlighted];
    [sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
}

- (void)setupSearchCapabilities {
    
    self.results = [[NSMutableArray alloc] init];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    UITableViewController *srtvc = [sb instantiateViewControllerWithIdentifier:@"deploytest_search_table_view_controller"];
    srtvc.tableView.dataSource = self;
    srtvc.tableView.delegate = self;
    
    //    UINib *testBankCellNib = [UINib nibWithNibName:@"TestBankCell" bundle:nil];
    //    [srtvc.tableView registerNib:testBankCellNib forCellReuseIdentifier:kCellIdentifier];
    
    self.searchResultsTableViewController = srtvc;
    
    // Init a search controller with its table view controller for results.
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsTableViewController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    
    // Make an appropriate size for search bar and add it as a header view for initial table view.
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // Enable presentation context.
    self.definesPresentationContext = YES;
}

- (void)listAllTest {
    
    __weak typeof(self) wo = self;
    dispatch_queue_t queue = dispatch_queue_create("com.vibetech.deploytest.LIST",DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        [wo.tm requestTestListForUser:self.user_id doneBlock:^(BOOL status) {
            //do nothing
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo.tableRefreshControl endRefreshing];
            });
        }];
    });
}

- (IBAction)refreshTestList:(id)sender {
    
    [self listAllTest];
}

- (void)sortButtonAction:(id)sender {
    
    self.isAscending = (_isAscending) ? NO : YES;
    
    if ([tableView_active isEqual:ResultsTableView]) {
        [self reloadSearchResults];
    }
    
    if (![tableView_active isEqual:ResultsTableView]) {
        [self reloadFetchedResultsController];
    }
}

- (void)reloadSearchResults {
    
    NSArray *items = [NSArray arrayWithArray:self.results];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:self.isAscending];
    NSArray *sorted = [items sortedArrayUsingDescriptors:@[date_modified]];
    
    // Set up results.
    self.results = [NSArray arrayWithArray:sorted];
    
    // Reload search table view.
    [tableView_active reloadData];
}

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = 1;
    
    if ([tableView isEqual:ResultsTableView]) {
        return count;
    } else {
        return [[self.fetchedResultsController sections] count];
    }
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    if ([tableView isEqual:ResultsTableView]) {
        
        if (self.results) {
            return self.results.count;
        } else {
            return count;
        }
        
    } else {
        
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        count = [sectionInfo numberOfObjects];
        
    }
    
    NSString *deployedTestTitle =  NSLocalizedString(@"Deployed Test", nil);
    NSString *countTitle = [NSString stringWithFormat:@"%@ (%ld)", deployedTestTitle, (long)count];
    
    self.title = countTitle;
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView_active = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    if ([tableView isEqual:ResultsTableView]) {
        [self configureFilteredCell:cell indexPath:indexPath];
    }
    
    if (![tableView isEqual:ResultsTableView]) {
        [self configureCell:cell atIndexPath:indexPath];
    }
    
    return cell;
}

- (void)configureFilteredCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = self.results[indexPath.row];
    DeployedTestItemCell *testCell = (DeployedTestItemCell *)cell;
    [self configureCell:testCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    DeployedTestItemCell *testCell = (DeployedTestItemCell *)cell;
    [self configureCell:testCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(DeployedTestItemCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *title = [NSString stringWithFormat:@"%@", [mo valueForKey:@"name"] ];
    NSString *description = [NSString stringWithFormat:@"%@", [mo valueForKey:@"desc"] ];
    
    NSString *testCountLabel = NSLocalizedString(@"Question(s)", nil);
    NSString *count = [NSString stringWithFormat:@"%@ %@", [mo valueForKey:@"item_count"], testCountLabel ];
    
    cell.testName.text = title;
    cell.testDescription.text = description;
    cell.testCount.text = count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    tableView_active = tableView;
    
    NSManagedObject *mo = nil;
    
    if ([tableView_active isEqual:ResultsTableView]) {
        mo = self.results[indexPath.row];
    }
    
    if (![tableView_active isEqual:ResultsTableView]) {
        mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }

    mo_selected = nil;
//    __weak typeof(self) wo = self;
//    [self.tm requestPreviewTest:mo doneBlock:^(BOOL status) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [wo performSegueWithIdentifier:@"show_deploy_test_information" sender:self];
//        });
//    }];
    
    NSInteger questionCount = [[mo valueForKey:@"item_count"] integerValue];
    
    if (questionCount > 0) {
    __weak typeof(self) wo = self;
    [self.tm requestPreviewTest:mo doneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo performSegueWithIdentifier:@"show_deploy_test_information" sender:self];
        });
    }];
    }
    else {
        NSString *title = NSLocalizedString(@"Test Player", nil);
        NSString *message = NSLocalizedString(@"No questions to answer", nil);
        NSString *okayTitle = NSLocalizedString(@"Okay", nil);
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                                 message:message
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:okayTitle
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"show_deploy_test_information"]) {
        DeployedTestInformation *information = (DeployedTestInformation *)[segue destinationViewController];
        information.delegate = self;
        information.quiz_mo = [self getSelectedTest];
    }
    
    if ([segue.identifier isEqualToString:@"preview_deployed_test"]) {
        DeployedTestPreview *preview_test = (DeployedTestPreview *)[segue destinationViewController];
        preview_test.quiz_mo = [self getSelectedTest];
    }
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTestEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    //    NSPredicate *predicate_type = [self predicateForKeyPath:@"questionTypeName" value:@"Multiple Choice"];
    //    NSCompoundPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate_type]];
    //    [fetchRequest setPredicate:predicate];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:self.isAscending];
    [fetchRequest setSortDescriptors:@[date_modified]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Search Results Updating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    UISearchBar *searchBar = searchController.searchBar;
    
    if (searchBar.text.length > 0) {
        
        NSString *text = searchBar.text;
        
        NSArray *fetchedObjects = self.fetchedResultsController.fetchedObjects;
        
        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(NSManagedObject *mo, NSDictionary *bindings) {
            
            NSString *object = [NSString stringWithFormat:@"%@", [mo valueForKey:@"search_string"]];
            NSStringCompareOptions options = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;
            NSRange range = [object rangeOfString:text options:options];
            
            return range.location != NSNotFound;
        }];
        
        // Set up results.
        self.results = [fetchedObjects filteredArrayUsingPredicate:predicate];;
        
        // Reload search table view.
        [self.searchResultsTableViewController.tableView reloadData];
    }
}

#pragma mark - QuizInformation Delegate

- (void)buttonActionType:(NSString *)type {
    
    if ([type isEqualToString:@"start"]) {
        
        mo_selected = [self getSelectedTest];
        if (mo_selected != nil) {
            [self performSegueWithIdentifier:@"preview_deployed_test" sender:self];
        }

    }
}

- (NSManagedObject *)getSelectedTest {
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *quiz_id = [mo valueForKey:@"id"];
    NSPredicate *predicate = [self predicateForKeyPath:@"id" value:quiz_id];
    NSManagedObject *test_object = [self.tm getEntity:kQuizEntity predicate:predicate];

    return test_object;
}

@end
