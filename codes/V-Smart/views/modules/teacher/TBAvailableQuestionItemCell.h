//
//  TBAvailableQuestionItemCell.h
//  V-Smart
//
//  Created by Julius Abarra on 03/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBAvailableQuestionItemCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *selectionView;
@property (strong, nonatomic) IBOutlet UIView *titleView;
@property (strong, nonatomic) IBOutlet UIView *questionView;
@property (strong, nonatomic) IBOutlet UIView *pointView;
@property (strong, nonatomic) IBOutlet UIView *webViewContainer;

@property (strong, nonatomic) IBOutlet UIImageView *selectionImage;
@property (strong, nonatomic) IBOutlet UIImageView *packageTypeImage;

@property (strong, nonatomic) IBOutlet UILabel *questionTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *difficultyLevelLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *pointLabel;

- (void)showSelection:(BOOL)selection;

@end
