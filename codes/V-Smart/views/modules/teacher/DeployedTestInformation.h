//
//  DeployedTestInformation.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/28/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DeployedTestInformationDelegate <NSObject>
@required
- (void)buttonActionType:(NSString *)type;
@end

@interface DeployedTestInformation : UIViewController

@property (nonatomic, strong) NSManagedObject *quiz_mo;
@property (nonatomic, weak) id <DeployedTestInformationDelegate> delegate;

@end
