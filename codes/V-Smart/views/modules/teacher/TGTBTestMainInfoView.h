//
//  TGTBTestMainInfoView.h
//  V-Smart
//
//  Created by Julius Abarra on 07/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGTBTestMainInfoView : UITableViewController

@property (strong, nonatomic) NSManagedObject *testObject;

@end
