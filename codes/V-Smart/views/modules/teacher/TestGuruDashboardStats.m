//
//  TestGuruDashboardStats.m
//  V-Smart
//
//  Created by Ryan Migallos on 21/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruDashboardStats.h"

@implementation TestGuruDashboardStats

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // QUESTION
    self.questionImageView.image = [UIImage imageNamed:@"add_question_white72x72"];
    self.questionTitle.text = NSLocalizedString(@"Questions", nil);
    self.questionCount.text = @"1";
    
    // TESTS
    self.testImageView.image = [UIImage imageNamed:@"test_white72x72"];
    self.testTitle.text =  NSLocalizedString(@"Tests", nil);
    self.testCount.text = @"2";
    
    // DEPLOYED
    self.deployImageView.image = [UIImage imageNamed:@"test_player_white72x72"];
    self.deployTitle.text = NSLocalizedString(@"Deployed Tests", nil);
    self.deployCount.text = @"3";
}

@end
