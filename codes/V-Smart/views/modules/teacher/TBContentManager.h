//
//  TBContentManager.h
//  TestBankVersion2.1
//
//  Created by Julius Abarra on 08/02/2016.
//  Copyright © 2016 Vibe Technologies, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TBContentManager : NSObject

// Test Details Default
#define kTestMinAttempts        @"1"
#define kTestMaxAttempts        @"10"
#define kTestMinTimeLimit       @"00:15:00"
#define kTestMaxTimeLimit       @"24:00:00"
#define kTestHasPassingScore    @"0"
#define kTestHasExpiry          @"1"
#define kTestIsGraded           @"1"
#define kTestMaxTitleChar       @"50"

// Test Options Default
#define kTestShuffleChoices     @"1"
#define kTestShuffleQuestions   @"1"
#define kTestForcedSubmit       @"0"
#define kTestShowScore          @"1"
#define kTestShowResult         @"0"
#define kTestShowResultWCA      @"0"
#define kTestShowFeedbacks      @"0"
#define kTestSetPassword        @"0"
#define kTestMaxPasswordChar    @"25"

// ST Public Properties
@property (strong, nonatomic) NSMutableDictionary *testDetails;
@property (strong, nonatomic) NSMutableDictionary *testOptions;
@property (strong, nonatomic) NSArray *testAssignedQuestions;
@property (strong, nonatomic) NSNumber *actionType;

// ST Dedicated Methods
+ (id)sharedInstance;
- (void)destroyContents;
- (void)recreateObjects;

// ST Recipient Helpers
- (int)updateTestAttempt:(int)currentTestAttempt lastAcceptableTestAttempt:(int)lastTestAttempt;
- (int)updateTestScore:(int)score lastAcceptableTestScore:(int)lastTestScore totalQuestionPoints:(int)points;

@end
