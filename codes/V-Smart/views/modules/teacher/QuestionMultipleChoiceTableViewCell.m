//
//  QuestionMultipleChoiceTableViewCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 7/30/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "QuestionMultipleChoiceTableViewCell.h"
#import "ChoiceHeader.h"
#import "ChoiceItemCell.h"

@interface QuestionMultipleChoiceTableViewCell() <UITableViewDataSource, UITextFieldDelegate, UITableViewDelegate, ChoiceItemDelegate>

@property (strong, nonatomic) IBOutlet UILabel *questionTypeTitle;
@property (strong, nonatomic) IBOutlet UILabel *questionTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionPointLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableViewObject;

@property (strong, nonatomic) ChoiceHeader *choiceHeader;
@property (strong, nonatomic) NSArray *items;

@property (strong, nonatomic) NSManagedObject *question;
@property (strong, nonatomic) NSManagedObject *choiceObjectUpdate;

@end

@implementation QuestionMultipleChoiceTableViewCell

static NSString *kChoiceIdentifier = @"choice_item_identifier";
static NSInteger CHOICE_ITEM_MAX_LIMIT = 8;

- (void)awakeFromNib {
    // Initialization code
    self.questionTypeTitle.text = NSLocalizedString(@"Multiple Choice", nil);
    self.questionTitleLabel.text = NSLocalizedString(@"Title", nil);
    self.questionPointLabel.text = NSLocalizedString(@"Points", nil);
    self.questionLabel.text = NSLocalizedString(@"Question", nil);
    self.questionPointsField.text = @"1.0";
    self.questionPointsField.delegate = self;
    
    self.tableViewObject.dataSource = self;
    self.tableViewObject.delegate = self;
    
    // Choice
    UINib *ChoiceItemCellNib = [UINib nibWithNibName:@"ChoiceItemCell" bundle:nil];
    [self.tableViewObject registerNib:ChoiceItemCellNib forCellReuseIdentifier:kChoiceIdentifier];
    
    // Set action mode
    self.isEditMode = YES;
    self.isCreateAction = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)addChoiceButtonAction:(id)sender {
    NSLog(@"%s ...", __PRETTY_FUNCTION__);
    
    if (self.items.count <= CHOICE_ITEM_MAX_LIMIT) {
        
        NSInteger order = 0;
        NSString *order_number = [NSString stringWithFormat:@"%ld", (long)order];
        NSString *question_id = [NSString stringWithFormat:@"%@", [self.question valueForKey:@"id"] ];

        if (self.items.count > 0) {
            NSManagedObject *last_mo = (NSManagedObject *)[self.items lastObject];
            order_number = [NSString stringWithFormat:@"%@", [last_mo valueForKey:@"order_number"] ];
            order = [order_number integerValue] + 1;
            order_number = [NSString stringWithFormat:@"%ld", (long)order];
        }
        
        NSManagedObjectContext *ctx = self.question.managedObjectContext;
        NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:@"Choice" inManagedObjectContext:ctx];
        [c_mo setValue:[NSNumber numberWithDouble:[order_number doubleValue]] forKey:@"order_number"];
        [c_mo setValue:@"0" forKey:@"is_correct"];
        [c_mo setValue:@"" forKey:@"suggestive_feedback"];
        [c_mo setValue:@"" forKey:@"text"];
        
        [c_mo setValue:@"" forKey:@"date_created"];
        [c_mo setValue:@"" forKey:@"date_modified"];
        [c_mo setValue:@"" forKey:@"id"];
        [c_mo setValue:@"1" forKey:@"is_deleted"];
        [c_mo setValue:question_id forKey:@"question_id"];
        
        self.choiceObjectUpdate = c_mo;

        NSMutableArray *list_items = [NSMutableArray arrayWithArray:self.items];
        [list_items addObject:self.choiceObjectUpdate];
        self.items = [NSArray arrayWithArray:[self sortItems:list_items] ];
        
        [self.tableViewObject reloadData];
        
//        [self debugList:self.items];
    }
}

#pragma mark - Table view delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    CGFloat width = self.tableViewObject.frame.size.width;
    CGRect headerFrame = CGRectMake(0, 0, width, 50);
    self.choiceHeader = [[ChoiceHeader alloc] initWithFrame:headerFrame];
    self.choiceHeader.addChoiceButton.hidden = NO;
    
    SEL addChoiceAction = @selector(addChoiceButtonAction:);
    [self.choiceHeader.addChoiceButton addTarget:self action:addChoiceAction forControlEvents:UIControlEventTouchUpInside];

    // If read-only
    if (!self.isEditMode) {
        self.choiceHeader.addChoiceButton.userInteractionEnabled = NO;
    }

    return self.choiceHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50.0f;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kChoiceIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    // If read-only
    if (!self.isEditMode) {
        cell.userInteractionEnabled = NO;
    }
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)object atIndexPath:(NSIndexPath *)indexPath {
    
    ChoiceItemCell *cell = (ChoiceItemCell *)object;
    
    NSManagedObject *mo = self.items[indexPath.row];
    
    cell.choiceLabel.text = @"";
    
    // Configure Text
    NSString *text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"text"] ];
    cell.choiceLabel.text = text;
    
    // Configure 
    [self configureCheckStateForCell:cell withManagedObject:mo];
    
    SEL checkedAction = @selector(buttonCheckedAction:);
    [cell.checkButton addTarget:self action:checkedAction forControlEvents:UIControlEventTouchUpInside];
    
    SEL eraseAction = @selector(buttonEraseAction:);
    [cell.eraseButton addTarget:self action:eraseAction forControlEvents:UIControlEventTouchUpInside];
    
    cell.delegate = self;
}

- (void)configureCheckStateForCell:(ChoiceItemCell *)cell withManagedObject:(NSManagedObject *) mo {

    cell.checkButton.selected = NO;
    NSString *is_correct = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_correct"] ];
    if (![is_correct isEqualToString:@"0"]) {
        cell.checkButton.selected = YES;
    }
}

- (void)buttonCheckedAction:(UIButton *)sender {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    UITableView *tbv = self.tableViewObject;
    CGPoint point = [sender convertPoint:CGPointZero toView:tbv];
    NSIndexPath *indexPath = [tbv indexPathForRowAtPoint:point];

    NSInteger idx = indexPath.row;
    NSManagedObject *choice = self.items[idx];
    
    ChoiceItemCell *cell = (ChoiceItemCell *)[tbv cellForRowAtIndexPath:indexPath];
//    BOOL status = (cell.checkButton.selected == YES) ? NO : YES;
//    cell.checkButton.selected = status;
//    [self correctValueForStatus:status managedObject:choice];
    
    cell.checkButton.selected = YES;
    [self correctValueForStatus:YES managedObject:choice];
    
    // Deselect unselected choice (previously selected and saved to core data)
    NSInteger nSections = [tbv numberOfSections];
    for (int j = 0; j < nSections; j++) {
        NSInteger nRows = [tbv numberOfRowsInSection:j];
        for (int i = 0; i < nRows; i++) {
            NSIndexPath *unselectedIndexPath = [NSIndexPath indexPathForRow:i inSection:j];
            
            // Check if not selected choice
            if (unselectedIndexPath != indexPath) {
                ChoiceItemCell *unselectedCell = (ChoiceItemCell *)[tbv cellForRowAtIndexPath:unselectedIndexPath];
                unselectedCell.checkButton.selected = NO;
            }
        }
    }
    
// NOTE: Enable if multiple answers are allowed
//    for (NSInteger i = 0; i < self.items.count; i++) {
//        if (i != idx) {
//            NSManagedObject *choice_mo = self.items[i];
//            BOOL mo_status = (status) ? NO : YES;
//            [self correctValueForStatus:mo_status managedObject:choice_mo];
//        }
//    }
    
    for (NSInteger i = 0; i < self.items.count; i++ ) {
        if (i != idx ) {
            NSManagedObject *choice_mo = self.items[i];
            [self correctValueForStatus:NO managedObject:choice_mo];
        }
    }
    
    [tbv reloadData];
    
    // FOR TESTING
    [self debugList:self.items];
}

- (void)correctValueForStatus:(BOOL)status managedObject:(NSManagedObject *)mo {
    
    if (status == YES ) {
        [mo setValue:@"100" forKey:@"is_correct"];
    }
    
    if (status == NO) {
        [mo setValue:@"0" forKey:@"is_correct"];
    }
}

- (void)buttonEraseAction:(UIButton *)sender {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    UITableView *tbv = self.tableViewObject;
    CGPoint point = [sender convertPoint:CGPointZero toView:tbv];
    NSIndexPath *indexPath = [tbv indexPathForRowAtPoint:point];
    
    NSManagedObject *c = self.items[indexPath.row];
    NSMutableArray *choices = [NSMutableArray arrayWithArray:self.items];
    [choices removeObject:c];
    
    self.items = [NSArray arrayWithArray:[self sortItems:choices] ];
    [self.tableViewObject reloadData];
}

- (NSArray *)sortItems:(NSArray *)items {
    NSSortDescriptor *orderDesscriptor = [NSSortDescriptor sortDescriptorWithKey:@"order_number" ascending:YES];
    return [items sortedArrayUsingDescriptors: @[orderDesscriptor] ];
}

- (void)choicesForObject:(NSManagedObject *)mo {
    
    self.question = mo;
    
    NSSet *set = [mo valueForKey:@"choices"];
    NSArray *choices = [set allObjects];
    if (choices.count > 0) {
        self.items = [NSArray arrayWithArray:[self sortItems:choices] ];
        [self.tableViewObject reloadData];
        [self debugList:choices];
    }
}

- (void) updatedChoiceItemTitle:(NSString *)title point:(CGPoint)point {

    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSIndexPath *indexPath = [self.tableViewObject indexPathForRowAtPoint:point];
    
    NSManagedObject *choice = self.items[indexPath.row];
    
    NSString *text = [NSString stringWithFormat:@"%@", title];
    if ([text isEqualToString:@"(null)"] || [text isEqualToString:@"<null>"]  ) {
        text = @"";
    }
    
    [choice setValue:title forKey:@"text"];
    
//    [self debugList:self.items];
}

- (void)debugList:(NSArray *)list {
    
    if (list.count > 0) {
        NSMutableArray *testList = [NSMutableArray arrayWithCapacity:list.count];
        for (NSManagedObject *m in list) {
            NSMutableDictionary *d = [NSMutableDictionary dictionary];
            for (NSString *k in m.entity.propertiesByName.allKeys ) {
                NSString *v = [NSString stringWithFormat:@"%@", [m valueForKey:k] ];
                if (![k isEqualToString:@"question"]) {
                    [d setValue:v forKey:k];
                }
            }
            [testList addObject:d];
        }
//        NSLog(@"test list : %@", testList);
    }
}

- (NSManagedObject *)saveContents:(NSDictionary *)object {
    
    if ( (self.question != nil) && (object != nil) ) {
        
        BOOL save_status = YES;
        
        NSString *name = [self normalizeData: self.questionField.text ];
        NSString *points = [self normalizeData: self.questionPointsField.text ];
        NSString *question_text = [self normalizeData: self.questionTextArea.text ];
        
        if ( (name.length <= 0) || (points.length <= 0) || (question_text.length <= 0)) {
            if (points.length <= 0) {
                self.questionPointsField.text = @"1.0";
            }
            [self showMessage];
            save_status = NO;
            return nil;
        }
        
        if (self.items.count <= 0) {
            [self showMessage];
            save_status = NO;
            return nil;
        }
        
        if (self.items.count > 0) {
            for (NSManagedObject *c in self.items) {
                NSString *text = [self normalizeData: [c valueForKey:@"text"] ];
                if (text.length <= 0) {
                    [self showMessage];
                    save_status = NO;
                    break;
                }
            }
        }
        
        if (save_status) {
            
            NSSet *choices = [NSSet setWithArray:self.items];
            NSMutableDictionary *data = [NSMutableDictionary dictionary];
            for (NSString *key in [object allKeys]) {
                
                if ( ![key isEqualToString:@"tags"] ) {
                    [data setValue:[object objectForKey:key] forKey:key];
                }
                
                if ( [key isEqualToString:@"tags"] ) {
                    NSMutableArray *array = [object objectForKey:key];
                    [data setValue:[self buildTags:array managedObject:self.question] forKey:key];
                }

            }
            data[@"name"] = name;
            data[@"points"] = points;
            data[@"question_text"] = question_text;
            data[@"choices"] = choices;
            
            NSManagedObjectContext *ctx = self.question.managedObjectContext;
            for (NSString *key in [data allKeys] ) {
                [self.question setValue:[data objectForKey:key] forKey:key];
            }
            [self saveTreeContext:ctx];
            
            return self.question;
        }
    }
    
    return nil;
}

- (void)showMessage {
    
    dispatch_async(dispatch_get_main_queue(), ^{
//        NSString *message = NSLocalizedString(@"Please enter a text for new items", nil);
//        NSString *ok = NSLocalizedString(@"OK", nil);
//        NSString *title_alert = NSLocalizedString(@"Save", nil);
//        UIAlertView *av = [[UIAlertView alloc] initWithTitle:title_alert
//                                                     message:message
//                                                    delegate:self
//                                           cancelButtonTitle:ok
//                                           otherButtonTitles:nil];
//        [av show];
        
        // REFACTOR
        // Rephrase message
        NSString *message = NSLocalizedString(@"Please fill in all required fields for question", nil);
        NSString *ok = NSLocalizedString(@"Okay", nil);
        NSString *title_alert = @"";
        
        if (self.isCreateAction) {
            title_alert = NSLocalizedString(@"Save Error", nil);
        }
        else {
            title_alert = NSLocalizedString(@"Update Error", nil);
        }
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:title_alert
                                                     message:message
                                                    delegate:self
                                           cancelButtonTitle:ok
                                           otherButtonTitles:nil];
        [av show];
    });
}

- (void)saveTreeContext:(NSManagedObjectContext *)context {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    if (!context) {
        return;
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"ERROR saving: %@", error);
    }
    
    if (context.parentContext) {
        [self saveTreeContext:context.parentContext];
    }
}

- (NSString *)normalizeData:(NSString *)string {
    
    NSString *text = @"";
    
    if (string.length > 0) {
        
        NSString *value = [NSString stringWithFormat:@"%@", string ];
        text = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    
    return text;
}

- (NSSet *)buildTags:(NSArray *)tags managedObject:(NSManagedObject *)mo {
    return [self insertTags:tags managedObject:mo];
}

- (NSSet *)insertTags:(NSArray *)tags managedObject:(NSManagedObject *)mo {
    
    NSManagedObjectContext *ctx = mo.managedObjectContext;
    NSSet *sets = [NSSet set];
    
    if (tags.count > 0) {
        
        NSMutableSet *tag_sets = [NSMutableSet set];
        
        for (NSDictionary *t in tags) {
            NSString *tag_id = [self normalizeData: t[@"id"] ];
            NSString *tag_name = [self normalizeData: t[@"tag"] ];
            
            NSManagedObject *t_mo = [NSEntityDescription insertNewObjectForEntityForName:@"Tag" inManagedObjectContext:ctx];
            [t_mo setValue:tag_id forKey:@"id"];
            [t_mo setValue:tag_name forKey:@"tag"];
            
            [tag_sets addObject:t_mo];
        }
        
        sets = [NSSet setWithSet:tag_sets];
        
        // assign tags
        [mo setValue:sets forKey:@"tags"];
    }
    
    return sets;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ( textField == self.questionPointsField ) {
        
        // allow backspace
        if (!string.length)
        {
            return YES;
        }
        
        NSCharacterSet *numberSet = [NSCharacterSet decimalDigitCharacterSet];
        if ([string rangeOfCharacterFromSet:[numberSet invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        
        // verify max length has not been exceeded
        NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if (updatedText.length > 3) // 4 was chosen for SSN verification
        {
            // suppress the max length message only when the user is typing
            // easy: pasted data has a length greater than 1; who copy/pastes one character?
            if (string.length > 1)
            {
                // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
            }
            return NO;
        }
        
//        NSArray *sep = [updatedText componentsSeparatedByString:@"."];
//        if([sep count] >= 2) {
//            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
//            return !([sepStr length]>1);
//        }
    }
    return YES;
}

@end
