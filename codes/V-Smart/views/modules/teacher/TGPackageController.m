//
//  TGPackageController.m
//  V-Smart
//
//  Created by Ryan Migallos on 17/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TGPackageController.h"
#import "TGPackageHeaderView.h"
#import "TGPackageCell.h"
#import "TestGuruDataManager.h"
#import "TestGuruSpringboard.h"
#import "TestGuruCourseListView.h"
#import "TestGuruConstants.h"
#import "TestGuruDataManager+Course.h"

@interface TGPackageController () <NSFetchedResultsControllerDelegate, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {
    NSMutableDictionary *_objectChanges;
    NSMutableDictionary *_sectionChanges;
}

@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;


// DOWNLOAD QUEUEING
@property (strong, nonatomic) dispatch_queue_t queue;
@property (strong, nonatomic) NSMutableDictionary *activeDownloads;

@end

@implementation TGPackageController

static NSString * const kCellIdentifier = @"tg_package_cell_identifier";
static NSString * const kReusableViewIdentifier = @"tg_reusable_view_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.queue = dispatch_queue_create("com.vibetech.testguru.PACKAGE",DISPATCH_QUEUE_CONCURRENT);
    self.tm = [TestGuruDataManager sharedInstance];
    [self.tm requestPackageTypes:^(BOOL status) {
        //do nothing
    }];
    
    NSString *titleString = NSLocalizedString(@"Test Guru", nil);
//    NSString *titleString = NSLocalizedString(@"Dashboard", nil);
//    self.title = [titleString uppercaseString];
    self.title = titleString;
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self customizeNavigationController];
    [self executePaginationSettings];
}

- (void)executePaginationSettings {
    [self.tm requestPaginationSettings:nil];
}

- (void)customizeNavigationController {
    
    UIColor *color = UIColorFromHex(0xF69C42);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    } else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
//    /* TRIGERRED BY QUESTION TYPE VIEW CONTROLLER */
//    if ([segue.identifier isEqualToString:@"PACKAGE_TEST_GURU_MODULE"]) {
//        TestGuruSpringboard *springBoard = (TestGuruSpringboard *)[segue destinationViewController];
//        
//        NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] lastObject];
//        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//        springBoard.package_type_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"id"] ];
//        springBoard.package_type_name = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"value"] ];
//    }
//    
//    if ([segue.identifier isEqualToString:@"COURSE_TEST_GURU_MODULE"]) {
//        TestGuruCourseListView *courseListView = (TestGuruCourseListView *)[segue destinationViewController];
//        
//        NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] lastObject];
//        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//        courseListView.package_type_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"id"] ];
//        courseListView.package_type_name = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"value"] ];
//        courseListView.package_type_image_data = [NSData dataWithData: [mo valueForKey:@"image_data"] ];
//    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    TGPackageCell *cell = (TGPackageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    NSString *title = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"value"] ];
    cell.packageName.text = title;

    cell.packageImageView.image = nil;
    
    NSData *image_data = [mo valueForKey:@"image_data"];
    
    if (image_data == nil) {
        NSString *image_url = [mo valueForKey:@"image_url"];
        if ( (image_url != nil) && (image_url.length > 0) ) {
            [self downloadImageForObject:mo indexPath:indexPath];
        }
    }
    
    if (image_data != nil) {
        cell.packageImageView.image = [UIImage imageWithData:image_data];
    }
    
    return cell;
}

- (void)downloadImageForObject:(NSManagedObject *)mo indexPath:(NSIndexPath *)indexPath {
    
    if (self.activeDownloads == nil) {
        self.activeDownloads = [NSMutableDictionary dictionary];
    }
    
    NSManagedObject *mo_in_progress = [self.activeDownloads objectForKey:indexPath];
    if (mo_in_progress == nil) {
        [self.activeDownloads setObject:mo forKey:indexPath];
        __weak typeof(self) wo = self;
        dispatch_async(self.queue, ^{
            NSString *entityName = mo.entity.name;
            [wo.tm downloadImageForEntity:entityName withObject:mo doneBlock:^(BOOL status) {
                if (status) {
                    [wo.activeDownloads removeObjectForKey:indexPath];
                }
            }];
        });
    }
}

#pragma mark <UICollectionViewDelegate>


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{

    UICollectionReusableView *reusableView = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        
        NSString *header = NSLocalizedString(@"Select Assessment Package", nil);
        TGPackageHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                             withReuseIdentifier:kReusableViewIdentifier
                                                                                    forIndexPath:indexPath];
        headerView.headerLabel.text = header;
        
        return headerView;
    }
    
    return reusableView;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *package_type_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"id"] ];
    NSString *package_type_name = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"value"] ];
    NSData *image_url = [NSData dataWithData: [mo valueForKey:@"image_url"] ];
    
    [self.tm saveObject:package_type_id forKey:kTGQB_SELECTED_PACKAGE_ID];
    [self.tm saveObject:package_type_name forKey:kTGQB_SELECTED_PACKAGE_NAME];
    [self.tm saveObject:image_url forKey:kTGQB_SELECTED_PACKAGE_IMAGE_URL];
    
    
    self.collectionView.userInteractionEnabled = NO;
    
//    __weak typeof(self) wo = self;
//    [self.tm requestCourseListForUser:[self.tm loginUser] doneBlock:^(BOOL status) {
//        self.collectionView.userInteractionEnabled = YES;
//        if (status) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [wo performSegueWithIdentifier:@"COURSE_TEST_GURU_MODULE" sender:self];
//            });
//        }
//    }];

    // PAGINATED API CALL
    NSString *limit = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTG_PAGINATION_LIMIT] ];
    NSDictionary *settings = @{@"limit":limit, @"current_page":@"1", @"search_keyword":@"", @"sort_key":@""};
    NSString *userid = [NSString stringWithFormat:@"%@", [self.tm loginUser] ];
    __weak typeof(self) wo = self;
    [self.tm requestPaginatedCourseListForUser:userid withSettingsForPagination:settings dataBlock:^(NSDictionary *data) {
        self.collectionView.userInteractionEnabled = YES;
        if (data != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo performSegueWithIdentifier:@"COURSE_TEST_GURU_MODULE" sender:self];
            });
        }
    }];
    
//    [self performSegueWithIdentifier:@"COURSE_TEST_GURU_MODULE" sender:self];
}

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}

// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}
 
- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

#pragma mark <NSFetchedResultsControllerDelegate>

- (NSFetchedResultsController *)fetchedResultsController
{
    BOOL isAscending = YES;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kPackageTypeEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:isAscending];
    [fetchRequest setSortDescriptors:@[date_modified]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    _objectChanges = [NSMutableDictionary dictionary];
    _sectionChanges = [NSMutableDictionary dictionary];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    if (type == NSFetchedResultsChangeInsert || type == NSFetchedResultsChangeDelete) {
        NSMutableIndexSet *changeSet = _sectionChanges[@(type)];
        if (changeSet != nil) {
            [changeSet addIndex:sectionIndex];
        } else {
            _sectionChanges[@(type)] = [[NSMutableIndexSet alloc] initWithIndex:sectionIndex];
        }
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    NSMutableArray *changeSet = _objectChanges[@(type)];
    if (changeSet == nil) {
        changeSet = [[NSMutableArray alloc] init];
        _objectChanges[@(type)] = changeSet;
    }
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [changeSet addObject:newIndexPath];
            break;
        case NSFetchedResultsChangeDelete:
            [changeSet addObject:indexPath];
            break;
        case NSFetchedResultsChangeUpdate:
            [changeSet addObject:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [changeSet addObject:@[indexPath, newIndexPath]];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    NSMutableArray *moves = _objectChanges[@(NSFetchedResultsChangeMove)];
    if (moves.count > 0) {
        NSMutableArray *updatedMoves = [[NSMutableArray alloc] initWithCapacity:moves.count];
        
        NSMutableIndexSet *insertSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        NSMutableIndexSet *deleteSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        for (NSArray *move in moves) {
            NSIndexPath *fromIP = move[0];
            NSIndexPath *toIP = move[1];
            
            if ([deleteSections containsIndex:fromIP.section]) {
                if (![insertSections containsIndex:toIP.section]) {
                    NSMutableArray *changeSet = _objectChanges[@(NSFetchedResultsChangeInsert)];
                    if (changeSet == nil) {
                        changeSet = [[NSMutableArray alloc] initWithObjects:toIP, nil];
                        _objectChanges[@(NSFetchedResultsChangeInsert)] = changeSet;
                    } else {
                        [changeSet addObject:toIP];
                    }
                }
            } else if ([insertSections containsIndex:toIP.section]) {
                NSMutableArray *changeSet = _objectChanges[@(NSFetchedResultsChangeDelete)];
                if (changeSet == nil) {
                    changeSet = [[NSMutableArray alloc] initWithObjects:fromIP, nil];
                    _objectChanges[@(NSFetchedResultsChangeDelete)] = changeSet;
                } else {
                    [changeSet addObject:fromIP];
                }
            } else {
                [updatedMoves addObject:move];
            }
        }
        
        if (updatedMoves.count > 0) {
            _objectChanges[@(NSFetchedResultsChangeMove)] = updatedMoves;
        } else {
            [_objectChanges removeObjectForKey:@(NSFetchedResultsChangeMove)];
        }
    }
    
    NSMutableArray *deletes = _objectChanges[@(NSFetchedResultsChangeDelete)];
    if (deletes.count > 0) {
        NSMutableIndexSet *deletedSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        [deletes filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSIndexPath *evaluatedObject, NSDictionary *bindings) {
            return ![deletedSections containsIndex:evaluatedObject.section];
        }]];
    }
    
    NSMutableArray *inserts = _objectChanges[@(NSFetchedResultsChangeInsert)];
    if (inserts.count > 0) {
        NSMutableIndexSet *insertedSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        [inserts filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSIndexPath *evaluatedObject, NSDictionary *bindings) {
            return ![insertedSections containsIndex:evaluatedObject.section];
        }]];
    }
    
    UICollectionView *cv = self.collectionView;
    
    [cv performBatchUpdates:^{
        NSIndexSet *deletedSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        if (deletedSections.count > 0) {
            [cv deleteSections:deletedSections];
        }
        
        NSIndexSet *insertedSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        if (insertedSections.count > 0) {
            [cv insertSections:insertedSections];
        }
        
        NSArray *deletedItems = _objectChanges[@(NSFetchedResultsChangeDelete)];
        if (deletedItems.count > 0) {
            [cv deleteItemsAtIndexPaths:deletedItems];
        }
        
        NSArray *insertedItems = _objectChanges[@(NSFetchedResultsChangeInsert)];
        if (insertedItems.count > 0) {
            [cv insertItemsAtIndexPaths:insertedItems];
        }
        
        NSArray *reloadItems = _objectChanges[@(NSFetchedResultsChangeUpdate)];
        if (reloadItems.count > 0) {
            [cv reloadItemsAtIndexPaths:reloadItems];
        }
        
        NSArray *moveItems = _objectChanges[@(NSFetchedResultsChangeMove)];
        for (NSArray *paths in moveItems) {
            [cv moveItemAtIndexPath:paths[0] toIndexPath:paths[1]];
        }
    } completion:nil];
    
    _objectChanges = nil;
    _sectionChanges = nil;
}

#pragma mark <UICollectionViewDelegateFlowLayout>

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 5.0f;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    CGFloat dimension = (collectionView.frame.size.width / 2) - 2;
    return CGSizeMake(dimension, dimension);
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [self.collectionView performBatchUpdates:nil completion:nil];
}

@end
