//
//  QuestionBankController.m
//  V-Smart
//
//  Created by Ryan Migallos on 7/24/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "QuestionBankController.h"
#import "QuestionBankCell.h"
#import "QuestionInfoController.h"
#import "QuestionCreateController.h"
#import "TestGuruDataManager.h"
#import "VSmartValues.h"
#import "AccountInfo.h"
#import "Utils.h"

#define ResultsTableView self.searchResultsTableViewController.tableView

@interface QuestionBankController ()

<NSFetchedResultsControllerDelegate, UISearchResultsUpdating, UISearchControllerDelegate,
UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate> {
    
    NSManagedObject *mo_selected;
    UITableView *tableView_active;
}

@property (nonatomic, strong) TestGuruDataManager *tm;

@property (nonatomic, strong) IBOutlet UIButton *addQuestion;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *tableRefreshControl;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) UITableViewController *searchResultsTableViewController;
@property (nonatomic, strong) NSArray *results;
@property (nonatomic, assign) BOOL isAscending;
@property (nonatomic, assign) BOOL isEditMode;

@property (nonatomic, strong) NSString *user_id;

@end

@implementation QuestionBankController

static NSString *kCellIdentifier = @"question_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    self.title = NSLocalizedString(@"Questions", nil);
    // Uncomment the following line to preserve selection between presentations.
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.isAscending = NO;
    
    self.tm = [TestGuruDataManager sharedInstance];
    self.managedObjectContext = self.tm.mainContext;

    [self loadSelectionTypes];

    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    SEL refreshAction = @selector(listAllQuestion);
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    [self.addQuestion addTarget:self action:@selector(addQuestionAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self listAllQuestion];
    [self setupRightBarButton];
    [self setupSearchCapabilities];
}

- (void)loadSelectionTypes {

    __weak typeof(self) wo = self;
    dispatch_queue_t queue = dispatch_queue_create("com.vibetech.question.TYPES",DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        [wo.tm requestQuestionType:nil];
    });
    dispatch_async(queue, ^{
        [wo.tm requestProficiencyLevel:nil];
    });
    dispatch_async(queue, ^{
        [wo.tm requestLearningSkill:nil];
    });
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addQuestionAction:(id)sender {
//    
//    if ([self.tm clearContentsForEntity:kQuestionEntity predicate:nil]) {
        [self performSegueWithIdentifier:@"create_question_segue" sender:self];
//    }
}

- (void)setupRightBarButton {

    UIImage *sortImage = [UIImage imageNamed:@"sort_258x258"];
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sortButton.frame = CGRectMake(0, 0, 44, 44);
    sortButton.showsTouchWhenHighlighted = YES;
    [sortButton setImage:sortImage forState:UIControlStateNormal];
    [sortButton setImage:sortImage forState:UIControlStateHighlighted];
    [sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
}

- (void)setupSearchCapabilities {
    
    self.results = [[NSMutableArray alloc] init];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    UITableViewController *srtvc = [sb instantiateViewControllerWithIdentifier:@"search_table_view_controller"];
    srtvc.tableView.dataSource = self;
    srtvc.tableView.delegate = self;
    self.searchResultsTableViewController = srtvc;

    // Init a search controller with its table view controller for results.
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsTableViewController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    
    // Make an appropriate size for search bar and add it as a header view for initial table view.
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
//    self.navigationItem.titleView = self.searchController.searchBar;
    
    // Enable presentation context.
    self.definesPresentationContext = YES;
}

- (void)listAllQuestion {
    
    __weak typeof(self) wo = self;
    [self.tm requestQuestionListForUser:self.user_id doneBlock:^(BOOL status) {
        //do nothing
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.tableRefreshControl endRefreshing];
        });
    }];
}

- (IBAction)refreshQuestionList:(id)sender {
    
    [self listAllQuestion];
}

- (void)sortButtonAction:(id)sender {
    
    self.isAscending = (_isAscending) ? NO : YES;
    
    if ([tableView_active isEqual:ResultsTableView]) {
        [self reloadSearchResults];
    }
    
    if (![tableView_active isEqual:ResultsTableView]) {
        [self reloadFetchedResultsController];
    }
}

- (void)reloadSearchResults {
    
    NSArray *items = [NSArray arrayWithArray:self.results];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:self.isAscending];
    NSArray *sorted = [items sortedArrayUsingDescriptors:@[date_modified]];
    
    // Set up results.
    self.results = [NSArray arrayWithArray:sorted];
    
    // Reload search table view.
    [tableView_active reloadData];
}

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (NSManagedObject *)managedObjectFromButtonAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = 1;
    
    if ([tableView isEqual:ResultsTableView]) {
        return count;
    } else {
        return [[self.fetchedResultsController sections] count];
    }
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    if ([tableView isEqual:ResultsTableView]) {
        
        if (self.results) {
            return self.results.count;
        } else {
            return count;
        }
        
    } else {
        
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        count = [sectionInfo numberOfObjects];
        
    }

    NSString *questionTitle =  @"Question";
    NSString *countTitle = [NSString stringWithFormat:@"%@ (%ld)", questionTitle, (long)count];
    
    self.title = countTitle;
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView_active = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    if ([tableView isEqual:ResultsTableView]) {
        [self configureFilteredCell:cell indexPath:indexPath];
    }
    
    if (![tableView isEqual:ResultsTableView]) {
        [self configureCell:cell atIndexPath:indexPath];
    }
    
    return cell;
}

- (void)configureFilteredCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = self.results[indexPath.row];
    QuestionBankCell *questionCell = (QuestionBankCell *)cell;
    [self configureCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    QuestionBankCell *questionCell = (QuestionBankCell *)cell;
    [self configureCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(QuestionBankCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    NSString *title = [NSString stringWithFormat:@"%@", [mo valueForKey:@"name"] ];
    NSString *type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"questionTypeName"] ];
    NSString *uploader_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ];
    
    cell.questionName.text = title;
    cell.questionType.text = type;
    
    cell.deleteButton.hidden = NO;
    cell.editButton.hidden = NO;
    
    if (![uploader_id isEqualToString:self.user_id]) {
        cell.deleteButton.hidden = YES;
        cell.editButton.hidden = YES;
    }
    
    SEL deleteSelector = @selector(deleteButtonAction:);
    [cell.deleteButton addTarget:self action:deleteSelector forControlEvents:UIControlEventTouchUpInside];
    
    SEL editSelector = @selector(editButtonAction:);
    [cell.editButton addTarget:self action:editSelector forControlEvents:UIControlEventTouchUpInside];
}

- (void)deleteButtonAction:(id)sender {
    
    mo_selected = [self managedObjectFromButtonAction:sender];
    NSString *question_title = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"name"]];
    NSString *message_string = NSLocalizedString(@"Are you sure you want to delete", nil);
    NSString *message = [NSString stringWithFormat:@"%@ %@?", message_string, question_title];
    NSString *yesTitle = NSLocalizedString(@"Yes", nil);
    NSString *noTitle = NSLocalizedString(@"No", nil);
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Delete" message:message delegate:self cancelButtonTitle:noTitle otherButtonTitles:yesTitle,nil];
    [av show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        //TODO: perform delete operation
        [self.tm requestRemoveQuestion:mo_selected doneBlock:^(BOOL status) {
            
        }];
    }
}

- (void)editButtonAction:(id)sender {
    
    self.isEditMode = YES;

    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [tableView_active indexPathForRowAtPoint:buttonPosition];

    NSManagedObject *mo = nil;
    
    if ([tableView_active isEqual:ResultsTableView]) {
        NSLog(@"Search TableView");
        mo = self.results[indexPath.row];
    }
    
    if (![tableView_active isEqual:ResultsTableView]) {
        NSLog(@"List TableView");
        mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    mo_selected = mo;
    
    __weak typeof(self) wo = self;
    [self.tm requestDetailsForQuestion:mo_selected doneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo performSegueWithIdentifier:@"selected_question_info" sender:nil];
            [tableView_active deselectRowAtIndexPath:indexPath animated:YES];
        });
    }];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.isEditMode = NO;
    
    NSManagedObject *mo = nil;
    
    if ([tableView isEqual:ResultsTableView]) {
        mo = self.results[indexPath.row];
    }
    
    if (![tableView isEqual:ResultsTableView]) {
        mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    mo_selected = mo;
    tableView_active = tableView;
    
    __weak typeof(self) wo = self;
    [self.tm requestDetailsForQuestion:mo_selected doneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo performSegueWithIdentifier:@"selected_question_info" sender:nil];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        });
    }];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    if ([segue.identifier isEqualToString:@"selected_question_info"]) {
        QuestionInfoController *info = (QuestionInfoController *)[segue destinationViewController];
        info.question = mo_selected;
        info.isEditMode = self.isEditMode;
    }

    if ([segue.identifier isEqualToString:@"create_question_segue"]) {
        QuestionCreateController *create = (QuestionCreateController *)[segue destinationViewController];
        create.user_id = self.user_id;
    }
    
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionBankEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
//    NSPredicate *predicate_type = [self predicateForKeyPath:@"questionTypeName" value:@"Multiple Choice"];
//    NSCompoundPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate_type]];
//    [fetchRequest setPredicate:predicate];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:self.isAscending];
    [fetchRequest setSortDescriptors:@[date_modified]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Search Results Updating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    UISearchBar *searchBar = searchController.searchBar;
    
    if (searchBar.text.length > 0) {
        
        NSString *text = searchBar.text;
        
        NSArray *fetchedObjects = self.fetchedResultsController.fetchedObjects;
        
        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(NSManagedObject *mo, NSDictionary *bindings) {
            
            NSString *object = [NSString stringWithFormat:@"%@", [mo valueForKey:@"search_string"]];
            NSStringCompareOptions options = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;
            NSRange range = [object rangeOfString:text options:options];
            
            return range.location != NSNotFound;
        }];
        
        // Set up results.
        self.results = [fetchedObjects filteredArrayUsingPredicate:predicate];;
        
        // Reload search table view.
        [self.searchResultsTableViewController.tableView reloadData];
    }
}


@end
