//
//  TGQBQuestionContentHeaderView.m
//  V-Smart
//
//  Created by Ryan Migallos on 22/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGQBQuestionContentHeaderView.h"
#import "TestGuruConstants.h"

@implementation TGQBQuestionContentHeaderView

-  (instancetype)initWithFrame:(CGRect)rect {
    
    self = [super initWithFrame:rect];
    
    if (self) {
        [self applyShadowToView:self.headerTitleLabel];
        [self applyShadowToView:self.headerButtton];
        
        NSString *nibName = @"TGQBQuestionContentHeaderView";
        NSBundle *mainBundle = [NSBundle mainBundle];
        NSArray *nib = [mainBundle loadNibNamed:nibName owner:self options:nil];
        [nib[0] setFrame:rect];
        self = nib[0];
    }
    
    return self;
}

- (void)applyShadowToView:(UIView *)object {
    
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.5f;
}

@end
