//
//  TestBankSortHeader.h
//  V-Smart
//
//  Created by Julius Abarra on 11/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TestBankSortHeaderDelegate <NSObject>

@required
- (void)sortDidSelectButtonAction:(NSString *)actionType sortType:(NSManagedObject *)sortTypeObject isOldest:(BOOL)isOldest;

@optional
- (void)sortOptionCount:(NSUInteger)count;

@end

@interface TestBankSortHeader : UIViewController

@property (weak, nonatomic) id <TestBankSortHeaderDelegate> delegate;

@end