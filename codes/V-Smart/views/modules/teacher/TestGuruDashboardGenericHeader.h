//
//  TestGuruDashboardGenericHeader.h
//  V-Smart
//
//  Created by Ryan Migallos on 20/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruDashboardGenericHeader : UICollectionReusableView

@property (strong, nonatomic) IBOutlet UILabel *headerLabel;

@end
