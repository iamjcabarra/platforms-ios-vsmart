//
//  TGQBAnswersTableViewCell.m
//  V-Smart
//
//  Created by Julius Abarra on 04/10/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGQBAnswersTableViewCell.h"
#import "TGTokenTextCollectionCell.h"
#import "TestGuruDataManager.h"

@interface TGQBAnswersTableViewCell() <NSFetchedResultsControllerDelegate, UITextFieldDelegate, UICollectionViewDelegateFlowLayout> {
    NSMutableDictionary *_objectChanges;
    NSMutableDictionary *_sectionChanges;
}

@property (strong, nonatomic) IBOutlet UILabel *answerLabel;
@property (strong, nonatomic) IBOutlet UIImageView *checkImage;
@property (strong, nonatomic) IBOutlet UILabel *caseSensitiveLabel;
@property (strong, nonatomic) IBOutlet UIButton *caseSensitiveButton;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UITextField *answerTextField;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;

@property (strong, nonatomic) TestGuruDataManager *tgdm;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObject *mo;
@property (strong, nonatomic) NSString *questionID;

@property (assign, nonatomic) BOOL editCollection;
@property (strong, nonatomic) NSString *editedAnswer;

@end

static NSString *kTextTokenCellIdentifier = @"tg_text_token_identifier";

@implementation TGQBAnswersTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.tgdm = [TestGuruDataManager sharedInstance];
    
    NSString *answerText = NSLocalizedString(@"Answers", "");
    self.answerLabel.text = [answerText uppercaseString];
    
    NSString *caseSensitiveText = NSLocalizedString(@"Case Sensitive", "");
    self.caseSensitiveLabel.text = caseSensitiveText;
    
    self.editCollection = NO;
    self.answerTextField.delegate = self;
    
    [self.caseSensitiveButton addTarget:self action:@selector(toggleCaseSensitive:) forControlEvents:UIControlEventTouchUpInside];
    
//    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
//    layout.estimatedItemSize = CGSizeMake(50, 28);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(170, 44);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TGTokenTextCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kTextTokenCellIdentifier forIndexPath:indexPath];
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *answer = [NSString stringWithFormat:@"%@", [mo valueForKey:@"answer"]];
    cell.tokenText.text = answer;
    [cell.tokenDeleteButton addTarget:self action:@selector(deleteAnswerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    self.answerTextField.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"answer"]];
    self.editedAnswer = [NSString stringWithFormat:@"%@", [mo valueForKey:@"answer"]];
    self.editCollection = YES;
}

- (NSManagedObject *)managedObjectFromButtonAction:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

#pragma mark <NSFetchedResultsControllerDelegate>

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *context = self.tgdm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kSplittedAnswerEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSPredicate *predicate = [self.tgdm predicateForKeyPath:@"question_id" andValue:self.questionID];
    [fetchRequest setPredicate:predicate];
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"time_stamp" ascending:YES];
    [fetchRequest setSortDescriptors:@[descriptor]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:context
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    _objectChanges = [NSMutableDictionary dictionary];
    _sectionChanges = [NSMutableDictionary dictionary];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    if (type == NSFetchedResultsChangeInsert || type == NSFetchedResultsChangeDelete) {
        NSMutableIndexSet *changeSet = _sectionChanges[@(type)];
        if (changeSet != nil) {
            [changeSet addIndex:sectionIndex];
        } else {
            _sectionChanges[@(type)] = [[NSMutableIndexSet alloc] initWithIndex:sectionIndex];
        }
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    NSMutableArray *changeSet = _objectChanges[@(type)];
    if (changeSet == nil) {
        changeSet = [[NSMutableArray alloc] init];
        _objectChanges[@(type)] = changeSet;
    }
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [changeSet addObject:newIndexPath];
            break;
        case NSFetchedResultsChangeDelete:
            [changeSet addObject:indexPath];
            break;
        case NSFetchedResultsChangeUpdate:
            [changeSet addObject:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [changeSet addObject:@[indexPath, newIndexPath]];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    NSMutableArray *moves = _objectChanges[@(NSFetchedResultsChangeMove)];
    if (moves.count > 0) {
        NSMutableArray *updatedMoves = [[NSMutableArray alloc] initWithCapacity:moves.count];
        
        NSMutableIndexSet *insertSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        NSMutableIndexSet *deleteSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        for (NSArray *move in moves) {
            NSIndexPath *fromIP = move[0];
            NSIndexPath *toIP = move[1];
            
            if ([deleteSections containsIndex:fromIP.section]) {
                if (![insertSections containsIndex:toIP.section]) {
                    NSMutableArray *changeSet = _objectChanges[@(NSFetchedResultsChangeInsert)];
                    if (changeSet == nil) {
                        changeSet = [[NSMutableArray alloc] initWithObjects:toIP, nil];
                        _objectChanges[@(NSFetchedResultsChangeInsert)] = changeSet;
                    } else {
                        [changeSet addObject:toIP];
                    }
                }
            } else if ([insertSections containsIndex:toIP.section]) {
                NSMutableArray *changeSet = _objectChanges[@(NSFetchedResultsChangeDelete)];
                if (changeSet == nil) {
                    changeSet = [[NSMutableArray alloc] initWithObjects:fromIP, nil];
                    _objectChanges[@(NSFetchedResultsChangeDelete)] = changeSet;
                } else {
                    [changeSet addObject:fromIP];
                }
            } else {
                [updatedMoves addObject:move];
            }
        }
        
        if (updatedMoves.count > 0) {
            _objectChanges[@(NSFetchedResultsChangeMove)] = updatedMoves;
        } else {
            [_objectChanges removeObjectForKey:@(NSFetchedResultsChangeMove)];
        }
    }
    
    NSMutableArray *deletes = _objectChanges[@(NSFetchedResultsChangeDelete)];
    if (deletes.count > 0) {
        NSMutableIndexSet *deletedSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        [deletes filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSIndexPath *evaluatedObject, NSDictionary *bindings) {
            return ![deletedSections containsIndex:evaluatedObject.section];
        }]];
    }
    
    NSMutableArray *inserts = _objectChanges[@(NSFetchedResultsChangeInsert)];
    if (inserts.count > 0) {
        NSMutableIndexSet *insertedSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        [inserts filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSIndexPath *evaluatedObject, NSDictionary *bindings) {
            return ![insertedSections containsIndex:evaluatedObject.section];
        }]];
    }
    
    UICollectionView *cv = self.collectionView;
    
    [cv performBatchUpdates:^{
        NSIndexSet *deletedSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        if (deletedSections.count > 0) {
            [cv deleteSections:deletedSections];
        }
        
        NSIndexSet *insertedSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        if (insertedSections.count > 0) {
            [cv insertSections:insertedSections];
        }
        
        NSArray *deletedItems = _objectChanges[@(NSFetchedResultsChangeDelete)];
        if (deletedItems.count > 0) {
            [cv deleteItemsAtIndexPaths:deletedItems];
        }
        
        NSArray *insertedItems = _objectChanges[@(NSFetchedResultsChangeInsert)];
        if (insertedItems.count > 0) {
            [cv insertItemsAtIndexPaths:insertedItems];
        }
        
        NSArray *reloadItems = _objectChanges[@(NSFetchedResultsChangeUpdate)];
        if (reloadItems.count > 0) {
            [cv reloadItemsAtIndexPaths:reloadItems];
        }
        
        NSArray *moveItems = _objectChanges[@(NSFetchedResultsChangeMove)];
        for (NSArray *paths in moveItems) {
            [cv moveItemAtIndexPath:paths[0] toIndexPath:paths[1]];
        }
    } completion:nil];
    
    _objectChanges = nil;
    _sectionChanges = nil;
}

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.collectionView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.returnKeyType == UIReturnKeyDone) {
        NSString *updatedText = [NSString stringWithFormat:@"%@", textField.text];
        self.editCollection ? [self updateAnswer:updatedText] : [self insertNewAnswer:updatedText];
        self.answerTextField.text = @"";
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSString *updatedText = [NSString stringWithFormat:@"%@", textField.text];
    self.editCollection ? [self updateAnswer:updatedText] : [self insertNewAnswer:updatedText];
    self.answerTextField.text = @"";
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString *originalText = [NSString stringWithFormat:@"%@", textField.text];
    
    if (!string.length) {
        return YES;
    }
    
    NSString *expression = @",";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:updatedText
                                                        options:0
                                                          range:NSMakeRange(0, [updatedText length])];
    if (numberOfMatches == 1) {
        self.editCollection ? [self updateAnswer:updatedText] : [self insertNewAnswer:originalText];
        self.answerTextField.text = @"";
        return NO;
    }
    
    if (updatedText.length > 50) {
        return NO;
    }
    
    return YES;
}

#pragma mark - Public Methods

- (void)setObjectData:(NSManagedObject *)object {
    if (object != nil) {
        self.mo = object;
        self.questionID = [NSString stringWithFormat:@"%@", [object valueForKey:@"id"]];
        
        NSString *is_case_sensitive = [self.tgdm stringValue:[self.mo valueForKey:@"is_case_sensitive"]];
        UIImage *image = [is_case_sensitive isEqualToString:@"1"] ? [UIImage imageNamed:@"checkbox48x48.png"] : [UIImage imageNamed:@"checkbox_unable48x48.png"];
        self.checkImage.image = image;
        
        self.answerTextField.text = @"";
    }
}

- (void)displayAnswers:(NSSet *)answerSet {
    if (answerSet.count > 0) {
        NSArray *answerList = [NSArray arrayWithArray:[answerSet allObjects]];
        if (answerList.count > 0) {
            [self.tgdm answerBatchInsert:answerList doneBlock:^(BOOL flag) {}];
        }
    }
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wo reloadFetchedResultsController];
    });
}

#pragma mark - Private Methods

- (void)insertNewAnswer:(NSString *)answer {
    answer = [answer stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    [self.tgdm insertNewAnswer:answer withQuestionID:self.questionID doneBlock:^(BOOL status) {
        if (status) {
            NSString *concatenatedAnswers = [self getConcatenatedAnswers];
            NSDictionary *data = @{@"text": concatenatedAnswers};
            [self updateDetailsWithData:data];
        }
    }];
}

- (void)updateAnswer:(NSString *)answer {
    answer = [answer stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    [self.tgdm updateAnswer:self.editedAnswer withNewAnswer:answer withQuestionID:self.questionID doneBlock:^(BOOL status) {
        if (status) {
            self.editCollection = NO;
            
            NSString *concatenatedAnswers = [self getConcatenatedAnswers];
            NSDictionary *data = @{@"text": concatenatedAnswers};
            [self updateDetailsWithData:data];
            
            __weak typeof(self) wo = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSArray *index = [wo.collectionView indexPathsForVisibleItems];
                [wo.collectionView reloadItemsAtIndexPaths:index];
            });
        }
    }];
}

- (void)deleteAnswerAction:(id)sender {
    NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
    NSString *answer = [NSString stringWithFormat:@"%@", [mo valueForKey:@"answer"]];
    
    [self.tgdm removeAnswer:answer withQuestionID:self.questionID doneBlock:^(BOOL status) {
        if (status) {
            self.editCollection = NO;
            NSString *concatenatedAnswers = [self getConcatenatedAnswers];
            NSDictionary *data = @{@"text": concatenatedAnswers};
            [self updateDetailsWithData:data];
        }
    }];
}

- (void)toggleCaseSensitive:(id)sender {
    if (self.mo != nil) {
        NSString *is_case_sensitive = [self.tgdm stringValue:[self.mo valueForKey:@"is_case_sensitive"]];
        UIImage *image = [is_case_sensitive isEqualToString:@"1"] ? [UIImage imageNamed:@"checkbox_unable48x48.png"] : [UIImage imageNamed:@"checkbox48x48.png"];
        self.checkImage.image = image;
        
        NSString *new_value = [is_case_sensitive isEqualToString:@"1"] ? @"0" : @"1";
        NSDictionary *data = @{@"is_case_sensitive": new_value};
        NSPredicate *predicate = [self.tgdm predicateForKeyPath:@"id" andValue:self.questionID];
        [self.tgdm updateQuestion:kQuestionEntity predicate:predicate details:data];
    }
}

- (NSString *)getConcatenatedAnswers {
    NSArray *answers = [self.fetchedResultsController fetchedObjects];
    return [self.tgdm concatenateAnswers:answers];
}

- (void)updateDetailsWithData:(NSDictionary *)data {
    [self.tgdm updateChoiceItemWithQuestionID:self.questionID andData:data];
}

@end
