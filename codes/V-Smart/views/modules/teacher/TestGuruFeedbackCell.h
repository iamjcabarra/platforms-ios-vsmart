//
//  TestGuruFeedbackCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 10/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruFeedbackCell : UITableViewCell

- (void)displayData:(NSDictionary *)data;

@end
