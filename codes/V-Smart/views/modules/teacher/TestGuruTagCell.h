//
//  TestGuruTagCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 12/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruTagCell : UITableViewCell

- (void)displayData:(NSDictionary *)data;

@end
