//
//  TGPackageCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 17/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TGPackageCell.h"
#import <QuartzCore/QuartzCore.h>

@interface TGPackageCell()

@property (strong, nonatomic) IBOutlet UIView *containerView;
//@property (strong, nonatomic) IBOutlet UILabel *packageName;
//@property (strong, nonatomic) IBOutlet UIImageView *packageImageView;

@end

@implementation TGPackageCell


- (void)awakeFromNib {
    // Initialization code
    
//    CALayer *viewLayer = self.containerView.layer;
//    
//    //ROUND CORNER
//    viewLayer.cornerRadius = 5.0f;
//    
//    //BORDER
//    viewLayer.borderColor = [UIColor lightGrayColor].CGColor;
//    viewLayer.borderWidth = 1.0f;
    
//    //SHADOW
//    viewLayer.shadowColor = [UIColor purpleColor].CGColor;
//    viewLayer.shadowOpacity = 1;
//    viewLayer.shadowRadius = 1.0f;
//    viewLayer.shadowOffset = CGSizeMake(2.0,2.0);
    
    CALayer *imageLayer = self.packageImageView.layer;
    imageLayer.shadowColor = [UIColor lightGrayColor].CGColor;
    imageLayer.shadowOffset = CGSizeMake(2.0,2.0);
    imageLayer.shadowRadius = 2.0f;
    imageLayer.shadowOpacity = 1.0f;
}

@end
