//
//  TestGuruListView.h
//  V-Smart
//
//  Created by Ryan Migallos on 23/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruListView : UIViewController

//@property (strong, nonatomic) NSString *package_type_id;
//@property (strong, nonatomic) NSString *package_type_name;
//@property (strong, nonatomic) NSData *package_type_image_data;

//@property (strong, nonatomic) NSString *course_id;
//@property (strong, nonatomic) NSString *course_name;

@property (assign, nonatomic) BOOL isGroupBy;
@property (strong, nonatomic) NSString *linkType;
@property (strong, nonatomic) NSString *groupByType;
@property (strong, nonatomic) NSManagedObject *headerObject;


@end
