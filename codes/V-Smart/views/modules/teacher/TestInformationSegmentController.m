//
//  TestInformationSegmentController.m
//  V-Smart
//
//  Created by Julius Abarra on 14/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestInformationSegmentController.h"
#import "TestMainInfoTableView.h"
#import "TestSettingsTableView.h"

#define kSegueIdentifierMainInfo @"embedTestMainInfoSegment"
#define kSegueIdentifierSettings @"embedTestSettingsSegment"

@interface TestInformationSegmentController ()

@property (strong, nonatomic) TestMainInfoTableView *mainInfo;
@property (strong, nonatomic) TestSettingsTableView *settings;
@property (strong, nonatomic) NSString *currentSegueIdentifier;

@end

@implementation TestInformationSegmentController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set current segue identifier and perform initial segue using this identifier
    self.currentSegueIdentifier = kSegueIdentifierMainInfo;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kSegueIdentifierMainInfo]) {
        self.mainInfo = [segue destinationViewController];
        
        if (self.childViewControllers.count > 0) {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
        }
        else {
            [self addChildViewController:segue.destinationViewController];
            ((UIViewController *)segue.destinationViewController).view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:((UIViewController *)segue.destinationViewController).view];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:kSegueIdentifierSettings]) {
        self.settings = [segue destinationViewController];
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController {
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
    }];
}

- (void)swapViewControllers:(NSString *)segueIdentifier {
    [self performSegueWithIdentifier:segueIdentifier sender:nil];
}

@end
