//
//  TBDetailViewSegmentController.m
//  TestBankVersion2.1
//
//  Created by Julius Abarra on 06/02/2016.
//  Copyright © 2016 Vibe Technologies, Inc. All rights reserved.
//

#import "TBDetailViewSegmentController.h"
#import "TBTestDetailsSegmentView.h"
#import "TBTestOptionsSegmentView.h"

#define kSegueIdentifierTestDetails @"embedTestDetails"
#define kSegueIdentifierTestOptions @"embedTestOptions"

@interface TBDetailViewSegmentController ()

@property (strong, nonatomic) TBTestDetailsSegmentView *details;
@property (strong, nonatomic) TBTestOptionsSegmentView *options;

@property (strong, nonatomic) NSString *currentSegueIdentifier;

@end

@implementation TBDetailViewSegmentController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Custom View
    self.view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.view.layer.borderWidth = 1.0f;
    
    // Set current segue identifier and perform initial segue using this identifier
    self.currentSegueIdentifier = kSegueIdentifierTestDetails;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kSegueIdentifierTestDetails]) {
        self.details = [segue destinationViewController];
        
        if (self.childViewControllers.count > 0) {
            [self swapFromView:[self.childViewControllers objectAtIndex:0] toView:segue.destinationViewController];
        }
        else {
            [self addChildViewController:segue.destinationViewController];
            ((UIViewController *)segue.destinationViewController).view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:((UIViewController *)segue.destinationViewController).view];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:kSegueIdentifierTestOptions]) {
        self.options = [segue destinationViewController];
        [self swapFromView:[self.childViewControllers objectAtIndex:0] toView:segue.destinationViewController];
    }
}

- (void)swapFromView:(UIViewController *)fromView toView:(UIViewController *)toView {
    toView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    UIViewAnimationOptions transition = UIViewAnimationOptionTransitionNone;
    
    if ([toView isEqual:self.details]) {
        transition = UIViewAnimationOptionTransitionFlipFromRight;
    }
    
    if ([toView isEqual:self.options]) {
        transition = UIViewAnimationOptionTransitionFlipFromLeft;
    }
    
    [fromView willMoveToParentViewController:nil];
    [self addChildViewController:toView];
    [self transitionFromViewController:fromView
                      toViewController:toView
                              duration:0.5
                               options:transition
                            animations:nil
                            completion:^(BOOL finished) {
                                [fromView removeFromParentViewController];
                                [toView didMoveToParentViewController:self];
                            }];
}

- (void)swapViews:(NSString *)segueIdentifier {
    [self performSegueWithIdentifier:segueIdentifier sender:nil];
}

@end
