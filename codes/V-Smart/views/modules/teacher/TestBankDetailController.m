//
//  TestBankDetailController.m
//  V-Smart
//
//  Created by Ryan Migallos on 9/7/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "TestBankDetailController.h"
#import "TestGuruDataManager.h"
#import "QuizHeader.h"
#import "TestBankQuestionList.h"
#import "HUD.h"


@interface TestBankDetailController () <NSFetchedResultsControllerDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet QuizHeader *quizHeader;

@property (nonatomic, strong) TestGuruDataManager *tm;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, assign) int actionFlag;

@end

@implementation TestBankDetailController

static NSString *kTestCellIdentifier = @"test_question_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tm = [TestGuruDataManager sharedInstance];
    
    [self setupRightBarButton];
    
    if (self.editMode == YES) {
        [self displayContents];
    }
    
    if (self.editMode == NO) {
        [self displayDefaults];
    }
    
    [self.quizHeader.buttonAssignQuestion addTarget:self action:@selector(showQuestionList:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)showQuestionList:(UIButton *)button {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    __weak typeof(self) wo = self;
//    [self.tm requestQuestionListForUser:self.user_id enableChoice:NO doneBlock:^(BOOL status) {
//        //do nothing
//        if (status == YES) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [wo performSegueWithIdentifier:@"show_testbank_question_list" sender:self];
//            });
//        }
//   }];

     dispatch_async(dispatch_get_main_queue(), ^{
          [wo performSegueWithIdentifier:@"show_testbank_question_list" sender:self];
     });
}

- (void)displayDefaults {
    [self.quizHeader displayDefaultValues];
}

- (void)displayContents {
    [self.quizHeader displayData:self.quiz_mo];
}

- (NSString *)quizValueForKey:(NSString *)key {
    
    NSString *string = [NSString stringWithFormat:@"%@", [self.quiz_mo valueForKey:key] ];
    return string;
}

- (void)setupRightBarButton {
    
    NSString *titleLabel = (self.editMode) ? NSLocalizedString(@"Update", nil) : NSLocalizedString(@"Save", nil);
    SEL saveAction = (self.editMode) ? @selector(updateButtonAction:) : @selector(saveButtonAction:);
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithTitle:titleLabel
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:saveAction];
    self.navigationItem.rightBarButtonItem = saveButtonItem;
}

- (void)saveButtonAction:(id)sender {
    // ENHANCEMENT#279:
    [self showConfirmationMessage:1];
    // [self postTestObject:YES];
}

- (void)updateButtonAction:(id)sender {
    // ENHANCEMENT#279:
    [self showConfirmationMessage:2];
    // [self postTestObject:NO];
}

// ENHANCEMENT#279:
// Confirm the user if he/she really wants to update selected test or save a new test
- (void)showConfirmationMessage:(int)actionType {
    self.actionFlag = actionType;
    
    NSString *title = @"";
    NSString *message = @"";
    NSString *yTitle = NSLocalizedString(@"Yes", nil);
    NSString *nTitle = NSLocalizedString(@"No", nil);
    
    switch (actionType) {
        case 1:
            title = NSLocalizedString(@"Save Test", nil);
            message = NSLocalizedString(@"Do you really want to save this test?", nil);
            break;
        case 2:
            title = NSLocalizedString(@"Update Test", nil);
            message = NSLocalizedString(@"Do you really want to update this test?", nil);
            break;
        default:
            break;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:nTitle
                                          otherButtonTitles:yTitle, nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // If yes
    if (buttonIndex == 1) {
        // Save
        if (self.actionFlag == 1) {
            [self postTestObject:YES];
        }
        // Update
        if (self.actionFlag == 2) {
            [self postTestObject:NO];
        }
    }
}

- (void)postTestObject:(BOOL)newitem {

    [self.view endEditing:YES];
    
    NSMutableDictionary *headerData = [self.quizHeader getHeaderValues];
    [headerData setValue:_user_id forKey:@"user_id"];
    
    NSString *name_text = headerData[@"name"];
    NSString *description_text = headerData[@"description"];
    
    NSString *alertTitle = NSLocalizedString(@"Update Error", nil);
    NSString *indicatorString = NSLocalizedString(@"Updating...", nil);
    BOOL areRequiredFieldsEmpty = (name_text.length == 0) || (description_text.length == 0) ? YES: NO;
    
    NSSet *question_set = [self.quiz_mo valueForKey:@"questions"];
    NSArray *question_list = [question_set allObjects];
    
    if (newitem) {
        [headerData setValue:[self getTotalFromObject:self.quiz_mo] forKey:@"total_score"];
        alertTitle = NSLocalizedString(@"Save Error", nil);
        indicatorString = NSLocalizedString(@"Saving...", nil);
    }
    
//    NSString *indicatorString = NSLocalizedString(@"Updating...", nil);
    
//    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    if (question_list.count > 0 && !areRequiredFieldsEmpty) {
        [HUD showUIBlockingIndicatorWithText:indicatorString];
        __weak typeof(self) wo = self;
        
        [self.tm updateTestObject:self.quiz_mo data:headerData newItem:newitem doneBlock:^(BOOL status) {
            if (status) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                            [HUD hideUIBlockingIndicator];
                        
                        // ENHANCEMENT#285
                        // Echo message after successfullly creating or updating a test
                        NSString *butOkay = NSLocalizedString(@"Okay", nil);
                        NSString *message = NSLocalizedString(@"Test has been successfully updated.", nil);
                        
                        if (newitem) {
                            message = NSLocalizedString(@"A new test has been successfully created.", nil);
                        }
                        
                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                                     message:message
                                                                    delegate:wo
                                                           cancelButtonTitle:butOkay
                                                           otherButtonTitles:nil];
                        [av show];
                        
                            [wo.navigationController popViewControllerAnimated:YES];
                    });
                
                [self.tm requestTestListForUser:self.user_id doneBlock:^(BOOL status) {
                }];
                
            } else {
                [HUD hideUIBlockingIndicator];
                
                // NSString *alertTitle =  NSLocalizedString(@"Save Error", nil);
                NSString *message = NSLocalizedString(@"There was a problem with the connection", nil);
                AlertWithMessageAndDelegate(alertTitle, message, self);
            }
        }];
    }

//    NSString *boxTitle = NSLocalizedString(@"Save Error", nil);
    
//    if ( (name_text.length == 0) || (description_text.length == 0) ) {
//        NSString *message = @"Please enter required fields";
//        AlertWithMessageAndDelegate(boxTitle, message, self);
//    }
    
    if (areRequiredFieldsEmpty) {
        NSString *message = NSLocalizedString(@"Please fill in all required fields", nil);
//        AlertWithMessageAndDelegate(boxTitle, message, self);
        AlertWithMessageAndDelegate(alertTitle, message, self);
    }
    
//    if (question_list.count == 0) {
    if (question_list.count == 0 && !areRequiredFieldsEmpty) {
        [HUD hideUIBlockingIndicator];
//        NSString *message = NSLocalizedString(@"Please add a question", nil);
        NSString *message = NSLocalizedString(@"You cannot update this test without at least one (1) question.", nil);
        
        if (newitem) {
            message = NSLocalizedString(@"You cannot save this test without at least one (1) question.", nil);
        }
        
//        NSString *message = NSLocalizedString(@"You cannot %@ this test without at least one (1) question.", action);
//        AlertWithMessageAndDelegate(boxTitle, message, self);
        AlertWithMessageAndDelegate(alertTitle, message, self);
    }
}

- (NSString *)getTotalFromObject:(NSManagedObject *)object {
    
    NSSet *question_set = [object valueForKey:@"questions"];
    NSArray *question_list = [question_set allObjects];
    
    NSString *total_score = @"1";
    if (question_list.count > 0) {
        NSInteger count = 0;
        for (NSManagedObject *mo in question_list) {
            NSString *points = [mo valueForKey:@"points"];
            count = count + [points integerValue];
            
            NSString *question_text = [mo valueForKey:@"question_text"];
            NSLog(@"%s XXXXX : %@", __PRETTY_FUNCTION__, question_text);
        }
        total_score = [NSString stringWithFormat:@"%ld", (long)count];
    }
    
    return total_score;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTestCellIdentifier forIndexPath:indexPath];
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSLog(@"MO ENTITY NAME!!!! [%@]", mo.entity.name);
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"question_text"] ];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"question_type_name"] ];
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        NSManagedObject *m = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete", nil) message:NSLocalizedString(@"Are you sure you want to delete", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* noAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:noAlertAction];
        
        __weak typeof(self) wo = self;
        
        UIAlertAction* yesAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [wo.tm removeQuestions:m doneBlock:nil];
        }];
        
        [alertController addAction:yesAlertAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *m = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete", nil) message:NSLocalizedString(@"Are you sure you want to delete", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* noAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:noAlertAction];
    
    __weak typeof(self) wo = self;
    
    UIAlertAction* yesAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [wo.tm removeQuestions:m doneBlock:nil];
    }];
    
    [alertController addAction:yesAlertAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"show_testbank_question_list"]) {
        TestBankQuestionList *questionList = (TestBankQuestionList *)[segue destinationViewController];
        questionList.quiz_mo = self.quiz_mo;
    }
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *context = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuizQuestionEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"quiz = %@", self.quiz_mo];
    [fetchRequest setPredicate:predicate];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *question_text = [NSSortDescriptor sortDescriptorWithKey:@"question_text" ascending:YES];
    [fetchRequest setSortDescriptors:@[question_text]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:context
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
