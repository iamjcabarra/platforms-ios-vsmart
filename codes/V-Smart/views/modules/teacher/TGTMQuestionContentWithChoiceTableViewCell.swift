//
//  TGTMQuestionContentWithChoiceTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 27/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
import WebKit

class TGTMQuestionContentWithChoiceTableViewCell: UITableViewCell, WKScriptMessageHandler, WKUIDelegate {
    
    @IBOutlet var choiceCheckImage: UIImageView!
    @IBOutlet var choiceTextLabel: UILabel!
    @IBOutlet var choiceImage: UIImageView!
    @IBOutlet var choiceImageHeight: NSLayoutConstraint!
    @IBOutlet var choiceWebView: UIView!
    @IBOutlet var choiceWebViewHeight: NSLayoutConstraint!
    
    fileprivate var webViewObject: WKWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Web View Supporting MathJax
    
    func setCustomScript(forSource source: String, inController controller: WKUserContentController, withInjectionAtStart start: Bool) {
        var time = WKUserScriptInjectionTime.atDocumentEnd
        if start { time = WKUserScriptInjectionTime.atDocumentStart }
        let userScript = WKUserScript(source: source, injectionTime: time, forMainFrameOnly: false)
        controller.addUserScript(userScript)
    }
    
    func loadWebView(withContent content: String) {
        // Configure WKWebview
        let controller = WKUserContentController()
        self.setCustomScript(forSource: "MathJax.js", inController: controller, withInjectionAtStart: true)
        let configuration = WKWebViewConfiguration()
        configuration.userContentController = controller
        
        // Initialize the WKWebView with the current frame and the configuration
        if self.webViewObject == nil { self.webViewObject = WKWebView(frame: self.choiceWebView.frame, configuration: configuration) }
        self.webViewObject.scrollView.isScrollEnabled = true
        self.choiceWebView.addSubview(self.webViewObject)
        
        // Update WKWebView object constraints
        self.webViewObject.mas_makeConstraints { (make) in
            make?.edges.isEqual(self.choiceWebView)
        }
        
        // Load HTML content
        let htmlString = "<!DOCTYPE html><html><head><meta name='viewport' content='initial-scale=1.5'/><style>span{ font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: 150; font-size: 17px; }</style><meta charset='UTF-8'></head><body><script type='text/javascript'>window.onload = function() { window.location.href = 'ready://' + document.body.offsetHeight; }</script><span>\(content)</span></body></html>"
        self.webViewObject.loadHTMLString(htmlString, baseURL: nil)
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("User Content Controller: \(message)")
    }
    
    // MARK: - Update Choice Image Height
    
    func zeroChoiceImageHeight(_ zero: Bool) {
        self.choiceImageHeight.constant = zero ? 0 : 115.0
    }
}
