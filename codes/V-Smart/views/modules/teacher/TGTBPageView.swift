//
//  TGTBPageView.swift
//  V-Smart
//
//  Created by Ryan Migallos on 11/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol TGTBPageViewDelegate: class {
    func didFinishSelectingObject(_ questionid:String, indexPath:IndexPath)
}

class TGTBPageView: UITableViewController, NSFetchedResultsControllerDelegate, UIViewControllerTransitioningDelegate {
    
    weak var delegate:TGTBPageViewDelegate?
    
    // MARK: - Shared Data Manager
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tm = TestGuruDataManager.sharedInstance()
        return tm!
    }()
    
    // MARK: - Designated Initializer
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!)  {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.commonInit()
    }
    
    fileprivate func commonInit() {
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        return sectionCount
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        return sectionData.numberOfObjects
    }

    fileprivate let kCellId = "tgtb_drawer_cell_identifier"
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kCellId, for: indexPath)
        // Configure the cell...
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ cell: UITableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        
        let bullet_number = (indexPath as NSIndexPath).row + 1
        
        let question_title: String = "\( bullet_number ). \( (mo.value(forKey: "name")! as AnyObject).description )"
        cell.textLabel!.text = question_title
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        let question_id = mo.value(forKey: "id") as! String
        delegate?.didFinishSelectingObject(question_id, indexPath: indexPath)
        self.dismiss(animated: true, completion: nil)
    }

    /////////////////////////////////// NSFETCHRESULTSCONTROLLER ///////////////////////////////////
        
    /*
        NOTE: CUSTOM ANIMATION
    */
    // MARK: - Fetched results controller
    
    fileprivate lazy var customDescriptors: [NSSortDescriptor] = {
        
        let ascending = false
        
        let date_modified = NSSortDescriptor(key: "sort_date", ascending: ascending)
        let question_text = NSSortDescriptor(key: "question_text", ascending: ascending)
        let question_id = NSSortDescriptor(key: "id", ascending: ascending)
        
        return [date_modified, question_text, question_id]
    }()
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        // Set ManagedObjectContext
        let ctx = dataManager.mainContext
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kQuestionEntity)
        //let fetchRequest = NSFetchRequest(entityName: kQuestionEntity)
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        fetchRequest.sortDescriptors = customDescriptors
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest,
            managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            self.configureCell(tableView.cellForRow(at: indexPath!)!, atIndexPath: indexPath!)
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .fade)
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    // MARK: - UIViewControllerTransitioningDelegate
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        if presented == self {
            return CustomPresentationController(presentedViewController: presented, presenting: presenting)
        }
        
        return nil
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if presented == self {
            return CustomPresentationAnimationController(isPresenting: true)
        }
        else {
            return nil
        }
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if dismissed == self {
            return CustomPresentationAnimationController(isPresenting: false)
        }
        else {
            return nil
        }
    }
    
/////////////////////////////////// UIPresentationController ///////////////////////////////////
    
    /// Custom Presentation Controller
    // MARK: - Custom Presentation Controller
    class CustomPresentationController: UIPresentationController {
        
        /**
         Dimming View - Creates a Fullscreen view
         */
        lazy var dimmingView :UIView = {
            
            // ATTACHED A GESTURE RECOGNIZER
            let action = #selector(CustomPresentationController.dimmingViewTapped(_:))
            let tap = UITapGestureRecognizer(target: self, action: action)
            
            let view = UIView(frame: self.containerView!.bounds)
            view.addGestureRecognizer(tap)
            
            view.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
            view.alpha = 0.0
            
            return view
        }()
        
        /**
         Removes the Dimming View when the user taps
         - Parameter gesture: user tap gesture
         */
        func dimmingViewTapped(_ gesture: UIGestureRecognizer) {
            if gesture.state == UIGestureRecognizerState.recognized {
                self.presentingViewController.dismiss(animated: true, completion: nil)
            }
        }
        
        override func presentationTransitionWillBegin() {
            
            let containerView = self.containerView
            let presentedViewController = self.presentedViewController
            
            // Make sure the dimming view is the size of the container's bounds, and fully transparent
            dimmingView.frame = containerView!.bounds
            dimmingView.alpha = 0.0
            
            // Insert the dimming view below everything else
            containerView!.insertSubview(self.dimmingView, at:0)
            
            // Fade in the dimming view alongside the transition
            if let transitionCoordinator = presentedViewController.transitionCoordinator {
                transitionCoordinator.animate( alongsideTransition: {(context: UIViewControllerTransitionCoordinatorContext!) -> Void in
                    self.dimmingView.alpha = 1.0
                    }, completion:nil)
            }
            self.dimmingView.alpha = 1.0
        }
        
        override func dismissalTransitionWillBegin()  {
            // Fade out the dimming view alongside the transition
            if let transitionCoordinator = presentedViewController.transitionCoordinator {
                transitionCoordinator.animate(alongsideTransition: {(context: UIViewControllerTransitionCoordinatorContext!) -> Void in
                    self.dimmingView.alpha  = 0.0
                    }, completion:nil)
            }
            self.dimmingView.alpha = 0.0
        }
        
        override var adaptivePresentationStyle : UIModalPresentationStyle {
            return UIModalPresentationStyle.overFullScreen
        }
        
        override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
            var decrement = 1.25 as CGFloat
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                decrement = 3
            }
            
            let w = parentSize.width / decrement
            let h = parentSize.height
            
            return CGSize(width: w, height: h)
        }
        
        override func containerViewDidLayoutSubviews() {
            self.dimmingView.frame = self.containerView!.bounds
            self.presentedView?.frame = self.frameOfPresentedViewInContainerView
        }
        
        override var shouldPresentInFullscreen : Bool {
            return true
        }
        
        override var frameOfPresentedViewInContainerView : CGRect {
            var presentedViewFrame = CGRect.zero
            let containerBounds = self.containerView?.bounds
            presentedViewFrame.size = size(forChildContentContainer: (self.presentedViewController as UIContentContainer), withParentContainerSize: containerBounds!.size)
            presentedViewFrame.origin.x = 0
            
            return presentedViewFrame
        }
    }
    
/////////////////////////////////// UIViewControllerAnimatedTransitioning ///////////////////////////////////
    
    /*
    NOTE: CUSTOM ANIMATION
    */
    
    // MARK: - Custom Animation Transition
    
    class CustomPresentationAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
        
        let isPresenting :Bool
        let duration :TimeInterval = 0.5
        
        init(isPresenting: Bool) {
            self.isPresenting = isPresenting
            super.init()
        }
        
        // ---- UIViewControllerAnimatedTransitioning methods
        
        // CHECKED 1
        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
            return self.duration
        }
        
        // CHECKED 2
        func animateTransition(using transitionContext: UIViewControllerContextTransitioning)  {
            
            guard
                let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
                let fromView = fromVC.view,
                let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
                let toView = toVC.view
                else {
                    return
            }
            
            let containerView = transitionContext.containerView
            
            if isPresenting {
                containerView.addSubview(toView)
            }
            
            let animatingVC = isPresenting ? toVC : fromVC
            let animatingView = animatingVC.view
            
            let appearedFrame = transitionContext.finalFrame(for: animatingVC)
            var dismissedFrame = appearedFrame
            dismissedFrame.origin.x -= dismissedFrame.size.width
            
            let initialFrame = isPresenting ? dismissedFrame : appearedFrame
            let finalFrame = isPresenting ? appearedFrame : dismissedFrame
            
            animatingView?.frame = initialFrame
            
            UIView.animate(withDuration: self.duration,
                delay: 0.0,
                usingSpringWithDamping: 1.0,
                initialSpringVelocity: 0.0,
                options: [.allowUserInteraction, .beginFromCurrentState],
                animations: { animatingView?.frame = finalFrame },
                completion: { (completed: Bool) -> Void in
                    if !self.isPresenting { fromView.removeFromSuperview() }
                    transitionContext.completeTransition(true)
            })
        }
    }
}
