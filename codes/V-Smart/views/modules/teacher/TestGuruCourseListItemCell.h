//
//  TestGuruCourseListItemCell.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 09/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruCourseListItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *courseName;
@property (weak, nonatomic) IBOutlet UILabel *courseCode;
@property (weak, nonatomic) IBOutlet UILabel *courseDescription;


@end
