//
//  TestGuruSpringboard.m
//  V-Smart
//
//  Created by Ryan Migallos on 21/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruSpringboard.h"
#import "TestGuruDashboardStats.h"
#import "TestGuruDashboardGenericHeader.h"
#import "TestGuruDashboardFooterControl.h"
#import "TestGuruDashboardButtonCell.h"
#import "TestGuruDataManager.h"
#import "TGQuestionDetailsView.h"
#import "TGQBQuestionDetailViewController.h"
#import "TestGuruListView.h"
#import "MainHeader.h"
#import "TestGuruQuestionGroupView.h"
#import "TGTBCreateTestView.h"
#import "TGTBTestActionConstants.h"
#import "TestGuruConstants.h"
#import "HUD.h"
#import "QuestionTypesList.h"
#import "AppDelegate.h"
#import "V_Smart-Swift.h"
#import "UIImageView+WebCache.h"

@interface TestGuruSpringboard () <UICollectionViewDelegateFlowLayout, QuestionTypesListDelegate, TGQuestionDetailsDelegate, TestGuruDashboardFooterDelegate, TGQBQuestionDetailDelegate>

@property (strong, nonatomic) TestGuruDataManager *tm;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) IBOutlet UIButton *buttonQuestions;
@property (strong, nonatomic) IBOutlet UIButton *buttonTests;
@property (strong, nonatomic) IBOutlet UIButton *buttonDeployments;

//VIEWS TO BE STACKED LATER
@property (strong, nonatomic) IBOutlet UIView *statViewsContainer;
@property (strong, nonatomic) IBOutlet UIView *questionViewCount;
@property (strong, nonatomic) IBOutlet UIView *testViewCount;
@property (strong, nonatomic) IBOutlet UIView *deployedTestViewCount;

@property (strong, nonatomic) IBOutlet UILabel *questionCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *testCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *deployCountLabel;

@property (strong, nonatomic) IBOutlet UILabel *questionHeaderLabel;
@property (strong, nonatomic) IBOutlet UILabel *testHeaderLabel;
@property (strong, nonatomic) IBOutlet UILabel *deployHeaderLabel;

@property (strong, nonatomic) TestGuruDashboardButtonCell *allQuestionCell;
@property (strong, nonatomic) TestGuruDashboardButtonCell *allTestCell;

@property (strong, nonatomic) NSArray *items;
@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *question_id_new;
@property (strong, nonatomic) NSString *test_id_new;
@property (strong, nonatomic) NSString *questionCountString;
@property (strong, nonatomic) NSString *testCountString;
@property (strong, nonatomic) NSString *package_type_id;
@property (strong, nonatomic) NSDictionary *pagingInformation;
@property (strong, nonatomic) NSString *groupByType;

@property (assign, nonatomic) BOOL isGroupBy;

@property (weak, nonatomic) IBOutlet UILabel *packageName;
@property (weak, nonatomic) IBOutlet UIImageView *packageImage;

@end

@implementation TestGuruSpringboard

static NSString *ObserverCTX = @"ObserverCTX";

static NSString * const kHeaderStatsIdentifier = @"collection_header_stat_view";
static NSString * const kGenericHeaderIdentifier = @"collection_header_section";
static NSString * const kFooterIdentifier = @"springboard_footer_control";
static NSString * const kCell = @"springboard_cell_identifier";
static NSString * const kPreviewCell = @"testpreview_cell_identifier";
CGFloat kHeight = 70;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //NSLocalization
    self.questionHeaderLabel.text = NSLocalizedString(@"Questions", nil);
    self.testHeaderLabel.text =  NSLocalizedString(@"Tests", nil);
    self.deployHeaderLabel.text = NSLocalizedString(@"Deployed Tests", nil);
    
    [self.buttonQuestions addTarget:self action:@selector(redirectToQuestionModule) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonTests addTarget:self action:@selector(redirectToTestModule) forControlEvents:UIControlEventTouchUpInside];
    //    [self.buttonDeployments addTarget:self action:@selector(redirectToDeployedModule) forControlEvents:UIControlEventTouchUpInside];
    
    self.questionCountString = @"0";
    self.testCountString = @"0";
    
    self.question_id_new = @"";
    self.test_id_new = @"";
    
    ResourceManager *rm = [AppDelegate resourceInstance];
    [rm startSocketIOClient];
}

- (void)setupStackViewComponentForPackageID:(NSString *)packageid {
    
    NSLog(@"-------[ 2 ]------ %s", __PRETTY_FUNCTION__);
    
    NSArray *components = @[_questionViewCount, _testViewCount, _deployedTestViewCount];
    
    NSLog(@"package type id : %@", packageid);
    
    /*
     package id : 1
     package value : Free
     package id : 2
     package value : Premium
     package id : 3
     package value : User Defined
     */
    
    if ([packageid isEqualToString:@"1"]) {
        components = @[_questionViewCount];
    }
    
    if ([packageid isEqualToString:@"2"]) {
        components = @[_questionViewCount, _testViewCount, _deployedTestViewCount];
        self.buttonQuestions.userInteractionEnabled = NO;
    }
    
    //iOS 8 Compatible Stack View
    TZStackView *stackView = [[TZStackView alloc] initWithArrangedSubviews:components];
    stackView.translatesAutoresizingMaskIntoConstraints = NO;
    stackView.distribution = UIStackViewDistributionFillEqually;
    stackView.alignment = TZStackViewAlignmentFill;
    stackView.axis = UILayoutConstraintAxisHorizontal;
    stackView.spacing = 4;
    [self.statViewsContainer addSubview:stackView];
    
    NSDictionary *bindings = NSDictionaryOfVariableBindings(stackView);
    NSDictionary *metrics = @{@"spacer":@0};
    
    NSString *horizontalArrangements = @"H:|-spacer-[stackView]-spacer-|";
    NSString *verticalArrangements = @"V:|-spacer-[stackView]-spacer-|";
    
    NSArray *contrainsts1 = [NSLayoutConstraint constraintsWithVisualFormat:horizontalArrangements options:0 metrics:metrics views:bindings];
    NSArray *contrainsts2 = [NSLayoutConstraint constraintsWithVisualFormat:verticalArrangements options:0 metrics:metrics views:bindings];
    
    [self.statViewsContainer addConstraints:contrainsts1];
    [self.statViewsContainer addConstraints:contrainsts2];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.package_type_id = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID]];
        NSString *package_type_name = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_NAME]];
        NSString *package_type_image_url = [self.tm stringValue:[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_URL]];
        NSString *course_name = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTGQB_SELECTED_COURSE_NAME]];
        [wo.packageImage sd_setImageWithURL:[NSURL URLWithString:package_type_image_url]];
        wo.packageName.text = [package_type_name uppercaseString];
        self.title = course_name;
    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
       [self setupStackViewComponentForPackageID:self.package_type_id];
    });
    
    [self customizeNavigationController];
    [self loadDashBoardStatistics];
    [self loadTestCreateDropDownDefaults];
    self.isGroupBy = NO;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
}

- (void)customizeNavigationController {
    
    UIColor *color = UIColorFromHex(0xF69C42);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    } else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

- (void)loadDashBoardStatistics {
    
    self.tm = [TestGuruDataManager sharedInstance];
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    __weak typeof(self) wo = self;
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);     // pair 1 enter
    [self.tm requestDashboardStatistics:^(NSDictionary *data) {
        if (data) {
            self.questionCountString = [NSString stringWithFormat:@"%@", data[@"question_count"] ];
            self.testCountString = [NSString stringWithFormat:@"%@", data[@"test_count"] ];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                wo.questionCountLabel.text = [NSString stringWithFormat:@"%@", data[@"question_count"] ];
                wo.testCountLabel.text = [NSString stringWithFormat:@"%@", data[@"test_count"] ];
                wo.deployCountLabel.text = [NSString stringWithFormat:@"%@", data[@"deployed_test_count"] ];
            });
        }
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo loadItems];
        });
    });
    
}

- (void)loadTestCreateDropDownDefaults {
    
    // ACADEMIC TERM
    NSDictionary *parameters = @{@"current_page": @"1", @"limit": @"100", @"search_key": @""};
    [self.tm requestPaginatedAcademicTermsForParameters:parameters dataBlock:^(NSDictionary *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *message = data != nil ? @"Success loading academic terms..." : @"Failed loading academic terms...";
            NSLog(@"XXX ---> %@", message);
        });
    }];
    
    // ASSESSMENT TYPE
    NSString *courseid = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTGQB_SELECTED_COURSE_ID]];
    [self.tm requestTestAssessmentTypeListForCourse:courseid isForTestGuru:YES isForApproval:NO doneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *message = status ? @"Success loading assessment types..." : @"Failed loading assessment types...";
            NSLog(@"XXX ---> %@", message);
        });
    }];
}

- (NSString *)statsForEntity:(NSString *)string {
    NSUInteger count = [self.tm fetchCountForEntity:string];
    NSString *stats = [NSString stringWithFormat:@"%lu", (unsigned long)count];
    return stats;
}

- (void)loadItems {
    
    self.items = @[
                   
                   @{ @"header" : @"Question Bank",
                      @"list" : @[
                              @{@"title":@"All Questions",
                                @"count":self.questionCountString,
                                @"type":@"questions_module",
                                @"image":@"yellow_folder72x72"},
                              
                              @{@"title":@"By Tags",
                                @"count":@"",
                                @"type":@"questions_module",
                                @"image":@"yellow_folder72x72"},
                              
                              @{@"title":@"By Question Type",
                                @"count":@"",
                                @"type":@"questions_module",
                                @"image":@"yellow_folder72x72"},
                              
                              @{@"title":@"By Difficulty Level",
                                @"count":@"",
                                @"type":@"questions_module",
                                @"image":@"yellow_folder72x72"},
                              
                              @{@"title":@"By Learning Skill",
                                @"count":@"",
                                @"type":@"questions_module",
                                @"image":@"yellow_folder72x72"},
                              
                              @{@"title":@"By Competency Code",
                                @"count":@"",
                                @"type":@"questions_module",
                                @"image":@"yellow_folder72x72"},
                              
                              @{@"title":@"Add Question",
                                @"count":@"",
                                @"type":@"question",
                                @"image":@"addbutton_question_icon72x72"}
                              
                              ]
                      //                      @"footer" : @{@"title":@"Add Question",@"type":@"question",@"image":@"addbutton_question_icon72x72"}
                      },
                   
                   @{ @"header" : @"TEST BANK",
                      @"list" : @[
                              @{@"title":@"All Tests",
                                @"count":self.testCountString,
                                @"type":@"tests_module",
                                @"image":@"blue_folder72x72"},
                              
                              @{@"title":@"Add Tests",
                                @"count":@"",
                                @"type":@"test",
                                @"image":@"add_tests_icon72x72"}
                              
                              ]
                      }
                   
                   ];
    //////////
    // FREE //
    //////////
    if ([self.package_type_id isEqualToString:@"1"]) {
        self.items = @[
                       @{ @"header" : @"Question Bank",
                          @"list" : @[
                                  @{@"title":@"All Questions",
                                    @"count":self.questionCountString,
                                    @"type":@"questions_module",
                                    @"image":@"yellow_folder72x72"},
                                  
                                  @{@"title":@"By Tags",
                                    @"count":@"",
                                    @"type":@"questions_module",
                                    @"image":@"yellow_folder72x72"},
                                  
                                  @{@"title":@"By Question Type",
                                    @"count":@"",
                                    @"type":@"questions_module",
                                    @"image":@"yellow_folder72x72"},
                                  
                                  @{@"title":@"By Difficulty Level",
                                    @"count":@"",
                                    @"type":@"questions_module",
                                    @"image":@"yellow_folder72x72"},
                                  
                                  @{@"title":@"By Learning Skill",
                                    @"count":@"",
                                    @"type":@"questions_module",
                                    @"image":@"yellow_folder72x72"},
                                  
                                  @{@"title":@"By Competency Code",
                                    @"count":@"",
                                    @"type":@"questions_module",
                                    @"image":@"yellow_folder72x72"},
                                  ]
                          //                          @"footer" : @{@"title":@"Add Question",@"type":@"question",@"image":@"addbutton_question_icon72x72"}
                          }
                       
                       ];
    }
    
    /////////////
    // PREMIUM //
    /////////////
    if ([self.package_type_id isEqualToString:@"2"]) {
        
        self.items = @[
                       
                       @{ @"header" : @"TEST BANK",
                          @"list" : @[
                                  @{@"title":@"All Tests",
                                    @"count":self.testCountString,
                                    @"type":@"tests_module",
                                    @"image":@"blue_folder72x72"},
                                  
                                  @{@"title":@"Add Tests",
                                    @"count":@"",
                                    @"type":@"test",
                                    @"image":@"add_tests_icon72x72"}
                                  
                                  ]
                          }
                       ];
    }
    
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    //QUESTIONS_MODULE_OBJECT
    if ([segue.identifier isEqualToString:@"QUESTIONS_MODULE_OBJECT"]) {
        TestGuruListView *listView = (TestGuruListView *)[segue destinationViewController];
        listView.isGroupBy = self.isGroupBy;
    }
    
    if ([segue.identifier isEqualToString:@"MODAL_QUESTION_TYPE_LIST"]) {
        QuestionTypesList *qtl = (QuestionTypesList *)[segue destinationViewController];
        qtl.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"TESTGURU_GROUP_MODULE_OBJECT"]) {
        TestGuruQuestionGroupView *tgqgv = (TestGuruQuestionGroupView *)[segue destinationViewController];
        tgqgv.groupByType = [NSString stringWithFormat:@"%@", self.groupByType];
        tgqgv.pagingInformation = [NSDictionary dictionaryWithDictionary:self.pagingInformation];
    }
    
    /* TRIGERRED BY QUESTION TYPE VIEW CONTROLLER */
    if ([segue.identifier isEqualToString:@"SHOW_QUESTION_DETAILS_V22"]) {
        
        //IF GLOBAL NEW QUESTION ID HAS CONTENT
        if (self.question_id_new.length > 0) {
            
            NSManagedObject *object = [self.tm firstManagedObject];
            if (object != nil) {
                
                TGQBQuestionDetailViewController *qd = (TGQBQuestionDetailViewController *)[segue destinationViewController];
                qd.delegate = self;
                [qd setQuestionObject:object];
                qd.editMode = NO;
            }
            
            self.question_id_new = @""; //RESET TO DEFAULT EMPTY VALUES
        }
    }
    
    // CREATE TEST
    if ([segue.identifier isEqualToString:@"showCreateTestView"]) {
        if (self.test_id_new.length > 0) {
            NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:self.test_id_new];
            NSManagedObject *object = [self.tm getEntity:kTestInformationEntity predicate:predicate];
            
            if (object != nil) {
                TGTBCreateTestView *createTestView = (TGTBCreateTestView *)[segue destinationViewController];
                createTestView.customBackBarButtonTitle = self.title;
                createTestView.testObject = object;
                createTestView.crudActionType = TGTBCrudActionTypeCreate;
            }
        }
    }
}

- (void)createQuestionTypeObject:(NSDictionary *)dictionary {
    
    NSLog(@"%s ------", __PRETTY_FUNCTION__);
    if (dictionary != nil) {
        self.question_id_new = [NSString stringWithFormat:@"%@", dictionary[@"question_id"] ];
        [self performSegueWithIdentifier:@"SHOW_QUESTION_DETAILS_V22" sender:self];
    }
}

- (void)didFinishCreateOperation:(BOOL)state {
    NSLog(@"%s ------", __PRETTY_FUNCTION__);
    [self performSegueWithIdentifier:@"QUESTIONS_MODULE_OBJECT" sender:self];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    NSInteger count = [self.items count];
    return count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger count = 0;
    
    if ([self.items count] > 0) {
        
        NSDictionary *data = self.items[section];
        NSArray *objects = data[@"list"];
        count = [objects count];
    }
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger section = indexPath.section;
    
    NSString *identifier = kCell;
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    // Configure the cell
    NSDictionary *object = self.items[section];
    NSArray *objects = object[@"list"];
    
    NSInteger row = indexPath.row;
    NSDictionary *data = objects[row];
    
    NSString *title = [NSString stringWithFormat:@"%@", data[@"title"] ];
    
    NSString *imageString = [NSString stringWithFormat:@"%@", data[@"image"] ];
    
    // REMOVE PREVIEW CELL
    TestGuruDashboardButtonCell *c = (TestGuruDashboardButtonCell *)cell;
    c.countLabel.text = @"";
    c.titleLabel.text = title;
    c.imageView.image = [UIImage imageNamed:imageString];
    
    NSUInteger count = [data[@"count"] integerValue];
    NSString *key = (count > 0) ? @"items" : @"item";
    NSString *countString = NSLocalizedString(key, nil);
    NSString *valueString = [NSString stringWithFormat:@"%@ %@", data[@"count"], countString ];
    
    if (section == 0 && row == 0) {
        c.countLabel.text = valueString;
    }
    
    if (section == 1 && row == 0) {
        c.countLabel.text = valueString;
    }
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    NSInteger section = indexPath.section;
    NSDictionary *object = self.items[section];
    NSString *headerString = [NSString stringWithFormat:@"%@", object[@"header"] ];
    NSString *header = NSLocalizedString(headerString, nil);
    
    NSDictionary *footer = object[@"footer"];
    
    NSString *title = @"";
    NSString *imageString = @"";
    NSString *typeString = @"";
    
    if (footer != nil) {
        title = [NSString stringWithFormat:@"%@", footer[@"title"] ];
        imageString = [NSString stringWithFormat:@"%@", footer[@"image"] ];
        typeString = [NSString stringWithFormat:@"%@", footer[@"type"] ];
    }
    
    //    NSString *title = [NSString stringWithFormat:@"%@", footer[@"title"] ];
    //    NSString *imageString = [NSString stringWithFormat:@"%@", footer[@"image"] ];
    //    NSString *typeString = [NSString stringWithFormat:@"%@", footer[@"type"] ];
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        TestGuruDashboardGenericHeader *sectionView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                                         withReuseIdentifier:kGenericHeaderIdentifier
                                                                                                forIndexPath:indexPath];
        sectionView.headerLabel.text = header;
        
        return sectionView;
    }
    
    if (kind == UICollectionElementKindSectionFooter) {
        
        TestGuruDashboardFooterControl *footerControl = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                                                                                           withReuseIdentifier:kFooterIdentifier
                                                                                                  forIndexPath:indexPath];
        footerControl.delegate = self;
        footerControl.buttonLabel.text = title;
        footerControl.typeString = typeString;
        footerControl.imageView.image = [UIImage imageNamed:imageString];
        
        return footerControl;
    }
    
    return reusableview;
}

- (void)didFinishSelectingType:(NSString *)type {
    
    if ([type isEqualToString:@"question"]) {
        //        [self showQuestionTypes];
    }
    
    if ([type isEqualToString:@"test"]) {
        //        [self showTestDetailView];
    }
}

- (void)showQuestionTypes {
    
    /* SHOWS A MODAL VIEW OF THE QUESTION TYPES */
    __weak typeof(self) wo = self;
    [self.tm requestQuestionType:^(BOOL status) {
        if (status == YES) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo performSegueWithIdentifier:@"MODAL_QUESTION_TYPE_LIST" sender:self];
            });
        }
    }];
}

- (void)showTestDetailView {
    
    NSString *indicatorStr = NSLocalizedString(@"Loading", nil);
    [HUD showUIBlockingIndicatorWithText:indicatorStr];
    
    dispatch_queue_t queue = dispatch_queue_create("com.vibetech.types.FILTER",DISPATCH_QUEUE_SERIAL);
    dispatch_group_t group = dispatch_group_create();
    __weak typeof(self) wo = self;
    
    dispatch_group_enter(group);
    dispatch_group_async(group, queue, ^{
        //GRADED | STAGE
        NSString *sectionTitle1 = NSLocalizedString(@"Question Types", nil);
        NSString *sectionOrder1 = @"0";
        [wo.tm requestFilterOptionType:@"questions/types" title:sectionTitle1 order:sectionOrder1 doneBlock:^(BOOL status1) {
            if (status1) {
                dispatch_group_leave(group);
            }
        }];
    });
    
    dispatch_group_enter(group);
    dispatch_group_async(group, queue, ^{
        //GRADED | STAGE
        NSString *sectionTitle2 = NSLocalizedString(@"Difficulty Level", nil);
        NSString *sectionOrder2 = @"1";
        [wo.tm requestFilterOptionType:@"proficiency" title:sectionTitle2 order:sectionOrder2 doneBlock:^(BOOL status2) {
            if (status2) {
                dispatch_group_leave(group);
            }
        }];
    });
    
    dispatch_group_enter(group);
    dispatch_group_async(group, queue, ^{
        //GRADED | STAGE
        NSString *sectionTitle3 = NSLocalizedString(@"Learning Skill", nil);
        NSString *sectionOrder3 = @"2";
        [wo.tm requestFilterOptionType:@"learning" title:sectionTitle3 order:sectionOrder3 doneBlock:^(BOOL status3) {
            if (status3) {
                dispatch_group_leave(group);
            }
        }];
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self.tm insertNewTestWitContentBlock:^(NSString *test_id) {
            if ( (test_id != nil) && (test_id.length > 0) ) {
                wo.test_id_new = [NSString stringWithFormat:@"%@", test_id];
                [wo performSegueWithIdentifier:@"showCreateTestView" sender:self];
            }
        }];
        [HUD hideUIBlockingIndicator];
    });
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 20;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = (collectionView.frame.size.width / 3) - 20;
    return CGSizeMake(width, kHeight);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    CGFloat width = collectionView.frame.size.width;
    CGSize size = CGSizeMake(width, 44);
    
    return size;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    
    CGFloat width = collectionView.frame.size.width;
    
    return CGSizeMake(width, kHeight);
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [self.collectionView performBatchUpdates:nil completion:nil];
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger section = indexPath.section;
    NSDictionary *object = self.items[section];
    NSArray *list = object[@"list"];
    
    NSInteger row = indexPath.row;
    NSDictionary *data = list[row];
    
    NSString *type = [NSString stringWithFormat:@"%@", data[@"type"] ];
    
    if ([type isEqualToString:@"questions_module"]) {
        [self loadFilterTypeForSegueWithIdentifier:@"QUESTIONS_MODULE_OBJECT" index:indexPath.row];
    }
    
    if ([type isEqualToString:@"question"]) {
        [self showQuestionTypes];
    }
    
    if ([type isEqualToString:@"tests_module"]) {
        [self loadTestListWithIndex:indexPath.row];
    }
    
    if ([type isEqualToString:@"test"]) {
        [self showTestDetailView];
    }
}

- (void)redirectToQuestionModule {
    
    /* REDIRECT TO ALL QUESTIONS */
    [self loadFilterTypeForSegueWithIdentifier:@"QUESTIONS_MODULE_OBJECT" index:0];
}

- (void)redirectToTestModule {
    [self performSegueWithIdentifier:@"TEST_MODULE_TYPE" sender:self];
    [self hideBackButtonTitle];
}

//- (void)redirectToDeployedModule {
//    [self performSegueWithIdentifier:@"DEPLOY_MODULE_TYPE" sender:self];
//}

- (void)loadTestListWithIndex:(NSInteger)index {
    
    NSLog(@"---------------> %s", __PRETTY_FUNCTION__);
    __weak typeof(self) wo = self;
    dispatch_queue_t queue = dispatch_queue_create("com.vibetech.types.TEST",DISPATCH_QUEUE_SERIAL);
    dispatch_group_t group = dispatch_group_create();
    
    if (index == 1) {
        dispatch_group_enter(group);
        dispatch_group_async(group, queue, ^{
            [wo.tm requestGradingTypes:@"1" dataBlock:^(NSDictionary *data) {
                self.isGroupBy = YES;
                self.pagingInformation = [NSDictionary dictionaryWithDictionary:data];
                dispatch_group_leave(group);
            }];
        });
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        if ((index == 1) && (self.pagingInformation != nil)) {
            [self performSegueWithIdentifier:@"TESTGURU_GROUP_MODULE_OBJECT" sender:self];
        }
        
        if (index == 0) {
            [self performSegueWithIdentifier:@"TEST_MODULE_TYPE" sender:self];
            [self hideBackButtonTitle];
        }
    });
}

- (void)loadFilterTypeForSegueWithIdentifier:(NSString *)identifier index:(NSInteger)index {
    
    NSString *indicatorStr = NSLocalizedString(@"Loading", nil);
    [HUD showUIBlockingIndicatorWithText:indicatorStr];
    
    NSLog(@"---------------> %s", __PRETTY_FUNCTION__);
    dispatch_queue_t queue = dispatch_queue_create("com.vibetech.types.FILTER",DISPATCH_QUEUE_SERIAL);
    dispatch_group_t group = dispatch_group_create();
    
    if (index == 0) {
        __weak typeof(self) wo = self;
        
        dispatch_group_enter(group);
        dispatch_group_async(group, queue, ^{
            //GRADED | STAGE
            NSString *sectionTitle1 = NSLocalizedString(@"Question Types", nil);
            NSString *sectionOrder1 = @"0";
            [wo.tm requestFilterOptionType:@"questions/types" title:sectionTitle1 order:sectionOrder1 doneBlock:^(BOOL status1) {
                if (status1) {
                    dispatch_group_leave(group);
                }
            }];
        });
        
        dispatch_group_enter(group);
        dispatch_group_async(group, queue, ^{
            //GRADED | STAGE
            NSString *sectionTitle2 = NSLocalizedString(@"Difficulty Level", nil);
            NSString *sectionOrder2 = @"1";
            [wo.tm requestFilterOptionType:@"proficiency" title:sectionTitle2 order:sectionOrder2 doneBlock:^(BOOL status2) {
                if (status2) {
                    dispatch_group_leave(group);
                }
            }];
        });
        
        dispatch_group_enter(group);
        dispatch_group_async(group, queue, ^{
            //GRADED | STAGE
            NSString *sectionTitle3 = NSLocalizedString(@"Learning Skill", nil);
            NSString *sectionOrder3 = @"2";
            [wo.tm requestFilterOptionType:@"learning" title:sectionTitle3 order:sectionOrder3 doneBlock:^(BOOL status3) {
                if (status3) {
                    dispatch_group_leave(group);
                }
            }];
        });
    }
    
    if (([identifier isEqualToString:@"QUESTIONS_MODULE_OBJECT"]) && (index != 0)) {
        identifier = @"TESTGURU_GROUP_MODULE_OBJECT";
    }
    
    //------------------------------ QUESTION MODULE ------------------------------
    
    //types : tags, questiontypes, difficulty,learningskills
    
    NSString *linkType = @"";
    
    //GROUP BY TAGS
    if ([identifier isEqualToString:@"TESTGURU_GROUP_MODULE_OBJECT"] && (index == 1)) {
        self.isGroupBy = YES;
        linkType = @"tags";
        self.groupByType = @"TAGS";
    }
    
    //GROUP BY QUESTION TYPES
    if ([identifier isEqualToString:@"TESTGURU_GROUP_MODULE_OBJECT"] && (index == 2)) {
        self.isGroupBy = YES;
        linkType = @"questiontypes";
        self.groupByType = @"QUESTION_TYPES";
    }
    
    //GROUP BY DIFFICULTY LEVEL
    if ([identifier isEqualToString:@"TESTGURU_GROUP_MODULE_OBJECT"] && (index == 3)) {
        self.isGroupBy = YES;
        linkType = @"difficulty";
        self.groupByType = @"DIFFICULTY_LEVELS";
    }
    
    //GROUP BY LEARNING SKILL
    if ([identifier isEqualToString:@"TESTGURU_GROUP_MODULE_OBJECT"] && (index == 4)) {
        self.isGroupBy = YES;
        linkType = @"learningskills";
        self.groupByType = @"LEARNING_SKILLS";
    }
    
    //GROUP BY COMPTENCY CODE
    if ([identifier isEqualToString:@"TESTGURU_GROUP_MODULE_OBJECT"] && (index == 5)) {
        self.isGroupBy = YES;
        linkType = @"competencies";
        self.groupByType = @"COMPETENCY_CODES";
    }
    
    
    if ([identifier isEqualToString:@"TESTGURU_GROUP_MODULE_OBJECT"]) {
        if (linkType.length > 0) {
            dispatch_group_enter(group);
            dispatch_group_async(group, queue, ^{
                [self.tm requestHeadersWithLinkType:linkType withPage:@"1" dataBlock:^(NSDictionary *data) {
                    self.pagingInformation = [NSDictionary dictionaryWithDictionary:data];
                    dispatch_group_leave(group);
                }];
            });
        }
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        if ([identifier isEqualToString:@"TESTGURU_GROUP_MODULE_OBJECT"] && self.pagingInformation != nil) {
            [self performSegueWithIdentifier:identifier sender:self];
        }
        
        if ([identifier isEqualToString:@"QUESTIONS_MODULE_OBJECT"]) {
            [self performSegueWithIdentifier:identifier sender:self];
        }
        
        [HUD hideUIBlockingIndicator];
    });
}

- (void)hideBackButtonTitle {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

@end
