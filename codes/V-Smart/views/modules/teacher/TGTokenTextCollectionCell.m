//
//  TGTokenTextCollectionCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 06/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGTokenTextCollectionCell.h"

@interface TGTokenTextCollectionCell()

@property (weak, nonatomic) IBOutlet UIView *tagContainer;

@end

@implementation TGTokenTextCollectionCell

- (void)awakeFromNib {
    [self applyShadowToView:self.tagContainer];
}

- (void)applyShadowToView:(UIView *)object {
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.3f;
}

@end
