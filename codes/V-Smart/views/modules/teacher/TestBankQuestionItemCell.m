//
//  TestBankQuestionItemCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestBankQuestionItemCell.h"

@implementation TestBankQuestionItemCell

- (void)awakeFromNib {
    // Initialization code
    self.noChoiceLabel.text = NSLocalizedString(@"Empty choices", nil);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
