//
//  TestMainInfoView.m
//  V-Smart
//
//  Created by Julius Abarra on 14/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "TestMainInfoView.h"
#import "TestDatePickerController.h"
#import "TestTimePickerController.h"
#import "TestContentManager.h"

@interface TestMainInfoView () <UITextFieldDelegate, UITextViewDelegate, TestDatePickerDelegate, TestTimePickerController>

@property (strong, nonatomic) TestContentManager *sharedTestContentManager;

@property (strong, nonatomic) TestDatePickerController *datePickerView;
@property (strong, nonatomic) TestTimePickerController *timePickerView;

@property (strong, nonatomic) UIPopoverController *datePickerPopOverController;
@property (strong, nonatomic) UIPopoverController *timePickerPopOverController;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *maximumAttemptsLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLimitLabel;
@property (strong, nonatomic) IBOutlet UILabel *passingRateLabel;
@property (strong, nonatomic) IBOutlet UILabel *openingDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *closingDateLabel;

@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (strong, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) IBOutlet UITextField *maximumAttemptsTextField;
@property (strong, nonatomic) IBOutlet UITextField *timeLimitTextField;
@property (strong, nonatomic) IBOutlet UITextField *passingRateTextField;
@property (strong, nonatomic) IBOutlet UITextField *openingDateTextField;
@property (strong, nonatomic) IBOutlet UITextField *closingDateTextField;

@property (strong, nonatomic) IBOutlet UIButton *timeLimitButton;
@property (strong, nonatomic) IBOutlet UIButton *openingDateButton;
@property (strong, nonatomic) IBOutlet UIButton *closingDateButton;
@property (strong, nonatomic) IBOutlet UIButton *isGradedButton;

@property (assign, nonatomic) NSInteger timeFrameButtonTag;
@property (assign, nonatomic) BOOL isTestGraded;

@property (assign, nonatomic) int tempAttempts;
@property (assign, nonatomic) int tempPassingRate;

@end

@implementation TestMainInfoView

#define kDefaultAttempts    @"1"
#define kDefaultTimeLimit   @"00:15:00"
#define kDefaultPassingRate @"75"

#define kRightColorNCG UIColorFromHex(0x3498DB)
#define kWrongColorNCG UIColorFromHex(0xFF6666)
#define kRightColorYCG UIColorFromHex(0x3498DB).CGColor
#define kWrongColorYCG UIColorFromHex(0xFF6666).CGColor

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sharedTestContentManager = [TestContentManager sharedInstance];
    
    // Localize strings
    [self setUpStringLocalization];
    
    // Customize text fields
    [self customizeTextFields];
    
    // Customize buttons
    [self customizeButtons];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Default values
    [self setUpDefaultValues];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Text Fields

- (void)customizeTextFields {
    // Border color
    self.descriptionTextView.layer.borderColor = kRightColorYCG;
    self.titleTextField.layer.borderColor = kRightColorYCG;
    self.maximumAttemptsTextField.layer.borderColor = kRightColorYCG;
    self.timeLimitTextField.layer.borderColor = kRightColorYCG;
    self.passingRateTextField.layer.borderColor = kRightColorYCG;
    self.openingDateTextField.layer.borderColor = kRightColorYCG;
    self.closingDateTextField.layer.borderColor = kRightColorYCG;
    
    // Border width
    self.descriptionTextView.layer.borderWidth = 1.0f;
    self.titleTextField.layer.borderWidth = 1.0f;
    self.maximumAttemptsTextField.layer.borderWidth = 1.0f;
    self.timeLimitTextField.layer.borderWidth = 1.0f;
    self.passingRateTextField.layer.borderWidth = 1.0f;
    self.openingDateTextField.layer.borderWidth = 1.0f;
    self.closingDateTextField.layer.borderWidth = 1.0f;
    
    // Disable text fields with popup
    self.timeLimitTextField.enabled = NO;
    self.openingDateTextField.enabled = NO;
    self.closingDateTextField.enabled = NO;
    
    // Set text field tags
    self.titleTextField.tag = 100;
    self.maximumAttemptsTextField.tag = 200;
    self.timeLimitTextField.tag = 300;
    self.passingRateTextField.tag = 400;
    self.openingDateTextField.tag = 500;
    self.closingDateTextField.tag = 600;
    
    // Set text field delegates
    self.titleTextField.delegate = self;
    self.descriptionTextView.delegate = self;
    self.maximumAttemptsTextField.delegate = self;
    self.timeLimitTextField.delegate = self;
    self.passingRateTextField.delegate = self;
    self.openingDateTextField.delegate = self;
    self.closingDateTextField.delegate = self;
    
    // Add notification to enabled text fields
    [self.titleTextField addTarget:self
                            action:@selector(textFieldDidChange:)
                  forControlEvents:UIControlEventEditingChanged];
    
    [self.maximumAttemptsTextField addTarget:self
                                      action:@selector(textFieldDidChange:)
                            forControlEvents:UIControlEventEditingChanged];
    
    [self.passingRateTextField addTarget:self
                                  action:@selector(textFieldDidChange:)
                        forControlEvents:UIControlEventEditingChanged];
}

#pragma mark - Custom Buttons

- (void)customizeButtons {
    // Set button tags
    self.openingDateButton.tag = 100;
    self.closingDateButton.tag = 200;
    
    // Actions for buttons
    [self.timeLimitButton addTarget:self
                             action:@selector(showPopOverTimePicker:)
                   forControlEvents:UIControlEventTouchUpInside];
    
    [self.openingDateButton addTarget:self
                               action:@selector(showPopOverDatePicker:)
                     forControlEvents:UIControlEventTouchUpInside];
    
    [self.closingDateButton addTarget:self
                               action:@selector(showPopOverDatePicker:)
                     forControlEvents:UIControlEventTouchUpInside];
    
    [self.isGradedButton addTarget:self
                            action:@selector(changeGradingStatus:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    // Set isGradedButton title
    NSString *title = [NSString stringWithFormat:@"%@", [[self localizeString:@"Graded"] uppercaseString]];
    [self.isGradedButton setTitle:title forState:UIControlStateNormal];
    [self.isGradedButton setTitle:title forState:UIControlStateHighlighted];
}

#pragma mark - String Localization

- (void)setUpStringLocalization {
    self.titleLabel.text = [[self localizeString:@"Title"] uppercaseString];
    self.descriptionLabel.text = [[self localizeString:@"Brief Description"] uppercaseString];
    self.maximumAttemptsLabel.text = [[self localizeString:@"Attempts"] uppercaseString];
    self.timeLimitLabel.text = [[self localizeString:@"Time Limit"] uppercaseString];
    self.passingRateLabel.text = [[self localizeString:@"Passing Rate"] uppercaseString];
    self.openingDateLabel.text = [[self localizeString:@"Opening Date"] uppercaseString];
    self.closingDateLabel.text = [[self localizeString:@"Closing Date"] uppercaseString];
    
    // Text field place holder
    self.titleTextField.placeholder = [NSString stringWithFormat:@"%@...", [self localizeString:@"Please enter title of test here"]];
}

- (NSString *)localizeString:(NSString *)string {
    NSString *localized = NSLocalizedString(string, nil);
    return localized;
}

#pragma mark - Default Values

- (void)setUpDefaultValues {
    BOOL isNotEmpty = self.sharedTestContentManager.testMainInfo.count > 0;
    
    if (!isNotEmpty) {
        // Set values of text fields
        self.maximumAttemptsTextField.text = kDefaultAttempts;
        self.passingRateTextField.text = kDefaultPassingRate;
        self.timeLimitTextField.text = kDefaultTimeLimit;
        self.openingDateTextField.text = [self getDateTodayWithFormat:@"EEE, dd MMM yyyy, hh:mm a"];
        self.closingDateTextField.text = [self addDayToDate:[NSDate date] daysToAdd:1 formatToReturn:@"EEE, dd MMM yyyy, hh:mm a"];
        
        // Set value of button
        self.isTestGraded = YES;
        
        [self updateTestMainInfoContents];
        
        // For test changes made mapping
        NSDictionary *userInfo = @{@"testSection":@"testMainInfo", @"testData":self.sharedTestContentManager.testMainInfo};
        [self notifyTestForTestCreationPreEntries:userInfo];
    }
    
    if (isNotEmpty) {
        // Set values of text fields
        self.titleTextField.text = [self.sharedTestContentManager.testMainInfo objectForKey:@"name"];
        self.descriptionTextView.text = [self.sharedTestContentManager.testMainInfo objectForKey:@"description"];
        self.maximumAttemptsTextField.text = [self.sharedTestContentManager.testMainInfo objectForKey:@"attempts"];
        self.timeLimitTextField.text = [self.sharedTestContentManager.testMainInfo objectForKey:@"time_limit"];
        self.passingRateTextField.text = [self.sharedTestContentManager.testMainInfo objectForKey:@"passing_rate"];
        self.openingDateTextField.text = [self.sharedTestContentManager.testMainInfo objectForKey:@"date_open"];
        self.closingDateTextField.text = [self.sharedTestContentManager.testMainInfo objectForKey:@"date_close"];
        
        // Set value of button
        NSString *isTestGrade = [self.sharedTestContentManager.testMainInfo objectForKey:@"quiz_stage_id"];
        self.isTestGraded = ![isTestGrade boolValue];
        [self changeGradingStatus:nil];
    }
}

#pragma mark - Date Picker Popover

- (void)showPopOverDatePicker:(id)sender {
    // End active text field editing
    [self.view endEditing:YES];
    
    UIButton *button = (UIButton *)sender;
    self.timeFrameButtonTag = button.tag;
    
    // Create date picker controller
    self.datePickerView = [[TestDatePickerController alloc] initWithNibName:@"TestDatePickerView" bundle:nil];
    self.datePickerView.delegate = self;
    
    // Create popover controller
    self.datePickerPopOverController = [[UIPopoverController alloc] initWithContentViewController:self.datePickerView];
    self.datePickerPopOverController.popoverContentSize = CGSizeMake(320.0f, 235.0f);
    
    // Render popover
    [self.datePickerPopOverController presentPopoverFromRect:button.bounds
                                                      inView:button
                                    permittedArrowDirections:UIPopoverArrowDirectionDown
                                                    animated:YES];
    
    [self invalidateTestMainInfo];
}

#pragma mark - Time Picker Popover

- (void)showPopOverTimePicker:(id)sender {
    // End active text field editing
    [self.view endEditing:YES];
    
    UIButton *button = (UIButton *)sender;
    
    // Create time picker controller
    self.timePickerView = [[TestTimePickerController alloc] initWithNibName:@"TestTimePickerView" bundle:nil];
    self.timePickerView.delegate = self;
    
    // Create popover controller
    self.timePickerPopOverController = [[UIPopoverController alloc] initWithContentViewController:self.timePickerView];
    self.timePickerPopOverController.popoverContentSize = CGSizeMake(320.0f, 235.0f);
    
    // Render popover
    [self.timePickerPopOverController presentPopoverFromRect:button.bounds
                                                      inView:button
                                    permittedArrowDirections:UIPopoverArrowDirectionDown
                                                    animated:YES];
    
    [self invalidateTestMainInfo];
}

#pragma mark - Test Date Picker Delegate

- (void)selectedDate:(NSString *)dateString {
    // Opening date
    if (self.timeFrameButtonTag == 100) {
        self.openingDateTextField.text = dateString;
    }
    
    // Closing date
    if (self.timeFrameButtonTag == 200) {
        self.closingDateTextField.text = dateString;
    }
    
    [self validateTestSchedule];
    [self invalidateTestMainInfo];
    [self updateTestMainInfoContents];
    [self notifyTestForActionPreparation];
}

#pragma mark - Test Time Picker Delegate

- (void)selectedTime:(NSString *)timeString {
    self.timeLimitTextField.text = timeString;
    [self validateTestSchedule];
    [self invalidateTestMainInfo];
    [self updateTestMainInfoContents];
    [self notifyTestForActionPreparation];
}

#pragma mark - Test Time and Date Validation

- (void)validateTestSchedule {
    // Get dates difference
    double dateRangeSeconds = [self computeTimeDifferenceInSecondsFromDate:self.openingDateTextField.text
                                                                fromFormat:@"EEE, dd MMM yyyy, hh:mm a"
                                                                    toDate:self.closingDateTextField.text
                                                                  toFormat:@"yyyy-MM-dd HH:mm:ss"];
    // Convert time limit to seconds
    double timeLimitSeconds = [self convertTimeStringToSeconds:self.timeLimitTextField.text];
    
    // If closing date is less than the opening date
    if (dateRangeSeconds < 0) {
        NSDate *openingDate = [self changeStringToDate:self.openingDateTextField.text format:@"EEE, dd MMM yyyy, hh:mm a"];
        NSString *closingDate = [self addDayToDate:openingDate daysToAdd:1 formatToReturn:@"EEE, dd MMM yyyy, hh:mm a"];
        self.closingDateTextField.text = closingDate;
    }
    else {
        // If time limit is more than the difference of dates (opening and closing)
        if (timeLimitSeconds > dateRangeSeconds) {
            NSDate *closingDate = [self changeStringToDate:self.closingDateTextField.text format:@"EEE, dd MMM yyyy, hh:mm a"];
            closingDate = [closingDate dateByAddingTimeInterval:(timeLimitSeconds - dateRangeSeconds)];
            
            NSString *closingDateString = [self changeDateToString:closingDate format:@"EEE, dd MMM yyyy, hh:mm a"];
            self.closingDateTextField.text = closingDateString;
        }
    }
}

#pragma mark - Grading Status Action

- (void)changeGradingStatus:(id)sender {
    // End active text field editing
    [self.view endEditing:YES];
    
    UIColor *bgColor;
    
    if (self.isTestGraded) {
        bgColor = [UIColor grayColor];
        self.isTestGraded = NO;
    }
    else {
        bgColor = UIColorFromHex(0X3498DB);
        self.isTestGraded = YES;
    }
    
    [self.isGradedButton setBackgroundColor:bgColor];
    [self invalidateTestMainInfo];
    [self updateTestMainInfoContents];
    [self notifyTestForActionPreparation];
}

#pragma mark - Test Main Info Invalidation

- (void)invalidateTestMainInfo {
    // Title
    if ([self.titleTextField.text isEqualToString:@""]) {
        [self changeBorderColorOfTextField:self.titleTextField color:kWrongColorNCG];
    }
    else {
        [self changeBorderColorOfTextField:self.titleTextField color:kRightColorNCG];
    }
    
    // Description
    if ([self.descriptionTextView.text isEqualToString:@""]) {
        [self changeBorderColorOfTextView:self.descriptionTextView color:kWrongColorNCG];
    }
    else {
        [self changeBorderColorOfTextView:self.descriptionTextView color:kRightColorNCG];
    }
    
    // Atttempts
    if ([self.maximumAttemptsTextField.text isEqualToString:@""]) {
        [self changeBorderColorOfTextField:self.maximumAttemptsTextField color:kWrongColorNCG];
    }
    else if ([self.maximumAttemptsTextField.text intValue] < [kDefaultAttempts intValue]) {
        [self changeBorderColorOfTextField:self.maximumAttemptsTextField color:kWrongColorNCG];
    }
    else {
        [self changeBorderColorOfTextField:self.maximumAttemptsTextField color:kRightColorNCG];
    }
    
    // Date and Time
    double timeLimit = [self convertTimeStringToSeconds:self.timeLimitTextField.text];
    double dateRange = [self computeTimeDifferenceInSecondsFromDate:self.openingDateTextField.text
                                                         fromFormat:@"EEE, dd MMM yyyy, hh:mm a"
                                                             toDate:self.closingDateTextField.text
                                                           toFormat:@"yyyy-MM-dd HH:mm:ss"];
    if (timeLimit > dateRange || dateRange < 0) {
        [self changeBorderColorOfTextField:self.timeLimitTextField color:kWrongColorNCG];
        [self changeBorderColorOfTextField:self.openingDateTextField color:kWrongColorNCG];
        [self changeBorderColorOfTextField:self.closingDateTextField color:kWrongColorNCG];
    }
    else {
        [self changeBorderColorOfTextField:self.timeLimitTextField color:kRightColorNCG];
        [self changeBorderColorOfTextField:self.openingDateTextField color:kRightColorNCG];
        [self changeBorderColorOfTextField:self.closingDateTextField color:kRightColorNCG];
    }
    
    // Passing Rate
    if ([self.passingRateTextField.text isEqualToString:@""]) {
        [self changeBorderColorOfTextField:self.passingRateTextField color:kWrongColorNCG];
    }
    else if ([self.passingRateTextField.text intValue] < [kDefaultPassingRate intValue]) {
        [self changeBorderColorOfTextField:self.passingRateTextField color:kWrongColorNCG];
    }
    else {
        [self changeBorderColorOfTextField:self.passingRateTextField color:kRightColorNCG];
    }
}

#pragma mark - Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self invalidateTestMainInfo];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSInteger textFieldTag = textField.tag;
    
    // Attempts
    if (textFieldTag == 200) {
        textField.text = [self updateTestAttempts];
        [self changeBorderColorOfTextField:textField color:kRightColorNCG];
    }
    
    // Passing Rate
    if (textFieldTag == 400) {
        textField.text = [self updateTestPassingRate];
        [self changeBorderColorOfTextField:textField color:kRightColorNCG];
    }
    
    [self updateTestMainInfoContents];
    [self notifyTestForActionPreparation];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSInteger textFieldTag = textField.tag;
    
    // Title
    if (textFieldTag == 100) {
        
        // If backspace
        if ([string length] == 0) {
            return YES;
        }
        
        // 50 characters only for test title
        if ([textField.text length] >= 50) {
            return NO;
        }
    }
    
    // Attempts and Passing Rate
    if (textFieldTag == 200 || textFieldTag == 400) {
        
        // If backspace
        if ([string length] == 0) {
            return YES;
        }
        
        // Limit to only numeric characters
        NSCharacterSet *numberCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; ++i) {
            unichar c = [string characterAtIndex:i];
            
            if (![numberCharSet characterIsMember:c]) {
                return NO;
            }
        }
        
        // Limit to only up to 100
        NSString *newText;
        newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if ([newText intValue] > 100) {
            return NO;
        }
    }
    
    return YES;
}

- (void)textFieldDidChange:(id)sender {
    UITextField *textField = (UITextField *)sender;
    NSInteger textFieldTag = textField.tag;
    int intValue = [textField.text intValue];
    
    // Title
    if (textFieldTag == 100) {
        if ([textField.text isEqualToString:@""]) {
            [self changeBorderColorOfTextField:textField color:kWrongColorNCG];
        }
        else {
            [self changeBorderColorOfTextField:textField color:kRightColorNCG];
        }
    }
    
    // Attempts
    if (textFieldTag == 200) {
        if ([textField.text isEqualToString:@""] || intValue < [kDefaultAttempts intValue]) {
            [self changeBorderColorOfTextField:textField color:kWrongColorNCG];
        }
        else {
            [self changeBorderColorOfTextField:textField color:kRightColorNCG];
        }
    }
    
    // Passing Rate
    if (textFieldTag == 400) {
        if ([textField.text isEqualToString:@""] || intValue < [kDefaultPassingRate intValue]) {
            [self changeBorderColorOfTextField:textField color:kWrongColorNCG];
        }
        else {
            [self changeBorderColorOfTextField:textField color:kRightColorNCG];
        }
    }
    
    [self updateTestMainInfoContents];
    [self notifyTestForActionPreparation];
}

#pragma mark - Text View Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([self.titleTextField.text isEqualToString:@""]) {
        [self changeBorderColorOfTextField:self.titleTextField color:kWrongColorNCG];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        [self changeBorderColorOfTextView:textView color:kWrongColorNCG];
    }
    else {
        [self changeBorderColorOfTextView:textView color:kRightColorNCG];
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        [self changeBorderColorOfTextView:textView color:kWrongColorNCG];
    }
    else {
        [self changeBorderColorOfTextView:textView color:kRightColorNCG];
    }
    
    [self updateTestMainInfoContents];
    [self notifyTestForActionPreparation];
}

#pragma mark - Test Main Info Default Values Updating

- (NSString *)updateTestAttempts {
    int attempts = [self.maximumAttemptsTextField.text intValue];
    
    if (attempts >= [kDefaultAttempts intValue]) {
        self.tempAttempts = attempts;
    }
    else {
        if (self.tempAttempts < [kDefaultAttempts intValue]) {
            self.tempAttempts = [kDefaultAttempts intValue];
        }
    }
    
    NSString *stringValue = [NSString stringWithFormat:@"%d", self.tempAttempts];
    
    return stringValue;
}

- (NSString *)updateTestPassingRate {
    int passingRate = [self.passingRateTextField.text intValue];
    
    if (passingRate >= [kDefaultPassingRate intValue]) {
        self.tempPassingRate = passingRate;
    }
    else {
        if (self.tempPassingRate < [kDefaultPassingRate intValue]) {
            self.tempPassingRate = [kDefaultPassingRate intValue];
        }
    }
    
    NSString *stringValue = [NSString stringWithFormat:@"%d", self.tempPassingRate];
    
    return stringValue;
}

#pragma mark - Date and Time Related Methods

- (NSString *)getDateTodayWithFormat:(NSString *)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    dateFormatter.dateFormat = format;
    
    return [dateFormatter stringFromDate:[NSDate date]];;
}

- (NSString *)addDayToDate:(NSDate *)date daysToAdd:(int)days formatToReturn:(NSString *)format {
    if (date != nil) {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDate *tomorrow = [calendar dateByAddingUnit:NSCalendarUnitDay
                                                value:days
                                               toDate:date
                                              options:0];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateStyle = NSDateFormatterMediumStyle;
        dateFormatter.dateFormat = format;
        
        if (tomorrow != nil) {
            return [dateFormatter stringFromDate:tomorrow];
        }
    }
    
    return @"";
}

- (NSString *)changeDateString:(NSString *)string fromFormat:(NSString *)fromFormat toFormat:(NSString *)toFormat {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = fromFormat;
    
    NSDate *date = [formatter dateFromString:string];
    formatter.dateFormat = toFormat;
    
    NSString *newDateString = [formatter stringFromDate:date];
    
    if (newDateString == nil) {
        newDateString = @"";
    }
    
    return newDateString;
}

- (NSDate *)changeStringToDate:(NSString *)dateString format:(NSString *)format {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:format];
    return [dateFormat dateFromString:dateString];
}

- (NSString *)changeDateToString:(NSDate *)date format:(NSString *)format {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:format];
    return [dateFormat stringFromDate:date];
}

- (double)computeTimeDifferenceInSecondsFromDate:(NSString *)fDate fromFormat:(NSString *)fromFormat
                                          toDate:(NSString *)tDate toFormat:(NSString*)toFormat {
    
    NSString *fDateString = [self changeDateString:fDate fromFormat:fromFormat toFormat:toFormat];
    NSString *tDateString = [self changeDateString:tDate fromFormat:fromFormat toFormat:toFormat];
    
    if (!([fDateString isEqualToString:@""] && [tDateString isEqualToString:@""])) {
        NSDate *fDateObject = [self changeStringToDate:fDateString format:toFormat];
        NSDate *tDateObject = [self changeStringToDate:tDateString format:toFormat];
        
        if (fDateObject != nil && tDateObject != nil) {
            NSTimeInterval secondsDifference = [tDateObject timeIntervalSinceDate:fDateObject];
            return secondsDifference;
        }
    }
    
    return 0;
}

- (double)convertTimeStringToSeconds:(NSString *)timeString {
    NSArray *time = [timeString componentsSeparatedByString:@":"];
    
    if (time.count >= 3) {
        int h = [time[0] intValue];
        int m = [time[1] intValue];
        int s = [time[2] intValue];
        
        return (h * 3600) + (m * 60) + s;
    }
    
    return 0;
}

#pragma mark - Change Border Color

- (void)changeBorderColorOfTextField:(UITextField *)textField color:(UIColor *)color {
    textField.layer.borderColor = color.CGColor;
}

- (void)changeBorderColorOfTextView:(UITextView *)textView color:(UIColor *)color {
    textView.layer.borderColor = color.CGColor;
}

#pragma mark - Update Content Manager

- (void)updateTestMainInfoContents {
    // Get contents from text fields
    NSString *testTitle = self.titleTextField.text;
    NSString *testDescription = self.descriptionTextView.text;
    NSString *testAttempts = self.maximumAttemptsTextField.text;
    NSString *testTimeLimit = self.timeLimitTextField.text;
    NSString *testPassingRate = self.passingRateTextField.text;
    NSString *testOpeningDate = self.openingDateTextField.text;
    NSString *testClosingDate = self.closingDateTextField.text;
    
    // Is test graded?
    NSString *testIsGraded = @"0";
    if (self.isTestGraded) {
        testIsGraded = @"1";
    }
    
    // Save to test content manager
    [self.sharedTestContentManager.testMainInfo setObject:testTitle forKey:@"name"];
    [self.sharedTestContentManager.testMainInfo setObject:testDescription forKey:@"description"];
    [self.sharedTestContentManager.testMainInfo setObject:testAttempts forKey:@"attempts"];
    [self.sharedTestContentManager.testMainInfo setObject:testTimeLimit forKey:@"time_limit"];
    [self.sharedTestContentManager.testMainInfo setObject:testPassingRate forKey:@"passing_rate"];
    [self.sharedTestContentManager.testMainInfo setObject:testOpeningDate forKey:@"date_open"];
    [self.sharedTestContentManager.testMainInfo setObject:testClosingDate forKey:@"date_close"];
    [self.sharedTestContentManager.testMainInfo setObject:testIsGraded forKey:@"quiz_stage_id"];
}

#pragma mark - Test Notifications

- (void)notifyTestForTestCreationPreEntries:(NSDictionary *)d {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setPreEntriesForTestCreation" object:self userInfo:d];
}

- (void)notifyTestForActionPreparation {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"prepareToSaveOrUpdateTest" object:nil];
}

@end