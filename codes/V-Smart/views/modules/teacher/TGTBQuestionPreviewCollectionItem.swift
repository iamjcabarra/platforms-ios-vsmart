//
//  TGTBQuestionPreviewCollectionItem.swift
//  V-Smart
//
//  Created by Ryan Migallos on 11/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class TGTBQuestionPreviewCollectionItem: UICollectionViewCell {

    @IBOutlet var textLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        textLabel.text = ""
    }

}
