//
//  TestBankController.m
//  V-Smart
//
//  Created by Ryan Migallos on 8/28/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "TestBankController.h"
#import "TestBankCell.h"
#import "TestGuruDataManager.h"
#import "TestBankDetailController.h"
#import "TestBankDeployController.h"
#import "AppDelegate.h"
#import "AccountInfo.h"
#import "Utils.h"
#import "VSmartValues.h"
#import "HUD.h"

#define ResultsTableView self.searchResultsTableViewController.tableView

@interface TestBankController ()

<NSFetchedResultsControllerDelegate, UISearchResultsUpdating, UISearchControllerDelegate,
UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate> {
    NSManagedObject *mo_selected;
}

@property (nonatomic, strong) TestGuruDataManager *tm;

@property (nonatomic, strong) IBOutlet UIButton *addTestButton;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *tableRefreshControl;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) UITableViewController *searchResultsTableViewController;
@property (nonatomic, strong) NSArray *results;
@property (nonatomic, assign) BOOL isAscending;
@property (nonatomic, assign) BOOL isEditMode;

@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) UITableView *activeTableView;

@end

@implementation TestBankController

static NSString *kCellIdentifier = @"test_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
//    self.title = NSLocalizedString(@"Tests", nil);
    // Uncomment the following line to preserve selection between presentations.
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.isAscending = NO;
    
    self.tm = [TestGuruDataManager sharedInstance];
    self.managedObjectContext = self.tm.mainContext;
    
    [self loadSelectionTypes];
    
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    SEL refreshAction = @selector(listAllTest);
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    [self.addTestButton addTarget:self action:@selector(addTestAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self listAllTest];
    [self setupRightBarButton];
    [self setupSearchCapabilities];
}

- (void)loadSelectionTypes {
    
    __weak typeof(self) wo = self;
    dispatch_queue_t queue = dispatch_queue_create("com.vibetech.test.TYPES",DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        //GRADED | STAGE
        [wo.tm requestType:kEndPointTestGuruGradedType doneBlock:nil];
    });
    dispatch_async(queue, ^{
        //RESULT TYPE
        [wo.tm requestType:kEndPointTestGuruResultType doneBlock:nil];
    });
    dispatch_async(queue, ^{
        //QUIZ CATEGORY
        [wo.tm requestType:kEndPointTestGuruQuizCategory doneBlock:nil];
    });
    dispatch_async(queue, ^{
        //TEST TYPE
        [wo.tm requestType:kEndPointTestGuruTestType doneBlock:nil];
    });
    dispatch_async(queue, ^{
        //PROFICIENCY LEVEL
        [wo.tm requestType:kEndPointTestGuruProficiencyLevel doneBlock:nil];
    });
    dispatch_async(queue, ^{
        //LEARNING SKILL
        [wo.tm requestType:kEndPointTestGuruLearningSkill doneBlock:nil];
    });
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addTestAction:(id)sender {
    
    self.isEditMode = NO;

    BOOL status = [self.tm clearContentsForEntity:kQuizQuestionEntity predicate:nil];
    if (status == YES) {
        [self performSegueWithIdentifier:@"selected_test_info" sender:self];
    }
}

- (void)setupRightBarButton {
    
    UIImage *sortImage = [UIImage imageNamed:@"sort_258x258"];
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sortButton.frame = CGRectMake(0, 0, 44, 44);
    sortButton.showsTouchWhenHighlighted = YES;
    [sortButton setImage:sortImage forState:UIControlStateNormal];
    [sortButton setImage:sortImage forState:UIControlStateHighlighted];
    [sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
}

- (void)setupSearchCapabilities {
    
    self.results = [[NSMutableArray alloc] init];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    UITableViewController *srtvc = [sb instantiateViewControllerWithIdentifier:@"test_search_table_view_controller"];
    srtvc.tableView.dataSource = self;
    srtvc.tableView.delegate = self;
    
//    UINib *testBankCellNib = [UINib nibWithNibName:@"TestBankCell" bundle:nil];
//    [srtvc.tableView registerNib:testBankCellNib forCellReuseIdentifier:kCellIdentifier];
    
    self.searchResultsTableViewController = srtvc;
    
    // Init a search controller with its table view controller for results.
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsTableViewController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    
    // Make an appropriate size for search bar and add it as a header view for initial table view.
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // Enable presentation context.
    self.definesPresentationContext = YES;
}

- (void)listAllTest {
    
    __weak typeof(self) wo = self;
    dispatch_queue_t queue = dispatch_queue_create("com.vibetech.quiz.LIST",DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        [wo.tm requestTestListForUser:self.user_id doneBlock:^(BOOL status) {
            //do nothing
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo.tableRefreshControl endRefreshing];
            });
        }];
    });
}

- (IBAction)refreshTestList:(id)sender {
    
    [self listAllTest];
}

- (void)sortButtonAction:(id)sender {
    
    self.isAscending = (_isAscending) ? NO : YES;
    
//    if ([tableView_active isEqual:ResultsTableView]) {
//        [self reloadSearchResults];
//    }
//    
//    if (![tableView_active isEqual:ResultsTableView]) {
//        [self reloadFetchedResultsController];
//    }
    
    if ([self.activeTableView isEqual:ResultsTableView]) {
        [self reloadSearchResults];
    }
    
    if (![self.activeTableView  isEqual:ResultsTableView]) {
        [self reloadFetchedResultsController];
    }
}

- (void)reloadSearchResults {
    
    NSArray *items = [NSArray arrayWithArray:self.results];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:self.isAscending];
    NSArray *sorted = [items sortedArrayUsingDescriptors:@[date_modified]];
    
    // Set up results.
    self.results = [NSArray arrayWithArray:sorted];
    
    // Reload search table view.
    // [tableView_active reloadData];
    [self.activeTableView reloadData];
}

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (NSManagedObject *)managedObjectFromButtonAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = 1;
    
    if ([tableView isEqual:ResultsTableView]) {
        return count;
    } else {
        return [[self.fetchedResultsController sections] count];
    }
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    if ([tableView isEqual:ResultsTableView]) {
        
        if (self.results) {
            return self.results.count;
        } else {
            return count;
        }
        
    } else {
        
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        count = [sectionInfo numberOfObjects];
        
    }
    
    NSString *title_object = NSLocalizedString(@"Test", nil);
    NSString *titleString = [NSString stringWithFormat:@"%@ (%ld)", title_object, (long)count];
    self.title = titleString;
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // tableView_active = tableView;
    self.activeTableView = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    if ([tableView isEqual:ResultsTableView]) {
        [self configureFilteredCell:cell indexPath:indexPath];
    }
    
    if (![tableView isEqual:ResultsTableView]) {
        [self configureCell:cell atIndexPath:indexPath];
    }
    
    return cell;
}

- (void)configureFilteredCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = self.results[indexPath.row];
    TestBankCell *testCell = (TestBankCell *)cell;
    [self configureCell:testCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TestBankCell *testCell = (TestBankCell *)cell;
    [self configureCell:testCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(TestBankCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *title = [NSString stringWithFormat:@"%@", [mo valueForKey:@"name"] ];
    NSString *description = [NSString stringWithFormat:@"%@", [mo valueForKey:@"desc"] ];

    NSString *testCountLabel = NSLocalizedString(@"Question(s)", nil);
    NSString *count = [NSString stringWithFormat:@"%@ %@", [mo valueForKey:@"item_count"], testCountLabel ];
    
    cell.testName.text = title;
    cell.testDescription.text = description;
    cell.testCount.text = count;
    
    SEL deleteSelector = @selector(deleteButtonAction:);
    [cell.deleteButton addTarget:self action:deleteSelector forControlEvents:UIControlEventTouchUpInside];
        
    SEL editSelector = @selector(editTestButtonAction:);
    [cell.editButton addTarget:self action:editSelector forControlEvents:UIControlEventTouchUpInside];

    SEL deploySelector = @selector(deployButtonAction:);
    [cell.deployButton addTarget:self action:deploySelector forControlEvents:UIControlEventTouchUpInside];
}

- (void)deleteButtonAction:(id)sender {
    
    mo_selected = [self managedObjectFromButtonAction:sender];
    NSString *test_title = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"name"]];
    NSString *message_string = NSLocalizedString(@"Are you sure you want to delete", nil);
    NSString *message = [NSString stringWithFormat:@"%@ %@?", message_string, test_title];
    NSString *yesTitle = NSLocalizedString(@"Yes", nil);
    NSString *noTitle = NSLocalizedString(@"No", nil);
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Delete"
                                                 message:message
                                                delegate:self
                                       cancelButtonTitle:noTitle
                                       otherButtonTitles:yesTitle,nil];
    [av show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        //TODO: perform delete operation
        [self.tm requestRemoveTest:mo_selected doneBlock:^(BOOL status) {
            
        }];
    }
}

- (void)assignButtonAction:(id)sender {
    
//    mo_selected = [self managedObjectFromButtonAction:sender];
//    NSString *question_title = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"name"]];
}

- (void)editTestButtonAction:(id)sender {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    self.isEditMode = YES;
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    // NSIndexPath *indexPath = [tableView_active indexPathForRowAtPoint:buttonPosition];
    NSIndexPath *indexPath = [self.activeTableView indexPathForRowAtPoint:buttonPosition];
    
    NSManagedObject *mo = nil;
    
//    if ([tableView_active isEqual:ResultsTableView]) {
//        NSLog(@"Search TableView");
//        mo = self.results[indexPath.row];
//    }
//    
//    if (![tableView_active isEqual:ResultsTableView]) {
//        NSLog(@"List TableView");
//        mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//    }
    
    if ([self.activeTableView isEqual:ResultsTableView]) {
        NSLog(@"Search TableView");
        mo = self.results[indexPath.row];
    }
    
    if (![self.activeTableView isEqual:ResultsTableView]) {
        NSLog(@"List TableView");
        mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    mo_selected = mo;
    
    NSString *indicatorString = NSLocalizedString(@"Loading...", nil);
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    __weak typeof(self) wo = self;
    [self.tm requestDetailsForTest:mo_selected doneBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [wo performSegueWithIdentifier:@"selected_test_info" sender:nil];
                // [tableView_active deselectRowAtIndexPath:indexPath animated:YES];
                [wo.activeTableView deselectRowAtIndexPath:indexPath animated:YES];
            });

        }
    }];
    
}

- (void)deployButtonAction:(id)sender {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    self.isEditMode = YES;
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    // NSIndexPath *indexPath = [tableView_active indexPathForRowAtPoint:buttonPosition];
    NSIndexPath *indexPath = [self.activeTableView indexPathForRowAtPoint:buttonPosition];
    
    NSManagedObject *mo = nil;
    
//    if ([tableView_active isEqual:ResultsTableView]) {
//        NSLog(@"Search TableView");
//        mo = self.results[indexPath.row];
//    }
//    
//    if (![tableView_active isEqual:ResultsTableView]) {
//        NSLog(@"List TableView");
//        mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//    }
    
    if ([self.activeTableView isEqual:ResultsTableView]) {
        NSLog(@"Search TableView");
        mo = self.results[indexPath.row];
    }
    
    if (![self.activeTableView isEqual:ResultsTableView]) {
        NSLog(@"List TableView");
        mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    mo_selected = mo;
    
    __weak typeof(self) wo = self;
    [self.tm requestDetailsForTest:mo_selected doneBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo performSegueWithIdentifier:@"show_deploy_test_courses" sender:nil];
                // [tableView_active deselectRowAtIndexPath:indexPath animated:YES];
                [wo.activeTableView deselectRowAtIndexPath:indexPath animated:YES];
            });
            
        }
    }];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    if ([segue.identifier isEqualToString:@"selected_test_info"]) {
        TestBankDetailController *info = (TestBankDetailController *)[segue destinationViewController];
        info.user_id = [NSString stringWithFormat:@"%@", self.user_id];
        info.editMode = self.isEditMode;
        info.quiz_mo = (info.editMode) ? [self quizObjectFromTestObject:mo_selected] : [self createNewRecord];
    }
    
    if ([segue.identifier isEqualToString:@"show_deploy_test_courses"]) {
        TestBankDeployController *dep = (TestBankDeployController *)[segue destinationViewController];
        dep.quiz_mo = mo_selected;
    }
}

- (NSManagedObject *)createNewRecord {
    
    NSManagedObject *quizObject = [self.tm insertNewRecordForEntity:kQuizEntity];
//    NSString *id_object = [Utils randomNumber];
//    [quizObject setValue:id_object forKey:@"id"];
    
    return quizObject;
}

- (NSManagedObject *)quizObjectFromTestObject:(NSManagedObject *)mo {
    
    NSString *quiz_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"quiz_settings_id"] ];
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"quiz_settings_id" andValue:quiz_id];
    NSManagedObject *quizObject = [self.tm getEntity:kQuizEntity predicate:predicate];
    return quizObject;
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTestEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
//    NSPredicate *predicate_type = [self predicateForKeyPath:@"questionTypeName" value:@"Multiple Choice"];
//    NSCompoundPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate_type]];
//    [fetchRequest setPredicate:predicate];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:self.isAscending];
    [fetchRequest setSortDescriptors:@[date_modified]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Search Results Updating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    UISearchBar *searchBar = searchController.searchBar;
    
    if (searchBar.text.length > 0) {
        
        NSString *text = searchBar.text;
        
        NSArray *fetchedObjects = self.fetchedResultsController.fetchedObjects;
        
        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(NSManagedObject *mo, NSDictionary *bindings) {
            
            NSString *object = [NSString stringWithFormat:@"%@", [mo valueForKey:@"search_string"]];
            NSStringCompareOptions options = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;
            NSRange range = [object rangeOfString:text options:options];
            
            return range.location != NSNotFound;
        }];
        
        // Set up results.
        self.results = [fetchedObjects filteredArrayUsingPredicate:predicate];;
        
        // Reload search table view.
        [self.searchResultsTableViewController.tableView reloadData];
    }
}

@end
