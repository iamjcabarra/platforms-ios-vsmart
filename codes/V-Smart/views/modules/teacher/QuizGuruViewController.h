//
//  QuizGuruViewController.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 9/9/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface QuizGuruViewController : BaseViewController<UIWebViewDelegate>

@property (nonatomic, strong) UIWebView *theWebView;
@end
