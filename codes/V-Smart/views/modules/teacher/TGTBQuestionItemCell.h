//
//  TGTBQuestionItemCell.h
//  V-Smart
//
//  Created by Julius Abarra on 08/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGTBQuestionItemCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *questionTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionDescriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionPointValue;
@property (strong, nonatomic) IBOutlet UILabel *questionPointLabel;

@property (strong, nonatomic) IBOutlet UIButton *checkBoxButton;
@property (strong, nonatomic) IBOutlet UIImageView *packageTypeImage;
@property (weak, nonatomic) IBOutlet UIView *detailView;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (void)updateDifficultyLevelView:(NSString *)difficultyLevelID;
- (void)shouldHideCheckbox:(BOOL)hide;
- (void)updatePackageImage:(NSString *)packageID;
- (void)loadWebViewWithContent:(NSString *)content;
- (void)updatePoints:(NSString *)points;

@end
