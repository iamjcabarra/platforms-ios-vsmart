//
//  TGCourseListController.swift
//  V-Smart
//
//  Created by Ryan Migallos on 13/06/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class TGCourseListController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate {

    // MARK: - Shared Data Manager
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tgdm = TestGuruDataManager.sharedInstance()
        return tgdm!
    }()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var packageImage: UIImageView!
    @IBOutlet weak var packageName: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var loadMoreButton: UIButton!
    
    fileprivate var searchBarActive: Bool = false
    fileprivate var currentPage: Int = 0
    fileprivate var totalFilteredTests: Int = 0
    fileprivate var totalPages: Int = 0
    fileprivate var totalItems: Int = 0
    fileprivate var searchKeyword: String! = ""
    fileprivate var isAcending: Bool = false
    fileprivate var temporaryCoursecount = 1
    
    // CELL IDENTIFIER
    let kCourseCellIdentifier = "courseCellIdentifier";
    let kQuestionCourseIdentifier = "tg_course_cell_identifier";
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loadMoreButton.isHidden = true
        self.tableView.tableFooterView = self.loadMoreButton //CHECKED
        let actionLoad = #selector(self.loadMoreButtonAction(_:))
        self.loadMoreButton.addTarget(self, action: actionLoad, for: .touchUpInside)
        
        // Do any additional setup after loading the view.
        self.searchBar.delegate = self;
        self.searchBar.placeholder = NSLocalizedString("Search", comment: "")
        self.title = NSLocalizedString("Course List", comment: "")
        
        // Resusable Cell
        let courseCellItemNib = UINib(nibName: "CPCourseCellItem", bundle: nil)
        self.tableView.register(courseCellItemNib, forCellReuseIdentifier: kCourseCellIdentifier)
        
        // Initial Paginated Course List
        // NOTE: Comment if unpaginated
        let settings = getSettingsForCoursePagination(reset: true, loadNextPage: false)
        listPaginateedCourseForSettings(settings)
    }
    
    func loadMoreButtonAction(_ sender:UIButton!) {
        self.isAcending = false
        let settings = getSettingsForCoursePagination(reset: false, loadNextPage: true)
        listPaginateedCourseForSettings(settings)
    }
    
    // MARK: - Paginated Course List
    func getSettingsForCoursePagination(reset:Bool, loadNextPage:Bool) -> [AnyHashable: Any] {
        let value = self.dataManager.fetchObject(forKey: kTG_PAGINATION_LIMIT)
        var limit = self.dataManager.stringValue(value)
        var current_page = "1"
        let sort_key: String! = ""
        
        if Int(limit!)! < 1 {
            limit = "10"
        }
        
        if (reset == false) {
            let number = loadNextPage ? (self.currentPage + 1) : self.currentPage
            current_page = NSString(format: "%zd", number) as String
        }
        
        return ["limit":limit!, "current_page":current_page, "search_keyword":self.searchKeyword, "sort_key":sort_key!];
    }
    
    func listPaginateedCourseForSettings(_ settings:[AnyHashable: Any]) {
        let userid = self.dataManager.loginUser() as String
        self.loadMoreButton.isHidden = true
        self.dataManager.requestPaginatedCourseList(forUser: userid, withSettingsForPagination: settings) { (data) in
            if data != nil {
                
                let current_page = data?["current_page"] as! String
                let total_items = data?["total_items"] as! String
                let total_filtered = data?["total_filtered"] as! String
                let total_pages = data?["total_pages"] as! String
                
                self.currentPage = Int(current_page)!
                self.totalItems = Int(total_items)!
                self.totalFilteredTests = Int(total_filtered)!
                self.totalPages = Int(total_pages)!
            }
            
            DispatchQueue.main.async(execute: { 
                self.reloadFetchedResultsController()
                let hidden = (self.totalPages > self.currentPage) ? false : true
                self.loadMoreButton.isHidden = hidden
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.main.async {
            let package_type_name = self.dataManager.fetchObject(forKey: kTGQB_SELECTED_PACKAGE_NAME) as! String
            let package_type_image_url = self.dataManager.fetchObject(forKey: kTGQB_SELECTED_PACKAGE_IMAGE_URL) as! NSString
            self.packageImage.sd_setImage(with: URL(string: package_type_image_url as String))
            self.packageName.text = package_type_name.uppercased()
        }
        
        customizeNavigationController()
        self.tableView.isUserInteractionEnabled = true
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
/*        let package_type_name = self.dataManager.fetchObject(forKey: kTGQB_SELECTED_PACKAGE_NAME) as! String
        let package_type_image_data = self.dataManager.fetchObject(forKey: kTGQB_SELECTED_PACKAGE_IMAGE_DATA) as! Data
        
        DispatchQueue.main.async {
            self.packageImage.image = UIImage(data: package_type_image_data)
            self.packageName.text = package_type_name.uppercased()
        } */
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Search Bar Delegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchKeyword = searchBar.text!;
        let settings = getSettingsForCoursePagination(reset: true, loadNextPage: false)
        listPaginateedCourseForSettings(settings)
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.searchKeyword = searchText;
            let settings = getSettingsForCoursePagination(reset: true, loadNextPage: false)
            listPaginateedCourseForSettings(settings)
        }
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBarActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = self.searchKeyword;
        self.searchBarActive = false
    }
    
    func customizeNavigationController() {
        let color = UIColor(hex6: 0x4274b9)
        
        if floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1 {
            self.navigationController?.navigationBar.tintColor = color
        } else {
            self.navigationController?.navigationBar.barTintColor = color;
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationController?.navigationBar.isTranslucent = false
        }
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let count = self.fetchedResultsController.sections?.count
        return count!;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        let count = sectionInfo.numberOfObjects
        return count;
    }
    
    func tableView(_ tbv: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView = tbv;
        let cell = tbv.dequeueReusableCell(withIdentifier: kCourseCellIdentifier, for: indexPath)
        self.configureCell(cell, atIndexPath: indexPath)
        return cell;
    }
    
    fileprivate func configureCell(_ cell: UITableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        
        let courseCell = cell as! CPCourseCellItem
        
        let course_name = mo.value(forKey: "course_name") as! String!
        let grade_level = mo.value(forKey: "grade_level") as! String!
        let schedule = mo.value(forKey: "schedule") as! String!
        
        courseCell.courseNameLabel.text = course_name;
        courseCell.courseSectionLabel.text = grade_level;
        courseCell.courseScheduleLabel.text = schedule;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        
        let course_id = mo.value(forKey: "course_id") as! String!
        let course_name = mo.value(forKey: "course_name") as! String!
        
        self.dataManager.save(course_id, forKey: kTGQB_SELECTED_COURSE_ID)
        self.dataManager.save(course_name, forKey: kTGQB_SELECTED_COURSE_NAME)
        
        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: "SPRINGBOARD_TEST_GURU_MODULE", sender: self)
        });
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = self.tableView.indexPathForSelectedRow
        self.tableView.deselectRow(at: indexPath!, animated: true)
        self.tableView.isUserInteractionEnabled = true
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
    fileprivate lazy var customDescriptors: [NSSortDescriptor] = {
        let ascending = true
        var comparator = #selector(NSString.caseInsensitiveCompare(_:))
        let question_index = NSSortDescriptor(key: "course_name", ascending: ascending, selector:comparator)
        
        return [question_index]
    }()
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        // Set ManagedObjectContext
        let ctx = dataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: kCourseListEntity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kCourseListEntity)
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 10
                
        // Edit the sort key as appropriate.
        fetchRequest.sortDescriptors = customDescriptors
        
        // Edit the section name key path and cache name if appropriate.
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest,
                                             managedObjectContext: ctx!,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil)
        frc.delegate = self
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
            
        case .insert:
            self.tableView.insertSections( IndexSet(integer: sectionIndex), with: .fade)
            
        case .delete:
            self.tableView.deleteSections( IndexSet(integer: sectionIndex), with: .fade)
            
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        let tbv = self.tableView
        switch type {
            
        case .insert:
            tbv?.insertRows( at: [newIndexPath!] , with: .fade)
            
        case .delete:
            tbv?.deleteRows( at: [indexPath!], with: .fade)
            
        case .update:
            self.configureCell( (tbv?.cellForRow(at: indexPath!)!)!, atIndexPath:indexPath!)

        case .move:
            tbv?.deleteRows(at: [indexPath!], with: .fade)
            tbv?.insertRows(at: [indexPath!], with: .fade)
        }
    }
    
    func predicateForKeyPath(keypath:String, value:String) -> NSPredicate {
        
        // create left and right expression
        let left = NSExpression(forKeyPath: keypath)
        let right = NSExpression(forConstantValue: value)
        
        // create predicate options
        let predicate = NSComparisonPredicate(leftExpression: left,
                                              rightExpression: right,
                                              modifier: .direct,
                                              type: NSComparisonPredicate.Operator.like,
                                              options: [NSComparisonPredicate.Options.diacriticInsensitive, NSComparisonPredicate.Options.caseInsensitive])
        return predicate
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
}
