//
//  TBClassHelper.m
//  TestBankVersion2.1
//
//  Created by Julius Abarra on 06/02/2016.
//  Copyright © 2016 Vibe Technologies, Inc. All rights reserved.
//

#import "TBClassHelper.h"

@implementation TBClassHelper

- (NSString *)localizeString:(NSString *)string {
    NSString *localized = NSLocalizedString(string, nil);
    return localized;
}

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

- (void)justifyLabel:(UILabel *)label string:(NSString *)string {
    NSMutableParagraphStyle *paragraphStyles = [[NSMutableParagraphStyle alloc] init];
    paragraphStyles.alignment = NSTextAlignmentJustified;
    paragraphStyles.firstLineHeadIndent = 1.0f;
    NSDictionary *attributes = @{NSParagraphStyleAttributeName:paragraphStyles};
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:string attributes:attributes];
    label.attributedText = attributedString;
}

- (CGFloat)getHeightOfLabel:(UILabel*)label {
    CGSize maxSize = CGSizeMake(label.frame.size.width, label.frame.size.height);
    CGSize requiredSize = [label sizeThatFits:maxSize];
    
    return requiredSize.height;
}

- (NSString *)getDateTodayWithFormat:(NSString *)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    
    NSString *dateString = [self stringValue:[dateFormatter stringFromDate:[NSDate date]]];
    
    if ([dateString isEqualToString:@""]) {
        dateString = [self dateStringDefaultValue];
    }
    
    return dateString;
}

- (NSString *)transformDateString:(NSString *)string fromFormat:(NSString *)fromFormat toFormat:(NSString *)toFormat {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = fromFormat;
    
    NSDate *date = [dateFormatter dateFromString:string];
    dateFormatter.dateFormat = toFormat;
    
    NSString *newDateString = [self stringValue:[dateFormatter stringFromDate:date]];
    
    if ([newDateString isEqualToString:@""]) {
        newDateString = [self dateStringDefaultValue];
    }
    
    return newDateString;
}

- (NSString *)addDayToDate:(NSDate *)date numberOfDays:(int)days formatToReturn:(NSString *)format {
    if (date != nil) {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDate *nextDate = [calendar dateByAddingUnit:NSCalendarUnitDay
                                                value:days
                                               toDate:date
                                              options:0];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = format;
        
        NSString *dateString = [self stringValue:[dateFormatter stringFromDate:nextDate]];
        
        if ([dateString isEqualToString:@""]) {
            dateString = [self dateStringDefaultValue];
        }
        
        return dateString;
    }
    
    return [self dateStringDefaultValue];
}

- (NSString *)convertDateToString:(NSDate *)date format:(NSString *)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    
    NSString *dateString = [self stringValue:[dateFormatter stringFromDate:date]];
    
    if ([dateString isEqualToString:@""]) {
        dateString = [self dateStringDefaultValue];
    }
    
    return dateString;
}

- (NSDate *)convertStringToDate:(NSString *)dateString format:(NSString *)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    
    return [dateFormatter dateFromString:dateString];
}

- (NSString *)dateStringDefaultValue {
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy";
    
    NSString *year = [dateFormatter stringFromDate:date];
    NSString *formattedDate = [NSString stringWithFormat:@"%@-01-01 00:00:00", year];
    
    return formattedDate;
}

- (double)computeTimeDifferenceInSecondsFromDate:(NSString *)fromDate
                                          toDate:(NSString *)toDate
                                      fromFormat:(NSString *)fromFormat
                                        toFormat:(NSString *)toFormat {
    
    NSString *fDateString = [self transformDateString:fromDate fromFormat:fromFormat toFormat:toFormat];
    NSString *tDateString = [self transformDateString:toDate fromFormat:fromFormat toFormat:toFormat];
    
    if (!([fDateString isEqualToString:@""] && [tDateString isEqualToString:@""])) {
        NSDate *fDateObject = [self convertStringToDate:fDateString format:toFormat];
        NSDate *tDateObject = [self convertStringToDate:tDateString format:toFormat];
        
        if (fDateObject != nil && tDateObject != nil) {
            NSTimeInterval secondsDifference = [tDateObject timeIntervalSinceDate:fDateObject];
            return secondsDifference;
        }
    }
    
    return 0;
}

- (double)convertTimeStringToSeconds:(NSString *)timeString {
    NSArray *time = [timeString componentsSeparatedByString:@":"];
    
    if (time.count == 3) {
        int h = [time[0] intValue];
        int m = [time[1] intValue];
        int s = [time[2] intValue];
        
        return (h * 3600) + (m * 60) + s;
    }
    
    return 0;
}

@end
