//
//  DeployedTestPreview.m
//  V-Smart
//
//  Created by Ryan Migallos on 9/28/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "DeployedTestPreview.h"
#import "QuestionListView.h"
#import "QuizResultsController.h"
#import "AppDelegate.h"
#import "RadioButton.h"
#import "MZTimerLabel.h"
#import "MainHeader.h"
#import <QuartzCore/QuartzCore.h>



@interface DeployedTestPreview () <QuestionListViewDelegate, QuizResultsControllerDelegate, UITableViewDataSource, UIScrollViewDelegate>

@property (nonatomic, strong) ResourceManager *rm;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageQuestionHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *imageQuestion;

@property (strong, nonatomic) IBOutlet UILabel *labelTime;
@property (strong, nonatomic) IBOutlet UILabel *labelQuestionNumber;
@property (strong, nonatomic) IBOutlet UILabel *labelQuestion;
@property (strong, nonatomic) IBOutlet UIButton *buttonQuestionList;
@property (strong, nonatomic) IBOutlet UIButton *buttonPrev;
@property (strong, nonatomic) IBOutlet UIButton *buttonNext;
@property (strong, nonatomic) IBOutlet UIButton *buttonSubmit;

@property (strong, nonatomic) IBOutlet UIProgressView *quizProgress;
@property (strong, nonatomic) IBOutlet UILabel *labelProgress;
@property (strong, nonatomic) IBOutlet UIView *multipleChoiceContainer;

@property (strong, nonatomic) NSArray *questions;
@property (strong, nonatomic) NSArray *choices;
//@property (strong, nonatomic) NSMutableArray *radioButtons;
@property (strong, nonatomic) NSMutableOrderedSet *radioButtons;
@property (strong, nonatomic) MZTimerLabel *timerComponent;

@property (strong, nonatomic) NSMutableDictionary *lookupTable;

@property (assign, nonatomic) NSInteger currentIndex;

@end

@implementation DeployedTestPreview

static NSString *const kCellIdentifier = @"cell_id_quiz";


- (NSArray *)generateChoicesFromQuestionObject:(NSManagedObject *)mo {

    NSMutableArray *questionchoices = [NSMutableArray array];
    
    NSSet *set = [mo valueForKey:@"choices"];
    NSArray *items = [set allObjects];
    if (items.count > 0) {
        
        for (NSManagedObject *cmo in items) {
            
            NSString *date_created = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"date_created"] ];
            NSString *date_modified = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"date_modified"] ];
            NSString *answer_id = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"id"] ];
            NSString *is_correct = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"is_correct"] ];
            NSString *is_deleted = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"is_deleted"] ];
            NSString *order_number = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"order_number"] ];
            NSInteger index = [order_number integerValue];
            
            NSString *question_id = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"question_id"] ];
            NSString *suggestive_feedback = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"suggestive_feedback"] ];
            NSString *answer_text = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"text"] ];
            
            NSDictionary *a = @{@"date_created":date_created,
                                @"date_modified":date_modified,
                                @"id":answer_id,
                                @"is_correct":is_correct,
                                @"is_deleted":is_deleted,
                                @"order_number":@(index),
                                @"question_id":question_id,
                                @"suggestive_feedback":suggestive_feedback,
                                @"answer_text":answer_text};
            
            [questionchoices addObject:a];
        }
    }
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"order_number" ascending:YES];
    [questionchoices sortUsingDescriptors: @[descriptor] ];

    
    return questionchoices;
}

- (NSArray *)generateQuestions:(NSManagedObject *)mo {

    NSLog(@"%s : %@", __PRETTY_FUNCTION__, mo.entity.name);
    
    NSMutableArray *questions = [NSMutableArray array];
    
    NSSet *set = [mo valueForKey:@"questions"];
    NSArray *items = [set allObjects];
    if (items.count > 0) {
        
        for (NSInteger i = 0; i < items.count; i++) {
            NSManagedObject *qmo = (NSManagedObject *)items[i];
            NSString *qid = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"id"] ];
            NSString *points = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"points"] ];
            NSString *question_id = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"question_id"] ];
            NSString *question_text = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"question_text"] ];
            NSString *image_url = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"image_url"] ];
            NSString *question_type_id = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"question_type_id"] ];
            NSString *question_type_name = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"question_type_name"] ];
            NSString *quiz_id = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"quiz_id"] ];
            NSString *question_bullet_number = [NSString stringWithFormat:@"%ld", (long)i ];
            NSString *question_answer_text = [NSString stringWithFormat:@"%@", @"answer" ];
            NSString *score = @"0";
            
            NSDictionary *d = @{@"id":qid,
                                @"points":points,
                                @"question_id":question_id,
                                @"question_text":question_text,
                                @"question_type_id":question_type_id,
                                @"question_type_name":question_type_name,
                                @"question_bullet_number":question_bullet_number,
                                @"question_answer_text":question_answer_text,
                                @"score":score,
                                @"quiz_id":quiz_id,
                                @"image_url":image_url,
                                @"mo":qmo};
            
            [questions addObject:d];
        }
    }
    
    return questions;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.rm = [AppDelegate resourceInstance];
    
    self.title = [NSString stringWithFormat:@"%@", [self.quiz_mo valueForKey:@"name"] ];
    self.lookupTable = [NSMutableDictionary dictionary];
    
    NSLog(@"%s: %@", __PRETTY_FUNCTION__, self.quiz_mo.entity.name);
    self.questions = [self generateQuestions:self.quiz_mo];
    
    _currentIndex = 0;
    [self questionWithIndex:_currentIndex];
    
    [self.buttonQuestionList addTarget:self action:@selector(questionListViewer:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonNext addTarget:self action:@selector(cycleQuestions:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonPrev addTarget:self action:@selector(cycleQuestions:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonSubmit addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSTimeInterval time_remaining = [[self.quiz_mo valueForKey:@"time_remaining"] floatValue];
    self.timerComponent = [[MZTimerLabel alloc] initWithLabel:self.labelTime andTimerType:MZTimerLabelTypeTimer];
    [self.timerComponent setCountDownTime:time_remaining]; //** Or you can use [timer3 setCountDownToDate:aDate];
    [self.timerComponent start];
    
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(previewImage:)];
    [singleTap setNumberOfTapsRequired:1];
    [self.imageQuestion addGestureRecognizer:singleTap];

    self.scrollView.minimumZoomScale = 1.0;
    self.scrollView.maximumZoomScale = 5.0;
}

- (void)previewImage:(UIGestureRecognizer *)recognizer {
    CGFloat value = 122;

    if (self.imageQuestionHeightConstraint.constant == 122) {
        value = 350;
    } else {
        value = 122;
    }

    self.imageQuestionHeightConstraint.constant = value;

    [UIView animateWithDuration:0.45 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)retakeExam {
    
    _questions = [self generateQuestions:self.quiz_mo];
    
    NSTimeInterval time_remaining = [[self.quiz_mo valueForKey:@"time_remaining"] floatValue];
    [self.timerComponent reset];
    [self.timerComponent setCountDownTime:time_remaining];
    [self.timerComponent start];
    _currentIndex = 0;
    [self questionWithIndex:_currentIndex];
}

- (void)questionListViewer:(id)sender {
    
//    [self performSegueWithIdentifier:@"showQuestionListBox" sender:self];
}

- (void)cycleQuestions:(id)sender {
    
    UIButton *b = (UIButton *)sender;
    
    if ( b == self.buttonNext) {
        _currentIndex++;
        [self questionWithIndex:_currentIndex];
    }
    
    if (b == self.buttonPrev) {
        _currentIndex--;
        [self questionWithIndex:_currentIndex];
    }
}

- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageQuestion;
}

- (void)questionWithIndex:(NSInteger)index {
    
    NSLog(@"questionWithIndex : %ld", (long)index);
    
    //OUT OF BOUNDS HANDLING
    if (index < 0) {
        index = 0;
    }
    
    if (index >= _questions.count) {
        index = _questions.count - 1;
    }
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //BUTTON HANDLING
        wo.buttonPrev.hidden = (index == 0) ? YES : NO;
        wo.buttonNext.hidden = (index < wo.questions.count) ? NO : YES;
        if (index == (wo.questions.count-1) ) {
            wo.buttonSubmit.hidden = NO;
            wo.buttonNext.hidden = YES;
        }
        if (index < (wo.questions.count-1) ) {
            wo.buttonSubmit.hidden = YES;
        }
        
        wo.buttonSubmit.hidden = YES;
        
        //QUESTION CREATION
        if ([wo.questions count] > 0) {
            
            NSDictionary *q = (NSDictionary *)wo.questions[index];
            NSManagedObject *qmo = (NSManagedObject *)q[@"mo"];
            
            [wo updateQuizProgress:_questions];
            
            //QUESTION ID
            NSString *question_id = [NSString stringWithFormat:@"%@", q[@"question_id"] ];
            NSLog(@"question id : %@", question_id);
            
            //QUESTION ANSWER TEXT
            NSString *question_answer_text = [NSString stringWithFormat:@"%@", q[@"question_answer_text"] ];
            
            //BULLET NUMBER
            NSString *question_bullet_number = [NSString stringWithFormat:@"%@", q[@"question_bullet_number"] ];
            NSInteger bullet_number = [question_bullet_number integerValue] + 1;
            wo.labelQuestionNumber.text = [NSString stringWithFormat:@"%ld", (long)bullet_number];
            
//            //ACTUAL QUESTION
//            wo.labelQuestion.text = [NSString stringWithFormat:@"%@", q[@"question_text"] ];
            
            //ACTUAL QUESTION
            NSString *question_text = [NSString stringWithFormat:@"%@", q[@"question_text"] ];
            if (question_text) {
                NSArray *split = [question_text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                split = [split filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
                question_text = [NSString stringWithFormat:@"%@", [split componentsJoinedByString:@" "] ];
                
//                question_text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pulvinar urna a mattis congue. Donec enim urna, rutrum sed vehicula nec, efficitur sit amet urna. Duis condimentum viverra justo sit amet hendrerit. Suspendisse potenti. Sed tempus, tortor quis ornare volutpat, justo quam commodo sem, blandit semper urna dui in libero. Curabitur sit amet facilisis eros. Phasellus leo ligula, rutrum sed ullamcorper vitae, suscipit rutrum augue. Sed pretium risus nec metus laoreet elementum. Pellentesque et purus a nisi ultrices volutpat ut sed magna. Vivamus euismod ac lacus at finibus.In pulvinar imperdiet elit sed faucibus. Phasellus convallis sit amet eros quis commodo. Quisque id aliquet mi. Sed id tristique nibh. Donec varius risus quis vulputate ornare. Donec facilisis in nulla sed placerat. In pellentesque eros eros, quis facilisis velit pulvinar sit amet. Fusce interdum risus vitae dictum fermentum. Donec accumsan elit massa, at pretium sapien suscipit non. Fusce non velit ac arcu efficitur venenatis. Nunc vitae iaculis dui. Donec rutrum, mauris posuere vulputate pulvinar, orci erat congue lacus, id accumsan odio odio nec nisi. Nulla facilisi. Vivamus turpis turpis, gravida eget felis vulputate, elementum consectetur urna. Nam sagittis faucibus erat ut aliquet. Maecenas ac leo erat.Donec diam ipsum, malesuada eu facilisis non, dignissim sit amet tortor. Ut ac sapien dolor. Etiam et nibh pellentesque, finibus magna non, rutrum turpis. Morbi tempor pharetra vehicula. Nulla et neque quis nunc egestas cursus. Integer facilisis, tellus dictum tempus aliquam, nisi nibh faucibus magna, quis scelerisque quam elit ut nibh. Duis vestibulum sit amet nibh interdum ullamcorper. Integer fringilla augue nec dolor malesuada tristique.";
    
                wo.labelQuestion.text = question_text;
                
            }
            
            CGFloat labelWidth = wo.labelQuestion.frame.size.width;
            [wo.labelQuestion setNumberOfLines:0];
            [wo.labelQuestion setPreferredMaxLayoutWidth:labelWidth];

            NSString *image_url = [NSString stringWithFormat:@"%@", q[@"image_url"] ];
            
            __weak typeof(self) weakObject = self;
            [self.activityIndicator stopAnimating];
            if ([image_url isEqualToString:@"<null>"] || [image_url isEqualToString:@"(null)"] || [image_url isEqualToString:@"null"] || !(image_url.length > 0)) {
                weakObject.imageQuestionHeightConstraint.constant = 0;
            } else {
                [self.activityIndicator startAnimating];
                NSData *image_data = [q[@"image_data"] data];
                if (image_data) {
                    [self.activityIndicator stopAnimating];
                    weakObject.imageQuestion.image = [UIImage imageWithData:image_data];
                } else {

//                    NSDictionary *data = @{@"quiz_id":self.quizid, @"image_url":image_url};
                    [self.rm downloadImageTestPreview:image_url dataBlock:^(NSData *binary) {
                        if (binary) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                UIImage *image = [UIImage imageWithData:binary];
                                if (image) {
                                    NSLog(@"IMAGE IS NOT NIL");
                                } else {
                                    NSLog(@"IMAGE IS NIL!");
                                }
                                weakObject.imageQuestion.image = image;
                                [weakObject.activityIndicator stopAnimating];
                            });
                        }
                    }];
                }
                weakObject.imageQuestionHeightConstraint.constant = 122;
            }
            
            
            //MULTIPLE CHOICES
            wo.choices = [wo generateChoicesFromQuestionObject:qmo];
            
            if (wo.choices.count > 0) {
                
                if (wo.radioButtons.count > 0) {
                    
                    for (RadioButton *b in wo.radioButtons) {
                        [b removeFromSuperview];
                    }
                    
                    [wo.radioButtons removeAllObjects];
                }
                
//                wo.radioButtons = [NSMutableArray arrayWithCapacity:wo.choices.count];
                wo.radioButtons = [NSMutableOrderedSet orderedSetWithCapacity:wo.choices.count];
                
                CGRect radioButtonFrame = CGRectMake(10, 10, 500, 30);
                for (NSDictionary *c in wo.choices) {
                    
                    NSString *answer_text = [NSString stringWithFormat:@"%@", c[@"answer_text"] ];
                    
                    [wo.lookupTable setObject:c forKey:answer_text];//save to lookup table
                    
                    RadioButton *radionButton = [[RadioButton alloc] initWithFrame:radioButtonFrame];
                    [radionButton addTarget:wo action:@selector(onRadioButtonValueChanged:) forControlEvents:UIControlEventTouchUpInside];
                    radioButtonFrame.origin.y += 40;
                    [radionButton setTitle:answer_text forState:UIControlStateNormal];
                    [radionButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                    radionButton.titleLabel.font = [UIFont boldSystemFontOfSize:17];
                    [radionButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                    [radionButton setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
                    radionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                    radionButton.titleEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 0);
                    
                    if ([answer_text isEqualToString:question_answer_text]) {
                        [radionButton setSelected:YES];
                    }
                    
                    [wo.multipleChoiceContainer addSubview:radionButton];
                    [radionButton layoutSubviews];
                    [wo.radioButtons addObject:radionButton];
                }
                
//                [wo.radioButtons[0] setGroupButtons:wo.radioButtons]; // Setting buttons into the group
                [[wo.radioButtons array][0] setGroupButtons:[wo.radioButtons array]]; // Setting buttons into the group
                
                
                [wo.multipleChoiceContainer setNeedsDisplay];
            }
            
        }
        
    });
    
}

- (void)updateQuizProgress:(NSArray *)list {
    
    NSMutableArray *answers = [NSMutableArray array];
    for (NSDictionary *d in list) {
        NSString *score = [NSString stringWithFormat:@"%@", d[@"score"] ];
        if (![score isEqualToString:@"(null)"]) {
            [answers addObject:score];
        }
    }
    
    NSNumber *questionCount = [NSNumber numberWithUnsignedInteger: self.questions.count ];
    NSNumber *answerCount = [NSNumber numberWithUnsignedInteger: answers.count ];
    double percent = [answerCount doubleValue] / [questionCount doubleValue];
    self.quizProgress.progress = percent;
    
    NSString *progressText = [NSString stringWithFormat:@"%@ of %@ questions answered", answerCount, questionCount];
    self.labelProgress.text = progressText;
}

- (void)onRadioButtonValueChanged:(id)sender {
//    RadioButton *b = (RadioButton *)sender;
    
    if (self.lookupTable) {
        
//        NSString *answer_text = b.titleLabel.text;
//        NSDictionary *c = [self.lookupTable valueForKey:answer_text];
//        NSString *question_id = [NSString stringWithFormat:@"%@", c[@"question_id"] ];
//        NSString *question_answer_text = [NSString stringWithFormat:@"%@", c[@"answer_text"] ];
//        NSString *is_correct = [NSString stringWithFormat:@"%@", c[@"is_correct"] ];
//        NSDictionary *d = @{@"question_id":question_id,
//                            @"question_answer_text":question_answer_text,
//                            @"is_correct":is_correct};
//        
//        __weak typeof(self) weakObject = self;
//        [self.rm setAnswerObject:d doneBlock:^(BOOL status) {
//            weakObject.questions = [weakObject.rm fetchQuestionsForQuizID:weakObject.quizid];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [weakObject updateQuizProgress:weakObject.questions];
//            });
//        }];
        
    }
}

- (void)submitAction:(id)sender {
    
    NSLog(@"GUMANA KA....");
    
    
//    __weak typeof(self) weakObject = self;
//    NSDictionary *d = @{@"userid": self.userid,
//                        @"quizid": self.quizid,
//                        @"courseid": self.courseid,
//                        @"seconds" : [NSNumber numberWithDouble:self.timerComponent.getTimeCounted]
//                        };
//    [self.rm submitQuizForUser:d doneBlock:^(BOOL status) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [weakObject performSegueWithIdentifier:@"showResultBox" sender:self];
//        });
//    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
//    [self customizeNavigationController];
}

- (void)customizeNavigationController {
    
    UIColor *color = UIColorFromHex(0x37B34A);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    } else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

#pragma mark - QuizResultsControllerDelegate

- (void)didFinishTappingButtonType:(NSString *)type {
    
    NSLog(@"button type : %@", type);
    
    if ([type isEqualToString:@"retake"]) {
        [self retakeExam];
    }
    
    if ([type isEqualToString:@"review"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

#pragma mark - QuestionListViewDelegate

- (void)didFinishSelectingQuestionIndex:(NSString *)indexString {
    
    NSInteger indexValue = [indexString integerValue];
    NSLog(@"index value : %ld", (long)indexValue);
    [self questionWithIndex:indexValue];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
//    if ([segue.identifier isEqualToString:@"showQuestionListBox"]) {
//        QuestionListView *questionListView = (QuestionListView *)segue.destinationViewController;
//        questionListView.quizid = [NSString stringWithFormat:@"%@", self.quizid];
//        questionListView.delegate = self;
//    }
//    
//    if ([segue.identifier isEqualToString:@"showResultBox"]) {
//        QuizResultsController *resultsController = (QuizResultsController *)segue.destinationViewController;
//        resultsController.userid = [NSString stringWithFormat:@"%@", self.userid];
//        resultsController.quizid = [NSString stringWithFormat:@"%@", self.quizid];
//        resultsController.courseid = [NSString stringWithFormat:@"%@", self.courseid];
//        resultsController.delegate = self;
//    }
}

@end


//@property (strong, nonatomic) IBOutlet UITableView *tableView;
//
//@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
//@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageQuestionHeightConstraint;
//
//@property (weak, nonatomic) IBOutlet UIImageView *imageQuestion;
//@property (strong, nonatomic) IBOutlet UILabel *labelTime;
//@property (strong, nonatomic) IBOutlet UILabel *labelQuestionNumber;
//@property (strong, nonatomic) IBOutlet UILabel *labelQuestion;
//@property (strong, nonatomic) IBOutlet UIButton *buttonQuestionList;
//@property (strong, nonatomic) IBOutlet UIButton *buttonPrev;
//@property (strong, nonatomic) IBOutlet UIButton *buttonNext;
//@property (strong, nonatomic) IBOutlet UIButton *buttonSubmit;
//
//@property (strong, nonatomic) IBOutlet UIProgressView *quizProgress;
//@property (strong, nonatomic) IBOutlet UILabel *labelProgress;
//@property (strong, nonatomic) IBOutlet UIView *multipleChoiceContainer;
//
//@property (strong, nonatomic) ResourceManager *rm;
//
//@property (strong, nonatomic) NSDictionary *quizData;
//@property (strong, nonatomic) NSArray *questions;
//@property (strong, nonatomic) NSArray *choices;
////@property (strong, nonatomic) NSMutableArray *radioButtons;
//@property (strong, nonatomic) NSMutableOrderedSet *radioButtons;
//
//@property (strong, nonatomic) MZTimerLabel *timerComponent;
//@property (strong, nonatomic) NSMutableDictionary *lookupTable;
//@property (assign, nonatomic) NSInteger currentIndex;
//@property (strong, nonatomic) NSArray *choice_item_objects;
//
//@property (assign, nonatomic) BOOL isSubmitted;
//@property (assign, nonatomic) BOOL isReview;
//
//@end
//
//@implementation DeployedTestPreview
//
//static NSString *const kCellIdentifier = @"question_answer_cell_identifier";
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//    self.title = [NSString stringWithFormat:@"%@", self.quizName];
//    self.lookupTable = [NSMutableDictionary dictionary];
//    
//    self.rm = [AppDelegate resourceInstance];
//    
////    self.quizData = [self.rm fetchQuizDataForQuizID:self.quizid];
////    self.questions = [self.rm fetchQuestionsForQuizID:self.quizid];
//    self.questions = [self.quiz_mo valueForKey:@"questions"];//[self.rm fetchQuestionsForQuizID:self.quizid];
//    
////    self.currentIndex = 0;
//////    [self questionWithIndex:self.currentIndex];
////    
//////    [self.buttonQuestionList addTarget:self action:@selector(questionListViewer:) forControlEvents:UIControlEventTouchUpInside];
//////    [self.buttonNext addTarget:self action:@selector(cycleQuestions:) forControlEvents:UIControlEventTouchUpInside];
//////    [self.buttonPrev addTarget:self action:@selector(cycleQuestions:) forControlEvents:UIControlEventTouchUpInside];
//////    [self.buttonSubmit addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
////    
//////    NSTimeInterval time_remaining = [self.quizData[@"time_remaining"] floatValue];
////    NSTimeInterval time_remaining = [[self.quiz_mo valueForKey:@"time_remaining"] floatValue];//[self.quizData[@"time_remaining"] floatValue];
////    self.timerComponent = [[MZTimerLabel alloc] initWithLabel:self.labelTime andTimerType:MZTimerLabelTypeTimer];
////    [self.timerComponent setCountDownTime:time_remaining]; //** Or you can use [timer3 setCountDownToDate:aDate];
////    [self.timerComponent start];
////    
////    [self.timerComponent startWithEndingBlock:^(NSTimeInterval countTime) {
////        //oh my gosh, it's awesome!!
//////        [self submitAnswers];
////    }];
//    
////    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(previewImage:)];
////    [singleTap setNumberOfTapsRequired:1];
////    [self.imageQuestion addGestureRecognizer:singleTap];
//    
////    self.scrollView.minimumZoomScale = 1.0;
////    self.scrollView.maximumZoomScale = 5.0;
////    
////    self.navigationItem.hidesBackButton = YES;
////    
////    self.isSubmitted = NO;
//}
//
////- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
////    return self.imageQuestion;
////}
////
////- (void)previewImage:(UIGestureRecognizer *)recognizer {
////    CGFloat value = 122;
////    
////    if (self.imageQuestionHeightConstraint.constant == 122) {
////        value = 350;
////    } else {
////        value = 122;
////    }
////    
////    self.imageQuestionHeightConstraint.constant = value;
////    
////    [UIView animateWithDuration:0.45 animations:^{
////        [self.view layoutIfNeeded];
////    }];
////}
//
////- (void)retakeExam {
////    
////    self.isReview = NO;
////    
////    _quizData = [_rm fetchQuizDataForQuizID:self.quizid];
////    _questions = [_rm fetchQuestionsForQuizID:self.quizid];
////    
////    //    NSTimeInterval time_limit = [self.quizData[@"time_limit"] floatValue];
////    //    NSTimeInterval time_limit = self.actual_time_limit;
////    //    [self.timerComponent reset];
////    //    [self.timerComponent setCountDownTime:time_limit];
////    //    [self.timerComponent start];
////    
////    if (self.timerComponent != nil) {
////        [self.timerComponent reset];
////        [self.timerComponent start];
////        self.currentIndex = 0;
////        [self questionWithIndex:self.currentIndex];
////    }
////}
//
////- (void)reviewExam {
////    
////    self.isReview = YES;
////    
////    [self.buttonSubmit setTitle:@"Exit" forState:UIControlStateNormal];
////    [self.buttonSubmit setTitle:@"Exit" forState:UIControlStateHighlighted];
////    
////    if (self.timerComponent != nil) {
////        [self.timerComponent reset];
////        [self.timerComponent pause];
////        self.currentIndex = 0;
////        [self questionWithIndex:self.currentIndex];
////    }
////}
////
////- (void)questionListViewer:(id)sender {
////    
////    [self performSegueWithIdentifier:@"showQuestionListBox" sender:self];
////}
//////
////- (void)cycleQuestions:(id)sender {
////    
////    UIButton *b = (UIButton *)sender;
////    
////    if ( b == self.buttonNext) {
////        self.currentIndex++;
////        [self questionWithIndex:self.currentIndex];
////    }
////    
////    if (b == self.buttonPrev) {
////        self.currentIndex--;
////        [self questionWithIndex:self.currentIndex];
////    }
////}
////
//////- (void)questionWithIndex:(NSInteger)index {
//////
//////    NSLog(@"questionWithIndex : %ld", (long)index);
//////
//////    //OUT OF BOUNDS HANDLING
//////    if (index < 0) {
//////        index = 0;
//////    }
//////
//////    if (index >= self.questions.count) {
//////        index = self.questions.count - 1;
//////    }
//////
//////    __weak typeof(self) wo = self;
//////
//////    dispatch_async(dispatch_get_main_queue(), ^{
//////
//////        //BUTTON HANDLING
//////        wo.buttonPrev.hidden = (index == 0) ? YES : NO;
//////        wo.buttonNext.hidden = (index < wo.questions.count) ? NO : YES;
//////        if (index == (wo.questions.count-1) ) {
//////            wo.buttonSubmit.hidden = NO;
//////        }
//////        if (index < (wo.questions.count-1) ) {
//////            wo.buttonSubmit.hidden = YES;
//////        }
//////
//////
//////        //QUESTION CREATION
//////        if ([wo.questions count] > 0) {
//////
//////            NSDictionary *q = (NSDictionary *)wo.questions[index];
//////
//////            [wo updateQuizProgress:wo.questions];
//////
//////            //QUESTION ID
//////            NSString *question_id = [NSString stringWithFormat:@"%@", q[@"question_id"] ];
//////
//////            //QUESTION ANSWER TEXT
//////            NSString *question_answer_text = [NSString stringWithFormat:@"%@", q[@"question_answer_text"] ];
//////
//////            //BULLET NUMBER
//////            NSString *question_bullet_number = [NSString stringWithFormat:@"%@", q[@"question_bullet_number"] ];
//////            NSLog(@"BULLET STRING : %@", question_bullet_number);
//////
//////            NSInteger bullet_number = [question_bullet_number integerValue] + 1;
//////            wo.labelQuestionNumber.text = [NSString stringWithFormat:@"%ld", (long)bullet_number];
//////
//////            //ACTUAL QUESTION
//////            NSString *question_text = [NSString stringWithFormat:@"%@", q[@"question_text"] ];
//////            if (question_text) {
//////                NSArray *split = [question_text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//////                split = [split filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
//////                question_text = [NSString stringWithFormat:@"%@", [split componentsJoinedByString:@" "] ];
//////                wo.labelQuestion.text = question_text;
//////            }
//////
//////            NSString *image_url = [NSString stringWithFormat:@"%@", q[@"image_url"] ];
//////
//////            __weak typeof(self) weakObject = self;
//////            [self.activityIndicator stopAnimating];
//////            if (image_url.length > 0) {
//////                [self.activityIndicator startAnimating];
//////                NSLog(@"PASOK DITO SA IMAGE URL > 0");
//////                NSData *image_data = [q[@"image_data"] data];
//////                if (image_data) {
//////                    [self.activityIndicator stopAnimating];
//////                    weakObject.imageQuestion.image = [UIImage imageWithData:image_data];
//////                } else {
//////
//////                    NSDictionary *data = @{@"quiz_id":self.quizid, @"image_url":image_url};
//////                        [self.rm downloadImageFromQuizID:data dataBlock:^(NSData *binary) {
//////
//////                            if (binary) {
//////                                dispatch_async(dispatch_get_main_queue(), ^{
//////                                    UIImage *image = [UIImage imageWithData:binary];
//////                                    if (image) {
//////                                        NSLog(@"IMAGE IS NOT NIL");
//////                                    } else {
//////                                        NSLog(@"IMAGE IS NIL!");
//////                                    }
//////                                    weakObject.imageQuestion.image = image;
//////                                    [weakObject.activityIndicator stopAnimating];
//////                                });
//////                            }
//////                        }];
//////                }
//////                weakObject.imageQuestionHeightConstraint.constant = 122;
//////            } else {
//////                weakObject.imageQuestionHeightConstraint.constant = 0;
//////            }
//////            NSLog(@"HERE IS THE IMAGE URL [%@]", image_url);
//////
//////            CGFloat labelWidth = wo.labelQuestion.frame.size.width;
//////            [wo.labelQuestion setNumberOfLines:0];
//////            [wo.labelQuestion setPreferredMaxLayoutWidth:labelWidth];
//////
//////            //MULTIPLE CHOICES
//////            wo.choices = [wo.rm fetchChoicesForQuestionID:question_id];
//////
//////            if (wo.choices.count > 0) {
//////
//////                if (wo.radioButtons.count > 0) {
//////
//////                    for (RadioButton *b in wo.radioButtons) {
//////                        [b removeFromSuperview];
//////                    }
//////
//////                    [wo.radioButtons removeAllObjects];
//////                }
//////
////////                wo.radioButtons = [NSMutableArray arrayWithCapacity:wo.choices.count];
//////                wo.radioButtons = [NSMutableOrderedSet orderedSetWithCapacity:wo.choices.count];
//////
//////                CGRect radioButtonFrame = CGRectMake(0, 0, 30, 30);
//////                //CGRect radioButtonFrame = CGRectMake(10, 10, 300, 30);
//////                for (NSDictionary *c in wo.choices) {
//////
//////                    NSString *answer_text = [NSString stringWithFormat:@"%@", c[@"answer_text"] ];
//////                    [wo.lookupTable setObject:c forKey:answer_text];//save to lookup table
//////
//////                    RadioButton *radionButton = [[RadioButton alloc] initWithFrame:radioButtonFrame];
//////                    [radionButton addTarget:self action:@selector(onRadioButtonValueChanged:) forControlEvents:UIControlEventTouchUpInside];
//////                    radioButtonFrame.origin.y += 40;
//////                    [radionButton setTitle:answer_text forState:UIControlStateNormal];
//////                    [radionButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//////                    radionButton.titleLabel.font = [UIFont boldSystemFontOfSize:17];
//////                    [radionButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
//////                    [radionButton setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
//////                    radionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//////                    radionButton.titleEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 0);
//////
//////                    if ([answer_text isEqualToString:question_answer_text]) {
//////                        [radionButton setSelected:YES];
//////                    }
//////
////////                    [wo.multipleChoiceContainer addSubview:radionButton];
////////                    [radionButton layoutSubviews];
//////                    [wo.radioButtons addObject:radionButton];
//////                }
//////
////////                [wo.radioButtons[0] setGroupButtons:wo.radioButtons]; // Setting buttons into the group
//////                [[wo.radioButtons array][0] setGroupButtons:[wo.radioButtons array]]; // Setting buttons into the group
//////                [self.tableView reloadData];
//////            }
//////        }
//////
//////    });
//////
//////}
////
////- (void)questionWithIndex:(NSInteger)index {
////    
////    NSLog(@"questionWithIndex : %ld", (long)index);
////    
////    //OUT OF BOUNDS HANDLING
////    if (index < 0) {
////        index = 0;
////    }
////    
////    if (index >= self.questions.count) {
////        index = self.questions.count - 1;
////    }
////    
////    __weak typeof(self) wo = self;
////    
////    dispatch_async(dispatch_get_main_queue(), ^{
////        
////        //BUTTON HANDLING
////        wo.buttonPrev.hidden = (index == 0) ? YES : NO;
////        wo.buttonNext.hidden = (index < wo.questions.count) ? NO : YES;
////        if (index == (wo.questions.count-1) ) {
////            wo.buttonSubmit.hidden = NO;
////        }
////        if (index < (wo.questions.count-1) ) {
////            wo.buttonSubmit.hidden = YES;
////        }
////        
////        
////        //QUESTION CREATION
////        if ([wo.questions count] > 0) {
////            
////            NSDictionary *q = (NSDictionary *)wo.questions[index];
////            
////            [wo updateQuizProgress:wo.questions];
////            
////            //QUESTION ID
////            NSString *question_id = [NSString stringWithFormat:@"%@", q[@"question_id"] ];
////            
////            //QUESTION ANSWER TEXT
////            NSString *question_answer_text = [NSString stringWithFormat:@"%@", q[@"question_answer_text"] ];
////            
////            //BULLET NUMBER
////            NSString *question_bullet_number = [NSString stringWithFormat:@"%@", q[@"question_bullet_number"] ];
////            NSLog(@"BULLET STRING : %@", question_bullet_number);
////            
////            NSInteger bullet_number = [question_bullet_number integerValue] + 1;
////            wo.labelQuestionNumber.text = [NSString stringWithFormat:@"%ld", (long)bullet_number];
////            
////            //ACTUAL QUESTION
////            NSString *question_text = [NSString stringWithFormat:@"%@", q[@"question_text"] ];
////            if (question_text) {
////                NSArray *split = [question_text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
////                split = [split filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
////                question_text = [NSString stringWithFormat:@"%@", [split componentsJoinedByString:@" "] ];
////                wo.labelQuestion.text = question_text;
////            }
////            
////            NSString *image_url = [NSString stringWithFormat:@"%@", q[@"image_url"] ];
////            
////            __weak typeof(self) weakObject = self;
////            [self.activityIndicator stopAnimating];
////            // BUG FIX VIBALHP
////            
////            if ([image_url isEqualToString:@"<null>"] || [image_url isEqualToString:@"(null)"] || [image_url isEqualToString:@"null"] || !(image_url.length > 0)) {
////                weakObject.imageQuestionHeightConstraint.constant = 0;
////            } else {
////                [self.activityIndicator startAnimating];
////                NSData *image_data = [q[@"image_data"] data];
////                if (image_data) {
////                    [self.activityIndicator stopAnimating];
////                    weakObject.imageQuestion.image = [UIImage imageWithData:image_data];
////                } else {
////                    
//////                    NSDictionary *data = @{@"quiz_id":self.quizid, @"image_url":image_url};
////                    [self.rm downloadImageTestPreview:image_url dataBlock:^(NSData *binary) {
////                        if (binary) {
////                            dispatch_async(dispatch_get_main_queue(), ^{
////                                UIImage *image = [UIImage imageWithData:binary];
////                                if (image) {
////                                    NSLog(@"IMAGE IS NOT NIL");
////                                } else {
////                                    NSLog(@"IMAGE IS NIL!");
////                                }
////                                weakObject.imageQuestion.image = image;
////                                [weakObject.activityIndicator stopAnimating];
////                            });
////                        }
////                    }];
////                }
////                weakObject.imageQuestionHeightConstraint.constant = 122;
////            }
////            NSLog(@"HERE IS THE IMAGE URL [%@]", image_url);
////            
////            CGFloat labelWidth = wo.labelQuestion.frame.size.width;
////            [wo.labelQuestion setNumberOfLines:0];
////            [wo.labelQuestion setPreferredMaxLayoutWidth:labelWidth];
////            
////            //MULTIPLE CHOICES
//////            wo.choices = [wo.rm fetchChoicesForQuestionID:question_id];
////            wo.choices = [self.questions valueForKey:@"choices"];
////            
////            wo.radioButtons = [NSMutableOrderedSet orderedSetWithCapacity:wo.choices.count];
////            
////            if (wo.choices.count > 0) {
////                
////                for (NSDictionary *c in wo.choices) {
////                    
////                    //                    NSString *answer_text = [NSString stringWithFormat:@"%@", c[@"answer_text"] ];
////                    NSString *answer_text = [NSString stringWithFormat:@"%@", c[@"text"] ];
////                    
////                    [wo.lookupTable setObject:c forKey:answer_text];//save to lookup table
////                    
////                    NSNumber *flag = [NSNumber numberWithBool:NO];
////                    if ([answer_text isEqualToString:question_answer_text]) {
////                        flag = [NSNumber numberWithBool:YES];
////                    }
////                    
////                    NSDictionary *data = @{@"title": answer_text,
////                                           @"selected":flag};
////                    
////                    [wo.radioButtons addObject:data];
////                }
////                
////                [wo.tableView reloadData];
////            }
////        }
////        
////    });
////    
////}
////
////- (void)updateQuizProgress:(NSArray *)list {
////    
////    NSMutableArray *answers = [NSMutableArray array];
////    for (NSDictionary *d in list) {
////        NSString *score = [NSString stringWithFormat:@"%@", d[@"score"] ];
////        if (![score isEqualToString:@"(null)"]) {
////            [answers addObject:score];
////        }
////    }
////    
////    NSNumber *questionCount = [NSNumber numberWithUnsignedInteger: self.questions.count ];
////    NSNumber *answerCount = [NSNumber numberWithUnsignedInteger: answers.count ];
////    double percent = [answerCount doubleValue] / [questionCount doubleValue];
////    self.quizProgress.progress = percent;
////    
////    NSString *progressText = [NSString stringWithFormat:@"%@ of %@ questions answered", answerCount, questionCount];
////    self.labelProgress.text = progressText;
////}
////
////- (void)onRadioButtonValueChanged:(id)sender {
////    RadioButton *b = (RadioButton *)sender;
////    
////    if (self.lookupTable) {
////        
////        NSString *answer_text = b.titleLabel.text;
////        NSDictionary *c = [self.lookupTable valueForKey:answer_text];
////        NSString *question_id = [NSString stringWithFormat:@"%@", c[@"question_id"] ];
////        NSString *question_answer_text = [NSString stringWithFormat:@"%@", c[@"answer_text"] ];
////        NSString *is_correct = [NSString stringWithFormat:@"%@", c[@"is_correct"] ];
////        NSDictionary *d = @{@"question_id":question_id,
////                            @"question_answer_text":question_answer_text,
////                            @"is_correct":is_correct};
////        
////        __weak typeof(self) weakObject = self;
////        [self.rm setAnswerObject:d doneBlock:^(BOOL status) {
////            weakObject.questions = [weakObject.rm fetchQuestionsForQuizID:weakObject.quizid];
////            dispatch_async(dispatch_get_main_queue(), ^{
////                [weakObject updateQuizProgress:weakObject.questions];
////            });
////        }];
////        
////    }
////}
////
////- (void)submitAction:(id)sender {
////    
////    //    if (self.isReview == YES) {
////    //        [self.navigationController popViewControllerAnimated:YES];
////    //    }
////    //
////    //    if (self.isReview == NO) {
//////    [self submitAnswers];
////    //    }
////}
////
////- (void)submitAnswers {
////    
//////    __weak typeof(self) wo = self;
//////    
//////    if (self.timerComponent != nil) {
//////        [self.timerComponent pause];
//////        NSDictionary *d = @{@"userid": self.userid,
//////                            @"quizid": self.quizid,
//////                            @"courseid": self.courseid,
//////                            @"seconds" : [NSNumber numberWithDouble:self.timerComponent.getTimeCounted]
//////                            };
//////        
//////        [self.rm submitQuizForUser:d doneBlock:^(BOOL status) {
//////            
//////            
//////            if (status == YES) {
//////                wo.isSubmitted = YES;
//////            }
//////            
//////            dispatch_async(dispatch_get_main_queue(), ^{
//////                [wo performSegueWithIdentifier:@"showResultBox" sender:self];
//////                //                if ([wo.delegate respondsToSelector:@selector(didFinishSubmittingQuizAnswersForData:)]) {
//////                //                    [wo.delegate didFinishSubmittingQuizAnswersForData:d];
//////                //                }
//////            });
//////        }];
//////    }
////}
////
////- (void)didReceiveMemoryWarning {
////    [super didReceiveMemoryWarning];
////    // Dispose of any resources that can be recreated.
////}
////
////- (void)viewWillAppear:(BOOL)animated {
////    [super viewWillAppear:animated];
////    [self customizeNavigationController];
////}
////
////- (void)viewWillDisappear:(BOOL)animated {
////    [super viewWillDisappear:animated];
////    
////    //    [self tearDownObserver];
////    [self.timerComponent timerInvalidation];
////}
////
////- (void)customizeNavigationController {
////    
////    UIColor *color = UIColorFromHex(0x37B34A);
////    
////    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
////        // iOS 6.1 or earlier
////        self.navigationController.navigationBar.tintColor = color;
////    } else {
////        // iOS 7.0 or later
////        self.navigationController.navigationBar.barTintColor = color;
////        self.navigationController.navigationBar.translucent = NO;
////    }
////    
////    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
////}
////
////#pragma mark - QuizResultsControllerDelegate
////
//////- (void)didFinishTappingButtonType:(NSString *)type {
//////    
//////    NSLog(@"button type : %@", type);
//////    
//////    if ([type isEqualToString:@"retake"]) {
//////        [self.navigationController popViewControllerAnimated:YES];
//////        [self retakeExam];
//////    }
//////    
//////    if ([type isEqualToString:@"review"]) {
//////        [self.navigationController popViewControllerAnimated:YES];
//////        [self reviewExam];
//////    }
//////    
//////    if ([type isEqualToString:@"Quiz_List"]) {
//////        
//////        NSArray *controller_list = [self.navigationController viewControllers];
//////        for (UIViewController *vc in controller_list) {
//////            NSString *vc_title = vc.title;
//////            NSLog(@"view controller names : %@", vc_title);
//////            if ([vc_title isEqualToString:@"Quiz List"]) {
//////                [self.navigationController popToViewController:vc animated:YES];
//////                break;
//////            }
//////        }
//////    }
//////}
////
////#pragma mark - QuestionListViewDelegate
////
//////- (void)didFinishSelectingQuestionIndex:(NSString *)indexString {
//////    
//////    NSInteger indexValue = [indexString integerValue];
//////    NSLog(@"index value : %ld", (long)indexValue);
//////    [self questionWithIndex:indexValue];
//////}
////
////#pragma mark - Navigation
////
////// In a storyboard-based application, you will often want to do a little preparation before navigation
//////- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//////    
//////    if ([segue.identifier isEqualToString:@"showQuestionListBox"]) {
//////        QuestionListView *questionListView = (QuestionListView *)segue.destinationViewController;
//////        questionListView.quizid = [NSString stringWithFormat:@"%@", self.quizid];
//////        questionListView.delegate = self;
//////    }
//////    
//////    if ([segue.identifier isEqualToString:@"showResultBox"]) {
//////        QuizResultsController *resultsController = (QuizResultsController *)segue.destinationViewController;
//////        resultsController.userid = [NSString stringWithFormat:@"%@", self.userid];
//////        resultsController.quizid = [NSString stringWithFormat:@"%@", self.quizid];
//////        resultsController.courseid = [NSString stringWithFormat:@"%@", self.courseid];
//////        resultsController.delegate = self;
//////    }
//////}
////
////- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
////    return 1;
////}
////
////- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
////    
////    return [self.radioButtons count];
////}
////
////- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
////    
////    NSInteger row = indexPath.row;
////    NSArray *items = [self.radioButtons array];
////    
////    NSDictionary *data = items[row];
////    NSString *title_string = data[@"title"];
////    BOOL state = [data[@"selected"] boolValue];
////    
////    TestPreviewAnswerCell *cell = (TestPreviewAnswerCell *)[tableView dequeueReusableCellWithIdentifier:kCellIdentifier
////                                                                                         forIndexPath:indexPath];
////    cell.selectionStyle = UITableViewCellSelectionStyleNone;
////    cell.answerLabel.text = title_string;
////    cell.buttonContainer.selected = state;
////    
////    return cell;
////}
////
////- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
////    
////    if (self.isReview == NO) {
////        
////        if (self.lookupTable) {
////            
////            NSInteger row = indexPath.row;
////            NSArray *items = [self.radioButtons array];
////            
////            NSDictionary *data = items[row];
////            NSString *answer_text = data[@"title"];
////            NSDictionary *c = [self.lookupTable valueForKey:answer_text];
////            
////            NSString *question_id = [NSString stringWithFormat:@"%@", c[@"question_id"] ];
////            NSString *question_answer_text = [NSString stringWithFormat:@"%@", c[@"answer_text"] ];
////            NSString *is_correct = [NSString stringWithFormat:@"%@", c[@"is_correct"] ];
////            NSDictionary *d = @{@"question_id":question_id,
////                                @"question_answer_text":question_answer_text,
////                                @"is_correct":is_correct};
////            
////            __weak typeof(self) wo = self;
////            [self.rm setAnswerObject:d doneBlock:^(BOOL status) {
////                wo.questions = [wo.rm fetchQuestionsForQuizID:wo.quizid];
////                dispatch_async(dispatch_get_main_queue(), ^{
////                    [wo questionWithIndex:wo.currentIndex];
////                    
////                    [wo updateQuizProgress:wo.questions];
////                });
////            }];
////            
////        }
////    }
////    
////}
////
////- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
////    return UITableViewAutomaticDimension;
////}
////
////- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
////    return UITableViewAutomaticDimension;
////}
//
//@end

