//
//  QuestionCreateController.m
//  V-Smart
//
//  Created by Ryan Migallos on 8/7/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "QuestionCreateController.h"
#import "QuestionHeader.h"
#import "TestGuruDataManager.h"
#import "QuestionEssayTableViewCell.h"
#import "QuestionShortAnswerTableViewCell.h"
#import "QuestionTrueFalseTableViewCell.h"
#import "QuestionMultipleChoiceTableViewCell.h"
#import "TestGuruOptionMenu.h"

@interface QuestionCreateController () <NSFetchedResultsControllerDelegate, UITableViewDelegate, UIPopoverControllerDelegate, UITextFieldDelegate, OptionMenuDelegate, UIAlertViewDelegate>


@property (nonatomic, strong) TestGuruDataManager *tm;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UIButton *addItemButton;
@property (strong, nonatomic) IBOutlet UILabel *questionTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *numberOfItemsLabel;
@property (strong, nonatomic) IBOutlet UITextField *typeField;
@property (strong, nonatomic) IBOutlet UITextField *itemField;
@property (strong, nonatomic) IBOutlet UIButton *typeButton;
@property (strong, nonatomic) NSDictionary *type;

@property (strong, nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) NSString *question_type_id;

@property (strong, nonatomic) IBOutlet QuestionHeader *headerView;

@end

@implementation QuestionCreateController

static NSString *kEssayCellIdentifier = @"essay_cell_identifier";
static NSString *kShortAnswerCellIdentifier = @"shortanswer_cell_identifier";
static NSString *kTrueFalseCellIdentifier = @"truefalse_cell_identifier";
static NSString *kMultipleChoiceCellIdentifier = @"multiplechoice_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.allowsSelection = NO;
    
    self.tm = [TestGuruDataManager sharedInstance];
    self.managedObjectContext = self.tm.mainContext;
    
    // Uncomment the following line to preserve selection between presentations.
    self.questionTypeLabel.text = NSLocalizedString(@"Question Types", nil);
    self.numberOfItemsLabel.text = NSLocalizedString(@"Number of Items", nil);
    
    [self setDefaultValues];
    
    [self registerCellObjectsInTableView:self.tableView];
    [self.typeButton addTarget:self action:@selector(actionOptionMenuButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.addItemButton addTarget:self action:@selector(addQuestionItem:) forControlEvents:UIControlEventTouchUpInside];
    
    [self setupRightBarButton];
    [self setupOptionMenu];
}

- (void)setDefaultValues {
    
    [self.tm clearContentsForEntity:kQuestionEntity predicate:nil];
    
    self.typeField.userInteractionEnabled = NO;
    self.typeField.text = @"";
    self.itemField.text = @"1";
    self.itemField.delegate = self;
}

- (void)registerCellObjectsInTableView:(UITableView *)object {
    
    // Essay
    UINib *essayNib = [UINib nibWithNibName:@"QuestionEssayTableViewCell" bundle:nil];
    [object registerNib:essayNib forCellReuseIdentifier:kEssayCellIdentifier];
    
    // ShortAnswer
    UINib *shortAnswerNib = [UINib nibWithNibName:@"QuestionShortAnswerTableViewCell" bundle:nil];
    [object registerNib:shortAnswerNib forCellReuseIdentifier:kShortAnswerCellIdentifier];
    
    // True False
    UINib *trueFalseNib = [UINib nibWithNibName:@"QuestionTrueFalseTableViewCell" bundle:nil];
    [object registerNib:trueFalseNib forCellReuseIdentifier:kTrueFalseCellIdentifier];
    
    // Multiple Choice
    UINib *multipleChoiceNib = [UINib nibWithNibName:@"QuestionMultipleChoiceTableViewCell" bundle:nil];
    [object registerNib:multipleChoiceNib forCellReuseIdentifier:kMultipleChoiceCellIdentifier];
}

- (void)setupRightBarButton {
    
    NSString *saveTitle = NSLocalizedString(@"Save", nil);
    SEL saveAction = @selector(saveInfoButtonAction:);
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:saveTitle style:UIBarButtonItemStylePlain target:self action:saveAction];
    self.navigationItem.rightBarButtonItem = saveButton;
}

- (void)setupOptionMenu
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    TestGuruOptionMenu *om = (TestGuruOptionMenu *)[sb instantiateViewControllerWithIdentifier:@"question_option_menu"];
    om.entity = kQuestionTypeEntity;
    om.preferredContentSize = CGSizeMake(200, 180);
    om.delegate = self;
    self.popover = [[UIPopoverController alloc] initWithContentViewController:om];
    self.popover.popoverContentSize = CGSizeMake(200, 180);
    self.popover.delegate = self;
}

//- (NSInteger *)getItemCount:(UITextField *)textField {
//
//    NSInteger count = 1;
//    NSString *item_string = [NSString stringWithFormat:@"%@", textField.text];
//    if (item_string.length > 0 ) {
//
//        NSCharacterSet *nonNumbers = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
//        NSRange r = [item_string rangeOfCharacterFromSet: nonNumbers];
//        BOOL status = (r.location == NSNotFound);
//        
//    }
//    return count;
//}

- (void)selectedOptionMenuForEntity:(NSString *)entity withObject:(NSManagedObject *)object {

    NSMutableDictionary *d = [NSMutableDictionary dictionary];
    NSArray *keys = object.entity.propertiesByName.allKeys;
    for (NSString *k in keys) {
        NSString *value = [NSString stringWithFormat:@"%@", [object valueForKey:k] ];
        [d setValue:value forKey:k];
    }
    self.type = [NSDictionary dictionaryWithDictionary:d];
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        wo.typeField.text = self.type[@"value"];
        [wo.popover dismissPopoverAnimated:YES];
    });
}

- (void)actionOptionMenuButton:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    UIButton *b = (UIButton *)sender;
    [self.popover presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
}

- (void)addQuestionItem:(UIButton *)sender {
    
    NSString *question_type = [NSString stringWithFormat:@"%@", self.typeField.text ];
    
    if (question_type.length > 0) {
        NSString *question_type_id = [NSString stringWithFormat:@"%@", self.type[@"id"] ];
        NSString *item_count = [NSString stringWithFormat:@"%@", self.itemField.text];
        NSMutableDictionary *object = [self.headerView getValues];
        [object setValue:question_type_id forKey:@"question_type_id"];
        [self.tm insertQuestionWithItemCount:item_count object:object doneBlock:^(BOOL status) {
            if (status) {
                NSLog(@"Inserted %@ questions", item_count);
            }
        }];
    }
    
    if (question_type.length == 0) {
        
        NSString *message = NSLocalizedString(@"Please select a question type", nil);
        NSString *ok = NSLocalizedString(@"Okay", nil);
        NSString *title_alert = NSLocalizedString(@"Add Error", nil);
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:title_alert
                                                     message:message
                                                    delegate:self
                                           cancelButtonTitle:ok
                                           otherButtonTitles:nil];
        [av show];
    }
}

- (void)saveInfoButtonAction:(id)sender {
    
//    NSLog(@"%s", __PRETTY_FUNCTION__);
//    
//    NSDictionary *data = [self.headerView getValues];
//    NSLog(@"data : %@", data);
//    
//    NSInteger section = 0; // first section index
//    NSArray *objects = [self.fetchedResultsController fetchedObjects];
//    NSInteger count = [objects count];
//    
//    if (count > 0) {
//        
//        BOOL save_status = NO;
//        
//        for (NSInteger i = 0; i < count; i++) {
//            
//            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:section];
//            NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//            NSString *cell_identifier = [self cellIdentifierForObject:mo];
//            NSLog(@"cell identifier : %@", cell_identifier);
//            
//            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//            if ([cell_identifier isEqualToString:kTrueFalseCellIdentifier]) {
//                //True or False
//                QuestionTrueFalseTableViewCell *tfc = (QuestionTrueFalseTableViewCell *)cell;
//                save_status = ([tfc saveContents:data] == nil) ? NO : YES;
//                if (save_status == NO) {
//                    NSLog(@"XXX ERROR TRUE OR FALSE");
//                    break;
//                }
//            }
//            
//            if ([cell_identifier isEqualToString:kShortAnswerCellIdentifier]) {
//                //Fill in the Blanks (Short Answer)
//                QuestionShortAnswerTableViewCell *sac = (QuestionShortAnswerTableViewCell *)cell;
//                save_status = ([sac saveContents:data] == nil) ? NO : YES;
//                if (save_status == NO) {
//                    NSLog(@"XXX SHORT ANSWER");
//                    break;
//                }
//            }
//            
//            if ([cell_identifier isEqualToString:kMultipleChoiceCellIdentifier]) {
//                //Multiple Choice
//                QuestionMultipleChoiceTableViewCell *mcc = (QuestionMultipleChoiceTableViewCell *)cell;
//                save_status = ([mcc saveContents:data] == nil) ? NO : YES;
//                if (save_status == NO) {
//                    NSLog(@"XXX MULTIPLE CHOICE");
//                    break;
//                }
//            }
//            
//            if ([cell_identifier isEqualToString:kEssayCellIdentifier]) {
//                //Essay
//                QuestionEssayTableViewCell *etc = (QuestionEssayTableViewCell *)cell;
//                save_status = ([etc saveContents:data] == nil) ? NO : YES;
//                if (save_status == NO) {
//                    NSLog(@"XXX ESSAY");
//                    break;
//                }
//            }
//            
//        }//for
//
//        // PROCEED POST BODY OPERATION
//        if (save_status) {
//            __weak typeof(self) wo = self;
//            [self.tm requestNewQuestionForContext:self.managedObjectContext doneBlock:^(BOOL status) {
//                if (status) {
//                    [wo.tm requestQuestionListForUser:self.user_id doneBlock:^(BOOL status) {
//                        if (status) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                [wo.navigationController popViewControllerAnimated:YES];
//                            });
//                        }
//
//                    } ];
//                }
//                else {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        NSString *avTitle = NSLocalizedString(@"Save Error", nil);
//                        NSString *butOkay = NSLocalizedString(@"Okay", nil);
//                        NSString *message = NSLocalizedString(@"Incomplete Question", nil);
//                        
//                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:avTitle
//                                                                     message:message
//                                                                    delegate:wo
//                                                           cancelButtonTitle:butOkay
//                                                           otherButtonTitles:nil];
//                        [av show];
//                    });
//                }
//            }];
//        }
//        
//    }//count condition
    
    // ENHANCEMENT#279:
    [self showConfirmationMessage];
    
}

// ENHANCEMENT#279:
// Confirm the user if he/she really wants to save new question
- (void)showConfirmationMessage {
    NSString *title = NSLocalizedString(@"Save Question", nil);
    NSString *message = NSLocalizedString(@"Do you really want to save this question?", nil);
    NSString *yTitle = NSLocalizedString(@"Yes", nil);
    NSString *nTitle = NSLocalizedString(@"No", nil);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:nTitle
                                          otherButtonTitles:yTitle, nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // If yes
    if (buttonIndex == 1) {
        NSLog(@"%s", __PRETTY_FUNCTION__);
        NSDictionary *data = [self.headerView getValues];
        NSLog(@"data : %@", data);
        NSInteger section = 0; // first section index
        NSArray *objects = [self.fetchedResultsController fetchedObjects];
        NSInteger count = [objects count];
        
        if (count > 0) {
            BOOL save_status = NO;
            
            for (NSInteger i = 0; i < count; i++) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:section];
                NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
                NSString *cell_identifier = [self cellIdentifierForObject:mo];
                NSLog(@"cell identifier : %@", cell_identifier);
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                
                if ([cell_identifier isEqualToString:kTrueFalseCellIdentifier]) {
                    //True or False
                    QuestionTrueFalseTableViewCell *tfc = (QuestionTrueFalseTableViewCell *)cell;
                    save_status = ([tfc saveContents:data] == nil) ? NO : YES;
                    if (save_status == NO) {
                        NSLog(@"XXX ERROR TRUE OR FALSE");
                        break;
                    }
                }
                
                if ([cell_identifier isEqualToString:kShortAnswerCellIdentifier]) {
                    //Fill in the Blanks (Short Answer)
                    QuestionShortAnswerTableViewCell *sac = (QuestionShortAnswerTableViewCell *)cell;
                    save_status = ([sac saveContents:data] == nil) ? NO : YES;
                    if (save_status == NO) {
                        NSLog(@"XXX SHORT ANSWER");
                        break;
                    }
                }
                
                if ([cell_identifier isEqualToString:kMultipleChoiceCellIdentifier]) {
                    //Multiple Choice
                    QuestionMultipleChoiceTableViewCell *mcc = (QuestionMultipleChoiceTableViewCell *)cell;
                    save_status = ([mcc saveContents:data] == nil) ? NO : YES;
                    mcc.isCreateAction = YES;
                    if (save_status == NO) {
                        NSLog(@"XXX MULTIPLE CHOICE");
                        break;
                    }
                }
                
                if ([cell_identifier isEqualToString:kEssayCellIdentifier]) {
                    //Essay
                    QuestionEssayTableViewCell *etc = (QuestionEssayTableViewCell *)cell;
                    save_status = ([etc saveContents:data] == nil) ? NO : YES;
                    if (save_status == NO) {
                        NSLog(@"XXX ESSAY");
                        break;
                    }
                }
                
            }//for
            
            // PROCEED POST BODY OPERATION
            if (save_status) {
                __weak typeof(self) wo = self;
                [self.tm requestNewQuestionForContext:self.managedObjectContext doneBlock:^(BOOL status) {
                    if (status) {
                        [wo.tm requestQuestionListForUser:self.user_id doneBlock:^(BOOL status) {
                            if (status) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    // ENHANCEMENT#285
                                    // Echo message after successfullly creating a new question
                                    NSString *butOkay = NSLocalizedString(@"Okay", nil);
                                    NSString *message = NSLocalizedString(@"A new question has been successfully created.", nil);
                                    
                                    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                                                 message:message
                                                                                delegate:wo
                                                                       cancelButtonTitle:butOkay
                                                                       otherButtonTitles:nil];
                                    [av show];
                                    
                                    [wo.navigationController popViewControllerAnimated:YES];
                                });
                            }
                            
                        } ];
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSString *avTitle = NSLocalizedString(@"Save Error", nil);
                            NSString *butOkay = NSLocalizedString(@"Okay", nil);
                            NSString *message = NSLocalizedString(@"There must be at least 2 choices for each question, one of which must be chosen as the correct answer.", nil);
                            
                            UIAlertView *av = [[UIAlertView alloc] initWithTitle:avTitle
                                                                         message:message
                                                                        delegate:wo
                                                               cancelButtonTitle:butOkay
                                                               otherButtonTitles:nil];
                            [av show];
                        });
                    }
                }];
            }
        }//count condition
        
        // ENHANCEMENT#279
        // Added error message if count is <= 0
        else {
            NSString *message = NSLocalizedString(@"Please fill in all required fields for question", nil);
            NSString *ok = NSLocalizedString(@"Okay", nil);
            NSString *title_alert = NSLocalizedString(@"Save Error", nil);
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:title_alert
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:ok
                                               otherButtonTitles:nil];
            [av show];
        }
    }
}

- (void)eraseQuestionAction:(UIButton *)button {
    NSManagedObject *mo = [self managedObjectFromButtonAction:button];
    NSManagedObjectContext *context = mo.managedObjectContext;
    [context deleteObject:mo];
    [context save:nil];
}

- (NSManagedObject *)managedObjectFromButtonAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

#pragma mark - Table View Delegate

- (NSString *)cellIdentifierForObject:(NSManagedObject *)mo {
    
    NSString *type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"question_type_id"] ];
    //    NSLog(@"cell question type : %@", type);
    
    NSString *cell_identifier = kShortAnswerCellIdentifier;
    
    /*
    {
    "id": "1",
    "value": "True or False"
    },
    {
    "id": "9",
    "value": "Short Answer"
    },
    {
        "id": "3",
        "value": "Multiple Choice"
    },
    {
    "id": "6",
    "value": "Essay"
    }
     */
    
    if ([type isEqualToString:@"1"]) {
        //True or False
        cell_identifier = kTrueFalseCellIdentifier;
    }
    
    if ([type isEqualToString:@"2"] || [type isEqualToString:@"9"]) {
        //Fill in the Blanks (Short Answer)
        cell_identifier = kShortAnswerCellIdentifier;
    }
    
    if ([type isEqualToString:@"3"]) {
        //Multiple Choice
        cell_identifier = kMultipleChoiceCellIdentifier;
    }
    
    if ([type isEqualToString:@"6"]) {
        //Essay
        cell_identifier = kEssayCellIdentifier;
    }
    
    return cell_identifier;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *cell_identifier = [self cellIdentifierForObject:mo];
    
    if ([cell_identifier isEqualToString:kTrueFalseCellIdentifier]) {
        //True or False
        return 455.0f;
    }
    
    if ([cell_identifier isEqualToString:kShortAnswerCellIdentifier]) {
        //Fill in the Blanks (Short Answer)
        return 300.0f;
    }
    
    if ([cell_identifier isEqualToString:kMultipleChoiceCellIdentifier]) {
        //Multiple Choice
        return 550.0f;
    }
    
    if ([cell_identifier isEqualToString:kEssayCellIdentifier]) {
        //Essay
        return 300.0f;
    }
    
    return 300;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *cell_identifier = [self cellIdentifierForObject:mo];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier forIndexPath:indexPath];
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *cell_identifier = [self cellIdentifierForObject:mo];
    
    if ([cell_identifier isEqualToString:kTrueFalseCellIdentifier]) {
        //True or False
        QuestionTrueFalseTableViewCell *tfc = (QuestionTrueFalseTableViewCell *)cell;
        [tfc.eraseButton addTarget:self action:@selector(eraseQuestionAction:) forControlEvents:UIControlEventTouchUpInside];
        [tfc choicesForObject:mo];
    }
    
    if ([cell_identifier isEqualToString:kShortAnswerCellIdentifier]) {
        //Fill in the Blanks (Short Answer)
        QuestionShortAnswerTableViewCell *sac = (QuestionShortAnswerTableViewCell *)cell;
        [sac.eraseButton addTarget:self action:@selector(eraseQuestionAction:) forControlEvents:UIControlEventTouchUpInside];
        sac.question = mo;
    }
    
    if ([cell_identifier isEqualToString:kMultipleChoiceCellIdentifier]) {
        //Multiple Choice
        QuestionMultipleChoiceTableViewCell *mcc = (QuestionMultipleChoiceTableViewCell *)cell;
        [mcc.eraseButton addTarget:self action:@selector(eraseQuestionAction:) forControlEvents:UIControlEventTouchUpInside];
        [mcc choicesForObject:mo];
    }
    
    if ([cell_identifier isEqualToString:kEssayCellIdentifier]) {
        //Essay
        QuestionEssayTableViewCell *etc = (QuestionEssayTableViewCell *)cell;
        [etc.eraseButton addTarget:self action:@selector(eraseQuestionAction:) forControlEvents:UIControlEventTouchUpInside];
        etc.question = mo;
    }
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
//    NSPredicate *predicate_type = [self predicateForKeyPath:@"is_" value:@"Multiple Choice"];
//    NSCompoundPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate_type]];
//    [fetchRequest setPredicate:predicate];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:NO];
    [fetchRequest setSortDescriptors:@[date_modified]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// NOTE: This code assumes you have set the UITextField(s)'s delegate property to the object that will contain this code, because otherwise it would never be called.
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.itemField) {
        
        // allow backspace
        if (!string.length)
        {
            return YES;
        }
        
        // Prevent invalid character input, if keyboard is numberpad
        if (textField.keyboardType == UIKeyboardTypeNumberPad)
        {
            if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
            {
                // BasicAlert(@"", @"This field accepts only numeric entries.");
                return NO;
            }
        }
        
        // verify max length has not been exceeded
        NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if (updatedText.length > 4) // 4 was chosen for SSN verification
        {
            // suppress the max length message only when the user is typing
            // easy: pasted data has a length greater than 1; who copy/pastes one character?
            if (string.length > 1)
            {
                // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
            }
            
            return NO;
        }
        
    }
    
    return YES;
}

@end
