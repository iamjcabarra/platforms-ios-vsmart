//
//  TestInformationSegmentController.h
//  V-Smart
//
//  Created by Julius Abarra on 14/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestInformationSegmentController : UIViewController

- (void)swapViewControllers:(NSString *)segueIdentifier;

@end
