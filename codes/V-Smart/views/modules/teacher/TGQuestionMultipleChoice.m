//
//  TGQuestionMultipleChoice.m
//  V-Smart
//
//  Created by Ryan Migallos on 10/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TGQuestionMultipleChoice.h"
#import "TGGenericChoiceItem.h"
#import "TestGuruDataManager.h"

@interface TGQuestionMultipleChoice() <UITextFieldDelegate, UITextViewDelegate, NSFetchedResultsControllerDelegate, TGGenericChoiceItemDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate>

// Localization Items
@property (strong, nonatomic) IBOutlet UILabel *titleIndicator;
@property (strong, nonatomic) IBOutlet UILabel *descriptionIndicator;
@property (strong, nonatomic) IBOutlet UILabel *pointsIndicator;
@property (strong, nonatomic) IBOutlet UILabel *imageIndicator;

// Text View Items
@property (strong, nonatomic) IBOutlet UIView *textViewContainer;
@property (strong, nonatomic) IBOutlet UILabel *descriptionPlaceholder;

// Image Placeholders
@property (strong, nonatomic) IBOutlet UIImageView *imageViewOne;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewThree;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewFour;

// Image Buttons
@property (strong, nonatomic) IBOutlet UIButton *buttonOne;
@property (strong, nonatomic) IBOutlet UIButton *buttonTwo;
@property (strong, nonatomic) IBOutlet UIButton *buttonThree;
@property (strong, nonatomic) IBOutlet UIButton *buttonFour;

// Add Choice Button
@property (strong, nonatomic) IBOutlet UIButton *buttonAddChoice;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) TestGuruDataManager *tm;

@property (strong, nonatomic) NSString *question_id;
@property (strong, nonatomic) NSManagedObject *mo;

@property (nonatomic, strong) UIPopoverController *popOver;

@end

@implementation TGQuestionMultipleChoice

static NSString *kGenericIdentifier  = @"tg_generic_choice_identifier";

- (void)awakeFromNib {
    
    self.tm = [TestGuruDataManager sharedInstance];
    // Initialization code
    
    NSString *title_string = NSLocalizedString(@"Title", nil);
    self.titleIndicator.text = [title_string uppercaseString];
    
    NSString *question_string = NSLocalizedString(@"Question", nil);
    self.descriptionIndicator.text = [question_string uppercaseString];
    
    NSString *points_string = NSLocalizedString(@"Points", nil);
    self.pointsIndicator.text = [points_string uppercaseString];
    
    self.titleField.placeholder = NSLocalizedString(@"Please enter a title", nil);
    
    // Choice
    UINib *genericNib = [UINib nibWithNibName:@"TGGenericChoiceItem" bundle:nil];
    [self.table registerNib:genericNib forCellReuseIdentifier:kGenericIdentifier];
    
    [self.buttonAddChoice addTarget:self action:@selector(addChoiceItemAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.buttonOne addTarget:self action:@selector(presentPopOver:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.buttonTwo addTarget:self action:@selector(presentPopOver:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.buttonThree addTarget:self action:@selector(presentPopOver:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.buttonFour addTarget:self action:@selector(presentPopOver:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)presentPopOver:(id)sender {
    UIButton *b = (UIButton *)sender;
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    [imgPicker setDelegate:self];
    [imgPicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [imgPicker setAllowsEditing:YES];
    
    imgPicker.modalPresentationStyle = UIModalPresentationPopover;
    
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:imgPicker];
    [popover presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    self.popOver = popover;
    
//    let imgPicker = UIImagePickerController()
//    imgPicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
//    
//    imgPicker.modalPresentationStyle = UIModalPresentationStyle.Popover
//    
//    self.presentViewController(imgPicker, animated: true, completion: nil)
//    
//    let popper = imgPicker.popoverPresentationController
//    // returns a UIPopoverPresentationController
//    popper?.barButtonItem = sender as? UIBarButtonItem
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
//    viewController.preferredContentSize = CGSizeMake(200, 100);
}

- (void)setObjectData:(NSManagedObject *)object {
    
    self.mo = object;
    self.question_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"id"] ];
    _fetchedResultsController = nil;
    NSError * error = nil;
    [self.fetchedResultsController performFetch:&error];

    NSLog(@" %s -------------- XXXX ---------------> ", __PRETTY_FUNCTION__);
    
    // Hide place holder
    if (self.descriptionLabel.text.length > 0) {
        self.descriptionPlaceholder.hidden = YES;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.table = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kGenericIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)object atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    TGGenericChoiceItem *cell = (TGGenericChoiceItem *)object;
    cell.contentView.backgroundColor = self.table.backgroundColor;
    
    NSString *choice_text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"text"] ];
    NSString *is_correct = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_correct"] ];
    cell.checkImageView.image = [self checkImageForString:is_correct];
    cell.deleteButton.hidden = self.isTrashHidden;
    
    cell.delegate = self;
    cell.customTextField.text = choice_text;
    cell.mo = mo;
}

- (UIImage *)checkImageForString:(NSString *)string {
    
    BOOL flag = [self isCorrectSelection:string];
    NSString *imageName = (flag) ? @"check_icn_active150" : @"check_icn150";
    return [UIImage imageNamed:imageName];
}

- (BOOL)isCorrectSelection:(NSString *)value {
    return [value isEqualToString:@"100"];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    BOOL isAscending = YES;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kChoiceItemEntity];

    NSPredicate *p1 = [NSPredicate predicateWithFormat:@"question = %@", self.mo];
    NSPredicate *p2 = [self.tm predicateForKeyPath:@"question_id" andValue:self.question_id];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[p1,p2]];
    [fetchRequest setPredicate:predicate];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *order_number = [NSSortDescriptor sortDescriptorWithKey:@"order_number" ascending:isAscending];
    [fetchRequest setSortDescriptors:@[order_number]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.table beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.table insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.table deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.table;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.table endUpdates];
}

- (NSPredicate *)predicateForQuestionObject:(NSManagedObject *)object {
    
    NSString *choice_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"id"] ];
    NSString *order_number = [NSString stringWithFormat:@"%@", [object valueForKey:@"order_number"] ];
    NSString *questionID = [NSString stringWithFormat:@"%@", [object valueForKey:@"question_id"] ];
    
    NSPredicate *p1 = [self.tm predicateForKeyPath:@"id" andValue:choice_id];
    NSPredicate *p2 = [self.tm predicateForKeyPath:@"order_number" andValue:order_number];
    NSPredicate *p3 = [self.tm predicateForKeyPath:@"question_id" andValue:questionID];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[p1,p2,p3]];
    
    return predicate;
}

#pragma mark - TGGenericChoiceItemDelegate
- (void)updatedText:(NSString *)text withManagedObject:(NSManagedObject *)object {
    
    NSPredicate *predicate = [self predicateForQuestionObject:object];
    [self.tm updateQuestionChoiceText:text withPredicate:predicate];
}

- (void)didPressChoiceAction:(TGGenericChoiceAction)action withManagedObject:(NSManagedObject *)object {
    
    NSPredicate *predicate = [self predicateForQuestionObject:object];
    NSLog(@"predicate object : %@", predicate);
    
    if (action == TGGenericChoiceCheck) {
        NSLog(@"-------------------------> CHOICE [CHECKED|UNCHECKED]");
        NSString *is_correct = [NSString stringWithFormat:@"%@", [object valueForKey:@"is_correct"] ];
        BOOL flag = [self isCorrectSelection:is_correct];
        is_correct = (flag) ? @"0" : @"100";
        NSDictionary *userData = @{@"is_correct":is_correct};
        [self.tm updateEntity:kChoiceItemEntity details:userData predicate:predicate];
    }
    
    if (action == TGGenericChoiceDelete) {
        [self.tm deleteChoiceItem:object];
    }
    
    if (action == TGGenericChoiceUpload) {
        //TODO
        NSLog(@"-------------------------> CHOICE IMAGE UPLOAD");
    }
}

- (void)addChoiceItemAction:(UIButton *)sender {
    
    [self.tm insertChoiceItemWithQuestionID:self.question_id];
    
    NSInteger row = ([self.table numberOfRowsInSection:0] - 1);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    [self.table scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}


#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // verify max length has not been exceeded
    NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    NSString *originalText = [NSString stringWithFormat:@"%@", textField.text];
    
    if ( textField == self.pointsField ) {
        
        // allow backspace
        if (!string.length)
        {
            return YES;
        }
        
        NSCharacterSet *numberSet = [NSCharacterSet decimalDigitCharacterSet];
        if ([string rangeOfCharacterFromSet:[numberSet invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        
        if (updatedText.length > 3) // 4 was chosen for SSN verification
        {
            // suppress the max length message only when the user is typing
            // easy: pasted data has a length greater than 1; who copy/pastes one character?
            if (string.length > 1)
            {
                // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
            }
            return NO;
        }
        
        if (self.mo != nil) {

            //QUESTION POINTS STORE TO CORE DATA
            [self updateDetailsWithValue:updatedText andKey:@"points"];
        }
        
    }
    
    if ( textField == self.titleField ) {
        
        if (self.mo != nil) {
            
            //QUESTION TITLE STORE TO CORE DATA
            [self updateDetailsWithValue:updatedText andKey:@"name"];
        }
    }
    
    return YES;
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    
    NSString *updatedText = [NSString stringWithFormat:@"%@", textView.text];
    if (self.mo != nil) {
        //QUESTION DESCRIPTION STORE TO CORE DATA
        [self updateDetailsWithValue:updatedText andKey:@"question_text"];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.descriptionPlaceholder.hidden = YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSString *updated_string = [NSString stringWithFormat:@"%@", textView.text];
    if (updated_string.length > 1) {
        self.descriptionPlaceholder.hidden = YES;
    }
    
    return YES;
}

- (void)updateDetailsWithValue:(NSString *)value andKey:(NSString *)key {
    
    NSString *entity = kQuestionEntity;
    
    NSDictionary *userData = @{key:value};
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:self.question_id];
    [self.tm updateEntity:entity details:userData predicate:predicate];
}

@end