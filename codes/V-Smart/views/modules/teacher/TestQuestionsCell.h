//
//  TestQuestionsCell.h
//  V-Smart
//
//  Created by Julius Abarra on 26/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestQuestionsCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *questionNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionPointsLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionPointsTitleLabel;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxButton;

@end
