//
//  TGQuestionDetailsView.h
//  V-Smart
//
//  Created by Ryan Migallos on 07/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGQuestionDetailsDelegate <NSObject>
@optional
- (void)didFinishCreateOperation:(BOOL)state;
- (void)didFinishDeleteOperationWithObject:(NSManagedObject *)object;
@end

@interface TGQuestionDetailsView : UIViewController

@property (weak,   nonatomic) id <TGQuestionDetailsDelegate> delegate;
@property (assign, nonatomic) BOOL editMode;
@property (strong, nonatomic) NSManagedObject *mo;

@end
