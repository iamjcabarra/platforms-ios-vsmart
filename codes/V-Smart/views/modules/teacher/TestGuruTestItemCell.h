//
//  TestGuruTestItemCell.h
//  V-Smart
//
//  Created by Julius Abarra on 08/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruTestItemCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *testTitleLabel;
//@property (strong, nonatomic) IBOutlet UILabel *testDescriptionLabel;
//@property (strong, nonatomic) IBOutlet UILabel *testGradedAndTestTypeLabel;
//@property (strong, nonatomic) IBOutlet UILabel *testDateCanTakeAndTimeLimit;
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionCountLabel;

//@property (strong, nonatomic) IBOutlet UIButton *editButton;
//@property (strong, nonatomic) IBOutlet UIButton *deleteButton;

- (void)setTestGradedAndTestType:(NSString *)is_graded_id;
- (void)setDifficultyLevel:(NSString *)difficulty_level_id;

@end
