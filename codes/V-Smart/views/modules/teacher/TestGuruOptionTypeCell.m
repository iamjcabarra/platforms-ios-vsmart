//
//  TestGuruOptionTypeCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 10/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruOptionTypeCell.h"
#import "MainHeader.h"

@interface TestGuruOptionTypeCell()
@property (strong, nonatomic) IBOutlet UIView *typeBackground;
@end

@implementation TestGuruOptionTypeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    NSLog(@"%s ---------> ", __PRETTY_FUNCTION__);
    
    UIColor *labelBackgroundColor = UIColorFromHex(0xDFEAF2);
    UIColor *labelTextColor = UIColorFromHex(0x0080FF);
    UIColor *typeBackgroundColor = UIColorFromHex(0x0080FF);
    
    if (selected) {
        UIColor *selectedColor = [self colorForType:self.optionStyle];
        
        labelTextColor = [UIColor whiteColor];
        labelBackgroundColor = selectedColor;
        typeBackgroundColor = selectedColor;
    }
    
    self.typeLabel.textColor = labelTextColor;
    self.typeLabel.backgroundColor = labelBackgroundColor;
    self.typeBackground.backgroundColor = typeBackgroundColor;
    
    //FORCE TO REDRAW
    [self setNeedsDisplay];
}

- (void)showHighlight:(BOOL)state forStyle:(TGOptionTypeStyle)style {
    NSLog(@"%s ----", __PRETTY_FUNCTION__);
    
    UIColor *labelBackgroundColor = UIColorFromHex(0xDFEAF2);
    UIColor *labelTextColor = UIColorFromHex(0x0080FF);
    UIColor *typeBackgroundColor = UIColorFromHex(0x0080FF);

    if (state) {
        UIColor *selectedColor = [self colorForType:style];

        labelTextColor = [UIColor whiteColor]; 
        labelBackgroundColor = selectedColor;
        typeBackgroundColor = selectedColor;
    }

    self.typeLabel.textColor = labelTextColor;
    self.typeLabel.backgroundColor = labelBackgroundColor;
    self.typeBackground.backgroundColor = typeBackgroundColor;

    //FORCE TO REDRAW
    [self setNeedsDisplay];
}

- (UIColor *)colorForType:(TGOptionTypeStyle)option {
    
    UIColor *selectedColor = UIColorFromHex(0x0080FF);
    
    UIColor *easyColor = UIColorFromHex(0x68BF61);
    UIColor *moderateColor = UIColorFromHex(0xFFCD02);
    UIColor *difficultColor = UIColorFromHex(0xF8931F);

    if (option == TGOptionTypeStyleValue1) {
        selectedColor = easyColor;
    }
    
    if (option == TGOptionTypeStyleValue2) {
        selectedColor = moderateColor;
    }
    
    if (option == TGOptionTypeStyleValue3) {
        selectedColor = difficultColor;
    }
    
    return selectedColor;
}

@end
