//
//  TestGuruSortHeaderCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 27/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruSortHeaderCell.h"
#import "MainHeader.h"

@interface TestGuruSortHeaderCell()

@property (strong, nonatomic) UIImage *ascendingImage;
@property (strong, nonatomic) UIImage *descendingImage;

@property (assign, nonatomic) BOOL ascending;

@end

@implementation TestGuruSortHeaderCell

- (void)awakeFromNib {
    // Initialization code
    
    self.ascendingImage = [UIImage imageNamed:@"icn_up_arrow"];
    self.descendingImage = [UIImage imageNamed:@"icn_down_arrow"];
    [self.arrowButton setImage:self.ascendingImage forState:UIControlStateNormal];
    [self.arrowButton setImage:self.descendingImage forState:UIControlStateSelected];
    
    self.arrowButton.userInteractionEnabled = NO;

}

- (void)toggleSortAction:(UIButton *)button {
    
    self.ascending = (button.selected) ? NO : YES;
    self.arrowButton.selected = self.ascending;
}

- (void)showState {

    NSLog(@"----> %s", __PRETTY_FUNCTION__);
    
    //BACKGROUND COLOR
    UIColor *active = UIColorFromHex(0x007AFF);
    UIColor *inActive = UIColorFromHex(0xDFEAF2);
    self.backgroundObject.backgroundColor = (self.selected) ? active : inActive;
    
    //TEXT COLOR
    UIColor *activeText = [UIColor whiteColor];
    UIColor *inActiveText = UIColorFromHex(0x007AFF);
    self.orderLabel.textColor = (self.selected) ? activeText : inActiveText;
    
    if (self.selected) {
        self.ascending = (self.arrowButton.selected) ? NO : YES;
        self.arrowButton.selected = self.ascending;
    }
}

@end
