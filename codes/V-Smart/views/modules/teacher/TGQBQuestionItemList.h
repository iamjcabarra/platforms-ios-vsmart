//
//  TGQBQuestionItemList.h
//  V-Smart
//
//  Created by Ryan Migallos on 10/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGQBQuestionItemListDelegate <NSObject>
@required
- (void)selectedMenuItem:(NSString *)string withObject:(NSManagedObject *)object withIndexPath:(NSIndexPath *)indexPath;
@end

@interface TGQBQuestionItemList : UITableViewController
@property (nonatomic, weak) id <TGQBQuestionItemListDelegate> delegate;

- (void)setupNotification;
- (void)teardownNotification;
@end
