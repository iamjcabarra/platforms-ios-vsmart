//
//  QuestionHeader.m
//  V-Smart
//
//  Created by Ryan Migallos on 8/10/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "QuestionHeader.h"
#import "TestGuruDataManager.h"
#import "TestGuruOptionMenu.h"

@interface QuestionHeader() <OptionMenuDelegate, UIPopoverControllerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *proficiencyLabel;
@property (strong, nonatomic) IBOutlet UILabel *correctFeedbackLabel;
@property (strong, nonatomic) IBOutlet UILabel *learningSkillLabel;
@property (strong, nonatomic) IBOutlet UILabel *wrongFeedbackLabel;
@property (strong, nonatomic) IBOutlet UILabel *generalFeedbackLabel;
@property (strong, nonatomic) IBOutlet UILabel *publishLabel;
@property (strong, nonatomic) IBOutlet UILabel *tagsLabel;
@property (strong, nonatomic) IBOutlet UITextField *proficiencyField;
@property (strong, nonatomic) IBOutlet UITextField *correctFeedbackField;
@property (strong, nonatomic) IBOutlet UITextField *learningSkillField;
@property (strong, nonatomic) IBOutlet UITextField *wrongFeedbackField;
@property (strong, nonatomic) IBOutlet UITextField *generalFeedbackField;
@property (strong, nonatomic) IBOutlet UITextField *tagsField;
@property (strong, nonatomic) IBOutlet UISwitch *isPublish;
@property (strong, nonatomic) IBOutlet UIButton *proficiencyButton;
@property (strong, nonatomic) IBOutlet UIButton *skillButton;

@property (strong, nonatomic) UIPopoverController *proficiencyPopoverControl;
@property (strong, nonatomic) UIPopoverController *skillPopoverControl;

@property (strong, nonatomic) NSDictionary *proficiency;
@property (strong, nonatomic) NSDictionary *skill;

@property (strong, nonatomic) NSManagedObject *mo;

- (IBAction)proficiencyAction:(id)sender;
- (IBAction)skillAction:(id)sender;

@end

@implementation QuestionHeader

- (void) displayDefaultValues {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);

    __weak typeof(self) wo = self;
    dispatch_queue_t queue = dispatch_queue_create("com.vibetech.header.TYPES",DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        
        wo.proficiency = [self defaultValueForMenuEntity:kProficiencyEntity name:nil];
        wo.skill = [self defaultValueForMenuEntity:kSkillEntity name:nil];

        dispatch_async(dispatch_get_main_queue(), ^{
            wo.correctFeedbackField.text = @"";
            wo.wrongFeedbackField.text = @"";
            wo.generalFeedbackField.text = @"";
            wo.proficiencyField.text = self.proficiency[@"value"];
            wo.learningSkillField.text = self.skill[@"value"];
        });
    });
}

- (NSDictionary *)defaultValueForMenuEntity:(NSString *)entity name:(NSString *)string {
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    TestGuruDataManager *tm = [TestGuruDataManager sharedInstance];

    //Default value
    NSString *attribute_string = @"id";
    NSString *attribute_value = @"1";

    if (string) {
        attribute_string = @"value";
        attribute_value = [NSString stringWithFormat:@"%@", string];
    }
    
    NSManagedObject *mo = [tm getEntity:entity
                              attribute:attribute_string
                              parameter:attribute_value
                                context:tm.mainContext];
    NSArray *keys = mo.entity.propertiesByName.allKeys;
    for (NSString *k in keys) {
        NSString *value = [NSString stringWithFormat:@"%@", [mo valueForKey:k] ];
        [data setValue:value forKey:k];
    }
    return [NSDictionary dictionaryWithDictionary:data];
}

- (void)initializeLocalization {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    //self.proficiencyLabel.text = NSLocalizedString(@"Level of Difficulty", nil);
    self.proficiencyLabel.text = NSLocalizedString(@"Difficulty Level", nil);               // ENH#863-jca-01-05-2016
    self.correctFeedbackLabel.text = NSLocalizedString(@"Correct Answer Feedback", nil);
    self.learningSkillLabel.text = NSLocalizedString(@"Learning Skill", nil);
    self.wrongFeedbackLabel.text = NSLocalizedString(@"Wrong Answer Feedback", nil);
    self.generalFeedbackLabel.text = NSLocalizedString(@"General Feedback", nil);
    self.publishLabel.text = NSLocalizedString(@"Publish", nil);
    self.tagsLabel.text = NSLocalizedString(@"Tags", nil);
    
    self.tagsLabel.hidden = YES;
    self.tagsField.hidden = YES;
    
    [self displayDefaultValues];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super initWithCoder:aDecoder]) {
        [self initializeLocalization];
    }
    return self;
}

-  (instancetype)initWithFrame:(CGRect)rect {
    
    self = [super initWithFrame:rect];
    if (self) {
        [self initializeLocalization];
        
//        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"QuestionHeader" owner:self options:nil];
//        [nib[0] setFrame:rect];
//        self = nib[0];
    }
    
    return self;
}

// ENH#863-jca-01-05-2016
// IBOutlets are not set in initWithCoder
- (void)awakeFromNib {
    [self initializeLocalization];
}

- (void)initializePopover:(UIPopoverController *)popover withEntity:(NSString *)entity {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    CGSize size = CGSizeMake(250, 180);
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    TestGuruOptionMenu *om = (TestGuruOptionMenu *)[sb instantiateViewControllerWithIdentifier:@"question_option_menu"];
    om.entity = entity;
    om.preferredContentSize = size;
    om.delegate = self;
    popover = [[UIPopoverController alloc] initWithContentViewController:om];
    popover.popoverContentSize = size;
    popover.delegate = self;
}

- (void)selectedOptionMenuForEntity:(NSString *)entity withObject:(NSManagedObject *)object {
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    NSArray *keys = object.entity.propertiesByName.allKeys;
    for (NSString *k in keys) {
        NSString *value = [NSString stringWithFormat:@"%@", [object valueForKey:k] ];
        [data setValue:value forKey:k];
    }
    
    if ([entity isEqualToString:kProficiencyEntity]) {
        self.proficiencyField.text = [NSString stringWithFormat:@"%@", [object valueForKey:@"value"] ];
        [self.proficiencyPopoverControl dismissPopoverAnimated:YES];
        self.proficiency = [NSDictionary dictionaryWithDictionary:data];
    }
    
    if ([entity isEqualToString:kSkillEntity]) {
        self.learningSkillField.text = [NSString stringWithFormat:@"%@", [object valueForKey:@"value"] ];
        [self.skillPopoverControl dismissPopoverAnimated:YES];
        self.skill = [NSDictionary dictionaryWithDictionary:data];
    }
}

- (IBAction)proficiencyAction:(id)sender {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    UIButton *b = (UIButton *)sender;
    CGSize size = CGSizeMake(250, 180);
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    TestGuruOptionMenu *om = (TestGuruOptionMenu *)[sb instantiateViewControllerWithIdentifier:@"question_option_menu"];
    om.entity = kProficiencyEntity;
    om.preferredContentSize = size;
    om.delegate = self;
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:om];
    popover.popoverContentSize = size;
    popover.delegate = self;
    self.proficiencyPopoverControl = popover;
    
    [popover presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
}

- (IBAction)skillAction:(id)sender {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    UIButton *b = (UIButton *)sender;
    CGSize size = CGSizeMake(250, 180);
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    TestGuruOptionMenu *om = (TestGuruOptionMenu *)[sb instantiateViewControllerWithIdentifier:@"question_option_menu"];
    om.entity = kSkillEntity;
    om.preferredContentSize = size;
    om.delegate = self;
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:om];
    popover.popoverContentSize = size;
    popover.delegate = self;
    self.skillPopoverControl = popover;
    
    [popover presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
}

- (NSMutableDictionary *)getValues {
    
    NSString *proficiency_level_id = [NSString stringWithFormat:@"%@", self.proficiency[@"id"] ];
    NSString *learning_skills_id = [NSString stringWithFormat:@"%@", self.skill[@"id"] ];
    NSString *correct_answer_feedback = [NSString stringWithFormat:@"%@", self.correctFeedbackField.text ];
    NSString *wrong_answer_feedback = [NSString stringWithFormat:@"%@", self.wrongFeedbackField.text ];
    NSString *general_feedback = [NSString stringWithFormat:@"%@", self.generalFeedbackField.text ];
    NSString *is_public = (self.isPublish.on == YES) ? @"1" : @"0";
    NSString *tag_string = [NSString stringWithFormat:@"%@", self.tagsField.text];
    NSMutableArray *tags = [self generateTagsFromString:tag_string];
    
    NSMutableDictionary *d = [@{@"proficiency_level_id": proficiency_level_id,
                                @"learning_skills_id": learning_skills_id,
                                @"correct_answer_feedback": correct_answer_feedback,
                                @"wrong_answer_feedback": wrong_answer_feedback,
                                @"general_feedback": general_feedback,
                                @"is_public": is_public} mutableCopy];
    if ([tags count] > 0) {
        d[@"tags"] = tags;
    }
    
    return d;
}

- (void)setValuesWithManagedObject:(NSManagedObject *)object {

    self.tagsLabel.hidden = YES;
    self.tagsField.hidden = YES;
    
    self.mo = object;
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *proficiency_string = [NSString stringWithFormat:@"%@", [object valueForKey:@"proficiencyLevelName"] ];
        NSLog(@"proficiency : %@", proficiency_string);
        self.proficiencyField.text = proficiency_string;
        self.proficiency = [self defaultValueForMenuEntity:kProficiencyEntity name:proficiency_string];
        NSLog(@"proficiency : %@", self.proficiency);
        
        NSString *skill_string = [NSString stringWithFormat:@"%@", [object valueForKey:@"learningSkillsName"] ];
        NSLog(@"skill : %@", skill_string);
        self.learningSkillField.text = skill_string;
        self.skill = [self defaultValueForMenuEntity:kSkillEntity name:skill_string];
        NSLog(@"skill : %@", self.skill);
        
        NSString *correct_answer_feedback = [NSString stringWithFormat:@"%@", [object valueForKey:@"correct_answer_feedback"] ];
        self.correctFeedbackField.text = correct_answer_feedback;
        NSLog(@"wo.correctFeedbackField.text : %@", self.correctFeedbackField.text);
        
        NSString *wrong_answer_feedback = [NSString stringWithFormat:@"%@", [object valueForKey:@"wrong_answer_feedback"] ];
        self.wrongFeedbackField.text = wrong_answer_feedback;
        NSLog(@"wo.correctFeedbackField.text : %@", self.correctFeedbackField.text);
        
        NSString *general_feedback = [NSString stringWithFormat:@"%@",  [object valueForKey:@"general_feedback"] ];
        self.generalFeedbackField.text = general_feedback;
        NSLog(@"wo.generalFeedbackField.text : %@", self.generalFeedbackField.text);
        
        self.isPublish.on = [self publishPubliclyForQuestion:object];
        NSLog(@"isPublish : %@", (self.isPublish.on) ? @"YES" : @"NO" );
        
        NSString *tags = [self displayTagsForQuestion:object];
        self.tagsField.text = tags;
        NSLog(@"wo.tagsField.text : %@", self.tagsField.text);
    });
    
}

- (BOOL)publishPubliclyForQuestion:(NSManagedObject *)mo {
    BOOL status = NO;
    NSString *is_public = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_public"] ];
    if ([is_public isEqualToString:@"1"]) {
        status = YES;
    }
    return status;
}

- (NSString *)displayTagsForQuestion:(NSManagedObject *)mo {
    
    NSSet *tags_object = [mo valueForKey:@"tags"];
    NSArray *tags = [NSArray arrayWithArray:tags_object.allObjects];
    
    NSMutableString *tag_string = [NSMutableString string];
    for (NSInteger i = 0; i < tags.count; i++) {
        NSManagedObject *t = tags[i];
        NSString *value = [NSString stringWithFormat:@"%@", [t valueForKey:@"tag"] ];
        NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
        value = [value stringByTrimmingCharactersInSet:whitespaces];
        [tag_string appendString:value];
        if (i < (tags.count - 1) ) {
            [tag_string appendString:@","];
        }
    }
    
    return [NSString stringWithFormat:@"%@", tag_string];
}

- (NSMutableArray *)generateTagsFromString:(NSString *)tags {
    NSMutableArray *l = [NSMutableArray array];
    if (tags.length > 0) {
        NSArray *tokens = [tags componentsSeparatedByString:@","];
        if (tokens.count > 0) {

            for (NSInteger i = 0; i < tokens.count; i++) {
                NSString *tag_id = [NSString stringWithFormat:@"%ld", (long)i];
                NSString *tag_name = [NSString stringWithFormat:@"%@", tokens[i] ];
                NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
                tag_name = [tag_name stringByTrimmingCharactersInSet:whitespaces];
                NSDictionary *d = @{@"id":tag_id, @"tag":tag_name};
                [l addObject:d];
            }
        }
    }
    return l;
}

@end
