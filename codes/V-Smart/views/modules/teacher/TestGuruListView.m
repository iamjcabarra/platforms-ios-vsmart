//
//  TestGuruListView.m
//  V-Smart
//
//  Created by Ryan Migallos on 23/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruListView.h"
#import "TestGuruQuestionItemCell.h"
#import "TestGuruEmptyPlaceholder.h"
#import "TestGuruDynamicHeader.h"
#import "TestGuruSortHeader.h"
#import "TestGuruDataManager.h"
#import "QuestionSamplerView.h"
#import "TGQuestionDetailsView.h"
#import "QuestionTypesList.h"
#import "MainHeader.h"
#import "UIImageView+WebCache.h"
#import "TGQBQuestionDetailViewController.h"
#import "TestGuruConstants.h"
#import "HUD.h"


////////////// NSSTRING HTML CATEGORY IMPLEMENTATION //////////////

@interface NSString (HtmlCustomCheck)
- (BOOL)containsHTML;
@end

@implementation NSString (HtmlCustomCheck)

- (BOOL)containsHTML {
    
    NSString *stringcopy = [self copy];

    NSString *expression = @"<[^>]+>";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:stringcopy
                                                        options:0
                                                          range:NSMakeRange(0, [stringcopy length])];
    if (numberOfMatches == 1) {
        return YES;
    }
    
    return NO; //default value
}

@end

////////////// NSSTRING HTML CATEGORY IMPLEMENTATION //////////////


@interface TestGuruListView () <NSFetchedResultsControllerDelegate, UIScrollViewDelegate, UITableViewDelegate, UISearchBarDelegate, TestGuruDynamicHeaderDelegate, TestGuruSortHeaderDelegate, TestGuruEmptyPlaceholderDelegate, QuestionTypesListDelegate, TGQuestionDetailsDelegate, QuestionSamplerDelegate, TGQBQuestionDetailDelegate>

@property (strong, nonatomic) IBOutlet TestGuruEmptyPlaceholder *emptyView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *drawerHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *sortDrawerHeight;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *loadMoreButton;

@property (strong, nonatomic) UIRefreshControl *tableRefreshControl;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) UIButton *filterButton;
@property (strong, nonatomic) UIButton *sortButton;

@property (strong, nonatomic) TestGuruDataManager *tm;
@property (assign, nonatomic) BOOL editOperation;
@property (strong, nonatomic) dispatch_queue_t queue;
@property (strong, nonatomic) NSMutableDictionary *activeDownloads;

@property (strong, nonatomic) NSDateFormatter *formatter;
@property (strong, nonatomic) NSDateFormatter *dateToStringFormatter;
@property (strong, nonatomic) NSString *question_id_new;

@property (assign, nonatomic) NSInteger nextPage;

@property (strong, nonatomic) NSString *current_page;
@property (strong, nonatomic) NSString *total_filtered;
//@property (strong, nonatomic) NSString *total_items;
@property (strong, nonatomic) NSString *total_pages;
@property (strong, nonatomic) NSString *total_parsed;

@property (strong, nonatomic) NSArray *filterIDs;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpaceConstraint;

@property (strong, nonatomic) NSString *user_id;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UIImageView *packageImage;
@property (weak, nonatomic) IBOutlet UILabel *packageName;

@property (assign, nonatomic) BOOL isProcessing;

@property (strong, nonatomic) NSArray *sortDescriptors;
@property (strong, nonatomic) NSDictionary *sortData;

//GROUP BY

@property (strong, nonatomic) NSString *section_id;
@property (strong, nonatomic) NSString *section_header;
@property (assign, nonatomic) BOOL sortOrderAscending;


@property (strong, nonatomic) NSManagedObject *editObject;

@end

@implementation TestGuruListView

static CGFloat kZeroHeight = 0.0f;
//static CGFloat kNormalHeight = 280.0f;
static CGFloat kNormalSortHeight = 132.0f;

static NSString *kQuestionTypeCellIdentifier = @"question_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.queue = dispatch_queue_create("com.vibetech.testguru.LIST",DISPATCH_QUEUE_SERIAL);
    self.tm = [TestGuruDataManager sharedInstance];

    self.user_id = [self.tm loginUser];
    
    self.sortOrderAscending = NO;
    self.total_parsed = @"0";
    self.searchBar.delegate = self;
    self.searchBar.placeholder = NSLocalizedString(@"Search", nil);
    
    UINib *questionItemCellNib = [UINib nibWithNibName:@"TestGuruQuestionItemCell" bundle:nil];
    [self.tableView registerNib:questionItemCellNib forCellReuseIdentifier:kQuestionTypeCellIdentifier];
    
    self.formatter = [[NSDateFormatter alloc] init];
    [self.formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [self.formatter setTimeZone:[NSTimeZone localTimeZone]];
    
    self.dateToStringFormatter = [[NSDateFormatter alloc] init];
    [self.dateToStringFormatter setDateFormat:@"EEE, MMM dd, yyyy hh:mm aa"];
    [self.dateToStringFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    self.question_id_new = @"";//DEFAULT VALUE
    
    [self setupDrawerComponents];
    
    
    [self.loadMoreButton setTitle:NSLocalizedString(@"Load More", nil) forState:UIControlStateNormal];
    [self.loadMoreButton addTarget:self action:@selector(loadMore:) forControlEvents:UIControlEventTouchUpInside];
    
    [self setupRightBarButton];
    
    if (!self.isGroupBy) {
        [self filterQuestionsPageOne];
    } else {
        [self setUpGroupByData];
        [self initiateSeeMore:@"1"];
    }
    [self setupBlankPage];
    
    [self setUpSortDescriptors];
}

- (void)setUpSortDescriptors {
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"sort_date" ascending:self.sortOrderAscending];
    NSSortDescriptor *question_text = [NSSortDescriptor sortDescriptorWithKey:@"question_text" ascending:self.sortOrderAscending];
    NSSortDescriptor *question_id = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:self.sortOrderAscending];
    self.sortDescriptors = @[date_modified,question_text,question_id];
}

- (void)setUpGroupByData {
    
    self.section_id = [NSString stringWithFormat:@"%@", [self.headerObject valueForKey:@"section_id"] ];
    self.section_header = [NSString stringWithFormat:@"%@", [self.headerObject valueForKey:@"section_header"] ];
}

//- (void)initiateNextGroupByPage {
//    NSInteger totalPages = [self.total_pages integerValue];
//    NSInteger currentPage = [self.current_page integerValue];
//    
//    NSString *nextPageStr = [NSString stringWithFormat:@"%ld", (long)currentPage+1];
//    
//    if (totalPages > currentPage) {
//        self.loadMoreButton.enabled = NO;
//        [self initiateFilter:nextPageStr];
//    }
//}

- (void)initiateSeeMore:(NSString *)page {
    if (!self.isProcessing) {
        NSString *indicatorStr = NSLocalizedString(@"Loading", nil);
        [HUD showUIBlockingIndicatorWithText:indicatorStr];
        [self.tm requestQuestionForLinkType:self.linkType withID:self.section_id withPage:page withKeyword:self.searchBar.text withLimit:@"50" dataBlock:^(NSDictionary *data) {
            if (data != nil) {
                self.current_page = data[@"current_page"];
                self.total_filtered = data[@"total_filtered"];
                //                self.total_items = data[@"total_items"];
                self.total_pages = data[@"total_pages"];
                self.total_parsed = data[@"total_parsed"];
            }
            
            self.loadMoreButton.enabled = YES;
            [self checkLoadMore];
            [self checkForEmptyView];
            
            [self updateTitle:self.total_filtered];
            self.isProcessing = NO;
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!self.isGroupBy) {
        self.title = NSLocalizedString(@"Questions", nil);
    }
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *package_type_name = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_NAME]];
        NSString *package_type_image_url = [self.tm stringValue:[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_URL]];
        [wo.packageImage sd_setImageWithURL:[NSURL URLWithString:package_type_image_url]];
        wo.packageName.text = [package_type_name uppercaseString];
    });
    
    [self customizeNavigationController];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
//    NSString *package_type_name = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_NAME]];
////    NSData *package_type_image_data = [NSData dataWithData:[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_DATA] ];
//    NSString *package_type_image_url = [self.tm stringValue:[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_URL]];
//    
//    
//    __weak typeof(self) wo = self;
//    dispatch_async(dispatch_get_main_queue(), ^{
////        wo.packageImage.image = [UIImage imageWithData:package_type_image_data];
//        [wo.packageImage sd_setImageWithURL:[[NSURL alloc] initWithString:package_type_image_url]];
//        wo.packageName.text = [package_type_name uppercaseString];
//    });
}

- (void)updateTitle:(NSString *)countStr {
    NSString *questionTitle = @"";
    NSString *questionCountTitleA = NSLocalizedString(@"Questions", nil);
    NSString *questionCountTitleB = NSLocalizedString(@"Question", nil);
    
    if (!self.isGroupBy) {
        questionTitle = ([countStr integerValue] > 1) ? [questionCountTitleA uppercaseString] : [questionCountTitleB uppercaseString];
    } else {
        questionTitle = self.section_header;
    }
    
    NSString *countTitle = [NSString stringWithFormat:@"%@ (%@)", questionTitle, self.total_filtered];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.title = countTitle;
    });
}

- (void)customizeNavigationController {
    
    UIColor *color = UIColorFromHex(0xF69C42);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    } else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if (!self.isGroupBy) {
    [self initiateFilter:@"1"];
    } else {
        [self initiateSeeMore:@"1"];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([self.searchBar.text isEqualToString:@""]) {
        if (!self.isGroupBy) {
        [self initiateFilter:@"1"];
        } else {
            [self initiateSeeMore:@"1"];
        }
        
    }
}

- (void)loadMoreAction {
    
    NSInteger totalPages = [self.total_pages integerValue];
    NSInteger currentPage = [self.current_page integerValue];
    
    NSString *nextPageStr = [NSString stringWithFormat:@"%ld", (long)currentPage+1];
    
    if (totalPages > currentPage) {
        self.loadMoreButton.enabled = NO;
        if (!self.isGroupBy) {
            [self initiateFilter:nextPageStr];
        } else {
            [self initiateSeeMore:nextPageStr];
        }
    }
}

- (void)initiateFilter:(NSString *)page {
    if (!self.isProcessing) {
        NSString *indicatorStr = NSLocalizedString(@"Loading", nil);
        [HUD showUIBlockingIndicatorWithText:indicatorStr];
        self.isProcessing = YES;
        self.loadMoreButton.hidden = YES;
        
        [self.tm requestFilteredQuestionBankWithPage:page withKeyword:self.searchBar.text filterIDs:self.filterIDs sort:self.sortData dataBlock:^(NSDictionary *data) {
            if (data != nil) {
                self.current_page = data[@"current_page"];
                self.total_filtered = data[@"total_filtered"];
//                self.total_items = data[@"total_items"];
                self.total_parsed = data[@"total_parsed"];
                self.total_pages = data[@"total_pages"];
            }
            
            self.loadMoreButton.enabled = YES;
            [self checkLoadMore];
            [self checkForEmptyView];
            [self updateTitle:self.total_filtered];
            self.isProcessing = NO;
            
            __weak typeof(self) wo = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo reloadFetchedResultsController];
            });
        }];
    }
}

- (void)checkForEmptyView {
    BOOL evHidden = YES;
    BOOL tvHidden = NO;
    
    NSLog(@"TOTAL PARSED [%@]", self.total_parsed);
    
    if ([self.total_parsed integerValue] == 0) {
        evHidden = NO;
        tvHidden= YES;
    } else {
        evHidden = YES;
        tvHidden = NO;
    }
    
    if (![self.searchBar.text isEqual:@""]) {
        [self.emptyView initializeImagePlaceHolderWithType:self.searchBar.text];
    }
    
    if ([[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID] isEqualToString:@"1"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.emptyView.messageLabel.text = NSLocalizedString(@"No available questions yet.", nil);
            self.emptyView.createButton.hidden = YES;
        });
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.emptyView.hidden = evHidden;
        self.tableView.hidden = tvHidden;
    });
    
}

- (void)loadMore:(UIButton *)sender {
    NSLog(@"%s -----> ", __PRETTY_FUNCTION__);
    [self loadMoreAction];
}

- (void)checkLoadMore {
    NSInteger totalPages = [self.total_pages integerValue];
    NSInteger currentPage = [self.current_page integerValue];
    BOOL isHidden = YES;
    
    if (totalPages > currentPage) {
        isHidden = NO;
    } else {
        isHidden = YES;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.loadMoreButton.hidden = isHidden;
        [HUD hideUIBlockingIndicator];
    });
}

- (void)filterQuestionsPageOne {
    self.filterIDs = [self.tm filterOptionsSettedTo:YES];
    NSString *nextPageStr = [NSString stringWithFormat:@"%@", @1];
    [self initiateFilter:nextPageStr];
}

- (UIButton *)buttonWithName:(NSString *)title position:(CGFloat)position action:(SEL)action  {
    
    UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
    b.frame = CGRectMake(position, 0, 80, 44);
    b.showsTouchWhenHighlighted = YES;
    
    [b setTitle:title forState:UIControlStateNormal];
    [b setTitle:title forState:UIControlStateSelected];
    
    UIColor *normalColor = [UIColor whiteColor];
    [b setTitleColor:normalColor forState:UIControlStateNormal];
//    UIColor *selectedColor = [UIColor blueColor];
    [b setTitleColor:normalColor forState:UIControlStateSelected];
    [b addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    return b;
}

- (void)setupDrawerComponents {
    
    //DEFAULT
//    __weak typeof(self) wo = self;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        wo.drawerHeight.constant = 280.0f;
        self.sortDrawerHeight.constant = kZeroHeight;
        self.verticalSpaceConstraint.constant = -280.0f;
//        [self.view setNeedsUpdateConstraints];
//        [self.view layoutIfNeeded];
//    });
}

- (void)showFilterAction:(UIButton *)sender {
    
    NSLog(@"%s -----> ", __PRETTY_FUNCTION__);
    
    [self animateDrawerNavigationWithType:@"FILTER"];
}

- (void)showSortAction:(UIButton *)sender {
    
    NSLog(@"%s -----> ", __PRETTY_FUNCTION__);
    
    [self animateDrawerNavigationWithType:@"SORT"];
}

- (void)animateDrawerNavigationWithType:(NSString *)type {
    
    if ([type isEqualToString:@"FILTER"]) {
        self.sortDrawerHeight.constant = kZeroHeight;
        CGFloat height = (self.verticalSpaceConstraint.constant == -280.0f) ? 0 : -280.0f;
        self.verticalSpaceConstraint.constant = height;
    }

    if ([type isEqualToString:@"SORT"]) {
        self.verticalSpaceConstraint.constant = -280.0f;
        
        CGFloat height = (self.sortDrawerHeight.constant == kZeroHeight) ? kNormalSortHeight : kZeroHeight;
        self.sortDrawerHeight.constant = height;
    }

    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)setupRightBarButton {
    
    UIView *v;
    
    if (!self.isGroupBy) {
        SEL filterAction = @selector(showFilterAction:);
        self.filterButton = [self buttonWithName:@"Filter" position:0 action:filterAction];
        
        SEL sortAction = @selector(showSortAction:);
        self.sortButton = [self buttonWithName:@"Sort" position:80 action:sortAction];
        
        v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 44)];
        [v addSubview:self.filterButton];
        [v addSubview:self.sortButton];
    } else {
        UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
        b.frame = CGRectMake(0, 0, 88, 44);
        b.showsTouchWhenHighlighted = YES;
        
        [b setTitle:@"Oldest" forState:UIControlStateNormal];
        [b setTitle:@"Newest" forState:UIControlStateSelected];
        
        UIColor *normalColor = [UIColor whiteColor];
        [b setTitleColor:normalColor forState:UIControlStateNormal];
        UIColor *selectedColor = [UIColor whiteColor];
        [b setTitleColor:selectedColor forState:UIControlStateSelected];
        [b addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        v = b;
    }

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:v];
}

- (void)sortButtonAction:(UIButton *)sender {
    NSLog(@"%s -----> ", __PRETTY_FUNCTION__);
    
    sender.selected = (sender.selected) ? NO : YES;
    
    self.sortOrderAscending = sender.selected;
    [self setUpSortDescriptors];
    [self reloadFetchedResultsController];
}

- (void)setupBlankPage {
    self.editOperation = NO;
    self.emptyView.delegate = self;
    [self.emptyView initializeImagePlaceHolderWithType:@"TESTGURU_QUESTION"];
}

#pragma mark - Question Types List delegate

- (void)createQuestionTypeObject:(NSDictionary *)dictionary {
    
    self.editOperation = NO;
    
    if (dictionary != nil) {
        self.question_id_new = [NSString stringWithFormat:@"%@", dictionary[@"question_id"] ];
        [self performSegueWithIdentifier:@"SHOW_EDIT_QUESTION_DETAILS_V22" sender:self];
    }
}

#pragma mark - TestGuru Empty Placeholder delegate

- (void)didFinishSelectingOperationObject:(NSDictionary *)dictionary {
    
    NSLog(@"%s OBJECT : %@", __PRETTY_FUNCTION__, dictionary);
    
    [self performSegueWithIdentifier:@"PRESENT_MODAL_QUESTION_TYPE_LIST" sender:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger count = [[self.fetchedResultsController sections] count];

    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    
    return count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo name];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.tableView = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kQuestionTypeCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TestGuruQuestionItemCell *questionCell = (TestGuruQuestionItemCell *)cell;
    [self configureQuestionCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(TestGuruQuestionItemCell *)cell managedObject:(NSManagedObject *)mo
            objectAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *name = [mo valueForKey:@"name"];
    NSString *formatted_date_modified = [mo valueForKey:@"formatted_date_modified"];
    NSString *question_text = [mo valueForKey:@"question_text"];
    NSString *questionTypeName = [mo valueForKey:@"questionTypeName"];
    NSString *is_public = [mo valueForKey:@"is_public"];
    NSString *proficiency_level_id = [mo valueForKey:@"proficiency_level_id"];
    NSString *proficiency_name = [mo valueForKey:@"proficiencyLevelName"];
    NSString *question_type_icon = [mo valueForKey:@"question_type_icon"];
    NSString *package_type_id = [mo valueForKey:@"package_type_id"];
    NSString *question_type_id = [mo valueForKey:@"question_type_id"];
    
    cell.questionImage.image = nil;
    
    NSString *firstImage = [self getFirstImage:mo];
    if (firstImage.length > 0) {
        [cell.questionImage sd_setImageWithURL:[NSURL URLWithString:firstImage] ];
    }
    
    cell.questionTitleLabel.text = name;
    cell.dateLabel.text = formatted_date_modified;
    cell.questionTextLabel.text = question_text;
    cell.questionTextLabel.hidden = NO;
    cell.webView.hidden = YES;
    
    // FILL IN THE BLANKS
    if ([question_type_id isEqualToString:@"2"]) {
        question_text = [question_text stringByReplacingOccurrencesOfString:@"-blank-" withString:@"_______________"];
    }
    
    if ([question_text containsHTML]) {
        cell.questionTextLabel.hidden = YES;
        cell.webView.hidden = NO;
        [cell loadWebViewWithContents:question_text];
    }
    
    cell.questionTextLabel.text = question_text;
    cell.questionTypeLabel.text = [questionTypeName uppercaseString];
    cell.questionDifficultyLabel.text = proficiency_name;
    
    cell.editButton.hidden = YES;
    [cell.editButton addTarget:self action:@selector(editButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.editButton.hidden = ([package_type_id isEqualToString:@"1"]);
    
    [cell setDifficultyLevelID:proficiency_level_id];
    [cell setQuestionTypeIcon:question_type_icon];
    [cell setPublic:is_public];
}

- (void)editButtonAction:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *indicatorStr = NSLocalizedString(@"Loading", nil);
    [HUD showUIBlockingIndicatorWithText:indicatorStr];
    
    self.editOperation = YES;
    __weak typeof(self) wo = self;
    [self.tm deepCopyManagedObject:mo objectBlock:^(NSManagedObject *object) {
        self.editObject = object;
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD hideUIBlockingIndicator];
            [wo performSegueWithIdentifier:@"SHOW_EDIT_QUESTION_DETAILS_V22" sender:self];
        });
    }];
}

- (void)deleteButtonAction:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self initiateDelete:mo];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"SAMPLE_QUESTION_VIEW" sender:self];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_ID] isEqualToString:@"1"]) {
        return NO;
    }

    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//        [self.tm requestRemoveQuestion:mo doneBlock:nil];
        [self initiateDelete:mo];        
    }
}

- (void)initiateDelete:(NSManagedObject *)mo {
    NSString *questionTitle = [NSString stringWithFormat:@"%@", [mo valueForKey:@"name"] ];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete", nil)
                                                                             message:NSLocalizedString(@"Are you sure you want to delete this item?", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *yesAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               [self.tm requestRemoveQuestion:mo doneBlock:^(BOOL status) {
                   NSString *message = (status) ? NSLocalizedString(@"Successfully deleted", nil) : NSLocalizedString(@"Failed to deleted", nil);
                   [self showSuccessDelete:message withQuestionTitle:questionTitle];
                                                               }];
                                                           }];
    
    UIAlertAction *noAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:nil];
    [alertController addAction:yesAlertAction];
    [alertController addAction:noAlertAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showSuccessDelete:(NSString *)message withQuestionTitle:(NSString *)question_title {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:[NSString stringWithFormat:@"%@ %@", message, question_title]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Okay", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:nil];
    [alertController addAction:cancelAction];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

#pragma mark - Fetched results controller

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (NSFetchedResultsController *)fetchedResultsController
{
//    BOOL isAscending = self.sortOrderAscending;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionBankEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Edit the sort key as appropriate.
//    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"sort_date" ascending:isAscending];
//    NSSortDescriptor *question_text = [NSSortDescriptor sortDescriptorWithKey:@"question_text" ascending:isAscending];
//    NSSortDescriptor *question_id = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:isAscending];
    [fetchRequest setSortDescriptors:self.sortDescriptors];

    // Edit the section name key path and cache name if appropriate.
    
    NSString *section_name = nil; //(self.isGroupBy) ? @"section_name" : nil;
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:section_name
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"testguru_dynamic_filter_header"]) {
        TestGuruDynamicHeader *dynamicHeader = (TestGuruDynamicHeader *)[segue destinationViewController];
        dynamicHeader.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"testguru_sort_header"]) {
        TestGuruSortHeader *sortHeader = (TestGuruSortHeader *)[segue destinationViewController];
        sortHeader.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"PRESENT_MODAL_QUESTION_TYPE_LIST"]) {
        QuestionTypesList *qtl = (QuestionTypesList *)[segue destinationViewController];
        qtl.delegate = self;
    }
    
    // Get the new view controller using [segue destinationViewController].
    if ([segue.identifier isEqualToString:@"SAMPLE_QUESTION_VIEW"]) {
        QuestionSamplerView *sampler = (QuestionSamplerView *)[segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        sampler.indexPath = indexPath;
        sampler.entityObject = kQuestionBankEntity;
        sampler.delegate = self;
        sampler.sortDescriptors = self.sortDescriptors;
    }

    if ([segue.identifier isEqualToString:@"SHOW_EDIT_QUESTION_DETAILS_V22"]) {
        [self displayQuestionDetailsForSegue:segue];
    }
}

//- (NSIndexPath *)indexPathForGroup {
//    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//    if (self.sortOrderAscending) {
//        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:0];
//        NSInteger reverseRow = [sectionInfo numberOfObjects] - indexPath.row - 1;
//        NSIndexPath *reverseIndexPath = [NSIndexPath indexPathForRow:reverseRow inSection:indexPath.section];
//        indexPath = reverseIndexPath;
//    }
//    return indexPath;
//}

- (void)displayQuestionDetailsForSegue:(UIStoryboardSegue *)segue {
    
    NSManagedObject *object = nil;
    
    if (self.editOperation == NO) {
        //CREATE NEW QUESTION ITEM
        //CHECK IF NEW QUESTION ID IS NOT EMPTY
        if (self.question_id_new.length > 0) {
            NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:self.question_id_new];
            object = [self.tm getEntity:kQuestionEntity predicate:predicate];
            self.question_id_new = @""; //RESET TO DEFAULT VALUES
        }
    }
    
    if (self.editOperation == YES) {
        object = self.editObject;//[self.tm getEntity:kQuestionEntity predicate:predicate];
    }
    
    if (object != nil) {
        TGQBQuestionDetailViewController *qd = (TGQBQuestionDetailViewController *)[segue destinationViewController];
        qd.delegate = self;
        [qd setQuestionObject:object];
        qd.editMode = self.editOperation;
    }
    
}

///////////////////////////////
/// TGQuestionDetailsDelegate
///////////////////////////////
- (void)didFinishCreateOperation:(BOOL)state {
    
    if (self.isGroupBy == NO) {
        [self filterQuestionsPageOne];
    }
    
    if (self.isGroupBy == YES) {
        [self initiateSeeMore:@"1"];
    }
}

///////////////////////////////
/// TGQBQuestionDetailDelegate
///////////////////////////////
#pragma mark - TGQBQuestionDetailDelegate
- (void)didFinishDeleteOperationWithObject:(NSManagedObject *)object {
    
    //TODO
    NSLog(@"delete operation");
}

//- (void)sortDidSelectButtonAction:(NSString *)actionType withSortDescriptors:(NSMutableArray *)sortDescriptors {
//    
//    if ([actionType isEqualToString:@"APPLY"]) {
//        if (sortDescriptors) {
//            self.sortButton.selected = YES;
//            self.sortDescriptors = [NSArray arrayWithArray:sortDescriptors];
//            NSLog(@"sortDescriptors: %@", sortDescriptors);
//            [self reloadFetchedResultsController];
//        } else {
//            self.sortButton.selected = NO;
//        }
//    }
//    
//    NSInteger count = [sortDescriptors count];
//    BOOL hasSort = YES;
//    NSSortDescriptor *sortDescriptor2 = [NSSortDescriptor sortDescriptorWithKey:@"sort_date" ascending:NO];
//    if ((count == 1) && [[sortDescriptors lastObject] isEqual:sortDescriptor2]) {
//        hasSort = NO;
//    }
//    
//    
//    self.sortButton.titleLabel.font = (hasSort) ? [UIFont fontWithName:@"Helvetica-Bold" size:18] : [UIFont fontWithName:@"Helvetica" size:17];
//    
//        self.sortDrawerHeight.constant = kZeroHeight;
//        [self.view setNeedsUpdateConstraints];
//        [UIView animateWithDuration:0.25f animations:^{
//            [self.view layoutIfNeeded];
//        }];
//}
    
- (void)sortDidSelectButtonAction:(NSString *)actionType withSortData:(NSDictionary *)sortData {
    if ([actionType isEqualToString:@"APPLY"]) {
        if (sortData) {
            self.sortButton.selected = YES;
            self.sortData = sortData;
            
            // DYNAMIC SORTING
            NSSortDescriptor *index = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
            self.sortDescriptors = @[index];
            
            [self filterQuestionsPageOne];
        }
        else {
            self.sortButton.selected = NO;
        }
    }
    
    BOOL hasSort = YES;
    BOOL isViewBy = [sortData objectForKey:@"view_by"];
    
    if ([sortData count] == 1 && isViewBy) {
        hasSort = NO;
    }
    
    UIFont *font = (hasSort) ? [UIFont fontWithName:@"Helvetica-Bold" size:18] : [UIFont fontWithName:@"Helvetica" size:17];
    self.sortButton.titleLabel.font = font;
    
        self.sortDrawerHeight.constant = kZeroHeight;
        [self.view setNeedsUpdateConstraints];
        [UIView animateWithDuration:0.25f animations:^{
            [self.view layoutIfNeeded];
        }];
    }

- (void)didSelectButtonAction:(NSString *)actionType {
    
    if ([actionType isEqualToString:@"CANCEL"]) {
        self.verticalSpaceConstraint.constant = -280.0f;
        [self.view setNeedsUpdateConstraints];
        [UIView animateWithDuration:0.25f animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    
    if ([actionType isEqualToString:@"APPLY"]) {
        self.verticalSpaceConstraint.constant = -280.0f;
        [self.view setNeedsUpdateConstraints];
        [UIView animateWithDuration:0.25f animations:^{
            [self.view layoutIfNeeded];
        }];
        
        [self filterQuestionsPageOne];
    }
}

- (void)filterOptionCount:(NSUInteger)count {
    dispatch_async(dispatch_get_main_queue(), ^{
    self.filterButton.selected = (count > 0) ? YES : NO;
        self.filterButton.titleLabel.font = (count > 0) ? [UIFont fontWithName:@"Helvetica-Bold" size:18] : [UIFont fontWithName:@"Helvetica" size:17];
    });
}

#pragma mark - UIScrollViewDelegate

//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//    
//    if (!decelerate) {
//        [self fetchImagesForVisibleCell];
//    }
//}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    [self fetchImagesForVisibleCell];
//}
//
//- (void)fetchImagesForVisibleCell {
//    
//    NSArray *index_list = [self.tableView indexPathsForVisibleRows];
//    for (NSIndexPath *indexPath in index_list) {
//        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//        [self downloadImageForObject:mo indexPath:indexPath];
//    }
//}
//
//- (void)downloadImageForObject:(NSManagedObject *)mo indexPath:(NSIndexPath *)indexPath {
//    
//    if (self.activeDownloads == nil) {
//        self.activeDownloads = [NSMutableDictionary dictionary];
//    }
//    
//    NSManagedObject *mo_in_progress = [self.activeDownloads objectForKey:indexPath];
//    if (mo_in_progress == nil) {
//        
//        [self.activeDownloads setObject:mo forKey:indexPath];
//        
//        NSData *image_data = [NSData dataWithData:[mo valueForKey:@"image_data"]] ;
//        
//        
//        __weak typeof(self) wo = self;
//        
//        if (image_data == nil) {
//        dispatch_async(self.queue, ^{
//            [wo.tm downloadQuestionImageForObject:mo doneBlock:^(BOOL status) {
//                if (status) {
//                    [wo.activeDownloads removeObjectForKey:indexPath];
//                }
//            }];
//        });
//        } else {
//            [self.activeDownloads removeObjectForKey:indexPath];
//        }
//        
////        dispatch_async(self.queue, ^{
////            [wo.tm downloadQuestionImageForObject:mo doneBlock:^(BOOL status) {
////                if (status) {
////                    [wo.activeDownloads removeObjectForKey:indexPath];
////                }
////            }];
////        });
//    }
//}

- (NSString *)getFirstImage:(NSManagedObject *)mo {
    
    if (mo != nil) {
        NSString *image_one = [mo valueForKey:@"image_one"];
        if (image_one.length > 0) {
            return image_one;
        }
        
        NSString *image_two = [mo valueForKey:@"image_two"];
        if (image_two.length > 0) {
            return image_two;
        }
        
        NSString *image_three = [mo valueForKey:@"image_three"];
        if (image_three.length > 0) {
            return image_three;
        }
        
        NSString *image_four = [mo valueForKey:@"image_four"];
        if (image_four.length > 0) {
            return image_four;
        } else {
            return @"";
        }
    } else {
        return @"";
    }
}


////////////
//QuestionSamplerDelegate
///////////

- (void)didSelectEditIndex:(NSIndexPath *)indexPath withEditObject:(NSManagedObject *)object {
    self.editOperation = YES;
    self.editObject = object;
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wo performSegueWithIdentifier:@"SHOW_EDIT_QUESTION_DETAILS_V22" sender:self];
    });
}

@end
