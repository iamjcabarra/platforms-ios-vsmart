//
//  TestGuruSortHeaderCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 27/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruSortHeaderCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *orderLabel;
@property (strong, nonatomic) IBOutlet UIButton *arrowButton;

@property (strong, nonatomic) IBOutlet UIView *backgroundObject;
- (void)showState;

@end
