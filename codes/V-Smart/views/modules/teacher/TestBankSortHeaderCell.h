//
//  TestBankSortHeaderCell.h
//  V-Smart
//
//  Created by Julius Abarra on 11/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestBankSortHeaderCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *sortByLabel;

- (void)showState:(BOOL)selected;

@end
