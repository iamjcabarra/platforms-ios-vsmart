//
//  TGQBLearningCompetencyViewCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 18/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGQBLearningCompetencyViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *codeField;
@property (strong, nonatomic) IBOutlet UILabel *learningCompetencyField;

@end
