//
//  QuestionInfoController.h
//  V-Smart
//
//  Created by Ryan Migallos on 7/30/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionInfoController : UITableViewController

@property (nonatomic, strong) NSManagedObject *question;
@property (nonatomic, assign) BOOL isEditMode;

@end

