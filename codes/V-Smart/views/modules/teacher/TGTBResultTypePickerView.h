//
//  TGTBResultTypePickerView.h
//  V-Smart
//
//  Created by Julius Abarra on 16/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

@protocol TGTBResultTypePickerViewDelegate <NSObject>

@required
- (void)selectedResultType:(NSDictionary *)resultTypeObject;

@end

#import <UIKit/UIKit.h>

@interface TGTBResultTypePickerView : UITableViewController

@property (weak, nonatomic) id <TGTBResultTypePickerViewDelegate> delegate;

@end
