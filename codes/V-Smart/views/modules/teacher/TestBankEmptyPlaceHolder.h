//
//  TestBankEmptyPlaceHolder.h
//  V-Smart
//
//  Created by Julius Abarra on 27/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TestBankEmptyPlaceHolderDelegate <NSObject>

@required
- (void)didFinishSelectingCreateAction;

@end

@interface TestBankEmptyPlaceHolder : UIView

@property (weak, nonatomic) id <TestBankEmptyPlaceHolderDelegate> delegate;

- (void)showEmptyViewWithMessage:(NSString *)message shouldHideCreateButton:(BOOL)hideCreateButton;

@end

