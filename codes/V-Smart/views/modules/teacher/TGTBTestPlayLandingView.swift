//
//  TGTBTestPlayLandingView.swift
//  V-Smart
//
//  Created by Ryan Migallos on 03/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

@objc protocol TGTBTestPlayLandingDelegate: class {
    func didViewQuestions()
}

@objc class TGTBTestPlayLandingView: UIViewController, TGTBTestPlayInfoDelegate {
    
    // MARK: - Segue Constants
    fileprivate let kTestPlayIdentifier = "TGTB_TESTPLAY_INFO_EMBED"
    fileprivate let kTestQuestionSamplerIdentifier = "TGTB_QUESTION_SAMPLER_VIEW"
    fileprivate let kTestPreviewSample2 = "TGTB_QUESTION_SAMPLER_VIEW_2"
    
    // MARK: - Embedded View Controllers
    fileprivate var testPlayinfo: TGTBTestPlayInfoView!
    
    var testObject: NSManagedObject?
    
    weak var delegate: TGTBTestPlayLandingDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Helper Functions
    fileprivate func customizeNavigationBar() {
        
        self.title = "TEST BANK PREVIEW"
        
        let nb = (self.navigationController?.navigationBar)!
        let color = UIColor(rgba: "#FE9A3D")
        let textColor = UIColor.white
        
        nb.barTintColor = color;
        nb.isTranslucent = false;
        nb.titleTextAttributes = [ NSForegroundColorAttributeName : textColor ]
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //EMBED SEGUE
        if let vc = segue.destination as? TGTBTestPlayInfoView , segue.identifier == kTestPlayIdentifier {
            vc.delegate = self
            self.testPlayinfo = vc
            self.testPlayinfo.mo = self.testObject
        }
        
        if let qsv = segue.destination as? QuestionSamplerView , segue.identifier == kTestQuestionSamplerIdentifier {
            
            qsv.indexPath = IndexPath(item: 0, section: 0)
            qsv.entityObject = kQuestionBankEntity;
//            qsv.delegate = self;
            qsv.sortDescriptors = setUpSortDescriptors()
            qsv.closeButtonHidden()
        }
        
    }
    
    func didClickViewQuestions(_ flag: Bool) {
        self.navigationController?.popViewController(animated: false);
        self.delegate?.didViewQuestions()
    }
    
    func setUpSortDescriptors() -> [NSSortDescriptor] {
        let date_modified = NSSortDescriptor(key: "sort_date", ascending: true)
        let question_text = NSSortDescriptor(key: "question_text", ascending: true)
        let question_id = NSSortDescriptor(key: "id", ascending: true)
        
        return [date_modified, question_text, question_id]
    }
}
