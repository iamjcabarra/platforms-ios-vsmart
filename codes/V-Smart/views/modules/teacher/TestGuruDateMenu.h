//
//  TestGuruDateMenu.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/14/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *kTimeLimitType = @"timelimit";
static NSString *kOpeningType = @"openingdate";
static NSString *kClosingType = @"closingdate";

@protocol DateOptionMenuDelegate <NSObject>
@required
- (void)selectedDateForType:(NSString *)type withObject:(NSDictionary *)object;
@end

@interface TestGuruDateMenu : UIViewController
@property (nonatomic, assign) UIDatePickerMode mode;
@property (nonatomic, strong) NSDate *value;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, weak) id <DateOptionMenuDelegate> delegate;

@end
