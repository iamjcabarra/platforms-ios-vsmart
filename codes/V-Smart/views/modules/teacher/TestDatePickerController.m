//
//  TestDatePickerController.m
//  V-Smart
//
//  Created by Julius Abarra on 15/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestDatePickerController.h"

@interface TestDatePickerController ()

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;

@property (strong, nonatomic) NSString *dateToReturn;

@end

@implementation TestDatePickerController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Background view
    self.backgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.backgroundView.layer.borderWidth = 1.0f;
    
    // Initialize date
    self.dateToReturn = [self getDateTodayWithFormat:@"EEE, dd MMM yyyy, hh:mm a"];
    
    // Actions for buttons
    [self.datePicker addTarget:self
                        action:@selector(changeDateAction:)
              forControlEvents:UIControlEventValueChanged];
    
    [self.doneButton addTarget:self
                        action:@selector(selectDateAction:)
              forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Date Picker Action

- (void)changeDateAction:(id)sender {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    dateFormatter.dateFormat = @"EEE, dd MMM yyyy, hh:mm a";
    dateFormatter.timeZone = [NSTimeZone localTimeZone];
    self.dateToReturn = [dateFormatter stringFromDate:[self.datePicker date]];
}

#pragma mark - Apply Action

- (void)selectDateAction:(id)sender {
    [self.delegate selectedDate:self.dateToReturn];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Date Today

- (NSString *)getDateTodayWithFormat:(NSString *)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    dateFormatter.dateFormat = format;
    dateFormatter.timeZone = [NSTimeZone localTimeZone];
    
    return [dateFormatter stringFromDate:[NSDate date]];;
}

@end
