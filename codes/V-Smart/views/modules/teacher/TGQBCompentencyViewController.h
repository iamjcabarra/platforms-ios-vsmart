//
//  TGQBCompentencyViewController.h
//  V-Smart
//
//  Created by Ryan Migallos on 17/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGQBCompetencyComponentDelegate <NSObject>
@optional
- (void)didFinishSelectingLearningCompetencyCode:(NSString *)competencyCode;
@end

@interface TGQBCompentencyViewController : UIViewController
@property (weak, nonatomic) id <TGQBCompetencyComponentDelegate> delegate;
@end
