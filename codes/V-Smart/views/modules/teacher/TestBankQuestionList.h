//
//  TestBankQuestionList.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestBankQuestionList : UIViewController

@property (nonatomic, strong) NSManagedObject *quiz_mo;

@end
