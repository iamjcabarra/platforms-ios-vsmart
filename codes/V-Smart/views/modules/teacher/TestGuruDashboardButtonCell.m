//
//  TestGuruDashboardButtonCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 21/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruDashboardButtonCell.h"
#import <QuartzCore/QuartzCore.h>


@implementation TestGuruDashboardButtonCell

- (void)awakeFromNib {
    
    CALayer *imageLayer = self.imageView.layer;
    imageLayer.shadowColor = [UIColor lightGrayColor].CGColor;
    imageLayer.shadowOffset = CGSizeMake(2.0,2.0);
    imageLayer.shadowRadius = 5.0f;
    imageLayer.shadowOpacity = 0.9f;
}

@end
