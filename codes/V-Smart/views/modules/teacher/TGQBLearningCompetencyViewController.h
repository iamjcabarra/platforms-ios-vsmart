//
//  TGQBLearningCompetencyViewController.h
//  V-Smart
//
//  Created by Ryan Migallos on 18/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGQBCompetencyDelegate <NSObject>
@optional
- (void)didFinishSelectingLearningCompetency:(NSManagedObject *)object;

@end

@interface TGQBLearningCompetencyViewController : UITableViewController
@property (weak, nonatomic) id <TGQBCompetencyDelegate> delegate;
- (void)competencySearchWithText:(NSString *)string isActive:(BOOL)isactive;
@end
