//
//  NSDateExtension.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 03/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import Foundation
import UIKit


extension Foundation.Date {
    
    struct Date {
        static let timeFormatter: DateFormatter = {
            let df = DateFormatter()
            df.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            df.dateFormat = "h:mm a"
            return df
        }()
    }
    var time: String { return Date.timeFormatter.string(from: self) }
    
    var year:    Int { return (Calendar.autoupdatingCurrent as NSCalendar).component(.year,   from: self) }
    var month:   Int { return (Calendar.autoupdatingCurrent as NSCalendar).component(.month,  from: self) }
    var day:     Int { return (Calendar.autoupdatingCurrent as NSCalendar).component(.day,    from: self) }
    
    struct DateComponents {
        static let formatter: DateComponentsFormatter = {
            let dcFormatter = DateComponentsFormatter()
            dcFormatter.calendar = Calendar(identifier: Calendar.Identifier.iso8601)
            dcFormatter.unitsStyle = .full
            dcFormatter.maximumUnitCount = 1
            dcFormatter.zeroFormattingBehavior = .default
            dcFormatter.allowsFractionalUnits = false
            dcFormatter.allowedUnits = [.year, .month, .weekday, .day, .hour, .minute, .second]
            return dcFormatter
        }()
    }
    
    var elapsedTime: String {
        if timeIntervalSinceNow > -60.0  { return NSLocalizedString("Just Now", comment: "") }
        if isDateInToday                 { return NSLocalizedString("Today at", comment: "") + " \(time)" }
        if isDateInYesterday             { return NSLocalizedString("Yesterday at", comment: "") + " \(time)" }
        let localizedAgo = NSLocalizedString("ago", comment: "")
        return (DateComponents.formatter.string(from: Foundation.Date().timeIntervalSince(self)) ?? "") + " \(localizedAgo)"
    }
    var isDateInToday: Bool {
        return Calendar.autoupdatingCurrent.isDateInToday(self)
    }
    var isDateInYesterday: Bool {
        return Calendar.autoupdatingCurrent.isDateInYesterday(self)
    }
    
    
    
    
    
    
    
    
    
}
