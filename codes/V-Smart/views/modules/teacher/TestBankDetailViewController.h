//
//  TestBankDetailViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 13/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestBankDetailViewController : UIViewController

@property (strong, nonatomic) NSManagedObject *testObject;
@property (strong, nonatomic) NSString *customBackBarButtonTitle;
@property (assign, nonatomic) int actionType;

@end
