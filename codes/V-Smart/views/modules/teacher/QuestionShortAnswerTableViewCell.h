//
//  QuestionShortAnswerTableViewCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 7/30/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionShortAnswerTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextField *questionField;
@property (strong, nonatomic) IBOutlet UITextField *questionPointsField;
@property (strong, nonatomic) IBOutlet UITextView *questionTextArea;
@property (strong, nonatomic) IBOutlet UIButton *eraseButton;

@property (strong, nonatomic) NSManagedObject *question;
- (NSManagedObject *)saveContents:(NSDictionary *)object;

@end
