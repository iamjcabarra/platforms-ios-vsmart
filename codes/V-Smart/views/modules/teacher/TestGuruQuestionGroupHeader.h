//
//  TestGuruQuestionGroupHeader.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 21/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruQuestionGroupHeader : UIView
@property (weak, nonatomic) IBOutlet UILabel *sectionNameLbl;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;
@property (weak, nonatomic) IBOutlet UILabel *collapseIndicator;
@property (weak, nonatomic) IBOutlet UIButton *seeMoreButton;


@end
