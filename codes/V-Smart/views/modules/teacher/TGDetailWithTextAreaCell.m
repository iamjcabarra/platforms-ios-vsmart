//
//  TGDetailWithTextAreaCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 16/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TGDetailWithTextAreaCell.h"

@interface TGDetailWithTextAreaCell()

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *pointLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) IBOutlet UILabel *imageLabel;

@property (strong, nonatomic) IBOutlet UITextField *titleField;
@property (strong, nonatomic) IBOutlet UITextField *pointField;
@property (strong, nonatomic) IBOutlet UITextView *questionField;

@property (strong, nonatomic) IBOutlet UIImageView *imageOne;
@property (strong, nonatomic) IBOutlet UIImageView *imageTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imageThree;
@property (strong, nonatomic) IBOutlet UIImageView *imageFour;

@property (strong, nonatomic) IBOutlet UIButton *buttonOne;
@property (strong, nonatomic) IBOutlet UIButton *buttonTwo;
@property (strong, nonatomic) IBOutlet UIButton *buttonThree;
@property (strong, nonatomic) IBOutlet UIButton *buttonFour;

@property (strong, nonatomic) IBOutlet UIButton *buttonRemove;

@end

@implementation TGDetailWithTextAreaCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
