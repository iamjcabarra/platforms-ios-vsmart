//
//  TestBankDetailViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 13/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestBankDetailViewController.h"
#import "TestContentManager.h"
#import "TestGuruDataManager.h"
#import "AccountInfo.h"
#import "HUD.h"

@interface TestBankDetailViewController () <UINavigationBarDelegate>

@property (strong, nonatomic) TestContentManager *sharedTestContentManager;
@property (strong, nonatomic) TestGuruDataManager *tgdm;

@property (strong, nonatomic) IBOutlet UIButton *testInformationButton;
@property (strong, nonatomic) IBOutlet UIButton *assignQuestionsButton;

@property (strong, nonatomic) IBOutlet UIImageView *arrowImageA;
@property (strong, nonatomic) IBOutlet UIImageView *arrowImageB;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *testInformationHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *assignQuestionsHeight;

@property (strong, nonatomic) IBOutlet UIButton *saveAndCloseButton;

@property (strong, nonatomic) NSString *userid;
@property (strong, nonatomic) NSString *preTestMainInfoEntries;
@property (strong, nonatomic) NSString *preTestSettingsEntries;

@property (strong, nonatomic) NSArray *preTestAssignedQuestions;

@property (assign, nonatomic) BOOL isSectionACollapsed;
@property (assign, nonatomic) BOOL isSectionBCollapsed;

@property (assign, nonatomic) BOOL isOkayToSaveOrUpdate;

@end

@implementation TestBankDetailViewController

#define kDefaultAttempts    @"1"
#define kDefaultTimeLimit   @"00:15:00"
#define kDefaultPassingRate @"75"

static CGFloat kZeroHeight = 0.0f;
static CGFloat kNormalHeight = 450.0f;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set user id
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.userid = [NSString stringWithFormat:@"%d", account.user.id];
    
    self.sharedTestContentManager = [TestContentManager sharedInstance];
    [self.sharedTestContentManager recreateObjects];
    
    self.sharedTestContentManager.actionType = self.actionType;
    self.sharedTestContentManager.testid = [self stringValue:[self.testObject valueForKey:@"id"]];
    
    self.tgdm = [TestGuruDataManager sharedInstance];
    
    // Clear test-related data from core data
    if (self.actionType == 1) {
        [self.tgdm clearTestBankRelatedDataFromCoreData];
    }
    
    // Detail navigation and default views
    [self setUpNavigationView];
    [self setUpDefaultViewForTestDetail];
    
    NSString *buttonTitle = @"";
    
    // Default values
    if (self.actionType == 1) {
        [self setUpDefaultValuesforTestProperties];
        buttonTitle = NSLocalizedString(@"Save & Close", nil);
    }
    if (self.actionType == 2) {
        [self setUpDefaultValuesForTestToUpdate];
        buttonTitle = NSLocalizedString(@"Update & Close", nil);
    }
    
    // Set button title
    [self.saveAndCloseButton setTitle:buttonTitle forState:UIControlStateNormal];
    [self.saveAndCloseButton setTitle:buttonTitle forState:UIControlStateDisabled];
    [self.saveAndCloseButton setTitle:buttonTitle forState:UIControlStateHighlighted];

    // Action for save and close button
    [self.saveAndCloseButton addTarget:self
                                action:@selector(postTestOperation:)
                      forControlEvents:UIControlEventTouchUpInside];
    
    // Disable save and close button by default
    self.saveAndCloseButton.enabled = NO;
    [self.saveAndCloseButton setBackgroundColor:[UIColor grayColor]];
    
    self.isOkayToSaveOrUpdate = NO;
    self.preTestAssignedQuestions = [NSArray array];
    
    // Add an observer that will respond to initializing test entries
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setPreEntriesForTestCreation:)
                                                 name:@"setPreEntriesForTestCreation"
                                               object:nil];
    
    // Add an observer that will respond to initializing test assigned questions
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setPreEntriesForTestQuestion:)
                                                 name:@"setPreEntriesForTestQuestion"
                                               object:nil];
    
    // Add an observer that will respond to creating or updating test
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(prepareToSaveOrUpdateTest:)
                                                 name:@"prepareToSaveOrUpdateTest"
                                               object:nil];
    
    [self setUpCustomBackBarButton];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Clear test content manager
    [self.sharedTestContentManager clearEntries];
    
    // Remove observers
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"setPreEntriesForTestCreation"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"setPreEntriesForTestQuestion"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"prepareToSaveOrUpdateTest"
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation View

- (void)setUpNavigationView {
    NSString *title;
    
    if (self.actionType == 1) {
        title = NSLocalizedString(@"Create Test", nil);
    }
    
    if (self.actionType == 2) {
        title = NSLocalizedString(@"Edit Test", nil);
    }

    self.title = title;
}

#pragma mark - Default View Settings

- (void)setUpDefaultViewForTestDetail {
    // Initialize flags for collapsible sections
    self.isSectionACollapsed = YES;
    self.isSectionBCollapsed = NO;
    
    // Update UI
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        wo.testInformationHeight.constant = kNormalHeight;
        wo.assignQuestionsHeight.constant = kZeroHeight;
        wo.arrowImageA.image = [UIImage imageNamed:@"icn_down_arrow"];
        wo.arrowImageB.image = [UIImage imageNamed:@"icn_up_arrow"];
        [self.view setNeedsUpdateConstraints];
        [self.view layoutIfNeeded];
    });
    
    // Set button titles
    NSString *testInformationTitle = [NSString stringWithFormat:@"   %@", NSLocalizedString(@"Test Information", nil)];
    NSString *assignQuestionsTitle = [NSString stringWithFormat:@"   %@", NSLocalizedString(@"Assign Questions", nil)];
    
    [self.testInformationButton setTitle:testInformationTitle forState:UIControlStateNormal];
    [self.assignQuestionsButton setTitle:assignQuestionsTitle forState:UIControlStateNormal];
    
    // Set button tags
    self.testInformationButton.tag = 100;
    self.assignQuestionsButton.tag = 200;
    
    // Actions for buttons
    [self.testInformationButton addTarget:self
                                   action:@selector(collapseTestDetailSection:)
                         forControlEvents:UIControlEventTouchUpInside];
    
    [self.assignQuestionsButton addTarget:self
                                   action:@selector(collapseTestDetailSection:)
                         forControlEvents:UIControlEventTouchUpInside];
}

- (void)collapseTestDetailSection:(id)sender {
    UIButton *sectionButton = (UIButton *)sender;
    NSInteger tag = sectionButton.tag;
    
    CGFloat height;
    NSString *imageString;
    
    UIImageView *sectionButtonView;
    
    // TEST INFORMATION SECTION
    if (tag == 100) {
        if (self.isSectionACollapsed) {
            height = kZeroHeight;
            imageString = @"icn_up_arrow";
            self.isSectionACollapsed = NO;
        }
        else {
            height = kNormalHeight;
            imageString = @"icn_down_arrow";
            self.isSectionACollapsed = YES;
        }
        
        sectionButtonView = self.arrowImageA;
        self.testInformationHeight.constant = height;
    }
    
    // ASSIGN QUESTIONS SECTION
    if (tag == 200) {
        if (self.isSectionBCollapsed) {
            height = kZeroHeight;
            imageString = @"icn_up_arrow";
            self.isSectionBCollapsed = NO;
        }
        else {
            height = kNormalHeight + 250.0f;
            imageString = @"icn_down_arrow";
            self.isSectionBCollapsed = YES;
        }
        
        sectionButtonView = self.arrowImageB;
        self.assignQuestionsHeight.constant = height;
    }
    
    // UPDATE TEST DETAIL VIEW
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.5f
                          delay:0.0f
         usingSpringWithDamping:1.0f
          initialSpringVelocity:0.5f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         sectionButtonView.image = [UIImage imageNamed:imageString];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

#pragma mark - Test Properties Default Values

- (void)setUpDefaultValuesforTestProperties {
    // TO-DO: 5 API calls? Think of more logical and efficient solution
    [self.tgdm requestTestProperty:@"TestCategory"
                          endPoint:kEndPointTestCategoryList
                         doneBlock:^(BOOL status) {
    }];
    
    [self.tgdm requestTestProperty:@"TestResultType"
                          endPoint:kEndPointTestResultTypeList
                         doneBlock:^(BOOL status) {
    }];
    
    [self.tgdm requestTestProperty:@"TestType"
                          endPoint:kEndPointTestTypeList
                         doneBlock:^(BOOL status) {
    }];
    
    [self.tgdm requestTestProperty:@"TestDifficultyLevel"
                          endPoint:kEndPointTestDifficultyLevelList
                         doneBlock:^(BOOL status) {
    }];
    
    [self.tgdm requestTestProperty:@"TestLearningSkill"
                          endPoint:kEndPointTestLearningSkillList
                         doneBlock:^(BOOL status) {
    }];
}

#pragma mark - Default Values of Test to Update

- (void)setUpDefaultValuesForTestToUpdate {
    NSManagedObject *testDetailObject = [self getTestPropertyObjectForEntity:kTestDetailEntity];
    
    if (testDetailObject != nil) {
        // Acess data from managed object
        NSString *name = [self stringValue:[testDetailObject valueForKey:@"name"]];
        NSString *test_description = [self stringValue:[testDetailObject valueForKey:@"test_description"]];
        NSString *attempts = [self stringValue:[testDetailObject valueForKey:@"attempts"]];
        NSString *time_limit = [self stringValue:[testDetailObject valueForKey:@"time_limit"]];
        NSString *passing_rate = [self stringValue:[testDetailObject valueForKey:@"passing_rate"]];
        NSString *date_open = [self stringValue:[testDetailObject valueForKey:@"date_open"]];
        NSString *date_close = [self stringValue:[testDetailObject valueForKey:@"date_close"]];
        NSString *quiz_stage_id = [self stringValue:[testDetailObject valueForKey:@"quiz_stage_id"]];
        NSString *quiz_category_id = [self stringValue:[testDetailObject valueForKey:@"quiz_category_id"]];
        NSString *quiz_category_name = [self stringValue:[testDetailObject valueForKey:@"quiz_category_name"]];
        NSString *quiz_result_type_id = [self stringValue:[testDetailObject valueForKey:@"quiz_result_type_id"]];
        NSString *quiz_result_type_name = [self stringValue:[testDetailObject valueForKey:@"quiz_result_type_name"]];
        NSString *quiz_type_id = [self stringValue:[testDetailObject valueForKey:@"quiz_type_id"]];
        NSString *question_type_name = [self stringValue:[testDetailObject valueForKey:@"question_type_name"]];
        NSString *difficulty_level_id = [self stringValue:[testDetailObject valueForKey:@"difficulty_level_id"]];
        NSString *difficulty_level_name = [self stringValue:[testDetailObject valueForKey:@"difficulty_level_name"]];
        NSString *learning_skills_id = [self stringValue:[testDetailObject valueForKey:@"learning_skills_id"]];
        NSString *learning_skills_name = [self stringValue:[testDetailObject valueForKey:@"learning_skills_name"]];
        NSString *quiz_shuffling_mode = [self stringValue:[testDetailObject valueForKey:@"quiz_shuffling_mode"]];
        NSString *is_shuffle_answers = [self stringValue:[testDetailObject valueForKey:@"is_shuffle_answers"]];
        NSString *show_feedbacks = [self stringValue:[testDetailObject valueForKey:@"show_feedbacks"]];
        NSString *allow_review = [self stringValue:[testDetailObject valueForKey:@"allow_review"]];
        NSString *is_forced_complete = [self stringValue:[testDetailObject valueForKey:@"is_forced_complete"]];
        
        date_open = [self changeDateString:date_open fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"EEE, dd MMM yyyy, hh:mm a"];
        date_close = [self changeDateString:date_close fromFormat:@"yyyy-MM-dd HH:mm:ss" toFormat:@"EEE, dd MMM yyyy, hh:mm a"];

        // Save to test content manager: Test Main Info
        [self.sharedTestContentManager.testMainInfo setObject:name forKey:@"name"];
        [self.sharedTestContentManager.testMainInfo setObject:test_description forKey:@"description"];
        [self.sharedTestContentManager.testMainInfo setObject:attempts forKey:@"attempts"];
        [self.sharedTestContentManager.testMainInfo setObject:time_limit forKey:@"time_limit"];
        [self.sharedTestContentManager.testMainInfo setObject:passing_rate forKey:@"passing_rate"];
        [self.sharedTestContentManager.testMainInfo setObject:date_open forKey:@"date_open"];
        [self.sharedTestContentManager.testMainInfo setObject:date_close forKey:@"date_close"];
        [self.sharedTestContentManager.testMainInfo setObject:quiz_stage_id forKey:@"quiz_stage_id"];
        
        // Save to test content manager: Test Settings
        NSDictionary *test_category = @{@"id":quiz_category_id, @"value":quiz_category_name};
        NSDictionary *test_result_type = @{@"id":quiz_result_type_id, @"value":quiz_result_type_name};
        NSDictionary *test_type = @{@"id":quiz_type_id, @"value":question_type_name};
        NSDictionary *test_difficulty_level = @{@"id":difficulty_level_id, @"value":difficulty_level_name};
        NSDictionary *test_learning_skill = @{@"id":learning_skills_id, @"value":learning_skills_name};
        
        [self.sharedTestContentManager.testSettings setObject:test_category forKey:@"test_category"];
        [self.sharedTestContentManager.testSettings setObject:test_result_type forKey:@"test_result_type"];
        [self.sharedTestContentManager.testSettings setObject:test_type forKey:@"test_type"];
        [self.sharedTestContentManager.testSettings setObject:test_difficulty_level forKey:@"test_difficulty_level"];
        [self.sharedTestContentManager.testSettings setObject:test_learning_skill forKey:@"test_learning_skill"];
        [self.sharedTestContentManager.testSettings setObject:is_shuffle_answers forKey:@"is_shuffle_answers"];
        [self.sharedTestContentManager.testSettings setObject:quiz_shuffling_mode forKey:@"quiz_shuffling_mode"];
        [self.sharedTestContentManager.testSettings setObject:show_feedbacks forKey:@"show_feedbacks"];
        [self.sharedTestContentManager.testSettings setObject:allow_review forKey:@"allow_review"];
        [self.sharedTestContentManager.testSettings setObject:is_forced_complete forKey:@"is_forced_complete"];
    }
    
    // For test changes made mapping
    [self setUpPreEntriesForTest];
}

- (NSManagedObject *)getTestPropertyObjectForEntity:(NSString *)entityName {
    NSString *testid = [self stringValue:[self.testObject valueForKey:@"id"]];
    
    NSPredicate *predicate = [self predicateForKeyPath:@"id" value:testid];
    NSManagedObject *mo = [self.tgdm getObjectForEntity:entityName withSortDescriptor:@"id" andPredicate:predicate];
    
    if (mo != nil) {
        return mo;
    }
    
    return nil;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    
    return predicate;
}

#pragma mark - Custom Back Bar Button

- (void)setUpCustomBackBarButton {
    NSString *title = [NSString stringWithFormat:@"< %@", self.customBackBarButtonTitle];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 200, 42);
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:UIColorFromHex(0X3083FB) forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button addTarget:self action:@selector(backBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
}

- (void)backBarButtonAction:(id)sender {
    BOOL didChange = [self didTestContentsChange];
    
    if (didChange) {
        NSString *avTitle = @"";
        NSString *message = @"";
        
        if (self.actionType == 1) {
            avTitle = NSLocalizedString(@"Create Test", nil);
            message = NSLocalizedString(@"Do you want to cancel creating this test?", nil);
        }
        
        if (self.actionType == 2) {
            avTitle = NSLocalizedString(@"Edit Test", nil);
            message = NSLocalizedString(@"Do you want to discard changes you made with this test?", nil);
        }
        
        NSString *posR = NSLocalizedString(@"Yes", nil);
        NSString *negR = NSLocalizedString(@"No", nil);
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:avTitle
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *posAction = [UIAlertAction actionWithTitle:posR
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                              [self.navigationController popViewControllerAnimated:YES];
                                                          }];
        
        UIAlertAction *negAction = [UIAlertAction actionWithTitle:negR
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
        [alert addAction:posAction];
        [alert addAction:negAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    // Both initial and final entries for test main info or settings are equal
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (BOOL)didTestContentsChange {
    // Get initial entries for test main info and settings
    NSString *preTestMainInfoEntries = [self stringValue:self.preTestMainInfoEntries];
    NSString *preTestSettingsEntries = [self stringValue:self.preTestSettingsEntries];
    
    // Get final entries for test main info and settings
    NSString *postTestMainInfoEntries = [self getConcatenatedValuesForDictionary:self.sharedTestContentManager.testMainInfo];
    NSString *postTestSettingsEntries = [self getConcatenatedValuesForDictionary:self.sharedTestContentManager.testSettings];
    
    // Determine if initial and final entries for test main info and settings are equal or not
    BOOL isTestMainInfoPreAndPostEqual = [preTestMainInfoEntries isEqualToString:postTestMainInfoEntries];
    BOOL isTestSettingsPreAndPostEqual = [preTestSettingsEntries isEqualToString:postTestSettingsEntries];
    
    // Determine if there is a difference in the test pre and post assigned questions
    BOOL isPreAndPostTestAQDifferent = YES;
    
    int preAssignedQuestionsCount = (int)self.preTestAssignedQuestions.count;
    int postAssignedQuestionsCount = (int)self.sharedTestContentManager.testAssignedQuestions.count;
    
    if (preAssignedQuestionsCount == postAssignedQuestionsCount) {
        isPreAndPostTestAQDifferent = [self preAssignedQuestions:self.preTestAssignedQuestions
                                 differFromPostAssignedQuestions:self.sharedTestContentManager.testAssignedQuestions];
    }
    
    // Either initial or final entries for test main info, settings or assigned questions are not equal
    if (!isTestMainInfoPreAndPostEqual || !isTestSettingsPreAndPostEqual || isPreAndPostTestAQDifferent) {
        return YES;
    }
    
    return NO;
}

- (BOOL)preAssignedQuestions:(NSArray *)pre differFromPostAssignedQuestions:(NSArray *)post {
    NSSet *set = [NSSet setWithArray:pre];
    
    for (NSManagedObject *mo in post) {
        NSString *qid = [self stringValue:[mo valueForKey:@"question_id"]];
        
        if (![set containsObject:qid]) {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - Test Changes Made Mapping

- (void)setUpPreEntriesForTest {
    self.preTestMainInfoEntries = [self getConcatenatedValuesForDictionary:self.sharedTestContentManager.testMainInfo];
    self.preTestSettingsEntries = [self getConcatenatedValuesForDictionary:self.sharedTestContentManager.testSettings];
}

- (NSString *)getConcatenatedValuesForDictionary:(NSDictionary *)d {
    NSString *concatenatedValues = @"";
    NSArray *sortedKeys = [[d allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    for (NSString *k in sortedKeys) {
        NSString *entry = @"";
        
        if ([d[k] isKindOfClass:[NSDictionary class]]) {
            if (d[k] != nil) {
                entry = [self getConcatenatedValuesForDictionary:d[k]];
            }
        }
        
        if  ([d[k] isKindOfClass:[NSString class]]) {
            entry = [[self stringValue:d[k]] stringByReplacingOccurrencesOfString:@" " withString:@""];
        }
        
        concatenatedValues = [[NSString stringWithFormat:@"%@%@", concatenatedValues, entry] uppercaseString];
    }
    
    return concatenatedValues;
}

#pragma mark - Save or Update Test

- (void)postTestOperation:(id)sender {
    if (self.isOkayToSaveOrUpdate) {
        NSString *indicatorString = @"";
        
        if (self.actionType == 1) {
            indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Saving", nil)];
        }
        
        if (self.actionType == 2) {
            indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Updating", nil)];
        }
        
        [HUD showUIBlockingIndicatorWithText:indicatorString];
        
        NSString *testName = [self.sharedTestContentManager.testMainInfo objectForKey:@"name"];
        NSString *testDescription = [self.sharedTestContentManager.testMainInfo objectForKey:@"description"];
        NSString *testAttempts = [self.sharedTestContentManager.testMainInfo objectForKey:@"attempts"];
        NSString *testTimeLimit = [self.sharedTestContentManager.testMainInfo objectForKey:@"time_limit"];
        NSString *testPassingRate = [self.sharedTestContentManager.testMainInfo objectForKey:@"passing_rate"];
        NSString *testOpeningDate = [self.sharedTestContentManager.testMainInfo objectForKey:@"date_open"];
        NSString *testClosingDate = [self.sharedTestContentManager.testMainInfo objectForKey:@"date_close"];
        NSString *testIsGraded = [self.sharedTestContentManager.testMainInfo objectForKey:@"quiz_stage_id"];
        NSString *testTotalScore = @"100";
        
        testOpeningDate = [self changeDateString:testOpeningDate fromFormat:@"EEE, dd MMM yyyy, hh:mm a" toFormat:@"yyyy-MM-dd HH:mm:ss"];
        testClosingDate = [self changeDateString:testClosingDate fromFormat:@"EEE, dd MMM yyyy, hh:mm a" toFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDictionary *testCategory = [self.sharedTestContentManager.testSettings objectForKey:@"test_category"];
        NSDictionary *testResultType = [self.sharedTestContentManager.testSettings objectForKey:@"test_result_type"];
        NSDictionary *testType = [self.sharedTestContentManager.testSettings objectForKey:@"test_type"];
        NSDictionary *testDifficultyLevel = [self.sharedTestContentManager.testSettings objectForKey:@"test_difficulty_level"];
        NSDictionary *testLearningSkill = [self.sharedTestContentManager.testSettings objectForKey:@"test_learning_skill"];
        
        NSString *testCategoryID = [self stringValue:testCategory[@"id"]];
        NSString *testResultTypeID = [self stringValue:testResultType[@"id"]];
        NSString *testTypeID = [self stringValue:testType[@"id"]];
        NSString *testDifficultyLevelID = [self stringValue:testDifficultyLevel[@"id"]];
        NSString *testLearningSkillID = [self stringValue:testLearningSkill[@"id"]];
        
        NSString *shuffleAnswer = [self.sharedTestContentManager.testSettings objectForKey:@"is_shuffle_answers"];
        NSString *shuffleQuestion = [self.sharedTestContentManager.testSettings objectForKey:@"quiz_shuffling_mode"];
        NSString *showFeedback = [self.sharedTestContentManager.testSettings objectForKey:@"show_feedbacks"];
        NSString *allowReview = [self.sharedTestContentManager.testSettings objectForKey:@"allow_review"];
        NSString *forcedComplete = [self.sharedTestContentManager.testSettings objectForKey:@"is_forced_complete"];
        
        NSMutableArray *assignedQuestions = [NSMutableArray array];
        
        for (NSManagedObject *mo in self.sharedTestContentManager.testAssignedQuestions) {
            NSDictionary *questionsData = [NSDictionary dictionaryWithDictionary:[self buildTestQuestionDataFromObject:mo]];
            
            if (questionsData != nil) {
                [assignedQuestions addObject:questionsData];
            }
        }
        
        NSDictionary *postTestBody = @{@"user_id":self.userid,
                                       @"name":testName,
                                       @"description":testDescription,
                                       @"quiz_category_id":testCategoryID,
                                       @"quiz_type_id":testTypeID,
                                       @"quiz_result_type_id":testResultTypeID,
                                       @"learning_skills_id":testLearningSkillID,
                                       @"difficulty_level_id":testDifficultyLevelID,
                                       @"date_open":testOpeningDate,
                                       @"date_close":testClosingDate,
                                       @"attempts":testAttempts,
                                       @"quiz_shuffling_mode":shuffleQuestion,
                                       @"is_shuffle_answers":shuffleAnswer,
                                       @"total_score":testTotalScore,
                                       @"time_limit":testTimeLimit,
                                       @"is_forced_complete":forcedComplete,
                                       @"quiz_stage_id":testIsGraded,
                                       @"show_feedbacks":showFeedback,
                                       @"allow_review":allowReview,
                                       @"passing_rate":testPassingRate,
                                       @"questions":assignedQuestions
                                       };
        
        BOOL isNewTest = YES;
        NSString *testIDString = @"";
        
        if (self.actionType == 2) {
            isNewTest = NO;
            testIDString = [NSString stringWithFormat:@"%@", [self.testObject valueForKey:@"id"]];
        }
        
        NSLog(@"postTestBody: %@", postTestBody);
        
        [self.tgdm requestCreateTest:postTestBody testid:testIDString isNew:isNewTest doneBlock:^(BOOL status) {
            if (status) {
                __weak typeof(self) wo = self;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUD hideUIBlockingIndicator];
                    [wo.sharedTestContentManager clearEntries];
                    [wo showAlertViewForActionType:wo.actionType andOperationSuccessful:YES];
                });
            }
            else {
                __weak typeof(self) wo = self;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUD hideUIBlockingIndicator];
                    [wo showAlertViewForActionType:wo.actionType andOperationSuccessful:NO];
                });
            }
        }];
    }
    else {
        [self showAlertViewForActionType:self.actionType andOperationSuccessful:NO];
    }
}

- (NSDictionary *)buildTestQuestionDataFromObject:(NSManagedObject *)object {
    if (object != nil) {
        NSString *question_id = [self stringValue:[object valueForKey:@"question_id"]];
        NSString *question_type_id = [self stringValue:[object valueForKey:@"question_type_id"]];
        NSString *question_text = [self stringValue:[object valueForKey:@"question_text"]];
        NSString *points = [self stringValue:[object valueForKey:@"points"]];
        NSString *question_choices = [self buildTestQuestionChoiceDataFromObject:object];
        
        NSDictionary *data = @{@"question_id":question_id,
                               @"question_type_id":question_type_id,
                               @"question_text":question_text,
                               @"points":points,
                               @"question_choices": question_choices
                               };
        return data;
    }
    
    return nil;
}

- (NSString *)buildTestQuestionChoiceDataFromObject:(NSManagedObject *)object {
    NSString *choices_string = @"";
    NSMutableArray *list = [NSMutableArray array];
    NSSet *choices_set = [object valueForKey:@"choices"];
    NSArray *choice_list = [NSArray arrayWithArray:[choices_set allObjects]];
    
    if (choice_list.count > 0) {
        for (NSManagedObject *mo in choice_list) {
            NSString *choice_id = [self stringValue:[mo valueForKey:@"id"]];
            NSString *date_created = [self stringValue:[mo valueForKey:@"date_created"]];
            NSString *date_modified = [self stringValue:[mo valueForKey:@"date_modified"]];
            NSString *is_correct = [self stringValue:[mo valueForKey:@"is_correct"]];
            NSString *is_deleted = [self stringValue:[mo valueForKey:@"is_deleted"]];
            NSString *order_number = [self stringValue:[mo valueForKey:@"order_number"]];
            NSString *question_id = [self stringValue:[mo valueForKey:@"question_id"]];
            NSString *suggestive_feedback = [self stringValue:[mo valueForKey:@"suggestive_feedback"]];
            NSString *text = [self stringValue:[mo valueForKey:@"text"]];
            
            NSDictionary *dictionary = @{@"id":choice_id,
                                         @"date_created":date_created,
                                         @"date_modified":date_modified,
                                         @"is_correct":is_correct,
                                         @"is_deleted":is_deleted,
                                         @"order_number":order_number,
                                         @"question_id":question_id,
                                         @"suggestive_feedback":suggestive_feedback,
                                         @"text":text
                                         };
            [list addObject:dictionary];
        }
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:list options:0 error:nil];
        NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        choices_string = [self normalizeQuestionChoiceString:string encyrpt:YES];
    }
    
    return choices_string;
}

- (NSString *)normalizeQuestionChoiceString:(NSString *)string encyrpt:(BOOL)encyrpt {
    NSString *choiceString = [self stringValue:string];
    
    if (encyrpt == NO) {
        choiceString = [choiceString stringByReplacingOccurrencesOfString:@"_qw_" withString:@"["];
        choiceString = [choiceString stringByReplacingOccurrencesOfString:@"_qe_" withString:@"]"];
        choiceString = [choiceString stringByReplacingOccurrencesOfString:@"_as_" withString:@"{"];
        choiceString = [choiceString stringByReplacingOccurrencesOfString:@"_ad_" withString:@"}"];
    }
    
    if (encyrpt == YES) {
        choiceString = [choiceString stringByReplacingOccurrencesOfString:@"[" withString:@"_qw_"];
        choiceString = [choiceString stringByReplacingOccurrencesOfString:@"]" withString:@"_qe_"];
        choiceString = [choiceString stringByReplacingOccurrencesOfString:@"{" withString:@"_as_"];
        choiceString = [choiceString stringByReplacingOccurrencesOfString:@"}" withString:@"_ad_"];
    }

    return choiceString;
}

#pragma mark - Get String Value

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

#pragma mark - Change Date Format

- (NSString *)changeDateString:(NSString *)string fromFormat:(NSString *)fromFormat toFormat:(NSString *)toFormat {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = fromFormat;
    
    NSDate *date = [formatter dateFromString:string];
    formatter.dateFormat = toFormat;
    
    NSString *newDateString = [formatter stringFromDate:date];
    
    if (newDateString == nil) {
        newDateString = @"";
    }
    
    return newDateString;
}

#pragma mark - Confirmation Message Dialog

- (void)showAlertViewForActionType:(int)type andOperationSuccessful:(BOOL)success {
    NSString *butOkay = NSLocalizedString(@"Okay", nil);
    NSString *avTitle = @"";
    NSString *message = @"";
    
    if (success) {
        if (type == 1) {
            avTitle = @"Create Test";
            message = NSLocalizedString(@"Test successfully created.", nil);
        }
        if (type == 2) {
            avTitle = @"Edit Test";
            message = NSLocalizedString(@"Test successfully updated.", nil);
        }
        
        // To update test list view after saving or updating test and do nothing
        // whatever the operation status will be
        [self.tgdm requestTestListForUser:self.userid doneBlock:^(BOOL status){}];
    }
    else {
        if (type == 1) {
            avTitle = @"Create Test";
            message = NSLocalizedString(@"Test Bank is failed to create test.", nil);
        }
        if (type == 2) {
            avTitle = @"Edit Test";
            message = NSLocalizedString(@"Test Bank is failed to update test.", nil);
        }
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:avTitle
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *theAction = [UIAlertAction actionWithTitle:butOkay
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                          if (success) {
                                                              [self.navigationController popViewControllerAnimated:YES];
                                                          }
                                                      }];
    [alert addAction:theAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Test Notifications

- (void)setPreEntriesForTestCreation:(NSNotification *)notification {
    NSDictionary *testInfo = notification.userInfo;

    if (testInfo != nil) {
        NSString *testSection = [self stringValue:testInfo[@"testSection"]];
        NSDictionary *testData = testInfo[@"testData"];
        
        if ([testSection isEqualToString:@"testMainInfo"]) {
            self.preTestMainInfoEntries = [self getConcatenatedValuesForDictionary:testData];
        }
        
        if ([testSection isEqualToString:@"testSettings"]) {
            self.preTestSettingsEntries = [self getConcatenatedValuesForDictionary:testData];
        }
    }
}

- (void)setPreEntriesForTestQuestion:(NSNotification *)notification {
    NSDictionary *testInfo = notification.userInfo;
    
    if (testInfo != nil) {
        self.preTestAssignedQuestions = testInfo[@"preAssignedQuestions"];
    }
}

- (void)prepareToSaveOrUpdateTest:(NSNotification *)notification {
    // Check if there are changes to test
    BOOL didNotChange = ![self didTestContentsChange];
    
    // Check if there are empty values for required fields
    BOOL doesTestMainInfoHaveEmptyFields = [self areThereFieldsEmptyForDictionary:self.sharedTestContentManager.testMainInfo];
    BOOL doesTestSettingsHaveEmptyFields = [self areThereFieldsEmptyForDictionary:self.sharedTestContentManager.testSettings];
    
    // Check if there is at least one question assigned
    BOOL hasNoQuestionAssigned = self.sharedTestContentManager.testAssignedQuestions.count == 0;
    
    if (didNotChange || doesTestMainInfoHaveEmptyFields || doesTestSettingsHaveEmptyFields || hasNoQuestionAssigned) {
        self.isOkayToSaveOrUpdate = NO;
        self.saveAndCloseButton.enabled = NO;
        [self.saveAndCloseButton setBackgroundColor:[UIColor grayColor]];
    }
    else {
        self.isOkayToSaveOrUpdate = YES;
        self.saveAndCloseButton.enabled = YES;
        [self.saveAndCloseButton setBackgroundColor:UIColorFromHex(0x6BBE65)];
    }
}

- (BOOL)areThereFieldsEmptyForDictionary:(NSDictionary *)d {
    NSArray *keys = [d allKeys];
    
    // If empty
    if (d.count == 0) {
        return YES;
    }
    
    // If not empty but has empty value at least one key
    for (NSString *k in keys) {
        if ([d[k] isKindOfClass:[NSDictionary class]]) {
            if (d[k] == nil) {
                return YES;
            }
            else {
                BOOL hasEmptyFields = [self areThereFieldsEmptyForDictionary:d[k]];
                
                if (hasEmptyFields) {
                    return YES;
                }
            }
        }
        else {
            NSString *s = [self stringValue:d[k]];
            
            if ([s isEqualToString:@""]) {
                return YES;
            }
            
            // Check for fields with minimum acceptable values
            if ([k isEqualToString:@"attempts"]) {
                if ([s intValue] < [kDefaultAttempts intValue]) {
                    return YES;
                }
            }
            
            if ([k isEqualToString:@"passing_rate"]) {
                if ([s intValue] < [kDefaultPassingRate intValue]) {
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

@end
