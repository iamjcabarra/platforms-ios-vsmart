//
//  TBSectionSelectionView.h
//  V-Smart
//
//  Created by Julius Abarra on 12/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestGuruConstants.h"

@protocol TBSectionSelectionDelegate <NSObject>
@optional
- (void)didFinishGeneratingPDFprocess:(NSString *)processid testID:(NSString *)testid email:(BOOL)status playlist:(BOOL)flag;
@end

@interface TBSectionSelectionView : UIViewController

@property (weak, nonatomic) id <TBSectionSelectionDelegate> delegate;
@property (strong, nonatomic) NSManagedObject *testObject;
@property (assign, nonatomic) TGTBSwipeButtonActionType testSwipeButtonActionType;

@end
