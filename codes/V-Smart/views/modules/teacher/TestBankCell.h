//
//  TestBankCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 8/28/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestBankCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *testName;
@property (strong, nonatomic) IBOutlet UILabel *testDescription;
@property (strong, nonatomic) IBOutlet UILabel *testCount;

@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong, nonatomic) IBOutlet UIButton *editButton;
@property (strong, nonatomic) IBOutlet UIButton *deployButton;

@end
