//
//  TestGuruQuestionItemCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 28/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruQuestionItemCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionDifficultyLabel;
@property (strong, nonatomic) IBOutlet UIImageView *questionImage;
@property (strong, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (void)setQuestionTypeIcon:(NSString *)question_type_icon;
- (void)setDifficultyLevelID:(NSString *)difficulty_level_id;
- (void)setPublic:(NSString *)is_public;
- (void)loadWebViewWithContents:(NSString *)string;
@end
