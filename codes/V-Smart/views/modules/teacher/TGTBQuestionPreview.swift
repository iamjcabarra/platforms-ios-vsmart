//
//  TGTBQuestionPreview.swift
//  V-Smart
//
//  Created by Ryan Migallos on 11/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


@objc protocol TGTBQuestionPreviewDelegate: class {
    @objc optional func didPressSave()
    @objc optional func didPressEdit()
}

@objc class TGTBQuestionPreview: UIViewController, NSFetchedResultsControllerDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, TGTBPageViewDelegate, TGTBSidePanelDelegate, TGTBQuestionSamplerPageDelegate, TGTBAssignQuestionDelegate, TGTBTestOptionMenuDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    fileprivate var testOptionMenu: TGTBTestOptionMenu!
    fileprivate var popoverController: UIPopoverController!

    @IBOutlet var collection: UICollectionView!
    @IBOutlet var leftButton: UIButton!
    @IBOutlet var rightButton: UIButton!
    
    
    @IBOutlet weak var cancelButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    fileprivate var imageIndex : Int?
    fileprivate var previewData: [AnyHashable: Any]?
    
    var testObject: NSManagedObject?
    var selectedTestAction: TGTBSwipeButtonActionType?

    // MARK: - Shared Data Manager
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tm = TestGuruDataManager.sharedInstance()
        return tm!
    }()
    
    // CELL IDENTIFIER
    let cellID = "YGTB_QUESTION_PREVIEW_COLLECTION_ITEM"
    let kModalSegue = "TGTB_PRESENT_CUSTOM_PAGE"
    let kSidePanelModelSegue = "TGTB_PRESENT_CUSTOM_SIDE_PANEL"
    let kModalSectionSelection = "SHOW_SECTION_SELECTION_VIEW"
    let kEditTestView = "SHOW_EDIT_TEST_VIEW"

    weak var delegate: TGTBQuestionPreviewDelegate?
    var isEditingMode = false
    var index: IndexPath? = nil
    var totalItems: NSInteger? = 0
    
    let notif = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        //REGISTER NIB FILE
//        let cellNib = UINib(nibName: "TGTBQuestionPreviewCollectionItem", bundle: nil)
//        collection.registerNib(cellNib, forCellWithReuseIdentifier: cellID)

//        //REGISTER NIB FILE
//        let cellNib = UINib(nibName: "TGTBQuestionPrevCollectionItem", bundle: nil)
//        collection.registerNib(cellNib, forCellWithReuseIdentifier: cellID)
        
        
        leftButton.addTarget(self, action: #selector(TGTBQuestionPreview.cycleButtonAction(_:)), for: UIControlEvents.touchUpInside)
        rightButton.addTarget(self, action: #selector(TGTBQuestionPreview.cycleButtonAction(_:)), for: UIControlEvents.touchUpInside)
        
        setupNavigationControls()
        
        self.cancelButtonConstraint.constant = (isEditingMode) ? -110 : 0
        self.cancelButton.customSetTitle("Close Preview")
        
        self.cancelButton!.addTarget(self, action: #selector(TGTBQuestionPreview.cancelCellButtonAction(_:)), for: .touchUpInside)
        if isEditingMode {
            self.addButton.customSetTitle("Add")
            self.addButton.isHidden = false
        self.addButton!.addTarget(self, action: #selector(TGTBQuestionPreview.addCellButtonAction(_:)), for: .touchUpInside)
            self.saveButton.customSetTitle("Save")
            self.saveButton!.addTarget(self, action: #selector(TGTBQuestionPreview.saveButtonAction(_:)), for: .touchUpInside)
        } else {
            self.addButton.isHidden = true
            self.saveButton.isHidden = true
        }
        
        self.leftButton.isHidden = true
    }
    
    func cycleButtonAction(_ button:UIButton!) {
        
        let indexes = collection.indexPathsForVisibleItems
        let path: IndexPath = indexes.first!
        let section = (path as NSIndexPath).section
        var position: Int = (path as NSIndexPath).row
        
        if button == rightButton {
            position = position + 1
            
            if position >= totalItems {
                position = (totalItems! - 1)
            }
        }
        
        
        if button == leftButton {
            position = position - 1
            
            if position <= 0 {
                position = 0
            }
        }
        
        let indexPath = IndexPath(item: position, section: section)
        self.collection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        self.updateButtons("buttons", indexRow: (indexPath as NSIndexPath).row)
    }
    
    func updateButtons(_ action: String, indexRow: Int) {
        var currentPos : Int = 0
        let totalItems = self.collection.numberOfItems(inSection: 0)
        let lastIndex = totalItems - 1
        
        if action == "buttons"{
            currentPos = indexRow
        } else {
            currentPos = ((self.collection.indexPathsForVisibleItems.last as NSIndexPath?)?.row)!
        }
        
        if currentPos == 0 {
            self.leftButton.isHidden = true
            self.rightButton.isHidden = false
        }
        
        if currentPos == lastIndex {
            self.rightButton.isHidden = true
            self.leftButton.isHidden = false
        }
        
        if currentPos > 0 && currentPos < lastIndex {
            self.rightButton.isHidden = false
            self.leftButton.isHidden = false
        }
        
        // OVERRIDE: HIDE ALL WHEN TOTAL ITEMS IS 1
        if totalItems == 1 {
            self.leftButton.isHidden = true
            self.rightButton.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // collection view data source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            totalItems = 0
            return totalItems!
        }
        totalItems = sectionData.numberOfObjects
        
        if totalItems == 1 {
            self.leftButton.isHidden = true
            self.rightButton.isHidden = true
        }
        
        return totalItems!;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath)
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }

    fileprivate func configureCell(_ collectionCell: UICollectionViewCell!, atIndexPath indexPath: IndexPath) {
        
        let cell = collectionCell as! TGTBQuestionSamplerPage
        cell.delegate = self;
        
        let mo = fetchedResultsController.object(at: indexPath) 
        cell.headerView?.display(mo, withIndexRow: (indexPath as NSIndexPath).row, withTotalRow: totalItems!)
        cell.display(mo)
        
        cell.editButton?.isHidden = true
        cell.saveButton?.isEnabled = true
        cell.saveEditView?.isHidden = true;
    
//        cell.cancelButton!.addTarget(self, action: "cancelCellButtonAction:", forControlEvents: .TouchUpInside)
//        cell.addButton!.addTarget(self, action: "addCellButtonAction:", forControlEvents: .TouchUpInside)
    
    }
    
    func saveButtonAction(_ button:UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
        self.delegate?.didPressSave!()
    }
        
    func cancelCellButtonAction(_ button:UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
//        self.navigationController?.popViewControllerAnimated(false)
    }
    
    func addCellButtonAction(_ button:UIButton) {
        self.performSegue(withIdentifier: "TGTB_ASSIGN_QUESTION_VIEW", sender: self)
    }

    // flow layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.size.height
        let width = collectionView.frame.size.width
        return CGSize(width: width, height: height)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        print("---------------------------------------------> " + #function)
        resizeContents()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("---------------------------------------------> " + #function)
        self.delay(0.1) {
            self.updateButtons("", indexRow: 0)
        }
        resizeContents()
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("---------------------------------------------> " + #function)
        resizeContents()
    }
    
    fileprivate func resizeContents() {

        print("---------------------------------------------> " + #function)
        
        let layout = self.collection.collectionViewLayout as! UICollectionViewFlowLayout
        let size = self.collection.bounds.size
        let w = size.width
        let h = size.height
        
        layout.itemSize = CGSize(width: w, height: h)
        layout.invalidateLayout()
    }
    
    // helper functions
    func setupNavigationControls() {
        
        self.navigationItem.backBarButtonItem = nil
        self.navigationItem.hidesBackButton = true

        let backButton = generateCustomButton("icon_menubutton_150px.png", action: #selector(TGTBQuestionPreview.showSideMenu(_:)))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)

        if isEditingMode {
            self.checkDeleteButton()
        } else {
            let optionButton = generateCustomButton("icn_option_button.png", action: #selector(TGTBQuestionPreview.optionButtonAction(_:)))
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: optionButton)
        }
    }
    
    func optionButtonAction(_ sender: UIButton) {
        self.testOptionMenu = TGTBTestOptionMenu (nibName: "TGTBTestOptionMenu", bundle: nil)
        self.testOptionMenu.delegate = self
        
        self.popoverController = UIPopoverController (contentViewController: self.testOptionMenu)
        self.popoverController.contentSize = CGSize(width: 150.0, height: 72.0)
        _ = [self.popoverController .present(from: sender.bounds, in: sender, permittedArrowDirections: UIPopoverArrowDirection.up, animated: true)]
    }
    
    func generateCustomButton(_ imageName: String, action: Selector) -> UIButton {
        
        let button = UIButton(type: UIButtonType.system)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.customSetImage(imageName)
        button.tintColor = UIColor.white
        button.addTarget(self, action: action, for: .touchUpInside)

        return button
    }
    
    func showSideMenu(_ button:UIBarButtonItem) {
//        performSegueWithIdentifier(kModalSegue, sender: self)
        performSegue(withIdentifier: kSidePanelModelSegue, sender: self)
    }

    func deleteItemButtonAction(_ button:UIButton) {
        
        let indexes = collection.indexPathsForVisibleItems
        for indexPath in indexes {
            let mo = fetchedResultsController.object(at: indexPath)
            performDeleteOperationWithQuestion(mo)
        }
    }

    func checkDeleteButton() {
        let totalAssigned = self.dataManager.fetchCount(forEntity: kQuestionEntity)
        if isEditingMode {
            if totalAssigned > 1 {
                let trashButton = generateCustomButton("icon_vibe_delete_150px.png", action: #selector(TGTBQuestionPreview.deleteItemButtonAction(_:)))
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: trashButton)
            } else {
                self.navigationItem.rightBarButtonItem = nil;
            }
        }
        
    }

    func performDeleteOperationWithQuestion(_ object:NSManagedObject!) {

        let message = NSLocalizedString("Are you sure you want to delete this item?", comment: "comment")
        let yesString = NSLocalizedString("Yes", comment: "comment")
        let noString = NSLocalizedString("No", comment: "comment")

        let alertView = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
       
        let confirmAction = UIAlertAction(title: yesString, style: UIAlertActionStyle.cancel) { (Alert) -> Void in
            self.dataManager.removeQuestions(object, withAttribute: "id", doneBlock: nil)
            self.checkDeleteButton()
        }

        let cancelAction = UIAlertAction(title: noString, style: UIAlertActionStyle.default) { (Alert) -> Void in
            // do nothing
        }
        
        alertView.addAction(confirmAction)
        alertView.addAction(cancelAction)
        present(alertView, animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    //    - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //    if ([segue.identifier isEqualToString:@"SAMPLE_QUESTION_IMAGE_VIEW"]) {
    //    QuestionImagePreview *imagePreview = (QuestionImagePreview *)[segue destinationViewController];
    //    imagePreview.previewData = self.previewData;
    //    imagePreview.index = self.index;
    //    }
    //    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == kModalSegue {
             let pgv = segue.destination as! TGTBPageView
            pgv.delegate = self;
        }

        if segue.identifier == kSidePanelModelSegue {
            let spv = segue.destination as! TGTBSidePanel
            spv.delegate = self;
        }
        
        if segue.identifier == "TGTB_ASSIGN_QUESTION_VIEW" {
            let aqv = segue.destination as! TGTBAssignQuestionView
            let items = self.dataManager.getObjectsForEntity(kQuestionEntity, predicate: nil) as! [NSManagedObject]
            let setOfDictObjects = NSMutableSet()
            
            for object in items {
                let keys = Array(object.entity.propertiesByName.keys)
                let dict = object.dictionaryWithValues(forKeys: keys)
                setOfDictObjects.add(dict)
            }
            
            aqv.setOfAssignedObjects = setOfDictObjects
            aqv.delegate = self
        }
        
        if segue.identifier == "SAMPLE_QUESTION_IMAGE_VIEW" {
            let qip = segue.destination as! QuestionImagePreview
            qip.previewData = self.previewData
            qip.index = self.imageIndex!
        }
        
        if segue.identifier == kModalSectionSelection {
            let ssv = segue.destination as! TBSectionSelectionView
            ssv.testObject = self.testObject
            ssv.testSwipeButtonActionType = self.selectedTestAction!
        }

        if segue.identifier == kEditTestView {
            let etv = segue.destination as! TGTBCreateTestView
            etv.customBackBarButtonTitle = self.title
            etv.testObject = self.testObject
            etv.crudActionType = TGTBCrudActionType.edit
            etv.isFromTestDetailsView = true
        }
    }
    
    // MARK: - Navigation
    func didFinishSelectingObject(_ questionid: String, indexPath: IndexPath) {
        self.collection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.updateButtons("buttons", indexRow: (indexPath as NSIndexPath).row)
        resizeContents()
    }
    
    // MARK - TGTBTestOptionMenu Delegate
    func selectedTestOption(_ option: TGTBSwipeButtonActionType) {
        self.selectedTestAction = option
        
        // Deploy Test
        if (option == .deploy) {
            performSegue(withIdentifier: kModalSectionSelection, sender: nil)
        }
        
        // Save to PDF
        if (option == .saveAsPDF) {
            performSegue(withIdentifier: kModalSectionSelection, sender: nil)
        }
        
        // Edit Test
        if (option == .edit) {
            self.delegate?.didPressEdit!()
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    /////////////////////////////////// NSFETCHRESULTSCONTROLLER ///////////////////////////////////
    
    /*
    NOTE: CUSTOM ANIMATION
    */
    // MARK: - Fetched results controller
    
    fileprivate lazy var customDescriptors: [NSSortDescriptor] = {
        
        let ascending = false
        
        let date_modified = NSSortDescriptor(key: "sort_date", ascending: ascending)
        let question_text = NSSortDescriptor(key: "question_text", ascending: ascending)
        let question_id = NSSortDescriptor(key: "id", ascending: ascending)
        
        return [date_modified, question_text, question_id]
    }()
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        // Set ManagedObjectContext
        let ctx = dataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: kQuestionEntity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kQuestionEntity)
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        fetchRequest.sortDescriptors = customDescriptors
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest,
            managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    fileprivate var blockOperation = BlockOperation()
    fileprivate var shouldReloadCollectionView = false
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.shouldReloadCollectionView = false
        self.blockOperation = BlockOperation()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
        didChange sectionInfo: NSFetchedResultsSectionInfo,
        atSectionIndex sectionIndex: Int,
        for type: NSFetchedResultsChangeType) {
            
        let collectionView = self.collection
        switch type {
        case NSFetchedResultsChangeType.insert:
            self.blockOperation.addExecutionBlock({
                collectionView?.insertSections( IndexSet(integer: sectionIndex) )
            })
        case NSFetchedResultsChangeType.delete:
            self.blockOperation.addExecutionBlock({
                collectionView?.deleteSections( IndexSet(integer: sectionIndex) )
            })
        case NSFetchedResultsChangeType.update:
            self.blockOperation.addExecutionBlock({
                collectionView?.reloadSections( IndexSet(integer: sectionIndex ) )
            })
        default:()
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
        didChange anObject: Any,
        at indexPath: IndexPath?,
        for type: NSFetchedResultsChangeType,
        newIndexPath: IndexPath?) {
            
        let collectionView = self.collection
            
        switch type {
            
        case NSFetchedResultsChangeType.insert:
            
            if (collectionView?.numberOfSections)! > 0 {
                
                if collectionView?.numberOfItems( inSection: (newIndexPath! as NSIndexPath).section ) == 0 {
                    self.shouldReloadCollectionView = true
                } else {
                    self.blockOperation.addExecutionBlock( {
                            collectionView?.insertItems( at: [newIndexPath!] )
                    } )
                }
                
            } else {
                self.shouldReloadCollectionView = true
            }
            
        case NSFetchedResultsChangeType.delete:
            
            if collectionView?.numberOfItems( inSection: (indexPath! as NSIndexPath).section ) == 1 {
                self.shouldReloadCollectionView = true
            } else {
                self.blockOperation.addExecutionBlock( {
                    collectionView?.deleteItems( at: [indexPath!] )
                } )
            }
            
        case NSFetchedResultsChangeType.update:
            self.blockOperation.addExecutionBlock({
                collectionView?.reloadItems( at: [indexPath!] )
            })
            
        case NSFetchedResultsChangeType.move:
            self.blockOperation.addExecutionBlock({
                collectionView?.moveItem( at: indexPath!, to: newIndexPath! )
            })
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        // Checks if we should reload the collection view to fix a bug @ http://openradar.appspot.com/12954582
        if self.shouldReloadCollectionView {
            self.collection.reloadData()
        } else {
            self.collection.performBatchUpdates({ self.blockOperation.start() }, completion: nil )
        }
    }
//    - (void)transitionWithData:(NSDictionary *)previewData index:(NSInteger)index segueID:(NSString *)segueIdentifier {
//    self.previewData = [NSDictionary dictionaryWithDictionary:previewData];
//    self.index = index;
//    [self performSegueWithIdentifier:segueIdentifier sender:self];
//    }
//    
    
    func transition(withData previewData: [AnyHashable: Any]!, index: Int, segueID segueIdentifier: String!) {
        self.previewData = previewData
        self.imageIndex = index
        self.performSegue(withIdentifier: segueIdentifier, sender: self)
    }
        
    func didFinishAssigning(_ status: Bool) {
        self.notif.post(name: Notification.Name(rawValue: kNotificationTestCreateVerification), object: nil)
        UIView.performWithoutAnimation {
            self.collection.reloadItems(at: self.collection.indexPathsForVisibleItems)
        }
        self.checkDeleteButton()
    }
}
