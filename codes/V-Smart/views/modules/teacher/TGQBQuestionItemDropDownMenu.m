//
//  TGQBQuestionItemDropDownMenu.m
//  V-Smart
//
//  Created by Ryan Migallos on 10/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGQBQuestionItemDropDownMenu.h"
#import "TGQBQuestionItemList.h"
#import "TestGuruDataManager.h"
#import "VSmartValues.h"

@interface TGQBQuestionItemDropDownMenu() <UIPopoverControllerDelegate, TGQBQuestionItemListDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *checkImage;
@property (strong, nonatomic) IBOutlet UIImageView *arrowImage;
@property (strong, nonatomic) IBOutlet UILabel *labelItem;
@property (strong, nonatomic) IBOutlet UIButton *selectionButton;
@property (strong, nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) TestGuruDataManager *tm;

@property (strong, nonatomic) TGQBQuestionItemList *itemListController;

@end

@implementation TGQBQuestionItemDropDownMenu

- (id)initWithFrame:(CGRect)aRect {
    
    if ((self = [super initWithFrame:aRect])) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder*)coder {
    
    if ((self = [super initWithCoder:coder])) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization {
    
    self.tm = [TestGuruDataManager sharedInstance];
    [self setupOptionMenu];
}

- (IBAction)showQuestionItemList:(UIButton *)sender {
    
//    NSUInteger item_count = [self.tm fetchCountForEntity:kQuestionEntity];
//    if ( (item_count > 1) && (self.popover.isPopoverVisible == NO) ) {
        UIButton *b = (UIButton *)sender;
    
    NSUInteger item_count = [self.tm fetchCountForEntity:kQuestionEntity];
    
    CGFloat height = 44 * item_count;
    CGFloat width = self.bounds.size.width;
    self.itemListController.preferredContentSize = CGSizeMake(width, height);
    self.popover.popoverContentSize = CGSizeMake(width, height);
    
    [self.popover presentPopoverFromRect:b.bounds
                                  inView:b
                permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    
        self.arrowImage.image = [UIImage imageNamed:@"icn_up_arrow_black.png"];
//    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
        self.arrowImage.image = [UIImage imageNamed:@"icn_down_arrow_black.png"];
}

- (void)setupOptionMenu {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
    TGQBQuestionItemList *om = (TGQBQuestionItemList *)[sb instantiateViewControllerWithIdentifier:@"tgqb_question_item_list"];
    [om setupNotification];
    
    self.itemListController = om;
    
//    NSUInteger item_count = [self.tm fetchCountForEntity:kQuestionEntity];
    
//    CGFloat height = 44 * item_count;
//    height = height + 44;
//    CGFloat width = self.bounds.size.width;
//    om.preferredContentSize = CGSizeMake(width, height);
    om.delegate = self;
    self.popover = [[UIPopoverController alloc] initWithContentViewController:om];
//    self.popover.popoverContentSize = CGSizeMake(width, height);
    self.popover.delegate = self;
}

- (void)teardownNotificationForItemList {
    TGQBQuestionItemList *om = (TGQBQuestionItemList *)self.popover.contentViewController;
    [om teardownNotification];
}

- (void)selectedMenuItem:(NSString *)string withObject:(NSManagedObject *)object withIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    self.labelItem.text = [NSString stringWithFormat:@"%@", string];
    
    NSString *complete = [NSString stringWithFormat:@"%@", [object valueForKey:@"complete"] ];
    NSString *image_string = ([complete isEqualToString:@"1"]) ? @"check_icn_active150.png" : @"check_icn150.png";
    self.checkImage.image = [UIImage imageNamed:image_string];
    
    
    if ([(NSObject *)self.delegate respondsToSelector:@selector(didFinishSelectingItem:withObject:withIndexPath:)]) {
        [self.delegate didFinishSelectingItem:string withObject:object withIndexPath:indexPath];
    }
    
    if (self.popover.isPopoverVisible) {
        [self.popover dismissPopoverAnimated:YES];
        self.arrowImage.image = [UIImage imageNamed:@"icn_down_arrow_black.png"];
    }
}

- (void)displayCheckState:(NSString *)complete {
    if (complete != nil) {
        NSString *image_string = ([complete isEqualToString:@"1"]) ? @"check_icn_active150" : @"check_icn150.png";
        self.checkImage.image = [UIImage imageNamed:image_string];
    }
}

- (void)applyShadowToView:(UIView *)object {
    
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.5f;
}

@end
