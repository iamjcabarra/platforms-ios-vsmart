//
//  QuestionBankCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 7/28/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionBankCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *questionName;
@property (strong, nonatomic) IBOutlet UILabel *questionType;
@property (strong, nonatomic) IBOutlet UIButton *editButton;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;

@end
