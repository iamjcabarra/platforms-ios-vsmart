//
//  QuestionImageRadioItemCell.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 03/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionImageRadioItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *radioButton;
@property (weak, nonatomic) IBOutlet UILabel *buttonTitle;
@property (weak, nonatomic) IBOutlet UIImageView *choiceImage;
@property (weak, nonatomic) IBOutlet UIButton *choiceImageButton;

@end
