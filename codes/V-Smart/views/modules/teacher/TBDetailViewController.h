//
//  TBDetailViewController.h
//  TestBankVersion2.1
//
//  Created by Julius Abarra on 06/02/2016.
//  Copyright © 2016 Vibe Technologies, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBDetailViewController : UIViewController

@property (strong, nonatomic) NSString *customBackBarButtonTitle;
@property (strong, nonatomic) NSManagedObject *testObject;
@property (assign, nonatomic) int actionType;

@end
