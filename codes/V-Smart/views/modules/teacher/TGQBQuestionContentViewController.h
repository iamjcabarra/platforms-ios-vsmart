//
//  TGQBQuestionContentViewController.h
//  V-Smart
//
//  Created by Ryan Migallos on 11/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGQBQuestionContentViewController : UITableViewController
- (void)setObjectData:(NSManagedObject *)object;
@end
