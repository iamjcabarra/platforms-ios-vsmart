//
//  TestGuruQuestionGroupView.m
//  V-Smart
//
//  Created by Carmelito Bayarcal on 21/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestGuruQuestionGroupView.h"
#import "TestGuruDataManager.h"
#import "TestGuruQuestionGroupHeader.h"
#import "QuestionGroupTableViewCell.h"
#import "QuestionSamplerView.h"
#import "TGQuestionDetailsView.h"
#import "HUD.h"
#import "TGQBQuestionDetailViewController.h"
#import "TestGuruListView.h"
#import "TestGuruConstants.h"
#import "UIImageView+WebCache.h"


@interface TestGuruQuestionGroupView () <NSFetchedResultsControllerDelegate, UIScrollViewDelegate, UITableViewDelegate, QuestionGroupTableViewDelegate, TGQBQuestionDetailDelegate, QuestionSamplerDelegate>

@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UILabel *emptyViewMessage;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSIndexPath *prev;
@property (assign, nonatomic) BOOL editOperation;

@property (strong, nonatomic) NSString *question_id_new;

@property (strong, nonatomic) NSIndexPath *previewIndexPath;
@property (strong, nonatomic) NSIndexPath *currentIndexPath;
@property (weak, nonatomic) IBOutlet UIButton *loadMoreButton;

@property (strong, nonatomic) UIButton *sortButton;
@property (assign, nonatomic) BOOL sortOrderAscending;

//Collapsible table paging
@property (assign, nonatomic) NSInteger ctotal_parsed;
@property (assign, nonatomic) NSInteger ctotal_filtered;
@property (assign, nonatomic) NSInteger ctotal_pages;
@property (assign, nonatomic) NSInteger ccurrent_page;

//Question table paging
@property (assign, nonatomic) NSInteger total_parsed;
//@property (assign, nonatomic) NSInteger total_filtered;
//@property (assign, nonatomic) NSInteger total_pages;
//@property (assign, nonatomic) NSInteger current_page;

@property (strong, nonatomic) TestGuruQuestionGroupHeader *selectedHeaderView;
@property (strong, nonatomic) TestGuruQuestionGroupHeader *previousHeaderView;

@property (strong, nonatomic) NSDictionary *final_data;

@property (strong, nonatomic) NSString *headerLinkType;
@property (strong, nonatomic) NSString *questionLinkType;
@property (strong, nonatomic) NSManagedObject *headerObject;

@property (weak, nonatomic) IBOutlet UILabel *packageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *packageImage;


@property (strong, nonatomic) NSManagedObject *editObject;


@end

@implementation TestGuruQuestionGroupView
static NSString *kQuestionGroupCellIdentifier = @"question_group_cell_identifier";
static NSString *kQuestionGroupHeaderIdentifier = @"question_group_header_identifier";

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.tm = [TestGuruDataManager sharedInstance];
    
    self.sortOrderAscending = YES;
    
    [self setUpLinkTypes];
    self.ctotal_filtered = [self.pagingInformation[@"total_filtered"] integerValue];
    self.ctotal_pages = [self.pagingInformation[@"total_pages"] integerValue];
    self.ccurrent_page = [self.pagingInformation[@"current_page"] integerValue];
    self.ctotal_parsed = [self.pagingInformation[@"total_parsed"] integerValue];
    
    [self.loadMoreButton addTarget:self action:@selector(loadMore:) forControlEvents:UIControlEventTouchUpInside];
    
    [self checkLoadMore];
    
    [self setupRightBarButton];
    
    self.currentIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *package_type_name = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_NAME]];
        NSString *package_type_image_url = [self.tm stringValue:[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_URL]];
        [wo.packageImage sd_setImageWithURL:[NSURL URLWithString:package_type_image_url]];
        wo.packageLabel.text = [package_type_name uppercaseString];
    });
    
    [self customizeNavigationController];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
//    NSString *package_type_name = [NSString stringWithFormat:@"%@", [self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_NAME]];
////    NSData *package_type_image_data = [NSData dataWithData:[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_DATA] ];
//    
//    NSString *package_type_image_url = [self.tm stringValue:[self.tm fetchObjectForKey:kTGQB_SELECTED_PACKAGE_IMAGE_URL]];
//    
//    __weak typeof(self) wo = self;
//    dispatch_async(dispatch_get_main_queue(), ^{
////        wo.packageImage.image = [UIImage imageWithData:package_type_image_data];
//        [wo.packageImage sd_setImageWithURL:[[NSURL alloc] initWithString:package_type_image_url]];
//        wo.packageLabel.text = [package_type_name uppercaseString];
//    });
}

- (void)performReload {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [self.tm requestHeadersWithLinkType:self.headerLinkType withPage:@"1" dataBlock:^(NSDictionary *data) {
        self.ctotal_filtered = [data[@"total_filtered"] integerValue];
        self.ctotal_pages = [data[@"total_pages"] integerValue];
        self.ccurrent_page = [data[@"current_page"] integerValue];
        self.ctotal_parsed = [data[@"total_parsed"] integerValue];
        self.currentIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        
        [self checkLoadMore];
    }];
}

- (void)performUpdateLabel {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:self.currentIndexPath];
    NSString *section_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_id"]];
    
    [self.tm updateGroupByCountWithSectionID:section_id doneBlock:^(BOOL status) {
    dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"RELOAD THE TABLE!!! SECTION %ld", (long)self.currentIndexPath.section);
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:self.currentIndexPath.section] withRowAnimation:UITableViewRowAnimationNone];
    });
    }];
}

- (void)customizeNavigationController {
    
    UIColor *color = UIColorFromHex(0xF69C42);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    } else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

- (void)setupRightBarButton {
    SEL sortAction = @selector(sortButtonAction:);
    self.sortButton = [self buttonWithName:@"Sort" position:80 action:sortAction];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.sortButton];
}

- (void)sortButtonAction:(UIButton *)sender {
    NSLog(@"%s -----> ", __PRETTY_FUNCTION__);
    
    sender.selected = (sender.selected) ? NO : YES;
    
    self.sortOrderAscending = !sender.selected;
    NSInteger reverseSec = [[self.fetchedResultsController sections] count] - self.prev.section - 1;
    NSIndexPath *reverseIndexPath = [NSIndexPath indexPathForRow:0 inSection:reverseSec];
    self.prev = reverseIndexPath;
    
    [self reloadFetchedResultsController];
}

- (UIButton *)buttonWithName:(NSString *)title position:(CGFloat)position action:(SEL)action  {
    
    UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
    b.frame = CGRectMake(0, 0, 44, 44);
    b.imageEdgeInsets = UIEdgeInsetsMake(7, 7, 7, 7);
    b.showsTouchWhenHighlighted = YES;
    
    [b setImage:[UIImage imageNamed:@"icn_sort_name_asc.png"] forState:UIControlStateNormal];
    [b setImage:[UIImage imageNamed:@"icn_sort_name_desc.png"] forState:UIControlStateSelected];
    
    [b addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    return b;
}

- (void)setUpLinkTypes {
    NSString *groupBy = [NSString stringWithFormat:@"%@", self.groupByType];
    
    /** 
     headerLinktypes : tags,difficulty,learningskills,questiontypes, competencies
     questionLinktypes : tag,difficultylevel,learningskill,type, competencycode
     **/
    
    if ([groupBy isEqualToString:@"TAGS"]) {
        
        self.headerLinkType = @"tags";
        self.questionLinkType = @"tag";
        
    } else if ([groupBy isEqualToString:@"QUESTION_TYPES"]) {
        
        self.headerLinkType = @"questiontypes";
        self.questionLinkType = @"type";
        
    } else if ([groupBy isEqualToString:@"DIFFICULTY_LEVELS"]) {
        
        self.headerLinkType = @"difficulty";
        self.questionLinkType = @"difficultylevel";
        
    } else if ([groupBy isEqualToString:@"LEARNING_SKILLS"]) {
        
        self.headerLinkType = @"learningskills";
        self.questionLinkType = @"learningskill";
        
    } else if ([groupBy isEqualToString:@"COMPETENCY_CODES"]) {
        
        self.headerLinkType = @"competencies";
        self.questionLinkType = @"competencycode";
        
    }
}

- (void)checkLoadMore {
    BOOL isHidden = YES;
    
    if (self.ctotal_pages > self.ccurrent_page) {
        isHidden = NO;
    }
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        wo.loadMoreButton.enabled = YES;
        wo.loadMoreButton.hidden = isHidden;
    });
}

- (void)loadNextPage {
    NSString *nextPage = [NSString stringWithFormat:@"%ld", (long)self.ccurrent_page + 1];
    
    if (self.ctotal_pages > self.ccurrent_page) {
        self.loadMoreButton.enabled = NO;
        [self.tm requestHeadersWithLinkType:self.headerLinkType withPage:nextPage dataBlock:^(NSDictionary *data) {
                if (data != nil) {
                    self.ctotal_filtered = [data[@"total_filtered"] integerValue];
                    self.ctotal_pages = [data[@"total_pages"] integerValue];
                    self.ccurrent_page = [data[@"current_page"] integerValue];
                }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self reloadFetchedResultsController];
            });
            
                [self checkLoadMore];
        }];
    }
}

- (void)loadMore:(UIButton *)sender {
    NSLog(@"%s -----> ", __PRETTY_FUNCTION__);
    [self loadNextPage];
}

#pragma mark TableView DataSource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"testing_identifier"];
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    BOOL is_collapsed = [(NSNumber *)[mo valueForKey:@"is_collapsed"] boolValue];
    
    
    if (((self.currentIndexPath.row == indexPath.row) && (self.currentIndexPath.section == indexPath.section)) || (is_collapsed == YES)) {
        cell=[tableView dequeueReusableCellWithIdentifier:kQuestionGroupCellIdentifier];
    [self configureCell:cell atIndexPath:indexPath];
    }
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)c atIndexPath:(NSIndexPath *)indexPath {
    QuestionGroupTableViewCell *cell = (QuestionGroupTableViewCell *)c;
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *section_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_id"]];
    
    cell.section_id = section_id;
    cell.delegate = self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSString *groupBy = [NSString stringWithFormat:@"%@", self.groupByType];
    NSInteger count = [[self.fetchedResultsController sections] count];
    
    NSString *questionCountTitleA = @"";
    NSString *questionCountTitleB = @"";
    NSString *emptyMessage = @"";
    
    if ([groupBy isEqualToString:@"TAGS"]) {
        
        questionCountTitleA = NSLocalizedString(@"Tags", nil);
        questionCountTitleB = NSLocalizedString(@"Tag", nil);
        emptyMessage = NSLocalizedString(@"No available tags", nil);
        
    } else if ([groupBy isEqualToString:@"QUESTION_TYPES"]) {
        
        questionCountTitleA = NSLocalizedString(@"Question Types", nil);
        questionCountTitleB = NSLocalizedString(@"Question Type", nil);
        emptyMessage = NSLocalizedString(@"No available question types", nil);
        
    } else if ([groupBy isEqualToString:@"DIFFICULTY_LEVELS"]) {
        
        questionCountTitleA = NSLocalizedString(@"Difficulty Levels", nil);
        questionCountTitleB = NSLocalizedString(@"Difficulty Level", nil);
        emptyMessage = NSLocalizedString(@"No available difficulty levels", nil);
        
    } else if ([groupBy isEqualToString:@"LEARNING_SKILLS"]) {
        
        questionCountTitleA = NSLocalizedString(@"Learning Skills", nil);
        questionCountTitleB = NSLocalizedString(@"Learning Skill", nil);
        emptyMessage = NSLocalizedString(@"No available learning skills", nil);
        
    } else if ([groupBy isEqualToString:@"COMPETENCY_CODES"]) {
        
        questionCountTitleA = NSLocalizedString(@"Competency Codes", nil);
        questionCountTitleB = NSLocalizedString(@"Competency Code", nil);
        emptyMessage = NSLocalizedString(@"No available competency code", nil);
        
    }
    
    NSString *questionTitle = (count > 1) ? questionCountTitleA : questionCountTitleB;
    NSString *countTitle = [NSString stringWithFormat:@"%@ %@ (%ld)", NSLocalizedString(@"Filter By", nil), questionTitle, (long)count];
    
    self.title = countTitle;
    
    if (count == 0) {
        self.emptyView.hidden = NO;
        self.emptyViewMessage.text = emptyMessage;
        tableView.hidden = YES;
    } else {
        self.emptyView.hidden = YES;
        self.emptyViewMessage.text = @"";
        tableView.hidden = NO;
    }
    
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    BOOL is_collapsed = [(NSNumber *)[mo valueForKey:@"is_collapsed"] boolValue];
    CGFloat height = 0;
    
    if (is_collapsed) {
        height = self.total_parsed * 215;
        if (height > self.tableView.frame.size.height) {
            height = self.tableView.frame.size.height/1.3;
        }
    }
    
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.0f;
}

#pragma mark - Creating View for TableView Section

- (void)collapseHeader:(TestGuruQuestionGroupHeader *)groupHeader withSectionIndex:(NSInteger)indexSection{
    groupHeader.backgroundView.backgroundColor = [UIColor whiteColor];
    groupHeader.seeMoreButton.hidden = YES;
    groupHeader.collapseIndicator.text = @"+";
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexSection] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)expandHeader:(TestGuruQuestionGroupHeader *)groupHeader withSectionIndex:(NSInteger)indexSection {
    groupHeader.backgroundView.backgroundColor = UIColorFromHex(0xC8EBFF);
    groupHeader.collapseIndicator.text = @"-";
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexSection] withRowAnimation:UITableViewRowAnimationNone];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    BOOL is_collapsed = [(NSNumber *)[mo valueForKey:@"is_collapsed"] boolValue];
    NSString *section_header = [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_header"]];
    NSString *item_count = [NSString stringWithFormat:@"%@", [mo valueForKey:@"item_count"] ];
    
    TestGuruQuestionGroupHeader* v = [[[NSBundle mainBundle] loadNibNamed:@"TestGuruQuestionGroupHeader" owner:self options:nil] firstObject];
    v.sectionNameLbl.text = [NSString stringWithFormat:@"%@ ( %@ )", section_header, item_count];
    
    if (is_collapsed) {
        [self expandHeader:v withSectionIndex:indexPath.section];
        v.seeMoreButton.hidden = ([item_count integerValue] > 10) ? NO : YES;
    } else {
        [self collapseHeader:v withSectionIndex:indexPath.section];
    }
    
    v.seeMoreButton.tag = section;
    [v.seeMoreButton addTarget:self action:@selector(seeMoreButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    v.tag = section;
    
    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [v addGestureRecognizer:headerTapped];
    
    return v;
}

- (void)seeMoreButtonAction:(UIButton *)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    
    NSLog(@"ROW [%ld], SEC [%ld]", (long)indexPath.row, (long)indexPath.section);
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    self.headerObject = mo;
    dispatch_async(dispatch_get_main_queue(), ^{
       [self performSegueWithIdentifier:@"QUESTIONS_MODULE_OBJECT" sender:self]; 
    });
}

#pragma mark - Table header gesture tapped

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TestGuruQuestionGroupHeader* v = (TestGuruQuestionGroupHeader *)gestureRecognizer.view;
    self.selectedHeaderView = v;
    
    BOOL is_collapsed = [(NSNumber *)[mo valueForKey:@"is_collapsed"] boolValue];
    
    if (is_collapsed) {
        [self.tm updateGroupByObject:nil withPrevious:mo];
        [self collapseHeader:v withSectionIndex:indexPath.section];
    } else {
        self.currentIndexPath = indexPath;
        [self requestPageOne:mo];
    }
}

//- (void)transferData:(NSIndexPath *)indexPath {
//    QuestionGroupTableViewCell *selectedCell = (QuestionGroupTableViewCell *)[self.tableView cellForRowAtIndexPath:self.currentIndexPath];
//    selectedCell.total_filtered = self.total_parsed;
//    NSLog(@"SELECTEDCELL.TOTAL FILTERED = [%ld]", (long)selectedCell.total_filtered);
//}

- (void)checkPrevious:(NSIndexPath *)indexPath {
    
    NSLog(@"PREV INDEX PATH [%@]", self.prev);
    
    if (self.prev != indexPath) {
        if (self.prev != nil) {
            NSManagedObject *prevObject = [self.fetchedResultsController objectAtIndexPath:self.prev];
            [self.tm updateGroupByObject:nil withPrevious:prevObject];
            [self collapseHeader:self.previousHeaderView withSectionIndex:self.prev.section];
        }
        self.prev = indexPath;
        self.previousHeaderView = self.selectedHeaderView;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)transitionWithObject:(NSManagedObject *)editObject withSegueIdentifier:(NSString *)identifier withIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"---->%s <----- ", __PRETTY_FUNCTION__);
    NSString *segueIdentifier = [NSString stringWithFormat:@"%@", identifier];
    
    if ([segueIdentifier isEqualToString:@"SAMPLE_QUESTION_VIEW"]) {
        self.previewIndexPath = indexPath;
    }
    
    if ([segueIdentifier isEqualToString:@"SHOW_EDIT_QUESTION_DETAILS_V22"]) {
        self.editObject = editObject;
        self.editOperation = YES;
    }
    
    [self performSegueWithIdentifier:identifier sender:self];
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    if ([segue.identifier isEqualToString:@"SAMPLE_QUESTION_VIEW"]) {
        QuestionSamplerView *sampler = (QuestionSamplerView *)[segue destinationViewController];
        sampler.indexPath = self.previewIndexPath;
        sampler.entityObject = kQuestionBankEntity;
        sampler.sortDescriptors = [self createPreviewSortDescriptor];
        sampler.delegate = self;
        
    }
    
    if ([segue.identifier isEqualToString:@"SHOW_EDIT_QUESTION_DETAILS_V22"]) {
        [self displayQuestionDetailsForSegue:segue];
    }
    
    if ([segue.identifier isEqualToString:@"QUESTIONS_MODULE_OBJECT"]) {
        TestGuruListView *listView = (TestGuruListView *)[segue destinationViewController];
        listView.isGroupBy = YES;
        listView.linkType = self.questionLinkType;
        listView.groupByType = self.groupByType;
        listView.headerObject = self.headerObject;
    }
    
}

- (NSArray *)createPreviewSortDescriptor {
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"sort_date" ascending:NO];
    NSSortDescriptor *question_text = [NSSortDescriptor sortDescriptorWithKey:@"question_text" ascending:NO];
    NSSortDescriptor *question_id = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:NO];
    return @[date_modified, question_text, question_id];
}

- (void)displayQuestionDetailsForSegue:(UIStoryboardSegue *)segue {
    
    NSManagedObject *object = nil;
    
    if (self.editOperation == YES) {
        object = self.editObject;
    }
    
    if (object != nil) {
        TGQBQuestionDetailViewController *qd = (TGQBQuestionDetailViewController *)[segue destinationViewController];
        [qd setQuestionObject:object];
        qd.delegate = self;
        qd.editMode = self.editOperation;
    }
}

- (void)didFinishCreateOperation:(BOOL)state {
    NSLog(@"%s ------", __PRETTY_FUNCTION__);
    self.loadMoreButton.enabled = NO;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.currentIndexPath.row inSection:self.currentIndexPath.section];
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self requestPageOne:mo];
}

- (void)requestPageOne:(NSManagedObject *)mo {
    NSString *section_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_id"]];

    NSString *indicatorStr = NSLocalizedString(@"Loading", nil);
    [HUD showUIBlockingIndicatorWithText:indicatorStr];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.currentIndexPath.row inSection:self.currentIndexPath.section];
    [self checkPrevious:indexPath];
    
    dispatch_queue_t queue = dispatch_queue_create("com.vibetech.types.GROUPBY",DISPATCH_QUEUE_SERIAL);
    dispatch_group_t group = dispatch_group_create();
    
    dispatch_group_enter(group);
    dispatch_group_async(group, queue, ^{
        [self.tm requestQuestionForLinkType:self.questionLinkType withID:section_id withPage:@"1" withKeyword:@"" withLimit:@"10" dataBlock:^(NSDictionary *data) {
            self.final_data = data;
            dispatch_group_leave(group);
        }];
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        if (self.final_data != nil) {
            self.total_parsed = [self.final_data[@"total_parsed"] integerValue];
//            [self transferData:indexPath];
            [self.tm updateGroupByObject:mo withPrevious:nil];
            [self updateSectionHeader:mo];
        } else {
            [self displayAlert];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD hideUIBlockingIndicator];
        });
    });
}

- (void)updateSectionHeader:(NSManagedObject *)mo {
    NSString *item_count = [NSString stringWithFormat:@"%@", [mo valueForKey:@"item_count"] ];
    self.selectedHeaderView.seeMoreButton.hidden = ([item_count integerValue] > 10) ? NO : YES;
    [self expandHeader:self.selectedHeaderView withSectionIndex:self.currentIndexPath.section];
}

- (void)displayAlert {
    NSString *butOkay = NSLocalizedString(@"Close", nil);
    
    NSString *groupBy = [NSString stringWithFormat:@"%@", self.groupByType];

    NSString *message = @"";
    
    if ([groupBy isEqualToString:@"TAGS"]) {

        message = NSLocalizedString(@"No question for this tag.", nil);
        
    } else if ([groupBy isEqualToString:@"QUESTION_TYPES"]) {
        
        message = NSLocalizedString(@"No question for this question type.", nil);
        
    } else if ([groupBy isEqualToString:@"DIFFICULTY_LEVELS"]) {
        
        message = NSLocalizedString(@"No question for this difficulty level.", nil);
        
    } else if ([groupBy isEqualToString:@"LEARNING_SKILLS"]) {
        
        message = NSLocalizedString(@"No question for this learning skills.", nil);
        
    } else if ([groupBy isEqualToString:@"COMPETENCY_CODES"]) {
        
        message = NSLocalizedString(@"No question for this competency code.", nil);
        
    }
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                 message:message
                                                delegate:self
                                       cancelButtonTitle:butOkay
                                       otherButtonTitles:nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [av show];
    });
}

////////////
//QuestionSamplerDelegate
///////////

- (void)didSelectEditIndex:(NSIndexPath *)indexPath withEditObject:(NSManagedObject *)object {
    self.editOperation = YES;
    self.editObject = object;
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wo performSegueWithIdentifier:@"SHOW_EDIT_QUESTION_DETAILS_V22" sender:self];
    });
}



#pragma mark - Fetched results controller

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (NSFetchedResultsController *)fetchedResultsController
{
    //    BOOL isAscending = NO;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionGroupByEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *section_id = [NSSortDescriptor sortDescriptorWithKey:@"section_header" ascending:self.sortOrderAscending];
    [fetchRequest setSortDescriptors:@[section_id]];
    
    // Edit the section name key path and cache name if appropriate.
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:@"section_id"
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}


@end
