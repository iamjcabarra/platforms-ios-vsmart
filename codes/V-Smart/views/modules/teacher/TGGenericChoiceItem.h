//
//  TGGenericChoiceItem.h
//  V-Smart
//
//  Created by Ryan Migallos on 07/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TGGenericChoiceAction) {
    TGGenericChoiceCheck,
    TGGenericChoiceUpload,
    TGGenericChoiceDelete,
    TGgenericChoiceDefault
};

@protocol TGGenericChoiceItemDelegate <NSObject>
@optional
- (void)updatedText:(NSString *)text withManagedObject:(NSManagedObject *)object;
- (void)didPressChoiceAction:(TGGenericChoiceAction)action withManagedObject:(NSManagedObject *)object;
- (void)didPerformChoiceImageAction:(TGGenericChoiceAction)action withImageData:(NSData *)imageData withStringURL:(NSString *)stringUrl withManagedObject:(NSManagedObject *)object;
@end

@interface TGGenericChoiceItem : UITableViewCell

@property (weak,   nonatomic) id <TGGenericChoiceItemDelegate> delegate;
@property (strong, nonatomic) IBOutlet UITextField *customTextField;
@property (strong, nonatomic) IBOutlet UIImageView *checkImageView;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong, nonatomic) IBOutlet UIImageView *uploadImageView;
@property (strong, nonatomic) NSManagedObject *mo;

@property (weak, nonatomic) IBOutlet UIButton *removeChoiceImageButton;

@property (strong, nonatomic) IBOutlet UIButton *uploadButton;

@end
