//
//  TBNewTestItemCell.h
//  V-Smart
//
//  Created by Julius Abarra on 01/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"

@interface TBNewTestItemCell : MGSwipeTableCell

@property (strong, nonatomic) IBOutlet UIImageView *testStageTypeImage;
@property (strong, nonatomic) IBOutlet UIImageView *lockImage;
@property (strong, nonatomic) IBOutlet UILabel *testTitle;
@property (strong, nonatomic) IBOutlet UILabel *testStatus;
@property (strong, nonatomic) IBOutlet UILabel *testStartDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *testEndDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *pointsValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *itemsValueLabel;


- (void)updatePointsLabel:(NSString *)points;
- (void)updateItemsLabel:(NSString *)items;

@end
