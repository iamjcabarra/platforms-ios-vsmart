//
//  QuestionHeader.h
//  V-Smart
//
//  Created by Ryan Migallos on 8/10/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionHeader : UIView

- (void)setValuesWithManagedObject:(NSManagedObject *)object;
- (NSMutableDictionary *)getValues;

@end
