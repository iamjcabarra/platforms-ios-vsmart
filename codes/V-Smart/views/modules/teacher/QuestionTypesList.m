//
//  QuestionTypesList.m
//  V-Smart
//
//  Created by Ryan Migallos on 28/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "QuestionTypesList.h"
#import "QuestionTypeListItemCell.h"
#import "TestGuruDataManager.h"
#import "MainHeader.h"
#import <QuartzCore/QuartzCore.h>

//// BUTTON CATEGORY ////
@interface UIButton (CUSTOM)
- (void)setColor:(UIColor *)color forState:(UIControlState)state;
@end

@implementation UIButton (CUSTOM)
- (void)setColor:(UIColor *)color forState:(UIControlState)state {
    
    UIView *colorView = [[UIView alloc] initWithFrame:self.frame];
    colorView.backgroundColor = color;
    UIGraphicsBeginImageContext(colorView.bounds.size);
    [colorView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:state];
}
@end
//// BUTTON CATEGORY ////


@interface QuestionTypesList () <NSFetchedResultsControllerDelegate, UITextFieldDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) TestGuruDataManager *tm;

@property (strong, nonatomic) IBOutlet UILabel *labelHeader;
@property (strong, nonatomic) IBOutlet UILabel *labelFooter;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITextField *itemField;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UIButton *createButton;
@property (strong, nonatomic) IBOutlet UIButton *oneButton;
@property (strong, nonatomic) IBOutlet UIButton *twoButton;
@property (strong, nonatomic) IBOutlet UIButton *fiveButton;
@property (strong, nonatomic) IBOutlet UIButton *tenButton;
@property (strong, nonatomic) IBOutlet UIButton *fifteenButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerViewHeight;

@property (strong, nonatomic) NSMutableDictionary *group_button;
@property (strong, nonatomic) NSString *numberOfItems;
@property (strong, nonatomic) dispatch_queue_t queue;
@property (strong, nonatomic) NSMutableDictionary *activeDownloads;

@end

@implementation QuestionTypesList

static NSString *kQuestionTypeIdentifier = @"custom_question_type_cell_identifier";

- (void)applyShadowTheme:(UIView *)view radius:(CGFloat)radius {
    
    CALayer *buttonLayer = view.layer;
    buttonLayer.masksToBounds = NO;
    buttonLayer.shadowColor = [UIColor lightGrayColor].CGColor;
    buttonLayer.shadowOffset = CGSizeMake(2.0,2.0);
    buttonLayer.shadowRadius = radius;
    buttonLayer.shadowOpacity = 0.4f;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.queue = dispatch_queue_create("com.vibetech.testguru.QUESTIONTYPEIMAGE",DISPATCH_QUEUE_CONCURRENT);
    
    NSString *choose_template_string = NSLocalizedString(@"Choose Question Template", nil);
    NSString *number_of_items_string = NSLocalizedString(@"Number of Items to generate", nil);
    
    self.labelHeader.text = [choose_template_string uppercaseString];
    self.labelFooter.text = [number_of_items_string uppercaseString];
    
    self.tableView.estimatedRowHeight = 110.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.tm = [TestGuruDataManager sharedInstance];
    
    [self.tm requestQuestionType:nil];
    
    [self.tm requestType:kEndPointTestGuruProficiencyLevel doneBlock:nil];
    [self.tm requestType:kEndPointTestGuruLearningSkill doneBlock:nil];
    
    [self.createButton addTarget:self
                          action:@selector(createButtonAction:)
                forControlEvents:UIControlEventTouchUpInside];
    
    [self applyShadowTheme:self.createButton radius:5.0];
    [self applyShadowTheme:self.labelHeader radius:5.0];
    [self applyShadowTheme:self.labelHeader radius:5.0];
    [self applyShadowTheme:self.itemField radius:5.0];
    
    [self setupGroupButtons];
}

- (void)setupGroupButtons {
    if (self.group_button == nil) {
        self.group_button = [NSMutableDictionary dictionary];
    }
    
    SEL action = @selector(buttonNumberAction:);
    [self customizeButton:self.oneButton action:action tag:621 value:1 lookup:self.group_button];
    [self customizeButton:self.twoButton action:action tag:622 value:2 lookup:self.group_button];
    [self customizeButton:self.fiveButton action:action tag:623 value:5 lookup:self.group_button];
    [self customizeButton:self.tenButton action:action tag:624 value:10 lookup:self.group_button];
    [self customizeButton:self.fifteenButton action:action tag:625 value:15 lookup:self.group_button];
    
    self.oneButton.selected = YES;
    self.numberOfItems = @"1";
}

- (void)customizeButton:(UIButton *)button action:(SEL)action tag:(NSInteger)tag value:(NSInteger)value lookup:(NSMutableDictionary *)lookup {
    UIColor *selectionColor = UIColorFromHex(0x007AFF);
    [button addTarget:self action:@selector(buttonNumberAction:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = tag;
    [button setColor:selectionColor forState:UIControlStateSelected];
    [button setClipsToBounds:YES];
    
    [self applyShadowTheme:button radius:5.0];
    
    [lookup setObject:@(value) forKey:@(tag)];
}

- (void)buttonNumberAction:(UIButton *)button {
    NSArray *list = [self.group_button allKeys];
    
    for (NSNumber *item in list) {
        NSInteger tag = [item integerValue];
        UIButton *button_item = (UIButton *)[self.view viewWithTag:tag];
        if (button_item != button) {
            button_item.selected = NO;
        }
    }
    
    button.selected = YES;
    NSNumber *number = (NSNumber *)[self.group_button objectForKey:@(button.tag)];
    self.numberOfItems = [NSString stringWithFormat:@"%@", number];
    self.itemField.text = @"";
    [self.itemField resignFirstResponder];
}

- (void)createButtonAction:(id)sender {
    NSArray *indexes = [self.tableView indexPathsForSelectedRows];
    BOOL userSelection = (indexes.count > 0) ? YES : NO;

    NSString *title_string = @"Invalid Input";
    
    if (userSelection == NO) {
        NSString *message = NSLocalizedString(@"Kindly select a template to create a question", nil);
        AlertWithMessageAndDelegate(title_string, message, self);
        return;
    }
    
    NSString *item_count = [NSString stringWithFormat:@"%@", self.numberOfItems];
    BOOL userInput = (item_count.length > 0) ? YES : NO;
    
    NSInteger count_value = [item_count integerValue];
    if (count_value < 1) {
        userInput = NO;
    }
    
    if (userInput == NO) {
        NSString *message = NSLocalizedString(@"Please provide number of items", nil);
        AlertWithMessageAndDelegate(title_string, message, self);
        return;
    }
    
    if (count_value > 50) {
        NSString *message = NSLocalizedString(@"Maximum of 50 questions", nil);
        AlertWithMessageAndDelegate(title_string, message, self);
        return;
    }
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    __weak typeof(self) wo = self;
    NSString *question_type_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"id"] ];
    NSString *question_type_value = [NSString stringWithFormat:@"%@", [mo valueForKey:@"value"] ];
    
    NSDictionary *questionTypeData = @{@"id": question_type_id, @"value": question_type_value};
    
    [wo.tm insertQuestionWithType:questionTypeData itemCount:item_count dataBlock:^(NSDictionary *data) {
        if (data != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([(NSObject*)wo.delegate respondsToSelector:@selector(createQuestionTypeObject:)] ) {
                    NSLog(@"%s -----", __PRETTY_FUNCTION__);
                    [wo.delegate createQuestionTypeObject:data];
                    [wo dismissViewControllerAnimated:YES completion:nil];
                }
            });
        }
        
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)unwindFromConfirmationForm:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    
    NSString *questionTitle =  @"Question";
    NSString *countTitle = [NSString stringWithFormat:@"%@ (%ld)", questionTitle, (long)count];
    
    self.title = countTitle;
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kQuestionTypeIdentifier forIndexPath:indexPath];
    
    UIView *v = [[UIView alloc] init];
    v.backgroundColor = UIColorFromHex(0xDFEAF2);
    v.frame = cell.frame;
    cell.selectedBackgroundView = v;
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    QuestionTypeListItemCell *questionCell = (QuestionTypeListItemCell *)cell;
    [self configureQuestionTypeCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionTypeCell:(QuestionTypeListItemCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    NSString *question_type_title = [mo valueForKey:@"value"];
    NSString *question_type_description = [mo valueForKey:@"desc"];
    
    cell.titleLabel.text = question_type_title;
    cell.descriptionLabel.text = question_type_description;
    
    cell.questionImageView.image = nil;
    NSData *image_data = [mo valueForKey:@"image_data"];
    
    if (image_data == nil) {
        NSString *image_url = [mo valueForKey:@"image_url"];
        if ( (image_url != nil) && (image_url.length > 0) ) {
            [self downloadImageForObject:mo indexPath:indexPath];
        }
    }
    
    if (image_data != nil) {
        cell.questionImageView.image = [UIImage imageWithData:image_data];
    }
}

- (void)downloadImageForObject:(NSManagedObject *)mo indexPath:(NSIndexPath *)indexPath {
    if (self.activeDownloads == nil) {
        self.activeDownloads = [NSMutableDictionary dictionary];
    }
    
    NSManagedObject *mo_in_progress = [self.activeDownloads objectForKey:indexPath];
    
    if (mo_in_progress == nil) {
        [self.activeDownloads setObject:mo forKey:indexPath];
        __weak typeof(self) wo = self;
        dispatch_async(self.queue, ^{
            NSString *entityName = mo.entity.name;
            [wo.tm downloadImageForEntity:entityName withObject:mo doneBlock:^(BOOL status) {
                if (status) {
                    [wo.activeDownloads removeObjectForKey:indexPath];
                }
            }];
        });
    }
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController {
    BOOL isAscending = YES;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionTypeEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sort_id = [NSSortDescriptor sortDescriptorWithKey:@"value" ascending:isAscending];
    [fetchRequest setSortDescriptors:@[sort_id]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    NSArray *list = [self.group_button allKeys];
    
    for (NSNumber *item in list) {
        NSInteger tag = [item integerValue];
        UIButton *button_item = (UIButton *)[self.view viewWithTag:tag];
        button_item.selected = NO;
    }
    
    self.numberOfItems =  [NSString stringWithFormat:@"%@", self.itemField.text];
    
    __weak typeof(self) wo = self;
    wo.footerViewHeight.constant = 200;
    [UIView animateWithDuration:0.3f animations:^{
        [wo.view layoutIfNeeded];
    }];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    __weak typeof(self) wo = self;
    wo.footerViewHeight.constant = 100;
    [UIView animateWithDuration:0.3f animations:^{
        [wo.view layoutIfNeeded];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.itemField) {
        // allow backspace
        if (!string.length) {
            return YES;
        }
        
        // Prevent invalid character input, if keyboard is numberpad
        if (textField.keyboardType == UIKeyboardTypeNumberPad) {
            if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound) {
                // BasicAlert(@"", @"This field accepts only numeric entries.");
                return NO;
            }
        }
        
        // verify max length has not been exceeded
        NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if (updatedText.length > 2) // 4 was chosen for SSN verification
        {
            // suppress the max length message only when the user is typing
            // easy: pasted data has a length greater than 1; who copy/pastes one character?
            if (string.length > 1)
            {
                // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
            }
            
            return NO;
        }
        
        NSNumber *number = [NSNumber numberWithInteger: [updatedText integerValue] ];
        if ([number integerValue] < 0) {
            return NO;
        }
        
        self.numberOfItems = [NSString stringWithFormat:@"%@", number];
    }
    
    return YES;
}

@end
