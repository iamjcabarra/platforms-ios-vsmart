//
//  TGShareCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 09/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGShareCell : UITableViewCell

@property (readonly, nonatomic) BOOL is_shared;
- (void)displayStatus:(BOOL)flag;
- (void)displayTags:(NSSet *)tagSet;
- (NSString *)getTagValues;
- (NSSet *)getTagSet;
@end
