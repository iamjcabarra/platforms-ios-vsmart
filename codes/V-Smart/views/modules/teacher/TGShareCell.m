//
//  TGShareCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 09/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TGShareCell.h"
#import "TGTokenTextCollectionCell.h"
#import "TestGuruDataManager.h"
#import "MainHeader.h"

@interface TGShareCell() <NSFetchedResultsControllerDelegate, UITextFieldDelegate> {
    NSMutableDictionary *_objectChanges;
    NSMutableDictionary *_sectionChanges;
    NSString *edited_tag;
}

@property (strong, nonatomic) IBOutlet UILabel *typeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *checkImageView;
@property (strong, nonatomic) IBOutlet UIView *buttonContainer;
@property (strong, nonatomic) IBOutlet UILabel *buttonTitle;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) IBOutlet UITextField *tagsTextField;
@property (strong, nonatomic) IBOutlet UIView *fieldContainer;

@property (assign, nonatomic) BOOL is_shared;


@property (strong, nonatomic) IBOutlet UICollectionView *collection;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) TestGuruDataManager *tm;

@property (assign, nonatomic) BOOL editCollection;

@end

@implementation TGShareCell

static NSString *kTextTokenCellIdentifier = @"tg_text_token_identifier";

- (void)awakeFromNib {
    // Initialization code
    self.editCollection = NO;//DEFAULT VALUE;
    
    self.tm = [TestGuruDataManager sharedInstance];
    [self.tm clearContentsForEntity:kTagValuesEntity predicate:nil];
        
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collection.collectionViewLayout;
    layout.estimatedItemSize = CGSizeMake(50, 28);
    
    self.buttonTitle.text = NSLocalizedString(@"Shared to others", nil);
    self.typeLabel.text = NSLocalizedString(@"Tags", nil);
    self.tagsTextField.placeholder = NSLocalizedString(@"Type your tags here..", nil);
    
    [self.shareButton addTarget:self
                         action:@selector(shareButtonAction:)
               forControlEvents:UIControlEventTouchUpInside];
    
    [self applyShadowToView:self.fieldContainer];
    [self applyShadowToView:self.buttonContainer];
}

- (void)applyShadowToView:(UIView *)object {
    
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.4f;
}

- (void)shareButtonAction:(UIButton *)sender {

    BOOL flag = (self.is_shared) ? NO : YES;
    [self displayStatus:flag];
}

- (void)displayStatus:(BOOL)flag {
    
    self.is_shared = flag;
    
    //DEFAULT VALUES
    UIColor *containerBackgroundColor = UIColorFromHex(0xE6E6E6);
    UIColor *textColor = [UIColor darkGrayColor];
    NSString *defaultImage = @"checkbox_unable150x150";
    
    //SELECTED STATE
    if (flag) {
        containerBackgroundColor = UIColorFromHex(0x0080FF);
        textColor = [UIColor whiteColor];
        defaultImage = @"checkbox150x150";
    }
    
    UIImage *image = [UIImage imageNamed:defaultImage];
    self.checkImageView.image = image;
    self.buttonContainer.backgroundColor = containerBackgroundColor;
    self.buttonTitle.textColor = textColor;
}

- (void)displayTags:(NSSet *)tagSet {

    if (tagSet != nil) {
        BOOL flag = [self.tm clearContentsForEntity:kTagValuesEntity predicate:nil];
        
        if (flag) {
            NSArray *tag_list = [tagSet allObjects];
            if (tag_list.count > 0) {
                
                for (NSManagedObject *tag in tag_list) {
                    NSString *tagValue = [NSString stringWithFormat:@"%@", [tag valueForKey:@"tag"]];
                    [self insertTagValue:tagValue];
                }
            }
        }
    }
}

- (NSString *)getTagValues {
    
    NSArray *items = [self.fetchedResultsController fetchedObjects];
    
    NSMutableString *text = [NSMutableString string];
    for (NSInteger i = 0; i < [items count]; ++i ) {
         
        NSManagedObject *mo = (NSManagedObject *)items[i];
        NSString *tag_string = [NSString stringWithFormat:@"%@", [mo valueForKey:@"tag"] ];
        [text appendString:tag_string];
        
        if (i < ([items count]-1) ) {
            [text appendString:@","];
        }
    }
    
    return [NSString stringWithString:text];
}

- (NSSet *)getTagSet {
    
    NSArray *items = [self.fetchedResultsController fetchedObjects];
    return [self.tm processTags:items];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    self.editCollection = YES;
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    edited_tag = [NSString stringWithFormat:@"%@", [mo valueForKey:@"tag"] ];
    self.tagsTextField.text = edited_tag;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TGTokenTextCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kTextTokenCellIdentifier
                                                                                forIndexPath:indexPath];
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *token_text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"tag"] ];
    cell.tokenText.text = token_text;
    [cell.tokenDeleteButton addTarget:self action:@selector(deleteTagAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (NSManagedObject *)managedObjectFromButtonAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.collection];
    NSIndexPath *indexPath = [self.collection indexPathForItemAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (void)deleteTagAction:(id)sender {

    NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
    NSString *token_text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"tag"] ];
    [self.tm removeTagValue:token_text];
}

#pragma mark <NSFetchedResultsControllerDelegate>

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Object Context
    NSManagedObjectContext *context = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTagValuesEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    NSSortDescriptor *value = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
    [fetchRequest setSortDescriptors:@[value]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:context
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    _objectChanges = [NSMutableDictionary dictionary];
    _sectionChanges = [NSMutableDictionary dictionary];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    if (type == NSFetchedResultsChangeInsert || type == NSFetchedResultsChangeDelete) {
        NSMutableIndexSet *changeSet = _sectionChanges[@(type)];
        if (changeSet != nil) {
            [changeSet addIndex:sectionIndex];
        } else {
            _sectionChanges[@(type)] = [[NSMutableIndexSet alloc] initWithIndex:sectionIndex];
        }
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    NSMutableArray *changeSet = _objectChanges[@(type)];
    if (changeSet == nil) {
        changeSet = [[NSMutableArray alloc] init];
        _objectChanges[@(type)] = changeSet;
    }
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [changeSet addObject:newIndexPath];
            break;
        case NSFetchedResultsChangeDelete:
            [changeSet addObject:indexPath];
            break;
        case NSFetchedResultsChangeUpdate:
            [changeSet addObject:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [changeSet addObject:@[indexPath, newIndexPath]];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    NSMutableArray *moves = _objectChanges[@(NSFetchedResultsChangeMove)];
    if (moves.count > 0) {
        NSMutableArray *updatedMoves = [[NSMutableArray alloc] initWithCapacity:moves.count];
        
        NSMutableIndexSet *insertSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        NSMutableIndexSet *deleteSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        for (NSArray *move in moves) {
            NSIndexPath *fromIP = move[0];
            NSIndexPath *toIP = move[1];
            
            if ([deleteSections containsIndex:fromIP.section]) {
                if (![insertSections containsIndex:toIP.section]) {
                    NSMutableArray *changeSet = _objectChanges[@(NSFetchedResultsChangeInsert)];
                    if (changeSet == nil) {
                        changeSet = [[NSMutableArray alloc] initWithObjects:toIP, nil];
                        _objectChanges[@(NSFetchedResultsChangeInsert)] = changeSet;
                    } else {
                        [changeSet addObject:toIP];
                    }
                }
            } else if ([insertSections containsIndex:toIP.section]) {
                NSMutableArray *changeSet = _objectChanges[@(NSFetchedResultsChangeDelete)];
                if (changeSet == nil) {
                    changeSet = [[NSMutableArray alloc] initWithObjects:fromIP, nil];
                    _objectChanges[@(NSFetchedResultsChangeDelete)] = changeSet;
                } else {
                    [changeSet addObject:fromIP];
                }
            } else {
                [updatedMoves addObject:move];
            }
        }
        
        if (updatedMoves.count > 0) {
            _objectChanges[@(NSFetchedResultsChangeMove)] = updatedMoves;
        } else {
            [_objectChanges removeObjectForKey:@(NSFetchedResultsChangeMove)];
        }
    }
    
    NSMutableArray *deletes = _objectChanges[@(NSFetchedResultsChangeDelete)];
    if (deletes.count > 0) {
        NSMutableIndexSet *deletedSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        [deletes filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSIndexPath *evaluatedObject, NSDictionary *bindings) {
            return ![deletedSections containsIndex:evaluatedObject.section];
        }]];
    }
    
    NSMutableArray *inserts = _objectChanges[@(NSFetchedResultsChangeInsert)];
    if (inserts.count > 0) {
        NSMutableIndexSet *insertedSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        [inserts filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSIndexPath *evaluatedObject, NSDictionary *bindings) {
            return ![insertedSections containsIndex:evaluatedObject.section];
        }]];
    }
    
    UICollectionView *cv = self.collection;
    
    [cv performBatchUpdates:^{
        NSIndexSet *deletedSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        if (deletedSections.count > 0) {
            [cv deleteSections:deletedSections];
        }
        
        NSIndexSet *insertedSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        if (insertedSections.count > 0) {
            [cv insertSections:insertedSections];
        }
        
        NSArray *deletedItems = _objectChanges[@(NSFetchedResultsChangeDelete)];
        if (deletedItems.count > 0) {
            [cv deleteItemsAtIndexPaths:deletedItems];
        }
        
        NSArray *insertedItems = _objectChanges[@(NSFetchedResultsChangeInsert)];
        if (insertedItems.count > 0) {
            [cv insertItemsAtIndexPaths:insertedItems];
        }
        
        NSArray *reloadItems = _objectChanges[@(NSFetchedResultsChangeUpdate)];
        if (reloadItems.count > 0) {
            [cv reloadItemsAtIndexPaths:reloadItems];
        }
        
        NSArray *moveItems = _objectChanges[@(NSFetchedResultsChangeMove)];
        for (NSArray *paths in moveItems) {
            [cv moveItemAtIndexPath:paths[0] toIndexPath:paths[1]];
        }
    } completion:nil];
    
    _objectChanges = nil;
    _sectionChanges = nil;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    if (textField.returnKeyType == UIReturnKeyDone) {
        NSString *updatedText = [NSString stringWithFormat:@"%@", textField.text];
        
        if ( self.editCollection == NO ) {
            [self insertTagValue:updatedText];
        }
        
        if ( self.editCollection == YES ) {
            [self updateTagValue:updatedText];
        }
        
        self.tagsTextField.text = @"";
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
      
    NSString *updatedText = [NSString stringWithFormat:@"%@", textField.text];
    
    if ( self.editCollection == NO ) {
        [self insertTagValue:updatedText];
    }
    
    if ( self.editCollection == YES ) {
        [self updateTagValue:updatedText];
    }
    
    self.tagsTextField.text = @"";
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // verify max length has not been exceeded
    NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString *originalText = [NSString stringWithFormat:@"%@", textField.text];
    
    // allow backspace
    if (!string.length) {
        return YES;
    }
    
    NSString *expression = @",";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:updatedText
                                                        options:0
                                                          range:NSMakeRange(0, [updatedText length])];
    if (numberOfMatches == 1) {
        
        if ( self.editCollection == NO ) {
            [self insertTagValue:originalText];
        }
        
        if ( self.editCollection == YES ) {
            [self updateTagValue:originalText];
        }
        
        self.tagsTextField.text = @"";
        
        return NO;
    }
    
    if (updatedText.length > 50) { // 100 character limit for each tag
        if (string.length > 1) {
            NSLog(@"tags has a maximum limit of 100 characters");
        }
        return NO;
    }
    
    return YES;
}

- (void)insertTagValue:(NSString *)value {
    
    [self.tm insertNewTagValue:value];
}

- (void)updateTagValue:(NSString *)value {
    
    __weak typeof(self) wo = self;
    
    // EDITING SELECTED TAG
    if ( self.editCollection == YES ) {
        [self.tm updateTagValue:edited_tag withNewValue:value doneBlock:^(BOOL status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                wo.editCollection = NO;
                [wo.collection reloadData];
            });
        }];

    }
}

@end
