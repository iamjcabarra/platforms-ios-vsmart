//
//  TestBankSortHeaderCell.m
//  V-Smart
//
//  Created by Julius Abarra on 11/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestBankSortHeaderCell.h"
#import "MainHeader.h"

@interface TestBankSortHeaderCell ()
@property (strong, nonatomic) IBOutlet UIView *backgroundObject;
@end

@implementation TestBankSortHeaderCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)showState:(BOOL)selected {
    // Background Color
    UIColor *active = UIColorFromHex(0x007AFF);
    UIColor *inActive = UIColorFromHex(0xDFEAF2);
    self.backgroundObject.backgroundColor = (selected) ? active : inActive;
    
    // Text Color
    UIColor *activeText = [UIColor whiteColor];
    UIColor *inActiveText = UIColorFromHex(0x007AFF);
    self.sortByLabel.textColor = (selected) ? activeText : inActiveText;
}

@end
