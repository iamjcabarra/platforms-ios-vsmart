//
//  TGTBDatePickerView.h
//  V-Smart
//
//  Created by Julius Abarra on 14/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

@protocol TGTBDatePickerViewDelegate <NSObject>

@required
- (void)selectedDate:(NSString *)dateString;

@end

#import <UIKit/UIKit.h>

@interface TGTBDatePickerView : UIViewController

@property (assign, nonatomic) NSInteger dateType;
@property (assign, nonatomic) NSString *labelString;
@property (weak, nonatomic) id <TGTBDatePickerViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

@end
