//
//  TGTMTestCommentCreateViewController.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 29/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class TGTMTestCommentCreateViewController: UIViewController {

    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var commentLabel: UILabel!
    
    fileprivate lazy var tgmDataManager: TestGuruDataManager = {
        let tgdm = TestGuruDataManager.sharedInstance()
        return tgdm!
    }()
    
    var test_id: String!
    var comment_id: String?
    var comment: String?
    
    var delegate:TGTMTestCommentViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate?.dimIn()

        self.commentTextView.placeholder = NSLocalizedString("Enter your comment here...", comment: "")
        self.commentLabel.text = (comment_id == nil) ? "New comment" : "Edit comment"
        self.commentTextView.text = (comment == nil) ? "" : comment!
        self.sendButton.addTarget(self, action: #selector(self.sendButtonAction(_:)), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonAction(_ sender: AnyObject) {
        self.delegate?.dimOut()
        self.dismiss(animated: true, completion: nil)
    }
    
    func sendButtonAction(_ b: UIButton) {
        
        if comment_id != nil {
            self.tgmDataManager.editComment(self.commentTextView.text, forCommentID: comment_id, dataBlock: { (dict) in
                if dict == nil {
                    DispatchQueue.main.async(execute: {
                        self.delegate?.dimOut()
                    })
                    self.dismiss(animated: true, completion: nil)
                } else {
                    let errorMessage: String! = dict!["error"] as! String
                    self.displayAlert(withTitle: "", withMessage: errorMessage)
                    // display error
                }
            })
        } else {
        self.tgmDataManager.postComment(self.commentTextView.text, forTestID: test_id) { (dict) in
            // do nothing
            if dict == nil {
                DispatchQueue.main.async(execute: {
                    self.delegate?.dimOut()
                    self.delegate?.reloadList()
                })
                self.dismiss(animated: true, completion: nil)
            } else {
                let errorMessage: String! = dict!["error"] as! String
                self.displayAlert(withTitle: "", withMessage: errorMessage)
                // display error
            }
        }
    }
    }

    func displayAlert(withTitle title: String, withMessage message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
