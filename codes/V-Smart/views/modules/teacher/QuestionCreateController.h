//
//  QuestionCreateController.h
//  V-Smart
//
//  Created by Ryan Migallos on 8/7/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionCreateController : UITableViewController

@property (strong, nonatomic) NSString *user_id;

@end
