//
//  TestAssignQuestionsView.h
//  V-Smart
//
//  Created by Julius Abarra on 28/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

@protocol TestAssignQuestionsViewDelegate <NSObject>

@required
- (void)selectedQuestions:(NSArray *)questions;

@end


#import <UIKit/UIKit.h>

@interface TestAssignQuestionsView : UIViewController

@property (weak, nonatomic) id <TestAssignQuestionsViewDelegate> delegate;
@property (strong, nonatomic) NSArray *assignedQuestions;

@end
