//
//  UIView+FormScroll.h
//  V-Smart
//
//  Created by Julius Abarra on 25/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FormScroll)

- (void)scrollToPointY:(float)y;
- (void)scrollToView:(UIView *)view;
- (void)scrollElement:(UIView *)view toPointY:(float)y;

@end
