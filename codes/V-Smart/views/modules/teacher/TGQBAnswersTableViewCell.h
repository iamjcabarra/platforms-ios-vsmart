//
//  TGQBAnswersTableViewCell.h
//  V-Smart
//
//  Created by Julius Abarra on 04/10/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGQBAnswersTableViewCell : UITableViewCell

- (void)setObjectData:(NSManagedObject *)object;
- (void)displayAnswers:(NSSet *)answerSet;

@end
