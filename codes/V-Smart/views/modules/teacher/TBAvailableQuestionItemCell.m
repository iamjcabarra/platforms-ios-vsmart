//
//  TBAvailableQuestionItemCell.m
//  V-Smart
//
//  Created by Julius Abarra on 03/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TBAvailableQuestionItemCell.h"

@implementation TBAvailableQuestionItemCell

- (void)awakeFromNib {
    UIColor *borderColor = [UIColor lightGrayColor];
    CGFloat borderWidth = 1.0f;
    
    self.selectionView.layer.borderColor = borderColor.CGColor;
    self.selectionView.layer.borderWidth = borderWidth;
    self.titleView.layer.borderColor = borderColor.CGColor;
    self.titleView.layer.borderWidth = borderWidth;
    self.questionView.layer.borderColor = borderColor.CGColor;
    self.questionView.layer.borderWidth = borderWidth;
    self.pointView.layer.borderColor = borderColor.CGColor;
    self.pointView.layer.borderWidth = borderWidth;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)prepareForReuse {
    [self showSelection:NO];
    [super prepareForReuse];
}

- (void)showSelection:(BOOL)selection {
    UIImage *selected = [UIImage imageNamed:@"check_icn_active48.png"];
    UIImage *unselected = [UIImage imageNamed:@"check_icn48.png"];
    
    self.selectionImage.image = selection ? selected : unselected;
}

@end
