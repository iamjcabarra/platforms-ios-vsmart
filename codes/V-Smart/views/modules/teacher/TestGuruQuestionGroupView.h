//
//  TestGuruQuestionGroupView.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 21/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TestGuruQuestionGroupView : UIViewController
@property (strong, nonatomic) NSDictionary *pagingInformation;

@property (strong, nonatomic) NSString *groupByType;
@end
