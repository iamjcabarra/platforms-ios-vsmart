//
//  TGTBCreateTestSegmentView.m
//  V-Smart
//
//  Created by Julius Abarra on 07/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGTBCreateTestSegmentView.h"
#import "TGTBTestMainInfoView.h"
#import "TGTBTestSettingsView.h"
#import "TGTBTestQuestionView.h"

#define kSegueIdentifierTestMainInfo @"embedTestMainInfo"
#define kSegueIdentifierTestSettings @"embedTestSettings"
#define kSegueIdentifierTestQuestion @"embedTestQuestion"

@interface TGTBCreateTestSegmentView ()

@property (strong, nonatomic) TGTBTestMainInfoView *testMainInfoView;
@property (strong, nonatomic) TGTBTestSettingsView *testSettingsView;
@property (strong, nonatomic) TGTBTestQuestionView *testQuestionView;

@property (strong, nonatomic) NSString *currentSegueIdentifier;

@end

@implementation TGTBCreateTestSegmentView

- (void)viewDidLoad {
    [super viewDidLoad];

    self.currentSegueIdentifier = kSegueIdentifierTestMainInfo;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kSegueIdentifierTestMainInfo]) {
        self.testMainInfoView = (TGTBTestMainInfoView *)[segue destinationViewController];
        
        if (self.testObject != nil) {
            self.testMainInfoView.testObject = self.testObject;
        }
        
        if (self.childViewControllers.count > 0) {
            [self swapFromView:[self.childViewControllers objectAtIndex:0] toView:segue.destinationViewController];
        }
        else {
            [self addChildViewController:segue.destinationViewController];
            ((UIViewController *)segue.destinationViewController).view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:((UIViewController *)segue.destinationViewController).view];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:kSegueIdentifierTestSettings]) {
        self.testSettingsView = (TGTBTestSettingsView *)[segue destinationViewController];
        
        if (self.testObject != nil) {
            self.testSettingsView.testObject = self.testObject;
        }
        
        [self swapFromView:[self.childViewControllers objectAtIndex:0] toView:segue.destinationViewController];
    }
    else {
        self.testQuestionView = (TGTBTestQuestionView *)[segue destinationViewController];
        
        if (self.testObject != nil) {
            self.testQuestionView.testObject = self.testObject;
            self.testQuestionView.crudActionType = self.crudActionType;
        }
        
        [self swapFromView:[self.childViewControllers objectAtIndex:0] toView:segue.destinationViewController];
    }
}

- (void)swapFromView:(UIViewController *)fromView toView:(UIViewController *)toView {
    toView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    UIViewAnimationOptions transition = UIViewAnimationOptionTransitionNone;
    
    if ([toView isEqual:self.testMainInfoView]) {
        transition = UIViewAnimationOptionTransitionCrossDissolve;
    }
    
    if ([toView isEqual:self.testSettingsView]) {
        transition = UIViewAnimationOptionTransitionCrossDissolve;
    }
    
    if ([toView isEqual:self.testQuestionView]) {
        transition = UIViewAnimationOptionTransitionCrossDissolve;
    }
    
    [fromView willMoveToParentViewController:nil];
    [self addChildViewController:toView];
    [self transitionFromViewController:fromView
                      toViewController:toView
                              duration:0.0
                               options:transition
                            animations:nil
                            completion:^(BOOL finished) {
                                [fromView removeFromParentViewController];
                                [toView didMoveToParentViewController:self];
                            }];
}

- (void)swapEmbeddedViews:(NSString *)segueIdentifier {
    [self performSegueWithIdentifier:segueIdentifier sender:nil];
}

@end
