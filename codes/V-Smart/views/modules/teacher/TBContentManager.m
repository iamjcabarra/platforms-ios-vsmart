//
//  TBContentManager.m
//  TestBankVersion2.1
//
//  Created by Julius Abarra on 08/02/2016.
//  Copyright © 2016 Vibe Technologies, Inc. All rights reserved.
//

#import "TBContentManager.h"

@implementation TBContentManager

#pragma mark - Dedicated Methods

+ (id)sharedInstance {
    static TBContentManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        self.testDetails = [NSMutableDictionary dictionary];
        self.testOptions = [NSMutableDictionary dictionary];
        self.testAssignedQuestions = [NSArray array];
        self.actionType = [[NSNumber alloc] init];
    }
    
    return self;
}

- (void)destroyContents {
    self.testDetails = nil;
    self.testOptions = nil;
    self.testAssignedQuestions = nil;
    self.actionType = nil;
}

- (void)recreateObjects {
    self.testDetails = [NSMutableDictionary dictionary];
    self.testOptions = [NSMutableDictionary dictionary];
    self.testAssignedQuestions = [NSArray array];
    self.actionType = [[NSNumber alloc] init];
}

#pragma mark - Recipient Helpers

- (int)updateTestAttempt:(int)currentTestAttempt lastAcceptableTestAttempt:(int)lastTestAttempt {
    BOOL isAcceptable = (currentTestAttempt >= [kTestMinAttempts intValue]) && (currentTestAttempt <= [kTestMaxAttempts intValue]);
    
    if (isAcceptable) {
        return currentTestAttempt;
    }
    
    return lastTestAttempt;
}

- (int)updateTestScore:(int)score lastAcceptableTestScore:(int)lastTestScore totalQuestionPoints:(int)points {
    BOOL isAcceptable = (score > 0 && score <= points);
    
    if (isAcceptable) {
        return score;
    }
    
    return lastTestScore;
}

@end
