//
//  TestGuruEmptyPlaceholder.h
//  V-Smart
//
//  Created by Ryan Migallos on 23/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TestGuruEmptyPlaceholderDelegate <NSObject>
@required
- (void)didFinishSelectingOperationObject:(NSDictionary *)dictionary;
@end


@interface TestGuruEmptyPlaceholder : UIView
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UIButton *createButton;
@property (weak,   nonatomic) id <TestGuruEmptyPlaceholderDelegate> delegate;
- (void)initializeImagePlaceHolderWithType:(NSString *)objectType;
@end
