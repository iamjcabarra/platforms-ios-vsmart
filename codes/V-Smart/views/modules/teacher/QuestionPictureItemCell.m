//
//  QuestionPictureItemCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 03/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "QuestionPictureItemCell.h"

@implementation QuestionPictureItemCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
