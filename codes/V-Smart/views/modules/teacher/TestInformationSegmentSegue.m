//
//  TestInformationSegmentSegue.m
//  V-Smart
//
//  Created by Julius Abarra on 14/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestInformationSegmentSegue.h"

@implementation TestInformationSegmentSegue

- (void)perform {
    // It does nothing.
    // The TestInformationSegmentController class handles all of the viewcontroller action.
}

@end
