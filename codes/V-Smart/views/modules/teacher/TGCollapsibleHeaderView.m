//
//  TGCollapsibleHeaderView.m
//  V-Smart
//
//  Created by Ryan Migallos on 08/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TGCollapsibleHeaderView.h"
#import "TGDropDownCell.h"
#import "TGFeedBackCell.h"
#import "TGShareCell.h"
#import "TestGuruDataManager.h"

@interface TGCollapsibleHeaderView ()

@property (strong, nonatomic) IBOutlet TGDropDownCell *dropDownField;
@property (strong, nonatomic) IBOutlet TGFeedBackCell *feedbackField;
@property (strong, nonatomic) IBOutlet TGShareCell *shareField;

@end

@implementation TGCollapsibleHeaderView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if (self.mo) {
        
        NSLog(@"--------------------------------------------------------------------------------------------");
        
        NSLog(@"PACKAGE TYPE ID : %@", self.package_type_id);
        
        //DIFFICULTY LEVEL
        NSString *proficiency_level_id = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"proficiency_level_id"]];
        NSString *proficiencyLevelName = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"proficiencyLevelName"]];
        NSDictionary *proficiencyData = @{@"id":proficiency_level_id,@"value":proficiencyLevelName};
        [self.dropDownField setDifficultyValueForEntity:kProficiencyEntity withData:proficiencyData];
        
        //LEARNING SKILL
        NSString *learning_skills_id = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"learning_skills_id"]];
        NSString *learningSkillsName = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"learningSkillsName"]];
        NSDictionary *learningData = @{@"id":learning_skills_id,@"value":learningSkillsName};
        [self.dropDownField setLearningValueForEntity:kSkillEntity withData:learningData];
        
        //FEEDBACK
        NSString *general_feedback = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"general_feedback"]];
        NSLog(@"GENERAL FEEDBACK : %@", general_feedback);
        NSString *correct_answer_feedback = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"correct_answer_feedback"]];
        NSLog(@"CORRECT FEEDBACK : %@", correct_answer_feedback);
        NSString *wrong_answer_feedback = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"wrong_answer_feedback"]];
        NSLog(@"WRONG FEEDBACK : %@", wrong_answer_feedback);
        self.feedbackField.generalFeedback = general_feedback;
        self.feedbackField.correctAnswerFeedback = correct_answer_feedback;
        self.feedbackField.wrongAnswerFeedback = wrong_answer_feedback;
        
        //IS PUBLIC
        NSString *is_public = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"is_public"]];
        NSLog(@"IS PUBLIC : %@", is_public);
        BOOL flag = ([is_public isEqualToString:@"1"]) ? YES : NO;
        [self.shareField displayStatus:flag];

        //TAGS
        NSSet *tagSet = [NSSet setWithSet:[self.mo valueForKey:@"tags"]];
        [self.shareField displayTags:tagSet];
        
    }
}

- (NSDictionary *)getUserValues {
    
    //        "correct_answer_feedback" = "Correct!";
    //        "correct_answer_feedback" = "Correct!";
    //        "date_created" = "2015-11-27 02:16:31";
    //        "date_modified" = "2015-11-27 06:57:26";
    //        difficultyLevelName = Easy;
    //        "difficulty_level_id" = 1;
    //        "general_feedback" = "General Feedback";
    //        id = 3;
    //        "image_url" = "<null>";
    //        "is_deleted" = 0;
    //        "is_public" = 1;
    //        learningSkillsName = "Analysis/ Synthesis/ Evaluation";
    //        "learning_skills_id" = 4;
    //        name = "Sample true or false";
    //        "package_type_id" = 3;
    //        points = "1.00";
    //        questionTypeName = "True or False";
    //        "question_parent_id" = 0;
    //        "question_text" = "Sample true or false";
    //        "question_type_id" = 1;
    //        tags = "";
    //        "user_id" = 5;
    //        "wrong_answer_feedback" = "Wrong!";
    
    NSLog(@"------------------------------ USER INPUTS ------------------------------");

    //DIFFICULTY SKILL
    NSDictionary *difficultyOject = [self.dropDownField getDifficultyObject];
    NSString *proficiency_level_id = [NSString stringWithFormat:@"%@", difficultyOject[@"id"] ];
    NSString *proficiencyLevelName = [NSString stringWithFormat:@"%@", difficultyOject[@"value"] ];
    NSLog(@"DIFFICULTY LEVEL : %@", proficiency_level_id);
    NSLog(@"DIFFICULTY LEVEL NAME : %@", proficiencyLevelName);
    
    //LEARNING SKILL
    NSDictionary *learningObject = [self.dropDownField getLearningObject];
    NSString *learning_skills_id = [NSString stringWithFormat:@"%@", learningObject[@"id"] ];
    NSString *learningSkillsName = [NSString stringWithFormat:@"%@", learningObject[@"value"] ];
    NSLog(@"LEARNING SKILL : %@", learning_skills_id);
    NSLog(@"LEARNING SKILL NAME: %@", learningSkillsName);
    
    //FEEDBACK
    NSString *general_feedback = [NSString stringWithFormat:@"%@", self.feedbackField.generalFeedback ];
    NSLog(@"GENERAL FEEDBACK : %@", general_feedback);
    NSString *correct_answer_feedback = [NSString stringWithFormat:@"%@", self.feedbackField.correctAnswerFeedback  ];
    NSLog(@"CORRECT FEEDBACK : %@", correct_answer_feedback);
    NSString *wrong_answer_feedback = [NSString stringWithFormat:@"%@", self.feedbackField.wrongAnswerFeedback  ];
    NSLog(@"WRONG FEEDBACK : %@", wrong_answer_feedback);
    
    //IS PUBLIC
    BOOL flag = self.shareField.is_shared;
    NSString *is_public = (flag) ? @"1" : @"0";
    NSLog(@"IS SHARED : %@", is_public);

    //TODO: TAGS
//    NSString *tags = [NSString stringWithFormat:@"%@", [self.shareField getTagValues] ];
    NSSet *tags = [self.shareField getTagSet];
    
    NSDictionary *data = @{@"proficiency_level_id":proficiency_level_id,
                           @"proficiencyLevelName":proficiencyLevelName,
                           @"learning_skills_id":learning_skills_id,
                           @"learningSkillsName":learningSkillsName,
                           @"general_feedback":general_feedback,
                           @"correct_answer_feedback":correct_answer_feedback,
                           @"wrong_answer_feedback":wrong_answer_feedback,
                           @"is_public":is_public,
                           @"package_type_id":self.package_type_id,
                           @"tags":tags};
    return data;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
//    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
//    
//    // Code here will execute before the rotation begins.
//    // Equivalent to placing it in the deprecated method -[willRotateToInterfaceOrientation:duration:]
//    
//    
//    __weak typeof(self) wo = self;
//    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
//        
//        // Place code here to perform animations during the rotation.
//        // You can pass nil or leave this block empty if not necessary.
//        
//    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
//        [wo.feedbackField resizeContents];
//    }];
//}

//#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;
//}
//
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
