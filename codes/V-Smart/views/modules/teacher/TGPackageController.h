//
//  TGPackageController.h
//  V-Smart
//
//  Created by Ryan Migallos on 17/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGPackageController : UICollectionViewController

@end
