//
//  QuestionTextAreaItemCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 03/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionTextAreaItemCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextView *textArea;

@end
