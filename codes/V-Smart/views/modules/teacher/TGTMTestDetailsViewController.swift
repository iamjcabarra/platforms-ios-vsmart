//
//  TGTMTestDetailsViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 27/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
import WebKit

class TGTMTestDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet fileprivate var tableView: UITableView!
    @IBOutlet fileprivate var footerViewHeight: NSLayoutConstraint!
    @IBOutlet fileprivate var approveViewWidth: NSLayoutConstraint!
    @IBOutlet fileprivate var disapproveViewWidth: NSLayoutConstraint!
    @IBOutlet fileprivate var approveButton: UIButton!
    @IBOutlet fileprivate var disapproveButton: UIButton!
    
    // MARK: - Data Structure
    
    fileprivate struct QuestionInformation {
        var question: String
        var type: String
        var choices: [NSManagedObject]
        var images: [NSManagedObject]
        var extra: String
        
        init(question: String, type: String, choices: [NSManagedObject], images: [NSManagedObject], extra: String) {
            self.question = question
            self.type = type
            self.choices = choices
            self.images = images
            self.extra = extra
        }
    }
    
    var testID = ""
    var testName = ""
    var testStatus = ""
    
    fileprivate var questionInformationList = [QuestionInformation]()
    fileprivate var storedOffsets = [Int: CGFloat]()
    
    fileprivate let kQuestionHeaderCellIdentifier = "question_header_cell_identifier"
    fileprivate let kWithChoiceCellIdentifier = "question_content_with_choice_cell_identifier"
    fileprivate let kWithoutChoiceCellIdentifier = "question_content_without_choice_cell_identifier"
    fileprivate let kQuestionImageCellIdentifier = "question_image_cell_identifier"
    fileprivate let kCommentSegueIdentifier = "SHOW_TGTM_TEST_COMMENT_VIEW"
    
    // MARK: - Data Manager
    
    fileprivate lazy var tgmDataManager: TestGuruDataManager = {
        let tgdm = TestGuruDataManager.sharedInstance()
        return tgdm!
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.testName
        
        self.tableView.estimatedSectionHeaderHeight = 210.0
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        
        self.tableView.estimatedRowHeight = 165.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.assembleQuestionsForTestWithID(self.testID)
        self.renderFooterViewForTestWithStatus(self.testStatus)
        
        let approveTitle = NSLocalizedString("Approve", comment: "")
        self.approveButton.setTitle(approveTitle, for: UIControlState())
        self.approveButton.setTitle(approveTitle, for: .highlighted)
        self.approveButton.setTitle(approveTitle, for: .selected)
        
        let approveButtonAction = #selector(self.approveButtonAction(_:))
        self.approveButton.addTarget(self, action: approveButtonAction, for: .touchUpInside)
        
        let disApproveTitle = NSLocalizedString("Disapprove", comment: "")
        self.disapproveButton.setTitle(disApproveTitle, for: UIControlState())
        self.disapproveButton.setTitle(disApproveTitle, for: .highlighted)
        self.disapproveButton.setTitle(disApproveTitle, for: .selected)
        
        let disApproveButtonAction = #selector(self.disapproveButtonAction(_:))
        self.disapproveButton.addTarget(self, action: disApproveButtonAction, for: .touchUpInside)
        
        self.customNavigationBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Customize Navigation Bar
    
    fileprivate func customNavigationBar() {
        let commentButton = UIButton.init(type: UIButtonType.system)
        commentButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        commentButton.showsTouchWhenHighlighted = false
        commentButton.tintColor = UIColor.white
        
        let rightChevronImage = UIImage.init(named: "icn_right_thin_chevron_150px")
        commentButton.setImage(rightChevronImage, for: UIControlState())
        
        let commentButtonAction = #selector(self.commentButtonAction(_:))
        commentButton.addTarget(self, action: commentButtonAction, for: .touchUpInside)
        let commentButtonItem = UIBarButtonItem.init(customView: commentButton)
        
        let commentButtonLabel = UIButton.init(type: UIButtonType.system)
        let title = NSLocalizedString("Comments", comment: "")
        commentButtonLabel.frame = CGRect(x: 0, y: 0, width: 100, height: 20)
        commentButtonLabel.showsTouchWhenHighlighted = true
        commentButtonLabel.setTitle(title, for: UIControlState())
        commentButtonLabel.setTitle(title, for: .highlighted)
        commentButtonLabel.setTitle(title, for: .selected)
        commentButtonLabel.contentHorizontalAlignment = .right
        commentButtonLabel.tintColor = UIColor.white
        commentButtonLabel.addTarget(self, action: commentButtonAction, for: .touchUpInside)
        
        let commentButtonLabelItem = UIBarButtonItem.init(customView: commentButtonLabel)
        self.navigationItem.rightBarButtonItems = [commentButtonItem, commentButtonLabelItem]
    }
    
    // MARK: - Assemble Questions
    
    fileprivate func assembleQuestionsForTestWithID(_ testID: String) {
        let predicate = NSComparisonPredicate(keyPath: "quiz_id", withValue: testID, isExact: true)
        let descriptor = NSSortDescriptor(key: "sort_id", ascending: true)
        
        guard let questions = self.tgmDataManager.getObjectsForEntity(kTestQuestionItemEntity, predicate: predicate, sortDescriptor: descriptor) as? [NSManagedObject] else { return }
        
        for mo in questions {
            let type = self.tgmDataManager.stringValue(mo.value(forKey: "question_type_id"))
            var choices = [NSManagedObject]()
            
            // Ready for multiple blanks
            var answers = [String]()
            
            // FILL IN THE BLANKS AND IDENTIFICATION
            if type == "2" || type == "10" {
                if let choiceList = mo.value(forKey: "question_choices") as? Set<NSManagedObject> {
                    for choice in choiceList {
                        var text: String = self.tgmDataManager.stringValue(choice.value(forKey: "text"))
                        text = text.replacingOccurrences(of: ",", with: "/")
                        if type == "2" { text = "<font color=\"#3498DB\"><u>\(text)</u></font>" }
                        answers.append(text)
                    }
                }
            }
            // ALL OTHER QUESTION TYPES
            else {
                if let choiceList = mo.value(forKey: "question_choices") as? Set<NSManagedObject> {
                    for choice in choiceList {
                        choices.append(choice)
                    }
                }
            }
            
            var images = [NSManagedObject]()
            
            if let imageList = mo.value(forKey: "question_images") as? Set<NSManagedObject> {
                for image in imageList {
                    images.append(image)
                }
            }
            
            // NOTE: Refactor if multiple blanks is supported
            var question = self.tgmDataManager.stringValue(mo.value(forKey: "question_text"))
            
            // FILL IN THE BLANKS
            if (type == "2" && answers.count > 0) {
                question = question?.replacingOccurrences(of: "-blank-", with: answers[0])
            }
            
            // QUESTION TYPES WITH NO CHOICES
            let extra = type == "10" && answers.count > 0 ? "\(NSLocalizedString("Answer", comment: "")): \(answers[0])" : NSLocalizedString("Type your answer here...", comment: "")
            
            let sortedChoices = choices.sorted (by:  {
                let answer1: String! = self.tgmDataManager.stringValue($0.value(forKey: "order_number"))
                let answer2: String! = self.tgmDataManager.stringValue($1.value(forKey: "order_number"))
                return answer1 < answer2
            })
            
            self.questionInformationList.append(QuestionInformation(question: question!, type: type!, choices: sortedChoices, images: images, extra: extra))
        }
    }
    
    // MARK: - Render Footer View
    
    fileprivate func renderFooterViewForTestWithStatus(_ status: String) {
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.25, animations: {
            if self.testStatus == "1" {
                self.approveViewWidth.constant = 0
                self.disapproveViewWidth.constant = 0
                self.footerViewHeight.constant = 0
            }
            else if self.testStatus == "2" {
                self.approveViewWidth.constant = 125
                self.disapproveViewWidth.constant = 0
                self.footerViewHeight.constant = 75
            }
            else {
                self.approveViewWidth.constant = 125
                self.disapproveViewWidth.constant = 125
                self.footerViewHeight.constant = 75
            }
            self.view.layoutIfNeeded()
        })
    }
    
    // MARK: - Button Event Handlers
    
    func approveButtonAction(_ sender: UIButton) {
        let message = "\(NSLocalizedString("Processing", comment: ""))..."
        self.view.makeToastActivity(message: message)
        self.view.isUserInteractionEnabled = false
        
        let body = ["quiz_ids": [["id": self.testID, "status": "1"]]]
        
        self.tgmDataManager.requestChangeTestStatus("1", body: body, doneBlock: { (success) in
            DispatchQueue.main.async(execute: {
                self.view.hideToastActivity()
                self.view.isUserInteractionEnabled = true
                
                let messageA = NSLocalizedString("Approving of test was successful.", comment: "")
                let messageB = NSLocalizedString("Approving of test was unsuccessful.", comment: "")
                self.view.makeToast(message: success ? messageA : messageB, duration: 2.0, position: "Center" as AnyObject)
                self.view.isUserInteractionEnabled = true
                
                self.testStatus = success ? "1" : self.testStatus
                self.renderFooterViewForTestWithStatus(self.testStatus)
            })
        })
    }
    
    func disapproveButtonAction(_ sender: UIButton) {
        let message = "\(NSLocalizedString("Processing", comment: ""))..."
        self.view.makeToastActivity(message: message)
        self.view.isUserInteractionEnabled = false
        
        let body = ["quiz_ids": [["id": self.testID, "status": "2"]]]
        
        self.tgmDataManager.requestChangeTestStatus("2", body: body, doneBlock: { (success) in
            DispatchQueue.main.async(execute: {
                self.view.hideToastActivity()
                self.view.isUserInteractionEnabled = true
                
                let messageA = NSLocalizedString("Disapproving of test was successful.", comment: "")
                let messageB = NSLocalizedString("Disapproving of test was unsuccessful.", comment: "")
                self.view.makeToast(message: success ? messageA : messageB, duration: 2.0, position: "Center" as AnyObject)
                
                self.testStatus = success ? "2" : self.testStatus
                self.renderFooterViewForTestWithStatus(self.testStatus)
            })
        })
    }
    
    func commentButtonAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: self.kCommentSegueIdentifier, sender: nil)
        }
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.questionInformationList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let type = self.questionInformationList[section].type
        
        // FILL IN THE BLANKS
        if (type == "2") {
            return 0
        }
        // ALL OTHER QUESTION TYPES
        else {
            let choices = self.questionInformationList[section].choices
            return choices.count > 0 ? choices.count : 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let choices = self.questionInformationList[(indexPath as NSIndexPath).section].choices
        
        if choices.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: self.kWithChoiceCellIdentifier, for: indexPath) as! TGTMQuestionContentWithChoiceTableViewCell
            let choice = self.questionInformationList[(indexPath as NSIndexPath).section].choices[(indexPath as NSIndexPath).row]
            
            let text = choice.value(forKey: "text") as! String
            let is_correct = choice.value(forKey: "is_correct") as! String
            let choice_image = choice.value(forKey: "choice_image") as! String
            
            //cell.choiceTextLabel.attributedText = self.htmlString(text).htmlAttributedString()
            cell.choiceWebViewHeight.constant = 150.0
            cell.loadWebView(withContent: text)
            cell.choiceCheckImage.image = is_correct != "0" ? UIImage(named: "check_icn_active48.png") : UIImage(named: "check_icn48.png")
            cell.zeroChoiceImageHeight(choice_image == "" ? true : false)
            
            if choice_image != "" {
                let url = URL(string: choice_image)
                cell.choiceImage.sd_setImage(with: url, completed: { (image, error, type, url) in
                    if image == nil || error != nil { cell.choiceImage.image = UIImage(named: "unknown-file-03.png") }
                })
            }
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: self.kWithoutChoiceCellIdentifier, for: indexPath) as! TGTMQuestionContentWithoutChoiceTableViewCell
            let extra = self.questionInformationList[(indexPath as NSIndexPath).section].extra
            cell.questionAnswerTextView.placeholder = extra
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kQuestionHeaderCellIdentifier) as! TGTMQuestionHeaderTableViewCell
        
        let question: String = self.questionInformationList[section].question
        //cell.questionTextLabel.attributedText = self.htmlString(question).htmlAttributedString()
        cell.questionWebViewHeight.constant = 150.0
        cell.loadWebView(withContent: question)
        
        let question_count = "\(section + 1)."
        cell.questionNumberLabel.text = question_count
        
        let question_images_count = self.questionInformationList[section].images.count
        cell.questionImageCollectionView.isHidden = (question_images_count > 0 ? false : true)
        cell.zeroCollectionViewHeight(question_images_count > 0 ? false : true)
        
        if question_images_count > 0 {
            cell.setCollectionViewDataSourceDelegate(self, forSection: section)
            cell.collectionViewOffset = self.storedOffsets[section] ?? 0
        }
        
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, didEndDisplayingHeaderView view: UIView, forSection section: Int) {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kQuestionHeaderCellIdentifier) as! TGTMQuestionHeaderTableViewCell
        self.storedOffsets[section] = cell.collectionViewOffset
    }
    
    fileprivate func htmlString(_ string: String) -> String {
        return "<!DOCTYPE html><html><head><meta name='viewport' content='initial-scale=1.5'/><style>span{ font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: 150; font-size: 17px; }</style><meta charset='UTF-8'></head><body><script type='text/javascript'>window.onload = function() { window.location.href = 'ready://' + document.body.offsetHeight; }</script><span>\(string)</span></body></html>"
    }
    
}

// MARK: - Handle Question Image Collection View Data Source and Delegate

extension TGTMTestDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.questionInformationList[collectionView.tag].images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kQuestionImageCellIdentifier, for: indexPath) as! TGTMQuestionHeaderImageCollectionViewCell
        let image = self.questionInformationList[collectionView.tag].images[(indexPath as NSIndexPath).row]
        
        let image_url = image.value(forKey: "image") as! String
        let url = URL(string: image_url)
        
        cell.questionImage.sd_setImage(with: url, completed: { (image, error, type, url) in
            if image == nil || error != nil { cell.questionImage.image = UIImage(named: "unknown-file-03.png") }
        })
        
        //cell.frame.size = CGSize(width: 125, height: 125)
        
        return cell
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.kCommentSegueIdentifier {
            let commentView = segue.destination as! TGTMTestCommentViewController
            commentView.test_id = self.testID
        }
    }
    
}

// MARK: - Handle Text with HTML Contents

private extension String {
    func containsHTML() -> Bool {
        let expression = "<[^>]+>"
        
        do {
            let regEx = try NSRegularExpression(pattern: expression, options: .caseInsensitive)
            let numberOfMatches = regEx.numberOfMatches(in: self, options: .reportProgress , range: NSMakeRange(0, self.characters.count))
            if numberOfMatches == 1 { return true }
        }
        catch {
            print("Error in using regular expression.")
            return false
        }
        
        return false
    }
}

private extension String {
    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
            documentAttributes: nil) else { return nil }
        
        html.removeAttribute(NSParagraphStyleAttributeName, range: NSMakeRange(0, html.length))
        
        return html
    }
}

