//
//  TGTBTestOptionMenu.swift
//  V-Smart
//
//  Created by Julius Abarra on 23/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

protocol TGTBTestOptionMenuDelegate: class {
    func selectedTestOption (_ option:TGTBSwipeButtonActionType)
}

import UIKit

class TGTBTestOptionMenu: UIViewController {
    
    // Outlets
    @IBOutlet var deployButton: UIButton!
    @IBOutlet var savePDFButton: UIButton!
    @IBOutlet var editButton: UIButton!
    @IBOutlet var deployLabel: UILabel!
    @IBOutlet var savePDFLabel: UILabel!
    @IBOutlet var editLabel: UILabel!
    
    // Delegate
    weak var delegate:TGTBTestOptionMenuDelegate?
    
    // Data Manager
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tm = TestGuruDataManager.sharedInstance()
        return tm!
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Localize Labels
        self.deployLabel.text = NSLocalizedString("Deploy", comment: "");
        self.savePDFLabel.text = NSLocalizedString("Save to PDF", comment: "");
        self.editLabel.text = NSLocalizedString("Edit", comment: "");
        
        // Button Actions
        let deployAction = #selector( self.deployButtonAction(_:) )
        let savePDFAction = #selector( self.savePDFButtonAction(_:) )
        let editAction = #selector( self.editButtonAction(_:) )
        
        self.deployButton.addTarget(self, action: deployAction, for: .touchUpInside)
        self.savePDFButton.addTarget(self, action: savePDFAction, for: .touchUpInside)
        self.editButton.addTarget(self, action: editAction, for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func deployButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.delegate?.selectedTestOption(.deploy)
        })
    }
    
    func savePDFButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.delegate?.selectedTestOption(.saveAsPDF)
        })
    }
    
    func editButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.delegate?.selectedTestOption(.edit)
        })
    }
}
