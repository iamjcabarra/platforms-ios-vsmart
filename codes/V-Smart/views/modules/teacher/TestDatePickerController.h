//
//  TestDatePickerController.h
//  V-Smart
//
//  Created by Julius Abarra on 15/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

@protocol TestDatePickerDelegate <NSObject>

@required
- (void)selectedDate:(NSString *)dateString;

@end

#import <UIKit/UIKit.h>

@interface TestDatePickerController : UIViewController

@property (weak, nonatomic) id <TestDatePickerDelegate> delegate;

@end
