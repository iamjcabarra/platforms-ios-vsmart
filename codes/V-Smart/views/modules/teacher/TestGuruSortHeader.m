//
//  TestGuruSortHeader.m
//  V-Smart
//
//  Created by Ryan Migallos on 26/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruSortHeader.h"
#import "TestGuruSortHeaderCell.h"
#import "MainHeader.h"
#import <QuartzCore/QuartzCore.h>

//// BUTTON CATEGORY ////

@interface UIButton (CUSTOM)
- (void)setColor:(UIColor *)color forState:(UIControlState)state;
@end

@implementation UIButton (CUSTOM)
- (void)setColor:(UIColor *)color forState:(UIControlState)state {
    
    UIView *colorView = [[UIView alloc] initWithFrame:self.frame];
    colorView.backgroundColor = color;
    UIGraphicsBeginImageContext(colorView.bounds.size);
    [colorView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:state];
}
@end

//// BUTTON CATEGORY ////

@interface TestGuruSortHeader () <UICollectionViewDelegate>//<UICollectionViewDelegateFlowLayout, UICollectionViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *sortLabel;
@property (strong, nonatomic) IBOutlet UILabel *orderLabel;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIButton *applyButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *dateButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@property (strong, nonatomic) NSArray *items;

@property (strong, nonatomic) NSIndexPath *savedIndexPath;

@property (assign, nonatomic) BOOL isReset;
@property (assign, nonatomic) BOOL dateIsSelected;


@end

@implementation TestGuruSortHeader

static NSString * const kCell = @"sort_header_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupButtonControls];
    
    self.items = @[
                   @{
                       @"title": @"Question Type",
                       @"ascending":@NO,
                       @"selected":@YES
                    },
                   
                   @{
                       @"title": @"Difficulty Level",
                       @"ascending":@NO,
                       @"selected":@NO
                    },
                   
                   @{
                       @"title": @"Learning Skill",
                       @"ascending":@NO,
                       @"selected":@NO
                    },
                   
                   ];
}

- (void)setupButtonControls {
    self.isReset = NO;
    self.savedIndexPath = [NSIndexPath indexPathForItem:0 inSection:2];
    self.dateIsSelected = NO;
    
    self.orderLabel.text = NSLocalizedString(@"View By", nil);
    self.sortLabel.text = NSLocalizedString(@"Sort By", nil);
    
    [self title:@"Apply" forButton:self.applyButton];
    [self title:@"Cancel" forButton:self.cancelButton];
    [self.applyButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *newestTitle = NSLocalizedString(@"Newest", nil);
    NSString *oldestTitle = NSLocalizedString(@"Oldest", nil);
    [self.dateButton setTitle:newestTitle forState:UIControlStateNormal];
    [self.dateButton setTitle:oldestTitle forState:UIControlStateSelected];
    
    UIColor *inActiveText = UIColorFromHex(0x007AFF);
    UIColor *activeText = [UIColor whiteColor];
    [self.dateButton setTitleColor:inActiveText forState:UIControlStateNormal];
    [self.dateButton setTitleColor:activeText forState:UIControlStateSelected];
    
    UIColor *active = UIColorFromHex(0x007AFF);
    UIColor *inActive = UIColorFromHex(0xDFEAF2);
    [self.dateButton setColor:inActive forState:UIControlStateNormal];
    [self.dateButton setColor:active forState:UIControlStateSelected];
    
    CALayer *layer = self.dateButton.layer;
    layer.borderWidth = 1.0f;
    layer.borderColor = active.CGColor;
    [self.dateButton setTintColor:[UIColor clearColor]];
    
    [self.dateButton addTarget:self action:@selector(orderDateButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.resetButton addTarget:self action:@selector(resetButtonAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)orderDateButtonAction:(UIButton *)button {
    
    BOOL state = button.selected;
    button.selected = (state) ? NO : YES;
}

- (void)title:(NSString *)string forButton:(UIButton *)button {
    
    NSString *title = NSLocalizedString(string, nil);
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
}

//- (void)buttonAction:(UIButton *)sender {
//    NSString *actionType = (sender == self.applyButton) ? @"APPLY" : @"CANCEL";
//    NSMutableArray *sortDescriptors = nil;
//    
//    if ([actionType isEqualToString:@"CANCEL"]) {
//        self.isReset = NO;
//        if (self.isReset) {
//            self.savedIndexPath = [NSIndexPath indexPathForItem:0 inSection:2];
//        }
//
//        self.dateButton.selected = self.dateIsSelected;
//
//        [self.collectionView reloadData];
//        sortDescriptors = nil;
//    }
//    
//    if ([actionType isEqualToString:@"APPLY"]) {
//        sortDescriptors = [NSMutableArray arrayWithArray:[self getAllSortDetails] ];
//    }
//    
//    if ([(NSObject*)self.delegate respondsToSelector:@selector(sortDidSelectButtonAction:withSortDescriptors:)]) {
//        [self.delegate sortDidSelectButtonAction:actionType withSortDescriptors:sortDescriptors];
//    }
//}
    
- (void)buttonAction:(UIButton *)sender {
    NSString *actionType = (sender == self.applyButton) ? @"APPLY" : @"CANCEL";
    NSMutableDictionary *sortData = nil;
    
    if ([actionType isEqualToString:@"CANCEL"]) {
        self.isReset = NO;
        if (self.isReset) {
            self.savedIndexPath = [NSIndexPath indexPathForItem:0 inSection:2];
        }

        self.dateButton.selected = self.dateIsSelected;
        [self.collectionView reloadData];
        sortData = nil;
    }
    
    if ([actionType isEqualToString:@"APPLY"]) {
        sortData = [NSMutableDictionary dictionaryWithDictionary:[self getSortData]];
    }
    
    if ([(NSObject*)self.delegate respondsToSelector:@selector(sortDidSelectButtonAction:withSortData:)]) {
        [self.delegate sortDidSelectButtonAction:actionType withSortData:sortData];
    }
}

- (void)resetButtonAction:(UIButton *)sender {
    [self.collectionView reloadData];
    self.isReset = YES;
    self.dateButton.selected = NO;
}

- (NSMutableArray *)getAllSortDetails {
    
    NSMutableArray *sortDescriptors = nil;
    
    BOOL dateIsAscending = (self.dateButton.selected) ? YES : NO;
    NSSortDescriptor *sort_date = [NSSortDescriptor sortDescriptorWithKey:@"sort_date" ascending:dateIsAscending];
    sortDescriptors = [@[sort_date] mutableCopy];
    
    self.dateIsSelected = dateIsAscending;
    
    NSArray *indexPaths = [self.collectionView indexPathsForSelectedItems];
    if (indexPaths.count > 0) {
        NSIndexPath *indexPathSelected = [indexPaths lastObject];
        
        TestGuruSortHeaderCell *cell = (TestGuruSortHeaderCell *)[self.collectionView cellForItemAtIndexPath:indexPathSelected];
        self.savedIndexPath = indexPathSelected;
        
        BOOL selectedCellIsAscending = (cell.arrowButton.selected) ? NO : YES;
        
        NSString *sortKey = @"questionTypeName";
        if (indexPathSelected.row == 0) {
            sortKey = @"questionTypeName";
        } else if (indexPathSelected.row == 1) {
            sortKey = @"proficiencyLevelName";
        } else if (indexPathSelected.row == 2) {
            sortKey = @"learningSkillsName";
        }

        NSSortDescriptor *sort_by_id = [NSSortDescriptor sortDescriptorWithKey:sortKey ascending:selectedCellIsAscending];
        
        if (indexPathSelected != nil) {
            [sortDescriptors insertObject:sort_by_id atIndex:0];
    }
    } else {
        self.savedIndexPath = [NSIndexPath indexPathForItem:0 inSection:2];
    }
    
    
    return sortDescriptors;
}

- (NSDictionary *)getSortData {
    NSMutableDictionary *sortData = [NSMutableDictionary dictionary];
    
    // SORT BY
    NSArray *indexPaths = [self.collectionView indexPathsForSelectedItems];
    self.dateIsSelected = (self.dateButton.selected) ? YES : NO;
    
    if (indexPaths.count > 0) {
        NSIndexPath *indexPathSelected = [indexPaths lastObject];
        TestGuruSortHeaderCell *cell = (TestGuruSortHeaderCell *)[self.collectionView cellForItemAtIndexPath:indexPathSelected];
        self.savedIndexPath = indexPathSelected;
        
        NSString *prefix = (cell.arrowButton.selected) ? @"" : @"-";
        NSString *sort_key = @"question_type_name";
        
        if (indexPathSelected.row == 0) {
            sort_key = @"question_type_name";
        }
        else if (indexPathSelected.row == 1) {
            sort_key = @"difficulty_level_id";
        }
        else if (indexPathSelected.row == 2) {
            sort_key = @"learning_skills_id";
        }
        
        if (indexPathSelected != nil) {
            sort_key = [NSString stringWithFormat:@"%@%@", prefix, sort_key];
            [sortData setObject:sort_key forKey:@"sort_by"];
        }
    }
    else {
        self.savedIndexPath = [NSIndexPath indexPathForItem:0 inSection:2];
    }
    
    // VIEW BY
    NSString *date_modified = (self.dateButton.selected) ? @"date_modified" : @"-date_modified";
    [sortData setObject:date_modified forKey:@"view_by"];

    return sortData;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TestGuruSortHeaderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCell forIndexPath:indexPath];
    
    NSInteger row = indexPath.row;
    NSDictionary *data = self.items[row];
    
    UIColor *inActive = UIColorFromHex(0xDFEAF2);
    cell.backgroundObject.backgroundColor = inActive;
    
    NSString *title = [NSString stringWithFormat:@"%@", data[@"title"] ];
    cell.orderLabel.text = title;
    
    //TEXT COLOR
    UIColor *inActiveText = UIColorFromHex(0x007AFF);
    cell.orderLabel.textColor = inActiveText;
    
    cell.arrowButton.selected = NO;
    
    if (self.isReset == NO) {
        NSInteger savedRow = self.savedIndexPath.row;
        NSInteger savedSec = self.savedIndexPath.section;
        if ((savedRow == row) && (savedSec == 0)) {
            cell.selected = YES;
            [cell showState];
        }
    }
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

//- (void)collectionView:(UICollectionView *)cv didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
//    
//    TestGuruSortHeaderCell *cell = (TestGuruSortHeaderCell *)[cv cellForItemAtIndexPath:indexPath];
//    cell.backgroundObject.backgroundColor = UIColorFromHex(0x007AFF);
//}

- (void)collectionView:(UICollectionView *)cv didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    TestGuruSortHeaderCell *cell = (TestGuruSortHeaderCell *)[cv cellForItemAtIndexPath:indexPath];
    [cell showState];
    
    if (self.savedIndexPath.section != 2) {
        if (self.savedIndexPath.row != indexPath.row) {
            TestGuruSortHeaderCell *prevCell = (TestGuruSortHeaderCell *)[cv cellForItemAtIndexPath:self.savedIndexPath];
            [self.collectionView deselectItemAtIndexPath:self.savedIndexPath animated:NO];
            [prevCell showState];
        }
    }

}

- (void)collectionView:(UICollectionView *)cv didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    TestGuruSortHeaderCell *cell = (TestGuruSortHeaderCell *)[cv cellForItemAtIndexPath:indexPath];
    [cell showState];
}

#pragma mark <UICollectionViewDelegateFlowLayout>

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    
//    return UIEdgeInsetsMake(0, 20, 0, 0);
//}

@end
