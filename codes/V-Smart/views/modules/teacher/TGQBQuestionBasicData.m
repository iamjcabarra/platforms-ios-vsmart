//
//  TGQBQuestionBasicData.m
//  V-Smart
//
//  Created by Ryan Migallos on 12/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGQBQuestionBasicData.h"
#import "TestGuruDataManager.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"

#import "V_Smart-Swift.h"

typedef NS_ENUM (NSInteger, TGQuestionImageOptionValue) {
    TGQuestionImageOptionValueDefault,
    TGQuestionImageOptionValue1,
    TGQuestionImageOptionValue2,
    TGQuestionImageOptionValue3,
    TGQuestionImageOptionValue4,
};

@interface TGQBQuestionBasicData() <UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, UIImagePickerControllerDelegate>

// Localization Items
@property (strong, nonatomic) IBOutlet UILabel *titleIndicator;
@property (strong, nonatomic) IBOutlet UILabel *descriptionIndicator;
@property (strong, nonatomic) IBOutlet UILabel *imageIndicator;

// Text View Items
@property (strong, nonatomic) IBOutlet UIView *textViewContainer;
@property (strong, nonatomic) IBOutlet UILabel *descriptionPlaceholder;

// Image Placeholders
@property (strong, nonatomic) IBOutlet UIImageView *imageViewOne;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewThree;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewFour;

// Image Buttons
@property (strong, nonatomic) IBOutlet UIButton *buttonOne;
@property (strong, nonatomic) IBOutlet UIButton *buttonTwo;
@property (strong, nonatomic) IBOutlet UIButton *buttonThree;
@property (strong, nonatomic) IBOutlet UIButton *buttonFour;

@property (strong, nonatomic) IBOutlet UIButton *removeOne;
@property (strong, nonatomic) IBOutlet UIButton *removeTwo;
@property (strong, nonatomic) IBOutlet UIButton *removeThree;
@property (strong, nonatomic) IBOutlet UIButton *removeFour;

@property (strong, nonatomic) IBOutlet UIButton *addBlankButton;

@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) NSString *question_id;
@property (strong, nonatomic) NSManagedObject *mo;

@property (strong, nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) UIImagePickerController *imagePicker;

@property (assign, nonatomic) TGQuestionImageOptionValue questionImageOption;

@end

@implementation TGQBQuestionBasicData

- (void)awakeFromNib {
    
    self.tm = [TestGuruDataManager sharedInstance];
    // Initialization code
    
    self.questionImageOption = TGQuestionImageOptionValueDefault;
    
    NSString *title_string = NSLocalizedString(@"Title", nil);
    self.titleIndicator.text = [title_string uppercaseString];
    
    NSString *question_string = NSLocalizedString(@"Question", nil);
    self.descriptionIndicator.text = [question_string uppercaseString];
    self.descriptionLabel.delegate = self;
    
    NSString *points_string = NSLocalizedString(@"Points", nil);
    self.pointsIndicator.text = [points_string uppercaseString];
    self.pointsField.delegate = self;
    
    self.titleField.placeholder = NSLocalizedString(@"Please enter a title", nil);
    self.titleField.delegate = self;
    
    self.titleField.placeholder = @"";
    self.descriptionPlaceholder.text = @"";
    
//    [self applyShadowToView:self.textViewContainer];
    
    [self applyShadowToView:self.imageViewOne radius:1.0f];
    [self applyShadowToView:self.imageViewTwo radius:1.0f];
    [self applyShadowToView:self.imageViewThree radius:1.0f];
    [self applyShadowToView:self.imageViewFour radius:1.0f];
    
    
    //TESTING
    [self setupButtonTest];
    [self setupImagePickerControl];
    
    [self.imageIndicator localize];
    
    NSString *addBlankText = [NSString stringWithFormat:@"+ %@", NSLocalizedString(@"Add Blank", "")];
    [self.addBlankButton setTitle:addBlankText forState:UIControlStateNormal];
    [self.addBlankButton setTitle:addBlankText forState:UIControlStateSelected];
    [self.addBlankButton setTitle:addBlankText forState:UIControlStateHighlighted];
    [self.addBlankButton addTarget:self action:@selector(addBlankButtonAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)applyShadowToView:(UIView *)object {
    
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.5f;
}

- (void)applyShadowToView:(UIView *)object radius:(CGFloat)radius {
    
    CALayer *layer = object.layer;
    layer.shadowColor = [UIColor lightGrayColor].CGColor;
    layer.shadowOffset = CGSizeMake(2.0,2.0);
    layer.shadowRadius = radius;
    layer.shadowOpacity = 0.5f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setObjectData:(NSManagedObject *)object {
    if (object != nil) {
        self.mo = object;
        self.question_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"id"]];
        
        // Hide place holder
        if (self.descriptionLabel.text.length > 0) {
            self.descriptionPlaceholder.hidden = YES;
        }
        
        // Render Image if URL Exists using core data keys
        [self loadImageForKey:@"image_one" object:self.mo];
        [self loadImageForKey:@"image_two" object:self.mo];
        [self loadImageForKey:@"image_three" object:self.mo];
        [self loadImageForKey:@"image_four" object:self.mo];
        
        NSString *question_type_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"question_type_id"]];
        BOOL hide = [question_type_id isEqualToString:@"2"] ? NO : YES;
        self.addBlankButton.hidden = hide;
        
        if ([question_type_id isEqualToString:@"2"]) {
            NSString *question_text = [NSString stringWithFormat:@"%@", [object valueForKey:@"question_text"]];
            NSInteger count = [self countString:@"-blank-" inText:question_text];
            UIColor *color = count >= 1 ? [UIColor lightGrayColor] : UIColorFromHex(0x0080FF);
            [self.addBlankButton setBackgroundColor:color];
        }
    }
}

#pragma mark - Add Blank Button Action 

- (void)addBlankButtonAction:(id)sender {
    if (self.mo != nil) {
        NSString *currentQuestion = self.descriptionLabel.text;
        NSRange range = [currentQuestion rangeOfString:@"-blank-"];
        
        if (range.location == NSNotFound) {
            /*
            self.descriptionLabel.text = [NSString stringWithFormat:@"%@-blank-", currentQuestion];
            NSDictionary *userData = @{@"question_text": self.descriptionLabel.text};
            NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:self.question_id];
            [self.tm updateQuestion:kQuestionEntity predicate:predicate details:userData];
            [self.addBlankButton setBackgroundColor:[UIColor lightGrayColor]];
            */
           
            [self.descriptionLabel replaceRange:self.descriptionLabel.selectedTextRange withText:@" -blank- "];
            NSDictionary *userData = @{@"question_text": self.descriptionLabel.text};
            NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:self.question_id];
            [self.tm updateQuestion:kQuestionEntity predicate:predicate details:userData];
            [self.addBlankButton setBackgroundColor:[UIColor lightGrayColor]];
            
        }
        else {
            NSString *message = NSLocalizedString(@"Question cannot contain more than one (1) blank.", "");
            [self.contentView makeToast:message duration:2.0f position:@"center"];
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // verify max length has not been exceeded
    NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    NSString *originalText = [NSString stringWithFormat:@"%@", textField.text];
    
    
    NSInteger strCount = [updatedText length] - [[updatedText stringByReplacingOccurrencesOfString:@"." withString:@""] length];
    NSInteger numberOfMatches = strCount / [@"." length];
    
    NSInteger maximumLength = (numberOfMatches == 1) ? 4 : 3;
    
    if ( textField == self.pointsField ) {
        
        // allow backspace
//        if (!string.length)
//        {
//            return YES;
//        }
        
        NSCharacterSet *numberSet = [NSCharacterSet decimalDigitCharacterSet];
        if ([string rangeOfCharacterFromSet:[numberSet invertedSet]].location != NSNotFound)
        {
            // Allow ONE decimal point
//            NSError *error = NULL;
//            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"." options:NSRegularExpressionCaseInsensitive error:&error];
//            NSUInteger numberOfMatches = [regex numberOfMatchesInString:updatedText options:0 range:NSMakeRange(0, [updatedText length])];
            
            if ([string isEqualToString:@"."]) {
                
                if (numberOfMatches > 1) {
                    return NO;
                }
                
                return NO; // CHANGE TO YES IF 1 DECIMAL POINT IS ALLOWED
            }
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
        
        if (updatedText.length > maximumLength) // 4 was chosen for SSN verification
        {
            // suppress the max length message only when the user is typing
            // easy: pasted data has a length greater than 1; who copy/pastes one character?
            if (string.length > 1)
            {
                // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
            }
            return NO;
        }
        
        if (self.mo != nil) {
            
            if ([updatedText floatValue] > 1.0) {
                self.pointsIndicator.text = [NSLocalizedString(@"Points", nil) uppercaseString];
            } else {
                self.pointsIndicator.text = [NSLocalizedString(@"Point", nil) uppercaseString];
            }
            
            //QUESTION POINTS STORE TO CORE DATA
            NSDictionary *userData = @{@"points":updatedText};
            [self updateDetailsWithValue:userData];
        }
        
    }
    
    if ( textField == self.titleField ) {
        
        if (self.mo != nil) {
            //QUESTION TITLE STORE TO CORE DATA
            if (updatedText.length > 1) // 4 was chosen for SSN verification
            {
                NSCharacterSet *leadingTrailing = [NSCharacterSet whitespaceCharacterSet];
                updatedText = [updatedText stringByTrimmingCharactersInSet:leadingTrailing];
            }
            
            NSDictionary *userData = @{@"name":updatedText};
            [self updateDetailsWithValue:userData];
        }
    }
    
    return YES;
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    
//    NSString *updatedText = [NSString stringWithFormat:@"%@", textView.text];
//    if (self.mo != nil) {
//        //QUESTION DESCRIPTION STORE TO CORE DATA
//        NSDictionary *userData = @{@"question_text":updatedText};
//        [self updateDetailsWithValue:userData];
//    }
    
    NSString *updatedText = [NSString stringWithFormat:@"%@", textView.text];
    
    if (self.mo != nil) {
        NSString *question_type_id = [self.tm stringValue:[self.mo valueForKey:@"question_type_id"]];
        
        // Fill in the blanks only
        if ([question_type_id isEqualToString:@"2"]) {
            NSInteger count = [self countString:@"-blank-" inText:updatedText];
            
            if (count > 1) {
                NSRange lastBlank = [updatedText rangeOfString:@"-blank-" options:NSBackwardsSearch];
                
                if (lastBlank.location != NSNotFound) {
                    updatedText = [updatedText stringByReplacingCharactersInRange:lastBlank withString:@""];
                    textView.text = updatedText;
                }
            }
            
            UIColor *color = count >= 1 ? [UIColor lightGrayColor] : UIColorFromHex(0x0080FF);
            [self.addBlankButton setBackgroundColor:color];
        }
        
        NSDictionary *userData = @{@"question_text":updatedText};
        [self updateDetailsWithValue:userData];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.descriptionPlaceholder.hidden = YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSString *updated_string = [NSString stringWithFormat:@"%@", textView.text];
    if (updated_string.length > 1) {
        self.descriptionPlaceholder.hidden = YES;
    }
    return YES;
}

- (NSInteger)countString:(NSString *)string inText:(NSString *)text {
    NSInteger foundCount = 0;
    NSRange range = NSMakeRange(0, text.length);
    range = [text rangeOfString:string options:NSLiteralSearch range:range locale:nil];
    
    while (range.location != NSNotFound) {
        foundCount++;
        range = NSMakeRange(range.location + range.length, text.length - (range.location + range.length));
        range = [text rangeOfString:string options:NSLiteralSearch range:range locale:nil];
    }
    
    return foundCount;
}

////////////////////////////////////////////////////////////////
// ------------- IMAGE UPLOADING IMPLEMENTATION ------------- //
////////////////////////////////////////////////////////////////
- (void)loadImageForKey:(NSString *)key object:(NSManagedObject *)object {

    // GET IMAGE URL
    NSString *url = [NSString stringWithFormat:@"%@", [object valueForKey:key] ];
    NSURL *imageURL = [NSURL URLWithString:url];
        __weak typeof(self) wo = self;
        UIImageView *imageView = [wo imageViewForKey:key];
    UIButton *removeButton = [wo removeButtonForKey:key];
    if ( [url containsString:@"http"] ) {
        [imageView sd_setImageWithURL:imageURL];
        removeButton.hidden = NO;
    } else {
        if ([[imageURL scheme] isEqualToString:@"assets-library"]) {
            NSString *label_key = [self stringKeyForValue:key];
            NSData *image_data = [NSData dataWithData:[object valueForKey:label_key]];
            dispatch_async(dispatch_get_main_queue(), ^{
                imageView.image = [UIImage imageWithData:image_data];
                removeButton.hidden = NO;
            });
        }
    
    }
}

- (UIImageView *)imageViewForKey:(NSString *)key {
    
    if ([key isEqualToString:@"image_one"]) {
        self.imageViewOne.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewOne.image = [UIImage imageNamed:@"icn_no_image.png"];
        return self.imageViewOne;
    }
    
    if ([key isEqualToString:@"image_two"]) {
        self.imageViewTwo.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewTwo.image = [UIImage imageNamed:@"icn_no_image.png"];
        return self.imageViewTwo;
    }
    
    if ([key isEqualToString:@"image_three"]) {
        self.imageViewThree.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewThree.image = [UIImage imageNamed:@"icn_no_image.png"];
        return self.imageViewThree;
    }
    
    if ([key isEqualToString:@"image_four"]) {
        self.imageViewFour.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewFour.image = [UIImage imageNamed:@"icn_no_image.png"];
        return self.imageViewFour;
    }
    
    return nil;
}

- (UIButton *)removeButtonForKey:(NSString *)key {
    
    if ([key isEqualToString:@"image_one"]) {
        self.removeOne.hidden = YES;
        return self.removeOne;
    }
    
    if ([key isEqualToString:@"image_two"]) {
        self.removeTwo.hidden = YES;
        return self.removeTwo;
    }
    
    if ([key isEqualToString:@"image_three"]) {
        self.removeThree.hidden = YES;
        return self.removeThree;
    }
    
    if ([key isEqualToString:@"image_four"]) {
        self.removeFour.hidden = YES;
        return self.removeFour;
    }
    
    return nil;
}

////////////////////////////////////////////////////////////////
// ------------- IMAGE UPLOADING IMPLEMENTATION ------------- //
////////////////////////////////////////////////////////////////

- (void)setupImagePickerControl {
    
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = YES;
    self.imagePicker.modalPresentationStyle = UIModalPresentationPopover;
    self.popover = [[UIPopoverController alloc] initWithContentViewController:self.imagePicker];
}

- (void)setupButtonTest {
    
    SEL action = @selector(showImagePickerAction:);
//    SEL action = @selector(imageTestUploadAction:);
    
    [self.buttonOne addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [self.buttonTwo addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [self.buttonThree addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [self.buttonFour addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    SEL deleteAction = @selector(removeImageAction:);
    
    [self.removeOne addTarget:self action:deleteAction forControlEvents:UIControlEventTouchUpInside];
    [self.removeTwo addTarget:self action:deleteAction forControlEvents:UIControlEventTouchUpInside];
    [self.removeThree addTarget:self action:deleteAction forControlEvents:UIControlEventTouchUpInside];
    [self.removeFour addTarget:self action:deleteAction forControlEvents:UIControlEventTouchUpInside];
    
    //    self.imageViewOne.image = [UIImage imageNamed:@"abstract_icon1.png"];
    //    self.imageViewTwo.image = [UIImage imageNamed:@"abstract_icon2.png"];
    //    self.imageViewThree.image = [UIImage imageNamed:@"abstract_icon3.png"];
    //    self.imageViewFour.image = [UIImage imageNamed:@"abstract_icon4.png"];
}


- (void)removeImageAction:(UIButton *)sender {
    
    UIButton *b = sender;
    b.hidden = YES;
    NSString *delete_images = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"delete_images"] ];
    delete_images = [delete_images stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    NSString *item = @"";
    NSString *URLKey = @"image_one";
    
    if (b == self.removeOne) {
        self.imageViewOne.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewOne.image = [UIImage imageNamed:@"icn_no_image.png"];
        item = @"1";
        URLKey = @"image_one";
    }
    
    if (b == self.removeTwo) {
        self.imageViewTwo.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewTwo.image = [UIImage imageNamed:@"icn_no_image.png"];
        item = @"2";
        URLKey = @"image_two";
    }
    
    if (b == self.removeThree) {
        self.imageViewThree.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewThree.image = [UIImage imageNamed:@"icn_no_image.png"];
        item = @"3";
        URLKey = @"image_three";
    }
    
    if (b == self.removeFour) {
        self.imageViewFour.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewFour.image = [UIImage imageNamed:@"icn_no_image.png"];
        item = @"4";
        URLKey = @"image_four";
    }
    
    if (delete_images.length > 0) {
        item = [NSString stringWithFormat:@"|%@", item];
    }
    
    delete_images = [NSString stringWithFormat:@"%@%@", delete_images,item];
    
    NSArray *delete_images_list = [delete_images componentsSeparatedByString:@"|"];
    
    delete_images_list = [delete_images_list valueForKeyPath:@"@distinctUnionOfObjects.self"];
    
    NSString * result = [[delete_images_list valueForKey:@"description"] componentsJoinedByString:@"|"];
    
    NSDictionary *userData = @{@"delete_images":result,
                               URLKey:@""};
    [self updateDetailsWithValue:userData];
}

- (void)showImagePickerAction:(UIButton *)sender {
    
    UIButton *b = sender;
    
    if (b == self.buttonOne) {
        self.questionImageOption = TGQuestionImageOptionValue1;
    }
    
    if (b == self.buttonTwo) {
        self.questionImageOption = TGQuestionImageOptionValue2;
    }
    
    if (b == self.buttonThree) {
        self.questionImageOption = TGQuestionImageOptionValue3;
    }
    
    if (b == self.buttonFour) {
        self.questionImageOption = TGQuestionImageOptionValue4;
    }
    
    [self.popover presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    NSURL *imageURL = [info valueForKey: UIImagePickerControllerReferenceURL];
    
    NSString *key_label = @"image_one_data";
    NSString *key_label_url = @"image_one";
    NSString *image_index = @"1";
    
    if (self.questionImageOption == TGQuestionImageOptionValue1) {
        self.imageViewOne.image = chosenImage;
        self.removeOne.hidden = NO;
        key_label = @"image_one_data";
        key_label_url = @"image_one";
        image_index = @"1";
    }
    
    if (self.questionImageOption == TGQuestionImageOptionValue2) {
        self.imageViewTwo.image = chosenImage;
        self.removeTwo.hidden = NO;
        key_label = @"image_two_data";
        key_label_url = @"image_two";
        image_index = @"2";
    }
    
    if (self.questionImageOption == TGQuestionImageOptionValue3) {
        self.imageViewThree.image = chosenImage;
        self.removeThree.hidden = NO;
        key_label = @"image_three_data";
        key_label_url = @"image_three";
        image_index = @"3";
    }
    
    if (self.questionImageOption == TGQuestionImageOptionValue4) {
        self.imageViewFour.image = chosenImage;
        self.removeFour.hidden = NO;
        key_label = @"image_four_data";
        key_label_url = @"image_four";
        image_index = @"4";
    }

    CGFloat compression = 0.5;
    NSData *image_data = UIImageJPEGRepresentation(chosenImage, compression);
//    NSData *image_data = UIImagePNGRepresentation(chosenImage);
    
    NSString *delete_images = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"delete_images"] ];
    delete_images = [delete_images stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    NSMutableArray *delete_images_list = [NSMutableArray arrayWithArray:[delete_images componentsSeparatedByString:@"|"] ];
    
    delete_images_list = [NSMutableArray arrayWithArray:[delete_images_list valueForKeyPath:@"@distinctUnionOfObjects.self"] ];
    
    if ([delete_images_list containsObject:image_index]) {
        [delete_images_list removeObject:image_index];
    }
    
    NSString * result = [[delete_images_list valueForKey:@"description"] componentsJoinedByString:@"|"];
    
    NSDictionary *userData = @{key_label:image_data,
                               key_label_url:[imageURL absoluteString],
                               @"delete_images":result};
    
    [self updateDetailsWithValue:userData];
    
    [self.popover dismissPopoverAnimated:YES];
}

- (NSString *)stringKeyForValue:(NSString *)key {
 
    NSString *label_key = @"image_one_data";
    
    if ([key isEqualToString:@"image_one"]) {
        label_key = @"image_one_data";
    }
    
    if ([key isEqualToString:@"image_two"]) {
        label_key = @"image_two_data";
    }
    
    if ([key isEqualToString:@"image_three"]) {
        label_key = @"image_three_data";
    }
    
    if ([key isEqualToString:@"image_four"]) {
        label_key = @"image_four_data";
    }
    
    return label_key;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self.popover dismissPopoverAnimated:YES];
}

- (NSString *)keyForLabel:(NSString *)label {
    
    NSString *key = @"image_one";
    
    if ([label isEqualToString:@"1"]) {
        key = @"image_one";
    }
    
    if ([label isEqualToString:@"2"]) {
        key = @"image_two";
    }
    
    if ([label isEqualToString:@"3"]) {
        key = @"image_three";
    }
    
    if ([label isEqualToString:@"4"]) {
        key = @"image_four";
    }
    
    return key;
}

- (void)imageTestUploadAction:(UIButton *)button {
    
    NSLog(@"-----> %s", __PRETTY_FUNCTION__);
    
    NSDictionary *d1 = [self generateImageDataWithName:@"abstract_icon1" key:@"abstract_icon1" filename:@"abstract_icon1.png"];
    NSDictionary *d2 = [self generateImageDataWithName:@"abstract_icon2" key:@"abstract_icon2" filename:@"abstract_icon2.png"];
    NSDictionary *d3 = [self generateImageDataWithName:@"abstract_icon3" key:@"abstract_icon3" filename:@"abstract_icon3.png"];
    NSDictionary *d4 = [self generateImageDataWithName:@"abstract_icon4" key:@"abstract_icon4" filename:@"abstract_icon4.png"];
    NSArray *list = @[d1,d2,d3,d4];
    
    [self.tm uploadImageFiles:list doneBlock:^(BOOL status) {
        //TODO
    }];
}

- (NSDictionary *)generateImageDataWithName:(NSString *)name key:(NSString *)key filename:(NSString *)filename {
    
    UIImage *image = [UIImage imageNamed:filename];
    CGFloat compression = 0.6f;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    NSDictionary *dictionary = @{@"name":name, @"key":key, @"filename":filename, @"data":data};
    return dictionary;
}

- (void)updateDetailsWithValue:(NSDictionary *)userData {
    
    NSLog(@"user data : %@", userData);
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:self.question_id];
    [self.tm updateQuestion:kQuestionEntity predicate:predicate details:userData];
}
@end
