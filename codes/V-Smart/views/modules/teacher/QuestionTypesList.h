//
//  QuestionTypesList.h
//  V-Smart
//
//  Created by Ryan Migallos on 28/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuestionTypesListDelegate <NSObject>
@required
- (void)createQuestionTypeObject:(NSDictionary *)dictionary;
@end

@interface QuestionTypesList : UIViewController

@property (weak,   nonatomic) id <QuestionTypesListDelegate> delegate;
@end
