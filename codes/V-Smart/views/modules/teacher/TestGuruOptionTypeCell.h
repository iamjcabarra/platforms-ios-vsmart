//
//  TestGuruOptionTypeCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 10/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TGOptionTypeStyle) {
    TGOptionTypeStyleValue1,
    TGOptionTypeStyleValue2,
    TGOptionTypeStyleValue3,
    TGOptionTypeStyleDefault,
};

@interface TestGuruOptionTypeCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *typeLabel;
@property (assign, nonatomic) TGOptionTypeStyle optionStyle;

- (void)showHighlight:(BOOL)state forStyle:(TGOptionTypeStyle)style;

@end
