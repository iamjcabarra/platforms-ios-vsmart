//
//  TGTMTestCommentCell.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 28/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class TGTMTestCommentCell: MGSwipeTableCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var commentLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
