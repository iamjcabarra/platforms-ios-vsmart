//
//  TestBankQuestionList.m
//  V-Smart
//
//  Created by Ryan Migallos on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestBankQuestionList.h"
#import "TestBankQuestionItemCell.h"
#import "TestGuruDataManager.h"
#import "HUD.h"

#define ResultsTableView self.searchResultsTableViewController.tableView

@interface TestBankQuestionList () <NSFetchedResultsControllerDelegate, UISearchControllerDelegate,
UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate> {
    
    NSManagedObject *mo_selected;
    UITableView *tableView_active;
}

@property (nonatomic, strong) TestGuruDataManager *tm;

@property (nonatomic, strong) IBOutlet UILabel *questionListHeader;
@property (nonatomic, strong) IBOutlet UIView *searchContainer;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIButton *closeButton;
@property (nonatomic, strong) IBOutlet UIButton *saveButton;

@property (nonatomic, strong) UIRefreshControl *tableRefreshControl;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) UITableViewController *searchResultsTableViewController;
@property (nonatomic, strong) NSArray *results;
@property (nonatomic, assign) BOOL isAscending;
@property (nonatomic, assign) BOOL isEditMode;
@property (nonatomic, assign) BOOL allIsSelected;

@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSMutableSet *selectionList;
@property (weak, nonatomic) IBOutlet UITextField *questionSearchTextField;

@property (nonatomic, strong) NSSortDescriptor *sort_descriptor;

@end

@implementation TestBankQuestionList

static NSString *kCellIdentifier = @"testbank_question_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.selectionList = [NSMutableSet set];
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    [self setupLocalization];
    
    // Uncomment the following line to preserve selection between presentations.
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.tableView.allowsSelection = YES;
    self.tableView.allowsMultipleSelection = YES;
    
    self.isAscending = NO;
    self.allIsSelected = NO;
    self.sort_descriptor = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:self.isAscending];
    
    self.tm = [TestGuruDataManager sharedInstance];
    self.managedObjectContext = self.tm.mainContext;
    
    [self.closeButton addTarget:self action:@selector(closeAndDismiss:) forControlEvents:UIControlEventTouchUpInside];
    [self.saveButton addTarget:self action:@selector(saveSelectedItems:) forControlEvents:UIControlEventTouchUpInside];
    self.saveButton.hidden = YES;
    
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    SEL refreshAction = @selector(listAllQuestion);
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    [self setupRightBarButton];
    [self setupSearchCapabilities];
    
    [self listAllQuestion];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupLocalization {
  
    self.questionListHeader.text = NSLocalizedString(@"Select Questions", nil);
    
    NSString *closeTitle = NSLocalizedString(@"Cancel", nil);
    [self.closeButton setTitle:closeTitle forState:UIControlStateNormal];
    [self.closeButton setTitle:closeTitle forState:UIControlStateHighlighted];
    
    NSString *saveTitle = NSLocalizedString(@"Assign", nil);
    [self.saveButton setTitle:saveTitle forState:UIControlStateNormal];
    [self.saveButton setTitle:saveTitle forState:UIControlStateHighlighted];
}

- (void)setupRightBarButton {
    
//    UIImage *sortImage = [UIImage imageNamed:@"sort_258x258"];
//    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    sortButton.frame = CGRectMake(0, 0, 44, 44);
//    sortButton.showsTouchWhenHighlighted = YES;
//    [sortButton setImage:sortImage forState:UIControlStateNormal];
//    [sortButton setImage:sortImage forState:UIControlStateHighlighted];
//    [sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
}

- (void)setupSearchCapabilities {
    
//    self.results = [[NSMutableArray alloc] init];
//    
//    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestGuruStoryboard" bundle:nil];
//    UITableViewController *srtvc = [sb instantiateViewControllerWithIdentifier:@"testbank_search_table_view_controller"];
//    srtvc.tableView.dataSource = self;
//    srtvc.tableView.delegate = self;
////    srtvc.tableView.allowsSelection = YES;
////    srtvc.tableView.allowsMultipleSelection = YES;
//    
//    self.searchResultsTableViewController = srtvc;
//    
//    // Init a search controller with its table view controller for results.
//    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsTableViewController];
//    self.searchController.searchResultsUpdater = self;
//    self.searchController.delegate = self;
//    
//    // Make an appropriate size for search bar and add it as a header view for initial table view.
//    [self.searchController.searchBar sizeToFit];
//    self.tableView.tableHeaderView = self.searchController.searchBar;
////    [self.searchContainer addSubview:self.searchController.searchBar];
//    
//    
//    // Enable presentation context.
//    self.definesPresentationContext = YES;
    
    [self.questionSearchTextField addTarget:self action:@selector(updateSearch) forControlEvents:UIControlEventEditingChanged];
    
}

- (void)listAllQuestion {
    __weak typeof(self) wo = self;
    [self.tm requestAssignQuestionListForUser:self.user_id enableChoice:YES doneBlock:^(BOOL status) {
        //do nothing
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo.tableRefreshControl endRefreshing];
            });
    }];
    
//    // BUG FIX: App crashes when adding question to test
//    // Recursive api calls cause this crash
//    __weak typeof(self) wo = self;
//    NSString *indicatorString = NSLocalizedString(@"Loading...", nil);
//    [HUD showUIBlockingIndicatorWithText:indicatorString];
//    
//    [self.tm requestQuestionListForUser:self.user_id enableChoice:YES doneBlock:^(BOOL status) {
//        if (status) {
//            [HUD hideUIBlockingIndicator];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [wo.tableRefreshControl endRefreshing];
//            });
//        }
//        else {
//            [HUD hideUIBlockingIndicator];
//        }
//    }];
}

- (void)closeAndDismiss:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)saveSelectedItems:(UIButton *)sender {
//    __weak typeof(self) wo = self;
//    if (self.selectionList.count > 0) {
//        [self.tm insertQuestions:self.selectionList testObject:self.quiz_mo doneBlock:^(BOOL status) {
//            
//            if (status == YES) {
//                [wo dismissViewControllerAnimated:YES completion:nil];
//            }
//        }];
//    }
    
    // BUG FIX: App UI freezes when adding question to test (refactored)
    // Running multiple api calls in the background freezes the ui
    __weak typeof(self) wo = self;
    if (self.selectionList.count > 0) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [wo.tm insertQuestions:self.selectionList testObject:self.quiz_mo doneBlock:^(BOOL status) {
            if (status == YES) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                [wo dismissViewControllerAnimated:YES completion:nil];
                    });
            }
        }];
        });
    }
}

- (IBAction)refreshQuestionList:(id)sender {
    
    [self listAllQuestion];
}

- (void)sortButtonAction:(id)sender {
    
    self.isAscending = (_isAscending) ? NO : YES;
    
//    if ([tableView_active isEqual:ResultsTableView]) {
//        [self reloadSearchResults];
//    }
//    
//    if (![tableView_active isEqual:ResultsTableView]) {
//        [self reloadFetchedResultsController];
//    }
    
    [self reloadFetchedResultsController];
    
}

//- (void)reloadSearchResults {
//    
//    NSArray *items = [NSArray arrayWithArray:self.results];
//    
//    // Edit the sort key as appropriate.
//    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:self.isAscending];
//    NSArray *sorted = [items sortedArrayUsingDescriptors:@[date_modified]];
//    
//    // Set up results.
//    self.results = [NSArray arrayWithArray:sorted];
//    
//    // Reload search table view.
//    [tableView_active reloadData];
//}

- (void)reloadFetchedResultsController {
    
    self.allIsSelected = NO;
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = 1;
    
    if ([tableView isEqual:ResultsTableView]) {
        return count;
    } else {
        return [[self.fetchedResultsController sections] count];
    }
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    if ([tableView isEqual:ResultsTableView]) {
        
        if (self.results) {
            return self.results.count;
        } else {
            return count;
        }
        
    } else {
        
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        count = [sectionInfo numberOfObjects];
        
    }
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView_active = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
//    if ([tableView isEqual:ResultsTableView]) {
//        [self configureFilteredCell:cell indexPath:indexPath];
//    }
//    
//    if (![tableView isEqual:ResultsTableView]) {
//        [self configureCell:cell atIndexPath:indexPath];
//    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

//- (void)configureFilteredCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
//    NSManagedObject *mo = self.results[indexPath.row];
//    TestBankQuestionItemCell *questionCell = (TestBankQuestionItemCell *)cell;
//    [self configureCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
//}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    TestBankQuestionItemCell *questionCell = (TestBankQuestionItemCell *)cell;
    [self configureCell:questionCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(TestBankQuestionItemCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *title = [NSString stringWithFormat:@"%@", [mo valueForKey:@"name"] ];
    NSString *type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"questionTypeName"] ];
    
    UITableViewCellAccessoryType cellAccessoryType = UITableViewCellAccessoryNone;
    
//    if ([is_selected isEqualToString:@"1"]) {
//        cellAccessoryType = UITableViewCellAccessoryCheckmark;
//    } else {
//        cellAccessoryType = UITableViewCellAccessoryNone;
//    }
    
    if ([self.selectionList containsObject:[mo valueForKey:@"id"]]) {
        cellAccessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cellAccessoryType = UITableViewCellAccessoryNone;
    }
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        cell.questionName.text = title;
        cell.questionType.text = type;
        cell.accessoryType = cellAccessoryType;
//        [cell setSelected:selectValue];
        
    });
    
    // BUG FIX 692
    NSSet *choice_sets = [mo valueForKey:@"choices"];
    NSArray *choices = [choice_sets allObjects];
    
    if (choices.count > 0) {
        cell.noChoiceLabel.hidden = YES;
    } else {
        cell.noChoiceLabel.hidden = NO;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

//    NSManagedObject *mo = nil;
//    
//    if ([tableView isEqual:ResultsTableView]) {
//        mo = self.results[indexPath.row];
//    }
//    
//    if (![tableView isEqual:ResultsTableView]) {
//        mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//    }
    
    self.isEditMode = NO;
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *question_id = [mo valueForKey:@"id"];
    
    if ([self.selectionList containsObject:question_id]) {
        [self removeObjectWithID:question_id indexPath:indexPath];
    } else {
        NSSet *choice_sets = [mo valueForKey:@"choices"];
        NSArray *choices = [choice_sets allObjects];
        
        // BUG FIX 692
        if (choices.count > 0) {
        mo_selected = mo;
        tableView_active = tableView;
            
        if (self.selectionList) {
            [self addObjectWithID:question_id indexPath:indexPath];
        }

        NSLog(@"%s : %@", __PRETTY_FUNCTION__, self.selectionList);

        BOOL status = (self.selectionList == 0) ? YES : NO;
        __weak typeof(self) wo = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.saveButton setHidden:status];
        });
        } else {
//            UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
//                                                         message:NSLocalizedString(@"You can't add this question, it has empty choices", nil)
//                                                        delegate:self
//                                               cancelButtonTitle:NSLocalizedString(@"OK", nil)
//                                               otherButtonTitles:nil];
//            [av show];
//            
//            [tableView_active deselectRowAtIndexPath:indexPath animated:YES];
    //        NSString *question_id = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"id"] ];
    //        [self.selectionList removeObject:question_id];
        }
    }

}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = nil;
    
    if ([tableView isEqual:ResultsTableView]) {
        mo = self.results[indexPath.row];
    }
    
    if (![tableView isEqual:ResultsTableView]) {
        mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    mo_selected = mo;
    tableView_active = tableView;
    
    if (self.selectionList) {
        NSString *question_id = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"id"] ];
        [self removeObjectWithID:question_id indexPath:indexPath];
    }
    
    NSLog(@"%s : %@", __PRETTY_FUNCTION__, self.selectionList);

    BOOL status = (self.selectionList == 0) ? YES : NO;
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wo.saveButton setHidden:status];
    });
    
    return indexPath;
}

- (void)removeObjectWithID:(NSString *)question_id indexPath:(NSIndexPath *)indexPath {
    [self.selectionList removeObject:question_id];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
}

- (void)addObjectWithID:(NSString *)question_id indexPath:(NSIndexPath *)indexPath {
    [self.selectionList addObject:question_id];
    [self.tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
}

- (IBAction)selectAllAction:(id)sender {
    
    for (int i = 0; i < [self.tableView numberOfSections]; i++) {
        for (int j = 0; j < [self.tableView numberOfRowsInSection:i]; j++) {
            NSUInteger ints[2] = {i,j};
            
            NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:ints length:2];
            NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
            NSSet *choice_sets = [mo valueForKey:@"choices"];
            NSArray *choices = [choice_sets allObjects];
            
            NSString *question_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"id"] ];
            
                if (choices.count > 0) {
                    if (self.allIsSelected) {
                        [self removeObjectWithID:question_id indexPath:indexPath];
                    } else {
                        [self addObjectWithID:question_id indexPath:indexPath];
                    }
                }
        }
    }
    
    self.allIsSelected = (self.allIsSelected) ? NO : YES;
    BOOL status = (self.selectionList == 0) ? YES : NO;
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wo.saveButton setHidden:status];
    });
}

- (IBAction)sortAction:(id)sender {
    self.isAscending = (_isAscending) ? NO : YES;
    
    self.sort_descriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:self.isAscending];
    [self reloadFetchedResultsController];
}


#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionBankEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];

//    if (self.quiz_mo != nil) {
//        NSCompoundPredicate *predicate = [self filterResultsWithObject:self.quiz_mo];
//        [fetchRequest setPredicate:predicate];
//    }
    
    NSCompoundPredicate *predicate;
    if ([self.questionSearchTextField.text isEqualToString:@""]) {
        predicate = [self filterResultsWithObject:self.quiz_mo];
    } else {
        predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[[self predicateForKeyPathContains:@"search_string" value:self.questionSearchTextField.text]]];
    }
    
    [fetchRequest setPredicate:predicate];
    
    // Edit the sort key as appropriate.
//    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:self.isAscending];
//    [fetchRequest setSortDescriptors:@[date_modified]];
    
    [fetchRequest setSortDescriptors:@[self.sort_descriptor]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSCompoundPredicate *)filterResultsWithObject:(NSManagedObject *)mo {
    
    NSSet *list = [mo valueForKey:@"questions"];
    NSArray *questions = [NSArray arrayWithArray:[list allObjects] ];

    NSMutableArray *predicate_items = [NSMutableArray array];
    for (NSManagedObject *q in questions) {
        NSString *question_id = [NSString stringWithFormat:@"%@", [q valueForKey:@"question_id"] ];
        NSPredicate *p = [self predicateForKeyPath:@"id" value:question_id type:NSNotEqualToPredicateOperatorType];
        [predicate_items addObject:p];
    }
    
    NSCompoundPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates: predicate_items ];
    
    return predicate;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value type:(NSPredicateOperatorType)type {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:type
                                                                        options:options];
    return predicate;
}


- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Search Results Updating

//- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
//
//    UISearchBar *searchBar = searchController.searchBar;
//    
//    if (searchBar.text.length > 0) {
//        
//        NSString *text = searchBar.text;
//        
//        NSArray *fetchedObjects = self.fetchedResultsController.fetchedObjects;
//        
//        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(NSManagedObject *mo, NSDictionary *bindings) {
//            
//            NSString *object = [NSString stringWithFormat:@"%@", [mo valueForKey:@"search_string"]];
//            NSStringCompareOptions options = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;
//            NSRange range = [object rangeOfString:text options:options];
//            
//            return range.location != NSNotFound;
//        }];
//        
//        // Set up results.
//        self.results = [fetchedObjects filteredArrayUsingPredicate:predicate];;
//        
//        // Reload search table view.
//        [self.searchResultsTableViewController.tableView reloadData];
//    }
//}

- (void)updateSearch {
    [self reloadFetchedResultsController];
}

@end
