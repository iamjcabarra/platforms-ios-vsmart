//
//  TestGuruDashboard.m
//  V-Smart
//
//  Created by Ryan Migallos on 7/20/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruDashboard.h"
#import "TestGuruDashboardHeader.h"
#import "TestGuruDashboardGenericHeader.h"
#import "TestGuruDashBoardCell.h"
#import "TestGuruDataManager.h"

#import "VSmartValues.h"
#import "AccountInfo.h"
#import "Utils.h"
#import "MainHeader.h"

@interface TestGuruDashboard () <UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) TestGuruDataManager *tm;
@property (nonatomic, weak) TestGuruDashboardHeader *headerView;
@property (nonatomic, weak) TestGuruDashboardGenericHeader *genericHeaderView;

@end

@implementation TestGuruDashboard

static NSString * const kHeaderIdentifier = @"test_guru_header_view";
static NSString * const kGenericHeaderIdentifier = @"test_guru_generic_header_view";
static NSString * const kCellIdentifier = @"test_guru_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    self.title = NSLocalizedString(@"Test Guru", nil);
    NSString *menu1 = NSLocalizedString(@"Question", nil);
    NSString *menu2 = NSLocalizedString(@"Test", nil);
    NSString *menu3 = NSLocalizedString(@"Test Player", nil);
    
    // Do any additional setup after loading the view.
    self.items = @[
                       @{@"title":menu1,
                         @"type":@"questions_module",
                         @"image":@"add_question150"},
                       
                       @{@"title":menu2,
                         @"type":@"tests_module",
                         @"image":@"test150"},
                       
                       @{@"title":menu3,
                         @"type":@"deployedtest_module",
                         @"image":@"test_player150"}
                   ];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self customizeNavigationController];
    [self loadDashBoardStatistics];
}

- (void)customizeNavigationController {
    
    UIColor *color = UIColorFromHex(0xF69C42);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    } else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}


- (void)loadDashBoardStatistics {

    self.tm = [TestGuruDataManager sharedInstance];
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    dispatch_queue_t q = dispatch_queue_create("com.testguru.STATISTICS", DISPATCH_QUEUE_SERIAL);

    __weak typeof(self) wo = self;
    
    dispatch_async(q, ^{
//        [wo.tm requestListCountForUserWithID:user_id fromEndPoint:kEndPointTestGuruQuestionList listBlock:^(NSArray *list) {
//            if (list != nil) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    self.headerView.questionCount.text = [NSString stringWithFormat:@"%li", (unsigned long)list.count];
//                });
//            }
//        }];

        [wo.tm requestQuestionCountForUser:user_id dataBlock:^(NSDictionary *data) {
            if (data[@"questions"] != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.headerView.questionCount.text = [NSString stringWithFormat:@"%@", data[@"questions"] ];
                });
            }
        }];

    });        
    
    dispatch_async(q, ^{
//        [wo.tm requestListCountForUserWithID:user_id fromEndPoint:kEndPointTestGuruTestList listBlock:^(NSArray *list) {
//            if (list != nil) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    self.headerView.testCount.text = [NSString stringWithFormat:@"%li", (unsigned long)list.count];
//                });
//            }
//        }];

        [wo.tm requestTestCountForUser:user_id dataBlock:^(NSDictionary *data) {
            if (data[@"quiz"] != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.headerView.testCount.text = [NSString stringWithFormat:@"%@", data[@"quiz"] ];
                });
            }
        }];

    });

    dispatch_async(q, ^{
//        [wo.tm requestListCountForUserWithID:user_id fromEndPoint:kEndPointTestGuruTestList listBlock:^(NSArray *list) {
//            if (list != nil) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    self.headerView.deployCount.text = [NSString stringWithFormat:@"%li", (unsigned long)list.count];
//                });
//            }
//        }];

        [wo.tm requestDeployedTestCountForUser:user_id dataBlock:^(NSDictionary *data) {
            if (data[@"quiz"] != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.headerView.deployCount.text = [NSString stringWithFormat:@"%@", data[@"quiz"] ];
                });
            }
        }];

    });

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
// Get the new view controller using [segue destinationViewController].
// Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TestGuruDashBoardCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    NSDictionary *data = self.items[indexPath.row];
    NSString *title_string = [NSString stringWithFormat:@"%@", data[@"title"] ];
    NSString *image_string = [NSString stringWithFormat:@"%@", data[@"image"] ];
    
    // Configure the cell
    cell.testModuleTitle.text = title_string;
    cell.testModuleImageView.image = [UIImage imageNamed:image_string];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        self.headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                         withReuseIdentifier:kHeaderIdentifier
                                                                                forIndexPath:indexPath];
        reusableview = self.headerView;
    }
    
    return reusableview;
}

#pragma mark <UICollectionViewDelegate>

// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}

// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *data = self.items[indexPath.row];
    NSString *type = [NSString stringWithFormat:@"%@", data[@"type"] ];
    
    if ([type isEqualToString:@"questions_module"]) {
        [self performSegueWithIdentifier:@"V1_QUESTIONS_MODULE_TYPE" sender:self];
    }

    if ([type isEqualToString:@"tests_module"]) {
        [self performSegueWithIdentifier:@"V1_TEST_MODULE_TYPE" sender:self];
    }
    
    if ([type isEqualToString:@"deployedtest_module"]) {
        [self performSegueWithIdentifier:@"V1_DEPLOY_MODULE_TYPE" sender:self];
    }
}

#pragma mark <UICollectionViewDelegateFlowLayout>

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(30, 30, 30, 30);
}

@end
