//
//  TestBankDetailController.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/7/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestBankDetailController : UITableViewController

@property (nonatomic, assign) BOOL editMode;
@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSManagedObject *quiz_mo;

@end
