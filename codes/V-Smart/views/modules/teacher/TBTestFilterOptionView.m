//
//  TBTestFilterOptionView.m
//  V-Smart
//
//  Created by Julius Abarra on 02/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TBTestFilterOptionView.h"
#import "TBClassHelper.h"

@interface TBTestFilterOptionView () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) TBClassHelper *classHelper;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *filterOptions;

@end

static NSString *kCellIdentifier = @"testFilterOptionCellIdentifier";

@implementation TBTestFilterOptionView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Test Filter Option
    [self setUpTestFilterOptions];
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Test Grading Type List

- (void)setUpTestFilterOptions {
    NSString *option1 = [self.classHelper localizeString:@"All"];
    NSString *option2 = [self.classHelper localizeString:@"Graded"];
    NSString *option3 = [self.classHelper localizeString:@"Non-Graded"];
    
    self.filterOptions = @[@{@"option":option1, @"value":@"*"},
                           @{@"option":option2, @"value":@"1"},
                           @{@"option":option3, @"value":@"0"},
                           ];
}

- (void)setUpTestGradingTypeListTableView {
    // Set Protocols
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Allow Selection
    self.tableView.allowsSelection = YES;
    self.tableView.allowsMultipleSelection = NO;
    
    // Default Height
    self.tableView.rowHeight = 44.0f;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filterOptions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];
    }
    
    NSDictionary *d = [self.filterOptions objectAtIndex:indexPath.row];
    NSString *testFilterOption = [d[@"option"] uppercaseString];
    
    cell.textLabel.text = testFilterOption;
    return cell;
}

#pragma mark - Table Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *d = [self.filterOptions objectAtIndex:indexPath.row];
    
    NSString *testFilterOption = [d[@"option"] uppercaseString];
    NSString *testFilterValue = d[@"value"];
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
    [self.delegate selectedTestFilterOption:@{@"option":testFilterOption, @"value":testFilterValue}];
    }];
}

@end
