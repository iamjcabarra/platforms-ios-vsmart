//
//  TestGuruDynamicHeader.m
//  V-Smart
//
//  Created by Ryan Migallos on 23/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruDynamicHeader.h"
#import "TestGuruDynamicHeaderCell.h"
#import "TestGuruDynamicReusableView.h"
#import "TestGuruDataManager.h"

@interface TestGuruDynamicHeader () <UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, NSFetchedResultsControllerDelegate> {
    NSMutableDictionary *_objectChanges;
    NSMutableDictionary *_sectionChanges;
}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) TestGuruDataManager *tm;

@property (strong, nonatomic) IBOutlet UIButton *applyButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@property (strong, nonatomic) NSArray *items;

@property (strong, nonatomic) NSMutableSet *selectedFilterObjects;
@property (strong, nonatomic) NSMutableSet *deselectedFilterObjects;

@property (assign, nonatomic) CGFloat widthDivider;
@end

@implementation TestGuruDynamicHeader


dispatch_queue_t fetchQueue;

static NSString * const kCell = @"dynamic_header_cell_identifier";
static NSString * const kHeader = @"dynamic_header_reusableviewl_identifier";
CGFloat kCollectionHeight = 30;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tm = [TestGuruDataManager sharedInstance];
    
    [self title:@"Apply" forButton:self.applyButton];
    [self title:@"Cancel" forButton:self.cancelButton];
    
    [self initializeSets];
    
    [self.applyButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.resetButton addTarget:self action:@selector(resetButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSUInteger count = [[self.tm filterOptionsSettedTo:YES] count];
    if ([(NSObject*)self.delegate respondsToSelector:@selector(filterOptionCount:)]) {
        [self.delegate filterOptionCount:count];
    }
    
    [self checkIfHasFilterTypes];
    
        BOOL checkDelegate = [self.delegate isKindOfClass: NSClassFromString(@"TGTBAssignQuestionView")];
    
    self.widthDivider = (checkDelegate) ? 3 : 4;
}

- (void)checkIfHasFilterTypes {
    NSInteger count = [self.tm fetchCountForEntity:kTestGuruFilterOptions];
    if (count == 0) {
        
        dispatch_queue_t queue = dispatch_queue_create("com.vibetech.types.FILTER",DISPATCH_QUEUE_SERIAL);
        dispatch_group_t group = dispatch_group_create();
        __weak typeof(self) wo = self;
        
        dispatch_group_enter(group);
        dispatch_group_async(group, queue, ^{
            //GRADED | STAGE
            NSString *sectionTitle1 = NSLocalizedString(@"Question Types", nil);
            NSString *sectionOrder1 = @"0";
            [wo.tm requestFilterOptionType:@"questions/types" title:sectionTitle1 order:sectionOrder1 doneBlock:^(BOOL status1) {
                if (status1) {
                    dispatch_group_leave(group);
                }
            }];
        });
        
        dispatch_group_enter(group);
        dispatch_group_async(group, queue, ^{
            //GRADED | STAGE
            NSString *sectionTitle2 = NSLocalizedString(@"Difficulty Level", nil);
            NSString *sectionOrder2 = @"1";
            [wo.tm requestFilterOptionType:@"proficiency" title:sectionTitle2 order:sectionOrder2 doneBlock:^(BOOL status2) {
                if (status2) {
                    dispatch_group_leave(group);
                }
            }];
        });
        
        dispatch_group_enter(group);
        dispatch_group_async(group, queue, ^{
            //GRADED | STAGE
            NSString *sectionTitle3 = NSLocalizedString(@"Learning Skill", nil);
            NSString *sectionOrder3 = @"2";
            [wo.tm requestFilterOptionType:@"learning" title:sectionTitle3 order:sectionOrder3 doneBlock:^(BOOL status3) {
                if (status3) {
                    dispatch_group_leave(group);
                }
            }];
        });
        
        
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            [self buttonAction:self.applyButton];
        });
    }
}

- (void)initializeSets {
    self.selectedFilterObjects = [NSMutableSet setWithArray:[self.tm filterOptionsSettedTo:YES]];//[NSMutableSet set];
    self.deselectedFilterObjects = [NSMutableSet setWithArray:[self.tm filterOptionsSettedTo:NO]];
}

- (void)title:(NSString *)string forButton:(UIButton *)button {

    NSString *title = NSLocalizedString(string, nil);
    [button setTitle:title forState:UIControlStateNormal];
}

- (void)resetButtonAction:(UIButton *)sender {
//    NSArray *arrayUnselected = [self.tm filterOptionsSettedTo:NO];
    NSSet *copyOfDeselected = [NSSet setWithSet:self.deselectedFilterObjects];
    
    for (NSManagedObject *mo in copyOfDeselected) {
        [self addToSelected:mo];
    }
    
//    dispatch_async(dispatch_get_main_queue(), ^ {
        [self.collectionView reloadItemsAtIndexPaths:[self.collectionView indexPathsForVisibleItems]];
//    });
    
}

- (void)buttonAction:(UIButton *)sender {
    
    NSString *actionType = (sender == self.applyButton) ? @"APPLY" : @"CANCEL";
    
    if ([actionType isEqual:@"APPLY"]) {
        [self updateSelections];
        [self removeAllSelected];
        NSUInteger count = [[self.tm filterOptionsSettedTo:YES] count];
        if ([(NSObject*)self.delegate respondsToSelector:@selector(filterOptionCount:)]) {
            NSLog(@"COUNTTTTT [%lu]", (unsigned long)count);
            [self.delegate filterOptionCount:count];
        }
    }
    
    if ([actionType isEqual:@"CANCEL"]) {
        
        [self initializeSets];
    }
    
    if ([(NSObject*)self.delegate respondsToSelector:@selector(didSelectButtonAction:)]) {
        [self.delegate didSelectButtonAction:actionType];
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self.collectionView reloadItemsAtIndexPaths:[self.collectionView indexPathsForVisibleItems]];
        });
    }
}

- (void)removeAllSelected {
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    if ([def valueForKey:@"all_selected_test_guru_question_filter"] == nil) {
        [def setValue:@"1" forKey:@"all_selected_test_guru_question_filter"];
        [def synchronize];
    }
}

- (void)updateSelections {
    [self.tm updateFilterOptionObjects:self.selectedFilterObjects deselect:self.deselectedFilterObjects];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (NSManagedObject *)checkArrayOfObjects:(NSMutableSet *)set contains:(NSManagedObject *) mo {
    NSManagedObject *finalObject = nil;
    
    NSString *section_order = [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_order"] ];
    NSString *type_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"type_id"] ];
    
    
    NSPredicate *sectionOrderPredicate = [NSPredicate predicateWithFormat:@"section_order == %@", section_order];
    NSPredicate *typeIDPredicate = [NSPredicate predicateWithFormat:@"type_id == %@", type_id];
    //    NSCompoundPredicate = [NSCompoundPredicate a]
    NSCompoundPredicate *predicates = [NSCompoundPredicate andPredicateWithSubpredicates:@[sectionOrderPredicate, typeIDPredicate]];
    
    NSArray *filteredArray = [[set filteredSetUsingPredicate:predicates] allObjects];
    finalObject =  filteredArray.count > 0 ? filteredArray.firstObject : nil;
    
    //    for (NSManagedObject *object in set) {
    //
    //        NSLog(@"SECTION ORDER [%@] [%@], TYPE ID [%@] [%@]", [object valueForKey:@"section_order"], [mo valueForKey:@"section_order"], [object valueForKey:@"type_id"], [mo valueForKey:@"type_id"] );
    //
    //        if ([object valueForKey:@"section_order"] == [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_order"]]) {
    //            if ([object valueForKey:@"type_id"] == [NSString stringWithFormat:@"%@", [mo valueForKey:@"type_id"]]) {
    //                finalObject = object;
    //            }
    //        }
    //    }

    
    return finalObject;
}

- (void)addToSelected:(NSManagedObject *)mo {
    [self.selectedFilterObjects addObject:mo];
    
    NSManagedObject *object = [self checkArrayOfObjects:self.deselectedFilterObjects contains:mo];
    if (object != nil) {
        [self.deselectedFilterObjects removeObject:object];
    }
    
//    for (NSManagedObject *deselectedObject in self.deselectedFilterObjects) {
//        if ([deselectedObject valueForKey:@"section_order"] == [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_order"]]) {
//            if ([deselectedObject valueForKey:@"type_id"] == [NSString stringWithFormat:@"%@", [mo valueForKey:@"type_id"]]) {
//                [self.deselectedFilterObjects removeObject:deselectedObject];
//                break;
//            }
//        }
//    }
}

- (void)removeFromSelected:(NSManagedObject *)mo {
    [self.deselectedFilterObjects addObject:mo];
    
    NSManagedObject *object = [self checkArrayOfObjects:self.selectedFilterObjects contains:mo];
    if (object != nil) {
        [self.selectedFilterObjects removeObject:object];
    }
    
//    for (NSManagedObject *selectedObject in self.selectedFilterObjects) {
//        if ([selectedObject valueForKey:@"section_order"] == [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_order"]]) {
//            if ([selectedObject valueForKey:@"type_id"] == [NSString stringWithFormat:@"%@", [mo valueForKey:@"type_id"]]) {
//                [self.selectedFilterObjects removeObject:selectedObject];
//                break;
//            }
//        }
//    }
}

- (BOOL)selectRowZero:(NSIndexPath *)indexPath {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][indexPath.section];
    NSArray *sectionObjects = [sectionInfo objects];
    NSInteger countValue = 0;
    for (NSManagedObject *selectedObject in self.selectedFilterObjects) {
        if ([selectedObject valueForKey:@"section_order"] == [NSString stringWithFormat:@"%ld", (long)indexPath.section]) {
            for (NSManagedObject *object in sectionObjects) {
                if ([[selectedObject valueForKey:@"type_id"] isEqual:[object valueForKey:@"type_id"]]) {
                    countValue++;
                }
            }
        }
    }
    
    if (countValue >= ([self.collectionView numberOfItemsInSection:indexPath.section] - 1)) {
        return YES;
    }
    return NO;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"-------> %s", __PRETTY_FUNCTION__);
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TestGuruDynamicHeaderCell *cell = (TestGuruDynamicHeaderCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.checkboxButton.selected = (cell.checkboxButton.selected) ? NO : YES;

    BOOL selected = cell.checkboxButton.selected;

    if (indexPath.row == 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][indexPath.section];
        NSArray *sectionObjects = [sectionInfo objects];
        for (NSManagedObject *object in sectionObjects) {
            if (selected) {
                [self addToSelected:object];
            } else {
                [self removeFromSelected:object];
            }
        }
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
    } else {
        NSIndexPath *rowZeroIndexPath = [NSIndexPath indexPathForRow:0 inSection:indexPath.section];
        NSManagedObject *rowZeroObject = [self.fetchedResultsController objectAtIndexPath:rowZeroIndexPath];
        TestGuruDynamicHeaderCell *rowZeroCell = (TestGuruDynamicHeaderCell *)[collectionView cellForItemAtIndexPath:rowZeroIndexPath];
        
        if (selected) {
            [self addToSelected:mo];
            if ([self selectRowZero:indexPath]) {
                if (rowZeroCell.checkboxButton.selected == NO) {
                    rowZeroCell.checkboxButton.selected = YES;
                    [self addToSelected:rowZeroObject];
                }
            }
        } else {
            [self removeFromSelected:mo];
            if (rowZeroCell.checkboxButton.selected) {
                rowZeroCell.checkboxButton.selected = NO;
                [self removeFromSelected:rowZeroObject];
            }
        }
    }
}

- (BOOL)checkArray:(NSMutableSet *)set contains:(NSManagedObject *) mo {
    BOOL status = NO;
    
    for (NSManagedObject *object in set) {
        if ([object valueForKey:@"section_order"] == [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_order"]]) {
            if ([object valueForKey:@"type_id"] == [NSString stringWithFormat:@"%@", [mo valueForKey:@"type_id"]]) {
                status = YES;
            }
        }
    }
    
    return status;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    NSInteger count = [[self.fetchedResultsController sections] count];
    NSLog(@"%s count : %ld", __PRETTY_FUNCTION__, (long)count );
    
//    NSInteger count = 0;
//    if (self.items.count > 0) {
//        count = [self.items count];
//    }
    return count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
    
//    NSInteger count = 0;
//    
//    if ([self.items count] > 0) {
//        NSDictionary *object = self.items[section];
//        NSArray *rows = object[@"list"];
//        count = [rows count];
//    }
//    
//    NSLog(@"%s row count : %ld",__PRETTY_FUNCTION__, (long)count);
//    
//    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCell forIndexPath:indexPath];
    
    [self configureCell:cell indexPath:indexPath];
    return cell;
}

- (void)configureCell:(UICollectionViewCell *)c indexPath:(NSIndexPath *)indexPath {
    
    TestGuruDynamicHeaderCell *cell = (TestGuruDynamicHeaderCell *)c;
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *title = [NSString stringWithFormat:@"%@", [mo valueForKey:@"type_value"] ];
    BOOL selected = [(NSNumber *)[mo valueForKey:@"selected"] boolValue];

//    if ([self.selectedFilterObjects containsObject: mo]) {
//        selected = YES;
//    }
    
    if ([self checkArrayOfObjects:self.selectedFilterObjects contains:mo]) {
        selected = YES;
    }
    
//    if ([self.deselectedFilterObjects containsObject:mo]) {
//        selected = NO;
//    }
    
    if ([self checkArrayOfObjects:self.deselectedFilterObjects contains:mo]) {
        selected = NO;
    }

    
    cell.titleLabel.text = title;
    cell.checkboxButton.selected = selected;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];

//    TestGuruDynamicReusableView *sectionView = nil;
    
    TestGuruDynamicReusableView *sectionView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                                  withReuseIdentifier:kHeader
                                                                                         forIndexPath:indexPath];
        sectionView.titleLabel.text = @"";
    
//    if (kind == UICollectionElementKindSectionHeader) {
    
        NSString *headerString = [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_title"] ];
        sectionView.titleLabel.text = headerString;
        
//        return sectionView;
//    }
    
    return sectionView;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
//    CGFloat spacer = (collectionView.frame.size.width / 2);
    return 0;
}

//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
//    
//    return 20;
//}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = (collectionView.frame.size.width / self.widthDivider);
    width = width - 10;
    return CGSizeMake(width, kCollectionHeight);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    CGFloat width = collectionView.frame.size.width;
    CGSize size = CGSizeMake(width, 35);
    
    return size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(0, 10, 0, 0);
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
//    
//    CGFloat width = collectionView.frame.size.width;
//    
//    CGSize size = CGSizeMake(width, 0);
//    
//    if (section > 0) {
//        size = CGSizeMake(width, kHeight);
//    }
//    
//    return size;
//}

#pragma mark <NSFetchedResultsControllerDelegate>

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }

    // Object Context
    NSManagedObjectContext *context = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTestGuruFilterOptions];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
//    NSSortDescriptor *type_order = [NSSortDescriptor sortDescriptorWithKey:@"type_order" ascending:YES];
    NSSortDescriptor *section_order = [NSSortDescriptor sortDescriptorWithKey:@"section_order" ascending:YES];
    NSSortDescriptor *type_id = [NSSortDescriptor sortDescriptorWithKey:@"type_id" ascending:YES];
    
//    NSSortDescriptor *date_order = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
    [fetchRequest setSortDescriptors:@[section_order,type_id]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:context
                                                                            sectionNameKeyPath:@"section_order"
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    _objectChanges = [NSMutableDictionary dictionary];
    _sectionChanges = [NSMutableDictionary dictionary];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    if (type == NSFetchedResultsChangeInsert || type == NSFetchedResultsChangeDelete) {
        NSMutableIndexSet *changeSet = _sectionChanges[@(type)];
        if (changeSet != nil) {
            [changeSet addIndex:sectionIndex];
        } else {
            _sectionChanges[@(type)] = [[NSMutableIndexSet alloc] initWithIndex:sectionIndex];
        }
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    NSMutableArray *changeSet = _objectChanges[@(type)];
    if (changeSet == nil) {
        changeSet = [[NSMutableArray alloc] init];
        _objectChanges[@(type)] = changeSet;
    }
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [changeSet addObject:newIndexPath];
            break;
        case NSFetchedResultsChangeDelete:
            [changeSet addObject:indexPath];
            break;
        case NSFetchedResultsChangeUpdate:
            [changeSet addObject:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [changeSet addObject:@[indexPath, newIndexPath]];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    NSMutableArray *moves = _objectChanges[@(NSFetchedResultsChangeMove)];
    if (moves.count > 0) {
        NSMutableArray *updatedMoves = [[NSMutableArray alloc] initWithCapacity:moves.count];
        
        NSMutableIndexSet *insertSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        NSMutableIndexSet *deleteSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        for (NSArray *move in moves) {
            NSIndexPath *fromIP = move[0];
            NSIndexPath *toIP = move[1];
            
            if ([deleteSections containsIndex:fromIP.section]) {
                if (![insertSections containsIndex:toIP.section]) {
                    NSMutableArray *changeSet = _objectChanges[@(NSFetchedResultsChangeInsert)];
                    if (changeSet == nil) {
                        changeSet = [[NSMutableArray alloc] initWithObjects:toIP, nil];
                        _objectChanges[@(NSFetchedResultsChangeInsert)] = changeSet;
                    } else {
                        [changeSet addObject:toIP];
                    }
                }
            } else if ([insertSections containsIndex:toIP.section]) {
                NSMutableArray *changeSet = _objectChanges[@(NSFetchedResultsChangeDelete)];
                if (changeSet == nil) {
                    changeSet = [[NSMutableArray alloc] initWithObjects:fromIP, nil];
                    _objectChanges[@(NSFetchedResultsChangeDelete)] = changeSet;
                } else {
                    [changeSet addObject:fromIP];
                }
            } else {
                [updatedMoves addObject:move];
            }
        }
        
        if (updatedMoves.count > 0) {
            _objectChanges[@(NSFetchedResultsChangeMove)] = updatedMoves;
        } else {
            [_objectChanges removeObjectForKey:@(NSFetchedResultsChangeMove)];
        }
    }
    
    NSMutableArray *deletes = _objectChanges[@(NSFetchedResultsChangeDelete)];
    if (deletes.count > 0) {
        NSMutableIndexSet *deletedSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        [deletes filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSIndexPath *evaluatedObject, NSDictionary *bindings) {
            return ![deletedSections containsIndex:evaluatedObject.section];
        }]];
    }
    
    NSMutableArray *inserts = _objectChanges[@(NSFetchedResultsChangeInsert)];
    if (inserts.count > 0) {
        NSMutableIndexSet *insertedSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        [inserts filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSIndexPath *evaluatedObject, NSDictionary *bindings) {
            return ![insertedSections containsIndex:evaluatedObject.section];
        }]];
    }
    
    UICollectionView *cv = self.collectionView;
    
    [cv performBatchUpdates:^{
        NSIndexSet *deletedSections = _sectionChanges[@(NSFetchedResultsChangeDelete)];
        if (deletedSections.count > 0) {
            [cv deleteSections:deletedSections];
        }
        
        NSIndexSet *insertedSections = _sectionChanges[@(NSFetchedResultsChangeInsert)];
        if (insertedSections.count > 0) {
            [cv insertSections:insertedSections];
        }
        
        NSArray *deletedItems = _objectChanges[@(NSFetchedResultsChangeDelete)];
        if (deletedItems.count > 0) {
            [cv deleteItemsAtIndexPaths:deletedItems];
        }
        
        NSArray *insertedItems = _objectChanges[@(NSFetchedResultsChangeInsert)];
        if (insertedItems.count > 0) {
            [cv insertItemsAtIndexPaths:insertedItems];
        }
        
        NSArray *reloadItems = _objectChanges[@(NSFetchedResultsChangeUpdate)];
        if (reloadItems.count > 0) {
            [cv reloadItemsAtIndexPaths:reloadItems];
        }
        
        NSArray *moveItems = _objectChanges[@(NSFetchedResultsChangeMove)];
        for (NSArray *paths in moveItems) {
            [cv moveItemAtIndexPath:paths[0] toIndexPath:paths[1]];
        }
    } completion:nil];
    
    _objectChanges = nil;
    _sectionChanges = nil;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [self.collectionView performBatchUpdates:nil completion:nil];
}

@end
