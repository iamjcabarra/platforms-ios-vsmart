//
//  TGQBTagCompetencyMenu.h
//  V-Smart
//
//  Created by Ryan Migallos on 11/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TGQBTagCompetencyMenuDelegate <NSObject>
@optional
- (void)willShowCompetencyView;
@end

@interface TGQBTagCompetencyMenu : UITableViewCell
@property (nonatomic, weak) id <TGQBTagCompetencyMenuDelegate> delegate;
- (void)displayTags:(NSSet *)tagSet;
- (void)updateCompetencyCodeValue:(NSString *)string;
- (void)displayCompetencyCodeValue:(NSString *)string;
- (NSString *)getTagValues;
- (NSSet *)getTagSet;

- (void)setObjectData:(NSManagedObject *)object;

@end
