//
//  TestSettingsView.m
//  V-Smart
//
//  Created by Julius Abarra on 14/01/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "TestSettingsView.h"
#import "TestSettingsReusableController.h"
#import "TestGuruDataManager.h"
#import "TestContentManager.h"

@interface TestSettingsView () <TestSettingsTestPropertyPickerDelegate>

@property (strong, nonatomic) TestContentManager *sharedTestContentManager;
@property (strong, nonatomic) TestGuruDataManager *tgdm;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) TestSettingsReusableController *settingsReusableView;
@property (strong, nonatomic) UIPopoverController *reusablePopOverController;

@property (strong, nonatomic) IBOutlet UIView *rightView;
@property (strong, nonatomic) IBOutlet UIView *leftView;

@property (strong, nonatomic) IBOutlet UILabel *categoryLabel;
@property (strong, nonatomic) IBOutlet UILabel *resultTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *testTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *levelOfDifficultyLabel;
@property (strong, nonatomic) IBOutlet UILabel *learningSkillLabel;

@property (strong, nonatomic) IBOutlet UITextField *categoryTextField;
@property (strong, nonatomic) IBOutlet UITextField *resultTypeTextField;
@property (strong, nonatomic) IBOutlet UITextField *testTypeTextField;
@property (strong, nonatomic) IBOutlet UITextField *levelOfDifficultyTextField;
@property (strong, nonatomic) IBOutlet UITextField *learningSkillTextField;

@property (strong, nonatomic) IBOutlet UILabel *optionsLabel;
@property (strong, nonatomic) IBOutlet UILabel *shuffleAnswerLabel;
@property (strong, nonatomic) IBOutlet UILabel *shuffleQuestionLabel;
@property (strong, nonatomic) IBOutlet UILabel *showFeedbackLabel;
@property (strong, nonatomic) IBOutlet UILabel *allowReviewLabel;
@property (strong, nonatomic) IBOutlet UILabel *forcedCompleteLabel;

@property (strong, nonatomic) IBOutlet UISwitch *shuffleAnswerSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *shuffleQuestionSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *showFeedbackSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *allowReviewSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *forcedCompleteSwitch;

@property (strong, nonatomic) IBOutlet UIButton *categoryButton;
@property (strong, nonatomic) IBOutlet UIButton *resultTypeButton;
@property (strong, nonatomic) IBOutlet UIButton *testTypeButton;
@property (strong, nonatomic) IBOutlet UIButton *levelOfDifficultyButton;
@property (strong, nonatomic) IBOutlet UIButton *learningSkillButton;

@property (assign, nonatomic) NSInteger selectedTestPropertyButtonTag;

@end

@implementation TestSettingsView

#define kRightColorNCG UIColorFromHex(0x3498DB)
#define kWrongColorNCG UIColorFromHex(0xFF6666)
#define kRightColorYCG UIColorFromHex(0x3498DB).CGColor
#define kWrongColorYCG UIColorFromHex(0xFF6666).CGColor

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sharedTestContentManager = [TestContentManager sharedInstance];
    
    self.tgdm = [TestGuruDataManager sharedInstance];
    self.managedObjectContext = self.tgdm.mainContext;
    
    // Localize strings
    [self setUpStringLocalization];
    
    // Customize options view
    self.rightView.layer.borderColor = kRightColorYCG;
    self.rightView.layer.borderWidth = 1.0f;
    
    // Customize text fields
    [self customizeTextFields];
    
    // Customize buttons
    [self customizeButtons];
    
    // Customize switches
    [self customizeSwitches];
    
    // Default values
    [self setUpDefaultValues];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Text Fields

- (void)customizeTextFields {
    // Border color
    self.categoryTextField.layer.borderColor = kRightColorYCG;
    self.resultTypeTextField.layer.borderColor = kRightColorYCG;
    self.testTypeTextField.layer.borderColor = kRightColorYCG;
    self.levelOfDifficultyTextField.layer.borderColor = kRightColorYCG;
    self.learningSkillTextField.layer.borderColor = kRightColorYCG;
    
    // Border width
    self.categoryTextField.layer.borderWidth = 1.0f;
    self.resultTypeTextField.layer.borderWidth = 1.0f;
    self.testTypeTextField.layer.borderWidth = 1.0f;
    self.levelOfDifficultyTextField.layer.borderWidth = 1.0f;
    self.learningSkillTextField.layer.borderWidth = 1.0f;
    
    // Disable text fields with popup
    self.categoryTextField.enabled = NO;
    self.resultTypeTextField.enabled = NO;
    self.testTypeTextField.enabled = NO;
    self.levelOfDifficultyTextField.enabled = NO;
    self.learningSkillTextField.enabled = NO;
}

#pragma mark - Custom Buttons

- (void)customizeButtons {
    // Set button tags
    self.categoryButton.tag = 100;
    self.resultTypeButton.tag = 200;
    self.testTypeButton.tag = 300;
    self.levelOfDifficultyButton.tag = 400;
    self.learningSkillButton.tag = 500;
    
    // Actions for buttons
    [self.categoryButton addTarget:self
                            action:@selector(showReusablePopOver:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    [self.resultTypeButton addTarget:self
                              action:@selector(showReusablePopOver:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self.testTypeButton addTarget:self
                            action:@selector(showReusablePopOver:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    [self.levelOfDifficultyButton addTarget:self
                                     action:@selector(showReusablePopOver:)
                           forControlEvents:UIControlEventTouchUpInside];
    
    [self.learningSkillButton addTarget:self
                                 action:@selector(showReusablePopOver:)
                       forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Custom Switches

- (void)customizeSwitches {
    // Set switch tags
    self.shuffleAnswerSwitch.tag = 100;
    self.shuffleQuestionSwitch.tag = 200;
    self.showFeedbackSwitch.tag = 300;
    self.allowReviewSwitch.tag = 400;
    self.forcedCompleteSwitch.tag = 500;
    
    // Actions for switches
    [self.shuffleAnswerSwitch addTarget:self
                                 action:@selector(changeSwitch:)
                       forControlEvents:UIControlEventValueChanged];
    
    [self.shuffleQuestionSwitch addTarget:self
                                   action:@selector(changeSwitch:)
                         forControlEvents:UIControlEventValueChanged];
    
    [self.showFeedbackSwitch addTarget:self
                                action:@selector(changeSwitch:)
                      forControlEvents:UIControlEventValueChanged];
    
    [self.allowReviewSwitch addTarget:self
                               action:@selector(changeSwitch:)
                     forControlEvents:UIControlEventValueChanged];
    
    [self.forcedCompleteSwitch addTarget:self
                                  action:@selector(changeSwitch:)
                        forControlEvents:UIControlEventValueChanged];
}

#pragma mark - String Localization

- (void)setUpStringLocalization {
    // Rigth view
    self.categoryLabel.text = [[self localizeString:@"Category"] uppercaseString];
    self.resultTypeLabel.text = [[self localizeString:@"Result Type"] uppercaseString];
    self.testTypeLabel.text = [[self localizeString:@"Test Type"] uppercaseString];
    self.levelOfDifficultyLabel.text = [[self localizeString:@"Level of Difficulty"] uppercaseString];
    self.learningSkillLabel.text = [[self localizeString:@"Learning Skill"] uppercaseString];
    
    // Left view
    self.shuffleAnswerLabel.text = [self localizeString:@"Shuffle Answer"];
    self.shuffleQuestionLabel.text = [self localizeString:@"Shuffle Question"];
    self.showFeedbackLabel.text = [self localizeString:@"Show Feedback"];
    self.allowReviewLabel.text = [self localizeString:@"Allow Review"];
    self.forcedCompleteLabel.text = [self localizeString:@"Forced Complete"];
}

- (NSString *)localizeString:(NSString *)string {
    NSString *localized = NSLocalizedString(string, nil);
    return localized;
}

#pragma mark - Default Values

- (void)setUpDefaultValues {
    BOOL isNotEmpty = self.sharedTestContentManager.testSettings.count > 0;
    
    if (!isNotEmpty) {
        // Access test properties
        NSDictionary *d1 = [self getTestPropertyObjectForEntity:@"TestCategory"];
        NSDictionary *d2 = [self getTestPropertyObjectForEntity:@"TestResultType"];
        NSDictionary *d3 = [self getTestPropertyObjectForEntity:@"TestType"];
        NSDictionary *d4 = [self getTestPropertyObjectForEntity:@"TestDifficultyLevel"];
        NSDictionary *d5 = [self getTestPropertyObjectForEntity:@"TestLearningSkill"];
        
        // Set values of text fields
        self.categoryTextField.text = [self stringValue:d1[@"value"]];
        self.resultTypeTextField.text = [self stringValue:d2[@"value"]];
        self.testTypeTextField.text = [self stringValue:d3[@"value"]];
        self.levelOfDifficultyTextField.text = [self stringValue:d4[@"value"]];
        self.learningSkillTextField.text = [self stringValue:d5[@"value"]];
        
        // Set states of switches
        self.shuffleAnswerSwitch.on = NO;
        self.shuffleQuestionSwitch.on = NO;
        self.showFeedbackSwitch.on = NO;
        self.allowReviewSwitch.on = YES;
        self.forcedCompleteSwitch.on = NO;
        
        // Update Content Manager: Test Properties
        [self.sharedTestContentManager.testSettings setObject:d1 forKey:@"test_category"];
        [self.sharedTestContentManager.testSettings setObject:d2 forKey:@"test_result_type"];
        [self.sharedTestContentManager.testSettings setObject:d3 forKey:@"test_type"];
        [self.sharedTestContentManager.testSettings setObject:d4 forKey:@"test_difficulty_level"];
        [self.sharedTestContentManager.testSettings setObject:d5 forKey:@"test_learning_skill"];
        
        // Update Content Manager: Test Options
        [self.sharedTestContentManager.testSettings setObject:@"0" forKey:@"is_shuffle_answers"];
        [self.sharedTestContentManager.testSettings setObject:@"0" forKey:@"quiz_shuffling_mode"];
        [self.sharedTestContentManager.testSettings setObject:@"0" forKey:@"show_feedbacks"];
        [self.sharedTestContentManager.testSettings setObject:@"1" forKey:@"allow_review"];
        [self.sharedTestContentManager.testSettings setObject:@"0" forKey:@"is_forced_complete"];
        
        // For test changes made mapping
        NSDictionary *userInfo = @{@"testSection":@"testSettings", @"testData":self.sharedTestContentManager.testSettings};
        [self notifyTestForTestCreationPreEntries:userInfo];
        [self notifyTestForActionPreparation];
    }
    
    if (isNotEmpty) {
        // Access test properties
        NSDictionary *d1 = [self.sharedTestContentManager.testSettings objectForKey:@"test_category"];
        NSDictionary *d2 = [self.sharedTestContentManager.testSettings objectForKey:@"test_result_type"];
        NSDictionary *d3 = [self.sharedTestContentManager.testSettings objectForKey:@"test_type"];
        NSDictionary *d4 = [self.sharedTestContentManager.testSettings objectForKey:@"test_difficulty_level"];
        NSDictionary *d5 = [self.sharedTestContentManager.testSettings objectForKey:@"test_learning_skill"];
        
        // Set values of text fields
        self.categoryTextField.text = [self stringValue:d1[@"value"]];
        self.resultTypeTextField.text = [self stringValue:d2[@"value"]];
        self.testTypeTextField.text = [self stringValue:d3[@"value"]];
        self.levelOfDifficultyTextField.text = [self stringValue:d4[@"value"]];
        self.learningSkillTextField.text = [self stringValue:d5[@"value"]];
        
        // Set states of switches
        BOOL shuffleAnswer = [[self.sharedTestContentManager.testSettings objectForKey:@"is_shuffle_answers"] boolValue];
        BOOL shuffleQuestion = [[self.sharedTestContentManager.testSettings objectForKey:@"quiz_shuffling_mode"] boolValue];
        BOOL showFeedback = [[self.sharedTestContentManager.testSettings objectForKey:@"show_feedbacks"] boolValue];
        BOOL allowReview = [[self.sharedTestContentManager.testSettings objectForKey:@"allow_review"] boolValue];
        BOOL forcedComplete = [[self.sharedTestContentManager.testSettings objectForKey:@"is_forced_complete"] boolValue];
        
        self.shuffleAnswerSwitch.on = shuffleAnswer;
        self.shuffleQuestionSwitch.on = shuffleQuestion;
        self.showFeedbackSwitch.on = showFeedback;
        self.allowReviewSwitch.on = allowReview;
        self.forcedCompleteSwitch.on = forcedComplete;
    }
}

- (NSDictionary *)getTestPropertyObjectForEntity:(NSString *)entityName {
    NSManagedObject *mo = [self.tgdm getObjectForEntity:entityName withSortDescriptor:@"id" andPredicate:nil];
    NSDictionary *d = @{@"id":@"", @"value":@""};
    
    if (mo != nil) {
        NSString *testPropertyID = [self stringValue:[mo valueForKey:@"id"]];
        NSString *testPropertyValue = [self stringValue:[mo valueForKey:@"value"]];
        d = @{@"id":testPropertyID, @"value":testPropertyValue};
    }
    
    return d;
}

#pragma mark - Get String Value

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

#pragma mark - Reusable Popover

- (void)showReusablePopOver:(id)sender {
    UIButton *button = (UIButton *)sender;
    self.selectedTestPropertyButtonTag = button.tag;
    
    // Create test settings reusable controller
    self.settingsReusableView = [[TestSettingsReusableController alloc] initWithNibName:@"TestSettingsReusableView" bundle:nil];
    self.settingsReusableView.delegate = self;
    self.settingsReusableView.selectedTestProperty = self.selectedTestPropertyButtonTag;
    
    // Create test settings popover
    self.reusablePopOverController = [[UIPopoverController alloc] initWithContentViewController:self.settingsReusableView];
    self.reusablePopOverController.popoverContentSize = CGSizeMake(320.0f, 235.0f);
    
    // Render popover
    [self.reusablePopOverController presentPopoverFromRect:button.bounds
                                                    inView:button
                                  permittedArrowDirections:UIPopoverArrowDirectionDown
                                                  animated:YES];
}

#pragma mark - Change Switch

- (void)changeSwitch:(id)sender {
    UISwitch *theSwitch = (UISwitch *)sender;
    NSInteger switchTag = theSwitch.tag;
    
    BOOL switchState = [theSwitch isOn];
    NSString *stateString = @"0";
    
    if (switchState) {
        stateString = @"1";
    }
    
    // Shuffle answer
    if (switchTag == 100) {
        [self.sharedTestContentManager.testSettings setObject:stateString forKey:@"is_shuffle_answers"];
    }
    
    // Shuffle question
    if (switchTag == 200) {
        [self.sharedTestContentManager.testSettings setObject:stateString forKey:@"quiz_shuffling_mode"];
    }
    
    // Show feedback
    if (switchTag == 300) {
        [self.sharedTestContentManager.testSettings setObject:stateString forKey:@"show_feedbacks"];
    }
    
    // Allow review
    if (switchTag == 400) {
        [self.sharedTestContentManager.testSettings setObject:stateString forKey:@"allow_review"];
    }
    
    // Forced complete
    if (switchTag == 500) {
        [self.sharedTestContentManager.testSettings setObject:stateString forKey:@"is_forced_complete"];
    }
    
    [self notifyTestForActionPreparation];
}

#pragma mark - Test Settings Test Property Delegate

- (void)selectedTestProperty:(NSDictionary *)d {
    NSString *tagString = @"";
    NSString *testPropertyName = [self stringValue:d[@"value"]];
    
    // Category
    if (self.selectedTestPropertyButtonTag == 100) {
        self.categoryTextField.text = testPropertyName;
        tagString = @"test_category";
    }
    // Result type
    if (self.selectedTestPropertyButtonTag == 200) {
        self.resultTypeTextField.text = testPropertyName;
        tagString = @"test_result_type";
    }
    // Test type
    if (self.selectedTestPropertyButtonTag == 300) {
        self.testTypeTextField.text = testPropertyName;
        tagString = @"test_type";
    }
    // Level of difficulty
    if (self.selectedTestPropertyButtonTag == 400) {
        self.levelOfDifficultyTextField.text = testPropertyName;
        tagString = @"test_difficulty_level";
    }
    // Learning skill
    if (self.selectedTestPropertyButtonTag == 500) {
        self.learningSkillTextField.text = testPropertyName;
        tagString = @"test_learning_skill";
    }
    
    // Update test conten manager for test property
    if (![tagString isEqualToString:@""]) {
        [self.sharedTestContentManager.testSettings setObject:d forKey:tagString];
    }
    
    [self notifyTestForActionPreparation];
}

#pragma mark - Test Notifications

- (void)notifyTestForTestCreationPreEntries:(NSDictionary *)d {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setPreEntriesForTestCreation" object:self userInfo:d];
}

- (void)notifyTestForActionPreparation {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"prepareToSaveOrUpdateTest" object:nil];
}

@end