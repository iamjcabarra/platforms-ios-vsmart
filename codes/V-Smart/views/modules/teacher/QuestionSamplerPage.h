//
//  QuestionSamplerPage.h
//  V-Smart
//
//  Created by Ryan Migallos on 29/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionSamplerPageHeader.h"
#import <AssetsLibrary/AssetsLibrary.h>

@protocol QuestionSamplerPageDelegate <NSObject>
@required
- (void)transitionWithData:(NSDictionary *)previewData index:(NSInteger)index segueID:(NSString *)segueIdentifier;
@end

@interface QuestionSamplerPage : UICollectionViewCell

- (void)displayObject:(NSManagedObject *)object;


@property (nonatomic, weak) id <QuestionSamplerPageDelegate> delegate;

@property (weak, nonatomic) IBOutlet QuestionSamplerPageHeader *headerView;

@property (weak, nonatomic) IBOutlet UIView *separatorView;


//@property (weak, nonatomic) IBOutlet UIButton *saveButton;
//@property (weak, nonatomic) IBOutlet UIButton *soloEditButton;
//@property (weak, nonatomic) IBOutlet UIButton *editButton;
//@property (weak, nonatomic) IBOutlet UIView *saveEditView;


//typedef void (^ALAssetsLibraryAssetForURLResultBlock)(ALAsset *asset);
//typedef void (^ALAssetsLibraryAccessFailureBlock)(NSError *error);

@end
