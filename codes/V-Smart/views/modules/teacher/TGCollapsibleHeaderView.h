//
//  TGCollapsibleHeaderView.h
//  V-Smart
//
//  Created by Ryan Migallos on 08/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGCollapsibleHeaderView : UITableViewController
@property (strong, nonatomic) NSString *package_type_id;
@property (strong, nonatomic) NSManagedObject *mo;
- (NSDictionary *)getUserValues;

@end
