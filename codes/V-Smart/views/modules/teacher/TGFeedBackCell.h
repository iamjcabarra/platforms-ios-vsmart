//
//  TGFeedBackCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 08/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGFeedBackCell : UITableViewCell

@property (strong, nonatomic) NSString *generalFeedback;
@property (strong, nonatomic) NSString *correctAnswerFeedback;
@property (strong, nonatomic) NSString *wrongAnswerFeedback;

- (void)resizeContents;

@end
