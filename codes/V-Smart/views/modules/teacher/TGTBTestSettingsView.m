//
//  TGTBTestSettingsView.m
//  V-Smart
//
//  Created by Julius Abarra on 07/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGTBTestSettingsView.h"
#import "TestGuruDataManager.h"
#import "TBTestOptionInfoView.h"

@interface TGTBTestSettingsView () <UITextFieldDelegate, UITableViewDelegate>

@property (strong, nonatomic) TestGuruDataManager *tm;
@property (strong, nonatomic) NSString *test_id;

// LOCALIZED
@property (strong, nonatomic) IBOutlet UILabel *optionLabel;
@property (strong, nonatomic) IBOutlet UILabel *shuffleChoicesLabel;
@property (strong, nonatomic) IBOutlet UILabel *shuffleQuestionsLabel;
@property (strong, nonatomic) IBOutlet UILabel *forcedScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *showScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *showResultLabel;
@property (strong, nonatomic) IBOutlet UILabel *withCorrectAnswerLabel;
@property (strong, nonatomic) IBOutlet UILabel *showFeedbackMessagesLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UILabel *enterPasswordLabel;

@property (strong, nonatomic) IBOutlet UISwitch *shuffleChoicesSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *shuffleQuestionsSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *forcedScoreSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *showScoreSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *showResultSwitch;
@property (strong, nonatomic) IBOutlet UIButton *withCorrectAnswerButton;
@property (strong, nonatomic) IBOutlet UISwitch *showFeedbackMessagesSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *passwordSwitch;
@property (strong, nonatomic) IBOutlet UITextField *enterPasswordField;


@property (weak, nonatomic) IBOutlet UIButton *shuffleChoicesInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *shuffleQuestionsInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *forcedScoreInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *showScoreInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *showResultsInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *showCorrectAnswerInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *showFeedbackInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *setPasswordInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *showPassword;

@property (strong, nonatomic) TBTestOptionInfoView *optionInfoView;
@property (strong, nonatomic) UIPopoverController *optionInfoPopOverController;

@end

@implementation TGTBTestSettingsView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tm = [TestGuruDataManager sharedInstance];
    self.test_id = [self stringValue:self.testObject key:@"id"];
    NSLog(@"---------------- %s GUID: %@", __PRETTY_FUNCTION__, self.test_id);
    
    self.optionInfoView = [[TBTestOptionInfoView alloc] initWithNibName:@"TBTestOptionInfoView" bundle:nil];
    // Create Popover Controller
    self.optionInfoPopOverController = [[UIPopoverController alloc] initWithContentViewController:self.optionInfoView];
    self.optionInfoPopOverController.popoverContentSize = CGSizeMake(self.view.frame.size.width/2, 100.0f);
    
    [self customizeViews];
    [self customizeButtons];
    [self setupLocalization];
    [self displayValues];
}

- (void)customizeViews {
    
    self.enterPasswordField.placeholder = NSLocalizedString(@"Password", nil);

    [self addFunctionForSwitch:self.shuffleChoicesSwitch];
    [self addFunctionForSwitch:self.shuffleQuestionsSwitch];
    [self addFunctionForSwitch:self.forcedScoreSwitch];
    [self addFunctionForSwitch:self.showScoreSwitch];
    [self addFunctionForSwitch:self.showResultSwitch];
    [self addFunctionForSwitch:self.showFeedbackMessagesSwitch];
    
    [self.showPassword addTarget:self action:@selector(showPasswordAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // WITH CORRECT ANSWER
    [self.withCorrectAnswerButton addTarget:self action:@selector(checkBoxAction:) forControlEvents:UIControlEventTouchUpInside];

    // PASSWORD
    self.enterPasswordField.delegate = self;
    [self.passwordSwitch addTarget:self
                            action:@selector(passwordSwitchAction:)
                  forControlEvents:UIControlEventValueChanged];
}

- (void)customizeButtons {
    // Button Tag
    self.shuffleChoicesInfoButton.tag = 100;
    self.shuffleQuestionsInfoButton.tag = 200;
    self.forcedScoreInfoButton.tag = 300;
    self.showResultsInfoButton.tag = 400;
    self.showCorrectAnswerInfoButton.tag = 500;
    self.showFeedbackInfoButton.tag = 600;
    self.setPasswordInfoButton.tag = 700;
    self.showScoreInfoButton.tag = 800;
    
    // Action for Button
    [self.shuffleChoicesInfoButton addTarget:self
                                      action:@selector(showPopOverOptionInfo:)
                            forControlEvents:UIControlEventTouchUpInside];
    
    [self.shuffleQuestionsInfoButton addTarget:self
                                        action:@selector(showPopOverOptionInfo:)
                              forControlEvents:UIControlEventTouchUpInside];
    
    [self.forcedScoreInfoButton addTarget:self
                                    action:@selector(showPopOverOptionInfo:)
                          forControlEvents:UIControlEventTouchUpInside];
    
    [self.showResultsInfoButton addTarget:self
                                   action:@selector(showPopOverOptionInfo:)
                         forControlEvents:UIControlEventTouchUpInside];
    
    [self.showCorrectAnswerInfoButton addTarget:self
                                   action:@selector(showPopOverOptionInfo:)
                         forControlEvents:UIControlEventTouchUpInside];
    
    [self.showFeedbackInfoButton addTarget:self
                                  action:@selector(showPopOverOptionInfo:)
                        forControlEvents:UIControlEventTouchUpInside];
    
    [self.setPasswordInfoButton addTarget:self
                                     action:@selector(showPopOverOptionInfo:)
                           forControlEvents:UIControlEventTouchUpInside];
    
    [self.showScoreInfoButton addTarget:self
                                   action:@selector(showPopOverOptionInfo:)
                         forControlEvents:UIControlEventTouchUpInside];
}

- (void)showPopOverOptionInfo:(id)sender {
    UIButton *button = (UIButton *)sender;
    NSInteger tag = button.tag;
    
    // Render Popover
    [self.optionInfoPopOverController presentPopoverFromRect:button.bounds
                                                      inView:button
                                    permittedArrowDirections:UIPopoverArrowDirectionDown
                                                    animated:YES];
    
    // Option Contents
    NSString *title = @"";
    NSString *body = @"";
    
    switch (tag) {
        case 100:
            title = NSLocalizedString(@"Shuffle Choices", nil);
            body = NSLocalizedString(@"Allows you to shuffle choices", nil);
            break;
        case 200:
            title = NSLocalizedString(@"Shuffle Questions", nil);
            body = NSLocalizedString(@"Allows you to randomly shuffle selected questions in test", nil);
            break;
        case 300:
            title = NSLocalizedString(@"Force Score", nil);
            body = NSLocalizedString(@"Allows automatic submit of test when the set time ends", nil);
            break;
        case 400:
            title = NSLocalizedString(@"Show Results", nil);
            body = NSLocalizedString(@"Allow display of test result with examinee's answer after submission of test", nil);
            break;
        case 500:
            title = NSLocalizedString(@"With Correct Answer", nil);
            body = NSLocalizedString(@"Allows display of test result with examinee's answer and the correct answer after submission of test", nil);
            break;
        case 600:
            title = NSLocalizedString(@"Show Feedback Messages", nil);
            body = NSLocalizedString(@"Allows you to give explaination of each correct and wrong answer", nil);
            break;
        case 700:
            title = NSLocalizedString(@"Set Password", nil);
            body = NSLocalizedString(@"password will be used by students who are allowed to take the test", nil);
            break;
        case 800:
            title = NSLocalizedString(@"Show Score", nil);
            body = NSLocalizedString(@"Allows display of total score at the end of the test", nil);
            break;
        default:
            break;
    }
    
    [self.optionInfoView setTestOptionInfoWithTitle:title andBody:body];
}

- (void)setupLocalization {
    
    NSString *option_string = NSLocalizedString(@"Options", nil);
    NSString *shuffle_choice_string = NSLocalizedString(@"Shuffle Choices", nil);
    NSString *shuffle_question_string = NSLocalizedString(@"Shuffle Questions", nil);
    NSString *forced_score_string = NSLocalizedString(@"Forced Submit", nil);
    NSString *show_result_string = NSLocalizedString(@"Show Results", nil);
    NSString *correct_answer_string = NSLocalizedString(@"With Correct Answer", nil);
    NSString *show_feedback_string = NSLocalizedString(@"Show Feedback Messages", nil);
    NSString *set_password_string = NSLocalizedString(@"Set Password", nil);
    NSString *enter_password_string = NSLocalizedString(@"Enter Password", nil);
    
    self.optionLabel.text = [option_string uppercaseString];
    self.shuffleChoicesLabel.text = [shuffle_choice_string uppercaseString];
    self.shuffleQuestionsLabel.text = [shuffle_question_string uppercaseString];
    self.forcedScoreLabel.text = [forced_score_string uppercaseString];
    self.showResultLabel.text = [show_result_string uppercaseString];
    self.withCorrectAnswerLabel.text = [correct_answer_string uppercaseString];
    self.showFeedbackMessagesLabel.text = [show_feedback_string uppercaseString];
    self.passwordLabel.text = [set_password_string uppercaseString];
    self.enterPasswordLabel.text = [enter_password_string uppercaseString];
}

- (void)displayValues {
    
    if (self.testObject != nil) {
        
        /*
         allow_review
         attempts
         date_close
         date_created
         date_open
         difficulty_level_id
         formatted_date_close
         formatted_date_open
         general_instruction
         id
         is_expired
         is_forced_submit
         is_graded
         item_count
         name
         passing_score
         password
         quiz_category_id
         quiz_category_name
         quiz_settings_id
         quiz_stage_name
         show_correct_answer
         show_feedbacks
         show_result
         show_score
         shuffle_choices
         shuffle_questions
         test_description
         time_limit
         total_score
         user_id
         */

        BOOL shuffle_choices = [self switchValue:self.testObject key:@"shuffle_choices"];
        BOOL shuffle_questions = [self switchValue:self.testObject key:@"shuffle_questions"];
        BOOL is_forced_submit = [self switchValue:self.testObject key:@"is_forced_submit"];
        BOOL show_result = [self switchValue:self.testObject key:@"show_result"];
        BOOL show_score = [self switchValue:self.testObject key:@"show_score"];
        BOOL show_correct_answer = [self switchValue:self.testObject key:@"show_correct_answer"];
        BOOL show_feedbacks = [self switchValue:self.testObject key:@"show_feedbacks"];
        
        self.shuffleChoicesSwitch.on = shuffle_choices;
        self.shuffleQuestionsSwitch.on = shuffle_questions;
        self.forcedScoreSwitch.on = is_forced_submit;
        self.showResultSwitch.on = show_result;
        self.showScoreSwitch.on = show_score;
        self.withCorrectAnswerButton.selected = show_correct_answer;
        self.showFeedbackMessagesSwitch.on = show_feedbacks;
        
        // PASSWORD
        NSString *password_string = [self stringValue:self.testObject key:@"password"];
        self.passwordSwitch.on = (password_string.length > 0);
        self.enterPasswordField.userInteractionEnabled = self.passwordSwitch.on;
        self.enterPasswordField.backgroundColor = (self.passwordSwitch.on) ? [UIColor whiteColor] : [UIColor lightGrayColor];
        self.showPassword.enabled = self.passwordSwitch.on;
        self.enterPasswordField.text = password_string;
    }
}

- (BOOL)switchValue:(id)object key:(NSString *)key {
    
    BOOL state = NO;
    
    if (object != nil) {
        NSString *value = [self stringValue:object key:key];
        state = [value isEqualToString:@"1"];
        return state;
    }
    
    return state;
}

- (NSString *)stringValue:(NSManagedObject *)mo key:(NSString *)key{
    
    return [NSString stringWithFormat:@"%@", [mo valueForKey:key] ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

///////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    /*
     self.enterPasswordField
     */
    
    NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (self.testObject != nil) {
        
        if ( textField == self.enterPasswordField) {
            
            if (updatedText.length > 16) {
                return NO;
            }
        }
        
        NSString *key = [self keyForTextField:textField];
        NSDictionary *userData = @{key:updatedText};
        [self updateDetailsWithValue:userData];
    }
    
    return YES;
}

- (NSString *)keyForTextField:(UITextField *)textField {
    
    /*
     self.enterPasswordField
     */
    
    NSString *default_key = @"password";
    
    //TITLE FIELD
    if (textField == self.enterPasswordField) {
        default_key = @"password";
        return default_key;
    }
    
    return default_key;
}

- (void)showPasswordAction:(UIButton *)sender {
    self.enterPasswordField.secureTextEntry = sender.selected;
    
    BOOL newState = (sender.selected) ? NO : YES;
    
    sender.selected = newState;
    
    
    if (self.enterPasswordField.secureTextEntry == NO) {
        [self.enterPasswordField resignFirstResponder];
        self.enterPasswordField.font = [UIFont systemFontOfSize:14.0f];
    }
    
}

- (void)checkBoxAction:(UIButton *)sender {
    sender.selected = (sender.selected) ? NO : YES;
    
    NSNumber *state = [NSNumber numberWithBool:sender.selected];
    NSString *flag = [NSString stringWithFormat:@"%@", state];
    NSDictionary *userData = @{@"show_correct_answer":flag};
    [self updateDetailsWithValue:userData];
}

- (void)addFunctionForSwitch:(UISwitch *)view {
    
    [view addTarget:self action:@selector(triggerActionForSwitch:) forControlEvents:UIControlEventValueChanged];
}

- (void)passwordSwitchAction:(UISwitch *)switchView {
    
    BOOL flag = switchView.on;
    
    self.enterPasswordField.userInteractionEnabled = flag;
    self.enterPasswordField.backgroundColor =  (flag) ? [UIColor whiteColor] : [UIColor lightGrayColor];
    self.showPassword.enabled = flag;
    
    NSString *password = @"";
    
    if (flag) {
        password = [NSString stringWithFormat:@"%@", self.enterPasswordField.text];
    }
    
    NSDictionary *userData = @{@"password":password};
    [self updateDetailsWithValue:userData];
}

- (void)triggerActionForSwitch:(UISwitch *)switchView {
    
    NSNumber *state = [NSNumber numberWithBool:switchView.on];
    NSString *flag = [NSString stringWithFormat:@"%@", state];
    NSString *key = [self keyForSwitchView:switchView];
    NSDictionary *userData = @{key:flag};
    [self updateDetailsWithValue:userData];
}

- (NSString *)keyForSwitchView:(UISwitch *)switchView {
    
    /*
     self.shuffleChoicesSwitch.on = shuffle_choices;
     self.shuffleQuestionsSwitch.on = shuffle_questions;
     self.forcedScoreSwitch.on = is_forced_submit;
     self.showResultSwitch.on = show_result;
     self.showFeedbackMessagesSwitch.on = show_feedbacks;
     */
    
    NSString *default_key = @"shuffle_choices";
    
    //SHUFFLE CHOICE SWITCH
    if (switchView == self.shuffleChoicesSwitch) {
        default_key = @"shuffle_choices";
        return default_key;
    }
    
    //SHUFFLE QUESTION SWITCH
    if (switchView == self.shuffleQuestionsSwitch) {
        default_key = @"shuffle_questions";
        return default_key;
    }
    
    //FORCE SCORE SWITCH
    if (switchView == self.forcedScoreSwitch) {
        default_key = @"is_forced_submit";
        return default_key;
    }

    //SHOW SCORE SWITCH
    if (switchView == self.showScoreSwitch) {
        default_key = @"show_score";
        return default_key;
    }

    //SHOW RESULTS SWITCH
    if (switchView == self.showResultSwitch) {
        default_key = @"show_result";
        return default_key;
    }

    //FEEDBACK SWITCH
    if (switchView == self.showFeedbackMessagesSwitch ) {
        default_key = @"show_feedbacks";
        return default_key;
    }

    return default_key;
}

// HIDE FORCE SUBMIT SWITCH
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = 44;
    if (indexPath.row == 2) {
        height = 0;
    }
    
    return height;
}

- (void)updateDetailsWithValue:(NSDictionary *)userData {
    
    NSLog(@"user data : %@", userData);
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:self.test_id];
    [self.tm updateTest:kTestInformationEntity predicate:predicate details:userData];
}

@end
