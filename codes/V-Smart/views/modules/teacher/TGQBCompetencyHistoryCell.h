//
//  TGQBCompetencyHistoryCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 18/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGQBCompetencyHistoryCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *curriculumField;
@property (strong, nonatomic) IBOutlet UILabel *periodField;
@property (strong, nonatomic) IBOutlet UILabel *codeField;
@property (strong, nonatomic) IBOutlet UILabel *learningCompetencyField;

@end
