//
//  TestGuruQuestionDetailsViewController.h
//  V-Smart
//
//  Created by Ryan Migallos on 05/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestGuruQuestionDetailsViewController : UIViewController

@property (assign, nonatomic) BOOL editMode;

- (void)displayContentFromObject:(NSManagedObject *)mo;

@end
