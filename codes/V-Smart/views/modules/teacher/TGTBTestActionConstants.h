//
//  TGTBTestActionConstants.h
//  V-Smart
//
//  Created by Julius Abarra on 08/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TGTBTestActionConstants : NSObject

//typedef NS_ENUM (NSInteger, TGTBSwipeButtonActionType) {
//    TGTBSwipeButtonActionDeploy = 1,
//    TGTBSwipeButtonActionSaveAsPDF,
//    TGTBSwipeButtonActionEdit,
//    TGTBSwipeButtonActionDelete
//};
//
//typedef NS_ENUM (NSInteger, TGTBCrudActionType) {
//    TGTBCrudActionTypeCreate,
//    TGTBCrudActionTypeEdit,
//    TGTBCrudActionTypeDelete
//};

@end
