//
//  TGQBQuestionContentViewController.m
//  V-Smart
//
//  Created by Ryan Migallos on 11/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGQBQuestionContentViewController.h"
#import "TGQBQuestionContentHeaderView.h"
#import "TGQBCompentencyViewController.h"
#import "TGQBDifficultyLearningMenu.h"
#import "TGQBTagCompetencyMenu.h"
#import "TGQBQuestionBasicData.h"
#import "TGQBChoicesView.h"
#import "TGQBFeedBack.h"
#import "MainHeader.h"
#import "TestGuruDataManager.h"
#import "TGQBAnswersTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "V_Smart-Swift.h"

@interface TGQBQuestionContentViewController () <UITableViewDelegate, TGQBTagCompetencyMenuDelegate, TGQBChoicesViewDelegate, TGQBCompetencyComponentDelegate>

@property (strong, nonatomic) IBOutlet TGQBDifficultyLearningMenu *dropDownField;
@property (strong, nonatomic) IBOutlet TGQBTagCompetencyMenu *tagCompetencyMenu;
@property (strong, nonatomic) IBOutlet TGQBQuestionBasicData *basic;
@property (strong, nonatomic) IBOutlet TGQBChoicesView *userChoices;
@property (strong, nonatomic) IBOutlet TGQBAnswersTableViewCell *userAnswers;
@property (strong, nonatomic) IBOutlet TGQBFeedback *feedBack;

@property (strong, nonatomic) NSManagedObject *mo;
@property (assign, nonatomic) CGFloat table_height;
@property (assign, nonatomic) CGFloat answer_row_height;
@property (assign, nonatomic) CGFloat basic_data_row_height;

@property (assign, nonatomic) BOOL isVersion25;

@property (strong, nonatomic) TestGuruDataManager *tm;

@property (weak, nonatomic) IBOutlet UILabel *difficultyLabel;
@property (weak, nonatomic) IBOutlet UILabel *learningSkillLabel;

@property (strong, nonatomic) TGQBCompentencyViewController *competencyViewController;

@end

@implementation TGQBQuestionContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tm = [TestGuruDataManager sharedInstance];
    [self setObjectData:self.mo];
    
    CGFloat version = [[Utils getServerInstanceVersion] floatValue];
    
    self.isVersion25 = (version >= VSMART_SERVER_MAX_VER);
    
    [self.difficultyLabel localize];
    [self.learningSkillLabel localize];
}

- (void)setObjectData:(NSManagedObject *)object {
    if (object != nil) {
        self.mo = object;
        self.table_height = [self heightForObject:self.mo];
        self.answer_row_height = [self answerRowHeightForObject:self.mo];
        self.basic_data_row_height = [self basicDataRowHeightForObject:self.mo];
        
        NSString *question_id = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"id"] ];
        NSString *package_id = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"package_type_id"] ];
        NSString *course_id = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"course_id"] ];
        
        NSLog(@"----------- FROM SEGUE -----------");
        NSLog(@"question id : %@", question_id);
        NSLog(@"package id : %@", package_id);
        NSLog(@"course id : %@", course_id);
        
        //DIFFICULTY LEVEL
        NSString *proficiency_level_id = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"proficiency_level_id"]];
        NSString *proficiencyLevelName = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"proficiencyLevelName"]];
        NSDictionary *proficiencyData = @{@"id":proficiency_level_id,@"value":proficiencyLevelName};
        [self.dropDownField setDifficultyValueForEntity:kProficiencyEntity withData:proficiencyData];
        
        //LEARNING SKILL
        NSString *learning_skills_id = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"learning_skills_id"]];
        NSString *learningSkillsName = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"learningSkillsName"]];
        NSDictionary *learningData = @{@"id":learning_skills_id,@"value":learningSkillsName};
        [self.dropDownField setLearningValueForEntity:kSkillEntity withData:learningData];
        
        //SET OBJECT
        [self.dropDownField setObjectData:self.mo];
        
        //SET OBJECT
        [self.tagCompetencyMenu setObjectData:self.mo];
        
        //TAGS
        self.tagCompetencyMenu.delegate = self; //DELEGATE
        NSSet *tagSet = [NSSet setWithSet:[self.mo valueForKey:@"tags"]];
        [self.tagCompetencyMenu displayTags:tagSet];
        
        //SET COMPETENCY_CODE
        NSString *competency_code = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"competency_code"] ];
        if (![competency_code isEqualToString:@""] && ![competency_code isEqualToString:@"(null)"]) {
            NSLog(@"COMPETENCY CODE [%@]", competency_code);
            [self.tagCompetencyMenu displayCompetencyCodeValue:competency_code];
        } else {
//            [self.tagCompetencyMenu displayCompetencyCodeValue:NSLocalizedString(@"Please Select Competency Code", nil)];
            [self.tagCompetencyMenu displayCompetencyCodeValue:NSLocalizedString(@"Choose Learning Competency Code", nil)];  // unchecked
        }
        
        //TODO: NOTE
        //COMPETENCY CODE
        NSString *title_string = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"name"] ];
        NSString *point_string = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"points"] ];
        NSString *desc_string = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"question_text"] ];
        
        /*
         name
         points
         question_text
         */
        
        //QUESTION TITLE
        self.basic.titleField.text = title_string;
        
        //QUESTION DESCRIPTION
        if ([point_string floatValue] > 1.0) {
            self.basic.pointsIndicator.text = [NSLocalizedString(@"Points", nil) uppercaseString];
        } else {
            self.basic.pointsIndicator.text = [NSLocalizedString(@"Point", nil) uppercaseString];
        }
        self.basic.pointsField.text = point_string;
        
        
        //QUESTION POINTS
        self.basic.descriptionLabel.text = desc_string;
        
        //SET MO
        [self.basic setObjectData:self.mo];
        
        //TODO : NOTE
        //QUESTION IMAGES
        //ONE
        //TWO
        //THREE
        //FOUR
        
        /*
         general_feedback
         wrong_answer_feedback
         correct_answer_feedback
         */
        
        //USER CHOICES
        self.userChoices.delegate = self;
        self.userChoices.isTrashHidden = [self isVisibleStateForObject:self.mo];
        [self.userChoices setObjectData:self.mo];
        
        
        //FEEDBACK
//        self.feedBack.generalFeedback = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"general_feedback"] ];
//        self.feedBack.correctAnswerFeedback = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"correct_answer_feedback"] ];
//        self.feedBack.wrongAnswerFeedback = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"wrong_answer_feedback"] ];
        
        [self.feedBack setObjectData:self.mo];
        
        // USER ANSWERS
        [self.userAnswers setObjectData:self.mo];
        NSSet *answerSet = [NSSet setWithSet:[self.mo valueForKey:@"choices"]];
        [self.userAnswers displayAnswers:answerSet];
        
    } else {
        NSLog(@"NO QUESTION DATA UPON PRESENTION OF DETAILS VIEW");
    }
}

- (void)willShowCompetencyView {
    
    NSLog(@"---> %s", __PRETTY_FUNCTION__);
    
    __weak typeof(self) wo = self;
    
    [self.tm requestTestGuruCurriculumListWithBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo performSegueWithIdentifier:@"COMPETENCY_CODE_SBID" sender:self];
            });

        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//////////////////////////////////////////////////////////////////////
// ------------------------ SECTION HEADER ------------------------ //
//////////////////////////////////////////////////////////////////////

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return [self customViewForTableView:tableView withSection:section];
}

- (UIView *)customViewForTableView:(UITableView *)tableView withSection:(NSInteger)section {
    
    NSString *property_string = [NSString stringWithFormat:@"%@", NSLocalizedString(@"Question Properties", nil)];
    NSString *detail_string = [NSString stringWithFormat:@"%@", NSLocalizedString(@"Question Details", nil)];
    NSInteger count = [self.tm fetchCountForEntity:kQuestionEntity];
    
    NSString *section_title = (section == 0) ? [property_string uppercaseString] : [detail_string uppercaseString];
    BOOL hidden = ( (section == 0) && (count > 1) ) ? NO : YES;
    
    CGFloat width = tableView.frame.size.width;
    CGFloat height = 44.0f;
    CGRect rect = CGRectMake(0, 0, width, height);
    
    TGQBQuestionContentHeaderView *header  = [[TGQBQuestionContentHeaderView alloc] initWithFrame:rect];
    [header.headerButtton addTarget:self action:@selector(deleteHeaderButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    header.headerButtton.hidden = hidden;
    header.headerTitleLabel.text = section_title;
    
    [header.headerTitleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    
    CALayer *headerBottomBorder = [CALayer layer];
    headerBottomBorder.frame = CGRectMake(0.0f, 40.0f, tableView.frame.size.width, 1.0f);
    headerBottomBorder.backgroundColor = [UIColor colorWithWhite:0.5f alpha:1.0f].CGColor;
    [header.layer addSublayer:headerBottomBorder];
    
    header.backgroundColor = (section == 0) ? UIColorFromHex(0xFAFAFA) : UIColorFromHex(0xCAEEF6);
    
    return header;
}

- (void)deleteHeaderButtonAction:(UIButton *)button {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete", nil)
                                                                             message:NSLocalizedString(@"Are you sure you want to delete this item?", nil)
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
    NSString *question_id_object = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"id"]];
    NSNotificationCenter *notif = [NSNotificationCenter defaultCenter];
    [self.tm removeQuestions:self.mo withAttribute:@"id" doneBlock:^(BOOL status) {
        [notif postNotificationName:kNotificationQuestionDelete object:question_id_object];
    }];
    
    NSInteger count = [self.tm fetchCountForEntity:kQuestionEntity];
    button.hidden = (count > 1) ? NO : YES;
                                                           }];
    
    UIAlertAction *noAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:nil];
    [alertController addAction:yesAlertAction];
    [alertController addAction:noAlertAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

//////////////////////////////////////////////////////////////////////
// ------------------------ SECTION HEADER ------------------------ //
//////////////////////////////////////////////////////////////////////

- (void)expandTableWithHeight:(CGFloat)customHeight {
    
    NSLog(@"%s height :  %@", __PRETTY_FUNCTION__, @(customHeight) );
    
    [self.tableView beginUpdates];
    self.table_height = customHeight;
    [self.tableView endUpdates];
}

#pragma mark - Table View Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = UITableViewAutomaticDimension;

    
    // SECTION 0
    if (indexPath.section == 0) {
//        height = 80;
        height = 90;
    }
    
    // SECTION 1
    if (indexPath.section == 1) {
        
        if (indexPath.row == 0) {
            height = self.basic_data_row_height;
        }
        
        if (indexPath.row == 1) {
            height = self.table_height;
        }

        if (indexPath.row == 2) {
            height = (self.isVersion25) ? self.answer_row_height : 160;
        }
        
        if (indexPath.row == 3) {
            height = 160;
        }
        
    }
    
    return height;
}

- (BOOL)isVisibleStateForObject:(NSManagedObject *)object {
    
    BOOL state = NO;
    if (object != nil) {
        
        NSSet *choice_set = [object valueForKey:@"choices"];
        NSInteger count = [choice_set count];
        state = (count == 2) ? YES : NO;
    }
    
    return state;
}

- (CGFloat)heightForObject:(NSManagedObject *)object {
    
    CGFloat height = 180.0f; //DEFAULT VALUE

    if (object != nil) {
        /*
          id = 1; // TRUE OR FALSE
          id = 3; // MULTIPLE CHOICE
          id = 6; // ESSAY
          id = 9; // SHORT ANSWER
         */
        
        NSString *question_type_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"question_type_id"]];
        
        //TRUE OR FALSE
        if ([question_type_id isEqualToString:@"1"]) {
            height = 130.0f;
        }
        
        // FILL IN THE BLANKS
        if ([question_type_id isEqualToString:@"2"]) {
            height = 0.0f;
        }
        
        // MULTIPLE CHOICE
        if ([question_type_id isEqualToString:@"3"]) {
            height = 180.0f;
        }

        // ESSAY
        if ([question_type_id isEqualToString:@"6"]) {
            height = 0.0f;
        }

        // SHORT ANSWER
        if ([question_type_id isEqualToString:@"9"]) {
            height = 0.0f;
        }
        
        // IDENTIFICATION
        if ([question_type_id isEqualToString:@"10"]) {
            height = 0.0f;
        }
        
    }
    
    return height;
}

- (CGFloat)answerRowHeightForObject:(NSManagedObject *)object {
    CGFloat height = 115.0f;
    
    CGFloat version = [[Utils getServerInstanceVersion] floatValue];
    
    BOOL isVersion25 = (version >= VSMART_SERVER_MAX_VER);
    
    if (object != nil) {
        NSString *question_type_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"question_type_id"]];
        
        //TRUE OR FALSE
        if ([question_type_id isEqualToString:@"1"]) {
            height = 0.0f;
        }
        
        // FILL IN THE BLANKS
        if ([question_type_id isEqualToString:@"2"]) {
            height = 115.0f;
        }
        
        // MULTIPLE CHOICE
        if ([question_type_id isEqualToString:@"3"]) {
            height = 0.0f;
        }
        
        // ESSAY
        if ([question_type_id isEqualToString:@"6"]) {
            height = 0.0f;
        }
        
        // SHORT ANSWER
        if ([question_type_id isEqualToString:@"9"]) {
            height = 0.0f;
        }
        
        // IDENTIFICATION
        if ([question_type_id isEqualToString:@"10"]) {
            height = 115.0f;
        }
    }
    
    return height;
}

- (CGFloat)basicDataRowHeightForObject:(NSManagedObject *)object {
    CGFloat height = 233.0f;
    
    if (object != nil) {
        NSString *question_type_id = [NSString stringWithFormat:@"%@", [object valueForKey:@"question_type_id"]];
        
        // FILL IN THE BLANKS AND IDENTIFICATION
        if ([question_type_id isEqualToString:@"2"]) {
            height = 270.0f;
        }
    }
    
    return height;
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"COMPETENCY_CODE_SBID"]) {
        self.competencyViewController = (TGQBCompentencyViewController *)[segue destinationViewController];
        self.competencyViewController.delegate = self;
    }
}

- (void)didFinishSelectingLearningCompetencyCode:(NSString *)competencyCode {
    [self.tagCompetencyMenu updateCompetencyCodeValue:competencyCode];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.tableView reloadData];
}

@end
