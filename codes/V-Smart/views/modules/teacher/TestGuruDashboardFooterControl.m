//
//  TestGuruDashboardFooterControl.m
//  V-Smart
//
//  Created by Ryan Migallos on 21/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "TestGuruDashboardFooterControl.h"
#import <QuartzCore/QuartzCore.h>

@interface TestGuruDashboardFooterControl()
@property (strong, nonatomic) IBOutlet UIButton *addButton;
@end

@implementation TestGuruDashboardFooterControl

- (void)awakeFromNib {
    
    CALayer *imageLayer = self.imageView.layer;
    imageLayer.shadowColor = [UIColor lightGrayColor].CGColor;
    imageLayer.shadowOffset = CGSizeMake(2.0,2.0);
    imageLayer.shadowRadius = 5.0f;
    imageLayer.shadowOpacity = 0.9f;
    
    [self.addButton addTarget:self action:@selector(addButtonAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)prepareForReuse {
    
    self.buttonLabel.text = @"";
    self.imageView.image = nil;
    self.typeString = @"";
}

- (void)addButtonAction:(UIButton *)sender {
    
    if ([(NSObject *)self.delegate respondsToSelector:@selector(didFinishSelectingType:)]) {
        [self.delegate didFinishSelectingType:self.typeString];
    }
}

@end
