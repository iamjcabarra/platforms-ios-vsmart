//
//  TestGuruDashboardFooterControl.h
//  V-Smart
//
//  Created by Ryan Migallos on 21/10/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TestGuruDashboardFooterDelegate <NSObject>
@required
- (void)didFinishSelectingType:(NSString *)type;
@end

@interface TestGuruDashboardFooterControl : UICollectionReusableView

@property (weak, nonatomic) id <TestGuruDashboardFooterDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *buttonLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) NSString *typeString;

@end
