//
//  TGLearningTypeItem.h
//  V-Smart
//
//  Created by Ryan Migallos on 08/12/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGLearningTypeItem : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
- (void)showHighlight:(BOOL)enable;
@end
