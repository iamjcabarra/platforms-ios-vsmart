//
//  TGTBDatePickerView.m
//  V-Smart
//
//  Created by Julius Abarra on 14/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TGTBDatePickerView.h"
#import "TBClassHelper.h"
#import "TestGuruConstants.h"

@interface TGTBDatePickerView ()

@property (strong, nonatomic) TBClassHelper *classHelper;

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UILabel *pickerTitleLabel;
//@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;

@property (strong, nonatomic) NSString *dateToReturn;
@property (strong, nonatomic) NSString *dateFormatToReturn;

@end

@implementation TGTBDatePickerView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Title
    NSString *title = [self.classHelper localizeString:@"Set Start Date"];
    self.dateFormatToReturn = kTGTBUIDateFormatVeryLongStyle;
    
    if (self.dateType == 200) {
        title = [self.classHelper localizeString:@"Set End Date"];
    } else if (self.dateType == 0) {
        title = self.labelString;
        self.dateFormatToReturn = @"yyyy-MM-dd";
    }
    
    self.pickerTitleLabel.text = title;
    
    // Background View
    self.backgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.backgroundView.layer.borderWidth = 1.0f;
    
    // Initialize Date
    self.dateToReturn = [self.classHelper getDateTodayWithFormat:self.dateFormatToReturn];
    
    // Date Picker Action
    [self.datePicker addTarget:self
                        action:@selector(changeDateAction:)
              forControlEvents:UIControlEventValueChanged];
    
    // Done Button Action
    [self.doneButton addTarget:self
                        action:@selector(selectDateAction:)
              forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Date Picker Action

- (void)changeDateAction:(UIDatePicker *)sender {
    self.dateToReturn = [self.classHelper convertDateToString:[self.datePicker date] format:self.dateFormatToReturn];
    
    NSLog(@"%@", self.dateToReturn);
    
    
    
}


#pragma mark - Done Button Action

- (void)selectDateAction:(id)sender {
    [self.delegate selectedDate:self.dateToReturn];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


@end
