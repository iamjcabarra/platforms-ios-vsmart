//
//  StudentProfileTableViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 10/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentProfileTableViewController : UITableViewController

@property (strong, nonatomic) NSString *studentid;

@end
