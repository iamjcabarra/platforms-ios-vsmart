//
//  LPMCourseViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 17/06/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

class LPMCourseViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate {
    
    @IBOutlet fileprivate var courseTableView: UITableView!
    @IBOutlet fileprivate var courseSearchBar: UISearchBar!
    @IBOutlet fileprivate var loadMoreButton: UIButton!
    @IBOutlet fileprivate var emptyPlaceholderView: UIView!
    @IBOutlet fileprivate var emptyPlaceholderLabel: UILabel!
    @IBOutlet fileprivate var searchDimmedView: UIView!
    
    fileprivate let kCourseCellIdentifier = "course_cell_identifier"
    fileprivate let kLessonViewSegueIdentifier = "SHOW_LESSON_VIEW"
    
    fileprivate var searchKey = ""
    fileprivate var isAscending = true
    
    fileprivate var currentPage = 0
    fileprivate var totalFilteredTests = 0
    fileprivate var totalPages = 0
    fileprivate var totalItems = 0
    
    fileprivate var selectedCourseObject: NSManagedObject!
    fileprivate var tableRefreshControl: UIRefreshControl!
    
    fileprivate lazy var isVersion25:Bool = {
        let version:CGFloat = Utils.getServerInstanceVersion() as CGFloat
        return (version >= LPMConstants.Server.MAX_VERSION)
    }()
    
    // MARK: - Data Managers
    
    fileprivate lazy var lessonPlanDataManager: LessonPlanDataManager = {
        let lpdm = LessonPlanDataManager.sharedInstance()
        return lpdm!
    }()
    
    fileprivate lazy var testGuruDataManager: TestGuruDataManager = {
        let tgdm = TestGuruDataManager.sharedInstance()
        return tgdm!
    }()

    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide dimmed view by default
        self.searchDimmedView.isHidden = true
        
        // Hide empty place holder by default
        self.shouldShowEmptyPlaceholderView(false)
        
        // Load more button action
        let loadMoreButtonAction = #selector(self.loadMoreButtonAction(_:))
        self.loadMoreButton.addTarget(self, action: loadMoreButtonAction, for: .touchUpInside)
        
        // Dimmed view tap recognizer
        let cancelSearchOperation = #selector(self.cancelSearchOperation(_:))
        let tapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: cancelSearchOperation)
        self.searchDimmedView.addGestureRecognizer(tapGestureRecognizer)
        
        // Pull to refresh
        let refreshCourseListAction = #selector(self.refreshCourseListAction(_:))
        self.tableRefreshControl = UIRefreshControl.init()
        self.tableRefreshControl.addTarget(self, action: refreshCourseListAction, for: .valueChanged)
        self.courseTableView.addSubview(self.tableRefreshControl)
        
        // Request initial paginated course list
        let settings = getSettingsForCoursePaginationIsReset(true, shouldLoadNextPage: false)
        self.loadPaginatedCourseListForSettings(settings!, requestType: .load)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customizeNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Custom Navigation Bar
    
    func customizeNavigationBar() {
        // Decorate navigation bar
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "#3498DB")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        // Create navigation bar right button items
        let spacerButton = UIButton.init(type: UIButtonType.custom)
        spacerButton.frame = CGRect(x: 0, y: 0, width: 10, height: 20)
        spacerButton.showsTouchWhenHighlighted = false
        
        let searchButton = UIButton.init(type: UIButtonType.custom)
        searchButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        searchButton.showsTouchWhenHighlighted = true
        
        let searchButtonImage = UIImage.init(named: "search_white_48x48.png")
        searchButton.setImage(searchButtonImage, for: UIControlState())
        
        let searchButtonAction = #selector(self.searchButtonAction(_:))
        searchButton.addTarget(self, action: searchButtonAction, for: .touchUpInside)
        
        let sortButton = UIButton.init(type: UIButtonType.custom)
        sortButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        sortButton.showsTouchWhenHighlighted = true
        
        let sortButtonImage = UIImage.init(named: "sort_white48x48.png")
        sortButton.setImage(sortButtonImage, for: UIControlState())
        
        let sortButtonAction = #selector(self.sortButtonAction(_:))
        sortButton.addTarget(self, action: sortButtonAction, for: .touchUpInside)
        
        let spacerButtonItem = UIBarButtonItem.init(customView: spacerButton)
        let sortButtonItem = UIBarButtonItem.init(customView: sortButton)
        let searchButtonItem = UIBarButtonItem.init(customView: searchButton)
        
        self.navigationItem.rightBarButtonItems = [sortButtonItem, searchButtonItem, spacerButtonItem]
        
        // Custom navigation bar's search bar
        self.courseSearchBar.placeholder = NSLocalizedString("Search Course", comment: "")
        self.courseSearchBar.delegate = self
    }
    
    // MARK: - Course List Pagination
    
    func getSettingsForCoursePaginationIsReset(_ isReset: Bool, shouldLoadNextPage: Bool) -> Dictionary <String, String>? {
        var limit = "10"
        let pagination:String = self.testGuruDataManager.stringValue(self.testGuruDataManager.fetchObject(forKey: kTG_PAGINATION_LIMIT))
        var currentPage = "1"
        let sortKey =  self.isAscending ? "-course_name" : "course_name"
        
        if (Int(pagination) < 1) {
            limit = "10"
        }
        
        if (!isReset) {
            currentPage = "\(shouldLoadNextPage ? self.currentPage + 1 : self.currentPage)"
        }
        
        return ["limit": limit, "current_page": currentPage, "search_keyword": self.searchKey, "sort_key": sortKey]
    }
    
    func loadPaginatedCourseListForSettings(_ settings: Dictionary <String, String>, requestType: LPMCourseListRequestType) {
        self.loadMoreButton.isHidden = true
        
        if (requestType != .refresh) {
            let indicatorString = requestType == .load ? "\(NSLocalizedString("Loading", comment: ""))..." : "\(NSLocalizedString("Searching", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
        }
        
        let userid = self.lessonPlanDataManager.loginUser()
        self.lessonPlanDataManager.requestPaginatedCourseList(forUser: userid, withSettingsForPagination: settings) { (data) in
            if (data != nil) {
                self.currentPage = (data?["current_page"] as! NSString).integerValue
                self.totalFilteredTests = (data?["total_filtered"] as! NSString).integerValue
                self.totalPages = (data?["total_pages"] as! NSString).integerValue
                self.totalItems = (data?["total_items"] as! NSString).integerValue
            }
            
            DispatchQueue.main.async(execute: {
                self.reloadFetchedResultsController()
                
                // Navigation title
                self.title = "\(NSLocalizedString("Course List", comment: "")) (\(self.totalItems))"
                
                // Load more button
                let shouldHideButton = (self.totalPages > self.currentPage) ? false : true
                self.loadMoreButton.isHidden = shouldHideButton
                
                // Empty place holder
                let shouldShowEmptyPlaceholderView = (self.totalFilteredTests == 0) ? true : false
                self.shouldShowEmptyPlaceholderView(shouldShowEmptyPlaceholderView)
                
                requestType != .refresh ? HUD.hideUIBlockingIndicator() : self.tableRefreshControl.endRefreshing()
            })
        }
    }
    
    func refreshCourseListAction(_ sender: UIRefreshControl?) {
        let settings = getSettingsForCoursePaginationIsReset(true, shouldLoadNextPage: false)
        self.loadPaginatedCourseListForSettings(settings!, requestType: .refresh)
    }
    
    // MARK: - Button Event Handlers
    
    func loadMoreButtonAction(_ sender: UIButton) {
        let settings = getSettingsForCoursePaginationIsReset(false, shouldLoadNextPage: true)
        self.loadPaginatedCourseListForSettings(settings!, requestType: .load)
    }
    
    func searchButtonAction(_ sender: UIButton) {
        self.navigationItem.titleView = self.courseSearchBar
        self.courseSearchBar.becomeFirstResponder()
    }
    
    func sortButtonAction(_ sender: UIButton) {
        self.isAscending = self.isAscending ? false : true
        self.reloadFetchedResultsController()
    }
    
    // MARK: - Search Bar Delegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchDimmedView.isHidden = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText == "") {
            self.searchKey = searchText
            let settings = self.getSettingsForCoursePaginationIsReset(true, shouldLoadNextPage: false)
            self.loadPaginatedCourseListForSettings(settings!, requestType: .load)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchKey = searchBar.text!
        let settings = self.getSettingsForCoursePaginationIsReset(true, shouldLoadNextPage: false)
        self.loadPaginatedCourseListForSettings(settings!, requestType: .search)
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = self.searchKey
        self.navigationItem.titleView = nil
        self.searchDimmedView.isHidden = true
    }
    
    // MARK: - Tap Gesture Recognizer
    
    func cancelSearchOperation(_ sender: UITapGestureRecognizer) {
        self.navigationItem.titleView = nil
        self.searchDimmedView.isHidden = true
        self.view.endEditing(true)
        self.courseSearchBar.text = self.searchKey
    }
    
    // MARK: - Empty Placeholder View
    
    func shouldShowEmptyPlaceholderView(_ show: Bool) {
        if (show) {
            var message = NSLocalizedString("No available course yet.", comment: "")
            
            if (self.courseSearchBar.text != "") {
                message = "\(NSLocalizedString("No results found for", comment: "")) \"\(self.searchKey)\""
            }
            
            self.emptyPlaceholderLabel.text = message
        }
        
        self.emptyPlaceholderView.isHidden = !show
        self.courseTableView.isHidden = show
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kCourseCellIdentifier, for: indexPath) as! LPMCourseTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: LPMCourseTableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        
        guard
            let course_name = mo.value(forKey: "course_name") as? String,
            //let section_name = mo.value(forKey: "section_name") as? String
            let grade_level = mo.value(forKey: "grade_level") as? String,
            let schedule = mo.value(forKey: "schedule") as? String
            else { return }
        
        cell.courseImage.image = UIImage.init(named: "course-list2.png")
        cell.courseNameLabel.text = course_name
        cell.gradeLevelLabel.isHidden = (self.isVersion25 == true) ? false : true
        //cell.gradeLevelLabel.text = "\(section_name) - \(grade_level)"
        cell.gradeLevelLabel.text = grade_level
        cell.courseScheduleLabel.text = schedule
    }
    
    // MARK: - Table View Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCourseObject = self.fetchedResultsController.object(at: indexPath)
        
        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: self.kLessonViewSegueIdentifier, sender: nil)
            tableView.deselectRow(at: indexPath, animated: true)
        })
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = self.lessonPlanDataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: kCourseTableEntity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kCourseTableEntity)
        fetchRequest.fetchBatchSize = 20
        
        let descriptorSelector = #selector(NSString.localizedCaseInsensitiveCompare(_:))
        let sortDescriptor = NSSortDescriptor.init(key: "course_name", ascending: self.isAscending, selector:descriptorSelector)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.courseTableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.courseTableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.courseTableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.courseTableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.courseTableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.courseTableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! LPMCourseTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.courseTableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.courseTableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }

    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.courseTableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.courseTableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.kLessonViewSegueIdentifier {
            let lessonView = segue.destination as! LPMLessonViewController
            lessonView.selectedCourseObject = self.selectedCourseObject
            self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        }
    }
    
}
