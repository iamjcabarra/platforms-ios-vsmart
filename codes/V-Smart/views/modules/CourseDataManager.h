//
//  CourseDataManager.h
//  V-Smart
//
//  Created by Julius Abarra on 09/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


/////// PUBLIC ENTITIES //////
static NSString *kStudentEntity = @"Student";
static NSString *kStudentProfileEntity = @"StudentProfile";

/////// BLOCK TYPES //////
typedef void (^CourseDoneBlock)(BOOL status);
typedef void (^CourseDataBlock)(NSDictionary *data);
typedef void (^CourseContent)(NSArray *content);
typedef void (^CourseBinaryBlock)(NSData *binary);

@interface CourseDataManager : NSObject

@property (nonatomic, readonly) NSManagedObjectContext *mainContext;

+ (instancetype)sharedInstance;
- (void)saveContext;
- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate;
- (NSString *)stringValue:(id)object;

/////// PUBLIC METHODS //////
- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value;

- (NSManagedObject *)getEntity:(NSString *)entity
                     attribute:(NSString *)attribute
                     parameter:(NSString *)parameter
                       context:(NSManagedObjectContext *)context;

- (NSManagedObject *)getEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate;

- (NSArray *)getObjectsForEntity:(NSString *)entity
                       predicate:(NSPredicate *)predicate
                         context:(NSManagedObjectContext *)context;

/////// STUDENT API //////
- (void)requestStudentListForCourseWithID:(NSString *)courseid doneBlock:(CourseDoneBlock)doneBlock;
- (void)requestStudentListForCourseWithID:(NSString *)courseid dataBlock:(CourseDataBlock)dataBlock;
- (void)requestStudentProfileWithID:(NSString *)studentid doneBlock:(CourseDoneBlock)doneBlock;
- (void)downloadProfileImage:(NSManagedObject *)object binaryBlock:(CourseBinaryBlock)binaryBlock;

@end

static NSString *kQuestionBankEntity = @"QuestionBank";
static NSString *kQuestionGroupByEntity = @"QuestionGroupBy";
static NSString *kQuestionEntity = @"Question";
static NSString *kPackageTypeEntity = @"PackageType";
static NSString *kCourseListEntity = @"CourseList";
static NSString *kStageTypeEntity = @"StageType";
static NSString *kResultTypeEntity = @"ResultType";
static NSString *kCategoryTypeEntity = @"CategoryType";
static NSString *kTestTypeEntity = @"TestType";
static NSString *kQuestionTypeEntity = @"QuestionType";
static NSString *kProficiencyEntity = @"Proficiency";
static NSString *kSkillEntity = @"Skill";
static NSString *kChoiceEntity = @"Choice";
static NSString *kChoiceItemEntity = @"ChoiceItem";
static NSString *kTagEntity = @"Tag";
static NSString *kTagValuesEntity = @"TagValues";
static NSString *kTestSectionEntity = @"TestSection";
static NSString *kQuizEntity = @"Quiz";
static NSString *kQuizQuestionEntity = @"QuizQuestion";
static NSString *kQuizQuestionChoiceEntity = @"QuizQuestionChoice";
static NSString *kTestGuruFilterOptions = @"FilterOption";
static NSString *kDeployedTestEntity = @"DeployedTest";
static NSString *kTestDetailEntity = @"TestDetail";
static NSString *kTestQuestionEntity = @"TestQuestion";
static NSString *kTestQuestionChoiceEntity = @"TestQuestionChoice";
static NSString *kTestEntity = @"Test";
static NSString *kTestSortOptionEntity = @"TestSortOption";
static NSString *kCompetencyHistoryEntity = @"CompetencyHistory";
static NSString *kCompetencyEntity = @"Competency";
static NSString *kCourseCurriculumEntity = @"CourseCurriculum";
static NSString *kPeriodEntity = @"Period";
static NSString *kTestGradingTypeEntity = @"TestGradingType";
static NSString *kCourseSectionListEntity = @"CourseSectionList";
static NSString *kTestInformationEntity = @"TestInformation";
static NSString *kTestQuestionItemEntity = @"TestQuestionItem";
static NSString *kTestQuestionItemChoiceEntity = @"TestQuestionItemChoice";
static NSString *kTestQuestionItemImageEntity = @"TestQuestionItemImage";

/////// BLOCK TYPES //////
typedef void (^TestGuruDoneBlock)(BOOL status);
typedef void (^TestGuruDataBlock)(NSDictionary *data);
typedef void (^TestGuruContent)(NSString *content);
typedef void (^TestGuruProgressBlock)(CGFloat progress);
typedef void (^TestGuruDoneProgressBlock)(NSString *status);
typedef void (^TestGuruListBlock)(NSArray *list);
typedef void (^TestGuruNumberBlock)(NSInteger number);
typedef void (^TestGuruBinaryBlock)(NSData *binary);
typedef void (^TestGuruManagedObjectBlock)(NSManagedObject *object);

@interface TestGuruDataManager : NSObject

@property (nonatomic, readonly) TestGuruProgressBlock progressBlock;
@property (nonatomic, readonly) TestGuruDoneProgressBlock doneProgressBlock;
@property (nonatomic, readonly) NSManagedObjectContext *mainContext;

+ (instancetype)sharedInstance;
- (void)saveEntity:(NSString *)entity predicate:(NSPredicate *)predicate object:(NSDictionary *)data doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)saveContext;
- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate;
- (BOOL)clearContentsForEntity:(NSString *)entity;

/////// PUBLIC METHODS //////
- (NSString *)loginUser;
- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value;
- (NSPredicate *)predicateForKeyPath:(NSString *)keypath object:(id)object;
- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value;
- (NSManagedObject *)getEntity:(NSString *)entity attribute:(NSString *)attribute parameter:(NSString *)parameter context:(NSManagedObjectContext *)context;
- (NSUInteger)fetchCountForEntity:(NSString *)entity;
- (NSArray *)fetchObjectsForEntity:(NSString *)entity;
- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate;
- (NSManagedObject *)insertNewRecordForEntity:(NSString *)entity;
- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context;
- (void)saveObject:(id)object forKey:(NSString *)key;
- (id)fetchObjectForKey:(NSString *)key;

/////// QUESTION BANK ///////
- (void)requestCourseList:(TestGuruDoneBlock)doneBlock;
- (void)requestDashboardStatistics:(TestGuruDataBlock)dataBlock;

- (void)requestType:(NSString *)type doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestFilterOptionType:(NSString *)typeIdentifier title:(NSString *)title order:(NSString *)order
                      doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)updateFilterOptionObject:(NSManagedObject *)mo;
- (NSArray *)filterOptionsSettedTo:(BOOL)selected_status;
- (NSManagedObject *)firstManagedObject;
- (void)requestPaginationSettings:(TestGuruDoneBlock)doneBlock;
- (void)requestPackageTypes:(TestGuruDoneBlock)doneBlock;
- (void)requestQuestionType:(TestGuruDoneBlock)doneBlock;
- (void)requestProficiencyLevel:(TestGuruDoneBlock)doneBlock;
- (void)requestLearningSkill:(TestGuruDoneBlock)doneBlock;
- (void)requestQuestionCountForUser:(NSString *)userid dataBlock:(TestGuruDataBlock)dataBlock;
- (void)requestTestCountForUser:(NSString *)userid dataBlock:(TestGuruDataBlock)dataBlock;
- (void)requestDeployedTestCountForUser:(NSString *)userid dataBlock:(TestGuruDataBlock)dataBlock;
- (void)downloadQuestionImageForObject:(NSManagedObject *)object doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)downloadImageForEntity:(NSString *)entity withObject:(NSManagedObject *)object doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestQuestionListForTagsForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestQuestionListForQuestionTypeForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestQuestionListForDifficultyTypeForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestQuestionListForLearningSkillTypeForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestQuestionListForSharedStatusForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestQuestionListForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestQuestionListForUser:(NSString *)userid enableChoice:(BOOL)enable doneBlock:(TestGuruDoneBlock)doneBlock;

- (void)requestAssignQuestionListForUser:(NSString *)userid enableChoice:(BOOL)enable doneBlock:(TestGuruDoneBlock)doneBlock;

- (void)requestDetailsForQuestion:(NSManagedObject *)mo doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestDetailsForQuestion:(NSManagedObject *)mo withNewEntity:(NSString *)entity doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestDetailsForQuestionPreview:(NSManagedObject *)object withNewEntity:(NSString *)entity doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestUpdateDetailsForQuestion:(NSManagedObject *)mo doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestNewQuestionForContext:(NSManagedObjectContext *)context doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestNewQuestionsWithHeaderData:(NSDictionary *)headerData doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestUpdateQuestionsWithDoneBlock:(TestGuruDoneBlock)doneBlock;
- (void)postQuestionsObjectForEditMode:(BOOL)isEditMode withDoneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestUpdateDetailsForQuestion:(NSManagedObject *)mo headerData:(NSDictionary *)headerData doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestRemoveQuestion:(NSManagedObject *)mo doneBlock:(TestGuruDoneBlock)doneBlock;
- (NSSet *)processTags:(NSArray *)list;
- (void)insertNewTagValue:(NSString *)value;
- (void)insertNewTagValue:(NSString *)value withQuestionID:(NSString *)question_id doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)tagBatchInsert:(NSArray *)tagList doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)removeTagValue:(NSString *)value;
- (void)removeTagValue:(NSString *)value doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)clearTagValuesWithBlock:(TestGuruDoneBlock)doneBlock;
- (void)updateTagValue:(NSString *)value withNewValue:(NSString *)string context:(NSManagedObjectContext *)context doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)updateTagValue:(NSString *)value withNewValue:(NSString *)string doneBlock:(TestGuruDoneBlock)doneBlock;
- (NSSet *)insertTags:(NSArray *)tags managedObject:(NSManagedObject *)mo;
- (void)insertQuestionWithType:(NSDictionary *)questionTypeData itemCount:(NSString *)count dataBlock:(TestGuruDataBlock)dataBlock;
- (void)insertQuestionWithItemCount:(NSString *)count
                             object:(NSMutableDictionary *)object
                          doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)insertChoiceItemWithQuestionID:(NSString *)value;
- (void)insertChoiceItemWithQuestionID:(NSString *)value doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)saveQuestionChoices:(NSArray *)choices questionid:(NSString *)questionid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)deleteChoiceItem:(NSManagedObject *)choice;
- (void)deleteChoiceItem:(NSManagedObject *)choice doneBlock:(TestGuruDoneBlock)doneBlock;
- (BOOL)clearChoicesCheckStatusForQuestion:(NSString *)questionId;
- (void)updateQuestionChoiceText:(NSString *)text withPredicate:(NSPredicate *)predicate;
- (void)insertEntity:(NSString *)entity userData:(NSDictionary *)userData;
- (void)updateEntity:(NSString *)entity details:(NSDictionary *)dictionary predicate:(NSPredicate *)predicate;
- (void)updateQuestion:(NSString *)entity predicate:(NSPredicate *)predicate details:(NSDictionary *)details;
- (void)updateFilterOptionObjects:(NSSet *)selectedObjects deselect:(NSSet *)deselectedObjects;
- (void)validateQuestionObject:(NSManagedObject *)mo;
/////// QUESTION BANK CYCLE MANAGER ///////
- (void)fetchQuestionList:(TestGuruListBlock)list;
- (void)checkCompleteStates:(TestGuruNumberBlock)number;

//- (void)requestFilteredQuestionBankWithPage:(NSString *)pageNumber filterIDs:(NSArray *)arrayOfIDs withPackageID:(NSString *)packageID dataBlock:(TestGuruDataBlock)dataBlock;
//- (void)requestFilteredQuestionBankWithPage:(NSString *)pageNumber withKeyword:(NSString *)search_key
//                                  filterIDs:(NSArray *)arrayOfIDs dataBlock:(TestGuruDataBlock)dataBlock;

//- (void)requestFilteredQuestionBankWithPage:(NSString *)pageNumber
//                                withKeyword:(NSString *)search_key
//                                  filterIDs:(NSArray *)arrayOfIDs
//                                       sort:(NSDictionary *)sort
//                                  dataBlock:(TestGuruDataBlock)dataBlock;

- (void)requestFilteredAssignQuestionWithPage:(NSString *)pageNumber withKeyword:(NSString *)search_key
                                    filterIDs:(NSArray *)arrayOfIDs dataBlock:(TestGuruDataBlock)dataBlock;
- (void)updateGroupByObject:(NSManagedObject *)mo withPrevious:(NSManagedObject *)prevObject;


//Enhanced group by
- (void)requestQuestionForLinkType:(NSString *)type withID:(NSString *)type_id withPage:(NSString *)pageNumber withKeyword:(NSString *)search_key withLimit:(NSString *)limit
                         dataBlock:(TestGuruDataBlock)dataBlock;
- (void)requestHeadersWithLinkType:(NSString *)type withPage:(NSString *)pageNumber dataBlock:(TestGuruDataBlock)dataBlock;


////GROUP BY TAGS
//- (void)requestQuestionTagsForUser:(NSString *)userid withPackageID:(NSString *)package_id  withPage:(NSString *)pageNumber dataBlock:(TestGuruDataBlock)dataBlock; // dead
//- (void)requestQuestionWithTagID:(NSString *)tag_id
//                   withPackageID:(NSString *)packageID withPage:(NSString *)pageNumber
//                       dataBlock:(TestGuruDataBlock)dataBlock; // dead

//- (void)requestQuestionTagsForPackage:(NSString *)package_id withCourseID:(NSString *)course_id withPage:(NSString *)pageNumber dataBlock:(TestGuruDataBlock)dataBlock;
//- (void)requestQuestionWithTagID:(NSString *)tag_id
//                   withPackageID:(NSString *)packageID withCourseID:(NSString *)course_id withPage:(NSString *)pageNumber
//                       dataBlock:(TestGuruDataBlock)dataBlock;

////GROUP BY QUESTION TYPE
//- (void)requestQuestionTypesForUser:(NSString *)userid withPackageID:(NSString *)package_id  withPage:(NSString *)pageNumber dataBlock:(TestGuruDataBlock)dataBlock;
//- (void)requestQuestionWithQuestionTypeID:(NSString *)question_type_id
//                            withPackageID:(NSString *)packageID withPage:(NSString *)pageNumber
//                                dataBlock:(TestGuruDataBlock)dataBlock;
//
////GROUP BY DIFFICULTY LEVEL
//- (void)requestDifficultyLevelsForUser:(NSString *)userid withPackageID:(NSString *)package_id  withPage:(NSString *)pageNumber dataBlock:(TestGuruDataBlock)dataBlock;
//- (void)requestQuestionWithDifficultyLevelID:(NSString *)difficulty_level_id
//                               withPackageID:(NSString *)packageID withPage:(NSString *)pageNumber
//                                   dataBlock:(TestGuruDataBlock)dataBlock;
//
////GROUP BY LEARNING SKILLS
//- (void)requestLearningSkillsForUser:(NSString *)userid withPackageID:(NSString *)package_id  withPage:(NSString *)pageNumber dataBlock:(TestGuruDataBlock)dataBlock;
//- (void)requestQuestionWithLearningSkillID:(NSString *)learning_skill_id
//                             withPackageID:(NSString *)packageID withPage:(NSString *)pageNumber
//                                 dataBlock:(TestGuruDataBlock)dataBlock;
//
////GROUP BY SHARED STATUS
//- (void)requestSharedStatusForUser:(NSString *)userid withPackageID:(NSString *)package_id  withPage:(NSString *)pageNumber dataBlock:(TestGuruDataBlock)dataBlock;
//- (void)requestQuestionWithSharedStatusID:(NSString *)shared_status_id
//                           withPackageID:(NSString *)packageID withPage:(NSString *)pageNumber
//                               dataBlock:(TestGuruDataBlock)dataBlock;

- (void)deepCopyManagedObject:(NSManagedObject *)object objectBlock:(TestGuruManagedObjectBlock)objectBlock;

- (void)uploadImageFiles:(NSArray *)imageList doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)uploadImageFiles:(NSArray *)imageList dataBlock:(TestGuruDataBlock)dataBlock;

/////// TEST BANK ///////
- (void)requestTestListForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestTestListForTagsForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestTestListForTestCategoryForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestTestListForTestTypeForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestTestListForGradeRationgForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock;

- (void)requestDetailsForTest:(NSManagedObject *)object doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestPreviewTest:(NSManagedObject *)object doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestRemoveTest:(NSManagedObject *)mo doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestUpdateTest:(NSManagedObject *)mo newItem:(BOOL)isNew doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestDeployTest:(NSDictionary *)parameter doneBlock:(TestGuruDoneBlock)doneBlock;

- (NSDictionary *)fetchDefaultValueForEntity:(NSString *)entity;

- (void)requestTestSectionListForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)insertQuestions:(NSSet *)list testObject:(NSManagedObject *)object doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)removeQuestions:(NSManagedObject *)mo doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)removeQuestions:(NSManagedObject *)mo withAttribute:(NSString *)key doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)updateTestObject:(NSManagedObject *)mo data:(NSDictionary *)dictionary newItem:(BOOL)isNew doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestListCountForUserWithID:(NSString *)userid fromEndPoint:(NSString *)endPoint listBlock:(TestGuruListBlock)listBlock;
- (void)saveDeployedTestSectionToCoreData:(NSDictionary *)data;

- (void)downloadImageFromQuestionObject:(NSManagedObject *)mo binaryBlock:(TestGuruBinaryBlock)binaryBlock;
//- (void)requestFilteredQuestionBankWithPage:(NSString *)pageNumber filterIDs:(NSArray *)arrayOfIDs dataBlock:(TestGuruDataBlock)dataBlock;

/////// TEST GURU CURRICULUM v2.2 ///////
- (void)requestTestGuruCurriculumListWithBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestTestGuruCurriculumOverviewForCurriculumID:(NSString *)curriculumid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestTestGuruLearningCompetenciesForPeriodID:(NSString *)periodid
                                              userData:(NSDictionary *)userData doneBlock:(TestGuruDoneBlock)doneBlock;

/////// TEST BANK v2.0 ///////
- (void)requestTestProperty:(NSString *)entityName endPoint:(NSString *)endPoint doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestTestDetail:(NSManagedObject *)testObject doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestCreateTest:(NSDictionary *)postBody testid:(NSString *)testid isNew:(BOOL)isNew doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestDeleteTest:(NSString *)testid doneBlock:(TestGuruDoneBlock)doneBlock;

- (NSManagedObject *)getObjectForEntity:(NSString *)entity withSortDescriptor:(NSString *)sortDescriptor andPredicate:(NSPredicate *)predicate;
- (void)insertQuestionToTest: (NSArray *)questions testid:(NSString *)testid;
- (BOOL)removeQuestionFromTest:(NSManagedObject *)questionObject;
- (void)createTestSortOptionDefaultSettingsForUser:(NSString *)user_id;
- (void)clearTestBankRelatedDataFromCoreData;

/////// TEST BANK v2.2 ///////
- (void)requestPaginatedTestListForCourse:(NSString *)course_id andUser:(NSString *)user_id withParametersForPagination:(NSDictionary *)settings dataBlock:(TestGuruDataBlock)dataBlock;
- (void)requestPaginatedTestGradingTypeListForUser:(NSString *)user_id withParametersForPagination:(NSDictionary *)parameters dataBlock:(TestGuruDataBlock)dataBlock;
- (void)requestTestDetailsForTest:(NSString *)testid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestCourseSectionListForUser:(NSString *)user_id doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestDeployForTest:(NSString *)testid sections:(NSArray *)sections doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestDeleteForTest:(NSString *)testid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestGradingTypes:(NSString *)pageNumber dataBlock:(TestGuruDataBlock)dataBlock;

- (void)insertNewTestWitContentBlock:(TestGuruContent)contentBlock;
- (void)updateObject:(NSManagedObject *)mo withDetails:(NSDictionary *)dictionary;
- (void)updateObjectsFromEntity:(NSString *)entity details:(NSDictionary *)dictionary predicate:(NSPredicate *)predicate;
- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate;
- (void)postTestWithUserData:(NSDictionary *)dictionary edit:(BOOL)mode doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)updateTest:(NSString *)entity predicate:(NSPredicate *)predicate details:(NSDictionary *)details;
- (void)assignTestListWithDoneBlock:(TestGuruDoneBlock)doneBlock;
- (void)assignTestListWithSet:(NSMutableSet *)setOfDicts doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)copyQuestionItemsFromTestInformation:(NSManagedObject *)object doneBlock:(TestGuruDoneBlock)doneBlock;

- (NSString *)formatStringNumber:(NSString *)stringNumber;

@end

