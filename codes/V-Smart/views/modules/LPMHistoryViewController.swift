//
//  LPMHistoryViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 28/06/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMHistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {
    
    @IBOutlet fileprivate var historyTableView: UITableView!
    @IBOutlet fileprivate var emptyPlaceholderView: UIView!
    @IBOutlet fileprivate var emptyPlaceholderLabel: UILabel!
    
    var lpid = ""
    
    fileprivate let kHistoryCellIdentifier = "history_cell_identifier"
    
    // MARK: - Data Manager
    
    fileprivate lazy var lessonPlanDataManager: LessonPlanDataManager = {
        let lpdm = LessonPlanDataManager.sharedInstance()
        return lpdm!
    }()
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.historyTableView.estimatedRowHeight = 100.0;
        self.historyTableView.rowHeight = UITableViewAutomaticDimension;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Empty Placeholder View
    
    func shouldShowEmptyPlaceholderView(_ show: Bool) {
        if (show) {
            let message = NSLocalizedString("No available history.", comment: "")   // unchecked
            self.emptyPlaceholderLabel.text = message
        }
        
        self.emptyPlaceholderView.isHidden = !show
        self.historyTableView.isHidden = show
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        self.shouldShowEmptyPlaceholderView(sectionData.numberOfObjects > 0 ? false : true)
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kHistoryCellIdentifier, for: indexPath) as! LPMHistoryTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: LPMHistoryTableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        
        let avatar = mo.value(forKey: "avatar") as! String
        let firstName = mo.value(forKey: "first_name") as! String
        let lastName = mo.value(forKey: "last_name") as! String
        let dateCreated = mo.value(forKey: "date_created") as! String
        let dateModified = mo.value(forKey: "date_modified") as! String
        
        cell.userAvatarImage.sd_setImage(with: URL.init(string: avatar))
        cell.userNameLabel.text = "\(firstName) \(lastName)"
        
        let dateString = self.transformDateString(dateModified, fromFormat: "yyyy-MM-dd HH:mm:ss zzz", toFormat: "MMM. dd, yyyy, hh:mm a")
        cell.timeStampLabel.text = (dateCreated == dateModified) ? dateString : "\(dateString) \(NSLocalizedString("Edited", comment: ""))"
        
        var history = mo.value(forKey: "history") as! String
        history = history.replacingOccurrences(of: "@#edited-", with: "")
        let stringComponents = history.components(separatedBy: ",")
        
        if (stringComponents.count > 0) {
            var historyContent = ""
            
            if (stringComponents.count > 1) {
                for component in stringComponents {
                    historyContent = historyContent + "\n\t* \(component)"
                }

                history = "\(NSLocalizedString("Edited the following fields", comment: "")): \(historyContent)"
            }
            else {
                historyContent = historyContent + "\(stringComponents[0])"
                history = "\(NSLocalizedString("Edited", comment: "")): \(historyContent)"
            }
        }
        
        cell.historyLabel.text = history
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        

        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = self.lessonPlanDataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: kHistoryEntity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kHistoryEntity)
        fetchRequest.fetchBatchSize = 20
        
        let predicate1 = self.lessonPlanDataManager.predicate(forKeyPath: "lp_id", andValue: self.lpid)
        let predicate2 = self.lessonPlanDataManager.predicate(forKeyPath: "lpc_is_deleted", andValue: "0")
        let predicate3 = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1!, predicate2!])
        fetchRequest.predicate = predicate3
        
        let sortDescriptor = NSSortDescriptor.init(key: "sorted_date_modified", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.historyTableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.historyTableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.historyTableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.historyTableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.historyTableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.historyTableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! LPMHistoryTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.historyTableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.historyTableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.historyTableView.endUpdates()
    }
    
    // MARK: - Class Helpers
    
    func transformDateString(_ string: String, fromFormat: String, toFormat: String) -> String {
        let formatter = DateFormatter.init()
        formatter.dateFormat = fromFormat
        formatter.timeZone = TimeZone.current
        
        let date = formatter.date(from: "\(string) GMT")
        formatter.dateFormat = toFormat
        
        return formatter.string(from: date!)
    }
    
}
