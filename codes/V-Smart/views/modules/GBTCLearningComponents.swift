//
//  GBTCLearningComponents.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 26/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTCLearningComponents: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerLabel: UILabel!
    
    var term_id = "1"
    
    fileprivate let cellIdentifier = "gbt_learning_panel_cell"
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>?
    
    fileprivate var blockOperations: [BlockOperation] = []
    var scrollDelegate : ScrollCommunicationDelegate?
    
    var isAscending = true
    var student_name = ""
    var sort_by_gender = false
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    var is_quarter = false
    
    fileprivate let notification = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNotification()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.teardownNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension GBTCLearningComponents: UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, NSFetchedResultsControllerDelegate {
    
    // MARK : - STUDENT SEARCH IMPLEMENTATION
    
    func reloadCollection(withTermID term_id: String, withHeaderName header_name: String) {
        self.term_id = term_id
        if is_quarter {
            self.headerLabel.text = "Grade Summary"
        } else {
            self.headerLabel.text = header_name
        }
        self.reloadFetchedResultsController()
    }
    
    func reloadCollection(isQuarter is_quarter: Bool, withHeaderName header_name: String) {
        //        self.term_id = term_id
        self.is_quarter = is_quarter
        if is_quarter {
            self.headerLabel.text = "Grade Summary"
        } else {
            self.headerLabel.text = header_name
        }
        reloadFetchedResultsController()
    }
    
    func setUpNotification() {
        notification.addObserver(self, selector: #selector(self.studentSearchAction(_:)), name: NSNotification.Name(rawValue: "GBTC_STUDENT_SEARCH"), object: nil)
        notification.addObserver(self, selector: #selector(self.studentSortAction(_:)), name: NSNotification.Name(rawValue: "GBTC_STUDENT_SORT"), object: nil)
    }
    
    func teardownNotification() {
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "GBTC_STUDENT_SEARCH"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "GBTC_STUDENT_SORT"), object: nil)
    }
    
    func studentSortAction(_ notification: Notification) {
        let sort_by_gender = notification.object as! Bool
        self.sort_by_gender = sort_by_gender
        self.reloadFetchedResultsController()
    }
    
    func studentSearchAction(_ notification: Notification) {
        var name = notification.object as? String
        name = (name == nil) ? "" : name
        self.student_name = name!
        self.reloadFetchedResultsController()
    }
    
    /// MARK : - COLLECTION VIEW DATASOURCE AND DELEGATE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! GBTLearningComponentCollectionCell
        
        let mo = self.fetchedResultsController.object(at: indexPath) 
        let text = mo.value(forKey: "quarterly_grade") as! String
        cell.backgroundColor = UIColor.white
        cell.customLabel.textColor = UIColor.black
        cell.customLabel.font = UIFont(name: "Helvetica", size: 17)
        if (indexPath as NSIndexPath).section == 0 {
            cell.backgroundColor = UIColor(hex6: 0xecf0f1)
            cell.customLabel.font = UIFont(name: "Helvetica-Bold", size: 17)
            cell.customLabel.text = text
        } else {
            cell.backgroundColor = UIColor.white
            cell.customLabel.font = UIFont(name: "Helvetica", size: 17)
            cell.customLabel.text = "\(text)%"
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let mo = self.fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject
//        
//        let totalindex = collectionView.numberOfItemsInSection(indexPath.section) - 3
//        if (indexPath.section == 0) && (indexPath.row < totalindex) {
//            let cell = self.collectionView.cellForItemAtIndexPath(indexPath)!
//            showFilter(cell, mo: mo)
//        }
//        
//        let is_grade = self.gbdm.stringValue(mo.valueForKey("is_grade"))
//        if is_grade == "0" {
//            print("DONT SHOW POPOVER!!!!!!!!")
//            
//        } else {
//            //testing purposes
//            let popover: UIPopoverPresentationController = self.scoreMenu.popoverPresentationController!
//            popover.sourceRect = (self.collectionView.cellForItemAtIndexPath(indexPath)?.bounds)!
//            popover.sourceView = self.collectionView.cellForItemAtIndexPath(indexPath)
//            popover.delegate = self
//            presentViewController(self.scoreMenu, animated: true, completion:nil)
//            print("SHOW POPOVER!!!!!!!!")
//        }
    }
    
    func adaptivePresentationStyleForPresentationController(_ controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    // MARK: - Fetched Results Controller
//    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = gbdm.getMainContext()
        let ascending = self.isAscending
        let entity = GBTConstants.Entity.GRADESUMMARYGRADE
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
//        let fetchRequest = NSFetchRequest(entityName: entity)
        
        fetchRequest.fetchBatchSize = 20
        
        var finalPredicate = [NSPredicate]()
        if self.student_name.characters.count > 0 {
            let predicateName = NSComparisonPredicate(keyPath: "name", withValue: self.student_name, isExact: false)
            let predicateHeader = NSComparisonPredicate(keyPath: "name", withValue: "AA_AA_AA_AA_00", isExact: false)
            
            let namePredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicateName, predicateHeader])
            
            finalPredicate.append(namePredicate)
        }
        
        if self.is_quarter {
            let predicateTerm = NSComparisonPredicate(keyPath: "for_quarter", withValue: "1", isExact: true)
            finalPredicate.append(predicateTerm)
        } else {
            let predicateTerm = NSComparisonPredicate(keyPath: "term_id", withValue: self.term_id, isExact: true)
            finalPredicate.append(predicateTerm)
        }
        
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: finalPredicate)
        
        //TODO FILTER SORT SEGMENTED CONTROL
        var section_key_path = "name"
        
        let sortIndex = NSSortDescriptor(key: "index", ascending: ascending)
        
        
        let descriptorSelector = #selector(NSString.localizedCompare(_:))
        if self.sort_by_gender {
            let sortGender = NSSortDescriptor(key: "gender", ascending: ascending, selector:descriptorSelector)
            fetchRequest.sortDescriptors = [sortGender, sortIndex]
            section_key_path = "gender"
        } else {
            let sortName = NSSortDescriptor(key: "name", ascending: ascending, selector:descriptorSelector)
            fetchRequest.sortDescriptors = [sortName, sortIndex]
            section_key_path = "name"
        }
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                   managedObjectContext: ctx!,
                                                                   sectionNameKeyPath: section_key_path,
                                                                   cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch _ as NSError {
            //            error = error1
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //println("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    fileprivate func addProcessingBlock(_ processingBlock:@escaping (Void)->Void) {
        blockOperations.append(BlockOperation(block: processingBlock))
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        blockOperations.removeAll(keepingCapacity: false)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
            
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.insertItems(at: [newIndexPath]) }
            
        case .update:
            guard let newIndexPath = newIndexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.reloadItems(at: [newIndexPath]) }
            
        case .move:
            guard let indexPath = indexPath else { return }
            guard let newIndexPath = newIndexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.moveItem(at: indexPath, to: newIndexPath) }
            
        case .delete:
            guard let indexPath = indexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.deleteItems(at: [indexPath]) }
            
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
            
        case .insert:
            addProcessingBlock { [weak self] in self?.collectionView.insertSections(IndexSet(integer: sectionIndex)) }
            
        case .update:
            addProcessingBlock { [weak self] in self?.collectionView.reloadSections(IndexSet(integer: sectionIndex)) }
            
        case .delete:
            addProcessingBlock { [weak self] in self?.collectionView.deleteSections(IndexSet(integer: sectionIndex)) }
            
        default: break
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView.performBatchUpdates({
            self.blockOperations.forEach { $0.start() }
            }, completion: { finished in
                self.blockOperations.removeAll(keepingCapacity: false)
        })
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.collectionView!.reloadData()
        
        do {
            try self.fetchedResultsController.performFetch()
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y;
        self.scrollDelegate?.gbScrollViewDidScroll(GBPanelType.LearningPanel, scrollTo: offsetY)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewWillBeginDragging(GBPanelType.LearningPanel)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.LearningPanel)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate == false {
            self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.LearningPanel)
        }
    }
}
