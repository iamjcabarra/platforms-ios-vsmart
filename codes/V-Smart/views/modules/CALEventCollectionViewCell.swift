//
//  CALEventCollectionViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 13/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class CALEventCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var eventColorView: UIView!
    @IBOutlet var eventActionImage: UIImageView!
    @IBOutlet var eventActionButton: UIButton!
    @IBOutlet var eventLabel: UILabel!
    @IBOutlet var eventStartDateLabel: UILabel!
    
}
