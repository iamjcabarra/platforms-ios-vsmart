 //
//  FolderViewController.m
//  V-Smart
//
//  Created by VhaL on 2/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NoteBookViewController.h"
#import "NoteBookCollectionViewController.h"
#import <Parse/Parse.h>

#define kMaxLimit 20
#define kRowHeight 44

@interface NoteBookViewController ()
@property (nonatomic, strong) NoteBookCollectionViewController *otherView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@end

@implementation NoteBookViewController
@synthesize noteBookArray, textField, parent, delegate, selectedFolder, clearButton, otherView, refreshControl;

-(void)viewDidLoad{
    [super viewDidLoad];
    self.tableView.layer.borderWidth = 1;
    self.tableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    UINib *cellNib = [UINib nibWithNibName:@"NoteBookCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"NoteBookCell"];
    
    [self loadData];
    [self addRefreshMode];
}

-(void)addRefreshMode{
    self.refreshControl = [[UIRefreshControl alloc] init];
    NSString *pullRefresh = NSLocalizedString(@"Pull to Refresh",nil);
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:pullRefresh];
    [self.refreshControl addTarget:self action:@selector(refreshTriggered) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshControl];
}

-(void)refreshTriggered{
    [self loadData];
    [otherView loadData];
    [self.refreshControl endRefreshing];
}

-(void)castOtherController{
    otherView = (NoteBookCollectionViewController*)self.otherNoteBookViewController;
}

-(void)loadData{

    NSString *code = [Utils getSettingsSchoolCode];
    NSString *user = [VSmart sharedInstance].account.user.username;
    
    if (user == nil) {
        user = [VSmart sharedInstance].account.user.email;
    }
    
    NSLog(@"code: %@", code);
    NSLog(@"user: %@", user);

    if (code != nil && user != nil) {
        NSLog(@"Can track notes event");
    NSDictionary *dimensions = @{@"user": user, @"code": code};
    [PFAnalytics trackEvent:@"notes" dimensions:dimensions];
    }
    
    noteBookArray = [NSMutableArray arrayWithArray:[[NoteDataManager sharedInstance] getNoteBooks:NO useMainThread:YES]];
    [self moveGeneralNotebookToFirst];
    [self.tableView reloadData];
}

-(void)moveGeneralNotebookToFirst{
    NoteBook *notebookGeneral;
    for (NoteBook *notebook in noteBookArray) {
        if ([notebook.noteBookName isEqualToString:kNotebookGeneralName]){
            notebookGeneral = notebook;
            break;
        }
    }
    
    if (notebookGeneral){
        [noteBookArray removeObject:notebookGeneral];
        [noteBookArray insertObject:notebookGeneral atIndex:0];
    }
}

-(IBAction)buttonAddTapped:(id)sender{
    [self viewAllNoteBooksButtonTapped:nil];
    
    NoteBook *result = [[NoteDataManager sharedInstance] addNoteBook:textField.text withColor:0 useMainThread:YES];
    
    [self reflectAddedNoteBookInView:result];
    [otherView reflectAddedNoteBookInView:result];
    
    textField.text = @"";
    [textField resignFirstResponder];
}

-(IBAction)buttonEditNoteBookTapped:(NoteBookCell*)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    NoteBook *noteBook = [noteBookArray objectAtIndex:indexPath.row];
    
    noteBook.noteBookName = [sender.noteBookTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    noteBook.modified = [NSDate date];
    
    [[NoteDataManager sharedInstance] editNoteBook:noteBook useMainThread:YES];
    
    [self reflectModifiedNoteBookInView:noteBook inIndexPath:indexPath];
    [otherView reflectModifiedNoteBookInView:noteBook inIndexPath:[NSIndexPath indexPathForRow:(indexPath.row + 1) inSection:0]];
}

-(void)reflectAddedNoteBookInView:(NoteBook*)newNoteBook{
    [noteBookArray addObject:newNoteBook];
    
    NSSortDescriptor *sortByNoteBookName = [[NSSortDescriptor alloc] initWithKey:@"noteBookName" ascending:YES];
    noteBookArray = [NSMutableArray arrayWithArray:[noteBookArray sortedArrayUsingDescriptors:@[sortByNoteBookName]]];
    
    [self moveGeneralNotebookToFirst];
    
    [self.tableView reloadData];
}

-(void)reflectModifiedNoteBookInView:(NoteBook*)modifiedNoteBook inIndexPath:(NSIndexPath*)indexPath{
    
    NoteBookCell *cell = (NoteBookCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.noteBookLabel.text = modifiedNoteBook.noteBookName;
    cell.selectedColorId = modifiedNoteBook.colorId.intValue;
    [cell updateNotebookColor];
    [cell hideShareIndicator:![modifiedNoteBook.isShared boolValue]];
}

-(void)reflectDeletedNoteBookInView:(NSIndexPath*)indexPath{
    [noteBookArray removeObjectAtIndex:indexPath.row];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
}

-(IBAction)viewAllNoteBooksButtonTapped:(id)sender{
    
    selectedFolder = nil;
    
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
    
    if ([delegate respondsToSelector:@selector(buttonFolderApplyTapped:)]){
        [delegate performSelector:@selector(buttonFolderApplyTapped:) withObject:selectedFolder];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self buttonAddTapped:nil];
    [self.textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)text{
    if(text.length == 0){
        if(self.textField.text.length != 0)
            return YES;
    }
    else if(kMaxLimit <= self.textField.text.length){
        return NO;
    }
    return YES;
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kRowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [noteBookArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"NoteBookCell";
    NoteBookCell *cell = (NoteBookCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        cell = [[NoteBookCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NoteBookCell __weak *weakCell = cell;
    
    [cell setAppearanceWithBlock:^{
        weakCell.leftUtilityButtons = [self leftButtons];
        weakCell.rightUtilityButtons = [self rightButtons];
        weakCell.delegate = self;
        weakCell.containingTableView = self.tableView;
    } force:NO];
    
    [cell setCellHeight:cell.frame.size.height];
    [cell initializeCell];
    cell.delegate = self;
    
    NoteBook *folder = [noteBookArray objectAtIndex:indexPath.row];
    
    cell.noteBookLabel.text = folder.noteBookName;
    cell.noteBookTextField.text = folder.noteBookName;
    
    cell.selectedColorId = folder.colorId.intValue;
    [cell updateNotebookColor];
    
    if ([folder.isShared boolValue])
        [cell hideShareIndicator:NO];
    else
        [cell hideShareIndicator:YES];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [textField resignFirstResponder];
    
    selectedFolder = [noteBookArray objectAtIndex:indexPath.row];
    
    if ([delegate respondsToSelector:@selector(buttonFolderApplyTapped:)]){
        [delegate performSelector:@selector(buttonFolderApplyTapped:) withObject:selectedFolder withObject:[NSNumber numberWithBool:YES]];
    }
    
//    SWTableViewCell *cell = (SWTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
}

#pragma mark - SWTableViewDelegate

- (NSArray *)rightButtons{
    UIImage *copyImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_flyout_rename.png"] scaledToSize:CGSizeMake(15, 15)];
//    UIImage *shareImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_notes_share.png"] scaledToSize:CGSizeMake(15, 15)];
    UIImage *trashImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_notes_trash.png"] scaledToSize:CGSizeMake(15, 15)];
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0] icon:copyImg];
//    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:0] icon:shareImg];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0] icon:trashImg];
//    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0] title:@""];
    return rightUtilityButtons;
}

- (NSArray *)leftButtons{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:0.07 green:0.75f blue:0.16f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"check.png"]];
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:1.0f green:1.0f blue:0.35f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"clock.png"]];
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:1.0f green:0.231f blue:0.188f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"cross.png"]];
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:0.55f green:0.27f blue:0.07f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"list.png"]];
    
    return leftUtilityButtons;
}

-(void)swipeableTableViewCell:(NoteBookCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            NSLog(@"left button 0 was pressed");
            break;
        case 1:
            NSLog(@"left button 1 was pressed");
            break;
        case 2:
            NSLog(@"left button 2 was pressed");
            break;
        case 3:
            NSLog(@"left btton 3 was pressed");
        default:
            break;
    }
}

-(void)swipeableTableViewCell:(NoteBookCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    NoteBook *noteBook = [noteBookArray objectAtIndex:indexPath.row];
    
    switch (index) {
        case 0: //rename
        {
            if ([noteBook.noteBookName isEqualToString:kNotebookGeneralName]){
                

                /* localizable strings */
                NSString *cannotRename = NSLocalizedString(@"cannot be renamed", nil); //checked
                [self.view makeToast:cannotRename duration:2.0f position:@"center"];
                
                [cell hideUtilityButtonsAnimated:YES];
                return;
            }
            
            [cell hideEditControls:NO];
            [cell hideUtilityButtonsAnimated:YES];
            break;
        }
//        case 1: //share
//        {
//            noteBook.isShared = [NSNumber numberWithBool:![noteBook.isShared boolValue]];
//            
//            if (![[NoteDataManager sharedInstance] shareFolder:noteBook isShared:[noteBook.isShared boolValue] useMainThread:YES])
//                return;
//            
//            [cell hideUtilityButtonsAnimated:YES];
//            
//            [self reflectModifiedNoteBookInView:noteBook inIndexPath:indexPath];
//            [otherView reflectModifiedNoteBookInView:noteBook inIndexPath:[NSIndexPath indexPathForRow:(indexPath.row+1) inSection:0]];
//            
//            break;
//        }
        case 1: //delete
        {
            if ([noteBook.noteBookName isEqualToString:kNotebookGeneralName]){
                
                /* localizable strings */
                NSString *cannotDelete = NSLocalizedString(@"cannot be deleted", nil); //checked
                [self.view makeToast:cannotDelete duration:2.0f position:@"center"];
                
                [cell hideUtilityButtonsAnimated:YES];
                return;
            }
            
            bool deleteSuccess = [[NoteDataManager sharedInstance] deleteNoteBook:noteBook useMainThread:YES];
            
            if (deleteSuccess){
                [self reflectDeletedNoteBookInView:indexPath];
                [otherView reflectDeletedNoteBookInView:[NSIndexPath indexPathForRow:(indexPath.row+1) inSection:0]];
            }
            
            break;
        }
        default:
            break;
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(NoteBookCell *)cell {
    return YES;
}


@end
