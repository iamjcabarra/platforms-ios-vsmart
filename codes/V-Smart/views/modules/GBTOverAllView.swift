//
//  GBTOverAllGradesView.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 03/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTOverAllView: UIViewController, UIPopoverPresentationControllerDelegate {
    
    var studentPanel:GBTOverAllGradesStudentListPanel!
    var learningComponentPanel:GBTOverAllGradesView!
    
    var scrollingView : UIScrollView?
    
    @IBOutlet weak var consolidatedButton: UIButton!
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.consolidatedButton.addTarget(self, action: #selector(self.consolidatedButtonAction(_:)), for: .touchUpInside)
    }
    
    func consolidatedButtonAction(_ button: UIButton) {
        DispatchQueue.main.async {
            _ = self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        if segue.identifier == "GBT_OVERALL_STUDENT_LIST_PANEL_EMBED" {
            self.studentPanel = segue.destination as! GBTOverAllGradesStudentListPanel
            self.studentPanel.scrollDelegate = self
        }
        
        if segue.identifier == "GBT_OVERALL_GRADE_VIEW_EMBED" {
            self.learningComponentPanel = segue.destination as! GBTOverAllGradesView
            self.learningComponentPanel.scrollDelegate = self
        }
        
    }
}



extension GBTOverAllView : ScrollCommunicationDelegate {
    
    func gbScrollViewDidScroll(_ panel: GBPanelType, scrollTo: CGFloat) {
        if self.scrollingView == self.studentPanel.tableView {
            let offSetX = self.learningComponentPanel.collectionView.contentOffset.x
            self.learningComponentPanel.collectionView.setContentOffset(CGPoint(x: offSetX, y: scrollTo), animated: false)
        }
        
        if self.scrollingView == self.learningComponentPanel?.collectionView {
            self.studentPanel.tableView.setContentOffset(CGPoint(x: 0, y: scrollTo), animated: false)
        }
    }
    
    func gbScrollViewWillBeginDragging(_ panel: GBPanelType) {
        
        if self.scrollingView == nil {
            switch panel {
            case .StudentPanel :
                self.scrollingView = self.studentPanel.tableView
                self.learningComponentPanel.collectionView.isUserInteractionEnabled = false
            case .LearningPanel :
                self.learningComponentPanel.collectionView.isUserInteractionEnabled = false
                self.studentPanel.tableView.isUserInteractionEnabled = false
            default :
                print("NO SCOLL RIGHT PANEL")
            }
        }
    }
    
    func gbScrollViewDidEndDecelerating(_ panel: GBPanelType) {
        self.scrollingView = nil
        self.learningComponentPanel.collectionView.isUserInteractionEnabled = true
        self.studentPanel.tableView.isUserInteractionEnabled = true
    }
}
