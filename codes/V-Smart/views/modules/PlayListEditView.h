//
//  PlayListEditView.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/20/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PlayListEditDelegate <NSObject>
@optional
- (void)didFinishUpdate:(BOOL)status;
@end

@interface PlayListEditView : UIViewController
@property (nonatomic, weak) id <PlayListEditDelegate> delegate;
@property (nonatomic, strong) NSManagedObject *document;
@property (nonatomic, assign) BOOL isTeacherMode;
@end
