//
//  CPCourseCellItem.h
//  V-Smart
//
//  Created by Julius Abarra on 28/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPCourseCellItem : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *courseNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *courseSectionLabel;
@property (strong, nonatomic) IBOutlet UILabel *courseScheduleLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseDescription;

@end