//
//  CoursePlayerQuizSheetCell.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 29/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionSamplerPageHeader.h"
#import <AssetsLibrary/AssetsLibrary.h>

@protocol CoursePlayerQuizSheetCellDelegate <NSObject>
@required
- (void)transitionWithData:(NSDictionary *)previewData index:(NSInteger)index segueID:(NSString *)segueIdentifier;
@end

@interface CoursePlayerQuizSheetCell : UICollectionViewCell

- (void)displayObject:(NSManagedObject *)object;

@property (nonatomic, weak) id <CoursePlayerQuizSheetCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *questionAndImageTableHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *choiceTableHeight;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property (strong, nonatomic) NSString *questionID;
@property (assign, nonatomic) NSInteger number_of_correct_answers;


- (void)reloadFetchedResultsController;

@end
