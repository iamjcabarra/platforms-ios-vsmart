//
//  SchoolStreamFeedCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 2/13/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#define MAS_SHORTHAND
#import "Masonry.h"

#import "SchoolStreamFeedCell.h"

@interface SchoolStreamFeedCell()
@property (strong, nonatomic) UILabel *labelImage;
@end

@implementation SchoolStreamFeedCell

- (void)awakeFromNib {
    // Initialization code
    NSLog(@"awake on nib custom cell");
    
    self.labelImage = [UILabel new];
    self.labelImage.textColor = [UIColor whiteColor];
    self.labelImage.font = [UIFont systemFontOfSize:25];
    self.labelImage.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.labelImage];
    [self.labelImage makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.avatarImage);
    }];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)showAlias {
    
    self.labelImage.text = [NSString stringWithFormat:@"%@%@",[self.firstName substringToIndex:1],[self.lastName substringToIndex:1]];
    UIImage *img = nil;
    UIGraphicsBeginImageContext(self.labelImage.bounds.size);
    [self.labelImage.layer renderInContext:UIGraphicsGetCurrentContext()];
    img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.avatarImage.image = img;
    
    [self setNeedsDisplay];
}

@end
