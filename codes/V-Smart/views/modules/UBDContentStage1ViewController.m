//
//  UBDContentStage1ViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/25/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "UBDContentStage1ViewController.h"
#import "UBDContentManager.h"

@interface UBDContentStage1ViewController () <UITextViewDelegate>

@property (strong, nonatomic) UBDContentManager *sharedUBDContentManager;

@property (strong, nonatomic) IBOutlet UILabel *lblContentTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblStage1P1;
@property (strong, nonatomic) IBOutlet UILabel *lblStage1P2;
@property (strong, nonatomic) IBOutlet UILabel *lblStage1P3;
@property (strong, nonatomic) IBOutlet UILabel *lblStage1P4;
@property (strong, nonatomic) IBOutlet UILabel *lblStage1P5;
@property (strong, nonatomic) IBOutlet UILabel *lblStage1P6;
@property (strong, nonatomic) IBOutlet UILabel *lblStage1P7;

@property (strong, nonatomic) IBOutlet UITextView *txtStage1P1;
@property (strong, nonatomic) IBOutlet UITextView *txtStage1P2;
@property (strong, nonatomic) IBOutlet UITextView *txtStage1P3;
@property (strong, nonatomic) IBOutlet UITextView *txtStage1P4;
@property (strong, nonatomic) IBOutlet UITextView *txtStage1P5;
@property (strong, nonatomic) IBOutlet UITextView *txtStage1P6;
@property (strong, nonatomic) IBOutlet UITextView *txtStage1P7;

@end

@implementation UBDContentStage1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Use singleton class shared instance
    self.sharedUBDContentManager = [UBDContentManager sharedInstance];
    
    // To dismiss keyboard when touching outside of text fields
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    // Set delegates
    self.txtStage1P1.delegate = self;
    self.txtStage1P2.delegate = self;
    self.txtStage1P3.delegate = self;
    self.txtStage1P4.delegate = self;
    self.txtStage1P5.delegate = self;
    self.txtStage1P6.delegate = self;
    self.txtStage1P7.delegate = self;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self updateContentManager];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Get data from singleton class and render them to text fields
    self.txtStage1P1.text = [self.sharedUBDContentManager.contentStage objectForKey:@"1"];
    self.txtStage1P2.text = [self.sharedUBDContentManager.contentStage objectForKey:@"2"];
    self.txtStage1P3.text = [self.sharedUBDContentManager.contentStage objectForKey:@"3"];
    self.txtStage1P4.text = [self.sharedUBDContentManager.contentStage objectForKey:@"4"];
    self.txtStage1P5.text = [self.sharedUBDContentManager.contentStage objectForKey:@"5"];
    self.txtStage1P6.text = [self.sharedUBDContentManager.contentStage objectForKey:@"6"];
    self.txtStage1P7.text = [self.sharedUBDContentManager.contentStage objectForKey:@"7"];
    
    // Keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Keyboard notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Handle Keyboard Movements

- (void)keyboardWillShow:(NSNotification *)notification {
    if ([self.txtStage1P7 isFirstResponder]) {
        CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        float newVerticalPosition = -keyboardSize.height;
        [self moveFrameToVerticalPosition:newVerticalPosition forDuration:0.3f];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [self moveFrameToVerticalPosition:0.0f forDuration:0.3f];
}

- (void)moveFrameToVerticalPosition:(float)position forDuration:(float)duration {
    CGRect frame = self.view.frame;
    frame.origin.y = position;
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Update Content Manager

- (void)updateContentManager {
    // Get input data from text fields
    NSString *stage1P1 = self.txtStage1P1.text;
    NSString *stage1P2 = self.txtStage1P2.text;
    NSString *stage1P3 = self.txtStage1P3.text;
    NSString *stage1P4 = self.txtStage1P4.text;
    NSString *stage1P5 = self.txtStage1P5.text;
    NSString *stage1P6 = self.txtStage1P6.text;
    NSString *stage1P7 = self.txtStage1P7.text;
    
    // Set data to singleton class collection property
    [self.sharedUBDContentManager.contentStage setObject:stage1P1 forKey:@"1"];
    [self.sharedUBDContentManager.contentStage setObject:stage1P2 forKey:@"2"];
    [self.sharedUBDContentManager.contentStage setObject:stage1P3 forKey:@"3"];
    [self.sharedUBDContentManager.contentStage setObject:stage1P4 forKey:@"4"];
    [self.sharedUBDContentManager.contentStage setObject:stage1P5 forKey:@"5"];
    [self.sharedUBDContentManager.contentStage setObject:stage1P6 forKey:@"6"];
    [self.sharedUBDContentManager.contentStage setObject:stage1P7 forKey:@"7"];
}

#pragma mark - Text View Delegate

- (void)textViewDidChange:(UITextView *)textView {
    [self updateContentManager];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self moveFrameToVerticalPosition:0.0f forDuration:0.3f];
}

@end
