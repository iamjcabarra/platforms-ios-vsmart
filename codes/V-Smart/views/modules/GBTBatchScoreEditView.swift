//
//  GBTBatchScoreEditView.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 09/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

//import CoreData

class GBTBatchScoreEditView: UIViewController {
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var componentLbl: UILabel!
    @IBOutlet weak var testNameLbl: UILabel!
    @IBOutlet weak var highestScoreLbl: UILabel!
    
    var dataDict: [String:AnyObject]?
    var editData: [[String : AnyObject]?]?
    var highest_score: String!
//    private var _fetchedResultsController: NSFetchedResultsController? = nil
    
    fileprivate let notification = NotificationCenter.default
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cancelButton.addTarget(self, action: #selector(self.cancelButtonAction(_:)), for: .touchUpInside)
        self.saveButton.addTarget(self, action: #selector(self.saveButtonAction(_:)), for: .touchUpInside)
        displayData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cancelButtonAction(_ b: UIButton) {
//        self.navigationController?.popViewControllerAnimated(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func saveButtonAction(_ b: UIButton) {
        if self.editData != nil {
            let data = self.correctArray()
            self.gbdm.changeGradebookScore(withData: data) { (done, error) in
                if error == nil {
                    let component_id =  self.dataDict!["component_id"]! as! String
                    let term_id =  self.dataDict!["term_id"]! as! String
                    
                    let dict: [String:String] = ["term_id": term_id,
                                                 "component_id": component_id]
                    self.notification.post(name: Notification.Name(rawValue: "GBT_RELOAD_GRADEBOOK_CONTENT"), object: dict)
                    //                        self.delegate?.didEndChangeScore(true)
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.displayAlert(withTitle: "Error", withMessage: error!)
                }
            }
        }
        
        
        
        
    }
    
    func correctArray() -> [[String:AnyObject]?] {
        var array = [[String:AnyObject]!]()
        
        for data in self.editData! {
            let qid = self.gbdm.stringValue(data?["quiz_id"])
            let score = self.gbdm.stringValue(data?["actual_result"])
            let participant_id = self.gbdm.stringValue(data?["participant_id"])
            
            let dict: [String:String] = ["qid": qid!,
                                         "score": score!,
                                         "participant_id": participant_id!]
            array.append(dict as [String : AnyObject]!)
        }
        
        return array
//        let qid = self.gbdm.stringValue(self.edit)
        
        
        
    }
    
    func displayAlert(withTitle title: String, withMessage message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func displayData() {
        let index =  self.dataDict!["index"]! as! Double
        let component_id =  self.dataDict!["component_id"]! as! String
        let term_id =  self.dataDict!["term_id"]! as! String
        
//        let componentData = self.gbdm.fetchComponentName(forComponentID: component_id, inTermID: term_id)
        self.gbdm.fetchComponentName(forComponentID: component_id, inTermID: term_id) { (listBlock) in
            DispatchQueue.main.async(execute: {
                if listBlock?.count > 0 {
                    let test_name = self.gbdm.stringValue(listBlock?.last!["name"])
                    self.componentLbl.text = test_name
                }
            })
        }

        
//        let testData = self.gbdm.fetchTestName(forComponentID: component_id, inTermID: term_id, withIndex: index)
        self.gbdm.fetchTestName(forComponentID: component_id, inTermID: term_id, withIndex: index) { (listBlock) in
            DispatchQueue.main.async(execute: { 
                if listBlock?.count > 0 {
                    let test_name = self.gbdm.stringValue(listBlock?.last!["actual_result"])
                    self.testNameLbl.text = test_name
                }
            })
            
        }
        
        
//        let highestData = self.gbdm.fetchHighestScore(forComponentID: component_id, inTermID: term_id, withIndex: index, com)
        self.gbdm.fetchHighestScore(forComponentID: component_id, inTermID: term_id, withIndex: index) { (listBlock) in
            DispatchQueue.main.async(execute: { 
                if listBlock?.count > 0 {
                    var highest_score = self.gbdm.stringValue(listBlock?.last!["actual_result"])
                    highest_score = self.gbdm.formatStringNumber(highest_score!)
                    self.highestScoreLbl.text = highest_score
                    self.highest_score = highest_score
                }
            })
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension GBTBatchScoreEditView: UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UITextFieldDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard editData!.count > 0 else {
            return 0
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.editData?.count)!
        
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GBTBatchScoreEditCell", for: indexPath)
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ cell: UITableViewCell, atIndexPath: IndexPath) {
        let dict = self.editData![(atIndexPath as NSIndexPath).row]
        let name = self.gbdm.stringValue(dict?["name"])
        var actual_result = self.gbdm.stringValue(dict?["actual_result"])
        actual_result = self.gbdm.formatStringNumber(actual_result!)
//        let mo = fetchedResultsController.objectAtIndexPath(atIndexPath) as! NSManagedObject
//        var actual_result = self.gbdm.stringValue(mo.valueForKey("actual_result"))
//        let name = self.gbdm.stringValue(mo.valueForKey("name"))
        let eCell = cell as! GBTBatchScoreEditCell
//
//        
//        actual_result = self.gbdm.formatStringNumber(actual_result)
//        
////
//        //        let keyPath = optionType.keyPath()
//        //        guard
//        //            let name = mo.valueForKey(keyPath) as? String else { return }
        eCell.editScoreTextField!.text = actual_result
        eCell.studentNameLbl.text = name
        eCell.editScoreTextField.delegate = self
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        let mo = fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject
        //
        //        let column = optionType.columns()
        //        let keyNameValue = mo.valueForKey( column.keyName ) as! String
        //        let keyIdValue = mo.valueForKey( column.keyID ) as! String
        //
        //        var data:[String:AnyObject] = ["name":keyNameValue,"id":keyIdValue]
        //
        //        // supply user data
        //        if userData != nil {
        //            for (key, value) in userData! {
        //                data[key] = "\(value)"
        //            }
        //        }
        //
        //        self.delagate?.didFinishSelecting(self.optionType, data: data)
        //
        //        defer {
        //            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        //            self.dismissViewControllerAnimated(true, completion: nil)
        //        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let countdots = textField.text!.components(separatedBy: ".").count - 1
        
        if countdots > 0 && string == "." {
            return false
        }
        
        // Create an `NSCharacterSet` set which includes everything *but* the digits
        let inverseSet = CharacterSet(charactersIn:"0123456789.").inverted
        
        // At every character in this "inverseSet" contained in the string,
        // split the string up into components which exclude the characters
        // in this inverse set
        let components = string.components(separatedBy: inverseSet)
        
        // Rejoin these components
        let filtered = components.joined(separator: "")  // use join("", components) if you are using Swift 1.2
        
        let is_valid = string == filtered
        //        if Double(string)
        let cell: GBTBatchScoreEditCell = textField.superview!.superview as! GBTBatchScoreEditCell
        let indexPath = self.tableView.indexPath(for: cell)
        
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        
        if String(txtAfterUpdate).characters.count > 0 && is_valid {
            if Double(txtAfterUpdate as String)! > Double(self.highest_score) {
//                is_valid = false
                textField.text = self.highest_score
                self.editData![((indexPath as NSIndexPath?)?.row)!]?["actual_result"]! = self.highest_score as AnyObject
                return false
            }
        }
        
        if is_valid {
            self.editData![((indexPath as NSIndexPath?)?.row)!]?["actual_result"]! = txtAfterUpdate
        }
        
        // If the original string is equal to the filtered string, i.e. if no
        // inverse characters were present to be eliminated, the input is valid
        // and the statement returns true; else it returns false
        return is_valid
        
//        let countdots = textField.text!.componentsSeparatedByString(".").count - 1
//        
//        if countdots > 0 && string == "." {
//            return false
//        }
//        
//        // Create an `NSCharacterSet` set which includes everything *but* the digits
//        let inverseSet = NSCharacterSet(charactersInString:"0123456789.").invertedSet
//        
//        // At every character in this "inverseSet" contained in the string,
//        // split the string up into components which exclude the characters
//        // in this inverse set
//        let components = string.componentsSeparatedByCharactersInSet(inverseSet)
//        
//        // Rejoin these components
//        let filtered = components.joinWithSeparator("")  // use join("", components) if you are using Swift 1.2
//        
//        // If the original string is equal to the filtered string, i.e. if no
//        // inverse characters were present to be eliminated, the input is valid
//        // and the statement returns true; else it returns false
//        let isValid = string == filtered
//        
//        if isValid {
//            let cell: GBTBatchScoreEditCell = textField.superview!.superview as! GBTBatchScoreEditCell
//            let indexPath = self.tableView.indexPathForCell(cell)
//            
//            var txtAfterUpdate:NSString = textField.text! as NSString
//            txtAfterUpdate = txtAfterUpdate.stringByReplacingCharactersInRange(range, withString: string)
//            
//            print("ito row \(self.editData![(indexPath?.row)!]["actual_result"])")
//            self.editData![(indexPath?.row)!]["actual_result"]! = txtAfterUpdate
//            print("ito edit \(self.editData![(indexPath?.row)!]["actual_result"])")
//            
//        }
//
//        return isValid
    }
    
    
//    func textField(textField: UITextField!, shouldChangeCharactersInRange range: NSRange, replacementString string: String!) -> Bool {
//        var txtAfterUpdate:NSString = textField.text! as NSString
//        txtAfterUpdate = txtAfterUpdate.stringByReplacingCharactersInRange(range, withString: string)
//        
//        self.callMyMethod(txtAfterUpdate)
//        return true
//    }
    
    
    // MARK: - Fetched Results Controller
    
//    private var fetchedResultsController: NSFetchedResultsController {
//        if _fetchedResultsController != nil {
//            return _fetchedResultsController!
//        }
//        
//        let ctx = gbdm.getMainContext()
//        let ascending = true
//        let entity = GBTConstants.Entity.STUDENTGRADES
//        let keyPath = "name"
//        
//        
////                let dataDict: [String:AnyObject] = ["quiz_id":quiz_id,
////                                                    "component_id":self.component_id,
////                                                    "term_id":self.term_id,
////                                                    "index":index]
//        
//        let index =  self.dataDict!["index"]!
//        let component_id =  self.dataDict!["component_id"]!
//        let term_id =  self.dataDict!["term_id"]!
//        
//        let pIndex = NSComparisonPredicate(keyPath: "index", withValue: index, isExact: true)
//        let pComponent = NSComparisonPredicate(keyPath: "component_id", withValue: component_id, isExact: true)
//        let pTerm = NSComparisonPredicate(keyPath: "term_id", withValue: term_id, isExact: true)
//        
//        
//        let namePred = NSComparisonPredicate(keyPath: "name", withValue: "AA_AA_AA_AA_00", isExact: true)
//        let namePred1 = NSComparisonPredicate(keyPath: "name", withValue: "00_AA_AA_AA_01", isExact: true)
//        let namePred2 = NSComparisonPredicate(keyPath: "name", withValue: "00_AA_AA_AA_02", isExact: true)
//        
//        let notNamePred = NSCompoundPredicate(notPredicateWithSubpredicate: namePred)
//        let notNamePred1 = NSCompoundPredicate(notPredicateWithSubpredicate: namePred1)
//        let notNamePred2 = NSCompoundPredicate(notPredicateWithSubpredicate: namePred2)
//        
//        let finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [pIndex,pComponent,pTerm,notNamePred,notNamePred1,notNamePred2])
//        
//        let fetchRequest = NSFetchRequest(entityName: entity)
//        fetchRequest.fetchBatchSize = 20
//        
//        fetchRequest.predicate = finalPredicate
//        
//        let descriptorSelector = #selector(NSString.localizedCompare(_:))
//        let sortName = NSSortDescriptor(key: keyPath, ascending: ascending, selector:descriptorSelector)
//        fetchRequest.sortDescriptors = [sortName]
//        
//        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx, sectionNameKeyPath: nil, cacheName: nil)
//        frc.delegate = self
//        
//        _fetchedResultsController = frc
//        do {
//            try _fetchedResultsController!.performFetch()
//        } catch {
//            abort()
//        }
//        
//        return _fetchedResultsController!
//    }
//    
//    func controllerWillChangeContent(controller: NSFetchedResultsController) {
//        tableView.beginUpdates()
//    }
//    
//    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
//        
//        let tableViewController = tableView
//        
//        switch type {
//        case .Insert:
//            tableViewController.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
//        case .Delete:
//            tableViewController.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
//        default:
//            return
//        }
//    }
//    
//    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
//        
//        let tableViewController = tableView
//        
//        switch (type) {
//        case .Insert:
//            if let indexPath = newIndexPath {
//                tableViewController.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
//            }
//            break;
//        case .Delete:
//            if let indexPath = indexPath {
//                tableViewController.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
//            }
//            break;
//        case .Update:
//            if let indexPath = indexPath {
//                if let cell = tableViewController.cellForRowAtIndexPath(indexPath) {
//                    configureCell(cell, atIndexPath: indexPath)
//                }
//            }
//            break;
//        case .Move:
//            if let indexPath = indexPath {
//                tableViewController.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
//            }
//            
//            if let newIndexPath = newIndexPath {
//                tableViewController.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Fade)
//            }
//            break;
//        }
//        
//    }
//    
//    func controllerDidChangeContent(controller: NSFetchedResultsController) {
//        tableView.endUpdates()
//    }
}
