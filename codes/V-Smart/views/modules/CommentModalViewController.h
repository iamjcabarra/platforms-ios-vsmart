//
//  CommentModalViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 9/28/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CommentModalViewDelegate <NSObject>

@required
- (void)reloadCommentTable;

@end

@interface CommentModalViewController : UIViewController

@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) NSManagedObject *lesson_mo;
@property (nonatomic, strong) NSManagedObject *comment_mo;

@end
