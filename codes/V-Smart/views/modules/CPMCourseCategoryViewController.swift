//
//  CPMCourseCategoryViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 14/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class CPMCourseCategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate {
    
    @IBOutlet fileprivate var courseCategorySearchBar: UISearchBar!
    @IBOutlet fileprivate var searchDimmedView: UIView!
    @IBOutlet fileprivate var courseCategoryTableView: UITableView!
    @IBOutlet fileprivate var emptyPlaceholderView: UIView!
    @IBOutlet fileprivate var emptyPlaceholderLabel: UILabel!
    
    fileprivate let kCourseCategoryCellIdentifier = "course_category_cell_identifier"
    fileprivate let kCurriculumViewSegueIdentifier = "SHOW_CURRICULUM_VIEW"
    
    fileprivate var searchKey = ""
    fileprivate var isAscending = true
    fileprivate var temporaryCourseCategoryListCount = 1
    
    fileprivate var tableRefreshControl: UIRefreshControl!
    fileprivate var selectedCourseCategoryObject: NSManagedObject!
    
    // MARK: - Data Manager
    
    fileprivate lazy var curriculumPlannerDataManager: CPMDataManager = {
        let cpdm = CPMDataManager.sharedInstance
        return cpdm
    }()
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide dimmed view by default
        self.searchDimmedView.isHidden = true
        
        // Hide empty place holder by default
        self.shouldShowEmptyPlaceholderView(false)
        
        // Dimmed view tap recognizer
        let cancelSearchOperation = #selector(self.cancelSearchOperation(_:))
        let tapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: cancelSearchOperation)
        self.searchDimmedView.addGestureRecognizer(tapGestureRecognizer)
        
        // Empty navigation bar title by default
        self.title = ""
        
        // Pull to refresh
        let refreshLessonListAction = #selector(self.refreshLessonListAction(_:))
        self.tableRefreshControl = UIRefreshControl.init()
        self.tableRefreshControl.addTarget(self, action: refreshLessonListAction, for: .valueChanged)
        self.courseCategoryTableView.addSubview(self.tableRefreshControl)
        
        // Request initial course category list
        self.loadCourseCategoryListForRequestType(.load)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customizeNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Custom Navigation Bar
    
    func customizeNavigationBar() {
        // Decorate navigation bar
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "#4475B7")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        // Create navigation bar right button items
        let spacerButton = UIButton.init(type: UIButtonType.custom)
        spacerButton.frame = CGRect(x: 0, y: 0, width: 10, height: 20)
        spacerButton.showsTouchWhenHighlighted = false
        
        let searchButton = UIButton.init(type: UIButtonType.custom)
        searchButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        searchButton.showsTouchWhenHighlighted = true
        
        let searchButtonImage = UIImage.init(named: "search_white_48x48.png")
        searchButton.setImage(searchButtonImage, for: UIControlState())
        
        let searchButtonAction = #selector(self.searchButtonAction(_:))
        searchButton.addTarget(self, action: searchButtonAction, for: .touchUpInside)
        
        let sortButton = UIButton.init(type: UIButtonType.custom)
        sortButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        sortButton.showsTouchWhenHighlighted = true
        
        let sortButtonImage = UIImage.init(named: "sort_white48x48.png")
        sortButton.setImage(sortButtonImage, for: UIControlState())
        
        let sortButtonAction = #selector(self.sortButtonAction(_:))
        sortButton.addTarget(self, action: sortButtonAction, for: .touchUpInside)
        
        let spacerButtonItem = UIBarButtonItem.init(customView: spacerButton)
        let sortButtonItem = UIBarButtonItem.init(customView: sortButton)
        let searchButtonItem = UIBarButtonItem.init(customView: searchButton)
        
        self.navigationItem.rightBarButtonItems = [sortButtonItem, searchButtonItem, spacerButtonItem]
        
        // Custom navigation bar's search bar
        self.courseCategorySearchBar.placeholder = NSLocalizedString("Search Course Category", comment: "")
        self.courseCategorySearchBar.delegate = self
    }
    
    // MARK: - List Course Category
    
    func loadCourseCategoryListForRequestType(_ requestType: CPMCourseCategoryListRequestType) {
        if (requestType != .refresh) {
            let indicatorString = requestType == .load ? "\(NSLocalizedString("Loading", comment: ""))..." : "\(NSLocalizedString("Searching", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
        }

        let userid = self.curriculumPlannerDataManager.retrieveUserID()
        self.curriculumPlannerDataManager.requestCourseCategoryListForUserWithID(userid) { (dictionaryBlock) in
            if let dictionary = dictionaryBlock {
                DispatchQueue.main.async(execute: {
                    let count = dictionary["count"] as! Int
                    self.title = "\(NSLocalizedString("Course Category List", comment: "")) (\(count))"
                    self.temporaryCourseCategoryListCount = count
                })
            }
            else {
                DispatchQueue.main.async(execute: {
                    let message = NSLocalizedString("There was an error loading this page. Please check your internet connection.", comment: "")
                    self.showNotificationMessage(message)
                })
            }
            
            DispatchQueue.main.async(execute: {
                self.reloadFetchedResultsController()
                requestType != .refresh ? HUD.hideUIBlockingIndicator() : self.tableRefreshControl.endRefreshing()
            })
        }
    }
    
    func refreshLessonListAction(_ sender: UIRefreshControl?) {
        self.loadCourseCategoryListForRequestType(.refresh)
    }
    
    // MARK: - Button Event Handlers
    
    func searchButtonAction(_ sender: UIButton) {
        self.navigationItem.titleView = self.courseCategorySearchBar
        self.courseCategorySearchBar.becomeFirstResponder()
    }
    
    func sortButtonAction(_ sender: UIButton) {
        self.isAscending = self.isAscending ? false : true
        self.reloadFetchedResultsController()
    }
    
    // MARK: - Search Bar Delegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchDimmedView.isHidden = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText == "") {
            self.searchKey = searchText
            self.reloadFetchedResultsController()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchKey = searchBar.text!
        self.reloadFetchedResultsController()
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = self.searchKey
        self.navigationItem.titleView = nil
        self.searchDimmedView.isHidden = true
    }
    
    // MARK: - Tap Gesture Recognizer
    
    func cancelSearchOperation(_ sender: UITapGestureRecognizer) {
        self.navigationItem.titleView = nil
        self.searchDimmedView.isHidden = true
        self.view.endEditing(true)
        self.courseCategorySearchBar.text = self.searchKey
    }
    
    // MARK: - Alert View Message
    
    func showNotificationMessage(_ message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: NSLocalizedString("Okay", comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Empty Placeholder View
    
    func shouldShowEmptyPlaceholderView(_ show: Bool) {
        var shouldShow = show
        
        if (self.courseCategorySearchBar.text == "") {
            shouldShow = self.temporaryCourseCategoryListCount > 0 ? false : true
        }
        
        if (shouldShow) {
            var message = NSLocalizedString("No available course category yet.", comment: "")
            
            if (self.courseCategorySearchBar.text != "") {
                message = "\(NSLocalizedString("No results found for", comment: "")) \"\(self.searchKey)\""
            }
            
            self.emptyPlaceholderLabel.text = message
        }
        
        self.emptyPlaceholderView.isHidden = !shouldShow
        self.courseCategoryTableView.isHidden = show
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        self.shouldShowEmptyPlaceholderView((sectionData.numberOfObjects > 0) ? false : true)
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kCourseCategoryCellIdentifier, for: indexPath) as! CPMCourseCategoryTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: CPMCourseCategoryTableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        
        cell.courseCategoryNameLabel.text = mo.value(forKey: "name") as? String
        cell.courseCategoryInitialLabel.text = mo.value(forKey: "category_description") as? String
    }
    
    // MARK: - Table View Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCourseCategoryObject = fetchedResultsController.object(at: indexPath)
        
        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: self.kCurriculumViewSegueIdentifier, sender: nil)
            tableView.deselectRow(at: indexPath, animated: true)
        })
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: CPMConstants.Entity.COURSE_CATEGORY)
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = self.curriculumPlannerDataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: CPMConstants.Entity.COURSE_CATEGORY)
        fetchRequest.fetchBatchSize = 20
        
        if (self.searchKey != "") {
            let predicate = self.curriculumPlannerDataManager.predicateForKeyPath("name", containsValue: self.searchKey)
            fetchRequest.predicate = predicate
        }
        
        let descriptorSelector = #selector(NSString.localizedCaseInsensitiveCompare(_:))
        let sortDescriptor = NSSortDescriptor.init(key: "name", ascending: self.isAscending, selector:descriptorSelector)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.courseCategoryTableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.courseCategoryTableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.courseCategoryTableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.courseCategoryTableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.courseCategoryTableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.courseCategoryTableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! CPMCourseCategoryTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.courseCategoryTableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.courseCategoryTableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.courseCategoryTableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.courseCategoryTableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.kCurriculumViewSegueIdentifier {
            let curriculumView = segue.destination as? CPMCurriculumViewController
            let courseCategoryID = self.selectedCourseCategoryObject.value(forKey: "id") as! String
            let courseCategoryName = self.selectedCourseCategoryObject.value(forKey: "name") as! String
            let userID = self.curriculumPlannerDataManager.retrieveUserID()
            
            curriculumView?.courseCategoryID = courseCategoryID
            curriculumView?.courseCategoryName = courseCategoryName
            curriculumView?.userID = userID
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        }
    }

}
