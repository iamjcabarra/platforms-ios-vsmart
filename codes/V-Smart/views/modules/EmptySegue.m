//
//  EmptySegue.m
//  V-Smart
//
//  Created by Julius Abarra on 9/18/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "EmptySegue.h"

@implementation EmptySegue

- (void)perform {
    // Nothing. The LessonDetailsTabBarViewController class handles all of the view
    // controller action.
}

@end
