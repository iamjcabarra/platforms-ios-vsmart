//
//  CurriculumPlannerContainer.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/15/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface CurriculumPlannerContainer : BaseViewController

@end
