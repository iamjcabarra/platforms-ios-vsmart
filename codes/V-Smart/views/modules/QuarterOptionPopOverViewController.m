//
//  QuarterOptionPopOverViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "QuarterOptionPopOverViewController.h"

@interface QuarterOptionPopOverViewController ()

@end

@implementation QuarterOptionPopOverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
