//
//  ImageWithButtonCell.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 29/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class ImageWithButtonCVCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var button: UIButton!
}
