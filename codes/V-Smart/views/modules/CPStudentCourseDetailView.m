//
//  CPStudentCourseDetailView.m
//  V-Smart
//
//  Created by Julius Abarra on 29/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "CPStudentCourseDetailView.h"
#import "TBNewTestItemCell.h"
#import "CPTestPWValidationView.h"
#import "TestGuruDataManager.h"
#import "TestGuruDataManager+Course.h"
#import "TBClassHelper.h"
#import "AccountInfo.h"
#import "V_Smart-Swift.h"
#import "HUD.h"

@interface CPStudentCourseDetailView () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate, CPTestPWValidationViewDelegate, CPTestInfoDelegate, CoursePlayerQuizSheetDelegate>

@property (strong, nonatomic) TestGuruDataManager *tgdm;
@property (strong, nonatomic) TBClassHelper *classHelper;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObject *selectedTestObject;
@property (strong, nonatomic) NSManagedObject *testInfo;

@property (strong, nonatomic) IBOutlet UIView *emptyView;
@property (strong, nonatomic) IBOutlet UILabel *emptyMessageLabel;

@property (strong, nonatomic) IBOutlet UIView *visualEffectView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *headerViewHeight;

@property (strong, nonatomic) IBOutlet UILabel *scheduleLabel;
@property (strong, nonatomic) IBOutlet UILabel *sectionLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *codeLabel;
@property (strong, nonatomic) IBOutlet UILabel *roomLabel;
@property (strong, nonatomic) IBOutlet UILabel *availableTestLabel;
@property (strong, nonatomic) IBOutlet UILabel *scheduleValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *sectionValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *codeValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *roomValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionValueLabel;

@property (strong, nonatomic) IBOutlet UITableView *testTable;
@property (strong, nonatomic) IBOutlet UIButton *loadMoreButton;

@property (assign, nonatomic) NSInteger currentPage;
@property (assign, nonatomic) NSInteger totalFilteredTests;
@property (assign, nonatomic) NSInteger totalPages;
@property (assign, nonatomic) NSInteger totalItems;

@property (strong, nonatomic) NSString *searchKeyword;
@property (assign, nonatomic) BOOL isAscending;

@property (strong, nonatomic) NSManagedObject *quizObject;
@property (strong, nonatomic) NSDictionary *testResults;

@property (assign, nonatomic) NSTimeInterval timeConsumed;

@end

@implementation CPStudentCourseDetailView

static NSString *kTestCellIdentifier = @"testItemCellIdentifier";
static NSString *kShowPasswordValidationView = @"SHOW_PASSWORD_MODAL_VIEW";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Data Manager
    self.tgdm = [TestGuruDataManager sharedInstance];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // String Localization
    [self localizeLabels];
    
    // Course Header
    [self loadCourseHeaderValues];
    [self updateHeaderViewHeight];

    // Test Table View
    [self setUpTestTableView];

    // Search Default
    self.searchKeyword = @"";

    // Sort Default
    self.isAscending = NO;
    
    // Pagination Settings
    self.currentPage = 0;
    self.totalFilteredTests = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    
    // Load More Button
    self.loadMoreButton.hidden = YES;
    [self.loadMoreButton addTarget:self
                            action:@selector(loadMoreButtonAction:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    // Empty View
    [self shouldShowEmptyView:NO];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelSearchWhenTapped)];
    [self.visualEffectView addGestureRecognizer:tap];
    
    // Initial Paginated Test List
    NSDictionary *settings = [self getSettingsForTestPaginationIsReset:YES shouldLoadNextPage:NO];
    [self listPaginatedTestForSettings:settings];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Custom Navigation Bar
    [self customizeNavigationController];

    // Right Bar Buttons
    [self setUpRightBarButtons];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Navigation Bar

- (void)customizeNavigationController {
    UIColor *color = UIColorFromHex(0xEF4136);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    }
    else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Custom Navigation Title View
    self.searchBar.placeholder = NSLocalizedString(@"Search Available Test", nil);
    self.searchBar.delegate = self;
}

#pragma mark - Custom Right Bar Buttons

- (void)setUpRightBarButtons {
    // Spacer Button
    UIButton *spacerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    spacerButton.frame = CGRectMake(0, 0, 10, 20);
    
    // Search Button
    UIImage *searchImage = [UIImage imageNamed:@"search_white_48x48.png"];
    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    searchButton.frame = CGRectMake(0, 0, 20, 20);
    searchButton.showsTouchWhenHighlighted = YES;
    
    [searchButton setImage:searchImage forState:UIControlStateNormal];
    [searchButton setImage:searchImage forState:UIControlStateHighlighted];
    [searchButton addTarget:self action:@selector(searchButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Sort Button
    UIImage *sortImage = [UIImage imageNamed:@"sort_white48x48.png"];
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    sortButton.frame = CGRectMake(0, 0, 20, 20);
    sortButton.showsTouchWhenHighlighted = YES;
    
    [sortButton setImage:sortImage forState:UIControlStateNormal];
    [sortButton setImage:sortImage forState:UIControlStateHighlighted];
    [sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Right Button Items
    UIBarButtonItem *spacerButtonItem = [[UIBarButtonItem alloc] initWithCustomView:spacerButton];
    UIBarButtonItem *sortButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
    UIBarButtonItem *searchButtonItem = [[UIBarButtonItem alloc] initWithCustomView:searchButton];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:sortButtonItem, searchButtonItem, spacerButtonItem, nil];
}

#pragma mark - Course Datail Header 

- (void)localizeLabels {
    self.scheduleLabel.text = NSLocalizedString(@"Schedule", nil);
    self.sectionLabel.text = NSLocalizedString(@"Section", nil);
    self.descriptionLabel.text = NSLocalizedString(@"Description", nil);
    self.codeLabel.text = NSLocalizedString(@"Code", nil);
    self.roomLabel.text = NSLocalizedString(@"Room", nil);
    self.availableTestLabel.text = NSLocalizedString(@"Available Tests", nil);
}

- (void)loadCourseHeaderValues {
    if (self.courseObject != nil) {
        self.title = [self.courseObject valueForKey:@"course_name"];
        self.scheduleValueLabel.text = [self.courseObject valueForKey:@"schedule"];
        self.sectionValueLabel.text = [self.courseObject valueForKey:@"section_name"];
        self.codeValueLabel.text = [self.courseObject valueForKey:@"course_code"];
        self.roomValueLabel.text = [self.courseObject valueForKey:@"venue"];
        
        NSString *description = [self.courseObject valueForKey:@"course_description"];
        [self.classHelper justifyLabel:self.descriptionValueLabel string:description];
        self.descriptionValueLabel.numberOfLines = 0;
    }
}

- (void)updateHeaderViewHeight {
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.25f animations:^{
            CGFloat additionalHeight = [self.classHelper getHeightOfLabel:self.descriptionValueLabel];
            CGFloat headerViewHeight = (additionalHeight > 0.0f) ? 80.0f : 95.0f;
            wo.headerViewHeight.constant = headerViewHeight + additionalHeight;
            [wo.view layoutIfNeeded];
        }];
    });
}

#pragma mark - Test Table View

- (void)setUpTestTableView {
    // Set Protocols
    self.testTable.dataSource = self;
    self.testTable.delegate = self;
    
    // Allow Selection
    self.testTable.allowsSelection = YES;
    self.testTable.allowsMultipleSelection = NO;
    
    // Default Height
    self.testTable.rowHeight = 125.0f;

    // Resusable Cell
    UINib *testCellItemNib = [UINib nibWithNibName:@"TBNewTestItemCell" bundle:nil];
    [self.testTable registerNib:testCellItemNib forCellReuseIdentifier:kTestCellIdentifier];
}

#pragma mark - Paginated Test List

- (NSDictionary *)getSettingsForTestPaginationIsReset:(BOOL)isReset shouldLoadNextPage:(BOOL)nextPage {
    NSString *limit = [self.tgdm stringValue:[self.tgdm fetchObjectForKey:kTG_PAGINATION_LIMIT]];
    NSString *current_page = @"1";
    
    if ([limit integerValue] < 1) {
        limit = @"10";
    }

    if (!isReset) {
        NSInteger number = nextPage ? self.currentPage + 1 : self.currentPage;
        current_page = [NSString stringWithFormat:@"%zd", number];
    }
    
    return @{@"limit":limit, @"current_page":current_page, @"search_keyword":self.searchKeyword};
}

- (void)listPaginatedTestForSettings:(NSDictionary *)settings {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Loading", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    NSString *courseid = [NSString stringWithFormat:@"%@", [self.courseObject valueForKey:@"course_id"]];
    NSString *userid = [self.tgdm loginUser];
    
    self.loadMoreButton.hidden = YES;
    
    [self.tgdm requestPaginatedTestListForCourse:courseid andUser:userid withSettingsForPagination:settings dataBlock:^(NSDictionary *data) {
        if (data != nil) {
            self.currentPage = [data[@"current_page"] integerValue];
            self.totalItems = [data[@"total_items"] integerValue];
            self.totalFilteredTests = [data[@"total_filtered"] integerValue];
            self.totalPages = [data[@"total_pages"] integerValue];
        }
        
        __weak typeof(self) wo = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo reloadFetchedResultsController];
            BOOL hideButton = (wo.totalPages > wo.currentPage) ? NO : YES;
            wo.loadMoreButton.hidden = hideButton;
            BOOL showEmptyView = (wo.totalFilteredTests > 0) ? NO : YES;
            [wo shouldShowEmptyView:showEmptyView];
            [wo updateAssignQuestionLabelWithCount:wo.totalItems];
            [HUD hideUIBlockingIndicator];
        });
    }];
}

#pragma mark - Actions for Buttons

- (void)searchButtonAction:(id)sender {
    self.navigationItem.titleView = self.searchBar;
    [self.searchBar becomeFirstResponder];
}

- (void)sortButtonAction:(id)sender {
    self.isAscending = (self.isAscending) ? NO : YES;
    
    // Just Tempory
    // CHANGE if API-driven sort has been implemented by back-end
    NSString *limit = [NSString stringWithFormat:@"%zd", self.totalFilteredTests];
    NSDictionary *settings = @{@"limit":limit, @"current_page":@"1", @"search_keyword":self.searchKeyword};
    [self listPaginatedTestForSettings:settings];
}

- (void)loadMoreButtonAction:(id)sender {
    self.isAscending = NO;
    NSDictionary *settings = [self getSettingsForTestPaginationIsReset:NO shouldLoadNextPage:YES];
    [self listPaginatedTestForSettings:settings];
}

#pragma mark - Search Bar Delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    self.visualEffectView.hidden = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText isEqualToString:@""]) {
        self.searchKeyword = searchText;
        NSDictionary *settings = [self getSettingsForTestPaginationIsReset:YES shouldLoadNextPage:NO];
        [self listPaginatedTestForSettings:settings];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.searchKeyword = searchBar.text;
    NSDictionary *settings = [self getSettingsForTestPaginationIsReset:YES shouldLoadNextPage:NO];
    [self listPaginatedTestForSettings:settings];
    [self.searchBar resignFirstResponder];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    self.navigationItem.titleView = nil;
    self.visualEffectView.hidden = YES;
}

- (void)cancelSearchWhenTapped {
    self.navigationItem.titleView = nil;
    self.visualEffectView.hidden = YES;
    [self.view endEditing:YES];
    self.searchBar.text = self.searchKeyword;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.testTable = tableView;
    
    TBNewTestItemCell *cell = [tableView dequeueReusableCellWithIdentifier:kTestCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TBNewTestItemCell *testCellItem = (TBNewTestItemCell *)cell;
    [self configureQuestionCell:testCellItem managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(TBNewTestItemCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.testStatus.hidden = YES;
    
    NSString *name = [mo valueForKey:@"name"];
    NSString *formatted_date_open = [[mo valueForKey:@"formatted_date_open"] uppercaseString];
    NSString *formatted_date_close = [[mo valueForKey:@"formatted_date_close"] uppercaseString];
    NSString *item_string = [NSString stringWithFormat:@"%@", [mo valueForKey:@"item_count"]];
    NSString *point_string = [NSString stringWithFormat:@"%@", [mo valueForKey:@"total_score"]];
    NSString *is_graded = [mo valueForKey:@"is_graded"];
    NSString *image_string = [is_graded isEqualToString:@"1"] ? @"graded.png" : @"non-graded.png";
    NSString *password_string = [NSString stringWithFormat:@"%@", [mo valueForKey:@"password"]];
    
    if ([formatted_date_close isEqualToString:@""]) {
        formatted_date_close = NSLocalizedString(@"No Expiration", nil);
    }
    
    NSString *start_date_string = NSLocalizedString(@"Start Date", nil);
    NSString *end_date_string = NSLocalizedString(@"End Date", nil);
    
    cell.lockImage.hidden = ([password_string isEqualToString:@""]) ? YES : NO;
    
    cell.testTitle.text = name;
    cell.testStartDateLabel.text = [NSString stringWithFormat:@"%@: %@", start_date_string, formatted_date_open];
    cell.testEndDateLabel.text = [NSString stringWithFormat:@"%@: %@", end_date_string, formatted_date_close];
    cell.testStageTypeImage.image = [UIImage imageNamed:image_string];
    
    cell.itemsValueLabel.text = [NSString stringWithFormat:@"%@", item_string];
    [cell updateItemsLabel:item_string];
    
    cell.pointsValueLabel.text = [NSString stringWithFormat:@"%@", point_string];
    [cell updatePointsLabel:point_string];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedTestObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    AccountInfo *account = [[VSmart sharedInstance] account];
    NSString *position = [NSString stringWithFormat:@"%@", account.user.position];

    NSString *quiz_id = [NSString stringWithFormat:@"%@", [self.selectedTestObject valueForKey:@"id"]];
    [self.tgdm saveObject:quiz_id forKey:kCP_SELECTED_QUIZ_ID];

    if ([position isEqualToString:kModeIsStudent]) {
        NSString *testPassword = [self.tgdm stringValue:[self.selectedTestObject valueForKey:@"password"]];
        
        if (![testPassword isEqualToString:@""]) {
            [self performSegueWithIdentifier:kShowPasswordValidationView sender:nil];
        }
        else {
            [self performPreRunTest];
        }
        
        [self.testTable deselectRowAtIndexPath:indexPath animated:YES];
    }
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tgdm.mainContext;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCourseTestListEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSSortDescriptor *sort_date_created = [NSSortDescriptor sortDescriptorWithKey:@"sort_date_created" ascending:self.isAscending];
    [fetchRequest setSortDescriptors:@[sort_date_created]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.testTable beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.testTable insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.testTable deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.testTable;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.testTable endUpdates];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self updateHeaderViewHeight];
}

#pragma mark - Reload Fetched Results

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.testTable reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error: %@", [err localizedDescription]);
    }
}

#pragma mark - Application Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kShowPasswordValidationView]) {
        CPTestPWValidationView *passwordValidationView = (CPTestPWValidationView *)[segue destinationViewController];
        passwordValidationView.testObject = self.selectedTestObject;
        passwordValidationView.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"CP_COURSE_PLAYER_TEST_INFO_SEGUE"]) {
        CPTestInfo *info = (CPTestInfo *)[segue destinationViewController];
        [self hideNavigationBackButtonTitle];
        
        if (self.testInfo != nil) {
            NSLog(@"test object : %@", self.testInfo);
            info.delegate = self;
            info.mo = self.testInfo;
        }
    }
    
    if ([segue.identifier isEqualToString:@"CP_COURSE_PLAYER_RUN_SEGUE"]) {
        CoursePlayerQuizSheet *cpqs = (CoursePlayerQuizSheet *)[segue destinationViewController];
        cpqs.delegate = self;
        cpqs.title = self.title;
    }
    
    if ([segue.identifier isEqualToString:@"CP_SHOW_RESULTS_SEGUE"]) {
        CoursePlayerResultsView *cpr = (CoursePlayerResultsView *)[segue destinationViewController];
        cpr.results = self.testResults;
        cpr.quizObject = self.quizObject;
        cpr.title = self.title;
        NSString *hms = [self stringFromTimeInterval:self.timeConsumed];
        
        cpr.timeConsumed = hms;
    }
    
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

#pragma mark - CPInfo Delegate

- (void)didClickSubmitButton:(BOOL)flag quizObject:(NSManagedObject *)quizObject testResults:(NSDictionary *)testResults timeConsumed:(NSTimeInterval)timeConsumed {
    
    if (flag) {
        self.quizObject = quizObject;
        self.testResults = testResults;
        self.timeConsumed = timeConsumed;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"CP_SHOW_RESULTS_SEGUE" sender: self];
        });
    }
}

- (void)didClickStartButton:(BOOL)flag {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"CP_COURSE_PLAYER_RUN_SEGUE" sender:self];
    });
}

#pragma mark - CPTestPWValidationViewDelegate

- (void)shouldAllowToRunTest:(BOOL)run {
    if (run) {
        [self performPreRunTest];
    }
}

- (void)performPreRunTest {
    NSString *cs_id = [NSString stringWithFormat:@"%@", [self.tgdm fetchObjectForKey:kCP_SELECTED_CS_ID]];
    NSString *quiz_id = [NSString stringWithFormat:@"%@", [self.tgdm fetchObjectForKey:kCP_SELECTED_QUIZ_ID]];
    
    NSDictionary *userData = @{@"quiz_id": quiz_id, @"cs_id": cs_id};
    __weak typeof(self) wo = self;
    
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Processing", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    [self.tgdm requestRunOrPrerun:@"prerun" parameter:userData dataBlock:^(NSDictionary *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD hideUIBlockingIndicator];
        });
        
        if (data != nil) {
            wo.testInfo = data[@"mo"];
            NSString *test_id = [wo.testInfo valueForKey:@"id"];
            NSString *error = [data valueForKey:@"error"];
            
            if (error.length > 0) {
                NSLog(@"YOU CAN NOT TAKE THIS EXAM!!!");
                dispatch_async(dispatch_get_main_queue(), ^{
                    [wo displayWarning:error];
                });
            } else {
                if (test_id.length > 0) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [wo performSegueWithIdentifier:@"CP_COURSE_PLAYER_TEST_INFO_SEGUE" sender:wo];
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [wo displayWarning:@"You cannot take this test."];
                    });
                    
                }
            }
        }
    }];
}

- (void)hideNavigationBackButtonTitle {
    UIBarButtonItem *bbi = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = bbi;
}

#pragma mark - Custom Empty View

- (void)shouldShowEmptyView:(BOOL)show {
    if (show) {
        NSString *message = NSLocalizedString(@"No available test yet.", nil);
        
        if (![self.searchKeyword isEqualToString:@""]) {
            message = NSLocalizedString(@"No results found for", nil);
            message =[NSString stringWithFormat:@"%@ \"%@\"", message, self.searchKeyword];
        }
        
        self.emptyMessageLabel.text = message;
    }
    
    self.emptyView.hidden = !show;
    self.testTable.hidden = show;
}

- (void)updateAssignQuestionLabelWithCount:(NSInteger)count {
    NSString *title = NSLocalizedString(@"Available Test", nil);
    
    if (count > 1) {
        title = NSLocalizedString(@"Available Tests", nil);
    }
    
    self.availableTestLabel.text = [NSString stringWithFormat:@"%@ (%zd)", title, count];
}

- (void)displayWarning:(NSString *)message {
    NSString *title_string = @"";
    NSString *okayTitle = NSLocalizedString(@"Okay", nil);
    NSString *message_string = NSLocalizedString(message, nil);
    
    UIAlertController *av = [UIAlertController alertControllerWithTitle:title_string
                                                                message:message_string
                                                         preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okayAction = [UIAlertAction actionWithTitle:okayTitle
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                       }];
    
    [av addAction:okayAction];
    [self presentViewController:av animated:YES completion:nil];
}

@end
