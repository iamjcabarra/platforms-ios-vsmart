//
//  LessonPlanDataManager.h
//  V-Smart
//
//  Created by Julius Abarra on 9/1/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

/////// PUBLIC ENTITIES //////
static NSString *kCourseTableEntity = @"CourseTable";
static NSString *kLessonPlanEntity = @"LessonPlan";
static NSString *kLessonPlanDetailsEntity = @"LessonPlanDetails";
static NSString *kLessonTemplateEntity = @"LessonTemplate";
static NSString *kContentEntity = @"Content";
static NSString *kProcessEntity = @"Process";
static NSString *kHistoryEntity = @"History";
static NSString *kCommentEntity = @"Comment";
static NSString *kCurriculumEntity = @"Curriculum";
static NSString *kCurriculumPeriodEntity = @"CurriculumPeriod";
static NSString *kCurriculumContentEntity = @"CurriculumContent";
static NSString *kCurriculumLessonPlanEntity = @"CurriculumLessonPlan";
static NSString *kLearningCompentencyEntity = @"LearningCompetency";
static NSString *kLearningCompetencyContentEntity = @"LearningCompetencyContent";
static NSString *kLearningCompetencyLessonEntity = @"LearningCompetencyLesson";
static NSString *kTemplateEntity = @"Template";
static NSString *kTemplateContentEntity = @"TemplateContent";
static NSString *kTemplateProcessEntity = @"TemplateProcess";
static NSString *kRubricsEntity = @"Rubrics";
static NSString *kCopyLessonEntity = @"CopyLesson";
static NSString *kCopyLessonContentEntity = @"CopyLessonContent";
static NSString *kCopyLessonContentProcessEntity = @"CopyLessonContentProcess";
static NSString *kFileEntity = @"File";
static NSString *kPreAssociatedCompetency = @"PreAssociatedCompetency";

/////// CONSTANTS //////
#define kLPServerDateFormat         @"yyyy-MM-dd HH:mm:ss"
#define kLPDisplayDateFormat        @"EEE. dd MMMM yyyy"
#define kLPDisplayDateFormatLong    @"EEE, MMMM dd, yyyy hh:mm a"
#define KLPDisplayDateFormatShort   @"MMMM dd, yyyy"

/////// BLOCK TYPES //////
typedef void (^LessonPlanDoneBlock)(BOOL status);
typedef void (^LessonPlanDataBlock)(NSDictionary *data);
typedef void (^LessonPlanContent)(NSArray *content);
typedef void (^LessonPlanManagedObjectBlock)(NSManagedObject *object);

@interface LessonPlanDataManager : NSObject

@property (nonatomic, readonly) NSManagedObjectContext *mainContext;
@property (nonatomic, copy, readwrite) LessonPlanContent doneProgressBlock;
@property (nonatomic, strong) NSString *errorMessage;

+ (instancetype)sharedInstance;
- (void)saveContext;
- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate;
- (NSString *)stringValue:(id)object;
- (NSString *)loginUser;
- (NSString *)userEmail;

/////// PUBLIC METHODS //////
- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value;

- (NSManagedObject *)getEntity:(NSString *)entity
                     attribute:(NSString *)attribute
                     parameter:(NSString *)parameter
                       context:(NSManagedObjectContext *)context;

- (NSManagedObject *)getEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate;

- (NSArray *)getObjectsForEntity:(NSString *)entity
                       predicate:(NSPredicate *)predicate
                         context:(NSManagedObjectContext *)context;

- (NSArray *)getObjectsForEntity:(NSString *)entity
                   withPredicate:(NSPredicate *)predicate
               andSortDescriptor:(NSString *)descriptor;

- (NSArray *)getObjectsForEntity:(NSString *)entity
                       predicate:(NSPredicate *)predicate
                  sortDescriptor:(NSSortDescriptor *)descriptor;

- (NSArray *)getObjectsForEntity:(NSString *)entity
                        withFKey:(NSString *)fKey
                    andFKeyValue:(NSString *)fValue
                         andSKey:(NSString *)sKey
                    andSKeyValue:(NSString *)sValue
                        sortedBy:(NSString *)sortDescriptor;

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value;
- (NSManagedObject *)getLessonDetailsObjectForLessonWithID:(NSString *)lpid;
- (NSArray *)getLessonStageProcessesForLessonWithID:(NSString *)lpid andStageID:(NSString *)stageid;

- (void)showErrorMessageDialog;

/////// COURSE LIST //////
- (void)requestCourseListForUser:(NSString *)userid dataBlock:(LessonPlanDataBlock)dataBlock;
- (void)requestPaginatedCourseListForUser:(NSString *)userid withSettingsForPagination:(NSDictionary *)settings dataBlock:(LessonPlanDataBlock)dataBlock;

/////// LESSON PLAN //////
- (void)requestLessonPlanListForUser:(NSString *)userid dataBlock:(LessonPlanDataBlock)dataBlock;
- (void)requestLessonPlanListForUser:(NSString *)userid courseid:(NSString *)courseid dataBlock:(LessonPlanDataBlock)dataBlock;
- (void)requestLessonPlanDetailsForLessonPlanWithID:(NSString *)lessonplanid doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestLessonTemplateList:(LessonPlanDoneBlock)doneBlock;
- (void)requestLessonTemplateForLessonTemplateWithID:(NSString *)templateid doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestPostLessonPlanComment:(NSManagedObject *)mo comment:(NSString *)comment doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestPostComment:(NSDictionary *)commentData doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestUpdateLessonPlanComment:(NSManagedObject *)mo comment:(NSString *)comment doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestDeleteLessonPlanComment:(NSManagedObject *)mo doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestUpdateLessonPlanStatus:(NSManagedObject *)mo doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestRemoveLessonPlan:(NSManagedObject *)mo doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestCreateNewLessonPlan:(NSDictionary *)meta file:(NSDictionary *)object doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestCreateNewLessonPlanWithPostBody:(NSDictionary *)body doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestCreateNewLessonPlanWithPostBody:(NSDictionary *)body dataBlock:(LessonPlanDataBlock)dataBlock;
- (void)requestUpdateLessonPlanWithLessonPlanID:(NSString *)lpid meta:(NSDictionary *)meta doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestUpdateLessonPlanWithLessonPlanID:(NSString *)lpid andPostBody:(NSDictionary *)body doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestUpdateLessonPlanFileWithLearningContentID:(NSString *)lcid file:(NSDictionary *)object doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestUpdateLessonPlanFile:(NSString *)newFileAtPath forLearningContentWithID:(NSString *)lcid doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestDownloadPDFFileWithLessonPlanID:(NSString *)lessonplanid contentBlock:(LessonPlanContent)contentBlock;
- (void)requestDownloadLessonPlanWithID:(NSString *)lessonid contentBlock:(LessonPlanContent)contentBlock;
- (void)requestAssociatedCurriculumToLessonPlanWithID:(NSString *)lpid dataBlock:(LessonPlanDataBlock)dataBlock;
- (void)updateLessonPlanLocally:(NSString *)entity predicate:(NSPredicate *)predicate details:(NSDictionary *)details;
- (void)updateEntity:(NSString *)entity predicate:(NSPredicate *)predicate withData:(NSDictionary *)data;
- (void)prepareLessonForAction:(NSInteger)action copyObject:(NSManagedObject *)object additionalData:(NSDictionary *)data objectBlock:(LessonPlanManagedObjectBlock)objectBlock;
- (void)prepareLessonObjectForAction:(NSInteger)action
                          copyObject:(NSManagedObject *)object
                             addData:(NSDictionary *)data
                         objectBlock:(LessonPlanManagedObjectBlock)objectBlock;
- (NSDictionary *)prepareLessonPlanDataForPostOperation:(NSManagedObject *)object action:(NSInteger)action;
- (NSDictionary *)prepareLessonObject:(NSManagedObject *)object forAction:(NSInteger)action;
- (void)saveFilesInCoreData:(NSArray *)files doneBlock:(LessonPlanDoneBlock)doneBlock;
- (NSArray *)fetchPreAssociatedLearningCompetencies;
- (BOOL)removePreAssociatedLearningCompetency:(NSString *)competency;

/////// CURRICULUM //////
- (void)requestApprovedCurriculumList:(LessonPlanDoneBlock)doneBlock;
- (void)requestPeriodsForCurriculum:(NSManagedObject *)mo doneBlock:(LessonPlanDoneBlock)doneBlock;
- (void)requestCurriculumLearningCompetenciesForPeriodWithObject:(NSManagedObject *)object dataBlock:(LessonPlanDataBlock)dataBlock;

@end
