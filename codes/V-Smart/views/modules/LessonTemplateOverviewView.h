//
//  LessonTemplateOverviewView.h
//  V-Smart
//
//  Created by Julius Abarra on 08/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonTemplateOverviewView : UITableViewController

@property (assign, nonatomic) NSManagedObject *copiedLessonObject;

@end
