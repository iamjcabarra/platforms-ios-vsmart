//
//  StageProcessCellItem.m
//  V-Smart
//
//  Created by Julius Abarra on 08/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "StageProcessCellItem.h"

@implementation StageProcessCellItem

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)shouldHideProcessContentTextView:(BOOL)hide {
    self.processContentTextView.hidden = hide;
}

- (void)shouldHideUploadFileButton:(BOOL)hide {
    self.uploadFileButton.hidden = hide;
}

- (void)shouldHideClearFileButton:(BOOL)hide {
    self.clearFileButton.hidden = hide;
}

@end
