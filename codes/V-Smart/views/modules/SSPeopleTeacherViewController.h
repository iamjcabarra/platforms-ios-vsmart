//
//  SSPeopleTeacherViewController.h
//  V-Smart
//
//  Created by VhaL on 3/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSPeopleTeacherViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
