//
//  TagCell.m
//  V-Smart
//
//  Created by VhaL on 3/1/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "TagCell.h"

@implementation TagCell
@synthesize tagLabel, tagTextField, acceptButton, cancelButton, shareIndicatorImageView, delegate;


-(void)initializeCell{
    [self hideEditControls:YES];
    [self hideShareIndicator:YES];
}

-(IBAction)acceptButtonTapped:(id)sender{
    [self hideEditControls:YES];
    
    tagLabel.text = [tagTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([delegate respondsToSelector:@selector(buttonEditTagTapped:)])
    {
        [delegate performSelector:@selector(buttonEditTagTapped:) withObject:self];
    }
}

-(IBAction)cancelButtonTapped:(id)sender{
    [self hideEditControls:YES];
}

-(void)hideEditControls:(bool)hide{
    acceptButton.hidden  = hide;
    cancelButton.hidden = hide;
    tagTextField.hidden = hide;
    tagLabel.hidden = !hide;
    
    if (hide){
        [tagTextField resignFirstResponder];
    }
    else{
        [tagTextField becomeFirstResponder];
    }
}

-(void)hideShareIndicator:(bool)hide{
    shareIndicatorImageView.hidden = hide;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self acceptButtonTapped:acceptButton];
    return YES;
}

@end
