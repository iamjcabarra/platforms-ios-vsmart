//
//  CALCalendarViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 13/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CALCalendarViewController: UIViewController, JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    
    // We cache our colors because we do not want to be creating
    // a new color every time a cell is displayed. We do not want a laggy
    // scrolling calendar.
    
    fileprivate let colorA = UIColor(colorWithHexValue: 0xE6F4Fa)
    fileprivate let colorB = UIColor(colorWithHexValue: 0x939597)
    fileprivate let colorC = UIColor(colorWithHexValue: 0xC6CACC)
    fileprivate let colorD = UIColor(colorWithHexValue: 0x58C2E8)
    
    fileprivate var yy = 1
    fileprivate var mm = 1
    fileprivate var dd = 1
    
    fileprivate var countImageA = UIImage(named: "eventCountActive")
    fileprivate var countImageB = UIImage(named: "eventCountInactive")
    
    fileprivate var dateHelper = DateHelper()
    fileprivate let notification = NotificationCenter.default
    
    // MARK: - Shared Data Manager
    
    fileprivate lazy var dataManager: CALDataManager = {
        let cmdm = CALDataManager.sharedInstance
        return cmdm
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure initial date
        let date = Date()
        var calendar = Calendar.current
        calendar.timeZone = NSTimeZone.local
        
        self.yy = calendar.component(.year, from: date)
        self.mm = calendar.component(.month, from: date)
        self.dd = calendar.component(.day, from: date)
        
        // Set header label text
        let monthName = DateFormatter().monthSymbols[self.mm - 1]
        self.headerLabel.text = "\(monthName.uppercased()) \(self.yy)"
        
        // Set data source and delegate
        self.calendarView.dataSource = self
        self.calendarView.delegate = self
    
        // Register xib file for calendar view
        self.calendarView.registerCellViewXib(file: "CALCalendarDayCellView")
        
        // Set calendar day cell margins
        self.calendarView.cellInset = CGPoint(x: 3, y: 3)
        
        // Button action events
        self.backButton.addTarget(self, action: #selector(self.backButtonAction(_:)), for: .touchUpInside)
        self.forwardButton.addTarget(self, action: #selector(self.forwardButtonAction(_:)), for: .touchUpInside)
        
        // Start notifications
        self.setUpNotifications()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Destroy notifications
        self.tearDownNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Notifications
    
    fileprivate func setUpNotifications() {
        let notificationSelector = #selector(self.respondToEventListChange(_:))
        let notificationName = NSNotification.Name(rawValue: "CAL_NOTIFICATION_EVENT_LIST_CHANGE")
        self.notification.addObserver(self, selector: notificationSelector, name: notificationName, object: nil)
    }
    
    fileprivate func tearDownNotifications() {
        let notificationName = NSNotification.Name(rawValue: "CAL_NOTIFICATION_EVENT_LIST_CHANGE")
        self.notification.removeObserver(self, name: notificationName, object: nil)
    }
    
    func respondToEventListChange(_ notification: NSNotification) {
        DispatchQueue.main.async(execute: {
            self.calendarView.reloadData()
        })
    }
    
    // MARK: - Button Event Handlers
    
    func backButtonAction(_ sender: UIButton) {
        let tempMM = self.mm
        let tempYY = self.yy

        self.mm = ((tempMM - 1) >= 1) ? tempMM - 1 : 12
        self.yy = ((tempMM - 1) <= 0) ? tempYY - 1 : tempYY
        
        if self.yy > 0 {
            DispatchQueue.main.async(execute: {
                self.calendarView.reloadData()
                let monthName = DateFormatter().monthSymbols[self.mm - 1]
                self.headerLabel.text = "\(monthName.uppercased()) \(self.yy)"
            })
        }
        else {
            self.mm = tempMM
            self.yy = tempYY
            
            DispatchQueue.main.async(execute: {
                let message = "Can't go back!"
                self.view.makeToast(message: message)
            })
        }
        
    }
    
    func forwardButtonAction(_ sender: UIButton) {
        let tempMM = self.mm
        let tempYY = self.yy
        
        self.mm = ((tempMM + 1) > 12) ? 1 : tempMM + 1
        self.yy = ((tempMM + 1) > 12) ? tempYY + 1 : tempYY
        
        DispatchQueue.main.async(execute: {
            self.calendarView.reloadData()
            let monthName = DateFormatter().monthSymbols[self.mm - 1]
            self.headerLabel.text = "\(monthName.uppercased()) \(self.yy)"
        })
    }

    // MARK: - Apple Calendar View Data Source
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        
        let date = formatter.date(from: "\(self.yy) \(self.mm) 01")!    // same to prevent swiping
        let parameters = ConfigurationParameters(startDate: date,
                                                 endDate: date,
                                                 numberOfRows: 6,
                                                 calendar: Calendar.current,
                                                 generateInDates: .forAllMonths,
                                                 generateOutDates: .tillEndOfGrid,
                                                 firstDayOfWeek: .sunday)
        return parameters
    }
    
    // MARK: - Apple Calendar View Delegate
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplayCell cell: JTAppleDayCellView, date: Date, cellState: CellState) {
        let customCell = cell as! CALCalendarDayCellView
        customCell.dayLabel.text = cellState.text
        self.handleCellTextColor(view: cell, cellState: cellState)
        self.handleCellSelection(view: cell, cellState: cellState, isToday: date.isDateInToday)
        
        DispatchQueue.main.async(execute: {
            let count = self.dataManager.countEvent(forDate: date as NSDate)
            customCell.eventCounterImageView.isHidden = count > 0 ? false : true
            customCell.eventCounterImageView.image = date.isDateInToday ? self.countImageA : self.countImageB
            customCell.eventCounterLabel.isHidden = count > 0 ? false : true
            customCell.eventCounterLabel.text = "\(count)"
        })
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
        self.handleCellSelection(view: cell, cellState: cellState, isToday: date.isDateInToday)
        self.handleCellTextColor(view: cell, cellState: cellState)
        
        // Post notification
        let object = ["date": date]
        let name = Notification.Name(rawValue: "CAL_NOTIFICATION_DAY_SELECTION")
        self.notification.post(name: name, object: object)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
        self.handleCellSelection(view: cell, cellState: cellState, isToday: date.isDateInToday)
        self.handleCellTextColor(view: cell, cellState: cellState)
    }
    
    // MARK: - Handle the text color of the calendar
    
    func handleCellTextColor(view: JTAppleDayCellView?, cellState: CellState) {
        guard let customCell = view as? CALCalendarDayCellView else { return }
        customCell.dayLabel.textColor = cellState.isSelected ? self.colorA : cellState.dateBelongsTo == .thisMonth ? self.colorB : self.colorC
        
    }
    
    // MARK: - Handle the calendar selection
    
    func handleCellSelection(view: JTAppleDayCellView?, cellState: CellState, isToday: Bool) {
        guard let customCell = view as? CALCalendarDayCellView  else { return }
        customCell.selectedView.isHidden = cellState.isSelected ? false : true
        customCell.backgroundColor = isToday ? self.colorD : self.colorA
    }
    
}

// MARK: - UIColor Extension for Hexadecimal As Number and String

extension UIColor {
    convenience init(colorWithHexValue value: Int, alpha: CGFloat = 1.0){
        self.init(
            red: CGFloat((value & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((value & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(value & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
    
    convenience init(hexString: String) {
        let hexString = (hexString as NSString).trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
}
