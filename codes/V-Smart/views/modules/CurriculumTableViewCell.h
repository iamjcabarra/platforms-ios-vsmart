//
//  CurriculumTableViewCell.h
//  V-Smart
//
//  Created by Julius Abarra on 9/28/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurriculumTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblCurriculumName;
@property (strong, nonatomic) IBOutlet UILabel *lblCurriculumDescription;
@property (strong, nonatomic) IBOutlet UIButton *butRadio;

@end
