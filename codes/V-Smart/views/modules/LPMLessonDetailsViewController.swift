//
//  LPMLessonDetailsViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 24/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMLessonDetailsViewController: UIViewController {
    
    @IBOutlet fileprivate var headerView: UIView!
    @IBOutlet fileprivate var bodyView: UIView!
    @IBOutlet fileprivate var segmentedControl: UISegmentedControl!
    
    fileprivate let kViewSwapperSegueIdentifier = "SHOW_LESSON_DETAILS_SWAPPER"
    fileprivate let kUBDSegueIdentifier = "SHOW_UBD_COLLAPSIBLE_CONTENT"
    fileprivate let kSMDSegueIdentifier = "SHOW_SMD_COLLAPSIBLE_CONTENT"
    fileprivate let kDLLSegueIdentifier = "SHOW_DLL_COLLAPSIBLE_CONTENT"
    
    fileprivate let notification = NotificationCenter.default
    fileprivate var viewSwapper: LPMLessonDetailsViewSwapper!
    fileprivate var days = [Int]()
    
    // MARK: - Data Manager
    
    fileprivate lazy var lpmDataManager: LPMDataManager = {
        let lpdm = LPMDataManager.sharedInstance
        return lpdm
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let template = self.lpmDataManager.stringValue(self.lpmDataManager.fetchUserDefaultsObject(forKey: "LPM_USER_DEFAULT_TEMPLATE_ID"))
    
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.allowUserInteraction], animations: {
            self.headerView.isHidden = (template == LPMConstants.Template.DLL) ? false : true
            }, completion: { (animate) in
        })
        
        if template == LPMConstants.Template.DLL {
            self.segmentedControl.removeAllSegments()
            self.segmentedControl.tintColor = UIColor(rgba: "#B5F0FF")
            self.segmentedControl.addTarget(self, action: #selector(self.segmentedControlAction(_:)), for: .valueChanged)
            
            guard let dayList = self.lpmDataManager.fetchDLLTemplateContentDays() else {
                print("can't render ddl content days")
                self.popToViewController(LPMTemplateViewController.self)
                return
            }
            
            let attributeA = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16), NSForegroundColorAttributeName: UIColor(rgba: "#00AEEF")]
            self.segmentedControl.setTitleTextAttributes(attributeA, for: UIControlState.selected)
            
            let attributeB = [NSFontAttributeName: UIFont.systemFont(ofSize: 16), NSForegroundColorAttributeName: UIColor(rgba: "#00AEEF")]
            self.segmentedControl.setTitleTextAttributes(attributeB, for: UIControlState())

            if dayList.count > 0 {
                var index = 0
                
                for day in dayList {
                    if let id = day["id"] as? Int, let name = day["day"] as? String {
                        self.days.append(id)
                        self.segmentedControl.insertSegment(withTitle: name, at: index, animated: true)
                        index += 1
                    }
                }
                
                self.segmentedControl.selectedSegmentIndex = 0
            }
        }
        
        self.loadCollapsibleContentForTempalte(template!)
    }
    
    func segmentedControlAction(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        let day = self.days[index]
        self.lpmDataManager.save(object: day as AnyObject!, forKey: "LPM_USER_DEFAULT_DLL_TEMPLATE_DAY_ID")
        self.notification.post(name: Notification.Name(rawValue: "LPM_NOTIFICATION_DLL_TEMPLATE_RELOAD_COLLAPSIBLE_TABLE_VIEW"), object: nil)
    }
    
    // MARK: - Popping View Controller
    
    fileprivate func popToViewController(_ controller: AnyClass) {
        DispatchQueue.main.async(execute: {
            for vc in (self.navigationController?.viewControllers)! {
                if vc.isKind(of: controller)  {
                    _ = self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
        // MARK: - Loading Collapsible Content
    
    fileprivate func loadCollapsibleContentForTempalte(_ template: String) {
        var segueIdentifier = self.kDLLSegueIdentifier
        
        if template == LPMConstants.Template.DLL {
            segueIdentifier = self.kDLLSegueIdentifier
        }
        
        if template == LPMConstants.Template.SMD {
            segueIdentifier = self.kSMDSegueIdentifier
        }
        
        DispatchQueue.main.async(execute: {
            self.viewSwapper.swapToViewControllerWithSegueIdentifier(segueIdentifier)
        })
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.kViewSwapperSegueIdentifier {
            self.viewSwapper = segue.destination as? LPMLessonDetailsViewSwapper
        }
    }

}
