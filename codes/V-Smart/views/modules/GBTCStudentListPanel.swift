//
//  GBTCStudentListPanel.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 26/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTCStudentListPanel: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var sortSegmentedControl: UISegmentedControl!
    
    fileprivate let cellIdentifier = "gbt_student_panel_cell"
    var isAscending = true
    var student_name = ""
    var term_id = "1"
    
    var sort_by_gender = false
    
    var scrollDelegate : ScrollCommunicationDelegate?
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>?
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    var is_quarter = false
    
    fileprivate let notification = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let cellNib = UINib(nibName: "GBTStudentListCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellIdentifier)
        
        self.sortSegmentedControl.addTarget(self, action: #selector(self.sortSegmentedControlAction(_:)), for:.valueChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sortSegmentedControlAction(_ segment: UISegmentedControl) {
        if segment.selectedSegmentIndex == 0 {
            self.sort_by_gender = false
        } else {
            self.sort_by_gender = true
        }
        
        self.reloadFetchedResultsController()
        self.notification.post(name: Notification.Name(rawValue: "GBTC_STUDENT_SORT"), object: self.sort_by_gender)
    }
}

extension GBTCStudentListPanel: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, NSFetchedResultsControllerDelegate  {
    
    func reloadList(forTermID term_id: String) {
//        self.term_id = term_id
        reloadFetchedResultsController()
    }
    
    func reloadList(isQuarter is_quarter: Bool) {
        //        self.term_id = term_id
        self.is_quarter = is_quarter
        reloadFetchedResultsController()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! GBTStudentListCell
        cell.backgroundColor = UIColor.clear
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ cell: GBTStudentListCell, atIndexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: atIndexPath) 
        guard
            let name = mo.value(forKey: "name") as? String,
            let gender = mo.value(forKey: "gender") as? String
            else { return }
        
        var mOrF = "n/a"
        
        if gender.contains("1_") {
            mOrF = " - M"
        } else if gender.contains("2_") {
            mOrF = " - F"
        }
        
        cell.customLabel.text = name + mOrF
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: - Fetched Results Controller
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = gbdm.getMainContext()
        let ascending = self.isAscending
        let entity = GBTConstants.Entity.GRADESUMMARYSTUDENT
  
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
//        let fetchRequest = NSFetchRequest(entityName: entity)
        fetchRequest.fetchBatchSize = 20
        
        //PREDICATE
        /*
         NOTE: GB_SELECTED_TERM_ID <- please declared this in the constants
         FOR NOW: we are hard coding the term_id value
         */
        
        var finalPredicate = [NSComparisonPredicate]()
        if self.student_name.characters.count > 0 {
            let predicateName = NSComparisonPredicate(keyPath: "name", withValue: self.student_name, isExact: false)
            finalPredicate.append(predicateName)
        }
        
        if self.is_quarter {
            let forQuarterPredicate = NSComparisonPredicate(keyPath: "for_quarter", withValue: "1", isExact: true)
            finalPredicate.append(forQuarterPredicate)
        } else {
            let forQuarterPredicate = NSComparisonPredicate(keyPath: "for_quarter", withValue: "0", isExact: true)
            let predicateTerm = NSComparisonPredicate(keyPath: "term_id", withValue: self.term_id, isExact: true)
            finalPredicate.append(predicateTerm)
            finalPredicate.append(forQuarterPredicate)
        }
        
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: finalPredicate)
        
        let descriptorSelector = #selector(NSString.localizedCompare(_:))
        if self.sort_by_gender {
            let sortGender = NSSortDescriptor(key: "gender", ascending: ascending, selector:descriptorSelector)
            fetchRequest.sortDescriptors = [sortGender]
        } else {
            let sortName = NSSortDescriptor(key: "name", ascending: ascending, selector:descriptorSelector)
            fetchRequest.sortDescriptors = [sortName]
        }
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        let tableViewController = tableView
        
        switch type {
        case .insert:
            tableViewController?.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableViewController?.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        let tableViewController = tableView
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableViewController?.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = tableViewController?.cellForRow(at: indexPath) {
                    configureCell(cell as! GBTStudentListCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableViewController?.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y;
        self.scrollDelegate?.gbScrollViewDidScroll(GBPanelType.StudentPanel, scrollTo: offsetY)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewWillBeginDragging(GBPanelType.StudentPanel)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.StudentPanel)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate == false {
            self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.StudentPanel)
        }
    }
    
}

extension GBTCStudentListPanel: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("DID CHANGE \(searchText)")
        self.student_name = searchText
        self.reloadFetchedResultsController()
        self.notification.post(name: Notification.Name(rawValue: "GBTC_STUDENT_SEARCH"), object: searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("CANCEL BUTTON")
    }
    
}

