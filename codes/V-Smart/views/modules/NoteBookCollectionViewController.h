//
//  NoteBookCollectionViewController.h
//  V-Smart
//
//  Created by VhaL on 3/6/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteBookCollectionViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *noteBookArray;
-(void)loadData;
-(void)castOtherController;
@property (nonatomic, strong) id otherNoteBookViewController;
-(void)reflectAddedNoteBookInView:(NoteBook*)newNoteBook;
-(void)reflectModifiedNoteBookInView:(NoteBook*)modifiedNoteBook inIndexPath:(NSIndexPath*)indexPath;
-(void)reflectDeletedNoteBookInView:(NSIndexPath*)indexPath;
@end
