//
//  LPMStageContentTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 05/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMStageContentTableViewCell: UITableViewCell {
    
    @IBOutlet var processNameLabel: UILabel!
    @IBOutlet var processContentText: UITextView!
    @IBOutlet var uploadFileButton: UIButton!
    @IBOutlet var deleteFileButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func disableFileUploadCapability(_ disable: Bool) {
        if (disable) {
            self.processContentText.isHidden = true
            self.uploadFileButton.isHidden = false
            self.deleteFileButton.isHidden = false
        }
        else {
            self.processContentText.isHidden = false
            self.uploadFileButton.isHidden = true
            self.deleteFileButton.isHidden = true
        }
    }
    
    func updateUploadFileButtonTitle(_ title: String) {
        var newTitle = title
        
        if (title == "") {
            newTitle = NSLocalizedString("No file selected.", comment: "")
        }
        
        self.uploadFileButton.setImage(UIImage(named: "import 24x24.png"), for: UIControlState())
        self.uploadFileButton.setTitle("   \(newTitle)", for: UIControlState())
    }

}
