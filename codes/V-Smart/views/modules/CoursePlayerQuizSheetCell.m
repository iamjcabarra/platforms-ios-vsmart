//
//  CoursePlayerQuizSheetCell.m
//  V-Smart
//
//  Created by Carmelito Bayarcal on 29/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "CoursePlayerQuizSheetCell.h"

#import "UIImageView+WebCache.h"

#import "TestGuruDataManager.h"
#import "TestGuruDataManager+Course.h"
#import "QuestionTextItemCell.h"
#import "QuestionPictureItemCell.h"
#import "QuestionRadioItemCell.h"
#import "QuestionTextAreaItemCell.h"
#import "QuestionImageRadioItemCell.h"
#import <AssetsLibrary/AssetsLibrary.h> 
#import "UITextView+Placeholder.h"

#import "QuestionTextWithMathJaxCell.h"
#import "QuestionRadioItemWithMathJaxCell.h"
#import "QuestionImageRadioItemWithMathJaxCell.h"

#import "V_Smart-Swift.h"

typedef NS_ENUM(NSUInteger, TGQuestionType) {
    TGQuestionTypeText,
    TGQuestionTypePicture,
    TGQuestionTypeRadioButton,
    TGQuestionTypeTextArea,
    TGQuestionTypeImageRadioButton,
    
    TGQuestionTypeTextWithMathJax,
    TGQuestionTypeChoiceWithMathJax,
    TGQuestionTypeImageChoiceWithMathJax
};

@implementation NSString (HtmlCustomCheck)

- (BOOL)containsHTML {
    
    NSString *stringcopy = [self copy];
    
    NSString *expression = @"<[^>]+>";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:stringcopy
                                                        options:0
                                                          range:NSMakeRange(0, [stringcopy length])];
    if (numberOfMatches == 1) {
        return YES;
    }
    
    return NO; //default value
}

@end

@interface CoursePlayerQuizSheetCell() <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, UITextViewDelegate, QuestionTextWithMathJaxCellDelegate, QuestionRadioItemWithMathJaxCellDelegate, QuestionImageRadioItemWithMathJaxCellDelegate>
@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) NSArray *qItems;
//@property (strong, nonatomic) NSArray *cItems;
@property (assign, nonatomic) BOOL clearTable;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (weak, nonatomic) IBOutlet UITableView *choicesTable;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;

@property (strong, nonatomic) UIImage *loadedAssetImage;
@property (strong, nonatomic) ALAssetsLibrary *assetslibrary;

@property (strong, nonatomic) NSMutableOrderedSet *choiceImageSet;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) NSNotificationCenter *notif;

@property (strong, nonatomic) TestGuruDataManager *tm;


@end

@implementation CoursePlayerQuizSheetCell

static NSString *kQTextID = @"cell_sample_question_text_id";
static NSString *kQImageID = @"cell_sample_question_picture_id";
static NSString *kQChoiceID = @"cell_sample_question_choice_id";
static NSString *kQTextAreaID = @"cell_sample_question_textarea_id";
static NSString *kQImageChoiceID = @"cell_sample_question_choice_image_id";

static NSString *kQTextWithMathJaxID = @"cell_sample_question_text_with_mathjax_id";
static NSString *kQChoiceWithMathJaxID = @"cell_sample_question_choice_with_mathjax_id";
static NSString *kQImageChoiceWithMathJaxID = @"cell_sample_question_choice_image_with_mathjax_id";

- (void)awakeFromNib {
    // Initialization code
    
    self.tm = [TestGuruDataManager sharedInstance];
    
    self.clearTable = YES;
    [self.table reloadData];
    [self.choicesTable reloadData];
    self.clearTable = NO;
    
    self.assetslibrary = [[ALAssetsLibrary alloc] init];
    
    self.notif = [NSNotificationCenter defaultCenter];
    
    // Resusable Cell
    UINib *questionTextNib = [UINib nibWithNibName:@"QuestionTextWithMathJaxCell" bundle:nil];
    [self.table registerNib:questionTextNib forCellReuseIdentifier:kQTextWithMathJaxID];
    
    // Resusable Cell
    UINib *choiceRadioNib = [UINib nibWithNibName:@"QuestionRadioItemWithMathJaxCell" bundle:nil];
    [self.choicesTable registerNib:choiceRadioNib forCellReuseIdentifier:kQChoiceWithMathJaxID];
    
    // Resusable Cell
    UINib *choiceImageRadioNib = [UINib nibWithNibName:@"QuestionImageRadioItemWithMathJaxCell" bundle:nil];
    [self.choicesTable registerNib:choiceImageRadioNib forCellReuseIdentifier:kQImageChoiceWithMathJaxID];
    
}

- (NSString *)stringValue:(id)object {
    
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"]) {
        return @"";
    }
    
    return value;
}

- (void)displayObject:(NSManagedObject *)object {
    
    NSLog(@"object ~>%@<~", object);
    
    if (object != nil) {
//        
//        [self.table reloadData];
//        [self.choicesTable reloadData];
//        
        self.choiceImageSet = [NSMutableOrderedSet orderedSet];
        
        NSMutableOrderedSet *qOrdered_set = [NSMutableOrderedSet orderedSet];
//        NSMutableOrderedSet *cOrdered_set = [NSMutableOrderedSet orderedSet];
        NSMutableOrderedSet *imageSet = [NSMutableOrderedSet orderedSet];
        
        //QUESTION TEXT
        NSString *question_text = [self stringValue: [object valueForKey:@"question_text"] ];
        NSString *question_type_id = [self stringValue: [object valueForKey:@"question_type_id"] ];
        
        TGQuestionType questionTextType = TGQuestionTypeText;
        if ([question_text containsHTML] || [question_text containsString:@"-blank-"]) {
            questionTextType = TGQuestionTypeTextWithMathJax;
        }
        
        NSDictionary *textData = @{@"type": @(questionTextType),
                                   @"content": question_text,
                                   @"question_type_id":question_type_id};
        [qOrdered_set addObject:textData];
        
        //QUESTION IMAGE (if any)
        NSString *image_one = [NSString stringWithFormat:@"%@", [object valueForKey:@"image_one"]];
        if ([self isNotEmpty:image_one]) {
            NSData *image_one_data = [NSData dataWithData:[object valueForKey:@"image_one_data"]];
            NSDictionary *imageData = @{@"index":@"1",
                                        @"data":image_one,
                                        @"image_data":image_one_data};
            [imageSet addObject:imageData];
        }
        
        NSString *image_two = [NSString stringWithFormat:@"%@", [object valueForKey:@"image_two"]];
        if ([self isNotEmpty:image_two]) {
            NSData *image_two_data = [NSData dataWithData:[object valueForKey:@"image_two_data"]];
            NSDictionary *imageData = @{@"index":@"2",
                                        @"data":image_two,
                                        @"image_data":image_two_data};
            [imageSet addObject:imageData];
        }
        
        NSString *image_three = [NSString stringWithFormat:@"%@", [object valueForKey:@"image_three"]];
        if ([self isNotEmpty:image_three]) {
            NSData *image_three_data = [NSData dataWithData:[object valueForKey:@"image_three_data"]];
            NSDictionary *imageData = @{@"index":@"3",
                                        @"data":image_three,
                                        @"image_data":image_three_data};
            [imageSet addObject:imageData];
        }
        
        NSString *image_four = [NSString stringWithFormat:@"%@", [object valueForKey:@"image_four"]];
        if ([self isNotEmpty:image_four]) {
            NSData *image_four_data = [NSData dataWithData:[object valueForKey:@"image_four_data"]];
            NSDictionary *imageData = @{@"index":@"4",
                                        @"data":image_four,
                                        @"image_data":image_four_data};
            [imageSet addObject:imageData];
        }
        if (imageSet.count > 0) {
            NSDictionary *imageData = @{@"type": @(TGQuestionTypePicture),
                                        @"content": imageSet};
            [qOrdered_set addObject:imageData];
        }
        
        NSString *question_id = [NSString stringWithFormat:@"%@",[object valueForKey:@"question_id"]];
        NSArray *choices = [self.tm choicesForQuestionID:question_id];
        
        NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order_number_int" ascending:YES];
        NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
        NSArray *sortedChoices = [choices sortedArrayUsingDescriptors:descriptors];
        
        if ((sortedChoices != nil) &&  (sortedChoices.count > 0)) {
            for (NSManagedObject *c in sortedChoices) {
                NSString *choice_text = [self stringValue:[c valueForKey:@"text"]];
                NSString *choice_image = [self stringValue:[c valueForKey:@"question_choice_image"]];
                NSData *choice_image_data = [NSData dataWithData:[c valueForKey:@"question_choice_image_data"]];
                
                BOOL choiceHasImage = choice_image.length > 0;
                
                if (choiceHasImage) {
                    NSDictionary *choiceDict = @{
                                                 @"choice_title":choice_text,
                                                 @"data":choice_image,
                                                 @"image_data":choice_image_data};
                    
                    [self.choiceImageSet addObject:choiceDict];
                }
            }
        }
        
        
        NSString *points = [NSString stringWithFormat:@"%@", [object valueForKey:@"points"]];

        
        NSString *pointString = NSLocalizedString(@"Points", nil);
        CGFloat pointInt = [points floatValue];
        
        if (pointInt <= 1.0) {
            pointString = NSLocalizedString(@"Point", nil);
        }
        
        self.pointsLabel.text = [NSString stringWithFormat:@"%@ %@", points, pointString];
        [self.pointsLabel highlightString:points size:14.0f color:[UIColor whiteColor]];
        
        self.qItems = [NSArray arrayWithArray: [qOrdered_set array] ];
//        self.cItems = [NSArray arrayWithArray: [cOrdered_set array] ];
        [self.table reloadData];
//        [self.choicesTable reloadData];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor clearColor];
}

- (BOOL)isNotEmpty:(NSString *)string {
    BOOL isNotEmpty = NO;
    
    string = [string stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"<null>" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"null" withString:@""];
    
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceCharacterSet];
    string = [string stringByTrimmingCharactersInSet:whiteSpace];
    
    if (string.length > 0) {
        isNotEmpty = YES;
    }
    
    return isNotEmpty;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
//    if (self.clearTable) {
//        return 0;
//    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    if (tableView == self.table) {
        count = [self.qItems count];
    } else {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        count = [sectionInfo numberOfObjects];
    }
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *data = @{};
    
    NSUInteger type = [@(TGQuestionTypeText) unsignedIntegerValue];
    
    if (tableView == self.table) {
        data = self.qItems[indexPath.row];
        type = [(NSNumber *)data[@"type"] unsignedIntegerValue];
    } else {
        NSManagedObject *cmo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//        NSManagedObject *qmo = [cmo valueForKey:@"question"];
        
        NSString *choice_text = [cmo valueForKey:@"text"];
        NSString *question_choice_image = [cmo valueForKey:@"question_choice_image"];
        
        NSString *question_type_id = [NSString stringWithFormat:@"%@",[cmo valueForKey:@"question_type_id"]];
        if ([question_type_id isEqual:@"6"] || [question_type_id isEqual:@"9"] || [question_type_id isEqual:@"10"]) {
            type = [@(TGQuestionTypeTextArea) unsignedIntegerValue];
        } else {
            type = [@(TGQuestionTypeRadioButton) unsignedIntegerValue];
            
            if ([choice_text containsHTML]) {
                if (question_choice_image.length > 0) {
                    type = [@(TGQuestionTypeImageChoiceWithMathJax) unsignedIntegerValue];
                } else {
                    type = [@(TGQuestionTypeChoiceWithMathJax) unsignedIntegerValue];;
                }
            } else {
                if (question_choice_image.length > 0) {
                    type = [@(TGQuestionTypeImageRadioButton) unsignedIntegerValue];;
                }
            }
        }
    }
    
    NSString *identifier = [self identifierForType:type];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    //CHECK
    [self configureCell:cell tableView:tableView indexPath:indexPath];
    
    return cell;
}

- (NSString *)identifierForType:(TGQuestionType)type {
    
    NSString *identifier = kQTextID;
    
    if (type == TGQuestionTypeText) {
        identifier = kQTextID;
    }
    
    if (type == TGQuestionTypePicture) {
        identifier = kQImageID;
    }
    
    if (type == TGQuestionTypeRadioButton) {
        identifier = kQChoiceID;
    }
    
    if (type == TGQuestionTypeImageRadioButton) {
        identifier = kQImageChoiceID;
    }
    
    if (type == TGQuestionTypeTextArea) {
        identifier = kQTextAreaID;
    }
    
    if (type == TGQuestionTypeTextWithMathJax) {
        identifier = kQTextWithMathJaxID;
    }
    
    if (type == TGQuestionTypeChoiceWithMathJax) {
        identifier = kQChoiceWithMathJaxID;
    }
    
    if (type == TGQuestionTypeImageChoiceWithMathJax) {
        identifier = kQImageChoiceWithMathJaxID;
    }
    
    
    return identifier;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)configureCell:(UITableViewCell *)tvc tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = tvc.reuseIdentifier;
    
    /*
     NOTE: VERY IMPORTANT
     CELL HANDLING FOR "table" INSTANCE
     */
    
    if (tableView == self.table) {
        if ([identifier isEqualToString:kQTextID]) {
            QuestionTextItemCell *cell = (QuestionTextItemCell *)tvc;
            cell.backgroundColor = [UIColor clearColor];
            [self configureTextCell:cell indexPath:indexPath];
        }
        
        if ([identifier isEqualToString:kQImageID]) {
            QuestionPictureItemCell *cell = (QuestionPictureItemCell *)tvc;
            cell.backgroundColor = [UIColor clearColor];
            [self configurePictureCell:cell indexPath:indexPath];
        }
        
        if ([identifier isEqualToString:kQTextWithMathJaxID]) {
            QuestionTextWithMathJaxCell *cell = (QuestionTextWithMathJaxCell *)tvc;
            [self configureTextWithMathJaxCell:cell tableView:tableView indexPath:indexPath];
        }
    }
    
    /*
     NOTE: VERY IMPORTANT
     CELL HANDLING FOR "choicesTable" INSTANCE
     */
    
    if (tableView == self.choicesTable) {
        [self configureCell:tvc atIndexPath:indexPath];
    }
    
}


- (void)configureCell:(UITableViewCell *)tvc atIndexPath:(NSIndexPath *)indexPath {
    
    
    NSString *identifier = tvc.reuseIdentifier;
    
    if ([identifier isEqualToString:kQChoiceID]) {
        QuestionRadioItemCell *cell = (QuestionRadioItemCell *)tvc;
        cell.backgroundColor = [UIColor clearColor];
        [self configureRadioButtonCell:cell indexPath:indexPath];
    }
    
    if ([identifier isEqualToString:kQImageChoiceID]) {
        QuestionImageRadioItemCell *cell = (QuestionImageRadioItemCell *)tvc;
        cell.backgroundColor = [UIColor clearColor];
        [self configureImageRadioButtonCell:cell indexPath:indexPath];
    }
    
    if ([identifier isEqualToString:kQTextAreaID]) {
        QuestionTextAreaItemCell *cell = (QuestionTextAreaItemCell *)tvc;
        cell.backgroundColor = [UIColor clearColor];
        [self configureTextAreaCell:cell indexPath:indexPath];
    }
    
    if ([identifier isEqualToString:kQChoiceWithMathJaxID]) {
        QuestionRadioItemWithMathJaxCell *cell = (QuestionRadioItemWithMathJaxCell *)tvc;
        [self configureRadioButtonWithMathJaxCell:cell indexPath:indexPath];
    }
    
    if ([identifier isEqualToString:kQImageChoiceWithMathJaxID]) {
        QuestionImageRadioItemWithMathJaxCell *cell = (QuestionImageRadioItemWithMathJaxCell *)tvc;
        [self configureImageRadioButtonWithMathJaxCell:cell indexPath:indexPath];
    }
    
}

- (void)configureTextCell:(QuestionTextItemCell *)cell indexPath:(NSIndexPath *)indexPath {
    NSDictionary *data = self.qItems[indexPath.row];
    NSString *question_title = [self stringValue: data[@"content"] ];
    question_title = [question_title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    
    NSString *fontString = [self.tm fetchObjectForKey:kCP_SELECTED_FONT_SIZE];
    CGFloat fontSize = fontString.floatValue;
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:fontSize];
    cell.questionText.font = font;
    cell.questionText.text = question_title;
}

- (void)configureTextWithMathJaxCell:(QuestionTextWithMathJaxCell *)cell tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *data = self.qItems[indexPath.row];
    NSString *question_title = [self stringValue: data[@"content"] ];
    NSString *question_type_id = [self stringValue: data[@"question_type_id"] ];
    
    cell.delegate = self;
    cell.indexPath = indexPath;
    
    if ([question_title containsString:@"-blank-"] && [question_type_id isEqualToString:@"2"]) {
        NSManagedObject *fillInTheBlanksMo = [self.tm fetchFillInTheBlanksAnswerForQuestionID:self.questionID];//[self.tm fetchFillInTheBlanksAnswerForQuestionID:self.questionID];
        NSString *fillInTheBlanksAnswer = [fillInTheBlanksMo valueForKey:@"text"];
        
        NSString *cssForInput = @"<style>input {border-radius: 0;border: 0;outline: 0;text-align: center;background: transparent;border-bottom: 1px solid black;min-width:80px;font-size:17px;}</style>";
        
        NSString *scriptForInput = @"<script> function triggerInput() { var inputField = document.getElementById('blankfield_id'); inputField.style.width = ((inputField.value.length + 1) * 13) + 'px'; window.location.href = 'inapp://inputchange'; } </script>";
        
        NSString *inputTextInjection = [NSString stringWithFormat:@"%@<input id='blankfield_id' type='text' value='%@' placeholder='answer' oninput='triggerInput()' onselectstart='return false' onpaste='return false' onCopy='return false' onCut='return false' onDrag='return false' onDrop='return false' autocomplete='off' autocapitalize='off' autocorrect='off'> %@", cssForInput, fillInTheBlanksAnswer, scriptForInput];
        
        NSString *blankReplaced = [question_title stringByReplacingOccurrencesOfString:@"-blank-" withString: inputTextInjection];
        question_title = blankReplaced;
    }
    
    [cell loadWebViewWithContents:question_title];
}

- (void)configureRadioButtonWithMathJaxCell:(QuestionRadioItemWithMathJaxCell *)cell indexPath:(NSIndexPath *)indexPath {
    
    [cell.radioButton setImage:[UIImage imageNamed:@"gray_empty_button.png"] forState:UIControlStateNormal];
    [cell.radioButton setImage:[UIImage imageNamed:@"check_icn_active150.png"] forState:UIControlStateSelected];
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *choice_title = [self stringValue: [mo valueForKey:@"text"] ];
    
    cell.delegate = self;
    cell.indexPath = indexPath;
    [cell loadWebViewWithContents:choice_title];
    //    [cell load]
    
    [cell.radioButton addTarget:self action:@selector(selectAnswerAction:) forControlEvents:UIControlEventTouchUpInside];
    
//    NSString *is_correct = [self stringValue: data[@"is_correct"] ];
//    [cell.radioButton setImage:[UIImage imageNamed:@"vibe_radio_button_checked2_48px"] forState:UIControlStateNormal];
//    if (![is_correct isEqualToString:@"0"]) {
//        [cell.radioButton setImage:[UIImage imageNamed:@"vibe_radio_button_checked_48px"] forState:UIControlStateNormal];
//    }
    
    NSString *answer = [self stringValue: [mo valueForKey:@"answer"] ];
    cell.radioButton.selected = NO;
    if ([answer isEqualToString:@"1"]) {
        cell.radioButton.selected = YES;
    }
}

- (void)configureImageRadioButtonWithMathJaxCell:(QuestionImageRadioItemWithMathJaxCell *)cell indexPath:(NSIndexPath *)indexPath {
    
    //    NSDictionary *data = self.cItems[indexPath.row];
    //    NSString *choice_title = [self stringValue: data[@"content"] ];
    //
    //    //    [cell load]
    //
    //    NSString *is_correct = [self stringValue: data[@"is_correct"] ];
    //    [cell.radioButton setImage:[UIImage imageNamed:@"vibe_radio_button_checked2_48px"] forState:UIControlStateNormal];
    //    if (![is_correct isEqualToString:@"0"]) {
    //        [cell.radioButton setImage:[UIImage imageNamed:@"vibe_radio_button_checked_48px"] forState:UIControlStateNormal];
    //    }
    
    
    [cell.radioButton setImage:[UIImage imageNamed:@"gray_empty_button.png"] forState:UIControlStateNormal];
    [cell.radioButton setImage:[UIImage imageNamed:@"check_icn_active150.png"] forState:UIControlStateSelected];
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *choice_title = [self stringValue: [mo valueForKey:@"text"] ];
    NSString *choice_image = [self stringValue: [mo valueForKey:@"question_choice_image"] ];
    NSData *choice_image_data = [NSData dataWithData:[mo valueForKey:@"question_choice_image_data"] ];
    NSIndexPath *indexPathCheck = self.indexPath;
    
    cell.delegate = self;
    cell.indexPath = indexPath;
    [cell loadWebViewWithContents:choice_title];
    
//    NSString *is_correct = [self stringValue: data[@"is_correct"] ];
//    [cell.radioButton setImage:[UIImage imageNamed:@"vibe_radio_button_checked2_48px"] forState:UIControlStateNormal];
//    if (![is_correct isEqualToString:@"0"]) {
//        [cell.radioButton setImage:[UIImage imageNamed:@"vibe_radio_button_checked_48px"] forState:UIControlStateNormal];
//    }
    
    [cell.choiceImageButton addTarget:self action:@selector(choiceImageTapAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //    cell.buttonTitle.text = choice_title;
    NSURL *imageURL = [NSURL URLWithString:choice_image ];
    
    NSLog(@"CHOICE IMAGE [%@]", choice_image);
    
    if (indexPathCheck == self.indexPath) {
        
        
        if ([[imageURL scheme] isEqualToString:@"assets-library"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.choiceImage.image = [UIImage imageWithData:choice_image_data];
            });
        } else {
            [cell.choiceImage sd_setImageWithURL:imageURL];
        }
        //        [cell.choiceImage sd_setImageWithURL:imageURL];
        
        NSDictionary *choiceDict = @{
                                     @"choice_title":choice_title,
                                     @"data":choice_image,
                                     @"image_data":choice_image_data};
        
//        if (cell.choiceImageButton.tag == 0) {
//            cell.choiceImageButton.tag = self.choiceImageSet.count + 1;
//            NSLog(@"CHOICE TAG [%ld]", cell.choiceImageButton.tag);
//        }
//        if (![self.choiceImageSet containsObject:choiceDict]) {
//            NSLog(@"Pasok add object");
//            [self.choiceImageSet addObject:choiceDict];
//        }
        
        if ([self.choiceImageSet containsObject:choiceDict]) {
            NSInteger index = [self.choiceImageSet indexOfObject:choiceDict];
            index += 1;
            cell.choiceImageButton.tag = index;
        }
        
        [cell.radioButton addTarget:self action:@selector(selectAnswerAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    NSString *answer = [self stringValue: [mo valueForKey:@"answer"] ];
    cell.radioButton.selected = NO;
    if ([answer isEqualToString:@"1"]) {
        cell.radioButton.selected = YES;
    }
}

- (void)configurePictureCell:(QuestionPictureItemCell *)cell indexPath:(NSIndexPath *)indexPath {
    NSDictionary *data = self.qItems[indexPath.row];
    NSOrderedSet *imageArray = [NSOrderedSet orderedSetWithOrderedSet:data[@"content"] ];
    NSInteger arrayCount = imageArray.count;
    
    [self setUpImageCell:cell count:arrayCount];
    [self setImage:cell array:imageArray];
}

- (void)configureRadioButtonCell:(QuestionRadioItemCell *)cell indexPath:(NSIndexPath *)indexPath {
    
    //        if ( (sortedChoices != nil) &&  (sortedChoices.count > 0) ) {
    //            for (NSManagedObject *c in sortedChoices) {
    //                NSLog(@"CHOICES OBJECT [%@]", c);
    //                NSString *choice_text = [self stringValue: [c valueForKey:@"text"] ];
    //                NSString *choice_image = [self stringValue: [c valueForKey:@"question_choice_image"] ];
    //                NSString *is_correct = [self stringValue: [c valueForKey:@"is_correct"] ];
    //                NSData *choice_image_data = [NSData dataWithData:[c valueForKey:@"question_choice_image_data"] ];
    //
    //                NSLog(@"CHOICE IMAGE ~~ %s [%@]", __PRETTY_FUNCTION__ ,choice_image);
    //
    //
    //                TGQuestionType type = TGQuestionTypeRadioButton;
    //
    //                if (choice_image.length > 0) {
    //                    type = TGQuestionTypeImageRadioButton;
    //                }
    //
    //                NSDictionary *choiceData = @{@"type": @(type),
    //                                             @"content": choice_text,
    //                                             @"choice_image": choice_image,
    //                                             @"is_correct": is_correct,
    //                                             @"choice_image_data":choice_image_data};
    //                [cOrdered_set addObject:choiceData];
    //            }
    //        }
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *choice_title = [self stringValue: [mo valueForKey:@"text"] ];
    cell.buttonTitle.text = choice_title;
    
    [cell.radioButton addTarget:self action:@selector(selectAnswerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *answer = [self stringValue: [mo valueForKey:@"answer"] ];
    cell.radioButton.selected = NO;
    if ([answer isEqualToString:@"1"]) {
        cell.radioButton.selected = YES;
    }
    
//    NSString *is_correct = [self stringValue: [mo valueForKey:@"is_correct"] ];
//    [cell.radioButton setImage:[UIImage imageNamed:@"vibe_radio_button_checked2_48px"] forState:UIControlStateNormal];
//    if (![is_correct isEqualToString:@"0"]) {
//        [cell.radioButton setImage:[UIImage imageNamed:@"vibe_radio_button_checked_48px"] forState:UIControlStateNormal];
//    }
    
    NSString *fontString = [self.tm fetchObjectForKey:kCP_SELECTED_FONT_SIZE];
    CGFloat fontSize = fontString.floatValue;
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:fontSize];
    cell.buttonTitle.font = font;
    cell.buttonTitle.text = choice_title;
}

- (void)configureImageRadioButtonCell:(QuestionImageRadioItemCell *)cell indexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *choice_title = [self stringValue: [mo valueForKey:@"text"] ];
    NSString *choice_image = [self stringValue: [mo valueForKey:@"question_choice_image"] ];
    NSData *choice_image_data = [NSData dataWithData:[mo valueForKey:@"question_choice_image_data"] ];
    NSIndexPath *indexPathCheck = self.indexPath;
    
//    NSString *is_correct = [self stringValue: [mo valueForKey:@"is_correct"] ];
//    [cell.radioButton setImage:[UIImage imageNamed:@"vibe_radio_button_checked2_48px"] forState:UIControlStateNormal];
//    if (![is_correct isEqualToString:@"0"]) {
//        [cell.radioButton setImage:[UIImage imageNamed:@"vibe_radio_button_checked_48px"] forState:UIControlStateNormal];
//    }
    
    [cell.choiceImageButton addTarget:self action:@selector(choiceImageTapAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.buttonTitle.text = choice_title;
    NSURL *imageURL = [NSURL URLWithString:choice_image ];
    
    NSLog(@"CHOICE IMAGE [%@]", choice_image);
    
    if (indexPathCheck == self.indexPath) {
        
        if ([[imageURL scheme] isEqualToString:@"assets-library"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.choiceImage.image = [UIImage imageWithData:choice_image_data];
            });
        } else {
            [cell.choiceImage sd_setImageWithURL:imageURL];
        }
        
        NSDictionary *choiceDict = @{
                                     @"choice_title":choice_title,
                                     @"data":choice_image,
                                     @"image_data":choice_image_data};
        
//        if (cell.choiceImageButton.tag == 0) {
//            cell.choiceImageButton.tag = self.choiceImageSet.count + 1;
//            NSLog(@"CHOICE TAG [%ld]", cell.choiceImageButton.tag);
//        }
//        if (![self.choiceImageSet containsObject:choiceDict]) {
//            NSLog(@"Pasok add object");
//            [self.choiceImageSet addObject:choiceDict];
//        }
        if ([self.choiceImageSet containsObject:choiceDict]) {
            NSInteger index = [self.choiceImageSet indexOfObject:choiceDict];
            index += 1;
            cell.choiceImageButton.tag = index;
        }
    }
    
    [cell.radioButton addTarget:self action:@selector(selectAnswerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *answer = [self stringValue: [mo valueForKey:@"answer"] ];
    cell.radioButton.selected = NO;
    if ([answer isEqualToString:@"1"]) {
        cell.radioButton.selected = YES;
    }
    
    NSString *fontString = [self.tm fetchObjectForKey:kCP_SELECTED_FONT_SIZE];
    CGFloat fontSize = fontString.floatValue;
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:fontSize];
    cell.buttonTitle.font = font;
    cell.buttonTitle.text = choice_title;
}

- (void)selectAnswerAction:(UIButton *)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.choicesTable];
    NSIndexPath *indexPath = [self.choicesTable indexPathForRowAtPoint:buttonPosition];
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *choiceID = [mo valueForKey:@"id"];
    
    ////////MULTIPLE ANSWER SUPPORT///////////
    
    // ENABLE MULTIPLE SELECT
    if (self.number_of_correct_answers > 1) {
        
        NSString *answer = [mo valueForKey:@"answer"];
        NSString *answer_value = ([answer isEqualToString:@"1"]) ? @"0" : @"1";
        
        NSDictionary *userData = @{@"answer":answer_value};
        NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:choiceID];
        [self updateAnswerCorrect:userData withPredicate:predicate];
    }
    
    // ONE ANSWER ONLY
    if (self.number_of_correct_answers == 1 || self.number_of_correct_answers == 0) {
        [self removeOtherCorrectAnswer];
        
        NSDictionary *userData = @{@"answer":@"1"};
        NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:choiceID];
        [self updateAnswerCorrect:userData withPredicate:predicate];
    }
}

- (void)configureTextAreaCell:(QuestionTextAreaItemCell *)cell indexPath:(NSIndexPath *)indexPath {
    cell.textArea.placeholder = NSLocalizedString(@"Type your answer here...", nil);
    cell.textArea.delegate = self;
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *text = [mo valueForKey:@"text"];
    
    NSString *fontString = [self.tm fetchObjectForKey:kCP_SELECTED_FONT_SIZE];
    CGFloat fontSize = fontString.floatValue;
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:fontSize];
    
    cell.textArea.font = font;
    cell.textArea.text = text;
}

- (void)setImage:(QuestionPictureItemCell *)cell array:(NSOrderedSet *)imageArray {
    
    BOOL oneHasImage = NO;
    BOOL twoHasImage = NO;
    BOOL threeHasImage = NO;
    
    NSIndexPath *indexPathCheck = self.indexPath;
    NSInteger imageArrayCount = imageArray.count;
    for (NSDictionary *imageDict in imageArray) {
        
        UIImageView *questionImageView = cell.qImageOne; // default image view
        
        NSString *imageIndex = [NSString stringWithFormat:@"%@", imageDict[@"index"]];
        
        NSString *imageURLString = [NSString stringWithFormat:@"%@", imageDict[@"data"]];
        NSURL *imageURL = [NSURL URLWithString:imageURLString];
        
        if (indexPathCheck == self.indexPath) {
            NSLog(@"IMAGE URL STRING [%@]", imageDict[@"data"]);
            
            if (imageArrayCount == 1) {
                questionImageView = cell.qImageOne;
                [cell.activityIndicator stopAnimating];
            }
            
            if (imageArrayCount == 2) {
                if (oneHasImage == NO) {
                    questionImageView = cell.qImageOne;
                    oneHasImage = YES;
                } else {
                    questionImageView = cell.qImageTwo;
                    [cell.activityIndicator stopAnimating];
                }
            }
            
            if (imageArrayCount == 3) {
                if ((twoHasImage) && (threeHasImage == NO)) {
                    questionImageView = cell.qImageThree;
                    [cell.activityIndicator stopAnimating];
                    threeHasImage = YES;
                }
                
                if ((oneHasImage) && (twoHasImage == NO)) {
                    questionImageView = cell.qImageTwo;
                    twoHasImage = YES;
                }
                
                if (oneHasImage == NO) {
                    questionImageView = cell.qImageOne;
                    oneHasImage = YES;
                }
                
            }
            
            if (imageArrayCount == 4) {
                if ([imageIndex isEqualToString:@"1"]) {
                    questionImageView = cell.qImageOne;
                }
                
                if ([imageIndex isEqualToString:@"2"]) {
                    questionImageView = cell.qImageTwo;
                }
                
                if ([imageIndex isEqualToString:@"3"]) {
                    questionImageView = cell.qImageThree;
                }
                
                if ([imageIndex isEqualToString:@"4"]) {
                    questionImageView = cell.qImageFour;
                    [cell.activityIndicator stopAnimating];
                }
            }
            
            if ([[imageURL scheme] isEqualToString:@"assets-library"]) {
                NSData *image_data = [NSData dataWithData:imageDict[@"image_data"]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    questionImageView.image = [UIImage imageWithData:image_data];
                });
            } else {
                [questionImageView sd_setImageWithURL:imageURL];
            }
            
            
            
        }
    }
}

- (void)setUpImageCell:(QuestionPictureItemCell *)cell count:(NSInteger )count {
    NSInteger imageCount = count;
    
    BOOL oneHidden = YES;
    BOOL twoHidden = YES;
    BOOL threeHidden = YES;
    BOOL fourHidden = YES;
    
    CGFloat oneLocation = 0;
    CGFloat twoLocation = 0;
    CGFloat threeLocation = 0;
    CGFloat fourLocation = 0;
    
    if (imageCount == 1) {
        oneHidden = NO;
    }
    
    if (imageCount == 2) {
        oneHidden = NO;
        twoHidden = NO;
        oneLocation -= 65.0f;
        twoLocation += 65.0f;
    }
    
    if (imageCount == 3) {
        oneHidden = NO;
        twoHidden = NO;
        threeHidden = NO;
        oneLocation -= 130.0f;
        threeLocation += 130.0f;
    }
    
    if (imageCount == 4) {
        oneHidden = NO;
        twoHidden = NO;
        threeHidden = NO;
        fourHidden = NO;
        oneLocation -= 195.0f;
        twoLocation -= 65.0f;
        threeLocation += 65.0f;
        fourLocation += 195.0f;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.qImageOne.hidden = oneHidden;
        cell.qImageTwo.hidden = twoHidden;
        cell.qImageThree.hidden = threeHidden;
        cell.qImageFour.hidden = fourHidden;
        
        cell.buttonOne.hidden = oneHidden;
        cell.buttonTwo.hidden = twoHidden;
        cell.buttonThree.hidden = threeHidden;
        cell.buttonFour.hidden = fourHidden;
        
        cell.qImageOneConstraint.constant = oneLocation;
        cell.qImageTwoConstraint.constant = twoLocation;
        cell.qImageThreeConstraint.constant = threeLocation;
        cell.qImageFourConstraint.constant = fourLocation;
    });
    
    cell.qImageOne.image = nil;
    cell.qImageTwo.image = nil;
    cell.qImageThree.image = nil;
    cell.qImageFour.image = nil;
    
    [cell.activityIndicator startAnimating];
    
    // for preview
    cell.buttonOne.tag = 1;
    cell.buttonTwo.tag = 2;
    cell.buttonThree.tag = 3;
    cell.buttonFour.tag = 4;
    
    [cell.buttonOne addTarget:self action:@selector(questionImageTapAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonTwo addTarget:self action:@selector(questionImageTapAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonThree addTarget:self action:@selector(questionImageTapAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonFour addTarget:self action:@selector(questionImageTapAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)questionImageTapAction:(UIButton *)sender {
    __weak typeof(self) wo = self;
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.table];
    NSIndexPath *indexPath = [self.table indexPathForRowAtPoint:buttonPosition];
    
    NSMutableDictionary *previewData = [NSMutableDictionary dictionary];
    NSDictionary *data = self.qItems[indexPath.row];
    NSOrderedSet *contentSet = data[@"content"];
    
    [previewData setObject:contentSet forKey:@"content"];
    
    if ([(NSObject*)wo.delegate respondsToSelector:@selector(transitionWithData:index:segueID:)]) {
        [wo.delegate transitionWithData:previewData index:sender.tag segueID:@"SAMPLE_QUESTION_IMAGE_VIEW"];
    }
}

- (void)choiceImageTapAction:(UIButton *)sender {
    __weak typeof(self) wo = self;
    NSMutableDictionary *previewData = [NSMutableDictionary dictionary];
    NSOrderedSet *contentSet = [NSOrderedSet orderedSetWithOrderedSet:self.choiceImageSet];
    
    [previewData setObject:contentSet forKey:@"content"];
    
    if ([(NSObject*)wo.delegate respondsToSelector:@selector(transitionWithData:index:segueID:)]) {
        [wo.delegate transitionWithData:previewData index:sender.tag segueID:@"SAMPLE_QUESTION_IMAGE_VIEW"];
    }
}

#pragma mark - Fetched results controller

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.choicesTable reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (NSFetchedResultsController *)fetchedResultsController
{
    BOOL isAscending = YES;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCoursePlayerQuestionChoiceEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    NSPredicate *question_id_predicate = [self.tm predicateForKeyPath:@"question_id" andValue:self.questionID];
    NSPredicate *question_type_predicate = [self.tm predicateForKeyPath:@"question_type_id" andValue:@"2"];
    
    NSPredicate *not_question_type_predicate = [NSCompoundPredicate notPredicateWithSubpredicate:question_type_predicate];
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[question_id_predicate, not_question_type_predicate]];
    
    [fetchRequest setPredicate:predicate];
    
    // Edit the sort key as appropriate.
        NSSortDescriptor *order_number = [NSSortDescriptor sortDescriptorWithKey:@"order_number_int" ascending:isAscending];
    //    NSSortDescriptor *question_text = [NSSortDescriptor sortDescriptorWithKey:@"question_text" ascending:isAscending];
    //    NSSortDescriptor *question_id = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:isAscending];
    [fetchRequest setSortDescriptors:@[order_number]];
    
    
    // Edit the section name key path and cache name if appropriate.
    
    NSString *section_name = nil; //(self.isGroupBy) ? @"section_name" : nil;
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:section_name
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.choicesTable beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.choicesTable insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.choicesTable deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.choicesTable;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.self.choicesTable endUpdates];
}

- (void)removeOtherCorrectAnswer {
    NSPredicate *predicate_question = [self.tm predicateForKeyPath:@"question_id" andValue:self.questionID];
    NSPredicate *predicate_answer = [self.tm predicateForKeyPath:@"answer" andValue:@"1"];
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate_question,predicate_answer]];
    
    NSArray *choices = [self.tm getObjectsForEntity:kCoursePlayerQuestionChoiceEntity predicate:predicate];
    
    
    for (NSManagedObject *c_mo in choices) {
        NSString *choice_id = [c_mo valueForKey:@"id"];
        
        NSDictionary *userData = @{@"answer":@"0"};
        [self updateDetailsWithValue:userData ofChoiceID:choice_id];
    }
}

- (void)updateDetailsWithValue:(NSDictionary *)userData ofChoiceID:(NSString *)choiceID{
    
    NSLog(@"user data : %@", userData);
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:choiceID];
    [self.tm executeUpdateForEntity:kCoursePlayerQuestionChoiceEntity details:userData predicate:predicate doneBlock:^(BOOL status) {
//        [self.notif postNotificationName:kNotificationCoursePlayerAnswered object:nil];
    }];
}

- (void)updateAnswerCorrect:(NSDictionary *)userData withPredicate:(NSPredicate *)predicate{
    
    NSLog(@"user data : %@", userData);
//    NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:choiceID];
//    [self.tm executeUpdateForEntity:kCoursePlayerQuestionChoiceEntity details:userData predicate:predicate doneBlock:^(BOOL status) {
//        [self.notif postNotificationName:kNotificationCoursePlayerAnswered object:nil];
//    }];
    
    [self.tm executeUpdateForEntity:kCoursePlayerQuestionChoiceEntity details:userData predicate:predicate doneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.notif postNotificationName:kNotificationCoursePlayerAnswered object:nil];
        });
    }];
}

#pragma mark - CELL DELEGATES

- (void)didFinishloading:(NSIndexPath *)indexPath {
    //    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.table reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (void)didFinishloadingChoice:(NSIndexPath *)indexPath {
    //    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.choicesTable reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (void)didInputTextInBlank:(NSString *)inputText {
    
    NSCharacterSet *leadingTrailing = [NSCharacterSet whitespaceCharacterSet];
    NSString *value = [inputText stringByTrimmingCharactersInSet:leadingTrailing];
    NSString *answerStatus = @"0";
    if (value.length > 0) {
        answerStatus = @"1";
    }
    
    NSDictionary *userData = @{@"answer":answerStatus,
                               @"text":inputText};
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"question_id" andValue:self.questionID];
    [self updateAnswerCorrect:userData withPredicate:predicate];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    
    
    NSCharacterSet *leadingTrailing = [NSCharacterSet whitespaceCharacterSet];
    NSString *value = [textView.text stringByTrimmingCharactersInSet:leadingTrailing];
    NSString *answerStatus = @"0";
    if (value.length > 0) {
        answerStatus = @"1";
    }
    
    NSDictionary *userData = @{@"answer":answerStatus,
                               @"text":textView.text};
    NSPredicate *predicate = [self.tm predicateForKeyPath:@"question_id" andValue:self.questionID];
    [self updateAnswerCorrect:userData withPredicate:predicate];
    
    //    NSString *updatedText = [NSString stringWithFormat:@"%@", textView.text];
    //    if (self.mo != nil) {
    //        //QUESTION DESCRIPTION STORE TO CORE DATA
    //        NSDictionary *userData = @{@"question_text":updatedText};
    //        [self updateDetailsWithValue:userData];
    //    }
}

@end
