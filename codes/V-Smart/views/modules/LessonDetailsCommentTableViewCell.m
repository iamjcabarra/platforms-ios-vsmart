//
//  LessonDetailsCommentTableViewCell.m
//  V-Smart
//
//  Created by Julius Abarra on 9/21/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "LessonDetailsCommentTableViewCell.h"

@implementation LessonDetailsCommentTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
