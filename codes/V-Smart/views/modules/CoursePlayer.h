//
//  CoursePlayer.h
//  V-Smart
//
//  Created by Ryan Migallos on 10/3/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>


@interface CoursePlayer : UIViewController

@property (nonatomic, strong) WKWebView *wkWebView;
- (void)setTopicInfo:(NSDictionary *)object;
@end
