//
//  PlayListDataManager.m
//  V-Smart
//
//  Created by Carmelito Bayarcal on 26/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "PlayListDataManager.h"

//@implementation PlayListDataManager
//
//@end



//#import "TestGuruDataManager.h"

#import "Utils.h"
#import "AccountInfo.h"
#import "VSmartValues.h"
#import "NSDate+Helper.h"

#import <MobileCoreServices/MobileCoreServices.h>

static NSString *storeFilename = @"playlistdata.sqlite";

@interface PlayListDataManager() <NSURLSessionTaskDelegate>

#pragma mark - PROPERTIES

@property (nonatomic, readwrite) NSManagedObjectContext *masterContext;
@property (nonatomic, readwrite) NSManagedObjectContext *mainContext;
@property (nonatomic, readwrite) NSManagedObjectContext *workerContext;

@property (nonatomic, readwrite) NSManagedObjectModel *model;
@property (nonatomic, readwrite) NSPersistentStore *store;
@property (nonatomic, readwrite) NSPersistentStoreCoordinator *coordinator;
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSDateFormatter *formatter;

//@property (nonatomic, readwrite) TestGuruProgressBlock progressBlock;
//@property (nonatomic, readwrite) TestGuruDoneProgressBlock doneProgressBlock;


@property (strong, nonatomic) NSUserDefaults *userDefaults;

@end

@implementation PlayListDataManager

#pragma mark - SINGLETON

+ (instancetype)sharedInstance {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    static PlayListDataManager *singleton = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        singleton = [[self alloc] init];
    });
    return singleton;
}

#pragma mark - SETUP

- (id)init {
    NSLog(@"Running %s", __PRETTY_FUNCTION__);
    
    self = [super init];
    if (!self) { return nil; }
    
    // Managed Object Model
    self.model = [self modelFromFrameWork:@"PlayListModel"];
    // Persistent Store Coordinator
    self.coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    
    BOOL compatible = [self isCompatibleWithCoreDataMetadata:[self storeURL]];
    if (!compatible) {
        self.store = nil;
    }
    
    // CORE DATA 3-LAYER STACK for MASTER, MAIN, WORKER
    [self initializeManagedObjectsWithCoordinator:self.coordinator];
    
    //SESSION MANAGER
    self.session = [NSURLSession sharedSession];
    
    //DATE FORMATTER
    self.formatter = [[NSDateFormatter alloc] init];
    
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    
    return self;
}

- (void)initializeManagedObjectsWithCoordinator:(NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // DATA FLUSHER
    // [WARNING: YOU ARE NOT ALLOWED TO USE THIS CONTEXT]
    // Core Data Stack for the Master Thread [MASTER] -> [PERSISTENTSTORE]
    _masterContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_masterContext performBlockAndWait:^{
        [_masterContext setPersistentStoreCoordinator:persistentStoreCoordinator];
        [_masterContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // UI RELATED CONTEXT
    // Core Data Stack for the Main Thread [MAIN] -> [MASTER]
    _mainContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_mainContext setParentContext:_masterContext];
    [_mainContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    
    // BACKGROUND CONTEXT
    // Core Data Stack for the Worker Thread [WORKER] -> [MAIN]
    _workerContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_workerContext performBlockAndWait:^{
        [_workerContext setParentContext:_mainContext];
        [_workerContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // LOAD STORE FILE
    [self loadStore];
}

- (void)loadStore {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSDictionary *sqliteConfig = @{@"journal_mode" : @"DELETE"};//@{@"journal_mode" : @"WAL"}
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption  : @YES ,
                              NSInferMappingModelAutomaticallyOption        : @YES , //Light Weight Migration
                              NSSQLitePragmasOption                         : sqliteConfig
                              };
    NSError *error = nil;
    _store = [_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:[self storeURL] options:options error:&error];
    if (!_store) {
        //abort();
    } else {
        NSLog(@"Successfully added store: %@", _store);
    }
}

#pragma mark - MODELS (Initialization)

- (NSManagedObjectModel *)modelFromFrameWork:(NSString *)name {
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSURL *modelFileURL = [appBundle URLForResource:name withExtension:@"momd"];
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelFileURL];
    
    return model;
}

#pragma mark - SAVING

- (void)saveContext {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSManagedObjectContext *ctx = _workerContext;
    [ctx performBlockAndWait:^{
        [self saveTreeContext:ctx];
    }];
}

- (void)saveTreeContext:(NSManagedObjectContext *)context {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    if (!context) {
        return;
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"ERROR saving: %@", error);
    }
    
    if (context.parentContext) {
        [self saveTreeContext:context.parentContext];
    }
}

#pragma mark - PATHS

- (NSURL *)storeURL {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSURL *fileURL = [self applicationStoresDirectory];
    
    return [fileURL URLByAppendingPathComponent:storeFilename];
}

- (NSURL *)applicationStoresDirectory {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    NSString *filePath = [self applicationDocumentsDirectory];
    NSURL *storesDirectory = [[NSURL fileURLWithPath:filePath] URLByAppendingPathComponent:@"Stores"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtURL:storesDirectory withIntermediateDirectories:YES attributes:nil error:&error]) {
            NSLog(@"Successfully created Stores directory");
            if (error) {
                NSLog(@"Failed to create Stores directory : %@", error);
            }
        }
    }
    return storesDirectory;
}

- (NSString *)applicationDocumentsDirectory {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    return [NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - FAULTING (Memory Management)

- (void)faultObjectWithID:(NSManagedObjectID *)objectID inContext:(NSManagedObjectContext *)context {
    
    if (!objectID || !context) { return; }
    
    NSManagedObject *object = [context objectWithID:objectID];
    if (object.hasChanges) {
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"ERROR saving: %@", error);
        }
    }
    if (!object.isFault) {
        [context refreshObject:object mergeChanges:NO];
    }
    else {
        NSLog(@"Skipped faulting an object that is already a fault");
    }
    
    // Repeat the process if the context has a parent
    if (context.parentContext) {
        [self faultObjectWithID:objectID inContext:context.parentContext];
    }
}

#pragma mark - MIGRATION

- (BOOL)isCompatibleWithCoreDataMetadata:(NSURL *)storeUrl {
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *filePath = [self stringValue: storeUrl.path];
    
    if ([fm fileExistsAtPath:filePath]) {
        NSLog(@"checking model for compatibility...");
        
        NSError *error = nil;
        NSDictionary *metaData = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
                                                                                            URL:storeUrl
                                                                                          error:&error];
        NSManagedObjectModel *model = self.coordinator.managedObjectModel;
        
        if ([model isConfiguration:nil compatibleWithStoreMetadata:metaData]) {
            NSLog(@"model is compatible...");
            return YES;
            
        } else {
            
            if ([fm removeItemAtPath:filePath error:NULL]) {
                NSLog(@"removed file : %@", filePath);
                NSLog(@"model is incompatible...");
                return NO;
            }//REMOVE THE STORE FILE
            
        }
    }
    
    NSLog(@"file does not exists..");
    return NO;
}

- (BOOL)migrateStore:(NSURL *)sourceStore {
    NSLog(@"SKIPPED MIGRATION: Source is already compatible");
    
    BOOL success = NO;
    NSError *error = nil;
    
    //STEP 1 - Gather the Source, Destination and Mapping Model
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:sourceStore error:&error];
    NSManagedObjectModel *sourceModel = [NSManagedObjectModel mergedModelFromBundles:nil forStoreMetadata:sourceMetadata];
    NSManagedObjectModel *destinModel = _model;
    NSMappingModel *mappingModel = [NSMappingModel mappingModelFromBundles:nil forSourceModel:sourceModel destinationModel:destinModel];
    
    //STEP 2 - Perform Migration, assuming the mapping model isn't null
    if (mappingModel) {
        NSMigrationManager *migrationManager = [[NSMigrationManager alloc] initWithSourceModel:sourceModel destinationModel:destinModel];
        
        //OBSERVER
        [migrationManager addObserver:self forKeyPath:@"migrationProgress" options:NSKeyValueObservingOptionNew context:NULL];
        
        NSURL *destinStore = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Temp.sqlite"];
        success = [migrationManager migrateStoreFromURL:sourceStore
                                                   type:NSSQLiteStoreType
                                                options:nil
                                       withMappingModel:mappingModel
                                       toDestinationURL:destinStore
                                        destinationType:NSSQLiteStoreType
                                     destinationOptions:nil
                                                  error:&error];
        if (success) {
            //STEP 3 - Replace the old store with the new migrated store
            if ([self replaceStore:sourceStore withStore:destinStore]) {
                NSLog(@"SUCCESSFULLY MIGRATED %@ to the Current Model", sourceStore.path);
                [migrationManager removeObserver:self forKeyPath:@"migrationProgress"];
            }
            //STEP 3
        }
        else {
            NSLog(@"FAILED MIGRATION : %@", error);
        }
    }
    else {
        NSLog(@"FAILED MIGRATION: Mapping Model is null");
    }
    
    return YES;// indicates migration has finished, regardless of outcome
}

- (BOOL)replaceStore:(NSURL *)old withStore:(NSURL *)new {
    
    BOOL success = NO;
    NSError *Error = nil;
    if ([[NSFileManager defaultManager] removeItemAtURL:old error:&Error]) {
        
        Error = nil;
        if ([[NSFileManager defaultManager] moveItemAtURL:new toURL:old error:&Error]) {
            success = YES;
        }
        else {
            NSLog(@"FAILED to re-home new store %@", Error);
        }
    }
    else {
        NSLog(@"FAILED to remove old store %@: Error:%@", old, Error);
    }
    return success;
}

#pragma mark - WORKER

- (NSString *)baseURL {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *server = [self stringValue: [defaults stringForKey:@"baseurl_preference"] ];
    //    NSString *server = @"172.16.7.174"; //HARD CODING
    
    return server;
}

- (NSURL *)buildURL:(NSString *)string {
    
    NSString *path = [Utils buildUrl:string];
    NSLog(@"path : %@", path);
    return [NSURL URLWithString:path];
}

- (NSString *)emptyString:(NSString *)value {
    
    if ([value isEqualToString:@"(null)"]) {
        return @"";
    }
    return value;
}

- (NSString *)normalizeStringObject:(NSString *)string {
    
    NSCharacterSet *leadingTrailing = [NSCharacterSet whitespaceCharacterSet];
    NSString *value = [string stringByTrimmingCharactersInSet:leadingTrailing];
    
    return value;
}

- (NSString *)stringValue:(id)object {
    
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] ||  [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

- (BOOL)isArrayObject:(id)object {
    return [object isKindOfClass:[NSArray class]];
}

- (BOOL)isDictionaryObject:(id)object {
    return [object isKindOfClass:[NSDictionary class]];
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption | NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath object:(id)object {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:object];
    
    // predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption | NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (NSManagedObject *)getEntity:(NSString *)entity attribute:(NSString *)attribute
                     parameter:(NSString *)parameter context:(NSManagedObjectContext *)context {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (parameter) {
        
        // create predicate
        NSPredicate *predicate = [self predicateForKeyPath:attribute andValue:parameter];
        
        // set predicate
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
}

- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSUInteger)fetchCountForEntity:(NSString *)entity {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
    [request setIncludesSubentities:NO]; //Omit subentities. Default is YES (i.e. include subentities)
    
    NSError *err;
    
    NSManagedObjectContext *contex = _workerContext;
    NSUInteger count = [contex countForFetchRequest:request error:&err];
    if(count == NSNotFound) {
        return 0;
    }
    
    return count;
}

- (NSArray *)fetchObjectsForEntity:(NSString *)entity {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    NSError *err;
    NSManagedObjectContext *contex = _workerContext;
    NSArray *items = [contex executeFetchRequest:request error:&err];
    if(items.count > 0) {
        return items;
    }
    
    return 0;
}

- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return items;
    }
    
    return nil;
}

- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSManagedObjectContext *context = _workerContext;
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSManagedObject *)insertNewRecordForEntity:(NSString *)entity {
    
    NSManagedObjectContext *ctx = _workerContext;
    return [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:ctx];
}

- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    //    float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
    //    if (ver >= 9.0) {
    //        // Only executes on version 8 or above.
    //        NSLog(@"%s OS %f", __PRETTY_FUNCTION__, ver);
    //
    //        if (_store) {
    //
    //            NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:fetchRequest];
    //            NSError *deleteError = nil;
    //            NSArray *list = [_coordinator executeRequest:delete withContext:_workerContext error:&deleteError];
    //
    //            if (list != nil) {
    //                return YES;
    //            }
    //            return NO;
    //        }
    //    }
    //
    //    if (ver < 9.0) {
    // Only executes on version 8 or above.
    //        NSLog(@"%s OS %f", __PRETTY_FUNCTION__, ver);
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *lmo in items) {
            [context deleteObject:lmo];
        }
        [self saveTreeContext:context];
        
        return YES;
    }
    //    }
    
    return NO;
}

- (BOOL)clearContentsForEntity:(NSString *)entity {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *lmo in items) {
            [context deleteObject:lmo];
        }
        [self saveTreeContext:context];
        
        return YES;
    }
    
    return NO;
}

- (BOOL)clearDataForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)ctx {
    
    //CLEAR CONTENTS
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate != nil) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *mo in items) {
            [ctx deleteObject:mo];
        }
        [self saveTreeContext:ctx];
    }
    
    return YES;
}

- (NSTimeInterval)parseTimeFromString:(NSString *)string {
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    
    NSInteger h, m, s;
    [scanner scanInteger:&h];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&m];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&s];
    
    return h * 3600 + m * 60 + s;
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (NSDate *)parseDateFromString:(NSString *)dateString {
    
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@".000Z" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" togo" withString:@""];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [self.formatter setTimeZone:gmt];
    NSString *template = @"yyyy-MM-dd HH:mm:ss";
    [self.formatter setDateFormat:template];
    NSDate *dateObject = [self.formatter dateFromString:dateString];
    return dateObject;
}

- (id)parseResponseData:(NSData *)data {
    
    if (data) {
        NSError *jsonError = nil;
        
        id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
        
        if (jsonError) {
            NSLog(@"JSON Error : %@", jsonError.localizedDescription);
        }
        
        if (!jsonError) {
            return object;
        }
    }
    
    return nil;
}

- (NSString *)jsonStringFromObject:(id)object {
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:object options:0 error:nil];
    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return string;
}

- (NSMutableURLRequest *)buildRequestWithMethod:(NSString *)method NSURL:(NSURL *)url body:(id)body {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:method];
    
    if (body) {
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:body options:0 error:&error];
        
        NSString *j_string = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        NSLog(@"j_string : <start>---- %@ ---<end>", j_string);
        [request setHTTPBody:postData];
    }
    
    return request;
}

- (BOOL)ownedByUser:(NSString *)userid {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return [user_id isEqualToString:userid];
}

- (NSString *)loginUser {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return user_id;
}


#pragma - mark User Standard Defaults Helper Functions

- (void)saveObject:(id)object forKey:(NSString *)key {
    NSLog(@"%s [%@] [%@]", __PRETTY_FUNCTION__, object, key);
    [self.userDefaults setObject:object forKey:key];
    [self.userDefaults synchronize];
}

- (id)fetchObjectForKey:(NSString *)key {
    id object = [self.userDefaults objectForKey:key];
    return object;
}



#pragma - mark PlayList implementations

- (void)requestFileSizeLimit:(PlayListErrorBlock)errorBlock {
    NSString *playlistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointPlayListMaxUpload]];
    NSLog(@"play list path : %@", playlistPath);
    NSURL *playlistURL = [NSURL URLWithString:playlistPath];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:playlistURL body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          errorBlock([error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          NSDictionary *meta = dictionary[@"_meta"];
                                          NSString *metaStatus = [self stringValue:meta[@"status"]];
                                          
                                          NSArray *records = dictionary[@"records"];
                                          
                                          if (records > 0 || [metaStatus.lowercaseString isEqualToString:@"success"]) {
                                              
                                              NSString *size = [self stringValue:records.lastObject];
                                              
                                              [self saveObject:size forKey:kPL_FILESIZE_LIMIT_BYTES];
                                              
                                              if (errorBlock) {
                                                  errorBlock(nil);
                                              }
                                          }
                                      }
                                      
                                  }];
    [task resume];
}

- (void)requestPlayListForUser:(NSString *)userid doneBlock:(PlayListDoneBlock)doneBlock {
    
    NSString *playlistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointPlayListV2, userid]];
    NSLog(@"play list path : %@", playlistPath);
    NSURL *playlistURL = [NSURL URLWithString:playlistPath];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:playlistURL body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          NSDictionary *meta = dictionary[@"_meta"];
                                          NSString *metaCount = [self stringValue:meta[@"count"]];
                                          NSString *metaStatus = meta[@"status"];
                                          
                                          NSManagedObjectContext *ctx = self.workerContext;
                                          
                                          if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
                                              [ctx performBlock:^{
                                                  [self clearDataForEntity:kPlayListEntityV2 withPredicate:nil context:ctx];
                                              }];
                                              
                                              if (doneBlock) {
                                                  doneBlock(NO);
                                              }
                                              
                                              return;
                                          }
                                          
                                          if (dictionary) {
                                              
                                              NSArray *records = dictionary[@"records"];
                                              
                                              if (records > 0) {
                                                  
                                                  NSManagedObjectContext *ctx = _workerContext;
                                                  NSString *username = [NSString stringWithFormat:@"%@", [VSmart sharedInstance].account.user.username];
                                                  
                                                  if ([username isEqualToString:@"(null)"] || [username isEqualToString:@"<null>"] || [username isEqualToString:@"null"]) {
                                                      username = [NSString stringWithFormat:@"%@", [VSmart sharedInstance].account.user.email];
                                                  }
                                                  
                                                  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"owner_username == %@ AND is_downloaded == 0", username];
                                                  
                                                  [ctx performBlockAndWait:^{
                                                      
                                                      //CLEAR CONTENTS (IMPORTANT)
                                                      [self clearDataForEntity:kPlayListEntityV2 withPredicate:predicate context:ctx];
                                                      
                                                      //REFRESH DATA
                                                      for (NSDictionary *d in records) {
                                                          
                                                          NSLog(@"play list item : %@", d);
                                                          
                                                          /*
                                                           "date_created" = "2015-03-11 05:22:54";
                                                           description = "my upload 1";
                                                           "file_name" = "ios-android-windows-phone.png";
                                                           "file_size" = 25489;
                                                           "first_name" = Alexis;
                                                           "last_name" = Johnson;
                                                           "module_id" = 1;
                                                           title = "my upload title 1";
                                                           "upload_id" = 53;
                                                           "user_id" = 3;
                                                           uuid = 20132ec92d6e415a92b7d1610f8632ec;
                                                           */
                                                          
                                                          // BUG FIX 595
                                                          NSString *shared = @"0";
                                                          NSMutableArray *section_names = [NSMutableArray array];
                                                          NSMutableArray *sharedToArray = [NSMutableArray array];
                                                          
                                                          if (d[@"cs_ids"]) {
                                                              NSArray *cs_ids = d[@"cs_ids"];
                                                              sharedToArray = [NSMutableArray arrayWithArray:cs_ids];
                                                              if (cs_ids.count > 0) {
                                                                  shared = @"1";
                                                                  
                                                                  for (NSDictionary *section in cs_ids) {
                                                                      NSString *sectionName = [section valueForKey:@"name"];
                                                                      //                                                                      NSString *sectionSubject = [section valueForKey:@"name"];
                                                                      //                                                                      NSString *section_name_subject = [NSString stringWithFormat:@"%@- %@", sectionName, sectionSubject];
                                                                      NSString *section_name_subject = [self stringValue: sectionName];
                                                                      
                                                                      [section_names addObject:section_name_subject];
                                                                  }
                                                                  
                                                              } else {
                                                                  shared = @"0";
                                                              }
                                                              NSLog(@"Exists");
                                                          }
                                                          else {
                                                              NSLog(@"Does not exist");
                                                          }
                                                          
                                                          NSString *section_names_string = [section_names componentsJoinedByString: @", "];
                                                          
                                                          NSData *sharedArrayData = [NSKeyedArchiver archivedDataWithRootObject:sharedToArray];
                                                          
                                                          NSLog(@"SECTION NAMES STRING -> %@ <- ", section_names_string);
                                                          
                                                          NSString *dateCreated = [self stringValue: d[@"date_created"] ];
                                                          NSDate *date_created = [self parseDateFromString:dateCreated];
                                                          
                                                          //                                                          NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                                          //                                                          NSString *template = @"yyyy-MM-dd HH:mm:ss";
                                                          //                                                          [formatter setDateFormat:template];
                                                          //                                                          return [formatter dateFromString:string];
                                                          NSString *sharedToCount = [NSString stringWithFormat:@"%lu", (unsigned long)sharedToArray.count];
                                                          NSString *description = [self stringValue: d[@"description"] ]; //SEARCH
                                                          NSString *file_name = [self stringValue: d[@"file_name"] ]; //SEARCH
                                                          NSString *mime_type = @"image/jpeg";
                                                          if (![file_name isEqualToString:@"(null)"]) {
                                                              mime_type = [self stringValue: [self mimeTypeForPath:file_name]];
                                                          }
                                                          
                                                          NSString *file_extension = [self stringValue: [file_name pathExtension] ];
                                                          file_extension = [self stringValue: [file_extension lowercaseString] ];
                                                          
                                                          
                                                          NSString *file_type = [self categoryForFileExtension:file_extension];
                                                          
                                                          NSString *file_size = [self stringValue: d[@"file_size"] ];
                                                          if ([file_size isEqualToString:@"(null)"]) {
                                                              file_size = @"0";
                                                          }
                                                          
                                                          NSString *first_name = [self stringValue: d[@"first_name"] ]; //SEARCH
                                                          NSString *last_name = [self stringValue: d[@"last_name"] ]; //SEARCH
                                                          NSString *module_id = [self stringValue: d[@"module_id"] ];
                                                          NSString *title = [self stringValue: d[@"title"] ]; //SEARCH
                                                          NSString *upload_id = [self stringValue: d[@"upload_id"] ];
                                                          NSString *user_id = [self stringValue: d[@"user_id"] ];
                                                          NSString *tags = [self stringValue:d[@"tags"]];
                                                          
                                                          //                                                          NSString *shared = @"0";
                                                          //                                                          if ([userid isEqualToString:user_id]) {
                                                          //                                                              shared = @"1";
                                                          //                                                          }
                                                          
                                                          NSString *uuid = [self stringValue: d[@"uuid"] ];
                                                          NSString *search_string = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@",
                                                                                     description, file_name, first_name, last_name, title, file_extension, file_type];
                                                          
                                                          NSManagedObject *mo = [self getEntity:kPlayListEntityV2 attribute:@"uuid" parameter:uuid context:ctx];
                                                          if (mo == nil) {
                                                              mo = [NSEntityDescription insertNewObjectForEntityForName:kPlayListEntityV2 inManagedObjectContext:ctx];
                                                          }
                                                          
                                                          NSString *progress = [self stringValue: [mo valueForKey:@"progress"] ];
                                                          if ([progress isEqualToString:@"(null)"]) {
                                                              progress = @"";
                                                          }
                                                          
                                                          [mo setValue:date_created forKey:@"date_created"];
                                                          [mo setValue:description forKey:@"file_desc"];
                                                          [mo setValue:file_name forKey:@"file_name"];
                                                          [mo setValue:file_size forKey:@"file_size"];
                                                          [mo setValue:first_name forKey:@"first_name"];
                                                          [mo setValue:file_extension forKey:@"file_extension"];
                                                          [mo setValue:file_type forKey:@"file_type"];
                                                          [mo setValue:mime_type forKey:@"mime_type"];
                                                          [mo setValue:last_name forKey:@"last_name"];
                                                          [mo setValue:module_id forKey:@"module_id"];
                                                          [mo setValue:title forKey:@"title"];
                                                          [mo setValue:upload_id forKey:@"upload_id"];
                                                          [mo setValue:user_id forKey:@"user_id"];
                                                          [mo setValue:uuid forKey:@"uuid"];
                                                          [mo setValue:search_string forKey:@"search_string"];
                                                          [mo setValue:shared forKey:@"shared"];
                                                          [mo setValue:progress forKey:@"progress"];
                                                          [mo setValue:section_names_string forKey:@"section_names"];
                                                          [mo setValue:sharedArrayData forKey:@"sharedArrayData"];
                                                          [mo setValue:tags forKey:@"tags"];
                                                          [mo setValue:sharedToCount forKey:@"sharedToCount"];
                                                          
                                                          if (![[mo valueForKey:@"is_downloaded"] isEqualToString:@"1"]) {
                                                              [mo setValue:@"0" forKey:@"is_downloaded"];
                                                          }
                                                          
                                                          // for checking purposes
                                                          [mo setValue:username forKey:@"owner_username"];
                                                          
                                                      }
                                                      
                                                      [self saveTreeContext:ctx];
                                                      
                                                  }];
                                                  
                                                  
                                                  if (doneBlock) {
                                                      doneBlock(YES);
                                                  }
                                              }
                                              
                                          }
                                          
                                      }
                                      
                                  }];
    [task resume];
}

- (void)requestDeletePlayListItemWithUploadID:(NSString *)upload_id doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointPlayListDeleteV2, upload_id]];
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:nil];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              
                                              NSManagedObjectContext *ctx = _workerContext;
                                              NSLog(@"DELETION RESPONSE : %@", dictionary);
                                              
                                              NSPredicate *predicate = [NSPredicate predicateWithFormat:@"upload_id == %@", upload_id];
                                              
                                              [self clearDataForEntity:kPlayListEntityV2 withPredicate:predicate context:ctx];
                                              //
                                          }
                                          
                                          if (doneBlock) {
                                              doneBlock(YES);
                                          }
                                      }//end
                                  }];
    
    [task resume];
    
}



- (void)updatePlayListFileForUUID:(NSString *)uuid rawFile:(NSData *)rawFile {
    
    NSManagedObjectContext *ctx = _workerContext;
    [ctx performBlock:^{
        
        NSManagedObject *mo = [self getEntity:kPlayListEntityV2 attribute:@"uuid" parameter:uuid context:ctx];
        
        NSString *file_desc = [self stringValue: [mo valueForKey:@"file_desc"] ];
        NSString *file_extension = [self stringValue: [mo valueForKey:@"file_extension"] ];
        NSString *file_name = [self stringValue: [mo valueForKey:@"file_name"] ];
        NSString *file_size = [self stringValue: [mo valueForKey:@"file_size"] ];
        NSString *file_type = [self stringValue: [mo valueForKey:@"file_type"] ];
        NSString *first_name = [self stringValue: [mo valueForKey:@"first_name"] ];
        NSString *last_name = [self stringValue: [mo valueForKey:@"last_name"] ];
        NSString *title = [self stringValue: [mo valueForKey:@"title"] ];
        NSString *shared = [self stringValue: [mo valueForKey:@"shared"] ];
        NSString *mime_type = [self stringValue: [mo valueForKey:@"mime_type"] ];
        NSString *upload_id = [self stringValue: [mo valueForKey:@"upload_id"] ];
        [mo setValue:uuid forKey:@"uuid"];
        
        [mo setValue:@"1" forKey:@"is_downloaded"];
        
        
        NSManagedObject *filemo = [self getEntity:kPlayListFileEntityV2 attribute:@"uuid" parameter:uuid context:ctx];
        if (filemo == nil) {
            filemo = [NSEntityDescription insertNewObjectForEntityForName:kPlayListFileEntityV2 inManagedObjectContext:ctx];
        }
        
        [filemo setValue:uuid forKey:@"uuid"];
        [filemo setValue:file_desc forKey:@"file_desc"];
        [filemo setValue:file_extension forKey:@"file_extension"];
        [filemo setValue:file_name forKey:@"file_name"];
        [filemo setValue:file_size forKey:@"file_size"];
        [filemo setValue:file_type forKey:@"file_type"];
        [filemo setValue:mime_type forKey:@"mime_type"];
        [filemo setValue:first_name forKey:@"first_name"];
        [filemo setValue:last_name forKey:@"last_name"];
        [filemo setValue:title forKey:@"title"];
        [filemo setValue:shared forKey:@"shared"];
        
        NSLog(@"FILE TYPE !!!![%@]", file_type);
        NSLog(@"MIME TYPE !!!![%@]", mime_type);
        NSLog(@"FILE EXTENSION!!!! [%@]", file_extension);
        NSLog(@"FILE NAME!!!! [%@]", file_name);
        
        // SAVE TO APPLICATION SUPPORT
        if ([self isMedia:mime_type]) {
            AccountInfo *account = [Utils getArchive:kGlobalAccount];
            
            NSString *playlistPath = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *path = [NSString stringWithFormat:@"%@/PLAYLIST/%@",playlistPath, account.user.email];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NULL]) {
                NSError *error = nil;
                //Create one
                if (![[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error]) {
                    NSLog(@"%@", error.localizedDescription);
                }
            }
            
            NSLog(@"HERE IS THE PATH [%@]", path);
            
            NSString *filepath = [NSString stringWithFormat:@"%@/%@%@", path, upload_id, file_name];
            
            //saving is done on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                [rawFile writeToFile:filepath atomically:YES];
                NSLog(@"File Saved !");
                NSLog(@"HERE IS THE FILE PATH [%@]",filepath);
                NSData* data = [filepath dataUsingEncoding:NSUTF8StringEncoding];
                [filemo setValue:data forKey:@"raw_file"];
            });
        } else {
            [filemo setValue:rawFile forKey:@"raw_file"];
        }
        
        [self saveTreeContext:ctx];
    }];
}

- (NSString *)categoryForFileExtension:(NSString *)extension {
    
    NSString *defaultType = @"OTHERS";
    
    /*
     FILE CLASSIFICATION:
     ALL 		- All files
     DOCUMENT 	- DOC, DOCX, RTF, TXT, XLS, XLSX, PPT, PPTX, PDF
     MEDIA		- MP3, MP4
     IMAGE 		- JPG, PNG, GIFF, TIFF
     OTHERS		- XML, HTML, undeclared file formats
     */
    
    NSArray *doctypes = @[@"doc",@"docx",@"rtf",@"txt",@"xls",@"xlsx",@"ppt",@"pdf",@"epub",@"csv"];
    NSArray *mediatypes = @[@"mp3",@"mp4"];
    NSArray *imagetypes = @[@"jpg",@"jpeg",@"png",@"giff",@"tiff"];
    //    NSArray *othertypes = @[@"xml",@"html"];
    
    NSString *file_extension = [self stringValue: [extension lowercaseString] ];
    
    for (NSString *ext in doctypes) {
        if ([ext isEqualToString:file_extension]) {
            return @"DOCUMENT";
        }
    }
    
    for (NSString *ext in mediatypes) {
        if ([ext isEqualToString:file_extension]) {
            return @"MEDIA";
        }
    }
    
    for (NSString *ext in imagetypes) {
        if ([ext isEqualToString:file_extension]) {
            return @"IMAGE";
        }
    }
    
    return defaultType;
}

- (NSString *)generateBoundaryString
{
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}

- (void)requestUpdatePlayListItemForUploadID:(NSString *)uploadid meta:(NSDictionary *)meta file:(NSDictionary *)object errorBlock:(PlayListErrorBlock)errorBlock {
    
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointPlayListUploadV2, uploadid]];
    NSLog(@"path : %@", query);
    NSLog(@"meta : %@", meta);
    //    NSLog(@"file : %@", object);
    
    // configure the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // boundary
    NSString *boundary = [self generateBoundaryString];
    
    // set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create body
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:meta file:object];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    [config setAllowsCellularAccess:YES];//USE CELLULAR DATA
    NSURLSession *uploadsession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
//    NSURLSessionUploadTask *uploadTask = [uploadsession uploadTaskWithRequest:request fromData:httpBody];
//    [uploadTask resume];//START upload
    
    [[uploadsession uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"COMPLETION RESPONSE %@, \n DATA %@, \n ERROR %@", response, [self parseResponseData:data], [error localizedDescription]);
        NSDictionary *dictionary = [self parseResponseData:data];
        
        NSDictionary *meta = dictionary[@"_meta"];
        NSString *status = [self stringValue:meta[@"status"]];
        
        if (error == nil) {
            if ([status isEqualToString:@"1"] || [status isEqualToString:@"true"] || [status isEqualToString:@"SUCCESS"]) {
                errorBlock(nil);
            } else {
                errorBlock(status);
            }
        } else {
            NSLog(@"ERROR %@", [error localizedDescription]);
            errorBlock([error localizedDescription]);
        }
    }] resume];
    
    
//    self.doneProgressBlock = ^(NSString *status) {
//        NSLog(@"TASKS COMPLETE...");
//        if ([status isEqualToString:@"TRUE"]) {
//            if (doneBlock) {
//                doneBlock(YES);
//            }
//        }
//    };
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
   didSendBodyData:(int64_t)bytesSent
    totalBytesSent:(int64_t)totalBytesSent
totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend {
    
    CGFloat progress = (double)totalBytesSent / (double)totalBytesExpectedToSend;
    NSLog(@"upload progress : %f", progress);
    
    if (self.progressBlock) {
        self.progressBlock(progress);
    }
}

//- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
//
//    if (self.doneProgressBlock) {
//        self.doneProgressBlock(@"TRUE");
//    }
//}

- (void)updateProgressForUUID:(NSString *)uuid progress:(NSString *)progress {
    
    NSManagedObjectContext *ctx = _workerContext;
    [ctx performBlock:^{
        NSManagedObject *mo = [self getEntity:kPlayListEntityV2 attribute:@"uuid" parameter:uuid context:ctx];
        [mo setValue:progress forKey:@"progress"];
        [self saveTreeContext:ctx];
    }];
}

-(BOOL)isMedia:(NSString *)mimeType {
    
    BOOL isMedia;
    
    if ([mimeType rangeOfString:@"video"].location == NSNotFound) {
        isMedia = NO;
    } else {
        return YES;
    }
    
    if ([mimeType rangeOfString:@"audio"].location == NSNotFound) {
        isMedia = NO;
    } else {
        return  YES;
    }
    
    return isMedia;
}

- (NSString *)mimeTypeForPath:(NSString *)path
{
    // get a mime type for an extension using MobileCoreServices.framework
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    //    CFStringRef extension = (__bridge CFStringRef)path;
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mime_type_object = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    NSString *mimetype = [self stringValue: mime_type_object];
    if ([mimetype isEqualToString:@"(null)"]) {
        mimetype = @"text/html";
    }
    
    assert(mimetype != NULL);
    
    return mimetype;
}



- (void)requestListOfTeachers:(PlayListListBlock)listBlock {
    
    NSString *userid = [self loginUser];
    
    NSString *playlistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointPlayListTeacherListV2, userid]];
    NSLog(@"play list path : %@", playlistPath);
    NSURL *playlistURL = [NSURL URLWithString:playlistPath];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:playlistURL body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
          {
              if (error) {
                  NSLog(@"error %@", [error localizedDescription]);
                  if (listBlock) {
                      listBlock(nil);
                  }
              }
              
              if (!error) {
                  
                  NSDictionary *dictionary = [self parseResponseData:responsedata];
                  
                  if (dictionary) {
                      
                      BOOL status = [self isArrayObject:dictionary[@"records"]];
                      
                      if (status == NO) {
                          NSLog(@"could not request teacher list");
                          if (listBlock) {
                              listBlock([NSArray array]);
                          }
                          return;
                      }
                      
                      if (status == YES) {
                          
                          NSArray *records = dictionary[@"records"];
                          if (records.count > 0) {
                              
                              NSMutableArray *arrayOfUsers = [NSMutableArray array];
                              for (NSDictionary *record in records) {
                                  
                                  NSString *context_id = [self stringValue:record[@"context_id"] ];
                                  NSString *name = [self stringValue:record[@"name"] ];
                                  NSString *group_level_id = [self stringValue:record[@"group_level_id"] ];
                                  NSString *category = [self stringValue:record[@"category"] ];
                                  
                                  NSDictionary *userDict = @{@"context_id":context_id,
                                                             @"name":name,
                                                             @"group_level_id":group_level_id,
                                                             @"category":category};
                                  
                                  [arrayOfUsers addObject:userDict];
                              }
                              
                              if (listBlock) {
                                  listBlock(arrayOfUsers);
                              }
                          }
                      }
                  }
              }
          }];
    [task resume];
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters
                              file:(NSDictionary *)fileObject
{
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    if (fileObject) {
        
        NSString *fieldname = @"file";
        NSString *filename = [self stringValue: fileObject[@"filename"] ];
        NSData *data = [NSData dataWithData: fileObject[@"filedata"] ];
        NSString *mimetype  = [self stringValue: fileObject[@"mimetype"] ];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldname, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}


- (void)requestListOfUserForSearchText:(NSString *)searchText listBlock:(PlayListListBlock)listBlock {
    
    NSString *userid = [self loginUser];
    
    NSString *encodedSearchText = [self urlEncode:searchText];
    
    NSString *playlistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointPlayListSearchUserV2, encodedSearchText, userid]];
    NSLog(@"play list path : %@", playlistPath);
    NSURL *playlistURL = [NSURL URLWithString:playlistPath];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:playlistURL body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          if (listBlock) {
                                              NSMutableArray *arrayOfUsers = [NSMutableArray array];
                                              listBlock(arrayOfUsers);
                                          }
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          
                                          if (dictionary) {
                                              
                                              if ([dictionary[@"records"] isKindOfClass:[NSArray class]]) {
                                                  NSArray *records = dictionary[@"records"];
                                                  if (records != nil) {
                                                      if (records.count > 0) {
                                                          
                                                          NSMutableArray *arrayOfUsers = [NSMutableArray array];
                                                          for (NSDictionary *record in records) {
                                                              
                                                              NSString *context_id = [self stringValue:record[@"context_id"]];
                                                              NSString *name = [self stringValue:record[@"name"]];
                                                              NSString *group_level_id = [self stringValue:record[@"group_level_id"]];
                                                              NSString *category = [self stringValue:record[@"category"]];
                                                              
                                                              NSDictionary *userDict = @{@"context_id":context_id,
                                                                                         @"name":name,
                                                                                         @"group_level_id": group_level_id,
                                                                                         @"category":category};
                                                              
                                                              [arrayOfUsers addObject:userDict];
                                                          }
                                                          
                                                          if (listBlock) {
                                                              listBlock(arrayOfUsers);
                                                          }
                                                      }
                                                  }
                                              }
                                          }
                                      }
                                      
                                  }];
    
    [task resume];
}

- (NSString *)urlEncode:(id<NSObject>)value
{
    //make sure param is a string
    if ([value isKindOfClass:[NSNumber class]]) {
        value = [(NSNumber*)value stringValue];
    }
    
    NSAssert([value isKindOfClass:[NSString class]], @"request parameters can be only of NSString or NSNumber classes. '%@' is of class %@.", value, [value class]);
    
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                 NULL,
                                                                                 (__bridge CFStringRef) value,
                                                                                 NULL,
                                                                                 (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                 kCFStringEncodingUTF8));
}



- (void)postUpdateOfUploadID:(NSString *)upload_id withPostBody:(NSDictionary *)postBody doneBlock:(PlayListDoneBlock)doneBlock {
    
    //    NSString *upload_id = [NSString stringWithFormat:@"%@", meta[@"upload_id"] ];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointPlayListUpdateMetaV2, upload_id]];
    
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              NSLog(@"updated data : %@", dictionary);
                                          }
                                          
                                          if (doneBlock) {
                                              doneBlock(YES);
                                          }
                                          
                                      }//end
                                      
                                  }];
    
    [task resume];
}


@end
