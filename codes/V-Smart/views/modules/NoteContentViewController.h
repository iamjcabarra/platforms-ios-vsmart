//
//  NoteContentViewController.h
//  V-Smart
//
//  Created by VhaL on 2/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NNote.h"
#import "NNoteTag.h"
#import "NNoteBook.h"

@interface NoteContentViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) id delegate;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) Note *noteObject;
@property (nonatomic, assign) bool isDuplicateOperation;
@property (nonatomic, strong) NoteBook *currentNotebook;
@property (nonatomic, strong) NSString *noteStringFromIdeaPad;
-(IBAction)buttonColorTapped:(id)sender;
@end
