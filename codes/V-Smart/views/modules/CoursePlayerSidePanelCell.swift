//
//  CoursePlayerSidePanelCell.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 29/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class CoursePlayerSidePanelCell: UICollectionViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet var textLabel: UILabel!
    
}
