//
//  QuizController.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/6/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuizControllerDelegate <NSObject>

@optional
- (void)didFinishSubmittingQuizAnswersForData:(NSDictionary *)data;

@end

@interface QuizController : UIViewController

@property (nonatomic, weak) id <QuizControllerDelegate> delegate;

@property (nonatomic, strong) NSString *quizName;
@property (nonatomic, strong) NSString *userid;
@property (nonatomic, strong) NSString *quizid;
@property (nonatomic, strong) NSString *courseid;

@property (nonatomic, assign) BOOL started;

@end
