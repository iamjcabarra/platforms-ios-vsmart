//
//  PlayListView.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/10/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "PlayListView.h"
#import "PlayListViewCell.h"
#import "PlayListOptionController.h"
#import "PlayListEditView.h"
#import "PlayListNewItem.h"
#import "PlayListDocumentViewer.h"

#import "PlayListDataManager.h"
#import "VSmart.h"
#import "VSmartValues.h"
#import "Utils.h"
#import "AppDelegate.h"

#import "SharedToViewController.h"

#import "V_Smart-Swift.h"

#define ResultsTableView self.searchResultsTableViewController.tableView

@interface PlayListView ()
<NSFetchedResultsControllerDelegate, UISearchControllerDelegate, UITableViewDataSource,
UITableViewDelegate, UIPopoverControllerDelegate, NSURLSessionDownloadDelegate, UISearchBarDelegate, PlayListOptionDelegate, PlayListNewItemDelegate, PlayListEditDelegate>

@property (nonatomic, strong) IBOutlet UISegmentedControl *segementedControl;
@property (nonatomic, strong) IBOutlet UIButton *buttonSortAlpha;
@property (nonatomic, strong) IBOutlet UIButton *buttonSortNumeric;
@property (nonatomic, strong) IBOutlet UIButton *buttonUpload;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) PlayListDataManager *plm;

@property (nonatomic, strong) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, strong) IBOutlet UIView *searchViewFrame;
@property (nonatomic, strong) IBOutlet UIButton *sortButton;
@property (nonatomic, strong) IBOutlet UILabel *labelDate;
@property (nonatomic, strong) IBOutlet UILabel *labelTypeName;
@property (nonatomic, strong) IBOutlet UILabel *labelScore;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl *tableRefreshControl;

@property (nonatomic, strong) UIPopoverController *popover;
//@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UITableViewController *searchResultsTableViewController;
//@property (nonatomic, strong) NSArray *results;

@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSString *sort_type;
@property (nonatomic, strong) NSString *sort_by_property;
@property (nonatomic, assign) BOOL ascending;

@property (nonatomic, assign) BOOL is_new_upload;

@property (nonatomic, strong) NSMutableDictionary *downloadTable;

@property (nonatomic, strong) NSManagedObject *mo_selected;

@property (nonatomic, strong) NSArray *sharedArray;

@end

@implementation PlayListView

static NSString *kPlayListCellIdentifier = @"playlist_cell_identifier";
static NSString *kPlayListCacheName = @"PlayListCache";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Playlist";
    [self customizeNavigationController];
    // DEFAULT VALUES ///////////////////
    self.sort_type = @"*";
    self.ascending = NO;
    self.sort_by_property = @"date_created";

    self.plm = [PlayListDataManager sharedInstance];
    self.managedObjectContext = self.plm.mainContext;
    
    [self.segementedControl addTarget:self action:@selector(filterAction:) forControlEvents:UIControlEventValueChanged];
    [self.buttonSortAlpha addTarget:self action:@selector(buttonSortAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonSortNumeric addTarget:self action:@selector(buttonSortAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonUpload addTarget:self action:@selector(uploadAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self setupSearchCapabilities];
    
    self.user_id = [NSString stringWithFormat:@"%d", [VSmart sharedInstance].account.user.id];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"PlayListStoryboard" bundle:nil];
    PlayListOptionController *oc = (PlayListOptionController *)[sb instantiateViewControllerWithIdentifier:@"optionController"];
    oc.delegate = self;
    self.popover = [[UIPopoverController alloc] initWithContentViewController:oc];
    self.popover.delegate = self;
    
    SEL refreshAction = @selector(listAllPlayList);
    
    // Implement refresh control
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    [self listAllPlayList];
}

- (void)listAllPlayList {
    __weak typeof(self) wo = self;
    [self.plm requestPlayListForUser:self.user_id doneBlock:^(BOOL status) {
        NSLog(@"PLAY list done: %d", status);
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.tableRefreshControl endRefreshing];
//            [wo reloadFetchedResultsController];
        });
    }];
}

- (void)didFinishUpdate:(BOOL)status {
    if (status == YES) {
        [self listAllPlayList];
    }
}

- (void)customizeNavigationController {
    UIColor *color = UIColorFromHex(0x3083FB);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    }
    else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

- (void)didFinishSelectingOption:(NSString *)option withData:(NSDictionary *)data {
    
    NSLog(@"option : %@", option);
    NSLog(@"data : %@", data);
    
    [self.popover dismissPopoverAnimated:YES];
    
    if ([option isEqualToString:@"delete"]) {
        [self initiateDelete];
    }
    
    if ([option isEqualToString:@"edit"]) {
        [self performSegueWithIdentifier:@"showEditPage" sender:self];
    }
    
    if ([option isEqualToString:@"download"]) {
        [self initiateDownloadWithData:data];
    }
}

- (void)initiateDelete {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete", nil) message:NSLocalizedString(@"Are you sure you want to delete", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* noAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:noAlertAction];
    
    NSString *uploader_id = [self.mo_selected valueForKey:@"user_id"];
    NSString *upload_id = [self.mo_selected valueForKey:@"upload_id"];
    
    __weak typeof(self) wo = self;
    
    UIAlertAction* yesAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([uploader_id isEqualToString:wo.user_id]) {
            [wo.plm requestDeletePlayListItemWithUploadID:upload_id doneBlock:^(BOOL status) {
                //                [wo.rm requestPlayListForUser:wo.user_id doneBlock:^(BOOL status) {
                //                    NSLog(@"PLAY list done: %d", status);
                //                }];
            }];
        }
    }];
    
    [alertController addAction:yesAlertAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)initiateDownloadWithData:(NSDictionary *)data {
    
    NSString *uuid = [NSString stringWithFormat:@"%@", data[@"uuid"] ];
    NSLog(@"session configuration UUID : %@", uuid);
    
    //initialiaze once
    if (self.downloadTable == nil) {
        self.downloadTable = [NSMutableDictionary dictionary];
    }
    
    if ([self.downloadTable objectForKey:uuid] == nil) {
        NSString *download_path = [Utils buildUrl:[NSString stringWithFormat:kEndPointDownLoadPlayListItemV2, uuid]];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:download_path]];
        NSLog(@"Download path [%@]", download_path);
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:uuid];
        [config setAllowsCellularAccess:YES];//USE CELLULAR DATA
        NSURLSession *backgroundsession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
        NSURLSessionDownloadTask *downloadTask = [backgroundsession downloadTaskWithRequest:request];
        [self.downloadTable setObject:downloadTask forKey:uuid];
        [downloadTask resume];//START DOWNLOAD
    }
}

#pragma mark - NSURLSessionDownloadDelegate

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    
    CGFloat progress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    NSString *p = [NSString stringWithFormat:@"%f", progress];
    NSString *uuid = session.configuration.identifier;
    [self.plm updateProgressForUUID:uuid progress:p];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    
    NSData *data = [[NSFileManager defaultManager] contentsAtPath:[location path]];
    NSString *uuid = session.configuration.identifier;
    NSLog(@"didFinishDownloadingToURL UUID : %@", uuid);
    //    NSString *username_string = [VSmart sharedInstance].account.user.username;
    [self.plm updatePlayListFileForUUID:uuid rawFile:data];
}

- (void)buttonSortAction:(id)sender {
    
    NSLog(@"button sort action...");
    
    UIButton *b = (UIButton *)sender;
    
    BOOL selected = (b.selected) ? NO : YES;
    b.selected = selected;
    self.ascending = selected;
    self.is_new_upload = NO;
    
    if (b == self.buttonSortAlpha) {
        //self.sort_by_property = @"file_name";
        self.sort_by_property = @"title";
    }
    
    if (b == self.buttonSortNumeric) {
        self.sort_by_property = @"date_created";
    }
    
    [self reloadFetchedResultsController];
}

- (void)filterAction:(id)sender {
    
    NSLog(@"segmented controller clicked...");
    
    UISegmentedControl *s = (UISegmentedControl *)sender;
    
    switch (s.selectedSegmentIndex) {
        case 0:
            self.sort_type = @"*";
            break;
            
        case 1:
            self.sort_type = @"DOCUMENT";
            break;
            
        case 2:
            self.sort_type = @"MEDIA";
            break;
            
        case 3:
            self.sort_type = @"IMAGE";
            break;
            
        case 4:
            self.sort_type = @"OTHERS";
            break;
            
        default:
            break;
    }
    
    [self reloadFetchedResultsController];
}

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (void)setupSearchCapabilities {
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.tableView.width, 44)];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = NSLocalizedString(@"Search", nil);
    self.tableView.tableHeaderView = self.searchBar;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    // terminate all pending download connections
    NSArray *allDownloads = [self.downloadTable allValues];
    [allDownloads makeObjectsPerformSelector:@selector(cancel)];
}

- (void)uploadAction:(id)sender {
    [self performSegueWithIdentifier:@"showNewUpload" sender:self];
}

- (void)didFinishUploading {
    self.is_new_upload = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self reloadFetchedResultsController];
    });
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showNewUpload"]) {
        
        BOOL isTeacher = NO;
        NSString *position = [VSmart sharedInstance].account.user.position;
        if ([position isEqualToString:kModeIsTeacher]) {
            isTeacher = YES;
        }
        
        PlayListNewItem *upload = (PlayListNewItem *)segue.destinationViewController;
        upload.module_id = @"1";
        upload.teachMode = isTeacher;
        upload.delegate = self;
        upload.title = @"Upload File";
    }
    
    if ([segue.identifier isEqualToString:@"showDocumentViewer"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSString *uuid = [NSString stringWithFormat:@"%@", [mo valueForKey:@"uuid"] ];
        
        NSManagedObject *document_mo = [self.plm getEntity:kPlayListFileEntityV2
                                                attribute:@"uuid"
                                                parameter:uuid
                                                  context:self.managedObjectContext];
        
        NSData *content_data = [document_mo valueForKey:@"raw_file"];
        NSString *mime_type = [NSString stringWithFormat:@"%@", [document_mo valueForKey:@"mime_type"] ];
        
        PlayListDocumentViewer *dv = (PlayListDocumentViewer *)segue.destinationViewController;
        dv.contentData = content_data;
        dv.mimeType = mime_type;
        dv.title = @"Document";
        
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    if ([segue.identifier isEqualToString:@"showEditPage"]) {
        BOOL isTeacher = NO;
        NSString *position = [VSmart sharedInstance].account.user.position;
        if ([position isEqualToString:kModeIsTeacher]) {
            isTeacher = YES;
        }
        
        NSString *uuid = [NSString stringWithFormat:@"%@", [self.mo_selected valueForKey:@"uuid"] ];
        NSManagedObject *document_mo = [self.plm getEntity:kPlayListEntityV2
                                                attribute:@"uuid"
                                                parameter:uuid
                                                  context:self.managedObjectContext];
        
        PlayListEditView *ev = (PlayListEditView *)[segue destinationViewController];
        ev.delegate = self;
        ev.isTeacherMode = isTeacher;
        ev.document = document_mo;
    }
    
    if ([segue.identifier isEqualToString:@"showSharedSegue"]) {
        
        SharedToViewController *stv = (SharedToViewController *)[segue destinationViewController];
        stv.sharedArray = self.sharedArray;
    }
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    count = [sectionInfo numberOfObjects];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPlayListCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    PlayListViewCell *plvcell = (PlayListViewCell *)cell;
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    NSString *title = [NSString stringWithFormat:@"%@", [mo valueForKey:@"title"] ];
    //NSString *file_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"file_name"] ];
    NSString *description = [NSString stringWithFormat:@"%@", [mo valueForKey:@"file_desc"] ];
    NSString *date_created = [NSDateFormatter localizedStringFromDate:[mo valueForKey:@"date_created"]
                                                            dateStyle:NSDateFormatterMediumStyle
                                                            timeStyle:NSDateFormatterNoStyle];
    
    NSString *fileSizeObject = [NSString stringWithFormat:@"%@", [mo valueForKey:@"file_size"] ];
    NSNumber *fileSizeNumber = [NSNumber numberWithDouble:[fileSizeObject doubleValue]];
    NSString *file_size = [Utils fileSizeToHumanReadable:fileSizeNumber];
    NSString *file_icon = [NSString stringWithFormat:@"icn_%@", [mo valueForKey:@"file_extension"] ];
    NSString *shared = [NSString stringWithFormat:@"%@", [mo valueForKey:@"shared"] ];
    NSString *progress = [NSString stringWithFormat:@"%@", [mo valueForKey:@"progress"] ];
    NSString *section_names = [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_names"] ];
    
    NSString *first_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"] ];
    NSString *last_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"] ];
    
    NSString *uploader_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ];
    
    plvcell.buttonOption.hidden = NO;
    plvcell.sharedButton.hidden = NO;
    plvcell.sharedToIndicator.hidden = YES;
    plvcell.sharedButton.hidden = YES;
    
    if (![uploader_id isEqualToString:self.user_id]) {
        if (![progress isEqualToString:@""]) {
            plvcell.buttonOption.hidden = YES;
        }
    }
    
    NSString *sharedToCount = [NSString stringWithFormat:@"%@", [mo valueForKey:@"sharedToCount"] ];
    NSInteger count = [sharedToCount integerValue];
    
    if (count > 0) {
        NSString *sharedToLocalized = NSLocalizedString(@"Shared to", nil);
        NSString *displayString = (count == 1) ? section_names : sharedToCount;
        NSString *sharedToString = [NSString stringWithFormat:@"%@ %@", sharedToLocalized, displayString];
        plvcell.sharedButton.hidden = NO;
        plvcell.sharedToIndicator.hidden = NO;
        plvcell.sharedToIndicator.text = sharedToString;
        [plvcell.sharedToIndicator highlightString:displayString size:17 color:UIColorFromHex(0xFC3A37)];
    }
    
    UIImage *fileImage = ([UIImage imageNamed:file_icon] != nil) ? [UIImage imageNamed:file_icon] : [UIImage imageNamed:@"icn_blankdoc.png"];
    plvcell.imageViewFile.image = fileImage;
    //plvcell.labelFilename.text = file_name;
    plvcell.labelFilename.text = title;
    plvcell.labelFileDescription.text = description;
    plvcell.labelDate.text = [NSString stringWithFormat:@"by %@ %@ | %@", first_name, last_name, date_created];
    plvcell.labelFileSize.text = file_size;
    [plvcell.buttonOption addTarget:self
                             action:@selector(actionForButtonOption:)
                   forControlEvents:UIControlEventTouchUpInside];
    plvcell.progressIndicator.progress = [progress floatValue];
    
    [plvcell.sharedButton addTarget:self
                             action:@selector(sharedButtonAction:)
                   forControlEvents:UIControlEventTouchUpInside];
}

- (void)sharedButtonAction:(id)sender {
    
    UIButton *b = (UIButton *)sender;
    NSManagedObject *mo = [self managedObjectFromButtonAction:b];
    
    NSData *sharedArrayData = [mo valueForKey:@"sharedArrayData"];
    
    NSArray *sharedArray = [NSKeyedUnarchiver unarchiveObjectWithData:sharedArrayData];
    
    self.sharedArray = sharedArray;
    
    [self performSegueWithIdentifier:@"showSharedSegue" sender:nil];
}

- (NSManagedObject *)managedObjectFromButtonAction:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (void)actionForButtonOption:(id)sender {
    
    UIButton *b = (UIButton *)sender;
    NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
    
    self.mo_selected = mo;
    
    NSArray *keys = mo.entity.propertiesByName.allKeys;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    for (NSString *key in keys) {
        if (![key isEqualToString:@"search_string"]) {
            NSString *val = [NSString stringWithFormat:@"%@", [mo valueForKey:key] ];
            [data setValue:val forKey:key];
        }
    }
    
    NSString *shareFlag = [NSString stringWithFormat:@"%@", [mo valueForKey:@"shared"] ];
    
    PlayListOptionController *oc = (PlayListOptionController *)self.popover.contentViewController;
    
    oc.shared = shareFlag;
    oc.data = data;
    
    
    NSString *uploader_id = [mo valueForKey:@"user_id"];
    
    NSString *progress = [NSString stringWithFormat:@"%@", [mo valueForKey:@"progress"] ];
    
    oc.optionList = @[];
    
    if ([uploader_id isEqualToString:self.user_id]) {
        oc.optionList = @[
                            @{@"image":@"icn_download_pl_button", @"title":@"Download", @"type":@"download"},
                            @{@"image":@"icn_edit_button", @"title":@"Edit", @"type":@"edit"},
                            @{@"image":@"icn_download_pl_button", @"title":@"Delete", @"type":@"delete"} ];
        
        if (![progress isEqualToString:@""]) {
            oc.optionList = @[ @{@"image":@"icn_edit_button", @"title":@"Edit", @"type":@"edit"},
                               @{@"image":@"icn_download_pl_button", @"title":@"Delete", @"type":@"delete"} ];
        }
    }
    
    if (![uploader_id isEqualToString:self.user_id]) {
        if ([progress isEqualToString:@""]) {
            oc.optionList = @[ @{@"image":@"icn_download_pl_button", @"title":@"Download", @"type":@"download"} ];
        }
    }
    
    CGFloat height = 44;
    
    height = height * oc.optionList.count;
    
    oc.preferredContentSize = CGSizeMake(150, height);
    
    if ([oc.optionList  isEqual: @[]]) {
        
    } else {
        [self.popover presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.tableView = tableView;
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    self.mo_selected = mo;
    
    NSString *progress = [NSString stringWithFormat:@"%@", [mo valueForKey:@"progress"] ];
    if (![progress isEqualToString:@""]) {
        
        NSString *mimeType = [mo valueForKey:@"mime_type"];
        
        if ([self isMedia:mimeType]) {
            NSString *uuid = [NSString stringWithFormat:@"%@", [mo valueForKey:@"uuid"] ];
            
            NSManagedObject *document_mo = [self.plm getEntity:kPlayListFileEntityV2
                                                    attribute:@"uuid"
                                                    parameter:uuid
                                                      context:self.managedObjectContext];
            
            NSData *content_data = [document_mo valueForKey:@"raw_file"];
            
            NSString* filePath = [[NSString alloc] initWithData:content_data encoding:NSUTF8StringEncoding];
            
            NSURL *videoURL = [NSURL fileURLWithPath:filePath];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *playerViewController = [AVPlayerViewController new];
            playerViewController.player = player;
            
            [self presentViewController:playerViewController animated:YES completion:nil];
            
        } else {
            [self performSegueWithIdentifier:@"showDocumentViewer" sender:self];
        }
    }
    
    if ([progress isEqualToString:@""] ) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

-(BOOL)isMedia:(NSString *)mimeType {
    
    BOOL isMedia;
    
    if ([mimeType rangeOfString:@"video"].location == NSNotFound) {
        isMedia = NO;
    } else {
        return YES;
    }
    
    if ([mimeType rangeOfString:@"audio"].location == NSNotFound) {
        isMedia = NO;
    } else {
        return  YES;
    }
    
    return isMedia;
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kPlayListEntityV2];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    self.user_id = [NSString stringWithFormat:@"%d", [VSmart sharedInstance].account.user.id];
    NSString *username = [NSString stringWithFormat:@"%@", [VSmart sharedInstance].account.user.username];
    
    if ([username isEqualToString:@"(null)"] || [username isEqualToString:@"<null>"] || [username isEqualToString:@"null"]) {
        username = [NSString stringWithFormat:@"%@", [VSmart sharedInstance].account.user.email];
    }
    
    NSCompoundPredicate *predicate;
    
    NSPredicate *predicate_type_1 = [self predicateForKeyPath:@"file_type" value:self.sort_type];
    NSPredicate *predicate_type_2 = [self predicateForKeyPath:@"owner_username" value:username];
    NSPredicate *predicate_type_3 = [self predicateForKeyPathContains:@"search_string" value:self.searchBar.text];
    
    if (self.searchBar.text.length > 0) {
        predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate_type_1,predicate_type_2, predicate_type_3]];
    } else {
        predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate_type_1,predicate_type_2]];
    }
    
    [fetchRequest setPredicate:predicate];
    
    if ([self.sort_by_property isEqualToString: @"date_created"] && (self.is_new_upload == NO)) {
        NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_created" ascending:self.ascending];
        [fetchRequest setSortDescriptors:@[date_modified]];
    } else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:self.sort_by_property
                                                                       ascending: (!self.ascending)
                                                                        selector:@selector(localizedStandardCompare:)];
        [fetchRequest setSortDescriptors:@[sortDescriptor]];
    }
    
    if (self.is_new_upload) {
        NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_created" ascending:NO];
        [fetchRequest setSortDescriptors:@[date_modified]];
    }
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Search Results Updating

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self reloadFetchedResultsController];
}

@end
