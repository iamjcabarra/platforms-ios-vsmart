//
//  LPMLessonCreatorViewSwapper.swift
//  V-Smart
//
//  Created by Julius Abarra on 24/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMLessonCreatorViewSwapper: UIViewController {
    
    fileprivate let kLessonOverviewSVCSegueIdentifier = "SHOW_LESSON_OVERVIEW"
    fileprivate let kLessonDetailsSVCSegueIdentifier = "SHOW_LESSON_DETAILS"
    
    fileprivate var overview: LPMLessonOverviewTableViewController!
    fileprivate var detailsView: LPMLessonDetailsViewController!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - View Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var svcExists = false
        
        if segue.identifier == self.kLessonOverviewSVCSegueIdentifier {
            self.overview = segue.destination as? LPMLessonOverviewTableViewController
            svcExists = true
        }
        
        if segue.identifier == self.kLessonDetailsSVCSegueIdentifier {
            self.detailsView = segue.destination as? LPMLessonDetailsViewController
            svcExists = true
        }
        
        if svcExists {
            if self.childViewControllers.count > 0 {
                self.swapFromViewController(self.childViewControllers[0], toViewController: segue.destination)
            }
            else {
                self.addChildViewController(segue.destination)
                segue.destination.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                self.view.addSubview(segue.destination.view)
                segue.destination.didMove(toParentViewController: self)
            }
        }
    }
    
    // MARK:- View Swappers
    
    func swapToViewControllerWithSegueIdentifier(_ segueIdentifier: String) {
        self.performSegue(withIdentifier: segueIdentifier, sender: nil)
    }
    
    func swapFromViewController(_ fromViewController: UIViewController, toViewController: UIViewController) {
        toViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        fromViewController.willMove(toParentViewController: nil)
        self.addChildViewController(toViewController)
        
        self.transition(
            from: fromViewController,
            to: toViewController,
            duration: 0.2,
            options: UIViewAnimationOptions.transitionCrossDissolve,
            animations: nil,
            completion: { finished in
                fromViewController.removeFromParentViewController()
                toViewController.didMove(toParentViewController: self)
        })
    }

}
