//
//  PlayListEditView.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/20/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "PlayListEditView.h"
#import "AppDelegate.h"
#import "SelectedCollectionViewCell.h"
#import "CheckBoxCell.h"
#import "PlayListDataManager.h"

@interface PlayListEditView () <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) IBOutlet UITextField *titleLabel;
@property (strong, nonatomic) IBOutlet UITextField *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) PlayListDataManager *plm;

@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *upload_id;
@property (strong, nonatomic) NSString *module_id;

@property (strong, nonatomic) NSArray *searchResult;
@property (strong, nonatomic) NSMutableArray *shareList;

@property (weak, nonatomic) IBOutlet UITextField *tagField;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarHeight;
@property (weak, nonatomic) IBOutlet UILabel *shareToLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagLabel;

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation PlayListEditView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.plm = [PlayListDataManager sharedInstance];
    
    self.user_id = [NSString stringWithFormat:@"%d", [VSmart sharedInstance].account.user.id];
    
    self.upload_id = [NSString stringWithFormat:@"%@", [self.document valueForKey:@"upload_id"] ];
    NSLog(@"%s upload_id: %@", __PRETTY_FUNCTION__, self.upload_id);
    self.module_id = [NSString stringWithFormat:@"%@", [self.document valueForKey:@"module_id"] ];
    NSLog(@"%s module_id: %@", __PRETTY_FUNCTION__, self.module_id);
    
    NSString *updateTitle = NSLocalizedString(@"Update", nil);
    [self.saveButton setTitle:updateTitle forState:UIControlStateNormal];
    [self.saveButton setTitle:updateTitle forState:UIControlStateHighlighted];
    [self.saveButton addTarget:self action:@selector(updateAction:) forControlEvents:UIControlEventTouchUpInside];

    NSString *cancelTitle = NSLocalizedString(@"Cancel", nil);
    [self.cancelButton setTitle:cancelTitle forState:UIControlStateNormal];
    [self.cancelButton setTitle:cancelTitle forState:UIControlStateHighlighted];
    [self.cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *titleData = [NSString stringWithFormat:@"%@", [self.document valueForKey:@"title"] ];
    NSString *descriptionData = [NSString stringWithFormat:@"%@", [self.document valueForKey:@"file_desc"] ];
    NSString *tags = [NSString stringWithFormat:@"%@", [self.document valueForKey:@"tags"] ];
    
    self.titleLabel.text = titleData;
    self.descriptionLabel.text = descriptionData;
    self.tagField.text = tags;
    
    [self setUpTableViews];
    [self insertSharedToShareList];
    [self requestTeacherList];
    
    
    [self versionCheckUI];
}

- (void)versionCheckUI {
    CGFloat version = [[Utils getServerInstanceVersion] floatValue];
    BOOL isVersion25 = (version >= VSMART_SERVER_MAX_VER);
    BOOL isHidden = (isVersion25) ? NO : YES;
    
    self.collectionView.hidden = isHidden;
    self.tableView.hidden = isHidden;
    self.searchBar.hidden = isHidden;
    self.shareToLabel.hidden = isHidden;
    self.tagLabel.hidden = isHidden;
    self.tagField.hidden = isHidden;
}

- (void)requestTeacherList {
    AccountInfo *account = [VSmart sharedInstance].account;
    self.user_id = [NSString stringWithFormat:@"%d", account.user.id ];
    
    if (self.isTeacherMode) {
        [self initiateSearchForString:@" "];
    } else {
        [self.plm requestListOfTeachers:^(NSArray *list) {
            if (list != nil) {
                if (list.count > 0) {//TODO: RYANM.
                    self.searchResult = list;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
        self.searchBarHeight.constant = 0;
        [self.view layoutIfNeeded];
    }
}

- (void)updateAction:(id)sender {
    
    NSString *title = [NSString stringWithFormat:@"%@", self.titleLabel.text];
    NSString *file_desc = [NSString stringWithFormat:@"%@", self.descriptionLabel.text];
    NSString *tags = [NSString stringWithFormat:@"%@", self.tagField.text];
    
    NSMutableArray *shareToArray = [NSMutableArray array];
    
    for (NSDictionary *dict in self.shareList) {
        NSString *context_id = [NSString stringWithFormat:@"%@", dict[@"context_id"]];
        NSString *group_level_id = [NSString stringWithFormat:@"%@", dict[@"group_level_id"]];
        
        NSDictionary *shareDict = @{@"id":@"0",
                                    @"group_level_id":group_level_id,
                                    @"context_id":context_id};
        
        [shareToArray addObject:shareDict];
    }
    
    NSDictionary *body = @{ @"module_id":@"4",
                            @"description":file_desc,
                            @"title":title,
                            @"tags":tags,
                            @"shared_to":shareToArray};
    
    
    __weak typeof(self) wo = self;
    [self.plm postUpdateOfUploadID:self.upload_id withPostBody:body doneBlock:^(BOOL status) {
        if (status == YES) {
            
//            [wo.plm requestPlayListForUser:self.user_id doneBlock:^(BOOL status) {
//                //do nothing
//            }];
            
            if ([(NSObject *)self.delegate respondsToSelector:@selector(didFinishUpdate:)]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [wo.delegate didFinishUpdate:YES];
                    [wo dismissViewControllerAnimated:YES completion:nil];
                });
            }
            
        }
    }];
}

- (void)setUpTableViews {
    // Resusable Cell
    UINib *cellItemNib = [UINib nibWithNibName:@"CheckBoxCell" bundle:nil];
    [self.tableView registerNib:cellItemNib forCellReuseIdentifier:@"SharedToCellID"];
    
    // Default Height
    self.tableView.estimatedRowHeight = 44.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    
//    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
//    layout.estimatedItemSize = CGSizeMake(50, 28);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(170, 44);
}

- (void)cancelAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)insertSharedToShareList {
    
    self.shareList = [NSMutableArray array];
    NSData *data = [self.document valueForKey:@"sharedArrayData"];
    NSArray *sharedArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    for (NSDictionary *dict in sharedArray) {
        NSString *context_id = [NSString stringWithFormat:@"%@", dict[@"context_id"]];
        NSString *name = [NSString stringWithFormat:@"%@", dict[@"name"]];
        NSString *group_level_id = [NSString stringWithFormat:@"%@", dict[@"group_level_id"]];
        NSString *category = [NSString stringWithFormat:@"%@", dict[@"category"]];
        
        NSDictionary *sharedDict = @{@"context_id":context_id,
                                     @"name":name,
                                     @"group_level_id": group_level_id,
                                     @"category":category};
        
        [self.shareList addObject:sharedDict];
    }
    
    [self resizeCollectionView];
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSString *)jsonStringFromArrayObject:(NSArray *)array {
    
    NSError *error = nil;
    NSData *dataObject = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    if (error) {
        NSLog(@"json error : %@", [error localizedDescription] );
    }
    NSString *jsonString = [[NSString alloc] initWithData:dataObject encoding:NSUTF8StringEncoding];
    //    NSString *jsonString = [NSString stringWithUTF8String:[dataObject bytes]];
    
    return jsonString;
}

- (void)initiateSearchForString:(NSString *)searchString {
    [self.plm requestListOfUserForSearchText:searchString listBlock:^(NSArray *list) {
        self.searchResult = list;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self initiateSearchForString:self.searchBar.text];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.7 target:self selector:@selector(observePause) userInfo:nil repeats:NO];
}

- (void)observePause {
    if (self.searchBar.text.length >= 3) {
        [self initiateSearchForString:self.searchBar.text];
    } else if (self.searchBar.text.length == 0) {
        [self initiateSearchForString:@" "];
    }
}
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchResult.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SharedToCellID" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    CheckBoxCell *itemCell = (CheckBoxCell *)cell;
    
    NSDictionary *userDict = self.searchResult[indexPath.row];
    NSString *name = [NSString stringWithFormat:@"%@", userDict[@"name"]];
    NSString *category = [NSString stringWithFormat:@"%@", userDict[@"category"]];
    NSString *context_id = [NSString stringWithFormat:@"%@", userDict[@"context_id"]];
    
    BOOL contains = NO;
    for (NSDictionary *shareItem in self.shareList) {
        NSString *s_context_id = [NSString stringWithFormat:@"%@", shareItem[@"context_id"]];
        if ([s_context_id isEqualToString:context_id]) {
            contains = YES;
            break;
        }
    }
    
    if (contains) {
        itemCell.checkBoxButton.selected = YES;
    }
    
    itemCell.checkBoxLabel.text = name;
    itemCell.cellCategory.text = category;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CheckBoxCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    BOOL checkBoxState = cell.checkBoxButton.selected;
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.checkBoxButton.selected = (checkBoxState) ? false : true;
    });
    
    NSDictionary *userDict = self.searchResult[indexPath.row];
    NSDictionary *dict = [NSDictionary dictionaryWithDictionary:userDict];
    
    if (checkBoxState) {
        NSString *context_id = [NSString stringWithFormat:@"%@", dict[@"context_id"]];
        NSArray *filteredShareList = [self.shareList filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id object, NSDictionary *bindings) {
            NSString *s_context_id = [NSString stringWithFormat:@"%@", object[@"context_id"]];
            return [s_context_id isEqualToString:context_id];  // Return YES for each object you want in filteredArray.
        }]];
        
        NSDictionary *sUserDict = [filteredShareList lastObject];
        NSUInteger index = [self.shareList indexOfObject:sUserDict];
        
        [self.shareList removeObjectAtIndex:index];
    } else {
        [self.shareList addObject:dict];
    }
    
//    (checkBoxState) ? [self.shareList removeObjectAtIndex:index] : [self.shareList addObject:dict];
    
    [self resizeCollectionView];
    [self.collectionView reloadData];
}

- (void)resizeCollectionView {
    
    CGFloat height = 0;
    if (self.shareList.count > 0) {
        height = 50;
        if (self.shareList.count > 3) {
            height = 100;
        }
    }
    
    self.collectionViewHeight.constant = height;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.shareList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SelectedCollectionViewCell *cell = (SelectedCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"selectedCell" forIndexPath:indexPath];
    
    cell.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.layer.masksToBounds = NO;
    cell.layer.shadowOffset = CGSizeMake(0, 3);
    cell.layer.shadowOpacity = 0.2;
    cell.layer.shadowRadius = 2;
    cell.layer.cornerRadius = 5;
    
    NSDictionary *userDict = self.shareList[indexPath.row];
    NSString *name = [NSString stringWithFormat:@"%@", userDict[@"name"]];
    
    cell.nameLabel.text = name;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger i = indexPath.row;
    
    [self uncheckTableCell:i];
    
    [self.shareList removeObjectAtIndex:i];
    [self resizeCollectionView];
    [self.collectionView reloadData];
}

- (void)uncheckTableCell:(NSInteger )i {
    NSDictionary *shareUserDict = self.shareList[i];
    
    NSString *context_id = [NSString stringWithFormat:@"%@", shareUserDict[@"context_id"]];
    NSArray *filteredShareList = [self.searchResult filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id object, NSDictionary *bindings) {
        NSString *s_context_id = [NSString stringWithFormat:@"%@", object[@"context_id"]];
        return [s_context_id isEqualToString:context_id];  // Return YES for each object you want in filteredArray.
    }]];
    
    NSDictionary *sUserDict = [filteredShareList lastObject];
    NSUInteger index = [self.searchResult indexOfObject:sUserDict];
//    NSInteger index = [self.searchResult indexOfObject:shareUserDict];
    if (@(index) != nil) {
        
        NSArray *visibleIndexPaths = [self.tableView indexPathsForVisibleRows];
        NSIndexPath *deselectIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
        
        if ([visibleIndexPaths containsObject:deselectIndexPath]) {
            CheckBoxCell *cell = [self.tableView cellForRowAtIndexPath:deselectIndexPath];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.checkBoxButton.selected = NO;
            });
            
        }
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 3;
}


@end
