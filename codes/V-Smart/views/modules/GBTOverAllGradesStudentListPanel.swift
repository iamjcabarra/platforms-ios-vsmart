//
//  GBTStudentListPanelView.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 03/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTOverAllGradesStudentListPanel: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate let cellIdentifier = "gbt_student_panel_cell"
    var isAscending = true
    
    var scrollDelegate : ScrollCommunicationDelegate?
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>?
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    var is_quarter = false
    
    fileprivate let notification = NotificationCenter.default
    
    @IBOutlet weak var searchBar: UISearchBar!
    var student_name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.searchBar.delegate = self
        
        let cellNib = UINib(nibName: "GBTStudentListCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellIdentifier)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension GBTOverAllGradesStudentListPanel: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, NSFetchedResultsControllerDelegate  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! GBTStudentListCell
        cell.backgroundColor = UIColor.clear
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ cell: GBTStudentListCell, atIndexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: atIndexPath)
        guard
            let name = mo.value(forKey: "name") as? String
            else { return }
        
        cell.customLabel.text = name
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: - Fetched Results Controller
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = gbdm.getMainContext()
        let ascending = self.isAscending
        let entity = GBTConstants.Entity.OVERALLGRADESSTUDENT
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        
        
        if self.student_name.characters.count > 0 {
            let predicate = NSComparisonPredicate(keyPath: "name", withValue: self.student_name, isExact: false)
            fetchRequest.predicate = predicate
        }
        fetchRequest.fetchBatchSize = 20
        
        let descriptorSelector = #selector(NSString.localizedCompare(_:))
        let section_sort = NSSortDescriptor(key: "section_sort", ascending: ascending, selector:descriptorSelector)
        fetchRequest.sortDescriptors = [section_sort]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        let tableViewController = tableView
        
        switch type {
        case .insert:
            tableViewController?.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableViewController?.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        let tableViewController = tableView
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableViewController?.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = tableViewController?.cellForRow(at: indexPath) {
                    configureCell(cell as! GBTStudentListCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableViewController?.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y;
        self.scrollDelegate?.gbScrollViewDidScroll(GBPanelType.StudentPanel, scrollTo: offsetY)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewWillBeginDragging(GBPanelType.StudentPanel)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.StudentPanel)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate == false {
            self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.StudentPanel)
        }
    }
    
}

extension GBTOverAllGradesStudentListPanel: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("DID CHANGE \(searchText)")
        self.student_name = searchText
        self.reloadFetchedResultsController()
        self.notification.post(name: Notification.Name(rawValue: "GBT_OVERALL_STUDENT_SEARCH"), object: searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("CANCEL BUTTON")
    }
    
}
