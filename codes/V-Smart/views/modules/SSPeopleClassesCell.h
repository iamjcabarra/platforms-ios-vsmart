//
//  SSPeopleClasses.h
//  V-Smart
//
//  Created by VhaL on 4/1/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSPeopleClassesCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *name;

-(void)initializeCell;
@end
