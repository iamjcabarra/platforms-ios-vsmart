//
//  CreateLessonPlanView.h
//  V-Smart
//
//  Created by Julius Abarra on 08/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LessonPlanConstants.h"

@interface CreateLessonPlanView : UIViewController

@property (assign, nonatomic) LPActionType actionType;
@property (strong, nonatomic) NSManagedObject *copiedLessonObject;

@end
