//
//  GradeBookViewController.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 9/6/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface GradeBookViewController : BaseViewController<UIWebViewDelegate>
{
    
}
@property (nonatomic, strong) UIWebView *theWebView;
@end
