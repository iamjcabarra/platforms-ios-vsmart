//
//  GBConstants.swift
//  V-Smart
//
//  Created by Ryan Migallos on 16/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import Foundation

struct GBTConstants {
    
    struct Entity {
        static let SECTION = "GBMSection"
        static let COURSE = "GBMCourse"
        static let TERM = "GBMTerm"
        static let STUDENT = "GBMStudent"
        static let STUDENTGRADES = "GBMStudentGrades"
        static let LEARNINGCOMPONENT = "GBMLearningComponents"
        static let SCORE = "GBMScore"
        static let RESULT = "GBMResultType"
        
        static let GRADESUMMARYSTUDENT = "GBMGradeSummaryStudent"
        static let GRADESUMMARYGRADE = "GBMGradeSummaryGrade"
        
        static let OVERALLGRADESSTUDENT = "GBMOverAllGradesStudent"
        static let OVERALLGRADES = "GBMOverAllGrades"
    }
    
    struct SegueName {
        static let GBTSTUDENTPANEL = "GBT_STUDENT_LIST_PANEL_EMBED"
        static let GBTLEARNINGCOMPONENTPANEL = "GBT_LEARNING_COMPONENT_PANEL_EMBED"
        static let GBTGRADEPANEL = "GBT_GRADE_PANEL_EMBED"
        static let GBTTESTPAPER = "GBT_SHOW_TESTPAPER_SEGUE"
        static let GBTCHANGESCORE = "GBT_OPTION_CHANGE_SCORE_SEGUE"
        static let GBTBATCHEDIT = "GBT_BATCHEDIT_SEGUE"
    }

    struct StoryBoard {
        static let GBTSCOREMENU = "GBT_SCOREMENU_SB"
        static let GBTTESTPAPER = "GBM_TEST_PAPER_SB"
    }
    
    struct Notification {
        static let DISPLAYTEST = "NOTIF_DISPLAY_TEST_PAPER"
        static let BATCHEDITSCORE = "NOTIF_BATCH_EDIT_SCORE"
    }
    
    struct UserDefined {
        static let GBTCSID = "GBT_USER_DEFINED_CSID"
        static let GBTCOURSEID = "GBT_USER_DEFINED_COURSE_ID"
        static let GBTSECTIONID = "GBT_USER_DEFINED_SECTION_ID"
        static let GBTCOURSEISTEACHING = "GBT_USER_DEFINED_COURSE_IS_TEACHING"
    }
}

protocol Routes: ParseProtocol, NetworkProtocol {
    func route(_ method:String, uri:String, body:[String:AnyObject]?) -> URLRequest
    func route(_ method:String, uri:String, query:[String:AnyObject]?, body:[String:AnyObject]?) -> URLRequest
}

extension Routes {
    func route(_ method:String, uri:String, body:[String:AnyObject]?) -> URLRequest {
        let endpoint = "vsmart-rest-dev" + "\(uri)"
        let url = buildURLFromRequestEndPoint(endpoint as NSString)
        let request = buildURLRequestWithMethod(method as NSString, url: url, andBody: body as AnyObject?)
        return request  as URLRequest
    }
    
    func route(_ method:String, uri:String, query:[String:AnyObject]?, body:[String:AnyObject]?) -> URLRequest {
        let endpoint = "vsmart-rest-dev" + "\(uri)"
        
        var url = buildURLFromRequestEndPoint(endpoint as NSString)

        if let dictionary = query {
            
            var queryVariables = [URLQueryItem]()
            for (name, object) in dictionary {
                let value = object as! String
                let queryObject = URLQueryItem(name: name, value: value)
                queryVariables.append(queryObject)
            }

            var urlComponents = URLComponents(string: url.absoluteString)!
            urlComponents.queryItems = queryVariables
            url = urlComponents.url!
        }
        
        let request = buildURLRequestWithMethod(method as NSString, url: url, andBody: body as AnyObject?)
        return request  as URLRequest
    }

}
