//
//  LPMLessonActionPopover.swift
//  V-Smart
//
//  Created by Julius Abarra on 23/06/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol LPMLessonActionPopoverDelegate: class {
    func selectedLessonAction(_ action: LPMLessonActionType)
}

class LPMLessonActionPopover: UITableViewController {
    
    weak var delegate:LPMLessonActionPopoverDelegate?
    
    var lessonStatus = ""
    var lessonid = ""
    
    fileprivate var actionList = [[String:Any]]()
    
    // MARK: - Data Manager
    
    fileprivate lazy var lpmDataManager: LPMDataManager = {
        let lpdm = LPMDataManager.sharedInstance
        return lpdm
    }()
    
    // MARK: - View Life Cycle
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let kEdit = NSLocalizedString("Edit", comment: "")
        let kSubmit = NSLocalizedString("Submit", comment: "")
        let kDownload = NSLocalizedString("Download", comment: "")
        let kOpen = NSLocalizedString("Open", comment: "")
        let kDelete = NSLocalizedString("Delete", comment: "")
        
        if (self.lessonStatus == "0") {
            self.actionList = [
                ["actionImage": "submit_blk_icn32x32.png", "actionName": kSubmit, "actionType": LPMLessonActionType.submit],
                ["actionImage": "edit_blk_icn32x32.png", "actionName": kEdit, "actionType": LPMLessonActionType.edit],
                ["actionImage": "delete_blk_icn32x32.png", "actionName": kDelete, "actionType": LPMLessonActionType.delete]
            ]
        }
        else if (self.lessonStatus == "1") {
            self.actionList = [
                /*["actionImage": "edit_blk_icn32x32.png", "actionName": kEdit, "actionType": LPMLessonActionType.edit],*/
                ["actionImage": "delete_blk_icn32x32.png", "actionName": kDelete, "actionType": LPMLessonActionType.delete]
            ]
        }
        else {
            let isDownloaded = self.lpmDataManager.isFileDownloadedForKey("file_key", value: lessonid)
            let actionName = isDownloaded ? kOpen : kDownload
            let actionType = isDownloaded ? LPMLessonActionType.open : LPMLessonActionType.download
            let actionImage = isDownloaded ? "open-44px.png" : "download_blk_icn32x32.png"
            
            self.actionList = [
                ["actionImage": actionImage, "actionName": actionName, "actionType": actionType],
                ["actionImage": "edit_blk_icn32x32.png", "actionName": kEdit, "actionType": LPMLessonActionType.edit],
                ["actionImage": "delete_blk_icn32x32.png", "actionName": kDelete, "actionType": LPMLessonActionType.delete]
            ]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table View Data Source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.actionList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        
        let action = self.actionList[(indexPath as NSIndexPath).row]
        let image = self.imageWithImage(UIImage(named: action["actionImage"] as! String)!, scaledToSize: CGSize(width: 32.0, height: 32.0))
        cell.imageView?.image = image
        cell.textLabel?.text = action["actionName"] as? String
        
        return cell
    }
    
    // MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let action = self.actionList[(indexPath as NSIndexPath).row]
        let actionType = action["actionType"] as! LPMLessonActionType
        
        self.dismiss(animated: true, completion: {
            self.delegate?.selectedLessonAction(actionType)
        })
    }
    
    // MARK: - Resizing Image View
    
    fileprivate func imageWithImage(_ image: UIImage, scaledToSize newSize: CGSize) -> UIImage{
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!.withRenderingMode(.alwaysOriginal)
    }
    
}
