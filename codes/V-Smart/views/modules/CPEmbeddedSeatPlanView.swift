//
//  CPEmbeddedSeatPlanView.swift
//  V-Smart
//
//  Created by Julius Abarra on 30/01/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class CPEmbeddedSeatPlanView: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NSFetchedResultsControllerDelegate, CPAttendanceStatusSelectionViewDelegate {
    
    @IBOutlet weak var presentLabel: UILabel!
    @IBOutlet weak var presentCountLabel: UILabel!
    @IBOutlet weak var lateLabel: UILabel!
    @IBOutlet weak var lateCountLabel: UILabel!
    @IBOutlet weak var absentLabel: UILabel!
    @IBOutlet weak var absentCountLabel: UILabel!
    @IBOutlet weak var teacherLabel: UILabel!
    @IBOutlet weak var teacherImage: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate var attendanceStatusSelectionView: CPAttendanceStatusSelectionView!
    
    fileprivate var blockOperations: [BlockOperation] = []
    fileprivate var cellIdentifier = "seat_cell_identifier"
    fileprivate var numberOfSeatsPerRow = 6
    
    fileprivate var longPressGesture : UILongPressGestureRecognizer!
    fileprivate var fromSelectedObject: NSManagedObject? = nil
    fileprivate var toSelectedObject: NSManagedObject? = nil
    
    var courseSectionID = ""
    
    // MARK: - Shared Data Manager
    
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tgdm = TestGuruDataManager.sharedInstance()
        return tgdm!
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Localize labels
        self.presentLabel.text = NSLocalizedString("Present", comment: "").uppercased()
        self.lateLabel.text = NSLocalizedString("Late", comment: "").uppercased()
        self.absentLabel.text = NSLocalizedString("Absent", comment: "").uppercased()
        self.teacherLabel.text = NSLocalizedString("Teacher", comment: "").uppercased()
        
        // Single selection only
        self.collectionView.allowsSelection = true
        self.collectionView.allowsMultipleSelection = false
        self.collectionView.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        
        // Teacher's seat
        if let account = VSmart.sharedInstance().account, let instance = Utils.getVibeServer(), let firstName = account.user.firstname,
            let lastName = account.user.lastname, let avatar = account.user.avatar  {
            
            self.teacherLabel.text = "\(lastName), \(firstName)"
            let avatarURL = URL(string: "http://\(instance)/\(avatar)")
            
            self.teacherImage.sd_setImage(with: avatarURL, completed: { (image, error, type, url) in
                if image == nil || error != nil {
                    self.teacherImage.image = UIImage(named: "placeholder_152.png")
                }
            })
        }
        
        // Long press for cell swapping
        self.longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(_:)))
        self.collectionView.addGestureRecognizer(self.longPressGesture)
        
        // Request seating arrangement
        self.loadSeatingArrangement()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func loadSeatingArrangement() {
        self.dataManager.requestSeatingArrangement2ForCourse(withSectionID: self.courseSectionID) { (dataBlock) in
            DispatchQueue.main.async(execute: {
                self.reloadFetchedResultsController()
                
                if dataBlock != nil {
                    let absentCount = dataBlock!["absent_count"] as! Int
                    let lateCount = dataBlock!["late_count"] as! Int
                    let presentCount = dataBlock!["count_all"] as! Int - (lateCount + absentCount)
                    
                    self.absentCountLabel.text = "\(absentCount)"
                    self.presentCountLabel.text = "\(presentCount)"
                    self.lateCountLabel.text = "\(lateCount)"
                }
            })
        }
    }
    
    func handleLongGesture(_ gesture: UILongPressGestureRecognizer) {
        
        switch(gesture.state) {
        
        case UIGestureRecognizerState.began:
            guard let selectedIndexPath = self.collectionView.indexPathForItem(at: gesture.location(in: self.collectionView)) else { break }
            self.fromSelectedObject = self.fetchedResultsController.object(at: selectedIndexPath)
            self.collectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
            
        case UIGestureRecognizerState.changed:
            self.collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
            
        case UIGestureRecognizerState.ended:
            self.collectionView.endInteractiveMovement()
            
            guard let selectedIndexPath = self.collectionView.indexPathForItem(at: gesture.location(in: self.collectionView)) else { break }
            self.toSelectedObject = self.fetchedResultsController.object(at: selectedIndexPath)
            
        default:
            self.collectionView.cancelInteractiveMovement()
            
        }
        
        if self.fromSelectedObject != nil && self.toSelectedObject != nil {
            let from_id = self.dataManager.stringValue(self.fromSelectedObject!.value(forKey: "csp_id"))
            let from_user_id = self.dataManager.stringValue(self.fromSelectedObject!.value(forKey: "user_id"))
            let from_ordering = self.dataManager.stringValue(self.fromSelectedObject!.value(forKey: "ordering"))
            let to_id = self.dataManager.stringValue(self.toSelectedObject!.value(forKey: "csp_id"))
            let to_user_id = self.dataManager.stringValue(self.toSelectedObject!.value(forKey: "user_id"))
            let to_ordering = self.dataManager.stringValue(self.toSelectedObject!.value(forKey: "ordering"))
            
            let from_body = ["id": from_id, "user_id": from_user_id, "ordering": to_ordering, "is_deleted": "0"]
            let to_body = ["id": to_id, "user_id": to_user_id, "ordering": from_ordering, "is_deleted": "0"]
            let body = ["cs_students":[from_body, to_body]]
            let teacherID = self.dataManager.loginUser()
            
            self.dataManager.requestChangeSeatOrderForCourse(withSectionID: self.courseSectionID,
                                                             andTeacherID: teacherID!,
                                                             withPostBody: body, doneBlock: { (success) in
                // IMPORTANT: Set Immediates to nil
                self.fromSelectedObject = nil
                self.toSelectedObject = nil
                                  
                // Request GET SEAT API regardless if request for ordering seat is successful or not
                self.loadSeatingArrangement()
                
                // Prompt user if error occurs
                DispatchQueue.main.async(execute: {
                    if !success {
                        let message = NSLocalizedString("Sorry, there was an error updating seat order. Please try again.", comment: "")    // unchecked
                        self.view.makeToast(message: message, duration: 2.0, position: "center" as AnyObject)
                    }
                })
            })
        }
    }
    
    // MARK: - Collection View Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath) as! CPSeatPlanCollectionViewCell
        let mo = self.fetchedResultsController.object(at: indexPath) 
        
        let first_name = self.dataManager.stringValue(mo.value(forKey: "first_name"))
        let last_name = self.dataManager.stringValue(mo.value(forKey: "last_name"))
        let avatar = self.dataManager.stringValue(mo.value(forKey: "avatar"))
        let is_late = self.dataManager.stringValue(mo.value(forKey: "is_late"))
        let is_absent = self.dataManager.stringValue(mo.value(forKey: "is_absent"))
        
        cell.studentNameLabel.text = "\(last_name!), \(first_name!)"
        
        cell.studentImage.sd_setImage(with: URL(string: avatar!), completed: { (image, error, type, url) in
            if image == nil || error != nil {
                cell.studentImage.image = UIImage(named: "placeholder_152.png")
            }
        })
        
        var status = "0"
        if is_late == "1" { status = "1" }
        if is_absent == "1" { status = "2" }
        
        cell.changeAttendaceStatusImage(forStatus: status)
        
        return cell
    }
    
    // MARK: - Collection View Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.attendanceStatusSelectionView = CPAttendanceStatusSelectionView(nibName: "CPAttendanceStatusSelectionView", bundle: nil)
        self.attendanceStatusSelectionView.seatObject = self.fetchedResultsController.object(at: indexPath)
        self.attendanceStatusSelectionView.delegate = self
        
        self.attendanceStatusSelectionView.modalPresentationStyle = .formSheet
        self.attendanceStatusSelectionView.preferredContentSize = CGSize(width: 350.0, height: 250.0)
        self.present(self.attendanceStatusSelectionView, animated: true, completion: nil)
    }
    
    func refreshSeatingArrangementView() {
        self.loadSeatingArrangement()
    }
    
    // MARK: Collection View Flow Layout Delegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left + flowLayout.sectionInset.right + (flowLayout.minimumInteritemSpacing * CGFloat(self.numberOfSeatsPerRow - 1))
        let size = (collectionView.bounds.width - totalSpace) / CGFloat(self.numberOfSeatsPerRow)
        
        if size <= 0 {
            DispatchQueue.main.async(execute: {
                let message = NSLocalizedString("", comment: "")
                self.view.makeToast(message: message, duration: 2.0, position: "center" as AnyObject)
            })
        }
        
        return CGSize(width: (size > 0 ?  size : 0), height: (size > 0 ?  size : 0))
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let context = self.dataManager.mainContext
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kStudentSeatingArrangementEntity)
        fetchRequest.fetchBatchSize = 20
        
        let sortDescriptor = NSSortDescriptor(key: "ordering", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
       
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                   managedObjectContext: context!,
                                                                   sectionNameKeyPath: nil,
                                                                   cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch _ as NSError {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    fileprivate func addProcessingBlock(_ processingBlock:@escaping (Void)->Void) {
        self.blockOperations.append(BlockOperation(block: processingBlock))
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.blockOperations.removeAll(keepingCapacity: false)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.insertItems(at: [newIndexPath]) }
            
        case .update:
            guard let newIndexPath = newIndexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.reloadItems(at: [newIndexPath]) }
            
        case .move:
            guard let indexPath = indexPath else { return }
            guard let newIndexPath = newIndexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.moveItem(at: indexPath, to: newIndexPath) }
            
        case .delete:
            guard let indexPath = indexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.deleteItems(at: [indexPath]) }
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.addProcessingBlock { [weak self] in self?.collectionView.insertSections(IndexSet(integer: sectionIndex)) }
            
        case .update:
            self.addProcessingBlock { [weak self] in self?.collectionView.reloadSections(IndexSet(integer: sectionIndex)) }
            
        case .delete:
            self.addProcessingBlock { [weak self] in self?.collectionView.deleteSections(IndexSet(integer: sectionIndex)) }
            
        default: break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.collectionView.performBatchUpdates({
            self.blockOperations.forEach { $0.start() }
        }, completion: { finished in
            self.blockOperations.removeAll(keepingCapacity: false)
        })
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.collectionView.reloadData()
        
        var error: NSError? = nil
        
        do {
            try self.fetchedResultsController.performFetch()
        }
        catch let error1 as NSError {
            error = error1
            print(error?.localizedDescription ?? "")
        }
    }

}

