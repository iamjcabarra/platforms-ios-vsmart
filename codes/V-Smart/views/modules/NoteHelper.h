//
//  NoteHelper.h
//  V-Smart
//
//  Created by Ryan Migallos on 2/6/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^NoteHelperDoneBlock)(BOOL status);

@interface NoteHelper : NSObject

- (void)saveMessage:(NSString *)message doneBlock:(NoteHelperDoneBlock)doneBlock;

@end
