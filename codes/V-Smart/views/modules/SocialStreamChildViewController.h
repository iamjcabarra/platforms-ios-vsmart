//
//  PostStreamView.h
//  V-Smart
//
//  Created by VhaL on 3/13/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "HMSegmentedControl.h"

@interface SocialStreamChildViewController : UIViewController <UIWebViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextViewDelegate>

+(void)postToDefaultGroup:(NSString*)message showResult:(BOOL)showResult;
@end
