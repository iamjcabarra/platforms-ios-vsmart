//
//  CourseDashBoard.h
//  V-Smart
//
//  Created by Ryan Migallos on 10/2/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface CourseDashBoard : UICollectionViewController

@end
