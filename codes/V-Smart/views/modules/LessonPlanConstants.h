//
//  LessonPlanConstants.h
//  V-Smart
//
//  Created by Julius Abarra on 08/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LessonPlanConstants : NSObject

typedef NS_ENUM (NSInteger, LPActionType) {
    LPActionTypeCreate = 1,
    LPActionTypeUpdate,
};

@end
