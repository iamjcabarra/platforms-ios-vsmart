//
//  TeacherShortcut.m
//  V-Smart
//
//  Created by Ryan Migallos on 4/17/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#define MAS_SHORTHAND
#import "Masonry.h"

#import "TeacherShortcut.h"
#import "TeacherClassCell.h"
#import "PeopleShortcutDetail.h"
#import "TeacherStudentCell.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"

@interface TeacherShortcut () <NSFetchedResultsControllerDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *segmentedContainer;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsControllerObject;
@property (strong, nonatomic) NSBlockOperation *blockOperation;
@property (assign, nonatomic) BOOL shouldReloadCollectionView;

@property (strong, nonatomic) ResourceManager *rm;
@property (strong, nonatomic) NSString *section_id;
@property (strong, nonatomic) NSArray *section_list;
@property (strong, nonatomic) HMSegmentedControl *segmentedControl;
@property (strong, nonatomic) NSMutableDictionary *lookup;//SEGMENT LOOKUP
@property (assign, nonatomic) NSInteger selectedIndex;

@property (strong, nonatomic) dispatch_queue_t queue;

@end

@implementation TeacherShortcut

static NSString *kCellIdentifier = @"teacher_shortcut_cell";
static NSString *kGridIdentifier = @"class_section_id";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.queue = dispatch_queue_create("com.vibetechonologies.people.SHORTCUT",DISPATCH_QUEUE_CONCURRENT);
    
    self.rm = [AppDelegate resourceInstance];
    
//    self.section_id = @"*";
    self.section_id = @"";
    self.selectedIndex = 0;
    self.title = @"People";
    
//    [self loadHeaders];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.tableView.allowsSelection = YES;
    
    [self loadHeaders];
}

- (void)loadHeaders {
    __weak typeof(self) wo = self;
    NSString *user_id = [NSString stringWithFormat:@"%d",[VSmart sharedInstance].account.user.id];
    [self.rm requestPeopleForUserID:user_id dataBlock:^(NSDictionary *data) {
        
        if (data != nil) {
            wo.lookup = [NSMutableDictionary dictionary];
            NSMutableArray *items = [NSMutableArray array];
            
            for (NSString *k in [data allKeys]) {
                NSString *section_name = [NSString stringWithFormat:@"%@", [data valueForKey:k]];
                
                if ([section_name isEqualToString:@"<null>"]) {
                    section_name = @"Teachers";
                }
                
                [items addObject:section_name];
                [wo.lookup setValue:k forKey:section_name];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self configureHeaderMenu:items];
                [self loadDataOnSection:items[self.selectedIndex]];
            });
        } else {
            
        }
    }];
}

- (void)loadDataOnSection:(NSString *)section {
    if (section != nil) {
        self.section_id = [NSString stringWithFormat:@"%@", [self.lookup valueForKey:section]];
        [self reloadFetchedResultsController];
    }
}

- (void)configureHeaderMenu:(NSArray *)items {

    if (items.count > 0) {
        
        HMSegmentedControl *sc = [[HMSegmentedControl alloc] init];
        sc.sectionTitles = items;
        sc.selectedSegmentIndex = self.selectedIndex;
        sc.backgroundColor = UIColorFromHex(0xf5f4f4);
        sc.titleTextAttributes = @{ NSForegroundColorAttributeName : UIColorFromHex(0x1d6483)};
        sc.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : UIColorFromHex(0x1d6483)};
        sc.selectionIndicatorColor = UIColorFromHex(0x00a9d5);
        sc.selectionStyle = HMSegmentedControlSelectionStyleBox;
        sc.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationUp;
        [sc addTarget:self action:@selector(segmentedAction:) forControlEvents:UIControlEventValueChanged];

//        self.segmentedControl.

        self.segmentedControl = sc;
        
        
        [self.segmentedContainer addSubview:self.segmentedControl];
        
        //Apply Constraints
        [self.segmentedControl makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.segmentedContainer);
            make.center.equalTo(self.segmentedContainer);
        }];
    }
}

- (void)segmentedAction:(id)sender {
    
    HMSegmentedControl *sc = (HMSegmentedControl *)sender;
    
    NSInteger index = sc.selectedSegmentIndex;
    self.selectedIndex = sc.selectedSegmentIndex;
    NSArray *sectionTitles = sc.sectionTitles;
    NSString *title = [NSString stringWithFormat:@"%@", sectionTitles[index] ];
    self.section_id = [NSString stringWithFormat:@"%@", [self.lookup valueForKey:title] ];
    
    [self reloadFetchedResultsController];
}

- (void)reloadFetchedResultsController
{
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)object atIndexPath:(NSIndexPath *)indexPath {
    TeacherStudentCell *cell = (TeacherStudentCell *)object;
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *name = [NSString stringWithFormat:@"%@ %@", [mo valueForKey:@"first_name"], [mo valueForKey:@"last_name"] ];
    NSString *flag = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_logged_in"] ];
    
    //if ([mo valueForKey:@"thumbnail"]) {
    //    cell.profileImage.image = [UIImage imageWithData:[mo valueForKey:@"thumbnail"]];
    //}
    //else {
    //    [self.rm downloadImagePeopleShortcut:mo dataBlock:^(NSData *binary) {
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            cell.profileImage.image = [UIImage imageWithData:binary];
    //        });
    //    }];
    //}
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.labelStatus.backgroundColor = [wo loginStatus:flag];
    cell.labelName.text = name;
    });
    
    // Bug #1151
    // jca-05112016
    // Fix disappearing of teacher's avatar
    NSString *avatar_url = [NSString stringWithFormat:@"%@", [mo valueForKey:@"avatar"]];
    [cell.profileImage sd_setImageWithURL:[NSURL URLWithString:avatar_url]];
}

- (UIColor *)loginStatus:(NSString *)flag {
    
    UIColor *online = UIColorFromHex(0x00FF00);
    UIColor *offline = [UIColor whiteColor];
    UIColor *color = ([flag isEqualToString:@"1"]) ? online : offline;
    
    return color;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.tableView.allowsSelection = NO;
    
    self.tableView = tableView;
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *profile_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ];
    
    [self.rm requestUserProfileWithID:profile_id dataBlock:^(NSDictionary *data) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"showShortcutTeacherDetail" sender:self];
        });
        
    }];

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showShortcutTeacherDetail"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        NSString *first_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"] ];
        NSString *last_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"] ];
        NSString *section_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_name"] ];
        NSString *user_type_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_type_id"] ];
        NSString *info_percentage = [NSString stringWithFormat:@"%@", [mo valueForKey:@"info_percentage"] ];
        NSString *count_liked = [NSString stringWithFormat:@"%@", [mo valueForKey:@"count_liked"] ];
        NSString *count_posted = [NSString stringWithFormat:@"%@", [mo valueForKey:@"count_posted"] ];
        NSString *count_received_likes = [NSString stringWithFormat:@"%@", [mo valueForKey:@"count_received_likes"] ];
        NSString *count_replied = [NSString stringWithFormat:@"%@", [mo valueForKey:@"count_replied"] ];
        NSData *thumbnail = [NSData dataWithData:[mo valueForKey:@"thumbnail"]];
        NSString *avatar_url = [NSString stringWithFormat:@"%@", [mo valueForKey:@"avatar"] ];
        
        NSDictionary *data = @{@"first_name":first_name,
                               @"last_name":last_name,
                               @"section_name":section_name,
                               @"user_type_id":user_type_id,
                               @"info_percentage":info_percentage,
                               @"count_liked":count_liked,
                               @"count_posted":count_posted,
                               @"count_received_likes":count_received_likes,
                               @"count_replied":count_replied,
                               @"thumbnail":thumbnail,
                               @"avatar_url":avatar_url};
        
        PeopleShortcutDetail *d = (PeopleShortcutDetail *)segue.destinationViewController;
        d.profileData = data;
        
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.rm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kShortcutPeopleEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];

    /*
     avatar = "/uploads/16/avatars/20150401/a1c1add31cbeb7a0ee609b9f16d6daff.";
     "first_name" = "Carryll ";
     "is_logged_in" = 1;
     "last_name" = Fritz;
     "mid_name" = Patel;
     "section_id" = 1;
     "section_name" = Gold;
     "user_id" = 16;
     "user_type_id" = 4;
     */
    
    // Filter By Section ID
    NSPredicate *predicate = [self predicateForKeyPath:@"section_id" value:self.section_id];
    [fetchRequest setPredicate:predicate];
    
    NSSortDescriptor *sort_name = [NSSortDescriptor sortDescriptorWithKey:@"first_name" ascending:YES];
    NSArray *sortDescriptors = @[sort_name];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    NSArray *paths = [self.tableView indexPathsForVisibleRows];
//    [self.tableView reloadRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationFade];
//}

@end
