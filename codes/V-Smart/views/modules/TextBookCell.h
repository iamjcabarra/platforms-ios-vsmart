//
//  TextBookCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 6/16/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Book;

@interface TextBookCell : UICollectionViewCell

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *dateLastRead;

@property (nonatomic, strong) Book *book;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, assign) CGFloat downloadProgress;

- (void)setBookProgressVisibility:(BOOL)show;
- (void)setBookProgress:(CGFloat)progress withText:(NSString *)progressText;
- (void)setCover:(NSString *)coverUrl;
- (void)setProgressVisibility:(BOOL)show;
- (void)setCompleted:(BOOL)completed;
- (void)setReadStatus:(BOOL)isRead;
- (void)setCloudVisibility:(BOOL)isVisible;
- (void)setAverageText:(NSString *)text;
- (void)setAverageVisibility:(BOOL)visible;
- (void)setDateLastReadVisibility:(BOOL)visible;

@end
