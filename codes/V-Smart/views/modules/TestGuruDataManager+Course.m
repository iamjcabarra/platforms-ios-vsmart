//
//  TestGuruDataManager+Course.m
//  V-Smart
//
//  Created by Ryan Migallos on 28/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TestGuruDataManager+Course.h"
#import "NSMutableArray+Shuffle.h"

@implementation TestGuruDataManager (Course)

- (void)requestRunOrPrerun:(NSString *)runType parameter:(NSDictionary *)parameter dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSString *user_id = [self loginUser];
    
    NSString *quiz_id = [NSString stringWithFormat:@"%@", parameter[@"quiz_id"] ];
    NSString *cs_id = [NSString stringWithFormat:@"%@", parameter[@"cs_id"] ];
    
    NSString *timeRemainingKey = [NSString stringWithFormat:@"USER%@TESTID%@", user_id, quiz_id];
    NSString *interuptedTimeRemaining = [self stringValue:[self fetchObjectForKey:timeRemainingKey] ];
    
    NSString *time_remaining = @"";
    if (timeRemainingKey.length > 0) {
        time_remaining = interuptedTimeRemaining;
    }
    
    NSDictionary *postBody = @{
                               @"time_remaining":time_remaining
                               };
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCoursePlayerRunOrPrerun, quiz_id, runType, user_id, cs_id] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
              if (dataBlock) {
                  dataBlock(@{@"error":[error localizedDescription]});
              }
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
              NSDictionary *meta = dictionary[@"_meta"];
              
              if (dictionary[@"records"] != nil ) {
                  NSDictionary *records = dictionary[@"records"];
                  NSDictionary *d = records[@"data"];
                  
                  NSString *test_id = [self stringValue:d[@"id"]];
                  NSString *time_remaining = [self stringValue:records[@"time_remaining"]];
                  
                  NSManagedObjectContext *ctx = self.workerContext;
                  
                  NSString *status = [self stringValue:records[@"status"]];
                  
                  if ([status isEqualToString:@"ERROR"]) {
                      NSString *errorMessage = records[@"message"];
                      if (dataBlock) {
                          dataBlock(@{@"error":errorMessage});
                      }
                      return;
                  }
                  
                  // ANOTHER ERROR HANDLING
                  NSString *meta_status = meta[@"status"];
                  if ([meta_status isEqualToString:@"ERROR"]) {
                      NSString *errorMessage = [self stringValue:records[@"more"]];
                      if (dataBlock) {
                          dataBlock(@{@"error":errorMessage});
                      }
                      return;
                  }
                  
                  if ([runType isEqualToString:@"run"]) {
                      NSManagedObject *mo = [self getEntity:kCoursePlayerTestEntity attribute:@"id" parameter:test_id context:ctx];
                      [ctx performBlock:^{
                          [[NSUserDefaults standardUserDefaults] removeObjectForKey:timeRemainingKey];
                          [[NSUserDefaults standardUserDefaults] synchronize];
                          [mo setValue:time_remaining forKey:@"time_remaining"];
                      }];
                      [self saveTreeContext:ctx];
                      if (dataBlock) {
                          dataBlock(@{@"mo":mo});
                      }
                      return;
                  }
                  
                  [self clearDataForEntity:kCoursePlayerTestEntity withPredicate:nil context:ctx];
                  [self clearDataForEntity:kCoursePlayerQuestionListEntity withPredicate:nil context:ctx];
                  [self clearDataForEntity:kCoursePlayerQuestionChoiceEntity withPredicate:nil context:ctx];
                  
                  [ctx performBlock:^{
                      NSString *quiz_settings_id = [self stringValue:d[@"quiz_settings_id"]];
                      NSString *name = [self stringValue:d[@"name"]];
                      NSString *description = [self stringValue:d[@"description"]];
                      NSString *general_instruction = [self stringValue:d[@"general_instruction"]];
                      NSString *quiz_category_id = [self stringValue:d[@"quiz_category_id"]];
                      NSString *quiz_type_id = [self stringValue:d[@"quiz_type_id"]];
                      NSString *quiz_result_type_id = [self stringValue:d[@"quiz_result_type_id"]];
                      NSString *learning_skills_id = [self stringValue:d[@"learning_skills_id"]];
                      NSString *difficulty_level_id = [self stringValue:d[@"difficulty_level_id"]];
                      NSString *date_open = [self stringValue:d[@"date_open"]];
                      NSString *date_close = [self stringValue:d[@"date_close"]];
                      NSString *attempts = [self stringValue:d[@"attempts"]];
                      NSString *quiz_shuffling_mode = [self stringValue:d[@"quiz_shuffling_mode"]];
                      NSString *is_shuffle_answers = [self stringValue:d[@"is_shuffle_answers"]];
                      NSString *total_score = [self stringValue:d[@"total_score"]];
                      NSString *time_limit = [self stringValue:d[@"time_limit"]];
                      NSString *is_forced_complete = [self stringValue:d[@"is_forced_complete"]];
                      NSString *password = [self stringValue:d[@"password"]];
                      NSString *quiz_stage_id = [self stringValue:d[@"quiz_stage_id"]];
                      NSString *show_feedbacks = [self stringValue:d[@"show_feedbacks"]];
                      NSString *allow_review = [self stringValue:d[@"allow_review"]];
                      NSString *passing_rate = [self stringValue:d[@"passing_rate"]];
                      NSString *quiz_category_name = [self stringValue:d[@"quiz_category_name"]];
                      NSString *question_type_name = [self stringValue:d[@"question_type_name"]];
                      NSString *learning_skills_name = [self stringValue:d[@"learning_skills_name"]];
                      NSString *difficulty_level_name = [self stringValue:d[@"difficulty_level_name"]];
                      NSString *quiz_stage_name = [self stringValue:d[@"quiz_stage_name"]];
                      NSString *attempts_count = [self stringValue:records[@"attempts_count"]];
                      
                      NSString *show_score = [self stringValue:d[@"show_score"]];
                      NSString *show_result = [self stringValue:d[@"show_result"]];
                      NSString *show_correct_answer = [self stringValue:d[@"show_correct_answer"]];
                      
                      NSInteger attemptValue = [attempts integerValue] - [attempts_count integerValue];
                      attempts = [NSString stringWithFormat:@"%zd", attemptValue];
                      
                      id attempt = d[@"attempt"];
                      
                      if ([attempt isKindOfClass:[NSArray class]]) {
                          // do something
                      } else {
                          // do something
                      }
                      
                      NSManagedObject *mo = [self getEntity:kCoursePlayerTestEntity attribute:@"id" parameter:test_id context:ctx];
                      
                      [mo setValue:test_id forKey:@"id"];
                      [mo setValue:quiz_settings_id forKey:@"quiz_settings_id"];
                      [mo setValue:name forKey:@"name"];
                      [mo setValue:description forKey:@"quiz_description"];
                      [mo setValue:general_instruction forKey:@"general_instruction"];
                      [mo setValue:quiz_category_id forKey:@"quiz_category_id"];
                      [mo setValue:quiz_type_id forKey:@"quiz_type_id"];
                      [mo setValue:quiz_result_type_id forKey:@"quiz_result_type_id"];
                      [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
                      [mo setValue:difficulty_level_id forKey:@"difficulty_level_id"];
                      [mo setValue:date_open forKey:@"date_open"];
                      [mo setValue:date_close forKey:@"date_close"];
                      [mo setValue:attempts forKey:@"attempts"];
                      [mo setValue:quiz_shuffling_mode forKey:@"quiz_shuffling_mode"];
                      [mo setValue:is_shuffle_answers forKey:@"is_shuffle_answers"];
                      [mo setValue:total_score forKey:@"total_score"];
                      [mo setValue:time_limit forKey:@"time_limit"];
                      [mo setValue:is_forced_complete forKey:@"is_forced_complete"];
                      [mo setValue:password forKey:@"password"];
                      [mo setValue:quiz_stage_id forKey:@"quiz_stage_id"];
                      [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
                      [mo setValue:allow_review forKey:@"allow_review"];
                      [mo setValue:passing_rate forKey:@"passing_rate"];
                      [mo setValue:quiz_category_name forKey:@"quiz_category_name"];
                      [mo setValue:question_type_name forKey:@"question_type_name"];
                      [mo setValue:learning_skills_name forKey:@"learning_skills_name"];
                      [mo setValue:difficulty_level_name forKey:@"difficulty_level_name"];
                      [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
                      [mo setValue:time_remaining forKey:@"time_remaining"];
                      [mo setValue:attempts_count forKey:@"attempts_count"];
                      
                      [mo setValue:attempts_count forKey:@"attempts_count"];
                      [mo setValue:show_score forKey:@"show_score"];
                      [mo setValue:show_result forKey:@"show_result"];
                      [mo setValue:show_correct_answer forKey:@"show_correct_answer"];
                      
                      NSArray *q_array = records[@"questions"];
                      NSMutableSet *q_sets = [NSMutableSet set];
                      
                      if (q_array.count > 0) {
                          NSInteger index = 0;
                          for (NSDictionary *q_dict in q_array) {
                              
                              NSString *question_id = [self stringValue:q_dict[@"question_id"]];
                              NSString *quiz_id = [self stringValue:q_dict[@"quiz_id"]];
                              NSString *question_type_id = [self stringValue:q_dict[@"question_type_id"]];
                              NSString *question_type_name = [self stringValue:q_dict[@"question_type_name"]];
                              NSString *question_text = [self stringValue:q_dict[@"question_text"]];
                              NSString *points = [self stringValue:q_dict[@"points"]];
                              NSString *is_case_sensitive = [self stringValue:q_dict[@"is_case_sensitive"]];
                              
                              NSString *number_of_correct_answers = ([question_type_id isEqualToString:@"3"]) ? [self stringValue:q_dict[@"number_of_correct_answers"] ] : @"1";
                              
                              number_of_correct_answers = (number_of_correct_answers.length == 0) ? @"1" : number_of_correct_answers;
                              
                              if ([points containsString:@"null"]) {
                                  points = @"0";
                              }
                              
                              points = [self formatStringNumber:points];
                              NSString *image_one = @"";
                              NSString *image_two = @"";
                              NSString *image_three = @"";
                              NSString *image_four = @"";
                              
                              NSArray *q_images = q_dict[@"question_images"];
                              
                              for (NSDictionary *imageDict in q_images) {
                                  NSString *image_index = [self stringValue:imageDict[@"index"]];
                                  NSString *image = [self stringValue:imageDict[@"image"]];
                                  
                                  if (image.length > 0) {
                                      if ([image_index isEqual:@"1"]) {
                                          image_one = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
                                      }
                                      if ([image_index isEqual:@"2"]) {
                                          image_two = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
                                      }
                                      if ([image_index isEqual:@"3"]) {
                                          image_three = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
                                      }
                                      if ([image_index isEqual:@"4"]) {
                                          image_four = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
                                      }
                                  }
                              }
                              
                              NSManagedObjectContext *question_ctx = self.workerContext;
                              NSManagedObject *q_mo = [self getEntity:kCoursePlayerQuestionListEntity attribute:@"question_id" parameter:question_id context:question_ctx];
                              
                              [q_mo setValue:question_id forKey:@"question_id"];
                              [q_mo setValue:quiz_id forKey:@"quiz_id"];
                              [q_mo setValue:question_type_id forKey:@"question_type_id"];
                              [q_mo setValue:question_type_name forKey:@"question_type_name"];
                              [q_mo setValue:question_text forKey:@"question_text"];
                              [q_mo setValue:points forKey:@"points"];
                              [q_mo setValue:is_case_sensitive forKey:@"is_case_sensitive"];
                              [q_mo setValue:number_of_correct_answers forKey:@"number_of_correct_answers"];
                              
                              [q_mo setValue:image_one forKey:@"image_one"];
                              [q_mo setValue:image_two forKey:@"image_two"];
                              [q_mo setValue:image_three forKey:@"image_three"];
                              [q_mo setValue:image_four forKey:@"image_four"];
                              [q_mo setValue:@"0" forKey:@"is_visited"];
                              
                              
                              // PREPARED FOR Q_CHOICES DONT DELETE
                              //                          BOOL q_choices_is_array = [self isArrayObject:q_dict[@"question_choices"]];
                              
                              NSMutableSet *c_sets = [NSMutableSet set];
                              NSManagedObjectContext *choice_ctx = self.workerContext;
                              
                              // ESSAY AND SHORTANSWER
                              if ([question_type_id isEqual:@"6"] || [question_type_id isEqual:@"9"] || [question_type_id isEqual:@"10"] || [question_type_id isEqual:@"2"]) {
                                  NSManagedObject *c_mo = [self getEntity:kCoursePlayerQuestionChoiceEntity attribute:@"id" parameter:question_id context:choice_ctx];//[NSEntityDescription insertNewObjectForEntityForName:kCoursePlayerQuestionChoiceEntity inManagedObjectContext:choice_ctx];
                                  
                                  NSString *text = @"";
                                  
                                  
                                  NSArray *q_choices = q_dict[@"question_choices"];
                                  
                                  /**
                                   "question_choices": [
                                   {
                                   "text": "HELLO ITO ITO ITO"
                                   }
                                   ]
                                   **/
                                  if (([q_choices isKindOfClass:NSNull.class] == false) && ([q_choices isKindOfClass:NSArray.class]) && q_choices.count == 1) {
                                      NSDictionary *c_dict = q_choices.lastObject;
                                      text = [c_dict valueForKey:@"text"];
                                  }
                                  
                                  NSString *answer = (text.length > 0) ? @"1" : @"0";
                                  
                                  [c_mo setValue:question_type_id forKey:@"question_type_id"];
                                  [c_mo setValue:@"0" forKey:@"id"];
                                  [c_mo setValue:question_id forKey:@"question_id"];
                                  [c_mo setValue:text forKey:@"text"];
                                  [c_mo setValue:@"" forKey:@"suggestive_feedback"];
                                  [c_mo setValue:@"1" forKey:@"is_correct"];
                                  //                              [c_mo setValue:question_choice_image forKey:@"question_choice_image"];
                                  [c_mo setValue:@"0" forKey:@"order_number"];
                                  [c_mo setValue:@"0" forKey:@"is_deleted"];
                                  [c_mo setValue:[self stringDate:[NSDate date]] forKey:@"date_created"];
                                  [c_mo setValue:[self stringDate:[NSDate date]] forKey:@"date_modified"];
                                  [c_mo setValue:answer forKey:@"answer"];
                                  
                                  // additional ordernumber
                                  [c_mo setValue:@(1) forKey:@"order_number_int"];
                                  
                                  [c_sets addObject:c_mo];
                              }
                              
                              // FOR MULTIPLE CHOICES AND TOF
                              else {
                                  NSMutableArray *q_choices = q_dict[@"question_choices"];
                                  
                                  BOOL is_shuffle_choices = [is_shuffle_answers isEqualToString:@"1"];
                                  
                                  if (is_shuffle_choices) {
                                      [q_choices shuffleV21];
                                  }
                                  
                                  NSInteger order_index = 0;
                                  for (NSDictionary *c_dict in q_choices) {
                                      
                                      NSString *choice_id = [self stringValue:c_dict[@"id"]];
                                      NSString *question_id = [self stringValue:c_dict[@"question_id"]];
                                      NSString *text = [self stringValue:c_dict[@"text"]];
                                      NSString *suggestive_feedback = [self stringValue:c_dict[@"suggestive_feedback"]];
                                      NSString *is_correct = [self stringValue:c_dict[@"is_correct"]];
                                      NSString *question_choice_image = [self stringValue:c_dict[@"question_choice_image"]];
                                      NSString *order_number = [self stringValue:c_dict[@"order_number"]];
                                      NSString *is_deleted = [self stringValue:c_dict[@"is_deleted"]];
                                      NSString *date_created = [self stringValue:c_dict[@"date_created"]];
                                      NSString *date_modified = [self stringValue:c_dict[@"date_modified"]];
                                      NSString *user_choice = [self stringValue:c_dict[@"user_choice"]];
                                      
                                      if ([is_correct isEqualToString:@"1"]) {
                                          is_correct = @"100";
                                      }
                                      
                                      NSString *choice_image = @"";
                                      if (question_choice_image.length > 0) {
                                          if (![question_choice_image  isEqual: @"null"]) {
                                              choice_image = [NSString stringWithFormat:@"%@/%@", homeurl, question_choice_image];
                                          }
                                      }
                                      
                                      [choice_ctx performBlock:^{
                                          NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:kCoursePlayerQuestionChoiceEntity inManagedObjectContext:choice_ctx];
                                      
                                      [c_mo setValue:question_type_id forKey:@"question_type_id"];
                                      [c_mo setValue:choice_id forKey:@"id"];
                                      [c_mo setValue:question_id forKey:@"question_id"];
                                      [c_mo setValue:text forKey:@"text"];
                                      [c_mo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
                                      [c_mo setValue:is_correct forKey:@"is_correct"];
                                      [c_mo setValue:choice_image forKey:@"question_choice_image"];
                                      [c_mo setValue:order_number forKey:@"order_number"];
                                      [c_mo setValue:is_deleted forKey:@"is_deleted"];
                                      [c_mo setValue:date_created forKey:@"date_created"];
                                      [c_mo setValue:date_modified forKey:@"date_modified"];
                                      [c_mo setValue:user_choice forKey:@"answer"];
                                      [c_mo setValue:user_choice forKey:@"user_choice"];
                                      
                                      // additional ordernumber
//                                      [c_mo setValue:@(order_number.integerValue) forKey:@"order_number_int"];
                                      
                                      if (is_shuffle_choices) {
                                          [c_mo setValue:@(order_index) forKey:@"order_number_int"];
                                      } else {
                                          [c_mo setValue:@([order_number integerValue]) forKey:@"order_number_int"];
                                      }
                                  
                                      [c_sets addObject:c_mo];
                                      }];
                                      order_index++;
                                  }
                              }
                              
                              [q_mo setValue:[NSSet setWithSet:c_sets] forKey:@"choices"];
                              [q_mo setValue:@(index) forKey:@"index"];
                              [q_sets addObject:q_mo];
                              
                              index ++;
                              
                          }
                      } else {
                          if (dataBlock) {
                              dataBlock(@{@"error":@"No available questions"});
                          }
                          return;
                      }
                      [mo setValue:[NSSet setWithSet:q_sets] forKey:@"questions"];
                      
                      [self saveTreeContext:ctx];
                      
                      if (dataBlock) {
                          dataBlock(@{@"mo": mo});
                      }
                      
                  }];
              }
          }//end
      }];
    [task resume];
}

- (NSString *)stringDate:(NSDate *)date {
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [self.formatter setTimeZone:gmt];
    NSString *template = @"yyyy-MM-dd hh:mm:ss";
    [self.formatter setDateFormat:template];
    NSString *date_string = [self.formatter stringFromDate:date];
    
    return date_string;
}

- (void)requestPaginatedCourseListForUser:(NSString *)userid withSettingsForPagination:(NSDictionary *)settings dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSString *limit = [self stringValue:settings[@"limit"]];
    NSString *current_page = [self stringValue:settings[@"current_page"]];
    NSString *search_keyword = [self stringValue:settings[@"search_keyword"]];
    NSString *enconded_search_keyword = [self urlEncode:search_keyword];
    NSString *sort_key = [self stringValue:settings[@"sort_key"]];
    
    NSManagedObjectContext *ctx = self.workerContext;
    
    if ([current_page isEqualToString:@"1"]) {
        [self clearDataForEntity:kCourseListEntity withPredicate:nil context:ctx];
    }
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCPPaginatedCourseListV2, userid, limit, current_page, enconded_search_keyword, sort_key]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSDictionary *records = dictionary[@"records"];
                
//                NSDictionary *pagination = records[@"pagination"];
                
                NSMutableDictionary *pagination = [NSMutableDictionary dictionary];
                if (records[@"pagination"] != nil) {
                    NSDictionary *object = (NSDictionary *)records[@"pagination"];
                    for (NSString *k in object.allKeys) {
                        pagination[k] = [self stringValue: object[k] ];
                    }
                }
                
                NSArray *data = records[@"data"];
                
                NSLog(@"records: %@", records);
                NSLog(@"pagination: %@", data);
                NSLog(@"data: %@", data);
                
                if ([self isArrayObject:data] && data.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d in data) {
                            NSString *course_id = [self stringValue:d[@"course_id"]];
                            NSString *course_name = [self stringValue:d[@"course_name"]];
                            NSString *initial = [self stringValue:d[@"initial"]];
                            NSString *course_description = [self stringValue:d[@"description"]];
                            NSString *course_code = [self stringValue:d[@"course_code"]];
                            NSString *course_banner = [self stringValue:d[@"course_banner"]];
                            NSString *grade_level_id = [self stringValue:d[@"grade_level_id"]];
                            NSString *school_level_id = [self stringValue:d[@"school_level_id"]];
                            NSString *cs_id = [self stringValue:d[@"cs_id"]];
                            NSString *section_id = [self stringValue:d[@"section_id"]];
                            NSString *schedule = [self stringValue:d[@"schedule"]];
                            NSString *venue = [self stringValue:d[@"venue"]];
                            NSString *section_name = [self stringValue:d[@"section_name"]];
                            NSString *grade_level = [self stringValue:d[@"grade_level"]];
                            NSString *school_level = [self stringValue:d[@"school_level"]];
                            NSString *search_string = [NSString stringWithFormat:@"%@%@%@%@", course_name, section_name, grade_level, schedule];
                            
                            NSManagedObject *mo = [self getEntity:kCourseListEntity attribute:@"course_id" parameter:course_id context:ctx];
                            
                            [mo setValue:userid forKey:@"user_id"];
                            [mo setValue:course_id forKey:@"course_id"];
                            [mo setValue:course_name forKey:@"course_name"];
                            [mo setValue:initial forKey:@"initial"];
                            [mo setValue:course_description forKey:@"course_description"];
                            [mo setValue:course_code forKey:@"course_code"];
                            [mo setValue:course_banner forKey:@"course_banner"];
                            [mo setValue:grade_level_id forKey:@"grade_level_id"];
                            [mo setValue:school_level_id forKey:@"school_level_id"];
                            [mo setValue:cs_id forKey:@"cs_id"];
                            [mo setValue:section_id forKey:@"section_id"];
                            [mo setValue:schedule forKey:@"schedule"];
                            [mo setValue:venue forKey:@"venue"];
                            [mo setValue:section_name forKey:@"section_name"];
                            [mo setValue:grade_level forKey:@"grade_level"];
                            [mo setValue:school_level forKey:@"school_level"];
                            [mo setValue:search_string forKey:@"search_string"];
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                }
                
                if (dataBlock) {
                    dataBlock(pagination);
                }
            }
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)executeUpdateForEntity:(NSString *)entity details:(NSDictionary *)dictionary predicate:(NSPredicate *)predicate doneBlock:(TestGuruDoneBlock)doneBlock {
//    NSManagedObject *mo = [self getEntity:entity predicate:predicate];
//    
//    NSManagedObjectContext *ctx = mo.managedObjectContext;
//    if (mo != nil) {
//        
//        [ctx performBlockAndWait:^{
//            for (NSString *key in [dictionary allKeys]) {
//                NSString *v = [NSString stringWithFormat:@"%@", dictionary[key] ];
//                [mo setValue:v forKey:key];
//            }
//            
//            [self saveTreeContext:ctx];
//        }];
//        if (doneBlock) {
//            doneBlock(YES);
//        }
//    }
//
    NSManagedObject *mo = [self getEntity:entity predicate:predicate];
    NSManagedObjectContext *ctx = mo.managedObjectContext;
    
    if (mo != nil) {
        [ctx performBlock:^{
            for (NSString *key in [dictionary allKeys]) {
                NSString *v = [NSString stringWithFormat:@"%@", dictionary[key]];
                [mo setValue:v forKey:key];
            }
            
            [self saveTreeContext:ctx];
        }];
        
        if (doneBlock) {
            doneBlock(YES);
        }
    }
    
}

- (void)requestCourseListForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCPCourseListForUser, userid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        NSManagedObjectContext *ctx = self.workerContext;
        [self clearDataForEntity:kCourseListEntity withPredicate:nil context:ctx];
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"records: %@", records);
                
                if ([self isArrayObject:records] && records.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d in records) {
                            NSLog(@"data: %@", d);
                            
                            NSString *course_id = [self stringValue:d[@"course_id"]];
                            NSString *course_name = [self stringValue:d[@"course_name"]];
                            NSString *initial = [self stringValue:d[@"initial"]];
                            NSString *course_description = [self stringValue:d[@"description"]];
                            NSString *course_code = [self stringValue:d[@"course_code"]];
                            NSString *cs_id = [self stringValue:d[@"cs_id"]];
                            NSString *section_id = [self stringValue:d[@"section_id"]];
                            NSString *section_name = [self stringValue:d[@"section_name"]];
                            NSString *grade_level = [self stringValue:d[@"grade_level"]];
                            NSString *schedule = [self stringValue:d[@"schedule"]];
                            NSString *venue = [self stringValue:d[@"venue"]];
                            
                            NSManagedObject *mo = [self getEntity:kCourseListEntity attribute:@"cs_id" parameter:cs_id context:ctx];
                            
                            [mo setValue:course_id forKey:@"course_id"];
                            [mo setValue:course_name forKey:@"course_name"];
                            [mo setValue:initial forKey:@"initial"];
                            [mo setValue:course_description forKey:@"course_description"];
                            [mo setValue:course_code forKey:@"course_code"];
                            [mo setValue:cs_id forKey:@"cs_id"];
                            [mo setValue:section_id forKey:@"section_id"];
                            [mo setValue:section_name forKey:@"section_name"];
                            [mo setValue:grade_level forKey:@"grade_level"];
                            [mo setValue:schedule forKey:@"schedule"];
                            [mo setValue:venue forKey:@"venue"];
                            
                            NSString *search_string = [NSString stringWithFormat:@"%@%@%@%@", course_name, section_name, grade_level, schedule];
                            [mo setValue:search_string forKey:@"search_string"];
                            
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                }
                
                if (doneBlock) {
                    doneBlock(YES);
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestCourseListForUser:(NSString *)userid dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kCourseListEntity withPredicate:nil context:ctx];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCPCourseListForUser, userid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"records: %@", records);
                
                if ([self isArrayObject:records] && records.count > 0) {
                    [ctx performBlockAndWait:^{
                        for (NSDictionary *d in records) {
                            NSLog(@"data: %@", d);
                            
                            NSString *course_id = [self stringValue:d[@"course_id"]];
                            NSString *course_name = [self stringValue:d[@"course_name"]];
                            NSString *initial = [self stringValue:d[@"initial"]];
                            NSString *course_description = [self stringValue:d[@"description"]];
                            NSString *course_code = [self stringValue:d[@"course_code"]];
                            NSString *cs_id = [self stringValue:d[@"cs_id"]];
                            NSString *section_id = [self stringValue:d[@"section_id"]];
                            NSString *section_name = [self stringValue:d[@"section_name"]];
                            NSString *grade_level = [self stringValue:d[@"grade_level"]];
                            NSString *schedule = [self stringValue:d[@"schedule"]];
                            NSString *venue = [self stringValue:d[@"venue"]];
                            
                            NSManagedObject *mo = [self getEntity:kCourseListEntity attribute:@"cs_id" parameter:cs_id context:ctx];
                            
                            [mo setValue:course_id forKey:@"course_id"];
                            [mo setValue:course_name forKey:@"course_name"];
                            [mo setValue:initial forKey:@"initial"];
                            [mo setValue:course_description forKey:@"course_description"];
                            [mo setValue:course_code forKey:@"course_code"];
                            [mo setValue:cs_id forKey:@"cs_id"];
                            [mo setValue:section_id forKey:@"section_id"];
                            [mo setValue:section_name forKey:@"section_name"];
                            [mo setValue:grade_level forKey:@"grade_level"];
                            [mo setValue:schedule forKey:@"schedule"];
                            [mo setValue:venue forKey:@"venue"];
                            
                            NSString *search_string = [NSString stringWithFormat:@"%@%@%@%@", course_name, section_name, grade_level, schedule];
                            [mo setValue:search_string forKey:@"search_string"];
                            
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                }
                
                if (dataBlock) {
                    NSDictionary *data = @{@"count":@(records.count)};
                    dataBlock(data);
                }
            }
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestPaginatedTestListForCourse:(NSString *)courseid andUser:(NSString *)userid withSettingsForPagination:(NSDictionary *)settings dataBlock:(TestGuruDataBlock)dataBlock {
    NSString *limit = [self stringValue:settings[@"limit"]];
    NSString *current_page = [self stringValue:settings[@"current_page"]];
    NSString *search_keyword = [self stringValue:settings[@"search_keyword"]];
    NSString *enconded_search_keyword = [self urlEncode:search_keyword];
    
    NSManagedObjectContext *ctx = self.workerContext;
    
    if ([current_page isEqualToString:@"1"]) {
        [self clearDataForEntity:kCourseTestListEntity withPredicate:nil context:ctx];
    }
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCPPaginatedCourseTestList, courseid, userid, limit, current_page, enconded_search_keyword]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSDictionary *records = dictionary[@"records"];
                NSDictionary *pagination = records[@"pagination"];
                NSArray *data = records[@"data"];
                
                NSLog(@"records: %@", records);
                NSLog(@"pagination: %@", data);
                NSLog(@"data: %@", data);
                
                if ([self isArrayObject:data] && data.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d in data) {
                            NSString *test_id = [self stringValue:d[@"id"]];
                            NSString *name = [self stringValue:d[@"name"]];
                            NSString *general_instruction = [self stringValue:d[@"general_instruction"]];
                            NSString *quiz_settings_id = [self stringValue:d[@"quiz_settings_id"]];
                            NSString *description = [self stringValue:d[@"test_description"]];
                            NSString *quiz_category_id = [self stringValue:d[@"quiz_category_id"]];
                            NSString *quiz_category_name = [self stringValue:d[@"quiz_category_name"]];
                            NSString *difficulty_level_id = [self stringValue:d[@"difficulty_level_id"]];
                            NSString *date_open = [self stringValue:d[@"date_open"]];
                            NSString *formatted_date_open = [self stringValue:d[@"formatted_date_open"]];
                            NSString *date_close = [self stringValue:d[@"date_close"]];
                            NSString *formatted_date_close = [self stringValue:d[@"formatted_date_close"]];
                            NSString *attempts = [self stringValue:d[@"attempts"]];
                            NSString *quiz_shuffling_mode = [self stringValue:d[@"quiz_shuffling_mode"]];
                            NSString *is_shuffle_answers = [self stringValue:d[@"is_shuffle_answers"]];
                            NSString *total_score = [self stringValue:d[@"total_score"]];
                            NSString *time_limit = [self stringValue:d[@"time_limit"]];
                            NSString *is_forced_complete = [self stringValue:d[@"is_forced_complete"]];
                            NSString *is_graded = [self stringValue:d[@"is_graded"]];
                            NSString *password = [self stringValue:d[@"password"]];
                            NSString *show_feedbacks = [self stringValue:d[@"show_feedbacks"]];
                            NSString *allow_review = [self stringValue:d[@"allow_review"]];
                            NSString *passing_rate = [self stringValue:d[@"passing_rate"]];
                            NSString *quiz_stage_name = [self stringValue:d[@"quiz_stage_name"]];
                            NSString *item_count = [self stringValue:d[@"item_count"]];
                            NSString *date_created = [self stringValue:d[@"date_created"]];
                            NSString *formatted_date_created = [self stringValue:d[@"formatted_date_created"]];
                            
                            total_score = [self formatStringNumber:total_score];
                            passing_rate = [self formatStringNumber:passing_rate];
                            
                            NSManagedObject *mo = [self getEntity:kCourseTestListEntity attribute:@"id" parameter:test_id context:ctx];
                            
                            NSString *sort_date_created_string = [self translateDate:date_created from:@"UTC" to:@"GMT"];
                            NSDate *sort_date_created = [self parseDateFromString:sort_date_created_string];
                            
                            [mo setValue:test_id forKey:@"id"];
                            [mo setValue:name forKey:@"name"];
                            [mo setValue:general_instruction forKey:@"general_instruction"];
                            [mo setValue:quiz_settings_id forKey:@"quiz_settings_id"];
                            [mo setValue:description forKey:@"test_description"];
                            [mo setValue:quiz_category_id forKey:@"quiz_category_id"];
                            [mo setValue:quiz_category_name forKey:@"quiz_category_name"];
                            [mo setValue:difficulty_level_id forKey:@"difficulty_level_id"];
                            [mo setValue:date_open forKey:@"date_open"];
                            [mo setValue:formatted_date_open forKey:@"formatted_date_open"];
                            [mo setValue:date_close forKey:@"date_close"];
                            [mo setValue:formatted_date_close forKey:@"formatted_date_close"];
                            [mo setValue:attempts forKey:@"attempts"];
                            [mo setValue:quiz_shuffling_mode forKey:@"quiz_shuffling_mode"];
                            [mo setValue:is_shuffle_answers forKey:@"is_shuffle_answers"];
                            [mo setValue:total_score forKey:@"total_score"];
                            [mo setValue:time_limit forKey:@"time_limit"];
                            [mo setValue:is_forced_complete forKey:@"is_forced_complete"];
                            [mo setValue:is_graded forKey:@"is_graded"];
                            [mo setValue:password forKey:@"password"];
                            [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
                            [mo setValue:allow_review forKey:@"allow_review"];
                            [mo setValue:passing_rate forKey:@"passing_rate"];
                            [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
                            [mo setValue:item_count forKey:@"item_count"];
                            [mo setValue:date_created forKey:@"date_created"];
                            [mo setValue:formatted_date_created forKey:@"formatted_date_created"];
                            [mo setValue:sort_date_created forKey:@"sort_date_created"];
                            
                            NSString *search_string = [NSString stringWithFormat:@"%@%@%@%@", name, test_id, description, general_instruction];
                            [mo setValue:search_string forKey:@"search_string"];
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                }
                
                if (dataBlock) {
                    dataBlock(pagination);
                }
            }
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
}

//- (void)submitAnswers:(TestGuruDataBlock)dataBlock {
//
////    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
////    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
//
////    NSString *test_id = @"92";
//    NSString *test_id = [self fetchObjectForKey:kCP_SELECTED_QUIZ_ID];
//
//    NSDictionary *postBody = [self createSubmitAnswersBody];
//    NSLog(@"POST BODY ~~~~>%@<~~~~~", postBody);
//
//    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCoursePlayerSubmitAnswers, test_id] ];
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
//    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
//                                  {
//                                      if (error) {
//                                          NSLog(@"error %@", [error localizedDescription]);
//              if (dataBlock) {
//                  dataBlock(nil);
//                                          }
//                                      }
//
//                                      if (!error) {
//                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
//                                          NSLog(@"ITO YUNG SUBMIT RESPONSE [%@]", dictionary);
//
//              if (dictionary != nil) {
//
//                  NSDictionary *meta = dictionary[@"_meta"];
//                  NSString *status = meta[@"status"];
//
//                  if ([status isEqualToString:@"SUCCESS"]) {
//
//                      NSDictionary *records = dictionary[@"records"];
//                      NSDictionary *test_results = records[@"test_result"];
//
//                      NSString *total_score = [self stringValue:test_results[@"total_score"]];
//                      NSString *general_score = [self stringValue:test_results[@"general_score"]];
//                      NSString *passing_score = [self stringValue:test_results[@"passing_score"]];
//                      NSString *no_of_correct_answers = [self stringValue:test_results[@"no_of_correct_answers"]];
//                      NSString *no_of_answered_questions = [self stringValue:test_results[@"no_of_answered_questions"]];
//                      NSString *no_of_not_answered = [self stringValue:test_results[@"no_of_not_answered"]];
//                      NSString *no_of_questions = [self stringValue:test_results[@"no_of_questions"]];
//                      NSString *no_of_unchecked = [self stringValue:test_results[@"no_of_unchecked"]];
//
//                      total_score = [self formatStringNumber:total_score];
//                      general_score = [self formatStringNumber:general_score];
//
//                      NSDictionary *data = @{
//                                             @"total_score":total_score,
//                                             @"general_score":general_score,
//                                             @"passing_score":passing_score,
//                                             @"no_of_correct_answers":no_of_correct_answers,
//                                             @"no_of_answered_questions":no_of_answered_questions,
//                                             @"no_of_not_answered":no_of_not_answered,
//                                             @"no_of_questions":no_of_questions,
//                                             @"no_of_unchecked":no_of_unchecked
//                                             };
//
//                      if (dataBlock) {
//                          dataBlock(data);
//                      }
//
//                  } else {
//                      if (dataBlock) {
//                          dataBlock(nil);
//                      }
//                  }
//              }
//                                      }
//                                  }];
//    [task resume];
//}
//
//- (NSDictionary *)createSubmitAnswersBody {
//
//    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
//    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
//
////    NSString *cs_id = @"2";//TODO MAKE DYNAMIC
//    NSString *cs_id = [self fetchObjectForKey:kCP_SELECTED_CS_ID];
//
//    NSArray *answers = [self answerArray];
//
//    NSDictionary *postBody = @{
//                               @"user_id":user_id,
//                               @"cs_id":cs_id,
//                               @"answers":answers,
//                               @"is_finish":@"1"
//                               };
//
//    return postBody;
//}
//
//- (NSArray *)answerArray {
//
//    NSArray *q_array = [self getObjectsForEntity:kCoursePlayerQuestionListEntity predicate:nil];
//
//
//    NSMutableArray *answerArray = [NSMutableArray array];
//
//    for (NSManagedObject *q_mo in q_array) {
//
//        NSDictionary *answerDict = [NSDictionary dictionary];
//
//        NSString *question_type_id = [q_mo valueForKey:@"question_type_id"];
//        NSString *question_id = [q_mo valueForKey:@"question_id"];
//        NSString *question_points = [q_mo valueForKey:@"points"];
//
//        NSPredicate *predicate = [self predicateForKeyPath:@"question_id" object:question_id];
//        NSArray *c_array = [self getObjectsForEntity:kCoursePlayerQuestionChoiceEntity predicate:predicate];
//
//        NSString *text = @"";
//
//        // ESSAY/SHORT BODY
//        if ([question_type_id isEqual:@"6"] || [question_type_id isEqual:@"9"]) {
//            NSManagedObject *c_mo = [c_array lastObject];
//
//            text = [c_mo valueForKey:@"text"];
//        }
//
//        // MULTIPLE/TOF BODY
//        else {
//
//            NSMutableArray *choices_array = [NSMutableArray array];
//
//
//            for (NSManagedObject *c_mo in c_array) {
//
//                NSString *choice_id = [c_mo valueForKey:@"id"];
//                NSString *question_id = [c_mo valueForKey:@"question_id"];
//                NSString *text = [c_mo valueForKey:@"text"];
//                NSString *suggestive_feedback = [c_mo valueForKey:@"suggestive_feedback"];
//                NSString *is_correct = [c_mo valueForKey:@"is_correct"];
//                NSString *order_number = [c_mo valueForKey:@"order_number"];
//                NSString *is_deleted = [c_mo valueForKey:@"is_deleted"];
//                NSString *date_created = [c_mo valueForKey:@"date_created"];
//                NSString *date_modified = [c_mo valueForKey:@"date_modified"];
//
//                NSString *answer = [c_mo valueForKey:@"answer"];
//
//                NSString *user_choice = ([answer isEqualToString:@"1"]) ? @"1" : @"0";
//
//
//                NSDictionary *cDict = @{
//                                        @"id":choice_id,
//                                        @"question_id":question_id,
//                                        @"text":text,
//                                        @"suggestive_feedback":suggestive_feedback,
//                                        @"is_correct":is_correct,
//                                        @"order_number":order_number,
//                                        @"is_deleted":is_deleted,
//                                        @"date_created": date_created,
//                                        @"date_modified": date_modified,
//                                        @"user_choice":user_choice
//                                        };
//                [choices_array addObject:cDict];
//            }
//
//
//            NSError *jsonError = nil;
//            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:choices_array options:NSJSONWritingPrettyPrinted error:&jsonError];
//            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//
//            text = jsonString;
//        }
//
//        answerDict = @{
//                       @"question_id" : question_id,
//                       @"question_answer_text":text,
//                       @"score":question_points,
//                       @"time_spent" : @"00:00:00"
//                       };
//
//        [answerArray addObject:answerDict];
//    }
//    return answerArray;
//}


- (void)requestShowResult:(TestGuruDataBlock)dataBlock {
    
    NSString *user_id = [self loginUser];
    NSString *quiz_id = [self fetchObjectForKey:kCP_SELECTED_QUIZ_ID];
    NSString *cs_id = [self fetchObjectForKey:kCP_SELECTED_CS_ID];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCoursePlayerShowResults, quiz_id, user_id, cs_id] ];
    NSLog(@"XXX request show result: %@", url.absoluteString);
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          if (dataBlock) {
                                              dataBlock(nil);
                                          }
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
//                                          NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                                          
                                          if (dictionary[@"records"] != nil ) {
                                              NSDictionary *records = dictionary[@"records"];
                                              NSDictionary *d = records[@"data"];
                                              NSDictionary *test_result = [self processTestResult:records[@"test_result"]];
                                              
                                              NSString *status = records[@"status"];
                                              
                                              if ([status isEqualToString:@"ERROR"]) {
                                                  NSString *errorMessage = records[@"message"];
                                                  if (dataBlock) {
                                                      dataBlock(@{@"error":errorMessage});
                                                  }
                                                  return;
                                              }
                                              
                                              NSManagedObjectContext *ctx = self.workerContext;
                                              
                                              [self clearDataForEntity:kCoursePlayerTestEntity withPredicate:nil context:ctx];
                                              [self clearDataForEntity:kCoursePlayerQuestionListEntity withPredicate:nil context:ctx];
                                              [self clearDataForEntity:kCoursePlayerQuestionChoiceEntity withPredicate:nil context:ctx];
                                              
                                              NSString *test_id = [self stringValue:d[@"id"]];
                                              NSManagedObject *mo = [self getEntity:kCoursePlayerTestEntity attribute:@"id" parameter:test_id context:ctx];
                                              
                                              [ctx performBlock:^{
                                                  //                                                  NSString *time_remaining = [self stringValue:records[@"time_remaining"]];
                                                  NSString *quiz_settings_id = [self stringValue:d[@"quiz_settings_id"]];
                                                  NSString *name = [self stringValue:d[@"name"]];
                                                  NSString *description = [self stringValue:d[@"description"]];
                                                  NSString *general_instruction = [self stringValue:d[@"general_instruction"]];
                                                  NSString *quiz_category_id = [self stringValue:d[@"quiz_category_id"]];
                                                  NSString *quiz_type_id = [self stringValue:d[@"quiz_type_id"]];
                                                  NSString *quiz_result_type_id = [self stringValue:d[@"quiz_result_type_id"]];
                                                  NSString *learning_skills_id = [self stringValue:d[@"learning_skills_id"]];
                                                  NSString *difficulty_level_id = [self stringValue:d[@"difficulty_level_id"]];
                                                  NSString *date_open = [self stringValue:d[@"date_open"]];
                                                  NSString *date_close = [self stringValue:d[@"date_close"]];
                                                  NSString *attempts = [self stringValue:d[@"attempts"]];
                                                  NSString *quiz_shuffling_mode = [self stringValue:d[@"quiz_shuffling_mode"]];
                                                  NSString *is_shuffle_answers = [self stringValue:d[@"is_shuffle_answers"]];
                                                  NSString *total_score = [self stringValue:d[@"total_score"]];
                                                  NSString *time_limit = [self stringValue:d[@"time_limit"]];
                                                  NSString *is_forced_complete = [self stringValue:d[@"is_forced_complete"]];
                                                  NSString *password = [self stringValue:d[@"password"]];
                                                  NSString *quiz_stage_id = [self stringValue:d[@"quiz_stage_id"]];
                                                  NSString *show_feedbacks = [self stringValue:d[@"show_feedbacks"]];
                                                  NSString *allow_review = [self stringValue:d[@"allow_review"]];
                                                  NSString *passing_rate = [self stringValue:d[@"passing_rate"]];
                                                  NSString *quiz_category_name = [self stringValue:d[@"quiz_category_name"]];
                                                  NSString *question_type_name = [self stringValue:d[@"question_type_name"]];
                                                  NSString *learning_skills_name = [self stringValue:d[@"learning_skills_name"]];
                                                  NSString *difficulty_level_name = [self stringValue:d[@"difficulty_level_name"]];
                                                  NSString *quiz_stage_name = [self stringValue:d[@"quiz_stage_name"]];
                                                  NSString *attempts_count = [self stringValue:records[@"attempts_count"]];
                                                  
                                                  NSString *show_score = [self stringValue:d[@"show_score"]];
                                                  NSString *show_result = [self stringValue:d[@"show_result"]];
                                                  NSString *show_correct_answer = [self stringValue:d[@"show_correct_answer"]];
                                                  
                                                  NSInteger attemptValue = [attempts integerValue] - [attempts_count integerValue];
                                                  attempts = [NSString stringWithFormat:@"%zd", attemptValue];
                                                  
                                                  id attempt = d[@"attempt"];
                                                  
                                                  if ([attempt isKindOfClass:[NSArray class]]) {
                                                      // do something// no use yet
                                                  } else {
                                                      // do something// no use yet
                                                  }
                                                  
                                                  [mo setValue:test_id forKey:@"id"];
                                                  [mo setValue:quiz_settings_id forKey:@"quiz_settings_id"];
                                                  [mo setValue:name forKey:@"name"];
                                                  [mo setValue:description forKey:@"quiz_description"];
                                                  [mo setValue:general_instruction forKey:@"general_instruction"];
                                                  [mo setValue:quiz_category_id forKey:@"quiz_category_id"];
                                                  [mo setValue:quiz_type_id forKey:@"quiz_type_id"];
                                                  [mo setValue:quiz_result_type_id forKey:@"quiz_result_type_id"];
                                                  [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
                                                  [mo setValue:difficulty_level_id forKey:@"difficulty_level_id"];
                                                  [mo setValue:date_open forKey:@"date_open"];
                                                  [mo setValue:date_close forKey:@"date_close"];
                                                  [mo setValue:attempts forKey:@"attempts"];
                                                  [mo setValue:quiz_shuffling_mode forKey:@"quiz_shuffling_mode"];
                                                  [mo setValue:is_shuffle_answers forKey:@"is_shuffle_answers"];
                                                  [mo setValue:total_score forKey:@"total_score"];
                                                  [mo setValue:time_limit forKey:@"time_limit"];
                                                  [mo setValue:is_forced_complete forKey:@"is_forced_complete"];
                                                  [mo setValue:password forKey:@"password"];
                                                  [mo setValue:quiz_stage_id forKey:@"quiz_stage_id"];
                                                  [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
                                                  [mo setValue:allow_review forKey:@"allow_review"];
                                                  [mo setValue:passing_rate forKey:@"passing_rate"];
                                                  [mo setValue:quiz_category_name forKey:@"quiz_category_name"];
                                                  [mo setValue:question_type_name forKey:@"question_type_name"];
                                                  [mo setValue:learning_skills_name forKey:@"learning_skills_name"];
                                                  [mo setValue:difficulty_level_name forKey:@"difficulty_level_name"];
                                                  [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
                                                  [mo setValue:attempts_count forKey:@"attempts_count"];
                                                  [mo setValue:show_score forKey:@"show_score"];
                                                  [mo setValue:show_result forKey:@"show_result"];
                                                  [mo setValue:show_correct_answer forKey:@"show_correct_answer"];
                                                  
                                                  NSArray *q_array = records[@"questions"];
                                                  NSMutableSet *q_sets = [NSMutableSet set];
                                                  
                                                  if (q_array.count > 0) {
                                                      q_sets = [self processQuestionArray:q_array show_feedbacks:show_feedbacks];
                                                      
                                                      NSLog(@"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
                                                      [self testingQuestion:(NSMutableSet *)q_sets];
                                                  } else {
                                                      if (dataBlock) {
                                                          dataBlock(@{@"error":@"No available questions"});
                                                      }
                                                      return;
                                                  }
                                                  
                                                  [mo setValue:[NSSet setWithSet:q_sets] forKey:@"questions"];
                                                  
                                                  [self saveTreeContext:ctx];
                                                  
                                                  NSSet *customQuestions = [mo valueForKey:@"questions"];
                                                  
                                                  NSLog(@"RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");
                                                  [self testingQuestion:(NSMutableSet *)customQuestions];
                                                  
                                                  if (dataBlock) {
                                                      dataBlock(@{@"mo": mo,
                                                                  @"test_result": test_result});
                                                  }
                                              }];
                                          }
                                      }//end
                                  }];
    [task resume];
}

- (void)testingQuestion:(NSMutableSet *)questionSet {
    
    NSArray *qlist = [questionSet allObjects];
    for (NSManagedObject *q in qlist) {
        NSLog(@"ITO NA A FINAL TEST");
        NSSet *c = [q valueForKey:@"choices"];
        [self testingChoices:(NSMutableSet *)c];
    }
}

- (void)testingChoices:(NSMutableSet *)choiceSet {
    
    NSArray *testItems = [choiceSet allObjects];
    NSLog(@"----------------CHOICE ITEM-----------------");
    
    for (NSManagedObject *m in testItems) {
        
        NSLog(@">>>>>>>>>>>>>>>>>>>>>>>>");
        
        NSString *question_id = [m valueForKey:@"question_id"];
        NSString *question_type_id = [m valueForKey:@"question_type_id"];
        NSString *text = [m valueForKey:@"text"];
        NSLog(@"question id : %@", question_id);
        NSLog(@"question type id : %@", question_type_id);
        NSLog(@"text value: %@", text);
    }
}

- (NSMutableSet *)processQuestionArray:(NSArray *)q_array show_feedbacks:(NSString *)show_feedbacks {
    
    NSMutableSet *q_sets = [NSMutableSet set];
    
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    NSInteger index = 0;
    for (NSDictionary *q_dict in q_array) {
        
        NSString *question_id = [self stringValue:q_dict[@"question_id"]];
        NSString *quiz_id = [self stringValue:q_dict[@"quiz_id"]];
        NSString *question_type_id = [self stringValue:q_dict[@"question_type_id"]];
        NSString *question_type_name = [self stringValue:q_dict[@"question_type_name"]];
        NSString *question_text = [self stringValue:q_dict[@"question_text"]];
        NSString *points = [self stringValue:q_dict[@"points"]];
        NSString *is_case_sensitive = [self stringValue:q_dict[@"is_case_sensitive"]];
        
        if ([points containsString:@"null"]) {
            points = @"0";
        }
        
        points = [self formatStringNumber:points];
        NSString *image_one = @"";
        NSString *image_two = @"";
        NSString *image_three = @"";
        NSString *image_four = @"";
        
        NSArray *q_images = q_dict[@"question_images"];
        
        for (NSDictionary *imageDict in q_images) {
            NSString *image_index = [self stringValue:imageDict[@"index"]];
            NSString *image = [self stringValue:imageDict[@"image"]];
            
            if (image.length > 0) {
                if ([image_index isEqual:@"1"]) {
                    image_one = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
                }
                if ([image_index isEqual:@"2"]) {
                    image_two = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
                }
                if ([image_index isEqual:@"3"]) {
                    image_three = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
                }
                if ([image_index isEqual:@"4"]) {
                    image_four = [NSString stringWithFormat:@"%@/%@", homeurl, image ];
                }
            }
        }
        
        NSString *general_feedback = @"";
        NSString *correct_answer_feedback = @"";
        NSString *wrong_answer_feedback = @"";
        
        BOOL feedbacks_is_array = [self isArrayObject:q_dict[@"feedbacks"]];
        if (feedbacks_is_array) {
            NSArray *feedbacksArray = q_dict[@"feedbacks"];
            NSDictionary *feedbacks = feedbacksArray.lastObject;
            
            general_feedback = feedbacks[@"general_feedback"];
            correct_answer_feedback = feedbacks[@"correct_answer_feedback"];
            wrong_answer_feedback = feedbacks[@"wrong_answer_feedback"];
        }
        
        NSManagedObjectContext *question_ctx = self.workerContext;
//        NSManagedObject *q_mo = [self getEntity:kCoursePlayerQuestionListEntity attribute:@"question_id" parameter:question_id context:question_ctx];
        NSManagedObject *q_mo = [NSEntityDescription insertNewObjectForEntityForName:kCoursePlayerQuestionListEntity
                                                              inManagedObjectContext:question_ctx];
        
        [q_mo setValue:question_id forKey:@"question_id"];
        [q_mo setValue:quiz_id forKey:@"quiz_id"];
        [q_mo setValue:question_type_id forKey:@"question_type_id"];
        [q_mo setValue:question_type_name forKey:@"question_type_name"];
        [q_mo setValue:question_text forKey:@"question_text"];
        [q_mo setValue:points forKey:@"points"];
        [q_mo setValue:is_case_sensitive forKey:@"is_case_sensitive"];
        
        [q_mo setValue:general_feedback forKey:@"general_feedback"];
        [q_mo setValue:correct_answer_feedback forKey:@"correct_answer_feedback"];
        [q_mo setValue:wrong_answer_feedback forKey:@"wrong_answer_feedback"];
        
        [q_mo setValue:image_one forKey:@"image_one"];
        [q_mo setValue:image_two forKey:@"image_two"];
        [q_mo setValue:image_three forKey:@"image_three"];
        [q_mo setValue:image_four forKey:@"image_four"];
        
        
        // PREPARED FOR Q_CHOICES DONT DELETE
        NSMutableSet *c_sets = [NSMutableSet set];
        c_sets = [self processChoices:q_dict showFeedbacks:show_feedbacks];

        [q_mo setValue:[NSSet setWithSet:c_sets] forKey:@"choices"];
        
        NSLog(@"BAKIT DITO COMPLETO ABANGAN...");
        NSSet *itoYungSaved = [q_mo valueForKey:@"choices"];
        [self testingChoices:(NSMutableSet *)itoYungSaved];
        
        [q_mo setValue:@(index) forKey:@"index"];
        [q_sets addObject:q_mo];
        
        index ++;
    }
    
    return q_sets;
}

- (NSMutableSet *)processChoices:(NSDictionary *)q_dict showFeedbacks:(NSString *)show_feedbacks {
    
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    
    NSString *question_type_id = [self stringValue:q_dict[@"question_type_id"]];
    NSString *question_id = [self stringValue:q_dict[@"question_id"]];
    
    NSMutableSet *c_sets = [NSMutableSet set];
    NSManagedObjectContext *choice_ctx = self.workerContext;
    
    // FOR ESSAY AND SHORT ANSWER
    if ([question_type_id isEqual:@"6"] || [question_type_id isEqual:@"9"]) {
        NSManagedObject *c_mo = [self getEntity:kCoursePlayerQuestionChoiceEntity attribute:@"id" parameter:question_id context:choice_ctx];
        
        NSString *question_choices = [self stringValue:q_dict[@"question_choices"]];
        
        [c_mo setValue:question_type_id forKey:@"question_type_id"];
        [c_mo setValue:@"0" forKey:@"id"];
        [c_mo setValue:question_id forKey:@"question_id"];
        [c_mo setValue:question_choices forKey:@"text"];
        [c_mo setValue:@"" forKey:@"suggestive_feedback"];
        [c_mo setValue:@"100" forKey:@"is_correct"];
        [c_mo setValue:@"0" forKey:@"order_number"];
        [c_mo setValue:@"0" forKey:@"is_deleted"];
        [c_mo setValue:[self stringDate:[NSDate date]] forKey:@"date_created"];
        [c_mo setValue:[self stringDate:[NSDate date]] forKey:@"date_modified"];
        [c_mo setValue:@"0" forKey:@"answer"];
        
        // additional ordernumber
        [c_mo setValue:@(1) forKey:@"order_number_int"];
        
        [c_sets addObject:c_mo];
        
    } else if ([question_type_id isEqual:@"10"] || [question_type_id isEqual:@"2"])  {
        NSManagedObject *c_mo = [self getEntity:kCoursePlayerQuestionChoiceEntity attribute:@"id" parameter:question_id context:choice_ctx];
        
        NSString *question_choices = @"";
        id question_choices_array = q_dict[@"question_choices"];
        
        if ([question_choices_array isKindOfClass:[NSArray class]]) {
            NSDictionary *dict = (NSDictionary *)[question_choices_array lastObject];
            NSString *text = dict[@"text"];
            question_choices = text;
        } else {
            question_choices = [self stringValue:q_dict[@"question_choices"]];// student's answer
        }
        
        NSString *correct_answer = [self stringValue:q_dict[@"correct_answer"]];// correct answer
        
        NSString *is_correct = @"0";
        
        NSString *is_case_sensitive = [self stringValue:q_dict[@"is_case_sensitive"]];
        if ([is_case_sensitive isEqualToString:@"0"]) {
            NSString *lower_student_answer = [question_choices lowercaseString];
            NSString *lower_correct_answer = [correct_answer lowercaseString];
            NSArray *lower_correct_answer_array = [lower_correct_answer componentsSeparatedByString:@","];
            
            if ([lower_correct_answer_array containsObject:lower_student_answer]) {
                is_correct = @"100";
            }
        } else if ([is_case_sensitive isEqualToString:@"1"]) {
            NSArray *correct_answer_array = [correct_answer componentsSeparatedByString:@","];
            if ([correct_answer_array containsObject:question_choices]) {
                is_correct = @"100";
            }
        }
        
        [c_mo setValue:question_type_id forKey:@"question_type_id"];
        [c_mo setValue:@"0" forKey:@"id"];
        [c_mo setValue:question_id forKey:@"question_id"];
        [c_mo setValue:correct_answer forKey:@"text"];
        [c_mo setValue:@"" forKey:@"suggestive_feedback"];
        [c_mo setValue:is_correct forKey:@"is_correct"];
        [c_mo setValue:@"0" forKey:@"order_number"];
        [c_mo setValue:@"0" forKey:@"is_deleted"];
        [c_mo setValue:[self stringDate:[NSDate date]] forKey:@"date_created"];
        [c_mo setValue:[self stringDate:[NSDate date]] forKey:@"date_modified"];
        [c_mo setValue:question_choices forKey:@"answer"];
        
        // additional ordernumber
        [c_mo setValue:@(1) forKey:@"order_number_int"];
        
        [c_sets addObject:c_mo];
        
    } else if ( [question_type_id isEqual:@"3"] )  {
        //MULTIPLE CHOICE
        
        [self processMultipleChoiceForQuestion:question_id
                                  questionType:question_type_id
                                      feedback:show_feedbacks
                                       choices:q_dict
                                          home:homeurl
                                   questionSet:c_sets
                                       context:choice_ctx];

    } else if ( [question_type_id isEqual:@"1"] ) {
        // TRUE OR FALSE
        
        [self processTrueOrFalseForQuestion:question_id
                               questionType:question_type_id
                                   feedback:show_feedbacks
                                    choices:q_dict
                                       home:homeurl
                                questionSet:c_sets
                                    context:choice_ctx];
        
        
    } else { // FOR TRUE OR FALSE AND MULTIPLE CHOICE
        
//        NSArray *q_choices = q_dict[@"question_choices"];
//        
//        for (NSDictionary *c_dict in q_choices) {
//            
//            NSString *choice_id = [self stringValue:c_dict[@"id"]];
//            NSString *question_id = [self stringValue:c_dict[@"question_id"]];
//            NSString *text = [self stringValue:c_dict[@"text"]];
//            NSString *suggestive_feedback = [self stringValue:c_dict[@"suggestive_feedback"]];
//            NSString *is_correct = [self stringValue:c_dict[@"is_correct"]];
//            NSString *question_choice_image = [self stringValue:c_dict[@"question_choice_image"]];
//            NSString *order_number = [self stringValue:c_dict[@"order_number"]];
//            NSString *is_deleted = [self stringValue:c_dict[@"is_deleted"]];
//            NSString *date_created = [self stringValue:c_dict[@"date_created"]];
//            NSString *date_modified = [self stringValue:c_dict[@"date_modified"]];
//            
//            NSString *user_choice = [self stringValue:c_dict[@"user_choice"]];
//            
//            if ([is_correct isEqualToString:@"1"]) {
//                is_correct = @"100";
//            }
//            
//            NSString *choice_image = @"";
//            if (question_choice_image.length > 0) {
//                if (![question_choice_image  isEqual: @"null"]) {
//                    choice_image = [NSString stringWithFormat:@"%@/%@", homeurl, question_choice_image];
//                }
//            }
//            
//            NSManagedObject *c_mo = [self getEntity:kCoursePlayerQuestionChoiceEntity attribute:@"id" parameter:choice_id context:choice_ctx];//[NSEntityDescription insertNewObjectForEntityForName:kCoursePlayerQuestionChoiceEntity inManagedObjectContext:choice_ctx];
//            
//            [c_mo setValue:question_type_id forKey:@"question_type_id"];
//            [c_mo setValue:choice_id forKey:@"id"];
//            [c_mo setValue:question_id forKey:@"question_id"];
//            [c_mo setValue:text forKey:@"text"];
//            [c_mo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
//            [c_mo setValue:is_correct forKey:@"is_correct"];
//            [c_mo setValue:choice_image forKey:@"question_choice_image"];
//            [c_mo setValue:order_number forKey:@"order_number"];
//            [c_mo setValue:is_deleted forKey:@"is_deleted"];
//            [c_mo setValue:date_created forKey:@"date_created"];
//            [c_mo setValue:date_modified forKey:@"date_modified"];
//            
//            NSString *answer = ([user_choice isEqualToString:@"1"]) ? @"1" : @"0";
//            
//            // additional ordernumber
//            [c_mo setValue:@(order_number.integerValue) forKey:@"order_number_int"];
//            
//            [c_mo setValue:answer forKey:@"answer"];
//            
//            [c_mo setValue:user_choice forKey:@"user_choice"];
//            
//            [c_sets addObject:c_mo];
//        }
//        
//        // FOR SHOW FEEDBACK
//        if ([show_feedbacks isEqualToString:@"1"]) {
//            NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:kCoursePlayerQuestionChoiceEntity inManagedObjectContext:choice_ctx];
//            
//            NSString *general_feedback = @"";
//            NSString *correct_answer_feedback = @"";
//            NSString *wrong_answer_feedback = @"";
//            
//            BOOL feedbacks_is_array = [self isArrayObject:q_dict[@"feedbacks"]];
//            if (feedbacks_is_array) {
//                NSArray *feedbacksArray = q_dict[@"feedbacks"];
//                NSDictionary *feedbacks = feedbacksArray.lastObject;
//                
//                general_feedback = feedbacks[@"general_feedback"];
//                correct_answer_feedback = feedbacks[@"correct_answer_feedback"];
//                wrong_answer_feedback = feedbacks[@"wrong_answer_feedback"];
//            }
//            
//            NSPredicate *predicate_qid = [self predicateForKeyPath:@"question_id" object:question_id];
//            NSPredicate *predicate_uc = [self predicateForKeyPath:@"user_choice" object:@"1"];
//            NSPredicate *predicate_ic = [self predicateForKeyPath:@"is_correct" object:@"100"];
//            NSPredicate *c_predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate_qid, predicate_uc, predicate_ic]];
//            
//            NSArray *choices = [self getObjectsForEntity:kCoursePlayerQuestionChoiceEntity predicate:c_predicate];
//            
//            NSString *feedback = wrong_answer_feedback;
//            
//            if (choices.count > 0) {
//                feedback = correct_answer_feedback;
//            }
//            
//            NSString *uuid = [NSString stringWithFormat:@"%@-%@-%@",
//                              [Utils randomNumber],[Utils randomNumber],[Utils randomNumber]];
//            
//            [c_mo setValue:uuid forKey:@"id"];
//            [c_mo setValue:question_id forKey:@"question_id"];
//            [c_mo setValue:feedback forKey:@"text"];
//            [c_mo setValue:@"feedback" forKey:@"suggestive_feedback"];// FOR FEEDBACK ONLY
//            [c_mo setValue:@"100" forKey:@"is_correct"];
//            //                              [c_mo setValue:question_choice_image forKey:@"question_choice_image"];
//            [c_mo setValue:@"1000" forKey:@"order_number"];
//            [c_mo setValue:@"0" forKey:@"is_deleted"];
//            [c_mo setValue:[self stringDate:[NSDate date]] forKey:@"date_created"];
//            [c_mo setValue:[self stringDate:[NSDate date]] forKey:@"date_modified"];
//            [c_mo setValue:@"0" forKey:@"answer"];
//            
//            // additional ordernumber
//            [c_mo setValue:@(1000) forKey:@"order_number_int"];
//            
//            [c_sets addObject:c_mo];
//        }
        
    }
    return c_sets;

}

- (void)processMultipleChoiceForQuestion:(NSString *)question_id
                            questionType:(NSString *)question_type_id
                                feedback:(NSString *)show_feedbacks
                                 choices:(NSDictionary *)q_dict
                                    home:(NSString *)homeurl
                             questionSet:(NSMutableSet *)c_sets
                                 context:(NSManagedObjectContext *)choice_ctx {
    
    NSLog(@"SKAGEN MULTIPLE CHOICE --- 0 : %s", __PRETTY_FUNCTION__);
    
    NSArray *q_choices = q_dict[@"question_choices"];
    
//    NSLog(@"question choices : %@", q_choices);
    
    for (NSDictionary *c_dict in q_choices) {
        
        NSString *choice_id = [self stringValue:c_dict[@"id"]];
        NSString *question_id = [self stringValue:c_dict[@"question_id"]];
        NSString *text = [self stringValue:c_dict[@"text"]];
        NSString *suggestive_feedback = [self stringValue:c_dict[@"suggestive_feedback"]];
        NSString *is_correct = [self stringValue:c_dict[@"is_correct"]];
        NSString *question_choice_image = [self stringValue:c_dict[@"question_choice_image"]];
        NSString *order_number = [self stringValue:c_dict[@"order_number"]];
        NSString *is_deleted = [self stringValue:c_dict[@"is_deleted"]];
        NSString *date_created = [self stringValue:c_dict[@"date_created"]];
        NSString *date_modified = [self stringValue:c_dict[@"date_modified"]];
        
        NSString *user_choice = [self stringValue:c_dict[@"user_choice"]];
        
        if ([is_correct isEqualToString:@"1"]) {
            is_correct = @"100";
        }
        
        NSString *choice_image = @"";
        if (question_choice_image.length > 0) {
            if (![question_choice_image  isEqual: @"null"]) {
                choice_image = [NSString stringWithFormat:@"%@/%@", homeurl, question_choice_image];
            }
        }
        
        NSManagedObject *c_mo = [self getEntity:kCoursePlayerQuestionChoiceEntity attribute:@"id" parameter:choice_id context:choice_ctx];//
        
        [c_mo setValue:question_type_id forKey:@"question_type_id"];
        [c_mo setValue:choice_id forKey:@"id"];
        [c_mo setValue:question_id forKey:@"question_id"];
        [c_mo setValue:text forKey:@"text"];
        [c_mo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
        [c_mo setValue:is_correct forKey:@"is_correct"];
        [c_mo setValue:choice_image forKey:@"question_choice_image"];
        [c_mo setValue:order_number forKey:@"order_number"];
        [c_mo setValue:is_deleted forKey:@"is_deleted"];
        [c_mo setValue:date_created forKey:@"date_created"];
        [c_mo setValue:date_modified forKey:@"date_modified"];
        
        NSString *answer = ([user_choice isEqualToString:@"1"]) ? @"1" : @"0";
        
        // additional ordernumber
        [c_mo setValue:@(order_number.integerValue) forKey:@"order_number_int"];
        
        [c_mo setValue:answer forKey:@"answer"];
        
        [c_mo setValue:user_choice forKey:@"user_choice"];
        
        [c_sets addObject:c_mo];
    }
    
    // FOR SHOW FEEDBACK
    if ([show_feedbacks isEqualToString:@"1"]) {
        NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:kCoursePlayerQuestionChoiceEntity inManagedObjectContext:choice_ctx];
        
        NSString *general_feedback = @"";
        NSString *correct_answer_feedback = @"";
        NSString *wrong_answer_feedback = @"";
        
        BOOL feedbacks_is_array = [self isArrayObject:q_dict[@"feedbacks"]];
        if (feedbacks_is_array) {
            NSArray *feedbacksArray = q_dict[@"feedbacks"];
            NSDictionary *feedbacks = feedbacksArray.lastObject;
            
            general_feedback = feedbacks[@"general_feedback"];
            correct_answer_feedback = feedbacks[@"correct_answer_feedback"];
            wrong_answer_feedback = feedbacks[@"wrong_answer_feedback"];
        }
        
        NSPredicate *predicate_qid = [self predicateForKeyPath:@"question_id" object:question_id];
        NSPredicate *predicate_uc = [self predicateForKeyPath:@"user_choice" object:@"1"];
        NSPredicate *predicate_ic = [self predicateForKeyPath:@"is_correct" object:@"100"];
        NSPredicate *c_predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate_qid, predicate_uc, predicate_ic]];
        
        NSArray *choices = [self getObjectsForEntity:kCoursePlayerQuestionChoiceEntity predicate:c_predicate];
        
        NSString *feedback = wrong_answer_feedback;
        
        if (choices.count > 0) {
            feedback = correct_answer_feedback;
        }
        
        NSString *uuid = [NSString stringWithFormat:@"%@-%@-%@",
                          [Utils randomNumber],[Utils randomNumber],[Utils randomNumber]];
        
        [c_mo setValue:uuid forKey:@"id"];
        [c_mo setValue:question_id forKey:@"question_id"];
        [c_mo setValue:feedback forKey:@"text"];
        [c_mo setValue:@"feedback" forKey:@"suggestive_feedback"];// FOR FEEDBACK ONLY
        [c_mo setValue:@"100" forKey:@"is_correct"];
        //                              [c_mo setValue:question_choice_image forKey:@"question_choice_image"];
        [c_mo setValue:@"1000" forKey:@"order_number"];
        [c_mo setValue:@"0" forKey:@"is_deleted"];
        [c_mo setValue:[self stringDate:[NSDate date]] forKey:@"date_created"];
        [c_mo setValue:[self stringDate:[NSDate date]] forKey:@"date_modified"];
        [c_mo setValue:@"0" forKey:@"answer"];
        
        // additional ordernumber
        [c_mo setValue:@(1000) forKey:@"order_number_int"];
        
        [c_sets addObject:c_mo];
    }
}

- (void)processTrueOrFalseForQuestion:(NSString *)question_id
                         questionType:(NSString *)question_type_id
                             feedback:(NSString *)show_feedbacks
                                 choices:(NSDictionary *)q_dict
                                 home:(NSString *)homeurl
                          questionSet:(NSMutableSet *)c_sets
                              context:(NSManagedObjectContext *)choice_ctx {
    
    NSLog(@"SKAGEN TRUE OR FALSE --- 1 : %s", __PRETTY_FUNCTION__);
    
    NSArray *q_choices = q_dict[@"question_choices"];
    
//    NSLog(@"question choices : %@", q_choices);
    
    for (NSDictionary *c_dict in q_choices) {
        
        NSString *choice_id = [self stringValue:c_dict[@"id"]];
        NSString *question_id = [self stringValue:c_dict[@"question_id"]];
        NSString *text = [self stringValue:c_dict[@"text"]];
        NSString *suggestive_feedback = [self stringValue:c_dict[@"suggestive_feedback"]];
        NSString *is_correct = [self stringValue:c_dict[@"is_correct"]];
        NSString *question_choice_image = [self stringValue:c_dict[@"question_choice_image"]];
        NSString *order_number = [self stringValue:c_dict[@"order_number"]];
        NSString *is_deleted = [self stringValue:c_dict[@"is_deleted"]];
        NSString *date_created = [self stringValue:c_dict[@"date_created"]];
        NSString *date_modified = [self stringValue:c_dict[@"date_modified"]];
        
        NSString *user_choice = [self stringValue:c_dict[@"user_choice"]];
        
        if ([is_correct isEqualToString:@"1"]) {
            is_correct = @"100";
        }
        
        NSString *choice_image = @"";
        if (question_choice_image.length > 0) {
            if (![question_choice_image  isEqual: @"null"]) {
                choice_image = [NSString stringWithFormat:@"%@/%@", homeurl, question_choice_image];
            }
        }
        
        NSManagedObject *c_mo = [self getEntity:kCoursePlayerQuestionChoiceEntity attribute:@"id" parameter:choice_id context:choice_ctx];//
        
        [c_mo setValue:question_type_id forKey:@"question_type_id"];
        [c_mo setValue:choice_id forKey:@"id"];
        [c_mo setValue:question_id forKey:@"question_id"];
        [c_mo setValue:text forKey:@"text"];
        [c_mo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
        [c_mo setValue:is_correct forKey:@"is_correct"];
        [c_mo setValue:choice_image forKey:@"question_choice_image"];
        [c_mo setValue:order_number forKey:@"order_number"];
        [c_mo setValue:is_deleted forKey:@"is_deleted"];
        [c_mo setValue:date_created forKey:@"date_created"];
        [c_mo setValue:date_modified forKey:@"date_modified"];
        
        NSString *answer = ([user_choice isEqualToString:@"1"]) ? @"1" : @"0";
        
        // additional ordernumber
        [c_mo setValue:@(order_number.integerValue) forKey:@"order_number_int"];
        
        [c_mo setValue:answer forKey:@"answer"];
        
        [c_mo setValue:user_choice forKey:@"user_choice"];
        
        [c_sets addObject:c_mo];
    }
    
    // FOR SHOW FEEDBACK
    if ([show_feedbacks isEqualToString:@"1"]) {
        NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:kCoursePlayerQuestionChoiceEntity inManagedObjectContext:choice_ctx];
        
        NSString *general_feedback = @"";
        NSString *correct_answer_feedback = @"";
        NSString *wrong_answer_feedback = @"";
        
        BOOL feedbacks_is_array = [self isArrayObject:q_dict[@"feedbacks"]];
        if (feedbacks_is_array) {
            NSArray *feedbacksArray = q_dict[@"feedbacks"];
            NSDictionary *feedbacks = feedbacksArray.lastObject;
            
            general_feedback = feedbacks[@"general_feedback"];
            correct_answer_feedback = feedbacks[@"correct_answer_feedback"];
            wrong_answer_feedback = feedbacks[@"wrong_answer_feedback"];
        }
        
        NSPredicate *predicate_qid = [self predicateForKeyPath:@"question_id" object:question_id];
        NSPredicate *predicate_uc = [self predicateForKeyPath:@"user_choice" object:@"1"];
        NSPredicate *predicate_ic = [self predicateForKeyPath:@"is_correct" object:@"100"];
        NSPredicate *c_predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate_qid, predicate_uc, predicate_ic]];
        
        NSArray *choices = [self getObjectsForEntity:kCoursePlayerQuestionChoiceEntity predicate:c_predicate];
        
        NSString *feedback = wrong_answer_feedback;
        
        if (choices.count > 0) {
            feedback = correct_answer_feedback;
        }
        
        NSString *uuid = [NSString stringWithFormat:@"%@-%@-%@",
                          [Utils randomNumber],[Utils randomNumber],[Utils randomNumber]];
        
        [c_mo setValue:uuid forKey:@"id"];
        [c_mo setValue:question_id forKey:@"question_id"];
        [c_mo setValue:feedback forKey:@"text"];
        [c_mo setValue:@"feedback" forKey:@"suggestive_feedback"];// FOR FEEDBACK ONLY
        [c_mo setValue:@"100" forKey:@"is_correct"];
        //                              [c_mo setValue:question_choice_image forKey:@"question_choice_image"];
        [c_mo setValue:@"1000" forKey:@"order_number"];
        [c_mo setValue:@"0" forKey:@"is_deleted"];
        [c_mo setValue:[self stringDate:[NSDate date]] forKey:@"date_created"];
        [c_mo setValue:[self stringDate:[NSDate date]] forKey:@"date_modified"];
        [c_mo setValue:@"0" forKey:@"answer"];
        
        // additional ordernumber
        [c_mo setValue:@(1000) forKey:@"order_number_int"];
        
        [c_sets addObject:c_mo];
    }
}

- (NSDictionary *)processTestResult:(NSDictionary *)test_result {
    
    NSString *total_score = [self stringValue:test_result[@"total_score"]];
    NSString *general_score = [self stringValue:test_result[@"general_score"]];
    NSString *passing_score = [self stringValue:test_result[@"passing_score"]];
    NSString *no_of_correct_answers = [self stringValue:test_result[@"no_of_correct_answers"]];
    NSString *no_of_answered_questions = [self stringValue:test_result[@"no_of_answered_questions"]];
    NSString *no_of_not_answered = [self stringValue:test_result[@"no_of_not_answered"]];
    NSString *no_of_questions = [self stringValue:test_result[@"no_of_questions"]];
    NSString *no_of_unchecked = [self stringValue:test_result[@"no_of_unchecked"]];
    
    total_score = [self formatStringNumber:total_score];
    general_score = [self formatStringNumber:general_score];
    
    NSDictionary *data = @{
                           @"total_score":total_score,
                           @"general_score":general_score,
                           @"passing_score":passing_score,
                           @"no_of_correct_answers":no_of_correct_answers,
                           @"no_of_answered_questions":no_of_answered_questions,
                           @"no_of_not_answered":no_of_not_answered,
                           @"no_of_questions":no_of_questions,
                           @"no_of_unchecked":no_of_unchecked
                           };
    
    return data;
}

- (void)submitAnswersForQuestionID:(NSString *)question_id timeSpent:(NSString *)timeSpent isFinished:(NSString *)isFinish dataBlock:(TestGuruDataBlock)dataBlock {
    
    NSString *test_id = [self fetchObjectForKey:kCP_SELECTED_QUIZ_ID];
    
    NSString *user_id = [self loginUser];
    NSString *timeRemainingKey = [NSString stringWithFormat:@"USER%@TESTID%@", user_id, test_id];
    
    NSDictionary *postBody = [self createSubmitAnswersBodyForQuestionID:question_id timeSpent:timeSpent isFinish:isFinish];
    NSLog(@"POST BODY ~~~~>%@<~~~~~", postBody);
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCoursePlayerSubmitAnswers, test_id] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          if (dataBlock) {
                                              dataBlock(@{
                                                          @"error": [error localizedDescription]
                                                          });
                                          }
                                      }
                                      
                                      if (!error) {
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          NSLog(@"ITO YUNG SUBMIT RESPONSE [%@]", dictionary);
                                          
                                          if (dictionary == nil) {
                                              if (dataBlock) {
                                                  dataBlock(@{
                                                              @"error": @"Error in sending answers, please try again"
                                                              });
                                              }
                                          }
                                          
                                          if ([isFinish isEqualToString:@"1"]) {
                                              
                                              [[NSUserDefaults standardUserDefaults] removeObjectForKey:timeRemainingKey];
                                              [[NSUserDefaults standardUserDefaults] synchronize];
                                              
                                              if (dictionary != nil) {
                                                  
                                                  NSDictionary *meta = dictionary[@"_meta"];
                                                  NSString *status = meta[@"status"];
                                                  
                                                  if ([status isEqualToString:@"SUCCESS"]) {
                                                      
                                                      NSDictionary *records = dictionary[@"records"];
                                                      NSDictionary *test_results = records[@"test_result"];
                                                      
                                                      NSString *total_score = [self stringValue:test_results[@"total_score"]];
                                                      NSString *general_score = [self stringValue:test_results[@"general_score"]];
                                                      NSString *passing_score = [self stringValue:test_results[@"passing_score"]];
                                                      NSString *no_of_correct_answers = [self stringValue:test_results[@"no_of_correct_answers"]];
                                                      NSString *no_of_answered_questions = [self stringValue:test_results[@"no_of_answered_questions"]];
                                                      NSString *no_of_not_answered = [self stringValue:test_results[@"no_of_not_answered"]];
                                                      NSString *no_of_questions = [self stringValue:test_results[@"no_of_questions"]];
                                                      NSString *no_of_unchecked = [self stringValue:test_results[@"no_of_unchecked"]];
                                                      
                                                      total_score = [self formatStringNumber:total_score];
                                                      general_score = [self formatStringNumber:general_score];
                                                      
                                                      NSDictionary *data = @{
                                                                             @"total_score":total_score,
                                                                             @"general_score":general_score,
                                                                             @"passing_score":passing_score,
                                                                             @"no_of_correct_answers":no_of_correct_answers,
                                                                             @"no_of_answered_questions":no_of_answered_questions,
                                                                             @"no_of_not_answered":no_of_not_answered,
                                                                             @"no_of_questions":no_of_questions,
                                                                             @"no_of_unchecked":no_of_unchecked
                                                                             };
                                                      
                                                      if (dataBlock) {
                                                          dataBlock(data);
                                                      }
                                                      
                                                  } else {
                                                      if (dataBlock) {
                                                          dataBlock(nil);
                                                      }
                                                  }
                                              }
                                              
                                          }
                                      }
                                  }];
    [task resume];
}

- (NSDictionary *)createSubmitAnswersBodyForQuestionID:(NSString *)question_id timeSpent:(NSString *)timeSpent isFinish:(NSString *)isFinish {
    
    NSString *user_id = [self loginUser];
    
    //    NSString *cs_id = @"2";//TODO MAKE DYNAMIC
    NSString *cs_id = [self fetchObjectForKey:kCP_SELECTED_CS_ID];
    
    NSArray *answers = [NSArray array];
    
    answers = [self answerArrayForQuestionID:question_id timeSpent:timeSpent isFinish:isFinish];
    
    NSDictionary *postBody = @{
                               @"user_id":user_id,
                               @"cs_id":cs_id,
                               @"answers":answers,
                               @"is_finish":isFinish
                               };
    
    return postBody;
}

- (NSArray *)answerArrayForQuestionID:(NSString *)questionID timeSpent:(NSString *)timeSpent isFinish:(NSString *)isFinish {
    
    NSArray *q_array = [NSArray array];
    
    if ([isFinish isEqualToString:@"1"]) {
        q_array = [self getObjectsForEntity:kCoursePlayerQuestionListEntity predicate:nil];
    } else {
        NSPredicate *questionPredicate = [self predicateForKeyPath:@"question_id" object:questionID];
        q_array = [self getObjectsForEntity:kCoursePlayerQuestionListEntity predicate:questionPredicate];
    }
    
    NSMutableArray *answerArray = [NSMutableArray array];
    
    for (NSManagedObject *q_mo in q_array) {
        
        NSDictionary *answerDict = [NSDictionary dictionary];
        
        NSString *question_type_id = [q_mo valueForKey:@"question_type_id"];
        NSString *question_id = [q_mo valueForKey:@"question_id"];
        NSString *question_points = [q_mo valueForKey:@"points"];
        
        NSPredicate *predicate = [self predicateForKeyPath:@"question_id" object:question_id];
        NSArray *c_array = [self getObjectsForEntity:kCoursePlayerQuestionChoiceEntity predicate:predicate];
        
        NSString *text = @"";
        
        // ESSAY/SHORT BODY
        if ([question_type_id isEqual:@"6"] || [question_type_id isEqual:@"9"] || [question_type_id isEqual:@"10"] || [question_type_id isEqual:@"2"]) {
            NSManagedObject *c_mo = [c_array lastObject];
            
            text = [c_mo valueForKey:@"text"];
        }
        
        // MULTIPLE/TOF BODY
        else {
            
            NSMutableArray *choices_array = [NSMutableArray array];
            
            
            for (NSManagedObject *c_mo in c_array) {
                
//                NSString *choice_id = [c_mo valueForKey:@"id"];
//                NSString *question_id = [c_mo valueForKey:@"question_id"];
//                NSString *text = [c_mo valueForKey:@"text"];
//                NSString *suggestive_feedback = [c_mo valueForKey:@"suggestive_feedback"];
//                NSString *is_correct = [c_mo valueForKey:@"is_correct"];
//                NSString *order_number = [c_mo valueForKey:@"order_number"];
//                NSString *is_deleted = [c_mo valueForKey:@"is_deleted"];
//                NSString *date_created = [c_mo valueForKey:@"date_created"];
//                NSString *date_modified = [c_mo valueForKey:@"date_modified"];
//                
//                NSString *answer = [c_mo valueForKey:@"answer"];
//                
//                NSString *user_choice = ([answer isEqualToString:@"1"]) ? @"1" : @"0";
//                
//                
//                NSDictionary *cDict = @{
//                                        @"id":choice_id,
//                                        @"question_id":question_id,
//                                        @"text":text,
//                                        @"suggestive_feedback":suggestive_feedback,
//                                        @"is_correct":is_correct,
//                                        @"order_number":order_number,
//                                        @"is_deleted":is_deleted,
//                                        @"date_created": date_created,
//                                        @"date_modified": date_modified,
//                                        @"user_choice":user_choice
//                                        };
//                [choices_array addObject:cDict];
                
                NSString *choice_id = [c_mo valueForKey:@"id"];
                NSString *question_id = [c_mo valueForKey:@"question_id"];
                NSString *text = [c_mo valueForKey:@"text"];
                NSString *suggestive_feedback = [c_mo valueForKey:@"suggestive_feedback"];
                NSString *is_correct = [c_mo valueForKey:@"is_correct"];
                NSString *order_number = [c_mo valueForKey:@"order_number"];
                NSString *is_deleted = [c_mo valueForKey:@"is_deleted"];
                NSString *date_created = [c_mo valueForKey:@"date_created"];
                NSString *date_modified = [c_mo valueForKey:@"date_modified"];
                NSString *answer = [c_mo valueForKey:@"answer"];
                NSString *user_choice = ([answer isEqualToString:@"1"]) ? @"1" : @"0";
                NSString *question_choice_image = [c_mo valueForKey:@"question_choice_image"];
                NSString *homeurl = [NSString stringWithFormat:@"http://%@/", [Utils getVibeServer]];
                question_choice_image = [question_choice_image stringByReplacingOccurrencesOfString:homeurl withString:@""];
                
                NSLog(@"HOME URL: %@", homeurl);
                NSLog(@"QUESTOIN CHOICE IMAGE: %@", question_choice_image);
                
                NSDictionary *cDict = @{
                                        @"id":choice_id,
                                        @"question_id":question_id,
                                        @"text":text,
                                        @"suggestive_feedback":suggestive_feedback,
                                        @"is_correct":is_correct,
                                        @"order_number":order_number,
                                        @"is_deleted":is_deleted,
                                        @"date_created": date_created,
                                        @"date_modified": date_modified,
                                        @"user_choice":user_choice,
                                        @"question_choice_image": question_choice_image
                                        };
                [choices_array addObject:cDict];
            }
            
            
            NSError *jsonError = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:choices_array options:NSJSONWritingPrettyPrinted error:&jsonError];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            text = jsonString;
        }
        
        NSString *time_spent = ([question_id isEqualToString:questionID]) ? timeSpent : @"00:00:00";
        
        answerDict = @{
                       @"question_id" : question_id,
                       @"question_answer_text":text,
                       @"score":question_points,
                       @"time_spent" : time_spent
                       };
        
        [answerArray addObject:answerDict];
    }
    return answerArray;
}

- (NSArray *)choicesForQuestionID:(NSString *)question_id {
    NSPredicate *predicate = [self predicateForKeyPath:@"question_id" object:question_id];
    NSArray *c_array = [self getObjectsForEntity:kCoursePlayerQuestionChoiceEntity predicate:predicate];
    return c_array;
}

- (NSString *)stringFromSeconds:(NSInteger)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (void)saveTimeRemainingForTest:(NSString *)test_id timeRemaining:(NSString *)timeRemaining {
    NSString *user_id = [self loginUser];
    NSString *timeRemainingKey = [NSString stringWithFormat:@"USER%@TESTID%@", user_id, test_id];
    
    [self saveObject:timeRemaining forKey:timeRemainingKey];
}


- (NSManagedObject *)fetchFillInTheBlanksAnswerForQuestionID:(NSString *)question_id {
    
    NSManagedObjectContext *ctx = [self workerContext];
    NSPredicate *predicate = [self predicateForKeyPath:@"question_id" andValue:question_id];
    
    NSArray *c_array = [self getObjectsForEntity:kCoursePlayerQuestionChoiceEntity predicate:predicate context:ctx];
    
    
    if (c_array != nil && c_array.count > 0) {
        
        NSManagedObject *c_mo = [c_array lastObject];
        
        return c_mo;
    }
    
    
    return nil;
}

#pragma mark - Seating Arrangement and Attendance

- (void)requestSeatingArrangementForCourseWithSectionID:(NSString *)sectionID doneBlock:(TestGuruDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointSeatingArrangement, sectionID]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            NSDictionary *parsedmeta = dictionary[@"_meta"];
            NSString *metastatus = parsedmeta[@"status"];
            
            NSLog(@"seating arrangement dictionary: %@", dictionary);
            NSLog(@"seating arrangement parsedmeta: %@", parsedmeta);
            NSLog(@"seating arrangement metastatus: %@", metastatus);
            
            BOOL isSuccess = NO;
            
            if ([[metastatus uppercaseString] isEqualToString:@"SUCCESS"]) {
                isSuccess = YES;
            }
            
            if (isSuccess) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"seating arrangement records: %@", records);
                
                [self clearContentsForEntity:kStudentSeatingArrangementEntity predicate:nil];
                
                if (records.count > 0) {
                    NSManagedObjectContext *ctx = self.workerContext;
                    
                    [ctx performBlock:^{
                        
                        for (NSDictionary *d in records) {
                            // Parse data from API
                            NSString *avatar = [NSString stringWithFormat:@"http://%@%@", [Utils getVibeServer], [self stringValue:d[@"avatar"]]];
                            NSString *contact_number = [self stringValue:d[@"contact_number"]];
                            NSString *csp_id = [self stringValue:d[@"csp_id"]];
                            NSString *date_time = [self stringValue:d[@"date_time"]];
                            NSString *email = [self stringValue:d[@"email"]];
                            NSString *first_name = [self stringValue:d[@"first_name"]];
                            NSString *is_absent = [self stringValue:d[@"is_absent"]];
                            NSString *is_late = [self stringValue:d[@"is_late"]];
                            NSString *last_name = [self stringValue:d[@"last_name"]];
                            NSString *level = [self stringValue:d[@"level"]];
                            NSString *ordering = [self stringValue:d[@"ordering"]];
                            NSString *user_id = [self stringValue:d[@"user_id"]];
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:kStudentSeatingArrangementEntity attribute:@"user_id" parameter:user_id context:ctx];
                            
                            // Save data to core data
                            [mo setValue:avatar forKey:@"avatar"];
                            [mo setValue:contact_number forKey:@"contact_number"];
                            [mo setValue:csp_id forKey:@"csp_id"];
                            [mo setValue:date_time forKey:@"date_time"];
                            [mo setValue:email forKey:@"email"];
                            [mo setValue:first_name forKey:@"first_name"];
                            [mo setValue:is_absent forKey:@"is_absent"];
                            [mo setValue:is_late forKey:@"is_late"];
                            [mo setValue:last_name forKey:@"last_name"];
                            [mo setValue:level forKey:@"level"];
                            [mo setValue:@([ordering integerValue]) forKey:@"ordering"];
                            [mo setValue:user_id forKey:@"user_id"];
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                }
            }
            
            if (doneBlock) {
                doneBlock(isSuccess);
            }
        }
    }];
    
    [task resume];
}

- (void)requestSeatingArrangement2ForCourseWithSectionID:(NSString *)sectionID dataBlock:(TestGuruDataBlock)dataBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointSeatingArrangement, sectionID]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            NSDictionary *parsedmeta = dictionary[@"_meta"];
            NSString *metastatus = parsedmeta[@"status"];
            
            NSLog(@"seating arrangement dictionary: %@", dictionary);
            NSLog(@"seating arrangement parsedmeta: %@", parsedmeta);
            NSLog(@"seating arrangement metastatus: %@", metastatus);
            
            BOOL isSuccess = NO;
            
            if ([[metastatus uppercaseString] isEqualToString:@"SUCCESS"]) {
                isSuccess = YES;
            }
            
            if (isSuccess) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"seating arrangement records: %@", records);
                
                [self clearContentsForEntity:kStudentSeatingArrangementEntity predicate:nil];
                
                if (records.count > 0) {
                    NSManagedObjectContext *ctx = self.workerContext;
                    
                    [ctx performBlock:^{
                        
                        NSInteger lateCount = 0;
                        NSInteger absentCount = 0;
                        
                        for (NSDictionary *d in records) {
                            // Parse data from API
                            NSString *avatar = [NSString stringWithFormat:@"http://%@%@", [Utils getVibeServer], [self stringValue:d[@"avatar"]]];
                            NSString *contact_number = [self stringValue:d[@"contact_number"]];
                            NSString *csp_id = [self stringValue:d[@"csp_id"]];
                            NSString *date_time = [self stringValue:d[@"date_time"]];
                            NSString *email = [self stringValue:d[@"email"]];
                            NSString *first_name = [self stringValue:d[@"first_name"]];
                            NSString *is_absent = [self stringValue:d[@"is_absent"]];
                            NSString *is_late = [self stringValue:d[@"is_late"]];
                            NSString *last_name = [self stringValue:d[@"last_name"]];
                            NSString *level = [self stringValue:d[@"level"]];
                            NSString *ordering = [self stringValue:d[@"ordering"]];
                            NSString *user_id = [self stringValue:d[@"user_id"]];
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:kStudentSeatingArrangementEntity attribute:@"user_id" parameter:user_id context:ctx];
                            
                            // Save data to core data
                            [mo setValue:avatar forKey:@"avatar"];
                            [mo setValue:contact_number forKey:@"contact_number"];
                            [mo setValue:csp_id forKey:@"csp_id"];
                            [mo setValue:date_time forKey:@"date_time"];
                            [mo setValue:email forKey:@"email"];
                            [mo setValue:first_name forKey:@"first_name"];
                            [mo setValue:is_absent forKey:@"is_absent"];
                            [mo setValue:is_late forKey:@"is_late"];
                            [mo setValue:last_name forKey:@"last_name"];
                            [mo setValue:level forKey:@"level"];
                            [mo setValue:@([ordering integerValue]) forKey:@"ordering"];
                            [mo setValue:user_id forKey:@"user_id"];
                            
                            if ([is_late isEqualToString:@"1"]) {
                                lateCount++;
                            }
                            
                            if ([is_absent isEqualToString:@"1"]) {
                                absentCount++;
                            }
                        }
                        
                        [self saveTreeContext:ctx];
                        
                        if (dataBlock) {
                            dataBlock(@{@"late_count":@(lateCount), @"absent_count":@(absentCount), @"count_all":@(records.count)});
                        }
                        
                    }];
                }
            }
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestChangeAttendanceStatusWithPostBody:(NSDictionary *)body doneBlock:(TestGuruDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointChangeAttendanceStatus]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:body];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (doneBlock) {
                doneBlock(isOkayToParse);
            }
        }
    }];
    
    [task resume];
}

- (void)requestChangeSeatOrderForCourseWithSectionID:(NSString *)sectionID andTeacherID:(NSString *)teacherID withPostBody:(NSDictionary *)body doneBlock:(TestGuruDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointChangeSeatingOrder, sectionID, teacherID]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:body];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (doneBlock) {
                doneBlock(isOkayToParse);
            }
        }
    }];
    
    [task resume];

}

- (NSInteger)getLateCount {
    NSPredicate *predicate = [self predicateForKeyPath:@"is_late" andValue:@"1"];
    NSArray *objects = [self getObjectsForEntity:kStudentSeatingArrangementEntity predicate:predicate];
    return objects.count;
}

- (NSInteger)getAbsentCount {
    NSPredicate *predicate = [self predicateForKeyPath:@"is_absent" andValue:@"1"];
    NSArray *objects = [self getObjectsForEntity:kStudentSeatingArrangementEntity predicate:predicate];
    return objects.count;
}

- (NSInteger)getCountAll {
    NSArray *objects = [self getObjectsForEntity:kStudentSeatingArrangementEntity predicate:nil];
    return objects.count;
}


@end
