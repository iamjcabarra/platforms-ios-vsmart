//
//  GBTMainViewController.swift
//  V-Smart
//
//  Created by Ryan Migallos on 15/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

enum GBPanelType: String {
    case StudentPanel
    case LearningPanel
    case GradePanel
}

protocol ScrollCommunicationDelegate {
    func gbScrollViewDidScroll(_ panel: GBPanelType, scrollTo: CGFloat)
    func gbScrollViewWillBeginDragging(_ panel: GBPanelType)
    func gbScrollViewDidEndDecelerating(_ panel: GBPanelType)
}

class GBTMainViewController: UIViewController, GBMDropDownDelegate, UIPopoverPresentationControllerDelegate {

    var leftPanel:GBTStudentListPanel!
    var centerPanel:GBTScrollingLearningComponent!
    var rightPanel:GBTGradePanel!
    
    var scrollingView : UIScrollView?
        
    @IBOutlet weak var segmentView: UIView!
    
    var sectionMenuOption:GBMDropDown!
    var courseMenuOption:GBMDropDown!
    
    @IBOutlet var sectionField: UITextField!
    @IBOutlet var courseField: UITextField!
    
    @IBOutlet var sectionButton: UIButton!
    @IBOutlet var courseButton: UIButton!
    @IBOutlet weak var consolidatedButton: UIButton!
    
    fileprivate let segmentedControl = HMSegmentedControl()

    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    var selectedSection:[String:AnyObject]!
    
    //String
    var quiz_id = "" //default value
    var student_id = "" //default value
    
    var term_ids = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defer {
            gbdm.requestResultType { (done) in
                // do nothing
            }
        }
        
        defer {
            setupOptionMenus()
        }
        
        HUD.showUIBlockingIndicator(withText: "\(NSLocalizedString("Please wait", comment: ""))...")
        gbdm.requestSection { (done) in
            if done {
                
                self.selectedSection = self.getDefaultValueForSection()
                let sectionName = self.selectedSection["section"] as? String
                let sectionID = self.selectedSection["section_id"] as? String
                let is_advised = self.selectedSection["is_advised"] as? String
                DispatchQueue.main.async(execute: {
                    self.sectionField.text = sectionName!
                    self.consolidatedButton.isHidden = (is_advised == "1") ? false : true
                })
                
                print("done fetching sections...")
                
                let section_id = sectionID! // DYNAMIC IMPLEMENTATION
                self.gbdm.save(object: section_id as AnyObject!, forKey: GBTConstants.UserDefined.GBTSECTIONID)
//                let section_id = "12"//TESTING
                
                self.gbdm.requestCourses(inSection: section_id, handler: { (status) in
                    if status {
                        print("done fetching sections...")
                        
                        let courseDict = self.getDefaultValueForCourse()
                        let courseName = courseDict["course_name"] as? String
                        let cs_id = courseDict["cs_id"] as? String
                        let is_teaching = courseDict["is_teaching"] as? String
                        let course_id = courseDict["course_id"] as? String
                        
                        self.gbdm.save(object: is_teaching as AnyObject!, forKey: GBTConstants.UserDefined.GBTCOURSEISTEACHING)
                        self.gbdm.save(object: cs_id as AnyObject!, forKey: GBTConstants.UserDefined.GBTCSID)
                        self.gbdm.save(object: course_id as AnyObject!, forKey: GBTConstants.UserDefined.GBTCOURSEID)
                        
                        DispatchQueue.main.async(execute: {
                            self.courseField.text = courseName!
                        })
                        
                        self.gbdm.requestGradeBookContent(cs_id!, quizId: "", resultTypeid: "", search: "", handler: { (done, error) in
                            if error == nil {
                                DispatchQueue.main.async(execute: {
                                    self.setUpSegmentedViews(forCSID: cs_id!, persistTermIndex: nil, persistComponentIndex: nil)
                                })
                            } else {
                                self.displayAlert(withTitle: "", withMessage: error!)
                            }
                        })
                        
//                        self.gbdm.requestGradeBookContent(cs_id!, quizId: "", resultTypeid: "", search: "", handler: { (flag) in
//                            if flag {
//                                dispatch_async(dispatch_get_main_queue(), {
//                                    self.setUpSegmentedViews(forCSID: cs_id!, persistTermIndex: nil, persistComponentIndex: nil)
//                                })
//                            } else {
//                                
//                            }
//                        })
                    }
                })
            }
        }
        
        self.consolidatedButton.addTarget(self, action: #selector(self.consolidatedButtonAction(_:)), for: .touchUpInside)
    }
    
    @IBAction func overAllGradesButtonAction(_ sender: Any) {
        
        self.performSegue(withIdentifier: "GBT_OVERALL_GRADES_SEGUE", sender: nil)
        
        
    }
    func consolidatedButtonAction(_ button: UIButton) {
        HUD.showUIBlockingIndicator(withText: "\(NSLocalizedString("Please wait", comment: ""))...")
        let mysection_id = gbdm.fetchUserDefaultsObject(forKey: GBTConstants.UserDefined.GBTSECTIONID) as! String
        
//        gbdm.requestSummary(forSectionID: mysection_id) { done in
//            if done {
//                dispatch_async(dispatch_get_main_queue(), {
//                    HUD.hideUIBlockingIndicator()
//                    self.performSegueWithIdentifier("GBT_CONSOLIDATED_GRADE_SEGUE", sender: nil)
//                })
//                print("done requesting summary...")
//            }
//        }
        
        
        gbdm.requestSummary(forSectionID: mysection_id) { (done, error) in
            
            if error == nil {
                DispatchQueue.main.async(execute: {
                    HUD.hideUIBlockingIndicator()
                    self.performSegue(withIdentifier: "GBT_CONSOLIDATED_GRADE_SEGUE", sender: nil)
                })
            } else {
                self.displayAlert(withTitle: "", withMessage: error!)
            }
            
            /*if done {
                DispatchQueue.main.async(execute: {
                    HUD.hideUIBlockingIndicator()
                    self.performSegue(withIdentifier: "GBT_CONSOLIDATED_GRADE_SEGUE", sender: nil)
                })
                print("done requesting summary...")
            }*/
        }
    }
    
    func displayAlert(withTitle title: String, withMessage message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        
        DispatchQueue.main.async {
            HUD.hideUIBlockingIndicator()
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // enable notification
        notication(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // disable notification
        notication(false)
    }
    
    func notication(_ enable:Bool) {

        let nc = NotificationCenter.default
        let event = GBTConstants.Notification.DISPLAYTEST
        let action = #selector(GBTMainViewController.processTestPaper(_:))
        
        if enable == true {
            nc.addObserver(self, selector: action, name: NSNotification.Name(rawValue: event), object: nil)
            nc.addObserver(self, selector: #selector(self.reloadAction(_:)), name: NSNotification.Name(rawValue: "GBT_RELOAD_GRADEBOOK_CONTENT"), object: nil)
            nc.addObserver(self, selector: #selector(self.sortAction(_:)), name: NSNotification.Name(rawValue: "GBT_SORT_GRADEBOOK_CONTENT"), object: nil)
        }
        
        if enable == false {
            nc.removeObserver(self, name: NSNotification.Name(rawValue: event), object: nil)
            nc.removeObserver(self, name: NSNotification.Name(rawValue: "GBT_RELOAD_GRADEBOOK_CONTENT"), object: nil)
            nc.removeObserver(self, name: NSNotification.Name(rawValue: "GBT_SORT_GRADEBOOK_CONTENT"), object: nil)
        }
    }
    
    func reloadAction(_ notification:Notification) {
        HUD.showUIBlockingIndicator(withText: "\(NSLocalizedString("Please wait", comment: ""))...")
        
        let cs_id = gbdm.fetchUserDefaultsObject(forKey: GBTConstants.UserDefined.GBTCSID) as! String
        
//        self.gbdm.requestGradeBookContent(cs_id, quizId: "", resultTypeid: "", search: "", handler: { (flag) in
//            if flag {
//                dispatch_async(dispatch_get_main_queue(), {
//                    self.setUpSegmentedViews(forCSID: cs_id, persistTermIndex: true)
//                })
//            }
//        })
        
        let dict = notification.object as? [String:String]
        let term_id = (dict != nil) ? dict!["term_id"] : nil
        let component_id = (dict != nil) ? dict!["component_id"] : nil
        
        self.requestGradebookContent(forCSID: cs_id, persisTermIndex: term_id, persistComponentIndex: component_id)
    }
    
    func sortAction(_ notification:Notification) {
        HUD.showUIBlockingIndicator(withText: "\(NSLocalizedString("Please wait", comment: ""))...")
        
        let cs_id = gbdm.fetchUserDefaultsObject(forKey: GBTConstants.UserDefined.GBTCSID) as! String
        
        //        self.gbdm.requestGradeBookContent(cs_id, quizId: "", resultTypeid: "", search: "", handler: { (flag) in
        //            if flag {
        //                dispatch_async(dispatch_get_main_queue(), {
        //                    self.setUpSegmentedViews(forCSID: cs_id, persistTermIndex: true)
        //                })
        //            }
        //        })
        
        let dict = notification.object as? [String:String]
        
        let quiz_id = (dict != nil) ? dict!["quiz_id"] : nil
        let result_type_id = (dict != nil) ? dict!["result_type_id"] : nil
        
        let term_id = (dict != nil) ? dict!["term_id"] : nil
        let component_id = (dict != nil) ? dict!["component_id"] : nil
        
        self.gbdm.requestGradeBookContent(cs_id, quizId: quiz_id!, resultTypeid: result_type_id!, search: "", handler: { (done, error) in
            if error == nil {
                DispatchQueue.main.async(execute: {
                    self.setUpSegmentedViews(forCSID: cs_id, persistTermIndex: term_id, persistComponentIndex: component_id)
                })
            } else {
                self.displayAlert(withTitle: "", withMessage: error!)
            }
        })
        
        //            if flag {
        //                dispatch_async(dispatch_get_main_queue(), {
        //                    self.setUpSegmentedViews(forCSID: cs_id, persistTermIndex: persisTermIndex, persistComponentIndex: persistComponentIndex)
        //                })
        //            }
        //        })
        //self.requestGradebookContent(forCSID: cs_id, persisTermIndex: term_id, persistComponentIndex: component_id)
    }
    
    func processTestPaper(_ notification:Notification) {
        
        let dictionary: [String:String] = notification.object as! [String:String]
        self.quiz_id = "\(dictionary["quiz_id"]!)"
        self.student_id = "\(dictionary["student_id"]!)"
        
        let identifier = GBTConstants.SegueName.GBTTESTPAPER
        self.performSegue(withIdentifier: identifier, sender: self)
    }
    
    func getDefaultValueForSection() -> [String:AnyObject] {
        
        let defaultValue = self.gbdm.retrieveDefaultValue(GBTConstants.Entity.SECTION,
                                                          properties: ["section","section_id", "is_advised"],
                                                          key: "section") as [String:AnyObject]
        print("----> default value : \(defaultValue)")
        
        return defaultValue
    }
    
    func getDefaultValueForCourse() -> [String:AnyObject] {
        
        let defaultValue = self.gbdm.retrieveDefaultValue(GBTConstants.Entity.COURSE,
                                                          properties: ["course_name","cs_id", "is_teaching", "course_id"],
                                                          key: "course_name") as [String:AnyObject]
        print("----> default value : \(defaultValue)")
        
        return defaultValue
    }
    
    func setupOptionMenus() {

        self.sectionMenuOption = optionMenu(.section, delegate:self)
        self.setupButton(self.sectionButton)
        
        self.courseMenuOption = optionMenu(.course, delegate:self)
        self.setupButton(self.courseButton)
    }
    
    func optionMenu(_ type:GBMDropDownMenuType, delegate:UIViewController? ) -> GBMDropDown {
        
        let menu = GBMDropDown(menu: type, delegate: self)
        menu.modalPresentationStyle = .popover

        return menu
    }
    
    func setupButton(_ button:UIButton) {
        let action = #selector(GBTMainViewController.optionAction(_:))
        let event = UIControlEvents.touchUpInside
        button.addTarget(self, action:action, for: event)
    }
    
    func optionAction(_ button:UIButton) {
        let menu = ( button == sectionButton ) ? sectionMenuOption : courseMenuOption
        
        // Popover setup
        let popController = (menu?.popoverPresentationController!)! as UIPopoverPresentationController
        popController.permittedArrowDirections = .up
        popController.delegate = self
        
        // Popover from an arbitrary anchor point
        popController.sourceView = button
        popController.sourceRect = CGRect(x: 0, y: 0, width: button.frame.size.width, height: button.frame.size.height)
        
        // present menu
        self.present(menu!, animated: true, completion: nil)
    }
    
    func didFinishSelecting(_ type:GBMDropDownMenuType, data: [String : AnyObject]) {

        let dataName = data["name"] as! String
        let dataID = data["id"] as! String
        print("\(#function) -- XXX -- \(dataName) = \(dataID)")
        
        HUD.showUIBlockingIndicator(withText: "\(NSLocalizedString("Please wait", comment: ""))...")
        
        if type.description() == GBTConstants.Entity.SECTION {
            let is_advised = data["is_advised"] as! String
            DispatchQueue.main.async(execute: { 
                self.consolidatedButton.isHidden = (is_advised == "1") ? false : true
            })
            self.sectionField.text = dataName
            self.gbdm.save(object: dataID as AnyObject!, forKey: GBTConstants.UserDefined.GBTSECTIONID)
            self.requestCourse(forSectionID: dataID)
        }
        
        if type.description() == GBTConstants.Entity.COURSE {
            let is_teaching = data["is_teaching"] as! String
            let course_id = data["course_id"] as? String
            self.courseField.text = dataName
            self.gbdm.save(object: is_teaching as AnyObject!, forKey: GBTConstants.UserDefined.GBTCOURSEISTEACHING)
            self.requestGradebookContent(forCSID: dataID, persisTermIndex: nil, persistComponentIndex: nil)
            self.gbdm.save(object: dataID as AnyObject!, forKey: GBTConstants.UserDefined.GBTCSID)
            self.gbdm.save(object: course_id as AnyObject!, forKey: GBTConstants.UserDefined.GBTCOURSEID)
        }
    }
    
    func requestCourse(forSectionID sectionid:String) {
        
        self.gbdm.requestCourses(inSection: sectionid, handler: { (status) in
            if status {
                print("done fetching sections...")
                
                let courseDict = self.getDefaultValueForCourse()
                let courseName = courseDict["course_name"] as? String
                let cs_id = courseDict["cs_id"] as! String
                let course_id = courseDict["course_id"] as! String
                let is_teaching = courseDict["is_teaching"] as! String
                
                DispatchQueue.main.async(execute: {
                    self.courseField.text = courseName!
                })
                self.gbdm.save(object: is_teaching as AnyObject!, forKey: GBTConstants.UserDefined.GBTCOURSEISTEACHING)
                self.gbdm.save(object: cs_id as AnyObject!, forKey: GBTConstants.UserDefined.GBTCSID)
                self.gbdm.save(object: course_id as AnyObject!, forKey: GBTConstants.UserDefined.GBTCOURSEID)
                self.requestGradebookContent(forCSID: cs_id, persisTermIndex: nil, persistComponentIndex: nil)
//                self.gbdm.requestGradeBookContent(cs_id, quizId: "", resultTypeid: "", search: "", handler: { (flag) in
//                    if flag {
//                        dispatch_async(dispatch_get_main_queue(), {
//                            self.setUpSegmentedViews(forCSID: cs_id)
//                        })
//                    }
//                })
            }
        })
    }
    
    func requestGradebookContent(forCSID cs_id: String, persisTermIndex: String?, persistComponentIndex: String?) {
        self.gbdm.requestGradeBookContent(cs_id, quizId: "", resultTypeid: "", search: "", handler: { (done, error) in
            if error == nil {
                DispatchQueue.main.async(execute: {
                    self.setUpSegmentedViews(forCSID: cs_id, persistTermIndex: persisTermIndex, persistComponentIndex: persistComponentIndex)
                })
            } else {
                self.displayAlert(withTitle: "", withMessage: error!)
            }
        })
//            if flag {
//                dispatch_async(dispatch_get_main_queue(), {
//                    self.setUpSegmentedViews(forCSID: cs_id, persistTermIndex: persisTermIndex, persistComponentIndex: persistComponentIndex)
//                })
//            }
//        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    fileprivate func setUpSegmentedViews(forCSID cs_id: String, persistTermIndex: String?, persistComponentIndex: String?) {
        let termList = self.gbdm.fetchTerms(forSectionID: cs_id)
        print(termList)
        
        if termList.count == 0 {
            DispatchQueue.main.async(execute: {
                HUD.hideUIBlockingIndicator()
            })
            return;
        }
        
//                let termNames = ["1st Quarter", "2nd Quarter", "3rd Quarter", "4th Quarter", "Final"]//[String]()
        
        var termNames = [String]()
//
        if termList.count > 0 {
            for section in termList {
                if let id = section["id"], let name = section["name"]{
                    self.term_ids.append(id)
                    termNames.append(name)
                }
            }
        }
        
        let index = (persistTermIndex != nil) ? self.getIndex(persistTermIndex) : 0
        
        if term_ids.count > 0 {
            self.centerPanel.reloadPage(forTermID: self.term_ids[index], component_id: persistComponentIndex)
            self.leftPanel.reloadList(forTermID: self.term_ids[index])
            self.rightPanel.reloadList(forTermID: self.term_ids[index])
        }
        
        // SET UP SEGMENTED VIEWS
        segmentedControl.sectionTitles = termNames
        segmentedControl.selectedSegmentIndex = index
        segmentedControl.backgroundColor = UIColor(rgba: "#f5f4f4")
        segmentedControl.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(rgba: "#1d6483")]
        segmentedControl.selectedTitleTextAttributes = [NSForegroundColorAttributeName: UIColor(rgba: "#1d6483")]
        segmentedControl.selectionIndicatorColor = UIColor(rgba: "#00a9d5")
        segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox
        segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationUp
        
        // SWITCH VIEW
        self.segmentedControl.indexChangeBlock = { (index) in
            DispatchQueue.main.async(execute: {
                self.centerPanel.reloadPage(forTermID: self.term_ids[index], component_id: nil)
                self.leftPanel.reloadList(forTermID: self.term_ids[index])
                self.rightPanel.reloadList(forTermID: self.term_ids[index])
                
//                print("SELECTED TERM NAME \(termNames[index]) SELECTED TERM_ID \(self.term_ids[index])")
                //                let message = "\(NSLocalizedString("Loading", comment: ""))..."
                //                //                self.view.makeToastActivity(message: message)
                //                HUD.showUIBlockingIndicatorWithText(message)
            })
        }
        
        // EMBED VIEW
        self.segmentView.addSubview(self.segmentedControl)
        
        // CONSTRAINTS
        segmentedControl.mas_makeConstraints { (make) in
            make?.edges.isEqual(self.segmentView)
        }
        
        DispatchQueue.main.async(execute: {
            HUD.hideUIBlockingIndicator()
        })
    }
    
    func getIndex(_ term_id: String!) -> Int {
        var index = 0
        if self.term_ids.contains(term_id) {
            index = self.term_ids.index(of: term_id)!
        }
        
        return index
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        
        if segue.identifier == GBTConstants.SegueName.GBTSTUDENTPANEL {
            self.leftPanel = segue.destination as! GBTStudentListPanel
            self.leftPanel.scrollDelegate = self
//            self.leftPanel.term_id = "4"
        }
        
        if segue.identifier == GBTConstants.SegueName.GBTLEARNINGCOMPONENTPANEL {
            let nav = segue.destination as! UINavigationController
            self.centerPanel = nav.topViewController as! GBTScrollingLearningComponent
//            self.centerPanel.term_id = "4"
            self.centerPanel.scrollDelegate = self
        }
        
        if segue.identifier == GBTConstants.SegueName.GBTGRADEPANEL {
            self.rightPanel = segue.destination as! GBTGradePanel
            self.rightPanel.scrollDelegate = self
//            self.rightPanel.term_id = "4"
        }
        
        if segue.identifier == GBTConstants.SegueName.GBTTESTPAPER {
            let tpv = segue.destination as! GBMTestPaperView
            
            let participant_id = "\(self.student_id)"
            let studentObject = gbdm.fetchStudent(participant_id) 
            let student_user_id = "\(studentObject["user_id"]!)"
            
            let csid = gbdm.fetchUserDefaultsObject(forKey: GBTConstants.UserDefined.GBTCSID) as! String
            let quizid = "\(self.quiz_id)"
            tpv.loadWebContent(student_user_id, csid: csid, quizid: quizid)
//            tpv.userid = "68"
//            tpv.csid = "24"
//            tpv.quizid = "48"
        }
    }
}

extension GBTMainViewController : ScrollCommunicationDelegate {
    
    func gbScrollViewDidScroll(_ panel: GBPanelType, scrollTo: CGFloat) {
        if self.scrollingView == self.leftPanel.tableView {
            let offSetX = self.centerPanel.displayedViewController?.collectionView.contentOffset.x
            self.rightPanel.tableView.setContentOffset(CGPoint(x: 0, y: scrollTo), animated: false)
            
            if self.centerPanel.displayedViewController != nil {
                self.centerPanel.displayedViewController?.collectionView.setContentOffset(CGPoint(x: offSetX!, y: scrollTo), animated: false)
            }
        }
        
        if self.scrollingView == self.centerPanel.displayedViewController?.collectionView {
            self.rightPanel.tableView.setContentOffset(CGPoint(x: 0, y: scrollTo), animated: false)
            self.leftPanel.tableView.setContentOffset(CGPoint(x: 0, y: scrollTo), animated: false)
        }
        
        if self.scrollingView == self.rightPanel.tableView {
            let offSetX = self.centerPanel.displayedViewController?.collectionView.contentOffset.x
            self.leftPanel.tableView.setContentOffset(CGPoint(x: 0, y: scrollTo), animated: false)
            if self.centerPanel.displayedViewController != nil {
                self.centerPanel.displayedViewController?.collectionView.setContentOffset(CGPoint(x: offSetX!, y: scrollTo), animated: false)
            }
        }
    }
    
    func gbScrollViewWillBeginDragging(_ panel: GBPanelType) {
        
        if self.scrollingView == nil {
            switch panel {
            case .StudentPanel :
                self.scrollingView = self.leftPanel.tableView
                if self.centerPanel.displayedViewController != nil {
                    self.centerPanel.displayedViewController?.collectionView.isUserInteractionEnabled = false
                }
                self.rightPanel.tableView.isUserInteractionEnabled = false
            case .LearningPanel :
                if self.centerPanel.displayedViewController != nil {
                    self.centerPanel.displayedViewController?.collectionView.isUserInteractionEnabled = false
                }
                self.leftPanel.tableView.isUserInteractionEnabled = false
                self.rightPanel.tableView.isUserInteractionEnabled = false
            case .GradePanel :
                self.scrollingView = self.rightPanel.tableView
                self.centerPanel.displayedViewController?.collectionView.isUserInteractionEnabled = false
                self.leftPanel.tableView.isUserInteractionEnabled = false
            }
        }
    }
    
    func gbScrollViewDidEndDecelerating(_ panel: GBPanelType) {
        self.scrollingView = nil
        if self.centerPanel.displayedViewController != nil {
            self.centerPanel.displayedViewController?.collectionView.isUserInteractionEnabled = true
        }
        self.leftPanel.tableView.isUserInteractionEnabled = true
        self.rightPanel.tableView.isUserInteractionEnabled = true
    }
}
