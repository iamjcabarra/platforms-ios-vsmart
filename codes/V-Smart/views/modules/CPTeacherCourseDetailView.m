//
//  CPTeacherCourseDetailView.m
//  V-Smart
//
//  Created by Julius Abarra on 30/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "CPTeacherCourseDetailView.h"
#import "CPCourseDetailSegmentView.h"
#import "TBClassHelper.h"

@interface CPTeacherCourseDetailView () <UISearchBarDelegate>

@property (strong, nonatomic) CPCourseDetailSegmentView *segmentView;
@property (strong, nonatomic) TBClassHelper *classHelper;

@property (strong, nonatomic) IBOutlet UIView *visualEffectView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *headerViewHeight;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (strong, nonatomic) IBOutlet UILabel *scheduleLabel;
@property (strong, nonatomic) IBOutlet UILabel *sectionLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *codeLabel;
@property (strong, nonatomic) IBOutlet UILabel *roomLabel;
@property (strong, nonatomic) IBOutlet UILabel *scheduleValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *sectionValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *codeValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *roomValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionValueLabel;

@property (strong, nonatomic) NSString *searchKeyword;

@property (assign, nonatomic) NSInteger selectedSegmedtedViewIndex;
@property (assign, nonatomic) BOOL isAscending;
@property (assign, nonatomic) BOOL isVersion25;

@end

static NSString *kShowStudentListView = @"SHOW_STUDENT_LIST_VIEW";
static NSString *kShowAssignedTestsView = @"SHOW_ASSIGNED_TESTS_VIEW";
static NSString *kShowSeatPlanView = @"SHOW_SEAT_PLAN_VIEW";
static NSString *kShowAttendanceView = @"SHOW_ATTENDANCE_VIEW";
static NSString *kEmbeddedSegmentView = @"EMBED_COURSE_DETAIL_SEGMENT_VIEW";

@implementation CPTeacherCourseDetailView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // String Localization
    [self localizeLabels];
    
    // Version Check
    CGFloat version = [[Utils getServerInstanceVersion] floatValue];
    self.isVersion25 = NO;//(version >= VSMART_SERVER_MAX_VER);
    
    // Course Header
    [self loadCourseHeaderValues];
    [self updateHeaderViewHeight];
    
    // Segmented Control
    // [self customizeSegmentedControl];
    self.selectedSegmedtedViewIndex = 0;
    
    // Search Default
    self.searchKeyword = @"";
    
    // Sort Default
    self.isAscending = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelSearchWhenTapped)];
    [self.visualEffectView addGestureRecognizer:tap];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Custom Navigation Bar
    [self customizeNavigationController];
    
    // Right Bar Buttons
    [self setUpRightBarButtons];
    
    // Dynamic Student Count Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processStudentCountAction:)
                                                 name:@"processStudentCountAction"
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Remove Notification Observers
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"processStudentCountAction"
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Navigation Bar

- (void)customizeNavigationController {
    UIColor *color = UIColorFromHex(0xEF4136);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    }
    else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Custom Navigation Title View
    self.searchBar.placeholder = NSLocalizedString(@"Search Student", nil);
    self.searchBar.delegate = self;
}

#pragma mark - Custom Right Bar Buttons

- (void)setUpRightBarButtons {
    // Spacer Button
    UIButton *spacerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    spacerButton.frame = CGRectMake(0, 0, 10, 20);
    
    // Search Button
    UIImage *searchImage = [UIImage imageNamed:@"search_white_48x48.png"];
    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    searchButton.frame = CGRectMake(0, 0, 20, 20);
    searchButton.showsTouchWhenHighlighted = YES;
    
    [searchButton setImage:searchImage forState:UIControlStateNormal];
    [searchButton setImage:searchImage forState:UIControlStateHighlighted];
    [searchButton addTarget:self action:@selector(searchButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Sort Button
    UIImage *sortImage = [UIImage imageNamed:@"sort_white48x48.png"];
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    sortButton.frame = CGRectMake(0, 0, 20, 20);
    sortButton.showsTouchWhenHighlighted = YES;
    
    [sortButton setImage:sortImage forState:UIControlStateNormal];
    [sortButton setImage:sortImage forState:UIControlStateHighlighted];
    [sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Right Button Items
    UIBarButtonItem *spacerButtonItem = [[UIBarButtonItem alloc] initWithCustomView:spacerButton];
    UIBarButtonItem *sortButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
    UIBarButtonItem *searchButtonItem = [[UIBarButtonItem alloc] initWithCustomView:searchButton];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:sortButtonItem, searchButtonItem, spacerButtonItem, nil];
}

#pragma mark - Course Datail Header

- (void)localizeLabels {
    self.scheduleLabel.text = NSLocalizedString(@"Schedule", nil);
    self.sectionLabel.text = NSLocalizedString(@"Section", nil);
    self.descriptionLabel.text = NSLocalizedString(@"Description", nil);
    self.codeLabel.text = NSLocalizedString(@"Code", nil);
    self.roomLabel.text = NSLocalizedString(@"Room", nil);
}

- (void)loadCourseHeaderValues {
    if (self.courseObject != nil) {
        self.title = [self.courseObject valueForKey:@"course_name"];
        self.scheduleValueLabel.text = [self.courseObject valueForKey:@"schedule"];
        self.sectionValueLabel.text = [self.courseObject valueForKey:@"section_name"];
        self.codeValueLabel.text = [self.courseObject valueForKey:@"course_code"];
        self.roomValueLabel.text = [self.courseObject valueForKey:@"venue"];
        
        NSString *description = [self.courseObject valueForKey:@"course_description"];
        [self.classHelper justifyLabel:self.descriptionValueLabel string:description];
        self.descriptionValueLabel.numberOfLines = 0;
    }
}

- (void)updateHeaderViewHeight {
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.25f animations:^{
            CGFloat additionalHeight = [self.classHelper getHeightOfLabel:self.descriptionValueLabel];
            CGFloat headerViewHeight = (additionalHeight > 0.0f) ? 80.0f : 95.0f;
            wo.headerViewHeight.constant = headerViewHeight + additionalHeight;
            [wo.view layoutIfNeeded];
        }];
        
        // Segmented Control
        [self customizeSegmentedControl];
    });
}

#pragma mark - Course Detail Segmented Views

- (void)customizeSegmentedControl {
    /*
    self.segmentedControl.tintColor = UIColorFromHex(0x00AEEF);
    self.segmentedControl.selectedSegmentIndex = 0;
    
    NSString *segmentATitle = [self.classHelper localizeString:@"Student List"];
    [self.segmentedControl setTitle:segmentATitle forSegmentAtIndex:0];
    
    NSString *segmentBTitle = [self.classHelper localizeString:@"Assigned Tests"];
    [self.segmentedControl setTitle:segmentBTitle forSegmentAtIndex:1];
    
    [self.segmentedControl addTarget:self
                              action:@selector(segmentedControlAction:)
                    forControlEvents:UIControlEventValueChanged];
    
    // NOTE: Remove if test list view is enabled in teacher mode
    [self.segmentedControl removeSegmentAtIndex:1 animated:YES];
     */
    
    /*
    [self.segmentedControl removeAllSegments];
    
    NSString *segmentATitle = [self.classHelper localizeString:@"Student List"];
    [self.segmentedControl insertSegmentWithTitle:segmentATitle atIndex:0 animated:YES];
    
    //NSString *segmentBTitle = [self.classHelper localizeString:@"Assigned Tests"];
    //[self.segmentedControl insertSegmentWithTitle:segmentBTitle atIndex:1 animated:YES];
    
    NSString *segmentCTitle = [self.classHelper localizeString:@"Seat Plan"];               // unchecked
    [self.segmentedControl insertSegmentWithTitle:segmentCTitle atIndex:1 animated:YES];
    
    NSString *segmentDTitle = [self.classHelper localizeString:@"Attendance"];              // unchecked
    [self.segmentedControl insertSegmentWithTitle:segmentDTitle atIndex:2 animated:YES];
    
    self.segmentedControl.tintColor = UIColorFromHex(0x00AEEF);
    self.segmentedControl.selectedSegmentIndex = 0;
    
    [self.segmentedControl addTarget:self
                              action:@selector(segmentedControlAction:)
                    forControlEvents:UIControlEventValueChanged];
    
    // NOTE: Remove if test list view is enabled in teacher mode
    //[self.segmentedControl removeSegmentAtIndex:1 animated:YES];
     */
    
    [self.segmentedControl removeAllSegments];
    
    NSString *segmentATitle = [self.classHelper localizeString:@"Student List"];
    [self.segmentedControl insertSegmentWithTitle:segmentATitle atIndex:0 animated:YES];
    
    if (self.isVersion25 == YES) {
        NSString *segmentBTitle = [self.classHelper localizeString:@"Seating Arrangement"];
        [self.segmentedControl insertSegmentWithTitle:segmentBTitle atIndex:1 animated:YES];
        
        NSString *segmentCTitle = [self.classHelper localizeString:@"Attendance"];
        [self.segmentedControl insertSegmentWithTitle:segmentCTitle atIndex:2 animated:YES];
    }
    
    UIFont *font = [UIFont boldSystemFontOfSize:15.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    [self.segmentedControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
    self.segmentedControl.tintColor = UIColorFromHex(0x00AEEF);
    self.segmentedControl.selectedSegmentIndex = self.selectedSegmedtedViewIndex;
    
    [self.segmentedControl addTarget:self
                              action:@selector(segmentedControlAction:)
                    forControlEvents:UIControlEventValueChanged];
}

- (void)segmentedControlAction:(id)sender {
    /*
    __weak typeof(self) wo = self;
    
    UISegmentedControl *sc = (UISegmentedControl *)sender;
    NSInteger index = sc.selectedSegmentIndex;
    
    // Student List View
    if (index == 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.searchBar.placeholder = NSLocalizedString(@"Search Student", nil);
            [wo.segmentView swapEmbeddedViews:kShowStudentListView];
        });
    }
    // Assigned Tests View
    if (index == 1) {
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.searchBar.placeholder = NSLocalizedString(@"Search Assigned Test", nil);
            [wo.segmentView swapEmbeddedViews:kShowAssignedTestsView];
        });
    }
    
    // Clear Search Text Field
    self.searchBar.text = @"";
    */
    
    __weak typeof(self) wo = self;
    
    UISegmentedControl *sc = (UISegmentedControl *)sender;
    NSInteger index = sc.selectedSegmentIndex;
    
    // Student List View
    if (index == 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.searchBar.placeholder = NSLocalizedString(@"Search Student", nil);
            [wo.segmentView swapEmbeddedViews:kShowStudentListView];
        });
    }
    
    if (self.isVersion25 == YES) {
        // Seating Arrangement
        if (index == 1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                wo.searchBar.placeholder = NSLocalizedString(@"Search Student", nil);
                [wo.segmentView swapEmbeddedViews:kShowSeatPlanView];
            });
        }
        // Attendance
        if (index == 2) {
            dispatch_async(dispatch_get_main_queue(), ^{
                wo.searchBar.placeholder = NSLocalizedString(@"Search Student", nil);
                [wo.segmentView swapEmbeddedViews:kShowAttendanceView];
            });
        }
    }
    
    // Update selected segmented view index
    self.selectedSegmedtedViewIndex = index;
    
    // Clear Search Text Field
    self.searchBar.text = @"";
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kEmbeddedSegmentView]) {
        self.segmentView = (CPCourseDetailSegmentView *)[segue destinationViewController];
        
        if (self.courseObject != nil) {
            self.segmentView.courseObject = self.courseObject;
        }
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self updateHeaderViewHeight];
}

#pragma mark - Actions for Buttons

- (void)searchButtonAction:(id)sender {
    self.navigationItem.titleView = self.searchBar;
    [self.searchBar becomeFirstResponder];
}

- (void)sortButtonAction:(id)sender {
    self.isAscending = (self.isAscending) ? NO : YES;
    
    NSDictionary *dictionary = @{@"isAscending":[NSNumber numberWithBool:self.isAscending]};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"processSortAction"
                                                        object:nil
                                                      userInfo:dictionary];
}

#pragma mark - Search Bar Delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    self.visualEffectView.hidden = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText isEqualToString:@""]) {
        [self postSearchNotification:searchText];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self postSearchNotification:searchBar.text];
    [self.searchBar resignFirstResponder];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    searchBar.text = self.searchKeyword;
    self.navigationItem.titleView = nil;
    self.visualEffectView.hidden = YES;
}

- (void)cancelSearchWhenTapped {
    self.navigationItem.titleView = nil;
    self.visualEffectView.hidden = YES;
    [self.view endEditing:YES];
    self.searchBar.text = self.searchKeyword;
}

#pragma mark - Actions for Notifications

- (void)processStudentCountAction:(NSNotification *)notification {
    NSDictionary *info = notification.userInfo;
    
    if (info != nil) {
        NSString *countString = [self.classHelper stringValue:info[@"count"]];
        NSString *title = [self.classHelper localizeString:@"Student List"];
        title = [NSString stringWithFormat:@"%@ (%zd)", title, [countString integerValue]];
        [self.segmentedControl setTitle:title forSegmentAtIndex:0];
    }
}

- (void)postSearchNotification:(NSString *)searchKeyword {
    self.searchKeyword = searchKeyword;
    
    NSDictionary *dictionary = @{@"searchKeyword":searchKeyword};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"processSearchAction"
                                                        object:nil
                                                      userInfo:dictionary];
}

@end
