//
//  PostStreamView.m
//  V-Smart
//
//  Created by VhaL on 3/13/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "SchoolStreamChildViewController.h"
#import "JSONHTTPClient.h"
#import "JMSPostMessage.h"
#import "JMSEmoItem.h"
#import "EmojiViewController.h"
#import "EmoticonViewController.h"
#import <Parse/Parse.h>

@interface SchoolStreamChildViewController ()
@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, strong) IBOutlet UIButton *buttonPost;
@property (nonatomic, strong) IBOutlet UITextView *textView;
@property (nonatomic, strong) IBOutlet UIView *postView;

@property (nonatomic, strong) UIPopoverController *popOverController;
@property (nonatomic, strong) EmojiViewController *emojiController;
@property (nonatomic, strong) EmoticonViewController *emoticonController;
@property (nonatomic, assign) BOOL disablePost;
@property (nonatomic, assign) int postViewHeightDiff;
@end

@implementation SchoolStreamChildViewController
@synthesize disablePost, postViewHeightDiff;

-(AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

-(void)viewDidLoad{
    [self setupWebview];
    
    self.textView.layer.borderColor = [UIColor grayColor].CGColor;
    self.textView.layer.borderWidth = 1;
    self.textView.layer.cornerRadius = 5;
    
//    if ([[self account].user.position isEqualToString:kModeIsStudent]) {
    disablePost = YES;
//    }
    
    [self setupShadows];
    [self setupGestures];
    [self setupEmoItems];
    
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [self.webView addGestureRecognizer:singleTap];
}
    
//BUG FIX #104
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationHideProfileView object:self];
}

- (void)singleTapping:(UIGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    disablePost = YES;
    
    if (disablePost){
        postViewHeightDiff = 0;
        [self togglePostView:nil];
    }
    else{
        postViewHeightDiff = 40;
        [self togglePostView:nil];
    }
}

-(void)setupShadows{
    // drop shadow
    CALayer *layer = self.postView.layer;
    [layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [layer setShadowOpacity:0.5];
    [layer setShadowRadius:2.0];
    [layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    [layer setShadowPath:[UIBezierPath bezierPathWithRect:layer.bounds].CGPath];
}

-(void)setupGestures{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(togglePostView:)];
    
    tapGesture.numberOfTapsRequired = 1;
    
    UISwipeGestureRecognizer *swipeDownGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(togglePostView:)];
    
    swipeDownGesture.numberOfTouchesRequired = 1;
    swipeDownGesture.direction = UISwipeGestureRecognizerDirectionDown;
    
    [self.postView addGestureRecognizer:tapGesture];
    [self.postView addGestureRecognizer:swipeDownGesture];
    
    UISwipeGestureRecognizer *swipeUpGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(togglePostView:)];
    
    swipeUpGesture.numberOfTouchesRequired = 1;
    swipeUpGesture.direction = UISwipeGestureRecognizerDirectionUp;
    
    [self.postView addGestureRecognizer:swipeUpGesture];
}

-(void)setupEmoItems{
    
    self.emojiController = [[EmojiViewController alloc] init];
    self.emojiController.delegate = self;
    
    self.emoticonController = [[EmoticonViewController alloc] init];
    self.emoticonController.delegate = self;
    
    /*
     NOTE:
     Get a pointer to the JSONHTTPClient headers
     set the schoolcode_base64
     */
    NSMutableDictionary *headers = [JSONHTTPClient requestHeaders];
    headers[@"code"] = [Utils getSettingsSchoolCodeBase64];

    [JSONHTTPClient getJSONFromURLWithString:[Utils buildUrl:kEndPointGetEmojis] completion:^(NSDictionary *json, JSONModelError *err) {
        if(err == nil){
            NSArray *arrayRecords = [json objectForKey:@"records"];
            NSMutableArray *array = [NSMutableArray arrayWithArray:arrayRecords];
            NSDictionary *dict = @{@"records": array};
            
            NSError *parseError = nil;
            
            NSData *records = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
            NSString *jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
            
            self.emojiController.emoItems = [[JMSRecordEmoItem alloc] initWithString:jsonString error:&parseError];
            [self.emojiController loadData];
        }
    }];
    
    [JSONHTTPClient getJSONFromURLWithString:[Utils buildUrl: kEndPointGetEmoticons] completion:^(NSDictionary *json, JSONModelError *err) {
        if(err == nil){
            NSArray *arrayRecords = [json objectForKey:@"records"];
            NSMutableArray *array = [NSMutableArray arrayWithArray:arrayRecords];
            NSDictionary *dict = @{@"records": array};
            
            NSError *parseError = nil;
            
            NSData *records = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
            NSString *jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
            
            self.emoticonController.emoItems = [[JMSRecordEmoItem alloc] initWithString:jsonString error:&parseError];
            [self.emoticonController loadData];
        }
    }];
}

-(IBAction)togglePostView:(id)sender{
    NSLog(@"%@", @"togglePostView");
    
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         if (0 <= self.postView.frame.origin.y){
                             self.postView.frame = CGRectMake(self.postView.frame.origin.x,
                                                              self.postView.frame.origin.y - (self.postView.frame.size.height - postViewHeightDiff),
                                                              self.postView.frame.size.width,
                                                              self.postView.frame.size.height);
                             
                             self.webView.frame = CGRectMake(self.webView.frame.origin.x,
                                                             self.webView.frame.origin.y - (self.postView.frame.size.height - postViewHeightDiff),
                                                             self.webView.frame.size.width,
                                                             self.webView.frame.size.height + (self.postView.frame.size.height - postViewHeightDiff));
                             
                             [self.textView resignFirstResponder];
                         }
                         else{
                             self.postView.frame = CGRectMake(self.postView.frame.origin.x,
                                                              self.postView.frame.origin.y + (self.postView.frame.size.height - postViewHeightDiff),
                                                              self.postView.frame.size.width,
                                                              self.postView.frame.size.height);
                             
                             self.webView.frame = CGRectMake(self.webView.frame.origin.x,
                                                             self.webView.frame.origin.y + (self.postView.frame.size.height - postViewHeightDiff),
                                                             self.webView.frame.size.width,
                                                             self.webView.frame.size.height - (self.postView.frame.size.height - postViewHeightDiff));
                         }
                         
                     }
                     completion:^(BOOL finished){
//                         [self fixIOS7TextViewBug];
                     }];
    
}

-(void)setupWebview{
    
    // DEFAULT ADMIN VALUE
    int position = 1;
    
    // STUDENT MODE
    if ([[self account].user.position isEqualToString:kModeIsStudent]) {
        position = 4;
    }
    
    // TEACHER MODE
    if ([[self account].user.position isEqualToString:kModeIsTeacher]) {
        position = 3;
    }
    
//    NSString *lang = [VSmartHelpers getDeviceLocale]; // device language
//    NSString *url = VS_FMT([Utils buildUrl:kEndPointSchoolStream], [self account].user.id, lang );
    NSString *url = VS_FMT([Utils buildUrl:kEndPointSchoolStream], [self account].user.id, position );
    NSLog(@"social stream url : %@", url);
    
    NSURLRequest *webRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [self.webView loadRequest:webRequest];
    
    for (id subview in self.webView.subviews)
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            ((UIScrollView *)subview).bounces = NO;
    
    [self.view addSubview:self.webView];
}

-(IBAction)buttonPostTapped:(id)sender{
    
    if (self.postView.frame.origin.y < 0){
        [self togglePostView:nil];
        return;
    }
    
    JMSPostMessage *postMessage = [JMSPostMessage new];
    postMessage.userId = [self account].user.id;
    postMessage.groupId = 1;
    postMessage.emoticonId = 0;
    postMessage.isAttached = 0;
    postMessage.message = [self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *postMessageAsString = [postMessage toJSONString];
    
    NSString *code = [Utils getSettingsSchoolCode];
    NSDictionary *dimensions = @{@"user": [self account].user.email, @"code": code};
    [PFAnalytics trackEvent:@"school" dimensions:dimensions];
    
    /*
     NOTE:
     Get a pointer to the JSONHTTPClient headers
     set the schoolcode_base64
     */
    NSMutableDictionary *headers = [JSONHTTPClient requestHeaders];
    headers[@"code"] = [Utils getSettingsSchoolCodeBase64];
    
    [JSONHTTPClient postJSONFromURLWithString:[Utils buildUrl:kEndPointPostSchoolStream] bodyString:postMessageAsString completion:^(NSDictionary *json, JSONModelError *err) {
        if (err == nil){
            VLog(@"%@", json);
        }
        else{
            VLog(@"%@", err);
        }
    }];
    
    self.textView.text = @"";
    [self togglePostView:nil];
}

-(IBAction)segmentedControlTapped:(UISegmentedControl*)sender{
    
    if (self.postView.frame.origin.y < 0){
        [self togglePostView:nil];
    }
    
    switch (sender.selectedSegmentIndex) {
        case 0:{ //emoji
            self.popOverController = [[UIPopoverController alloc] initWithContentViewController:self.emojiController];

            [self.popOverController setPopoverContentSize:CGSizeMake(self.emojiController.view.frame.size.width, self.emojiController.view.frame.size.height) animated:YES];
            
            self.emojiController.parent = self.popOverController;
            
            CGFloat equalSize = sender.frame.size.width/sender.numberOfSegments;
            CGFloat halfSize = equalSize/2;
            CGFloat position = (sender.selectedSegmentIndex * equalSize) + halfSize;
            
            [self.popOverController presentPopoverFromRect:
             CGRectMake(sender.frame.origin.x + position, sender.frame.origin.y, 1, 1)
                                            inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        }
            break;

        case 1:{ //emoticon
            self.popOverController = [[UIPopoverController alloc] initWithContentViewController:self.emoticonController];
            
            [self.popOverController setPopoverContentSize:CGSizeMake(self.emoticonController.view.frame.size.width, self.emoticonController.view.frame.size.height) animated:YES];
            
            self.emoticonController.parent = self.popOverController;
            
            CGFloat equalSize = sender.frame.size.width/sender.numberOfSegments;
            CGFloat halfSize = equalSize/2;
            CGFloat position = (sender.selectedSegmentIndex * equalSize) + halfSize;
            
            [self.popOverController presentPopoverFromRect:
             CGRectMake(sender.frame.origin.x + position, sender.frame.origin.y, 1, 1)
                                                    inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        }
            break;
        case 2:{ //attachment
            
        }
            break;
            
        default:
            break;
    }
}

-(void)emojiTapped:(JMSEmoItem*)emoji{
    self.textView.text = [NSString stringWithFormat:@"%@%@", self.textView.text, emoji.chars];
}

-(void)emoticonTapped:(JMSEmoItem*)emoticon{
    self.textView.text = [NSString stringWithFormat:@"%@%@", self.textView.text, emoticon.chars];
}

-(void)fixIOS7TextViewBug{
    //the bug is: if user trigger textview editing, the cursor starts at the bottom of the textview frame
    
    UITextView *textView = self.textView;
    
    CGRect line = [textView caretRectForPosition:
                   textView.selectedTextRange.start];
    CGFloat overflow = line.origin.y + line.size.height
    - ( textView.contentOffset.y + textView.bounds.size.height
       - textView.contentInset.bottom - textView.contentInset.top );
    if ( overflow > 0 ) {
        // We are at the bottom of the visible text and introduced a line feed, scroll down (iOS 7 does not do it)
        // Scroll caret to visible area
        CGPoint offset = textView.contentOffset;
        offset.y += overflow + 7; // leave 7 pixels margin
        // Cannot animate with setContentOffset:animated: or caret will not appear
        [UIView animateWithDuration:.2 animations:^{
            [textView setContentOffset:offset];
        }];
    }
}

#pragma mark - UITextView

- (BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    return NO;
}

#pragma mark - UIWebView Delegate

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//    [SVProgressHUD dismiss];
    VLog(@"Finished Loading View");
}

-(void)webView:(UIWebView *)theWebView didFailLoadWithError:(NSError *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [SVProgressHUD dismiss];
	VLog(@"Error: %@", [error localizedDescription]);
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//    [SVProgressHUD showWithStatus:MSG_LOADING_MODULE maskType:SVProgressHUDMaskTypeGradient];
}

- (BOOL)webView:(UIWebView *) theWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSString *url = [[request URL] absoluteString];
    
    if ([[[request URL] scheme] isEqualToString:@"http"] || [[[request URL] scheme] isEqualToString:@"https"]) {
        NSRange range = [url rangeOfString:APP_SERVER options:NSCaseInsensitiveSearch];
        if(range.location == NSNotFound) {
            [[UIApplication sharedApplication] openURL:[request URL]];
            return NO;
        }
	}
    
    return YES;
}

-(void)dealloc{
    [self.webView stopLoading];
 	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.webView.delegate = nil;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //VLog(@"ReaderSheet - touchesBegan");
    NSLog(@"TOUCH BEGAN!");
}

@end
