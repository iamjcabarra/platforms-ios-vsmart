//
//  StudentTableViewCell.h
//  V-Smart
//
//  Created by Julius Abarra on 09/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblStudentName;
@property (strong, nonatomic) IBOutlet UILabel *lblContactNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblEmailAddress;
@property (strong, nonatomic) IBOutlet UIImageView *imgAvatar;

@end
