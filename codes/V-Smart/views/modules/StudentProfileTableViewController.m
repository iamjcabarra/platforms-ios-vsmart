//
//  StudentProfileTableViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 10/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "StudentProfileTableViewController.h"
#import "StudentProfileHeaderView.h"
#import "StudentBasicInfoTableViewCell.h"
#import "StudentEducationTableViewCell.h"
#import "StudentSocialMediaTableViewCell.h"
#import "CourseDataManager.h"
#import "UIImageView+WebCache.h"

@interface StudentProfileTableViewController ()

@property (strong, nonatomic) CourseDataManager *cm;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet StudentProfileHeaderView *profileHeaderView;
@property (strong, nonatomic) IBOutlet StudentBasicInfoTableViewCell *basicInformationCell;
@property (strong, nonatomic) IBOutlet StudentEducationTableViewCell *educationCell;
@property (strong, nonatomic) IBOutlet StudentSocialMediaTableViewCell *socialMediaCell;

@end

@implementation StudentProfileTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set navigation bar title
    self.title = NSLocalizedString(@"Student Information", nil);
    
    // Initialize table view section header and footer height
    self.tableView.sectionHeaderHeight = 0;
    self.tableView.sectionFooterHeight = 0;
    
    // Use singleton class as data manager
    self.cm = [CourseDataManager sharedInstance];
    self.managedObjectContext = self.cm.mainContext;
    
    // Localized strings
    self.basicInformationCell.lblNickname.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Nickname", nil)];
    self.basicInformationCell.lblEmail.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Email", nil)];
    self.basicInformationCell.lblBirthdate.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Birthdate", nil)];
    self.basicInformationCell.lblBirthPlace.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Birth Place", nil)];
    self.basicInformationCell.lblAddress.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Address", nil)];
    self.basicInformationCell.lblNationality.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Nationality", nil)];
    self.basicInformationCell.lblSpokenLanguages.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Spoken Language", nil)];
    self.basicInformationCell.lblHobbies.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Hobbies", nil)];
    self.basicInformationCell.lblSports.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Sports", nil)];
    self.educationCell.lblPreSchool.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Pre-School", nil)];
    self.educationCell.lblElementarySchool.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Elementary School", nil)];
    self.educationCell.lblHighSchool.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"High School", nil)];
    self.socialMediaCell.lblTwitter.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Twitter", nil)];
    self.socialMediaCell.lblFacebook.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Facebook", nil)];
    self.socialMediaCell.lblInstagram.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Instagram", nil)];
    
    // Access managed object for student profile
    NSManagedObject *moStudentProfile = [self.cm getEntity:@"StudentProfile" attribute:@"id" parameter:self.studentid context:self.managedObjectContext];
    
    if (moStudentProfile != nil) {
        NSString *first_name = [self.cm stringValue:[moStudentProfile valueForKey:@"first_name"]];
        NSString *last_name = [self.cm stringValue:[moStudentProfile valueForKey:@"last_name"]];
        NSString *user_type = [self.cm stringValue:[moStudentProfile valueForKey:@"user_type"]];
        NSString *nickname = [self.cm stringValue:[moStudentProfile valueForKey:@"nickname"]];
        NSString *email = [self.cm stringValue:[moStudentProfile valueForKey:@"email"]];
        NSString *birth_date = [self.cm stringValue:[moStudentProfile valueForKey:@"birth_date"]];
        NSString *birth_place = [self.cm stringValue:[moStudentProfile valueForKey:@"birth_place"]];
        NSString *address = [self.cm stringValue:[moStudentProfile valueForKey:@"address"]];
        NSString *nationality = [self.cm stringValue:[moStudentProfile valueForKey:@"nationality"]];
        NSString *spoken_language = [self.cm stringValue:[moStudentProfile valueForKey:@"spoken_language"]];
        NSString *hobbies = [self.cm stringValue:[moStudentProfile valueForKey:@"hobbies"]];
        NSString *sports = [self.cm stringValue:[moStudentProfile valueForKey:@"sports"]];
        NSString *pre_school_name = [self.cm stringValue:[moStudentProfile valueForKey:@"pre_school_name"]];
        NSString *elem_school_name = [self.cm stringValue:[moStudentProfile valueForKey:@"elem_school_name"]];
        NSString *high_school_name = [self.cm stringValue:[moStudentProfile valueForKey:@"high_school_name"]];
        NSString *twitter = [self.cm stringValue:[moStudentProfile valueForKey:@"twitter"]];
        NSString *facebook = [self.cm stringValue:[moStudentProfile valueForKey:@"facebook"]];
        NSString *instagram = [self.cm stringValue:[moStudentProfile valueForKey:@"instagram"]];
        
        self.basicInformationCell.lblActNickname.text = nickname;
        self.basicInformationCell.lblActEmail.text = email;
        self.basicInformationCell.lblActBirthdate.text = birth_date;
        self.basicInformationCell.lblActBirthPlace.text = birth_place;
        self.basicInformationCell.lblActAddress.text = address;
        self.basicInformationCell.lblActNationality.text = nationality;
        self.basicInformationCell.lblActSpokenLanguages.text = spoken_language;
        self.basicInformationCell.lblActHobbies.text = hobbies;
        self.basicInformationCell.lblActSports.text = sports;
        self.educationCell.lblActPreSchool.text = pre_school_name;
        self.educationCell.lblActElementarySchool.text = elem_school_name;
        self.educationCell.lblActHighSchool.text = high_school_name;
        self.socialMediaCell.lblActTwitter.text = twitter;
        self.socialMediaCell.lblActFacebook.text = facebook;
        self.socialMediaCell.lblActInstagram.text = instagram;
        
        // REFACTOR
        // jca-01-06-2016
        // Save profile picture to core data before rendering it to profile view
        
        /*
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        dispatch_async(queue, ^{
            [self.cm downloadProfileImage:moStudentProfile binaryBlock:^(NSData *binary) {
                if (binary) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.profileHeaderView.imgAvatar.image = [UIImage imageWithData:binary];
                        self.profileHeaderView.imgAvatar.layer.cornerRadius = 50.0f;
                    });
                }
            }];
        });
         */
        
        NSString *avatar_url = [self.cm stringValue:[moStudentProfile valueForKey:@"avatar"]];
        
        [self.profileHeaderView.imgAvatar sd_setImageWithURL:[NSURL URLWithString:avatar_url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if ((image == nil) || (error != nil)) {
                self.profileHeaderView.imgAvatar.image = [UIImage imageNamed:@"circled_student_male.png"];
            }
        }];
    
        self.profileHeaderView.lblStudentName.text = [NSString stringWithFormat:@"%@ %@", first_name, last_name];
        self.profileHeaderView.lblPosition.text = user_type;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0f;
}

@end
