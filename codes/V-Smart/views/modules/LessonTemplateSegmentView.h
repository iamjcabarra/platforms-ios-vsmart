//
//  LessonTemplateSegmentView.h
//  V-Smart
//
//  Created by Julius Abarra on 08/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonTemplateSegmentView : UIViewController

@property (strong, nonatomic) NSString *selectedContentStage;
@property (strong, nonatomic) NSManagedObject *copiedLessonObject;

- (void)swapEmbeddedViews:(NSString *)segueIdentifier;

@end
