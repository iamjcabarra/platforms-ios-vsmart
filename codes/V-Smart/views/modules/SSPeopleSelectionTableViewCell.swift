//
//  SSPeopleSelectionTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 29/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class SSPeopleSelectionTableViewCell: UITableViewCell {

    @IBOutlet var userImage: UIImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var userType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
