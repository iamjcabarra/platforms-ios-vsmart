//
//  SSPeopleCell.m
//  V-Smart
//
//  Created by VhaL on 3/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "SSPeopleCell.h"
#import "SSPeopleItem.h"
@interface SSPeopleCell ()

@end

@implementation SSPeopleCell
@synthesize avatar, name, alias;

-(void)initializeCell{
    
    avatar.layer.cornerRadius = avatar.frame.size.width/2;
//    avatar.layer.borderWidth = 1;
    avatar.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    alias.layer.cornerRadius = avatar.frame.size.width/2;
//    alias.layer.borderWidth = 1;
    alias.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    alias.backgroundColor = [SSPeopleItem generateRandomColor];
//    alias.backgroundColor = [UIColor redColor];
}

@end
