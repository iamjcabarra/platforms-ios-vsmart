//
//  UBDContentStage2ViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/25/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "UBDContentStage2ViewController.h"
#import "UBDContentManager.h"
#import "FileBrowserTableViewController.h"

@interface UBDContentStage2ViewController () <UITextViewDelegate, FileBrowserViewDelegate>

@property (strong, nonatomic) UBDContentManager *sharedUBDContentManager;

@property (strong, nonatomic) IBOutlet UILabel *lblContentTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblStage2P1;
@property (strong, nonatomic) IBOutlet UILabel *lblStage2P2;
@property (strong, nonatomic) IBOutlet UILabel *lblStage2P3;
@property (strong, nonatomic) IBOutlet UILabel *lblStage2P4;
@property (strong, nonatomic) IBOutlet UILabel *lblStage2P5;
@property (strong, nonatomic) IBOutlet UILabel *lblStage2P6;
@property (strong, nonatomic) IBOutlet UILabel *lblStage2P7;
@property (strong, nonatomic) IBOutlet UILabel *lblStage2P8;
@property (strong, nonatomic) IBOutlet UILabel *lblUploadFile;

@property (strong, nonatomic) IBOutlet UITextView *txtStage2P1;
@property (strong, nonatomic) IBOutlet UITextView *txtStage2P2;
@property (strong, nonatomic) IBOutlet UITextView *txtStage2P3;
@property (strong, nonatomic) IBOutlet UITextView *txtStage2P4;
@property (strong, nonatomic) IBOutlet UITextView *txtStage2P5;
@property (strong, nonatomic) IBOutlet UITextView *txtStage2P6;
@property (strong, nonatomic) IBOutlet UITextView *txtStage2P7;
@property (strong, nonatomic) IBOutlet UITextView *txtStage2P8;

@property (strong, nonatomic) IBOutlet UIButton *butDeleteFile;

@end

@implementation UBDContentStage2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Use singleton class shared instance
    self.sharedUBDContentManager = [UBDContentManager sharedInstance];
    
    // To dismiss keyboard when touching outside of text fields
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    // Set delegates
    self.txtStage2P1.delegate = self;
    self.txtStage2P2.delegate = self;
    self.txtStage2P3.delegate = self;
    self.txtStage2P4.delegate = self;
    self.txtStage2P5.delegate = self;
    self.txtStage2P6.delegate = self;
    self.txtStage2P7.delegate = self;
    self.txtStage2P8.delegate = self;
    
    // Create gesture recognizer for rubric text view
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadFile:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.txtStage2P8 addGestureRecognizer:tapGestureRecognizer];
    self.txtStage2P8.userInteractionEnabled = YES;
    
    // ENHANCEMENT#865
    // jca-01-11-2016
    // Add "Upload a file" label
    [self.lblUploadFile addGestureRecognizer:tapGestureRecognizer];
    self.lblUploadFile.userInteractionEnabled = YES;
    
    // Add an action to delete file button
    [self.butDeleteFile addTarget:self action:@selector(deleteFileAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self updateContentManager];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Get data from singleton class and render them to text fields
    self.txtStage2P1.text = [self.sharedUBDContentManager.contentStage objectForKey:@"8"];
    self.txtStage2P2.text = [self.sharedUBDContentManager.contentStage objectForKey:@"9"];
    self.txtStage2P3.text = [self.sharedUBDContentManager.contentStage objectForKey:@"10"];
    self.txtStage2P4.text = [self.sharedUBDContentManager.contentStage objectForKey:@"11"];
    self.txtStage2P5.text = [self.sharedUBDContentManager.contentStage objectForKey:@"12"];
    self.txtStage2P6.text = [self.sharedUBDContentManager.contentStage objectForKey:@"13"];
    self.txtStage2P7.text = [self.sharedUBDContentManager.contentStage objectForKey:@"14"];
    self.txtStage2P8.text = [self.sharedUBDContentManager.contentStage objectForKey:@"15"];
    
    // ENHANCEMENT#865
    // jca-01-11-2016
    // Add "Upload a file" label
    if ([self.txtStage2P8.text isEqualToString:@""]) {
        self.lblUploadFile.text = NSLocalizedString(@"Upload a file", nil);
    }
    else {
        self.lblUploadFile.text = @"";
    }
    
    // Keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Keyboard notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Handle Keyboard Movements

- (void)keyboardWillShow:(NSNotification *)notification {
    if ([self.txtStage2P7 isFirstResponder] || [self.txtStage2P8 isFirstResponder]) {
        CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        float newVerticalPosition = -keyboardSize.height;
        [self moveFrameToVerticalPosition:newVerticalPosition forDuration:0.3f];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [self moveFrameToVerticalPosition:0.0f forDuration:0.3f];
}

- (void)moveFrameToVerticalPosition:(float)position forDuration:(float)duration {
    CGRect frame = self.view.frame;
    frame.origin.y = position;
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Update Content Manager

- (void)updateContentManager {
    // Get input data from text fields
    NSString *stage2P1 = self.txtStage2P1.text;
    NSString *stage2P2 = self.txtStage2P2.text;
    NSString *stage2P3 = self.txtStage2P3.text;
    NSString *stage2P4 = self.txtStage2P4.text;
    NSString *stage2P5 = self.txtStage2P5.text;
    NSString *stage2P6 = self.txtStage2P6.text;
    NSString *stage2P7 = self.txtStage2P7.text;
    NSString *stage2P8 = self.txtStage2P8.text;
    
    // Set data to singleton class collection property
    [self.sharedUBDContentManager.contentStage setObject:stage2P1 forKey:@"8"];
    [self.sharedUBDContentManager.contentStage setObject:stage2P2 forKey:@"9"];
    [self.sharedUBDContentManager.contentStage setObject:stage2P3 forKey:@"10"];
    [self.sharedUBDContentManager.contentStage setObject:stage2P4 forKey:@"11"];
    [self.sharedUBDContentManager.contentStage setObject:stage2P5 forKey:@"12"];
    [self.sharedUBDContentManager.contentStage setObject:stage2P6 forKey:@"13"];
    [self.sharedUBDContentManager.contentStage setObject:stage2P7 forKey:@"14"];
    [self.sharedUBDContentManager.contentStage setObject:stage2P8 forKey:@"15"];
}

#pragma mark - Methods for Action Buttons

- (void)deleteFileAction:(id)sender {
    self.sharedUBDContentManager.contentStageFile = nil;
    self.txtStage2P8.text = @"";
    [self updateContentManager];
    
    // ENHANCEMENT#865
    // jca-01-11-2016
    // Add "Upload a file" label
    self.lblUploadFile.text = NSLocalizedString(@"Upload a file", nil);
}

#pragma mark - Text View Delegate

- (void)textViewDidChange:(UITextView *)textView {
    [self updateContentManager];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self moveFrameToVerticalPosition:0.0f forDuration:0.3f];
}

#pragma mark - Browse and Upload File {

- (void)uploadFile:(UITapGestureRecognizer *)tapGesture {
    [self performSegueWithIdentifier:@"showFileBrowser" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showFileBrowser"]) {
        FileBrowserTableViewController *fileBrowser = (FileBrowserTableViewController *)[segue destinationViewController];
        fileBrowser.delegate = self;
    }
}

#pragma mark - File Browser View Delegate

- (void)selectedFile:(NSDictionary *)d {
    NSLog(@"Selected File: %@", d);
    self.txtStage2P8.text = d[@"filename"];
    [self.sharedUBDContentManager.contentStageFile setObject:d forKey:@"15"];
    
    // ENHANCEMENT#865
    // jca-01-11-2016
    // Add "Upload a file" label
    if ([d[@"filename"] isEqualToString:@""]) {
        self.lblUploadFile.text = NSLocalizedString(@"Upload a file", nil);
    }
    else {
        self.lblUploadFile.text = @"";
    }
}

@end
