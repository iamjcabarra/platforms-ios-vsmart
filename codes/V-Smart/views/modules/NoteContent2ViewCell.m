//
//  NoteContent2ViewCell.m
//  V-Smart
//
//  Created by VhaL on 2/12/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NoteContent2ViewCell.h"

@implementation NoteContent2ViewCell
@synthesize delegate, cell1Label, textFieldTitle, cell5SaveButton, cell3TextViewDescription;

-(void)initializeCellWithDelegate:(id)parent{
    delegate = parent;
    self.textFieldTitle.delegate = self;
}

-(IBAction)textFieldDidChange:(UITextField *)textField{
//    JCA-08012017: Handle empty note title and description.
//    cell1Label.text = textField.text;
//    
//    if (0 < [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length){
//        cell5SaveButton.enabled = YES;
//    }
//    else{
//        if (0 < [cell3TextViewDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length){
//            cell5SaveButton.enabled = YES;
//        }
//        else{
//            cell5SaveButton.enabled = NO;
//        }
//    }
    
    cell1Label.text = textField.text;
    NSInteger titleLength = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length;
    NSInteger descriptionLength = [cell3TextViewDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length;
    cell5SaveButton.enabled = (titleLength > 0 && descriptionLength > 0) ? YES : NO;
}

@end
