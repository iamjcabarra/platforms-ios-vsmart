//
//  SocialStreamDataManager.swift
//  V-Smart
//
//  Created by Ryan Migallos on 26/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

// SINGLETON INSTANCE
class SocialStreamDataManager : Routes {
    
    // ParseProtocol
    var dateFormatter: DateFormatter! = DateFormatter()
    var userDefaults: UserDefaults! = UserDefaults.standard
    
    var isProductionMode: Bool = true
    // MARK: - Singleton Method
    
    var socket: SocketIOClient!
    
    static let sharedInstance: SocialStreamDataManager = {
        let singleton = SocialStreamDataManager()
        return singleton
    }()
    
    // MARK: - Private Properties
    
    let db = VSCoreDataStack(name:"SocialStreamModule")
    fileprivate let session = URLSession.shared
    
    typealias SocialStreamDoneBlock = (_ doneBlock: Bool) -> Void
    typealias SocialStreamDictionaryBlock = (_ dictionary: [String:AnyObject]?) -> Void
    typealias SocialStreamListBlock = (_ listBlock: [[String:AnyObject]]?) -> Void
    typealias SocialStreamDoneBlockWithError = (_ done: Bool, _ error: String?) -> Void
    
    // MARK: - Utility Methods (Non-Swift Dependency)
    fileprivate func deviceUUID() -> String {
        guard let uuid = Utils.deviceUUID() else { return "" }
        return "\(uuid)"
    }
    
    func accountUserID() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.id)"
    }
    
    func accountUserEmail() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.email!)"
    }
    
    func accountUserPosition() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.position!)"
    }
    
    fileprivate func accountUserUserName() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.email!)"
    }
    
    fileprivate func accountUserAvatar() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.avatar!)"
    }
    
    fileprivate func getContext() -> NSManagedObjectContext! {
        return self.db.objectContext()
    }
    
    func getMainContext() -> NSManagedObjectContext! {
        return self.db.objectMainContext()
    }
    
    fileprivate func groupsFor(_ userid:String) -> String {
        return "/vsmart-rest-dev/v1/stream/getusergroups/\(userid)"
    }
    
    fileprivate func groupsFor(_ userid:String,sectionid:String) -> String {
        return "/vsmart-rest-dev/v1/stream/getsubgroups/\(userid)/\(sectionid)"
    }
    
    fileprivate func membersForGroup(_ groupid: String) -> String {
        return "/vsmart-rest-dev/v1/stream/get-sub-group-members/\(groupid)"
    }
    
    fileprivate func studentsForSection(_ csid: String) -> String {
        return "/vsmart-rest-dev/v2/courses/studentsbycsid/\(csid)"
    }
    
    fileprivate func messagesFor(_ userid:String, groupid:String) -> String {
        return "/vsmart-rest-dev/v1/stream/getgroupmessages/\(userid)/\(groupid)"
    }
    
    fileprivate func socketParameters() -> [String:AnyObject] {
        
        let sectionList = self.fetchSectionIDs()
        let sectionData = try! JSONSerialization.data(withJSONObject: sectionList, options: JSONSerialization.WritingOptions(rawValue: 0) )
        let j_string = String(data: sectionData, encoding: String.Encoding.utf8)!
        //print("XXX j_string : <start>---- \(j_string) ---<end>")
        
        let userid = "\(accountUserID())"
        let username = "\(accountUserEmail())"
        let uuid = "\(deviceUUID())"
        let devicemodel = "\(UIDevice.current.model)"
        let platform = "ios"
        let moduletype = "social"
        
        let parameters:[String:AnyObject] = ["platform":platform as AnyObject,
                                             "type":moduletype as AnyObject,
                                             "device_id":uuid as AnyObject,
                                             "username":username as AnyObject,
                                             "user_id":userid as AnyObject,
                                             "model":devicemodel as AnyObject,
                                             "section_id":j_string as AnyObject]
        
        print("NODE: params: \(parameters)")
        
        return parameters
    }
    
    // MARK: - Socket IO
    func connectSocket(_ port:String) {
        let url = URL(string: "http://\(self.retrieveVSmartBaseURL()):\(port)")!
        
        print("RRR socket server : \(url)")
        
        let parameters = socketParameters()
        
        self.socket = SocketIOClient(socketURL: url, config: [.log(false), .forcePolling(true), .connectParams(parameters)])//SocketIOClient(socketURL: url, options: [.log(true), .forcePolling(true), .connectParams(parameters)] )
        self.socket.on("connect") { data, ack in
            print("socket client connected...")
        }
        
        // debugger
//        self.socket.onAny { print("Got event: \($0.event), with items: \($0.items!)") }
        
        
        //////////////////////////////////////////////////////
        // LISTENERS
        //////////////////////////////////////////////////////
        
        self.save(object: "1" as AnyObject!, forKey: "SS_NOTIFICATION_SOCKET_STATUS")
        
        // notification
        self.socket.on("notification") { (data, ack) in
            print("NOTIFICATION DAW :\(data)")
            self.handleNotificationKey(withData: data.last as AnyObject?)
        }
        
        self.socket.on("online") { (data, ack) in
            print("ONLINE USERS DAW :\(data)")
            self.handleOnlineKey(withData: data.last as AnyObject?)
        }
        
        // group create
        self.socket.on(SSMConstants.SocketIOListenerKey.GROUP_CREATE) { (data, ack) in
            print("GROUP CREATE DAW :\(data)")
            self.handleSocketIOListenerForKey(SSMConstants.SocketIOListenerKey.GROUP_CREATE, data: data as [AnyObject])
        }
        
        // group update
        self.socket.on(SSMConstants.SocketIOListenerKey.GROUP_UPDATE) { (data, ack) in
            print("GROUP UPDATE DAW :\(data)")
            self.handleSocketIOListenerForKey(SSMConstants.SocketIOListenerKey.GROUP_UPDATE, data: data as [AnyObject])
        }
        
        // group remove
        self.socket.on(SSMConstants.SocketIOListenerKey.GROUP_REMOVE) { (data, ack) in
            print("GROUP REMOVE DAW :\(data)")
            self.handleSocketIOListenerForKey(SSMConstants.SocketIOListenerKey.GROUP_REMOVE, data: data as [AnyObject])
        }
        
        // group member add
        self.socket.on(SSMConstants.SocketIOListenerKey.GROUP_MEMBER_ADD) { (data, ack) in
            print("GROUP MEMBER ADD DAW :\(data)")
            self.handleSocketIOListenerForKey(SSMConstants.SocketIOListenerKey.GROUP_MEMBER_ADD, data: data as [AnyObject])
        }
        
        // group member remove
        self.socket.on(SSMConstants.SocketIOListenerKey.GROUP_MEMBER_REMOVE) { (data, ack) in
            print("GROUP MEMBER REMOVE DAW :\(data)")
            self.handleSocketIOListenerForKey(SSMConstants.SocketIOListenerKey.GROUP_MEMBER_REMOVE, data: data as [AnyObject])
        }
        
        self.socket.on(SSMConstants.SocketIOListenerKey.SOCKET_ERROR) { (data, ack) in
            self.save(object: "0" as AnyObject!, forKey: "SS_NOTIFICATION_SOCKET_STATUS")
        }
        
        if self.socket.status == .disconnected || self.socket.status == .notConnected {
        self.socket.connect()
    }
    }
    
    func disconnectSocket() {
        if self.socket != nil {
        self.socket.disconnect()
    }
    }
    
    fileprivate func handleNotificationKey(withData data:AnyObject?) {
        
        if data is [String:AnyObject] {
            let users = data!["users"] as? [[String:AnyObject]]
            
            guard users != nil else {
                return
            }
            
            for data in users! {
                
                let type: String! = self.stringValue(data["type"])
                
                if (type == "message") {
                    let dataIsNeeded = self.checkIfNeeded(data: data)
                    dataIsNeeded ? self.handleMessage(withData: data) : self.updateGroupNotification(withData: data)
                }
                
                if (type == "comment") {
                    self.handleComment(withData: data)
                }
                
                if (type == "message_likes") {
                    self.handleMessageLike(withData: data)
                }
                
                if (type == "comment_likes") {
                    self.handleCommentLike(withData: data)
                }
            }
        }
    }
    
    fileprivate func handleOnlineKey(withData data:AnyObject?) {
        
        if data is [String:AnyObject] {
            let users = data!["users"] as? [[String:AnyObject]]
            
            guard users != nil else {
                return
            }
            
            //            for data in users! {
            //
            //                let type = self.stringValue(data["type"])
            //
            //                if(type == "social") {
            self.handleOnline(withUsers: users)
            //                }
            //            }
        }
    }
    
    fileprivate func handleOnline(withUsers users: [[String:AnyObject]]!) {
        print("ONLINE HANDLED!!!!!!!!!!!!")
        guard users != nil else {
            return
        }
        
        var onlineUsers: Set<String> = Set<String>()
        var modelDictionary: [String:String] = [String:String]()
        for data in users {
            let user_id: String! = self.stringValue(data["user_id"])
            let model: String! = self.stringValue(data["model"])
            
            modelDictionary[user_id!] = model
            onlineUsers.insert(user_id!)
        }
        
        let entity = SSMConstants.Entity.MESSAGEFEED
        // fetch all
        guard let feedObjects = self.db.retrieveObjects(forEntity: entity, withFilter: nil) else {
            return
        }
        
        let ctx = self.getContext()
        
        ctx?.performAndWait({
            for feedObject in feedObjects {
                let fo_userid: String! = self.stringValue(feedObject.value(forKey: "user_id") as AnyObject?)
                
                var deviceModel: String! = ""
                var is_online = "0"
                if onlineUsers.contains(fo_userid!) {
                    deviceModel = self.stringValue(modelDictionary[fo_userid!])
                    is_online = "1"
                }
                
                feedObject.setValue(is_online, forKey: "is_online")
                feedObject.setValue(deviceModel, forKey: "device_model")
            }
        })
        
        _ = self.db.saveObjectContext(ctx!)
    }
    
    fileprivate func updateGroupNotification(withData data: [String: AnyObject]?) {
        self.prettyFunction()
        
        guard data != nil else {
            print("Can't update group notification!")
            return
        }
        
        let ctx = self.getContext()
        let selected_group_id: String! = self.stringValue(self.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID"))
        let group_id: String! = self.stringValue(data!["sub_group_id"])
        
        if selected_group_id != group_id {
            let predicate = NSComparisonPredicate(keyPath: "id", withValue: group_id, isExact: true)
            
            guard let group = self.db.retrieveEntity(SSMConstants.Entity.GROUP, filter: predicate) else {
                print("Can't update group notification (group)!")
                return
            }
            
            guard let count = group.value(forKey: "notification_count") as? Int else {
                print("Can't update group notification (count)!")
                return
            }
            
            ctx?.performAndWait({
                group.setValue(count + 1, forKey: "notification_count")
                _ = self.db.saveObjectContext(ctx!)
            })
        }
    }
    
    fileprivate func handleMessage(withData data: [String:AnyObject]?) {
        print("MESSAGE HANDLED!!!!!!!!!!!!")
        guard data != nil else {
            return
        }
        
        let homeUrl = "http://\(retrieveVSmartBaseURL())"
        
        let entity = SSMConstants.Entity.MESSAGEFEED
        let ctx = self.getContext()
        
        ctx?.performAndWait({
            /*
             {
             avatar = "/uploads/2/avatars/20160711/2e3c0708549d5f069699bae3ae11506f.png";
             "b_title" = "<null>";
             canonicalUrl = "";
             "date_created" = "2016-08-02 09:07:37 togo";
             "date_modified" = "2016-08-02 09:07:37 togo";
             description = "N/A";
             "emoticon_id" = "<null>";
             "first_name" = Web;
             "group_id" = 2;
             "icon_text" = "<null>";
             "icon_url" = "<null>";
             id = 265;
             image = no;
             "is_deleted" = 0;
             "is_image" = no;
             "is_message" = 1;
             "is_video" = no;
             "last_name" = "Teacher 1";
             message = "Idol ko si Elmer. ";
             "msg_type" = message;
             "parent_id" = "<null>";
             "sub_group_id" = 0;
             text = " Idol ko si Elmer. ";
             "thumb_img" = no;
             title = "N/A";
             type = message;
             url = "";
             "user_id" = 2;
             }
             */
            
            let message_id: String! = self.stringValue(data!["id"])
            let is_deleted: String! = self.stringValue(data!["is_deleted"])
            
            let predicate = self.db.createPredicate(key:"id", withValue: message_id!, isExact: true)
            if is_deleted == "1" {
                _ = self.db.clearEntity(entity, ctx: ctx!, filter: predicate)
                return
            }
            
            let mo = self.db.retrieveEntity(entity, context: ctx!, filter: predicate)
            
            let user_id: String! = self.stringValue(data!["user_id"])
            let group_id: String! = self.stringValue(data!["group_id"])
            let emoticon_id: String! = self.stringValue(data!["emoticon_id"])
            var message: String! = self.stringValue(data!["message"])
            let b_title: String! = self.stringValue(data!["b_title"])
            let is_attached: String! = self.stringValue(data!["is_attached"])
            let date_created: String! = self.stringValue(data!["date_created"])
            let date_modified: String! = self.stringValue(data!["date_modified"])
            let username: String! = self.stringValue(data!["username"])
            let avatar_image: String! = self.stringValue(data!["avatar"])
            var avatar: String! = "\(homeUrl)\(avatar_image!)"
            let attach_count: String! = self.stringValue(data!["attachCount"])
            let current_date: String! = self.stringValue(data!["current_date"])
            var icon_url: String! = self.stringValue(data!["icon_url"])
            let icon_text: String! = self.stringValue(data!["icon_text"])
            let first_name: String! = self.stringValue(data!["first_name"])
            let last_name: String! = self.stringValue(data!["last_name"])
            var type: String! = self.stringValue(data!["msg_type"])
            
            
            if type == "sticker" {
                message = message?.replacingOccurrences(of: "<img src='", with: "", options: .regularExpression, range: nil)
                message = message?.replacingOccurrences(of: "'/>", with: "", options: .regularExpression, range: nil)
                
                //self.stripHTLMTags(inString: message)
                
                message = (message?.contains(homeUrl))! ? message : "\(homeUrl)\(message!)"
            }
            
            // PARSE URL DATA
            var url_data = Set<NSManagedObject>()
            
            
            if type == "image" || type == "video" {
                type = "link"
                let urlDataObject = NSEntityDescription.insertNewObject(forEntityName: SSMConstants.Entity.URLDATA, into: ctx!)
                
                
                let text: String! = self.stringValue(data!["text"])
                let image: String! = self.stringValue(data!["image"])
                var thumb_img: String! = self.stringValue(data!["thumb_img"])
                let title: String! = self.stringValue(data!["title"])
                let canonical_url: String! = self.stringValue(data!["canonicalUrl"])
                let url: String! = self.stringValue(data!["url"])
                let description: String! = self.stringValue(data!["description"])
                
                thumb_img = (thumb_img?.contains(homeUrl))! ? thumb_img : "\(homeUrl)\(thumb_img!)"
                
                urlDataObject.setValue(text, forKey: "text")
                urlDataObject.setValue(image, forKey: "image")
                urlDataObject.setValue(thumb_img, forKey: "thumb_img")
                urlDataObject.setValue(title, forKey: "title")
                urlDataObject.setValue(canonical_url, forKey: "canonical_url")
                urlDataObject.setValue(url, forKey: "url")
                urlDataObject.setValue(description, forKey: "desc")
                
                url_data.insert(urlDataObject)
            }
            
            
            icon_url = (icon_url?.contains(homeUrl))! ? icon_url : "\(homeUrl)\(icon_url!)"
            avatar = (avatar.contains(homeUrl)) ? avatar : "\(homeUrl)\(avatar)"
            
            let date_modified_date = self.date(fromString: date_modified!, withFormat: "yyyy-MM-dd HH:mm:ss 'togo'", convertToLocalTime: true)
            
            mo.setValue(message_id, forKey: "id")
            mo.setValue(user_id, forKey: "user_id")
            mo.setValue(group_id, forKey: "group_id")
            mo.setValue(emoticon_id, forKey: "emoticon_id")
            mo.setValue(message, forKey: "message")
            mo.setValue(b_title, forKey: "b_title")
            mo.setValue(is_attached, forKey: "is_attached")
            mo.setValue(is_deleted, forKey: "is_deleted")
            mo.setValue(date_created, forKey: "date_created")
            mo.setValue(date_modified, forKey: "date_modified")
            mo.setValue(username, forKey: "username")
            mo.setValue(avatar, forKey: "avatar")
            mo.setValue(attach_count, forKey: "attach_count")
            mo.setValue(current_date, forKey: "current_date")
            mo.setValue(icon_url, forKey: "icon_url")
            mo.setValue(icon_text, forKey: "icon_text")
            mo.setValue(first_name, forKey: "first_name")
            mo.setValue(last_name, forKey: "last_name")
            mo.setValue(date_modified_date, forKey: "date_modified_date")
            mo.setValue(type, forKey: "type")
            mo.setValue("0", forKey: "like_count")
            mo.setValue("0", forKey: "comment_count")
            
            // URL_DATA RELATIONSHIP
            mo.setValue(url_data, forKey: "url_data")
            
            _ = self.db.saveObjectContext(ctx!)
        })
    }
    
    fileprivate func handleMessageLike(withData data: [String:AnyObject]?) {
        print("MESSAGE LIKE HANDLED!!!!!!!!!!!!")
        guard data != nil else {
            return
        }
        
        let feed_entity = SSMConstants.Entity.MESSAGEFEED
        let like_entity = SSMConstants.Entity.LIKES
        
        let content_id: String! = self.stringValue(data!["parent_id"])
        let predicate = NSComparisonPredicate(keyPath: "id", withValue: content_id)
        guard let messageObject = self.db.retrieveEntity(feed_entity, filter: predicate) else {
            return
        }
        
        let homeUrl = "http://\(retrieveVSmartBaseURL())"
        
        let ctx = self.getContext()
        
        ctx?.performAndWait({
            /*
             avatar = "/uploads/38/avatars/20160719/d8798c0a22b87088ef0179fdf0447dcd.jpg";
             "b_title" = "<null>";
             canonicalUrl = "<null>";
             "date_created" = "2016-08-02 09:38:25 togo";
             "date_modified" = "2016-08-02 09:38:25 togo";
             description = "<null>";
             "emoticon_id" = "<null>";
             "first_name" = Windows;
             "group_id" = "<null>";
             "icon_text" = "<null>";
             "icon_url" = "<null>";
             id = 145;
             image = "<null>";
             "is_deleted" = 0;
             "is_image" = "<null>";
             "is_message" = "<null>";
             "is_video" = "<null>";
             "last_name" = "Teacher 1";
             message = "<null>";
             "msg_type" = "<null>";
             "parent_id" = 245; /// MESSSAGE ID!!!!!!!
             "sub_group_id" = "<null>";
             text = "<null>";
             "thumb_img" = "<null>";
             title = "<null>";
             type = "message_likes";
             url = "<null>";
             "user_id" = 38;
             */
            
            
            let like_id: String! = self.stringValue(data!["id"])
            let user_id: String! = self.stringValue(data!["user_id"])
            let is_deleted: String! = self.stringValue(data!["is_deleted"])
            let date_created: String! = self.stringValue(data!["date_created"])
            let date_modified: String! = self.stringValue(data!["date_modified"])
            var avatar: String! = self.stringValue(data!["avatar"])
            let first_name: String! = self.stringValue(data!["first_name"])
            let last_name: String! = self.stringValue(data!["last_name"])
            
            
            avatar = (avatar?.contains(homeUrl))! ? avatar : "\(homeUrl)\(avatar!)"
            
            let date_modified_date = self.date(fromString: date_modified!, withFormat: "yyyy-MM-dd HH:mm:ss 'togo'")
            
            var likes = messageObject.value(forKey: "likes") as! Set<NSManagedObject>
            
            let loggedInUserID = self.accountUserID()
            
            let arrayOfFilteredLikes = likes.filter({
                //                ((self.stringValue($0.valueForKey("user_id"))) == user_id && (
                //                    (self.stringValue($0.valueForKey("message_id"))) == content_id) &&
                (self.stringValue($0.value(forKey: "like_id"))) == like_id// additional
            })
            
            let userIDFilteredLikes = likes.filter({
                (self.stringValue($0.value(forKey: "user_id"))) == loggedInUserID// additional
            })
            
            
            var is_liked = "0"
            
            if is_deleted == "1" {
                // DELETE!!
                if arrayOfFilteredLikes.count > 0 {
                    // REMOVE!!!!
                    print("UNLIKE~~~~~~~~~~")
                    likes.remove(arrayOfFilteredLikes.last!)
                    ctx?.delete(arrayOfFilteredLikes.last!)
                    
                    if userIDFilteredLikes.count > 0 {
                        is_liked = "1"
                    }
                    
                    if loggedInUserID == user_id {
                        is_liked = "0"
                    }
                    
                }
            }
            
            if is_deleted == "0" {
                if loggedInUserID == user_id {
                    is_liked = "1"
                }
                
                if userIDFilteredLikes.count > 0 {
                    is_liked = "1"
                }
                
                if arrayOfFilteredLikes.count == 0 {
                    // ADD!!!!!!!
                    
                    print("LIKE~~~~~~~~~~")
                    let likeObject = NSEntityDescription.insertNewObject(forEntityName: like_entity, into: ctx!)
                    likeObject.setValue(like_id, forKey: "like_id")
                    likeObject.setValue(user_id, forKey: "user_id")
                    likeObject.setValue(content_id, forKey: "message_id")
                    likeObject.setValue(is_deleted, forKey: "is_deleted")
                    likeObject.setValue(date_created, forKey: "date_created")
                    likeObject.setValue(date_modified, forKey: "date_modified")
                    likeObject.setValue(avatar, forKey: "avatar")
                    likeObject.setValue(first_name, forKey: "first_name")
                    likeObject.setValue(last_name, forKey: "last_name")
                    likeObject.setValue(date_modified_date, forKey: "date_modified_date")
                    
                    likes.insert(likeObject)
                }
            }
            
            let like_count: String! = self.stringValue(likes.count)
            // LIKE RELATIONSHIP
            messageObject.setValue(like_count, forKey: "like_count")
            messageObject.setValue(likes, forKey: "likes")
            messageObject.setValue(is_liked, forKey: "is_liked")
            
            _ = self.db.saveObjectContext(ctx!)
        })
    }
    
    fileprivate func handleComment(withData data: [String:AnyObject]?) {
        print("MESSAGE COMMENT HANDLED!!!!!!!!!!!!")
        guard data != nil else {
            return
        }
        
        let feed_entity = SSMConstants.Entity.MESSAGEFEED
        let comment_entity = SSMConstants.Entity.COMMENTS
        
        let message_id: String! = self.stringValue(data!["parent_id"])
        let predicate = NSComparisonPredicate(keyPath: "id", withValue: message_id)
        guard let messageObject = self.db.retrieveEntity(feed_entity, filter: predicate) else {
            return
        }
        
        let ctx = self.getContext()
        let homeUrl = "http://\(retrieveVSmartBaseURL())"
        ctx?.performAndWait({
            /*
             {
             avatar = "/uploads/38/avatars/20160719/d8798c0a22b87088ef0179fdf0447dcd.jpg";
             "b_title" = "<null>";
             canonicalUrl = "<null>";
             "date_created" = "2016-08-02 03:56:58 togo";
             "date_modified" = "2016-08-03 03:00:20 togo";
             description = "<null>";
             "emoticon_id" = "<null>";
             "first_name" = Windows;
             "group_id" = "<null>";
             "icon_text" = "<null>";
             "icon_url" = "<null>";
             id = 88;
             image = "<null>";
             "is_deleted" = 1;
             "is_image" = "<null>";
             "is_message" = "<null>";
             "is_video" = "<null>";
             "last_name" = "Teacher 1";
             message = test;
             "msg_type" = "<null>";
             "parent_id" = 245;
             "sub_group_id" = "<null>";
             text = "<null>";
             "thumb_img" = "<null>";
             title = "<null>";
             type = comment;
             url = "<null>";
             "user_id" = 38;
             }
             */
            
            let comment_id: String! = self.stringValue(data!["id"])
            let is_deleted: String! = self.stringValue(data!["is_deleted"])
            
            //            let predicate = self.db.createPredicate(key:"comment_id", withValue: comment_id, isExact: true)
            //            if is_deleted == "1" {
            //                self.db.clearEntity(comment_entity, ctx: ctx, filter: predicate)
            //                return
            //            }
            
            let user_id: String! = self.stringValue(data!["user_id"])
            let comment: String! = self.stringValue(data!["message"])
            let date_created: String! = self.stringValue(data!["date_created"])
            let date_modified: String! = self.stringValue(data!["date_modified"])
            var avatar: String! = self.stringValue(data!["avatar"])
            let first_name: String! = self.stringValue(data!["first_name"])
            let last_name: String! = self.stringValue(data!["last_name"])
            
            avatar = (avatar?.contains(homeUrl))! ? avatar : "\(homeUrl)\(avatar!)"
            
            let date_created_date = self.date(fromString: date_created!, withFormat: "yyyy-MM-dd HH:mm:ss 'togo'")
            let date_modified_date = self.date(fromString: date_modified!, withFormat: "yyyy-MM-dd HH:mm:ss 'togo'")
            
            let is_edited: String! = (date_created_date == date_modified_date) ? "0" : "1"
            
            var comments = messageObject.value(forKey: "comments") as! Set<NSManagedObject>
            
            let arrayOfFilteredComment = comments.filter({
                ((self.stringValue($0.value(forKey: "user_id"))) == user_id && (
                    (self.stringValue($0.value(forKey: "comment_id"))) == comment_id))
            })
            
            /* PAAAAANO KO MALALAMAN KUNG:
             COMMENT EDIT- check if comment id exist, then update
             COMMENT POST- check if comment id does not exist, then post
             COMMENT DELETE- is_deleted == 1, then delete
             */
            
            if arrayOfFilteredComment.count > 0 {
                
                let commentObject = arrayOfFilteredComment.last!
                
                if is_deleted == "1" {
                    //DELETE!!!
                    print("COMMENT DELETED~~~~~~~~~")
                    if comments.contains(commentObject) {
                        comments.remove(commentObject)
                        ctx?.delete(commentObject)
                    }
                    
                } else if is_deleted == "0" {
                    //EDIT!!!
                    print("COMMENT EDITTED~~~~~~~~~")
                    commentObject.setValue(comment_id, forKey: "comment_id")
                    commentObject.setValue(user_id, forKey: "user_id")
                    commentObject.setValue(message_id, forKey: "message_id")
                    commentObject.setValue(comment, forKey: "comment")
                    commentObject.setValue(is_deleted, forKey: "is_deleted")
                    commentObject.setValue(date_created, forKey: "date_created")
                    commentObject.setValue(date_modified, forKey: "date_modified")
                    commentObject.setValue(avatar, forKey: "avatar")
                    commentObject.setValue(first_name, forKey: "first_name")
                    commentObject.setValue(last_name, forKey: "last_name")
                    commentObject.setValue(date_created_date, forKey: "date_created_date")
                    commentObject.setValue(is_edited, forKey: "is_edited")
                    
                    comments.insert(commentObject)
                }
            }
            
            if is_deleted == "0" {
                if arrayOfFilteredComment.count == 0 {
                    // INSERT
                    print("COMMENT INSERTED~~~~~~~~~")
                    let commentObject = NSEntityDescription.insertNewObject(forEntityName: comment_entity, into: ctx!)
                    
                    commentObject.setValue(comment_id, forKey: "comment_id")
                    commentObject.setValue(user_id, forKey: "user_id")
                    commentObject.setValue(message_id, forKey: "message_id")
                    commentObject.setValue(comment, forKey: "comment")
                    commentObject.setValue(is_deleted, forKey: "is_deleted")
                    commentObject.setValue(date_created, forKey: "date_created")
                    commentObject.setValue(date_modified, forKey: "date_modified")
                    commentObject.setValue(avatar, forKey: "avatar")
                    commentObject.setValue(first_name, forKey: "first_name")
                    commentObject.setValue(last_name, forKey: "last_name")
                    commentObject.setValue(date_created_date, forKey: "date_created_date")
                    commentObject.setValue(is_edited, forKey: "is_edited")
                    commentObject.setValue(date_modified_date, forKey: "date_modified_date")
                    
                    comments.insert(commentObject)
                }
            }
            
            let comment_count: String! = self.stringValue(comments.count)
            // COMMENT RELATIONSHIP
            messageObject.setValue(comment_count, forKey: "comment_count")
            messageObject.setValue(comments, forKey: "comments")
            
            _ = self.db.saveObjectContext(ctx!)
            
        })
    }
    
    fileprivate func handleCommentLike(withData data: [String:AnyObject]?) {
        print("COMMENT LIKE HANDLED!!!!!!!!!!!!")
        guard data != nil else {
            return
        }
        
        let comment_entity = SSMConstants.Entity.COMMENTS
        let comment_like_entity = SSMConstants.Entity.COMMENT_LIKES
        
        let content_id: String! = self.stringValue(data!["parent_id"])
        let predicate = NSComparisonPredicate(keyPath: "comment_id", withValue: content_id)
        guard let commentObject = self.db.retrieveEntity(comment_entity, filter: predicate) else {
            return
        }
        
        let ctx = self.getContext()
        
        let homeUrl = "http://\(retrieveVSmartBaseURL())"
        
        ctx?.performAndWait({
            /*
             {
             avatar = "/uploads/2/avatars/20160711/2e3c0708549d5f069699bae3ae11506f.png";
             "b_title" = "<null>";
             canonicalUrl = "<null>";
             "date_created" = "2016-08-03 05:41:31 togo";
             "date_modified" = "2016-08-03 05:41:31 togo";
             description = "<null>";
             "emoticon_id" = "<null>";
             "first_name" = Web;
             "group_id" = "<null>";
             "icon_text" = "<null>";
             "icon_url" = "<null>";
             id = 158;
             image = "<null>";
             "is_deleted" = 0;
             "is_image" = "<null>";
             "is_message" = "<null>";
             "is_video" = "<null>";
             "last_name" = "Teacher 1";
             message = "<null>";
             "msg_type" = "<null>";
             "parent_id" = 101;
             "sub_group_id" = "<null>";
             text = "<null>";
             "thumb_img" = "<null>";
             title = "<null>";
             type = "comment_likes";
             url = "<null>";
             "user_id" = 2;
             }
             */
            
            let comment_like_id: String! = self.stringValue(data!["id"])
            let user_id: String! = self.stringValue(data!["user_id"])
            let is_deleted: String! = self.stringValue(data!["is_deleted"])
            let date_created: String! = self.stringValue(data!["date_created"])
            let date_modified: String! = self.stringValue(data!["date_modified"])
            var avatar: String! = self.stringValue(data!["avatar"])
            let first_name: String! = self.stringValue(data!["first_name"])
            let last_name: String! = self.stringValue(data!["last_name"])
            
            avatar = (avatar?.contains(homeUrl))! ? avatar : "\(homeUrl)\(avatar!)"
            
            let date_modified_date = self.date(fromString: date_modified!, withFormat: "yyyy-MM-dd HH:mm:ss 'togo'")
            
            var comment_like = commentObject.value(forKey: "comment_like") as! Set<NSManagedObject>
            let loggedInUserID = self.accountUserID()
            
            let arrayOfFilteredLikes = comment_like.filter({
                ((self.stringValue($0.value(forKey: "user_id"))) == user_id && (
                    (self.stringValue($0.value(forKey: "content_id"))) == content_id) &&
                    (self.stringValue($0.value(forKey: "comment_like_id"))) == comment_like_id)// additional
            })
            
            let userIDFilteredLikes = comment_like.filter({
                (self.stringValue($0.value(forKey: "user_id"))) == loggedInUserID// additional
            })
            
            var is_liked = "0"
            
            if is_deleted == "1" {
                if arrayOfFilteredLikes.count > 0 {
                    // REMOVE!!!!
                    print("UNLIKE~~~~~~~~~~")
                    comment_like.remove(arrayOfFilteredLikes.last!)
                    ctx?.delete(arrayOfFilteredLikes.last!)
                    
                    if userIDFilteredLikes.count > 0 {
                        is_liked = "1"
                    }
                    
                    if loggedInUserID == user_id {
                        is_liked = "0"
                    }
                }
            }
            
            if is_deleted == "0" {
                if loggedInUserID == user_id {
                    is_liked = "1"
                }
                
                if userIDFilteredLikes.count > 0 {
                    is_liked = "1"
                }
                
                if arrayOfFilteredLikes.count == 0 {
                    // ADD!!!!!!!
                    print("LIKE~~~~~~~~~~")
                    let commentLikeObject = NSEntityDescription.insertNewObject(forEntityName: comment_like_entity, into: ctx!)
                    commentLikeObject.setValue(comment_like_id, forKey: "comment_like_id")
                    commentLikeObject.setValue(user_id, forKey: "user_id")
                    commentLikeObject.setValue(content_id, forKey: "content_id")
                    commentLikeObject.setValue(is_deleted, forKey: "is_deleted")
                    commentLikeObject.setValue(date_created, forKey: "date_created")
                    commentLikeObject.setValue(date_modified, forKey: "date_modified")
                    commentLikeObject.setValue(avatar, forKey: "avatar")
                    commentLikeObject.setValue(first_name, forKey: "first_name")
                    commentLikeObject.setValue(last_name, forKey: "last_name")
                    commentLikeObject.setValue(date_modified_date, forKey: "date_modified_date")
                    
                    comment_like.insert(commentLikeObject)
                }
            }
            
            let comment_like_count: String! = self.stringValue(comment_like.count)
            // COMMENT RELATIONSHIP
            commentObject.setValue(comment_like_count, forKey: "comment_like_count")
            commentObject.setValue(comment_like, forKey: "comment_like")
            commentObject.setValue(is_liked, forKey: "is_liked")
            
            _ = self.db.saveObjectContext(ctx!)
        })
    }
    
    func checkIfNeeded(data:[String:AnyObject]) -> Bool {
        
        let selectedSectionID: String! = self.stringValue(self.fetchUserDefaultsObject(forKey: "SS_SELECTED_SECTION_ID"))
        let selectedGroupID: String! = self.stringValue(self.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID"))
        
        let group_id: String! = self.stringValue(data["group_id"])// SECTION
        let sub_group_id: String! = self.stringValue(data["sub_group_id"]) // GROUP
        
        
        var isNeeded = false
        if group_id == selectedSectionID {
            if sub_group_id == selectedGroupID {//|| selectedGroupID == "0" {
                isNeeded = true
            }
        }
        
        return isNeeded
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Message to Notes
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func saveNote(_ message:String, handler:SocialStreamDoneBlock) {
        let title_string = self.parseTitleFrom(message)
        // FOLDER NAME
        let nm = NoteDataManager.sharedInstance()
        let folder = nm?.getNoteBook(byNoteBookName: "General", useMainThread: true)
        let entry = NoteEntry()
        entry.title = "\(title_string)"
        entry.description = "\(message)"
        entry.tags = "Social Stream" //default
        entry.noteBookName = folder?.noteBookName
        entry.color = 5
        entry.fontTypeId = 0
        entry.fontSizeId = 10
        entry.isArchive = 0
        _ = nm?.addNote(entry, useMainThread: true)
        
        handler(true)
    }
    
    fileprivate func trimWhiteSpacesFor(_ string: String) -> String {
        return string.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    fileprivate func parseTitleFrom(_ content:String) -> String {
        var titleString = ""
        let string = self.trimWhiteSpacesFor(content)
        let array = string.components(separatedBy: CharacterSet.whitespaces)
        let filteredText = array.filter() { $0 == "" }
        if filteredText.count == 1 {
            titleString = filteredText.first!
        }
        
        if filteredText.count > 1 {
            titleString = "\(filteredText[0]) \(filteredText[1])"
        }
        return titleString
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    // MARK: - Network Methods (Public)
    
    func requestGroup(_ handler:SocialStreamDoneBlockWithError?) {
        
        let userid = accountUserID()
        let endpoint = groupsFor(userid)
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("GET", url: url, andBody: nil)
        
        let ctx = self.getContext()
        _ = self.db.clearEntity(SSMConstants.Entity.MESSAGEFEED, ctx: ctx!, filter: nil)
        _ = self.db.clearEntity(SSMConstants.Entity.LIKES, ctx: ctx!, filter: nil)
        _ = self.db.clearEntity(SSMConstants.Entity.COMMENTS, ctx: ctx!, filter: nil)
        _ = self.db.clearEntity(SSMConstants.Entity.COMMENT_LIKES, ctx: ctx!, filter: nil)
        _ = self.db.clearEntity(SSMConstants.Entity.URLDATA, ctx: ctx!, filter: nil)
        _ = self.db.clearEntity(SSMConstants.Entity.GROUP, ctx: ctx!, filter: nil)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                if handler != nil {
                    handler!(false, e.localizedDescription)
                }
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else {
                if handler != nil {
                    handler!(false, NSLocalizedString("You are not assigned to any section", comment: ""))
                }
                return
            }
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [[String:AnyObject]] else {
                    if handler != nil {
                        handler!(false, NSLocalizedString("You are not assigned to any section", comment: ""))
                    }
                    return
                }
                
                
                ctx?.performAndWait({
                    
                    let filter:NSPredicate? = nil
                    let status = self.db.clearEntity(SSMConstants.Entity.SECTION, ctx: ctx!, filter: filter)
                    if status == true {
                        
                        if records.count > 0 {
                        for r in records {
                            print("item group: \(r)")
                            /*
                             {
                             id = 100;
                             "group_id" = 18;
                             "date_created" = "2016-05-16 05:19:50";
                             "date_modified" = "2016-05-16 05:19:50";
                             name = "Araling Panlipunan 4";
                             section = 123;
                             "user_id" = 2;
                             }
                             */
                            let id: String! = self.stringValue(r["id"])
                            let group_id: String! = self.stringValue(r["group_id"])
                            let date_created: String! = self.stringValue(r["date_created"])
                            let date_modified = self.stringValue(r["date_modified"])
                            let name: String! = self.stringValue(r["name"])
                            let section: String! = self.stringValue(r["section"])
                            let user_id: String! = self.stringValue(r["user_id"])
                            
                            let predicate = self.db.createPredicate(key:"group_id", withValue: group_id!, isExact: true)
                            //                        let predicate = NSComparisonPredicate(keyPath: "group_id", withValue: group_id, isExact: true)
                            let mo = self.db.retrieveEntity(SSMConstants.Entity.SECTION, context: ctx!, filter: predicate)
                            
                            mo.setValue(id, forKey: "id")
                            mo.setValue(group_id, forKey: "group_id")
                            mo.setValue(date_created, forKey: "date_created")
                            mo.setValue(date_modified, forKey: "date_modified")
                            mo.setValue(name, forKey: "name")
                            mo.setValue(section, forKey: "section")
                            mo.setValue(user_id, forKey: "user_id")
                        }
                        _ = self.db.saveObjectContext(ctx!)
                        //                        self.db.displayRecords(SSMConstants.Entity.SECTION)
                            if handler != nil {
                                handler!(true, nil)
                            }
                        } else {
                            if handler != nil {
                                handler!(false, NSLocalizedString("You are not assigned to any section", comment: ""))
                            }
                        }
                        
                    }
                })
            }
        }) 
        task.resume()
    }
    
    func requestSubGroupForSection(_ section:String, handler:@escaping SocialStreamDoneBlock) {
        let userid = accountUserID()
        let endpoint = groupsFor(userid, sectionid: section)
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("GET", url: url, andBody: nil)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                handler(false)
            }
            
            let ctx = self.getContext()
            
            // CLEAR VALUES
            //let filter:NSPredicate? = nil
            //let status = self.db.clearEntity(SSMConstants.Entity.GROUP, ctx: ctx, filter: filter)
            
            // DEFAULT VALUE
            let predicate = self.db.createPredicate(key:"id", withValue: "0", isExact: true)
            let all_mo = self.db.retrieveEntity(SSMConstants.Entity.GROUP, context: ctx!, filter: predicate)
            let all_students_text = NSLocalizedString("All Students", comment: "")
            
            all_mo.setValue("0", forKey: "id")
            all_mo.setValue(section, forKey: "group_id")
            all_mo.setValue(all_students_text, forKey: "name")
            all_mo.setValue(section, forKey: "section_id")
            all_mo.setValue("1", forKey: "is_selected")
            all_mo.setValue(0, forKey: "notification_count")
            _ = self.db.saveObjectContext(ctx!)
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [[String:AnyObject]] else {
                    handler(false)
                    return
                }
                
                ctx?.performAndWait({
                    //if status == true {
                        for r in records {
                            print("sub item group: \(r)")
                            
                            let id: String! = self.stringValue(r["id"])
                            let group_id: String! = self.stringValue(r["group_id"])
                            let name: String! = self.stringValue(r["name"])
                            
                            let predicate = NSComparisonPredicate(keyPath: "id", withValue: id, isExact:true)
                            let mo = self.db.retrieveEntity(SSMConstants.Entity.GROUP, context: ctx!, filter: predicate)
                            
                            mo.setValue(section, forKey: "section_id") // DINAGDAG LANG NATIN TO
                            mo.setValue(id, forKey: "id") // GROUP SATIN TO
                            mo.setValue(group_id, forKey: "group_id") // SECTION SATIN TO
                            mo.setValue(name, forKey: "name")
                            mo.setValue("0", forKey: "is_selected")
                            //mo.setValue(0, forKey: "notification_count")
                        }
                        
                        let success = self.db.saveObjectContext(ctx!)
                        handler(success)
                    //}
                    //else {
                    //    handler(doneBlock: false)
                    //}
                })
            }
            else {
                handler(false)
            }
        }) 
        
        task.resume()
    }
    
    // MARK: - Network Methods User Message Feeds (Public)
    
    func requestUserMessages(forGroup group_id:String, inSection section_id: String, clearEntities: Bool = true, handler:@escaping SocialStreamDoneBlock) {
        
        let homeurl = "http://\(retrieveVSmartBaseURL())"
        let endpoint = messagesFor(section_id, groupid: group_id)
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("GET", url: url, andBody: nil)
        let ctx = self.getContext()
        //                let status = self.db.clearEntity(entity, ctx: ctx, filter: nil)
        if clearEntities {
            let entity = SSMConstants.Entity.MESSAGEFEED
            _ = self.db.clearEntity(entity, ctx: ctx!, filter: nil)
            _ = self.db.clearEntity(SSMConstants.Entity.LIKES, ctx: ctx!, filter: nil)
            _ = self.db.clearEntity(SSMConstants.Entity.COMMENTS, ctx: ctx!, filter: nil)
            _ = self.db.clearEntity(SSMConstants.Entity.COMMENT_LIKES, ctx: ctx!, filter: nil)
            _ = self.db.clearEntity(SSMConstants.Entity.URLDATA, ctx: ctx!, filter: nil)
        }
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                handler(false)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else {
                handler(false)
                return
            }
            
            if ( self.okayToParseResponse(dictionary) ) {
                
                guard let records = dictionary["records"] as? [[String:AnyObject]] else {
                    handler(false)
                    return
                }
                
                ctx?.performAndWait({
                    
//                    if status == true {
                        for r in records {
                            
                            print("item group: \(r)")
                            /*
                             {
                             id: "199",
                             user_id: "2",
                             group_id: "2",
                             emoticon_id: null,
                             message: "testing Posting",
                             b_title: null,
                             is_attached: "0",
                             is_deleted: "0",
                             date_created: "2016-07-26 03:47:52",
                             date_modified: "2016-07-26 03:47:52",
                             username: "webteacher1@vsmart.com",
                             avatar: "/uploads/2/avatars/20160711/2e3c0708549d5f069699bae3ae11506f.png",
                             attachCount: "0",
                             current_date: "2016-08-01 10:30:13",
                             icon_url: null,
                             icon_text: null,
                             first_name: "Web",
                             last_name: "Teacher 1",
                             }
                             */
                            
                            // PARSE URL DATA
                            var url_data = Set<NSManagedObject>()
                            var type: String! = "message"
                            if r["url_data"] is [[String:AnyObject]] {
                                let urlData = r["url_data"] as! [[String:AnyObject]]
                                
                                let baseData = urlData.last
                                type = self.stringValue(baseData!["msg_type"])
                                
                                
                                let url = self.stringValue(baseData!["url"])
                                if (url?.characters.count)! > 0 {
                                    type = "link"
                                }
                                
                                url_data = self.processURLData(urlData, context: ctx)
                            }
                            
                            var comments = Set<NSManagedObject>()
                            if r["comments"] is [[String:AnyObject]] {
                                let commentData = r["comments"] as! [[String:AnyObject]]
                                comments = self.processComments(commentData, context: ctx)
                            }
                            
                            var likes = Set<NSManagedObject>()
                            var is_liked = "0"
                            if r["post_likes"] is [[String:AnyObject]] {
                                let likeData = r["post_likes"] as! [[String:AnyObject]]
                                let loggedInUserID = self.accountUserID()
                                let arrayOfFilteredLikes = likeData.filter({
                                    ((self.stringValue($0["user_id"])) == loggedInUserID)
                                })
                                
                                is_liked = (arrayOfFilteredLikes.count > 0) ? "1" : "0"
                                
                                likes = self.processLikes(likeData, context: ctx)
                            }
                            
                            let message_id: String! = self.stringValue(r["id"])
                            let user_id: String! = self.stringValue(r["user_id"])
                            let group_id: String! = self.stringValue(r["group_id"])
                            let emoticon_id: String! = self.stringValue(r["emoticon_id"])
                            var message: String! = self.stringValue(r["message"])
                            let b_title: String! = self.stringValue(r["b_title"])
                            let is_attached: String! = self.stringValue(r["is_attached"])
                            let is_deleted: String! = self.stringValue(r["is_deleted"])
                            let date_created: String! = self.stringValue(r["date_created"])
                            let date_modified: String! = self.stringValue(r["date_modified"])
                            let username: String! = self.stringValue(r["username"])
                            let avatar_image: String! = self.stringValue(r["avatar"])
                            let avatar: String! = "\(homeurl)\(avatar_image!)"
                            let attach_count: String! = self.stringValue(r["attachCount"])
                            let current_date: String! = self.stringValue(r["current_date"])
                            let icon_url: String! = "\(homeurl)\(self.stringValue(r["icon_url"])!)"
                            let icon_text: String! = self.stringValue(r["icon_text"])
                            let first_name: String! = self.stringValue(r["first_name"])
                            let last_name: String! = self.stringValue(r["last_name"])
                            
                            if type == "sticker" {
                                message = message?.replacingOccurrences(of: "<img src='", with: "", options: .regularExpression, range: nil)
                                message = message?.replacingOccurrences(of: "'/>", with: "", options: .regularExpression, range: nil)
                                
                                //self.stripHTLMTags(inString: message)
                                
                                message = (message?.contains(homeurl))! ? message : "\(homeurl)\(message!)"
                            }
                            
                            let comment_count: String! = self.stringValue(comments.count)
                            let like_count: String! = self.stringValue(likes.count)
                            
                            let date_modified_date = self.date(fromString: date_modified!, withFormat: "yyyy-MM-dd HH:mm:ss", convertToLocalTime: true)
                            
                            let predicate = self.db.createPredicate(key:"id", withValue: message_id!, isExact: true)
                            let mo = self.db.retrieveEntity(SSMConstants.Entity.MESSAGEFEED, context: ctx!, filter: predicate)
                            
                            mo.setValue(message_id, forKey: "id")
                            mo.setValue(user_id, forKey: "user_id")
                            mo.setValue(group_id, forKey: "group_id")
                            mo.setValue(emoticon_id, forKey: "emoticon_id")
                            mo.setValue(message, forKey: "message")
                            mo.setValue(b_title, forKey: "b_title")
                            mo.setValue(is_attached, forKey: "is_attached")
                            mo.setValue(is_deleted, forKey: "is_deleted")
                            mo.setValue(date_created, forKey: "date_created")
                            mo.setValue(date_modified, forKey: "date_modified")
                            mo.setValue(username, forKey: "username")
                            mo.setValue(avatar, forKey: "avatar")
                            mo.setValue(attach_count, forKey: "attach_count")
                            mo.setValue(current_date, forKey: "current_date")
                            mo.setValue(icon_url, forKey: "icon_url")
                            mo.setValue(icon_text, forKey: "icon_text")
                            mo.setValue(first_name, forKey: "first_name")
                            mo.setValue(last_name, forKey: "last_name")
                            mo.setValue(date_modified_date, forKey: "date_modified_date")
                            mo.setValue(section_id, forKey: "section_id")
                            
                            mo.setValue(comment_count, forKey: "comment_count")
                            mo.setValue(like_count, forKey: "like_count")
                            mo.setValue(type, forKey: "type")
                            mo.setValue(is_liked, forKey: "is_liked")
                            
                            // URL_DATA RELATIONSHIP
                            mo.setValue(url_data, forKey: "url_data")
                            
                            // COMMENT RELATIONSHIP
                            mo.setValue(comments, forKey: "comments")
                            
                            // LIKE RELATIONSHIP
                            mo.setValue(likes, forKey: "likes")
                            
                            if user_id == self.accountUserID() {
                                mo.setValue("1", forKey: "is_online")
                                mo.setValue("Apple iPad", forKey: "device_model")
                            }
                            
                        }
                        let success = self.db.saveObjectContext(ctx!)
                        //                        self.db.displayRecords(entity)
                        handler(success)
                    
//                    }
//                    else {
//                        handler(doneBlock:false)
//                    }
                })
            }
            else {
                handler(false)
            }
        }) 
        task.resume()
    }
    
    fileprivate func processURLData(_ data:[[String:AnyObject]]!, context:NSManagedObjectContext!) -> Set<NSManagedObject>! {
        
        let entity = SSMConstants.Entity.URLDATA
        
        var objects = Set<NSManagedObject>()
        
        let homeUrl = "http://\(retrieveVSmartBaseURL())"
        
        for i in data {
            //            print("---- X7---- \(i)")
            let content_id: String! = self.stringValue(i["content_id"])
            
            let predicate = self.db.createPredicate(key:"content_id", withValue: content_id!, isExact: true)
            let mo = self.db.retrieveEntity(entity, context: context!, filter: predicate)
//            let mo = NSEntityDescription.insertNewObject(forEntityName: entity, into: context)
            
            /*
             content_id: "199",
             uuid: "34c58363ecb0497695792bab6f53ccf0",
             text: " testing Posting",
             image: "no",
             thumb_img: "no",
             title: "N/A",
             canonicalUrl: "",
             url: "",
             description: "N/A",
             is_video: "no",
             is_image: "no",
             msg_type: "message"
             */
            
            let content_uuid: String! = self.stringValue(i["uuid"])
            let text: String! = self.stringValue(i["text"])
            let image: String! = self.stringValue(i["image"])
            var thumb_img: String! = self.stringValue(i["thumb_img"])
            let title: String! = self.stringValue(i["title"])
            let canonical_url: String! = self.stringValue(i["canonicalUrl"])
            let url: String! = self.stringValue(i["url"])
            let description: String! = self.stringValue(i["description"])
            let is_video: String! = self.stringValue(i["is_video"])
            let is_image: String! = self.stringValue(i["is_image"])
            let msg_type: String! = self.stringValue(i["msg_type"])
            
            thumb_img = (thumb_img?.contains(homeUrl))! ? thumb_img : "\(homeUrl)\(thumb_img!)"
            
            mo.setValue(content_id, forKey: "content_id")
            mo.setValue(content_uuid, forKey: "uuid")
            mo.setValue(text, forKey: "text")
            mo.setValue(image, forKey: "image")
            mo.setValue(thumb_img, forKey: "thumb_img")
            mo.setValue(title, forKey: "title")
            mo.setValue(canonical_url, forKey: "canonical_url")
            mo.setValue(url, forKey: "url")
            mo.setValue(description, forKey: "desc")
            mo.setValue(is_video, forKey: "is_video")
            mo.setValue(is_image, forKey: "is_image")
            mo.setValue(msg_type, forKey: "msg_type")
            
            objects.insert(mo)
        }
        
        return objects
    }
    
    fileprivate func processComments(_ data:[[String:AnyObject]]!, context:NSManagedObjectContext!) -> Set<NSManagedObject>! {
        
        let entity = SSMConstants.Entity.COMMENTS
        
        var objects = Set<NSManagedObject>()
        let homeUrl = "http://\(retrieveVSmartBaseURL())"
        
        for i in data {
            //            print("---- X7---- \(i)")
            let comment_id: String! = self.stringValue(i["id"])
            
            let predicate = self.db.createPredicate(key:"comment_id", withValue: comment_id!, isExact: true)
            let mo = self.db.retrieveEntity(entity, context: context!, filter: predicate)
//            let mo = NSEntityDescription.insertNewObject(forEntityName: entity, into: context)
            
            //        "comments": [{
            //        "id": "80",
            //        "user_id": "2",
            //        "message_id": "199",
            //        "comment": "testing comment",
            //        "is_deleted": "0",
            //        "date_created": "2016-07-26 03:48:02",
            //        "date_modified": "2016-07-26 03:48:02",
            //        "username": "webteacher1@vsmart.com",
            //        "avatar": "/uploads/2/avatars/20160711/2e3c0708549d5f069699bae3ae11506f.png",
            //        "first_name": "Web",
            //        "last_name": "Teacher 1",
            //        "current_date": "2016-08-02 14:13:51",
            //        "comment_likes": [{
            //            "id": "140",
            //            "user_id": "38",
            //            "content_id": "94",
            //            "is_message": "0",
            //            "user_type": "teacher",
            //            "is_deleted": "0",
            //            "date_created": "2016-08-02 06:48:12",
            //            "date_modified": "2016-08-02 06:48:12",
            //            "username": "windowsteacher1@vsmart.com",
            //            "avatar": "/uploads/38/avatars/20160719/d8798c0a22b87088ef0179fdf0447dcd.jpg",
            //            "first_name": "Windows",
            //            "last_name": "Teacher 1"
            //            }]
            //        }]
            
            let user_id: String! = self.stringValue(i["user_id"])
            let message_id: String! = self.stringValue(i["message_id"])
            let comment: String! = self.stringValue(i["comment"])
            let is_deleted: String! = self.stringValue(i["is_deleted"])
            let date_created: String! = self.stringValue(i["date_created"])
            let date_modified: String! = self.stringValue(i["date_modified"])
            let username: String! = self.stringValue(i["username"])
            var avatar: String! = self.stringValue(i["avatar"])
            let first_name: String! = self.stringValue(i["first_name"])
            let last_name: String! = self.stringValue(i["last_name"])
            let current_date: String! = self.stringValue(i["current_date"])
            let is_edited: String! = (date_created == date_modified) ? "0" : "1"
            
            avatar = (avatar?.contains(homeUrl))! ? avatar : "\(homeUrl)\(avatar!)"
            
            let date_created_date = self.date(fromString: date_created!, withFormat: "yyyy-MM-dd HH:mm:ss")
            
            var comment_like = Set<NSManagedObject>()
            var is_liked = "0"
            if i["comment_likes"] is [[String:AnyObject]] {
                let comment_likes = i["comment_likes"] as! [[String:AnyObject]]
                
                let loggedInUserID = self.accountUserID()
                let arrayOfFilteredLikes = comment_likes.filter({
                    ((self.stringValue($0["user_id"])) == loggedInUserID)
                })
                
                is_liked = (arrayOfFilteredLikes.count > 0) ? "1" : "0"
                
                comment_like = self.processCommentLikes(comment_likes, context: context)
            }
            
            let comment_like_count: String! = self.stringValue(comment_like.count)
            
            mo.setValue(comment_id, forKey: "comment_id")
            mo.setValue(user_id, forKey: "user_id")
            mo.setValue(message_id, forKey: "message_id")
            mo.setValue(comment, forKey: "comment")
            mo.setValue(is_deleted, forKey: "is_deleted")
            mo.setValue(date_created, forKey: "date_created")
            mo.setValue(date_modified, forKey: "date_modified")
            mo.setValue(username, forKey: "username")
            mo.setValue(avatar, forKey: "avatar")
            mo.setValue(first_name, forKey: "first_name")
            mo.setValue(last_name, forKey: "last_name")
            mo.setValue(current_date, forKey: "current_date")
            mo.setValue(date_created_date, forKey: "date_created_date")
            mo.setValue(is_edited, forKey: "is_edited")
            
            mo.setValue(comment_like_count, forKey: "comment_like_count")
            mo.setValue(is_liked, forKey: "is_liked")
            
            mo.setValue(comment_like, forKey: "comment_like")
            
            let date_modified_date = self.date(fromString: date_modified!, withFormat: "yyyy-MM-dd HH:mm:ss", convertToLocalTime: true)
            mo.setValue(date_modified_date, forKey: "date_modified_date")
            
            objects.insert(mo)
        }
        
        return objects
    }
    
    
    fileprivate func processCommentLikes(_ data:[[String:AnyObject]]!, context:NSManagedObjectContext!) -> Set<NSManagedObject>! {
        
        let entity = SSMConstants.Entity.COMMENT_LIKES
        
        var objects = Set<NSManagedObject>()
        let homeUrl = "http://\(retrieveVSmartBaseURL())"
        
        for i in data {
            let comment_like_id: String! = self.stringValue(i["id"])
            
            let predicate = self.db.createPredicate(key:"comment_like_id", withValue: comment_like_id!, isExact: true)
            let mo = self.db.retrieveEntity(entity, context: context!, filter: predicate)
//            let mo = NSEntityDescription.insertNewObject(forEntityName: entity, into: context)
            
            //    [{
            //    "id": "140",
            //    "user_id": "38",
            //    "content_id": "94",
            //    "is_message": "0",
            //    "user_type": "teacher",
            //    "is_deleted": "0",
            //    "date_created": "2016-08-02 06:48:12",
            //    "date_modified": "2016-08-02 06:48:12",
            //    "username": "windowsteacher1@vsmart.com",
            //    "avatar": "/uploads/38/avatars/20160719/d8798c0a22b87088ef0179fdf0447dcd.jpg",
            //    "first_name": "Windows",
            //    "last_name": "Teacher 1"
            //    }]
            
            
            let user_id: String! = self.stringValue(i["user_id"])
            let content_id: String! = self.stringValue(i["content_id"])
            let is_message: String! = self.stringValue(i["is_message"])
            let user_type: String! = self.stringValue(i["user_type"])
            let is_deleted: String! = self.stringValue(i["is_deleted"])
            let date_created: String! = self.stringValue(i["date_created"])
            let date_modified: String! = self.stringValue(i["date_modified"])
            let username: String! = self.stringValue(i["username"])
            var avatar: String! = self.stringValue(i["avatar"])
            let first_name: String! = self.stringValue(i["first_name"])
            let last_name: String! = self.stringValue(i["last_name"])
            
            avatar = (avatar?.contains(homeUrl))! ? avatar : "\(homeUrl)\(avatar!)"
            
            let date_modified_date = self.date(fromString: date_modified!, withFormat: "yyyy-MM-dd HH:mm:ss")
            
            mo.setValue(comment_like_id, forKey: "comment_like_id")
            mo.setValue(user_id, forKey: "user_id")
            mo.setValue(content_id, forKey: "content_id")
            mo.setValue(is_message, forKey: "is_message")
            mo.setValue(user_type, forKey: "user_type")
            mo.setValue(is_deleted, forKey: "is_deleted")
            mo.setValue(date_created, forKey: "date_created")
            mo.setValue(date_modified, forKey: "date_modified")
            mo.setValue(username, forKey: "username")
            mo.setValue(avatar, forKey: "avatar")
            mo.setValue(first_name, forKey: "first_name")
            mo.setValue(last_name, forKey: "last_name")
            mo.setValue(date_modified_date, forKey: "date_modified_date")
            
            objects.insert(mo)
        }
        
        return objects
    }
    
    fileprivate func processLikes(_ data:[[String:AnyObject]]!, context:NSManagedObjectContext!) -> Set<NSManagedObject>! {
        
        let entity = SSMConstants.Entity.LIKES
        
        var objects = Set<NSManagedObject>()
        let homeUrl = "http://\(retrieveVSmartBaseURL())"
        
        for i in data {
            
            let like_id: String! = self.stringValue(i["id"])
            
            let predicate = self.db.createPredicate(key:"like_id", withValue: like_id!, isExact: true)
            let mo = self.db.retrieveEntity(entity, context: context!, filter: predicate)
            
//            let mo = NSEntityDescription.insertNewObject(forEntityName: entity, into: context)
            
            //            "post_likes": [{
            //                "id": "79",
            //                "user_id": "2",
            //                "content_id": "199",
            //                "is_message": "1",
            //                "user_type": "teacher",
            //                "is_deleted": "0",
            //                "date_created": "2016-07-26 09:21:07",
            //                "date_modified": "2016-07-26 09:21:07",
            //                "username": "webteacher1@vsmart.com",
            //                "avatar": "/uploads/2/avatars/20160711/2e3c0708549d5f069699bae3ae11506f.png",
            //                "first_name": "Web",
            //                "last_name": "Teacher 1"
            //            }],
            
            let user_id: String! = self.stringValue(i["user_id"])
            let content_id: String! = self.stringValue(i["content_id"])
            let is_message: String! = self.stringValue(i["is_message"])
            let is_deleted: String! = self.stringValue(i["is_deleted"])
            let date_created: String! = self.stringValue(i["date_created"])
            let date_modified: String! = self.stringValue(i["date_modified"])
            let username: String! = self.stringValue(i["username"])
            var avatar: String! = self.stringValue(i["avatar"])
            let first_name: String! = self.stringValue(i["first_name"])
            let last_name: String! = self.stringValue(i["last_name"])
            
            avatar = (avatar?.contains(homeUrl))! ? avatar : "\(homeUrl)\(avatar!)"
            
            let date_modified_date = self.date(fromString: date_modified!, withFormat: "yyyy-MM-dd HH:mm:ss")
            
            //            let comment_likes = i["comment_likes"] as! [[String:AnyObject]]
            
            mo.setValue(like_id, forKey: "like_id")
            mo.setValue(user_id, forKey: "user_id")
            mo.setValue(content_id, forKey: "message_id")
            mo.setValue(is_message, forKey: "is_message")
            mo.setValue(is_deleted, forKey: "is_deleted")
            mo.setValue(date_created, forKey: "date_created")
            mo.setValue(date_modified, forKey: "date_modified")
            mo.setValue(username, forKey: "username")
            mo.setValue(avatar, forKey: "avatar")
            mo.setValue(first_name, forKey: "first_name")
            mo.setValue(last_name, forKey: "last_name")
            mo.setValue(date_modified_date, forKey: "date_modified_date")
            
            objects.insert(mo)
        }
        
        return objects
    }
    
    func requestSocialStreamStickers(_ handler: SocialStreamListBlock?) {
        
        let endpoint = "/vsmart-rest-dev/v1/stream/getstickers"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("GET", url: url, andBody: nil)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                if handler != nil {
                    handler!(nil)
                }
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [[String:AnyObject]] else {
                    return
                }
                
                var allStickers = [[String:AnyObject]]()
                for record in records {
                    
                    
                    
                    let homeUrl = self.retrieveVSmartBaseURL()
                    
                    let category: String! = self.stringValue(record["category"])
                    let cover_url: String! = "http://\(homeUrl)\(self.stringValue(record["cover_url"])!)"
                    let stickers = record["stickers"] as! [[String:AnyObject]]
                    
                    let sticker: [String:AnyObject] = ["category":category as AnyObject,
                                                       "cover_url":cover_url as AnyObject,
                                                       "stickers":stickers as AnyObject];
                    allStickers.append(sticker)
                }
                
                if handler != nil {
                    handler!(allStickers)
                }
            }
        }) 
        task.resume()
    }
    
    func requestSocialStreamEmoticons(_ handler: SocialStreamListBlock?) {
        
        let endpoint = "/vsmart-rest-dev/v1/stream/getemoticons"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("GET", url: url, andBody: nil)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                if handler != nil {
                    handler!(nil)
                }
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [[String:AnyObject]] else {
                    return
                }
                
                var allEmoticons = [[String:AnyObject]]()
                for record in records {
                    let homeUrl = self.retrieveVSmartBaseURL()
                    
                    let sticker_id: String! = self.stringValue(record["id"])
                    let name: String! = self.stringValue(record["name"])
                    let chars: String! = self.stringValue(record["chars"])
                    let icon_text: String! = self.stringValue(record["icon_text"])
                    let icon_url: String! = "http://\(homeUrl)\(self.stringValue(record["icon_url"])!)"
                    let is_default: String! = self.stringValue(record["is_default"])
                    let is_deleted: String! = self.stringValue(record["is_deleted"])
                    let date_created: String! = self.stringValue(record["date_created"])
                    let date_modified: String! = self.stringValue(record["date_modified"])
                    
                    let sticker: [String:AnyObject] = [
                        "id": sticker_id as AnyObject,
                        "name": name as AnyObject,
                        "chars": chars as AnyObject,
                        "icon_text": icon_text as AnyObject,
                        "icon_url": icon_url as AnyObject,
                        "is_default": is_default as AnyObject,
                        "is_deleted": is_deleted as AnyObject,
                        "date_created": date_created as AnyObject,
                        "date_modified": date_modified as AnyObject
                    ]
                    allEmoticons.append(sticker)
                }
                
                if handler != nil {
                    handler!(allEmoticons)
                }
            }
        }) 
        task.resume()
    }
    
    func requestBadwords(_ handler: SocialStreamDoneBlock?) {
        let endpoint = "/vsmart-rest-dev/v1/stream/getbadwords"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("GET", url: url, andBody: nil)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                
                if handler != nil {
                    handler!(false)
                }
                
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [[String:AnyObject]] else {
                    return
                }
                
                
                let ctx = self.getContext()
                let entity = SSMConstants.Entity.BADWORDS
                
                ctx?.performAndWait({
                    for record in records {
                        let badword_id: String! = self.stringValue(record["id"])
                        let word: String! = self.stringValue(record["word"])
                        let replace_text: String! = self.stringValue(record["replace_text"])
                        let is_deleted: String! = self.stringValue(record["is_deleted"])
                        
                        let predicate = self.db.createPredicate(key:"badword_id", withValue: badword_id!, isExact: true)
                        let mo = self.db.retrieveEntity(entity, context: ctx!, filter: predicate)
                        
                        mo.setValue(badword_id, forKey: "badword_id")
                        mo.setValue(word, forKey: "word")
                        mo.setValue(replace_text, forKey: "replace_text")
                        mo.setValue(is_deleted, forKey: "is_deleted")
                    }
                    _ = self.db.saveObjectContext(ctx!)
                    if handler != nil {
                        handler!(true)
                    }
                })
            }
        }) 
        task.resume()
    }
    
    func fetchBadwords() -> [String:String]? {
        let entity = SSMConstants.Entity.BADWORDS
        guard let arrayOfBadObjects = self.db.retrieveObjects(forEntity: entity) else {
            return nil
        }
        
        var badwordDict = [String:String]()
        
        for badwordObject in arrayOfBadObjects {
            let replace_text = badwordObject.value(forKey: "replace_text") as! String
            let word = badwordObject.value(forKey: "word") as! String
            
            badwordDict[word] = replace_text
        }
        
        return badwordDict
    }
    
    func getAllBadwords(inMessage message: String) -> [String]! {
        var arrayOfBadWords = [String]()
        
        guard let badwords = self.fetchBadwords() else {
            return arrayOfBadWords
        }
        
        let words = message.components(separatedBy: CharacterSet(charactersIn: ", .-_+"))
        
        for word in words {
            let lowerCasedWord: String! = self.stringValue(word).lowercased()
            let badword = badwords[lowerCasedWord]
            if (badword != nil) {
                arrayOfBadWords.append(lowerCasedWord)
            }
        }
        
        return arrayOfBadWords
    }
    
    func postMessage(withData data: [String:AnyObject], handler: SocialStreamDoneBlockWithError?) {
        var mutableDict = data as [String: Any]
        
        let user_id: String! = self.accountUserID()
        let avatar: String! = self.accountUserAvatar()
        let userName: String! = self.accountUserUserName()
        
        mutableDict["user_id"] = Int(user_id)
        mutableDict["avatar"] = avatar
        mutableDict["username"] = userName
        mutableDict["is_attached"] = 0
        
        
        let endpoint = "/vsmart-rest-dev/v1/stream/postmessage"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("POST", url: url, andBody: mutableDict)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                
                if handler != nil {
                    handler!(false, e.localizedDescription)
                }
                
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                print("POST MESSAGE RESPONSE \(dictionary)")
                if handler != nil {
                    handler!(true, nil)
                }
            }
        }) 
        task.resume()
    }
    
    func editMessage(withID message_id: String, withData data: [String:AnyObject], handler: SocialStreamDoneBlockWithError?) {
        
        let endpoint = "/vsmart-rest-dev/v1/stream/putmessage/\(message_id)"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("POST", url: url, andBody: data as AnyObject?)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                
                if handler != nil {
                    handler!(false, e.localizedDescription)
                }
                
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                print("POST MESSAGE RESPONSE \(dictionary)")
                if handler != nil {
                    handler!(true, nil)
                }
            }
        }) 
        
        task.resume()
    }
    
    func postComment(withData data: [String:AnyObject], handler: SocialStreamDoneBlock?) {
        var mutableDict = data
        
        let user_id = self.accountUserID()
        let avatar = self.accountUserAvatar()
        let userName = self.accountUserUserName()
        
        mutableDict["user_id"] = Int(user_id) as AnyObject?
        mutableDict["avatar"] = avatar as AnyObject?
        mutableDict["username"] = userName as AnyObject?
        mutableDict["is_deleted"] = 0 as AnyObject?
        
        let endpoint = "/vsmart-rest-dev/v1/stream/postcomment"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("POST", url: url, andBody: mutableDict as AnyObject?)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                
                if handler != nil {
                    handler!(false)
                }
                
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                print("POST MESSAGE RESPONSE \(dictionary)")
                if handler != nil {
                    handler!(true)
                }
            }
        }) 
        task.resume()
    }
    
    func editComment(withID comment_id: String, withData data: [String:AnyObject], handler: SocialStreamDoneBlock?) {
        
        //        self.editedComment = [@{@"comment_id": comment_id, @"comment": comment } mutableCopy];
        
        let endpoint = "/vsmart-rest-dev/v1/stream/putcomment/\(comment_id)"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("POST", url: url, andBody: data as AnyObject?)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                
                if handler != nil {
                    handler!(false)
                }
                
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                print("POST MESSAGE RESPONSE \(dictionary)")
                if handler != nil {
                    handler!(true)
                }
            }
        }) 
        
        task.resume()
    }
    
    func removeComment(withID comment_id: String, completion: SocialStreamDoneBlockWithError?) {
        
        //        self.editedComment = [@{@"comment_id": comment_id, @"comment": comment } mutableCopy];
        
        let endpoint = "/vsmart-rest-dev/v1/stream/removecomment/\(comment_id)"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("POST", url: url, andBody: nil)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print("JSON Error \(e.localizedDescription)")
                if completion != nil {
                    completion!(false, e.localizedDescription)
                }
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            
            if ( self.okayToParseResponse(dictionary) ) {
                let entity = SSMConstants.Entity.COMMENTS
                
                let ctx = self.getContext()
                let predicate = self.db.createPredicate(key:"comment_id", withValue: comment_id, isExact: true)
                _ = self.db.clearEntity(entity, ctx: ctx!, filter: predicate)
                
                if completion != nil {
                    completion!(true, nil)
                }
            } else {
                var errorMessage: String! = "Something went wrong, please try again"
                if let records = dictionary["records"] as? [String:AnyObject] {
                    errorMessage = self.stringValue(records["more"])
                }
                if completion != nil {
                    completion!(false, errorMessage)
                }
            }
            
            
            //            if ( self.okayToParseResponse(dictionary) ) {
            //                print("POST MESSAGE RESPONSE \(dictionary)")
            //                if handler != nil {
            //                    handler!(doneBlock: true)
            //                }
            //            }
        }) 
        
        task.resume()
    }
    
    fileprivate func fetchSectionIDs() -> [String] {
        var list = [String]()
        let sections = fetchSections() as! [[String:AnyObject]]
        for item in sections {
            let section_id = item["id"] as! String
            list.append(section_id)
        }
        return list
    }
    
    // MARK: Message Feed Actions
    
    func postLikeMessage(withData data:[String:AnyObject], withHandler completion:SocialStreamDoneBlockWithError?) {
        var dataCopy = data
        
        let avatar = self.accountUserAvatar()
        let loggedInUserID = self.accountUserID()
        
        dataCopy["avatar"] = avatar as AnyObject!
        dataCopy["user_id"] = loggedInUserID as AnyObject!
        
        let endpoint = "/vsmart-rest-dev/v1/stream/postlike"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("POST", url: url, andBody: dataCopy)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print("JSON Error \(e.localizedDescription)")
                if completion != nil {
                    completion!(false, e.localizedDescription)
                }
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                if completion != nil {
                    completion!(true, nil)
                }
            } else {
                var errorMessage: String! = "Something went wrong, please try again"
                if let records = dictionary["records"] as? [String:AnyObject] {
                    errorMessage = self.stringValue(records["more"])
                }
                if completion != nil {
                    completion!(false, errorMessage)
                }
            }
        }) 
        task.resume()
    }
    
    func postUnlikeMessage(withData data:[String:AnyObject], withHandler completion:SocialStreamDoneBlockWithError?) {
        //    NSMutableDictionary *d = [@{@"avatar":avatar,
        //    @"username":username,
        //    @"user_id":user_id, // MY USER ID
        //    @"content_id":message_id,
        //    @"is_message":@1,
        //    @"is_deleted":@0} mutableCopy];
        
        var dataCopy = data
        
        let avatar = self.accountUserAvatar()
        let loggedInUserID = self.accountUserID()
        
        dataCopy["avatar"] = avatar as AnyObject
        dataCopy["user_id"] = loggedInUserID as AnyObject
        
        let message_id: String! = self.stringValue(data["content_id"])
        
        let content_id_predicate = NSComparisonPredicate(keyPath: "message_id", withValue: message_id, isExact: true)
        let loggedInUserID_predicate = NSComparisonPredicate(keyPath: "user_id", withValue: loggedInUserID, isExact: true)
        
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [content_id_predicate,loggedInUserID_predicate])
        guard let likeObject = self.db.retrieveEntity(SSMConstants.Entity.LIKES, filter: predicate) else {
            
            print("LIKE/MESSAGE DOES NOT EXIST")
            if completion != nil {
                completion!(false, "Post already unliked")
            }
            return
        }
        
        let like_id: String! = self.stringValue(likeObject.value(forKey: "like_id"))
        
        let endpoint = "/vsmart-rest-dev/v1/stream/removelike/\(like_id!)"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("POST", url: url, andBody: data as AnyObject?)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print("JSON Error \(e.localizedDescription)")
                if completion != nil {
                    completion!(true, e.localizedDescription)
                }
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                print("POST UNLIKE MESSAGE RESPONSE \(dictionary)")
                if completion != nil {
                    completion!(true, nil)
                }
            } else {
                var errorMessage: String! = "Something went wrong, please try again"
                if let records = dictionary["records"] as? [String:AnyObject] {
                    errorMessage = self.stringValue(records["more"])
                }
                if completion != nil {
                    completion!(false, errorMessage)
                }
            }
        }) 
        task.resume()
    }
    
    func postUnlikeComment(withData data:[String:AnyObject], withHandler completion:SocialStreamDoneBlockWithError?) {
        //    NSMutableDictionary *d = [@{@"avatar":avatar,
        //    @"username":username,
        //    @"user_id":user_id, // MY USER ID
        //    @"content_id":message_id,
        //    @"is_message":@1,
        //    @"is_deleted":@0} mutableCopy];
        
        var dataCopy = data
        
        let avatar = self.accountUserAvatar()
        let loggedInUserID = self.accountUserID()
        
        dataCopy["avatar"] = avatar as AnyObject?
        dataCopy["user_id"] = loggedInUserID as AnyObject?
        
        let message_id: String! = self.stringValue(data["content_id"])
        
        let content_id_predicate = NSComparisonPredicate(keyPath: "content_id", withValue: message_id, isExact: true)
        let loggedInUserID_predicate = NSComparisonPredicate(keyPath: "user_id", withValue: loggedInUserID, isExact: true)
        
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [content_id_predicate,loggedInUserID_predicate])
        guard let likeObject = self.db.retrieveEntity(SSMConstants.Entity.COMMENT_LIKES, filter: predicate) else {
            
            print("LIKE/MESSAGE DOES NOT EXIST")
            if completion != nil {
                completion!(false, "Post already unliked")
            }
            return
        }
        
        let comment_like_id: String! = self.stringValue(likeObject.value(forKey: "comment_like_id"))
        
        let endpoint = "/vsmart-rest-dev/v1/stream/removelike/\(comment_like_id!)"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("POST", url: url, andBody: data as AnyObject?)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print("JSON Error \(e.localizedDescription)")
                if completion != nil {
                    completion!(true, e.localizedDescription)
                }
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                print("POST UNLIKE MESSAGE RESPONSE \(dictionary)")
                if completion != nil {
                    completion!(true, nil)
                }
            } else {
                var errorMessage: String! = "Something went wrong, please try again"
                if let records = dictionary["records"] as? [String:AnyObject] {
                    errorMessage = self.stringValue(records["more"])
                }
                if completion != nil {
                    completion!(false, errorMessage)
                }
            }
        }) 
        task.resume()
    }
    
    func postRemoveMessage(withMessageID message_id:String, completion: SocialStreamDoneBlockWithError?) {
        
        let endpoint = "/vsmart-rest-dev/v1/stream/removemessage/\(message_id)"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("POST", url: url, andBody: nil)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print("JSON Error \(e.localizedDescription)")
                if completion != nil {
                    completion!(false, e.localizedDescription)
                }
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                let entity = SSMConstants.Entity.MESSAGEFEED
                
                let ctx = self.getContext()
                let predicate = self.db.createPredicate(key:"id", withValue: message_id, isExact: true)
                _ = self.db.clearEntity(entity, ctx: ctx!, filter: predicate)
                
                if completion != nil {
                    completion!(true, nil)
                }
            } else {
                var errorMessage: String! = "Something went wrong, please try again"
                if let records = dictionary["records"] as? [String:AnyObject] {
                    errorMessage = self.stringValue(records["more"])
                }
                if completion != nil {
                    completion!(false, errorMessage)
                }
            }
        }) 
        task.resume()
    }
    
    
    // MARK: - API and Core Data Implementations for Group and Member
    
    func requestMembersForGroup(_ groupID: String, handler: @escaping SocialStreamDoneBlock) {
        let success = self.db.clearEntity(SSMConstants.Entity.MEMBER, ctx: self.getContext(), filter: nil)
        if success {
            let endpoint = membersForGroup(groupID)
            let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
            let request = self.buildURLRequestWithMethod("GET", url: url, andBody: nil)
            
            let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let e = error {
                    print(e.localizedDescription)
                    handler(false)
                }
                
                guard let d = data, let dictionary = self.parseResponseData(d) as? [String: AnyObject] else {
                    handler(false)
                    return
                }
                
                if (self.okayToParseResponse(dictionary)) {
                    guard let records = dictionary["records"] as? [[String: AnyObject]] else {
                        handler(false)
                        return
                    }
                    
                    let ctx = self.getContext()
                    
                    ctx?.performAndWait({
                        for r in records {
                            print("group member: \(r)")
                            
                            let id: String! = self.stringValue(r["id"])
                            let first_name: String! = self.stringValue(r["first_name"])
                            let last_name: String! = self.stringValue(r["last_name"])
                            let user_type_id: String! = self.stringValue(r["user_type_id"])
                            let avatar: String! = self.stringValue(r["avatar"])
                            let avatarUrl: String! = "http://\(self.retrieveVSmartBaseURL())\(avatar!)"
                            let search_string: String! = "\(first_name)\(last_name)"
                            
                            let predicate = self.db.createPredicate(key:"id", withValue: id!, isExact: true)
                            let mo = self.db.retrieveEntity(SSMConstants.Entity.MEMBER, context: ctx!, filter: predicate)
                            
                            mo.setValue(id, forKey: "id")
                            mo.setValue(avatarUrl, forKey: "avatar")
                            mo.setValue(first_name, forKey: "first_name")
                            mo.setValue(last_name, forKey: "last_name")
                            mo.setValue(user_type_id, forKey: "user_type_id")
                            mo.setValue(search_string, forKey: "search_string")
                            mo.setValue("0", forKey: "is_removed")
                        }
                        
                        let done = self.db.saveObjectContext(ctx!)
                        handler(done)
                    })
                }
                else {
                    handler(false)
                }
            }) 
            
            task.resume()
        }
        else {
            handler(false)
        }
    }
    
    func requestStudentsForSection(_ sectionID: String, handler: @escaping SocialStreamDoneBlock) {
        let success = self.db.clearEntity(SSMConstants.Entity.STUDENT, ctx: self.getContext(), filter: nil)
        
        if success {
            let endpoint = studentsForSection(sectionID)
            let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
            let request = self.buildURLRequestWithMethod("GET", url: url, andBody: nil)
            
            let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let e = error {
                    print(e.localizedDescription)
                    handler(false)
                }
                
                guard let d = data, let dictionary = self.parseResponseData(d) as? [String: AnyObject] else {
                    handler(false)
                    return
                }
                
                if (self.okayToParseResponse(dictionary)) {
                    guard let records = dictionary["records"] as? [[String: AnyObject]] else {
                        handler(false)
                        return
                    }
                    
                    let ctx = self.getContext()
                    
                    ctx?.performAndWait({
                        for r in records {
                            print("student: \(r)")
                            
                            let id: String! = self.stringValue(r["id"])
                            let first_name: String! = self.stringValue(r["first_name"])
                            let last_name: String! = self.stringValue(r["last_name"])
                            let level: String! = self.stringValue(r["level"])
                            let avatar: String! = self.stringValue(r["avatar"])
                            let contact_number: String! = self.stringValue(r["contact_number"])
                            let email: String! = self.stringValue(r["email"])
                            let search_string: String! = "\(first_name)\(last_name)"
                            let avatarUrl: String! = "http://\(self.retrieveVSmartBaseURL())\(avatar!)"
                            
                            let predicate = self.db.createPredicate(key:"id", withValue: id!, isExact: true)
                            let mo = self.db.retrieveEntity(SSMConstants.Entity.STUDENT, context: ctx!, filter: predicate)
                            
                            mo.setValue(id, forKey: "id")
                            mo.setValue(first_name, forKey: "first_name")
                            mo.setValue(last_name, forKey: "last_name")
                            mo.setValue(level, forKey: "level")
                            mo.setValue(avatarUrl, forKey: "avatar")
                            mo.setValue(contact_number, forKey: "contact_number")
                            mo.setValue(email, forKey: "email")
                            mo.setValue(search_string, forKey: "search_string")
                        }
                        
                        let done = self.db.saveObjectContext(ctx!)
                        handler(done)
                    })
                }
                else {
                    handler(false)
                }
            }) 
            
            task.resume()
        }
    }
    
    func requestCreateGroupWithPostBody(_ body: [String: Any], handler: @escaping SocialStreamDoneBlock) {
        let endpoint = "/vsmart-rest-dev/v1/stream/post-add-sub-group"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("POST", url: url, andBody: body as [String:Any])
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                handler(false)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String: AnyObject] else {
                handler(false)
                return
            }
            
            if (self.okayToParseResponse(dictionary)) {
                print("post add sub group response: \(dictionary)")
                
                if let records = dictionary["records"] as? [String: AnyObject] {
                    if let data = records["data"] as? [String: AnyObject] {
                        self.handleSocketIOEmitterForKey(SSMConstants.SocketIOEmitterKey.GROUP_CREATE, data: data)
                    }
                }
                
                handler(true)
            }
            else {
                handler(false)
            }
        }) 
        
        task.resume()
        
    }
    
    func requestUpdateGroup(_ groupID: String, body: [String: AnyObject], handler: @escaping SocialStreamDoneBlock) {
        let endpoint = "/vsmart-rest-dev/v1/stream/post-update-sub-group/\(groupID)"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("POST", url: url, andBody: body as AnyObject?)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                handler(false)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String: AnyObject] else {
                handler(false)
                return
            }
            
            if (self.okayToParseResponse(dictionary)) {
                print("post update sub group response: \(dictionary)")
                
                if let records = dictionary["records"] as? [String: AnyObject] {
                    if let data = records["data"] as? [String: AnyObject] {
                        let nMembers = body["members"] as! [String]
                        let dMembers = body["deleted_members"] as! [String]
                        
                        // Filter members to add and remove
                        let predicate = NSPredicate(format: "NOT (id IN %@)", nMembers + dMembers)
                        let success = self.db.clearEntity(SSMConstants.Entity.MEMBER, ctx: self.getContext(), filter: predicate)
                        
                        // Emit if success
                        if success {
                            print("Can emit group update!")
                            self.handleSocketIOEmitterForKey(SSMConstants.SocketIOEmitterKey.GROUP_UPDATE, data: data)
                        }
                        else {
                            print("Can't emit group update!")
                        }
                    }
                }
                
                handler(true)
            }
            else {
                handler(false)
            }
        }) 
        
        task.resume()
    }
    
    func requestRemoveGroup(_ groupID: String, body: [String: AnyObject], handler: @escaping SocialStreamDoneBlock) {
        let endpoint = "/vsmart-rest-dev/v1/stream/post-remove-sub-group/\(groupID)"
        let url = self.buildURLFromRequestEndPoint(endpoint as NSString)
        let request = self.buildURLRequestWithMethod("POST", url: url, andBody: body as AnyObject?)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                handler(false)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String: AnyObject] else {
                handler(false)
                return
            }
            
            if (self.okayToParseResponse(dictionary)) {
                print("post remove sub group response: \(dictionary)")
                self.handleSocketIOEmitterForKey(SSMConstants.SocketIOEmitterKey.GROUP_REMOVE, data: ["id": groupID as AnyObject])
                handler(true)
            }
            else {
                handler(false)
            }
        }) 
        
        task.resume()
    }
    
    // MARK: - Socket IO Listeners for Group and Member
    
    fileprivate func handleSocketIOListenerForKey(_ key: String, data: [AnyObject]) {
        self.prettyFunction()
        
        guard data is [[String: AnyObject]] else {
            print("Can't listen to \(key) notification!")
            return
        }
        
        let current_section: String! = self.stringValue(self.fetchUserDefaultsObject(forKey: "SS_SELECTED_SECTION_ID"))
        let current_group: String! = self.stringValue(self.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID"))
        
        let ctx = self.getContext()
        
        // CREATE GROUP
        // {"group":{"id":"34","group_id":"24","name":"Another Kimmy","is_deleted":0,
        // "date_created":"2016-07-25 08:16:16","date_modified":"2016-07-25 08:16:16","user_id":"4"}}
        // important: section id and group data
        
        if key == SSMConstants.SocketIOListenerKey.GROUP_CREATE {
            for record in data {
                guard let group = record["group"] as? [String: AnyObject] else {
                    print("Can't understand response structure (group)!")
                    return
                }
                
                let section_id: String! = self.stringValue(group["group_id"])
                
                if section_id == current_section {
                let id: String! = self.stringValue(group["id"])
                    let found_group = self.didFindGroup(id!)
                    
                    if !found_group {
                        var success = false
                let name: String! = self.stringValue(group["name"])
                
                    ctx?.performAndWait({
                        let predicate1 = self.db.createPredicate(key:"id", withValue: id!, isExact: true)
                        let mo = self.db.retrieveEntity(SSMConstants.Entity.GROUP, context: ctx!, filter: predicate1)
                        
                        mo.setValue(id, forKey: "id")
                        mo.setValue(name, forKey: "name")
                        mo.setValue(section_id, forKey: "group_id")
                        mo.setValue(section_id, forKey: "section_id")
                        mo.setValue("0", forKey: "is_selected")
                        mo.setValue(0, forKey: "notification_count")
                        
                        success = self.db.saveObjectContext(ctx!)
                    })
                    
                    if success {
                        DispatchQueue.main.async(execute: {
                            let message = "\(NSLocalizedString("You have just been added to", comment: "")) \"\(name!)\" \(NSLocalizedString("group", comment: ""))."
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_MESSAGE_TOAST"), object: message)
                        })
                    }
                }
            }
        }
        }
        
        // REMOVE GROUP
        // {"group":{"id":"34","name":"Another Kimmy","group_id":"24"}}
        // important: id
        
        if key == SSMConstants.SocketIOListenerKey.GROUP_REMOVE {
            for record in data {
                guard let group = record["group"] as? [String: AnyObject] else {
                    print("Can't understand response structure (group)!")
                    return
                }
                
                let section_id: String! = self.stringValue(group["group_id"])
                
                if section_id == current_section {
                let id: String! = self.stringValue(group["id"])
                    let found_group = self.didFindGroup(id!)
                
                    if found_group {
                let predicate = self.db.createPredicate(key: "id", withValue: id!)
                let success = self.db.clearEntity(SSMConstants.Entity.GROUP, ctx: ctx!, filter: predicate)
                
                    if success {
                            let name: String! = self.stringValue(group["name"])
                            
                        if id == current_group {
                            DispatchQueue.main.async(execute: {
                                let message: String! = NSLocalizedString("You have been removed from this group.", comment: "")
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_MESSAGE_TOAST"), object: message)
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_DEFAULT_VIEW"), object: nil)
                            })
                        }
                        else {
                            DispatchQueue.main.async(execute: {
                                let message: String! = "\"\(name!)\" \(NSLocalizedString("group has been removed from this section.", comment: ""))"
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_MESSAGE_TOAST"), object: message)
                            })
                        }
                    }
                }
            }
        }
        }
        
        // UPDATE GROUP
        // {"group":{"id":"214","group_id":"2","name":"Hello","is_deleted":"0","date_created":
        // "2016-08-04 03:22:18","date_modified":"2016-08-04 04:01:10","user_id":"2"}}
        // important: section id and group data
        
        if key == SSMConstants.SocketIOListenerKey.GROUP_UPDATE {
            for record in data {
                guard let group = record["group"] as? [String: AnyObject] else {
                    print("Can't understand response structure (group)!")
                    return
                }
                
                let section_id: String! = self.stringValue(group["group_id"])
                
                if section_id == current_section {
                let id: String! = self.stringValue(group["id"])
                    let predicate = self.db.createPredicate(key:"id", withValue: id!, isExact: true)
                    
                    if let group_object = self.db.retrieveEntity(SSMConstants.Entity.GROUP, filter: predicate) {
                        var success = false
                let name: String! = self.stringValue(group["name"])
                let is_selected = (id == current_group) ? "1" : "0"
                
                    ctx?.performAndWait({
                            group_object.setValue(id, forKey: "id")
                            group_object.setValue(name, forKey: "name")
                            group_object.setValue(section_id, forKey: "group_id")
                            group_object.setValue(section_id, forKey: "section_id")
                            group_object.setValue(is_selected, forKey: "is_selected")
                        
                        success = self.db.saveObjectContext(ctx!)
                    })
                    
                    if success {
                        if id == current_group {
                            DispatchQueue.main.async(execute: {
                                self.save(object: name as AnyObject!, forKey: "SS_SELECTED_GROUP_NAME")
                                let message: String! = NSLocalizedString("This group has been updated.", comment: "")
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_MESSAGE_TOAST"), object: message)
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_CHANGE_GROUP"), object: nil)
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_CHANGE_GROUP_NAME"), object: nil)
                            })
                        }
                        else {
                            DispatchQueue.main.async(execute: {
                                let message: String! = "\"\(name!)\" \(NSLocalizedString("group has been updated.", comment: ""))"
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_MESSAGE_TOAST"), object: message)
                            })
                        }
                    }
                }
            }
        }
        }
        
        // GROUP MEMBER ADD
        // {"group":{"id":"223","name":"Singapore","group_id":"2"},"members":
        // [{"id":"6","first_name":"Web","last_name":"Student 3","user_type_id":"4",
        // "avatar":"/public/img/avatar/avatar-one.png"}]}
        // import: id, group id (for checking current selected section and group) and members data
        
        if key == SSMConstants.SocketIOListenerKey.GROUP_MEMBER_ADD {
            for record in data {
                guard let group = record["group"] as? [String: AnyObject] else {
                    print("Can't understand response structure (group)!")
                    return
                }
                
                guard let members = record["members"] as? [[String: AnyObject]] else {
                    print("Can't understand response structure (members)!")
                    return
                }
                
                let section_id: String! = self.stringValue(group["group_id"])
                
                if section_id == current_section {
                let group_id: String! = self.stringValue(group["id"])
                    let found_group = self.didFindGroup(group_id!)
                    
                    if found_group {
                        var success = false
                let name: String! = self.stringValue(group["name"])
                
                    ctx?.performAndWait({
                        for member in members {
                            print("New group member: \(member)")
                            
                            let id: String! = self.stringValue(member["id"])
                            let first_name: String! = self.stringValue(member["first_name"])
                            let last_name: String! = self.stringValue(member["last_name"])
                            let user_type_id: String! = self.stringValue(member["user_type_id"])
                            let avatar: String! = self.stringValue(member["avatar"])
                            let avatarUrl: String! = "http://\(self.retrieveVSmartBaseURL())\(avatar!)"
                            let search_string: String! = "\(first_name)\(last_name)"
                            
                            let predicate = self.db.createPredicate(key:"id", withValue: id!, isExact: true)
                            let mo = self.db.retrieveEntity(SSMConstants.Entity.MEMBER, context: ctx!, filter: predicate)
                            
                            mo.setValue(id, forKey: "id")
                            mo.setValue(avatarUrl, forKey: "avatar")
                            mo.setValue(first_name, forKey: "first_name")
                            mo.setValue(last_name, forKey: "last_name")
                            mo.setValue(user_type_id, forKey: "user_type_id")
                            mo.setValue(search_string, forKey: "search_string")
                            mo.setValue("0", forKey: "is_removed")
                        }
                        
                        success = self.db.saveObjectContext(ctx!)
                    })
                    
                    if success {
                        if group_id == current_group {
                            DispatchQueue.main.async(execute: {
                                let messageA: String! = "\(members.count) \(NSLocalizedString("members have been added to this group.", comment: ""))"
                                let messageB: String! = NSLocalizedString("A new member has been added to this group.", comment: "")
                                let messageC: String! = members.count > 1 ? messageA : messageB
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_MESSAGE_TOAST"), object: messageC)
                            })
                        }
                        else {
                            DispatchQueue.main.async(execute: {
                                let messageA: String! = "\(members.count) \(NSLocalizedString("members have been added to", comment: "")) \"\(name!)\" \(NSLocalizedString("group.", comment: ""))"
                                let messageB: String! = "\(NSLocalizedString("A new member has been added to", comment: "")) \"\(name!)\" \(NSLocalizedString("group.", comment: ""))"
                                let messageC: String! = "\(String(describing: members.count > 1 ? messageA : messageB))"
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_MESSAGE_TOAST"), object: messageC)
                            })
                        }
                    }
                }
            }
        }
        }
        
        // GROUP MEMBER REMOVE
        // {"members":[{"id":"757","user_id":"5","group_id":"2","sub_group_id":"223","is_deleted":1,
        // "date_created":"2016-08-04 06:59:51","date_modified":"2016-08-04 06:59:51"}]}
        // important: id
        
        if key == SSMConstants.SocketIOListenerKey.GROUP_MEMBER_REMOVE {
            let current_user = self.accountUserID()
            var remove_members = [String]()
            var remove_groups = [String]()
            var selected_group_id = ""
            
            for record in data {
                guard let members = record["members"] as? [[String: AnyObject]] else {
                    print("Can't understand response structure (members)!")
                    return
                }
                
                for member in members {
                    let section_id: String! = self.stringValue(member["group_id"])
                    
                    if section_id == current_section {
                        let user_id: String! = self.stringValue(member["user_id"])
                        let group_id: String! = self.stringValue(member["sub_group_id"])
                        let found_group = self.didFindGroup(group_id!)
                    
                        if found_group {
                            user_id! == current_user ? remove_groups.append(group_id!) : remove_members.append(user_id!)
                    selected_group_id = group_id!
                }
                    }
                }
                
                // ASSUMPTION: Updates came from a single group per emit
                // Removing same student from different groups at a time will never happen (OS concept)
                
                let predicate = self.db.createPredicate(key: "id", withValue: selected_group_id, isExact: true)
                let group_object = self.db.retrieveEntity(SSMConstants.Entity.GROUP, filter: predicate)
                let selected_group_name: String! = self.stringValue(group_object?.value(forKey: "name") as AnyObject?)
              
                if remove_groups.count > 0 {
                    let predicate = NSPredicate(format: "id IN %@", remove_groups)
                    let success = self.db.clearEntity(SSMConstants.Entity.GROUP, ctx: ctx!, filter: predicate)
                    
                    if success {
                        if remove_groups.contains(current_group!) {
                            DispatchQueue.main.async(execute: {
                                let message: String! = NSLocalizedString("You have been removed from this group.", comment: "")
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_MESSAGE_TOAST"), object: message)
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_DEFAULT_VIEW"), object: nil)
                            })
                        }
                        else {
                            DispatchQueue.main.async(execute: {
                                let message: String! = "\(NSLocalizedString("You have been removed from", comment: "")) \"\(selected_group_name!)\" \(NSLocalizedString("group.", comment: ""))"
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_MESSAGE_TOAST"), object: message)
                            })
                        }
                    }
                }
                
                if remove_members.count > 0 {
                    let predicate = NSPredicate(format: "id IN %@", remove_members)
                    let success = self.db.clearEntity(SSMConstants.Entity.MEMBER, ctx: ctx!, filter: predicate)
                    
                    if success {
                        DispatchQueue.main.async(execute: {
                            let messageA: String! = "\(remove_members.count) \(NSLocalizedString("members have been removed from", comment: "")) \"\(selected_group_name!)\" \(NSLocalizedString("group.", comment: ""))"
                            let messageB: String! = "\(NSLocalizedString("A member has been removed from", comment: "")) \"\(selected_group_name!)\" \(NSLocalizedString("group.", comment: ""))"
                            let messageC: String = remove_members.count > 1 ? messageA : messageB
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_MESSAGE_TOAST"), object: messageC)
                        })
                    }
                }
            }
        }
    }
    
    // MARK: - Socket IO Emitters for Group and Member
    
    fileprivate func handleSocketIOEmitterForKey(_ key: String, data: [String: AnyObject]) {
        self.prettyFunction()
        
        let current_group: String! = self.stringValue(self.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID"))
        let ctx = self.getContext()
        
        // GROUP CREATE
        // {"group":{"id":"34","group_id":"24","name":"Another Kimmy","is_deleted":0,
        // "date_created":"2016-07-25 08:16:16","date_modified":"2016-07-25 08:16:16",
        // "user_id":"4", "members":[{"id":"6","first_name":"Web","last_name":
        // "Student 3","user_type_id":"4","avatar":"/public/img/avatar/avatar-one.png"}]}}
        
        if key == SSMConstants.SocketIOEmitterKey.GROUP_CREATE {
            let id: String! = self.stringValue(data["id"])
            let user_id: String! = self.stringValue(data["user_id"])
            let group_id: String! = self.stringValue(data["group_id"])
            let name: String! = self.stringValue(data["name"])
            let is_deleted: String! = self.stringValue(data["is_deleted"])
            let date_created: String! = self.stringValue(data["date_created"])
            let date_modified: String! = self.stringValue(data["date_modified"])
            
            var new_members = [[String: AnyObject]]()
            
            if let members = data["members"] as? [[String: AnyObject]] {
                for m in members {
                    let member_id: String! = self.stringValue(m["id"])
                    let first_name: String! = self.stringValue(m["first_name"])
                    let last_name: String! = self.stringValue(m["last_name"])
                    let user_type_id: String! = self.stringValue(m["user_type_id"])
                    let avatar: String! = self.stringValue(m["avatar"])
                    
                    let member_data: [String:String] = ["id": member_id,"first_name": first_name,"last_name": last_name,"user_type_id": user_type_id,"avatar": avatar]
                    new_members.append(member_data as [String : AnyObject])
                }
            }
            
            let object = ["group": ["id": id!,"group_id": group_id!,"name": name!,"is_deleted": is_deleted!, "date_created": date_created!,"date_modified": date_modified!, "user_id": user_id!], "members": new_members] as [String : Any]
            self.socket.emit(key, object)
            
            // UPDATE UI
            ctx?.performAndWait({
                let predicate = self.db.createPredicate(key:"id", withValue: id!, isExact: true)
                let mo = self.db.retrieveEntity(SSMConstants.Entity.GROUP, context: ctx!, filter: predicate)
                
                mo.setValue(id, forKey: "id")
                mo.setValue(name, forKey: "name")
                mo.setValue(group_id, forKey: "group_id")
                mo.setValue(group_id, forKey: "section_id")
                mo.setValue("0", forKey: "is_selected")
                mo.setValue(0, forKey: "notification_count")
                
                _ = self.db.saveObjectContext(ctx!)
            })
        }
        
        // GROUP REMOVE
        // {"group":{"id":"34","name":"Another Kimmy","group_id":"24"}}
        
        if key == SSMConstants.SocketIOEmitterKey.GROUP_REMOVE {
            let id: String! = self.stringValue(data["id"])
            let predicate = self.db.createPredicate(key: "id", withValue: id!, isExact: true)
            
            if let group_object = self.db.retrieveEntity(SSMConstants.Entity.GROUP, filter: predicate) {
                let group_id: String! = self.stringValue(group_object.value(forKey: "id") as AnyObject?)
                let name: String! = self.stringValue(group_object.value(forKey: "name"))
                let section_id: String! = self.stringValue(group_object.value(forKey: "group_id"))
                let socket_data = ["group": ["id": group_id!, "name": name!, "group_id": section_id!]]
                self.socket.emit(key, socket_data)
            }
            
            let success = self.db.clearEntity(SSMConstants.Entity.GROUP, ctx: self.getContext(), filter: predicate)
            
            if success {
                if id == current_group {
                    DispatchQueue.main.async(execute: {
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_DEFAULT_VIEW"), object: nil)
                    })
                }
            }
        }
        
        // GROUP UPDATE
        // {"group":{"id":"34","group_id":"24","name":"Another Kimmy","is_deleted":0,
        // "date_created":"2016-07-25 08:16:16","date_modified":"2016-07-25 08:16:16",
        // "user_id":"4", "members":[{"id":"6","first_name":"Web","last_name":
        // "Student 3","user_type_id":"4","avatar":"/public/img/avatar/avatar-one.png"}]}}
        
        if key == SSMConstants.SocketIOEmitterKey.GROUP_UPDATE {
            let id: String! = self.stringValue(data["id"])
            let user_id: String! = self.stringValue(data["user_id"])
            let group_id: String! = self.stringValue(data["group_id"])
            let name: String! = self.stringValue(data["name"])
            let is_deleted: String! = self.stringValue(data["is_deleted"])
            let date_created: String! = self.stringValue(data["date_created"])
            let date_modified: String! = self.stringValue(data["date_modified"])
            
            let object = ["group": ["id": id!,"group_id": group_id!,"name": name!,"is_deleted": is_deleted!, "date_created": date_created!,"date_modified": date_modified!, "user_id": user_id!]]
            self.socket.emit(key, object)
            
            // GROUP MEMBER ADD
            // {"group":{"id":"223","name":"Singapore","group_id":"2"},"members":[{"id":"6","first_name":"Web",
            // "last_name":"Student 3","user_type_id":"4","avatar":"/public/img/avatar/avatar-one.png"}]}
            
            let predicate_add = self.db.createPredicate(key:"is_removed", withValue: "0", isExact: true)
            if let added_members = self.db.retrieveObjects(forEntity: SSMConstants.Entity.MEMBER, withFilter: predicate_add) {
                var new_members = [[String: AnyObject]]()
                
                for m in added_members {
                    let member_id: String! = self.stringValue(m.value(forKey: "id") as AnyObject?)
                    let first_name: String! = self.stringValue(m.value(forKey: "first_name") as AnyObject?)
                    let last_name: String! = self.stringValue(m.value(forKey: "last_name") as AnyObject?)
                    let user_type_id: String! = self.stringValue(m.value(forKey: "user_type_id") as AnyObject?)
                    let avatar: String! = self.stringValue(m.value(forKey: "avatar") as AnyObject?)
                    let homeUrl: String! = "http://\(self.retrieveVSmartBaseURL())"
                    let avatar_stripped: String! = (avatar?.replacingOccurrences(of: homeUrl, with: ""))
                    let member_data: [String : String] = ["id": member_id, "first_name": first_name, "last_name": last_name, "user_type_id": user_type_id, "avatar": avatar_stripped]
                    new_members.append(member_data as [String : AnyObject])
                }
                
                
                let emit_data = ["group":["id": id!, "name": name!, "group_id": group_id!], "members": new_members] as [String : Any]
                self.socket.emit(SSMConstants.SocketIOEmitterKey.GROUP_MEMBER_ADD, emit_data)
            }
            
            // GROUP MEMBER REMOVE
            // {"members":[{"id":"757","user_id":"5","group_id":"2","sub_group_id":"223","is_deleted":1,"date_created":
            // "2016-08-04 06:59:51","date_modified":"2016-08-04 06:59:51"}]}
            
            let predicate_remove = self.db.createPredicate(key:"is_removed", withValue: "1", isExact: true)
            if let removed_members = self.db.retrieveObjects(forEntity: SSMConstants.Entity.MEMBER, withFilter: predicate_remove) {
                var old_members = [[String: AnyObject]]()
                
                for m in removed_members {
                    let member_id: String! = self.stringValue(m.value(forKey: "id") as AnyObject?)
                    let member_data: [String : String] = ["id": member_id!, "user_id": member_id!, "group_id": group_id!, "sub_group_id": id!, "is_deleted": "1", "date_created": "", "date_modified": ""]
                    old_members.append(member_data as [String : AnyObject])
                }
                
                let emit_data = ["members": old_members]
                self.socket.emit(SSMConstants.SocketIOEmitterKey.GROUP_MEMBER_REMOVE, emit_data)
            }
            
            // UPDATE UI
            ctx?.performAndWait({
                let predicate = self.db.createPredicate(key:"id", withValue: id!, isExact: true)
                let mo = self.db.retrieveEntity(SSMConstants.Entity.GROUP, context: ctx!, filter: predicate)
                let is_selected = (id == current_group) ? "1" : "0"
                
                mo.setValue(id, forKey: "id")
                mo.setValue(name, forKey: "name")
                mo.setValue(group_id, forKey: "group_id")
                mo.setValue(group_id, forKey: "section_id")
                mo.setValue(is_selected, forKey: "is_selected")
                mo.setValue(0, forKey: "notification_count")
                
                _ = self.db.saveObjectContext(ctx!)
            })
        }
    }
    
    fileprivate func didFindGroup(_ groupID: String) -> Bool {
        let predicate = self.db.createPredicate(key: "id", withValue: groupID)
        let group_object = self.db.retrieveEntity(SSMConstants.Entity.GROUP, filter: predicate)
        return group_object != nil ? true : false
    }
    
    // MARK: - Core Data Related Implementations
    
    func fetchSections () -> [AnyObject]? {
        let entity = SSMConstants.Entity.SECTION
        let properties = ["group_id", "name", "id", "section"]
        guard let data = self.db.retrieveEntity(entity,
                                                properties: properties,
                                                filter: nil,
                                                sortKey: "group_id",
                                                ascending: true) else {
                                                    return nil
        }
        
        return data as [AnyObject]?
    }
    
    func removeGroupMember(_ memberID: String, handler: SocialStreamDoneBlock) {
        let predicate = self.db.createPredicate(key: "id", withValue: memberID, isExact: true)
        let success = self.db.updateEntity(SSMConstants.Entity.MEMBER, predicate: predicate, dictionary: ["is_removed": "1" as AnyObject])
        handler(success)
    }
    
    func createGroupMembers(_ members: [String], handler: @escaping SocialStreamDoneBlock) {
        if members.count > 0 {
            let ctx = self.getContext()
            
            ctx?.performAndWait({
                for id in members {
                    let predicate = self.db.createPredicate(key: "id", withValue: id, isExact: true)
                    
                    if let studentObject = self.db.retrieveEntity(SSMConstants.Entity.STUDENT, filter: predicate) {
                        let avatar: String! = self.stringValue(studentObject.value(forKey: "avatar"))
                        let first_name: String! = self.stringValue(studentObject.value(forKey: "first_name"))
                        let last_name: String! = self.stringValue(studentObject.value(forKey: "last_name"))
                        let search_string: String! = self.stringValue(studentObject.value(forKey: "search_string"))
                        
                        let memberObject = self.db.retrieveEntity(SSMConstants.Entity.MEMBER, context: ctx!, filter: predicate)
                        
                        memberObject.setValue(id, forKey: "id")
                        memberObject.setValue(avatar, forKey: "avatar")
                        memberObject.setValue(first_name, forKey: "first_name")
                        memberObject.setValue(last_name, forKey: "last_name")
                        memberObject.setValue(search_string, forKey: "search_string")
                        memberObject.setValue("0", forKey: "is_removed")
                    }
                }
                
                let success = self.db.saveObjectContext(ctx!)
                handler(success)
            })
        }
    }
    
    func mapSelectedGroup(_ groupID: String, data: [String: String]) -> Bool {
        let ctx = self.getContext()
        _ = self.db.clearEntity(SSMConstants.Entity.MAPPING, ctx: ctx!, filter: nil)
        
        let predicate = self.db.createPredicate(key:"group_id", withValue: groupID, isExact: true)
        let object = self.db.retrieveEntity(SSMConstants.Entity.MAPPING, context: ctx!, filter: predicate)
        let allKeys = Array(data.keys)
        
        for key in allKeys {
            let value: String! = self.stringValue(data[key] as AnyObject?)
            object.setValue(value, forKey: key)
        }
        
        return self.db.saveObjectContext(ctx!)
    }
    
    func updateSelectionForGroup(_ groupID: String) {
        // Select
        let predicateA = NSPredicate(format: "id == \(groupID)")
        _ = self.db.updateEntity(SSMConstants.Entity.GROUP, predicate: predicateA, dictionary: ["is_selected": "1" as AnyObject])
        
        // Deselect
        let predicateB = NSPredicate(format: "id != \(groupID)")
        _ = self.db.updateObjectsForEntity(SSMConstants.Entity.GROUP, predicate: predicateB, dictionary: ["is_selected": "0" as AnyObject])
    }
    
    func updateNotificationCountForGroup(_ groupID: String) {
        let predicate = NSPredicate(format: "id == \(groupID)")
        let group = self.db.retrieveEntity(SSMConstants.Entity.GROUP, filter: predicate)
        let notification_count_string: String! = self.stringValue(group?.value(forKey: "notification_count"))
        let notification_count = Int(notification_count_string!)
        
        if (notification_count != nil && notification_count! > 0) {
            _ = self.db.updateEntity(SSMConstants.Entity.GROUP, predicate: predicate, dictionary: ["notification_count": ((notification_count! - 1)) as AnyObject])
        }
    }
    
    func deepCopyStudentEntity(_ handler: @escaping SocialStreamDoneBlock) {
        guard let students = self.db.retrieveObjects(forEntity: SSMConstants.Entity.STUDENT, withFilter: nil) else {
            print("Can't retrieve student objects!")
            handler(false)
            return
        }
        
        let ctx = self.getContext()
        
        ctx?.performAndWait {
            for student in students {
                let id: String! = self.stringValue(student.value(forKey: "id") as AnyObject?)
                let avatar: String! = self.stringValue(student.value(forKey: "avatar") as AnyObject?)
                let first_name: String! = self.stringValue(student.value(forKey: "first_name") as AnyObject?)
                let last_name: String! = self.stringValue(student.value(forKey: "last_name") as AnyObject?)
                let search_string: String! = self.stringValue(student.value(forKey: "search_string") as AnyObject?)
                
                let predicate = self.db.createPredicate(key:"id", withValue: id!, isExact: true)
                let mo = self.db.retrieveEntity(SSMConstants.Entity.MEMBER, context: ctx!, filter: predicate)
                
                mo.setValue(id, forKey: "id")
                mo.setValue(avatar, forKey: "avatar")
                mo.setValue(first_name, forKey: "first_name")
                mo.setValue(last_name, forKey: "last_name")
                mo.setValue(search_string, forKey: "search_string")
                mo.setValue("0", forKey: "is_removed")
                mo.setValue("0", forKey: "user_type_id")
            }
            
            let success = self.db.saveObjectContext(ctx!)
            handler(success)
        }
    }
    
    // MARK: - Debugging Helper
    
    fileprivate func prettyFunction(_ file: NSString = #file, function: String = #function, line: Int = #line) {
        print("<start>--- file: \(file.lastPathComponent) function:\(function) line:\(line) ---<end>")
    }
    
    func requestUserMessage(withMessageID message_id:String, clearEntities:Bool, handler:@escaping SocialStreamDoneBlock) {
        
        let homeURL = "http://\(retrieveVSmartBaseURL())"
        let apiURL = "http://\(retrieveVSmartAPIURL())"
        let endpoint = "\(apiURL)/v1/stream/getmessage/\(message_id)"
        let url: URL = URL(string: endpoint)!
        let request = self.buildURLRequestWithMethod("GET", url: url, andBody: nil)
        let ctx = self.getContext()
        
        if clearEntities {
            let entity = SSMConstants.Entity.MESSAGEFEED
            _ = self.db.clearEntity(entity, ctx: ctx!, filter: nil)
            _ = self.db.clearEntity(SSMConstants.Entity.LIKES, ctx: ctx!, filter: nil)
            _ = self.db.clearEntity(SSMConstants.Entity.COMMENTS, ctx: ctx!, filter: nil)
            _ = self.db.clearEntity(SSMConstants.Entity.COMMENT_LIKES, ctx: ctx!, filter: nil)
            _ = self.db.clearEntity(SSMConstants.Entity.URLDATA, ctx: ctx!, filter: nil)
        }
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                handler(false)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else {
                handler(false)
                return
            }
            
            if ( self.okayToParseResponse(dictionary) ) {
                
                guard let records = dictionary["records"] as? [[String:AnyObject]] else {
                    handler(false)
                    return
                }
                
                ctx?.performAndWait({
                    
                    //                    if status == true {
                    for r in records {
                        
                        print("item group: \(r)")
                        /*
                         {
                         id: "199",
                         user_id: "2",
                         group_id: "2",
                         emoticon_id: null,
                         message: "testing Posting",
                         b_title: null,
                         is_attached: "0",
                         is_deleted: "0",
                         date_created: "2016-07-26 03:47:52",
                         date_modified: "2016-07-26 03:47:52",
                         username: "webteacher1@vsmart.com",
                         avatar: "/uploads/2/avatars/20160711/2e3c0708549d5f069699bae3ae11506f.png",
                         attachCount: "0",
                         current_date: "2016-08-01 10:30:13",
                         icon_url: null,
                         icon_text: null,
                         first_name: "Web",
                         last_name: "Teacher 1",
                         }
                         */
                        
                        // PARSE URL DATA
                        var url_data = Set<NSManagedObject>()
                        var type: String! = "message"
                        if r["url_data"] is [[String:AnyObject]] {
                            let urlData = r["url_data"] as! [[String:AnyObject]]
                            
                            let baseData = urlData.last
                            type = self.stringValue(baseData!["msg_type"])
                            
                            
                            let url = self.stringValue(baseData!["url"])
                            if (url?.characters.count)! > 0 {
                                type = "link"
                            }
                            
                            url_data = self.processURLData(urlData, context: ctx)
                        }
                        
                        var comments = Set<NSManagedObject>()
                        if r["comments"] is [[String:AnyObject]] {
                            let commentData = r["comments"] as! [[String:AnyObject]]
                            comments = self.processComments(commentData, context: ctx)
                        }
                        
                        var likes = Set<NSManagedObject>()
                        var is_liked = "0"
                        if r["post_likes"] is [[String:AnyObject]] {
                            let likeData = r["post_likes"] as! [[String:AnyObject]]
                            let loggedInUserID = self.accountUserID()
                            let arrayOfFilteredLikes = likeData.filter({
                                ((self.stringValue($0["user_id"])) == loggedInUserID)
                            })
                            
                            is_liked = (arrayOfFilteredLikes.count > 0) ? "1" : "0"
                            
                            likes = self.processLikes(likeData, context: ctx)
                        }
                        
                        let message_id: String! = self.stringValue(r["id"])
                        let user_id: String! = self.stringValue(r["user_id"])
                        //let group_id: String! = self.stringValue(r["group_id"])
                        let emoticon_id: String! = self.stringValue(r["emoticon_id"])
                        var message: String! = self.stringValue(r["message"])
                        let b_title: String! = self.stringValue(r["b_title"])
                        let is_attached: String! = self.stringValue(r["is_attached"])
                        let is_deleted: String! = self.stringValue(r["is_deleted"])
                        let date_created: String! = self.stringValue(r["date_created"])
                        let date_modified: String! = self.stringValue(r["date_modified"])
                        let username: String! = self.stringValue(r["username"])
                        let avatar_image: String! = self.stringValue(r["avatar"])
                        let avatar: String! = "\(homeURL)\(avatar_image!)"
                        let attach_count: String! = self.stringValue(r["attachCount"])
                        let current_date: String! = self.stringValue(r["current_date"])
                        let icon_url: String! = "\(homeURL)\(self.stringValue(r["icon_url"])!)"
                        let icon_text: String! = self.stringValue(r["icon_text"])
                        let first_name: String! = self.stringValue(r["first_name"])
                        let last_name: String! = self.stringValue(r["last_name"])
                        
                        if type == "sticker" {
                            message = message?.replacingOccurrences(of: "<img src='", with: "", options: .regularExpression, range: nil)
                            message = message?.replacingOccurrences(of: "'/>", with: "", options: .regularExpression, range: nil)
                            
                            //self.stripHTLMTags(inString: message)
                            
                            message = (message?.contains(homeURL))! ? message : "\(homeURL)\(message!)"
                        }
                        
                        let comment_count: String! = self.stringValue(comments.count)
                        let like_count: String! = self.stringValue(likes.count)
                        
                        let date_modified_date = self.date(fromString: date_modified!, withFormat: "yyyy-MM-dd HH:mm:ss", convertToLocalTime: true)
                        
                        let predicate = self.db.createPredicate(key:"group_id", withValue: "NOTIFICATION", isExact: true)
                        let mo = self.db.retrieveEntity(SSMConstants.Entity.MESSAGEFEED, context: ctx!, filter: predicate)
                        
                        mo.setValue(message_id, forKey: "id")
                        mo.setValue(user_id, forKey: "user_id")
                        mo.setValue("NOTIFICATION", forKey: "group_id") // for notification filtering
                        mo.setValue(emoticon_id, forKey: "emoticon_id")
                        mo.setValue(message, forKey: "message")
                        mo.setValue(b_title, forKey: "b_title")
                        mo.setValue(is_attached, forKey: "is_attached")
                        mo.setValue(is_deleted, forKey: "is_deleted")
                        mo.setValue(date_created, forKey: "date_created")
                        mo.setValue(date_modified, forKey: "date_modified")
                        mo.setValue(username, forKey: "username")
                        mo.setValue(avatar, forKey: "avatar")
                        mo.setValue(attach_count, forKey: "attach_count")
                        mo.setValue(current_date, forKey: "current_date")
                        mo.setValue(icon_url, forKey: "icon_url")
                        mo.setValue(icon_text, forKey: "icon_text")
                        mo.setValue(first_name, forKey: "first_name")
                        mo.setValue(last_name, forKey: "last_name")
                        mo.setValue(date_modified_date, forKey: "date_modified_date")
                        
                        mo.setValue(comment_count, forKey: "comment_count")
                        mo.setValue(like_count, forKey: "like_count")
                        mo.setValue(type, forKey: "type")
                        mo.setValue(is_liked, forKey: "is_liked")
                        
                        // URL_DATA RELATIONSHIP
                        mo.setValue(url_data, forKey: "url_data")
                        
                        // COMMENT RELATIONSHIP
                        mo.setValue(comments, forKey: "comments")
                        
                        // LIKE RELATIONSHIP
                        mo.setValue(likes, forKey: "likes")
                        
                    }
                    let success = self.db.saveObjectContext(ctx!)
                    //                        self.db.displayRecords(entity)
                    handler(success)
                    
                    //                    }
                    //                    else {
                    //                        handler(doneBlock:false)
                    //                    }
                })
            }
            else {
                handler(false)
            }
        })
        task.resume()
    }

}
