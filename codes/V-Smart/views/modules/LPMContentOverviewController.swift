//
//  LPMContentOverviewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 23/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMContentOverviewController: UIViewController {
    
    @IBOutlet fileprivate var teachingDateLabel: UILabel!
    @IBOutlet fileprivate var startLabel: UILabel!
    @IBOutlet fileprivate var endLabel: UILabel!
    @IBOutlet fileprivate var gradeLevelLabel: UILabel!
    @IBOutlet fileprivate var quarterLabel: UILabel!
    
    @IBOutlet fileprivate var titleTextField: UITextField!
    @IBOutlet fileprivate var startTextField: UITextField!
    @IBOutlet fileprivate var endTextField: UITextField!
    @IBOutlet fileprivate var gradeLevelTextField: UITextField!
    @IBOutlet fileprivate var quarterTextField: UITextField!
    
    @IBOutlet fileprivate var startButton: UIButton!
    @IBOutlet fileprivate var endButton: UIButton!
    @IBOutlet fileprivate var gradeLevelButton: UIButton!
    @IBOutlet fileprivate var quarterButton: UIButton!
    @IBOutlet fileprivate var cancelButton: UIButton!
    @IBOutlet fileprivate var createButton: UIButton!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let startButtonAction = #selector(self.startButtonAction(_:))
        self.startButton.addTarget(self, action: startButtonAction, for: .touchUpInside)
        
        let endButtonAction = #selector(self.endButtonAction(_:))
        self.endButton.addTarget(self, action: endButtonAction, for: .touchUpInside)
        
        let gradeLevelButtonAction = #selector(self.gradeLevelButtonAction(_:))
        self.gradeLevelButton.addTarget(self, action: gradeLevelButtonAction, for: .touchUpInside)
        
        let quarterButtonAction = #selector(self.quarterButtonAction(_:))
        self.quarterButton.addTarget(self, action: quarterButtonAction, for: .touchUpInside)

        let cancelButtonAction = #selector(self.cancelButtonAction(_:))
        self.cancelButton.addTarget(self, action: cancelButtonAction, for: .touchUpInside)
        
        let createButtonAction = #selector(self.createButtonAction(_:))
        self.createButton.addTarget(self, action: createButtonAction, for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Button Event Handlers
    
    func startButtonAction(_ sender: UIButton) {
        
    }
    
    func endButtonAction(_ sender: UIButton) {
        
    }
    
    func gradeLevelButtonAction(_ sender: UIButton) {
        
    }
    
    func quarterButtonAction(_ sender: UIButton) {
        
    }
    
    func cancelButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func createButtonAction(_ sender: UIButton) {
        
    }
}
