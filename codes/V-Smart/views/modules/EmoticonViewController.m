//
//  EmoticonViewController.m
//  V-Smart
//
//  Created by VhaL on 3/24/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "EmoticonViewController.h"
#import "EmoticonCell.h"


@interface EmoticonViewController ()
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@end

@implementation EmoticonViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    UINib *cellNib = [UINib nibWithNibName:@"EmoticonCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"EmoticonCell"];
}

-(void)loadData{
    [self.collectionView reloadData];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if ([collectionView isEqual:self.collectionView]){
        return self.emoItems.records.count;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    if ([collectionView isEqual:self.collectionView]){
        return 1;
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([collectionView isEqual:self.collectionView]){
        EmoticonCell *cell = (EmoticonCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:@"EmoticonCell" forIndexPath:indexPath];
        
        JMSEmoItem *item = [self.emoItems.records objectAtIndex:indexPath.row];
        [cell initializeCell:self];
        
        NSString *url = [Utils buildUrl:item.iconUrl];
        
        cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
        cell.labelName.text = item.name;
        return cell;
    }
    
    return nil;
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize retval = CGSizeMake(200, 30);
    return retval;
}

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    return UIEdgeInsetsMake(0, 0, 10, 0);
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([collectionView isEqual:self.collectionView]){
        VLog(@"cell selected");
        JMSEmoItem *item = [self.emoItems.records objectAtIndex:indexPath.row];
        
        UIPopoverController *popOverController = (UIPopoverController*)self.parent;
        [popOverController dismissPopoverAnimated:YES];
        
        if ([self.delegate respondsToSelector:@selector(emoticonTapped:)]){
            [self.delegate performSelector:@selector(emoticonTapped:) withObject:item];
        }
    }
}


@end
