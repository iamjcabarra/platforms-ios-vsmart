//
//  LPMCommentViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 01/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMCommentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {
    
    @IBOutlet fileprivate var commentTableView: UITableView!
    @IBOutlet fileprivate var newCommentText: UITextView!
    @IBOutlet fileprivate var sendNewCommentButton: UIButton!
    @IBOutlet fileprivate var emptyPlaceholderView: UIView!
    @IBOutlet fileprivate var emptyPlaceholderLabel: UILabel!
    
    var lpid:String! = ""
    
    fileprivate let kCommentCellIdentifier = "comment_cell_identifier"
    fileprivate let kCommentModalViewSegueIdentifier = "SHOW_COMMENT_MODAL_VIEW"
    
    fileprivate var userid:String! = ""
    fileprivate var selectedCommentObject: NSManagedObject!
    
    // MARK: - Data Manager
    
    fileprivate lazy var lessonPlanDataManager: LessonPlanDataManager = {
        let lpdm = LessonPlanDataManager.sharedInstance()
        return lpdm!
    }()
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userid = lessonPlanDataManager.loginUser()
        
        self.commentTableView.estimatedRowHeight = 150.0;
        self.commentTableView.rowHeight = UITableViewAutomaticDimension;
        
        let sendCommentButtonAction = #selector(self.sendCommentButtonAction(_:))
        self.sendNewCommentButton.addTarget(self, action: sendCommentButtonAction, for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Button Event Handlers
    
    func sendCommentButtonAction(_ sender: UIButton) {
        let comment:String = "\(self.newCommentText.text!)"
        
        if (comment != "") {
            let indicatorString = "\(NSLocalizedString("Posting", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
            
            let user_id_string:String = self.userid!
            let lp_id_string:String = self.lpid!
            print("RYAN user id string : \(user_id_string)")
            print("RYAN lesson plan id string : \(lp_id_string)")
            
            let commentData = ["user_id": user_id_string, "lp_id": lp_id_string, "comment": comment]
            
            print("RYAN comment Data : \(commentData)")
            
            self.lessonPlanDataManager.requestPostComment(commentData, doneBlock: { (success) in
                if (success) {
                    self.lessonPlanDataManager.requestLessonPlanDetailsForLessonPlan(withID: self.lpid, doneBlock: { (success) in
                        DispatchQueue.main.async(execute: {
                            HUD.hideUIBlockingIndicator()
                            
                            if (success) {
                                self.newCommentText.text = ""
                                self.reloadFetchedResultsController()
                            }
                            else {
                                let message = NSLocalizedString("There was an error posting this comment. Please try again later.", comment: "")
                                self.showNotificationMessage(message)
                            }
                        })
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        let message = NSLocalizedString("There was an error posting this comment. Please try again later.", comment: "")
                        self.showNotificationMessage(message)
                    })
                }
            })
        }
        
        self.newCommentText.endEditing(true)
    }
    
    func editCommentButtonAction(_ sender: UIButton) {
        self.selectedCommentObject = self.managedObjectFromButton(sender, inTableView: self.commentTableView)
        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: self.kCommentModalViewSegueIdentifier, sender: nil)
        })
    }
    
    func deleteCommentButtonAction(_ sender: UIButton) {
        let commentObject = self.managedObjectFromButton(sender, inTableView: self.commentTableView)
        
        let message = NSLocalizedString("Are you sure you want to delete this comment?", comment: "")
        let positiveString = NSLocalizedString("Yes", comment: "")
        let negativeString = NSLocalizedString("No", comment: "")
        
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let positiveAction = UIAlertAction(title: NSLocalizedString(positiveString, comment: ""), style: .default) { (Alert) -> Void in
            let indicatorString = "\(NSLocalizedString("Deleting", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
            
            self.lessonPlanDataManager.requestDeleteLessonPlanComment(commentObject, doneBlock: { (success) in
                if (success) {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        self.reloadFetchedResultsController()
                        alert.dismiss(animated: true, completion: nil)
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        let message = NSLocalizedString("There was an error deleting this comment. Please try again later.", comment: "")
                        self.showNotificationMessage(message)
                        alert.dismiss(animated: true, completion: nil)
                    })
                }
            })
        }
        
        let negativeAction = UIAlertAction(title: NSLocalizedString(negativeString, comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(positiveAction)
        alert.addAction(negativeAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK:- Alert Message View
    
    func showNotificationMessage(_ message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: NSLocalizedString("Okay", comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
    
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Empty Placeholder View
    
    func shouldShowEmptyPlaceholderView(_ show: Bool) {
        if (show) {
            let message = NSLocalizedString("No available comment.", comment: "")   // unchecked
            self.emptyPlaceholderLabel.text = message
        }
        
        self.emptyPlaceholderView.isHidden = !show
        self.commentTableView.isHidden = show
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        self.shouldShowEmptyPlaceholderView(sectionData.numberOfObjects > 0 ? false : true)
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kCommentCellIdentifier, for: indexPath) as! LPMCommentTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: LPMCommentTableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath) 
        
        let uid = mo.value(forKey: "user_id") as! String
        let avatar = mo.value(forKey: "avatar") as! String
        let firstName = mo.value(forKey: "first_name") as! String
        let lastName = mo.value(forKey: "last_name") as! String
        let dateCreated = mo.value(forKey: "date_created") as! String
        let dateModified = mo.value(forKey: "date_modified") as! String
        let comment = mo.value(forKey: "comment") as! String
        
        cell.userAvatarImage.sd_setImage(with: URL.init(string: avatar))
        cell.userNameLabel.text = "\(firstName) \(lastName)"
        
        let dateString = self.transformDateString(dateModified, fromFormat: "yyyy-MM-dd HH:mm:ss zzz", toFormat: "MMM. dd, yyyy, hh:mm a")
        cell.timeStampLabel.text = (dateCreated == dateModified) ? dateString : "\(dateString) \(NSLocalizedString("Edited", comment: ""))"
        
        cell.commentLabel.text = comment
        cell.backgroundColor = (uid == self.userid) ? UIColor(rgba: "#C3EDFF") : UIColor.white
        
        let own_user_id = self.lessonPlanDataManager.loginUser()
        let same_user = (own_user_id == uid) ? true : false
        
        cell.editButton.isHidden = same_user ? false : true
        cell.deleteButton.isHidden = same_user ? false : true
        
        let editCommentButtonAction = #selector(self.editCommentButtonAction(_:))
        cell.editButton.addTarget(self, action: editCommentButtonAction, for: .touchUpInside)
        
        let deleteCommentButtonAction = #selector(self.deleteCommentButtonAction(_:))
        cell.deleteButton.addTarget(self, action: deleteCommentButtonAction, for: .touchUpInside)
    }

    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = self.lessonPlanDataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: kCommentEntity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kCommentEntity)
        fetchRequest.fetchBatchSize = 20
        
        let predicate1 = self.lessonPlanDataManager.predicate(forKeyPath: "lp_id", andValue: self.lpid)
        let predicate2 = self.lessonPlanDataManager.predicate(forKeyPath: "lpc_is_deleted", andValue: "0")
        let predicate3 = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1!, predicate2!])
        fetchRequest.predicate = predicate3
        
        let sortDescriptor = NSSortDescriptor.init(key: "sorted_date_modified", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.commentTableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.commentTableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.commentTableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.commentTableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.commentTableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.commentTableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! LPMCommentTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.commentTableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.commentTableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.commentTableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.commentTableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.kCommentModalViewSegueIdentifier {
            let commentModalView = segue.destination as? LPMCommentModalController
            commentModalView?.selectedCommentObject = self.selectedCommentObject
        }
    }
    
    // MARK: - Class Helpers
    
    func transformDateString(_ string: String, fromFormat: String, toFormat: String) -> String {
        let formatter = DateFormatter.init()
        formatter.dateFormat = fromFormat
        formatter.timeZone = TimeZone.current
        
        let date = formatter.date(from: "\(string) GMT")
        formatter.dateFormat = toFormat
        
        return formatter.string(from: date!)
    }
    
    func managedObjectFromButton(_ button: UIButton, inTableView: UITableView) -> NSManagedObject {
        let buttonPosition = button.convert(CGPoint.zero, to: inTableView)
        let indexPath = inTableView.indexPathForRow(at: buttonPosition)
        let managedObject = self.fetchedResultsController.object(at: indexPath!) 
        
        return managedObject
    }
    
}
