//
//  SocialStreamGroupCell.h
//  V-Smart
//
//  Created by VhaL on 3/25/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialStreamGroupCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UILabel *groupName;

- (void)initializeCell:(id)delegate;
@end
