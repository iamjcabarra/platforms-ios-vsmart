//
//  SharedToViewController.m
//  V-Smart
//
//  Created by Carmelito Bayarcal on 20/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "SharedToViewController.h"
#import "CheckBoxCell.h"

@interface SharedToViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@end

@implementation SharedToViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINib *cellItemNib = [UINib nibWithNibName:@"CheckBoxCell" bundle:nil];
    [self.tableView registerNib:cellItemNib forCellReuseIdentifier:@"SharedToCellID"];
    // Do any additional setup after loading the view.
    
    // Default Height
    self.tableView.estimatedRowHeight = 44.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [self.cancelBtn addTarget:self action:@selector(cancelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)cancelButtonAction:(UIButton *)b {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.sharedArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SharedToCellID" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    CheckBoxCell *itemCell = (CheckBoxCell *)cell;
    
    NSDictionary *userDict = self.sharedArray[indexPath.row];
    NSString *name = [NSString stringWithFormat:@"%@", userDict[@"name"]];
    NSString *category = [NSString stringWithFormat:@"%@", userDict[@"category"]];
    
    itemCell.checkBoxButton.hidden = YES;
    itemCell.checkBoxLabel.text = name;
    itemCell.cellCategory.text = category;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CheckBoxCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    
//    BOOL checkBoxState = cell.checkBoxButton.selected;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        cell.checkBoxButton.selected = (checkBoxState) ? false : true;
//    });
//    
//    NSDictionary *userDict = self.sharedToList[indexPath.row];
//    NSDictionary *dict = [NSDictionary dictionaryWithDictionary:userDict];
//    
//    (checkBoxState) ? [self.shareList removeObject:dict] : [self.shareList addObject:dict];
//    
//    [self resizeCollectionView];
//    [self.collectionView reloadData];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
