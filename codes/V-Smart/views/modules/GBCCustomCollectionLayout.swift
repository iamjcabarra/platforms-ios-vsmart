//
//  GBCustomCollectionViewLayout.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 16/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

class GBCCustomCollectionLayout: UICollectionViewLayout {
    
    var numberOfColumns: Int!
    var itemAttributes : NSMutableArray!
    var itemsSize : NSMutableArray!
    var contentSize : CGSize!
    
    
    var orientation: UIInterfaceOrientation!
    //    private lazy var dataManager: GradeBookDataManager = {
    //        let tm = GradeBookDataManager.sharedInstance()
    //        return tm
    //    }()
    
    override func prepare() {
        self.itemAttributes?.removeAllObjects()
        //        self.numberOfColumns = Int(self.dataManager.countRecordsForEntity(kGradeBookEntityV2, attribute: "section_index", value: 1))
        //
        if self.collectionView?.numberOfSections == 0 {
            return
        }
        
        //        self.collectionView?.numberOfSections()
        
        self.numberOfColumns = self.collectionView?.numberOfItems(inSection: 0)
        //        print("NUMBER OF COLUMNS \(self.numberOfColumns)")
        
        if (self.itemAttributes != nil && self.itemAttributes.count > 0) {
            for section in 0..<self.collectionView!.numberOfSections {
                if section != 0 && section != 1  {
                    continue
                }
                //                self.collectionView.indexPathfor
                //
                //                self.
                //
                //            let numberOfItems : Int = self.collectionView!.numberOfItemsInSection(section)
                
                //                let visibleIndexes = self.collectionView?.indexPathForVisi
                //
                //                let lastIndex = (visibleIndexes?.count)! - 1
                //                var prevIndex = (visibleIndexes?.first?.row)! - 1
                //                prevIndex = (prevIndex < 0) ? 0 : prevIndex
                //
                //                let attributesNext : UICollectionViewLayoutAttributes = self.layoutAttributesForItemAtIndexPath(NSIndexPath(forItem: lastIndex, inSection: section))!
                //
                //                let attributesPrev : UICollectionViewLayoutAttributes = self.layoutAttributesForItemAtIndexPath(NSIndexPath(forItem: lastIndex, inSection: section))!
                //
                //                if section == 0 {
                //                    var frameNext = attributesNext.frame
                //                    frameNext.origin.y = self.collectionView!.contentOffset.y
                //                    attributesNext.frame = frameNext
                //
                //                    var framePrev = attributesPrev.frame
                //                    framePrev.origin.y = self.collectionView!.contentOffset.y
                //                    attributesNext.frame = framePrev
                //                }
                //                if section == 1 {
                //                    var frameNext = attributesNext.frame
                //                    frameNext.origin.y = self.collectionView!.contentOffset.y + 44
                //                    attributesNext.frame = frameNext
                //
                //                    var framePrev = attributesPrev.frame
                //                    framePrev.origin.y = self.collectionView!.contentOffset.y + 44
                //                    attributesNext.frame = framePrev
                //                }
                
                
                
                
                //                let numberOfItems : Int = self.collectionView!.numberOfItemsInSection(section)
                
                
                //                let visibleIndexPaths = self.collectionView?.indexPathsForVisibleItems()
                //                let filteredVisible = visibleIndexPaths?.filter({
                //                    ($0.section) == 0
                //                })
                
                //                let filteredNumberOfItems =/
                
                
                
                let numberOfItems : Int = self.collectionView!.numberOfItems(inSection: section)
                for index in 0..<numberOfItems {
                    print("RETURN SECTION! \(section), INDEX! \(index)")
                    
                    let attributes : UICollectionViewLayoutAttributes = self.layoutAttributesForItem(at: IndexPath(item: index, section: section))!
                    if section == 0 {
                        var frame = attributes.frame
                        frame.origin.y = self.collectionView!.contentOffset.y
                        attributes.frame = frame
                    }
//                    if section == 1 {
//                        var frame = attributes.frame
//                        frame.origin.y = self.collectionView!.contentOffset.y + 44
//                        attributes.frame = frame
//                    }
                    //
                    //                    if index == 0 {
                    //                        var frame = attributes.frame
                    //                        frame.origin.x = self.collectionView!.contentOffset.x
                    //                        attributes.frame = frame
                    //                    }
                }
            }
            return
        }
        
        let currentOrientation = UIApplication.shared.statusBarOrientation
        if (self.itemsSize == nil || self.itemsSize.count != numberOfColumns || self.orientation != currentOrientation) {
            self.calculateItemsSize()
        }
        
        var column = 0
        var xOffset : CGFloat = 0
        var yOffset : CGFloat = 0
        var contentWidth : CGFloat = 0
        var contentHeight : CGFloat = 0
        
        for section in 0..<self.collectionView!.numberOfSections {
            let sectionAttributes = NSMutableArray()
            
            for index in 0..<numberOfColumns {
                let itemSize = (self.itemsSize[index] as AnyObject).cgSizeValue
                let indexPath = IndexPath(item: index, section: section)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = CGRect(x: xOffset, y: yOffset, width: (itemSize?.width)!, height: (itemSize?.height)!).integral
                
                //                print("SECTION! \(section), INDEX! \(index)")
                
                //                if section == 0 && index == 0 {
                //                    attributes.zIndex = 1024;
                //                } else  if section == 0 || index == 0 {
                //                                    attributes.zIndex = 1023
                //                }
                //
                
                if section == 0 {
                    var frame = attributes.frame
                    frame.origin.y = self.collectionView!.contentOffset.y
                    attributes.frame = frame
                    attributes.zIndex = 1024
                }
//                if section == 1 {
//                    var frame = attributes.frame
//                    frame.origin.y = self.collectionView!.contentOffset.y + 44
//                    attributes.frame = frame
//                    attributes.zIndex = 1024
//                }
                
                if section != 1 && section != 0 {
                    attributes.zIndex = 1023
                }
                
                sectionAttributes.add(attributes)
                
                xOffset += (itemSize?.width)!
                column += 1
                
                if column == numberOfColumns {
                    if xOffset > contentWidth {
                        contentWidth = xOffset
                    }
                    
                    column = 0
                    xOffset = 0
                    yOffset += (itemSize?.height)!
                }
            }
            if (self.itemAttributes == nil) {
                self.itemAttributes = NSMutableArray(capacity: self.collectionView!.numberOfSections)
            }
            self.itemAttributes.add(sectionAttributes)
        }
        
        let attributes : UICollectionViewLayoutAttributes = (self.itemAttributes.lastObject as! NSMutableArray).lastObject as! UICollectionViewLayoutAttributes
        contentHeight = attributes.frame.origin.y + attributes.frame.size.height
        self.contentSize = CGSize(width: contentWidth, height: contentHeight)
    }
    
    override var collectionViewContentSize : CGSize {
        guard self.contentSize != nil else {
            return CGSize(width: 0, height: 0)
        }
        
        return self.contentSize
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let sectionAttributes = self.itemAttributes [(indexPath as NSIndexPath).section] as! [UICollectionViewLayoutAttributes]
        return sectionAttributes[(indexPath as NSIndexPath).row] as UICollectionViewLayoutAttributes
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var attributes = [UICollectionViewLayoutAttributes]()
        if self.itemAttributes != nil {
            for section in self.itemAttributes {
                
                let filteredArray  =  (section as! NSMutableArray).filtered(using: NSPredicate(block: { (evaluatedObject, bindings) -> Bool in
                    return rect.intersects((evaluatedObject as! UICollectionViewLayoutAttributes).frame)
                })
                    ) as! [UICollectionViewLayoutAttributes]
                
                
                attributes.append(contentsOf: filteredArray)
                
            }
        }
        
        return attributes
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    func sizeForItemWithColumnIndex(_ columnIndex: Int) -> CGSize {
        let height: CGFloat = 44
        var width: CGFloat = 88
        
        let orientation = UIApplication.shared.statusBarOrientation
        self.orientation = orientation// for checking purpose
        
        if orientation == .portrait || orientation == .portraitUpsideDown {
            
            // 4 CELLS MAXIMUM ARE VISIBLE
            if self.numberOfColumns <= 6 {
                width = (self.collectionView?.frame.width)!/self.numberOfColumns
            }
            
        } else if orientation == .landscapeLeft || orientation == .landscapeRight {
            
            // 7 CELLS MAXIMUM ARE VISIBLE WHEN IN LANDSCAPE
            if self.numberOfColumns <= 9 {
                width = (self.collectionView?.frame.width)!/self.numberOfColumns
            }
        }
        
        return CGSize(width: width, height: height)
    }
    
    func calculateItemsSize() {
        self.itemsSize = NSMutableArray(capacity: numberOfColumns)
        for index in 0..<numberOfColumns {
            self.itemsSize.add(NSValue(cgSize: self.sizeForItemWithColumnIndex(index)))
        }
    }
}
