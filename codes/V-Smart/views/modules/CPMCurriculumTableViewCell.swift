//
//  CPMCurriculumTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 18/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class CPMCurriculumTableViewCell: UITableViewCell {
    
    @IBOutlet var curriculumTitleLabel: UILabel!
    @IBOutlet var createdByLabel: UILabel!
    @IBOutlet var curriculumDescriptionLabel: UILabel!
    @IBOutlet var gradeLevelLabel: UILabel!
    @IBOutlet var effectivityLabel: UILabel!
    @IBOutlet var downloadButton: UIButton!
    @IBOutlet var actionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setDownloaded(_ downloaded: Bool) {
        let text = downloaded ? NSLocalizedString("Open", comment: "") : NSLocalizedString("Download", comment: "")
        let textColor = downloaded ? UIColor(rgba: "#3498DB") : UIColor(rgba: "#5ABE65")
        self.actionLabel.text = text.uppercased()
        self.actionLabel.textColor = textColor
        
        let buttonImage = downloaded ? UIImage(named: "download_blk_icn_disable48x48") : UIImage(named: "download_blk_icn48x48")
        self.downloadButton.setImage(buttonImage, for: UIControlState())
    }

}
