//
//  CourseCategoryItemCell.h
//  V-Smart
//
//  Created by Julius Abarra on 16/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CourseCategoryItemCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *courseCategoryNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *courseCategoryDescLabel;

@end
