//
//  QuizController.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/6/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "QuizController.h"
#import "QuestionListView.h"
#import "QuizResultsController.h"
#import "AppDelegate.h"
#import "ResourceManager.h"
#import "RadioButton.h"
#import "MZTimerLabel.h"
#import "MainHeader.h"
#import "TestPlayerAnswerCell.h"
#import <QuartzCore/QuartzCore.h>

@interface QuizController () <QuestionListViewDelegate, QuizResultsControllerDelegate, UITableViewDataSource, UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageQuestionHeightConstraint;

@property (weak, nonatomic) IBOutlet UIImageView *imageQuestion;
@property (strong, nonatomic) IBOutlet UILabel *labelTime;
@property (strong, nonatomic) IBOutlet UILabel *labelQuestionNumber;
@property (strong, nonatomic) IBOutlet UILabel *labelQuestion;
@property (strong, nonatomic) IBOutlet UIButton *buttonQuestionList;
@property (strong, nonatomic) IBOutlet UIButton *buttonPrev;
@property (strong, nonatomic) IBOutlet UIButton *buttonNext;
@property (strong, nonatomic) IBOutlet UIButton *buttonSubmit;

@property (strong, nonatomic) IBOutlet UIProgressView *quizProgress;
@property (strong, nonatomic) IBOutlet UILabel *labelProgress;
@property (strong, nonatomic) IBOutlet UIView *multipleChoiceContainer;

@property (strong, nonatomic) ResourceManager *rm;

@property (strong, nonatomic) NSDictionary *quizData;
@property (strong, nonatomic) NSArray *questions;
@property (strong, nonatomic) NSArray *choices;
//@property (strong, nonatomic) NSMutableArray *radioButtons;
@property (strong, nonatomic) NSMutableOrderedSet *radioButtons;

@property (strong, nonatomic) MZTimerLabel *timerComponent;
@property (strong, nonatomic) NSMutableDictionary *lookupTable;
@property (assign, nonatomic) NSInteger currentIndex;
@property (strong, nonatomic) NSArray *choice_item_objects;

@property (assign, nonatomic) BOOL isSubmitted;
@property (assign, nonatomic) BOOL isReview;

@end

@implementation QuizController

static NSString *const kCellIdentifier = @"question_answer_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = [NSString stringWithFormat:@"%@", self.quizName];
    self.lookupTable = [NSMutableDictionary dictionary];
    
    self.rm = [AppDelegate resourceInstance];
    
    self.quizData = [self.rm fetchQuizDataForQuizID:self.quizid];
    self.questions = [self.rm fetchQuestionsForQuizID:self.quizid];

    self.currentIndex = 0;
    [self questionWithIndex:self.currentIndex];
    
    [self.buttonQuestionList addTarget:self action:@selector(questionListViewer:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonNext addTarget:self action:@selector(cycleQuestions:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonPrev addTarget:self action:@selector(cycleQuestions:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonSubmit addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSTimeInterval time_remaining = [self.quizData[@"time_remaining"] floatValue];
    self.timerComponent = [[MZTimerLabel alloc] initWithLabel:self.labelTime andTimerType:MZTimerLabelTypeTimer];
    [self.timerComponent setCountDownTime:time_remaining]; //** Or you can use [timer3 setCountDownToDate:aDate];
    [self.timerComponent start];
    
    [self.timerComponent startWithEndingBlock:^(NSTimeInterval countTime) {
        //oh my gosh, it's awesome!!
        [self submitAnswers];
    }];

    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(previewImage:)];
    [singleTap setNumberOfTapsRequired:1];
    [self.imageQuestion addGestureRecognizer:singleTap];
    
    self.scrollView.minimumZoomScale = 1.0;
    self.scrollView.maximumZoomScale = 5.0;
    
    self.navigationItem.hidesBackButton = YES;
    
    self.isSubmitted = NO;
}

- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageQuestion;
}

- (void)previewImage:(UIGestureRecognizer *)recognizer {
    CGFloat value = 122;
    
    if (self.imageQuestionHeightConstraint.constant == 122) {
        value = 350;
    } else {
        value = 122;
    }
    
    self.imageQuestionHeightConstraint.constant = value;
    
    [UIView animateWithDuration:0.45 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)retakeExam {
    
    self.isReview = NO;
    
    _quizData = [_rm fetchQuizDataForQuizID:self.quizid];
    _questions = [_rm fetchQuestionsForQuizID:self.quizid];
    
    //    NSTimeInterval time_limit = [self.quizData[@"time_limit"] floatValue];
//    NSTimeInterval time_limit = self.actual_time_limit;
//    [self.timerComponent reset];
//    [self.timerComponent setCountDownTime:time_limit];
//    [self.timerComponent start];

    if (self.timerComponent != nil) {
        [self.timerComponent reset];
        [self.timerComponent start];
        self.currentIndex = 0;
        [self questionWithIndex:self.currentIndex];
    }
}

- (void)reviewExam {
    
    self.isReview = YES;
    
    [self.buttonSubmit setTitle:@"Exit" forState:UIControlStateNormal];
    [self.buttonSubmit setTitle:@"Exit" forState:UIControlStateHighlighted];
    
    if (self.timerComponent != nil) {
        [self.timerComponent reset];
        [self.timerComponent pause];
        self.currentIndex = 0;
        [self questionWithIndex:self.currentIndex];
    }
}

- (void)questionListViewer:(id)sender {

    [self performSegueWithIdentifier:@"showQuestionListBox" sender:self];
}

- (void)cycleQuestions:(id)sender {
    
    UIButton *b = (UIButton *)sender;
    
    if ( b == self.buttonNext) {
        self.currentIndex++;
        [self questionWithIndex:self.currentIndex];
    }
    
    if (b == self.buttonPrev) {
        self.currentIndex--;
        [self questionWithIndex:self.currentIndex];
    }
}

//- (void)questionWithIndex:(NSInteger)index {
//
//    NSLog(@"questionWithIndex : %ld", (long)index);
//    
//    //OUT OF BOUNDS HANDLING
//    if (index < 0) {
//        index = 0;
//    }
//    
//    if (index >= self.questions.count) {
//        index = self.questions.count - 1;
//    }
//    
//    __weak typeof(self) wo = self;
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        //BUTTON HANDLING
//        wo.buttonPrev.hidden = (index == 0) ? YES : NO;
//        wo.buttonNext.hidden = (index < wo.questions.count) ? NO : YES;
//        if (index == (wo.questions.count-1) ) {
//            wo.buttonSubmit.hidden = NO;
//        }
//        if (index < (wo.questions.count-1) ) {
//            wo.buttonSubmit.hidden = YES;
//        }
//        
//        
//        //QUESTION CREATION
//        if ([wo.questions count] > 0) {
//            
//            NSDictionary *q = (NSDictionary *)wo.questions[index];
//            
//            [wo updateQuizProgress:wo.questions];
//            
//            //QUESTION ID
//            NSString *question_id = [NSString stringWithFormat:@"%@", q[@"question_id"] ];
//            
//            //QUESTION ANSWER TEXT
//            NSString *question_answer_text = [NSString stringWithFormat:@"%@", q[@"question_answer_text"] ];
//            
//            //BULLET NUMBER
//            NSString *question_bullet_number = [NSString stringWithFormat:@"%@", q[@"question_bullet_number"] ];
//            NSLog(@"BULLET STRING : %@", question_bullet_number);
//            
//            NSInteger bullet_number = [question_bullet_number integerValue] + 1;
//            wo.labelQuestionNumber.text = [NSString stringWithFormat:@"%ld", (long)bullet_number];
//            
//            //ACTUAL QUESTION
//            NSString *question_text = [NSString stringWithFormat:@"%@", q[@"question_text"] ];
//            if (question_text) {
//                NSArray *split = [question_text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//                split = [split filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
//                question_text = [NSString stringWithFormat:@"%@", [split componentsJoinedByString:@" "] ];
//                wo.labelQuestion.text = question_text;
//            }
//
//            NSString *image_url = [NSString stringWithFormat:@"%@", q[@"image_url"] ];
//            
//            __weak typeof(self) weakObject = self;
//            [self.activityIndicator stopAnimating];
//            if (image_url.length > 0) {
//                [self.activityIndicator startAnimating];
//                NSLog(@"PASOK DITO SA IMAGE URL > 0");
//                NSData *image_data = [q[@"image_data"] data];
//                if (image_data) {
//                    [self.activityIndicator stopAnimating];
//                    weakObject.imageQuestion.image = [UIImage imageWithData:image_data];
//                } else {
//
//                    NSDictionary *data = @{@"quiz_id":self.quizid, @"image_url":image_url};
//                        [self.rm downloadImageFromQuizID:data dataBlock:^(NSData *binary) {
//
//                            if (binary) {
//                                dispatch_async(dispatch_get_main_queue(), ^{
//                                    UIImage *image = [UIImage imageWithData:binary];
//                                    if (image) {
//                                        NSLog(@"IMAGE IS NOT NIL");
//                                    } else {
//                                        NSLog(@"IMAGE IS NIL!");
//                                    }
//                                    weakObject.imageQuestion.image = image;
//                                    [weakObject.activityIndicator stopAnimating];
//                                });
//                            }
//                        }];
//                }
//                weakObject.imageQuestionHeightConstraint.constant = 122;
//            } else {
//                weakObject.imageQuestionHeightConstraint.constant = 0;
//            }
//            NSLog(@"HERE IS THE IMAGE URL [%@]", image_url);
//            
//            CGFloat labelWidth = wo.labelQuestion.frame.size.width;
//            [wo.labelQuestion setNumberOfLines:0];
//            [wo.labelQuestion setPreferredMaxLayoutWidth:labelWidth];
//            
//            //MULTIPLE CHOICES
//            wo.choices = [wo.rm fetchChoicesForQuestionID:question_id];
//            
//            if (wo.choices.count > 0) {
//                
//                if (wo.radioButtons.count > 0) {
//                    
//                    for (RadioButton *b in wo.radioButtons) {
//                        [b removeFromSuperview];
//                    }
//                    
//                    [wo.radioButtons removeAllObjects];
//                }
//                
////                wo.radioButtons = [NSMutableArray arrayWithCapacity:wo.choices.count];
//                wo.radioButtons = [NSMutableOrderedSet orderedSetWithCapacity:wo.choices.count];
//                
//                CGRect radioButtonFrame = CGRectMake(0, 0, 30, 30);
//                //CGRect radioButtonFrame = CGRectMake(10, 10, 300, 30);
//                for (NSDictionary *c in wo.choices) {
//                    
//                    NSString *answer_text = [NSString stringWithFormat:@"%@", c[@"answer_text"] ];
//                    [wo.lookupTable setObject:c forKey:answer_text];//save to lookup table
//                    
//                    RadioButton *radionButton = [[RadioButton alloc] initWithFrame:radioButtonFrame];
//                    [radionButton addTarget:self action:@selector(onRadioButtonValueChanged:) forControlEvents:UIControlEventTouchUpInside];
//                    radioButtonFrame.origin.y += 40;
//                    [radionButton setTitle:answer_text forState:UIControlStateNormal];
//                    [radionButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//                    radionButton.titleLabel.font = [UIFont boldSystemFontOfSize:17];
//                    [radionButton setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
//                    [radionButton setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
//                    radionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//                    radionButton.titleEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 0);
//                    
//                    if ([answer_text isEqualToString:question_answer_text]) {
//                        [radionButton setSelected:YES];
//                    }
//                    
////                    [wo.multipleChoiceContainer addSubview:radionButton];
////                    [radionButton layoutSubviews];
//                    [wo.radioButtons addObject:radionButton];
//                }
//                
////                [wo.radioButtons[0] setGroupButtons:wo.radioButtons]; // Setting buttons into the group
//                [[wo.radioButtons array][0] setGroupButtons:[wo.radioButtons array]]; // Setting buttons into the group
//                [self.tableView reloadData];
//            }
//        }
//
//    });
//    
//}

- (void)questionWithIndex:(NSInteger)index {
    
    NSLog(@"questionWithIndex : %ld", (long)index);
    
    //OUT OF BOUNDS HANDLING
    if (index < 0) {
        index = 0;
    }
    
    if (index >= self.questions.count) {
        index = self.questions.count - 1;
    }
    
    __weak typeof(self) wo = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //BUTTON HANDLING
        wo.buttonPrev.hidden = (index == 0) ? YES : NO;
        wo.buttonNext.hidden = (index < wo.questions.count) ? NO : YES;
        if (index == (wo.questions.count-1) ) {
            wo.buttonSubmit.hidden = NO;
        }
        if (index < (wo.questions.count-1) ) {
            wo.buttonSubmit.hidden = YES;
        }
        
        
        //QUESTION CREATION
        if ([wo.questions count] > 0) {
            
            NSDictionary *q = (NSDictionary *)wo.questions[index];
            
            [wo updateQuizProgress:wo.questions];
            
            //QUESTION ID
            NSString *question_id = [NSString stringWithFormat:@"%@", q[@"question_id"] ];
            
            //QUESTION ANSWER TEXT
            NSString *question_answer_text = [NSString stringWithFormat:@"%@", q[@"question_answer_text"] ];
            
            //BULLET NUMBER
            NSString *question_bullet_number = [NSString stringWithFormat:@"%@", q[@"question_bullet_number"] ];
            NSLog(@"BULLET STRING : %@", question_bullet_number);
            
            NSInteger bullet_number = [question_bullet_number integerValue] + 1;
            wo.labelQuestionNumber.text = [NSString stringWithFormat:@"%ld", (long)bullet_number];
            
            //ACTUAL QUESTION
            NSString *question_text = [NSString stringWithFormat:@"%@", q[@"question_text"] ];
            if (question_text) {
                NSArray *split = [question_text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                split = [split filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
                question_text = [NSString stringWithFormat:@"%@", [split componentsJoinedByString:@" "] ];
                wo.labelQuestion.text = question_text;
            }
            
            NSString *image_url = [NSString stringWithFormat:@"%@", q[@"image_url"] ];
            
            __weak typeof(self) weakObject = self;
            [self.activityIndicator stopAnimating];
            // BUG FIX VIBALHP
            
            if ([image_url isEqualToString:@"<null>"] || [image_url isEqualToString:@"(null)"] || [image_url isEqualToString:@"null"] || !(image_url.length > 0)) {
                weakObject.imageQuestionHeightConstraint.constant = 0;
            } else {
                [self.activityIndicator startAnimating];
                NSData *image_data = [q[@"image_data"] data];
                if (image_data) {
                    [self.activityIndicator stopAnimating];
                    weakObject.imageQuestion.image = [UIImage imageWithData:image_data];
                } else {
                    
                    NSDictionary *data = @{@"quiz_id":self.quizid, @"image_url":image_url};
                    [self.rm downloadImageFromQuizID:data dataBlock:^(NSData *binary) {
                        
                        if (binary) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                UIImage *image = [UIImage imageWithData:binary];
                                if (image) {
                                    NSLog(@"IMAGE IS NOT NIL");
                                } else {
                                    NSLog(@"IMAGE IS NIL!");
                                }
                                weakObject.imageQuestion.image = image;
                                [weakObject.activityIndicator stopAnimating];
                            });
                        }
                    }];
                }
                weakObject.imageQuestionHeightConstraint.constant = 122;
            }
            NSLog(@"HERE IS THE IMAGE URL [%@]", image_url);
            
            CGFloat labelWidth = wo.labelQuestion.frame.size.width;
            [wo.labelQuestion setNumberOfLines:0];
            [wo.labelQuestion setPreferredMaxLayoutWidth:labelWidth];
            
            //MULTIPLE CHOICES
            wo.choices = [wo.rm fetchChoicesForQuestionID:question_id];
            
            wo.radioButtons = [NSMutableOrderedSet orderedSetWithCapacity:wo.choices.count];
            
            if (wo.choices.count > 0) {
                
                for (NSDictionary *c in wo.choices) {
                    
                    NSString *answer_text = [NSString stringWithFormat:@"%@", c[@"answer_text"] ];
                    [wo.lookupTable setObject:c forKey:answer_text];//save to lookup table
                 
                    NSNumber *flag = [NSNumber numberWithBool:NO];
                    if ([answer_text isEqualToString:question_answer_text]) {
                        flag = [NSNumber numberWithBool:YES];
                    }
                    
                    NSDictionary *data = @{@"title": answer_text,
                                           @"selected":flag};
                    
                    [wo.radioButtons addObject:data];
                }
                
                [wo.tableView reloadData];
            }
        }
        
    });
    
}

- (void)updateQuizProgress:(NSArray *)list {
    
    NSMutableArray *answers = [NSMutableArray array];
    for (NSDictionary *d in list) {
        NSString *score = [NSString stringWithFormat:@"%@", d[@"score"] ];
        if (![score isEqualToString:@"(null)"]) {
            [answers addObject:score];
        }
    }
    
    NSNumber *questionCount = [NSNumber numberWithUnsignedInteger: self.questions.count ];
    NSNumber *answerCount = [NSNumber numberWithUnsignedInteger: answers.count ];
    double percent = [answerCount doubleValue] / [questionCount doubleValue];
    self.quizProgress.progress = percent;
    
    NSString *progressText = [NSString stringWithFormat:@"%@ of %@ questions answered", answerCount, questionCount];
    self.labelProgress.text = progressText;
}

- (void)onRadioButtonValueChanged:(id)sender {
    RadioButton *b = (RadioButton *)sender;
    
    if (self.lookupTable) {
        
        NSString *answer_text = b.titleLabel.text;
        NSDictionary *c = [self.lookupTable valueForKey:answer_text];
        NSString *question_id = [NSString stringWithFormat:@"%@", c[@"question_id"] ];
        NSString *question_answer_text = [NSString stringWithFormat:@"%@", c[@"answer_text"] ];
        NSString *is_correct = [NSString stringWithFormat:@"%@", c[@"is_correct"] ];
        NSDictionary *d = @{@"question_id":question_id,
                            @"question_answer_text":question_answer_text,
                            @"is_correct":is_correct};
        
        __weak typeof(self) weakObject = self;
        [self.rm setAnswerObject:d doneBlock:^(BOOL status) {
            weakObject.questions = [weakObject.rm fetchQuestionsForQuizID:weakObject.quizid];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakObject updateQuizProgress:weakObject.questions];
            });
        }];
        
    }
}

- (void)submitAction:(id)sender {

//    if (self.isReview == YES) {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//    
//    if (self.isReview == NO) {
        [self submitAnswers];
//    }
}

- (void)submitAnswers {
    
    __weak typeof(self) wo = self;
    
    if (self.timerComponent != nil) {
        [self.timerComponent pause];
        NSDictionary *d = @{@"userid": self.userid,
                            @"quizid": self.quizid,
                            @"courseid": self.courseid,
                            @"seconds" : [NSNumber numberWithDouble:self.timerComponent.getTimeCounted]
                            };
        
        [self.rm submitQuizForUser:d doneBlock:^(BOOL status) {
            

            if (status == YES) {
                wo.isSubmitted = YES;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo performSegueWithIdentifier:@"showResultBox" sender:self];
//                if ([wo.delegate respondsToSelector:@selector(didFinishSubmittingQuizAnswersForData:)]) {
//                    [wo.delegate didFinishSubmittingQuizAnswersForData:d];
//                }
            });
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self customizeNavigationController];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
//    [self tearDownObserver];
    [self.timerComponent timerInvalidation];
}

- (void)customizeNavigationController {
    
    UIColor *color = UIColorFromHex(0x37B34A);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    } else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

#pragma mark - QuizResultsControllerDelegate

- (void)didFinishTappingButtonType:(NSString *)type {
    
    NSLog(@"button type : %@", type);
    
    if ([type isEqualToString:@"retake"]) {
        [self.navigationController popViewControllerAnimated:YES];
        [self retakeExam];
    }

    if ([type isEqualToString:@"review"]) {
        [self.navigationController popViewControllerAnimated:YES];
        [self reviewExam];
    }
    
    if ([type isEqualToString:@"Quiz_List"]) {
        
        NSArray *controller_list = [self.navigationController viewControllers];
        for (UIViewController *vc in controller_list) {
            NSString *vc_title = vc.title;
            NSLog(@"view controller names : %@", vc_title);
            if ([vc_title isEqualToString:@"Quiz List"]) {
                [self.navigationController popToViewController:vc animated:YES];
                break;
            }
        }
    }
}

#pragma mark - QuestionListViewDelegate

- (void)didFinishSelectingQuestionIndex:(NSString *)indexString {
    
    NSInteger indexValue = [indexString integerValue];
    NSLog(@"index value : %ld", (long)indexValue);
    [self questionWithIndex:indexValue];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showQuestionListBox"]) {
        QuestionListView *questionListView = (QuestionListView *)segue.destinationViewController;
        questionListView.quizid = [NSString stringWithFormat:@"%@", self.quizid];
        questionListView.delegate = self;
    }

    if ([segue.identifier isEqualToString:@"showResultBox"]) {
        QuizResultsController *resultsController = (QuizResultsController *)segue.destinationViewController;
        resultsController.userid = [NSString stringWithFormat:@"%@", self.userid];
        resultsController.quizid = [NSString stringWithFormat:@"%@", self.quizid];
        resultsController.courseid = [NSString stringWithFormat:@"%@", self.courseid];
        resultsController.delegate = self;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.radioButtons count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSInteger row = indexPath.row;
    NSArray *items = [self.radioButtons array];
    
    NSDictionary *data = items[row];
    NSString *title_string = data[@"title"];
    BOOL state = [data[@"selected"] boolValue];
    
    TestPlayerAnswerCell *cell = (TestPlayerAnswerCell *)[tableView dequeueReusableCellWithIdentifier:kCellIdentifier
                                                                                         forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.answerLabel.text = title_string;
    cell.buttonContainer.selected = state;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.isReview == NO) {
        
        if (self.lookupTable) {
            
            NSInteger row = indexPath.row;
            NSArray *items = [self.radioButtons array];
            
            NSDictionary *data = items[row];
            NSString *answer_text = data[@"title"];
            NSDictionary *c = [self.lookupTable valueForKey:answer_text];
            
            NSString *question_id = [NSString stringWithFormat:@"%@", c[@"question_id"] ];
            NSString *question_answer_text = [NSString stringWithFormat:@"%@", c[@"answer_text"] ];
            NSString *is_correct = [NSString stringWithFormat:@"%@", c[@"is_correct"] ];
            NSDictionary *d = @{@"question_id":question_id,
                                @"question_answer_text":question_answer_text,
                                @"is_correct":is_correct};
            
            __weak typeof(self) wo = self;
            [self.rm setAnswerObject:d doneBlock:^(BOOL status) {
                wo.questions = [wo.rm fetchQuestionsForQuizID:wo.quizid];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [wo questionWithIndex:wo.currentIndex];
                    
                    [wo updateQuizProgress:wo.questions];
                });
            }];
            
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}


@end
