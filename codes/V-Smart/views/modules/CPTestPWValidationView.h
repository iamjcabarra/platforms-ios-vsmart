//
//  CPTestPWValidationView.h
//  V-Smart
//
//  Created by Julius Abarra on 30/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CPTestPWValidationViewDelegate <NSObject>

@required
- (void)shouldAllowToRunTest:(BOOL)run;

@end

@interface CPTestPWValidationView : UIViewController

@property (weak, nonatomic) id <CPTestPWValidationViewDelegate> delegate;
@property (strong, nonatomic) NSManagedObject *testObject;

@end
