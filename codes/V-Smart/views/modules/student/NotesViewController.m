//
//  NotesViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 9/6/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

// OLD IMPLEMENTATION
// JCA-08082017

#import "NotesViewController.h"

@interface NotesViewController (){}

@property (nonatomic, strong) NotesChildViewController *containerView;

@end

@implementation NotesViewController
@synthesize containerView;


- (void) viewDidLoad{
    [super viewDidLoad];

    [self setupNotesModule];
    [self _setupNotifications];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        dispatch_queue_t queue = dispatch_queue_create("com.pearson.course.ACTIVITY",DISPATCH_QUEUE_SERIAL);
        dispatch_async(queue, ^{
            ResourceManager *rm = [AppDelegate resourceInstance];
            NSString *details = @"Left the Notes module in Apple iPad";
            [rm requestLogActivityWithModuleType:@"7" details:details];
        });
    }
    
    [super viewWillDisappear:animated];
}

-(void)setupNotesModule{
    
    float profileY = [super profileHeight];
    float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    //float gridHeight = 638 + [super profileSize].size.height;
    
    float gridY = [super headerSize].size.height + profileY;
    
    containerView = [[NotesChildViewController alloc] init];
    containerView.view.frame = CGRectMake(0, gridY, self.view.frame.size.width, gridHeight);
    
    containerView.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	containerView.view.autoresizesSubviews = YES;

    [self addChildViewController:containerView];
    
    [self.view addSubview:containerView.view];
    [containerView didMoveToParentViewController:self];
    

}
-(void) _setupNotifications{
    VS_NCADD(kNotificationProfileHeight, @selector(_refreshController:))
}
- (NSString *) dashboardName{
    
    NSString *moduleName = NSLocalizedString(@"Notes", nil);
    return moduleName;
    
//    return kModuleNotes;
}

#pragma mark - Post Notification Events
-(void) _refreshController: (NSNotification *) notification{
    VLog(@"Received: kNotificationProfileHeight");
    
    [UIView animateWithDuration:0.45 animations:^{
        float profileY = [super profileHeight];
        float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
        //float gridHeight = 638 + [super profileSize].size.height;
        
        float gridY = [super headerSize].size.height + profileY;
        [self.containerView.view setFrame:CGRectMake(0, gridY, self.view.frame.size.width, gridHeight)];
        [self.containerView.view setNeedsLayout];
        
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            [super adjustProfileHeight];
        }
        [super showOrHideMiniAvatar];
    } completion:^(BOOL finished) {
        
    }];
}

@end

//#import "NotesViewController.h"
//
//@interface NotesViewController (){}
//@property (nonatomic, strong) NotesChildViewController *containerView;
//@end
//
//@implementation NotesViewController
//@synthesize containerView;
//
//
//- (void) viewDidLoad{
//    [super viewDidLoad];
//    
//    [self setupNotesModule];
//    [self _setupNotifications];
//    [super hideJumpMenu:NO];
//    [super showOrHideMiniAvatar];
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    
//    if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound) {
//        dispatch_queue_t queue = dispatch_queue_create("com.pearson.course.ACTIVITY", DISPATCH_QUEUE_SERIAL);
//        dispatch_async(queue, ^{
//            ResourceManager *rm = [AppDelegate resourceInstance];
//            NSString *details = @"Left the Notes module in Apple iPad";
//            [rm requestLogActivityWithModuleType:@"7" details:details];
//        });
//    }
//    
//    [super viewWillDisappear:animated];
//}
//
//- (void)setupNotesModule {
//    NSString *storyboardName = @"NTMStoryboard";
//    
//    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
//    self.containerView = [sb instantiateInitialViewController];
//    [self initiateCustomLayoutFor:self.containerView];
//    self.containerView.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
//    self.containerView.view.autoresizesSubviews = YES;
//    
//    [self addChildViewController:self.containerView];
//    [self.view addSubview:self.containerView.view];
//    [self.containerView didMoveToParentViewController:self];
//    [self.view sendSubviewToBack:self.containerView.view];
//}
//
//- (void)initiateCustomLayoutFor:(UIViewController *)viewcontroller {
//    float profileY = [super profileHeight];
//    float headerDecrement = ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
//    float gridHeight = self.view.frame.size.height - headerDecrement;
//    float gridY = [super headerSize].size.height + profileY;
//    CGRect customFrame = CGRectMake(0, gridY, self.view.frame.size.width, gridHeight);
//    [viewcontroller.view setFrame:customFrame];
//}
//
//- (void) _setupNotifications{
//    VS_NCADD(kNotificationProfileHeight, @selector(_refreshController:))
//}
//
//- (NSString *) dashboardName {
//    NSString *moduleName = NSLocalizedString(@"Notes", nil);
//    return moduleName;
//}
//
//#pragma mark - Post Notification Events
//
//- (void) _refreshController: (NSNotification *) notification {
//    VLog(@"Received: kNotificationProfileHeight");
//    
//    [UIView animateWithDuration:0.45 animations:^{
//        float profileY = [super profileHeight];
//        float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
//        float gridY = [super headerSize].size.height + profileY;
//        [self.containerView.view setFrame:CGRectMake(0, gridY, self.view.frame.size.width, gridHeight)];
//        [self.containerView.view setNeedsLayout];
//        
//        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
//            [super adjustProfileHeight];
//        }
//        
//        [super showOrHideMiniAvatar];
//    } completion:^(BOOL finished) {}];
//}
//
//@end
//
//
