//
//  CalendarViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 9/9/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "CalendarViewController.h"
#import "CalendarChildViewController.h"

@interface CalendarViewController ()
@property (nonatomic, strong) CalendarChildViewController *containerView;
@property (nonatomic, strong) UIViewController *cv;
@end

@implementation CalendarViewController
@synthesize containerView;

-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self setupChildViewController];
    
    [self _setupNotifications];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        dispatch_queue_t queue = dispatch_queue_create("com.pearson.course.ACTIVITY",DISPATCH_QUEUE_SERIAL);
        dispatch_async(queue, ^{
            ResourceManager *rm = [AppDelegate resourceInstance];
            NSString *details = @"Left the Calendar module in Apple iPad";
            [rm requestLogActivityWithModuleType:@"10" details:details];
        });
    }
    
    [super viewWillDisappear:animated];
}

-(void)setupChildViewController{
    
    /////// Old Calendar Implementation ///////
    float profileY = [super profileHeight];
    float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    float gridY = [super headerSize].size.height + profileY;
    
    containerView = [[CalendarChildViewController alloc] init];
    containerView.view.frame = CGRectMake(0, gridY - 20, self.view.frame.size.width, gridHeight + 70);
    containerView.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    containerView.view.autoresizesSubviews = YES;
    
    [self addChildViewController:containerView];
    [self.view addSubview:containerView.view];
    [containerView didMoveToParentViewController:self];
    [self.view sendSubviewToBack:containerView.view];
    
    /////// New Calendar Implementation ///////
    //    NSString *storyboardName = @"CALMainStoryboard";
    //    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    //    self.cv = [sb instantiateInitialViewController];
    //    [self initiateCustomLayoutFor:self.cv];
    //    self.cv.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    //    self.cv.view.autoresizesSubviews = YES;
    //
    //    [self addChildViewController:self.cv];
    //    [self.view addSubview:self.cv.view];
    //    [self.cv didMoveToParentViewController:self];
    //    [self.view sendSubviewToBack:self.cv.view];
}


-(void) _setupNotifications {
    VS_NCADD(kNotificationProfileHeight, @selector(_refreshController:))
}

- (NSString *) dashboardName {
    
    NSString *moduleName = NSLocalizedString(@"Calendar", nil);
    return moduleName;
    
    //    return kModuleCalendar;
}

#pragma mark - Post Notification Events

-(void) _refreshController: (NSNotification *) notification{
    VLog(@"Received: kNotificationProfileHeight");
    
    /////// Old Calendar Implementation ///////
    [UIView animateWithDuration:0.45 animations:^{
        float profileY = [super profileHeight];
        float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
        //float gridHeight = 638 + [super profileSize].size.height;
        
        float gridY = [super headerSize].size.height + profileY;
        [self.containerView.view setFrame:CGRectMake(0, gridY - 20, self.view.frame.size.width, gridHeight + 70)];
        [self.containerView.view setNeedsLayout];
        
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            [super adjustProfileHeight];
        }
        [super showOrHideMiniAvatar];
    } completion:^(BOOL finished) {
        
    }];
    
    /////// New Calendar Implementation ///////
    VLog(@"Received: kNotificationProfileHeight");
    //    [UIView animateWithDuration:0.45 animations:^{
    //        [self initiateCustomLayoutFor:self.cv];
    //        [self.cv.view setNeedsLayout];
    //
    //        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
    //            [super adjustProfileHeight];
    //        }
    //
    //        [super showOrHideMiniAvatar];
    //    }];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [SVProgressHUD dismiss];
}

- (void)initiateCustomLayoutFor:(UIViewController *)viewcontroller {
    float profileY = [super profileHeight];
    float headerDecrement = ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    float gridHeight = self.view.frame.size.height - headerDecrement;
    float gridY = [super headerSize].size.height + profileY;
    CGRect customFrame = CGRectMake(0, gridY, self.view.frame.size.width, gridHeight);
    [viewcontroller.view setFrame:customFrame];
}

@end
