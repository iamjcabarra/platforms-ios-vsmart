//
//  EducationalAppViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 9/9/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "EducationalAppViewController.h"

@interface EducationalAppViewController ()

@end

@implementation EducationalAppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self _setupView];
    [self _setupNotifications];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
}

-(void) _setupView {
    float profileY = [super profileHeight];
    float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    //float gridHeight = 638 + [super profileSize].size.height;
    
    float gridY = [super headerSize].size.height + profileY;
    
    self.theImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, gridY, self.view.frame.size.width, gridHeight + 44)];
    self.theImage.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	self.theImage.autoresizesSubviews = YES;
    self.theImage.image = [UIImage imageNamed:@"demo-educ-apps"];
    
    //NSString *url = VS_FMT(@"http://vsmart.vibeapi.net/grades/%i/%@", [super account].user.id, [super account].user.position);
    //VLog(@"URL: %@", url);
    //url = @"http://microsoft.com";
    //VLog(@"URL: %@", url);
    
    //NSURLRequest *webRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    //[self.theWebView loadRequest:webRequest];
    
    //    UIImage *patternTile = [UIImage imageNamed: @"bookshelf"];
    //	UIView *backgroundView = [[UIView alloc] init];
    //	backgroundView.backgroundColor = [UIColor colorWithPatternImage: patternTile];
    //	self.gridView.backgroundView = backgroundView;
    //    self.gridView.backgroundColor = [UIColor clearColor];
    
    //    for (id subview in self.theWebView.subviews)
    //        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
    //            ((UIScrollView *)subview).bounces = NO;
    
    [self.view addSubview:self.theImage];
}

-(void) _setupNotifications {
    VS_NCADD(kNotificationProfileHeight, @selector(_refreshController:))
}

- (NSString *) dashboardName {
    
    NSString *moduleName = NSLocalizedString(@"Educational Apps", nil);
    return moduleName;
    
//    return kModuleEducationalApps;
}

#pragma mark - Post Notification Events
-(void) _refreshController: (NSNotification *) notification {
    VLog(@"Received: kNotificationProfileHeight");
    
    [UIView animateWithDuration:0.45 animations:^{
        float profileY = [super profileHeight];
        float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
        //float gridHeight = 638 + [super profileSize].size.height;
        
        float gridY = [super headerSize].size.height + profileY;
        [self.theImage setFrame:CGRectMake(0, gridY, self.view.frame.size.width, gridHeight + 44)];
        [self.theImage setNeedsLayout];
        
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            [super adjustProfileHeight];
        }
        [super showOrHideMiniAvatar];
    } completion:^(BOOL finished) {
        
    }];
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
