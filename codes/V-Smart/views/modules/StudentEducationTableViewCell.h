//
//  StudentEducationTableViewCell.h
//  V-Smart
//
//  Created by Julius Abarra on 10/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentEducationTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblPreSchool;
@property (strong, nonatomic) IBOutlet UILabel *lblElementarySchool;
@property (strong, nonatomic) IBOutlet UILabel *lblHighSchool;
@property (strong, nonatomic) IBOutlet UILabel *lblActPreSchool;
@property (strong, nonatomic) IBOutlet UILabel *lblActElementarySchool;
@property (strong, nonatomic) IBOutlet UILabel *lblActHighSchool;

@end
