//
//  LessonDetailsTableViewCell.h
//  V-Smart
//
//  Created by Julius Abarra on 9/7/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonDetailsTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblProcessName;
@property (strong, nonatomic) IBOutlet UILabel *lblProcessContent;

@end
