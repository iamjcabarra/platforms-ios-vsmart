//
//  GBMDropDown.swift
//  V-Smart
//
//  Created by Ryan Migallos on 18/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol GBMDropDownDelegate {
    func didFinishSelecting(_ type:GBMDropDownMenuType, data:[String:AnyObject])
}

enum GBMDropDownMenuType {
    
    case course, section, result, term
    
    func keyPath() -> String {
        switch self {
        case .section:
            return "section"
        case .course:
            return "course_name"
        case .result:
            return "name"
        case .term:
            return "name"
        }
    }
    
    func entity() -> String {
        switch self {
        case .section:
            return GBTConstants.Entity.SECTION
        case .course:
            return GBTConstants.Entity.COURSE
        case .result:
            return GBTConstants.Entity.RESULT
        case .term:
            return GBTConstants.Entity.TERM
            
        }
    }
    
    func columns() -> (keyName:String, keyID:String) {
        switch self {
        case .section:
            return ("section","section_id")
        case .course:
            return ("course_name","cs_id")
        case .result:
            return ("name","id")
        case .term:
            return ("name","id")
        }
    }
    
    func description() -> String {
        return self.entity()
    }
}

class GBMDropDown: UIViewController {
    
    // MARK: Property
    
    fileprivate let cellID = "gbm_dropdown_menu_cell_identifier"
    
//    var optionType:GBMDropDownMenuType! = .section
    var optionType:GBMDropDownMenuType!
    var delagate:GBMDropDownDelegate?
    
    var userData:[String:AnyObject]?
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    @IBOutlet var tableView: UITableView!

    // MARK: Initializers
    
    init(menu type:GBMDropDownMenuType, delegate:GBMDropDownDelegate) {
        self.optionType = type
        self.delagate = delegate
        
        super.init(nibName:"GBMDropDown", bundle:nil)
    }

    
    init(menu type:GBMDropDownMenuType) {
        self.optionType = type
        super.init(nibName:"GBMDropDown", bundle:nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: ViewController Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.preferredContentSize = CGSize(width: 300, height: tableView.contentSize.height);
    }
}

extension GBMDropDown: UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ cell: UITableViewCell, atIndexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: atIndexPath)
        
        let keyPath = optionType.keyPath()
        guard
            let name = mo.value(forKey: keyPath) as? String else { return }
        cell.textLabel!.text = name
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mo = fetchedResultsController.object(at: indexPath)
        
        let column = optionType.columns()
        let keyNameValue = mo.value( forKey: column.keyName ) as! String
        let keyIdValue = mo.value( forKey: column.keyID ) as! String
        
        var data:[String:AnyObject] = ["name":keyNameValue as AnyObject,"id":keyIdValue as AnyObject]
        if optionType.entity() == GBTConstants.Entity.SECTION {
            data["is_advised"] = mo.value(forKey: "is_advised") as AnyObject
        }
        
        if optionType.entity() == GBTConstants.Entity.COURSE {
            data["is_teaching"] = mo.value(forKey: "is_teaching") as AnyObject
            data["course_id"] = mo.value(forKey: "course_id") as AnyObject
        }
        
        // supply user data
        if userData != nil {
            for (key, value) in userData! {
                data[key] = "\(value)" as AnyObject?
            }
        }
        
        defer {
            tableView.deselectRow(at: indexPath, animated: true)
            self.dismiss(animated: true) { 
                self.delagate?.didFinishSelecting(self.optionType, data: data)
            }
        }
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = gbdm.getMainContext()
        let ascending = true
        let entity = optionType.entity()
        let keyPath = optionType.keyPath()
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
//        let fetchRequest = NSFetchRequest(entityName: entity)
        fetchRequest.fetchBatchSize = 20
        
        let descriptorSelector = #selector(NSString.localizedCompare(_:))
        let sortName = NSSortDescriptor(key: keyPath, ascending: ascending, selector:descriptorSelector)
        fetchRequest.sortDescriptors = [sortName]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        let tableViewController = tableView
        
        switch type {
        case .insert:
            tableViewController?.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableViewController?.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        let tableViewController = tableView
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableViewController?.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = tableViewController?.cellForRow(at: indexPath) {
                    configureCell(cell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableViewController?.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
