//
//  FileBrowserTableViewCell.h
//  V-Smart
//
//  Created by Julius Abarra on 10/22/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FileBrowserTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblFileName;
@property (strong, nonatomic) IBOutlet UILabel *lblFileDateModified;
@property (strong, nonatomic) IBOutlet UILabel *lblFileSize;
@property (strong, nonatomic) IBOutlet UILabel *lblFileKind;

@end
