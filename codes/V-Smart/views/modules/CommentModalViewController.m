//
//  CommentModalViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/28/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "CommentModalViewController.h"
#import "LessonPlanDataManager.h"
#import "VSmartValues.h"

@interface CommentModalViewController () <UIAlertViewDelegate>

@property (nonatomic, strong) LessonPlanDataManager *lm;;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UILabel *lblEditComment;
@property (strong, nonatomic) IBOutlet UITextView *txtCommentToEdit;
@property (strong, nonatomic) IBOutlet UIButton *butPostComment;
@property (strong, nonatomic) IBOutlet UIButton *butClose;

@property (strong, nonatomic) NSString *commentid;

@end

@implementation CommentModalViewController

@synthesize delegate;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lblEditComment.text = NSLocalizedString(@"Comment", nil);
    
    // Use singleton class as data manager
    self.lm = [LessonPlanDataManager sharedInstance];
    self.managedObjectContext = self.lm.mainContext;
    
    // Text view decoration configuration
    self.txtCommentToEdit.backgroundColor = [UIColor whiteColor];
    self.txtCommentToEdit.layer.borderColor = [[UIColor grayColor] CGColor];
    self.txtCommentToEdit.layer.borderWidth = 1.0f;
    self.txtCommentToEdit.layer.cornerRadius = 6.0f;
    
    // Render comment data
    NSString *comment = [NSString stringWithFormat:@"%@",[self.comment_mo valueForKey:@"comment"]];
    self.txtCommentToEdit.text = comment;
    
    // Get comment id
    self.commentid = [NSString stringWithFormat:@"%@",[self.comment_mo valueForKey:@"id"]];
    
    // Implement action buttons
    [self.butPostComment addTarget:self
                            action:@selector(postCommentAction:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    [self.butClose addTarget:self
                            action:@selector(closeAction:)
                  forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)viewWillLayoutSubviews {
//    [super viewWillLayoutSubviews];
//    self.view.superview.bounds = CGRectMake(0, 0, 574, 225);
//}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.preferredContentSize = CGSizeMake(574, 225);
}

- (void)postCommentAction:(id)sender {
    BOOL hasComment = [self.txtCommentToEdit.text isEqualToString:@""];

    if (!hasComment) {
        NSString *lpid = [NSString stringWithFormat:@"%@", [self.lesson_mo valueForKey:@"id"]];
        
        __weak typeof(self) wo = self;
        [self.lm requestUpdateLessonPlanComment:self.comment_mo comment:self.txtCommentToEdit.text doneBlock:^(BOOL status) {
            if (status) {
                [self.lm requestLessonPlanDetailsForLessonPlanWithID:lpid doneBlock:^(BOOL status) {
                    if (status) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [wo.delegate reloadCommentTable];
                            [wo.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                        });
                    }
                }];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [wo.lm showErrorMessageDialog];
                });
            }
        }];
    }
}

- (void)closeAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
