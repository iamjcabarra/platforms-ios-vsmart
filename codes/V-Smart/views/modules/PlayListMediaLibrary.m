//
//  PlayListMediaLibrary.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/26/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "PlayListMediaLibrary.h"
#import "PlayListMediaCell.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface PlayListMediaLibrary ()

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) NSArray *assets;
@property (strong, nonatomic) NSMutableArray *capturedImages;
@end

@implementation PlayListMediaLibrary

static NSString * const kCellIdentitfier = @"collectioncell_identifier";

#pragma mark -  Helper Functions

+ (ALAssetsLibrary *)defaultAssetsLibrary
{
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    return library;
}

- (void)setupAssetLibrary {
    
    //captured images
    self.capturedImages = [@[] mutableCopy];
    self.assets = [@[] mutableCopy];
    
    [self requestAssetLibrary];
}

- (void)requestAssetLibrary {
    
    NSLog(@"request asset library");
    
    __block NSMutableArray *tmpAssets = [@[] mutableCopy];
    ALAssetsLibrary *assetsLibrary = [PlayListMediaLibrary defaultAssetsLibrary];
    [assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        
        [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
            if(result) {
                [tmpAssets addObject:result];
            }
        }];
        self.assets = tmpAssets;
        [self.collectionView reloadData];
    } failureBlock:^(NSError *error) {
        NSLog(@"Error loading images %@", error);
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // Do any additional setup after loading the view.
    [self setupAssetLibrary];
    [self.doneButton addTarget:self action:@selector(unwindFromConfirmationForm:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)unwindFromConfirmationForm:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.assets.count;;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // Configure the cell
    PlayListMediaCell *cell = (PlayListMediaCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentitfier forIndexPath:indexPath];
    
    // Configure the cell
    ALAsset *asset = self.assets[indexPath.row];
    cell.asset = asset;
    cell.imageView.image = [UIImage imageWithCGImage:[asset thumbnail]];
//    cell.backgroundColor = [UIColor redColor];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (CGFloat) collectionView:(UICollectionView *)collectionView
                    layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 4;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView
                    layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ALAsset *asset = self.assets[indexPath.row];
    [self.delegate selectedAsset:asset];
    [self unwindFromConfirmationForm:self];
}

@end
