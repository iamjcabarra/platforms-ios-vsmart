//
//  NoteContentTagViewCell.m
//  V-Smart
//
//  Created by VhaL on 2/13/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NoteContentTagViewCell.h"
#import "NoteContent4ViewCell.h"

@implementation NoteContentTagViewCell

@synthesize closeButton, tagLabel, delegate, originalText;

-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
//    closeButton.layer.borderColor = [UIColor whiteColor].CGColor;
//    closeButton.layer.cornerRadius = closeButton.frame.size.height/2;
//    closeButton.layer.borderWidth = 1;
}

-(IBAction)removeButtonTapped:(id)sender{
    NSLog(@"removeButtonTapped");
    
    if ([delegate respondsToSelector:@selector(buttonTagRemoveTapped:)]){
        [delegate performSelector:@selector(buttonTagRemoveTapped:) withObject:tagLabel];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing - original text %@", textField.text);
    originalText = [NSString stringWithString:textField.text];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing - to:%@ from:%@", textField.text, originalText);
    if ([delegate respondsToSelector:@selector(textFieldTagUpdatedReturned:withOriginalText:)]){
        [delegate performSelector:@selector(textFieldTagUpdatedReturned:withOriginalText:) withObject:textField withObject:originalText];
        [textField resignFirstResponder];
    }
    
    return NO;
}

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        
//        self.backgroundColor = [UIColor whiteColor];
//        self.layer.shadowColor = [UIColor blackColor].CGColor;
//        self.layer.shadowRadius = 5.0f;
//        self.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
//        self.layer.shadowOpacity = 0.5f;
//        
//        // Selected background view
//        UIView *backgroundView = [[UIView alloc]initWithFrame:self.bounds];
//        backgroundView.layer.borderColor = [[UIColor colorWithRed:0.529 green:0.808 blue:0.922 alpha:1]CGColor];
//        backgroundView.layer.borderWidth = 10.0f;
//        self.selectedBackgroundView = backgroundView;
//        
//        // set content view
//        CGRect frame  = CGRectMake(self.bounds.origin.x+5, self.bounds.origin.y+5, self.bounds.size.width-10, self.bounds.size.height-10);
//        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
//        self.imageView = imageView;
//        [imageView release];
//        self.imageView.contentMode = UIViewContentModeScaleAspectFill ;
//        self.imageView.clipsToBounds = YES;
//        [self.contentView addSubview:self.imageView];       
//        
//    }
//    return self;
//}
@end
