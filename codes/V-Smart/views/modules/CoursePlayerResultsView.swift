//
//  CoursePlayerResultsView.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 31/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

@objc class CoursePlayerResultsView: UIViewController {

    var results : NSDictionary?
    var quizObject : NSManagedObject?
    
    var timeConsumed : String?
    
    @IBOutlet weak var testTitleLabel: UILabel!
    @IBOutlet weak var testScoreLabel: UILabel!
    @IBOutlet weak var testTimeLabel: UILabel!
    @IBOutlet weak var viewResultsButton: UIButton!
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var finishButtonCenterConstraint: NSLayoutConstraint! // 76
    
    @IBOutlet weak var scoreLabelIndicator: UILabel!
    @IBOutlet weak var resultHeight: NSLayoutConstraint!
    
    // MARK: - Shared Data Manager
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tm = TestGuruDataManager.sharedInstance()
        return tm!
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setUpResultsUI()
        
        self.navigationItem.hidesBackButton = true
        
        self.finishButton.addTarget(self, action: #selector(self.finishButtonAction(_:)), for: .touchUpInside);
        self.viewResultsButton.addTarget(self, action: #selector(self.viewResultsButtonAction), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    func viewResultsButtonAction() {
        let indicatorStr = NSLocalizedString("Processing", comment: "")
        HUD.showUIBlockingIndicator(withText: indicatorStr)
        
        self.dataManager.requestShowResult { (data) in
            self.quizObject = data?["mo"] as? NSManagedObject
            self.results = data?["test_result"] as? NSDictionary
            
            DispatchQueue.main.async(execute: {
                HUD.hideUIBlockingIndicator()
                self.performSegue(withIdentifier: "CP_SHOW_RESULTS_SEGUE", sender: self)
            })
        }
    }
    
    func finishButtonAction(_ button:UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func setUpResultsUI() {
        let test_name = quizObject?.value(forKey: "name") as! String
        self.testTitleLabel.text = test_name
        
        let total_score = self.results?.value(forKey: "total_score") as! String
        let general_score = self.results?.value(forKey: "general_score") as! String
//        let passing_score = self.results?.valueForKey("passing_score") as! String
//        let no_of_correct_answers = self.results?.valueForKey("no_of_correct_answers") as! String
//        let no_of_answered_questions = self.results?.valueForKey("no_of_answered_questions") as! String
//        let no_of_not_answered = self.results?.valueForKey("no_of_not_answered") as! String
//        let no_of_questions = self.results?.valueForKey("no_of_questions") as! String
//        let no_of_unchecked = self.results?.valueForKey("no_of_unchecked") as! String
        
        let show_score = self.quizObject?.value(forKey: "show_score") as! String
        let show_result = self.quizObject?.value(forKey: "show_result") as! String
        
        let show_result_bool = (show_result == "1")
        
        self.viewResultsButton.isHidden = !show_result_bool
        self.finishButtonCenterConstraint.constant = (show_result_bool) ? 76 : 0
        
        let show_score_bool = (show_score == "1")
        self.scoreLabelIndicator.isHidden = !show_score_bool
        self.testScoreLabel.isHidden = !show_score_bool
        self.resultHeight.constant = (show_score_bool) ? 397 : 200
        
        let scoreStr = "\(total_score)/\(general_score)"
        self.testScoreLabel.text = "\(scoreStr)"
        
        let firstHalf = NSLocalizedString("You took", comment: "")
        let secondHalf = NSLocalizedString("to finish the test", comment: "")
        let hms = "\(self.timeConsumed!)"
        
        self.testTimeLabel.text = "\(firstHalf) \(hms) \n\(secondHalf)"
        self.testTimeLabel.highlightString(hms, size: 21, color: UIColor.black)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeNavigationController()
    }
    
    func customizeNavigationController() {
        let color = UIColor(hex6: 0x4274b9)
        
        if floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1 {
            self.navigationController?.navigationBar.tintColor = color
        } else {
            self.navigationController?.navigationBar.barTintColor = color;
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationController?.navigationBar.isTranslucent = false
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
            // Get the new view controller using segue.destinationViewController.
            // Pass the selected object to the new view controller.
            
            if let cpsr = segue.destination as? CoursePlayerShowResultsV2View , segue.identifier == "CP_SHOW_RESULTS_SEGUE" {
                cpsr.testObject = self.quizObject;
                cpsr.results = self.results;
            }
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
