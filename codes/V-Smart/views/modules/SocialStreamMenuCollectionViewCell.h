//
//  SocialStreamMenuCollectionViewCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 2/3/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EGOImageView;

@interface SocialStreamMenuCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet EGOImageView *iconImageView;

@end
