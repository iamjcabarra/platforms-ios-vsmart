//
//  Parsable.swift
//  V-Smart
//
//  Created by Julius Abarra on 09/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

protocol Parsable {
    func parseResponseData(_ data: Data) -> Any?
    func okayToParseResponse(_ response: [String: Any]?) -> Bool
    func stringValue(_ object: Any?) -> String!
}

extension Parsable {
    
    func parseResponseData(_ data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as AnyObject?
        }
        catch let error {
            print("Error parsing response data: \(error)")
        }
        
        return nil
    }
    
    func okayToParseResponse(_ response: [String: Any]?) -> Bool {
        if let r = response {
            print("Response: \(r)")
            
            if let meta = r["_meta"] as? [String: Any], let status = meta["status"] as? String {
                if (status.lowercased() == "success") {
                    return true
                }
            }
        }
        
        return false
    }
    
    func stringValue(_ object: Any?) -> String! {
        guard let o = object else { return "" }
        
        var string = "\(o)"
        
        if (string == "null" || string == "<null>" || string == "(null)") {
            string = ""
        }
        
        return string
    }
    
}
