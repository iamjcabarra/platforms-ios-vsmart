//
//  PlayListNewItem.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/26/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PlayListNewItemDelegate <NSObject>
@required
- (void)didFinishUploading;
@end


@interface PlayListNewItem : UIViewController
@property (nonatomic, strong) NSString *module_id;
@property (nonatomic, assign) BOOL teachMode;
@property (nonatomic, weak) id <PlayListNewItemDelegate> delegate;
@end
