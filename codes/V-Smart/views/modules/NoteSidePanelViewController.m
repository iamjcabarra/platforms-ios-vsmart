//
//  NoteSidePanelViewController.m
//  V-Smart
//
//  Created by VhaL on 2/28/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NoteSidePanelViewController.h"
#import "NotesChildViewController.h"

@interface NoteSidePanelViewController ()

@end

@implementation NoteSidePanelViewController
@synthesize noteBookViewController, tagViewController, colorPopOverViewController, buttonColor, buttonTag, buttonNoteBook, containerView, delegate;
-(void)viewDidLoad{
    [super viewDidLoad];
    
    // drop shadow
    [containerView.layer setShadowColor:[UIColor darkGrayColor].CGColor];
    [containerView.layer setShadowOpacity:0.8];
    [containerView.layer setShadowRadius:5.0];
    [containerView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [containerView.layer setShadowPath:[UIBezierPath bezierPathWithRect:containerView.bounds].CGPath];
    
    // add view controllers
    noteBookViewController = [NoteBookViewController new];
    tagViewController = [TagViewController new];
    colorPopOverViewController = [NoteColorViewController new];
    
    [self addChildViewController:noteBookViewController];
    [self addChildViewController:tagViewController];
    [self addChildViewController:colorPopOverViewController];
    
    [self.view addSubview:noteBookViewController.view];
    [self.view addSubview:tagViewController.view];
    [self.view addSubview:colorPopOverViewController.view];
    
    [noteBookViewController didMoveToParentViewController:self];
    [tagViewController didMoveToParentViewController:self];
    [colorPopOverViewController didMoveToParentViewController:self];
    
    //set default selected child view
    [self buttonFolderTapped:buttonNoteBook];
    
    //add hidden close button
    UIButton *button = [[UIButton alloc] initWithFrame:
                        CGRectMake(containerView.frame.size.width, 0, self.view.frame.size.width - containerView.frame.size.width, self.view.frame.size.height)];
    
    button.backgroundColor = [UIColor clearColor];
    [button addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    
    buttonColor.enabled = NO;
    buttonTag.enabled = NO;
}

-(void)closeView{
    if (delegate && [delegate respondsToSelector:@selector(sidePanelTapped:)]) {
        NotesChildViewController *parent = (NotesChildViewController*)delegate;
        [parent sidePanelTapped:nil];
    }
}

-(void)adjustFrame{
    noteBookViewController.view.frame = CGRectMake(0, 78, containerView.frame.size.width, self.view.frame.size.height - 78);
    
    tagViewController.view.frame = CGRectMake(0, 78, containerView.frame.size.width, self.view.frame.size.height - 78);
    
    colorPopOverViewController.view.frame = CGRectMake(0, 78, containerView.frame.size.width, self.view.frame.size.height - 78);
}

-(IBAction)buttonFolderTapped:(id)sender{
    [buttonNoteBook setSelected:YES];
    [buttonTag setSelected:NO];
    [buttonColor setSelected:NO];
    
    noteBookViewController.view.hidden = !buttonNoteBook.selected;
    tagViewController.view.hidden = !buttonTag.selected;
    colorPopOverViewController.view.hidden = !buttonColor.selected;
}

-(IBAction)buttonFilterTapped:(id)sender{
    [buttonNoteBook setSelected:NO];
    [buttonTag setSelected:YES];
    [buttonColor setSelected:NO];
    
    noteBookViewController.view.hidden = !buttonNoteBook.selected;
    tagViewController.view.hidden = !buttonTag.selected;
    colorPopOverViewController.view.hidden = !buttonColor.selected;
}

-(IBAction)buttonColorTapped:(id)sender{
    [buttonNoteBook setSelected:NO];
    [buttonTag setSelected:NO];
    [buttonColor setSelected:YES];
    
    noteBookViewController.view.hidden = !buttonNoteBook.selected;
    tagViewController.view.hidden = !buttonTag.selected;
    colorPopOverViewController.view.hidden = !buttonColor.selected;
}

@end
