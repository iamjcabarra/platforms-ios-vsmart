//
//  TBPdfPageCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 13/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TBPdfPageCell.h"

@interface TBPdfPageCell() {
    CGPDFPageRef page;
}

@end

@implementation TBPdfPageCell

- (id)initWithFrame:(CGRect)rect {
    
    self = [super initWithFrame:rect];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        NSLog(@"init with frame ...");
    }
    return self;
}

- (id)initWithCoder:(NSCoder*)coder {
    
    self = [super initWithCoder:coder];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        NSLog(@"init with coder ...");
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    if (page != NULL ) {
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSaveGState(ctx);
        CGContextTranslateCTM(ctx, 0.0, self.bounds.size.height);
        CGContextScaleCTM(ctx, 1.0, -1.0);
        CGContextConcatCTM(ctx, CGPDFPageGetDrawingTransform(page, kCGPDFCropBox, self.bounds, 0, true));
        CGContextDrawPDFPage(ctx, page);
        CGContextRestoreGState(ctx);
    }
}

- (void)renderPage:(CGPDFPageRef)pageref {
    page = pageref;
    [self setNeedsDisplay];
}

@end
