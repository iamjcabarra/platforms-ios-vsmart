//
//  CPMCurriculumViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 18/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class CPMCurriculumViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate, UIDocumentInteractionControllerDelegate {

    @IBOutlet fileprivate var curriculumSearchBar: UISearchBar!
    @IBOutlet fileprivate var searchDimmedView: UIView!
    @IBOutlet fileprivate var curriculumTableView: UITableView!
    @IBOutlet fileprivate var emptyPlaceholderView: UIView!
    @IBOutlet fileprivate var emptyPlaceholderLabel: UILabel!
    
    var courseCategoryID = ""
    var courseCategoryName = ""
    var userID = ""
    
    fileprivate let kCurriculumCellIdentifier = "curriculum_cell_identifier"
    fileprivate let kFileListViewSegueIdentifier = "SHOW_FILE_LIST_VIEW"
    fileprivate let kCurriculumDetailsViewSegueIdentifier = "SHOW_CURRICULUM_DETAILS_VIEW"
    
    fileprivate var searchKey = ""
    fileprivate var isAscending = true
    fileprivate var temporaryCurriculumListCount = 1
    
    fileprivate var sandboxHelper = VSSandboxHelper()
    fileprivate var tableRefreshControl: UIRefreshControl!
    fileprivate var selectedCurriculumObject: NSManagedObject!
    fileprivate var documentController: UIDocumentInteractionController!
    
    // MARK: - Data Manager
    
    fileprivate lazy var curriculumPlannerDataManager: CPMDataManager = {
        let cpdm = CPMDataManager.sharedInstance
        return cpdm
    }()
    
    // MARK: - Versioning
    
    fileprivate lazy var isVersion25: Bool = {
        let version: CGFloat = Utils.getServerInstanceVersion() as CGFloat
        return (version >= CPMConstants.Server.MAX_VERSION)
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide dimmed view by default
        self.searchDimmedView.isHidden = true
        
        // Hide empty place holder by default
        self.shouldShowEmptyPlaceholderView(false)
        
        // Dimmed view tap recognizer
        let cancelSearchOperation = #selector(self.cancelSearchOperation(_:))
        let tapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: cancelSearchOperation)
        self.searchDimmedView.addGestureRecognizer(tapGestureRecognizer)
        
        // Empty navigation bar title by default
        self.title = ""
        
        // Dynamic table view cell's height
        self.curriculumTableView.estimatedRowHeight = 150.0;
        self.curriculumTableView.rowHeight = UITableViewAutomaticDimension;
        
        // Pull to refresh
        let refreshCurriculumListAction = #selector(self.refreshCurriculumListAction(_:))
        self.tableRefreshControl = UIRefreshControl.init()
        self.tableRefreshControl.addTarget(self, action: refreshCurriculumListAction, for: .valueChanged)
        self.curriculumTableView.addSubview(self.tableRefreshControl)
        
        // Request initial lesson list
        self.loadCurriculumListForRequestType(.load)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customizeNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.curriculumPlannerDataManager.disconnectSocket()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Custom Navigation Bar
    
    func customizeNavigationBar() {
        // Decorate navigation bar
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "#4475B7")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        // Create navigation bar right button items
        let spacerButton = UIButton.init(type: UIButtonType.custom)
        spacerButton.frame = CGRect(x: 0, y: 0, width: 10, height: 20)
        spacerButton.showsTouchWhenHighlighted = false
        
        let searchButton = UIButton.init(type: UIButtonType.custom)
        searchButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        searchButton.showsTouchWhenHighlighted = true
        
        let searchButtonImage = UIImage.init(named: "search_white_48x48.png")
        searchButton.setImage(searchButtonImage, for: UIControlState())
        
        let searchButtonAction = #selector(self.searchButtonAction(_:))
        searchButton.addTarget(self, action: searchButtonAction, for: .touchUpInside)
        
        let sortButton = UIButton.init(type: UIButtonType.custom)
        sortButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        sortButton.showsTouchWhenHighlighted = true
        
        let sortButtonImage = UIImage.init(named: "sort_white48x48.png")
        sortButton.setImage(sortButtonImage, for: UIControlState())
        
        let sortButtonAction = #selector(self.sortButtonAction(_:))
        sortButton.addTarget(self, action: sortButtonAction, for: .touchUpInside)
        
        let openFolderButton = UIButton.init(type: UIButtonType.custom)
        openFolderButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        openFolderButton.showsTouchWhenHighlighted = true
        
        let openFolderButtonImage = UIImage.init(named: "white_folder_48px.png")
        openFolderButton.setImage(openFolderButtonImage, for: UIControlState())
        
        let openFolderButtonAction = #selector(self.openFolderButtonAction(_:))
        openFolderButton.addTarget(self, action: openFolderButtonAction, for: .touchUpInside)
        
        let spacerButtonItem = UIBarButtonItem.init(customView: spacerButton)
        let openFolderButtonItem = UIBarButtonItem.init(customView: openFolderButton)
        let sortButtonItem = UIBarButtonItem.init(customView: sortButton)
        let searchButtonItem = UIBarButtonItem.init(customView: searchButton)
        self.navigationItem.rightBarButtonItems = [openFolderButtonItem, sortButtonItem, searchButtonItem, spacerButtonItem]
        
        // Custom navigation bar's search bar
        self.curriculumSearchBar.placeholder = NSLocalizedString("Search Curriculum", comment: "")
        self.curriculumSearchBar.delegate = self
    }
    
    // MARK: - List Curriculum
    
    func loadCurriculumListForRequestType(_ requestType: CPMCurrculumListRequestType) {
        if (requestType != .refresh) {
            let indicatorString = requestType == .load ? "\(NSLocalizedString("Loading", comment: ""))..." : "\(NSLocalizedString("Searching", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
        }
        
        self.curriculumPlannerDataManager.requestCurriculumListForCourseCategoryWithID(self.courseCategoryID, andUserWithID: self.userID) { (dictionaryBlock) in
            
            if let dictionary = dictionaryBlock {
                DispatchQueue.main.async(execute: {
                    let curriculumCount = dictionary["count"] as! Int
                    self.title = "\(self.courseCategoryName) (\(curriculumCount))"
                    self.temporaryCurriculumListCount = curriculumCount
                })
            }
                
            else {
                DispatchQueue.main.async(execute: {
                    let message = NSLocalizedString("No records found.", comment: "")
                    self.showNotificationMessage(message)
                })
            }
            
            DispatchQueue.main.async(execute: {
                self.reloadFetchedResultsController()
                requestType != .refresh ? HUD.hideUIBlockingIndicator() : self.tableRefreshControl.endRefreshing()
            })
        }
    }
    
    func refreshCurriculumListAction(_ sender: UIRefreshControl?) {
        self.loadCurriculumListForRequestType(.refresh)
    }
    
    // MARK: - Button Event Handlers
    
    func searchButtonAction(_ sender: UIButton) {
        self.navigationItem.titleView = self.curriculumSearchBar
        self.curriculumSearchBar.becomeFirstResponder()
    }
    
    func sortButtonAction(_ sender: UIButton) {
        self.isAscending = self.isAscending ? false : true
        self.reloadFetchedResultsController()
    }
    
    func openFolderButtonAction(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: self.kFileListViewSegueIdentifier, sender: nil)
        })
    }
    
    // MARK: - Search Bar Delegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchDimmedView.isHidden = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText == "") {
            self.searchKey = searchText
            self.reloadFetchedResultsController()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchKey = searchBar.text!
        self.reloadFetchedResultsController()
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = self.searchKey
        self.navigationItem.titleView = nil
        self.searchDimmedView.isHidden = true
    }
    
    // MARK: - Tap Gesture Recognizer
    
    func cancelSearchOperation(_ sender: UITapGestureRecognizer) {
        self.navigationItem.titleView = nil
        self.searchDimmedView.isHidden = true
        self.view.endEditing(true)
        self.curriculumSearchBar.text = self.searchKey
    }
    
    // MARK: - Alert Message View
    
    func showNotificationMessage(_ message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: NSLocalizedString("Okay", comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Empty Placeholder View
    
    func shouldShowEmptyPlaceholderView(_ show: Bool) {
        var shouldShow = show
        
        if (self.curriculumSearchBar.text == "") {
            shouldShow = self.temporaryCurriculumListCount > 0 ? false : true
        }
        
        if (shouldShow) {
            var message = NSLocalizedString("No curriculum associated with this course category yet.", comment: "")
            
            if (self.curriculumSearchBar.text != "") {
                message = "\(NSLocalizedString("No results found for", comment: "")) \"\(self.searchKey)\""
            }
            
            self.emptyPlaceholderLabel.text = message
        }
        
        self.emptyPlaceholderView.isHidden = !shouldShow
        self.curriculumTableView.isHidden = show
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        self.shouldShowEmptyPlaceholderView((sectionData.numberOfObjects > 0) ? false : true)
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kCurriculumCellIdentifier, for: indexPath) as! CPMCurriculumTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: CPMCurriculumTableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath) 
        
        cell.curriculumTitleLabel.text = mo.value(forKey: "curriculum_title") as? String
        cell.createdByLabel.text = mo.value(forKey: "created_by") as? String
        cell.curriculumDescriptionLabel.text = mo.value(forKey: "curriculum_description") as? String
        cell.gradeLevelLabel.text = mo.value(forKey: "grade_level_name") as? String
        cell.effectivityLabel.text = mo.value(forKey: "effectivity") as? String
        self.justifyLabel(cell.curriculumDescriptionLabel)
    
        var filePath = ""
        if let path = mo.value(forKey: "downloaded_file_path") as? String { filePath = path }
        cell.setDownloaded(filePath == "" ? false : true)
        
        let downloadButtonAction = #selector(self.downloadButtonAction(_:))
        cell.downloadButton.addTarget(self, action: downloadButtonAction, for: .touchUpInside)
    }
    
    // MARK: - Table View Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCurriculumObject = fetchedResultsController.object(at: indexPath)
        
        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: self.kCurriculumDetailsViewSegueIdentifier, sender: nil)
            tableView.deselectRow(at: indexPath, animated: true)
        })
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: CPMConstants.Entity.CURRICULUM)
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = self.curriculumPlannerDataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: CPMConstants.Entity.CURRICULUM)
        fetchRequest.fetchBatchSize = 20
        
        let predicate1 = self.curriculumPlannerDataManager.predicateForKeyPath("course_category_id", exactValue: self.courseCategoryID)
        let predicate2 = self.curriculumPlannerDataManager.predicateForKeyPath("user_id", exactValue: self.userID)
        
        if (self.searchKey == "") {
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
        }
        else {
            let predicate3 = self.curriculumPlannerDataManager.predicateForKeyPath("curriculum_title", containsValue: self.searchKey)
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
        }
        
        let descriptorSelector = #selector(NSString.localizedCaseInsensitiveCompare(_:))
        let sortDescriptor = NSSortDescriptor.init(key: "curriculum_title", ascending: self.isAscending, selector:descriptorSelector)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.curriculumTableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.curriculumTableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.curriculumTableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.curriculumTableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.curriculumTableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.curriculumTableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! CPMCurriculumTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.curriculumTableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.curriculumTableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.curriculumTableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.curriculumTableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Download Curriculum
    
    func downloadButtonAction(_ sender: UIButton) {
        self.selectedCurriculumObject = self.managedObjectFromButton(sender, inTableView: self.curriculumTableView)
        
        var filePath = ""
        if let path = self.selectedCurriculumObject.value(forKey: "downloaded_file_path") as? String {
            filePath = path
        }
        
        if filePath == "" {
            if let curriculumID = self.selectedCurriculumObject.value(forKey: "curriculum_id") as? String, let courseCategoryID = self.selectedCurriculumObject.value(forKey: "course_category_id") as? String {
                
                if self.isVersion25 {
                    DispatchQueue.main.async(execute: {
                        let indicatorString = "\(NSLocalizedString("Generating PDF", comment: ""))..."
                        HUD.showUIBlockingIndicator(withText: indicatorString)
                    })
                    
                    self.curriculumPlannerDataManager.requestGenerationOfCurriculum(withID: curriculumID, forCourseCategory: courseCategoryID, completionHandler: { (success) in
                        if (success) {
                            self.curriculumPlannerDataManager.startSocketIOClient(forCurriculumWithID: curriculumID, completionHandler: { (data) in
                                if data != nil {
                                    let error: String! =  data!["error"]!
                                    let file_path: String! = data!["file_path"]!
                                    
                                    if error == "0" && file_path != "" {
                                        DispatchQueue.main.async(execute: {
                                            HUD.hideUIBlockingIndicator()
                                            let indicatorString = "\(NSLocalizedString("Downloading", comment: ""))..."
                                            HUD.showUIBlockingIndicator(withText: indicatorString)
                                        })
                                        
                                        let url = URL(string: file_path)
                                        self.curriculumPlannerDataManager.downloadCurriculumFrom(url: url!, completionHandler: { (dictionaryBlock) in
                                            DispatchQueue.main.async(execute: {
                                                HUD.hideUIBlockingIndicator()
                                                
                                                if let dictionary = dictionaryBlock {
                                                    self.processSavingOfDownloadedFile(dictionary)
                                                }
                                                else {
                                                    let message = NSLocalizedString("There was an error downloading this file. Please try again later.", comment: "")
                                                    self.showNotificationMessage(message)
                                                }
                                            })
                                        })
                                    }
                                    
                                    if error == "1" {
                                        DispatchQueue.main.async(execute: {
                                            HUD.hideUIBlockingIndicator()
                                            let message = "There was an error generating PDF. Please try again later."
                                            self.view.makeToast(message: message, duration: 3, position: "center" as AnyObject)
                                        })
                                    }
                                }
                            })
                        }
                        else {
                            DispatchQueue.main.async(execute: {
                                HUD.hideUIBlockingIndicator()
                                let message = "There was an error generating PDF. Please try again later."
                                self.view.makeToast(message: message, duration: 3, position: "center" as AnyObject)
                            })
                        }
                    })
                }
                else {
                    let indicatorString = "\(NSLocalizedString("Downloading", comment: ""))..."
                    HUD.showUIBlockingIndicator(withText: indicatorString)
                    
                    self.curriculumPlannerDataManager.requestDownloadCurriculumWithID(curriculumID, completionHandler: { (dictionaryBlock) in
                        DispatchQueue.main.async(execute: {
                            HUD.hideUIBlockingIndicator()
                            
                            if let dictionary = dictionaryBlock {
                                self.processSavingOfDownloadedFile(dictionary)
                            }
                            else {
                                let message = NSLocalizedString("There was an error downloading this file. Please try again later.", comment: "")
                                self.showNotificationMessage(message)
                            }
                        })
                    })
                }
            }
            else {
                let message = NSLocalizedString("There was an error downloading this file. Please try again later.", comment: "")
                self.showNotificationMessage(message)
            }
        }
        else {
            DispatchQueue.main.async(execute: {
                let fileURL = URL(fileURLWithPath: filePath)
                self.documentController = UIDocumentInteractionController(url: fileURL)
                self.documentController.delegate = self
                self.documentController.presentPreview(animated: true)
            })
        }
    }
    
    func processSavingOfDownloadedFile(_ file: NSDictionary) {
        // Get downloaded file details
        let fileName = file["fileName"] as! NSString
        let fileSize = file["fileSize"] as! NSNumber
        let fileData = file["fileData"] as! Data
        
        // Check if directory will become full
        let subdirectoryName = "\(CPMConstants.Sandbox.DIRECTORY_PREFIX)_\(self.curriculumPlannerDataManager.retrieveUserEmail())"
        let subdirectoryPath = self.sandboxHelper.makeSubdirectoryInDocumentsDirectory(subdirectoryName)
        let subdirectorySize = self.sandboxHelper.calculateSizeOfDirectoryAtPath(subdirectoryPath)
        
        if ((subdirectorySize + fileSize.uintValue) <= CPMConstants.Sandbox.MAX_DIRECTORY_SIZE) {
            var newFileName = "\(self.selectedCurriculumObject.value(forKey: "curriculum_title") as! String).\(fileName.pathExtension)"
            var newFilePath = "\(subdirectoryPath)/\(newFileName)"
            
            // Check if file already exists
            if (self.sandboxHelper.doesFileExistAtPath(newFilePath)) {
                newFileName = self.sandboxHelper.autoChangeFileName(newFileName, atPath: subdirectoryPath)
                newFilePath = "\(subdirectoryPath)/\(newFileName)"
            }
            
            // Write file to disk
            self.sandboxHelper.saveFileData(fileData, atPath: newFilePath, completion: { (success) in
                let posiviteMessage = "\"\(newFileName)\" \(NSLocalizedString("was successfully downloaded and saved in your device.", comment: ""))"
                let negativeMessage = "\(NSLocalizedString("There was an error saving", comment: "")) \"\(newFileName)\" \(NSLocalizedString("in your device.", comment: ""))"
                self.showNotificationMessage(success ? posiviteMessage : negativeMessage)
                
                // Save file path to core data
                if (success) {
                    let curriculumID = self.selectedCurriculumObject.value(forKey: "curriculum_id") as! String
                    let predicate1 = self.curriculumPlannerDataManager.predicateForKeyPath("curriculum_id", exactValue: curriculumID)
                    let predicate2 = self.curriculumPlannerDataManager.predicateForKeyPath("user_id", exactValue: self.userID)
                    let predicate3 = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1, predicate2])
                    let fileDetails = ["downloaded_file_path": newFilePath]
                    _ = self.curriculumPlannerDataManager.updateEntity(CPMConstants.Entity.CURRICULUM, filteredByPredicate: predicate3, withData: fileDetails as NSDictionary)
                }
            })
        }
        else {
            let message = NSLocalizedString("Not enough space in application storage. Please delete some files from curriculum planner directory and try again.", comment: "")
            self.showNotificationMessage(message)
        }
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.kCurriculumDetailsViewSegueIdentifier {
            let curriculumDetailsView = segue.destination as? CPMCurriculumDetailsView
            let curriculumID = self.selectedCurriculumObject.value(forKey: "curriculum_id") as! String
            let curriculumTitle = self.selectedCurriculumObject.value(forKey: "curriculum_title") as! String
            
            curriculumDetailsView?.curriculumID = curriculumID
            curriculumDetailsView?.curriculumTitle = curriculumTitle
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        }
        
        if segue.identifier == self.kFileListViewSegueIdentifier {
            let fileListView = segue.destination as? CPMFileListViewController
            fileListView?.headerTitle = NSLocalizedString("Curriculum Collection", comment: "")
            fileListView?.subDirectoryName = "\(CPMConstants.Sandbox.DIRECTORY_PREFIX)_\(self.curriculumPlannerDataManager.retrieveUserEmail())"
            fileListView?.isUploadAction = false
        }
    }
    
    // MARK: - Document Interaction Controller Delegate
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }
    
    // MARK: - Class Helpers
    
    func justifyLabel(_ label: UILabel) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        
        let attributedString = NSAttributedString(string: label.text!, attributes: [NSParagraphStyleAttributeName: paragraphStyle, NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)])
        label.attributedText = attributedString
        label.numberOfLines = 0
    }
    
    func managedObjectFromButton(_ button: UIButton, inTableView: UITableView) -> NSManagedObject {
        let buttonPosition = button.convert(CGPoint.zero, to: inTableView)
        let indexPath = inTableView.indexPathForRow(at: buttonPosition)
        let managedObject = self.fetchedResultsController.object(at: indexPath!) 
        
        return managedObject
    }

}
