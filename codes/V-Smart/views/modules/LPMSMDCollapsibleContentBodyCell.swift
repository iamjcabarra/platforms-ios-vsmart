//
//  LPMSMDCollapsibleContentBodyCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 13/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMSMDCollapsibleContentBodyCell: UITableViewCell {

    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var contentTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func enableEditing(_ enable: Bool) {
        let text = "\(NSLocalizedString("Type your text here", comment: ""))..."
        self.contentTextView.placeholder = enable ? text : ""
        self.contentTextView.isUserInteractionEnabled = enable
    }

}
