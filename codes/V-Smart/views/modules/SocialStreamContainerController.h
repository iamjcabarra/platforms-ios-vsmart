//
//  SocialStreamContainerController.h
//  V-Smart
//
//  Created by Ryan Migallos on 4/29/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SocialStreamContainerController : BaseViewController

@end
