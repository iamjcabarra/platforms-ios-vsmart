//
//  CurriculumPeriodViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 9/30/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

@protocol CurriculumPeriodViewDelegate <NSObject>

- (void)periodSelected:(NSDictionary *)d;

@end

#import <UIKit/UIKit.h>

@interface CurriculumPeriodViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate> 

@property (nonatomic, assign) id delegate;
@property (strong, nonatomic) IBOutlet UIPickerView *curriculumPeriodPicker;
@property (strong, nonatomic) NSArray *curriculumPeriodList;

@end
