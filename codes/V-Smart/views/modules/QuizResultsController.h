//
//  QuizResultsController.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/17/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuizResultsControllerDelegate <NSObject>
@required
- (void)didFinishTappingButtonType:(NSString *)type;
@end

@interface QuizResultsController : UIViewController

@property (strong, nonatomic) NSString *userid;
@property (strong, nonatomic) NSString *quizid;
@property (strong, nonatomic) NSString *courseid;

@property (weak, nonatomic) id <QuizResultsControllerDelegate> delegate;

@end
