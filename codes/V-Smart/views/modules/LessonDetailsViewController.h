//
//  LessonDetailsViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 9/7/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonDetailsViewController : UIViewController

@property (nonatomic, strong) NSManagedObject *course_mo;
@property (nonatomic, strong) NSManagedObject *lesson_mo;
@property (nonatomic, strong) NSManagedObject *lesson_details_mo;

@end
