//
//  CoursePlayerQuizSheet.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 29/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


@objc protocol CoursePlayerQuizSheetDelegate: class {
    func didClickSubmitButton(_ flag:Bool, quizObject:NSManagedObject, testResults: NSDictionary, timeConsumed:TimeInterval)
}

@objc class CoursePlayerQuizSheet: UITableViewController, NSFetchedResultsControllerDelegate, UICollectionViewDelegateFlowLayout, CoursePlayerSidePanelDelegate, CoursePlayerQuizSheetCellDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    var delegate : CoursePlayerQuizSheetDelegate?
    
    fileprivate var fontView:CoursePlayerFontPopoverView!
    fileprivate var popoverController: UIPopoverController!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var quizTitle: UILabel!
    @IBOutlet weak var quizTimer: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var minutesLabel: UILabel!
    @IBOutlet weak var secondsLabel: UILabel!
    
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet var leftButton: UIButton!
    @IBOutlet var rightButton: UIButton!
//    @IBOutlet weak var submitButton: UIButton!
    
    fileprivate var imageIndex : Int?
    fileprivate var previewData: [AnyHashable: Any]?
    
    var testObject: NSManagedObject?
    var selectedTestAction: NSInteger?
    
    var totalQuestions: Float?
    
    var quizObject:NSManagedObject?
    
    var submitAlert:UIAlertController?
    
    var isPortrait:Bool = false
    
    var queue:OperationQueue!
    
    var spent_seconds:Int = 0
    var timeSpentTimer: Timer!
    
    // (CONSIDER SCROLL) Buggy UI During Device Orientation
    fileprivate var tempSection = 0
    fileprivate var tempPosition = 0
    fileprivate var didRotate = false
    
    // MARK: - Shared Data Manager
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tm = TestGuruDataManager.sharedInstance()
        return tm!
    }()
    
    // CELL IDENTIFIER
    let cellID = "YGTB_QUESTION_PREVIEW_COLLECTION_ITEM"
    let kModalSegue = "TGTB_PRESENT_CUSTOM_PAGE"
    let kSidePanelModelSegue = "TGTB_PRESENT_CUSTOM_SIDE_PANEL"
    let kModalSectionSelection = "SHOW_SECTION_SELECTION_VIEW"
    let kEditTestView = "SHOW_EDIT_TEST_VIEW"
    
    var index: IndexPath? = nil
    var totalItems: NSInteger? = 0
    
    let notif = NotificationCenter.default
   
    var timerComponent: MZTimerLabel?
 
    override func viewDidLoad() {
        super.viewDidLoad()

        self.fontView = CoursePlayerFontPopoverView (nibName: "CoursePlayerFontPopoverView", bundle: nil)
//        self.fontView.delegate = self
        
        self.popoverController = UIPopoverController (contentViewController: self.fontView)
        self.popoverController.contentSize = CGSize(width: 317, height: 90)
        
        // TODO: Pleas refactor
        timerComponent = MZTimerLabel(label: quizTimer, andTimerType: MZTimerLabelTypeTimer)
        
        self.collectionView.backgroundColor = UIColor.clear
        if #available(iOS 10.0, *) {
            self.collectionView.isPrefetchingEnabled = false
        }
        
        leftButton.addTarget(self, action: #selector(self.cycleButtonAction(_:)), for: UIControlEvents.touchUpInside)
        rightButton.addTarget(self, action: #selector(self.cycleButtonAction(_:)), for: UIControlEvents.touchUpInside)
        
        setupNavigationControls()
        setUpQuizSheet()
        setUpNotification()
        
        self.dataManager.save("\(20)", forKey: kCP_SELECTED_FONT_SIZE);
//        self.submitButton.addTarget(self, action: #selector(self.submitButtonAction(_:)), forControlEvents: .TouchUpInside)
        
        self.leftButton.isHidden = true
        
        if(UIDeviceOrientationIsPortrait(UIDevice.current.orientation)) {
            self.isPortrait = true
        }
        
        self.queue = OperationQueue()
        self.queue?.maxConcurrentOperationCount = 1
        self.queue?.qualityOfService = QualityOfService.background
        
        self.startSpentTimeTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeNavigationController()
    }
    
    func customizeNavigationController() {
        let color = UIColor(hex6: 0x4274b9)
        
        if floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1 {
            self.navigationController?.navigationBar.tintColor = color
        } else {
            self.navigationController?.navigationBar.barTintColor = color;
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationController?.navigationBar.isTranslucent = false
        }
    }
    
    func startSpentTimeTimer() {
        self.timeSpentTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(CoursePlayerQuizSheet.spentTimeCounter(_:)), userInfo: nil, repeats: true)
    }
    
    func spentTimeCounter(_ timer:Timer) {
        spent_seconds += 1
    }
    
    func resetTimer() {
        spent_seconds = 0
    }
    
    func submitButtonAction() {
        let submitTitle = NSLocalizedString("Submit", comment: "")
        let submitMessage = NSLocalizedString("Are you sure you want to submit this test?", comment: "")
        
        self.submitAlert = UIAlertController(title: submitTitle, message: submitMessage, preferredStyle: .alert)
        
        let yesTitle = NSLocalizedString("Yes", comment: "")
        let noTitle = NSLocalizedString("No", comment: "")
        
        let yesAction = UIAlertAction(title: yesTitle, style: .default) { (alert) in
            self.initiateSubmit()
        }
        
        let noAction = UIAlertAction(title: noTitle, style: .default, handler: nil)
        
        self.submitAlert!.addAction(yesAction)
        self.submitAlert!.addAction(noAction)
        
        self.present(self.submitAlert!, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height
    }
    
    func initiateSubmit() {
        //PAUSE TIMER
        if (self.timerComponent != nil) {
            self.timerComponent?.pause()
        }
        
        let indexes = collectionView.indexPathsForVisibleItems
        let path: IndexPath = indexes.first!
        
        let mo = fetchedResultsController.object(at: path)
        
        let question_id = mo.value(forKey: "question_id") as! String
        let timeSpent : String = self.dataManager.string(fromSeconds: self.spent_seconds)
        
        let indicatorStr = NSLocalizedString("Processing", comment: "")
        HUD.showUIBlockingIndicator(withText: indicatorStr)
        
        self.dataManager.submitAnswers(forQuestionID: question_id, timeSpent: timeSpent, isFinished: "1") { (data) in
            DispatchQueue.main.async(execute: {
                HUD.hideUIBlockingIndicator()
            })
            
            if (data?["error"] != nil) {
                let errorMessage = (data?["error"] as AnyObject).description
                
                let alert = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString(errorMessage!, comment: ""), preferredStyle: .alert)
                
                let okayTitle = NSLocalizedString("Okay", comment: "")
                
                let okayAction = UIAlertAction(title: okayTitle, style: .default, handler: nil)
                
                alert.addAction(okayAction)
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true, completion: nil)
                })
            } else {
                DispatchQueue.main.async(execute: {
                    _ = self.navigationController?.popViewController(animated: false)
                });
                
                let total_score = data?["total_score"]
                let general_score = data?["general_score"]
                let passing_score = data?["passing_score"]
                let no_of_correct_answers = data?["no_of_correct_answers"]
                let no_of_answered_questions = data?["no_of_answered_questions"]
                let no_of_not_answered = data?["no_of_not_answered"]
                let no_of_questions = data?["no_of_questions"]
                let no_of_unchecked = data?["no_of_unchecked"]
                
                let dataCopy: [String:AnyObject] = [
                    
                    "total_score": total_score as AnyObject,
                    "general_score":general_score as AnyObject,
                    "passing_score":passing_score as AnyObject,
                    "no_of_correct_answers":no_of_correct_answers as AnyObject,
                    "no_of_answered_questions":no_of_answered_questions as AnyObject,
                    "no_of_not_answered":no_of_not_answered as AnyObject,
                    "no_of_questions":no_of_questions as AnyObject,
                    "no_of_unchecked":no_of_unchecked as AnyObject
                    
                ]
                
                self.delegate?.didClickSubmitButton(true, quizObject: self.quizObject!, testResults: dataCopy as NSDictionary, timeConsumed: (self.timerComponent?.getTimeCounted())!)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        _fetchedResultsController = nil
        teardownNotification()
        
        //TIMER COMPONENT INVALIDATE
        self.timerComponent?.timerInvalidation()
        self.dataManager.save("\(20)", forKey: kCP_SELECTED_FONT_SIZE);
        
        self.timeSpentTimer.invalidate()
        self.timeSpentTimer = nil
    }

    func setUpNotification() {
        self.notif.addObserver(self, selector: #selector(CoursePlayerQuizSheet.updateProgressView), name: NSNotification.Name(rawValue: kNotificationCoursePlayerAnswered), object: nil)
        
        self.notif.addObserver(self, selector: #selector(CoursePlayerQuizSheet.fontSizeChanged(_:)), name: NSNotification.Name(rawValue: kNotificationCoursePlayerFontSizeChanged), object: nil)
        
        self.notif.addObserver(self, selector: #selector(CoursePlayerQuizSheet.saveTimeRemaining(_:)), name: NSNotification.Name(rawValue: kNotificationCoursePlayerConnectivity), object: nil)
        
        self.notif.addObserver(self, selector:#selector(CoursePlayerQuizSheet.keyboardWillAppear(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        self.notif.addObserver(self, selector:#selector(CoursePlayerQuizSheet.keyboardWillDisappear(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.notif.addObserver(self, selector:#selector(CoursePlayerQuizSheet.viewDidRotate), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
//        self.notif
    }
    
    func teardownNotification() {
        self.notif.removeObserver(self, name: NSNotification.Name(rawValue: kNotificationCoursePlayerAnswered), object: nil)
        self.notif.removeObserver(self, name: NSNotification.Name(rawValue: kNotificationCoursePlayerFontSizeChanged), object: nil)
        self.notif.removeObserver(self, name: NSNotification.Name(rawValue: kNotificationCoursePlayerConnectivity), object: nil)
        self.notif.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        self.notif.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.notif.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    func saveTimeRemaining(_ notification:Notification) {
        
//        NSString *status = [NSString stringWithFormat:@"%@", [notification object] ];
        let status = "\(notification.object!)"
        
        //no internet
        if status == "0" {
            if (self.timerComponent != nil) {
                self.timerComponent?.pause()
                let time_remaining : String! = self.quizTimer?.text
                
                let test_id = (self.quizObject?.value(forKey: "id") as AnyObject).description
                self.dataManager.saveTimeRemaining(forTest: test_id, timeRemaining: time_remaining)
            }
        }
        
        //has internet
        if status == "1" {
            let cs_id = self.dataManager.fetchObject(forKey: kCP_SELECTED_CS_ID)
            let quiz_id = self.dataManager.fetchObject(forKey: kCP_SELECTED_QUIZ_ID)
            
            let userData : [AnyHashable: Any] = ["quiz_id":quiz_id, "cs_id": cs_id];
            
            self.dataManager.requestRunOrPrerun("run", parameter: userData, dataBlock: { (data) in
                DispatchQueue.main.async(execute: {
                    self.timerComponent?.start()
                })
            })
        }
        
    }
    
    func viewDidRotate() {
        
        self.didRotate = true
        
        if(UIDeviceOrientationIsPortrait(UIDevice.current.orientation)) {
            if isPortrait == false {
                isPortrait = true
                DispatchQueue.main.async {
                    self.resizeContents()
                    self.collectionView.reloadItems(at: self.collectionView.indexPathsForVisibleItems)
                    self.collectionView.layoutIfNeeded()
                    
                    // (CONSIDER SCROLL) Buggy UI During Device Orientation
                    let indexPath = IndexPath(item: self.tempPosition, section: self.tempSection)
                    self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                    self.showMessageToastForCell(at: indexPath)
                }
            } // else do nothing
        }
        
        if(UIDeviceOrientationIsLandscape(UIDevice.current.orientation)) {
            if isPortrait {
                isPortrait = false
                DispatchQueue.main.async {
                    self.resizeContents()
                    self.collectionView.reloadItems(at: self.collectionView.indexPathsForVisibleItems)
                    self.collectionView.layoutIfNeeded()
                    
                    // (CONSIDER SCROLL) Buggy UI During Device Orientation
                    let indexPath = IndexPath(item: self.tempPosition, section: self.tempSection)
                    self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                    self.showMessageToastForCell(at: indexPath)
                }
            } // else do nothing
        }
    }
    
    func keyboardWillAppear(_ notification: Notification){
        self.tableView.isScrollEnabled = true
    }
    
    func keyboardWillDisappear(_ notification: Notification){
        self.tableView.isScrollEnabled = false
    }
    
    func fontSizeChanged(_ notification:Notification) {
        let indexPaths = self.collectionView.indexPathsForVisibleItems
        
        UIView.performWithoutAnimation {
            self.collectionView.reloadItems(at: indexPaths)
        }
    }
    
    func updateProgressView() {
        let answeredCount = self.getNumberOfAnsweredQuestion() //self.dataManager.getObjectsForEntity(kCoursePlayerQuestionChoiceEntity, predicate: predicate)

        self.progressView.progress = Float(answeredCount)/totalQuestions!
        self.progressLabel.text = "\(answeredCount) \(NSLocalizedString("of", comment: "")) \(Int(totalQuestions!)) \(NSLocalizedString("items", comment: ""))"
    }
    
    func getNumberOfAnsweredQuestion() -> Int {
        
        let arrayOfAllQuestion = self.dataManager.getObjectsForEntity(kCoursePlayerQuestionListEntity, predicate: nil)
        var numberOfAnsweredQuestions = 0
        
        if arrayOfAllQuestion != nil {
            
            for q_mo in arrayOfAllQuestion! {
                
                let question_id = (q_mo as! NSManagedObject).value(forKey: "question_id")
                let predicate = self.dataManager.predicate(forKeyPath: "question_id", object: question_id)
                
                let arrayOfChoices = self.dataManager.getObjectsForEntity(kCoursePlayerQuestionChoiceEntity, predicate: predicate) as! [NSManagedObject]?
                
                if arrayOfChoices != nil {
                    let filteredArray = arrayOfChoices!.filter({
                        let answer: String! = self.dataManager.stringValue($0.value(forKey: "answer"))
                        return answer == "1"
                    })
                    
                    if filteredArray.count > 0 {
                        numberOfAnsweredQuestions += 1
                    }
                }
            }
            
        }
        
        return numberOfAnsweredQuestions;
    }
    
    func setUpQuizSheet() {
        
        let arrayOfQuiz : [Any] = self.dataManager.getObjectsForEntity(kCoursePlayerTestEntity, predicate: nil)
        let arrayOfQuestions = self.dataManager.getObjectsForEntity(kCoursePlayerQuestionListEntity, predicate: nil)
        self.totalQuestions = Float((arrayOfQuestions?.count)!)
        
        if arrayOfQuiz.count > 0 {
            let quizObject = arrayOfQuiz.last as! NSManagedObject?
            
            let name: String! = self.dataManager.stringValue((quizObject?.value(forKey: "name")))
            let time_limit: String! = self.dataManager.stringValue((quizObject?.value(forKey: "time_limit")))
            let time_remaining_string: AnyObject! = self.dataManager.stringValue(quizObject?.value(forKey: "time_remaining")) as AnyObject!
            //let time_remaining_string: String! =  as AnyObject).doubleValue
            
            self.quizTitle.text = name!;
            self.quizTimer.text = time_limit!;
            self.quizObject = quizObject;
            
//            self.progressView.progress = 0/totalQuestions!;
//            self.progressLabel.text = "\(0) \(NSLocalizedString("of", comment: "")) \(arrayOfQuestions.count) \(NSLocalizedString("items", comment: ""))"
            self.updateProgressView()
         
            progressView.transform = progressView.transform.scaledBy(x: 1, y: 5)
            
            //TIMER SETUP
            let time_remaining = time_remaining_string.doubleValue as TimeInterval
            self.timerComponent?.setCountDownTime(time_remaining)
            self.timerComponent?.start()
            self.timerComponent?.start(ending: { (countTime) in
                 //oh my gosh, it's awesome!!
                self.timesUpAction()
            })
        }
    }
    
    func timesUpAction() {
        self.view.endEditing(true)
        if ((self.navigationController?.visibleViewController?.isKind(of: UIAlertController.self)) != nil) {//([self.navigationController.visibleViewController isKindOfClass:[UIAlertController class]]) {
            
            DispatchQueue.main.async(execute: {
                self.submitAlert?.dismiss(animated: false, completion: nil)
                self.popoverController.dismiss(animated: true)
            })
        }
        
        let indexes = collectionView.indexPathsForVisibleItems
        let path: IndexPath = indexes.first!
        
        let mo = fetchedResultsController.object(at: path)
        
        let question_id = mo.value(forKey: "question_id") as! String
        let timeSpent : String = self.dataManager.string(fromSeconds: self.spent_seconds)
        
        let indicatorStr = NSLocalizedString("Processing", comment: "")
        HUD.showUIBlockingIndicator(withText: indicatorStr)
        
        self.dataManager.submitAnswers(forQuestionID: question_id, timeSpent: timeSpent, isFinished: "1") { (data) in
            DispatchQueue.main.async(execute: {
                HUD.hideUIBlockingIndicator()
            })
            
            if (data?["error"] != nil) {
                
                let errorMessage = (data?["error"] as AnyObject).description
                
                let alert = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString(errorMessage!, comment: ""), preferredStyle: .alert)
                
                let okayTitle = NSLocalizedString("Okay", comment: "")
                
                let okayAction = UIAlertAction(title: okayTitle, style: .default, handler: nil)
                
                alert.addAction(okayAction)
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true, completion: nil)
                })
            } else {
//            let timesUpTitle = NSLocalizedString("Times up!", comment: "")
//            let timesUpMessage = NSLocalizedString("Test is submitted", comment: "")
//            
//            let alert = UIAlertController(title: timesUpTitle, message: timesUpMessage, preferredStyle: .Alert)
//            
//            let okayTitle = NSLocalizedString("Okay", comment: "")
//            
//            let okayAction = UIAlertAction(title: okayTitle, style: .Default) { (alert) in
                if data != nil {
                    
                    //let dataCopy = data as NSDictionary
                    
                    /*
                     NSDictionary *data = @{
                     @"total_score":total_score,
                     @"general_score":general_score,
                     @"passing_score":passing_score,
                     @"no_of_correct_answers":no_of_correct_answers,
                     @"no_of_answered_questions":no_of_answered_questions,
                     @"no_of_not_answered":no_of_not_answered,
                     @"no_of_questions":no_of_questions,
                     @"no_of_unchecked":no_of_unchecked
                     };
                    */
                    
                    
                    let total_score = data?["total_score"]
                    let general_score = data?["general_score"]
                    let passing_score = data?["passing_score"]
                    let no_of_correct_answers = data?["no_of_correct_answers"]
                    let no_of_answered_questions = data?["no_of_answered_questions"]
                    let no_of_not_answered = data?["no_of_not_answered"]
                    let no_of_questions = data?["no_of_questions"]
                    let no_of_unchecked = data?["no_of_unchecked"]
                    
                    let dataCopy: [String:AnyObject] = [
                        
                        "total_score": total_score as AnyObject,
                                    "general_score":general_score as AnyObject,
                                    "passing_score":passing_score as AnyObject,
                                    "no_of_correct_answers":no_of_correct_answers as AnyObject,
                                    "no_of_answered_questions":no_of_answered_questions as AnyObject,
                                    "no_of_not_answered":no_of_not_answered as AnyObject,
                                    "no_of_questions":no_of_questions as AnyObject,
                                    "no_of_unchecked":no_of_unchecked as AnyObject
                    
                    ]
                    
                    
                    DispatchQueue.main.async(execute: {
                        _ = self.navigationController?.popViewController(animated: false)
                    });
                    
                    let timeConsumed = self.dataManager.stringValue(self.quizObject?.value(forKey: "time_remaining"))
                    
                    self.delegate?.didClickSubmitButton(true, quizObject: self.quizObject!, testResults: dataCopy as NSDictionary, timeConsumed: Double(timeConsumed!)!)
                }
//            }
            
//            alert.addAction(okayAction)
//            dispatch_async(dispatch_get_main_queue(), {
//                self.presentViewController(alert, animated: true, completion: nil)
//            })
        }
    }
    }
    
    func submitAnswerForIndex() {
        let indexes = collectionView.indexPathsForVisibleItems
        let path: IndexPath = indexes.first!
        
        let mo = fetchedResultsController.object(at: path) 
        
        let question_id = mo.value(forKey: "question_id") as! String
        
//        NSString *question_id = [q_mo valueForKey:@"question_id"];
//        NSPredicate *predicate = [self predicateForKeyPath:@"question_id" object:question_id];
//        NSArray *c_array = [self getObjectsForEntity:kCoursePlayerQuestionChoiceEntity predicate:predicate];
        
        let timeSpent : String = self.dataManager.string(fromSeconds: self.spent_seconds)
        
        let blockOperation: BlockOperation = BlockOperation {
            print("question_id [\(question_id)] SPENT!! \(timeSpent)")
            self.dataManager.submitAnswers(forQuestionID: question_id, timeSpent: timeSpent, isFinished: "0", dataBlock: nil)
            self.resetTimer()
        }
        
        self.queue?.addOperation(blockOperation)
    }
    
    func cycleButtonAction(_ button:UIButton!) {
        
        let indexes = collectionView.indexPathsForVisibleItems
        let path: IndexPath = indexes.first!
        let section = (path as NSIndexPath).section
        var position: Int = (path as NSIndexPath).row
        
        if button == rightButton {
            position = position + 1
            
            if position >= totalItems {
                position = (totalItems! - 1)
            }
        }
        
        if button == leftButton {
            position = position - 1
            
            if position <= 0 {
                position = 0
            }
        }
        
        self.submitAnswerForIndex()
        
        let indexPath = IndexPath(item: position, section: section)
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.showMessageToastForCell(at: indexPath)
        
        self.tempSection = section
        self.tempPosition = position
        
        self.updateButtons("buttons", indexRow: (indexPath as NSIndexPath).row)
    }
    
    func updateButtons(_ action: String, indexRow: Int) {
        var currentPos : Int = 0
        let totalItems = self.collectionView.numberOfItems(inSection: 0)
        let lastIndex = totalItems - 1
        
        if action == "buttons"{
            currentPos = indexRow
        } else {
            currentPos = ((self.collectionView.indexPathsForVisibleItems.last as NSIndexPath?)?.row)!
        }
        
        if currentPos == 0 {
            self.leftButton.isHidden = true
            self.rightButton.isHidden = false
        }
        
        if currentPos == lastIndex {
            self.rightButton.isHidden = true
            self.leftButton.isHidden = false
        }
        
        if currentPos > 0 && currentPos < lastIndex {
            self.rightButton.isHidden = false
            self.leftButton.isHidden = false
        }
        
        // OVERRIDE: HIDE ALL WHEN TOTAL ITEMS IS 1
        if totalItems == 1 {
            self.leftButton.isHidden = true
            self.rightButton.isHidden = true
        }
        
        self.updateVisit(currentPos)
    }
    
    func updateVisit(_ currentPos: Int) {
        let mo = fetchedResultsController.object(at: IndexPath(item: currentPos, section: 0))
        let questionID = mo.value(forKey: "question_id") as! String!
        let userData = ["is_visited":"1"]
        let predicate = self.dataManager.predicate(forKeyPath: "question_id", andValue: questionID)
        self.updateIsVisited(userData as NSDictionary, predicate: predicate!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // collection view data source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            totalItems = 0
            return totalItems!
        }
        totalItems = sectionData.numberOfObjects
        
        if totalItems == 1 {
            self.leftButton.isHidden = true
            self.rightButton.isHidden = true
        }
        
        return totalItems!;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath)
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
//    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
//        
//        let totalRow = collectionView.numberOfItemsInSection(indexPath.section)
//        self.submitButton.hidden = true
//        if indexPath.row == totalRow - 1 {
//            self.submitButton.hidden = false
//        }
//    }
//    
    fileprivate func configureCell(_ collectionCell: UICollectionViewCell!, atIndexPath indexPath: IndexPath) {
        self.view.endEditing(true)
        
        let cell = collectionCell as! CoursePlayerQuizSheetCell
        cell.delegate = self;
        
        let mo = fetchedResultsController.object(at: indexPath)
        let questionID = mo.value(forKey: "question_id") as! String!
        let number_of_correct_answers = mo.value(forKey: "number_of_correct_answers") as! String!
        //let is_case_sensitive: String! = mo.value(forKey: "is_case_sensitive") as! String!
        
        cell.questionID = questionID
        cell.number_of_correct_answers = Int(number_of_correct_answers!)!
        cell.display(mo)
        cell.reloadFetchedResultsController()
        
        let height = self.collectionView.frame.size.height;
        
        cell.questionAndImageTableHeight.constant = height/2;
        cell.choiceTableHeight.constant = (height/2) - 40;

        cell.submitButton.isHidden = true
//        let totalRow = collectionView.numberOfItemsInSection(indexPath.section)
        if (indexPath as NSIndexPath).row == (Int(totalQuestions!) - 1) {
            cell.submitButton.addTarget(self, action: #selector(self.submitButtonAction), for: .touchUpInside)
            cell.submitButton.isHidden = false;
        }
        
        /*
        if is_case_sensitive == "1" {
            let caseSensitiveMsg = NSLocalizedString("The correct answer for this question is case sensitive", comment: "")
            self.view.makeToast(message: caseSensitiveMsg, duration: 1.5, position: "center" as AnyObject)
        }
        */
        
        if self.didRotate == false {
            self.showMessageToastForCell(at: indexPath)
        }
    }
    
    fileprivate func showMessageToastForCell(at indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        guard let isCaseSensitive = mo.value(forKey: "is_case_sensitive") as? String else { return }
        
        if isCaseSensitive == "1" {
            let caseSensitiveMsg = NSLocalizedString("The correct answer for this question is case sensitive", comment: "")
            self.view.makeToast(message: caseSensitiveMsg, duration: 1.5, position: "center" as AnyObject)
        }
    }
    
    func cancelCellButtonAction(_ button:UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
        //        self.navigationController?.popViewControllerAnimated(false)
    }
    
    func addCellButtonAction(_ button:UIButton) {
        self.performSegue(withIdentifier: "TGTB_ASSIGN_QUESTION_VIEW", sender: self)
    }
    
    // flow layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.size.height
        let width = collectionView.frame.size.width
        return CGSize(width: width, height: height)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        print("---------------------------------------------> " + #function)
        resizeContents()
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("---------------------------------------------> " + #function)
        self.delay(0.1) {
            self.updateButtons("", indexRow: 0)
        }
        resizeContents()
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("---------------------------------------------> " + #function)
        resizeContents()
    }
    
    fileprivate func resizeContents() {
        
        print("---------------------------------------------> " + #function)
        
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let size = self.collectionView.bounds.size
        let w = size.width
        let h = size.height
        
        layout.itemSize = CGSize(width: w, height: h)
        layout.invalidateLayout()
    }
    
    // helper functions
    func setupNavigationControls() {
        
        self.navigationItem.backBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        
        let backButton = generateCustomButton("icon_menubutton_150px.png", action: #selector(CoursePlayerQuizSheet.showSideMenu(_:)))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        
        let optionButton = generateCustomButton("font_adjustment_icn.png", action: #selector(CoursePlayerQuizSheet.optionButtonAction(_:)))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: optionButton)
    }
    
    func generateCustomButton(_ imageName: String, action: Selector) -> UIButton {
        
        let button = UIButton(type: UIButtonType.system)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.customSetImage(imageName)
        button.tintColor = UIColor.white
        button.addTarget(self, action: action, for: .touchUpInside)
        
        return button
    }
    
    func optionButtonAction(_ sender: UIButton) {
        self.popoverController .present(from: sender.bounds, in: sender, permittedArrowDirections: UIPopoverArrowDirection.up, animated: true)
    }
    
    func showSideMenu(_ button:UIBarButtonItem) {
        performSegue(withIdentifier: kSidePanelModelSegue, sender: self)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == kSidePanelModelSegue {
            let spv = segue.destination as? CoursePlayerSidePanel
            spv?.delegate = self;
        }
        
        if segue.identifier == "SAMPLE_QUESTION_IMAGE_VIEW" {
            let qip = segue.destination as? QuestionImagePreview
            qip?.previewData = self.previewData
            qip?.index = self.imageIndex!
        }
        
    }
    
    // MARK: - Navigation
    func didFinishSelectingObject(_ questionid: String, indexPath: IndexPath) {
        self.submitAnswerForIndex()
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        self.updateButtons("buttons", indexRow: (indexPath as NSIndexPath).row)
        resizeContents()
    }
    
    /////////////////////////////////// NSFETCHRESULTSCONTROLLER ///////////////////////////////////
    
    /*
     NOTE: CUSTOM ANIMATION
     */
    // MARK: - Fetched results controller
    
    fileprivate lazy var customDescriptors: [NSSortDescriptor] = {
        
        let ascending = true
        let question_index = NSSortDescriptor(key: "index", ascending: ascending)
        
        return [question_index]
    }()
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kCoursePlayerQuestionListEntity)
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        // Set ManagedObjectContext
        let ctx = dataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: kCoursePlayerQuestionListEntity)
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        fetchRequest.sortDescriptors = customDescriptors
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest,
                                             managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    fileprivate var blockOperation = BlockOperation()
    fileprivate var shouldReloadCollectionView = false
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.shouldReloadCollectionView = false
        self.blockOperation = BlockOperation()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange sectionInfo: NSFetchedResultsSectionInfo,
                                     atSectionIndex sectionIndex: Int,
                                             for type: NSFetchedResultsChangeType) {
        
        let collectionView = self.collectionView
        switch type {
        case NSFetchedResultsChangeType.insert:
            self.blockOperation.addExecutionBlock({
                collectionView?.insertSections( IndexSet(integer: sectionIndex) )
            })
        case NSFetchedResultsChangeType.delete:
            self.blockOperation.addExecutionBlock({
                collectionView?.deleteSections( IndexSet(integer: sectionIndex) )
            })
        case NSFetchedResultsChangeType.update:
            self.blockOperation.addExecutionBlock({
                collectionView?.reloadSections( IndexSet(integer: sectionIndex ) )
            })
        default:()
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                                    at indexPath: IndexPath?,
                                                for type: NSFetchedResultsChangeType,
                                                              newIndexPath: IndexPath?) {
        
        let collectionView = self.collectionView
        
        switch type {
            
        case NSFetchedResultsChangeType.insert:
            
            if (collectionView?.numberOfSections)! > 0 {
                
                if collectionView?.numberOfItems( inSection: (newIndexPath! as NSIndexPath).section ) == 0 {
                    self.shouldReloadCollectionView = true
                } else {
                    self.blockOperation.addExecutionBlock( {
                        collectionView?.insertItems( at: [newIndexPath!] )
                    } )
                }
                
            } else {
                self.shouldReloadCollectionView = true
            }
            
        case NSFetchedResultsChangeType.delete:
            
            if collectionView?.numberOfItems( inSection: (indexPath! as NSIndexPath).section ) == 1 {
                self.shouldReloadCollectionView = true
            } else {
                self.blockOperation.addExecutionBlock( {
                    collectionView?.deleteItems( at: [indexPath!] )
                } )
            }
            
        case NSFetchedResultsChangeType.update:
            self.blockOperation.addExecutionBlock({
                collectionView?.reloadItems( at: [indexPath!] )
            })
            
        case NSFetchedResultsChangeType.move:
            self.blockOperation.addExecutionBlock({
                collectionView?.moveItem( at: indexPath!, to: newIndexPath! )
            })
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        // Checks if we should reload the collection view to fix a bug @ http://openradar.appspot.com/12954582
        if self.shouldReloadCollectionView {
            self.collectionView.reloadData()
        } else {
            self.collectionView.performBatchUpdates({ self.blockOperation.start() }, completion: nil )
        }
    }
    
    func transition(withData previewData: [AnyHashable: Any]!, index: Int, segueID segueIdentifier: String!) {
        self.previewData = previewData
        self.imageIndex = index
        self.performSegue(withIdentifier: segueIdentifier, sender: self)
    }
    

    func updateIsVisited(_ userData:NSDictionary, predicate: NSPredicate) {
        print("user data : \(userData)")
        self.dataManager.executeUpdate(forEntity: kCoursePlayerQuestionListEntity, details: userData as! [AnyHashable: Any], predicate: predicate, doneBlock: nil);
    }
    
//    - (void)updateAnswerCorrect:(NSDictionary *)userData withPredicate:(NSPredicate *)predicate{
//    
//    NSLog(@"user data : %@", userData);
//    //    NSPredicate *predicate = [self.tm predicateForKeyPath:@"id" andValue:choiceID];
//    [self.tm executeUpdateForEntity:kCoursePlayerQuestionChoiceEntity details:userData predicate:predicate doneBlock:^(BOOL status) {
//    [self.notif postNotificationName:kNotificationCoursePlayerAnswered object:nil];
//    }];
//    }

}
