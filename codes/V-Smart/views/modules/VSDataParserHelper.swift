//
//  VSDataParserHelper.swift
//  V-Smart
//
//  Created by Julius Abarra on 20/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

protocol ParseProtocol {
    func parseResponseData(_ data: Data) -> Any?
    func okayToParseResponse(_ response: [String: Any]?) -> Bool
    func stringValue(_ object: Any?) -> String!
    
    var dateFormatter: DateFormatter! { get set }
    var userDefaults: UserDefaults! { get set }
}

extension ParseProtocol {
    func parseResponseData(_ data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        }
        catch let error {
            print("Error parsing response data: \(error)")
        }
        
        return nil
    }
    
    func okayToParseResponse(_ response: [String: Any]?) -> Bool {
        guard let r = response, let meta = r["_meta"] as? [String: AnyObject], let status = meta["status"] as? String else {
            return false
        }
        
        if status.lowercased() == "success" {
            return true
        }
        else {
            return false
        }
    }
    
    func stringValue(_ object:Any?) -> String! {
        guard let objectCopy = object else {
            return ""
        }
        
        let value = "\(objectCopy)"
        
        if (value == "null") || (value == "<null>") || (value == "(null)") {
            return ""
        }
        
        return value as String!
    }
    
    func stripHTLMTags(inString str : String) -> String {
        let newStr = str.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        
        return newStr
    }
    
    func save(object: AnyObject!, forKey key: String!) {
        self.userDefaults.set(object, forKey: key)
        self.userDefaults.synchronize()
    }
    
    func fetchUserDefaultsObject(forKey key: String!) -> AnyObject! {
        let object = self.userDefaults.object(forKey: key)
        
        if object == nil {
            return "" as AnyObject!
        }
        
        return object as AnyObject!
    }
    
    func removeObjectForKey(_ key: String) {
        self.userDefaults.removeObject(forKey: key)
    }
    
    func date(fromString string: String, withFormat format: String, convertToLocalTime: Bool = false) -> Date {
        var tokenDot = string.components(separatedBy: ".")
        var tokenPlus = tokenDot[0].components(separatedBy: "+")
        let firstToken = tokenPlus[0]
        
        dateFormatter.dateFormat = format
        
        if convertToLocalTime { dateFormatter.timeZone = TimeZone(identifier: "UTC") }
        guard let formattedDate = dateFormatter.date(from: firstToken) else { return Date() }
        
        return formattedDate
    }
    
    func getStringDateTodayForFormat(_ format: String, localTimezone: Bool = false) -> String {
        dateFormatter.dateFormat = format
        if localTimezone { dateFormatter.timeZone = TimeZone(identifier: "UTC") }
        return dateFormatter.string(from: Date())
    }
    
    func dateString(_ string: String, fromFormat: String, toFormat: String) -> String {
        dateFormatter.dateFormat = fromFormat
        
        guard let showDate = dateFormatter.date(from: string) else {
            dateFormatter.dateFormat = toFormat
            return dateFormatter.string(from: Date())
        }
        
        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: showDate)
    }
    
}
