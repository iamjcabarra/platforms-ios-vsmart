//
//  LPMStageContentTVController.swift
//  V-Smart
//
//  Created by Julius Abarra on 05/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMStageContentTVController: UITableViewController, NSFetchedResultsControllerDelegate, UITextViewDelegate, LPMFileListViewControllerDelegate {

    var lessonContentStageID: String!
    
    fileprivate let kFileListViewSegueIdentifier = "SHOW_FILE_LIST_VIEW"
    fileprivate var selectedFileObject: NSManagedObject!
    
    // MARK: - Data Manager
    
    fileprivate lazy var lessonPlanDataManager: LessonPlanDataManager = {
        let lpdm = LessonPlanDataManager.sharedInstance()
        return lpdm!
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.allowsSelection = false;
        self.tableView.allowsMultipleSelection = false;
        self.tableView.rowHeight = 150.0;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table View Data Source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "stage_content_process_cell", for: indexPath) as! LPMStageContentTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: LPMStageContentTableViewCell, atIndexPath indexPath: IndexPath) {
        let processObject = fetchedResultsController.object(at: indexPath) 
  
        let processName = processObject.value(forKey: "learning_process_name") as! String
        let processContent = processObject.value(forKey: "content") as! String
        let processContentTypeFlag = processObject.value(forKey: "is_file") as! String
        let processContentFileName = processObject.value(forKey: "orig_filename") as! String
        
        cell.processNameLabel.text = processName.uppercased()
        cell.processContentText.text = processContent
        cell.disableFileUploadCapability(processContentTypeFlag == "1" ? true : false)
        
        let uploadFileButtonAction = #selector(self.uploadFileButtonAction(_:))
        cell.uploadFileButton.addTarget(self, action: uploadFileButtonAction, for: .touchUpInside)
        cell.updateUploadFileButtonTitle(processContentFileName)
        
        let deleteFileButtonAction = #selector(self.deleteFileButtonAction(_:))
        cell.deleteFileButton.addTarget(self, action: deleteFileButtonAction, for: .touchUpInside)
        
        let textViewTag = processObject.value(forKey: "lc_id") as! Int
        cell.processContentText.delegate = self
        cell.processContentText.tag = textViewTag
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = self.lessonPlanDataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: kCopyLessonContentProcessEntity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kCopyLessonContentProcessEntity)
        fetchRequest.fetchBatchSize = 20
        
        let predicate = NSPredicate(format: "p_content.stage_id == %@", self.lessonContentStageID)
        fetchRequest.predicate = predicate
        
        let sortDescriptor = NSSortDescriptor.init(key: "lps_id", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.tableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! LPMStageContentTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.kFileListViewSegueIdentifier {
            let fileListView = segue.destination as! LPMFileListViewController
            fileListView.headerTitle = NSLocalizedString("Select a file to upload", comment: "")
            fileListView.subDirectoryName = "\(LPMConstants.Sandbox.DIRECTORY_PREFIX)_\(self.lessonPlanDataManager.userEmail()!)_UPLOAD"
            fileListView.isUploadAction = true
            fileListView.delegate = self
        }
    }
    
    // MARK: - Button Event Handlers
    
    func uploadFileButtonAction(_ sender: UIButton) {
        // Uncomment to enable file upload through iTunes
        // self.selectedFileObject = self.managedObjectFromButton(sender, inTableView: self.tableView)
        // self.performSegue(withIdentifier: self.kFileListViewSegueIdentifier, sender: nil)
        
        // Comment out if you want to enable file upload through iTunes
        let message = NSLocalizedString("To upload, please use other platforms.", comment: "")
        let acTitle = NSLocalizedString("Okay", comment: "")
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: acTitle, style: .default) { (Alert) -> Void in alert.dismiss(animated: true, completion: nil) }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteFileButtonAction(_ sender: UIButton) {
        self.selectedFileObject = self.managedObjectFromButton(sender, inTableView: self.tableView)
        
        if let lcid = self.selectedFileObject.value(forKey: "lc_id") {
            let data = ["orig_filename": "", "file_path": ""]
            let predicate = self.lessonPlanDataManager.predicate(forKeyPath: "lc_id", andValue: "\(lcid)")
            self.lessonPlanDataManager.updateEntity(kCopyLessonContentProcessEntity, predicate: predicate, withData: data)
            self.progressViewPostNotification()
        }
    }
    
    func managedObjectFromButton(_ button: UIButton, inTableView: UITableView) -> NSManagedObject {
        let buttonPosition = button.convert(CGPoint.zero, to: inTableView)
        let indexPath = inTableView.indexPathForRow(at: buttonPosition)
        let managedObject = self.fetchedResultsController.object(at: indexPath!)
        
        return managedObject
    }
    
    // MARK: - File List View Delegate
    
    func selectedFileObject(_ fileObject: NSDictionary) {
        if let lcid = self.selectedFileObject.value(forKey: "lc_id") {
            let data = ["orig_filename": fileObject["fileName"] as! String, "file_path": fileObject["filePath"] as! String]
            let predicate = self.lessonPlanDataManager.predicate(forKeyPath: "lc_id", andValue: "\(lcid)")
            self.lessonPlanDataManager.updateEntity(kCopyLessonContentProcessEntity, predicate: predicate, withData: data)
            self.progressViewPostNotification()
        }
    }
 
    // MARK: - Saving of Updated Data
    
    func textViewDidChange(_ textView: UITextView) {
        let lcid: String! = "\(textView.tag)"
        let data: [String : String] = ["content": textView.text!]
        let predicate = self.lessonPlanDataManager.predicate(forKeyPath: "lc_id", andValue: lcid)
        self.lessonPlanDataManager.updateEntity(kCopyLessonContentProcessEntity, predicate: predicate, withData: data)
        self.progressViewPostNotification()
    }
    
    // MARK: - Progress View Post Notification
    
    func progressViewPostNotification() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateProgressView"), object: nil, userInfo: nil)
    }

}
