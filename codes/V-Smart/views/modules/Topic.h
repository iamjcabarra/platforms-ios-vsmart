//
//  Topic.h
//  V-Smart
//
//  Created by Ryan Migallos on 10/3/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Course;

@interface Topic : NSManagedObject

@property (nonatomic, retain) NSString * filename;
@property (nonatomic, retain) NSString * index;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Course *course;

@end
