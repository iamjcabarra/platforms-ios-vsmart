//
//  CurriculumViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 9/28/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

@protocol CurriculumViewDelegate <NSObject>

@required
- (void)moSelected:(NSManagedObject *)mo;

@end

#import <UIKit/UIKit.h>

@interface CurriculumViewController : UIViewController

@property (nonatomic, assign) id delegate;

@end
