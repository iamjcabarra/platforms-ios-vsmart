//
//  GBTGradePanelCell.swift
//  V-Smart
//
//  Created by Ryan Migallos on 16/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTGradePanelCell: UITableViewCell {

    @IBOutlet var initialGradeLabel: UILabel!
    @IBOutlet var quarterlyGradeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
