//
//  LessonViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 9/1/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonViewController : UIViewController

@property (nonatomic, strong) NSManagedObject *course_mo;

@end
