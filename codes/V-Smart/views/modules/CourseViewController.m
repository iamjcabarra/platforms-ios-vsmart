//
//  CourseViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/1/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "CourseViewController.h"
#import "CPCourseCellItem.h"
#import "LessonViewController.h"
#import "LessonPlanDataManager.h"
#import "VSmartValues.h"
#import "AccountInfo.h"
#import "Utils.h"
#import "HUD.h"

@interface CourseViewController () <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UISearchBarDelegate>

@property (strong, nonatomic) LessonPlanDataManager *lm;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl *tableRefreshControl;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UIView *emptyView;
@property (strong, nonatomic) IBOutlet UILabel *emptyMessageLabel;

@property (strong, nonatomic) IBOutlet UIView *visualEffectView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@property (assign, nonatomic) BOOL isAscending;
@property (strong, nonatomic) NSString *searchKeyword;

@property (strong, nonatomic) NSString *userid;
@property (strong, nonatomic) NSManagedObject *selectedCourseObject;

@property (assign, nonatomic) NSInteger temporaryCourseCount;

@property (assign, nonatomic) BOOL isVersion25;
 
@end

@implementation CourseViewController

static NSString *kCourseCellIdentifier = @"courseCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Get user id of currently logged user
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.userid = [NSString stringWithFormat:@"%d", account.user.id];
    
    // Set up course table
    [self setUpCourseTableView];
    
    // Set initial search keyword
    self.searchKeyword = @"";
    
    // Set initial result to descending order
    self.isAscending = NO;
    
    // Use singleton class as data manager
    self.lm = [LessonPlanDataManager sharedInstance];
    self.managedObjectContext = self.lm.mainContext;
    
    // Implement a selector for refresh action
    SEL refreshAction = @selector(refreshCourseList);
    
    // Implement refresh control
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    // List course
    [self listAllCourse];
    
    // Course Count (1 to avoid loading of empty view on first load)
    self.temporaryCourseCount = 1;
    
    // Empty View
    [self shouldShowEmptyView:NO];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelSearchWhenTapped)];
    [self.visualEffectView addGestureRecognizer:tap];
    
    CGFloat version = [[Utils getServerInstanceVersion] floatValue];
    
    NSLog(@"TEST GURU SERVER VERSION : %@", @(version) );
    
    self.isVersion25 = (version >= VSMART_SERVER_MAX_VER);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    // Custom navigation bar
    [self customizeNavigationController];
    
    // Custom navigation bar buttons
    [self setUpRightBarButtons];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Navigation Bar

- (void)customizeNavigationController {
    UIColor *color = [UIColor colorWithRed:0.0f/255.0f green: 100.0f/255.0f blue:190.0f/255.0f alpha:1.0f];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    }
    else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationItem.titleView = nil;
    
    // Custom Navigation Title View
    self.searchBar.placeholder = NSLocalizedString(@"Search Course", nil);
    self.searchBar.delegate = self;
 }

#pragma mark - Custom Right Bar Buttons

- (void)setUpRightBarButtons {
    // Spacer Button
    UIButton *spacerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    spacerButton.frame = CGRectMake(0, 0, 10, 20);
    
    // Search Button
    UIImage *searchImage = [UIImage imageNamed:@"search_white_48x48.png"];
    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    searchButton.frame = CGRectMake(0, 0, 20, 20);
    searchButton.showsTouchWhenHighlighted = YES;
    
    [searchButton setImage:searchImage forState:UIControlStateNormal];
    [searchButton setImage:searchImage forState:UIControlStateHighlighted];
    [searchButton addTarget:self action:@selector(searchButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Sort Button
    UIImage *sortImage = [UIImage imageNamed:@"sort_white48x48.png"];
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    sortButton.frame = CGRectMake(0, 0, 20, 20);
    sortButton.showsTouchWhenHighlighted = YES;
    
    [sortButton setImage:sortImage forState:UIControlStateNormal];
    [sortButton setImage:sortImage forState:UIControlStateHighlighted];
    [sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Right Button Items
    UIBarButtonItem *spacerButtonItem = [[UIBarButtonItem alloc] initWithCustomView:spacerButton];
    UIBarButtonItem *sortButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
    UIBarButtonItem *searchButtonItem = [[UIBarButtonItem alloc] initWithCustomView:searchButton];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:sortButtonItem, searchButtonItem, spacerButtonItem, nil];
}

#pragma mark - List Course

- (void)listAllCourse {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Loading", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    __weak typeof(self) wo = self;
    
    [self.lm requestCourseListForUser:self.userid dataBlock:^(NSDictionary *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD hideUIBlockingIndicator];
            
            BOOL show = (data != nil) ? NO : YES;
            NSString *message = NSLocalizedString(@"Sorry, there was an error loading this page. Please try again later.", nil);            
            [wo showMessage:message show:show completion:^(BOOL okay) {}];
            
            if (data != nil) {
                NSString *countString = [wo.lm stringValue:data[@"count"]];
                [wo updateNavigationTitle:[countString integerValue]];
                wo.temporaryCourseCount = [countString integerValue];
                [wo reloadFetchedResultsController];
            }
        });
    }];
}

- (void)refreshCourseList {
    __weak typeof(self) wo = self;
    
    [self.lm requestCourseListForUser:self.userid dataBlock:^(NSDictionary *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.tableRefreshControl endRefreshing];
            
            BOOL show = (data != nil) ? NO : YES;
            NSString *message = NSLocalizedString(@"Sorry, there was an error loading this page. Please try again later.", nil);            
            [wo showMessage:message show:show completion:^(BOOL okay) {}];
            
            if (data != nil) {
                NSString *countString = [wo.lm stringValue:data[@"count"]];
                [wo updateNavigationTitle:[countString integerValue]];
                wo.temporaryCourseCount = [countString integerValue];
                [wo reloadFetchedResultsController];
            }
        });
    }];
}

#pragma mark - Course Table View

- (void)setUpCourseTableView {
    // Set Protocols
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Allow Selection
    self.tableView.allowsSelection = YES;
    self.tableView.allowsMultipleSelection = NO;
    
    // Default Height
    self.tableView.estimatedRowHeight = 75.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    // Resusable Cell
    UINib *courseCellItemNib = [UINib nibWithNibName:@"CPCourseCellItem" bundle:nil];
    [self.tableView registerNib:courseCellItemNib forCellReuseIdentifier:kCourseCellIdentifier];
}

#pragma mark - Actions for Buttons

- (void)searchButtonAction:(id)sender {
    self.navigationItem.titleView = self.searchBar;
    [self.searchBar becomeFirstResponder];
}

- (void)sortButtonAction:(id)sender {
    self.isAscending = (self.isAscending) ? NO : YES;
    [self listAllCourse];
}

#pragma mark - Search Bar Delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    self.visualEffectView.hidden = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText isEqualToString:@""]) {
        self.searchKeyword = searchText;
        [self listAllCourse];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.searchKeyword = searchBar.text;
    [self listAllCourse];
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    searchBar.text = self.searchKeyword;
    self.navigationItem.titleView = nil;
    self.visualEffectView.hidden = YES;
}

- (void)cancelSearchWhenTapped {
    self.navigationItem.titleView = nil;
    self.visualEffectView.hidden = YES;
    [self.view endEditing:YES];
    self.searchBar.text = self.searchKeyword;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];;
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    
    // NOTE: REMOVE IF PAGINATED
    [self shouldShowEmptyView:(count > 0) ? NO : YES];
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView = tableView;
    
    CPCourseCellItem *cell = [tableView dequeueReusableCellWithIdentifier:kCourseCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    CPCourseCellItem *courseCellItem = (CPCourseCellItem *)cell;
    [self configureQuestionCell:courseCellItem managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(CPCourseCellItem *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    NSString *course_name = [mo valueForKey:@"course_name"];
    NSString *section_name = [mo valueForKey:@"section_name"];
    NSString *grade_level = [mo valueForKey:@"grade_level"];
    NSString *schedule = [mo valueForKey:@"schedule"];
    
    cell.courseSectionLabel.hidden = (self.isVersion25 == YES) ? NO : YES;
    cell.courseNameLabel.text = course_name;
    cell.courseSectionLabel.text = [NSString stringWithFormat:@"%@ - %@", section_name, grade_level];
    cell.courseScheduleLabel.text = schedule;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedCourseObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *courseid = [self.lm stringValue:[self.selectedCourseObject valueForKey:@"course_id"]];
   
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Please wait", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    __weak typeof(self) wo = self;
    
    [self.lm requestLessonPlanListForUser:self.userid courseid:courseid dataBlock:^(NSDictionary *data) {
        if (data != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [wo performSegueWithIdentifier:@"showLessonList" sender:nil];
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                NSString *message = NSLocalizedString(@"Sorry, there was an error accessing lesson plans associated with this course.", nil);
                message = [NSString stringWithFormat:@"%@\n[Empty Database]", message];
                [wo showMessage:message show:YES completion:^(BOOL okay) {}];
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                
                // Hot Fix (wrong response from server when DB is empty
                // jca-05112016
                [wo performSegueWithIdentifier:@"showLessonList" sender:nil];
            });
        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showLessonList"]) {
        LessonViewController *lesson = (LessonViewController *)[segue destinationViewController];
        lesson.course_mo = self.selectedCourseObject;
    }
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCourseTableEntity];
    [fetchRequest setFetchBatchSize:10];
    
    if (![self.searchKeyword isEqualToString:@""]) {
        NSPredicate *predicate = [self predicateForKeyPathContains:@"search_string" value:self.searchKeyword];
        [fetchRequest setPredicate:predicate];
    }
    
    NSSortDescriptor *search_string = [NSSortDescriptor sortDescriptorWithKey:@"search_string"
                                                                  ascending:self.isAscending
                                                                   selector:@selector(caseInsensitiveCompare:)];
    [fetchRequest setSortDescriptors:@[search_string]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
   
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - Reloading of Data

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

#pragma mark - Alert Messages

- (void)showMessage:(NSString *)message show:(BOOL)show completion:(void (^)(BOOL okay))response {
    if (show) {
        NSString *butOkay = NSLocalizedString(@"Okay", nil);
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *theAction = [UIAlertAction actionWithTitle:butOkay
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                              response(YES);
                                                          }];
        [alert addAction:theAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - Custom Empty View

- (void)shouldShowEmptyView:(BOOL)show {
    // NOTE: REFACTOR IF PAGINATED
    if ([self.searchBar.text isEqualToString:@""]) {
        show = (self.temporaryCourseCount > 0) ? NO : YES;
    }
    
    if (show) {
        NSString *message = NSLocalizedString(@"No available course yet.", nil);
        
        if (![self.searchBar.text isEqualToString:@""]) {
            message = NSLocalizedString(@"No results found for", nil);
            message =[NSString stringWithFormat:@"%@ \"%@\"", message, self.searchBar.text];
        }
        
        self.emptyMessageLabel.text = message;
    }
    
    self.emptyView.hidden = !show;
    self.tableView.hidden = show;
}

- (void)updateNavigationTitle:(NSInteger)count {
    NSString *title = NSLocalizedString(@"Course List", nil);
    self.title = [NSString stringWithFormat:@"%@ (%zd)", title, count];
}

@end
