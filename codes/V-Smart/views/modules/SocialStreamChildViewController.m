//
//  PostStreamView.m
//  V-Smart
//
//  Created by VhaL on 3/13/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

//#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#import "SocialStreamChildViewController.h"
#import "JSONHTTPClient.h"
#import "JMSPostMessage.h"
#import "JMSEmoItem.h"
#import "EmojiViewController.h"
#import "SocialStreamGroupCell.h"
#import "JMSGroupItem.h"
#import "EmoticonViewController.h"
#import <Parse/Parse.h>

@interface SocialStreamChildViewController ()
@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, strong) IBOutlet UIButton *buttonPost;
@property (nonatomic, strong) IBOutlet UITextView *textView;
@property (nonatomic, strong) IBOutlet UIView *postView;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) IBOutlet UIButton *emoticonButton;

@property (nonatomic, strong) HMSegmentedControl *segmentedSectionControl;

@property (nonatomic, strong) UIPopoverController *popOverController;
@property (nonatomic, strong) EmojiViewController *emojiController;
@property (nonatomic, strong) EmoticonViewController *emoticonController;

@property (nonatomic, strong) JMSRecordGroupItem *groupItems;
@property (nonatomic, assign) BOOL disablePost;
@property (nonatomic, assign) int postViewHeightDiff;
@property (nonatomic, strong) JMSGroupItem *currentGroup;
@property (nonatomic, strong) JMSEmoItem *emotionItem;
@end

@implementation SocialStreamChildViewController
@synthesize disablePost, postViewHeightDiff;

-(AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

-(void)viewDidLoad{
    
    self.textView.layer.borderColor = [UIColor grayColor].CGColor;
    self.textView.layer.borderWidth = 1;
    self.textView.layer.cornerRadius = 5;
    
    self.textView.layoutManager.allowsNonContiguousLayout = NO;
    self.textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    disablePost = NO;
    [self setupWebview];
    
    [self setupShadows];
    [self setupGestures];
    [self setupEmoItems];
    [self setupGroup];
}

-(void)setupShadows{
    // drop shadow
    CALayer *layer = self.postView.layer;
    [layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [layer setShadowOpacity:0.5];
    [layer setShadowRadius:2.0];
    [layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    [layer setShadowPath:[UIBezierPath bezierPathWithRect:layer.bounds].CGPath];
}

-(void)setupGestures{
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(togglePostView:)];
    
    tapGesture.numberOfTapsRequired = 1;
    
    UISwipeGestureRecognizer *swipeDownGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(togglePostView:)];
    
    swipeDownGesture.numberOfTouchesRequired = 1;
    swipeDownGesture.direction = UISwipeGestureRecognizerDirectionDown;
    
    [self.postView addGestureRecognizer:tapGesture];
    [self.postView addGestureRecognizer:swipeDownGesture];
    
    UISwipeGestureRecognizer *swipeUpGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(togglePostView:)];
    
    swipeUpGesture.numberOfTouchesRequired = 1;
    swipeUpGesture.direction = UISwipeGestureRecognizerDirectionUp;
    
    [self.postView addGestureRecognizer:swipeUpGesture];
}

- (void) setupGroupSection {
    
    NSMutableArray *items = [[NSMutableArray alloc] initWithCapacity:self.groupItems.records.count];
    
    for (JMSGroupItem *item in self.groupItems.records) {
        NSString *label = [NSString stringWithFormat:@"%@-%@", item.name, item.section];
        [items addObject:label];
    }
    
    /*
     * LEGACY CODE
     *
    
    self.segmentedSectionControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [self.segmentedSectionControl setSectionTitles:items];
    [self.segmentedSectionControl setSelectedSegmentIndex:0];
    [self.segmentedSectionControl setBackgroundColor:UIColorFromHex(0xf5f4f4)];
    [self.segmentedSectionControl setTextColor:UIColorFromHex(0x1d6483)];
    [self.segmentedSectionControl setSelectedTextColor:UIColorFromHex(0x1d6483)];
    [self.segmentedSectionControl setSelectionIndicatorColor:UIColorFromHex(0x00a9d5)];
    [self.segmentedSectionControl setSelectionStyle:HMSegmentedControlSelectionStyleBox];
    [self.segmentedSectionControl setSelectionLocation:HMSegmentedControlSelectionLocationUp];
     */
    
    self.segmentedSectionControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    self.segmentedSectionControl.sectionTitles = items;
    self.segmentedSectionControl.selectedSegmentIndex = 0;
    self.segmentedSectionControl.selectionIndicatorHeight = 4.0f;
    self.segmentedSectionControl.backgroundColor = UIColorFromHex(0xf5f4f4);
    self.segmentedSectionControl.titleTextAttributes = @{ NSForegroundColorAttributeName : UIColorFromHex(0x1d6483),
                                                   NSFontAttributeName : FONT_NEUE_LIGHT(15) };
    self.segmentedSectionControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : UIColorFromHex(0x1d6483)};
    self.segmentedSectionControl.selectionIndicatorColor = UIColorFromHex(0x00a9d5);
    self.segmentedSectionControl.selectionStyle = HMSegmentedControlSelectionStyleBox;
    self.segmentedSectionControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationUp;
    
    
    __weak typeof(self) weakSelf = self;
    [self.segmentedSectionControl setIndexChangeBlock:^(NSInteger index) {
        weakSelf.currentGroup = [weakSelf.groupItems.records objectAtIndex:index];
        [weakSelf loadWebview];
    }];
    
    [self.view addSubview:self.segmentedSectionControl];
}

-(void)setupGroup{
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    UINib *cellNib = [UINib nibWithNibName:@"SocialStreamGroupCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"SocialStreamGroupCell"];
    
    postViewHeightDiff = 45;
    [self togglePostView:nil];
    
    /*
     NOTE:
     Get a pointer to the JSONHTTPClient headers
     set the schoolcode_base64
     */
    NSMutableDictionary *headers = [JSONHTTPClient requestHeaders];
    headers[@"code"] = [Utils getSettingsSchoolCodeBase64];
    
    NSString *urlString = [Utils buildUrl:VS_FMT(kEndPointGetGroups, [self account].user.id)];
    NSLog(@"group url string : %@", urlString);
    
    [JSONHTTPClient getJSONFromURLWithString:urlString completion:^(NSDictionary *json, JSONModelError *err) {

        if(err == nil){
            
            /*
             NOTE: we must handle the server reply even if the records are empty
                   {"_meta":{"status":"SUCCESS","count":0},"records":{}}
             */
            
            NSArray *arrayRecords = [json objectForKey:@"records"];
            
            if ([arrayRecords count] > 0) {
                
                NSMutableArray *array = [NSMutableArray arrayWithArray:arrayRecords];
                NSDictionary *dict = @{@"records": array};
                
                NSError *parseError = nil;
                
                NSData *records = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
                NSString *jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
                
                self.groupItems = [[JMSRecordGroupItem alloc] initWithString:jsonString error:&parseError];
                
                if (0 < self.groupItems.records.count){
                    self.currentGroup = [self.groupItems.records objectAtIndex:0];
                }
                
                if (self.groupItems.records.count == 1){
                    self.collectionView.hidden = YES;

                    [UIView animateWithDuration:0.3
                                          delay:0.1
                                        options: UIViewAnimationOptionCurveEaseIn
                                     animations:^{
                                         
                                         self.postView.frame = CGRectMake(self.postView.frame.origin.x,
                                                                          self.postView.frame.origin.y - self.collectionView.frame.size.height - 1,
                                                                          self.postView.frame.size.width,
                                                                          self.postView.frame.size.height);
                                         
                                         self.webView.frame = CGRectMake(self.webView.frame.origin.x,
                                                                         self.webView.frame.origin.y - self.collectionView.frame.size.height - 1,
                                                                         self.webView.frame.size.width,
                                                                         self.webView.frame.size.height + self.collectionView.frame.size.height + 1);
                                     }
                                     completion:^(BOOL finished){
                                     }];
                    
                    postViewHeightDiff = 40;

                } else {
                    [self setupGroupSection];
                }

                
                [self loadWebview];
                
                [self performSelectorOnMainThread:@selector(loadData) withObject:nil waitUntilDone:NO];
                
            }

        }
    }];
    
//    [self.rm requestSocialStreamGroups:^(NSArray *list) {
//        if (list) {
//            NSLog(@"stickers list : %@", list);
//        }
//    }];
}

-(void)setupEmoItems{
    
    self.emojiController = [[EmojiViewController alloc] init];
    self.emojiController.delegate = self;
    self.emojiController.view.hidden = NO;
    
    self.emoticonController = [[EmoticonViewController alloc] init];
    self.emoticonController.delegate = self;
    self.emoticonController.view.hidden = NO;
    
    NSString *urlEmoji = [Utils buildUrl:kEndPointGetEmojis];
    
    /*
     NOTE:
     Get a pointer to the JSONHTTPClient headers
     set the schoolcode_base64
     */
    NSMutableDictionary *headers = [JSONHTTPClient requestHeaders];
    headers[@"code"] = [Utils getSettingsSchoolCodeBase64];
    
    [JSONHTTPClient getJSONFromURLWithString:urlEmoji completion:^(NSDictionary *json, JSONModelError *err) {
        if(err == nil){
            NSArray *arrayRecords = [json objectForKey:@"records"];
            NSMutableArray *array = [NSMutableArray arrayWithArray:arrayRecords];
            NSDictionary *dict = @{@"records": array};
            
            NSError *parseError = nil;
            
            NSData *records = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
            NSString *jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
            
            self.emojiController.emoItems = [[JMSRecordEmoItem alloc] initWithString:jsonString error:&parseError];
            [self.emojiController loadData];
        }
    }];
    
    NSString *urlEmotion = [Utils buildUrl:kEndPointGetEmoticons];
    
    [JSONHTTPClient getJSONFromURLWithString:urlEmotion completion:^(NSDictionary *json, JSONModelError *err) {
        if(err == nil){
            NSArray *arrayRecords = [json objectForKey:@"records"];
            NSMutableArray *array = [NSMutableArray arrayWithArray:arrayRecords];
            NSDictionary *dict = @{@"records": array};
            
            NSError *parseError = nil;
            
            NSData *records = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
            NSString *jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
            
            self.emoticonController.emoItems = [[JMSRecordEmoItem alloc] initWithString:jsonString error:&parseError];
            [self.emoticonController loadData];
        }
    }];
    
//    [self.rm requestSocialStreamStickers:^(NSArray *list) {
//        if (list) {
//            NSLog(@"stickers list : %@", list);
//        }
//    }];
//
//    [self.rm requestSocialStreamEmojis:^(NSArray *list) {
//        if (list) {
//            NSLog(@"emoji list : %@", list);
//        }
//    }];
//
//    [self.rm requestSocialStreamEmoticons:^(NSArray *list) {
//        if (list) {
//            NSLog(@"emoticons list : %@", list);
//        }
//    }];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    if (disablePost){
//        postViewHeightDiff = 0;
//        [self togglePostView:nil];
//    }
//    else{
//        postViewHeightDiff = 45;
//        [self togglePostView:nil];
//    }
}

-(void)loadData{
    [self.collectionView reloadData];
}

-(IBAction)togglePostView:(id)sender{
    NSLog(@"%@", @"togglePostView");
    
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         int diff = postViewHeightDiff;
                         if (postViewHeightDiff == 40)
                             diff = 0;
                         
                         if (diff <= self.postView.frame.origin.y){
                             self.postView.frame = CGRectMake(self.postView.frame.origin.x,
                                                              self.postView.frame.origin.y - (self.postView.frame.size.height - postViewHeightDiff),
                                                              self.postView.frame.size.width,
                                                              self.postView.frame.size.height);
                             
                             self.webView.frame = CGRectMake(self.webView.frame.origin.x,
                                                             self.webView.frame.origin.y - (self.postView.frame.size.height - postViewHeightDiff),
                                                             self.webView.frame.size.width,
                                                             self.webView.frame.size.height + (self.postView.frame.size.height - postViewHeightDiff));
                             
                             [self.textView resignFirstResponder];
                         }
                         else{
                             self.postView.frame = CGRectMake(self.postView.frame.origin.x,
                                                              self.postView.frame.origin.y + (self.postView.frame.size.height - postViewHeightDiff),
                                                              self.postView.frame.size.width,
                                                              self.postView.frame.size.height);
                             
                             self.webView.frame = CGRectMake(self.webView.frame.origin.x,
                                                             self.webView.frame.origin.y + (self.postView.frame.size.height - postViewHeightDiff),
                                                             self.webView.frame.size.width,
                                                             self.webView.frame.size.height - (self.postView.frame.size.height - postViewHeightDiff));
                         }
                         
                     }
                     completion:^(BOOL finished){
//                         [self fixIOS7TextViewBug];
                     }];
    
}

-(void)setupWebview{
    for (id subview in self.webView.subviews)
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            ((UIScrollView *)subview).bounces = NO;
    
    [self.view addSubview:self.webView];
}

-(void)loadWebview{
//    NSString *urlString = [Utils buildUrl:VS_FMT(kEndPointSocialStream, [self account].user.id, self.currentGroup.groupId)];
    
    int userid = [self account].user.id; //user id
    int groupid = self.currentGroup.groupId; // group id
    NSString *lang = [VSmartHelpers getDeviceLocale]; // device language
    
    NSString *urlString = [Utils buildUrl:VS_FMT(kEndPointSocialStreamLocalize, userid, groupid, lang)];
    
    NSURLRequest *webRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [self.webView loadRequest:webRequest];
}

+(void)postToDefaultGroup:(NSString*)message showResult:(BOOL)showResult{
    
    /*
     NOTE:
     Get a pointer to the JSONHTTPClient headers
     set the schoolcode_base64
     */
    NSMutableDictionary *headers = [JSONHTTPClient requestHeaders];
    headers[@"code"] = [Utils getSettingsSchoolCodeBase64];
    
    NSString *urlString = [Utils buildUrl:VS_FMT(kEndPointGetGroups, [VSmart sharedInstance].account.user.id)];
    [JSONHTTPClient getJSONFromURLWithString:urlString completion:^(NSDictionary *json, JSONModelError *err) {
        
        if(err == nil){
            NSArray *arrayRecords = [json objectForKey:@"records"];
            NSMutableArray *array = [NSMutableArray arrayWithArray:arrayRecords];
            NSDictionary *dict = @{@"records": array};
            
            NSError *parseError = nil;
            
            NSData *records = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
            NSString *jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
            
            JMSRecordGroupItem *groupItems = [[JMSRecordGroupItem alloc] initWithString:jsonString error:&parseError];
            
            if (0 < groupItems.records.count){
                JMSGroupItem *currentGroup = [groupItems.records objectAtIndex:0];
                [self post:message groupId:currentGroup.groupId emoticonId:0 showResult:showResult];
            }
        }
    }];
}

+(void)post:(NSString*)message groupId:(int)groupId emoticonId:(int)emoticonId showResult:(BOOL)showResult{
    
    JMSPostMessage *postMessage = [JMSPostMessage new];
    postMessage.userId = [VSmart sharedInstance].account.user.id;
    postMessage.groupId = groupId;
    postMessage.emoticonId = emoticonId;
    postMessage.isAttached = 0;
    postMessage.message = message;
    
    NSString *postMessageAsString = [postMessage toJSONString];
    
    NSString *code = [Utils getSettingsSchoolCode];
    NSDictionary *dimensions = @{@"user": [VSmart sharedInstance].account.user.email, @"code": code};
    [PFAnalytics trackEvent:@"social" dimensions:dimensions];
    
    /*
     NOTE:
     Get a pointer to the JSONHTTPClient headers
     set the schoolcode_base64
     */
    NSMutableDictionary *headers = [JSONHTTPClient requestHeaders];
    headers[@"code"] = [Utils getSettingsSchoolCodeBase64];
    
    [JSONHTTPClient postJSONFromURLWithString:[Utils buildUrl:kEndPointPostSocialStream] bodyString:postMessageAsString completion:^(NSDictionary *json, JSONModelError *err) {
        
        NSString *socialStreamLabel = NSLocalizedString(@"Social Stream", nil);
        NSString *postSuccessful = NSLocalizedString(@"Posting successful!", nil);
        NSString *postFailed = NSLocalizedString(@"Posting failed!", nil);
        NSString *okButton = NSLocalizedString(@"OK", nil);
        
        if (err == nil){
            VLog(@"%@", json);
            
            if (showResult){
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:socialStreamLabel
                                                                  message:postSuccessful
                                                                 delegate:nil
                                                        cancelButtonTitle:okButton
                                                        otherButtonTitles:nil];
                
                [message show];
            }
        }
        else{
            VLog(@"%@", err);
            
            if (showResult){
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:socialStreamLabel
                                                                  message:postFailed
                                                                 delegate:nil
                                                        cancelButtonTitle:okButton
                                                        otherButtonTitles:nil];
                
                [message show];

            }
        }
    }];
    
}

-(IBAction)buttonPostTapped:(id)sender{
    
    if (self.postView.frame.origin.y < 0){
        [self togglePostView:nil];
        return;
    }
    
    NSString *message = [self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    int emoticonId = 0;
    if (self.emotionItem)
        emoticonId = [self.emotionItem.id intValue];
    

    [SocialStreamChildViewController post:message groupId:self.currentGroup.groupId emoticonId:emoticonId showResult:NO];
    
    self.textView.text = @"";
    [self togglePostView:nil];
    [self emoticonButtonTapped:nil];
}

-(IBAction)segmentedControlTapped:(UISegmentedControl*)sender{
    
    if (self.postView.frame.origin.y < 0){
        [self togglePostView:nil];
    }
    
    switch (sender.selectedSegmentIndex) {
        case 0:{ //emoji
            self.popOverController = [[UIPopoverController alloc] initWithContentViewController:self.emojiController];
            
            [self.popOverController setPopoverContentSize:CGSizeMake(self.emojiController.view.frame.size.width, self.emojiController.view.frame.size.height) animated:YES];
            
            self.emojiController.parent = self.popOverController;
            
            CGFloat equalSize = sender.frame.size.width/sender.numberOfSegments;
            CGFloat halfSize = equalSize/2;
            CGFloat position = (sender.selectedSegmentIndex * equalSize) + halfSize;
            
            [self.popOverController presentPopoverFromRect:
             CGRectMake(sender.frame.origin.x + position, sender.frame.origin.y + 3, 1, 1)
                                                    inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        }
            break;
            
        case 1:{ //emoticon
            self.popOverController = [[UIPopoverController alloc] initWithContentViewController:self.emoticonController];
            
            [self.popOverController setPopoverContentSize:CGSizeMake(self.emoticonController.view.frame.size.width, self.emoticonController.view.frame.size.height) animated:YES];
            
            self.emoticonController.parent = self.popOverController;
            
            CGFloat equalSize = sender.frame.size.width/sender.numberOfSegments;
            CGFloat halfSize = equalSize/2;
            CGFloat position = (sender.selectedSegmentIndex * equalSize) + halfSize;
            
            [self.popOverController presentPopoverFromRect:
             CGRectMake(sender.frame.origin.x + position, sender.frame.origin.y + 3, 1, 1)
                                                    inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        }
            break;
        case 2:{ //attachment
            
        }
            break;
            
        default:
            break;
    }
}

-(void)emojiTapped:(JMSEmoItem*)emoji{
    self.textView.text = [NSString stringWithFormat:@"%@%@", self.textView.text, emoji.chars];
}

-(void)emoticonTapped:(JMSEmoItem*)emoticon{
    [self.emoticonButton setTitle:[NSString stringWithFormat:@"is feeling - %@", emoticon.name] forState:UIControlStateNormal];
    self.emotionItem = emoticon;
}

-(IBAction)emoticonButtonTapped:(id)sender{
    [self.emoticonButton setTitle:[NSString stringWithFormat:@""] forState:UIControlStateNormal];
    self.emotionItem = nil;
}

-(void)fixIOS7TextViewBug{
    //the bug is: if user trigger textview editing, the cursor starts at the bottom of the textview frame
    
    UITextView *textView = self.textView;
    
    CGRect line = [textView caretRectForPosition:
                   textView.selectedTextRange.start];
    CGFloat overflow = line.origin.y + line.size.height
    - ( textView.contentOffset.y + textView.bounds.size.height
       - textView.contentInset.bottom - textView.contentInset.top );
    if ( overflow > 0 ) {
        // We are at the bottom of the visible text and introduced a line feed, scroll down (iOS 7 does not do it)
        // Scroll caret to visible area
        CGPoint offset = textView.contentOffset;
        offset.y += overflow + 7; // leave 7 pixels margin
        // Cannot animate with setContentOffset:animated: or caret will not appear
        [UIView animateWithDuration:.2 animations:^{
            [textView setContentOffset:offset];
        }];
    }
}

#pragma mark - UITextView

- (BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    return NO;
}

#pragma mark - UIWebView Delegate

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//    [SVProgressHUD dismiss];
    VLog(@"Finished Loading View");
}

-(void)webView:(UIWebView *)theWebView didFailLoadWithError:(NSError *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//    [SVProgressHUD dismiss];
	VLog(@"Error: %@", [error localizedDescription]);
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//    [SVProgressHUD showWithStatus:MSG_LOADING_MODULE maskType:SVProgressHUDMaskTypeGradient];
}

- (BOOL)webView:(UIWebView *) theWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

    NSString *url = [[request URL] absoluteString];
    
    if ([[[request URL] scheme] isEqualToString:@"http"] || [[[request URL] scheme] isEqualToString:@"https"]){

        NSRange range = [url rangeOfString:APP_SERVER options:NSCaseInsensitiveSearch];
        if(range.location == NSNotFound) {
            VLog(@"APP_SERVER: %@", APP_SERVER);
            [[UIApplication sharedApplication] openURL:[request URL]];
            return NO;
        }
	}
    
    return YES;
}

-(void)dealloc{
    [self.webView stopLoading];
 	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.webView.delegate = nil;
}

-(CGSize)getRectFromTagEntry:(NSIndexPath *)indexPath{
    
    JMSGroupItem *item = (JMSGroupItem*)[self.groupItems.records objectAtIndex:indexPath.row];
    
    NSString *label = [NSString stringWithFormat:@"%@-%@", item.name, item.section];
    CGSize stringSize = [label sizeWithDefaultFont:[UIFont systemFontOfSize:16]];
    
    return stringSize;
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if ([collectionView isEqual:self.collectionView]){
        return self.groupItems.records.count;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    if ([collectionView isEqual:self.collectionView]){
        return 1;
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([collectionView isEqual:self.collectionView]){
        SocialStreamGroupCell *cell = (SocialStreamGroupCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:@"SocialStreamGroupCell" forIndexPath:indexPath];
        
        JMSEmoItem *item = [self.groupItems.records objectAtIndex:indexPath.row];
        [cell initializeCell:self];
        cell.groupName.text = item.name;
        return cell;
    }
    
    return nil;
}

-(void) collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    //change color when tapped
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor greenColor];
}

//-(void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath{
//    //change back on touch up
//    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//    cell.backgroundColor = [UIColor cyanColor];
//}


#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    CGSize retval = CGSizeMake(200, 30);
//    return retval;
    
    CGSize size = [self getRectFromTagEntry:indexPath];
    CGSize sizeNew = CGSizeMake(size.width + 12, size.height + 10);
    
    NSLog(@"cell width:%f height:%f", sizeNew.width, sizeNew.height);
    
    return sizeNew;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([collectionView isEqual:self.collectionView]){
        
        VLog(@"cell selected");
        self.currentGroup = [self.groupItems.records objectAtIndex:indexPath.row];
        [self loadWebview];
    }
}

@end
