//
//  LessonTemplateCollectionViewCell.h
//  V-Smart
//
//  Created by Julius Abarra on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonTemplateCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgTemplate;
@property (strong, nonatomic) IBOutlet UILabel *lblTemplateName;

@end
