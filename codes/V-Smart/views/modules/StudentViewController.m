//
//  StudentViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 09/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "StudentViewController.h"
#import "StudentTableViewCell.h"
#import "CourseDataManager.h"
#import "StudentProfileTableViewController.h"

#define ResultsTableView self.searchResultsTableViewController.tableView

@interface StudentViewController () <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, UISearchControllerDelegate> {
    
    NSManagedObject *mo_selected;
    UITableView *tableView_active;
}

@property (strong, nonatomic) CourseDataManager *cm;
@property (strong, nonatomic) UIRefreshControl *tableRefreshControl;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) UITableViewController *searchResultsTableViewController;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *searchBarView;

@property (strong, nonatomic) IBOutlet UILabel *lblCourseName;
@property (strong, nonatomic) IBOutlet UILabel *lblSchedule;
@property (strong, nonatomic) IBOutlet UILabel *lblSection;
@property (strong, nonatomic) IBOutlet UILabel *lblCourseCode;
@property (strong, nonatomic) IBOutlet UILabel *lblRoom;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutlet UILabel *lblActCourseName;
@property (strong, nonatomic) IBOutlet UILabel *lblActSchedule;
@property (strong, nonatomic) IBOutlet UILabel *lblActSection;
@property (strong, nonatomic) IBOutlet UILabel *lblActCourseCode;
@property (strong, nonatomic) IBOutlet UILabel *lblActRoom;
@property (strong, nonatomic) IBOutlet UILabel *lblActDescription;

@property (assign, nonatomic) BOOL isAscending;
@property (strong, nonatomic) NSArray *results;


@end

@implementation StudentViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Localized strings
    self.lblCourseName.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Course Name", nil)];
    self.lblSchedule.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Schedule", nil)];
    self.lblSection.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Section", nil)];
    self.lblCourseCode.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Course Code", nil)];
    self.lblRoom.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Room", nil)];
    self.lblDescription.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Description", nil)];
    
    // Set up course information
    if (self.courseDictionary != nil) {
        self.lblActCourseName.text = self.courseDictionary[@"course_name"];
        self.lblActSchedule.text = self.courseDictionary[@"schedule"];
        self.lblActSection.text = self.courseDictionary[@"section"];
        self.lblActCourseCode.text = self.courseDictionary[@"course_code"];
        self.lblActRoom.text = self.courseDictionary[@"room"];
        self.lblActDescription.text = self.courseDictionary[@"course_desc"];
    }
    
    // Set protocols for UITableView
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Set initial result to descending order
    self.isAscending = NO;
    
    // Use singleton class as data manager
    self.cm = [CourseDataManager sharedInstance];
    self.managedObjectContext = self.cm.mainContext;
    
    // Implement a selector for refresh action
    SEL refreshAction = @selector(listAllStudents);
    
    // Implement refresh control
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    // Invoke private methods
    [self listAllStudents];         // Request for student list from API
    [self setupRightBarButton];     // Add sort button to navigation bar
    [self setupSearchCapabilities]; // Add search bar to view
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Set navigation bar title
    self.title = NSLocalizedString(@"Student List", nil);
    
    // Decorate navigation bar
    UIColor *navColor = UIColorFromHex(0x2b5797);
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    self.navigationController.navigationBar.barTintColor = navColor;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLayoutSubviews {
    [self.searchController.searchBar sizeToFit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - List Student

- (void)listAllStudents {
    NSString *csid = self.courseDictionary[@"cs_id"];
    __weak typeof(self) wo = self;
    
    [self.cm requestStudentListForCourseWithID:csid doneBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo.tableRefreshControl endRefreshing];
                [wo reloadFetchedResultsController];
                [wo.tableView reloadData];
            });
        }
    }];
}

#pragma mark - Sort Button

- (void)setupRightBarButton {
    UIImage *sortImage = [UIImage imageNamed:@"sort_258x258"];
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    sortButton.frame = CGRectMake(0, 0, 44, 44);
    sortButton.showsTouchWhenHighlighted = YES;
    
    [sortButton setImage:sortImage forState:UIControlStateNormal];
    [sortButton setImage:sortImage forState:UIControlStateHighlighted];
    [sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
}

- (IBAction)sortButtonAction:(id)sender {
    self.isAscending = (self.isAscending) ? NO : YES;
    [self reloadFetchedResultsController];
}

#pragma mark - Search Bar

- (void)setupSearchCapabilities {
    //self.results = [[NSMutableArray alloc] init];
    //
    //UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TestPlayerStoryBoard" bundle:nil];
    //UITableViewController *srtvc = [sb instantiateViewControllerWithIdentifier:@"search_student_tvc"];
    //
    //srtvc.tableView.dataSource = self;
    //srtvc.tableView.delegate = self;
    //
    //self.searchResultsTableViewController = srtvc;
    //
    //// Init a search controller with its table view controller for results
    //self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsTableViewController];
    //self.searchController.searchResultsUpdater = self;
    //self.searchController.delegate = self;
    //
    //// Make an appropriate size for search bar and add it as a header view for initial table view
    ////[self.searchController.searchBar sizeToFit];
    ////self.tableView.tableHeaderView = self.searchController.searchBar;
    //
    //// Make an appropriate size for search bar and add it to view
    ////[self.searchController.searchBar sizeToFit];
    //[self.searchBarView addSubview:self.searchController.searchBar];
    //
    //// Enable presentation context
    //self.definesPresentationContext = YES;
    
    // REFACTOR
    // jca-01-05-2016
    // Reusing table view for presenting search results
    
    // Allocate and initialize results
    self.results = [[NSMutableArray alloc] init];
    
    // Init a search controller reusing the current table view controller for results
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    
    // Do not dim and hide the navigation during presentation
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = YES;
    
    // Make an appropriate size for search bar and add it as a header view for initial table view
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // Enable presentation context
    self.definesPresentationContext = YES;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = 1;
    
    if ([tableView isEqual:ResultsTableView]) {
        return count;
    }
    else {
        return [[self.fetchedResultsController sections] count];
    }
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    
    if ([tableView isEqual:ResultsTableView]) {
        if (self.results) {
            return self.results.count;
        }
        else {
            return count;
        }
    }
    else {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        count = [sectionInfo numberOfObjects];
    }
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView_active = tableView;
    
    StudentTableViewCell *studentContentCell = [tableView dequeueReusableCellWithIdentifier:@"studentCellIdentifier" forIndexPath:indexPath];
    
    if ([tableView isEqual:ResultsTableView]) {
        [self configureFilteredCell:studentContentCell indexPath:indexPath];
    }
    
    if (![tableView isEqual:ResultsTableView]) {
        [self configureCell:studentContentCell atIndexPath:indexPath];
    }
    
    return studentContentCell;
}

- (void)configureFilteredCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = self.results[indexPath.row];
    StudentTableViewCell *studentContentCell = (StudentTableViewCell *)cell;
    [self configureCell:studentContentCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    StudentTableViewCell *studentContentCell = (StudentTableViewCell *)cell;
    [self configureCell:studentContentCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(StudentTableViewCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    NSString *first_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"]];
    NSString *last_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"]];
    NSString *contact_number = [NSString stringWithFormat:@"%@", [mo valueForKey:@"contact_number"]];
    NSString *email = [NSString stringWithFormat:@"%@", [mo valueForKey:@"email"]];
    
    if ([contact_number isEqualToString:@""]) {
        contact_number = @"---";
    }
    
    cell.lblStudentName.text = [NSString stringWithFormat:@"%@ %@", first_name, last_name];
    cell.lblContactNumber.text = contact_number;
    cell.lblEmailAddress.text = email;
    cell.imgAvatar.layer.cornerRadius = 24.0f;
    
    //NSData *thumbnail_data = [mo valueForKey:@"thumbnail"];
    //
    //if ([UIImage imageWithData:thumbnail_data] == nil) {
    //    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    //    dispatch_async(queue, ^{
    //        [self.cm downloadImageStudentList:mo binaryBlock:^(NSData *binary) {
    //            if (binary) {
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    cell.imgAvatar.image = [UIImage imageWithData:binary];
    //                });
    //            }
    //        }];
    //    });
    //}
    //else {
    //    cell.imgAvatar.image = [UIImage imageWithData:thumbnail_data];
    //}
    
    // REFACTOR
    // jca-01-06-2016
    // Save profile picture to core data before rendering it to profile view
    // Fix disappearing of profile picture
    
    NSData *thumbnail = [mo valueForKey:@"thumbnail"];
    UIImage *profileImage = [UIImage imageWithData:thumbnail];
    
    if (profileImage == nil) {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        dispatch_async(queue, ^{
            [self.cm downloadProfileImage:mo binaryBlock:^(NSData *binary) {
            }];
        });
    }
    else {
        cell.imgAvatar.image = profileImage;
    }
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = nil;
    
    if ([tableView isEqual:ResultsTableView]) {
        mo = self.results[indexPath.row];
    }
    
    if (![tableView isEqual:ResultsTableView]) {
        mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    mo_selected = mo;
    tableView_active = tableView;
    
    NSString *studentid = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"id"]];
    __weak typeof(self) wo = self;
    
    [self.cm requestStudentProfileWithID:studentid doneBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo performSegueWithIdentifier:@"showStudentProfile" sender:nil];
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
            });
        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSString *studentid = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"id"]];
    
    if ([segue.identifier isEqualToString:@"showStudentProfile"]) {
        StudentProfileTableViewController *sf = (StudentProfileTableViewController *)segue.destinationViewController;
        sf.studentid = studentid;
    }
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kStudentEntity];
    [fetchRequest setFetchBatchSize:10];
    
    // REFACTOR
    // jca-01-05-2016
    // Reusing table view for presenting search results
    
    NSPredicate *predicate;
    
    if (self.searchController.active == NO) {
        predicate = [self predicateForKeyPath:@"first_name" value:@"*"];
    }
    else {
        predicate = [self predicateForKeyPathContains:@"search_string" value:self.searchController.searchBar.text];
    }
    
    fetchRequest.predicate = predicate;
    
    NSSortDescriptor *first_name = [NSSortDescriptor sortDescriptorWithKey:@"first_name" ascending:self.isAscending];
    [fetchRequest setSortDescriptors:@[first_name]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - Search Results Updating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    //UISearchBar *searchBar = searchController.searchBar;
    //
    //if (searchBar.text.length > 0) {
    //    NSString *text = searchBar.text;
    //    NSArray *fetchedObjects = self.fetchedResultsController.fetchedObjects;
    //
    //    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(NSManagedObject *mo, NSDictionary *bindings) {
    //        NSString *object = [NSString stringWithFormat:@"%@", [mo valueForKey:@"search_string"]];
    //        NSStringCompareOptions options = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;
    //        NSRange range = [object rangeOfString:text options:options];
    //        return range.location != NSNotFound;
    //    }];
    //
    //    // Set up results
    //    self.results = [fetchedObjects filteredArrayUsingPredicate:predicate];;
    //
    //    // Reload search table view
    //    [self.searchResultsTableViewController.tableView reloadData];
    //}
    //
    ////    // Reactivate sort button after cancelling typing at search bar
    ////    // Statement to make previous sorted list remain unchanged
    ////    self.isAscending = (self.isAscending) ? NO : YES;
    ////    [self sortButtonAction:self];
    
    // REFACTOR
    // jca-01-05-2016
    // Reusing table view for presenting search results
    
    [self reloadFetchedResultsController];
}

#pragma mark - Reloading of Results

- (void)reloadSearchResults {
    NSArray *items = [NSArray arrayWithArray:self.results];
    
    // Edit the sort key as appropriate
    NSSortDescriptor *first_name = [NSSortDescriptor sortDescriptorWithKey:@"first_name" ascending:self.isAscending];
    NSArray *sorted = [items sortedArrayUsingDescriptors:@[first_name]];
    
    // Set up results
    self.results = [NSArray arrayWithArray:sorted];
    
    // Reload search table view
    [tableView_active reloadData];
}

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

@end
