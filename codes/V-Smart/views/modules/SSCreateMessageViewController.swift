//
//  SSCreateMessageViewController.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 29/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


enum shownCV {
    case emoticonCV
    case stickerCV
    case noCV
}

private struct cvHandler {
    var displayedCV : shownCV
    var selectedIndex : Int = -2 // -2 == no , -1 == emotic, 0++ sticker
}

class SSCreateMessageViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var delegate:Dimmable?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet weak var stickerView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var emoticonButton: UIButton!
    @IBOutlet weak var stickerCollectionView: UICollectionView!
    
    @IBOutlet weak var isFeelingView: UIView!
    @IBOutlet weak var isFeelingImageView: UIImageView!
    @IBOutlet weak var isFeelingLabel: UILabel!
    @IBOutlet weak var isFeelingCloseButton: UIButton!
    
    var editMessageDict:[String:String]?
    
    var stickers: [[String:AnyObject]] = [[String:AnyObject]]()
    var sticker_categories: [[String:AnyObject]] = [[String:AnyObject]]()
    var emoticons: [[String:AnyObject]] = [[String:AnyObject]]()
    
    var hasFeeling = false
    var selectedEmoticon = [String:AnyObject]()
    var selectedEmoticonID = "";
    
    fileprivate lazy var ssdm: SocialStreamDataManager = {
        let dataManager = SocialStreamDataManager.sharedInstance
        return dataManager
    }()
    
    fileprivate var selectedCV : cvHandler = cvHandler(displayedCV: .noCV, selectedIndex: -2)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.text = NSLocalizedString("My Post", comment: "")
        
        self.setupUI()
        
        self.closeButton.addTarget(self, action: #selector(self.closeButtonAction(_:)), for: .touchUpInside)
        self.emoticonButton.addTarget(self, action: #selector(self.emoticonButtonAction(_:)), for: .touchUpInside)
        self.isFeelingCloseButton.addTarget(self, action: #selector(self.isFeelingCloseButtonAction(_:)), for: .touchUpInside)
        self.sendMessageButton.addTarget(self, action: #selector(self.sendMessageButtonAction(_:)), for: .touchUpInside)
        
        let nibName = UINib(nibName: "ImageWithButtonCVCell", bundle:nil)
        collectionView.register(nibName, forCellWithReuseIdentifier: "STICKER_CATEGORY_CELL_RID")
        stickerCollectionView.register(nibName, forCellWithReuseIdentifier: "STICKER_CELL_RID")
        
        self.loadStickers()
        self.loadEmoticons()
        self.editMessageAction()
    }
    
    func editMessageAction() {
        guard let editDict = self.editMessageDict else {
            return
        }
        
        let message = editDict["message"]
        
        let emoticon_id = editDict["emoticon_id"]
        
        let hasEmoticon = emoticon_id?.characters.count > 0
        if hasEmoticon {
            self.hasFeeling = true
            let icon_text: String = self.ssdm.stringValue(editDict["icon_text"] as AnyObject?)
            let icon_url: String = self.ssdm.stringValue(editDict["icon_url"] as AnyObject?)
            let homeUrl: String = self.ssdm.retrieveVSmartBaseURL()
            
            self.selectedEmoticonID = emoticon_id!
            
            let entireURL = (icon_url.contains(homeUrl)) ? icon_url : "http://\(homeUrl)\(icon_url)"
            
            self.getDataFromUrl(URL(string: entireURL)!) { (data, response, error) in
                guard let data = data , error == nil else { return }
                DispatchQueue.main.async(execute: {
                    self.isFeelingImageView.image = UIImage(data: data)
                })
            }
            
            DispatchQueue.main.async(execute: {
                self.isFeelingLabel.text = icon_text
            })
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.isFeelingView.isHidden = (hasEmoticon) ? false : true
        })
        
        DispatchQueue.main.async(execute: {
            self.messageTextView.text = message
        })
        
    }
    
    func setupUI() {
        self.delegate?.dimIn()
        
        self.messageTextView.placeholder = NSLocalizedString("Enter your message here...", comment: "")
        
        self.stickerCollectionView.isHidden = true
        
        self.isFeelingView.layer.masksToBounds = true
        self.isFeelingView.isHidden = true
        
        messageView.layer.cornerRadius = 10
        messageView.layer.borderColor = UIColor.black.cgColor
        messageView.layer.borderWidth = 0.25
        messageView.layer.masksToBounds = true
        
        stickerCollectionView.layer.cornerRadius = 10
        stickerCollectionView.layer.borderColor = UIColor.black.cgColor
        stickerCollectionView.layer.borderWidth = 0.25
        stickerCollectionView.layer.masksToBounds = true
        
        stickerView.clipsToBounds = true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    func isFeelingCloseButtonAction(_ b: UIButton) {
        self.hasFeeling = false
        UIView.animate(withDuration: 0.3, animations: {
            self.isFeelingView.isHidden = true
        })
    }
    
    func closeButtonAction(_ b: UIButton) {
        self.delegate?.dimOut()
        self.dismiss(animated: true, completion: nil)
    }
    
    func emoticonButtonAction(_ b: UIButton) {
        let isHidden = self.stickerCVAnimate(index: -1)
        
        if isHidden == false {
            self.selectedCV.displayedCV = shownCV.emoticonCV
            self.stickerCollectionView.reloadData()
        }
    }
    
    func sendMessageButtonAction(_ b: UIButton) {
        let message: String! = self.messageTextView.text
        let arrayOfBadwords = self.ssdm.getAllBadwords(inMessage: message!)
        
        guard message?.characters.count > 0 else {
            displayAlert(withTitle: "", withMessage: NSLocalizedString("Cannot post empty message", comment: ""))
            return
        }
        
        if arrayOfBadwords?.count > 0 {
            // HAS BADWORD DO NOT POST
            //let stringOfBadwords = arrayOfBadwords.joinWithSeparator(", ") // "1, 2, 3"
            //var message = stringOfBadwords + " is a bad word!!!"
            
            //if arrayOfBadwords.count > 1 {
            //    message = stringOfBadwords + " are bad words!!!"
            //}
            
            let message = NSLocalizedString("Oops! Your post contains vulgar words. V-Smart does not tolerate this.", comment: "")
            
            displayAlert(withTitle: NSLocalizedString("Invalid Entry", comment: ""), withMessage: message)
        } else {
            
            
            var messageDict = [String:AnyObject]()
            
            var emoticon_id: String! = "(null)"
            if hasFeeling {
                emoticon_id = self.selectedEmoticonID//self.ssdm.stringValue(self.selectedEmoticon["id"])
            }
            
            let messageType = self.messageType(forMessage: message)
            
            let group_id = self.ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID")//2
            let section_id = self.ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_SECTION_ID")//2
            messageDict = ["group_id":section_id!,
                           "sub_group_id":group_id!,
                           "emoticon_id":emoticon_id as AnyObject,
                           "is_message": messageType as AnyObject,
                           "message":message as AnyObject]
            
            if let editDict = self.editMessageDict {
                let message_id = editDict["message_id"]
                let user_id = editDict["user_id"]
                messageDict["user_id"] = user_id as AnyObject?
                
                self.ssdm.editMessage(withID: message_id!, withData: messageDict, handler: { (done, error) in
                    if error == nil {
                        DispatchQueue.main.async(execute: {
                            self.closeButtonAction(self.closeButton)
                        });
                    } else {
                        self.displayAlert(withTitle: "", withMessage: error!)
                    }
                    
                    self.displayMessageForSocketStatus()
                })
            } else {
                self.ssdm.postMessage(withData: messageDict, handler: { (done, error) in
                    if error == nil {
                        DispatchQueue.main.async(execute: {
                            self.closeButtonAction(self.closeButton)
                        });
                    } else {
                        self.displayAlert(withTitle: "", withMessage: error!)
                    }
                    
                    self.displayMessageForSocketStatus()
                })
            }
        }
    }
    
    func displayMessageForSocketStatus() {
        guard let socketStatus = self.ssdm.fetchUserDefaultsObject(forKey: "SS_NOTIFICATION_SOCKET_STATUS") as? String else {
            return
        }
        
        if socketStatus == "0" {
            self.view.makeToast(message: "Socket server is offline")
            
            let group_id = self.ssdm.stringValue(self.ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID"))//2
            let section_id = self.ssdm.stringValue(self.ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_SECTION_ID"))//2
            
            self.ssdm.requestUserMessages(forGroup: group_id!, inSection: section_id!, handler: { (doneBlock) in
                
            })
        }
    }
    
    func messageType(forMessage message: String?) -> Int {
        
        var type = 1
        
        if message != nil && message?.characters.count > 1 {
            let tokens = message?.components(separatedBy: " ")
            for w in tokens! {
                if w.hasPrefix("http://") || w.hasPrefix("https://") || w.hasPrefix("www.") || w.hasPrefix(".com") {
//                    if ((NSURL(string: w)?.host) != nil) {
                        type = 2
//                    }
                }
            }
        }
        
        return type
    }

    
    
    
    func displayAlert(withTitle title: String, withMessage message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Close", comment: ""), style: .cancel, handler: nil))
        
        DispatchQueue.main.async { 
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func loadStickers() {
        self.ssdm.requestSocialStreamStickers { (listBlock) in
            if listBlock != nil {
                self.sticker_categories = listBlock!
                DispatchQueue.main.async(execute: {
                    self.collectionView.reloadData()
                })
            }
        }
    }
    
    func loadEmoticons() {
        self.ssdm.requestSocialStreamEmoticons { (listBlock) in
            if listBlock != nil {
                self.emoticons = listBlock!
            }
        }
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        
        if collectionView == stickerCollectionView {
            if self.selectedCV.displayedCV == shownCV.emoticonCV { // emoticons are shown
                count = self.emoticons.count
            }
            
            if self.selectedCV.displayedCV == shownCV.stickerCV { // emoticons are shown
                count = self.stickers.count
            }
        }
        
        if collectionView == self.collectionView {
            count = self.sticker_categories.count
        }
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionView {
            let ibCell : ImageWithButtonCVCell = collectionView .dequeueReusableCell(withReuseIdentifier: "STICKER_CATEGORY_CELL_RID", for: indexPath) as! ImageWithButtonCVCell
            ibCell.button.isHidden = true
            ibCell.backgroundColor = UIColor.clear
            
            let category = self.sticker_categories[(indexPath as NSIndexPath).row]
            
            let cover_url = category["cover_url"] as! String
            
            guard let url = URL(string: cover_url) else {
                return ibCell
            }
            
            ibCell.tag = (indexPath as NSIndexPath).row
            ibCell.imageView.sd_setImage(with: url)
//            self.getDataFromUrl(url) { (data, response, error) in
//                guard let data = data where error == nil else { return }
//                if ibCell.tag == indexPath.row {
//                    dispatch_async(dispatch_get_main_queue(), {
//                        ibCell.imageView.image = UIImage(data: data)
//                    })
//                }
//            }
            
            return ibCell
        } else {
            let ibCell : ImageWithButtonCVCell = collectionView .dequeueReusableCell(withReuseIdentifier: "STICKER_CELL_RID", for: indexPath) as! ImageWithButtonCVCell
            ibCell.button.isHidden = true
            ibCell.backgroundColor = UIColor.clear
            
            var urlString = "";
            
            if self.selectedCV.displayedCV == shownCV.emoticonCV {
                let emoticon = self.emoticons[(indexPath as NSIndexPath).row]
                let icon_url = emoticon["icon_url"] as! String
                urlString = icon_url
            } else if self.selectedCV.displayedCV == shownCV.stickerCV {
                let stickers = self.stickers[(indexPath as NSIndexPath).row]
                let homeUrl = self.ssdm.retrieveVSmartBaseURL()
                let sticker_url = stickers["sticker_url"] as! String
                let homeStickerUrl = "http://\(homeUrl)\(sticker_url)"
                urlString = homeStickerUrl
            }
            
            guard let url = URL(string: urlString) else {
                return ibCell
            }
            
            ibCell.imageView.image = nil
            
            ibCell.tag = (indexPath as NSIndexPath).row
            ibCell.imageView.sd_setImage(with: url)
//            self.getDataFromUrl(url) { (data, response, error) in
//                guard let data = data where error == nil else { return }
//                if ibCell.tag == indexPath.row {
//                    dispatch_async(dispatch_get_main_queue(), {
//                        ibCell.imageView.image = UIImage(data: data)
//                    })
//                }
//            }
            
            return ibCell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionView {
            
            let indexRow = (indexPath as NSIndexPath).row
            
            
            let isHidden = self.stickerCVAnimate(index: indexRow)
            
            if isHidden == false {
                let category = self.sticker_categories[indexRow]
                
                let stickers = category["stickers"] as! [[String:AnyObject]]
                self.stickers = stickers
                self.selectedCV.displayedCV = shownCV.stickerCV
                self.stickerCollectionView.reloadData()
            }
        }
        
        if collectionView == self.stickerCollectionView {
            
            if self.selectedCV.displayedCV == shownCV.emoticonCV {
                
                self.hasFeeling = true
                
                let emoticon = self.emoticons[(indexPath as NSIndexPath).row]
                self.selectedEmoticonID = self.ssdm.stringValue(emoticon["id"])
                let icon_text: String! = self.ssdm.stringValue(emoticon["icon_text"])
                
                let ibCell = self.stickerCollectionView.cellForItem(at: indexPath) as! ImageWithButtonCVCell
                
                DispatchQueue.main.async(execute: { 
                    self.isFeelingImageView.image = ibCell.imageView.image
                    self.isFeelingLabel.text = icon_text
                })
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.isFeelingView.isHidden = false
                })
                print(emoticon)
            }
            
            if self.selectedCV.displayedCV == shownCV.stickerCV {
                let sticker = self.stickers[(indexPath as NSIndexPath).row]
                let stickerString: String! = self.ssdm.stringValue(sticker["sticker"])
                
                DispatchQueue.main.async(execute: { 
                    self.closeButtonAction(self.closeButton)
                })
                
                self.postStickerMessage(withData: stickerString!)
            }
        }
    }
    
    func postStickerMessage(withData stickerData: String) {
        var messageDict = [String:AnyObject]()
        
        let group_id = self.ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID")//2
        let section_id = self.ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_SECTION_ID")//2
        messageDict = ["group_id":section_id!,
                       "sub_group_id":group_id!,
                       "emoticon_id":0 as AnyObject,
                       "is_message": 0 as AnyObject,
                       "message":stickerData as AnyObject]
        
        if let editDict = self.editMessageDict {
            let message_id = editDict["message_id"]
            let user_id = editDict["user_id"]
            messageDict["user_id"] = user_id as AnyObject?
            
            self.ssdm.editMessage(withID: message_id!, withData: messageDict, handler: { (done, error) in
                if error == nil {
                    DispatchQueue.main.async(execute: {
                        self.closeButtonAction(self.closeButton)
                    });
                } else {
                    self.displayAlert(withTitle: "", withMessage: error!)
                }
                
                self.displayMessageForSocketStatus()
            })
        } else {
            self.ssdm.postMessage(withData: messageDict, handler: { (done, error) in
                if error == nil {
                    DispatchQueue.main.async(execute: {
                        self.closeButtonAction(self.closeButton)
                    });
                } else {
                    self.displayAlert(withTitle: "", withMessage: error!)
                }
                
                self.displayMessageForSocketStatus()
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.stickerCollectionView {
            return CGSize(width: 80, height: 80)
        } else {
            return CGSize(width: 50, height: 50)
        }
    }
    
    func stickerCVAnimate(index: Int) -> Bool {
        let indexRow = index
        
        var isHidden = false
        if self.selectedCV.selectedIndex != indexRow {
            self.selectedCV.selectedIndex = indexRow
            isHidden = false
        } else {
            self.selectedCV.selectedIndex = -2
            self.selectedCV.displayedCV = shownCV.noCV
            isHidden = true
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.stickerCollectionView.isHidden = isHidden
        })
        
        return isHidden
    }
    
    
    func getDataFromUrl(_ url:URL, completion: @escaping ((_ data: Data?, _ response: URLResponse?, _ error: Error? ) -> Void)) {
        
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            completion(data, response, error)
            }) .resume()
    }
    
    
    
    

}

enum Direction { case `in`, out }

protocol Dimmable {
    func dimIn()
    func dimOut()
    
    var dimView: UIView { get set }
}

extension Dimmable where Self: UIViewController {
    
    func dim(_ direction: Direction, color: UIColor = UIColor.black, alpha: CGFloat = 0.0, speed: Double = 0.0) {
        let entireWindow = self.navigationController!.view //(self.navigationController?.view != nil) ? self.view : self.navigationController!.view
        
        switch direction {
        case .in:
            
            // Create and add a dim view
            self.dimView.frame = (entireWindow!.frame)
            self.dimView.backgroundColor = color
            self.dimView.alpha = 0.0
            entireWindow!.addSubview(dimView)
            
            // Deal with Auto Layout
            self.dimView.translatesAutoresizingMaskIntoConstraints = false
            entireWindow!.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[dimView]|", options: [], metrics: nil, views: ["dimView": dimView]))
            entireWindow!.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[dimView]|", options: [], metrics: nil, views: ["dimView": dimView]))
            
            // Animate alpha (the actual "dimming" effect)
            UIView.animate(withDuration: speed, animations: { () -> Void in
                self.dimView.alpha = alpha
            }) 
            
        case .out:
            UIView.animate(withDuration: speed, animations: { () -> Void in
                self.dimView.alpha = alpha
                }, completion: { (complete) -> Void in
                    self.dimView.removeFromSuperview()
            })
        }
    }
    
    func dimIn() {
        self.dim(.in, alpha: 0.5, speed: 0.5)
    }
    
    func dimOut() {
        self.dim(.out, speed: 0.5)
    }
    
    
}
