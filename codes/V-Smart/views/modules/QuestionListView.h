//
//  QuestionListView.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/16/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuestionListViewDelegate <NSObject>
@required
- (void)didFinishSelectingQuestionIndex:(NSString *)indexString;
@end

@interface QuestionListView : UIViewController

@property (nonatomic, strong) NSString *quizid;

@property (nonatomic, weak) id <QuestionListViewDelegate> delegate;
@end
