//
//  GradeBookDataManager.m
//  V-Smart
//
//  Created by Carmelito Bayarcal on 27/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "GradeBookDataManager.h"

#import "Utils.h"
#import "AccountInfo.h"
#import "VSmartValues.h"
#import "NSDate+Helper.h"


#import <MobileCoreServices/MobileCoreServices.h>

static NSString *storeFilename = @"gradebookdata.sqlite";

@interface GradeBookDataManager() <NSURLSessionTaskDelegate>

#pragma mark - PROPERTIES

@property (nonatomic, readwrite) NSManagedObjectContext *masterContext;
@property (nonatomic, readwrite) NSManagedObjectContext *mainContext;
@property (nonatomic, readwrite) NSManagedObjectContext *workerContext;

@property (nonatomic, readwrite) NSManagedObjectModel *model;
@property (nonatomic, readwrite) NSPersistentStore *store;
@property (nonatomic, readwrite) NSPersistentStoreCoordinator *coordinator;
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSDateFormatter *formatter;

//@property (nonatomic, readwrite) TestGuruProgressBlock progressBlock;
//@property (nonatomic, readwrite) TestGuruDoneProgressBlock doneProgressBlock;


@property (strong, nonatomic) NSUserDefaults *userDefaults;

@end

@implementation GradeBookDataManager

#pragma mark - SINGLETON

+ (instancetype)sharedInstance {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    static GradeBookDataManager *singleton = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        singleton = [[self alloc] init];
    });
    return singleton;
}

#pragma mark - SETUP

- (id)init {
    NSLog(@"Running %s", __PRETTY_FUNCTION__);
    
    self = [super init];
    if (!self) { return nil; }
    
    // Managed Object Model
    self.model = [self modelFromFrameWork:@"GradeBookModel"];
    // Persistent Store Coordinator
    self.coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    
    BOOL compatible = [self isCompatibleWithCoreDataMetadata:[self storeURL]];
    if (!compatible) {
        self.store = nil;
    }
    
    // CORE DATA 3-LAYER STACK for MASTER, MAIN, WORKER
    [self initializeManagedObjectsWithCoordinator:self.coordinator];
    
    //SESSION MANAGER
    self.session = [NSURLSession sharedSession];
    
    //DATE FORMATTER
    self.formatter = [[NSDateFormatter alloc] init];
    
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    
    return self;
}

- (void)initializeManagedObjectsWithCoordinator:(NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // DATA FLUSHER
    // [WARNING: YOU ARE NOT ALLOWED TO USE THIS CONTEXT]
    // Core Data Stack for the Master Thread [MASTER] -> [PERSISTENTSTORE]
    _masterContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_masterContext performBlockAndWait:^{
        [_masterContext setPersistentStoreCoordinator:persistentStoreCoordinator];
        [_masterContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // UI RELATED CONTEXT
    // Core Data Stack for the Main Thread [MAIN] -> [MASTER]
    _mainContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_mainContext setParentContext:_masterContext];
    [_mainContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    
    // BACKGROUND CONTEXT
    // Core Data Stack for the Worker Thread [WORKER] -> [MAIN]
    _workerContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_workerContext performBlockAndWait:^{
        [_workerContext setParentContext:_mainContext];
        [_workerContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // LOAD STORE FILE
    [self loadStore];
}

- (void)loadStore {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSDictionary *sqliteConfig = @{@"journal_mode" : @"DELETE"};//@{@"journal_mode" : @"WAL"}
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption  : @YES ,
                              NSInferMappingModelAutomaticallyOption        : @YES , //Light Weight Migration
                              NSSQLitePragmasOption                         : sqliteConfig
                              };
    NSError *error = nil;
    _store = [_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:[self storeURL] options:options error:&error];
    if (!_store) {
        //abort();
    } else {
        NSLog(@"Successfully added store: %@", _store);
    }
}

#pragma mark - MODELS (Initialization)

- (NSManagedObjectModel *)modelFromFrameWork:(NSString *)name {
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSURL *modelFileURL = [appBundle URLForResource:name withExtension:@"momd"];
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelFileURL];
    
    return model;
}

#pragma mark - SAVING

- (void)saveContext {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSManagedObjectContext *ctx = _workerContext;
    [ctx performBlockAndWait:^{
        [self saveTreeContext:ctx];
    }];
}

- (void)saveTreeContext:(NSManagedObjectContext *)context {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    if (!context) {
        return;
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"ERROR saving: %@", error);
    }
    
    if (context.parentContext) {
        [self saveTreeContext:context.parentContext];
    }
}

#pragma mark - PATHS

- (NSURL *)storeURL {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSURL *fileURL = [self applicationStoresDirectory];
    
    return [fileURL URLByAppendingPathComponent:storeFilename];
}

- (NSURL *)applicationStoresDirectory {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    NSString *filePath = [self applicationDocumentsDirectory];
    NSURL *storesDirectory = [[NSURL fileURLWithPath:filePath] URLByAppendingPathComponent:@"Stores"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtURL:storesDirectory withIntermediateDirectories:YES attributes:nil error:&error]) {
            NSLog(@"Successfully created Stores directory");
            if (error) {
                NSLog(@"Failed to create Stores directory : %@", error);
            }
        }
    }
    return storesDirectory;
}

- (NSString *)applicationDocumentsDirectory {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    return [NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - FAULTING (Memory Management)

- (void)faultObjectWithID:(NSManagedObjectID *)objectID inContext:(NSManagedObjectContext *)context {
    
    if (!objectID || !context) { return; }
    
    NSManagedObject *object = [context objectWithID:objectID];
    if (object.hasChanges) {
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"ERROR saving: %@", error);
        }
    }
    if (!object.isFault) {
        [context refreshObject:object mergeChanges:NO];
    }
    else {
        NSLog(@"Skipped faulting an object that is already a fault");
    }
    
    // Repeat the process if the context has a parent
    if (context.parentContext) {
        [self faultObjectWithID:objectID inContext:context.parentContext];
    }
}

#pragma mark - MIGRATION

- (BOOL)isCompatibleWithCoreDataMetadata:(NSURL *)storeUrl {
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *filePath = [NSString stringWithFormat:@"%@", storeUrl.path];
    
    if ([fm fileExistsAtPath:filePath]) {
        NSLog(@"checking model for compatibility...");
        
        NSError *error = nil;
//        NSDictionary *metaData = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
//                                                                                            URL:storeUrl
//                                                                                          error:&error];
        NSDictionary *metaData = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:storeUrl options:nil error:&error];
        //        [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:storeUrl options:nil error:&error];

        NSManagedObjectModel *model = self.coordinator.managedObjectModel;
        
        if ([model isConfiguration:nil compatibleWithStoreMetadata:metaData]) {
            NSLog(@"model is compatible...");
            return YES;
            
        } else {
            
            if ([fm removeItemAtPath:filePath error:NULL]) {
                NSLog(@"removed file : %@", filePath);
                NSLog(@"model is incompatible...");
                return NO;
            }//REMOVE THE STORE FILE
            
        }
    }
    
    NSLog(@"file does not exists..");
    return NO;
}

- (BOOL)migrateStore:(NSURL *)sourceStore {
    NSLog(@"SKIPPED MIGRATION: Source is already compatible");
    
    BOOL success = NO;
    NSError *error = nil;
    
    //STEP 1 - Gather the Source, Destination and Mapping Model
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:sourceStore error:&error];
    NSManagedObjectModel *sourceModel = [NSManagedObjectModel mergedModelFromBundles:nil forStoreMetadata:sourceMetadata];
    NSManagedObjectModel *destinModel = _model;
    NSMappingModel *mappingModel = [NSMappingModel mappingModelFromBundles:nil forSourceModel:sourceModel destinationModel:destinModel];
    
    //STEP 2 - Perform Migration, assuming the mapping model isn't null
    if (mappingModel) {
        NSMigrationManager *migrationManager = [[NSMigrationManager alloc] initWithSourceModel:sourceModel destinationModel:destinModel];
        
        //OBSERVER
        [migrationManager addObserver:self forKeyPath:@"migrationProgress" options:NSKeyValueObservingOptionNew context:NULL];
        
        NSURL *destinStore = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Temp.sqlite"];
        success = [migrationManager migrateStoreFromURL:sourceStore
                                                   type:NSSQLiteStoreType
                                                options:nil
                                       withMappingModel:mappingModel
                                       toDestinationURL:destinStore
                                        destinationType:NSSQLiteStoreType
                                     destinationOptions:nil
                                                  error:&error];
        if (success) {
            //STEP 3 - Replace the old store with the new migrated store
            if ([self replaceStore:sourceStore withStore:destinStore]) {
                NSLog(@"SUCCESSFULLY MIGRATED %@ to the Current Model", sourceStore.path);
                [migrationManager removeObserver:self forKeyPath:@"migrationProgress"];
            }
            //STEP 3
        }
        else {
            NSLog(@"FAILED MIGRATION : %@", error);
        }
    }
    else {
        NSLog(@"FAILED MIGRATION: Mapping Model is null");
    }
    
    return YES;// indicates migration has finished, regardless of outcome
}

- (BOOL)replaceStore:(NSURL *)old withStore:(NSURL *)new {
    
    BOOL success = NO;
    NSError *Error = nil;
    if ([[NSFileManager defaultManager] removeItemAtURL:old error:&Error]) {
        
        Error = nil;
        if ([[NSFileManager defaultManager] moveItemAtURL:new toURL:old error:&Error]) {
            success = YES;
        }
        else {
            NSLog(@"FAILED to re-home new store %@", Error);
        }
    }
    else {
        NSLog(@"FAILED to remove old store %@: Error:%@", old, Error);
    }
    return success;
}

#pragma mark - WORKER

- (NSString *)baseURL {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *server = [NSString stringWithFormat:@"%@", [defaults stringForKey:@"baseurl_preference"] ];
    //    NSString *server = @"172.16.7.174"; //HARD CODING
    
    return server;
}

- (NSURL *)buildURL:(NSString *)string {
    
    NSString *path = [Utils buildUrl:string];
    NSLog(@"path : %@", path);
    return [NSURL URLWithString:path];
}

- (NSString *)emptyString:(NSString *)value {
    
    if ([value isEqualToString:@"(null)"]) {
        return @"";
    }
    return value;
}

- (NSString *)normalizeStringObject:(NSString *)string {
    
    NSCharacterSet *leadingTrailing = [NSCharacterSet whitespaceCharacterSet];
    NSString *value = [string stringByTrimmingCharactersInSet:leadingTrailing];
    
    return value;
}

- (NSString *)stringValue:(id)object {
    
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] ||  [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

- (BOOL)isArrayObject:(id)object {
    return [object isKindOfClass:[NSArray class]];
}

- (BOOL)isDictionaryObject:(id)object {
    return [object isKindOfClass:[NSDictionary class]];
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption | NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath object:(id)object {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:object];
    
    // predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption | NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (NSManagedObject *)getEntity:(NSString *)entity attribute:(NSString *)attribute
                     parameter:(NSString *)parameter context:(NSManagedObjectContext *)context {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (parameter) {
        
        // create predicate
        NSPredicate *predicate = [self predicateForKeyPath:attribute andValue:parameter];
        
        // set predicate
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
}

- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSUInteger)fetchCountForEntity:(NSString *)entity {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
    [request setIncludesSubentities:NO]; //Omit subentities. Default is YES (i.e. include subentities)
    
    NSError *err;
    
    NSManagedObjectContext *contex = _workerContext;
    NSUInteger count = [contex countForFetchRequest:request error:&err];
    if(count == NSNotFound) {
        return 0;
    }
    
    return count;
}

- (NSArray *)fetchObjectsForEntity:(NSString *)entity {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    NSError *err;
    NSManagedObjectContext *contex = _workerContext;
    NSArray *items = [contex executeFetchRequest:request error:&err];
    if(items.count > 0) {
        return items;
    }
    
    return 0;
}

- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return items;
    }
    
    return nil;
}

- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSManagedObjectContext *context = _workerContext;
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSManagedObject *)insertNewRecordForEntity:(NSString *)entity {
    
    NSManagedObjectContext *ctx = _workerContext;
    return [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:ctx];
}

- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    //    float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
    //    if (ver >= 9.0) {
    //        // Only executes on version 8 or above.
    //        NSLog(@"%s OS %f", __PRETTY_FUNCTION__, ver);
    //
    //        if (_store) {
    //
    //            NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:fetchRequest];
    //            NSError *deleteError = nil;
    //            NSArray *list = [_coordinator executeRequest:delete withContext:_workerContext error:&deleteError];
    //
    //            if (list != nil) {
    //                return YES;
    //            }
    //            return NO;
    //        }
    //    }
    //
    //    if (ver < 9.0) {
    // Only executes on version 8 or above.
    //        NSLog(@"%s OS %f", __PRETTY_FUNCTION__, ver);
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *lmo in items) {
            [context deleteObject:lmo];
        }
        [self saveTreeContext:context];
        
        return YES;
    }
    //    }
    
    return NO;
}

- (BOOL)clearContentsForEntity:(NSString *)entity {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *lmo in items) {
            [context deleteObject:lmo];
        }
        [self saveTreeContext:context];
        
        return YES;
    }
    
    return NO;
}

- (BOOL)clearDataForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)ctx {
    
    //CLEAR CONTENTS
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate != nil) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *mo in items) {
            [ctx deleteObject:mo];
        }
        [self saveTreeContext:ctx];
    }
    
    return YES;
}

- (NSTimeInterval)parseTimeFromString:(NSString *)string {
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    
    NSInteger h, m, s;
    [scanner scanInteger:&h];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&m];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&s];
    
    return h * 3600 + m * 60 + s;
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (NSDate *)parseDateFromString:(NSString *)dateString {
    
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@".000Z" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" togo" withString:@""];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [self.formatter setTimeZone:gmt];
    NSString *template = @"yyyy-MM-dd HH:mm:ss";
    [self.formatter setDateFormat:template];
    NSDate *dateObject = [self.formatter dateFromString:dateString];
    return dateObject;
}

- (id)parseResponseData:(NSData *)data {
    
    if (data) {
        NSError *jsonError = nil;
        
        id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
        
        if (jsonError) {
            NSLog(@"JSON Error : %@", jsonError.localizedDescription);
        }
        
        if (!jsonError) {
            return object;
        }
    }
    
    return nil;
}

- (NSString *)jsonStringFromObject:(id)object {
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:object options:0 error:nil];
    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return string;
}

- (NSMutableURLRequest *)buildRequestWithMethod:(NSString *)method NSURL:(NSURL *)url body:(id)body {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:method];
    
    if (body) {
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:body options:0 error:&error];
        
        NSString *j_string = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        NSLog(@"j_string : <start>---- %@ ---<end>", j_string);
        [request setHTTPBody:postData];
    }
    
    return request;
}

- (BOOL)ownedByUser:(NSString *)userid {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return [user_id isEqualToString:userid];
}

- (NSString *)loginUser {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return user_id;
}


#pragma - mark User Standard Defaults Helper Functions

- (void)saveObject:(id)object forKey:(NSString *)key {
    NSLog(@"%s [%@] [%@]", __PRETTY_FUNCTION__, object, key);
    [self.userDefaults setObject:object forKey:key];
    [self.userDefaults synchronize];
}

- (id)fetchObjectForKey:(NSString *)key {
    id object = [self.userDefaults objectForKey:key];
    return object;
}



#pragma - mark Gradebook implementations


- (void)requestCourseListForUserID:(NSString *)userid doneBlock:(GradeBookDoneBlock)doneBlock {
    NSString *quizlistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointCourseList, userid]];
    NSLog(@"quiz path : %@", quizlistPath);
    NSURL *quizlistURL = [NSURL URLWithString:quizlistPath];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:quizlistURL body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSDictionary *meta = dictionary[@"_meta"];
              NSString *metaCount = [self stringValue:meta[@"count"]];
              NSString *metaStatus = meta[@"status"];
              
              NSManagedObjectContext *ctx = self.workerContext;
              
              if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
                  [ctx performBlock:^{
                      [self clearDataForEntity:kCourseEntityV2 withPredicate:nil context:ctx];
                      [self clearDataForEntity:kGradeBookEntityV2 withPredicate:nil context:ctx];
                  }];
                  
                  if (doneBlock) {
                      doneBlock(NO);
                  }
                  
                  return;
              }
              
              if (dictionary) {
                  NSArray *records = dictionary[@"records"];
                  
                  if (records > 0) {
                      
                      NSManagedObjectContext *ctx = _workerContext;
                      
                      [ctx performBlock:^{
                          
                          //CLEAR CONTENTS (IMPORTANT)
                          [self clearDataForEntity:kCourseEntityV2 withPredicate:nil context:ctx];
                          
                          NSUInteger indexcount = 0;
                          
                          //REFRESH DATA
                          for (NSDictionary *d in records) {
                              
                              /*
                               {
                               "course_id": "1",
                               "course_name": "English 6",
                               "initial": "",
                               "description": "English",
                               "course_code": "ENGLISH",
                               "cs_id": "9",
                               "section_id": "1",
                               "schedule": "TBA",
                               "venue": "TBA"
                               }
                               */
                              
                              NSString *course_code = [NSString stringWithFormat:@"%@", d[@"course_code"] ];
                              NSString *course_description = [NSString stringWithFormat:@"%@", d[@"description"] ];
                              NSString *course_id = [NSString stringWithFormat:@"%@", d[@"course_id"] ];
                              NSString *course_index = [NSString stringWithFormat:@"%lu", (unsigned long)indexcount ];
                              NSString *course_name = [NSString stringWithFormat:@"%@", d[@"course_name"] ];
                              NSString *cs_id = [NSString stringWithFormat:@"%@", d[@"cs_id"] ];
                              NSString *initial = [NSString stringWithFormat:@"%@", d[@"initial"] ];
                              NSString *schedule = [NSString stringWithFormat:@"%@", d[@"schedule"] ];
                              NSString *section_id = [NSString stringWithFormat:@"%@", d[@"section_id"] ];
                              NSString *venue = [NSString stringWithFormat:@"%@", d[@"venue"] ];
                              
                              NSString *search_string = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",
                                                         course_description, course_name, initial, schedule, venue];
                              
                              //                              NSManagedObject *mo = [self getEntity:kCourseEntity attribute:@"course_id" parameter:course_id context:ctx];
                              
                              //                              if (mo == nil) {
                              NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kCourseEntityV2 inManagedObjectContext:ctx];
                              //                              }
                              
                              [mo setValue:course_code forKey:@"course_code"];
                              [mo setValue:course_description forKey:@"course_description"];
                              [mo setValue:course_id forKey:@"course_id"];
                              [mo setValue:course_index forKey:@"course_index"];
                              [mo setValue:course_name forKey:@"course_name"];
                              [mo setValue:cs_id forKey:@"cs_id"];
                              [mo setValue:initial forKey:@"initial"];
                              [mo setValue:schedule forKey:@"schedule"];
                              [mo setValue:section_id forKey:@"section_id"];
                              [mo setValue:venue forKey:@"venue"];
                              [mo setValue:userid forKey:@"user_id"];
                              [mo setValue:search_string forKey:@"search_string"];
                              
                              indexcount++;
                          }
                          
                          [self saveTreeContext:ctx];
                          
                          if (doneBlock) {
                              doneBlock(YES);
                          }
                      }];
                  }
              }
          }
      }];
    [task resume];
}


- (NSArray *)fetchCourseListForUserID:(NSString *)userid {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCourseEntityV2];
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"cs_id" ascending:YES];
    [fetchRequest setSortDescriptors:@[descriptor]];
    
    NSManagedObjectContext *context = _workerContext;
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        VLog(@"error: %@", [error localizedDescription]);
    }
    
    
    NSMutableArray *list = [NSMutableArray array];
    if ([items count] > 0) {
        for (NSManagedObject *mo in items) {
            NSArray *keys = mo.entity.propertiesByName.allKeys;
            NSMutableDictionary *d = [NSMutableDictionary dictionary];
            for (NSString *k in keys) {
                NSString *v = [NSString stringWithFormat:@"%@", [mo valueForKey:k] ];
                [d setValue:v forKey:k];
            }
            [list addObject:d];
        }
    }
    
    return list;
}

- (void)requestGradeListForUser:(NSString *)userid course:(NSString *)courseid doneBlock:(GradeBookDoneBlock)doneBlock {
    
    NSString *gradelistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointGradeList, userid, courseid]];
    NSLog(@"grade path : %@", gradelistPath);
    NSURL *gradelistURL = [NSURL URLWithString:gradelistPath];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:gradelistURL body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          NSDictionary *meta = dictionary[@"_meta"];
                                          NSString *metaCount = [self stringValue:meta[@"count"]];
                                          NSString *metaStatus = meta[@"status"];
                                          
                                          NSManagedObjectContext *ctx = self.workerContext;
                                          
                                          if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
                                              [ctx performBlock:^{
                                                  [self clearDataForEntity:kGradeBookRecEntityV2 withPredicate:nil context:ctx];
                                              }];
                                              
                                              if (doneBlock) {
                                                  doneBlock(NO);
                                              }
                                              
                                              return;
                                          }
                                          
                                          
                                          
                                          if (dictionary) {
                                              
                                              //                  NSLog(@"dictionary : %@", dictionary);
                                              NSArray *records = dictionary[@"records"];
                                              
                                              if (records > 0) {
                                                  
                                                  NSManagedObjectContext *ctx = _workerContext;
                                                  
                                                  [ctx performBlock:^{
                                                      
                                                      //CLEAR CONTENTS (IMPORTANT)
                                                      [self clearDataForEntity:kGradeBookRecEntityV2 withPredicate:nil context:ctx];
                                                      
                                                      //REFRESH DATA
                                                      
                                                      NSInteger counter = 0;
                                                      for (NSDictionary *d in records) {
                                                          
                                                          NSLog(@"GRADE BOOK : %@", d);
                                                          /*
                                                           {
                                                           "activity_name" = "Science Final Exam";
                                                           "activity_type_id" = 8;
                                                           class = "Science 6";
                                                           "date_performed" = "Nov 28 2014 12:51 PM";
                                                           remarks = "Quiz taken: 2014-11-28 12:52:33";
                                                           score = "2.00";
                                                           }
                                                           */
                                                          
                                                          NSString *activity_name = [NSString stringWithFormat:@"%@", d[@"activity_name"] ];
                                                          NSString *activity_type_id = [NSString stringWithFormat:@"%@", d[@"activity_type_id"] ];
                                                          NSString *class = [NSString stringWithFormat:@"%@", d[@"class"] ];
                                                          NSString *date_performed = [NSString stringWithFormat:@"%@", d[@"date_performed"] ];
                                                          NSString *remarks = [NSString stringWithFormat:@"%@", d[@"remarks"] ];
                                                          NSString *score = [NSString stringWithFormat:@"%@", d[@"score"] ];
                                                          NSString *search_string = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", activity_name, class, date_performed, remarks, score];
                                                          
//                                                          NSManagedObject *mo = [self getEntity:kGradeBookRecEntityV2 attribute:@"order_index" parameter:date_performed context:ctx];
                                                          
//                                                          if (mo == nil) {
                                                              NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kGradeBookRecEntityV2 inManagedObjectContext:ctx];
//                                                          }
                                                          
                                                          [mo setValue:activity_name forKey:@"activity_name"];
                                                          [mo setValue:activity_type_id forKey:@"activity_type_id"];
                                                          [mo setValue:class forKey:@"class_name"];
                                                          [mo setValue:date_performed forKey:@"date_performed"];
                                                          [mo setValue:remarks forKey:@"remarks"];
                                                          [mo setValue:score forKey:@"score"];
                                                          [mo setValue:search_string forKey:@"search_string"];
                                                          [mo setValue:@(counter) forKey:@"order_index"];
                                                          
                                                          counter++;
                                                          //                              NSLog(@"search string : %@", search_string);
                                                      }
                                                      
                                                      [self saveTreeContext:ctx];
                                                      
                                                  }];
                                                  
                                                  
                                                  if (doneBlock) {
                                                      doneBlock(YES);
                                                  }
                                              }
                                              
                                          }
                                          
                                      }
                                      
                                  }];
    [task resume];
}

- (void)requestCourseListForUser:(NSString *)userid dataBlock:(GradeBookDataBlock)dataBlock {
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kGBCourseEntityV2 withPredicate:nil context:ctx];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointGBCourseList, userid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"course records: %@", records);
                
                if (records.count > 0) {
                    [ctx performBlock:^{
                        NSInteger counter = 0;
                        NSDictionary *initial = [NSDictionary dictionary];
                        
                        for (NSDictionary *d in records) {
                            NSString *obj_id = [self stringValue:d[@"id"]];
                            NSString *course_id = [self stringValue:d[@"course_id"]];
                            NSString *section_id = [self stringValue:d[@"section_id"]];
                            NSString *details = [self stringValue:d[@"details"]];
                            NSString *course_name = [self stringValue:d[@"course_name"]];
                            NSString *section_name = [self stringValue:d[@"section_name"]];
                            NSString *grade_level_id = [self stringValue:d[@"grade_level_id"]];
                            
                            NSManagedObject *mo = [self getNewEntity:kGBCourseEntityV2
                                                           attribute:@"id"
                                                           parameter:obj_id
                                                             context:ctx];
                            
                            [mo setValue:obj_id forKey:@"id"];
                            [mo setValue:course_id forKey:@"course_id"];
                            [mo setValue:section_id forKey:@"section_id"];
                            [mo setValue:details forKey:@"details"];
                            [mo setValue:course_name forKey:@"course_name"];
                            [mo setValue:section_name forKey:@"section_name"];
                            [mo setValue:grade_level_id forKey:@"grade_level_id"];
                            
                            if (counter == 0) {
                                initial = @{@"id":obj_id, @"section_name":section_name, @"course_name":course_name};
                            }
                            
                            counter++;
                        }
                        
                        [self saveTreeContext:ctx];
                        
                        if (dataBlock) {
                            dataBlock(initial);
                        }
                    }];
                }
            }
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
}


- (void)requestGradeBookForClassID:(NSString *)class_id isAscending:(BOOL)isAscending doneBlock:(ResourceManagerDoneBlock)doneBlock {
    NSManagedObjectContext *gradeBookCtx = self.workerContext;
    [self clearContentsForEntity:kGradeBookEntityV2 predicate:nil];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointGradeBook, class_id] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          if (doneBlock) {
                                              doneBlock(NO);
                                          }
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          NSArray *records = dictionary[@"records"];
                                          
                                          NSDictionary *tableHeadDict = records[0];
                                          NSArray *tableHeadAr = tableHeadDict[@"table_head"];
                                          
                                          NSDictionary *tableBodyDict = records[1];
                                          NSArray *tableBodyAr = tableBodyDict[@"table_body"];
                                          
                                          [gradeBookCtx performBlockAndWait:^{
                                              NSMutableArray *column_nameArray = [NSMutableArray array];
                                              NSMutableArray *deploy_idArray = [NSMutableArray array];
                                              NSInteger index = 1;
                                              
                                              // TABLE HEADER
                                              for (NSDictionary *tableHeadDict in tableHeadAr) {
                                                  NSString *is_header = @"1";
                                                  NSString *column_name = [self stringValue:tableHeadDict[@"column_name"]];
                                                  
                                                  if (![column_name isEqualToString:@"AVATAR"]) {
                                                      NSString *deploy_id = [self stringValue:tableHeadDict[@"deploy_id"]];
                                                      NSString *activity_id = [self stringValue:tableHeadDict[@"activity_id"]];
                                                      NSString *activity_type_id = [self stringValue:tableHeadDict[@"activity_type_id"]];
                                                      NSString *activity_type = [self stringValue:tableHeadDict[@"activity_type"]];
                                                      NSString *activity_name = [self stringValue:tableHeadDict[@"activity_name"]];
                                                      NSString *item_id = [self stringValue:tableHeadDict[@"item_id"]];
                                                      
                                                      deploy_id = ([deploy_id isEqualToString:@""]) ? @"0" : deploy_id;
                                                      activity_id = ([activity_id isEqualToString:@""]) ? @"0" : activity_id;
                                                      activity_type_id = ([activity_type_id isEqualToString:@""]) ? @"0" : activity_type_id;
                                                      activity_type = ([activity_type isEqualToString:@""]) ? @"0" : activity_type;
                                                      activity_name = ([activity_name isEqualToString:@""]) ? column_name : activity_name;
                                                      item_id = ([item_id isEqualToString:@""]) ? @"0" : item_id;
                                                      
                                                      NSManagedObject *g_mo = [NSEntityDescription insertNewObjectForEntityForName:kGradeBookEntityV2 inManagedObjectContext:gradeBookCtx];
                                                      
                                                      [g_mo setValue:deploy_id forKey:@"id"];
                                                      [g_mo setValue:activity_name forKey:@"cell_value"];
                                                      [g_mo setValue:is_header forKey:@"is_header"];
                                                      [g_mo setValue:@(index) forKey:@"index"];
                                                      [g_mo setValue:@(1) forKey:@"section_index"];
                                                      [g_mo setValue:@"C02MM6N0FH01" forKey:@"search_key"]; // DEFAULT search key for header
                                                      
                                                      [column_nameArray addObject:column_name];
                                                      [deploy_idArray addObject:deploy_id];
                                                      
                                                      index += 1;
                                                  }
                                              }
                                              
                                              // FOR SORTING
                                              NSMutableDictionary *dict_for_sorting = [NSMutableDictionary dictionary];
                                              for (NSDictionary *tableBodyDict in tableBodyAr) {
                                                  NSString *key = [self stringValue:tableBodyDict[@"NAME"]];
                                                  [dict_for_sorting setObject:tableBodyDict forKey:key];
                                              }
                                              
                                              NSArray *sorted_keys = [[dict_for_sorting allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                                              
                                              if (!isAscending) {
                                                  sorted_keys = [[[sorted_keys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] reverseObjectEnumerator] allObjects];
                                              }
                                              
                                              NSMutableArray *sorted_dict = [NSMutableArray array];
                                              for (NSString *key in sorted_keys) {
                                                  [sorted_dict addObject:[dict_for_sorting objectForKey:key]];
                                              }
                                              
                                              NSInteger body_index = 1;
                                              NSInteger section_body_index = 2;
                                              
                                              // TABLE BODY
                                              for (NSDictionary *tableBodyDict in sorted_dict) {
                                                  NSString *is_header = @"0";
                                                  NSString *user_id = [self stringValue:tableBodyDict[@"user_id"]];
                                                  NSString *participant_id = [self stringValue:tableBodyDict[@"participant_id"]];
                                                  NSString *name = [self stringValue:tableBodyDict[@"NAME"]];
                                                  
                                                  NSManagedObject *name_mo = [NSEntityDescription insertNewObjectForEntityForName:kGradeBookEntityV2 inManagedObjectContext:gradeBookCtx];
                                                  
                                                  [name_mo setValue:user_id forKey:@"id"];
                                                  [name_mo setValue:name forKey:@"cell_value"];
                                                  [name_mo setValue:is_header forKey:@"is_header"];
                                                  [name_mo setValue:@(body_index) forKey:@"index"];
                                                  [name_mo setValue:@(section_body_index) forKey:@"section_index"];
                                                  [name_mo setValue:[name lowercaseString] forKey:@"search_key"];
                                                  
                                                  NSInteger arrayIndex = 0;
                                                  for (NSString *activity_score_key in column_nameArray) {
                                                      body_index += 1;
                                                      
                                                      if (![activity_score_key isEqualToString:@"NAME"] && ![activity_score_key isEqualToString:@"AVATAR"]) {
                                                          NSString *deploy_id = deploy_idArray[arrayIndex];
                                                          
                                                          NSString *activity_score = [self stringValue:tableBodyDict[activity_score_key]];
                                                          NSString *took_test = (activity_score.length == 0) ? @"0" : @"1";
                                                          activity_score = ((activity_score.length == 0)) ? @"0" : activity_score;
                                                          activity_score = [self formatStringNumber:activity_score];
                                                          
                                                          NSString *exp_activity_key = [NSString stringWithFormat:@"EXP_%@", activity_score_key];
                                                          NSString *exp_activity_score = [self stringValue:tableBodyDict[exp_activity_key]];
                                                          exp_activity_score = ((activity_score.length == 0)) ? @"0" : exp_activity_score;
                                                          exp_activity_score = [self formatStringNumber:exp_activity_score];
                                                          
                                                          NSString *rem_activity_key = [NSString stringWithFormat:@"REMARK_%@", activity_score_key];
                                                          NSString *remark_activity = [self stringValue:tableBodyDict[rem_activity_key]];
                                                          
                                                          NSString *totalAndExpected = ([took_test isEqualToString:@"0"]) ? @"-" : [NSString stringWithFormat:@"%@ / %@", activity_score, exp_activity_score];
                                                          
                                                          NSManagedObject *act_mo = [NSEntityDescription insertNewObjectForEntityForName:kGradeBookEntityV2 inManagedObjectContext:gradeBookCtx];
                                                          
                                                          [act_mo setValue:user_id forKey:@"id"];
                                                          [act_mo setValue:totalAndExpected forKey:@"cell_value"];
                                                          [act_mo setValue:is_header forKey:@"is_header"];
                                                          [act_mo setValue:@(body_index) forKey:@"index"];
                                                          [act_mo setValue:@(section_body_index) forKey:@"section_index"];
                                                          [act_mo setValue:[name lowercaseString] forKey:@"search_key"];
                                                          [act_mo setValue:class_id forKey:@"class_id"];
                                                          [act_mo setValue:name forKey:@"student_name"];
                                                          [act_mo setValue:participant_id forKey:@"participant_id"];
                                                          [act_mo setValue:deploy_id forKey:@"deploy_id"];
                                                          [act_mo setValue:exp_activity_score forKey:@"exp_score"];
                                                          [act_mo setValue:activity_score forKey:@"score"];
                                                          [act_mo setValue:remark_activity forKey:@"remark_activity"];
                                                          [act_mo setValue:took_test forKey:@"took_test"];
                                                      }
                                                      
                                                      arrayIndex += 1;
                                                  }
                                                  
                                                  body_index = 1;
                                                  section_body_index += 1;
                                              }
                                              
                                              [self saveTreeContext:gradeBookCtx];
                                              
                                              if (doneBlock) {
                                                  doneBlock(YES);
                                              }
                                          }];
                                      }
                                  }];
    
    [task resume];
}




- (void)postGradeBookChangeScoreForData:(NSDictionary *)body doneBlock:(GradeBookDoneBlock)doneBlock {
    
    NSString *participant_id = body[@"participant_id"];
    NSString *deploy_id = body[@"deploy_id"];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointGradeBookChangeScore, participant_id, deploy_id] ];
    NSLog(@"path : %@", url);
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:body];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              if (error) {
                                                  NSLog(@"error %@", [error localizedDescription]);
                                                  if (doneBlock) {
                                                      doneBlock(NO);
                                                  }
                                              }
                                              
                                              if (data != nil) {
                                                  NSDictionary *dictionary = [self parseResponseData:data];
                                                  
                                                  NSLog(@"RESPONSE DATA : %@", dictionary);
                                                  if (doneBlock) {
                                                      doneBlock(YES);
                                                  }
                                              }
                                              
                                          }];
    
    [postDataTask resume];
}

- (void)postGradeBookSetScoreForData:(NSDictionary *)body doneBlock:(GradeBookDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointGradeBookSetScore]];
    NSLog(@"path : %@", url);
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:body];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              if (error) {
                                                  NSLog(@"error %@", [error localizedDescription]);
                                                  if (doneBlock) {
                                                      doneBlock(NO);
                                                  }
                                              }
                                              
                                              if (data != nil) {
                                                  NSDictionary *dictionary = [self parseResponseData:data];
                                                  
                                                  NSLog(@"RESPONSE DATA : %@", dictionary);
                                                  if (doneBlock) {
                                                      doneBlock(YES);
                                                  }
                                              }
                                              
                                          }];
    
    [postDataTask resume];
}

- (NSUInteger)countRecordsForEntity:(NSString *)entity attribute:(NSString *)attribute value:(id)value {
    
    NSPredicate *predicate = [self predicateForKeyPath:attribute andValue:value];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
    [request setIncludesSubentities:NO]; //Omit subentities. Default is YES (i.e. include subentities)
    
    if (predicate) {
        [request setPredicate:predicate];
    }
    
    NSManagedObjectContext *ctx = _mainContext;
    
    NSError *err;
    NSUInteger count = [ctx countForFetchRequest:request error:&err];
    //    if(count == NSNotFound) {
    //    }
    
    
    
    return count;
}



- (NSManagedObject *)getNewEntity:(NSString *)entity attribute:(NSString *)attribute parameter:(NSString *)parameter context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (parameter) {
        NSPredicate *predicate = [self predicateForKeyPath:attribute andValue:parameter];
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
}

- (BOOL)isItOkayToParseUsingThisResponseData:(NSDictionary *)response {
    NSLog(@"response: %@", response);
    
    if (response != nil) {
        NSDictionary *parsedmeta = response[@"_meta"];
        
        if (parsedmeta != nil) {
            if ([self doesKeyExistInDictionary:parsedmeta key:@"status"]) {
                NSString *status = [self stringValue:parsedmeta[@"status"]];
                
                if ([status isEqualToString:@"SUCCESS"]) {
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

- (BOOL)doesKeyExistInDictionary:(NSDictionary *)d key:(NSString *)key {
    NSArray *keys = [d allKeys];
    
    for (NSString *k in keys) {
        if ([k isEqualToString:key]) {
            return YES;
        }
    }
    
    return NO;
}

- (NSString *)formatStringNumber:(NSString *)stringNumber {
    CGFloat floatNumber = [stringNumber floatValue];
    NSInteger intNumber = (NSInteger)floatNumber;
    
    if (floatNumber == intNumber) {
        stringNumber = [NSString stringWithFormat:@"%zd", intNumber];
    }
    else {
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        formatter.maximumFractionDigits = 2;
        formatter.roundingMode = NSNumberFormatterRoundUp;
        
        stringNumber = [formatter stringFromNumber:[NSNumber numberWithFloat:floatNumber]];
    }
    
    return stringNumber;
}


@end
