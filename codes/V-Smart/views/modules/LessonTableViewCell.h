//
//  LessonTableViewCell.h
//  V-Smart
//
//  Created by Julius Abarra on 9/1/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblLessonTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblLessonTimeFrame;
@property (strong, nonatomic) IBOutlet UILabel *lblLessonSchoolName;
@property (strong, nonatomic) IBOutlet UILabel *lblLessonStatus;
@property (strong, nonatomic) IBOutlet UIButton *butPopOverAction;

@end
