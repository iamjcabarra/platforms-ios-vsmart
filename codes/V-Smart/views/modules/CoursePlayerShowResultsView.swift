//
//  CoursePlayerShowResultsView.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 04/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

@objc class CoursePlayerShowResultsView: UIViewController, NSFetchedResultsControllerDelegate, UIScrollViewDelegate, CoursePlayerShowResultsDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    fileprivate var fontView:CoursePlayerFontPopoverView!
    fileprivate var popoverController: UIPopoverController!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var testTitleLabel: UILabel!
    @IBOutlet weak var testScoreLabel: UILabel!
    @IBOutlet weak var testQuestionCountLabel: UILabel!
    
    
    fileprivate var imageIndex : Int?
    fileprivate var previewData: [AnyHashable: Any]?
    
    var testObject:NSManagedObject?
    var results:NSDictionary?
    
    // MARK: - Shared Data Manager
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tm = TestGuruDataManager.sharedInstance()
        return tm!
    }()
    
    // CELL IDENTIFIER
    let cellID = "YGTB_QUESTION_PREVIEW_COLLECTION_ITEM"
    let kModalSegue = "TGTB_PRESENT_CUSTOM_PAGE"
    let kSidePanelModelSegue = "TGTB_PRESENT_CUSTOM_SIDE_PANEL"
    let kModalSectionSelection = "SHOW_SECTION_SELECTION_VIEW"
    let kEditTestView = "SHOW_EDIT_TEST_VIEW"
    
    var index: IndexPath? = nil
    var totalItems: NSInteger? = 0
    
    var show_correct_answer: NSString = "0"
    var show_feedbacks: NSString = "0"
    var show_score: NSString = "0"
    
    let notif = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.collectionView.layer.border
        setUpResultSheet()
        
        self.dataManager.save("\(20)", forKey: kCP_SELECTED_FONT_SIZE);
        //        self.submitButton.addTarget(self, action: #selector(self.submitButtonAction(_:)), forControlEvents: .TouchUpInside)]
//        UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collection.collectionViewLayout;
//        layout.estimatedItemSize = CGSizeMake(50, 28);
        
//        let flowLayout = self.collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
//        flowLayout.estimatedItemSize = CGSize(width: 600 , height: 50)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeNavigationController()
    }
    
    func customizeNavigationController() {
        self.title = NSLocalizedString("TEST RESULT", comment: "")//localize
        let color = UIColor(hex6: 0x4274b9)
        
        if floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1 {
            self.navigationController?.navigationBar.tintColor = color
        } else {
            self.navigationController?.navigationBar.barTintColor = color;
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationController?.navigationBar.isTranslucent = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        _fetchedResultsController = nil
    }
    
    func setUpResultSheet() {
//        let testTitle = self.testObject?.valueForKey("name") as! String
//        let questionCount = (self.testObject?.valueForKey("questions") as? NSSet)!.count
//        
//        self.show_correct_answer = self.testObject?.valueForKey("show_correct_answer") as! String
//        self.show_feedbacks = self.testObject?.valueForKey("show_feedbacks") as! String
//        self.show_score = self.testObject?.valueForKey("show_score") as! String
//        
//        let total_score = self.results?.valueForKey("total_score") as! String
//        let general_score = self.results?.valueForKey("general_score") as! String
//        let scoreStr = "\(total_score)/\(general_score)"
//        self.testScoreLabel.text = "Score: \(scoreStr)"
//        self.testScoreLabel.highlightString(scoreStr, size: 19, color: UIColor.blackColor())
//        
//        self.testTitleLabel.text = testTitle
//        self.testQuestionCountLabel.text = "\(questionCount) \(NSLocalizedString("Items", comment: ""))"
        
        if show_score == "0" {
            self.testScoreLabel.isHidden = true
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // collection view data source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            totalItems = 0
            return totalItems!
        }
        totalItems = sectionData.numberOfObjects
        return totalItems!;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath)
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ collectionCell: UICollectionViewCell!, atIndexPath indexPath: IndexPath) {
        let cell = collectionCell as! CoursePlayerShowResultsCell
        cell.delegate = self;
        
        cell.show_correct_answer = self.show_correct_answer as String
        cell.show_feedbacks = self.show_feedbacks as String
        cell.show_score = self.show_score as String
        
        let mo = fetchedResultsController.object(at: indexPath)
        let questionID = mo.value(forKey: "question_id") as! String!
        cell.questionID = questionID
        cell.display(mo)
        cell.reloadFetchedResultsController()
        
        cell.currentIndex = indexPath;
        
        cell.table.layoutIfNeeded()
        cell.choicesTable.layoutIfNeeded()
        
        cell.questionAndImageTableHeight.constant = cell.table.contentSize.height
        cell.choiceTableHeight.constant = cell.choicesTable.contentSize.height
        
        cell.numberIndicator.text = "\((indexPath as NSIndexPath).row + 1). "
    }
    
    func cancelCellButtonAction(_ button:UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
        //        self.navigationController?.popViewControllerAnimated(false)
    }
    
//    func addCellButtonAction(button:UIButton) {
//        self.performSegueWithIdentifier("TGTB_ASSIGN_QUESTION_VIEW", sender: self)
//    }
    
    // flow layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
//
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
//
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
//    func collectionView
//
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//
////        let cell = collectionView.cellForItemAtIndexPath(indexPath)
//        
////        let height = cell!.choicesTable.contentSize.height + cell!.table.contentSize.height
//        let height = collectionView.frame.size.height
//        let width = self.collectionView.bounds.size.width
////        UICollectires
//        
//        return CGSizeMake(width, 10)
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.size.height
        let width = collectionView.frame.size.width
        print("~>\(width)")
        return CGSize(width: width, height: height)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        print("---------------------------------------------> " + #function)
        resizeContents()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("---------------------------------------------> " + #function)
        resizeContents()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("---------------------------------------------> " + #function)
        resizeContents()
    }
    
    fileprivate func resizeContents() {
//        
//        print("---------------------------------------------> " + #function)
//        
//        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
//        let size = self.collectionView.bounds.size
//        let w = size.width
//        let h = size.height
//        
//        layout.itemSize = CGSizeMake(w, h)
//        layout.invalidateLayout()
    }
    
    func generateCustomButton(_ imageName: String, action: Selector) -> UIButton {
        
        let button = UIButton(type: UIButtonType.system)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.customSetImage(imageName)
        button.tintColor = UIColor.white
        button.addTarget(self, action: action, for: .touchUpInside)
        
        return button
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "SAMPLE_QUESTION_IMAGE_VIEW" {
            let qip = segue.destination as! QuestionImagePreview
            qip.previewData = self.previewData
            qip.index = self.imageIndex!
        }
        
    }
    /////////////////////////////////// NSFETCHRESULTSCONTROLLER ///////////////////////////////////
    
    /*
     NOTE: CUSTOM ANIMATION
     */
    // MARK: - Fetched results controller
    
    fileprivate lazy var customDescriptors: [NSSortDescriptor] = {
        
        let ascending = true
        let question_index = NSSortDescriptor(key: "index", ascending: ascending)
        
        return [question_index]
    }()
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kCoursePlayerQuestionListEntity)
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        // Set ManagedObjectContext
        let ctx = dataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: kCoursePlayerQuestionListEntity)
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        fetchRequest.sortDescriptors = customDescriptors
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest,
                                             managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    fileprivate var blockOperation = BlockOperation()
    fileprivate var shouldReloadCollectionView = false
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.shouldReloadCollectionView = false
        self.blockOperation = BlockOperation()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange sectionInfo: NSFetchedResultsSectionInfo,
                                     atSectionIndex sectionIndex: Int,
                                             for type: NSFetchedResultsChangeType) {
        
        let collectionView = self.collectionView
        switch type {
        case NSFetchedResultsChangeType.insert:
            self.blockOperation.addExecutionBlock({
                collectionView?.insertSections( IndexSet(integer: sectionIndex) )
            })
        case NSFetchedResultsChangeType.delete:
            self.blockOperation.addExecutionBlock({
                collectionView?.deleteSections( IndexSet(integer: sectionIndex) )
            })
        case NSFetchedResultsChangeType.update:
            self.blockOperation.addExecutionBlock({
                collectionView?.reloadSections( IndexSet(integer: sectionIndex ) )
            })
        default:()
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                                    at indexPath: IndexPath?,
                                                for type: NSFetchedResultsChangeType,
                                                              newIndexPath: IndexPath?) {
        
        let collectionView = self.collectionView
        
        switch type {
            
        case NSFetchedResultsChangeType.insert:
            
            if (collectionView?.numberOfSections)! > 0 {
                
                if collectionView?.numberOfItems( inSection: (newIndexPath! as NSIndexPath).section ) == 0 {
                    self.shouldReloadCollectionView = true
                } else {
                    self.blockOperation.addExecutionBlock( {
                        collectionView?.insertItems( at: [newIndexPath!] )
                    } )
                }
                
            } else {
                self.shouldReloadCollectionView = true
            }
            
        case NSFetchedResultsChangeType.delete:
            
            if collectionView?.numberOfItems( inSection: (indexPath! as NSIndexPath).section ) == 1 {
                self.shouldReloadCollectionView = true
            } else {
                self.blockOperation.addExecutionBlock( {
                    collectionView?.deleteItems( at: [indexPath!] )
                } )
            }
            
        case NSFetchedResultsChangeType.update:
            self.blockOperation.addExecutionBlock({
                collectionView?.reloadItems( at: [indexPath!] )
            })
            
        case NSFetchedResultsChangeType.move:
            self.blockOperation.addExecutionBlock({
                collectionView?.moveItem( at: indexPath!, to: newIndexPath! )
            })
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        // Checks if we should reload the collection view to fix a bug @ http://openradar.appspot.com/12954582
        if self.shouldReloadCollectionView {
            self.collectionView.reloadData()
        } else {
            self.collectionView.performBatchUpdates({ self.blockOperation.start() }, completion: nil )
        }
    }
    
//    func transitionWithData(previewData: [NSObject : AnyObject]!, index: Int, segueID segueIdentifier: String!) {
//        self.previewData = previewData
//        self.imageIndex = index
//        self.performSegueWithIdentifier(segueIdentifier, sender: self)
//    }
//    
    func refreshIndexPath(_ indexPath: IndexPath!) {
        print("ROW!!!!! [\(indexPath.row)]")
//        self.view.layoutIfNeeded()
        DispatchQueue.main.async { 
            self.collectionView.reloadItems(at: [indexPath])
        }
    }
}
