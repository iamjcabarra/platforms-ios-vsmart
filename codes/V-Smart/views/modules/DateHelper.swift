//
//  DateHelper.swift
//  V-Smart
//
//  Created by Julius Abarra on 15/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import Foundation

class DateHelper {
    fileprivate let formatter = DateFormatter()
    
    func date(fromString string: String) -> Date {
        formatter.timeZone = TimeZone(identifier: "UTC")
        guard let date = formatter.date(from: string) else {
            print("Error: Can't convert string to date!")
            return Date()
        }
        
        return date
    }
    
    func dateString(_ string: String, fromFormat: String, toFormat: String, fromTimeZone timeZone: String?, setToLocalTimeZone: Bool) -> String {
        formatter.dateFormat = fromFormat
        if timeZone != nil { formatter.timeZone = TimeZone(identifier: timeZone!) }
            
        guard let date = formatter.date(from: string) else {
            print("Error: Can't transform date string!")
            return ""
        }
        
        formatter.dateFormat = toFormat
        if setToLocalTimeZone { formatter.timeZone = NSTimeZone.local }
        
        return formatter.string(from: date)
    }
    
    func dateString(fromDate date: Date, outFormat format: String, setToLocalTimeZone: Bool) -> String {
        formatter.dateFormat = format
        if setToLocalTimeZone { formatter.timeZone = NSTimeZone.local }
        return formatter.string(from: date)
    }

}


