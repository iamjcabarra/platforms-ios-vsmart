//
//  NewCurriculumListView.h
//  V-Smart
//
//  Created by Julius Abarra on 16/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewCurriculumListView : UIViewController

typedef NS_ENUM (NSInteger, VSCPErrorType) {
    VSCPErrorTypeFileWriteError = 1,
    VSCPErrorTypeFileDownloadError,
    VSCPErrorTypeDirectoryFullError
};

@property (strong, nonatomic) NSManagedObject *courseCategoryObject;

@end
