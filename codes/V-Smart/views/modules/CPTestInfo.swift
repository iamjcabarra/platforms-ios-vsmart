//
//  CPTestInfo.swift
//  V-Smart
//
//  Created by Ryan Migallos on 29/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


@objc protocol CPTestInfoDelegate: class {
    func didClickStartButton(_ flag:Bool)
}

@objc class CPTestInfo: UITableViewController {
    
    // MARK: - Delegate Property
    weak var delegate:CPTestInfoDelegate?
    
    // MARK: - Storyboard Outlets
    
    // COURSE
    @IBOutlet var courseTitleLabel: UILabel!
    
    // INSTRUCTION
    @IBOutlet var instructionTitleLabel: UILabel!
    @IBOutlet var instructionDescriptionLabel: UILabel!
    
    // CELL TYPES
    // CELL TYPES
    @IBOutlet var examCount: TGTBTestPlayInfoCell!
    @IBOutlet weak var examCountLabel: UILabel!
    
    @IBOutlet var examTimeLimit: TGTBTestPlayInfoCell!
    @IBOutlet weak var examTimeLimitLabel: UILabel!
    
    @IBOutlet var examQuestion: TGTBTestPlayInfoCell!
    @IBOutlet weak var examQuestionLabel: UILabel!
    
    @IBOutlet var examScore: TGTBTestPlayInfoCell!
    @IBOutlet weak var examScoreLabel: UILabel!

    
    // BUTTON
    @IBOutlet var footerView: UIView!
    @IBOutlet var startButton: UIButton!
    
    // MARK: - Shared Data Manager
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tm = TestGuruDataManager.sharedInstance()
        return tm!
    }()
    
    var mo: NSManagedObject?
    
//    var timerComponent: MZTimerLabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO: Pleas refactor
//        timerComponent = MZTimerLabel(label: examTimeLimit.itemTextLabel, andTimerType: MZTimerLabelTypeTimer)
        
        let startAction = #selector( self.viewButtonAction(_:) )
        startButton.addTarget(self, action: startAction, for:.touchUpInside)
        
        customizedSubViews()
        loadUserData()
    }
    
    func loadUserData() {
        
        //COURSE TITLE
        
        
        guard let userData = mo else {
            print("NS MANAGED OBJECT IS NIL!!!...")
            return
        }
        
        courseTitleLabel.text = (userData.value(forKey: "name")! as AnyObject).description//
        self.title = dataManager.fetchObject(forKey: kTGQB_SELECTED_COURSE_NAME) as? String
        
        //GENERAL INSTRUCTIONS
        let sample_instruction = (userData.value(forKey: "general_instruction")! as AnyObject).description
        instructionDescriptionLabel.text = sample_instruction
        
        /*
         This test contains ___ question
         NOTE: question(s) <= dynamic
         */
        
        let highlightColor = UIColor.black
        
        //QUESTION COUNT
        let question_set = userData.value(forKey: "questions") as! NSSet
        let item_count = "\(question_set.count)"
        let question_string = (question_set.count > 1 ) ? "questions" : "question"
        let item_count_string = "This Test Contains \(item_count) \(question_string)."
        examQuestionLabel.text = item_count_string
        examQuestionLabel.highlightString(item_count, size: 21, color: highlightColor)
        
        /*
         You have ___ minutes to finish this test
         NOTE: minutes | minute <= dynamic
         */
        
        //TIME LIMIT
        let time_remaining = (userData.value(forKey: "time_remaining")! as AnyObject).description
        let time_limit = self.stringTimeFromSecondsString(time_remaining!);
        let time_limit_string = "You have \(time_limit) to finish this test."
        examTimeLimitLabel.text = time_limit_string
        examTimeLimitLabel.highlightString(time_limit, size: 21, color: highlightColor)
        
        //        let time_remaining = (userData.valueForKey("time_remaining")!.doubleValue)! as NSTimeInterval
        //        self.timerComponent?.setCountDownTime(time_remaining)
        
        /*
         The passing score of this test is ____
         NOTE: There is NO passing score for this test
         */
        
        //EXAM SCORE
        let total_score = (userData.value(forKey: "passing_rate")! as AnyObject).description
        let passing_score = ( Int(total_score!)! > 0 ) ? total_score : "NO"
        let with_passing_score_string = "The Passing score of this test is \(passing_score!)."
        let without_passing_score_string = "There is \(passing_score!) passing score for this test."
        examScoreLabel.text = ( Int(total_score!)! > 0 ) ? with_passing_score_string : without_passing_score_string
        examScoreLabel.highlightString(passing_score!, size: 21, color: highlightColor )
        
        /*
         You can take this test ___ more time
         NOTE: There is NO passing score for this test
         */
        
        //EXAM COUNT
        let attempt_count_string = (userData.value(forKey: "attempts")! as AnyObject).description
        let attempt_count_int = ( Int(attempt_count_string!)! + 1 )
        let number_attempts = "\(attempt_count_int)"
        let time_message_string = ( attempt_count_int > 1 ) ? "times" : "time"
        let attempt_string = "You can take this exam \(number_attempts) \(time_message_string)."
        examCountLabel.text = attempt_string
        examCountLabel.highlightString(number_attempts, size: 21, color: highlightColor)
    }
    
    func stringTimeFromSecondsString(_ secondsStr: String) -> String{
        let ti: TimeInterval = TimeInterval(Int(secondsStr)!)
        let seconds : Int = Int(ti.truncatingRemainder(dividingBy: 60))
        let minutes : Int = Int((ti / 60).truncatingRemainder(dividingBy: 60))
        let hours : Int = Int((ti / 3600))
        
        return String(format: "%02ld:%02ld:%02ld", hours, minutes, seconds);
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func customizedSubViews() {

        let imageView = UIImageView(image: UIImage(named: "course_player_background.png"))
        imageView.frame = self.tableView.frame
        self.tableView.backgroundView = imageView
        self.tableView.tableFooterView = footerView
        
        courseTitleLabel.customShadow(5.0)
        courseTitleLabel.customShadow(5.0)
        instructionTitleLabel.customShadow(5.0)
        instructionDescriptionLabel.customShadow(5.0)
        startButton.customShadow(5.0)
    }
    
    func viewButtonAction(_ sender:UIButton!) {
        print("view button action press")
        let indicatorStr = NSLocalizedString("Processing", comment: "")
        HUD.showUIBlockingIndicator(withText: indicatorStr)
        
        let cs_id: String! = self.dataManager.stringValue(self.dataManager.fetchObject(forKey: kCP_SELECTED_CS_ID))
        let quiz_id: String! = self.dataManager.stringValue(self.dataManager.fetchObject(forKey: kCP_SELECTED_QUIZ_ID))
        
        let userData: [String:String] = ["cs_id":cs_id!,
                        "quiz_id":quiz_id!]
        
//        self.dataManager.requestRunOrPrerun("run", parameter: userData, dataBlock: nil)
        self.dataManager.requestRunOrPrerun("run", parameter: userData) { (data) in
            if data != nil {
                DispatchQueue.main.async(execute: {
                    _ = [HUD .hideUIBlockingIndicator()]
                    _ = self.navigationController?.popViewController(animated: false)
                })
                self.delegate?.didClickStartButton(true)
            } else {
                _ = [HUD .hideUIBlockingIndicator()]
                let submitMessage = NSLocalizedString("Error in starting the test, please try again", comment: "")// todo localize
                
                let alert = UIAlertController(title: "", message: submitMessage, preferredStyle: .alert)
                
                let yesTitle = NSLocalizedString("Okay", comment: "")
                
                let yesAction = UIAlertAction(title: yesTitle, style: .cancel, handler: nil)
                
                alert.addAction(yesAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
}
