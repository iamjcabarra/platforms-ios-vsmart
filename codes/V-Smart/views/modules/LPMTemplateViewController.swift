//
//  LPMTemplateViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 04/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMTemplateViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, NSFetchedResultsControllerDelegate {

    @IBOutlet fileprivate var collectionView: UICollectionView!
    
    var courseID: String!
    
    fileprivate let kTemplateCellIdentifier = "template_cell_identifier"
    fileprivate let kMainActionViewSegueIdentifier = "SHOW_MAIN_ACTION_VIEW"
    fileprivate let kLessonCreatorViewSegueIdentifier = "SHOW_LESSON_CREATOR_VIEW"
    
    fileprivate var blockOperations: [BlockOperation] = []
    fileprivate var selectedCellIndexPath: IndexPath? = nil
    fileprivate var lessonObject: NSManagedObject!
    
    // MARK: - Data Managers
    
    fileprivate lazy var lessonPlanDataManager: LessonPlanDataManager = {
        let lpdm = LessonPlanDataManager.sharedInstance()
        return lpdm!
    }()
    
    fileprivate lazy var lpmDataManager: LPMDataManager = {
        let lpdm = LPMDataManager.sharedInstance
        return lpdm
    }()
    
    fileprivate lazy var isVersion25:Bool = {
        let version:CGFloat = Utils.getServerInstanceVersion() as! CGFloat
        return (version >= LPMConstants.Server.MAX_VERSION)
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.allowsSelection = true
        self.collectionView.allowsMultipleSelection = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.customizeNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Custom Navigation Bar
    
    func customizeNavigationBar() {
        self.title = NSLocalizedString("Lesson Plan Templates", comment: "")
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "#5ABE3E")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
    }
    
    // MARK: - Collection View Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        var count:Int = sectionData.numberOfObjects
        
        //BACKWARD COMPATIBILITY
        count = (self.isVersion25 == true) ? sectionData.numberOfObjects : 1
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let templateObject = self.fetchedResultsController.object(at: indexPath) 
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: self.kTemplateCellIdentifier, for: indexPath) as! LPMTemplateCollectionViewCell
        let name = templateObject.value(forKey: "name") as! String
        
        cell.templateLabel.text = name
        cell.highlightSelectedCell(indexPath == self.selectedCellIndexPath)
        
        return cell
    }
    
    // MARK: - Collection View Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! LPMTemplateCollectionViewCell
        
        if (self.selectedCellIndexPath != nil) {
            let previouslySelectedCell = self.collectionView.cellForItem(at: self.selectedCellIndexPath!) as! LPMTemplateCollectionViewCell
            previouslySelectedCell.highlightSelectedCell(false)
        }
        
        cell.highlightSelectedCell(true)
        self.selectedCellIndexPath = indexPath
        
        let templateObject = self.fetchedResultsController.object(at: indexPath) 
        self.showCreateLessonViewUsingTemplate(templateObject)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! LPMTemplateCollectionViewCell
        cell.highlightSelectedCell(false)
        self.selectedCellIndexPath = nil
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let context = self.lessonPlanDataManager.mainContext
        
//        let fetchRequest = NSFetchRequest()
//        let entity = NSEntityDescription.entity(forEntityName: kLessonTemplateEntity, in: context)
//
//        fetchRequest.entity = entity
//        fetchRequest.fetchBatchSize = 20
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kLessonTemplateEntity)
        
        let descriptorSelector = #selector(NSString.localizedCaseInsensitiveCompare(_:))
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true, selector: descriptorSelector)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context!, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch _ as NSError {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    fileprivate func addProcessingBlock(_ processingBlock:@escaping (Void)->Void) {
        self.blockOperations.append(BlockOperation(block: processingBlock))
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.blockOperations.removeAll(keepingCapacity: false)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.insertItems(at: [newIndexPath]) }
            
        case .update:
            guard let newIndexPath = newIndexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.reloadItems(at: [newIndexPath]) }
            
        case .move:
            guard let indexPath = indexPath else { return }
            guard let newIndexPath = newIndexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.moveItem(at: indexPath, to: newIndexPath) }
            
        case .delete:
            guard let indexPath = indexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.deleteItems(at: [indexPath]) }
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.addProcessingBlock { [weak self] in self?.collectionView.insertSections(IndexSet(integer: sectionIndex)) }
            
        case .update:
            self.addProcessingBlock { [weak self] in self?.collectionView.reloadSections(IndexSet(integer: sectionIndex)) }
            
        case .delete:
            self.addProcessingBlock { [weak self] in self?.collectionView.deleteSections(IndexSet(integer: sectionIndex)) }
            
        default: break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.collectionView.performBatchUpdates({
            self.blockOperations.forEach { $0.start() }
            }, completion: { finished in
                self.blockOperations.removeAll(keepingCapacity: false)
        })
    }
    
    // MARK: - Create Lesson Action
    
    fileprivate func showCreateLessonViewUsingTemplate(_ templateObject: NSManagedObject) {
        guard let template = templateObject.value(forKey: "id") as? String, let name = templateObject.value(forKey: "name") else {
            print("can't understand template")
            return
        }
        
        self.lpmDataManager.save(object: template as AnyObject!, forKey: "LPM_USER_DEFAULT_TEMPLATE_ID")
        self.lpmDataManager.save(object: name as AnyObject!, forKey: "LPM_USER_DEFAULT_NAVIGATION_TITLE")
        self.lpmDataManager.save(object: "0" as AnyObject!, forKey: "LPM_USER_DEFAULT_IS_VIEW_MODE")
        
        if template == LPMConstants.Template.UBD {
            let indicatorString = "\(NSLocalizedString("Please wait", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
            
            self.lessonPlanDataManager.requestLessonTemplateForLessonTemplate(withID: template) { (success) in
                if (success) {
                    if let templateObject = self.lessonPlanDataManager.getEntity(kTemplateEntity, with: nil) {
                        self.lessonPlanDataManager.prepareLessonObject(
                            forAction: LPMLessonActionType.add.rawValue,
                            copy: templateObject,
                            addData: ["course_id": self.courseID],
                            objectBlock: { (lessonObject) in
                                
                                if (lessonObject != nil) {
                                    DispatchQueue.main.async(execute: {
                                        HUD.hideUIBlockingIndicator()
                                        self.lessonObject = lessonObject
                                        self.performSegue(withIdentifier: self.kMainActionViewSegueIdentifier, sender: nil)
                                    })
                                }
                                else {
                                    DispatchQueue.main.async(execute: {
                                        HUD.hideUIBlockingIndicator()
                                        let message = NSLocalizedString("You cannot create a new lesson plan this time. Please try again later.", comment: "")
                                        self.showNotificationMessage(message)
                                    })
                                }
                        })
                    }
                }
                else {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        let message = NSLocalizedString("You cannot create a new lesson plan this time. Please try again later.", comment: "")
                        self.showNotificationMessage(message)
                    })
                }
            }
        }
        
        if template == LPMConstants.Template.DLL || template == LPMConstants.Template.SMD {
            let indicatorString = "\(NSLocalizedString("Please wait", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
            
            self.lpmDataManager.requestDetailsForLessonTemplate(template, handler: { (doneBlock) in
                if doneBlock {
                    self.lpmDataManager.setUpDefaultLessonOverviewForCourse(self.courseID, template: templateObject, handler: { (doneBlock) in
                        if doneBlock {
                            DispatchQueue.main.async(execute: {
                                HUD.hideUIBlockingIndicator()
                                self.performSegue(withIdentifier: self.kLessonCreatorViewSegueIdentifier, sender: nil)
                            })
                        }
                        else {
                            DispatchQueue.main.async(execute: {
                                HUD.hideUIBlockingIndicator()
                                let message = NSLocalizedString("You cannot create a new lesson plan this time. Please try again later.", comment: "")
                                self.showNotificationMessage(message)
                            })
                        }
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        let message = NSLocalizedString("You cannot create a new lesson plan this time. Please try again later.", comment: "")
                        self.showNotificationMessage(message)
                    })
                }
            })
        }
    }
    
    // MARK: - Alert View Message
    
    fileprivate func showNotificationMessage(_ message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: NSLocalizedString("Okay", comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == self.kMainActionViewSegueIdentifier {
            let mainActionView = segue.destination as? LPMMainActionViewController
            mainActionView?.actionType = LPMLessonActionType.add
            mainActionView?.lessonObject = self.lessonObject
        }
        
        if segue.identifier == self.kLessonCreatorViewSegueIdentifier {
            let lessonCreatorView = segue.destination as? LPMLessonCreatorViewController
            lessonCreatorView?.actionType = LPMLessonActionType.add
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }

}
