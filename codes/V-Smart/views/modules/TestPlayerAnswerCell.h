//
//  TestPlayerAnswerCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 06/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestPlayerAnswerCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *buttonContainer;
@property (strong, nonatomic) IBOutlet UILabel *answerLabel;

@end
