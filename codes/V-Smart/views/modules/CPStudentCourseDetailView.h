//
//  CPStudentCourseDetailView.h
//  V-Smart
//
//  Created by Julius Abarra on 29/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPStudentCourseDetailView : UIViewController

@property (strong, nonatomic) NSManagedObject *courseObject;

@end
