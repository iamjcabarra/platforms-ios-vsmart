//
//  StudentGradebookCell.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 08/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class StudentGradebookCell: UITableViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var viewCellFrame: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        CALayer *layer = self.viewCellFrame.layer;
        self.viewCellFrame.layer.borderColor = UIColor.lightGray.cgColor
        self.viewCellFrame.layer.borderWidth = 1.0;
        
        self.imageView?.contentMode = UIViewContentMode.scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
