//
//  TextbookViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/28/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#import "TextbookViewController.h"
#import "BookItemCell.h"
#import "HUD.h"
#import "Book.h"
#import "AssessmentListViewController.h"

@interface TextbookViewController () <NSFetchedResultsControllerDelegate>
{
    AQGridView *_gridView;
    NSDateFormatter *formatter;
}
@property (nonatomic, strong) UIDocumentInteractionController *documentInteractionController;
@property (nonatomic, strong) AssessmentListViewController *assessmentController;
@property (nonatomic, strong) NSFetchedResultsController *frc;
@property (nonatomic, strong) ResourceManager *rm;
@property (nonatomic, strong) NSString *useridPATH;
@end

@implementation TextbookViewController
@synthesize gridView=_gridView;

dispatch_queue_t _mainQueue;
dispatch_semaphore_t semaphore; //FLAG for waiting

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
//    _mainQueue = dispatch_queue_create("com.vibetechnologies.gcd.local", NULL);
    _mainQueue = dispatch_queue_create("com.vibetechnologies.gcd.local", DISPATCH_QUEUE_SERIAL);
    semaphore = dispatch_semaphore_create(1);//One thread at a time for mutable array update
    
	// Do any additional setup after loading the view.
    //self.view.backgroundColor = UIColorFromHex(0xeeeeee);
    self.view.backgroundColor = [UIColor whiteColor];
    
    TapDetectingWindow *tapWindow = (TapDetectingWindow *)[[UIApplication sharedApplication].windows objectAtIndex:0];
    tapWindow.controllerThatObserves = nil;
    tapWindow.viewToObserve = nil;
    
    [self setupTextbookGrid];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
    
    self.assessmentController = [[AssessmentListViewController alloc] init];
}

-(void) setupTextbookGrid {
    VLog(@"Loading ...");
    
//    [ProgressHUD show:@"Checking for books migration ..." Interaction:NO];
//    [Utils moveFilesFromCacheToApplicationSupport];
//    [ProgressHUD dismiss];
    
//    VLog(@"baseProfileView: %@", NSStringFromCGRect([super profileSize]));
//    VLog(@"baseHeaderView: %@", NSStringFromCGRect([super headerSize]));
//    VLog(@"Textview Size: %@", NSStringFromCGSize(self.view.frame.size));
    
    //self.gridView = [[AQGridView alloc] initWithFrame:CGRectMake(0, 310, self.view.frame.size.width, 638 + 56)];
    float profileY = [super profileHeight];
    float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    //float gridHeight = 638 + [super profileSize].size.height;
    
    float gridY = [super headerSize].size.height + profileY;
//    VLog(@"profileY: %.02f", profileY);
//    VLog(@"gridHeight: %.02f", gridHeight);
//    VLog(@"gridHeight-Profile: %.02f", [super profileSize].size.height);
//    VLog(@"gridHeight-Header: %.02f", [super headerSize].size.height);
//    VLog(@"gridHeight-Toolbar: %.02f", [super toolbarSize].size.height);
//    
//    VLog(@"gridY: %.02f", gridY);
    
    // gridHeight + [super profileSize].size.height + [super toolbarSize].size.height)
    self.gridView = [[AQGridView alloc] initWithFrame:CGRectMake(0, gridY, self.view.frame.size.width, gridHeight + 44)];
    self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	self.gridView.autoresizesSubviews = YES;
	self.gridView.delegate = self;
	self.gridView.dataSource = self;
    //[self.gridView setBounces:YES];
    
    __weak TextbookViewController *weakSelf = self;
    
    [self.gridView addPullToRefreshWithActionHandler:^{
        VLog(@"Added PullToRefresh");
        [weakSelf _refreshBooksOnShelf];
    }];
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget: self action: @selector(showPopupDeleteBookInfo:)];
    longPressGesture.minimumPressDuration = 0.5;
    longPressGesture.delegate = self;
    [self.gridView addGestureRecognizer:longPressGesture];
    
//    UIImage *patternTile = [UIImage imageNamed: @"bookshelf"];
//	UIView *backgroundView = [[UIView alloc] init];
//	backgroundView.backgroundColor = [UIColor colorWithPatternImage: patternTile];
//	self.gridView.backgroundView = backgroundView;
    self.gridView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.gridView];
    
    // CORE DATA SPECIFIC
    [self configureFetch];
    [self performFetch];
//    [self.gridView reloadData];
    [self _loadBooks];
    [self _setupNotifications];
    
    NSLog(@"%@", NSStringFromCGSize(self.gridView.contentSize));
}

- (NSString *) dashboardName {
    
    NSString *moduleName = NSLocalizedString(@"Textbook", nil);
    return moduleName;
//    return kModuleTextbooks;
}

//-(void) adjustModuleView {
//    VS_NCPOST(kNotificationProfileHeight)
//}

-(void) _setupNotifications {
    VS_NCADD(kNotificationUpdateQueue, @selector(_downloadProgressUpdate:))
    VS_NCADD(kNotificationProfileHeight, @selector(_refreshGridSize:))
    VS_NCADD(kNotificationTextbookShelfSelection, @selector(_displayQuizResultAction:))
    
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(closeReader)
												 name:@"org.vibalfoundation.vibereader.close"
											   object:nil];
}

#pragma mark - Notification Receivers Action
-(void) animateClosingReader {
	CGRect theFrame = detailViewController.view.frame;
    theFrame.origin = CGPointMake(self.view.frame.size.width, 0);
    
    [UIView animateWithDuration:0.5 animations:^{
        detailViewController.view.frame = theFrame;
        detailViewController.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self unloadReader];
    }];
}

-(void) unloadReader {
    if (detailViewController != nil) {
		[detailViewController.view removeFromSuperview];
		//[detailViewController release];
		detailViewController = nil;
        
        [self _loadBooks];
        [self refreshMedals];
        [self hideToolbar:NO];
	}
}

-(void) closeReader
{
    [Utils saveReaderState:NO];
    
    // Allow to refresh books to remove the "New" overlayed image.
    //[self refresh];
    //[self reloadGridForIndexPath:nil];
    [self animateClosingReader];
	//[self performSelectorOnMainThread:@selector(animateClosingReader) withObject:nil waitUntilDone:NO];
}

-(void) _refreshBooksOnShelf {
    VLog(@"Refreshing books on shelf");
    //[self _loadBooks];
//    [HUD showUIBlockingIndicatorWithText:MSG_BOOKS_FETCHING];
    [[VSmart sharedInstance] fetchBooks:^(BookListResponse *booklistResponse) {
//        [HUD hideUIBlockingIndicator];
        [self updateResourceWithItems:booklistResponse.result];
    } failureBlock:^(NSError *error, NSData *responseData) {
//        NSString *errorMessage = [[error userInfo] objectForKey:NSLocalizedDescriptionKey];
//        [HUD hideUIBlockingIndicator];
//        [SVProgressHUD showErrorWithStatus:errorMessage];
        __weak TextbookViewController *weakSelf = self;
        [weakSelf.gridView.pullToRefreshView stopAnimating];
    }];
}

-(void) _displayQuizResultAction: (NSNotification *) notification {
    Book *item = (Book *) [notification object];
    
    NSString *bookName = VS_FMT(@"%@", item.bookId);

    NSArray *items = [VSmartHelpers getBookExerciseResults:bookName];
    //VLog(@"%@: BookResult: %@", bookName, items);
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateCreated"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [items sortedArrayUsingDescriptors:sortDescriptors];
    
    popQuizMedalViewController = [[QuizMedalViewController alloc] initWithNibName:@"QuizMedalViewController" bundle:nil];
    popQuizMedalViewController.view.frame = popQuizMedalViewController.view.frame;
    popQuizMedalViewController.delegate = self;
    popQuizMedalViewController.items = sortedArray;
    
    [self presentPopupViewController:popQuizMedalViewController animationType:PopupViewAnimationSlideBottomTop];
}

-(void) _downloadProgressUpdate: (NSNotification *) notification
{
//    NSDictionary *item = (NSDictionary *)[notification object];
//    [self _updateCellOnGrid:item];
    [self updateWithNotificationData: (NSDictionary *)[notification object] ];
}

- (void)updateWithNotificationData:(NSDictionary *)data
{
    dispatch_async(_mainQueue, ^{
        NSMutableDictionary *item = [NSMutableDictionary dictionaryWithDictionary:data];
        item[@"user_id"] = [NSString stringWithFormat:@"%@",self.useridPATH];
        VSmartDownloadStatus status = [ data[@"download_status"] intValue];
        if (status == VSmartDownloadError) {
            
            item[@"download_active"] = @NO;
            item[@"progress"] = [NSNumber numberWithFloat:0];
            item[@"download_status"] = [NSNumber numberWithUnsignedInteger:VSmartDownloadNotStarted];

            Book *book = data[@"book"];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                NSString *errorMessage = VS_FMT(MSG_BOOKS_DOWNLOAD_ERROR, book.title);
                [super alertMessage:errorMessage];
            });
        }
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        [self.rm updateResourceWithObject:item];
        dispatch_semaphore_signal(semaphore);
    });
}

-(void) _refreshGridSize: (NSNotification *) notification {
    VLog(@"Received: kNotificationProfileHeight");
    //[self.gridView removeFromSuperview];
    //[self setupTextbookGrid];
    
    [UIView animateWithDuration:0.45 animations:^{
        float profileY = [super profileHeight];
        float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
        //float gridHeight = 638 + [super profileSize].size.height;
        
        float gridY = [super headerSize].size.height + profileY;
        [self.gridView setFrame:CGRectMake(0, gridY, self.view.frame.size.width, gridHeight + 44)];
        [self.gridView setNeedsLayout];
        
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            [super adjustProfileHeight];
        }
        [super showOrHideMiniAvatar];
        
    } completion:^(BOOL finished) {
        VLog(@"Received: Finished");
        [self.gridView setContentSize:CGSizeMake(self.gridView.contentSize.width, self.gridView.frame.size.height + 10)];
        //[self.gridView setNeedsLayout];
    }];
    
}

-(void) _updateCellOnGrid: (NSDictionary *) item {
//    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
    dispatch_async(_mainQueue, ^(void){

        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        Book *book = (Book *) [item objectForKey:@"book"];
        int bookIndex = [self bookIndexForBookId:book.bookId];
        [self.books setObject:item atIndexedSubscript:bookIndex];
        int row = [[item objectForKey:@"row"] intValue];
        dispatch_semaphore_signal(semaphore);
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if ([[item objectForKey:@"download_status"] intValue] == VSmartDownloadError) {
                NSString *errorMessage = VS_FMT(MSG_BOOKS_DOWNLOAD_ERROR, book.title);
                [super alertMessage:errorMessage];
            }
            [self performSelector:@selector(_refreshGrid:) withObject:[NSNumber numberWithInt:row] afterDelay:0.3];
        });
    });
}

-(void) _refreshGrid: (NSNumber *) rowIndex {
    int row = [rowIndex intValue];
    BOOL exists = [[self.gridView visibleCellIndices] containsIndex:row];
    //VLog(@"Exists: %@: %i", exists ? @"YES" : @"NO", row);
    
    if(exists) {
        [self.gridView beginUpdates];

        [self.gridView reloadItemsAtIndices:[NSIndexSet indexSetWithIndex:row]
                              withAnimation:AQGridViewItemAnimationNone];
        [self.gridView endUpdates];
    }
}

-(void) _loadBooks {

//    NSString *userBooks = VS_FMT(kGlobalBooks, [self account].user.id);
    NSString *userBooks = VS_FMT(kGlobalBooks, [self account].user.username);
//    VLog(@"_loadBooks: %@", userBooks);
    NSLog(@"_loadBooks: %@", userBooks);
    NSArray *cachedBooks = [Utils getArchive:userBooks];
//    NSArray *cachedBooks = nil;
    if (!cachedBooks) {
//        [HUD showUIBlockingIndicatorWithText:MSG_BOOKS_FETCHING];
        [[VSmart sharedInstance] fetchBooks:^(BookListResponse *booklistResponse) {
//            [HUD hideUIBlockingIndicator];
            [self updateResourceWithItems:booklistResponse.result];
        } failureBlock:^(NSError *error, NSData *responseData) {
//            [HUD hideUIBlockingIndicator];
            [self updateResourceWithItems:cachedBooks];
        }];
    } else {
        [self updateResourceWithItems:cachedBooks];
    }
}

-(void) _buildBooksData: (NSArray *) sourceBooks {
    self.books = [[NSMutableArray alloc] init];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        for (Book *book in sourceBooks) {
            VSmartDownloadStatus downloadStatus = VSmartDownloadNotStarted;
            
            NSString *bookIdentifier = [NSString stringWithFormat:@"%@",book.bookId];
            
            if ([VSmartHelpers isBookOnShelf:bookIdentifier]) {
                downloadStatus = VSmartDownloadExtracted;
            }

            NSDictionary *item = @{@"download_active": @NO, @"book_id": bookIdentifier,
                                   @"book": book, @"progress": [NSNumber numberWithFloat:0],
                                   @"download_status": [NSNumber numberWithUnsignedInteger:downloadStatus]};
            [self.books addObject:item];
        }
        
        // For troubleshooting only
        //[VSmartHelpers clearImportedBooks];
        
        // Add imported books too
        NSArray *importedBooks = [VSmartHelpers getImportedBooks];
        VLog(@"BOOK_IMPORTED: %i", [importedBooks count]);
        for (Book *book in importedBooks) {
            VSmartDownloadStatus downloadStatus = VSmartDownloadImported;

            NSString *bookIdentifier = [NSString stringWithFormat:@"%@",book.bookId];

            VLog(@"IMPORTED: %@", book.title);
            NSDictionary *item = @{@"download_active": @NO, @"book_id": bookIdentifier,
                                   @"book": book, @"progress": [NSNumber numberWithFloat:0],
                                   @"download_status": [NSNumber numberWithUnsignedInteger:downloadStatus]};
            //VLog(@"Book Import: %@", item);
            [self.books addObject:item];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [self.gridView reloadData];
            
            [self.gridView setContentSize:CGSizeMake(self.gridView.contentSize.width, self.gridView.contentSize.height + 10)];
            
            __weak TextbookViewController *weakSelf = self;
            [weakSelf.gridView.pullToRefreshView stopAnimating];
        });
    });
}

- (void)updateResourceWithItems:(NSArray *)items
{
    if ([items count] > 0) {
        
        for (Book *book in items) {
            VSmartDownloadStatus downloadStatus = VSmartDownloadNotStarted;
            NSString *bookIdentifier = [NSString stringWithFormat:@"%@",book.bookId];
            if ([VSmartHelpers isBookOnShelf:bookIdentifier]) {
                downloadStatus = VSmartDownloadExtracted;
            }
            NSDictionary *item = @{@"download_active": @NO, @"book_id": bookIdentifier,
                                   @"book": book, @"progress": [NSNumber numberWithFloat:0],
                                   @"download_status": [NSNumber numberWithUnsignedInteger:downloadStatus],
                                   @"user_id" : self.useridPATH};
            [self.rm updateResourceWithObject:item];
        }
    }
    
    NSArray *importedBooks = [VSmartHelpers getImportedBooks];
    if ([importedBooks count] > 0) {
        
        for (Book *book in importedBooks) {
            VSmartDownloadStatus downloadStatus = VSmartDownloadImported;
            NSString *bookIdentifier = [NSString stringWithFormat:@"%@",book.bookId];
            NSDictionary *item = @{@"download_active": @NO, @"book_id": bookIdentifier,
                                   @"book": book, @"progress": [NSNumber numberWithFloat:0],
                                   @"download_status": [NSNumber numberWithUnsignedInteger:downloadStatus],
                                   @"user_id" : self.useridPATH};
            
//            [self.rm updateResourceWithObject:item];
            [self.rm updateResourceWithObject:item import:YES];
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        __weak TextbookViewController *weakSelf = self;
        [weakSelf.gridView.pullToRefreshView stopAnimating];
    });
}

-(void) _fetchDownloadedBooks {
    NSArray *downloadedBooks = [VSmartHelpers getDownloadedBooks];
    VLog(@"Downloaded Books: %@", downloadedBooks);
}

- (NSDictionary *) _getProgressCompletedQuiz: (Book *) book {
    NSString *bookName = VS_FMT(@"%@", book.bookId);
    NSArray *items = [VSmartHelpers getBookExerciseResults:bookName];
    
    NSUInteger quizTaken = [items count];
    NSDictionary *quizData = [VSmartHelpers getBookExerciseData:bookName];
    //VLog(@"[%@] quizData: %@", bookName, quizData);
    
    NSArray *exerciseData = [quizData objectForKey:@"Exercises"];
    NSUInteger quizTotal = [exerciseData count];
    
    float quizCompleted = (float) quizTaken / (float)quizTotal;
    
    if (isnan(quizCompleted)){
        quizCompleted = 0;
    }
    
    NSString *completedLabel = NSLocalizedString(@"completed", nil);
    NSString *text = VS_FMT(@"%.2f%% %@ (%lu / %lu)", (float)(quizCompleted * 100), completedLabel, (unsigned long)quizTaken, (unsigned long)quizTotal);
    NSDictionary *item = @{@"progress": [NSNumber numberWithFloat:quizCompleted], @"text": text};
    
    return item;
}

- (NSString *) _computeAverage: (Book *) book {
    NSString *bookName = VS_FMT(@"%@", book.bookId);
    NSArray *items = [VSmartHelpers getBookExerciseResults:bookName];
    
    float quizTotal = 0;
    float quizResultTotal = 0;
    
    for (BookExerciseResult *result in items) {
        quizResultTotal += (float) result.result;
        quizTotal += (float) result.bookExercise.totalItems;
    }
    
    float averageCompleted = 0.0f;
    if(quizTotal != 0) {
        averageCompleted = (float) (quizResultTotal / quizTotal) * 50 + 50;
    }

    NSString *runningProgressLabel = NSLocalizedString(@"running progress", nil);
    return VS_FMT(@"%0.2f%% %@", averageCompleted, runningProgressLabel);
    
//    return VS_FMT(@"%0.2f%% running progress", averageCompleted);
}

-(void) testAlert: (id) sender {
    [self.view makeToast:@"Demo"];
}

#pragma mark - Long Press UIGesture Recognizer Delegate
-(void) showPopupDeleteBookInfo: (UIGestureRecognizer *) recognizer
{
    switch ( recognizer.state )
    {
        default:
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStatePossible:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded:
            // do nothing
            break;
        case UIGestureRecognizerStateBegan:
        {
            NSUInteger index = [self.gridView indexForItemAtPoint:[recognizer locationInView:self.gridView]];
            NSManagedObject *item = [self.frc objectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            VSmartDownloadStatus status = (VSmartDownloadStatus)[[item valueForKey:@"download_status"] intValue];
            if (status == VSmartDownloadExtracted || status == VSmartDownloadImported) {
                Book *book = (Book *)[item valueForKey:@"book"];
                VLog(@"Delete Book: %@", book.title);
                [self showPopupBookInfo:book forIndex:index withBookState:kBookStateDelete];                
            }
            break;
        }
    }
}

- (BOOL) gestureRecognizerShouldBegin: (UIGestureRecognizer *) gestureRecognizer
{
    CGPoint location = [gestureRecognizer locationInView:self.gridView];
    int bookCount = [[self.frc fetchedObjects] count];
    if ( [self.gridView indexForItemAtPoint:location] < bookCount )
        return ( YES );
    
    // touch is outside the bounds of any icon cells, so don't start the gesture
    return ( NO );
}

#pragma mark - Grid View Data Source

- (NSUInteger) numberOfItemsInGridView: (AQGridView *) aGridView
{
//    return _books.count;
//    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    return [[self.frc fetchedObjects] count];
}

- (AQGridViewCell *) gridView: (AQGridView *) aGridView cellForItemAtIndex: (NSUInteger) index
{
    NSManagedObjectContext *mo = [self.frc objectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    
    static NSString *CellIdentifier = @"BookCellIdentifier";
    
//    AQGridViewCell * cell = nil;
    BookItemCell * itemCell = (BookItemCell *)[aGridView dequeueReusableCellWithIdentifier: CellIdentifier];
//    NSDictionary *item = [self.books objectAtIndex:index];
//    VLog(@"BOOK_ITEM: %@", item);
//    Book *book = (Book *)[item objectForKey:@"book"];
    Book *book = (Book *)[mo valueForKey:@"book"];
    //BOOL isDownloadActive = [[item objectForKey:@"download_active"] boolValue];
//    VSmartDownloadStatus downloadStatus = [[item objectForKey:@"download_status"] intValue];
    VSmartDownloadStatus downloadStatus = [[mo valueForKey:@"download_status"] intValue];
    
    if (itemCell == nil)
    {
        CGRect cellFrameObject = CGRectMake(0.0, 0.0, 384.0, 228.0);
        itemCell = [[BookItemCell alloc] initWithFrame:cellFrameObject reuseIdentifier: CellIdentifier];
        itemCell.selectionStyle = AQGridViewCellSelectionStyleNone;
        //itemCell.layer.borderWidth = 1;
        //itemCell.layer.borderColor = [UIColor redColor].CGColor;
        itemCell.backgroundColor = [UIColor clearColor];
        //[itemCell.progressView hide];
        //VLog(@"dequeue");
        //[itemCell setProgressVisibility:isDownloadActive];
    }
    
    //CGRect rect = CGRectMake(0, 0, 135, 176);
    
    itemCell.book = book;
    itemCell.title = book.title;
    itemCell.author = [VSmartHelpers getAuthors:book.authors];

//    if (downloadStatus == VSmartDownloadImported) {
//        [itemCell setImage:[UIImage imageWithContentsOfFile:book.cover]];
//    } else {
//        [itemCell setCover:book.cover];
//    }
    
    NSData *binary_data = [mo valueForKey:@"thumbnail"];
    if (binary_data != nil) {
        [itemCell setImage:[UIImage imageWithData:binary_data]];
    }

//    VLog(@"CoverImage: %@", book.coverImage);
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    
    if(downloadStatus == VSmartDownloadNotStarted || downloadStatus == VSmartDownloadError || downloadStatus == VSmartDownloadCompleted) {
        [itemCell setProgressVisibility:NO];
        [itemCell setCloudVisibility:YES];
        [itemCell setBookProgressVisibility:NO];
    } else if (downloadStatus == VSmartDownloadInProgress || downloadStatus == VSmartDownloadZipExtraction) {
        [itemCell setProgressVisibility:YES];
        [itemCell setCloudVisibility:YES];
        [itemCell setBookProgressVisibility:NO];
//        CGFloat progress = (CGFloat)[[item objectForKey:@"progress"] floatValue];
        CGFloat progress = (CGFloat)[[mo valueForKey:@"progress"] floatValue];
        VLog(@"Downloading... %f", progress);
        [itemCell setDownloadProgress:progress];
    } else if (downloadStatus == VSmartDownloadImported) {
        [itemCell setProgressVisibility:NO];
        [itemCell setCloudVisibility:NO];
        [itemCell setCompleted:YES];
    } else if (downloadStatus == VSmartDownloadNotStarted) {
        // Not Started
    } else if (downloadStatus == VSmartDownloadError) {
        // Error
        [itemCell setCompleted:NO];
        VLog(@"Download Error. Resetting");
//        NSDictionary *dict = @{@"download_active": @NO,
//                               @"book_id": book.bookId,
//                               @"book": book,
//                               @"progress": [NSNumber numberWithFloat:0],
//                               @"row": [NSNumber numberWithInt: index ],
//                               @"download_status": [NSNumber numberWithUnsignedInteger:VSmartDownloadNotStarted]};
        NSString *errorMessage = VS_FMT(MSG_BOOKS_DOWNLOAD_ERROR, book.title);
        [super alertMessage:errorMessage];
        //        [self _updateCellOnGrid:dict];
    } else if (downloadStatus == VSmartDownloadZipExtraction) {
        // Zip Extraction
    } else if (downloadStatus == VSmartDownloadCompleted) {
        // Completed
    } else if (downloadStatus == VSmartDownloadExtracted) {
        // check read state
        [itemCell setCompleted:YES];
        
        dispatch_async(queue, ^{
            //VLog(@"Extracted Book Id: %i", book.bookId);
            VSmartDownloadBookStatus downloadBookStatus = [VSmartHelpers getBookReadingStatus:book.bookId];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                [itemCell setReadStatus:downloadBookStatus == VSmartDownloadedBookUnread ? NO : YES];
            });
        });
    }
    
    [itemCell setAverageVisibility:(downloadStatus == VSmartDownloadExtracted || downloadStatus == VSmartDownloadImported) ];
    
    if (downloadStatus == VSmartDownloadExtracted || downloadStatus == VSmartDownloadImported) {
        dispatch_async(queue, ^{

            // 1. First we check if book is PDF, then we hide the Progress indicator
            BOOL bookIsPdf = [book.mediaType isIn:kBookExtensionPdf, nil];
            [itemCell setBookProgressVisibility:!bookIsPdf];
            
            BOOL exerciseExists = [Utils isExerciseDataFileExists:VS_FMT(@"%@", book.bookId)];
            NSString *average = [self _computeAverage: book];
            
            NSDictionary *quizProgressData = nil;
            if(exerciseExists) {
                quizProgressData = [self _getProgressCompletedQuiz:book];
            }
            
            // 2. Then let's override to see if the book is EPUB and it contains quizzes?
            [itemCell setBookProgressVisibility:exerciseExists];
            //[formatter setDateFormat:@"dd MMM yyyy hh:mm a"];
            //NSString *dateString = [formatter stringFromDate:book.d];
            NSDictionary *bookInfoFromCollection = [VSmartHelpers getBookInfoFromCollection: book.bookId inCollection:nil];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                [itemCell setAverageText:average];
                [itemCell setAverageVisibility:exerciseExists];
//                [itemCell setDateLastRead:VS_FMT(@"Last Read: %@", [bookInfoFromCollection objectForKey:kBookDateLastRead])];
                
                NSString *lastReadLabel = NSLocalizedString(@"Last Read", nil);
                [itemCell setDateLastRead:VS_FMT(@"%@: %@", lastReadLabel, [bookInfoFromCollection objectForKey:kBookDateLastRead])];
                
                if (exerciseExists) {
                    [itemCell setBookProgress:[[quizProgressData objectForKey:@"progress"] floatValue] withText:[quizProgressData objectForKey:@"text"]];
                }
            });
        });
    }
    
    [itemCell setDateLastReadVisibility:(downloadStatus == VSmartDownloadExtracted || downloadStatus == VSmartDownloadImported)];
    
//    cell = itemCell;
//    return ( cell );
    return itemCell;
}

- (CGSize) portraitGridCellSizeForGridView: (AQGridView *) aGridView
{
    return ( CGSizeMake(384, 228.0) );
}

-(void) gridView:(AQGridView *)gridView didSelectItemAtIndex:(NSUInteger)index
{
    //BookItemCell *cell = (BookItemCell *)[gridView cellForItemAtIndex:index];
    //[cell.contentView makeToast:MSG_BOOKS_FAILED duration:2.0f position:@"center"];
    NSManagedObject *mo = [self.frc objectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    NSManagedObjectContext *ctx = [mo managedObjectContext];
//    BOOL status = [[VSmart sharedInstance] successfullyAuthenticated];
//    VLog(@"authenticated : %@", (status) ? @"YES" : @"NO");
    
    [gridView deselectItemAtIndex:index animated:NO];
    
//    NSDictionary *item = [self.books objectAtIndex:index];
//    VLog(@"Item Selected: %@", item);
//    VLog(@"Book Selected: %@", [item objectForKey:@"book"]);
//    Book *book = [item objectForKey:@"book"];
    Book *book = [mo valueForKey:@"book"];
    
//    VSmartDownloadStatus downloadStatus = [[item objectForKey:@"download_status"] intValue];
    VSmartDownloadStatus downloadStatus = [[mo valueForKey:@"download_status"] intValue];
    
//    if (downloadStatus == VSmartDownloadInProgress) {
////        if (status == NO) {
//            downloadStatus = VSmartDownloadError;
//            [mo setValue:[NSNumber numberWithBool:NO] forKey:@"download_active"];
//            [mo setValue:[NSNumber numberWithFloat:0] forKey:@"progress"];
//            [mo setValue:[NSNumber numberWithUnsignedInteger:downloadStatus] forKey:@"download_status"];
//            [ctx save:nil];
//            
//            dispatch_async(dispatch_get_main_queue(), ^(void){
//                NSString *errorMessage = VS_FMT(MSG_BOOKS_DOWNLOAD_ERROR, book.title);
//                [super alertMessage:errorMessage];
//            });
////        }
//    }
    
    if (downloadStatus == VSmartDownloadImported) {
        [self showPopupBookInfo: book forIndex:index withBookState:kBookStateRead];
    } else {
        if ([VSmartHelpers isBookOnShelf:book.bookId]) {
            [self showPopupBookInfo: book forIndex:index withBookState:kBookStateRead];
        } else {
            if (downloadStatus != VSmartDownloadInProgress) {
//                NSMutableDictionary *dictWithProgress = [[NSMutableDictionary alloc] initWithDictionary:item];
                NSMutableDictionary *dictWithProgress = [NSMutableDictionary dictionary];
                NSArray *attributes = [mo.entity.attributesByName allKeys];
                for (NSString *key in attributes) {
                    [dictWithProgress setObject:[mo valueForKey:key] forKey:key];
                }
                [dictWithProgress setObject:[NSNumber numberWithInteger:index] forKey:@"row"];
                //VLog(@"dictWithProgress: %@", dictWithProgress);
                
                /* localizable strings */
                NSString *contactingServer = NSLocalizedString(@"contacting server...", nil); //checked
                [self.view makeToast:contactingServer duration:2.0f position:@"center"];
                
                //Ensure this MO in the FRC will have the value of VSmartDownloadInProgress
//                if (status == YES) {
                    NSNumber *progress_status = [NSNumber numberWithInt:VSmartDownloadInProgress];
                    [mo setValue:progress_status forKeyPath:@"download_status"];
                    [ctx save:nil];
                    VS_NCPOST_OBJ(kNotificationQueueDownload, dictWithProgress)
//                }
            }
        }
    }
}

#pragma mark - Delegate Actions
-(void) didCloseQuizMedalWindow:(QuizMedalViewController *)popViewController {
    //VLog(@"closing...");
    [self dismissPopupViewControllerWithanimationType:PopupViewAnimationSlideBottomBottom];
}

-(void) actionButtonClicked:(BookInfoViewController *)popViewController withAction:(ActionType)actionType withIndex:(int)index andBookId:(NSString *)bookId forBook:(Book *)book {
    
    switch (actionType) {
        case ReadAction:
            [VSmartHelpers updateBookToCollection:bookId];
            
            [self dismissPopupViewControllerWithanimationType:PopupViewAnimationFade];
            //[SVProgressHUD showWithStatus:@"Loading book"];
            //[self reloadGridForIndexPath:index];
            //VLog(@"Book: %@", book);
            
            [self.gridView deselectItemAtIndex:[self.gridView indexOfSelectedItem] animated: YES];
            if ([book.mediaType isIn:kBookExtensionPdf, nil]) {
                NSDictionary *info = @{@"title": VS_FMT(@"%@", book.bookId), @"book_title": book.title};
                [self performSelector:@selector(openPDFReader:) withObject:info afterDelay:.5];
            } else {
                
//                [SVProgressHUD showWithStatus:kLoadingBookMessage maskType:SVProgressHUDMaskTypeClear];
                
                /* localizable strings */
                NSString *loadingBook = NSLocalizedString(@"Loading book", nil); //checked
                [SVProgressHUD showWithStatus:loadingBook maskType:SVProgressHUDMaskTypeClear];
                
                [self performSelector:@selector(openReader:) withObject:bookId afterDelay:.5];
            }
            break;
            
        case DeleteAction: {
            /*
            VLog(@"Delete");
            [self dismissPopupViewControllerWithanimationType:PopupViewAnimationFade];
            [SVProgressHUD showWithStatus:kRemovingBookMessage maskType:SVProgressHUDMaskTypeClear];
            NSDictionary *item = [self.books objectAtIndex:index];
            VLog(@"Item: %@", item);
            VSmartDownloadStatus downloadStatus = [[item objectForKey:@"download_status"] intValue];
            Book *book = [item objectForKey:@"book"];
            if (downloadStatus == VSmartDownloadImported) {
                [VSmartHelpers removeImportedBook:VS_FMT(@"%@", book.bookId)];
            } else {
                [VSmartHelpers removeBookToCollection:book.bookId];
            }
            [self removeBookData:book];
            break;
             */
            VLog(@"Delete");//checked
            [self dismissPopupViewControllerWithanimationType:PopupViewAnimationFade];//checked
//            [SVProgressHUD showWithStatus:kRemovingBookMessage maskType:SVProgressHUDMaskTypeClear];//checked
            
            /* localizable strings */
            NSString *removeBookMessage = NSLocalizedString(@"Removing book", nil); //checked
            [SVProgressHUD showWithStatus:removeBookMessage maskType:SVProgressHUDMaskTypeClear];//checked
            
            NSManagedObject *item = [self.frc objectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            Book *book = [item valueForKey:@"book"];
            VLog(@"Delete Book: %@", book.title);
            VSmartDownloadStatus status = [[item valueForKey:@"download_status"] intValue];
            if (status == VSmartDownloadImported) {
                [VSmartHelpers removeImportedBook:VS_FMT(@"%@", book.bookId)];
            } else {
                [VSmartHelpers removeBookToCollection:book.bookId];
            }
            [self removeBookData:book];

            //update the core data
            [self.rm deleteManagedObject:item];
            
            break;
        }
//        case AssessmentAction:{
//            NSDictionary *item = [self.books objectAtIndex:index];
//            Book *book = [item objectForKey:@"book"];
//            
//            NSArray *array = [[VibeDataManager sharedInstance] fetchCloudBooks];
//            
//            for (CloudBook *item in array) {
//                if ([item.title isEqualToString:book.title]){
//                    
//                    [self.assessmentController initializeList:item.uuid];
//                    [self dismissPopupViewControllerWithanimationType:PopupViewAnimationFade];
//                    [self presentPopupViewController:self.assessmentController animationType:PopupViewAnimationSlideBottomTop];
//
//                    self.assessmentController.bookTitle.text = book.title;
//                    
//                    break;
//                }
//            }
//            
//            break;
//        }
        default: {
            [self dismissPopupViewControllerWithanimationType:PopupViewAnimationFade];
            [self.gridView deselectItemAtIndex:[self.gridView indexOfSelectedItem] animated: YES];
            //VLog(@"Cancel");
            break;
        }

    }
    
    popBookInfoViewController.delegate = nil;
}

#pragma mark - Private Methods
-(void) removeBookData: (Book *) book {
    [self removeBook:VS_FMT(@"%@", book.bookId)];
    
    // Test
    //            NSArray *allBookExercises = [VSmartHelpers getBookExercises];
    //            VLog(@"Exercises: %@", allBookExercises);
}

-(void) removeBook: (NSString *) title {
    dispatch_async(_mainQueue, ^{
        [self removeBookWithOperation: title];
    });
}

-(void) removeBookWithOperation: (NSString *) title {
    
    /* localizable strings */
    NSString *removeBookMessage = NSLocalizedString(@"Removing book", nil); //checked
    [SVProgressHUD showWithStatus:removeBookMessage maskType:SVProgressHUDMaskTypeClear];//checked
    
//    [SVProgressHUD showWithStatus:kRemovingBookMessage maskType:SVProgressHUDMaskTypeClear];
    [VSmartHelpers removeBook:title];
    [VSmartHelpers removeExerciseData:title];
    [VSmartHelpers removeExerciseResultData:title];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self refreshMainAfterRemovalOperation];
        [self refreshMedals];
    });
}

-(void) refreshMainAfterRemovalOperation {
    //[[NSNotificationCenter defaultCenter] postNotificationName:kBookListNotification object:nil];
    [SVProgressHUD dismiss];
    [self _loadBooks];
}

//-(void) openReader:(NSNumber *) index {
-(void) openReader:(NSString *) bookId {
    
//    NSString *title = VS_FMT(@"%i", [index intValue] );
    NSString *bookIdentifier = VS_FMT(@"%@", bookId );
    VLog(@"openReader: Book Title: %@", bookIdentifier);
    
    [self hideToolbar:YES];
    
    if (detailViewController != nil) {
		[detailViewController.view removeFromSuperview];
		//[detailViewController release];
		detailViewController = nil;
	}
    
    detailViewController = [[EPubViewController alloc] init];
    
    CGRect theFrame = detailViewController.view.frame;
    //CGRect theFrame = self.view.frame;
    //VLog(@"MainView Width: %0.2f", self.view.frame.size.width);
    
    theFrame.origin = CGPointMake(self.view.frame.size.width, 0);
    detailViewController.view.frame = theFrame;
    //detailViewController.view.frame = self.view.bounds;
    theFrame.origin = CGPointMake(0,0);
    [detailViewController.view setNeedsDisplay];
    
	[self.view addSubview:detailViewController.view];
    [Utils saveReaderState:YES];
    
    [detailViewController loadEpubBook:bookIdentifier];
    
	[UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animDone:finished:context:)];
    
    //detailViewController.view.frame = theFrame;
    detailViewController.view.frame = self.view.bounds;
    //[detailViewController.view setNeedsDisplay];
	detailViewController.view.alpha = 1.0;
    [UIView commitAnimations];    
}

-(void)animDone:(NSString*) animationID finished:(BOOL) finished context:(void*) context
{
	//reader.containerView.transform = CGAffineTransformMakeRotation(-1 * M_PI / 2);
	//reader.currPage.transform = CGAffineTransformMakeRotation(M_PI / 2);
}

#pragma mark - Document Interaction Controller Delegate Methods
- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}

- (void) openPDFReader: (NSDictionary *) bookInfo {
    
    NSString *file = [bookInfo objectForKey:@"title"];
    NSString *bookPath = [Utils appLibraryBooksDirectory];
    NSString *pdfFilePath = [NSString stringWithFormat:@"%@/%@/%@.pdf", bookPath, file, file];
    
    NSURL *url = [NSURL fileURLWithPath:pdfFilePath];
    if (url) {
        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        [self.documentInteractionController setDelegate:self];
        [self.documentInteractionController setName:[bookInfo objectForKey:@"book_title"]];
        [self.documentInteractionController presentPreviewAnimated:YES];
    }
}

-(void) showPopupBookInfo: (Book *) book forIndex:(int) index withBookState: (NSString *) bookState {
    
    //EPub *epub = [[EPub alloc] initWithBookname:VS_FMT(@"%i", book.bookId)];
    
    popBookInfoViewController = [[BookInfoViewController alloc] initWithNibName:@"BookInfoViewController" bundle:nil];
    popBookInfoViewController.delegate = self;
    popBookInfoViewController.view.frame = popBookInfoViewController.view.frame;
    
    NSString *readLabel = NSLocalizedString(@"Read", nil); //checked
    NSString *deleteLabel = NSLocalizedString(@"Delete", nil); //checked
    
    NSString *buttonTextTitle = @"";
    
    if ([bookState isEqualToString:kBookStateRead]) {
//        NSDictionary *exercise = [Utils getExerciseFileContents:VS_FMT(@"%i", book.bookId)];
//        VLog(@"exercises:\n%@", [exercise objectForKey:@"exercises"]);
        popBookInfoViewController.buttonStateText = [NSString stringWithFormat:@"%@", kBookStateRead];
        buttonTextTitle = readLabel;
    }
    else if ([bookState isEqualToString:kBookStateDelete]) {
        //        NSDictionary *exercise = [Utils getExerciseFileContents:VS_FMT(@"%i", book.bookId)];
        //        VLog(@"exercises:\n%@", [exercise objectForKey:@"exercises"]);
        popBookInfoViewController.buttonStateText = [NSString stringWithFormat:@"%@", kBookStateDelete];
        buttonTextTitle = deleteLabel;
    }
    
    const float MAX_WIDTH = 439.0;
    const float MIN_WIDTH = 275.0;//236.0;
    
    //VLog(@"Main View: %@", NSStringFromCGRect(popBookInfoViewController.view.frame));
    
    CGSize textSize = { popBookInfoViewController.backgroundButton.bounds.size.width, 2000.0f };
    
    CGRect frameTitle = CGRectMake(10, 241, 418.0, 75.0 );
    
    CGRect rect = CGRectMake(0, 0, 120, 180);
    
    //UIImage *coverImg = [UIImage imageWithContentsOfFile:coverPath];
    UIImage *coverImg = [UIImage imageWithContentsOfFile:book.cover];
    if (![book.mediaType isIn:kBookExtensionPdf, nil]) {
        EPub *epub = [[EPub alloc] initWithBookname:VS_FMT(@"%@", book.bookId)];
        NSString *rootPath = epub.rootPath;
        NSString *bookPath = [Utils appLibraryBooksDirectory];
        NSString *customPath = [NSString stringWithFormat:@"/%@/%@/%@", VS_FMT(@"%@", book.bookId), rootPath, epub.coverImagePath];
        NSString *coverPath = [bookPath stringByAppendingFormat:@"%@", [rootPath isEqualToString:@""] ? [NSString stringWithFormat: @"/%@/%@", VS_FMT(@"%@", book.bookId), epub.coverImagePath] : customPath];
        coverImg = [UIImage imageWithContentsOfFile:coverPath];
    }

    //UIImage *finalImage = [[coverImg scaleImageToSize:rect.size] imageWithShadow];
    UIImage *finalImage = [coverImg scaleImageToSize:rect.size];
    popBookInfoViewController.bookImage.image = finalImage;
    
    //popBookInfoViewController.authorsLabel.text = [NSString stringWithFormat:@"By: %@", meta.authors];

    //popBookInfoViewController.titleLabel.text = title;
    popBookInfoViewController.titleText.text = book.title;

    NSDictionary* attribs = @{NSFontAttributeName:popBookInfoViewController.titleText.font};
    CGRect rectTitle = [book.title boundingRectWithSize:textSize
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:attribs
                                                context:nil];
    
//    CGSize titleSize = [book.title sizeWithFont:popBookInfoViewController.titleLabel.font
//                         constrainedToSize:textSize
//                             lineBreakMode:NSLineBreakByWordWrapping];
    CGSize titleSize = rectTitle.size;
    frameTitle.size.height = titleSize.height;
    //VLog(@"TitleSize: %.02f", titleSize.width);
    
    float fillerSize = 30;
    if (titleSize.width > MAX_WIDTH) {
        fillerSize = 0;
    }
    if (titleSize.width < MIN_WIDTH) {
        titleSize.width = MIN_WIDTH;
    }
    //CGRect newSize = CGRectMake(0, 0, titleSize.width + fillerSize, 439);
    //popBookInfoViewController.view.frame = newSize;
    
    //popBookInfoViewController.titleLabel.frame = frameTitle;
    
    NSString *bookId = book.bookId;
    //VLog(@"BookId: %i and Index: %i", bookId, index);
    NSDictionary *bookInfoFromCollection = [VSmartHelpers getBookInfoFromCollection: bookId inCollection:nil];
    VLog(@"bookInfoFromCollection: %@", bookInfoFromCollection);
    
    //NSString *authors = [meta.authors stringByReplacingOccurrencesOfString:@"�" withString:@"ñ"];
    popBookInfoViewController.authorText.text = [VSmartHelpers getAuthors:book.authors];
    popBookInfoViewController.publisherText.text = book.publisher;
    
    NSDate *dateAdded = (NSDate *)[bookInfoFromCollection objectForKey:kBookDateAdded];
    NSString *dateAddedText = [NSDate stringFromDate:dateAdded withFormat:kDateInfoFormat];
    
    popBookInfoViewController.dateAddedLabel.text = dateAddedText;// [NSString stringWithFormat:@"Date Added: %@", dateAddedText];
    //VLog(@"DateLastRead: %@", [bookInfoFromCollection objectForKey:kBookDateLastRead]);
    popBookInfoViewController.dateLastReadLabel.text = [bookInfoFromCollection objectForKey:kBookDateLastRead]; //[NSString stringWithFormat:@"Date Last Read: %@", [bookInfoFromCollection objectForKey:kBookDateLastRead]];
    popBookInfoViewController.bookId = bookId;
    popBookInfoViewController.index = index;
    popBookInfoViewController.book = book;
    
    //popBookInfoViewController.readOrDeleteButton.titleLabel.text = bookState;
    [popBookInfoViewController.readOrDeleteButton setTitle:buttonTextTitle forState: UIControlStateNormal]; //normal
    [popBookInfoViewController.readOrDeleteButton setTitle:buttonTextTitle forState: UIControlStateHighlighted];//pressed
    
    [popBookInfoViewController initButtons];
    
    [self presentPopupViewController:popBookInfoViewController animationType:PopupViewAnimationFade];
}

- (NSDictionary*)getConnectionInfoForRow:(int)row
{
//    for (NSDictionary *dict in self.queueIndexPaths) {
//        //VLog(@"dictRow: %i:%i", [[dict objectForKey:@"row"] intValue], row);
//        
//        if ([[dict objectForKey:@"row"] intValue] == row) {
//            return dict;
//        }
//    }
    return nil;
}

- (NSUInteger) indexOfObjectForKey: (NSString*) name forKey:(NSString *) key inArray: (NSArray*) array
{
    return [array indexOfObjectPassingTest:
            ^BOOL(id dictionary, NSUInteger idx, BOOL *stop) {
                return [[dictionary objectForKey: key] isEqualToString: name];
            }];
}

- (NSUInteger) indexOfObjectForNumber: (NSString *) number forKey:(NSString *) key inArray: (NSArray*) array
{
    return [array indexOfObjectPassingTest:
            ^BOOL(id dictionary, NSUInteger idx, BOOL *stop) {
//                int objId = [[dictionary objectForKey: key] intValue];
//                return objId == number;
                NSString *objId = [dictionary objectForKey:key];
                return [objId isEqualToString:number];
            }];
}

- (NSUInteger) indexOfObjectForId: (id) obj inArray: (NSArray*) array
{
    return [array indexOfObjectPassingTest:
            ^BOOL(id dictionary, NSUInteger idx, BOOL *stop) {
                return [dictionary isEqual:obj];
            }];
}

- (int) bookIndexForBookId: (NSString *) bookId {
    
    int dictId = [self indexOfObjectForNumber:bookId forKey:@"book_id" inArray:self.books];
    return dictId;
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Fetched Results Object

- (void)performFetch
{
    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    if (self.frc) {
        [self.frc.managedObjectContext performBlockAndWait:^{
            NSError *error = nil;
            if (![self.frc performFetch:&error]) {
                NSLog(@"Failed to perform fetch: %@", error);
            }
            [self.gridView reloadData];
        }];
    }
    else {
        VLog(@"Failed to fetch, the fetched results controller is nil.");
    }
}

- (void)configureFetch
{
    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    self.rm = [AppDelegate resourceInstance];//instantiate resource
    
    AccountInfo *account = [self account];
    self.useridPATH = VS_FMT(@"%@_%i", account.user.username, account.user.id);
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kTextBookEntity];
    request.predicate = [self.rm predicateForKeyPath:@"user_id" andValue:self.useridPATH];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"book_id" ascending:YES]];
    request.fetchBatchSize = 20;
    
    NSManagedObjectContext *context = _rm.mainContext;
    self.frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    self.frc.delegate = self;
}

#pragma mark - Fetched Results Controller

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
//    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    [self.gridView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
//    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    NSIndexSet *indices = [[NSIndexSet alloc] initWithIndex:indexPath.row];
    NSIndexSet *newIndices = [[NSIndexSet alloc] initWithIndex:newIndexPath.row];
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.gridView insertItemsAtIndices:newIndices withAnimation:AQGridViewItemAnimationNone];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.gridView deleteItemsAtIndices:indices withAnimation:AQGridViewItemAnimationNone];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self.gridView reloadItemsAtIndices:indices withAnimation:AQGridViewItemAnimationNone];
            break;
            
        case NSFetchedResultsChangeMove:
            [self.gridView deleteItemsAtIndices:indices withAnimation:AQGridViewItemAnimationNone];
            [self.gridView insertItemsAtIndices:newIndices withAnimation:AQGridViewItemAnimationNone];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
//    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    [self.gridView endUpdates];
    if ([[self.frc fetchedObjects] count] == 1) {
        [self.gridView reloadData];
    }
}

@end
