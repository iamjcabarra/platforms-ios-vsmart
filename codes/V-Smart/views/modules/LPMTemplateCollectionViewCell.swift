//
//  LPMTemplateCollectionViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 04/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMTemplateCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var templateImage: UIImageView!
    @IBOutlet var templateLabel: UILabel!
    
    func highlightSelectedCell(_ highlight: Bool) {
        self.contentView.backgroundColor = highlight ? UIColor(rgba: "#EFEFF4") : UIColor(rgba: "#FFFFFF")
    }
    
}
