//
//  GBTBatchScoreEditCell.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 09/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTBatchScoreEditCell: UITableViewCell {

    @IBOutlet weak var editScoreTextField: UITextField!
    @IBOutlet weak var studentNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
