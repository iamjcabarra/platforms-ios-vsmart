//
//  NoteContent3ViewCell.h
//  V-Smart
//
//  Created by VhaL on 2/12/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteContent3ViewCell : UITableViewCell <UITextViewDelegate>
@property (nonatomic, strong) IBOutlet UITextView *textViewDescription;
@property (nonatomic, strong) IBOutlet UILabel *labelCount;
@property (nonatomic, strong) IBOutlet UILabel *labelWordCount;
@property (nonatomic, strong) UIButton *cell5SaveButton;
@property (nonatomic, strong) UITextField *cell2Title;

@property (nonatomic, strong) IBOutlet UIButton *buttonToggle;

@property (nonatomic, strong) IBOutlet UIButton *buttonFontSegoe;
@property (nonatomic, strong) IBOutlet UIButton *buttonFontRancho;
@property (nonatomic, strong) IBOutlet UIButton *buttonFontJosefinSans;
@property (nonatomic, strong) IBOutlet UIButton *buttonFontRaleway;
@property (nonatomic, strong) IBOutlet UIButton *buttonFontUbuntu;
@property (nonatomic, strong) IBOutlet UISlider *sliderFontSize;

@property (nonatomic, strong) IBOutlet UIView *fontView;

//@property (nonatomic, assign) int *fontTypeId;
//@property (nonatomic, assign) int *fontSizeId;

@property (nonatomic, assign) id delegate;
-(void)initializeCellWithDelegate:(id)parent;
-(void)updateTextCount;
@end
