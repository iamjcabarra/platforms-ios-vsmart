//
//  PostStreamView.m
//  V-Smart
//
//  Created by VhaL on 3/13/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CalendarChildViewController.h"
#import "JSONHTTPClient.h"
#import "AccountInfo.h"
#import "VSmart.h"
#import "VSmartValues.h"
#import "Utils.h"

@interface CalendarChildViewController ()
@property (nonatomic, strong) IBOutlet UIWebView *webView;
@end

@implementation CalendarChildViewController

-(AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self setupWebview];
    [self loadWebview];
}

-(void)setupWebview{
    for (id subview in self.webView.subviews)
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            ((UIScrollView *)subview).bounces = NO;
    
    [self.view addSubview:self.webView];
}

-(void)loadWebview{

    /*
     NOTE: Account Types
     1|2    = Admin
     3      = Teacher
     4      = Student
     */
    
    int userid = [self account].user.id; // user identifier
    
    // DEFAULT ADMIN VALUE
    int position = 1;
    
    // STUDENT MODE
    if ([[self account].user.position isEqualToString:kModeIsStudent]) {
        position = 4;
    }
    
    // TEACHER MODE
    if ([[self account].user.position isEqualToString:kModeIsTeacher]) {
        position = 3;
    }
//    NSString *lang = [VSmartHelpers getDeviceLocale]; //language
//    NSString *urlString = [Utils buildUrl:VS_FMT(kEndPointCalendarLocalize, userid, position, lang)];
    
    NSString *calendarRequest = [NSString stringWithFormat:kEndPointCalendarLocalize, userid, position];
    NSString *urlString = [Utils buildUrl:calendarRequest];
    NSURLRequest *webRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [self.webView loadRequest:webRequest];
}

#pragma mark - UIWebView Delegate

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    //    [SVProgressHUD dismiss];
    VLog(@"Finished Loading View");
    [webView.scrollView setContentSize: CGSizeMake(webView.frame.size.width, webView.scrollView.contentSize.height)];

}

-(void)webView:(UIWebView *)theWebView didFailLoadWithError:(NSError *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    //    [SVProgressHUD dismiss];
	VLog(@"Error: %@", [error localizedDescription]);
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    //    [SVProgressHUD showWithStatus:MSG_LOADING_MODULE maskType:SVProgressHUDMaskTypeGradient];
}

-(void)dealloc{
    [self.webView stopLoading];
 	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.webView.delegate = nil;
}

@end
