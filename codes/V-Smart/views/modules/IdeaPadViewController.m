//
//  IdeaPadViewController.m
//  V-Smart
//
//  Created by VhaL on 3/5/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "IdeaPadViewController.h"
//#import "SocialStreamChildViewController.h"
#import "AppDelegate.h"


#define kRowHeight 67

@interface IdeaPadViewController ()
@property (nonatomic, strong) NSMutableArray *ideaPadArray;
@property (nonatomic, strong) SIdeaPad *currentIdea;

@property (nonatomic, weak) IBOutlet UILabel *labelNoEntry;
@property (nonatomic, weak) IBOutlet UILabel *labelFontTitle;
@property (nonatomic, weak) IBOutlet UITextView *textViewEntry;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, weak) IBOutlet UISlider *sliderFontIdea;
@property (nonatomic, weak) IBOutlet UISlider *sliderFontRecentIdea;

@property (nonatomic, weak) IBOutlet UIView *viewIdea;
@property (nonatomic, weak) IBOutlet UIView *viewRecent;

@property (nonatomic, weak) IBOutlet UIView *viewRecentIdea;
@property (nonatomic, weak) IBOutlet UIView *viewRecentList;
@property (nonatomic, weak) IBOutlet UITextView *textViewRecentIdea;

@property (nonatomic, weak) IBOutlet UIButton *buttonBack;

@property (nonatomic, assign) int lastUserId;

@property (nonatomic, strong) NoteContentViewController *noteContentViewController;

// RESOURCE MANAGER
@property (nonatomic, strong) ResourceManager *rm;
@property (nonatomic, strong) NSDictionary *lookupBadWords;
@property (nonatomic, strong) NSString *first_group;

@end

@implementation IdeaPadViewController
@synthesize labelNoEntry, textViewEntry, segmentedControl, ideaPadArray, currentIdea, viewIdea, viewRecent, sliderFontIdea, sliderFontRecentIdea, buttonBack, viewRecentIdea, viewRecentList, textViewRecentIdea, noteContentViewController, parent;

-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.rm = [AppDelegate resourceInstance];
    [self loadUserGroups];
    
    UINib *cellNib = [UINib nibWithNibName:@"IdeaPadCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"IdeaPadCell"];
    
    [self loadData];
    
    sliderFontRecentIdea.value = sliderFontIdea.value = [[NSUserDefaults standardUserDefaults] floatForKey:kUserDefaultFontSizeKey];
    textViewEntry.font = [UIFont systemFontOfSize:sliderFontIdea.value];
    textViewRecentIdea.font = [UIFont systemFontOfSize:sliderFontRecentIdea.value];
    
    self.labelFontTitle.text = NSLocalizedString(@"Font", nil);
    
    [self segmentSwitch:segmentedControl];
}

////call from delegate goes here
//-(void)buttonSaveTapped:(NoteEntry*)entry{
//
//}

- (void)loadUserGroups {
    
    __weak typeof(self) wo = self;
    [self.rm requestSocialStreamGroups:^(NSArray *list) {
        if ((list != nil) && ([list count] > 0)) {
            NSDictionary *g = [list firstObject];
            if ((g != nil) && ([g isKindOfClass:[NSDictionary class]])) {
                if (g[@"group_id"] != nil) {
            wo.first_group = [NSString stringWithFormat:@"%@", g[@"group_id"] ];
            [wo loadBadWords];
        }
            }
        }
    }];
}

- (void)loadBadWords {
    __weak typeof(self) wo = self;
    [self.rm requestBadWordsDoneBlock:^(BOOL status) {
        if (status == YES) {
            wo.lookupBadWords = [NSDictionary dictionaryWithDictionary:[wo.rm fetchBadwords]];
        }
    }];
}

//- (BOOL)analyzeString:(NSString *)string lookup:(NSDictionary *)hashmap {
//    
//    NSArray *words = [string componentsSeparatedByString:@" "];
//    
//    for (NSString *key in words) {
//        
//        NSString *comparedkey = [NSString stringWithFormat:@"%@", [key lowercaseString] ];
//        if ( [hashmap objectForKey:comparedkey] ) {
//            NSString *message = [NSString stringWithFormat:@"'%@' is bad word!!!", key ];
//            AlertWithMessageAndDelegate(@"Invalid Entry", message, self);
//            return NO;
//        }
//    }
//    return YES;
//}

-(void)viewWillAppear:(BOOL)animated{
    if (self.lastUserId != [VSmart sharedInstance].account.user.id){
        self.lastUserId = [VSmart sharedInstance].account.user.id;
        [self loadData];
    }
}

-(void)dismissPopupViewControllerWithanimationType:(PopupViewAnimation)animationType{
    [super dismissPopupViewControllerWithanimationType:animationType];
    
    [parent setPopoverContentSize:CGSizeMake(380, 605)];
}

-(IBAction)segmentSwitch:(id)sender {
    viewIdea.hidden = YES;
    viewRecent.hidden = YES;
    
    if (segmentedControl.selectedSegmentIndex == 0) {
        viewIdea.hidden = NO;
    }
    else{
        viewRecent.hidden = NO;
        viewRecentList.hidden = NO;
        viewRecentIdea.hidden = YES;
        
        self.tableView.hidden = YES;
        labelNoEntry.hidden = YES;
        
        if (0 < ideaPadArray.count){
            self.tableView.hidden = NO;
        }
        else{
            labelNoEntry.hidden = NO;
        }
    }
}

-(IBAction)buttonNewIdeaTapped:(id)sender{
    
    if ([textViewEntry.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0){
        return;
    }
    
    currentIdea = [[ShortcutDataManager sharedInstance] addIdeaPad:textViewEntry.text withColorId:0];
    
    [self loadData];
    textViewEntry.text = @"";
    
    // Bug #1309
    // jca-05052016
    // Disable automatic switching to Recent Tab
    //segmentedControl.selectedSegmentIndex = 1;
    //[self segmentSwitch:segmentedControl];
    
    // Implement a confirmation message
    NSString *message = NSLocalizedString(@"Your idea has been successfully saved.", nil);
    if (currentIdea == nil) {
        message = NSLocalizedString(@"There was an error saving your idea. Please try again later.", nil);
    }
    [self.view makeToast:message duration:2.0 position:kCAGravityCenter];
    
    viewRecentList.hidden = YES;
    viewRecentIdea.hidden = NO;
    textViewRecentIdea.text = currentIdea.content;
    
    [textViewEntry resignFirstResponder];
}

-(IBAction)buttonBackTapped:(id)sender{
    
    if (![currentIdea.content isEqualToString:textViewRecentIdea.text])
    {
        currentIdea.content = textViewRecentIdea.text;
        [[ShortcutDataManager sharedInstance] editIdeaPad:currentIdea];
    }
    
    [self loadData];
    textViewEntry.text = @"";
    
    viewRecentList.hidden = NO;
    viewRecentIdea.hidden = YES;
    
    [textViewRecentIdea resignFirstResponder];
}

-(IBAction)buttonClearTapped:(id)sender{
    textViewEntry.text = @"";
}

-(IBAction)sliderFontValueChange:(UISlider*)sender{
    [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:kUserDefaultFontSizeKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    textViewEntry.font = [UIFont systemFontOfSize:sender.value];
    textViewRecentIdea.font = [UIFont systemFontOfSize:sender.value];
    
    sliderFontRecentIdea.value = sliderFontIdea.value = sender.value;
}

-(IBAction)actionButtonTapped:(UIButton*)button{
    
    /* localizable strings */
    NSString *addToNotes = NSLocalizedString(@"Add to Notes", nil); //checked
    NSString *shareToSocial = NSLocalizedString(@"Share to Social Stream", nil); //checked
    NSString *deleteIdea = NSLocalizedString(@"Delete", nil); //checked
    NSString *cancelAction = NSLocalizedString(@"Cancel", nil); //checked
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:cancelAction
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:addToNotes,shareToSocial,deleteIdea,nil];
    actionSheet.destructiveButtonIndex = 2;
    [actionSheet showFromRect:self.view.frame inView:self.view animated:YES];
}

- (void)postContent:(NSString *)content {

    NSString *message = [NSString stringWithFormat:@"%@", content];
//    BOOL status = [self analyzeString:message lookup:self.lookupBadWords];
    BOOL status = [Utils analyzeString:message lookup:self.lookupBadWords];
    if (status) {
        
        NSNumber *group_number = @( [self.first_group integerValue] );
        
        NSMutableDictionary *data = [@{
                                       @"group_id":group_number,
                                       @"emoticon_id":@"(null)",
                                       @"is_message":@1,
                                       @"message":message
                                       
                                       } mutableCopy];
        
        [self.rm postMessage:data doneBlock:^(BOOL status) {
            if (status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    AlertWithMessageAndDelegate(@"Post Successful", message, self);
                });
            }
        }];
        
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) { //Add to Notes
        case 0:{
            noteContentViewController = [[NoteContentViewController alloc] init];
            noteContentViewController.delegate = self;
            
            noteContentViewController.noteStringFromIdeaPad = textViewRecentIdea.text;
            [self presentPopupViewController:noteContentViewController animationType:PopupViewAnimationSlideBottomBottom];
            
            [parent setPopoverContentSize:CGSizeMake(self.view.frame.size.width + 400, self.view.frame.size.height + 200)];

        }
            break;

//        case 1:{
//            [self.view makeToast:@"New Playlist"];
//        }
//            break;
            
//        case 1:{ //Email
//            [self showEmail:nil];
//        }
//            break;
            
        case 1:{
//            [SocialStreamChildViewController postToDefaultGroup:currentIdea.content showResult:YES];
            [self postContent:currentIdea.content];
        }
            break;
            
        case 2:{ //Delete
            bool deleteSuccess = [[ShortcutDataManager sharedInstance] deleteIdeaPad:currentIdea];
            if (deleteSuccess){
                viewRecentList.hidden = NO;
                viewRecentIdea.hidden = YES;
                
                [self loadData];
            }
        }
            break;
            
        default:
            break;
    }
}

-(IBAction)showEmail:(id)sender {
    NSString *emailTitle = @"Vsmart for IOS: ";
    NSString *messageBody = textViewRecentIdea.text;
    NSArray *toRecipents = [NSArray arrayWithObject:@""];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    [self presentViewController:mc animated:YES completion:NULL];
}

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)loadData{
    ideaPadArray = [NSMutableArray arrayWithArray:[[ShortcutDataManager sharedInstance] getIdeaPads]];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kRowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ideaPadArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"IdeaPadCell";
    IdeaPadCell *cell = (IdeaPadCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        cell = [[IdeaPadCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    IdeaPadCell __weak *weakCell = cell;
    
    [cell setAppearanceWithBlock:^{
        weakCell.leftUtilityButtons = [self leftButtons];
        weakCell.rightUtilityButtons = [self rightButtons];
        weakCell.delegate = self;
        weakCell.containingTableView = self.tableView;
    } force:NO];
    
    [cell setCellHeight:cell.frame.size.height];
    [cell initializeCell:self];
    
    SIdeaPad *ideaPad = [ideaPadArray objectAtIndex:indexPath.row];
    
    cell.labelContent.text = ideaPad.content;
    
    //NSString *subLabel = VS_FMT(@"%d words, %@", [self countWords:ideaPad.content], [ideaPad.modified relativeDateString]);
    //cell.labelWordCount.text = subLabel;
    //cell.labelWordCount.text = [NSString stringWithFormat:@"%d words", [self countWords:ideaPad.content]] ;
    
    // Refactor
    // jca-05052016
    // Dynamic "word"
    NSString *wordString = NSLocalizedString(@"word", nil);
    int countWords = [self countWords:ideaPad.content];
    
    if (countWords > 1) {
        wordString = NSLocalizedString(@"words", nil);
    }
    
    NSString *subLabel = [NSString stringWithFormat:@"%d %@, %@", countWords, wordString, [ideaPad.modified relativeDateString]];
    cell.labelWordCount.text = subLabel;
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
//    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    //cell.labelDateModified.text = [dateFormatter stringFromDate:ideaPad.modified];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row % 2 == 0) {
        [cell setBackgroundColor:UIColorFromHex(0xe1eff2)];
    } else {
        [cell setBackgroundColor:UIColorFromHex(0xf1f2f4)];
    }
}

- (int)countWords:(NSString*)stringContainer{
    NSString *string = [stringContainer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSArray *array = [string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"self != %@", @""];
    array = [array filteredArrayUsingPredicate:pred];
    
    return array.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    currentIdea = [ideaPadArray objectAtIndex:indexPath.row];
    viewRecentList.hidden = YES;
    viewRecentIdea.hidden = NO;
    textViewRecentIdea.text = currentIdea.content;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *deleteTitle = NSLocalizedString(@"Delete", nil);
    return deleteTitle;
}

#pragma mark - SWTableViewDelegate

- (NSArray *)rightButtons{
//    UIImage *copyImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_flyout_rename.png"] scaledToSize:CGSizeMake(15, 15)];
//    UIImage *shareImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_notes_share.png"] scaledToSize:CGSizeMake(15, 15)];
//    UIImage *trashImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_notes_trash.png"] scaledToSize:CGSizeMake(15, 15)];
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
//    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0] icon:copyImg];
//    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:0] icon:shareImg];
//    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0] icon:trashImg];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0.8] title:@"Delete"];
    return rightUtilityButtons;
}

- (NSArray *)leftButtons{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:0.07 green:0.75f blue:0.16f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"check.png"]];
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:1.0f green:1.0f blue:0.35f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"clock.png"]];
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:1.0f green:0.231f blue:0.188f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"cross.png"]];
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:0.55f green:0.27f blue:0.07f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"list.png"]];
    
    return leftUtilityButtons;
}

-(void)swipeableTableViewCell:(IdeaPadCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            NSLog(@"left button 0 was pressed");
            break;
        case 1:
            NSLog(@"left button 1 was pressed");
            break;
        case 2:
            NSLog(@"left button 2 was pressed");
            break;
        case 3:
            NSLog(@"left btton 3 was pressed");
        default:
            break;
    }
}

-(void)swipeableTableViewCell:(IdeaPadCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    SIdeaPad *ideaPad = [ideaPadArray objectAtIndex:indexPath.row];
    
    switch (index) {
        case 0: //delete
        {
            bool deleteSuccess = [[ShortcutDataManager sharedInstance] deleteIdeaPad:ideaPad];

            if (deleteSuccess){
                [ideaPadArray removeObjectAtIndex:indexPath.row];
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            }
            else{
                [cell hideUtilityButtonsAnimated:YES];
            }
            break;
        }
        case 1:
        {
            [cell hideUtilityButtonsAnimated:YES];
            break;
        }
        default:
            break;
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(IdeaPadCell *)cell {
    return YES;
}

@end
