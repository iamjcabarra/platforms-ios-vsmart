//
//  NewCurriculumDetailView.m
//  V-Smart
//
//  Created by Julius Abarra on 16/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <WebKit/WebKit.h>
#import "NewCurriculumDetailView.h"
#import "CurriculumDataManager.h"
#import "HUD.h"

@interface NewCurriculumDetailView () <WKNavigationDelegate>

@property (strong, nonatomic) CurriculumDataManager *cpdm;
@property (strong, nonatomic) WKWebView *webObject;

@end

@implementation NewCurriculumDetailView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Curriculum Planner Data Manager
    self.cpdm = [CurriculumDataManager sharedInstance];
    
    // Loading View
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Loading", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];

    // Set Up Web View
    [self setUpWebView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Set navigation bar title
    NSString *curriculumTitle = [self stringValue:[self.curriculumObject valueForKey:@"curriculum_title"]];
    self.title = curriculumTitle;
    
    // Decorate navigation bar
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    self.navigationController.navigationBar.barTintColor = UIColorFromHex(0x4475B7);
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Set Up WkWebView

- (void)setUpWebView {
    WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    self.webObject = [[WKWebView alloc] initWithFrame:self.view.frame configuration:theConfiguration];
    self.webObject.navigationDelegate = self;
    
    NSString *baseURL = [self.cpdm baseURL];
    NSString *curriculumID = [self stringValue:[self.curriculumObject valueForKey:@"curriculum_id"]];
    NSString *urlString = [NSString stringWithFormat:@"http://%@%@%@", baseURL, kEndPointCurriculumDetail, curriculumID];
    
    NSURL *nsurl = [NSURL URLWithString:urlString];
    NSURLRequest *nsrequest = [NSURLRequest requestWithURL:nsurl];
    [self.webObject loadRequest:nsrequest];
    
    [self.view addSubview:self.webObject];
}

#pragma mark - WKWebView Navigation Delegate

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Loading", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    [self showErrorMessageDialog:[error localizedDescription]];
    [HUD hideUIBlockingIndicator];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [HUD hideUIBlockingIndicator];
}

#pragma mark - Change Device Orientation

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.webObject removeFromSuperview];
    self.webObject = nil;
    [self setUpWebView];
}

#pragma mark - Alert Messages

- (void)showErrorMessageDialog:(NSString *)error {
    NSString *avTitle = [NSLocalizedString(@"Error", nil) uppercaseString];
    NSString *butPosR = NSLocalizedString(@"Okay", nil);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:avTitle
                                                                   message:error
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *posAction = [UIAlertAction actionWithTitle:butPosR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                          [self.navigationController popViewControllerAnimated:YES];
                                                      }];
    
    [alert addAction:posAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Class Helpers

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

@end
