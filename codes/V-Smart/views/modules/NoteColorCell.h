//
//  NoteColorCell.h
//  V-Smart
//
//  Created by VhaL on 3/3/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteColorCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *colorBackground;
@property (nonatomic, strong) IBOutlet UILabel *colorLabel;
@end
