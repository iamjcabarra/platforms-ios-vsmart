//
//  SettingsViewController.h
//  V-Smart
//
//  Created by VhaL on 4/30/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSSettingsViewController : UIViewController
@property (nonatomic, strong) IBOutlet UILabel *labelBuildNum;
@property (nonatomic, strong) IBOutlet UILabel *labelBuildDate;
@property (nonatomic, strong) IBOutlet UILabel *labelBuildTime;
@end
