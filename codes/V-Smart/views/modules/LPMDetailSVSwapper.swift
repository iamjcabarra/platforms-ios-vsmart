//
//  LPMDetailSVSwapper.swift
//  V-Smart
//
//  Created by Julius Abarra on 04/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMDetailSVSwapper: UIViewController {
    
    var lpid = ""
    
    fileprivate let kCommentSVCSegueIdentifier = "SHOW_COMMENT_VIEW"
    fileprivate let kHistorySVCSegueIdentifier = "SHOW_HISTORY_VIEW"
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.performSegue(withIdentifier: self.kCommentSVCSegueIdentifier, sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - View Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var svcExists = false
        
        if let commentSV = segue.destination as? LPMCommentViewController , segue.identifier == self.kCommentSVCSegueIdentifier {
            commentSV.lpid = self.lpid
            svcExists = true
        }
        
        if let historySV = segue.destination as? LPMHistoryViewController , segue.identifier == self.kHistorySVCSegueIdentifier {
            historySV.lpid = self.lpid
            svcExists = true
        }
        
        if svcExists {
            if self.childViewControllers.count > 0 {
                self.swapFromViewController(self.childViewControllers[0], toViewController: segue.destination)
            }
            else {
                self.addChildViewController(segue.destination)
                segue.destination.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                self.view.addSubview(segue.destination.view)
                segue.destination.didMove(toParentViewController: self)
            }
        }
    }
    
    // MARK:- View Swappers
    
    func swapToViewControllerWithSegueIdentifier(_ segueIdentifier: String) {
        self.performSegue(withIdentifier: segueIdentifier, sender: nil)
    }
    
    func swapFromViewController(_ fromViewController: UIViewController, toViewController: UIViewController) {
        toViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        fromViewController.willMove(toParentViewController: nil)
        self.addChildViewController(toViewController)
        
        self.transition(
            from: fromViewController,
            to: toViewController,
            duration: 0.2,
            options: UIViewAnimationOptions.transitionCrossDissolve,
            animations: nil,
            completion: { finished in
                fromViewController.removeFromParentViewController()
                toViewController.didMove(toParentViewController: self)
        })
    }

}
