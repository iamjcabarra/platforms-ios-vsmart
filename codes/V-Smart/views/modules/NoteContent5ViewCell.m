//
//  NoteContent5ViewCell.m
//  V-Smart
//
//  Created by VhaL on 2/12/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NoteContent5ViewCell.h"

@implementation NoteContent5ViewCell
@synthesize buttonSave, delegate;

-(void)initializeCellWithDelegate:(id)parent{
    delegate = parent;
}

-(IBAction)buttonSaveTapped:(id)sender{
    NSLog(@"buttonSaveTapped");
    
    if ([delegate respondsToSelector:@selector(buttonSaveTapped:)])
    {
        [delegate performSelector:@selector(buttonSaveTapped:) withObject:sender];
    }
}

-(void)initializeCell{
//    [buttonSave setBackgroundImage:[self imageFromColor:[UIColor clearColor]] forState:UIControlStateNormal];
//    [buttonSave setBackgroundImage:[self imageFromColor:[UIColor blueColor]] forState:UIControlStateHighlighted];
}

//- (UIImage *)imageFromColor:(UIColor *)color {
//    CGRect rect = CGRectMake(0, 0, 1, 1);
//    UIGraphicsBeginImageContext(rect.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetFillColorWithColor(context, [color CGColor]);
//    CGContextFillRect(context, rect);
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return image;
//}
@end
