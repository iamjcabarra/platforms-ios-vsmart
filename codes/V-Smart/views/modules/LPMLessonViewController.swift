//
//  LPMLessonViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 21/06/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMLessonViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate, UIDocumentInteractionControllerDelegate, LPMLessonActionPopoverDelegate {
    
    @IBOutlet fileprivate var lessonTableView: UITableView!
    @IBOutlet fileprivate var lessonSearchBar: UISearchBar!
    @IBOutlet fileprivate var searchDimmedView: UIView!
    @IBOutlet fileprivate var courseNameLabel: UILabel!
    @IBOutlet fileprivate var courseDescriptionLabel: UILabel!
    @IBOutlet fileprivate var emptyPlaceholderView: UIView!
    @IBOutlet fileprivate var emptyPlaceholderLabel: UILabel!
    @IBOutlet fileprivate var addLessonButton: UIButton!
    
    var selectedCourseObject: NSManagedObject!
    
    fileprivate let kLessonCellIdentifier = "lesson_cell_identifier"
    fileprivate let kLessonDetailsViewSegueIdentifier = "SHOW_DETAILS_VIEW"
    fileprivate let kFileListViewSegueIdentifier = "SHOW_FILE_LIST_VIEW"
    fileprivate let kTemplateViewSegueIdentifier = "SHOW_TEMPLATE_VIEW"
    fileprivate let kMainActionViewSegueIdentifier = "SHOW_MAIN_ACTION_VIEW"
    fileprivate let kContentOverviewSegueIdentifier = "SHOW_CONTENT_OVERVIEW"
    fileprivate let kLessonCreatorViewSegueIdentifier = "SHOW_LESSON_CREATOR_VIEW"
    
    fileprivate var selectedLessonObject: NSManagedObject!
    fileprivate var associatedCurriculum: NSDictionary?
    
    fileprivate var tableRefreshControl: UIRefreshControl!
    fileprivate var lessonActionPopover: LPMLessonActionPopover!
    fileprivate var sandboxHelper = VSSandboxHelper()
    fileprivate var documentController: UIDocumentInteractionController!
    
    fileprivate var searchKey = ""
    fileprivate var isAscending = false
    fileprivate var temporaryLessonListCount = 1
    
    // MARK: - Data Manager
    
    fileprivate lazy var lessonPlanDataManager: LessonPlanDataManager = {
        let lpdm = LessonPlanDataManager.sharedInstance()
        return lpdm!
    }()
    
    fileprivate lazy var lpmDataManager: LPMDataManager = {
        let lpdm = LPMDataManager.sharedInstance
        return lpdm
    }()
    
    fileprivate lazy var cpmDataManager: CPMDataManager = {
        let cpdm = CPMDataManager.sharedInstance
        return cpdm
    }()

    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Render selected course data
        self.courseNameLabel.text = self.selectedCourseObject.value(forKey: "course_name") as? String
        self.courseDescriptionLabel.text = self.selectedCourseObject.value(forKey: "course_description") as? String
        self.justifyLabel(self.courseDescriptionLabel)
        
        // Hide dimmed view by default
        self.searchDimmedView.isHidden = true
        
        // Hide empty place holder by default
        self.shouldShowEmptyPlaceholderView(false)
        
        // Dimmed view tap recognizer
        let cancelSearchOperation = #selector(self.cancelSearchOperation(_:))
        let tapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: cancelSearchOperation)
        self.searchDimmedView.addGestureRecognizer(tapGestureRecognizer)
        
        // Empty navigation bar title by default
        self.title = ""
        
        // Pull to refresh
        let refreshLessonListAction = #selector(self.refreshLessonListAction(_:))
        self.tableRefreshControl = UIRefreshControl.init()
        self.tableRefreshControl.addTarget(self, action: refreshLessonListAction, for: .valueChanged)
        self.lessonTableView.addSubview(self.tableRefreshControl)
        
        // Request initial lesson list
        self.loadLessonListForRequestType(.load)
        
        // Action button
        let addButtonAction = #selector(self.addButtonAction(_:))
        self.addLessonButton.addTarget(self, action: addButtonAction, for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customizeNavigationBar()
        self.addLessonButton.scaleAnimate(1.0)
        
        // TEMPORARY
        self.refreshLessonListAction(nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Custom Navigation Bar
    
    func customizeNavigationBar() {
        // Decorate navigation bar
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "#343434")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
    
        // Create navigation bar right button items
        let spacerButton = UIButton.init(type: UIButtonType.custom)
        spacerButton.frame = CGRect(x: 0, y: 0, width: 10, height: 20)
        spacerButton.showsTouchWhenHighlighted = false
        
        let searchButton = UIButton.init(type: UIButtonType.custom)
        searchButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        searchButton.showsTouchWhenHighlighted = true
        
        let searchButtonImage = UIImage.init(named: "search_white_48x48.png")
        searchButton.setImage(searchButtonImage, for: UIControlState())
        
        let searchButtonAction = #selector(self.searchButtonAction(_:))
        searchButton.addTarget(self, action: searchButtonAction, for: .touchUpInside)
        
        let sortButton = UIButton.init(type: UIButtonType.custom)
        sortButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        sortButton.showsTouchWhenHighlighted = true
        
        let sortButtonImage = UIImage.init(named: "sort_white48x48.png")
        sortButton.setImage(sortButtonImage, for: UIControlState())
        
        let sortButtonAction = #selector(self.sortButtonAction(_:))
        sortButton.addTarget(self, action: sortButtonAction, for: .touchUpInside)
        
        let openFolderButton = UIButton.init(type: UIButtonType.custom)
        openFolderButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        openFolderButton.showsTouchWhenHighlighted = true
        
        let openFolderButtonImage = UIImage.init(named: "white_folder_48px.png")
        openFolderButton.setImage(openFolderButtonImage, for: UIControlState())
        
        let openFolderButtonAction = #selector(self.openFolderButtonAction(_:))
        openFolderButton.addTarget(self, action: openFolderButtonAction, for: .touchUpInside)
        
        let spacerButtonItem = UIBarButtonItem.init(customView: spacerButton)
        let openFolderButtonItem = UIBarButtonItem.init(customView: openFolderButton)
        let sortButtonItem = UIBarButtonItem.init(customView: sortButton)
        let searchButtonItem = UIBarButtonItem.init(customView: searchButton)
        self.navigationItem.rightBarButtonItems = [openFolderButtonItem, sortButtonItem, searchButtonItem, spacerButtonItem]
        
        // Custom navigation bar's search bar
        self.lessonSearchBar.placeholder = NSLocalizedString("Search Lesson", comment: "")
        self.lessonSearchBar.delegate = self
    }
    
    // MARK: - List Lesson
    
    func loadLessonListForRequestType(_ requestType: LPMLessonListRequestType) {
        if (requestType != .refresh) {
            let indicatorString = requestType == .load ? "\(NSLocalizedString("Loading", comment: ""))..." : "\(NSLocalizedString("Searching", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
        }
        
        let userid = self.lessonPlanDataManager.loginUser()
        let courseid = self.selectedCourseObject.value(forKey: "course_id") as? String
        
        self.lessonPlanDataManager.requestLessonPlanList(forUser: userid, courseid: courseid) { (data) in
            if (data != nil) {
                DispatchQueue.main.async(execute: {
                    let lessonCount = data?["count"] as! Int
                    self.title = "\(NSLocalizedString("Lesson Plan List", comment: "")) (\(lessonCount))"
                    self.temporaryLessonListCount = lessonCount
                })
            }
            else {
                DispatchQueue.main.async(execute: {
                    let message = NSLocalizedString("There was an error loading this page. Please check your internet connection.", comment: "")
                    self.showNotificationMessage(message)
                })
            }
            
            DispatchQueue.main.async(execute: {
                self.reloadFetchedResultsController()
                requestType != .refresh ? HUD.hideUIBlockingIndicator() : self.tableRefreshControl.endRefreshing()
            })
        }
    }
    
    func refreshLessonListAction(_ sender: UIRefreshControl?) {
        self.loadLessonListForRequestType(.refresh)
    }
    
    // MARK: - Button Event Handlers
    
    func searchButtonAction(_ sender: UIButton) {
        self.navigationItem.titleView = self.lessonSearchBar
        self.lessonSearchBar.becomeFirstResponder()
    }
    
    func sortButtonAction(_ sender: UIButton) {
        self.isAscending = self.isAscending ? false : true
        self.reloadFetchedResultsController()
    }
    
    func openFolderButtonAction(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: self.kFileListViewSegueIdentifier, sender: nil)
        })
    }
    
    func addButtonAction(_ sender: UIButton) {
        let indicatorString = "\(NSLocalizedString("Please wait", comment: ""))..."
        HUD.showUIBlockingIndicator(withText: indicatorString)
        
        self.lessonPlanDataManager.requestLessonTemplateList { (success) in
            DispatchQueue.main.async(execute: {
                HUD.hideUIBlockingIndicator()
                
                if (success) {
                    self.performSegue(withIdentifier: self.kTemplateViewSegueIdentifier, sender: nil)
                }
                else {
                    let message = NSLocalizedString("You cannot create a new lesson plan this time. Please try again later.", comment: "")
                    self.showNotificationMessage(message)
                }
            })
        }
    }
    
    // MARK: - Search Bar Delegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchDimmedView.isHidden = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText == "") {
            self.searchKey = searchText
            self.reloadFetchedResultsController()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchKey = searchBar.text!
        self.reloadFetchedResultsController()
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = self.searchKey
        self.navigationItem.titleView = nil
        self.searchDimmedView.isHidden = true
    }
    
    // MARK: - Tap Gesture Recognizer
    
    func cancelSearchOperation(_ sender: UITapGestureRecognizer) {
        self.navigationItem.titleView = nil
        self.searchDimmedView.isHidden = true
        self.view.endEditing(true)
        self.lessonSearchBar.text = self.searchKey
    }
    
    // MARK: - Empty Placeholder View
    
    func shouldShowEmptyPlaceholderView(_ show: Bool) {
        var shouldShow = show
        
        if (self.lessonSearchBar.text == "") {
            shouldShow = self.temporaryLessonListCount > 0 ? false : true
        }
        
        if (shouldShow) {
            var message = NSLocalizedString("No lesson plan associated with this course. Create one today.", comment: "")
            
            if (self.lessonSearchBar.text != "") {
                message = "\(NSLocalizedString("No results found for", comment: "")) \"\(self.searchKey)\""
            }
            
            self.emptyPlaceholderLabel.text = message
        }
        
        self.emptyPlaceholderView.isHidden = !shouldShow
        self.lessonTableView.isHidden = shouldShow
    }
    
    // MARK: - Lesson Action Popover
    
    func showActionPopoverButtonAction(_ sender: UIButton) {
        self.selectedLessonObject = self.managedObjectFromButton(sender, inTableView: self.lessonTableView)
        
        self.lessonActionPopover = LPMLessonActionPopover(nibName: "LPMLessonActionPopover", bundle: nil)
        self.lessonActionPopover.lessonStatus = self.selectedLessonObject!.value(forKey: "status") as! String
        self.lessonActionPopover.lessonid = self.selectedLessonObject!.value(forKey: "id") as! String
        self.lessonActionPopover.delegate = self
        
        let status = self.selectedLessonObject.value(forKey: "status") as! String
        //let size = status == "1" ? CGSize(width: 160.0, height: 95.0) : CGSize(width: 160.0, height: 132.0)
        let size = status == "1" ? CGSize(width: 160.0, height: 45.0) : CGSize(width: 160.0, height: 132.0)
        
        self.lessonActionPopover.modalPresentationStyle = .popover
        self.lessonActionPopover.preferredContentSize = size
        self.lessonActionPopover.popoverPresentationController?.permittedArrowDirections = .right
        self.lessonActionPopover.popoverPresentationController?.sourceView = sender
        self.lessonActionPopover.popoverPresentationController?.sourceRect = sender.bounds
        self.present(self.lessonActionPopover, animated: true, completion: nil)
    }
    
    func selectedLessonAction(_ action: LPMLessonActionType) {
        let lessonid = self.selectedLessonObject.value(forKey: "id") as! String
        let lessonTitle = self.selectedLessonObject.value(forKey: "name") as! String
        
        switch action {
        case .add:
            print("Add")
        case .edit:
            self.editLessonAction()
        case .submit:
            let question = "\(NSLocalizedString("Are you sure you want to submit", comment: "")) \"\(lessonTitle)\"?"
            self.showAlertViewWithPolarQuestion(question, forActionType: .submit)
        case .download:
            self.downloadLessonAction()
        case .delete:
            let question = "\(NSLocalizedString("Are you sure you want to delete", comment: "")) \"\(lessonTitle)\"?"
            self.showAlertViewWithPolarQuestion(question, forActionType: .delete)
        case .open:
            print("Open")
            guard let fileName = self.lpmDataManager.fetchFileNameForFileKey(lessonid) else {
                return
            }
            
            let subdirectoryName = "\(LPMConstants.Sandbox.DIRECTORY_PREFIX)_\(self.lessonPlanDataManager.userEmail())"
            let subdirectoryPath = self.sandboxHelper.makeSubdirectoryInDocumentsDirectory(subdirectoryName)
            let filePath = "\(subdirectoryPath)/\(fileName)"
            let fileURL = URL(fileURLWithPath: filePath)
            
            DispatchQueue.main.async(execute: {
                self.documentController = UIDocumentInteractionController(url: fileURL)
                self.documentController.delegate = self
                self.documentController.presentPreview(animated: true)
            })
        }
    }
    
    // MARK:- Alert Message View
    
    func showAlertViewWithPolarQuestion(_ question: String, forActionType: LPMLessonActionType) {
        var alertViewTitle = ""
        
        switch forActionType {
        case .add:
            alertViewTitle =  NSLocalizedString("Add", comment: "").uppercased()
        case .edit:
            alertViewTitle =  NSLocalizedString("Edit", comment: "").uppercased()
        case .submit:
            alertViewTitle =  NSLocalizedString("Submit", comment: "").uppercased()
        case .download:
            alertViewTitle =  NSLocalizedString("Download", comment: "").uppercased()
        case .delete:
            alertViewTitle =  NSLocalizedString("Delete", comment: "").uppercased()
        case .open:
            alertViewTitle =  NSLocalizedString("Open", comment: "").uppercased()
        }
        
        let alert = UIAlertController(title: alertViewTitle, message: question, preferredStyle: .alert)
        
        let positiveString = NSLocalizedString("Yes", comment: "")
        let negativeString = NSLocalizedString("No", comment: "")
        
        let positiveAction = UIAlertAction(title: positiveString, style: .default) { (Alert) -> Void in
            
            switch forActionType {
                
            case .add:
                print("Add")
                
            case .edit:
                print("Edit")
                
            case .submit:
                let indicatorString = "\(NSLocalizedString("Submitting", comment: ""))..."
                HUD.showUIBlockingIndicator(withText: indicatorString)
                
                self.lessonPlanDataManager.requestUpdateLessonPlanStatus(self.selectedLessonObject, doneBlock: { (status) in
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        let positiveMessage = NSLocalizedString("Lesson plan was successfully submitted.", comment: "")
                        let negativeMessage = NSLocalizedString("Sorry, there was an error submitting this lesson plan. Please try again later.", comment: "")
                        self.showNotificationMessage(status ? positiveMessage : negativeMessage)
                        
                    })
                })
                
            case .download:
                print("Download")
                
            case .delete:
                let indicatorString = "\(NSLocalizedString("Deleting", comment: ""))..."
                HUD.showUIBlockingIndicator(withText: indicatorString)
                
                self.lessonPlanDataManager.requestRemoveLessonPlan(self.selectedLessonObject, doneBlock: { (status) in
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        let positiveMessage = NSLocalizedString("Lesson plan was successfully deleted.", comment: "")
                        let negativeMessage = NSLocalizedString("Sorry, there was an error deleting this lesson plan. Please try again later.", comment: "")
                        self.showNotificationMessage(status ? positiveMessage : negativeMessage)
                        self.temporaryLessonListCount -= 1
                        self.title = "\(NSLocalizedString("Lesson Plan List", comment: "")) (\(self.temporaryLessonListCount))"
                        self.shouldShowEmptyPlaceholderView(self.temporaryLessonListCount == 0 ? true : false)
                    })
                })
                
            case .open:
                print("Open")
            }
        }
        
        let negativeAction = UIAlertAction(title: negativeString, style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }

        alert.addAction(positiveAction)
        alert.addAction(negativeAction)
        self.present(alert, animated: true, completion: nil)
    }

    func showNotificationMessage(_ message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: NSLocalizedString("Okay", comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Edit Lesson Action
    
    func editLessonAction() {
        
        guard let templateid = self.selectedLessonObject.value(forKey: "template_id") as? String,
            let lpid = self.selectedLessonObject.value(forKey: "id") as? String,
            let name = self.selectedLessonObject.value(forKey: "name") as? String else {
            print("can't understand lesson object")
            return
        }
        
        self.lpmDataManager.save(object: templateid as AnyObject!, forKey: "LPM_USER_DEFAULT_TEMPLATE_ID")
        self.lpmDataManager.save(object: lpid as AnyObject!, forKey: "LPM_USER_DEFAULT_LESSON_PLAN_ID")
        self.lpmDataManager.save(object: name as AnyObject!, forKey: "LPM_USER_DEFAULT_NAVIGATION_TITLE")
        self.lpmDataManager.save(object: "0" as AnyObject!, forKey: "LPM_USER_DEFAULT_IS_VIEW_MODE")
        
        if templateid == LPMConstants.Template.UBD {
            let indicatorString = "\(NSLocalizedString("Please wait", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
            
            // Just temporary for curriculum association
            self.lessonPlanDataManager.requestLessonTemplateForLessonTemplate(withID: templateid) { (success) in
                if (success) {
                    self.lessonPlanDataManager.requestLessonPlanDetailsForLessonPlan(withID: lpid, doneBlock: { (success) in
                        self.lessonPlanDataManager.requestAssociatedCurriculumToLessonPlan(withID: lpid, dataBlock: { (curriculumData) in
                            var data = ["lp_id": lpid, "curriculum_id": "", "curriculum_title": ""]
                            
                            if let curriculum = curriculumData, let curriculumID = curriculum["curriculum_id"] as? String,
                                let curriculumTitle = curriculum["curriculum_title"] as? String {
                                        data = ["lp_id": lpid, "curriculum_id": curriculumID, "curriculum_title": curriculumTitle]
                                    }
                            
                            if (success) {
                                let predicate = self.lessonPlanDataManager.predicate(forKeyPath: "lp_id", andValue: lpid)
                                let lessonObject = self.lessonPlanDataManager.getEntity(kLessonPlanDetailsEntity, with: predicate)
                                
                                self.lessonPlanDataManager.prepareLessonObject(
                                    forAction: LPMLessonActionType.edit.rawValue,
                                    copy: lessonObject,
                                    addData: data,
                                    objectBlock: { (lessonObject) in
                                        
                                        if (lessonObject != nil) {
                                            DispatchQueue.main.async(execute: {
                                                HUD.hideUIBlockingIndicator()
                                                self.selectedLessonObject = lessonObject
                                                self.performSegue(withIdentifier: self.kMainActionViewSegueIdentifier, sender: nil)
                                            })
                                        }
                                        else {
                                            DispatchQueue.main.async(execute: {
                                                HUD.hideUIBlockingIndicator()
                                                let message = NSLocalizedString("You cannot edit this lesson plan this time. Please try again later.", comment: "")
                                                self.showNotificationMessage(message)
                                            })
                                        }
                                })
                            }
                            else {
                                DispatchQueue.main.async(execute: {
                                    HUD.hideUIBlockingIndicator()
                                    let message = NSLocalizedString("You cannot edit this lesson plan this time. Please try again later.", comment: "")
                                    self.showNotificationMessage(message)
                                })
                            }
                        })
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        let message = NSLocalizedString("You cannot edit this lesson plan this time. Please try again later.", comment: "")
                        self.showNotificationMessage(message)
                    })
                }
            }
        }
        
        if templateid == LPMConstants.Template.DLL || templateid == LPMConstants.Template.SMD {
            let indicatorString = "\(NSLocalizedString("Please wait", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
            
            self.lpmDataManager.requestDetailsForLessonPlanWithID(lpid, template: templateid, handler: { (doneBlock) in
                if doneBlock {
//                    // Work Around: Curriculum Association
//                    self.lessonPlanDataManager.requestAssociatedCurriculumToLessonPlanWithID(lpid, dataBlock: { (data) in
//                        if data != nil {
//                            if let curriculum = data {
//                                if let curriculumID = curriculum["curriculum_id"] as? String, let curriculumTitle = curriculum["curriculum_title"] as? String, let curriculumPeriods = curriculum["curriculum_periods"] as? [[String: String]] {
//                                   
//                                    var curriculum_period_id = ""
//                                    var curriculum_period_name = ""
//                                    
//                                    if let last_period_id = self.lpmDataManager.fetchLastAssociatedCurriculumPeriod() {
//                                        for cp in curriculumPeriods {
//                                            let period_id = self.lpmDataManager.stringValue(cp["period_id"])
//                                            let name = self.lpmDataManager.stringValue(cp["name"])
//                                            
//                                            if last_period_id == period_id {
//                                                curriculum_period_id = last_period_id
//                                                curriculum_period_name = name
//                                            }
//                                        }
//                                    }
//                                    
//                                    let object = ["lp_id": lpid, "curriculum_id": curriculumID, "curriculum_title": curriculumTitle, "curriculum_period_id": curriculum_period_id, "curriculum_period_name": curriculum_period_name]
//                                    let predicate = NSComparisonPredicate(keyPath: "lp_id", withValue: lpid, isExact: true)
//                                    self.lpmDataManager.updateEntity(LPMConstants.Entity.LESSON_OVERVIEW, predicate: predicate, data: object)
//                                    
//                                    if curriculum_period_id != "" {
//                                        self.cpmDataManager.requestLearningCompetencyListForPeriodWithID(curriculum_period_id, completionHandler: { (doneBlock) in
//                                            if let competencies = self.lpmDataManager.fetchPreAssociatedLearningCompetencies() {
//                                                self.cpmDataManager.antiJoinPreAssociatedLearningCompetencies(competencies)
//                                            }
//                                        })
//                                    }
//                                }
//                            }
//                        }
//                        
//                        dispatch_async(dispatch_get_main_queue(), {
//                            HUD.hideUIBlockingIndicator()
//                            self.performSegueWithIdentifier(self.kLessonCreatorViewSegueIdentifier, sender: nil)
//                        })
//                    })
                    
                    if let competencies = self.lpmDataManager.fetchPreAssociatedLearningCompetencies() {
                        var curriculum_id = ""
                        var curriculum_title = NSLocalizedString("Please Select", comment: "")
                        var curriculum_period_id = ""
                        var curriculum_period_name = ""
                        
                        for competency in competencies {
                            curriculum_id = self.lpmDataManager.stringValue(competency.value(forKey: "curriculum_id"))
                            curriculum_period_id = self.lpmDataManager.stringValue(competency.value(forKey: "period_id"))
                        }
                        
                        self.lessonPlanDataManager.requestAssociatedCurriculumToLessonPlan(withID: lpid, dataBlock: { (dataBlock) in
                            if  let data = dataBlock, let id = data["curriculum_id"] as? String,
                                let title = data["curriculum_title"] as? String,
                                let periods = data["curriculum_periods"] as? [[String: String]] {
                                
                                if id == curriculum_id {
                                    curriculum_title = title
                                }
                                
                                for period in periods {
                                    let period_id: String! = self.lpmDataManager.stringValue(period["period_id"])
                                    let period_name: String! = self.lpmDataManager.stringValue(period["period_name"])
                                    
                                    if period_id == curriculum_period_id {
                                        curriculum_period_name = period_name
                                        break
                                    }
                                }
                            }
                            
                            self.cpmDataManager.requestLearningCompetencyListForPeriodWithID(curriculum_period_id, completionHandler: { (doneBlock) in
                                _ = self.cpmDataManager.antiJoinPreAssociatedLearningCompetencies(competencies)
                            })
                            
                            let object = ["lp_id": lpid, "curriculum_id": curriculum_id, "curriculum_title": curriculum_title, "curriculum_period_id": curriculum_period_id, "curriculum_period_name": curriculum_period_name]
                            let predicate = NSComparisonPredicate(keyPath: "lp_id", withValue: lpid, isExact: true)
                            _ = self.lpmDataManager.updateEntity(LPMConstants.Entity.LESSON_OVERVIEW, predicate: predicate, data: object as NSDictionary)
                            
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        self.performSegue(withIdentifier: self.kLessonCreatorViewSegueIdentifier, sender: nil)
                    })
                        })
                    }
                    else {
                        let text = NSLocalizedString("Please Select", comment: "")
                        let object = ["lp_id": lpid, "curriculum_id": "", "curriculum_title": text, "curriculum_period_id": "", "curriculum_period_name": ""]
                        let predicate = NSComparisonPredicate(keyPath: "lp_id", withValue: lpid, isExact: true)
                        _ = self.lpmDataManager.updateEntity(LPMConstants.Entity.LESSON_OVERVIEW, predicate: predicate, data: object as NSDictionary)
                        
                        DispatchQueue.main.async(execute: {
                            HUD.hideUIBlockingIndicator()
                            self.performSegue(withIdentifier: self.kLessonCreatorViewSegueIdentifier, sender: nil)
                        })
                    }
                }
                else {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        let message = NSLocalizedString("You cannot edit this lesson plan this time. Please try again later.", comment: "")
                        self.showNotificationMessage(message)
                    })
                }
            })
        }
     }
    
    // MARK: - Download Lesson Action
    
    func downloadLessonAction() {
        let indicatorString = "\(NSLocalizedString("Downloading", comment: ""))..."
        HUD.showUIBlockingIndicator(withText: indicatorString)
        
        let lessonid = self.selectedLessonObject.value(forKey: "id") as! String
        self.lessonPlanDataManager.requestDownloadLessonPlan(withID: lessonid, contentBlock: { (content) in
            DispatchQueue.main.async(execute: {
                HUD.hideUIBlockingIndicator()
                
                if (content != nil) {
                    self.processSavingOfDownloadedFile(content!)
                }
                else {
                    let message = NSLocalizedString("There was an error downloading this file. Please try again later.", comment: "")
                    self.showNotificationMessage(message)
                }
            })
        })
    }
    
    func processSavingOfDownloadedFile(_ file: [Any]) {
        // Get downloaded file details
        let fileSize = file[0] as! NSNumber
        let fileData = file[1] as! Data
        let fileName = file[2] as! NSString
        
        // Check if directory will become full
        let subdirectoryName = "\(LPMConstants.Sandbox.DIRECTORY_PREFIX)_\(self.lessonPlanDataManager.userEmail())"
        let subdirectoryPath = self.sandboxHelper.makeSubdirectoryInDocumentsDirectory(subdirectoryName)
        let subdirectorySize = self.sandboxHelper.calculateSizeOfDirectoryAtPath(subdirectoryPath)

        let combinedSize = UInt.addWithOverflow(subdirectorySize, fileSize.uintValue)
        let storageFull = combinedSize.overflow || combinedSize.0 > LPMConstants.Sandbox.MAX_DIRECTORY_SIZE
        
        if !storageFull {
            var newFileName = "\(self.selectedLessonObject.value(forKey: "name") as! String).\(fileName.pathExtension)"
            var newFilePath = "\(subdirectoryPath)/\(newFileName)"
            
            // Check if file already exists
            if (self.sandboxHelper.doesFileExistAtPath(newFilePath)) {
                newFileName = self.sandboxHelper.autoChangeFileName(newFileName, atPath: subdirectoryPath)
                newFilePath = "\(subdirectoryPath)/\(newFileName)"
            }
            
            // Map and write file to disk
            let lessonid = self.selectedLessonObject.value(forKey: "id") as! String
            let file = ["file_key": lessonid, "file_name": newFileName]
            let mapped = self.lpmDataManager.mapDownloadedFile(file)
            
            if mapped {
            self.sandboxHelper.saveFileData(fileData, atPath: newFilePath, completion: { (success) in
                let posiviteMessage = "\"\(newFileName)\" \(NSLocalizedString("was successfully downloaded and saved in your device.", comment: ""))"
                let negativeMessage = "\(NSLocalizedString("There was an error saving", comment: "")) \"\(newFileName)\" \(NSLocalizedString("in your device.", comment: ""))"
                self.showNotificationMessage(success ? posiviteMessage : negativeMessage)
                    if !success { _ = self.lpmDataManager.unmapDownloadedFileForKey("file_key", value: lessonid) }
            })
        }
        else {
                let message = "\(NSLocalizedString("There was an error saving", comment: "")) \"\(newFileName)\" \(NSLocalizedString("in your device.", comment: ""))"
                self.showNotificationMessage(message)
            }
        }
        else {
            let message = NSLocalizedString("Not enough space in application storage. Please delete some files from lesson planner directory and try again.", comment: "")
            self.showNotificationMessage(message)
        }
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        self.shouldShowEmptyPlaceholderView((sectionData.numberOfObjects > 0) ? false : true)
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kLessonCellIdentifier, for: indexPath) as! LPMLessonTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: LPMLessonTableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath) 
        
        let name = "\(mo.value(forKey: "name") as! String)"
        cell.lessonTitleLabel.text = name
        
        let start_date = "\(mo.value(forKey: "start_date") as! String)"
        let end_date = "\(mo.value(forKey: "end_date") as! String)"
        cell.lessonTimeFrameLabel.text = "\(NSLocalizedString("Time Frame", comment: "")): \(start_date) \(NSLocalizedString("to", comment: "")) \(end_date)"
        
        let status = "\(mo.value(forKey: "status") as! String)"
        cell.changeLessonStatusLabel(status)
        
        let showActionPopoverButtonAction = #selector(self.showActionPopoverButtonAction(_:))
        cell.actionPopoverButtonExtension.addTarget(self, action: showActionPopoverButtonAction, for: .touchUpInside)
    }
    
    // MARK: - Table View Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedLessonObject = fetchedResultsController.object(at: indexPath) 
        let template_id = self.selectedLessonObject.value(forKey: "template_id") as! String
        let lpid = self.selectedLessonObject.value(forKey: "id") as! String
        let name = self.selectedLessonObject.value(forKey: "name") as! String
        

        self.lpmDataManager.save(object: template_id as AnyObject!, forKey: "LPM_USER_DEFAULT_TEMPLATE_ID")
        self.lpmDataManager.save(object: lpid as AnyObject!, forKey: "LPM_USER_DEFAULT_LESSON_PLAN_ID")
        self.lpmDataManager.save(object: name as AnyObject!, forKey: "LPM_USER_DEFAULT_NAVIGATION_TITLE")
        self.lpmDataManager.save(object: "1" as AnyObject!, forKey: "LPM_USER_DEFAULT_IS_VIEW_MODE")
        
        if template_id == LPMConstants.Template.UBD {
            let indicatorString = "\(NSLocalizedString("Please wait", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)

            self.lessonPlanDataManager.requestLessonPlanDetailsForLessonPlan(withID: lpid) { (success) in
                self.lessonPlanDataManager.requestAssociatedCurriculumToLessonPlan(withID: lpid, dataBlock: { (data) in
                    self.associatedCurriculum = data as NSDictionary?
                    
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        
                        if (success) {
                            if let lessonDetailsObject = self.lessonPlanDataManager.getLessonDetailsObjectForLesson(withID: lpid) {
                                self.selectedLessonObject = lessonDetailsObject
                                self.performSegue(withIdentifier: self.kLessonDetailsViewSegueIdentifier, sender: nil)
                                self.lessonTableView.deselectRow(at: indexPath, animated: true)
                            }
                            else {
                                let message = NSLocalizedString("You cannot access this lesson plan right now. Please try again later.", comment: "")
                                self.showNotificationMessage(message)
                            }
                        }
                        else {
                            let message = NSLocalizedString("You cannot access this lesson plan right now. Please try again later.", comment: "")
                            self.showNotificationMessage(message)
                        }
                    })
                })
            }
        }
        
        if template_id == LPMConstants.Template.DLL || template_id == LPMConstants.Template.SMD {
            let indicatorString = "\(NSLocalizedString("Please wait", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
        
            self.lpmDataManager.requestDetailsForLessonPlanWithID(lpid, template: template_id, handler: { (doneBlock) in
                if doneBlock {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        self.performSegue(withIdentifier: self.kLessonCreatorViewSegueIdentifier, sender: nil)
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        let message = NSLocalizedString("You cannot view this lesson plan this time. Please try again later.", comment: "")
                        self.showNotificationMessage(message)
                    })
                }
            })
        }
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = self.lessonPlanDataManager.mainContext
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kLessonPlanEntity)
        
//        let fetchRequest = NSFetchRequest(entityName: kLessonPlanEntity)
        fetchRequest.fetchBatchSize = 20
        
        if (self.searchKey != "") {
            let predicate = self.lessonPlanDataManager.predicate(forKeyPathContains: "name", value: self.searchKey)
            fetchRequest.predicate = predicate
        }
        
        let sortDescriptor = NSSortDescriptor.init(key: "sorted_date_modified", ascending: self.isAscending)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.lessonTableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.lessonTableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.lessonTableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.lessonTableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.lessonTableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.lessonTableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! LPMLessonTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.lessonTableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.lessonTableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.lessonTableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.lessonTableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == self.kLessonDetailsViewSegueIdentifier {
            let detailsView = segue.destination as? LPMDetailsViewController
            detailsView?.selectedCourseObject = self.selectedCourseObject
            detailsView?.selectedLessonObject = self.selectedLessonObject
            detailsView?.associatedCurriculum = self.associatedCurriculum // Just temporary
        }
    
        if segue.identifier == self.kTemplateViewSegueIdentifier {
            let templateView = segue.destination as? LPMTemplateViewController
            templateView?.courseID = self.selectedCourseObject.value(forKey: "course_id") as! String
        }
    
        if segue.identifier == self.kMainActionViewSegueIdentifier {
            let mainActionView = segue.destination as? LPMMainActionViewController
            mainActionView?.actionType = LPMLessonActionType.edit
            mainActionView?.lessonObject = self.selectedLessonObject
        }
        
        if segue.identifier == self.kMainActionViewSegueIdentifier {
            let lessonCreatorView = segue.destination as? LPMLessonCreatorViewController
            lessonCreatorView?.actionType = LPMLessonActionType.edit
        }
        
        if segue.identifier == self.kFileListViewSegueIdentifier {
            let fileListView = segue.destination as? LPMFileListViewController
            fileListView?.headerTitle = NSLocalizedString("Lesson plan collection", comment: "")
            fileListView?.subDirectoryName = "\(LPMConstants.Sandbox.DIRECTORY_PREFIX)_\(self.lessonPlanDataManager.userEmail())"
            fileListView?.isUploadAction = false
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }
    
    // MARK: - Class Helpers
    
    fileprivate func justifyLabel(_ label: UILabel) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        
        let attributedString = NSAttributedString(string: label.text!, attributes: [NSParagraphStyleAttributeName: paragraphStyle, NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)])
        label.attributedText = attributedString
        label.numberOfLines = 0
    }
    
    fileprivate func managedObjectFromButton(_ button: UIButton, inTableView: UITableView) -> NSManagedObject {
        let buttonPosition = button.convert(CGPoint.zero, to: inTableView)
        let indexPath = inTableView.indexPathForRow(at: buttonPosition)
        let managedObject = self.fetchedResultsController.object(at: indexPath!) 
        
        return managedObject
    }
    
    
    // MARK: - Document Interaction Controller Delegate
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }

}
