//
//  GradebookContainerController.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/9/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "GradebookContainerController.h"
#import <SafariServices/SafariServices.h>


@interface GradebookContainerController ()
@property (nonatomic, strong) UIViewController *containerView;
@end

@implementation GradebookContainerController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupChildViewController];
    [self layoutNotifications];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
}

- (void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        dispatch_queue_t queue = dispatch_queue_create("com.pearson.gradebook.ACTIVITY",DISPATCH_QUEUE_SERIAL);
        dispatch_async(queue, ^{
            ResourceManager *rm = [AppDelegate resourceInstance];
            NSString *details = @"Left the GradeBook module in Apple iPad";
            [rm requestLogActivityWithModuleType:@"6" details:details];
        });
    }
    
    [super viewWillDisappear:animated];
}

- (void)setupChildViewController {
    
    NSString *storyboardName = @"StudentGradeBookStoryboard";
    NSString *userPosition = [[NSString stringWithFormat:@"%@", [VSmart sharedInstance].account.user.position] lowercaseString];
    
    if ([userPosition isEqualToString:@"teacher"]) {
        
        CGFloat version = [[Utils getServerInstanceVersion] floatValue];
        storyboardName = (version < VSMART_SERVER_MAX_VER) ? @"TeacherGradeBookStoryboard" : @"GBStoryboardRRMV2";
        
//        storyboardName = @"TeacherGradeBookStoryboard";
//        
//        // HARD CODING FOR GRADE BOOK MODULE V2
//        storyboardName = @"GBStoryboardRRMV2";
    }
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    self.containerView = [sb instantiateInitialViewController];
    
    // UNCOMMENT FOR WEB VIEW RENDERING
//    self.containerView = [self test];    
    
    [self initiateCustomLayoutFor:self.containerView];
    self.containerView.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.containerView.view.autoresizesSubviews = YES;
    
    [self addChildViewController:self.containerView];
    [self.view addSubview:self.containerView.view];
    [self.containerView didMoveToParentViewController:self];
    [self.view sendSubviewToBack:self.containerView.view];
}

- (UIViewController *)test {
    
    AccountInfo *account = [Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%@", @(account.user.id) ];
    NSString *endpoint = @"/gradebook?is_mobile=1&user_id=";
    NSString *path = [Utils buildUrl: [NSString stringWithFormat:@"%@%@", endpoint, user_id ] ];
    NSURL *url = [NSURL URLWithString:path];
    return [[SFSafariViewController alloc] initWithURL:url entersReaderIfAvailable:NO];
}

- (void)layoutNotifications {
    VS_NCADD(kNotificationProfileHeight, @selector(resizeContainerView:))
}

- (NSString *) dashboardName {
    NSString *moduleName = NSLocalizedString(@"Gradebook", nil);
    return moduleName;
}

#pragma mark - Post Notification Events
- (void)resizeContainerView:(NSNotification *)notification {
    
    VLog(@"Received: kNotificationProfileHeight");
    [UIView animateWithDuration:0.45 animations:^{
        [self initiateCustomLayoutFor:self.containerView];
        [self.containerView.view setNeedsLayout];
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            [super adjustProfileHeight];
        }
        
        [super showOrHideMiniAvatar];
    }];
}

- (void)initiateCustomLayoutFor:(UIViewController *)viewcontroller {
    
    float profileY = [super profileHeight];
    float headerDecrement = ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    float gridHeight = self.view.frame.size.height - headerDecrement;
    float gridY = [super headerSize].size.height + profileY;
    CGRect customFrame = CGRectMake(0, gridY, self.view.frame.size.width, gridHeight);
    
    [viewcontroller.view setFrame:customFrame];
}

@end
