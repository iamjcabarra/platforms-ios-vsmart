//
//  ViewMessageViewController.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 08/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class SSViewMessageViewController: UIViewController {

    
    var delegate:Dimmable?
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    
    var viewMessageDict:[String:String?]?
    
    
    fileprivate lazy var ssdm: SocialStreamDataManager = {
        let dataManager = SocialStreamDataManager.sharedInstance
        return dataManager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate?.dimIn()
        
        let firstName: String = self.ssdm.stringValue(self.viewMessageDict!["first_name"] as AnyObject?)
        let lastName: String = self.ssdm.stringValue(self.viewMessageDict!["last_name"] as AnyObject?)
        let message: String = self.ssdm.stringValue(self.viewMessageDict!["message"] as AnyObject?)
        
        nameLabel.text = "\(firstName) \(lastName)"
        messageTextView.text = "\(message)"
        
        // Do any additional setup after loading the view.
    }

    @IBAction func closeButtonAction(_ sender: AnyObject) {
        
        self.delegate?.dimOut()
        self.dismiss(animated: true, completion: nil)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
