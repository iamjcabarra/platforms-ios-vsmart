//
//  CurriculumDataManager.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/15/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

/////// ENTITY TYPES //////
static NSString *kCurriculumTableEntity = @"CurriculumTable";
static NSString *kCurriculumListEntity = @"CurriculumList";
static NSString *kPeriodTableEntity = @"PeriodTable";
static NSString *kContentTableEntity = @"ContentTable";
static NSString *kContentValuesTableEntity = @"ContentValuesTable";
static NSString *kCourseCategoryEntity = @"CourseCategoryList";

/////// BLOCK TYPES //////
typedef void (^CurriculumDoneBlock)(BOOL status);
typedef void (^CurriculumDataBlock)(NSDictionary *data);
typedef void (^CurriculumContent)(NSString *content);
typedef void (^CurriculumArrayContent)(NSArray *content);

@interface CurriculumDataManager : NSObject

@property (nonatomic, readonly) NSManagedObjectContext *mainContext;

+ (instancetype)sharedInstance;
- (void)saveContext;
- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate;

/////// PUBLIC METHODS //////
- (NSString *)baseURL;
- (NSString *)loginUser;
- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value;
- (NSManagedObject *)getEntity:(NSString *)entity attribute:(NSString *)attribute parameter:(NSString *)parameter context:(NSManagedObjectContext *)context;

- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate;

/////// CURRICULUM ///////
- (void)requestCurriculumListWithBlock:(CurriculumDoneBlock)doneBlock;
- (void)requestCurriculumWithID:(NSString *)curriculumid doneBlock:(CurriculumDoneBlock)doneBlock;
- (void)requestCourseCategoryListForUser:(NSString *)userid doneBlock:(CurriculumDoneBlock)doneBlock;
- (void)requestCurriculumListForCourseCategory:(NSString *)course_category_id andUser:(NSString *)user_id doneBlock:(CurriculumDoneBlock)doneBlock;
- (void)requestDownloadCurriculumWithID:(NSString *)curriculumid contentBlock:(CurriculumArrayContent)contentBlock;

@end
