//
//  SSCommentCell.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 05/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class SSCommentCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var edittedLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var commentLikesLabel: UILabel!
    @IBOutlet weak var commentLikesButton: UIButton!
    @IBOutlet weak var likeCommentButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
