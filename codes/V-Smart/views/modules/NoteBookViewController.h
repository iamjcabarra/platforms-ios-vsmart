//
//  FilterViewController.h
//  V-Smart
//
//  Created by VhaL on 2/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NNoteBook.h"
#import "NoteDataManager.h"
#import "NoteBookCell.h"

@interface NoteBookViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SWTableViewCellDelegate, UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIButton *clearButton;
@property (nonatomic, strong) id delegate;
@property (nonatomic, strong) id parent;
@property (nonatomic, strong) NSMutableArray *noteBookArray;
@property (nonatomic, strong) IBOutlet UITextField *textField;
@property (nonatomic, strong) NoteBook *selectedFolder;
@property (nonatomic, strong) id otherNoteBookViewController;
-(void)loadData;
-(void)castOtherController;
-(void)reflectAddedNoteBookInView:(NoteBook*)newNoteBook;
-(void)reflectModifiedNoteBookInView:(NoteBook*)modifiedNoteBook inIndexPath:(NSIndexPath*)indexPath;
-(void)reflectDeletedNoteBookInView:(NSIndexPath*)indexPath;

@end
