//
//  GenericMenuTableView.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 04/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol GenericMenuDelegate {
    func didFinishSelecting(withType type: String, withData data:[String:AnyObject]?)
}

class GenericMenuTableView: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var delegate: GenericMenuDelegate?
    
    var menu: [[String:String]]!
    var selectedData: [String:AnyObject]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "genericCell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard self.menu != nil else {
            return
        }
        
        var height: CGFloat = 44
        height = height * CGFloat(self.menu.count)
        
        self.tableView.reloadData()
        
        self.preferredContentSize = CGSize(width: 150, height: height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard self.menu != nil else {
            return 0
        }
        
        
        return (menu?.count)!
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "genericCell", for: indexPath)
        
        let menuItem = menu[(indexPath as NSIndexPath).row]
        let title = menuItem["title"]
        
        let localizedTitle = NSLocalizedString(title!, comment: "")
        
        cell.textLabel?.text = localizedTitle
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuItem = menu[(indexPath as NSIndexPath).row]
        let type = menuItem["type"]
        
        self.dismiss(animated: true, completion: nil)
        self.delegate?.didFinishSelecting(withType: type!, withData: self.selectedData)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
