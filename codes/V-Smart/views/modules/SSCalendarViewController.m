//
//  SSCalendarViewController.m
//  V-Smart
//
//  Created by VhaL on 3/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "SSCalendarViewController.h"
#import "SSCalendarCell.h"
#import "SSCalendarItem.h"
#import "JMSCalendarItem.h"
#import "SSCalendarWeekItem.h"

@interface SSCalendarViewController ()

@property (nonatomic, strong) NSMutableArray *entriesMain;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *refreshControlTableView;
@end

@implementation SSCalendarViewController
@synthesize parent;

- (void)viewDidLoad{
    [super viewDidLoad];
    
    UINib *cellNib = [UINib nibWithNibName:@"SSCalendarCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"SSCalendarCell"];

    [self addRefreshMode];
    [self initializeData];
}

-(AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

-(void)addRefreshMode{
    self.refreshControlTableView = [[UIRefreshControl alloc] init];
    NSString *pullRefresh = NSLocalizedString(@"Pull to Refresh",nil);
    self.refreshControlTableView.attributedTitle = [[NSAttributedString alloc] initWithString:pullRefresh];
    [self.refreshControlTableView addTarget:self action:@selector(initializeData) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshControlTableView];
}

-(void)initializeData{
    self.entriesMain = [[NSMutableArray alloc] init];
    
    NSString *url = VS_FMT([Utils buildUrl:kEndPointSSCalendar], [self account].user.id);
    NSLog(@"get calendar : %@", url);
    
    [JSONHTTPClient getJSONFromURLWithString:url completion:^(NSDictionary *json, JSONModelError *err) {
       
        if(err == nil){
            NSArray *arrayRecords = [json objectForKey:@"records"];
            NSMutableArray *array = [arrayRecords mutableCopy];
            NSDictionary *dict = @{@"records": array};
            
            NSError *parseError = nil;
            
            NSData *records = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
            NSString *jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
            
            JMSRecordCalendarItem *calendarRecords = [[JMSRecordCalendarItem alloc] initWithString:jsonString error:&parseError];
            
            NSMutableArray *rawCalendarEntries = [NSMutableArray new];
            
            for (JMSCalendarItem *calendarItem in calendarRecords.records) {
                NSDate *dateStart = [calendarItem.eventStart dateFromString];
                NSDate *dateEnd = [calendarItem.eventEnd dateFromString];
                BOOL isDateEmpty = (dateStart == nil || dateEnd == nil);
                
                if (!isDateEmpty) {
                    //if same day, no prob :)
                    if ([dateStart getDay] == [dateEnd getDay]){
                        SSCalendarItem *item = [SSCalendarItem new];
                        item.dateKey = dateStart;
                        item.date = [dateStart stringWithDateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
                        item.detail = calendarItem.title;
                        item.duration = [NSString stringWithFormat:@"%@ - %@",
                                         [dateStart stringWithDateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle],
                                         [dateEnd stringWithDateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle]];
                        item.timeCoverage = [dateEnd getDurationFrom:dateStart];
                        
                        NSLog(@"date:%@ detail:%@ duration:%@ timeCov:%@", item.date, item.detail, item.duration, item.timeCoverage);
                        [rawCalendarEntries addObject:item];
                    }
                    else{
                        NSDate *dateStart12AM = [[dateStart stringWithFormat:@"yyyy-MM-dd 00:00:00"] dateFromString];
                        NSDate *dateStart1159PM = [[dateStart stringWithFormat:@"yyyy-MM-dd 23:59:59"] dateFromString];
                        NSDate *dateEnd12AM = [[dateEnd stringWithFormat:@"yyyy-MM-dd 00:00:00"] dateFromString];
                        
                        //add initial
                        SSCalendarItem *item = [SSCalendarItem new];
                        item.dateKey = dateStart;
                        item.date = [dateStart stringWithDateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
                        item.detail = calendarItem.title;
                        item.duration = [NSString stringWithFormat:@"%@ - %@",
                                         [dateStart stringWithDateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle],
                                         [dateStart1159PM stringWithDateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle]];
                        item.timeCoverage = [dateStart1159PM getDurationFrom:dateStart];
                        
                        NSLog(@"date:%@ detail:%@ duration:%@ timeCov:%@", item.date, item.detail, item.duration, item.timeCoverage);
                        [rawCalendarEntries addObject:item];
                        
                        
                        bool loop = YES;
                        
                        do {
                            dateStart12AM = [dateStart12AM dateByAddingTimeInterval:60*60*24]; //this corresponds to add 1 day
                            dateStart1159PM = [dateStart1159PM dateByAddingTimeInterval:60*60*24]; //this corresponds to add 1 day
                            
                            //check if this should be the last call
                            if ([dateStart12AM isEqualToDate:dateEnd12AM]){
                                loop = NO;
                            }
                            else{
                                SSCalendarItem *item = [SSCalendarItem new];
                                item.dateKey = dateStart12AM;
                                item.date = [dateStart12AM stringWithDateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
                                item.detail = calendarItem.title;
                                item.duration = [NSString stringWithFormat:@"%@ - %@",
                                                 [dateStart12AM stringWithDateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle],
                                                 [dateStart1159PM stringWithDateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle]];
                                item.timeCoverage = [dateStart1159PM getDurationFrom:dateStart12AM];
                                
                                NSLog(@"date:%@ detail:%@ duration:%@ timeCov:%@", item.date, item.detail, item.duration, item.timeCoverage);
                                [rawCalendarEntries addObject:item];
                            }
                            
                        } while (loop);
                        
                        
                        item = [SSCalendarItem new];
                        item.dateKey = dateStart12AM;
                        item.date = [dateStart12AM stringWithDateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
                        item.detail = calendarItem.title;
                        item.duration = [NSString stringWithFormat:@"%@ - %@",
                                         [dateStart12AM stringWithDateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle],
                                         [dateEnd stringWithDateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle]];
                        item.timeCoverage = [dateEnd getDurationFrom:dateStart12AM];
                        
                        NSLog(@"date:%@ detail:%@ duration:%@ timeCov:%@", item.date, item.detail, item.duration, item.timeCoverage);
                        [rawCalendarEntries addObject:item];
                    }
                }
            }
            
            NSSortDescriptor *sortByDate = [[NSSortDescriptor alloc] initWithKey:@"dateKey" ascending:NO];
            rawCalendarEntries = [NSMutableArray arrayWithArray:[rawCalendarEntries sortedArrayUsingDescriptors:@[sortByDate]]];
            
            for (SSCalendarItem *item in rawCalendarEntries) {
                //then check its weekday
                NSDate *startDate;
                NSDate *endDate;
                [NSDate getStartDayAndEndDayOfTheWeek:item.dateKey startDate:&startDate endDate:&endDate];
                
                NSString *weekName = [NSString stringWithFormat:@"%@ - %@",
                                      [startDate stringWithDateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterNoStyle],
                                      [endDate stringWithDateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterNoStyle]];
                
                
                SSCalendarWeekItem *weekItemEntry;
                
                bool isExisting = NO;
                for (SSCalendarWeekItem *weekItem in self.entriesMain) {
                    if ([weekItem.week isEqualToString:weekName]){
                        isExisting = YES;
                        weekItemEntry = weekItem;
                        break;
                    }
                }
                
                if (!isExisting){
                    weekItemEntry = [[SSCalendarWeekItem alloc] init];
                    [self.entriesMain addObject:weekItemEntry];
                    weekItemEntry.week = weekName;
                }
                
                [weekItemEntry.weekEntries addObject:item];
            }
            
            

        }
        else{
            /* localizable strings */
            NSString *connectionErrorMessage = NSLocalizedString(@"Please make sure you are connected to the server", nil); //checked
            [self.view makeToast:connectionErrorMessage duration:2.0f position:@"center"];
        }
        
        [self.tableView reloadData];
        [self.refreshControlTableView endRefreshing];
    }];


}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.entriesMain.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *cellHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 22)];
    UILabel *cellLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.tableView.frame.size.width-5, 22)];
    [cellHeader addSubview:cellLabel];
    [cellLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:14]];
    [cellLabel setTextColor:[UIColor darkTextColor]];
    
    SSCalendarWeekItem *weekItem = (SSCalendarWeekItem*)[self.entriesMain objectAtIndex:section];
    cellLabel.text = weekItem.week;
    
    [cellHeader setBackgroundColor:[UIColor whiteColor]];
    return cellHeader;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    SSCalendarWeekItem *weekItem = (SSCalendarWeekItem*)[self.entriesMain objectAtIndex:section];
    return weekItem.weekEntries.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 22;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SSCalendarCell *cell = (SSCalendarCell*)[tableView dequeueReusableCellWithIdentifier:@"SSCalendarCell"];
    if (cell==nil) {
        cell = [[SSCalendarCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SSCalendarCell"];
    }
    
    SSCalendarWeekItem *weekItem = (SSCalendarWeekItem*)[self.entriesMain objectAtIndex:indexPath.section];
    SSCalendarItem *item = (SSCalendarItem*)[weekItem.weekEntries objectAtIndex:indexPath.row];
    
    cell.date.text = item.date;
    cell.detail.text = item.detail;
    cell.duration.text = item.duration;
    cell.timeCoverage.text = item.timeCoverage;
    
    return cell;
}

@end
