//
//  SSCommentViewController.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 05/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class SSCommentViewController: UIViewController, NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, GenericMenuDelegate, UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var commentTxtField: UITextField!
    @IBOutlet weak var sendCommentButton: UIButton!
    @IBOutlet weak var textFieldBottomConstraint: NSLayoutConstraint!
    var message_id: String!
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: UILabel!
    
    @IBOutlet weak var commentFieldHeight: NSLayoutConstraint!
//    var keyboardHeight:CGFloat = 0.0
    
    // MARK: - Singleton Method
    fileprivate lazy var ssdm: SocialStreamDataManager = {
        let dataManager = SocialStreamDataManager.sharedInstance
        return dataManager
    }()
    
    var menuViewController : GenericMenuTableView?
    
    var edit_comment_id : String? = nil
    var show_like_comment_id : String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.headerLabel.text = NSLocalizedString("Comments", comment: "")
        
        self.tableView.estimatedRowHeight = 200
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        
        self.sendCommentButton.addTarget(self, action: #selector(self.sendMessageButtonAction(_:)), for: .touchUpInside)
        self.commentTxtField.delegate = self
        self.commentTxtField.placeholder = NSLocalizedString("Write your comment...", comment: "")
        
        self.menuViewController = GenericMenuTableView(nibName: "GenericMenuTableView", bundle: nil)
        self.menuViewController!.modalPresentationStyle = UIModalPresentationStyle.popover
        self.menuViewController?.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonAction(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func sendMessageButtonAction(_ b: UIButton) {
        let comment = self.commentTxtField.text!
        let arrayOfBadwords = self.ssdm.getAllBadwords(inMessage: comment)

        guard comment.characters.count > 0 else {
            displayAlert(withTitle: "", withMessage: NSLocalizedString("Cannot post empty message", comment: ""))
            return
        }

        if arrayOfBadwords?.count > 0 {
            // HAS BADWORD DO NOT POST
            //            let stringOfBadwords = arrayOfBadwords.joinWithSeparator(", ") // "1, 2, 3"
            //            var message = stringOfBadwords + " is a bad word! "
            //
            //            if arrayOfBadwords.count > 1 {
            //                message = stringOfBadwords + " are bad words!!!"
            //            }
            
            let message = NSLocalizedString("Oops! Your post contains vulgar words. V-Smart does not tolerate this.", comment: "")
            
            displayAlert(withTitle: NSLocalizedString("Invalid Entry", comment: ""), withMessage: message)
        } else {
            
            // NEW COMMENT
            if self.edit_comment_id == nil || self.edit_comment_id?.characters.count == 0 {
            var commendDict = [String:AnyObject]()
            commendDict = ["message_id":self.message_id as AnyObject,
                           "comment":comment as AnyObject]
            
            self.ssdm.postComment(withData: commendDict, handler: { (status) in
                if status == true {
                    DispatchQueue.main.async(execute: {
                            self.commentTxtField.text = ""
                        });
                    }
                })
            }
            
            // EDIT
            if self.edit_comment_id != nil && self.edit_comment_id?.characters.count > 0 {
                let commentData = ["comment":self.commentTxtField.text!]
                self.ssdm.editComment(withID: self.edit_comment_id!, withData: commentData as [String : AnyObject], handler: { (status) in
                    if status == true {
                        self.edit_comment_id = nil
                        DispatchQueue.main.async(execute: {
                            self.commentTxtField.text = ""
                    });
                }
            })
        }
            
        }
    }
    
    func displayAlert(withTitle title: String, withMessage message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Table View Data Source
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        self.emptyView.isHidden = (sectionCount > 0)
        self.emptyLabel.text = (sectionCount > 0) ? "" : NSLocalizedString("No comments in this post", comment: "")
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        let count = sectionData.numberOfObjects
        self.emptyView.isHidden = (count > 0)
        self.emptyLabel.text = (count > 0) ? "" : NSLocalizedString("No comments in this post", comment: "")
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SS_COMMENT_CELL_ID", for: indexPath)
        cell.selectionStyle = .none
        
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: UITableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        let commentCell = cell as! SSCommentCell
        //        avatar, first_name, last_name
        
        let user_id = self.ssdm.stringValue(mo.value(forKey: "user_id"))
        let loggedInUserID = self.ssdm.accountUserID()
        
        let owned = (user_id == loggedInUserID)
        
        let avatar = self.ssdm.stringValue(mo.value(forKey: "avatar"))
        
        let first_name = self.ssdm.stringValue(mo.value(forKey: "first_name"))
        let last_name = self.ssdm.stringValue(mo.value(forKey: "last_name"))
        let comment = self.ssdm.stringValue(mo.value(forKey: "comment"))
        let is_edited = self.ssdm.stringValue(mo.value(forKey: "is_edited"))
        let comment_like_count = self.ssdm.stringValue(mo.value(forKey: "comment_like_count"))
        let is_liked = self.ssdm.stringValue(mo.value(forKey: "is_liked"))
        
        commentCell.nameLabel.text = "\(first_name!) \(last_name!)"
        commentCell.commentLabel.text = comment
        
        commentCell.tag = (indexPath as NSIndexPath).row
        
        let avatarURL = URL(string: avatar!)!
        self.getDataFromUrl(avatarURL) { (data, response, error) in
            guard let data = data, error == nil else { return }
            if commentCell.tag == (indexPath as NSIndexPath).row {
                DispatchQueue.main.async(execute: {
                    commentCell.userImage.image = UIImage(data: data)
                })
            }
        }
        
        commentCell.commentLikesLabel.isHidden = (comment_like_count == "0" || comment_like_count == "")
        commentCell.commentLikesButton.isHidden = (comment_like_count == "0" || comment_like_count == "")
        commentCell.commentLikesLabel.text = "(\(comment_like_count!))"
        
        commentCell.likeCommentButton.isSelected = (is_liked == "1")
        
//        commentCell.edittedLabel.isHidden = (is_edited == "1") ? false : true
        
        commentCell.menuButton.isHidden = (owned) ? false : true
        commentCell.menuButton.addTarget(self, action: #selector(self.menuButtonAction(_:)), for: .touchUpInside)
        
        commentCell.likeCommentButton.addTarget(self, action: #selector(self.likeButtonAction(_:)), for: .touchUpInside)
        
        commentCell.commentLikesButton.addTarget(self, action: #selector(self.showLikeButtonAction(_:)), for: .touchUpInside)
        
        let date_modified_date = mo.value(forKey: "date_modified_date") as! Date
        let extra_string = (is_edited == "1") ? "Edited" : ""
        commentCell.edittedLabel.text = "\(date_modified_date.elapsedTime) \(extra_string)"
    }
    
    func likeButtonAction(_ b: UIButton) {
//        b.enabled = false
        let buttonPosition = b.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let object = self.fetchedResultsController.object(at: indexPath!) 
        
        // isLike == true, WILL PERFORM LIKE
        let isLike = (b.isSelected) ? false : true
        
        let comment_id = self.ssdm.stringValue(object.value(forKey: "comment_id"))
        
        let data: [String:AnyObject] = ["content_id":comment_id as AnyObject!,
                                        "is_message":0 as AnyObject,
                                        "is_deleted":0 as AnyObject]
        
        if isLike {
            self.ssdm.postLikeMessage(withData: data, withHandler: { (done, error) in
                
                if error == nil {
                    
                } else {
                    self.displayAlert(withTitle: "", withMessage: error!)
                }
                
                DispatchQueue.main.async(execute: {
                    b.isEnabled = true
                })
            })
        } else {
            self.ssdm.postUnlikeComment(withData: data, withHandler: { (done, error) in
                if error == nil {
                    
                } else {
                    self.displayAlert(withTitle: "", withMessage: error!)
                }
                DispatchQueue.main.async(execute: {
                    b.isEnabled = true
                })
            })
        }
    }
    
    func menuButtonAction(_ b: UIButton) {
        let buttonPosition = b.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let object = self.fetchedResultsController.object(at: indexPath!)
        
        
        let user_id = self.ssdm.stringValue(object.value(forKey: "user_id"))
        let loggedInUserID = self.ssdm.accountUserID()
        
        var data:[String:AnyObject] = [String:AnyObject]()
        data["comment_id"] = self.ssdm.stringValue(object.value(forKey: "comment_id")) as AnyObject?
        data["comment"] = self.ssdm.stringValue(object.value(forKey: "comment")) as AnyObject
        data["user_id"] = self.ssdm.stringValue(object.value(forKey: "user_id")) as AnyObject
        
        let owned = (user_id == loggedInUserID)
        
        
        var menuList = [[String:String]]()
        // not mine
        if owned == false {
            
        }
        
        // mine
        if owned == true {
                menuList = [
                    ["title":"Edit",
                        "type":"edit"],
                    ["title":"Delete",
                        "type":"delete"]]
        }
        
        self.menuViewController?.menu = menuList
        self.menuViewController?.selectedData = data
        let popover: UIPopoverPresentationController = self.menuViewController!.popoverPresentationController!
        popover.sourceView = b
        popover.sourceRect = b.bounds
        popover.delegate = self
        popover.permittedArrowDirections = UIPopoverArrowDirection.right
        present(self.menuViewController!, animated: true, completion:nil)
    }
    
    func didFinishSelecting(withType type: String, withData data: [String : AnyObject]?) {
        guard data != nil else {
            return
        }
        
        
        let user_id = self.ssdm.stringValue(data!["user_id"])
        let loggedInUserID = self.ssdm.accountUserID()
        
        let owned = (user_id == loggedInUserID)
        
        if owned {
            let comment_id = self.ssdm.stringValue(data!["comment_id"])
            let comment = self.ssdm.stringValue(data!["comment"])
            
            if type == "edit" {
                self.edit_comment_id = comment_id
                
                DispatchQueue.main.async(execute: { 
                  self.commentTxtField.text = comment
                })
            }
            
            if type == "delete" {
                let alertView = UIAlertController(title: "", message: NSLocalizedString("Are you sure you want to delete this comment?", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
                
                let yesAction = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: UIAlertActionStyle.default) { (Alert) -> Void in
                    self.ssdm.removeComment(withID: comment_id!, completion: { (done, error) in
                        if error == nil {
                            
                        } else {
                            self.displayAlert(withTitle: "", withMessage: error!)
//                            self.displayAlert(withTitle: error!)
                        }
                    })
                }
                
                let noAction = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: UIAlertActionStyle.cancel) { (Alert) -> Void in
                }
                
                alertView.addAction(noAction)
                alertView.addAction(yesAction)
                
                DispatchQueue.main.async {
                    self.present(alertView, animated: true, completion: nil)
                }
                
            }
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func getDataFromUrl(_ url:URL, completion: @escaping ((_ data: Data?, _ response: URLResponse?, _ error: Error? ) -> Void)) {
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            completion(data, response, error)
        }.resume()
        
        
        
//        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
//            completion(data, response, error)
//            }) .resume()
    }
    
    
    
    func showLikeButtonAction(_ b: UIButton) {
        let buttonPosition = b.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let object = self.fetchedResultsController.object(at: indexPath!)
        
        let content_id = self.ssdm.stringValue(object.value(forKey: "comment_id"))
        self.show_like_comment_id = content_id
        
        self.performSegue(withIdentifier: "SEGUE_SHOW_COMMENT_LIKES", sender: nil)
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.commentFieldHeight.constant = self.commentFieldHeight.constant + 100
        
        UIView.animate(withDuration: 0.3, animations: { 
            self.view.layoutIfNeeded()
        }) 
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.commentFieldHeight.constant = 46
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }) 
    }
    
    // MARK: - Fetched Results Controller
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = ssdm.getMainContext()
        
        let ascending = true
        
        let entity = SSMConstants.Entity.COMMENTS
        
//        let fetchRequest = NSFetchRequest(entityName: entity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)

        fetchRequest.fetchBatchSize = 20
        
        let predicate = NSComparisonPredicate(keyPath: "message_id", withValue: message_id, isExact: true)
        fetchRequest.predicate = predicate
        
        let sortDescriptor = NSSortDescriptor(key: "date_created_date", ascending: ascending)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        let tableViewController = tableView
        
        switch type {
        case .insert:
            tableViewController?.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableViewController?.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        let tableViewController = tableView
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableViewController?.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = tableViewController?.cellForRow(at: indexPath) {
                    configureCell(cell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableViewController?.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        tableView.reloadData()
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SEGUE_SHOW_COMMENT_LIKES" {
            let ssslv = segue.destination as! SSShowLikesViewController
            ssslv.entity = SSMConstants.Entity.COMMENT_LIKES
            ssslv.predicate_value = self.show_like_comment_id
            ssslv.predicate_keypath = "content_id"
//            ssslv.headerLabel.text = NSLocalizedString("Comment Likes", comment: "")
        }
    }
    
}
