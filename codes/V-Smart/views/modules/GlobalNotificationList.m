//
//  GlobalNotificationList.m
//  V-Smart
//
//  Created by Ryan Migallos on 2/23/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "GlobalNotificationList.h"
#import "GlobalNotificationCell.h"
#import "GlobalNotificationDetail.h"
#import "ResourceManager.h"
#import "AppDelegate.h"
#import "EGOImageLoader.h"

@interface GlobalNotificationList () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) ResourceManager *rm;

@property (nonatomic, strong) IBOutlet UITableView *tableView;

- (IBAction)unwindFromConfirmationForm:(id)sender;

@end

@implementation GlobalNotificationList

static NSString *kCellIdentifier = @"cell_global_notif_id";

- (IBAction)unwindFromConfirmationForm:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //execute fetch request
    self.rm = [AppDelegate resourceInstance];
    self.managedObjectContext = self.rm.mainContext;
    
    dispatch_queue_t queue = dispatch_queue_create("com.vsmart.global.NOTIFICATION",DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        [self.rm requestNotificationWithClearData:YES contentBlock:^(NSString *content) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGlobalReadUpdates object:self];
            });
        }];
    });
    
//    self.preferredContentSize = CGSizeMake(300, 350);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (void)configureCell:(UITableViewCell *)object atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *user_name = [NSString stringWithFormat:@"%@ %@", [mo valueForKey:@"first_name"], [mo valueForKey:@"last_name"]];
    NSString *group_name = [NSString stringWithFormat:@"%@ %@", [mo valueForKey:@"course_name"], [mo valueForKey:@"course_section"] ];
    NSString *action_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"action"] ];
    NSString *date_modified = [NSString stringWithFormat:@"%@", [mo valueForKey:@"date_modified"] ];
    NSURL *imageURL = [NSURL URLWithString: [mo valueForKey:@"avatar"] ];
    BOOL isSeen = [[mo valueForKey:@"is_seen"] boolValue];
    
    GlobalNotificationCell *cell = (GlobalNotificationCell *)object;
    
    cell.usernameLabel.text = user_name;
    cell.groupLabel.text = group_name;
    cell.commentLabel.text = action_name;
    cell.dateLabel.text = date_modified;
    cell.avatarImage.image = [[EGOImageLoader sharedImageLoader] imageForURL:imageURL shouldLoadWithObserver:nil];
    cell.seenLabel.hidden = isSeen;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *content_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"content_id"] ];
    NSString *notification_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"notification_id"] ];
    
    [self.rm setNotificationAsSeen:notification_id];

    __weak typeof (self) weakObj = self;
    
    [self.rm requestNotificationDetailsWithMessageID:content_id doneBlock:^(BOOL status) {
        
        if (status == NO) {
            dispatch_async(dispatch_get_main_queue(), ^{
                AlertWithMessageAndDelegate(@"Message", @"Content no longer exists", weakObj);
                [weakObj.tableView deselectRowAtIndexPath:indexPath animated:YES];
            });
            
        }
        
        if (status == YES) {
            [self performSegueWithIdentifier:@"showMessageDetail" sender:self];
        }
        
    }];
    
    //POST NOTIFICATION
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGlobalReadUpdates object:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"showMessageDetail"]) {

        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSString *group_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"group_id"] ];
        NSString *content_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"content_id"] ];
        NSString *user_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ];

        GlobalNotificationDetail *detail = (GlobalNotificationDetail *)segue.destinationViewController;
        detail.group_id = group_id;
        detail.message_id = content_id;
        detail.user_id = user_id;
    }
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    //    BOOL isAscending = self.sortOrderAscending;
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.managedObjectContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"GlobalNotification"];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Predicate
    NSPredicate *predicate = [self.rm predicateForKeyPath:@"is_deleted" andValue:@"0"];
    [fetchRequest setPredicate:predicate];
    
    // Edit the sort key as appropriate.
    //    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"sort_date" ascending:isAscending];
    //    NSSortDescriptor *question_text = [NSSortDescriptor sortDescriptorWithKey:@"question_text" ascending:isAscending];
    //    NSSortDescriptor *question_id = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:isAscending];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"content_id" ascending:NO];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
