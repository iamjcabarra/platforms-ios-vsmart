//
//  GradeBookCustomSectionHeader.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/13/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GradeBookCustomSectionHeader : UITableViewHeaderFooterView

@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;

@end
