//
//  CPSeatPlanCollectionViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 31/01/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class CPSeatPlanCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var studentImage: UIImageView!
    @IBOutlet weak var attendanceStatusImage: UIImageView!
    @IBOutlet weak var studentNameLabel: UILabel!
    
    func changeAttendaceStatusImage(forStatus status: String) {
        var imageName = "present_152.png"
        if status == "1" { imageName = "late_152.png" }
        if status == "2" { imageName = "absent_152.png"}
        self.attendanceStatusImage.image = UIImage(named: imageName)
    }
    
}
