//
//  CheckBoxCell.m
//  V-Smart
//
//  Created by Carmelito Bayarcal on 14/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "CheckBoxCell.h"

@implementation CheckBoxCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse {
    self.checkBoxButton.selected = NO;
}

@end
