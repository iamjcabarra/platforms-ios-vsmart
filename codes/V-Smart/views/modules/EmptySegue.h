//
//  EmptySegue.h
//  V-Smart
//
//  Created by Julius Abarra on 9/18/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptySegue : UIStoryboardSegue

@end
