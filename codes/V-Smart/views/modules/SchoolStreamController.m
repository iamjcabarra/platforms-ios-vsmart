//
//  SchoolStreamController.m
//  V-Smart
//
//  Created by Ryan Migallos on 2/13/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "AppDelegate.h"
#import "SchoolStreamController.h"
#import "SchoolStreamFeedCell.h"
#import "SocialStreamStickerCell.h"
#import "SocialStreamMessage.h"
#import "SocialStreamPeopleWhoLike.h"
#import "SocialStreamComment.h"
#import "JMSGroupItem.h"
#import <Parse/Parse.h>
#import <QuartzCore/QuartzCore.h>

#import "NoteHelper.h"
//#import "MainHeader.h"

@interface SchoolStreamController () <NSFetchedResultsControllerDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIButton *messageButton;
@property (nonatomic, strong) IBOutlet UIButton *teacherButton;
@property (nonatomic, strong) IBOutlet UIButton *announcemenButton;

@property (nonatomic, strong) JMSRecordGroupItem *groupItems;
@property (nonatomic, strong) JMSGroupItem *currentGroup;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) ResourceManager *rm;

@property (nonatomic, assign) CGFloat lastContentOffset;
@property (nonatomic, assign) BOOL loadingPrevData;
@property (nonatomic, assign) BOOL hideCommentBox;

@end

@implementation SchoolStreamController

static NSString *kCellTextIdentifier = @"cell_ss_text_id";
static NSString *kCellStickerIdentifier = @"cell_ss_sticker_id";

- (AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

- (void)viewDidLoad
{
    NSLog(@"School Stream Controller...");
    
    //execute fetch request
    self.rm = [AppDelegate resourceInstance];
    self.managedObjectContext = self.rm.mainContext;
    
    self.tableView.estimatedRowHeight = 44.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    //SOCKET IO
//    NSString *socketio = @"lms.vsmart.info";
//    NSInteger port = 8888;
    
    NSString *socketio = @"vibe.vsmartschool.me";
    NSInteger port = 8000;
    
    [self.rm reconnectSocketWithURL:socketio port:port];
    
    [self.messageButton addTarget:self
                           action:@selector(actionShowMessageBox:)
                 forControlEvents:UIControlEventTouchUpInside];
    
    [self.teacherButton addTarget:self action:@selector(loadStream:) forControlEvents:UIControlEventTouchUpInside];
    
    [self loadStream:self.teacherButton];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //CLOSE SOCKET IO
    [self.rm closeSocket];
}

- (void)loadStream:(id)sender
{
//    UIButton *b = (UIButton *)sender;
//    NSUInteger teacherGroupID =  (b == self.teacherButton) ? 1 : 0;
    [self.rm requestSocialStreamMessageWithGroupID:@"1" doneBlock:^(BOOL status) {
        if (status) {
            NSLog(@"requestSocialStreamMessageWithGroupID DONE!!!");
        }
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//    NSString *type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"type"] ];
//    NSString *cellid = ([type isEqualToString:@"message"]) ? kCellTextIdentifier : kCellStickerIdentifier;

    NSString *cellid = kCellTextIdentifier;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)object atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
//    NSString *type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"type"] ];
    
    NSString *user_name = [NSString stringWithFormat:@"%@ %@", [mo valueForKey:@"first_name"], [mo valueForKey:@"last_name"] ];
    NSString *date_modified = [[mo valueForKey:@"date_modified"] description];
    NSString *message = [[mo valueForKey:@"message"] description];
    BOOL hidden = ([[mo valueForKey:@"owned"] isEqualToString:@"1"]) ? NO : YES;
    BOOL is_liked = ([[mo valueForKey:@"is_liked"] isEqualToString:@"1"]) ? YES : NO;
    
    SchoolStreamFeedCell *cell = (SchoolStreamFeedCell *)object;
    
    cell.userNameLabel.text = user_name;
    cell.firstName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"] ];
    cell.lastName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"] ];
    
    cell.emotionLabel.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"icon_text"] ];
    
    cell.likeCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"likeCount"] ];
    cell.commentCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"commentCount"] ];
    
    cell.dateLabel.text = date_modified;
    cell.messageLabel.text = message;
    
    //ENABLE OPTION BUTTON
    cell.optionButton.hidden = hidden;
    
    //SHOW LIKE LIST
    [cell.peopleWhoLikeButton addTarget:self action:@selector(actionShowPeopleWhoLike:) forControlEvents:UIControlEventTouchUpInside];
    
    //SHOW COMMENT LIST
    [cell.peopleWhoComment addTarget:self action:@selector(actionShowWhoComment:) forControlEvents:UIControlEventTouchUpInside];
    
    //LIKE BUTTON ACTION
    [cell.likeButton addTarget:self action:@selector(actionLike:) forControlEvents:UIControlEventTouchUpInside];
    //SET SELECTED
    cell.likeButton.selected = is_liked;
    
    //ADD TO NOTE
    [cell.noteButton addTarget:self action:@selector(actionAddNote:) forControlEvents:UIControlEventTouchUpInside];
    
    //COMMENT BOX
    [cell.commentButton addTarget:self action:@selector(actionShowCommentBox:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell showAlias];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Return NO if you do not want the specified item to be editable.
    
    //    return YES;
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return [[mo valueForKey:@"owned"] isEqualToString:@"1"];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSMutableDictionary *data = [@{@"message_id":[mo valueForKey:@"message_id"],
                                       @"user_id":[mo valueForKey:@"user_id"] } mutableCopy];
        [self.rm requestRemoveMessage:data doneBlock:^(BOOL status) {
            if (status) {
                NSLog(@"delete successful...");
            }
        }];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if ([[mo valueForKey:@"owned"] isEqualToString:@"1"]) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [self performSegueWithIdentifier:@"showMessageBox" sender:cell];
    }
}

- (NSManagedObject *)managedObjectFromButtonAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (void)actionShowMessageBox:(id)sender
{
    [self performSegueWithIdentifier:@"showMessageBox" sender:sender];
}

- (void)actionShowPeopleWhoLike:(id)sender
{
//    [self performSegueWithIdentifier:@"showPeopleWhoLikeBox" sender:sender];
}

- (void)actionShowWhoComment:(id)sender
{
    self.hideCommentBox = YES;
//    [self performSegueWithIdentifier:@"showCommentBox" sender:sender];
}

- (void)actionShowCommentBox:(id)sender
{
    self.hideCommentBox = NO;
//    [self performSegueWithIdentifier:@"showCommentBox" sender:sender];
}

- (void)actionLike:(id)sender
{
    //POSSIBLE BUG #
    UIButton *starButton = (UIButton *)sender;
    
    NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
    NSString *avatar = [NSString stringWithFormat:@"%@", [mo valueForKey:@"avatar"] ];
    NSString *username = [NSString stringWithFormat:@"%@", [mo valueForKey:@"username"] ];
    NSString *user_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ];
    NSString *message_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];
    
    //LIKE MESSAGE
    NSMutableDictionary *d = [@{@"avatar":avatar,
                                @"username":username,
                                @"user_id":user_id,
                                @"content_id":message_id,
                                @"is_message":@1,
                                @"is_deleted":@0} mutableCopy];
    
    if (!starButton.selected) {
        [self.rm postLikeMessage:d doneBlock:^(BOOL status) {
            if (status) {
                NSLog(@"success like message...");
            }
        }];
    }
    
    if (starButton.selected) {
        [self.rm postUnlikeMessage:d doneBlock:^(BOOL status) {
            if (status) {
                NSLog(@"success unlike message...");
            }
            
        }];
    }
}

- (void)actionAddNote:(id)sender
{
    NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
    NSString *messageContent = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message"] ];
    
    __weak typeof (self) ws = self;
    NoteHelper *nh = [[NoteHelper alloc] init];
    [nh saveMessage:messageContent doneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            AlertWithMessageAndDelegate(@"Note Book", @"Added Note", ws);
        });
    }];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SocialStreamFeed" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Predicate
    NSPredicate *predicate = [self.rm predicateForKeyPath:@"is_deleted" andValue:@"0"];
    [fetchRequest setPredicate:predicate];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"message_id" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:self.managedObjectContext
                                                                                                  sectionNameKeyPath:nil
                                                                                                           cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void) addDataToBottomOfTableView {
    
    if (self.loadingPrevData) {
        
        NSArray *rows = [self.tableView indexPathsForVisibleRows];
        NSIndexPath *indexPath = [rows lastObject];
        
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSString *message_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];
        int groupid = self.currentGroup.groupId; // group id
        int messageid = [message_id intValue];
        
        __weak typeof (self) weakSelf = self;
        [self.rm requestSocialStreamPrevMessageWithGroupID:groupid messageID:messageid doneBlock:^(BOOL status) {
            weakSelf.loadingPrevData = NO;
        }];
        
    }
}

#pragma mark - UIScrollViewDelegate

typedef NS_ENUM (NSInteger, ScrollDirection) {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy
};

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    ScrollDirection scrollDirection;
    if (self.lastContentOffset > scrollView.contentOffset.y) {
        scrollDirection = ScrollDirectionUp;
    }
    
    if (self.lastContentOffset < scrollView.contentOffset.y) {
        scrollDirection = ScrollDirectionDown;
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 10.0) {
        self.loadingPrevData = YES;
        [self addDataToBottomOfTableView];
    }
    
    switch (scrollDirection) {
        case ScrollDirectionUp:
            NSLog(@"scrolling up...");
            [self enableMessageButton:YES];
            break;
            
        case ScrollDirectionDown:
            NSLog(@"scrolling down...");
            [self enableMessageButton:NO];
            break;
            
        default:
            break;
    }
}

- (void)enableMessageButton:(BOOL)status {
    
    [UIView animateWithDuration:1.0f
                          delay:0.5f
         usingSpringWithDamping:0.5f
          initialSpringVelocity:1.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         if (status) {
                             self.messageButton.alpha = 1.0f;
                             self.messageButton.hidden = NO;
                         } else {
                             self.messageButton.alpha = 0.0f;
                             self.messageButton.hidden = YES;
                         }
                         
                     } completion:^(BOOL finished) {
                         //do nothing
                     }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showMessageBox"]) {
        SocialStreamMessage *sm = (SocialStreamMessage *)segue.destinationViewController;
        sm.mdictionary = nil;//default value
        
        if (![sender isKindOfClass:[UIButton class]]) {
            NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
            
            NSString *user_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ];
            NSString *group_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"group_id"] ];
            NSString *emoticon_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"emoticon_id"] ];
            NSString *message = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message"] ];
            NSString *is_attached = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_attached"] ];
            NSString *is_deleted = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_deleted"] ];
            NSString *message_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];
            
            sm.mdictionary = [@{@"user_id":user_id,
                                @"group_id":group_id,
                                @"emoticon_id":emoticon_id,
                                @"message":message,
                                @"is_attached":is_attached,
                                @"is_deleted":is_deleted,
                                @"message_id":message_id
                                } mutableCopy];
        }
        
        NSString *groupid = [NSString stringWithFormat:@"%d", self.currentGroup.groupId];
        sm.groupid = (NSUInteger)[groupid integerValue];
    }
    
    if ([segue.identifier isEqualToString:@"showPeopleWhoLikeBox"]) {
        NSLog(@"show people who like box");
        SocialStreamPeopleWhoLike *sspwl = (SocialStreamPeopleWhoLike *)segue.destinationViewController;
        NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
        
        NSString *groupid = [NSString stringWithFormat:@"%d", self.currentGroup.groupId];
        sspwl.groupid = (NSUInteger)[groupid integerValue];
        sspwl.messageid = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];
    }
    
    if ([segue.identifier isEqualToString:@"showCommentBox"]) {
        
        SocialStreamComment *sc = (SocialStreamComment *)segue.destinationViewController;
        NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
        
        //GROUP ID
        NSString *groupid = [NSString stringWithFormat:@"%d", self.currentGroup.groupId];
        sc.groupid = (NSUInteger)[groupid integerValue];
        sc.messageid = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];
        sc.hideCommentBox = self.hideCommentBox;
    }
    
}

@end