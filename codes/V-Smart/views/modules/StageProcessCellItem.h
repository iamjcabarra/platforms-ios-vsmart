//
//  StageProcessCellItem.h
//  V-Smart
//
//  Created by Julius Abarra on 08/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StageProcessCellItem : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *processNameLabel;
@property (strong, nonatomic) IBOutlet UITextView *processContentTextView;
@property (strong, nonatomic) IBOutlet UIButton *uploadFileButton;
@property (strong, nonatomic) IBOutlet UIButton *clearFileButton;

- (void)shouldHideProcessContentTextView:(BOOL)hide;
- (void)shouldHideUploadFileButton:(BOOL)hide;
- (void)shouldHideClearFileButton:(BOOL)hide;

@end
