//
//  StudentViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 09/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentViewController : UIViewController

@property (strong, nonatomic) NSDictionary *courseDictionary;

@end
