//
//  PlayListOptionController.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/19/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "PlayListOptionController.h"
#import "AppDelegate.h"

@interface PlayListOptionController ()
@property (strong, nonatomic) ResourceManager *rm;
@end

@implementation PlayListOptionController

static NSString *kOptionCellIdentifier = @"option_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.rm = [AppDelegate resourceInstance];
    self.tableView.estimatedRowHeight = 0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

//- (void)viewWillLayoutSubviews{
//    [super viewWillLayoutSubviews];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.optionList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kOptionCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    // Configure the cell...
    NSDictionary *data = (NSDictionary *)self.optionList[indexPath.row];
    NSString *image = [NSString stringWithFormat:@"%@", data[@"image"] ];
    NSString *title = [NSString stringWithFormat:@"%@", data[@"title"] ];
    
//    cell.imageView.image = [UIImage imageNamed:image];
//    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.textLabel.text = title;
    });
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *menu = self.optionList[indexPath.row];
    NSString *option = [NSString stringWithFormat:@"%@", menu[@"type"] ];
    
    if([_delegate respondsToSelector:@selector(didFinishSelectingOption:withData:)]){
        [_delegate didFinishSelectingOption:option withData:self.data];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

@end
