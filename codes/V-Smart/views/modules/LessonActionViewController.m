//
//  LessonActionViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "LessonActionViewController.h"
#import "LessonActionTabBarViewController.h"
#import "LessonViewController.h"
#import "LessonPlanDataManager.h"
#import "UBDContentManager.h"

@interface LessonActionViewController () <UIAlertViewDelegate>

@property (strong, nonatomic) LessonPlanDataManager *lm;
@property (strong, nonatomic) UBDContentManager *sharedUBDContentManager;
@property (strong, nonatomic) LessonActionTabBarViewController *containerViewController;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong, nonatomic) IBOutlet UIButton *butOverview;
@property (strong, nonatomic) IBOutlet UIButton *butStage1;
@property (strong, nonatomic) IBOutlet UIButton *butStage2;
@property (strong, nonatomic) IBOutlet UIButton *butStage3;
@property (strong, nonatomic) IBOutlet UIButton *butPrevious;
@property (strong, nonatomic) IBOutlet UIButton *butNext;
@property (strong, nonatomic) IBOutlet UIButton *butSave;

@property (strong, nonatomic) NSString *userid;
@property (assign, nonatomic) BOOL actionStatus;

@property (strong, nonatomic) NSString *localizedNext;
@property (strong, nonatomic) NSString *localizedPrevious;
@property (strong, nonatomic) NSString *localizedFinish;
@property (strong, nonatomic) NSString *localizedSave;
@property (strong, nonatomic) NSString *localizedCancel;    // jca-01-11-2016: Added "Cancel" action (localization)


- (IBAction)butOverviewTabAction:(id)sender;
- (IBAction)butStage1TabAction:(id)sender;
- (IBAction)butStage2TabAction:(id)sender;
- (IBAction)butStage3TabAction:(id)sender;

@end

UIColor *bgColor;
UIColor *bnColor;

@implementation LessonActionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Use singleton class shared instance
    self.lm = [LessonPlanDataManager sharedInstance];
    self.managedObjectContext = self.lm.mainContext;
    self.sharedUBDContentManager = [UBDContentManager sharedInstance];
    
    // Clear existing data from content manager
    [self.sharedUBDContentManager clearEntries];
    
    // Store template id and action type to shared instance properties
    self.sharedUBDContentManager.courseid = self.courseid;
    self.sharedUBDContentManager.gradeLevel = self.gradeLevel;
    self.sharedUBDContentManager.templateid = self.templateid;
    self.sharedUBDContentManager.actionType = self.actionType;
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.userid = [NSString stringWithFormat:@"%d", account.user.id];
    self.actionStatus = NO;
    
    // Initialize generic background color for some ui objects
    bgColor = [UIColor colorWithRed:(38/255.f) green:(171/255.f) blue:(226/255.f) alpha:1.0f];
    bnColor = [UIColor colorWithRed:13.0f/255.0f green:201.0f/255.0f blue:27.0f/255.0f alpha:1.0f];
    
    // Hide previous button and identify the first view to show
    self.butPrevious.hidden = YES;
    self.sharedUBDContentManager.view = @"0";
    
    // Initialize progress view
    self.progressView.progress = 0.25f;
    
    // Localized button strings
    self.localizedPrevious = NSLocalizedString(@"Previous", nil);
    self.localizedNext = NSLocalizedString(@"Next", nil);
    self.localizedFinish = NSLocalizedString(@"Finish", nil);
    self.localizedSave = NSLocalizedString(@"Update", nil);         // jca-01-11-2016: Changed "Save" to "Update" for editing
    self.localizedCancel = NSLocalizedString(@"Cancel", nil);       // jca-01-11-2016: Added "Cancel" action (localization)
    
    [self.butPrevious setTitle:self.localizedPrevious forState:UIControlStateNormal];
    [self.butSave setTitle:self.localizedSave forState:UIControlStateNormal];
    
    self.butNext.backgroundColor = bgColor;
    self.butSave.backgroundColor = bnColor;
    
    // Add an action to buttons
    [self.butNext addTarget:self action:@selector(showNextView:) forControlEvents:UIControlEventTouchUpInside];
    [self.butPrevious addTarget:self action:@selector(showPreviousView:) forControlEvents:UIControlEventTouchUpInside];
    [self.butSave addTarget:self action:@selector(saveLessonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Invoke private methods
    [self setupActivityIndicator];
    [self setupTabBarView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Set navigation bar title
    if ([self.actionType isEqualToString:@"1"]) {
        self.title = NSLocalizedString(@"Add Lesson", nil);
        self.butSave.hidden = YES;
    }
    
    if ([self.actionType isEqualToString:@"2"]) {
        self.title = NSLocalizedString(@"Edit Lesson", nil);
        self.butSave.hidden = NO;
        
        if ([self.templateid isEqualToString:@"1"]) {
            [self renderCurrentDataOfSelectedLessonPlan];
        }
    }
    
    // Decorate navigation bar
    UIColor *navColor = [UIColor colorWithRed:13.0f/255.0f green:201.0f/255.0f blue:27.0f/255.0f alpha:1.0f];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    self.navigationController.navigationBar.barTintColor = navColor;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupActivityIndicator {
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.frame = CGRectMake(0.0f, 0.0f, 40.0f, 40.0f);
    self.activityIndicator.center = CGPointMake(self.view.bounds.size.width / 2.0f, self.view.bounds.size.height / 2.0f);
    self.activityIndicator.autoresizingMask = (UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin);
    self.activityIndicator.backgroundColor = [UIColor colorWithRed:217.0f/255.0f green:217.0f/255.0f blue:217.0f/255.0f alpha:1.0f];
    self.activityIndicator.layer.cornerRadius = 5.0f;
    [self.view addSubview:self.activityIndicator];
    [self.activityIndicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
}

- (void)setupTabBarView {
    [self.butOverview setBackgroundColor:[UIColor whiteColor]];
    [self.butOverview setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.butStage1 setBackgroundColor:bgColor];
    [self.butStage1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.butStage2 setBackgroundColor:bgColor];
    [self.butStage2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.butStage3 setBackgroundColor:bgColor];
    [self.butStage3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)butOverviewTabAction:(id)sender {
    [self.containerViewController swapViewControllers:@"embedOverviewTab"];
    
    //if ([self.actionType isEqualToString:@"2"]) {
    //    self.butSave.hidden = NO;
    //}
    
    // ENHANCEMENT#871
    // jca-01-11-2016
    // Add "Cancel" action (hiding save [finish] button when adding lesson)
    if ([self.actionType isEqualToString:@"2"]) {
        self.butSave.hidden = NO;
    }
    else {
        self.butSave.hidden = YES;
    }
    
    self.progressView.progress = 0.25f;
    self.butPrevious.hidden = YES;
    self.sharedUBDContentManager.view = @"0";
    
    [self.butNext setTitle:self.localizedNext forState:UIControlStateNormal];
    self.butNext.backgroundColor = bgColor;
    
    [self.butOverview setBackgroundColor:[UIColor whiteColor]];
    [self.butOverview setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.butStage1 setBackgroundColor:bgColor];
    [self.butStage1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.butStage2 setBackgroundColor:bgColor];
    [self.butStage2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.butStage3 setBackgroundColor:bgColor];
    [self.butStage3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)butStage1TabAction:(id)sender {
    [self.containerViewController swapViewControllers:@"embedStage1Tab"];
    
    //if ([self.actionType isEqualToString:@"2"]) {
    //    self.butSave.hidden = NO;
    //}
    
    // ENHANCEMENT#871
    // jca-01-11-2016
    // Add "Cancel" action (hiding save [finish] button when adding lesson)
    if ([self.actionType isEqualToString:@"2"]) {
        self.butSave.hidden = NO;
    }
    else {
        self.butSave.hidden = YES;
    }
    
    self.progressView.progress = 0.50f;
    self.butPrevious.hidden = NO;
    self.sharedUBDContentManager.view = @"1";
    
    [self.butNext setTitle:self.localizedNext forState:UIControlStateNormal];
    self.butNext.backgroundColor = bgColor;
    
    [self.butOverview setBackgroundColor:bgColor];
    [self.butOverview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.butStage1 setBackgroundColor:[UIColor whiteColor]];
    [self.butStage1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.butStage2 setBackgroundColor:bgColor];
    [self.butStage2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.butStage3 setBackgroundColor:bgColor];
    [self.butStage3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)butStage2TabAction:(id)sender {
    [self.containerViewController swapViewControllers:@"embedStage2Tab"];
    
    //if ([self.actionType isEqualToString:@"2"]) {
    //    self.butSave.hidden = NO;
    //}
    
    // ENHANCEMENT#871
    // jca-01-11-2016
    // Add "Cancel" action (hiding save [finish] button when adding lesson)
    if ([self.actionType isEqualToString:@"2"]) {
        self.butSave.hidden = NO;
    }
    else {
        self.butSave.hidden = YES;
    }
    
    self.progressView.progress = 0.75f;
    self.butPrevious.hidden = NO;
    self.sharedUBDContentManager.view = @"2";
    
    [self.butNext setTitle:self.localizedNext forState:UIControlStateNormal];
    self.butNext.backgroundColor = bgColor;
    
    [self.butOverview setBackgroundColor:bgColor];
    [self.butOverview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.butStage1 setBackgroundColor:bgColor];
    [self.butStage1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.butStage2 setBackgroundColor:[UIColor whiteColor]];
    [self.butStage2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.butStage3 setBackgroundColor:bgColor];
    [self.butStage3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)butStage3TabAction:(id)sender {
    [self.containerViewController swapViewControllers:@"embedStage3Tab"];
    
    //if ([self.actionType isEqualToString:@"2"]) {
    //    self.butSave.hidden = YES;
    //    [self.butNext setTitle:self.localizedSave forState:UIControlStateNormal];
    //}
    //else {
    //    [self.butNext setTitle:self.localizedFinish forState:UIControlStateNormal];
    //}
    
    // ENHANCEMENT#871
    // jca-01-11-2016
    // Add "Cancel" action (reusing of butSave and butNext)
    if ([self.actionType isEqualToString:@"2"]) {
        self.butSave.hidden = NO;
        [self.butNext setTitle:self.localizedCancel forState:UIControlStateNormal];
    }
    else {
        self.butSave.hidden = NO;
        [self.butSave setTitle:self.localizedFinish forState:UIControlStateNormal];
        [self.butNext setTitle:self.localizedCancel forState:UIControlStateNormal];
    }
    
    self.progressView.progress = 1.0f;
    self.butPrevious.hidden = NO;
    self.sharedUBDContentManager.view = @"3";

    self.butNext.backgroundColor = bgColor;
    [self.butOverview setBackgroundColor:bgColor];
    [self.butOverview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.butStage1 setBackgroundColor:bgColor];
    [self.butStage1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.butStage2 setBackgroundColor:bgColor];
    [self.butStage2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.butStage3 setBackgroundColor:[UIColor whiteColor]];
    [self.butStage3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

- (void)showNextView:(id)sender {
    NSInteger currentView = [self.sharedUBDContentManager.view integerValue];
    
    switch (currentView) {
        case 0:
            [self.containerViewController swapViewControllers:@"embedStage1Tab"];
            
            if ([self.actionType isEqualToString:@"2"]) {
                self.butSave.hidden = NO;
            }
            
            self.progressView.progress = 0.50f;
            self.butPrevious.hidden = NO;
            self.sharedUBDContentManager.view = @"1";
            
            [self.butNext setTitle:self.localizedNext forState:UIControlStateNormal];
            self.butNext.backgroundColor = bgColor;
            
            [self.butOverview setBackgroundColor:bgColor];
            [self.butOverview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.butStage1 setBackgroundColor:[UIColor whiteColor]];
            [self.butStage1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self.butStage2 setBackgroundColor:bgColor];
            [self.butStage2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.butStage3 setBackgroundColor:bgColor];
            [self.butStage3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
            
        case 1:
            [self.containerViewController swapViewControllers:@"embedStage2Tab"];
            
            if ([self.actionType isEqualToString:@"2"]) {
                self.butSave.hidden = NO;
            }
            
            self.progressView.progress = 0.75f;
            self.butPrevious.hidden = NO;
            self.sharedUBDContentManager.view = @"2";
            
            [self.butNext setTitle:self.localizedNext forState:UIControlStateNormal];
            self.butNext.backgroundColor = bgColor;
            
            [self.butOverview setBackgroundColor:bgColor];
            [self.butOverview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.butStage1 setBackgroundColor:bgColor];
            [self.butStage1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.butStage2 setBackgroundColor:[UIColor whiteColor]];
            [self.butStage2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self.butStage3 setBackgroundColor:bgColor];
            [self.butStage3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
            
        case 2:
            [self.containerViewController swapViewControllers:@"embedStage3Tab"];
            
            //if ([self.actionType isEqualToString:@"2"]) {
            //    self.butSave.hidden = YES;
            //    [self.butNext setTitle:self.localizedSave forState:UIControlStateNormal];
            //}
            //else {
            //    [self.butNext setTitle:self.localizedFinish forState:UIControlStateNormal];
            //}
            
            // ENHANCEMENT#871
            // jca-01-11-2016
            // Add "Cancel" action (reusing of butSave and butNext)
            if ([self.actionType isEqualToString:@"2"]) {
                self.butSave.hidden = NO;
                [self.butNext setTitle:self.localizedCancel forState:UIControlStateNormal];
            }
            else {
                self.butSave.hidden = NO;
                [self.butSave setTitle:self.localizedFinish forState:UIControlStateNormal];
                [self.butNext setTitle:self.localizedCancel forState:UIControlStateNormal];
            }
            
            self.progressView.progress = 1.0f;
            self.butPrevious.hidden = NO;
            self.sharedUBDContentManager.view = @"3";
        
            self.butNext.backgroundColor = bgColor;
            [self.butOverview setBackgroundColor:bgColor];
            [self.butOverview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.butStage1 setBackgroundColor:bgColor];
            [self.butStage1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.butStage2 setBackgroundColor:bgColor];
            [self.butStage2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.butStage3 setBackgroundColor:[UIColor whiteColor]];
            [self.butStage3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            break;
        default:
            //[self saveLessonPlan];
            
            // ENHANCEMENT#871
            // jca-01-11-2016
            // Add "Cancel" action (popping to lesson view)
            if ([self.actionType isEqualToString:@"1"]) {
                NSArray *viewControllers = self.navigationController.viewControllers;
                
                for (UIViewController *anVC in viewControllers) {
                    if ([anVC isKindOfClass:[LessonViewController class]]) {
                        [self.navigationController popToViewController:anVC animated:YES];
                        break;
                    }
                }
            }
            else {
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            [self.sharedUBDContentManager clearEntries];
            
            break;
    }
}

- (void)showPreviousView:(id)sender {
    NSInteger currentView = [self.sharedUBDContentManager.view integerValue];
    
    switch (currentView) {
        case 1:
            [self.containerViewController swapViewControllers:@"embedOverviewTab"];
            
            if ([self.actionType isEqualToString:@"2"]) {
                self.butSave.hidden = NO;
            }
            
            self.progressView.progress = 0.25f;
            self.butPrevious.hidden = YES;
            self.sharedUBDContentManager.view = @"0";
            
            [self.butNext setTitle:self.localizedNext forState:UIControlStateNormal];
            self.butNext.backgroundColor = bgColor;
            
            [self.butOverview setBackgroundColor:[UIColor whiteColor]];
            [self.butOverview setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self.butStage1 setBackgroundColor:bgColor];
            [self.butStage1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.butStage2 setBackgroundColor:bgColor];
            [self.butStage2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.butStage3 setBackgroundColor:bgColor];
            [self.butStage3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
            
        case 2:
            [self.containerViewController swapViewControllers:@"embedStage1Tab"];
            
            if ([self.actionType isEqualToString:@"2"]) {
                self.butSave.hidden = NO;
            }
            
            self.progressView.progress = 0.50f;
            self.butPrevious.hidden = NO;
            self.sharedUBDContentManager.view = @"1";
            
            [self.butNext setTitle:self.localizedNext forState:UIControlStateNormal];
            self.butNext.backgroundColor = bgColor;
            
            [self.butOverview setBackgroundColor:bgColor];
            [self.butOverview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.butStage1 setBackgroundColor:[UIColor whiteColor]];
            [self.butStage1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self.butStage2 setBackgroundColor:bgColor];
            [self.butStage2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.butStage3 setBackgroundColor:bgColor];
            [self.butStage3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
            
        case 3:
            [self.containerViewController swapViewControllers:@"embedStage2Tab"];
            
            if ([self.actionType isEqualToString:@"2"]) {
                self.butSave.hidden = NO;
            }
            
            self.progressView.progress = 0.75f;
            self.butPrevious.hidden = NO;
            self.sharedUBDContentManager.view = @"2";
            
            [self.butNext setTitle:self.localizedNext forState:UIControlStateNormal];
            self.butNext.backgroundColor = bgColor;
            
            [self.butOverview setBackgroundColor:bgColor];
            [self.butOverview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.butStage1 setBackgroundColor:bgColor];
            [self.butStage1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.butStage2 setBackgroundColor:[UIColor whiteColor]];
            [self.butStage2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self.butStage3 setBackgroundColor:bgColor];
            [self.butStage3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
            
        default:
            break;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedTabViewContainer"]) {
        self.containerViewController = [segue destinationViewController];
    }
}

- (void)renderCurrentDataOfSelectedLessonPlan {
    // Access lesson plan id from selected managed object
    NSString *lpid = [NSString stringWithFormat:@"%@", [self.lesson_mo valueForKey:@"id"]];
    self.sharedUBDContentManager.lpid = lpid;
    
    // Access data from selected managed object
    NSString *title = [NSString stringWithFormat:@"%@", [self.lesson_mo valueForKey:@"name"]];
    NSString *schoolName = [NSString stringWithFormat:@"%@", [self.lesson_mo valueForKey:@"school_name"]];
    NSString *schoolAddress = [NSString stringWithFormat:@"%@", [self.lesson_mo valueForKey:@"school_address"]];
    NSString *unit = [NSString stringWithFormat:@"%@", [self.lesson_mo valueForKey:@"unit"]];
    NSString *quarter = [NSString stringWithFormat:@"%@", [self.lesson_mo valueForKey:@"quarter"]];
    NSString *start = [NSString stringWithFormat:@"%@", [self.lesson_mo valueForKey:@"start_date"]];
    NSString *end = [NSString stringWithFormat:@"%@", [self.lesson_mo valueForKey:@"end_date"]];
    
    // OVERVIEW
    [self.sharedUBDContentManager.contentOverview setObject:title forKey:@"title"];
    [self.sharedUBDContentManager.contentOverview setObject:schoolName forKey:@"schoolName"];
    [self.sharedUBDContentManager.contentOverview setObject:schoolAddress forKey:@"schoolAddress"];
    [self.sharedUBDContentManager.contentOverview setObject:unit forKey:@"unit"];
    [self.sharedUBDContentManager.contentOverview setObject:quarter forKey:@"quarter"];
    [self.sharedUBDContentManager.contentOverview setObject:start forKey:@"start"];
    [self.sharedUBDContentManager.contentOverview setObject:end forKey:@"end"];
    
    // Access managed objects for process entity
    NSPredicate *stage1Predicate = [NSPredicate predicateWithFormat:@"lp_id == %@ AND stage_id == 1", lpid];
    NSPredicate *stage2Predicate = [NSPredicate predicateWithFormat:@"lp_id == %@ AND stage_id == 2", lpid];
    NSPredicate *stage3Predicate = [NSPredicate predicateWithFormat:@"lp_id == %@ AND stage_id == 3", lpid];
    
    // Collection objects for lesson content processes
    NSArray *stage1Processes = [self.lm getObjectsForEntity:kProcessEntity withPredicate:stage1Predicate andSortDescriptor:@"lc_id"];
    NSArray *stage2Processes = [self.lm getObjectsForEntity:kProcessEntity withPredicate:stage2Predicate andSortDescriptor:@"lc_id"];
    NSArray *stage3Processes = [self.lm getObjectsForEntity:kProcessEntity withPredicate:stage3Predicate andSortDescriptor:@"lc_id"];
    
    // Collection objects for lesson plan content stages
    NSMutableArray *stage1Contents = [NSMutableArray array];
    NSMutableArray *stage2Contents = [NSMutableArray array];
    NSMutableArray *stage3Contents = [NSMutableArray array];
    
    if (stage1Processes.count > 0) {
        for (int i = 0; i < stage1Processes.count; i++) {
            NSManagedObject *mo = (NSManagedObject *)[stage1Processes objectAtIndex:i];
            NSString *is_file = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_file"]];
            NSString *filename = [NSString stringWithFormat:@"%@", [mo valueForKey:@"orig_filename"]];
            NSString *content = [NSString stringWithFormat:@"%@", [mo valueForKey:@"p_content"]];
            
            if ([is_file isEqualToString:@"1"]) {
                [stage1Contents addObject:filename];
            }
            else {
                [stage1Contents addObject:content];
            }
        }
    }
    
    if (stage2Processes.count > 0) {
        for (int i = 0; i < stage2Processes.count; i++) {
            NSManagedObject *mo = (NSManagedObject *)[stage2Processes objectAtIndex:i];
            NSString *is_file = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_file"]];
            NSString *filename = [NSString stringWithFormat:@"%@", [mo valueForKey:@"orig_filename"]];
            NSString *content = [NSString stringWithFormat:@"%@", [mo valueForKey:@"p_content"]];
            NSString *lcid = [NSString stringWithFormat:@"%@", [mo valueForKey:@"lc_id"]];
            
            if ([is_file isEqualToString:@"1"]) {
                if ([filename isEqualToString:@"<null>"]) {
                    filename = @"";
                }
                
                [stage2Contents addObject:filename];
                [self.sharedUBDContentManager.contentWithFileList addObject:lcid];
            }
            else {
                [stage2Contents addObject:content];
            }
        }
    }
    
    if (stage3Processes.count > 0) {
        for (int i = 0; i < stage3Processes.count; i++) {
            NSManagedObject *mo = (NSManagedObject *)[stage3Processes objectAtIndex:i];
            NSString *is_file = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_file"]];
            NSString *filename = [NSString stringWithFormat:@"%@", [mo valueForKey:@"orig_filename"]];
            NSString *content = [NSString stringWithFormat:@"%@", [mo valueForKey:@"p_content"]];
            
            if ([is_file isEqualToString:@"1"]) {
                [stage3Contents addObject:filename];
            }
            else {
                [stage3Contents addObject:content];
            }
        }
    }
    
    // STAGE 1
    if (stage1Processes.count >= 7) {
        [self.sharedUBDContentManager.contentStage setObject:[stage1Contents objectAtIndex:0] forKey:@"1"];
        [self.sharedUBDContentManager.contentStage setObject:[stage1Contents objectAtIndex:1] forKey:@"2"];
        [self.sharedUBDContentManager.contentStage setObject:[stage1Contents objectAtIndex:2] forKey:@"3"];
        [self.sharedUBDContentManager.contentStage setObject:[stage1Contents objectAtIndex:3] forKey:@"4"];
        [self.sharedUBDContentManager.contentStage setObject:[stage1Contents objectAtIndex:4] forKey:@"5"];
        [self.sharedUBDContentManager.contentStage setObject:[stage1Contents objectAtIndex:5] forKey:@"6"];
        [self.sharedUBDContentManager.contentStage setObject:[stage1Contents objectAtIndex:6] forKey:@"7"];
    }
    
    // STAGE 2
    if (stage2Processes.count >= 8) {
        [self.sharedUBDContentManager.contentStage setObject:[stage2Contents objectAtIndex:0] forKey:@"8"];
        [self.sharedUBDContentManager.contentStage setObject:[stage2Contents objectAtIndex:1] forKey:@"9"];
        [self.sharedUBDContentManager.contentStage setObject:[stage2Contents objectAtIndex:2] forKey:@"10"];
        [self.sharedUBDContentManager.contentStage setObject:[stage2Contents objectAtIndex:3] forKey:@"11"];
        [self.sharedUBDContentManager.contentStage setObject:[stage2Contents objectAtIndex:4] forKey:@"12"];
        [self.sharedUBDContentManager.contentStage setObject:[stage2Contents objectAtIndex:5] forKey:@"13"];
        [self.sharedUBDContentManager.contentStage setObject:[stage2Contents objectAtIndex:6] forKey:@"14"];
        [self.sharedUBDContentManager.contentStage setObject:[stage2Contents objectAtIndex:7] forKey:@"15"];
    }

    // STAGE 3
    if (stage3Processes.count >= 5) {
        [self.sharedUBDContentManager.contentStage setObject:[stage3Contents objectAtIndex:0] forKey:@"16"];
        [self.sharedUBDContentManager.contentStage setObject:[stage3Contents objectAtIndex:1] forKey:@"17"];
        [self.sharedUBDContentManager.contentStage setObject:[stage3Contents objectAtIndex:2] forKey:@"21"];
        [self.sharedUBDContentManager.contentStage setObject:[stage3Contents objectAtIndex:3] forKey:@"22"];
        [self.sharedUBDContentManager.contentStage setObject:[stage3Contents objectAtIndex:4] forKey:@"23"];
    }
}

#pragma mark - Saving and Posting

- (void)saveLessonPlan {
    BOOL okay = [self isOkayToSave];
    
    if (okay) {
        [self.activityIndicator startAnimating];
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        // Get data from the content manager
        NSString *userid = self.userid;
        NSString *lpid = self.sharedUBDContentManager.lpid;
        NSString *courseid = self.sharedUBDContentManager.courseid;
        NSString *level = self.sharedUBDContentManager.gradeLevel;
        NSString *name = [self.sharedUBDContentManager.contentOverview objectForKey:@"title"];
        NSString *schoolname = [self.sharedUBDContentManager.contentOverview objectForKey:@"schoolName"];
        NSString *schooladdress = [self.sharedUBDContentManager.contentOverview objectForKey:@"schoolAddress"];
        NSString *unit = [self.sharedUBDContentManager.contentOverview objectForKey:@"unit"];
        NSString *quarter = [self.sharedUBDContentManager.contentOverview objectForKey:@"quarter"];
        NSString *start = [self.sharedUBDContentManager.contentOverview objectForKey:@"start"];
        NSString *end = [self.sharedUBDContentManager.contentOverview objectForKey:@"end"];
        
        // Get template process objects
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"content.template.template_id == %@", self.sharedUBDContentManager.templateid];
        NSArray *result = [self.lm getObjectsForEntity:kTemplateProcessEntity withPredicate:predicate andSortDescriptor:@"lps_id"];
        
        // Collection object for lesson's content
        NSMutableArray *content = [NSMutableArray array];
        
        if (result.count > 0) {
            int i;
            
            NSMutableArray *lpsidlist = [NSMutableArray array];
            // TO-DO: Make it mutable to save more than one file object for contents with files
            NSDictionary *fileObject = [NSDictionary dictionary];
            
            // List learning process stage id
            for (i = 0; i < result.count; i++) {
                NSManagedObject *object = (NSManagedObject *)[result objectAtIndex:i];
                NSString *lpsid = [object valueForKey:@"lps_id"];
                [lpsidlist addObject:lpsid];
            }
            
            for (i = 0; i < lpsidlist.count; i++) {
                NSString *lpsid = [NSString stringWithFormat:@"%@", lpsidlist[i]];
                
                // Inserting nil object to dictionary causes a crash
                if (![self.sharedUBDContentManager.contentStage objectForKey:lpsid]) {
                    [self.sharedUBDContentManager.contentStage setObject:@"" forKey:lpsid];
                }
                
                NSDictionary *d = @{@"lps_id":lpsid, @"content":[self.sharedUBDContentManager.contentStage objectForKey:lpsid]};
                [content addObject:d];
               
                // Content is a file [TO-DO: Use keyword is_file to check if it is a file or not instead of using lpsid]
                if ([lpsid isEqualToString:@"15"]) {
                    // TO-DO: Create a dictionary and save it to mutable array
                    fileObject = nil;
                    
                    if ([self.sharedUBDContentManager.contentStageFile objectForKey:@"15"] != nil) {
                        fileObject = [self.sharedUBDContentManager.contentStageFile objectForKey:@"15"];
                    }
                }
            }
            
            NSLog(@"---CONTENT: %@", content);
            
            // CREATE LESSON PLAN
            if ([self.actionType isEqualToString:@"1"]) {
                // Lesson plan content of overview
                NSDictionary *postContent = @{@"user_id":userid,
                                              @"name":name,
                                              @"course_id":courseid,
                                              @"level":level,
                                              @"unit":unit,
                                              @"quarter":quarter,
                                              @"start_date":start,
                                              @"end_date":end,
                                              @"school_name":schoolname,
                                              @"school_address":schooladdress,
                                              @"content":content
                                              };
                
                // Convert post content to json object
                NSError *err;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postContent options:NSJSONWritingPrettyPrinted error:&err];
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                
                // Lesson plan content of stage processes
                NSDictionary *postData = @{@"content": jsonString,
                                           @"file_learning_process_stage_id":@"15",
                                           @"template_id":self.sharedUBDContentManager.templateid
                                           };
            
                __weak typeof(self) wo = self;
                [self.lm requestCreateNewLessonPlan:postData file:fileObject doneBlock:^(BOOL status) {
                    if (status) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [wo.activityIndicator stopAnimating];
                            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                            [wo.sharedUBDContentManager clearEntries];
                            [wo showMessageWithAction:@"1" andIsOkayToSave:YES];
                        });
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [wo.lm showErrorMessageDialog];
                            [wo.activityIndicator stopAnimating];
                            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                        });
                    }
                }];
            }//end create lesson plan
            
            // UPDATE LESSON PLAN
            if ([self.actionType isEqualToString:@"2"]) {
                // Lesson plan content of overview
                NSDictionary *postContent = @{@"name":name,
                                              @"course_id":courseid,
                                              @"level":level,
                                              @"unit":unit,
                                              @"quarter":quarter,
                                              @"start_date":start,
                                              @"end_date":end,
                                              @"school_name":schoolname,
                                              @"school_address":schooladdress,
                                              @"content":content
                                              };
                
                // Convert post content to json object
                NSError *err;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postContent options:NSJSONWritingPrettyPrinted error:&err];
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                
                // Lesson plan contents of stage processes
                NSDictionary *postData = @{@"content": jsonString};
                
                // Update uploaded file
                BOOL sameFile = NO;
                BOOL isDeleted = [[self.sharedUBDContentManager.contentStage objectForKey:@"15"] isEqualToString:@""];
                
                if (!isDeleted) {
                    if ([self.sharedUBDContentManager.contentStageFile objectForKey:@"15"] != nil) {
                        fileObject = [self.sharedUBDContentManager.contentStageFile objectForKey:@"15"];
                    }
                    else {
                        sameFile = YES;
                    }
                }
                else {
                    fileObject = nil;
                }
                
                // Just consider rubrics for the mean time
                NSString *rubricsLCID = @"";
                
                if (self.sharedUBDContentManager.contentWithFileList.count > 0) {
                    rubricsLCID = self.sharedUBDContentManager.contentWithFileList[0];
                }
        
                __weak typeof(self) wo = self;
                [self.lm requestUpdateLessonPlanWithLessonPlanID:lpid meta:postData doneBlock:^(BOOL status) {
                    if (status) {
                        
                        // Post updated file to server
                        if (![rubricsLCID isEqualToString:@""] && !sameFile) {
                            [self.lm requestUpdateLessonPlanFileWithLearningContentID:rubricsLCID file:fileObject doneBlock:^(BOOL status) {
                                // Do nothing
                            }];
                        }
                        
                        // To update lesson plan details
                        [self.lm requestLessonPlanDetailsForLessonPlanWithID:lpid doneBlock:^(BOOL status) {
                            // Do nothing
                        }];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [wo.activityIndicator stopAnimating];
                            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                            [wo.sharedUBDContentManager clearEntries];
                            [wo showMessageWithAction:@"2" andIsOkayToSave:YES];
                        });
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [wo.lm showErrorMessageDialog];
                            [wo.activityIndicator stopAnimating];
                            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                        });
                    }
                }];
            }//end update lesson plan
        }//end has result
    }//end okay
    else {
        if ([self.actionType isEqualToString:@"1"]) {
            [self showMessageWithAction:@"1" andIsOkayToSave:NO];
        }
        else {
            [self showMessageWithAction:@"2" andIsOkayToSave:NO];
        }
    }//end not okay
}

- (BOOL)isOkayToSave {
    NSArray *keys = [self.sharedUBDContentManager.contentOverview allKeys];
    
    // Iterate through content overview and check for empty data
    for (NSString *k in keys) {
        if ([[self.sharedUBDContentManager.contentOverview objectForKey:k] isEqualToString:@""]) {
            return  NO;
        }
    }
    
    return YES;
}

- (void)showMessageWithAction:(NSString *)action andIsOkayToSave:(BOOL)status {
    //NSString *avTitle = NSLocalizedString(@"Saved", nil);
    NSString *butOkay = NSLocalizedString(@"Okay", nil);
    NSString *message = @"";
    
    self.actionStatus = status;
 
    if (status) {
        if ([action isEqualToString:@"1"]) {
            message = NSLocalizedString(@"Lesson plan successfully created.", nil);
        }
        else {
            message = NSLocalizedString(@"Lesson plan successfully updated.", nil);
        }
    }
    else {
        if ([action isEqualToString:@"1"]) {
            message = NSLocalizedString(@"Lesson plan is failed to create new lesson plan.", nil);
        }
        else {
            message = NSLocalizedString(@"Lesson plan is failed to update lesson plan.", nil);
        }
    }
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil                // jca-01-11-2016: Set to nil
                                                 message:message
                                                delegate:self
                                       cancelButtonTitle:butOkay
                                       otherButtonTitles:nil];
    [av show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        if (self.actionStatus) {
            if ([self.actionType isEqualToString:@"1"]) {
                NSArray *viewControllers = self.navigationController.viewControllers;
                
                for (UIViewController *anVC in viewControllers) {
                    if ([anVC isKindOfClass:[LessonViewController class]]) {
                        [self.navigationController popToViewController:anVC animated:YES];
                        break;
                    }
                }
            }
            else {
                 [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}

- (void)saveLessonAction:(id)sender {
    [self saveLessonPlan];
}

@end
