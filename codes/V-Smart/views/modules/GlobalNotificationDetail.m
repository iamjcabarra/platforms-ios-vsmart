//
//  GlobalNotificationDetail.m
//  V-Smart
//
//  Created by Ryan Migallos on 2/26/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#define MAS_SHORTHAND
#import "Masonry.h"

#import "GlobalNotificationDetail.h"
#import "AppDelegate.h"
#import "SocialStreamItemCell.h"
#import "SocialStreamStickerCell.h"
#import "SocialStreamPreviewCell.h"
#import "SocialStreamPeopleWhoLike.h"
#import "SocialStreamComment.h"
#import <QuartzCore/QuartzCore.h>

#import "V_Smart-Swift.h"

#import "NoteHelper.h"
#import "MainHeader.h"

@interface GlobalNotificationDetail () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) ResourceManager *rm;

@property (nonatomic, assign) BOOL hideCommentBox;

- (IBAction)unwindFromConfirmationForm:(id)sender;

@end

@implementation GlobalNotificationDetail

static NSString *kCellTextIdentifier = @"cell_ss_text_id";
static NSString *kCellStickerIdentifier = @"cell_ss_sticker_id";
static NSString *kCellPreviewIdentifier = @"cell_ss_preview_id";

- (IBAction)unwindFromConfirmationForm:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    NSLog(@"GlobalNotificationDetail...");
    
    //execute fetch request
    self.rm = [AppDelegate resourceInstance];
    self.managedObjectContext = self.rm.mainContext;
    
    self.tableView.estimatedRowHeight = 44.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
//    NSString *socketio = @"vibe.vsmartschool.me";
//    NSInteger port = 8000;
//    
//    [self.rm reconnectSocketWithURL:socketio port:port];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    //CLOSE SOCKET IO
//    [self.rm closeSocket];
}

//- (void)viewWillLayoutSubviews{
//    [super viewWillLayoutSubviews];
//    self.view.superview.bounds = CGRectMake(0, 0, 600, 300);
//}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.preferredContentSize = CGSizeMake(600, 300);
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self configureCellTable:tableView indexPath:indexPath];
}

- (id)configureCellTable:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *object = nil;
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *user_name = [NSString stringWithFormat:@"%@ %@", [mo valueForKey:@"first_name"], [mo valueForKey:@"last_name"] ];
    NSString *date_modified = [[mo valueForKey:@"date_modified"] description];
    NSString *message = [[mo valueForKey:@"message"] description];
    
    BOOL hidden = ([[mo valueForKey:@"owned"] isEqualToString:@"1"]) ? NO : YES;
    BOOL is_liked = ([[mo valueForKey:@"is_liked"] isEqualToString:@"1"]) ? YES : NO;
    
    NSString *type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"type"] ];
    
    if ([type isEqualToString:@"message"]) {
        
        SocialStreamItemCell *cell = (SocialStreamItemCell *)[tableView dequeueReusableCellWithIdentifier:kCellTextIdentifier
                                                                                             forIndexPath:indexPath];
        
        cell.userNameLabel.text = user_name;
        cell.firstName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"] ];
        cell.lastName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"] ];
        cell.emotionLabel.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"icon_text"] ];
        cell.likeCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"likeCount"] ];
        cell.commentCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"commentCount"] ];
        
        cell.dateLabel.text = date_modified;
        cell.messageLabel.text = message;
        
        //ENABLE OPTION BUTTON
        cell.optionButton.hidden = hidden;
        
        //SHOW LIKE LIST
        [cell.peopleWhoLikeButton addTarget:self action:@selector(actionShowPeopleWhoLike:) forControlEvents:UIControlEventTouchUpInside];
        
        //SHOW COMMENT LIST
        [cell.peopleWhoComment addTarget:self action:@selector(actionShowWhoComment:) forControlEvents:UIControlEventTouchUpInside];
        
        //LIKE BUTTON ACTION
        [cell.likeButton addTarget:self action:@selector(actionLike:) forControlEvents:UIControlEventTouchUpInside];
        //SET SELECTED
        cell.likeButton.selected = is_liked;
        
        //ADD TO NOTE
//        [cell.noteButton addTarget:self action:@selector(actionAddNote:) forControlEvents:UIControlEventTouchUpInside];
        
        //COMMENT BOX
        [cell.commentButton addTarget:self action:@selector(actionShowCommentBox:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell showAlias];
        
        return cell;
    }
    
    if ([type isEqualToString:@"sticker"]) {
        
        SocialStreamStickerCell *cell = (SocialStreamStickerCell *)[tableView dequeueReusableCellWithIdentifier:kCellStickerIdentifier
                                                                                                   forIndexPath:indexPath];
        
        cell.userNameLabel.text = user_name;
        cell.firstName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"] ];
        cell.lastName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"] ];
        cell.emotionLabel.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"icon_text"] ];
        cell.likeCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"likeCount"] ];
        cell.commentCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"commentCount"] ];
        cell.dateLabel.text = date_modified;
        
        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", message]];
        
        cell.stickerImageView.image = [[EGOImageLoader sharedImageLoader] imageForURL:imageURL shouldLoadWithObserver:nil];
        
        //ENABLE OPTION BUTTON
        cell.optionButton.hidden = hidden;
        
        //SHOW LIKE LIST
        [cell.peopleWhoLikeButton addTarget:self action:@selector(actionShowPeopleWhoLike:) forControlEvents:UIControlEventTouchUpInside];
        
        //SHOW COMMENT LIST
        [cell.peopleWhoComment addTarget:self action:@selector(actionShowWhoComment:) forControlEvents:UIControlEventTouchUpInside];
        
        //LIKE BUTTON ACTION
        [cell.likeButton addTarget:self action:@selector(actionLike:) forControlEvents:UIControlEventTouchUpInside];
        //SET SELECTED
        cell.likeButton.selected = is_liked;
        
        //ADD TO NOTE
//        [cell.noteButton addTarget:self action:@selector(actionAddNote:) forControlEvents:UIControlEventTouchUpInside];
        
        //COMMENT BOX
        [cell.commentButton addTarget:self action:@selector(actionShowCommentBox:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell showAlias];
        
        return cell;
    }
    
    if ([type isEqualToString:@"image"]) {
        
        SocialStreamPreviewCell *cell = (SocialStreamPreviewCell *)[tableView dequeueReusableCellWithIdentifier:kCellPreviewIdentifier
                                                                                                   forIndexPath:indexPath];
        
        cell.userNameLabel.text = user_name;
        cell.firstName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"] ];
        cell.lastName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"] ];
        cell.emotionLabel.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"icon_text"] ];
        cell.likeCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"likeCount"] ];
        cell.commentCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"commentCount"] ];
        cell.dateLabel.text = date_modified;
        
        NSString *content_id = [mo valueForKey:@"message_id"];
        
        
        NSManagedObject *po = [self.rm getEntity:kSocialStreamPreviewEntity
                                       attribute:@"content_id"
                                       parameter:content_id
                                         context:mo.managedObjectContext];
        
        /*
         url_description
         url
         title
         message_type
         image_thumb
         image
         content_id
         */
        
        cell.messageLabel.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message"] ];
        
        NSString *image_thumb = [NSString stringWithFormat:@"%@", [po valueForKey:@"image_thumb"] ];
        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", image_thumb]];
        cell.previewImageView.image = [[EGOImageLoader sharedImageLoader] imageForURL:imageURL shouldLoadWithObserver:nil];
        
        NSString *previewTitle = [NSString stringWithFormat:@"%@", [po valueForKey:@"title"] ];
        cell.previewTitleLabel.text = previewTitle;
        
        NSString *previewDescription = [NSString stringWithFormat:@"%@", [po valueForKey:@"url_description"] ];
        cell.previewDescriptionLabel.text = previewDescription;
        
        
        //ENABLE OPTION BUTTON
        cell.optionButton.hidden = hidden;
        
        //SHOW LIKE LIST
        [cell.peopleWhoLikeButton addTarget:self action:@selector(actionShowPeopleWhoLike:) forControlEvents:UIControlEventTouchUpInside];
        
        //SHOW COMMENT LIST
        [cell.peopleWhoComment addTarget:self action:@selector(actionShowWhoComment:) forControlEvents:UIControlEventTouchUpInside];
        
        //LIKE BUTTON ACTION
        [cell.likeButton addTarget:self action:@selector(actionLike:) forControlEvents:UIControlEventTouchUpInside];
        //SET SELECTED
        cell.likeButton.selected = is_liked;
        
        //ADD TO NOTE
//        [cell.noteButton addTarget:self action:@selector(actionAddNote:) forControlEvents:UIControlEventTouchUpInside];
        
        //COMMENT BOX
        [cell.commentButton addTarget:self action:@selector(actionShowCommentBox:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell showAlias];
        
        return cell;
    }
    
    NSLog(@"returning empty cell...");
    return object;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return NO;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//}

- (NSManagedObject *)managedObjectFromButtonAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (void)actionShowPeopleWhoLike:(id)sender
{
    [self performSegueWithIdentifier:@"showPeopleWhoLikeBox" sender:sender];
}

- (void)actionShowWhoComment:(id)sender
{
    self.hideCommentBox = YES;
    [self performSegueWithIdentifier:@"showCommentBox" sender:sender];
}

- (void)actionShowCommentBox:(id)sender
{
    self.hideCommentBox = NO;
    [self performSegueWithIdentifier:@"showCommentBox" sender:sender];
}

- (NSString *)getUserIDString {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return user_id;
}

- (void)actionLike:(id)sender
{
    UIButton *starButton = (UIButton *)sender;
    
    NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
    NSString *avatar = [NSString stringWithFormat:@"%@", [mo valueForKey:@"avatar"] ];
    NSString *username = [NSString stringWithFormat:@"%@", [mo valueForKey:@"username"] ];
    NSString *user_id = [self getUserIDString];//[NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ]; // BUG FIX #124
    NSString *message_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];
    
    //LIKE MESSAGE
    NSMutableDictionary *d = [@{@"avatar":avatar,
                                @"username":username,
                                @"user_id":user_id,
                                @"content_id":message_id,
                                @"is_message":@1,
                                @"is_deleted":@0} mutableCopy];
    
    if (!starButton.selected) {
        [self.rm postLikeMessage:d doneBlock:^(BOOL status) {
            if (status) {
                [self refreshPostWithMessageID:message_id];
                NSLog(@"success like message...");
            }
        }];
    }
    
    if (starButton.selected) {
        [self.rm postUnlikeMessage:d doneBlock:^(BOOL status) {
            if (status) {
                [self refreshPostWithMessageID:message_id];
                NSLog(@"success unlike message...");
            }
            
        }];
    }
}

- (void)refreshPostWithMessageID:(NSString *)messageID {
    [self.rm requestNotificationDetailsWithMessageID:messageID doneBlock:^(BOOL status) {
        
    }];
}

- (void)actionAddNote:(id)sender
{
//    NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
//    NSString *messageContent = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message"] ];
//    
//    __weak typeof (self) ws = self;
//    NoteHelper *nh = [[NoteHelper alloc] init];
//    [nh saveMessage:messageContent doneBlock:^(BOOL status) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            AlertWithMessageAndDelegate(@"Note Book", @"Added Note", ws);
//        });
//    }];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SocialStreamFeed" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Predicate
//    NSPredicate *p1 = [self.rm predicateForKeyPath:@"is_deleted" andValue:@"0"];
//    NSPredicate *p2 = [self.rm predicateForKeyPath:@"group_id" andValue:self.group_id];
    NSPredicate *p3 = [self.rm predicateForKeyPath:@"message_id" andValue:self.message_id];
//    NSPredicate *p4 = [self.rm predicateForKeyPath:@"user_id" andValue:self.user_id];
    
//    NSCompoundPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[p1, p2, p3, p4]];
    [fetchRequest setPredicate:p3];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"message_id" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:self.managedObjectContext
                                                                                                  sectionNameKeyPath:nil
                                                                                                           cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView cellForRowAtIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showPeopleWhoLikeBox"]) {
        NSLog(@"show people who like box");
        SocialStreamPeopleWhoLike *sspwl = (SocialStreamPeopleWhoLike *)segue.destinationViewController;
        NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
        
        NSString *groupid = [NSString stringWithFormat:@"%@", [mo valueForKey:@"group_id"] ];
        sspwl.groupid = (NSUInteger)[groupid integerValue];
        sspwl.messageid = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];
    }

    if ([segue.identifier isEqualToString:@"showCommentBox"]) {
        
        SocialStreamComment *sc = (SocialStreamComment *)segue.destinationViewController;
        NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
        
        //GROUP ID
        NSString *groupid = [NSString stringWithFormat:@"%@", [mo valueForKey:@"group_id"] ];
        sc.groupid = (NSUInteger)[groupid integerValue];
        sc.messageid = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];
        sc.hideCommentBox = self.hideCommentBox;
    }
    
}

@end

