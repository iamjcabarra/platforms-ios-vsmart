//
//  NotesContainerViewController.h
//  V-Smart
//
//  Created by VhaL on 2/6/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "NoteColorViewController.h"
#import "TagViewController.h"
#import "NoteBookViewController.h"
#import "NoteContentViewController.h"
#import "SWTableViewCell.h"
#import "UMTableViewCellDefault.h"
#import "UMTableViewCellArchived.h"
#import "NotesCollectionViewCell.h"
#import "NoteSidePanelViewController.h"
#import "NNote.h"
#import "NNoteTag.h"
#import "NNoteBook.h"
#import "NColor.h"

#import "NoteBookCollectionViewController.h"

@interface NotesChildViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SWTableViewCellDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UITableView *tableViewNotes;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewNotes;
@property (strong, nonatomic) IBOutlet NoteBookCollectionViewController *noteBookCollectionViewController;

@property (weak, nonatomic) IBOutlet UIButton *buttonAddNew;
@property (weak, nonatomic) IBOutlet UIButton *buttonCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *buttonTableView;

@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) id otherNoteBookViewController;

@property (nonatomic, strong) NoteColorViewController *colorPicker;
@property (nonatomic, strong) NoteBookViewController *folderPicker;
@property (nonatomic, strong) TagViewController *filterPicker;

@property (nonatomic, strong) NoteContentViewController *noteContentViewController;
@property (nonatomic, strong) NoteSidePanelViewController *noteSidePanelViewController;

-(IBAction)sidePanelTapped:(id)sender;
@end
