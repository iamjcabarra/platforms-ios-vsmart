//
//  PlayListContainerController.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/10/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "PlayListContainerController.h"

@interface PlayListContainerController ()
@property (nonatomic, strong) UIViewController *containerView;
@end

@implementation PlayListContainerController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupChildViewController];
    [self layoutNotifications];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        dispatch_queue_t queue = dispatch_queue_create("com.pearson.playlist.ACTIVITY",DISPATCH_QUEUE_SERIAL);
        dispatch_async(queue, ^{
            ResourceManager *rm = [AppDelegate resourceInstance];
            NSString *details = @"Left the PlayList module in Apple iPad";
            [rm requestLogActivityWithModuleType:@"6" details:details];
        });
    }
    
    [super viewWillDisappear:animated];
}

- (void)setupChildViewController {
    
    NSString *storyboardName = @"PlayListStoryboard";
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    self.containerView = [sb instantiateInitialViewController];
    [self initiateCustomLayoutFor:self.containerView];
    self.containerView.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.containerView.view.autoresizesSubviews = YES;
    
    [self addChildViewController:self.containerView];
    [self.view addSubview:self.containerView.view];
    [self.containerView didMoveToParentViewController:self];
    [self.view sendSubviewToBack:self.containerView.view];
}

- (void)layoutNotifications {
    VS_NCADD(kNotificationProfileHeight, @selector(resizeContainerView:))
}

- (NSString *) dashboardName {
    NSString *moduleName = NSLocalizedString(@"PlayList", nil);
    return moduleName;
}

#pragma mark - Post Notification Events
- (void)resizeContainerView:(NSNotification *)notification {
    
    VLog(@"Received: kNotificationProfileHeight");
    [UIView animateWithDuration:0.45 animations:^{
        [self initiateCustomLayoutFor:self.containerView];
        [self.containerView.view setNeedsLayout];
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            [super adjustProfileHeight];
        }
        
        [super showOrHideMiniAvatar];
    }];
}

- (void)initiateCustomLayoutFor:(UIViewController *)viewcontroller {
    
    float profileY = [super profileHeight];
    float headerDecrement = ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    float gridHeight = self.view.frame.size.height - headerDecrement;
    float gridY = [super headerSize].size.height + profileY;
    CGRect customFrame = CGRectMake(0, gridY, self.view.frame.size.width, gridHeight);
    
    [viewcontroller.view setFrame:customFrame];
}

@end
