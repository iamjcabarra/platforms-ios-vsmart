//
//  CPCourseDetailSegmentView.h
//  V-Smart
//
//  Created by Julius Abarra on 30/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPCourseDetailSegmentView : UIViewController

@property (strong, nonatomic) NSManagedObject *courseObject;

- (void)swapEmbeddedViews:(NSString *)segueIdentifier;

@end
