//
//  SSStickerCell.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 04/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class SSStickerCell: UITableViewCell {
    
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var feelingView: UIView!
    @IBOutlet weak var isFeelingLabel: UILabel!
    @IBOutlet weak var feelingLabel: UILabel!
    @IBOutlet weak var isFeelingImage: UIImageView!
    
    @IBOutlet weak var menuButton: UIButton!
    
    @IBOutlet weak var stickerImage: UIImageView!
    
    @IBOutlet weak var noLikesLabel: UILabel!
    @IBOutlet weak var noLikesButton: UIButton!
    @IBOutlet weak var noLikesView: UIView!
    
    @IBOutlet weak var noCommentsLabel: UILabel!
    @IBOutlet weak var noCommentsButton: UIButton!
    @IBOutlet weak var noCommentsView: UIView!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    
    override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
    
    //        dispatch_async(dispatch_get_main_queue()) {
        self.cellBackgroundView.customShadow(3)
        self.nameLbl.text = ""
        self.statusImage.image = nil
        self.statusLabel.text = ""
        self.timeLabel.text = ""
        
        self.feelingView.isHidden = true
        self.feelingLabel.text = ""
        self.isFeelingLabel.text = ""
        
    //    self.messageLabel.text = ""
        self.stickerImage.image = nil
        
        self.noLikesLabel.text = ""
        self.noCommentsLabel.text = ""
        
        self.likeButton.isSelected = false
            
        self.menuButton.isHidden = false
        
        self.noLikesView.isHidden = true
        self.noCommentsView.isHidden = true
    //        }
    }
    
}
