//
//  LPMDLLCollapsibleContentController.swift
//  V-Smart
//
//  Created by Julius Abarra on 26/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

private extension UILabel {
    var optimalHeight : CGFloat {
        get {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: CGFloat.greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = self.lineBreakMode
            label.font = self.font
            label.text = self.text
            label.sizeToFit()
            return label.frame.height
        }
    }
}

class LPMDLLCollapsibleContentController: UITableViewController, UITextViewDelegate {
    
    @IBOutlet fileprivate var subDetailsView: UIView!
    @IBOutlet fileprivate var segmentedControl: UISegmentedControl!
    
    fileprivate let kViewSwapperSegueIdentifier = "SHOW_SUB_DETAILS_VIEW_SWAPPER"
    fileprivate let kCommentSegueIdentifier = "SHOW_COMMENT_VIEW"
    fileprivate let kHistorySegueIdentifier = "SHOW_HISTORY_VIEW"
    
    fileprivate let notification = NotificationCenter.default
    fileprivate var viewSwapper: LPMLessonSubDetailsViewSwapper!
    fileprivate var viewMode: String!
    fileprivate var tablePosition: CGPoint!
    
    // MARK: - Data Manager
    
    fileprivate lazy var lpmDataManager: LPMDataManager = {
        let lpdm = LPMDataManager.sharedInstance
        return lpdm
    }()
    
    // MARK: - Data Structure
    
    fileprivate struct Section {
        var header: NSManagedObject
        var contents: [NSManagedObject]?
        var collapsed: Bool
        
        init(header: NSManagedObject, contents: [NSManagedObject]?, collapsed: Bool = false) {
            self.header = header
            self.contents = contents
            self.collapsed = collapsed
        }
    }
    
    fileprivate var sections = [Section]()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.tableView.estimatedSectionHeaderHeight = 155.0
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        
        self.tableView.estimatedRowHeight = 155.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.setUpNotification()
        self.setUpDLLTemplateCollapsibleContentElementsForDay(1)
        
        self.viewMode = self.lpmDataManager.stringValue(self.lpmDataManager.fetchUserDefaultsObject(forKey: "LPM_USER_DEFAULT_IS_VIEW_MODE"))
       
        if self.viewMode == "1" {
            self.subDetailsView.isHidden = false
            self.setupSegmentedControl()
        }
        else {
            self.subDetailsView.isHidden = true
        }
        
        // Force calling viewWillAppear
        // to make UITableViewController
        // behave as expected (keyboard
        // should not cover editing 
        // component
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tearDownNotification()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Segmented Control
    
    fileprivate func setupSegmentedControl() {
        self.segmentedControl.removeAllSegments()
        self.segmentedControl.tintColor = UIColor(rgba: "#B5F0FF")
        
        let titleA = NSLocalizedString("Comment", comment: "")
        let titleB = NSLocalizedString("History", comment: "")
        
        self.segmentedControl.insertSegment(withTitle: titleA, at: 0, animated: true)
        self.segmentedControl.insertSegment(withTitle: titleB, at: 1, animated: true)
        
        let attributeA = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16), NSForegroundColorAttributeName: UIColor(rgba: "#00AEEF")]
        self.segmentedControl.setTitleTextAttributes(attributeA, for: UIControlState.selected)
        
        let attributeB = [NSFontAttributeName: UIFont.systemFont(ofSize: 16), NSForegroundColorAttributeName: UIColor(rgba: "#00AEEF")]
        self.segmentedControl.setTitleTextAttributes(attributeB, for: UIControlState())
        
        let action = #selector(self.segmentedControlAction(_:))
        self.segmentedControl.addTarget(self, action: action, for: .valueChanged)
        
        self.segmentedControl.selectedSegmentIndex = 0
        self.switchSegmentedViewAtIndex(0)
    }
    
    fileprivate func switchSegmentedViewAtIndex(_ index: Int) {
        DispatchQueue.main.async(execute: {
            let svcSegueIdentifier = index == 0 ? self.kCommentSegueIdentifier : self.kHistorySegueIdentifier
            self.viewSwapper.swapToViewControllerWithSegueIdentifier(svcSegueIdentifier)
        })
    }
    
    func segmentedControlAction(_ sender: UISegmentedControl) {
        self.switchSegmentedViewAtIndex(sender.selectedSegmentIndex)
    }
    
    // MARK: Nofication Methods
    
    fileprivate func setUpNotification() {
        let name = "LPM_NOTIFICATION_DLL_TEMPLATE_RELOAD_COLLAPSIBLE_TABLE_VIEW"
        let selector = #selector(self.reloadCollapsibleTableView(_:))
        self.notification.addObserver(self, selector: selector, name: NSNotification.Name(rawValue: name), object: nil)
    }
    
    fileprivate func tearDownNotification() {
        let name = "LPM_NOTIFICATION_DLL_TEMPLATE_RELOAD_COLLAPSIBLE_TABLE_VIEW"
        self.notification.removeObserver(self, name: NSNotification.Name(rawValue: name), object: nil)
    }
    
    // MARK: - Handle Post Notification
    
    func reloadCollapsibleTableView(_ sender: Notification) {
        let key = "LPM_USER_DEFAULT_DLL_TEMPLATE_DAY_ID"
        
        guard let day = self.lpmDataManager.fetchUserDefaultsObject(forKey: key) as? Int else {
            print("can't reload collapsible table view")
            return
        }
        
        self.setUpDLLTemplateCollapsibleContentElementsForDay(day)
    }
    
    // MARK: Build Table View Data
    
    fileprivate func setUpDLLTemplateCollapsibleContentElementsForDay(_ day: Int) {
        guard let headers = self.lpmDataManager.fetchDLLTemplateContentElementsForDay(day, isHeader: true, headerID: nil) else {
            print("can't retrieve headers")
            return
        }
        
        self.sections.removeAll()
        
        for header in headers {
            if let header_id = header.value(forKey: "header_id") as? Int {
                let contents = self.lpmDataManager.fetchDLLTemplateContentElementsForDay(day, isHeader: false, headerID: header_id)
                self.sections.append(Section(header: header, contents: contents, collapsed: false))
            }
        }
        
        DispatchQueue.main.async(execute: {
            self.view.endEditing(true)
            UIView.transition(with: self.tableView, duration: 0.0, options: .transitionCrossDissolve, animations: { self.tableView.reloadData() }, completion: nil)
        })
    }
    
    // MARK: - Table View Data Source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.sections[section].collapsed == false {
            return 0
        }
        else {
            guard let contents = self.sections[section].contents else { return 0 }
            return contents.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contentCell = tableView.dequeueReusableCell(withIdentifier: "body_cell_identifier", for: indexPath) as! LPMDLLCollapsibleContentBodyCell
        let contentObject = self.sections[(indexPath as NSIndexPath).section].contents![(indexPath as NSIndexPath).row]
        
        let name = contentObject.value(forKey: "name") as! String
        let description = contentObject.value(forKey: "element_description") as! String
        let content = contentObject.value(forKey: "content") as! String
        
        contentCell.nameLabel.text = name
        
        contentCell.descriptionLabel.text = description
        self.justifyLabel(contentCell.descriptionLabel)
        contentCell.descriptionLabelHeight.constant = (description == "") ? 0 : contentCell.descriptionLabel.optimalHeight
        
        contentCell.contentTextView.text = content
        contentCell.contentTextViewHeight.constant = 200
        
        contentCell.contentTextView.tag = (indexPath as NSIndexPath).section * 1000 + (indexPath as NSIndexPath).row
        contentCell.contentTextView.delegate = self
        contentCell.enableEditing(self.viewMode == "1" ? false : true)
        
        return contentCell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "header_cell_identifier") as! LPMDLLCollapsibleContentHeaderCell
        let headerObject = self.sections[section].header
        let collapsed: Bool = self.sections[section].collapsed
        
        let name = headerObject.value(forKey: "name") as! String
        let description = headerObject.value(forKey: "element_description") as! String
        let is_content = headerObject.value(forKey: "is_content") as! String
        let content = headerObject.value(forKey: "content") as! String
        
        headerCell.nameLabel.text = name
        
        headerCell.descriptionLabel.text = description
        self.justifyLabel(headerCell.descriptionLabel)
        headerCell.descriptionLabelHeight.constant = collapsed ? headerCell.descriptionLabel.optimalHeight : 0

        headerCell.contentTextView.text = content
        headerCell.contentTextViewHeight.constant = (is_content != "0" && collapsed) ? 200 : 0
       
        headerCell.collapseButton.tag = section
        headerCell.collapseImage.image = collapsed ? UIImage(named: "Minus_Math_50.png") : UIImage(named: "Plus_Math_50.png")
        headerCell.collapseButton.addTarget(self, action: #selector(self.toggleCollapseButton(_:)), for: .touchUpInside)
        
        headerCell.contentTextView.tag = section * 1000 + 1000000
        headerCell.contentTextView.delegate = self
        headerCell.enableEditing(self.viewMode == "1" ? false : true)
        
        _ = headerCell.contentView.intrinsicContentSize
        headerCell.contentView.layer.borderColor = UIColor.white.cgColor
        headerCell.contentView.layer.borderWidth = 1.0
    
        return headerCell.contentView
    }
    
    // MARK: - Collapse Button Event Handler
    
    func toggleCollapseButton(_ sender: UIButton) {
        let section = sender.tag
        let collapsed = self.sections[section].collapsed
        self.sections[section].collapsed = !collapsed
        
        DispatchQueue.main.async(execute: {
            self.view.endEditing(true)
            UIView.transition(with: self.tableView, duration: 0.0, options: .transitionCrossDissolve, animations: { self.tableView.reloadData() }, completion: nil)
        })
    }
    
    // MARK: - Text View Delegate
    
    func textViewDidChange(_ textView: UITextView) {
        let header = (textView.tag / 1000000) == 1 ? true : false
        var content: NSManagedObject!
        
        if header {
            let section = (textView.tag - 1000000) / 1000
            content = self.sections[section].header
        }
        else {
            let section = textView.tag / 1000
            let row = textView.tag % 1000
            content = self.sections[section].contents![row]
        }
        
        let element_id = content.value(forKey: "id") as! Int
        let day_id = content.value(forKey: "day_id") as! Int
        let element_id_day_id = "\(element_id)-\(day_id)"
        
        let predicate = NSComparisonPredicate(keyPath: "element_id_day_id", withValue: element_id_day_id, isExact: true)
        let data = ["content": textView.text as NSString]
        self.saveChangedData(data as [String: AnyObject], predicate: predicate)
    }
    
        func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.tablePosition = self.tableView.contentOffset
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        DispatchQueue.main.async {
            self.tableView.setContentOffset(self.tablePosition, animated: true)
        }
    }
    
    // MARK: - UILabel Text Justification
    
    fileprivate func justifyLabel(_ label: UILabel) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        
        let attributedString = NSAttributedString(string: label.text!, attributes: [NSParagraphStyleAttributeName: paragraphStyle, NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)])
        label.attributedText = attributedString
        label.numberOfLines = 0
    }

    // MARK: - Saving Local Changes
    
    fileprivate func saveChangedData(_ data: [String: AnyObject], predicate: NSPredicate) {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            let status = self.lpmDataManager.updateEntity(LPMConstants.Entity.DLL_TEMPLATE_CONTENT_ELEMENT, predicate: predicate, data: data as NSDictionary)
            print("SAVING STATUS: \(status)")
        }
    }
            
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.kViewSwapperSegueIdentifier {
            self.viewSwapper = segue.destination as? LPMLessonSubDetailsViewSwapper
        }
    }

}
