//
//  CurriculumViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/28/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "CurriculumViewController.h"
#import "CurriculumTableViewCell.h"
#import "LessonPlanDataManager.h"
#import "VSmartValues.h"

@interface CurriculumViewController ()

<NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate> {
    NSManagedObject *mo_selected;
}

@property (nonatomic, strong) LessonPlanDataManager *lm;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UILabel *lblCurriculumPlannerList;
@property (strong, nonatomic) IBOutlet UIButton *butSaveCurriculum;
@property (strong, nonatomic) IBOutlet UIButton *butCancelCurriculum;

@end

@implementation CurriculumViewController

@synthesize delegate;

static NSString *kCurriculumCellIdentifier = @"curriculumCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lblCurriculumPlannerList.text = NSLocalizedString(@"Curriculum Planner List", nil);
    
    // Set data manager and context
    self.lm = [LessonPlanDataManager sharedInstance];
    self.managedObjectContext = self.lm.mainContext;
    
    mo_selected = nil;
    
    self.butSaveCurriculum.enabled = NO;
    
    // Add an action to buttons
    [self.butSaveCurriculum addTarget:self
                                 action:@selector(saveCurriculumAction:)
                       forControlEvents:UIControlEventTouchUpInside];
    
    [self.butCancelCurriculum addTarget:self
                                 action:@selector(cancelCurriculumAction:)
                       forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)saveCurriculumAction:(id)sender {
    if (mo_selected != nil) {
        [self.delegate moSelected:mo_selected];
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)cancelCurriculumAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CurriculumTableViewCell *curriculumCell = [tableView dequeueReusableCellWithIdentifier:kCurriculumCellIdentifier forIndexPath:indexPath];
    
    // Get managed object
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSLog(@"Curriculum MO: %@", mo);
    
    // Parse data
    NSString *curriculumTitle = [NSString stringWithFormat:@"%@", [mo valueForKey:@"curriculum_title"]];
    NSString *curriculumDescription = [NSString stringWithFormat:@"%@", [mo valueForKey:@"curriculum_description"]];
    
    // Render data to table view cell
    curriculumCell.lblCurriculumName.text = curriculumTitle;
    curriculumCell.lblCurriculumDescription.text = curriculumDescription;
    curriculumCell.butRadio.imageView.image = [UIImage imageNamed:@"radio_button32x32.png"];
    
    return curriculumCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CurriculumTableViewCell *curriculumCell = (CurriculumTableViewCell *)[self.tableView cellForRowAtIndexPath: indexPath];
    curriculumCell.butRadio.imageView.image = [UIImage imageNamed:@"radio_button_selected32x32.png"];
    self.butSaveCurriculum.enabled = YES;
    
    mo_selected = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSLog(@"Curriculum MO SELECTED: %@", mo_selected);
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    CurriculumTableViewCell *curriculumCell = (CurriculumTableViewCell *)[self.tableView cellForRowAtIndexPath: indexPath];
    curriculumCell.butRadio.imageView.image = [UIImage imageNamed:@"radio_button32x32.png"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75;
}

#pragma mark - FetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCurriculumEntity];
    [fetchRequest setFetchBatchSize:50];
    
    NSSortDescriptor *curriculum_id = [NSSortDescriptor sortDescriptorWithKey:@"curriculum_id.intValue" ascending:YES];
    [fetchRequest setSortDescriptors:@[curriculum_id]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSLog(@"Fetch Objects: %@", fetchedObjects);
    
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

@end
