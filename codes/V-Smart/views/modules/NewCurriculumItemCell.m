//
//  NewCurriculumItemCell.m
//  V-Smart
//
//  Created by Julius Abarra on 16/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "NewCurriculumItemCell.h"

@implementation NewCurriculumItemCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
