//
//  GradeBookView.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/9/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "GradeBookView.h"
#import "GradeBookViewCell.h"
#import "GradeBookCustomSectionHeader.h"
#import "GradeBookCourseList.h"
#import "AppDelegate.h"
#import "ResourceManager.h"
#import "VSmart.h"
#import "UIColor-Expanded.h"
#import <QuartzCore/QuartzCore.h>

#define ResultsTableView self.searchResultsTableViewController.tableView

@interface GradeBookView ()

<NSFetchedResultsControllerDelegate, UISearchResultsUpdating, UISearchControllerDelegate,
UITableViewDataSource, UITableViewDelegate, GradeBookCourseListDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) ResourceManager *rm;

@property (nonatomic, strong) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, strong) IBOutlet UIButton *sortButton;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UILabel *courseLabel;
@property (nonatomic, strong) IBOutlet UIButton *courseButton;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) UITableViewController *searchResultsTableViewController;
@property (nonatomic, strong) NSArray *results;

@property (nonatomic, strong) IBOutlet UIView *courseView;
//@property (nonatomic, strong) IBOutlet NSLayoutConstraint *courseHeightConstraints;
@property (nonatomic, strong) NSString *sort_type;
@property (nonatomic, strong) NSString *course_id;
@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, assign) BOOL sortFlag;


@property (nonatomic, strong) NSDateFormatter *formatter;

@end

@implementation GradeBookView

static NSString *kGradeBookCellIdentifier = @"grade_book_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.formatter = [[NSDateFormatter alloc] init];
    [self.formatter setDateFormat:@"MM dd yyyy hh:mma"];//Mar 16 2015 02:33 AM
    
    self.tableView.estimatedRowHeight = 150;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.sectionHeaderHeight = 60;
    
    self.rm = [AppDelegate resourceInstance];
    self.managedObjectContext = self.rm.mainContext;
    
    self.sort_type = @"10";
    
    self.segmentedControl.selectedSegmentIndex = 0;
//    [self toggleCouseVisibilityUsingControlIndex:self.segmentedControl.selectedSegmentIndex];
    
    [self setupSearchCapabilities];
    
    __weak typeof(self) weakObject = self;
    self.user_id = [NSString stringWithFormat:@"%d", [VSmart sharedInstance].account.user.id];
//    [self.rm requestCourseListForUserID:self.user_id doneBlock:^(BOOL status) {
//        NSArray *courseList = [weakObject.rm fetchCourseListForUserID:weakObject.user_id];
//        NSLog(@"course list : %@", courseList);
//        if (courseList.count > 0) {
//            NSMutableDictionary *d = (NSMutableDictionary *)courseList[0];
//            NSLog(@"course data : %@", d);
//            NSString *course_name = [NSString stringWithFormat:@"%@", d[@"course_name"] ];
//            weakObject.course_id = [NSString stringWithFormat:@"%@", d[@"cs_id"] ];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                weakObject.courseLabel.text = course_name;
//            });
//            [weakObject.rm requestGradeListForUser:weakObject.user_id course:weakObject.course_id doneBlock:^(BOOL status) {
//                NSLog(@"grade list done: %d", status);
//            }];
//        }
//    }];
    
    // SEGMENTED CONTROL
    [self.segmentedControl addTarget:self action:@selector(controlAction:) forControlEvents:UIControlEventValueChanged];
    
    // SORT BUTTON
    [self.sortButton addTarget:self action:@selector(changeSortingOrder:) forControlEvents:UIControlEventTouchUpInside];
    
    // COURSE SELECTION
    UITapGestureRecognizer *labelGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedLabel:)];
    [self.courseLabel addGestureRecognizer:labelGesture];
    
//    // TOTO DEBUG
//    [self.rm requestGradeBookForClassID:@"2" dataBlock:^(NSDictionary *data) {
//        
//    }];
}

- (void)changeSortingOrder:(id)sender {
    
    self.sortFlag = (self.sortFlag) ? NO : YES;;
    
    [self reloadFetchedResultsController];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"showCourseList"]) {
        GradeBookCourseList *cl = (GradeBookCourseList *)segue.destinationViewController;
        cl.delegate = self;
        cl.userid = @"3";
    }
}

- (void)didFinishSelectingCourseObject:(NSDictionary *)course {
    
    self.courseLabel.text = [NSString stringWithFormat:@"%@", course[@"course_name"] ];
    self.course_id = [NSString stringWithFormat:@"%@", course[@"course_id"] ];
    
//    [self.rm requestGradeListForUser:self.user_id course:self.course_id doneBlock:^(BOOL status) {
//        NSLog(@"grade list done: %d", status);
//    }];
}

- (void)userTappedLabel:(UITapGestureRecognizer *)recognizer {
    NSLog(@"gesture recognizer...");
    [self performSegueWithIdentifier:@"showCourseList" sender:self];
}

- (void)controlAction:(id)sender {

    UISegmentedControl *s = (UISegmentedControl *)sender;
    
    switch (s.selectedSegmentIndex) {
        case 0:
            self.sort_type = @"10";
            break;
            
        case 1:
            self.sort_type = @"8";
            break;
            
        default:
            break;
    }
    
    [self reloadFetchedResultsController];
}

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

//- (void)toggleCouseVisibilityUsingControlIndex:(NSInteger)index {
//    
//    CGFloat height = 0.0f;
//    if (index == 1) {
//        height = 40.0f;
//    }
//    
//    self.courseHeightConstraints.constant = height;
//    
//    [self.courseLabel setNeedsUpdateConstraints];
//    [self.courseLabel updateConstraintsIfNeeded];
//    
//    [self.courseView setNeedsUpdateConstraints];
//    [self.courseView updateConstraintsIfNeeded];
//    
//    [self.tableView setNeedsUpdateConstraints];
//    [self.tableView updateConstraintsIfNeeded];
//    
//    [UIView animateWithDuration:0.50f animations:^{
//        [self.courseLabel layoutIfNeeded];
//        [self.courseView layoutIfNeeded];
//        [self.tableView layoutIfNeeded];
//    }];
//}


- (void)setupSearchCapabilities {
    
    self.results = [[NSMutableArray alloc] init];
    
//    // A table view for results.
//    UITableView *searchResultsTableView = [[UITableView alloc] initWithFrame:self.tableView.frame];
//    searchResultsTableView.dataSource = self;
//    searchResultsTableView.delegate = self;
//    
//    // Registration of reuse identifiers.
//    [searchResultsTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kGradeBookCellIdentifier];
//    
//    // Init a search results table view controller and setting its table view.
//    self.searchResultsTableViewController = [[UITableViewController alloc] init];
//    self.searchResultsTableViewController.tableView = searchResultsTableView;
//    
//    // Init a search controller with its table view controller for results.
//    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsTableViewController];
//    self.searchController.searchResultsUpdater = self;
//    self.searchController.delegate = self;
//
//    [[UISearchBar appearance] setBackgroundColor:[UIColor whiteColor]];
//    [[UISearchBar appearance] setBarTintColor:UIColorFromHex(0xE3E3E3)];
//    
//    // Make an appropriate size for search bar and add it as a header view for initial table view.
//    [self.searchController.searchBar sizeToFit];
//    self.tableView.tableHeaderView = self.searchController.searchBar;
//    
//    // Enable presentation context.
//    self.definesPresentationContext = YES;
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"GradeBookStoryboard" bundle:nil];
    UITableViewController *srtvc = [sb instantiateViewControllerWithIdentifier:@"gradebook_table_search"];
    srtvc.tableView.dataSource = self;
    srtvc.tableView.delegate = self;
    self.searchResultsTableViewController = srtvc;
    
    // Init a search controller with its table view controller for results.
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsTableViewController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;

    [[UISearchBar appearance] setBackgroundColor:[UIColor whiteColor]];
    [[UISearchBar appearance] setBarTintColor:UIColorFromHex(0xE3E3E3)];

    
    // Make an appropriate size for search bar and add it as a header view for initial table view.
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // Enable presentation context.
    self.definesPresentationContext = YES;
}

- (void)willPresentSearchController:(UISearchController *)searchController {
    NSLog(@"%s ....... TEST", __PRETTY_FUNCTION__);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = 1;
    
    if ([tableView isEqual:ResultsTableView]) {
        return count;
    } else {
        return [[self.fetchedResultsController sections] count];
    }
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    if ([tableView isEqual:ResultsTableView]) {
        
        if (self.results) {
            return self.results.count;
        } else {
            return count;
        }
        
    } else {
        
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        count = [sectionInfo numberOfObjects];
        
    }
    
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
//    if (section == 0) {
//        return CGFLOAT_MIN;
//    }
//    
//    return tableView.sectionHeaderHeight;
    
    return 44.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 110.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *kHeaderIdentifier = @"section_header";
    
    GradeBookCustomSectionHeader *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kHeaderIdentifier];
    if(!headerView) {
        headerView = [[[NSBundle mainBundle] loadNibNamed:@"GradeBookCustomSectionHeader" owner:self options:nil] objectAtIndex:0];
    }
    
    UIView *v = [[UIView alloc] initWithFrame:headerView.bounds];
    v.backgroundColor = [UIColor whiteColor];
    headerView.backgroundView = v;
    headerView.dateLabel.text = @"Date Performed";
    headerView.nameLabel.text = @"Name";
    headerView.scoreLabel.text = @"Score";
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kGradeBookCellIdentifier forIndexPath:indexPath];
    
    if ([tableView isEqual:ResultsTableView]) {
        [self configureFilteredCell:cell indexPath:indexPath];
    }
    
    if (![tableView isEqual:ResultsTableView]) {
        [self configureCell:cell indexPath:indexPath];
    }
    
    return cell;
}

- (void)configureFilteredCell:(UITableViewCell *)gradebookviewcell indexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = self.results[indexPath.row];
    
    GradeBookViewCell *cell = (GradeBookViewCell *)gradebookviewcell;
    [self configureGradebookCell:cell withObject:mo atIndexPath:indexPath];
}

- (void)configureCell:(UITableViewCell *)gradebookviewcell indexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];

    GradeBookViewCell *cell = (GradeBookViewCell *)gradebookviewcell;
    [self configureGradebookCell:cell withObject:mo atIndexPath:indexPath];
}

- (void)configureGradebookCell:(GradeBookViewCell *)cell withObject:(NSManagedObject *)mo atIndexPath:(NSIndexPath *)indexPath {
    
    NSString *activity_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"activity_name"] ];
    NSString *date_performed = [NSString stringWithFormat:@"%@", [mo valueForKey:@"date_performed"] ];
//    NSLog(@"-------> data performed: %@", date_performed);
//    
//    [self.formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//Mar 16 2015 02:33 AM
//    NSDate *date = [self.formatter dateFromString:date_performed];
//    NSString *date_string = [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
//    NSString *time_string = [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle];
    
    NSString *date_time_object_string = [NSString stringWithFormat:@"%@", date_performed];
    
    NSString *score = [NSString stringWithFormat:@"%@", [mo valueForKey:@"score"] ];
    
    NSString *imageName = (self.segmentedControl.selectedSegmentIndex == 0) ? @"img_test" : @"img_quiz";
    cell.itemImageView.image = [UIImage imageNamed:imageName];
    
    cell.dateLabel.text = date_time_object_string;
    //    cell.timeLabel.text = time_string;
    cell.timeLabel.text = @"";
    cell.nameLabel.text = activity_name;
    cell.scoreLabel.text = score;
}

- (id)configureCellTable:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {

    GradeBookViewCell *cell = (GradeBookViewCell *)[tableView dequeueReusableCellWithIdentifier:kGradeBookCellIdentifier forIndexPath:indexPath];
    
    NSManagedObject *mo = ([tableView isEqual:ResultsTableView]) ? self.results[indexPath.row] : [self.fetchedResultsController objectAtIndexPath:indexPath];

    NSString *activity_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"activity_name"] ];
    NSString *date_performed = [NSString stringWithFormat:@"%@", [mo valueForKey:@"date_performed"] ];
    NSString *score = [NSString stringWithFormat:@"%@", [mo valueForKey:@"score"] ];
    
    cell.dateLabel.text = date_performed;
    cell.nameLabel.text = activity_name;
    cell.scoreLabel.text = score;

    return cell;
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kGradeBookEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    NSPredicate *predicate = [self predicateForKeyPath:@"activity_type_id" value:self.sort_type];
    [fetchRequest setPredicate:predicate];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"date_performed"
                                                                     ascending:self.sortFlag];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption | NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] indexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Search Results Updating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    UISearchBar *searchBar = searchController.searchBar;
    
    if (searchBar.text.length > 0) {
        
        NSString *text = searchBar.text;
        
        NSArray *fetchedObjects = self.fetchedResultsController.fetchedObjects;
        
        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(NSManagedObject *mo, NSDictionary *bindings) {
            
            NSString *object = [NSString stringWithFormat:@"%@", [mo valueForKey:@"search_string"]];
            NSStringCompareOptions options = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;
            NSRange range = [object rangeOfString:text options:options];
            
            return range.location != NSNotFound;
        }];
        
        // Set up results.
        self.results = [fetchedObjects filteredArrayUsingPredicate:predicate];;
        
        // Reload search table view.
        [self.searchResultsTableViewController.tableView reloadData];
    }
}

@end
