//
//  QuizInformation.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/6/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuizInformationDelegate <NSObject>
@required
- (void)buttonActionType:(NSString *)type;
@end

@interface QuizInformation : UIViewController

@property (nonatomic, strong) NSString *quizName;
@property (nonatomic, strong) NSString *userid;
@property (nonatomic, strong) NSString *quizid;
@property (nonatomic, strong) NSString *courseid;

@property (nonatomic, weak) id <QuizInformationDelegate> delegate;

@end
