//
//  NoteContent3ViewCell.m
//  V-Smart
//
//  Created by VhaL on 2/12/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NoteContent3ViewCell.h"
#import "NFontSize.h"
#import "NFontType.h"

#define kMaxLimit 5000

@interface NoteContent3ViewCell()

@end

@implementation NoteContent3ViewCell

@synthesize labelCount, textViewDescription, delegate, cell5SaveButton, labelWordCount, cell2Title, buttonToggle, buttonFontJosefinSans, buttonFontRaleway, buttonFontRancho, buttonFontSegoe, buttonFontUbuntu, fontView, sliderFontSize; //, fontTypeId, fontSizeId

-(IBAction)buttonToggleTapped:(id)sender{
    
    CGRect textViewDescriptionFrame;
    CGRect labelCountFrame;
    CGFloat alpha;
    
    
    if (fontView.alpha == 1){
        
        textViewDescriptionFrame = CGRectMake(textViewDescription.frame.origin.x, textViewDescription.frame.origin.y, textViewDescription.frame.size.width + (fontView.frame.size.width - buttonToggle.frame.size.width) , textViewDescription.frame.size.height);

        labelCountFrame = CGRectMake(labelCount.frame.origin.x + (fontView.frame.size.width - buttonToggle.frame.size.width), labelCount.frame.origin.y, labelCount.frame.size.width , labelCount.frame.size.height);
        
        alpha = 0;
    }
    else{
        
        textViewDescriptionFrame = CGRectMake(textViewDescription.frame.origin.x, textViewDescription.frame.origin.y, textViewDescription.frame.size.width - (fontView.frame.size.width - buttonToggle.frame.size.width) , textViewDescription.frame.size.height);
        
        labelCountFrame = CGRectMake(labelCount.frame.origin.x - (fontView.frame.size.width - buttonToggle.frame.size.width), labelCount.frame.origin.y, labelCount.frame.size.width , labelCount.frame.size.height);
        
        alpha = 1;
    }
    
    
    
    [UIView animateWithDuration:0.45 animations:^{
        
        fontView.alpha = alpha;
        textViewDescription.frame = textViewDescriptionFrame;
        labelCount.frame = labelCountFrame;
        
    } completion:^(BOOL finished) {
        
    }];
    
}

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.textViewDescription.editable = NO;
    //The setup code (in viewDidLoad in your view controller)
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.textViewDescription addGestureRecognizer:singleFingerTap];
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
//    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    self.textViewDescription.editable = YES;
    [self.textViewDescription becomeFirstResponder];
    
    //Do stuff here...
}

-(IBAction)fontSizeChanged:(UISlider*)sender{
    textViewDescription.font = [textViewDescription.font fontWithSize:sender.value];
}

-(IBAction)buttonFontTapped:(UIButton*)button{
    textViewDescription.font = [button.titleLabel.font fontWithSize:sliderFontSize.value];
}

-(void)initializeCellWithDelegate:(id)parent{
    
    delegate = parent;
    
    self.textViewDescription.delegate = self;
    [self updateTextCount];
    [self buttonToggleTapped:nil];
    
//    [Utils ListFonts];
    
    NFontType *defaultFontType = [[NoteDataManager sharedInstance] getFontTypeByFontTypeId:0 useMainThread:YES];
    NFontSize *defaultFontSize = [[NoteDataManager sharedInstance] getFontSizeByFontSizeId:10 useMainThread:YES];
    NFontSize *fontSizeMin = [[NoteDataManager sharedInstance] getFontSizeByFontSizeId:0 useMainThread:YES];
    NFontSize *fontSizeMax = [[NoteDataManager sharedInstance] getFontSizeByFontSizeId:19 useMainThread:YES];
    
    sliderFontSize.minimumValue = fontSizeMin.fontSize.intValue;
    sliderFontSize.value = defaultFontSize.fontSize.intValue;
    sliderFontSize.maximumValue = fontSizeMax.fontSize.intValue;
    
    buttonFontJosefinSans.titleLabel.font = [UIFont fontWithName:@"Josefin Sans" size:15];
    buttonFontRaleway.titleLabel.font = [UIFont fontWithName:@"Raleway" size:15];
    buttonFontRancho.titleLabel.font = [UIFont fontWithName:@"Rancho" size:15];
    buttonFontUbuntu.titleLabel.font = [UIFont fontWithName:@"Ubuntu" size:15];
    
    textViewDescription.font = [UIFont fontWithName:defaultFontType.fontName size:sliderFontSize.value];
}

-(void)updateTextCount{
//    JCA-08012017: Handle empty note title and description.
//    labelCount.text = [NSString stringWithFormat:@"%lu/%d", (unsigned long)textViewDescription.text.length, kMaxLimit];
//    
//    if (kMaxLimit < textViewDescription.text.length){
//        labelCount.textColor = [UIColor redColor];
//        cell5SaveButton.enabled = NO;
//    }
//    else{
//        labelCount.textColor = [UIColor colorWithRed:3.0/255 green:141.0/255 blue:191.0/255 alpha:1];
//        
//        if ([cell2Title.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0){
//            if ([textViewDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0){
//                cell5SaveButton.enabled = NO;
//            }
//            else{
//                cell5SaveButton.enabled = YES;
//            }
//        }
//        else{
//            cell5SaveButton.enabled = YES;
//        }
//    }
//    
//    [self countWords];
    
    labelCount.text = [NSString stringWithFormat:@"%lu/%d", (unsigned long)textViewDescription.text.length, kMaxLimit];
    NSInteger titleLength = [cell2Title.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length;
    NSInteger descriptionLength = [textViewDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length;
    cell5SaveButton.enabled = (titleLength > 0 && descriptionLength > 0) ? YES : NO;
    
    if (kMaxLimit < textViewDescription.text.length) {
        labelCount.textColor = [UIColor redColor];
    }
    else {
        labelCount.textColor = [UIColor colorWithRed:3.0 / 255.0 green:141.0 / 255.0 blue:191.0 / 255.0 alpha:1.0];
    }
    
    [self countWords];
}

-(void)countWords{
    NSString *string = [textViewDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSArray *array = [string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    NSPredicate *pred = [NSPredicate predicateWithFormat:@"self != %@", @""];
    array = [array filteredArrayUsingPredicate:pred];
    
    if (array.count <= 1)
        labelWordCount.text = [NSString stringWithFormat:@"%lu word", (unsigned long)array.count];
    else
        labelWordCount.text = [NSString stringWithFormat:@"%lu words", (unsigned long)array.count];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    [self updateTextCount];
}

-(void)textViewDidChange:(UITextView *)textView{
    [self updateTextCount];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if(text.length == 0){
        if(textView.text.length != 0)
            return YES;
    }
    else if(kMaxLimit <= textView.text.length){
        return NO;
    }
    return YES;
}

@end
