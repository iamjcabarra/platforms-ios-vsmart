//
//  SocialStreamPreviewCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 2/24/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@interface SocialStreamPreviewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *feedContainer;
@property (strong, nonatomic) IBOutlet EGOImageView *avatarImage;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *emotionLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIButton *optionButton;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;

@property (strong, nonatomic) IBOutlet EGOImageView *previewImageView;
@property (strong, nonatomic) IBOutlet UILabel *previewTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *previewDescriptionLabel;

@property (strong, nonatomic) IBOutlet UILabel *likeCountLabel;
@property (strong, nonatomic) IBOutlet UIButton *peopleWhoLikeButton;
@property (strong, nonatomic) IBOutlet UILabel *commentCountLabel;
@property (strong, nonatomic) IBOutlet UIButton *peopleWhoComment;

@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *commentButton;

@property (strong, nonatomic) IBOutlet UIImageView *statusImage;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;

- (void)showAlias;

@end
