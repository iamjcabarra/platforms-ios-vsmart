//
//  NewCurriculumDetailView.h
//  V-Smart
//
//  Created by Julius Abarra on 16/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewCurriculumDetailView : UIViewController

@property (strong, nonatomic) NSManagedObject *curriculumObject;

@end
