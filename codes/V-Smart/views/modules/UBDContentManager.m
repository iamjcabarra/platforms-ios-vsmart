//
//  UBDContentManager.m
//  V-Smart
//
//  Created by Julius Abarra on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "UBDContentManager.h"

@implementation UBDContentManager

#pragma mark - Singleton Methods

+ (id)sharedInstance {
    static UBDContentManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    // This is to make sure that singleton class will only be created once
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init {
    
    // Initialize collection objects when singleton class is created
    if (self = [super init]) {
        self.contentOverview = [NSMutableDictionary dictionary];
        self.contentStage = [NSMutableDictionary dictionary];
        self.contentStageFile = [NSMutableDictionary dictionary];
        self.contentWithFileList = [NSMutableArray array];
    }
    
    return self;
}

- (void)clearEntries {
    self.lpid = nil;
    self.courseid = nil;
    self.gradeLevel = nil;
    self.actionType = nil;
    self.view = nil;
    self.contentOverview = nil;
    self.contentStage = nil;
    self.contentWithFileList = nil;
    
    self.contentOverview = [NSMutableDictionary dictionary];
    self.contentStage = [NSMutableDictionary dictionary];
    self.contentStageFile = [NSMutableDictionary dictionary];
    self.contentWithFileList = [NSMutableArray array];
}

@end
