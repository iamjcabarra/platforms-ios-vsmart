//
//  CPCourseDetailSegmentView.m
//  V-Smart
//
//  Created by Julius Abarra on 30/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "CPCourseDetailSegmentView.h"
#import "CPEmbeddedStudentListView.h"
#import "CPEmbeddedAssignedTestsView.h"
#import "V_Smart-Swift.h"

@interface CPCourseDetailSegmentView ()

@property (strong, nonatomic) CPEmbeddedStudentListView *studentListView;
@property (strong, nonatomic) CPEmbeddedAssignedTestsView *assignedTestsView;
@property (strong, nonatomic) CPEmbeddedSeatPlanView *seatPlanView;
@property (strong, nonatomic) CPEmbeddedAttendanceView *attendanceView;

@property (strong, nonatomic) NSString *currentSegueIdentifier;

@end

@implementation CPCourseDetailSegmentView

static NSString *kShowStudentListView = @"SHOW_STUDENT_LIST_VIEW";
static NSString *kShowAssignedTestsView = @"SHOW_ASSIGNED_TESTS_VIEW";
static NSString *kShowSeatPlanView = @"SHOW_SEAT_PLAN_VIEW";
static NSString *kShowAttendanceView = @"SHOW_ATTENDANCE_VIEW";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.currentSegueIdentifier = kShowStudentListView;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kShowStudentListView]) {
        self.studentListView = (CPEmbeddedStudentListView *)[segue destinationViewController];
        
        if (self.courseObject != nil) {
            self.studentListView.courseObject = self.courseObject;
        }
        
        if (self.childViewControllers.count > 0) {
            [self swapFromView:[self.childViewControllers objectAtIndex:0] toView:segue.destinationViewController];
        }
        else {
            [self addChildViewController:segue.destinationViewController];
            ((UIViewController *)segue.destinationViewController).view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:((UIViewController *)segue.destinationViewController).view];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:kShowAssignedTestsView]) {
        self.assignedTestsView = (CPEmbeddedAssignedTestsView *)[segue destinationViewController];
        
        if (self.courseObject != nil) {
            self.assignedTestsView.courseObject = self.courseObject;
        }
        
        [self swapFromView:[self.childViewControllers objectAtIndex:0] toView:segue.destinationViewController];
    }
    else if ([segue.identifier isEqualToString:kShowSeatPlanView]) {
        self.seatPlanView = (CPEmbeddedSeatPlanView *)[segue destinationViewController];
        
        if (self.courseObject != nil) {
            self.seatPlanView.courseSectionID = [NSString stringWithFormat:@"%@",  [self.courseObject valueForKey:@"cs_id"]];
        }
        
        [self swapFromView:[self.childViewControllers objectAtIndex:0] toView:segue.destinationViewController];
    }
    else if ([segue.identifier isEqualToString:kShowAttendanceView]) {
        self.attendanceView = (CPEmbeddedAttendanceView *)[segue destinationViewController];
        [self swapFromView:[self.childViewControllers objectAtIndex:0] toView:segue.destinationViewController];
    }
}

- (void)swapFromView:(UIViewController *)fromView toView:(UIViewController *)toView {
    toView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    UIViewAnimationOptions transition = UIViewAnimationOptionTransitionCrossDissolve;
    
    [fromView willMoveToParentViewController:nil];
    [self addChildViewController:toView];
    [self transitionFromViewController:fromView
                      toViewController:toView
                              duration:0
                               options:transition
                            animations:nil
                            completion:^(BOOL finished) {
                                [fromView removeFromParentViewController];
                                [toView didMoveToParentViewController:self];
                            }];
}

- (void)swapEmbeddedViews:(NSString *)segueIdentifier {
    [self performSegueWithIdentifier:segueIdentifier sender:nil];
}

@end
