//
//  SSSelectedPeopleCollectionViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 29/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class SSSelectedPeopleCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var removeButton: UIButton!
    
}
