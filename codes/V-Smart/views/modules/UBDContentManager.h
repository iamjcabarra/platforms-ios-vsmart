//
//  UBDContentManager.h
//  V-Smart
//
//  Created by Julius Abarra on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UBDContentManager : NSObject

/////// PUBLIC PROPERTIES //////
@property (nonatomic, strong) NSString *lpid;
@property (nonatomic, strong) NSString *courseid;
@property (nonatomic, strong) NSString *gradeLevel;
@property (nonatomic, strong) NSString *templateid;
@property (nonatomic, strong) NSString *actionType;
@property (nonatomic, strong) NSString *view;
@property (nonatomic, strong) NSMutableDictionary *contentOverview;
@property (nonatomic, strong) NSMutableDictionary *contentStage;
@property (nonatomic, strong) NSMutableDictionary *contentStageFile;
@property (nonatomic, strong) NSMutableArray *contentWithFileList;

/////// PUBLIC SINGLETON METHODS //////
+ (id)sharedInstance;
- (void)clearEntries;

@end
