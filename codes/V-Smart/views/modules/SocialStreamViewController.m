//
//  SocialStreamViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 9/9/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "SocialStreamViewController.h"

//#import "SocialStreamChildViewController.h"
#import "SocialStreamController.h"

@interface SocialStreamViewController ()

//@property (nonatomic, strong) SocialStreamChildViewController *containerView;
@property (nonatomic, strong) SocialStreamController *containerView;

@end

@implementation SocialStreamViewController
@synthesize containerView;

-(void)viewDidLoad{
    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupChildViewController];
    
    [self _setupNotifications];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        dispatch_queue_t queue = dispatch_queue_create("com.pearson.course.ACTIVITY",DISPATCH_QUEUE_SERIAL);
        dispatch_async(queue, ^{
            ResourceManager *rm = [AppDelegate resourceInstance];
            NSString *details = @"Left the Social module in Apple iPad";
            [rm requestLogActivityWithModuleType:@"3" details:details];
        });
    }
    
    [super viewWillDisappear:animated];
}

- (UIViewController *)loadControllerWithIdentifier:(NSString *)identifier {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SocialStreamStoryboard" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:identifier];
}

- (void)setupChildViewController{
    
//    float profileY = [super profileHeight];
//    float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
//    float gridY = [super headerSize].size.height + profileY;
//    
//    containerView = [[SocialStreamChildViewController alloc] init];
//    containerView.view.frame = CGRectMake(0, gridY, self.view.frame.size.width, gridHeight - 5);
    
//    NSString *storyboardName = @"SocialStreamStoryboard";
//    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
//    containerView = [sb instantiateViewControllerWithIdentifier:@"SocialStreamModule"];
//    containerView.view.frame = customFrame;
//    containerView.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
//    containerView.view.autoresizesSubviews = YES;
//
//    [self addChildViewController:containerView];
//    [self.view addSubview:containerView.view];
//    [containerView didMoveToParentViewController:self];
//    [self.view sendSubviewToBack:containerView.view];
    
    NSString *storyboardName = @"SocialStreamStoryboard";
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    self.containerView = [sb instantiateViewControllerWithIdentifier:@"SocialStreamModule"];
    [self initiateCustomViewLayoutFor:self.containerView];
    self.containerView.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.containerView.view.autoresizesSubviews = YES;
    
    [self addChildViewController:self.containerView];
    [self.view addSubview:self.containerView.view];
    [self.containerView didMoveToParentViewController:self];
    [self.view sendSubviewToBack:self.containerView.view];
}

-(void) _setupNotifications {
    VS_NCADD(kNotificationProfileHeight, @selector(_refreshController:))
}

- (NSString *) dashboardName {
    
    NSString *moduleName = NSLocalizedString(@"Social Stream", nil);
    return moduleName;
    
//    return kModuleSocialStream;
}

#pragma mark - Post Notification Events

-(void) _refreshController: (NSNotification *) notification{
    VLog(@"Received: kNotificationProfileHeight");
    
    [UIView animateWithDuration:0.45 animations:^{
        float profileY = [super profileHeight];
        float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
        //float gridHeight = 638 + [super profileSize].size.height;
        
        float gridY = [super headerSize].size.height + profileY;
        [self.containerView.view setFrame:CGRectMake(0, gridY, self.view.frame.size.width, gridHeight - 5)];
        [self.containerView.view setNeedsLayout];
        
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            [super adjustProfileHeight];
        }
        [super showOrHideMiniAvatar];
    } completion:^(BOOL finished) {
        
    }];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [SVProgressHUD dismiss];
}

- (void)initiateCustomViewLayoutFor:(UIViewController *)viewcontroller {
    
    float profileY = [super profileHeight];
    float headerDecrement = ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    float gridHeight = self.view.frame.size.height - headerDecrement;
    float gridY = [super headerSize].size.height + profileY;
    CGRect customFrame = CGRectMake(0, gridY, self.view.frame.size.width, gridHeight);
    
    [viewcontroller.view setFrame:customFrame];
}


@end
