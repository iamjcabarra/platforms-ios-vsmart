//
//  CurriculumBrowserItemCell.h
//  V-Smart
//
//  Created by Julius Abarra on 18/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurriculumBrowserItemCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *fileIcon;
@property (strong, nonatomic) IBOutlet UILabel *filenameLabel;

- (void)showSelection:(BOOL)selection;

@end
