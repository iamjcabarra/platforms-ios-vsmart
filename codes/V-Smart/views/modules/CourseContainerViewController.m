//
//  CourseContainerViewController.m
//  V-Smart
//
//  Created by Ryan Migallos on 10/2/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CourseContainerViewController.h"

@interface CourseContainerViewController ()
@property (nonatomic, strong) UIViewController *containerView;

@property (assign, nonatomic) BOOL isConnected;
@property (assign, nonatomic) BOOL alertIsPresented;
@property (nonatomic, strong) UIAlertController *internetAlert;

@end

@implementation CourseContainerViewController

- (void)viewDidLoad {

    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupChildViewController];
    [self layoutNotifications];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
    
    self.internetAlert = [UIAlertController alertControllerWithTitle:@""
                                                           message:@" "
                                                    preferredStyle:UIAlertControllerStyleAlert];
    
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.internetAlert.view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:200];
    [self.internetAlert.view addConstraint:height];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30, 130, 200, 40)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = NSLocalizedString(@"No internet connection", nil);
    label.numberOfLines = 0;
    [self.internetAlert.view addSubview:label];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(100, 40, 70, 70)];
    UIImage *light = [UIImage imageNamed:@"no_internet_dark_72.png"];
    UIImage *dark = [UIImage imageNamed:@"no_internet_light_72.png"];
    imageView.animationImages = @[light, dark];
    imageView.animationDuration = 1.0f;
    [imageView startAnimating];
    [self.internetAlert.view addSubview:imageView];
    
    self.alertIsPresented = NO;
}

- (void) viewWillDisappear:(BOOL)animated {
    
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        dispatch_queue_t queue = dispatch_queue_create("com.pearson.course.ACTIVITY",DISPATCH_QUEUE_SERIAL);
        dispatch_async(queue, ^{
            ResourceManager *rm = [AppDelegate resourceInstance];
            NSString *details = @"Left the Course module in Apple iPad";
            [rm requestLogActivityWithModuleType:@"6" details:details];
        });
    }
    
    [self removeObservers];
    
    [super viewWillDisappear:animated];
}

- (void)setupChildViewController {

    NSString *storyboardName = @"Coursev2";
//    NSString *storyboardName = @"CourseV2JA";
//    NSString *storyboardName = @"CourseV2CB";
//    NSString *storyboardName = @"CSMStoryboard";
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    self.containerView = [sb instantiateInitialViewController];
    [self initiateCustomLayoutFor:self.containerView];
    self.containerView.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.containerView.view.autoresizesSubviews = YES;
    
    [self addChildViewController:self.containerView];
    [self.view addSubview:self.containerView.view];
    [self.containerView didMoveToParentViewController:self];
    [self.view sendSubviewToBack:self.containerView.view];
}

- (void)layoutNotifications {
    VS_NCADD(kNotificationProfileHeight, @selector(resizeContainerView:))
    VS_NCADD(kVSmartConnectivityNotif, @selector(connectivityAction:) )
}

- (void)removeObservers {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:kVSmartConnectivityNotif object:nil];
    [nc removeObserver:self name:kNotificationProfileHeight object:nil];
}

- (NSString *) dashboardName {
    NSString *moduleName = NSLocalizedString(@"Courses", nil);
    return moduleName;
}

- (void)connectivityAction:(NSNotification *)notification {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSString *status = [NSString stringWithFormat:@"%@", [notification object] ];
    NSLog(@"status : %@", status);
    __weak typeof(self) wo = self;
    
    if ([status isEqualToString:@"1"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.isConnected = YES;
            if (wo.alertIsPresented) {
                wo.alertIsPresented = NO;
                [wo dismissViewControllerAnimated:YES completion:nil];
            }
        });
    }
    
    if ([status isEqualToString:@"0"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.isConnected = NO;
            if (wo.alertIsPresented == NO) {
                [wo dismissViewControllerAnimated:YES completion:nil];
                [self presentViewController:wo.internetAlert animated:YES completion:nil];
                wo.alertIsPresented = YES;
            }
        });
    }
}

#pragma mark - Post Notification Events
- (void)resizeContainerView:(NSNotification *)notification {
    
    VLog(@"Received: kNotificationProfileHeight");
    [UIView animateWithDuration:0.45 animations:^{
        [self initiateCustomLayoutFor:self.containerView];
        [self.containerView.view setNeedsLayout];
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            [super adjustProfileHeight];
        }
        
        [super showOrHideMiniAvatar];
    }];
}

- (void)initiateCustomLayoutFor:(UIViewController *)viewcontroller {
    
    float profileY = [super profileHeight];
    float headerDecrement = ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    float gridHeight = self.view.frame.size.height - headerDecrement;
    float gridY = [super headerSize].size.height + profileY;
    CGRect customFrame = CGRectMake(0, gridY, self.view.frame.size.width, gridHeight);
    
    [viewcontroller.view setFrame:customFrame];
}

@end