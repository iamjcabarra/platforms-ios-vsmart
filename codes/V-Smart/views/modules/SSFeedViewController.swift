//
//  SSFeedViewController.swift
//  V-Smart
//
//  Created by Ryan Migallos on 28/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

private enum CELL_ID: String {
    case testingCellID = "message_cell_identifier"
    case textCellID = "SS_TEXT_CEL_ID"
    case stickerCellID = "SS_STICKER_CELL_ID"
    case linkCellID = "SS_LINK_CELL_ID"
}

class SSFeedViewController: UIViewController, NSFetchedResultsControllerDelegate, Dimmable, GenericMenuDelegate, UIPopoverPresentationControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var dimView: UIView = UIView()
    
    @IBOutlet weak var drawerButton: UIButton!
    @IBOutlet fileprivate var groupLabel: UILabel!
    @IBOutlet fileprivate var table: UITableView!
    @IBOutlet fileprivate var editButton: UIButton!
    @IBOutlet fileprivate weak var createMessageButton: UIButton!

    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyLabel: UILabel!
    
    
    fileprivate let notification = NotificationCenter.default
    fileprivate let segueIdenfifier = "SEGUE_EDIT_GROUP"
    
    fileprivate var isAscending = false
    fileprivate var groupActionType: SSMGroupActionType!
        
    @IBAction func editButtonAction(_ button:UIButton!) {
            groupActionType = .update
            ssdm.save(object: "1" as AnyObject!, forKey: "SS_GROUP_IS_EDIT_MODE")
                self.performSegue(withIdentifier: segueIdenfifier, sender: self)
            }
    
    let refreshControl = UIRefreshControl()
    
    // MARK: - Singleton Method
    
    fileprivate lazy var ssdm: SocialStreamDataManager = {
        let dataManager = SocialStreamDataManager.sharedInstance
        return dataManager
    }()

    var menuViewController : GenericMenuTableView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.table.estimatedRowHeight = 310
        self.table.rowHeight = UITableViewAutomaticDimension

        self.table.register(UITableViewCell.self, forCellReuseIdentifier: CELL_ID.testingCellID.rawValue)
        
        let textNibName = UINib(nibName: "SSTextCell", bundle:nil)
        self.table.register(textNibName, forCellReuseIdentifier: CELL_ID.textCellID.rawValue)
        
        let stickerNibName = UINib(nibName: "SSStickerCell", bundle:nil)
        self.table.register(stickerNibName, forCellReuseIdentifier: CELL_ID.stickerCellID.rawValue)
        
        let linkNibName = UINib(nibName: "SSLinkCell", bundle:nil)
        self.table.register(linkNibName, forCellReuseIdentifier: CELL_ID.linkCellID.rawValue)
        
        self.refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to Refresh", comment: ""))
        self.refreshControl.addTarget(self, action: #selector(self.refreshContolAction), for: UIControlEvents.valueChanged)
        table.addSubview(refreshControl)
        
        self.createMessageButton.addTarget(self, action: #selector(self.createMessageButtonAction(_:)), for: .touchUpInside)
        self.drawerButton.addTarget(self, action: #selector(self.drawerButtonAction(_:)), for: .touchUpInside)
        
        self.menuViewController = GenericMenuTableView(nibName: "GenericMenuTableView", bundle: nil)
        self.menuViewController!.modalPresentationStyle = UIModalPresentationStyle.popover
        self.menuViewController?.delegate = self
    }
    
    func refreshContolAction() {
        
        let selectedSectionID = self.ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_SECTION_ID")
        let selectedGroupID = self.ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID")
        
        let sID:String! = self.ssdm.stringValue(selectedSectionID)
        let gID:String! = self.ssdm.stringValue(selectedGroupID)

        self.ssdm.requestUserMessages(forGroup: gID!, inSection: sID!, handler: { (doneBlock) in
            DispatchQueue.main.async(execute: {
                self.refreshControl.endRefreshing()
                self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_CHANGE_GROUP"), object: nil)
            })
        })
    }

    func drawerButtonAction(_ b: UIButton) {
        self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_SIDE_PANEL"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.createMessageButton.scaleAnimate(0.2)
        self.setUpNotification()
        self.updateHeader()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tearDownNotification()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Update Message Feed Header
    
    fileprivate func updateHeader() {
        let groupID = ssdm.stringValue(ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID"))
        let groupName = ssdm.stringValue(ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_NAME"))
        self.groupLabel.text = groupName
        
        let position = self.ssdm.accountUserPosition().lowercased()
        let imageName = position == "teacher" && groupID != "0" ? "Edit-100px.png" : "Vsmart-Contacts-100.png"
        self.editButton.setImage(UIImage(named: imageName), for: UIControlState())
        self.editButton.setImage(UIImage(named: imageName), for: .highlighted)
        self.editButton.setImage(UIImage(named: imageName), for: .selected)
        
        self.reloadFetchedResultsController()
    }
    
    // MARK: - Notification Center
    
    func setUpNotification() {
        let createGroupButtonAction = #selector(createGroupButtonAction(_:))
        notification.addObserver(self, selector: createGroupButtonAction, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_CREATE_GROUP"), object: nil)
        
        let changeGroupButtonAction = #selector(changeGroupButtonAction(_:))
        notification.addObserver(self, selector: changeGroupButtonAction, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_CHANGE_GROUP"), object: nil)
    }
    
    func tearDownNotification() {
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_CREATE_GROUP"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_CHANGE_GROUP"), object: nil)
    }
    
    func createGroupButtonAction(_ notification: Notification) {
        groupActionType = .create
        ssdm.save(object: "1" as AnyObject!, forKey: "SS_GROUP_IS_EDIT_MODE")
        self.performSegue(withIdentifier: segueIdenfifier, sender: self)
    }
    
    func changeGroupButtonAction(_ notification: Notification) {
        self.updateHeader()
    }
    
    func createMessageButtonAction(_ button: UIButton) {
        notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_CREATE_MESSAGE"), object: nil)
    }

    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        self.emptyView.isHidden = (sectionCount > 0)
        self.emptyLabel.text = (sectionCount > 0) ? "" : NSLocalizedString("No conversation in this group", comment: "")
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        let count = sectionData.numberOfObjects
        self.emptyView.isHidden = (count > 0)
        self.emptyLabel.text = (count > 0) ? "" : NSLocalizedString("No conversation in this group", comment: "")
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let mo = fetchedResultsController.object(at: indexPath)
        let type:String! = self.ssdm.stringValue(mo.value(forKey: "type"))
        var cellID = CELL_ID.testingCellID.rawValue
        
        if type == "message" {
            cellID = CELL_ID.textCellID.rawValue
        }
        
        if type == "sticker" {
            cellID = CELL_ID.stickerCellID.rawValue
        }
        
        if type == "link" {
            cellID = CELL_ID.linkCellID.rawValue
            
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        cell.selectionStyle = .none
        configureCell(cell, atIndexPath: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let groupID:String! = self.ssdm.stringValue(self.ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID"))
        self.ssdm.updateNotificationCountForGroup(groupID!)
    }
    
    fileprivate func configureCell(_ cell:UITableViewCell, atIndexPath indexPath: IndexPath) {
        let object = fetchedResultsController.object(at: indexPath)
        
        if cell.reuseIdentifier == CELL_ID.textCellID.rawValue {
            self.configureTextCell(cell, atIndexPath: indexPath, object: object)
        }
        
        if cell.reuseIdentifier == CELL_ID.testingCellID.rawValue {
            self.configureTestingCell(cell, atIndexPath: indexPath, object: object)
        }
        
        if cell.reuseIdentifier == CELL_ID.stickerCellID.rawValue {
            self.configureStickerCell(cell, atIndexPath: indexPath, object: object)
        }
        
        if cell.reuseIdentifier == CELL_ID.linkCellID.rawValue {
            self.configureLinkCell(cell, atIndexPath: indexPath, object: object)
        }
        
    }
    
    func configureTextCell(_ cell:UITableViewCell, atIndexPath indexPath: IndexPath, object: NSManagedObject) {
        
        let avatar:String! = self.ssdm.stringValue(object.value(forKey: "avatar"))
        let first_name:String! = self.ssdm.stringValue(object.value(forKey: "first_name"))
        let last_name:String! = self.ssdm.stringValue(object.value(forKey: "last_name"))
        let is_online:String! = self.ssdm.stringValue(object.value(forKey: "is_online"))
        let device_model:String! = self.ssdm.stringValue(object.value(forKey: "device_model"))
//        let date_created:String! = self.ssdm.stringValue(object.valueForKey("date_created"))
        let date_modified_date = object.value(forKey: "date_modified_date") as! Date
        let message:String! = self.ssdm.stringValue(object.value(forKey: "message"))
        let like_count:String! = self.ssdm.stringValue(object.value(forKey: "like_count"))
        let is_liked:String! = self.ssdm.stringValue(object.value(forKey: "is_liked"))
        let comment_count:String! = self.ssdm.stringValue(object.value(forKey: "comment_count"))
        
        let txtCell = cell as! SSTextCell
        
        let emoticon_id:String! = self.ssdm.stringValue(object.value(forKey: "emoticon_id"))
        let icon_url : String! = self.ssdm.stringValue(object.value(forKey: "icon_url"))
        let icon_text: String! = self.ssdm.stringValue(object.value(forKey: "icon_text"))
        
        let hasEmoticon = ((emoticon_id?.characters.count)! > 0)
        
        let isOnline = (is_online == "1")
        
        let onlineViaLoc = NSLocalizedString("online via", comment: "")
        let offlineLoc = NSLocalizedString("offline", comment: "")
        
        let statusImageStr: String! = (isOnline) ? "online_image.png" : "offline_image.png"
        let statusLabelTxt: String! = (isOnline) ? "\(onlineViaLoc) \(device_model!)" : offlineLoc
        
        let user_id:String! = self.ssdm.stringValue(object.value(forKey: "user_id"))
        let type:String! = self.ssdm.stringValue(object.value(forKey: "type"))
        let loggedInUserID = self.ssdm.accountUserID()
        
        let owned = (user_id == loggedInUserID)
        let isSticker = (type == "sticker")
        
        txtCell.menuButton.isHidden = false
        if (owned == false && isSticker == true) {
            txtCell.menuButton.isHidden = true
        }
        
        txtCell.cellBackgroundView.customShadow(3)
        
        txtCell.nameLbl.text = "\(first_name!) \(last_name!)"
        txtCell.statusImage.image = UIImage(named: statusImageStr)
        txtCell.statusLabel.text = statusLabelTxt
        txtCell.timeLabel.text = date_modified_date.elapsedTime
        
        txtCell.feelingView.isHidden = (hasEmoticon) ? false : true
        txtCell.feelingLabel.text = (hasEmoticon) ? icon_text : ""
        txtCell.isFeelingLabel.text = (hasEmoticon) ? NSLocalizedString("- is feeling", comment: "") : ""
        
        txtCell.messageLabel.text = message
        
        txtCell.noLikesView.isHidden = (like_count == "0")
        txtCell.noCommentsView.isHidden = (comment_count == "0")
        
        txtCell.noLikesLabel.text = "(\(like_count!))"
        txtCell.noCommentsLabel.text = "(\((comment_count!)))"
        
        txtCell.likeButton.isSelected = (is_liked == "1")
        
        txtCell.likeButton.isEnabled = true
        
        txtCell.tag = (indexPath as NSIndexPath).row
        let url = URL(string: avatar!)!
        txtCell.userImage.sd_setImage(with: url)
//        self.getDataFromUrl(url) { (data, response, error) in
//            guard let data = data where error == nil else { return }
//            if txtCell.tag == indexPath.row {
//                dispatch_async(dispatch_get_main_queue(), {
//                    txtCell.userImage.image = UIImage(data: data)
//                })
//            }
//        }
        
        let iconURL = URL(string: icon_url!)!
        txtCell.isFeelingImage.sd_setImage(with: iconURL)
//        self.getDataFromUrl(iconURL) { (data, response, error) in
//            guard let data = data where error == nil else { return }
//            if txtCell.tag == indexPath.row {
//                dispatch_async(dispatch_get_main_queue(), {
//                    txtCell.isFeelingImage.image = UIImage(data: data)
//                })
//            }
//        }
        let numberOfLikeStr = (like_count == "1") ? NSLocalizedString("Like", comment: "") : NSLocalizedString("Likes", comment: "")
        txtCell.noLikesButton.setTitle(numberOfLikeStr, for: UIControlState())
        txtCell.noLikesButton.setTitle(numberOfLikeStr, for: .highlighted)
        
        let numberOfCommentStr = (comment_count == "1") ? NSLocalizedString("Comment", comment: "") : NSLocalizedString("Comments", comment: "")
        txtCell.noCommentsButton.setTitle(numberOfCommentStr, for: UIControlState())
        txtCell.noCommentsButton.setTitle(numberOfCommentStr, for: .highlighted)
        
        txtCell.menuButton.addTarget(self, action: #selector(self.menuButtonAction(_:)), for: .touchUpInside)
        txtCell.likeButton.addTarget(self, action: #selector(self.likeButtonAction(_:)), for: .touchUpInside)
        txtCell.noLikesButton.addTarget(self, action: #selector(self.showLikeButtonAction(_:)), for: .touchUpInside)
        
        txtCell.commentButton.addTarget(self, action: #selector(self.commentButtonAction(_:)), for: .touchUpInside)
        txtCell.noCommentsButton.addTarget(self, action: #selector(self.commentButtonAction(_:)), for: .touchUpInside)
         
//        txtCell.messageLabelButton.addTarget(self, action: #selector(self.viewMessageButtonAction(_:)), forControlEvents: .TouchUpInside)
    }
    
    func configureStickerCell(_ cell:UITableViewCell, atIndexPath indexPath: IndexPath, object: NSManagedObject) {
        
        let avatar:String! = self.ssdm.stringValue(object.value(forKey: "avatar"))
        let first_name:String! = self.ssdm.stringValue(object.value(forKey: "first_name"))
        let last_name:String! = self.ssdm.stringValue(object.value(forKey: "last_name"))
        let is_online:String! = self.ssdm.stringValue(object.value(forKey: "is_online"))
        let device_model:String! = self.ssdm.stringValue(object.value(forKey: "device_model"))
        //        let date_created:String!:String! = self.ssdm.stringValue(object.valueForKey("date_created"))
        let date_modified_date = object.value(forKey: "date_modified_date") as! Date
        let message:String! = self.ssdm.stringValue(object.value(forKey: "message"))
        let like_count:String! = self.ssdm.stringValue(object.value(forKey: "like_count"))
        let is_liked:String! = self.ssdm.stringValue(object.value(forKey: "is_liked"))
        let comment_count:String! = self.ssdm.stringValue(object.value(forKey: "comment_count"))
        
        let stickerCell = cell as! SSStickerCell
        
        let emoticon_id:String! = self.ssdm.stringValue(object.value(forKey: "emoticon_id"))
        let icon_url:String! = self.ssdm.stringValue(object.value(forKey: "icon_url"))
        let icon_text:String! = self.ssdm.stringValue(object.value(forKey: "icon_text"))
        
        let hasEmoticon = ((emoticon_id?.characters.count)! > 0)
        
        let isOnline = (is_online == "1")
        
        let onlineViaLoc = NSLocalizedString("online via", comment: "")
        let offlineLoc = NSLocalizedString("offline", comment: "")
        
        let statusImageStr: String! = (isOnline) ? "online_image.png" : "offline_image.png"
        let statusLabelTxt: String! = (isOnline) ? "\(onlineViaLoc) \(device_model!)" : offlineLoc
        
        let user_id:String! = self.ssdm.stringValue(object.value(forKey: "user_id"))
        let type:String! = self.ssdm.stringValue(object.value(forKey: "type"))
        let loggedInUserID = self.ssdm.accountUserID()
        
        let owned = (user_id == loggedInUserID)
        let isSticker = (type == "sticker")
        
        
        stickerCell.menuButton.isHidden = false
        if (owned == false && isSticker == true) {
            stickerCell.menuButton.isHidden = true
        }
        
        stickerCell.cellBackgroundView.customShadow(3)
        
        stickerCell.nameLbl.text = "\(first_name!) \(last_name!)"
        stickerCell.statusImage.image = UIImage(named: statusImageStr)
        stickerCell.statusLabel.text = statusLabelTxt
        stickerCell.timeLabel.text = date_modified_date.elapsedTime
        
        stickerCell.feelingView.isHidden = (hasEmoticon) ? false : true
        stickerCell.feelingLabel.text = (hasEmoticon) ? icon_text : ""
        stickerCell.isFeelingLabel.text = (hasEmoticon) ? NSLocalizedString("- is feeling", comment: "") : ""
        
        stickerCell.noLikesView.isHidden = (like_count == "0")
        stickerCell.noCommentsView.isHidden = (comment_count == "0")
        
        stickerCell.noLikesLabel.text = "(\(like_count!))"
        stickerCell.noCommentsLabel.text = "(\((comment_count!)))"
        
        stickerCell.likeButton.isSelected = (is_liked == "1")
        
        stickerCell.likeButton.isEnabled = true
        
        stickerCell.tag = (indexPath as NSIndexPath).row
        let url = URL(string: avatar!)!
        stickerCell.userImage.sd_setImage(with: url)
//        self.getDataFromUrl(url) { (data, response, error) in
//            guard let data = data where error == nil else { return }
//            if stickerCell.tag == indexPath.row {
//                dispatch_async(dispatch_get_main_queue(), {
//                    stickerCell.userImage.image = UIImage(data: data)
//                })
//            }
//        }
        
        let iconURL = URL(string: icon_url!)!
        stickerCell.isFeelingImage.sd_setImage(with: iconURL)
//        self.getDataFromUrl(iconURL) { (data, response, error) in
//            guard let data = data where error == nil else { return }
//            if stickerCell.tag == indexPath.row {
//                dispatch_async(dispatch_get_main_queue(), {
//                    stickerCell.isFeelingImage.image = UIImage(data: data)
//                })
//            }
//        }
        
        let stickerURL = URL(string: message!)!
        stickerCell.stickerImage.sd_setImage(with: stickerURL)
//        self.getDataFromUrl(stickerURL) { (data, response, error) in
//            guard let data = data where error == nil else { return }
//            if stickerCell.tag == indexPath.row {
//                dispatch_async(dispatch_get_main_queue(), {
//                    stickerCell.stickerImage.image = UIImage(data: data)
//                })
//            }
//        }
        let numberOfLikeStr = (like_count == "1") ? NSLocalizedString("Like", comment: "") : NSLocalizedString("Likes", comment: "")
        stickerCell.noLikesButton.setTitle(numberOfLikeStr, for: UIControlState())
        stickerCell.noLikesButton.setTitle(numberOfLikeStr, for: .highlighted)
        
        let numberOfCommentStr = (comment_count == "1") ? NSLocalizedString("Comment", comment: "") : NSLocalizedString("Comments", comment: "")
        stickerCell.noCommentsButton.setTitle(numberOfCommentStr, for: UIControlState())
        stickerCell.noCommentsButton.setTitle(numberOfCommentStr, for: .highlighted)
        
        stickerCell.menuButton.addTarget(self, action: #selector(self.menuButtonAction(_:)), for: .touchUpInside)
        stickerCell.likeButton.addTarget(self, action: #selector(self.likeButtonAction(_:)), for: .touchUpInside)
        stickerCell.noLikesButton.addTarget(self, action: #selector(self.showLikeButtonAction(_:)), for: .touchUpInside)
        
        stickerCell.commentButton.addTarget(self, action: #selector(self.commentButtonAction(_:)), for: .touchUpInside)
        stickerCell.noCommentsButton.addTarget(self, action: #selector(self.commentButtonAction(_:)), for: .touchUpInside)
    }
        
    func configureLinkCell(_ cell:UITableViewCell, atIndexPath indexPath: IndexPath, object: NSManagedObject) {
        
        let avatar:String! = self.ssdm.stringValue(object.value(forKey: "avatar"))
        let first_name:String! = self.ssdm.stringValue(object.value(forKey: "first_name"))
        let last_name:String! = self.ssdm.stringValue(object.value(forKey: "last_name"))
        let is_online:String! = self.ssdm.stringValue(object.value(forKey: "is_online"))
        let device_model:String! = self.ssdm.stringValue(object.value(forKey: "device_model"))
        //        let date_created:String! = self.ssdm.stringValue(object.valueForKey("date_created"))
        let date_modified_date = object.value(forKey: "date_modified_date") as! Date
        let message:String! = self.ssdm.stringValue(object.value(forKey: "message"))
        let like_count:String! = self.ssdm.stringValue(object.value(forKey: "like_count"))
        let is_liked:String! = self.ssdm.stringValue(object.value(forKey: "is_liked"))
        let comment_count:String! = self.ssdm.stringValue(object.value(forKey: "comment_count"))
        
        let linkCell = cell as! SSLinkCell
         
        let emoticon_id:String! = self.ssdm.stringValue(object.value(forKey: "emoticon_id"))
        let icon_url:String! = self.ssdm.stringValue(object.value(forKey: "icon_url"))
        let icon_text:String! = self.ssdm.stringValue(object.value(forKey: "icon_text"))
        
        let hasEmoticon = ((emoticon_id?.characters.count)! > 0)
        
        let isOnline = (is_online == "1")
        
        let onlineViaLoc = NSLocalizedString("online via", comment: "")
        let offlineLoc = NSLocalizedString("offline", comment: "")
        
        let statusImageStr: String! = (isOnline) ? "online_image.png" : "offline_image.png"
        let statusLabelTxt: String! = (isOnline) ? "\(onlineViaLoc) \(device_model!)" : offlineLoc
        
        let user_id:String! = self.ssdm.stringValue(object.value(forKey: "user_id"))
        let type:String! = self.ssdm.stringValue(object.value(forKey: "type"))
        let loggedInUserID = self.ssdm.accountUserID()
        
        let owned = (user_id == loggedInUserID)
        let isSticker = (type == "sticker")
        
        linkCell.menuButton.isHidden = false
        if (owned == false && isSticker == true) {
            linkCell.menuButton.isHidden = true
        }
        
        linkCell.cellBackgroundView.customShadow(3)
        
        linkCell.nameLbl.text = "\(first_name!) \(last_name!)"
        linkCell.statusImage.image = UIImage(named: statusImageStr)
        linkCell.statusLabel.text = statusLabelTxt
        linkCell.timeLabel.text = date_modified_date.elapsedTime
        
        linkCell.messageLabel.text = message
        
        linkCell.feelingView.isHidden = (hasEmoticon) ? false : true
        linkCell.feelingLabel.text = (hasEmoticon) ? icon_text : ""
        linkCell.isFeelingLabel.text = (hasEmoticon) ? NSLocalizedString("- is feeling", comment: "") : ""
        
        linkCell.noLikesView.isHidden = (like_count == "0")
        linkCell.noCommentsView.isHidden = (comment_count == "0")
        
        linkCell.noLikesLabel.text = "(\(like_count!))"
        linkCell.noCommentsLabel.text = "(\((comment_count!)))"
        
        linkCell.likeButton.isSelected = (is_liked == "1")
        
        linkCell.likeButton.isEnabled = true
        
        linkCell.tag = (indexPath as NSIndexPath).row
        let avatarUrl = URL(string: avatar!)!
        linkCell.userImage.sd_setImage(with: avatarUrl)
//        self.getDataFromUrl(avatarUrl) { (data, response, error) in
//            guard let data = data where error == nil else { return }
//            if linkCell.tag == indexPath.row {
//                dispatch_async(dispatch_get_main_queue(), {
//                    linkCell.userImage.image = UIImage(data: data)
//                })
//            }
//        }
        
        let iconURL = URL(string: icon_url!)!
        linkCell.isFeelingImage.sd_setImage(with: iconURL)
//        self.getDataFromUrl(iconURL) { (data, response, error) in
//            guard let data = data where error == nil else { return }
//            if linkCell.tag == indexPath.row {
//                dispatch_async(dispatch_get_main_queue(), {
//                    linkCell.isFeelingImage.image = UIImage(data: data)
//                })
//            }
//        }
        
        let urlDatas = object.value(forKey: "url_data") as! Set<NSManagedObject>
        let arrayUrlDatas = Array(urlDatas)
        let urlDataObject = arrayUrlDatas.last
         
         
        let thumb_img:String! = self.ssdm.stringValue(urlDataObject?.value(forKey: "thumb_img"))
        let title:String! = self.ssdm.stringValue(urlDataObject?.value(forKey: "title"))
        let description:String! = self.ssdm.stringValue(urlDataObject?.value(forKey: "desc"))
        
        linkCell.linkTitle.text = title
        linkCell.linkDescription.text = description
        
        let stickerURL = URL(string: thumb_img!)!
        linkCell.stickerImage.sd_setImage(with: stickerURL)
//        self.getDataFromUrl(stickerURL) { (data, response, error) in
//            guard let data = data where error == nil else { return }
//            if linkCell.tag == indexPath.row {
//                dispatch_async(dispatch_get_main_queue(), {
//                    linkCell.stickerImage.image = UIImage(data: data)
//                })
//            }
//        }
        
        let numberOfLikeStr = (like_count == "1") ? NSLocalizedString("Like", comment: "") : NSLocalizedString("Likes", comment: "")
        linkCell.noLikesButton.setTitle(numberOfLikeStr, for: UIControlState())
        linkCell.noLikesButton.setTitle(numberOfLikeStr, for: .highlighted)
        
        let numberOfCommentStr = (comment_count == "1") ? NSLocalizedString("Comment", comment: "") : NSLocalizedString("Comments", comment: "")
        linkCell.noCommentsButton.setTitle(numberOfCommentStr, for: UIControlState())
        linkCell.noCommentsButton.setTitle(numberOfCommentStr, for: .highlighted)
        
        linkCell.menuButton.addTarget(self, action: #selector(self.menuButtonAction(_:)), for: .touchUpInside)
        linkCell.likeButton.addTarget(self, action: #selector(self.likeButtonAction(_:)), for: .touchUpInside)
        linkCell.noLikesButton.addTarget(self, action: #selector(self.showLikeButtonAction(_:)), for: .touchUpInside)
        
        linkCell.commentButton.addTarget(self, action: #selector(self.commentButtonAction(_:)), for: .touchUpInside)
        linkCell.noCommentsButton.addTarget(self, action: #selector(self.commentButtonAction(_:)), for: .touchUpInside)
        
        linkCell.linkButton.addTarget(self, action: #selector(self.linkButtonAction(_:)), for: .touchUpInside)
    }
    
    func linkButtonAction(_ b: UIButton) {
        let buttonPosition = b.convert(CGPoint.zero, to: self.table)
        //let indexPath = self.table.indexPathForRow(at: buttonPosition)
        let indexPath = self.table.indexPathForRow(at: CGPoint(x: 0, y: buttonPosition.y))
        let object = self.fetchedResultsController.object(at: indexPath!)
        
        let urlDatas = object.value(forKey: "url_data") as! Set<NSManagedObject>
        let arrayUrlDatas = Array(urlDatas)
        let urlDataObject = arrayUrlDatas.last
        
        let urlStr:String! = self.ssdm.stringValue(urlDataObject?.value(forKey: "url"))
        
        let URL = Foundation.URL(string: urlStr!)
        
        guard URL != nil else {
            displayAlert(withMessage: "Invalid link")
            return
        }
        
        UIApplication.shared.openURL(URL!)
        
    }
 
    func likeButtonAction(_ b: UIButton) {
//        b.enabled = false
        let buttonPosition = b.convert(CGPoint.zero, to: self.table)
        let indexPath = self.table.indexPathForRow(at: buttonPosition)
        let object = self.fetchedResultsController.object(at: indexPath!)
        
        // isLike == true, WILL PERFORM LIKE
        let isLike = (b.isSelected) ? false : true
        
        //    NSMutableDictionary *d = [@{@"avatar":avatar, MY AVATAR
        //    @"username":username, // MY USER NAME
        //    @"user_id":user_id, // MY USER ID
        //    @"content_id":message_id,
        //    @"is_message":@1,
        //    @"is_deleted":@0} mutableCopy];
        
        let message_id:String! = self.ssdm.stringValue(object.value(forKey: "id"))
        
        let data: [String:AnyObject] = ["content_id":message_id as AnyObject!,
                                        "is_message":1 as AnyObject,
                                        "is_deleted":0 as AnyObject]
        
        if isLike {
            self.ssdm.postLikeMessage(withData: data, withHandler: { (done, error) in
                
                if error == nil {
                    
                } else {
                    self.displayAlert(withMessage: error!)
                }
                
                DispatchQueue.main.async(execute: { 
                    b.isEnabled = true
                })
            })
        } else {
            self.ssdm.postUnlikeMessage(withData: data, withHandler: { (done, error) in
                if error == nil {
                    
                } else {
                    self.displayAlert(withMessage: error!)
                }
                DispatchQueue.main.async(execute: {
                    b.isEnabled = true
                })
            })
        }
    }
    
    func menuButtonAction(_ b: UIButton) {
        let buttonPosition = b.convert(CGPoint.zero, to: self.table)
        let indexPath = self.table.indexPathForRow(at: buttonPosition)
        let object = self.fetchedResultsController.object(at: indexPath!)
        
        
        var data:[String:AnyObject] = [String:AnyObject]()
        let keys = object.entity.propertiesByName.keys
        for key in keys {
            let value:String! = self.ssdm.stringValue(object.value(forKey: key))
            data[key] = value as AnyObject
        }
        
        let user_id:String! = self.ssdm.stringValue(object.value(forKey: "user_id"))
        let type:String! = self.ssdm.stringValue(object.value(forKey: "type"))
        let loggedInUserID = self.ssdm.accountUserID()
        
        let owned = (user_id == loggedInUserID)
        let isSticker = (type == "sticker")
        
        
        var menuList = [[String:String]]()
        // not mine
        if owned == false {
            if isSticker == false {
                menuList = [["title":"Add To Note",
                              "type":"add_to_note"]]
            }
        }
        
        // mine
        if owned == true {
            if isSticker {
                menuList = [["title":"Delete",
                            "type":"delete"]]
            }
            
            if isSticker == false {
                menuList = [
                    ["title":"Edit",
                        "type":"edit"],
                    
                    ["title":"Add To Note",
                        "type":"add_to_note"],
                    
                    ["title":"Delete",
                        "type":"delete"]]
            }
        }
        
        self.menuViewController?.menu = menuList
        self.menuViewController?.selectedData = data
        let popover: UIPopoverPresentationController = self.menuViewController!.popoverPresentationController!
        popover.sourceView = b
        popover.delegate = self
        present(self.menuViewController!, animated: true, completion:nil)
    }
    
    func didFinishSelecting(withType type: String, withData data: [String : AnyObject]?) {
        guard data != nil else {
            return
        }
        
        
        let user_id:String! = self.ssdm.stringValue(data!["user_id"])
        let loggedInUserID = self.ssdm.accountUserID()
        
        let owned = (user_id == loggedInUserID)
        
        if owned {
            if type == "edit" {
                let message:String! = self.ssdm.stringValue(data!["message"])
                let message_id:String! = self.ssdm.stringValue(data!["id"])
                let icon_text:String! = self.ssdm.stringValue(data!["icon_text"])
                let icon_url:String! = self.ssdm.stringValue(data!["icon_url"])
                let emoticon_id:String! = self.ssdm.stringValue(data!["emoticon_id"])
                let user_id:String! = self.ssdm.stringValue(data!["user_id"])
                
                let editData: [String:String] = ["message":message!,
                                                 "message_id":message_id!,
                                                 "icon_text":icon_text!,
                                                 "icon_url":icon_url!,
                                                 "emoticon_id":emoticon_id!,
                                                 "user_id":user_id!]
                
                self.editMessage(withData: editData)
            }
            
            if type == "delete" {
                let alertView = UIAlertController(title: "", message: NSLocalizedString("Are you sure you want to delete this post?", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
                
                let yesAction = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: UIAlertActionStyle.default) { (Alert) -> Void in
                    let message_id:String! = self.ssdm.stringValue(data!["id"])
                    self.ssdm.postRemoveMessage(withMessageID: message_id!, completion: { (done, error) in
                        if error == nil {
                            
                        } else {
                            self.displayAlert(withMessage: error!)
                        }
                    })
                }
                
                let noAction = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: UIAlertActionStyle.cancel) { (Alert) -> Void in
                }
                
                alertView.addAction(noAction)
                alertView.addAction(yesAction)
                
                DispatchQueue.main.async {
                    self.present(alertView, animated: true, completion: nil)
                }
                
            }
        }
        
        if type == "add_to_note" {
            let message:String! = self.ssdm.stringValue(data!["message"])
            self.ssdm.saveNote(message!, handler: { done in
                if done == true {
                    let alertMessage = NSLocalizedString("Added Note", comment: "")
                    let alert = UIAlertController(title: "", message: alertMessage, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler:nil)
                    alert.addAction(okAction)
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                }
            })
        }
    }
    
    func editMessage(withData data:[String:String]) {
        notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_EDIT_MESSAGE"), object: data)
    }
    
    func showLikeButtonAction(_ b: UIButton) {
        let buttonPosition = b.convert(CGPoint.zero, to: self.table)
        let indexPath = self.table.indexPathForRow(at: buttonPosition)
        let object = self.fetchedResultsController.object(at: indexPath!)
        
        let message_id:String! = self.ssdm.stringValue(object.value(forKey: "id"))
        
        self.showLike(forMessageID: message_id!)
    }
    
    func showLike(forMessageID message_id:String) {
        notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_SHOW_LIKE_MESSAGE"), object: message_id)
    }
    
    
    func commentButtonAction(_ b: UIButton) {
        let buttonPosition = b.convert(CGPoint.zero, to: self.table)
        let indexPath = self.table.indexPathForRow(at: buttonPosition)
        let object = self.fetchedResultsController.object(at: indexPath!) 
        
        let message_id:String! = self.ssdm.stringValue(object.value(forKey: "id"))
        
        self.comment(forMessageID: message_id!)
    }
    
    func comment(forMessageID message_id:String) {
        notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_COMMENT_MESSAGE"), object: message_id)
    }
    
    
    func viewMessageButtonAction(_ b: UIButton) {
        let buttonPosition = b.convert(CGPoint.zero, to: self.table)
        let indexPath = self.table.indexPathForRow(at: buttonPosition)
        let object = self.fetchedResultsController.object(at: indexPath!) 
        
        let first_name:String! = self.ssdm.stringValue(object.value(forKey: "first_name"))
        let last_name:String! = self.ssdm.stringValue(object.value(forKey: "last_name"))
        let message:String! = self.ssdm.stringValue(object.value(forKey: "message"))
        
        let data:[String:String?] = ["first_name":first_name,
                    "last_name":last_name,
                    "message":message]
        
        self.viewMessage(withData: data)
//        self.comment(forMessageID: message_id)
    }
    
    func viewMessage(withData data:[String:String?]) {
        notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_VIEW_MESSAGE"), object: data)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func displayAlert(withMessage error: String) {
        
        let message = error
        
        let alertView = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let closeAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.cancel) { (Alert) -> Void in
        }
        
        alertView.addAction(closeAction)
        
        DispatchQueue.main.async { 
            self.present(alertView, animated: true, completion: nil)
        }
    }
 
    func configureTestingCell(_ cell:UITableViewCell, atIndexPath:IndexPath, object: NSManagedObject) {
        let message = "\( object.value(forKey: "message")! )"
        cell.textLabel?.text = message
    }
    
    func getDataFromUrl(_ url:URL, completion: @escaping ((_ data: Data?, _ response: URLResponse?, _ error: Error? ) -> Void)) {
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            completion(data, response, error)
            }) .resume()
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        //Return false if you do not want the specified item to be editable.
        
        let object = self.fetchedResultsController.object(at: indexPath) 
        let user_id:String! = self.ssdm.stringValue(object.value(forKey: "user_id"))
        let loggedInUserID = self.ssdm.accountUserID()
        
        let owned = (user_id == loggedInUserID)
        
        return owned
    }
    
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
//            // Delete the row from the data source
//            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
            let alertView = UIAlertController(title: "", message: NSLocalizedString("Are you sure you want to delete this post?", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
            
            let yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (Alert) -> Void in
                let object = self.fetchedResultsController.object(at: indexPath)
                let message_id:String! = self.ssdm.stringValue(object.value(forKey: "id"))
                
                self.ssdm.postRemoveMessage(withMessageID: message_id!, completion: { (done, error) in
                    if error == nil {
                        
                    } else {
                        self.displayAlert(withMessage: error!)
                    }
                })
            }
            
            let noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) { (Alert) -> Void in
            }
            
            alertView.addAction(noAction)
            alertView.addAction(yesAction)
            
            DispatchQueue.main.async {
                self.present(alertView, animated: true, completion: nil)
            }

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     //   let mo = fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject
        
    }
    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        
//        let mo = fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject
//        let type:String! = self.ssdm.stringValue(mo.valueForKey("type"))
////        var cellID = CELL_ID.testingCellID.rawValue
//        
//        if type == "message" {
//            return 220
////            cellID = CELL_ID.textCellID.rawValue
//    }
//        
//        if type == "sticker" {
//            return 310
////            cellID = CELL_ID.stickerCellID.rawValue
//        }
//        
//        if type == "link" {
//            return 270
//    }
//    
//        
////        310
//        
//        return 200;//UITableViewAutomaticDimension
//    }

    // MARK: - Fetched Results Controller
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = ssdm.getMainContext()
        
        let ascending = self.isAscending
        
        let entity = SSMConstants.Entity.MESSAGEFEED
        
        //let fetchRequest = NSFetchRequest(entityName: entity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        fetchRequest.fetchBatchSize = 20

//        let selectedSectionID = self.ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_SECTION_ID")
//        let sID:String! = self.ssdm.stringValue(selectedSectionID)
//        let sectionIDPredicate = NSComparisonPredicate(keyPath: "section_id", withValue: sID, isExact: true);
//        fetchRequest.predicate = sectionIDPredicate
        
        let groupIDPredicate = NSComparisonPredicate(keyPath: "group_id", withValue: "NOTIFICATION", isExact: true)
        let notPredicate = NSCompoundPredicate(notPredicateWithSubpredicate: groupIDPredicate)
        fetchRequest.predicate = notPredicate
        
//        let descriptorSelector = #selector(NSString.localizedCaseInsensitiveCompare(_:))
//        let sortDescriptor = NSSortDescriptor.init(key: "date_modified_date", ascending: ascending, selector:descriptorSelector)
        let sortDescriptor = NSSortDescriptor(key: "date_modified_date", ascending: ascending)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        table.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        let tableViewController = table
        
        switch type {
        case .insert:
            tableViewController?.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableViewController?.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        let tableViewController = table
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableViewController?.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = tableViewController?.cellForRow(at: indexPath) {
                    configureCell(cell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableViewController?.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        table.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        table.reloadData()
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == segueIdenfifier {
            let gmlv = segue.destination as! SSGroupMemberListViewController
            gmlv.actionType = groupActionType
        }
    }

}
