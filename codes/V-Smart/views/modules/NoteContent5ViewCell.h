//
//  NoteContent5ViewCell.h
//  V-Smart
//
//  Created by VhaL on 2/12/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteContent5ViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIButton *buttonSave;
@property (nonatomic, assign) id delegate;

-(IBAction)buttonSaveTapped:(id)sender;
-(void)initializeCellWithDelegate:(id)parent;
@end
