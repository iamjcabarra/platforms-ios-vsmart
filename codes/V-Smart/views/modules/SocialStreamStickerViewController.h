//
//  SocialStreamStickerViewController.h
//  V-Smart
//
//  Created by Ryan Migallos on 2/4/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SocialStreamStickerDelegate <NSObject>
@optional
- (void)didFinishSelectingData:(NSDictionary *)data type:(NSString *)type;
@end



@interface SocialStreamStickerViewController : UIViewController
@property (nonatomic, strong) NSArray *menuItems;
@property (nonatomic, weak) id <SocialStreamStickerDelegate> delegate;
@end
