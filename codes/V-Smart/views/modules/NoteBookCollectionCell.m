//
//  NoteBookCollectionCell.m
//  V-Smart
//
//  Created by VhaL on 3/6/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NoteBookCollectionCell.h"
#define kMaxLimit 20

@interface NoteBookCollectionCell ()
@property (nonatomic, strong) NSArray *optionArray;
@property (nonatomic, strong) NSArray *colors;
@property (nonatomic, assign) BOOL isActionDisplayed;

@property (nonatomic, weak) IBOutlet UIButton *buttonRed;
@property (nonatomic, weak) IBOutlet UIButton *buttonBlue;
@property (nonatomic, weak) IBOutlet UIButton *buttonYellow;
@property (nonatomic, weak) IBOutlet UIButton *buttonGreen;
@property (nonatomic, weak) IBOutlet UIButton *buttonPurple;
@property (nonatomic, weak) IBOutlet UIButton *buttonWhite;

@property (nonatomic, assign) BOOL isCellInitialized;
@end

@implementation NoteBookCollectionCell
@synthesize labelTitle, labelNotesCount, imageView, optionArray, delegate, isActionDisplayed, buttonAction, addMode;

@synthesize buttonBlue, buttonGreen, buttonPurple, buttonRed, buttonWhite, buttonYellow, selectedColorId, isCellInitialized;

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)text{
    if(text.length == 0){
        if(textField.text.length != 0)
            return YES;
    }
    else if(kMaxLimit <= textField.text.length){
        return NO;
    }
    return YES;
}

-(IBAction)buttonActionTapped:(id)sender{
    NSLog(@"%@", @"buttonActionTapped");
    
    //CGFloat yAxis = 93;
    CGFloat yAxis = self.frame.size.height / 2.0f;
    
    if (isActionDisplayed){
        //yAxis = 175;
        yAxis = self.frame.size.height;
    }
    
    [UIView animateWithDuration:0.2
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, yAxis, self.tableView.frame.size.width, self.tableView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                     }];
    
    isActionDisplayed = !isActionDisplayed;
    
    if (isActionDisplayed)
        [NSTimer scheduledTimerWithTimeInterval:5
                                         target:self
                                       selector:@selector(hideActionDisplay)
                                       userInfo:nil
                                        repeats:NO];
}

-(void)hideActionDisplay{
    if (isActionDisplayed)
        [self buttonActionTapped:nil];
}

-(IBAction)acceptButtonTapped:(id)sender{
    [self hideEditControls:YES];
    
    NSString *newNotebookName = [self.actionTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([[newNotebookName lowercaseString] isEqualToString:@"general"]){
        NSString *nameCannotBeUsed = NSLocalizedString(@"This name cannot be used", nil);
        [self makeToast:nameCannotBeUsed duration:2.0f position:@"center"];
        
        return;
    }
    
    // Bug Fix
    // Handle NoteBook with the same name
    NSArray *noteBookArray = [NSMutableArray arrayWithArray:[[NoteDataManager sharedInstance] getNoteBooks:NO useMainThread:YES]];
    
    for (NSManagedObject *mo in noteBookArray) {
        NSString *noteBookName = [self stringValue:[mo valueForKey:@"noteBookName"]];
        
        // Name already in use
        if ([noteBookName isEqualToString:newNotebookName]) {
            if (addMode) {
                NSString *nameCannotBeUsed = NSLocalizedString(@"This name is already in use.", nil);
                [self makeToast:nameCannotBeUsed duration:2.0f position:@"center"];
                
                return;
            }
            else {
                if (![noteBookName isEqualToString:labelTitle.text]) {
                    NSString *nameCannotBeUsed = NSLocalizedString(@"This name is already in use.", nil);
                    [self makeToast:nameCannotBeUsed duration:2.0f position:@"center"];
                    
                    return;
                }
            }
        }
    }
        
    // Handle NoteBook without name
    // Name is empty
    if ([newNotebookName isEqualToString:@""]) {
        if (addMode) {
            NSString *nameCannotBeUsed = NSLocalizedString(@"Invalid name for notebook.", nil);
        [self makeToast:nameCannotBeUsed duration:2.0f position:@"center"];
        
        return;
    }
        else {
            newNotebookName = labelTitle.text;
        }
    }
    
    self.actionTextField.text = newNotebookName;
    [self updateNotebookColor];
    
    
    if (addMode){
        if ([delegate respondsToSelector:@selector(buttonAddNoteBookTapped:)])
        {
            [delegate performSelector:@selector(buttonAddNoteBookTapped:) withObject:self];
        }
        
        return;
    }
    
    labelTitle.text = self.actionTextField.text;
    
    if ([delegate respondsToSelector:@selector(buttonEditNoteBookTapped:)])
    {
        [delegate performSelector:@selector(buttonEditNoteBookTapped:) withObject:self];
    }
}

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

-(IBAction)cancelButtonTapped:(id)sender{
    [self hideEditControls:YES];
}

-(void)hideEditControls:(BOOL)hide{
    
    if (!isCellInitialized)
        [self initializeCell];
    
    if (hide){
        self.defaultView.hidden = NO;
        self.editView.hidden = YES;
        [self.actionTextField resignFirstResponder];
    }
    else{
        self.defaultView.hidden = YES;
        self.editView.hidden = NO;
        self.actionTextField.text = labelTitle.text;
        [self.actionTextField becomeFirstResponder];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *editOption = NSLocalizedString(@"Edit", nil);
    NSString *deleteOption = NSLocalizedString(@"Delete", nil);
    
    // Feature #884
    // jca-04272016
    // Implement duplicate feature
    NSString *duplicateOption = NSLocalizedString(@"Duplicate", nil);
    
    if (optionArray == nil){
        optionArray = @[ @{@"option_name":@"Edit", @"option_title":editOption},
                        @{@"option_name":@"Duplicate", @"option_title":duplicateOption},
                         @{@"option_name":@"Delete", @"option_title":deleteOption} ];
    }
    
    return optionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    NSDictionary *option = (NSDictionary *)optionArray[indexPath.row];
    cell.textLabel.text = option[@"option_title"];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self buttonActionTapped:nil];
    
    NSDictionary *option = (NSDictionary *)optionArray[indexPath.row];
    NSString *actionTitle = option[@"option_name"];
    
    if ([actionTitle isEqualToString:@"Edit"]){
        [self hideEditControls:NO];
        return;
    }
    
    if ([delegate respondsToSelector:@selector(cellButtonActionApplyTapped:withActionTitle:)]){
        [delegate performSelector:@selector(cellButtonActionApplyTapped:withActionTitle:) withObject:self withObject:actionTitle];
    }
}

-(void)updateNotebookColor{
    if (self.colors == nil)
        self.colors = [[NoteDataManager sharedInstance] getColors:YES useMainThread:YES];
    
    for (Color *color in self.colors) {
        if (color.colorId.intValue == selectedColorId){
            
//            imageView.backgroundColor = [[NoteDataManager sharedInstance] getUIColorFromColorObject:color];
            NSString *imageString = [NSString stringWithFormat:@"icn_notebook_%@",color.colorName];
            imageView.image = [UIImage imageNamed:imageString];
            
            break;
        }
    }
}

-(void)initializeCell{
    
    if (self.colors == nil)
        self.colors = [[NoteDataManager sharedInstance] getColors:YES useMainThread:YES];
    
    [self initializeButton:buttonRed];
    [self initializeButton:buttonBlue];
    [self initializeButton:buttonYellow];
    [self initializeButton:buttonGreen];
    [self initializeButton:buttonPurple];
    [self initializeButton:buttonWhite];
    
    [self makeButtonRound:buttonRed];
    [self makeButtonRound:buttonBlue];
    [self makeButtonRound:buttonYellow];
    [self makeButtonRound:buttonGreen];
    [self makeButtonRound:buttonPurple];
    [self makeButtonRound:buttonWhite];
    
    [self buttonColorTapped:buttonRed];
    
    isCellInitialized = YES;
}

-(void)makeButtonRound:(UIButton*)button{
    button.layer.cornerRadius = button.frame.size.height/2;
    button.clipsToBounds = YES;
}

-(void)initializeButton:(UIButton*)buttonOuter{
    UIButton *buttonInner = [[UIButton alloc] initWithFrame:
                             CGRectMake(5, 5, buttonOuter.frame.size.width-10, buttonOuter.frame.size.height-10)];
    buttonInner.backgroundColor = buttonOuter.backgroundColor;
    buttonInner.layer.cornerRadius = buttonInner.frame.size.width/2;
    buttonInner.userInteractionEnabled = NO;
    
    buttonOuter.backgroundColor = [UIColor clearColor];
    [buttonOuter addSubview:buttonInner];
    
    buttonOuter.layer.borderWidth = 0;
    buttonOuter.layer.borderColor = buttonInner.backgroundColor.CGColor;
}

-(IBAction)buttonColorTapped:(id)sender{
    VLog(@"buttonColorTapped");
    
    buttonRed.layer.borderWidth = 0;
    buttonBlue.layer.borderWidth = 0;
    buttonYellow.layer.borderWidth = 0;
    buttonGreen.layer.borderWidth = 0;
    buttonPurple.layer.borderWidth = 0;
    buttonWhite.layer.borderWidth = 0;
    
    UIButton *button = (UIButton*)sender;
    button.layer.borderWidth = 2;
    
    if (buttonRed.layer.borderWidth == 2)
        selectedColorId = 0;
    
    if (buttonBlue.layer.borderWidth == 2)
        selectedColorId = 1;
    
    if (buttonYellow.layer.borderWidth == 2)
        selectedColorId = 2;
    
    if (buttonGreen.layer.borderWidth == 2)
        selectedColorId = 3;
    
    if (buttonPurple.layer.borderWidth == 2)
        selectedColorId = 4;
    
    
    if (buttonWhite.layer.borderWidth == 2)
        selectedColorId = 5;
    
//    if ([delegate respondsToSelector:@selector(buttonColorTapped:)])
//    {
//        [delegate performSelector:@selector(buttonColorTapped:) withObject:sender];
//    }
}

@end
