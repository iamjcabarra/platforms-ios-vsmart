//
//  TextBookController.m
//  V-Smart
//
//  Created by Ryan Migallos on 6/16/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "TextBookController.h"
#import "TextBookCell.h"
#import "TextBookCollectionCell.h"
#import "TextBookLayout.h"
#import "LazyPDFKit.h"
#import "HUD.h"
#import "EPub.h"
#import "Book.h"
#import "BookInfoViewController.h"
#import "QuizMedalViewController.h"
#import "AssessmentListViewController.h"
#import "EPubViewController.h"
#import "UIImageView+WebCache.h"
#import "UICollectionView+NSFetchedResultsController.h"

@interface TextBookController () <NSFetchedResultsControllerDelegate, UIGestureRecognizerDelegate, UIDocumentInteractionControllerDelegate,
UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, LazyPDFViewControllerDelegate, BookInfoViewControllerDelegate, QuizMedalViewControllerDelegate> {
    
    MBProgressHUD *mbHUD;
    BookInfoViewController *popBookInfoViewController;
    QuizMedalViewController *popQuizMedalViewController;
    EPubViewController *detailViewController;
}

@property (nonatomic, strong) ResourceManager *rm;
@property (nonatomic, strong) NSFetchedResultsController *frc;

@property (nonatomic, strong) NSString *useridPATH;
@property (nonatomic, strong) NSMutableArray *books;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) UICollectionView *gridView;
@property (nonatomic, strong) AssessmentListViewController *assessmentController;
@property (nonatomic, strong) UIDocumentInteractionController *documentInteractionController;

@property (nonatomic, strong) NSBlockOperation *blockOperation;
@property (nonatomic, assign) BOOL shouldReloadCollectionView;

@end

@implementation TextBookController

static NSString *kCellIdentifier = @"cell_identifier";

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    AccountInfo *account = [Utils getArchive:kGlobalAccount];
    self.useridPATH = VS_FMT(@"%@", account.user.email);
    
    self.rm = [AppDelegate resourceInstance];//instantiate resource
    [self.rm requestVersionCheckBlock:^(BOOL status) {
        NSLog(@"VERSION CHECK...");
    }];
    
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    TapDetectingWindow *tapWindow = (TapDetectingWindow *)[[UIApplication sharedApplication].windows objectAtIndex:0];
    tapWindow.controllerThatObserves = nil;
    tapWindow.viewToObserve = nil;
    
    [self setupTextbookGrid];
    [self _refreshBooksOnShelf];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
    
    self.assessmentController = [[AssessmentListViewController alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //    [self enableHomeButton:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //    [self enableHomeButton:YES];
    [self customNotificationEnable:NO];
}

- (void)setupTextbookGrid {
    
    // CORE DATA SPECIFIC
    [self instatiateCollectionView];
    [self configureFetch];
    //    [self _loadBooks];
    [self customNotificationEnable:YES];
}

- (void)instatiateCollectionView {
    NSLog(@"Loading ...");
    
    float profileY = [super profileHeight];
    float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    float gridY = [super headerSize].size.height + profileY;
    
//    CGRect collection_frame = CGRectMake(0, gridY, self.view.frame.size.width, gridHeight + 44);
    CGRect collection_frame = CGRectMake(0, gridY, self.view.frame.size.width, gridHeight);
    self.gridView.frame = collection_frame;
    
    // INITIALIZE UICOLLECTION VIEW WHEN IT'S NILL
    if (self.gridView == nil) {
        UICollectionViewFlowLayout *collection_layout = [[UICollectionViewFlowLayout alloc] init];
        collection_layout.minimumLineSpacing = 1;
        collection_layout.minimumInteritemSpacing = 1;
        CGFloat width = ((self.view.bounds.size.width / 2) - 2);
        collection_layout.itemSize = CGSizeMake(width, 230);
        collection_layout.sectionInset = UIEdgeInsetsMake(1, 1, 1, 1);
        self.gridView = [[UICollectionView alloc] initWithFrame:collection_frame collectionViewLayout:collection_layout];
        
        //Register Custom Cell
        //        [self.gridView registerClass:[TextBookCell class] forCellWithReuseIdentifier:kCellIdentifier];
        
        //Test
        UINib *nibCell = [UINib nibWithNibName:@"TextBookCollectionCell" bundle:nil];
        [self.gridView registerNib:nibCell forCellWithReuseIdentifier:kCellIdentifier];
    }
    
    self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.gridView.autoresizesSubviews = YES;
    self.gridView.alwaysBounceVertical = YES;
    
    self.gridView.delegate = self;
    self.gridView.dataSource = self;
    
    if (self.refreshControl == nil) {
        self.refreshControl = [[UIRefreshControl alloc] init];
    }
    self.refreshControl.tintColor = [UIColor grayColor];
    SEL refreshAction = @selector(_refreshBooksOnShelf);
    [self.refreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.gridView addSubview:self.refreshControl];
    
    SEL longPress = @selector(showPopupDeleteBookInfo:);
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:longPress];
    longPressGesture.minimumPressDuration = 0.5;
    longPressGesture.delegate = self;
    [self.gridView addGestureRecognizer:longPressGesture];
    
    self.gridView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.gridView];
}

- (NSString *) dashboardName {
    NSString *moduleName = NSLocalizedString(@"Textbook", nil);
    return moduleName;
}

- (void)customNotificationEnable:(BOOL)enable {
    
    [self notification:kNotificationProfileHeight
                action:@selector(_refreshGridSize:)
                enable:enable];
    
    [self notification:kNotificationTextbookShelfSelection
                action:@selector(_displayQuizResultAction:)
                enable:enable];
    
    [self notification:@"org.vibalfoundation.vibereader.close"
                action:@selector(closeReader)
                enable:enable];
    
    [self notification:kNofiticationBookDownloadError
                action:@selector(showBookDownloadError:)
                enable:enable];
}

- (void)notification:(NSString *)name action:(SEL)action enable:(BOOL)enable {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    if (enable == YES) {
        [nc addObserver:self selector:action name:name object:nil];
    }
    if (enable == NO) {
        [nc removeObserver:self name:name object:nil];
    }
}

#pragma mark - Notification Receivers Action

- (void)showBookDownloadError:(NSNotification *)notification {
    
    NSString *message = [NSString stringWithFormat:@"%@", [notification object] ];
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wo.view makeToast:message duration:2.0f position:@"center"];
    });
}

- (void)animateClosingReader {
    CGRect theFrame = detailViewController.view.frame;
    theFrame.origin = CGPointMake(self.view.frame.size.width, 0);
    [UIView animateWithDuration:0.5 animations:^{
        detailViewController.view.frame = theFrame;
        detailViewController.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self unloadReader];
    }];
}

- (void)unloadReader {
    
    if (detailViewController != nil) {
        [detailViewController.view removeFromSuperview];
        detailViewController = nil;
        
        [self.gridView reloadData];
        [self refreshMedals];
        [self hideToolbar:NO];
    }
}

- (void)closeReader {
    [Utils saveReaderState:NO];
    [self animateClosingReader];
}

- (void)_refreshBooksOnShelf {
    NSLog(@"Refreshing books on shelf");
    
    [self enableHomeButton:NO];
    __weak TextBookController *wo = self;
    
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Loading", nil)];
    [SVProgressHUD showWithStatus:indicatorString maskType:SVProgressHUDMaskTypeClear];
    
    [self.rm requestBookList:^(NSArray *list) {
        if (list.count > 0) {
            [wo updateResourceWithItems:list];
        }
        
        if (list.count == 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self enableHomeButton:YES];
                [SVProgressHUD dismiss];
                [wo.refreshControl endRefreshing];
            });
        }
        
    }];
}

- (void)_displayQuizResultAction:(NSNotification *)notification {
    Book *item = (Book *)[notification object];
    NSString *bookName = VS_FMT(@"%@", item.bookId);
    NSArray *items = [VSmartHelpers getBookExerciseResults:bookName];
    //NSLog(@"%@: BookResult: %@", bookName, items);
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateCreated" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [items sortedArrayUsingDescriptors:sortDescriptors];
    popQuizMedalViewController = [[QuizMedalViewController alloc] initWithNibName:@"QuizMedalViewController" bundle:nil];
    popQuizMedalViewController.view.frame = popQuizMedalViewController.view.frame;
    popQuizMedalViewController.delegate = self;
    popQuizMedalViewController.items = sortedArray;
    [self presentPopupViewController:popQuizMedalViewController animationType:PopupViewAnimationSlideBottomTop];
}

- (void)_refreshGridSize:(NSNotification *)notification {
    NSLog(@"Received: kNotificationProfileHeight");
    [UIView animateWithDuration:0.45 animations:^{
        float profileY = [super profileHeight];
        float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
        float gridY = [super headerSize].size.height + profileY;
        [self.gridView setFrame:CGRectMake(0, gridY, self.view.frame.size.width, gridHeight + 44)];
        [self.gridView setNeedsLayout];
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            [super adjustProfileHeight];
        }
        [super showOrHideMiniAvatar];
    } completion:^(BOOL finished) {
        NSLog(@"Received: Finished");
        [self.gridView setContentSize:CGSizeMake(self.gridView.contentSize.width, self.gridView.frame.size.height + 10)];
    }];
}

- (void)_refreshGrid:(NSNumber *)rowIndex {
    
    NSInteger row = rowIndex.integerValue;
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
    for (NSIndexPath *i in [self.gridView indexPathsForVisibleItems]) {
        if (i == indexPath) {
            [self.gridView reloadItemsAtIndexPaths:@[indexPath]];
            break;
        }
    }
}

- (void) _loadBooks {
    
    __weak typeof(self) wo = self;
    [self enableHomeButton:NO];
    
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Loading", nil)];
    [SVProgressHUD showWithStatus:indicatorString maskType:SVProgressHUDMaskTypeClear];
    
    [self.rm requestBookList:^(NSArray *list) {
        
        NSLog(@"DONE LOADING BOOKS...");
        
        if (list.count > 0) {
            [wo updateResourceWithItems:list];
        }
        
        if (list.count == 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self enableHomeButton:YES];
                [SVProgressHUD dismiss];
            });
        }
    }];
}

- (void)updateResourceWithItems:(NSArray *)items {
    
    NSArray *importedBooks = [VSmartHelpers getImportedBooks];
    NSLog(@"import book count : %lu", (unsigned long)importedBooks.count);
    
    if ([importedBooks count] > 0) {
        for (Book *book in importedBooks) {
            VSmartDownloadStatus downloadStatus = VSmartDownloadImported;
            NSString *bookIdentifier = [NSString stringWithFormat:@"%@",book.bookId];
            
            NSDictionary *item = @{@"download_active": @NO,
                                   @"book_id": bookIdentifier,
                                   @"book": book,
                                   @"progress": [NSNumber numberWithFloat:0],
                                   @"download_status": [NSNumber numberWithUnsignedInteger:downloadStatus],
                                   @"user_id" : self.useridPATH};
            [self.rm updateResourceWithObject:item import:YES];
        }
    }
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [SVProgressHUD dismiss];
        [wo enableHomeButton:YES];
        [wo.refreshControl endRefreshing];
    });
}

- (void)_fetchDownloadedBooks {
    NSArray *downloadedBooks = [VSmartHelpers getDownloadedBooks];
    NSLog(@"Downloaded Books: %@", downloadedBooks);
}

- (NSDictionary *)_getProgressCompletedQuiz:(Book *)book {
    NSString *bookName = VS_FMT(@"%@", book.bookId);
    NSArray *items = [VSmartHelpers getBookExerciseResults:bookName];
    NSUInteger quizTaken = [items count];
    NSDictionary *quizData = [VSmartHelpers getBookExerciseData:bookName];
    NSArray *exerciseData = [quizData objectForKey:@"Exercises"];
    NSUInteger quizTotal = [exerciseData count];
    
    float quizCompleted = (float) quizTaken / (float)quizTotal;
    if (isnan(quizCompleted)){
        quizCompleted = 0;
    }
    
    NSString *completedLabel = NSLocalizedString(@"completed", nil);
    NSString *text = VS_FMT(@"%.2f%% %@ (%lu / %lu)",
                            (float)(quizCompleted * 100),
                            completedLabel,
                            (unsigned long)quizTaken,
                            (unsigned long)quizTotal);
    NSDictionary *item = @{@"progress": [NSNumber numberWithFloat:quizCompleted], @"text": text};
    return item;
}

- (NSString *)_computeAverage:(Book *) book {
    NSString *bookName = VS_FMT(@"%@", book.bookId);
    NSArray *items = [VSmartHelpers getBookExerciseResults:bookName];
    
    float quizTotal = 0;
    float quizResultTotal = 0;
    
    for (BookExerciseResult *result in items) {
        quizResultTotal += (float) result.result;
        quizTotal += (float) result.bookExercise.totalItems;
    }
    
    float averageCompleted = 0.0f;
    if(quizTotal != 0) {
        averageCompleted = (float) (quizResultTotal / quizTotal) * 50 + 50;
    }
    
    NSString *runningProgressLabel = NSLocalizedString(@"running progress", nil);
    return VS_FMT(@"%0.2f%% %@", averageCompleted, runningProgressLabel);
}

- (void)testAlert:(id)sender{
    [self.view makeToast:@"Demo"];
}

#pragma mark - Long Press UIGesture Recognizer Delegate

-(void) showPopupDeleteBookInfo: (UIGestureRecognizer *) recognizer
{
    switch ( recognizer.state )
    {
        default:
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStatePossible:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded:
            // do nothing
            break;
        case UIGestureRecognizerStateBegan:
        {
            CGPoint p = [recognizer locationInView:self.gridView];
            NSIndexPath *index = [self.gridView indexPathForItemAtPoint:p];
            NSManagedObject *item = [self.frc objectAtIndexPath:index];
            
            VSmartDownloadStatus status = (VSmartDownloadStatus)[[item valueForKey:@"download_status"] intValue];
            if (status == VSmartDownloadExtracted || status == VSmartDownloadImported) {
                Book *book = (Book *)[item valueForKey:@"book"];
                VLog(@"Delete Book: %@", book.title);
                [self showPopupBookInfo:book forIndex:index.row withBookState:kBookStateDelete];
            }
            break;
        }
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
    CGPoint location = [gestureRecognizer locationInView:self.gridView];
    int bookCount = (int)[[self.frc fetchedObjects] count];
    
    NSIndexPath *index = [self.gridView indexPathForItemAtPoint:location];
    if ( index.row < bookCount ) {
        return YES;
    }
    
    return NO;
}

#pragma mark - Delegate Actions

-(void) didCloseQuizMedalWindow:(QuizMedalViewController *)popViewController {
    //NSLog(@"closing...");
    [self dismissPopupViewControllerWithanimationType:PopupViewAnimationSlideBottomBottom];
}

-(void) actionButtonClicked:(BookInfoViewController *)popViewController withAction:(ActionType)actionType
                  withIndex:(int)index andBookId:(NSString *)bookId forBook:(Book *)book {
    
    switch (actionType) {
        case ReadAction: {
            [VSmartHelpers updateBookToCollection:bookId];//UPDATE DATE LAST READ
            
            [self dismissPopupViewControllerWithanimationType:PopupViewAnimationFade];
            
            NSIndexPath *index_path = [[self.gridView indexPathsForSelectedItems] lastObject];
            NSManagedObject *mo = [self.frc objectAtIndexPath:index_path];
            
            [self.gridView deselectItemAtIndexPath:index_path animated:YES];
            
            if ([book.mediaType isIn:kBookExtensionPdf, nil]) {
                
                NSString *pdfFileName = VS_FMT(@"%@", book.bookId);
                NSString *passphrase = [NSString stringWithFormat:@"%@", [mo valueForKey:@"password"] ];
                
                NSDictionary *info = @{@"fileName": pdfFileName,
                                       @"book_title": book.title,
                                       @"password": passphrase};
                
                [self performSelector:@selector(openPDFReader:) withObject:info afterDelay:.5];
            } else {
                /* localizable strings */
                NSString *loadingBook = NSLocalizedString(@"Loading book", nil); //checked
                [SVProgressHUD showWithStatus:loadingBook maskType:SVProgressHUDMaskTypeClear];
                
                [self performSelector:@selector(openReader:) withObject:bookId afterDelay:.5];
            }
            break;
        }
            
        case DeleteAction: {
            NSLog(@"Delete");//checked
            [self dismissPopupViewControllerWithanimationType:PopupViewAnimationFade];//checked
            
            /* localizable strings */
            NSString *removeBookMessage = NSLocalizedString(@"Removing book", nil); //checked
            [SVProgressHUD showWithStatus:removeBookMessage maskType:SVProgressHUDMaskTypeClear];//checked
            
            [self deleteBookAtIndex:index];
            
            break;
        }
            
        default: {
            
            [self dismissPopupViewControllerWithanimationType:PopupViewAnimationFade];
            
            NSIndexPath *index_path = [[self.gridView indexPathsForSelectedItems] lastObject];
            [self.gridView deselectItemAtIndexPath:index_path animated:YES];
            
            //NSLog(@"Cancel");
            break;
        }
            
    }
    
    popBookInfoViewController.delegate = nil;
}

- (void)deleteBookAtIndex:(NSInteger)index {
    
    NSManagedObject *item = [self.frc objectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    Book *book = [item valueForKey:@"book"];
    VLog(@"Delete Book: %@", book.title);
    
    VSmartDownloadStatus status = [[item valueForKey:@"download_status"] intValue];
    
    NSOperationQueue *q = [[NSOperationQueue alloc] init];
    
    if (status == VSmartDownloadImported) {
        NSBlockOperation *b1 = [NSBlockOperation blockOperationWithBlock:^{
            [VSmartHelpers removeImportedBook:VS_FMT(@"%@", book.bookId)];
        }];
        [q addOperation:b1];
    } else {
        NSBlockOperation *b2 = [NSBlockOperation blockOperationWithBlock:^{
            [VSmartHelpers removeBookToCollection:book.bookId];
        }];
        [q addOperation:b2];
    }
    
    NSBlockOperation *b3 = [NSBlockOperation blockOperationWithBlock:^{
        [self removeBookData:book];
    }];
    [q addOperation:b3];
    
    NSBlockOperation *b4 = [NSBlockOperation blockOperationWithBlock:^{
        //update the core data
        [self.rm deleteManagedObject:item];
    }];
    [q addOperation:b4];
    
    //    __weak typeof(self) wo = self;
    //    NSBlockOperation *b5 = [NSBlockOperation blockOperationWithBlock:^{
    //        // resetup grid view
    //        [wo _loadBooks];
    //    }];
    //    [q addOperation:b5];
}

#pragma mark - Private Methods
- (void)removeBookData:(Book *)book {
    [self removeBook:VS_FMT(@"%@", book.bookId)];
}

- (void)removeBook:(NSString *)title {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^{
        [self removeBookWithOperation: title];
    });
}

- (void)removeBookWithOperation:(NSString *)title {
    
    /* localizable strings */
    NSString *removeBookMessage = NSLocalizedString(@"Removing book", nil); //checked
    [SVProgressHUD showWithStatus:removeBookMessage maskType:SVProgressHUDMaskTypeClear];//checked
    [VSmartHelpers removeBook:title];
    [VSmartHelpers removeExerciseData:title];
    [VSmartHelpers removeExerciseResultData:title];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self refreshMainAfterRemovalOperation];
        [self refreshMedals];
    });
}

- (void)refreshMainAfterRemovalOperation {
    [SVProgressHUD dismiss];
    [self _loadBooks];
}

- (void)openReader:(NSString *)bookId {
    NSString *bookIdentifier = VS_FMT(@"%@", bookId );
    VLog(@"openReader: Book Title: %@", bookIdentifier);
    
    [self hideToolbar:YES];
    
    if (detailViewController != nil) {
        [detailViewController.view removeFromSuperview];
        detailViewController = nil;
    }
    
    detailViewController = [[EPubViewController alloc] init];
    
    CGRect theFrame = detailViewController.view.frame;
    theFrame.origin = CGPointMake(self.view.frame.size.width, 0);
    detailViewController.view.frame = theFrame;
    theFrame.origin = CGPointMake(0,0);
    [detailViewController.view setNeedsDisplay];
    
    [self.view addSubview:detailViewController.view];
    [Utils saveReaderState:YES];
    
    [detailViewController loadEpubBook:bookIdentifier];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animDone:finished:context:)];
    
    detailViewController.view.frame = self.view.bounds;
    detailViewController.view.alpha = 1.0;
    
    [UIView commitAnimations];
}

- (void)animDone:(NSString *)animationID finished:(BOOL)finished context:(void *)context
{
    //reader.containerView.transform = CGAffineTransformMakeRotation(-1 * M_PI / 2);
    //reader.currPage.transform = CGAffineTransformMakeRotation(M_PI / 2);
}

#pragma mark - Document Interaction Controller Delegate Methods

- (UIViewController *) documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return self;
}

- (void)openPDFReader:(NSDictionary *)bookInfo {
    
    [self openPDFdocumentWithInfo:bookInfo];
    //    [self openPDFDocument:bookInfo];
    
    //    NSString *file = [bookInfo objectForKey:@"fileName"];
    //    NSString *bookPath = [Utils appLibraryBooksDirectory];
    //    NSString *pdfFilePath = [NSString stringWithFormat:@"%@/%@/%@.pdf", bookPath, file, file];
    //    NSURL *url = [NSURL fileURLWithPath:pdfFilePath];
    //    if (url) {
    //        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
    //        [self.documentInteractionController setDelegate:self];
    //        [self.documentInteractionController setName:[bookInfo objectForKey:@"book_title"]];
    //        [self.documentInteractionController presentPreviewAnimated:YES];
    //    }
}

- (void)openPDFdocumentWithInfo:(NSDictionary *)bookInfo {
    
    NSString *file = [NSString stringWithFormat:@"%@", bookInfo[@"fileName"] ];
    NSString *password = [NSString stringWithFormat:@"%@", bookInfo[@"password"] ];
    NSString *book_title = [NSString stringWithFormat:@"%@", bookInfo[@"book_title"] ];
    NSString *bookPath = [Utils appLibraryBooksDirectory];
    NSString *pdfFilePath = [NSString stringWithFormat:@"%@/%@/%@.pdf", bookPath, file, file];
    
    LazyPDFDocument *document = [LazyPDFDocument withDocumentFilePath:pdfFilePath password:password];
    document.fileTitle = [NSString stringWithFormat:@"%@", book_title];
    
    // Must have a valid LazyPDFDocument object in order to proceed with things
    if (document != nil) {
        
        LazyPDFViewController *lazyPDFViewController = [[LazyPDFViewController alloc] initWithLazyPDFDocument:document];
        lazyPDFViewController.delegate = self; // Set the LazyPDFViewController delegate to self
        lazyPDFViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        lazyPDFViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:lazyPDFViewController animated:YES completion:NULL];
        
    } else {
        // Log an error so that we know that something went wrong
        NSLog(@"%s [LazyPDFDocument withDocumentFilePath:'%@' password:'%@'] failed.", __FUNCTION__, pdfFilePath, password);
    }
}

//CHECKED
- (void)dismissLazyPDFViewController:(LazyPDFViewController *)viewController {
    // dismiss the modal view controller
    [self.gridView reloadData];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)showPopupBookInfo:(Book *)book forIndex:(NSUInteger)index withBookState:(NSString *)bookState {
    
    popBookInfoViewController = [[BookInfoViewController alloc] initWithNibName:@"BookInfoViewController" bundle:nil];
    popBookInfoViewController.delegate = self;
    popBookInfoViewController.view.frame = popBookInfoViewController.view.frame;
    
    NSString *readLabel = NSLocalizedString(@"Read", nil); //checked
    NSString *deleteLabel = NSLocalizedString(@"Delete", nil); //checked
    
    NSString *buttonTextTitle = @"";
    
    if ([bookState isEqualToString:kBookStateRead]) {
        popBookInfoViewController.buttonStateText = [NSString stringWithFormat:@"%@", kBookStateRead];
        buttonTextTitle = readLabel;
    }
    else if ([bookState isEqualToString:kBookStateDelete]) {
        popBookInfoViewController.buttonStateText = [NSString stringWithFormat:@"%@", kBookStateDelete];
        buttonTextTitle = deleteLabel;
    }
    
    const float MAX_WIDTH = 439.0;
    const float MIN_WIDTH = 275.0;//236.0;
    
    //NSLog(@"Main View: %@", NSStringFromCGRect(popBookInfoViewController.view.frame));
    
    CGSize textSize = { popBookInfoViewController.backgroundButton.bounds.size.width, 2000.0f };
    CGRect frameTitle = CGRectMake(10, 241, 418.0, 75.0 );
    CGRect rect = CGRectMake(0, 0, 120, 180);
    
    UIImage *coverImg = [UIImage imageWithContentsOfFile:book.cover];
    
    NSString *media_type = book.mediaType;
    
    // EPUB
    if ([media_type isIn:kBookExtensionPdf, nil] == NO) {
        EPub *epub = [[EPub alloc] initWithBookname:VS_FMT(@"%@", book.bookId)];
        NSString *rootPath = epub.rootPath;
        NSString *bookPath = [Utils appLibraryBooksDirectory];

        NSString *epubCoverImagePath = [NSString stringWithFormat: @"/%@/%@", VS_FMT(@"%@", book.bookId), epub.coverImagePath];
        NSString *customPath = [NSString stringWithFormat:@"/%@/%@/%@", VS_FMT(@"%@", book.bookId), rootPath, epub.coverImagePath];
        
        NSString *coverPath = [bookPath stringByAppendingFormat:@"%@", [rootPath isEqualToString:@""] ?  epubCoverImagePath : customPath];
        coverImg = [UIImage imageWithContentsOfFile:coverPath];
    }
    
    // PDF
    if ([media_type isIn:kBookExtensionPdf, nil] == YES) {
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
        NSManagedObject *mo = [self.frc objectAtIndexPath:indexPath];
        NSData *imageData = [mo valueForKey:@"thumbnail"];
        if (imageData != nil) {
            coverImg = [UIImage imageWithData:imageData];
        }
    }
    
    UIImage *finalImage = [coverImg scaleImageToSize:rect.size];
    popBookInfoViewController.bookImage.image = finalImage;
    popBookInfoViewController.titleText.text = book.title;
    
    NSDictionary* attribs = @{NSFontAttributeName:popBookInfoViewController.titleText.font};
    CGRect rectTitle = [book.title boundingRectWithSize:textSize
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:attribs
                                                context:nil];
    CGSize titleSize = rectTitle.size;
    frameTitle.size.height = titleSize.height;
    //NSLog(@"TitleSize: %.02f", titleSize.width);
    
    float fillerSize = 30;
    if (titleSize.width > MAX_WIDTH) {
        fillerSize = 0;
    }
    if (titleSize.width < MIN_WIDTH) {
        titleSize.width = MIN_WIDTH;
    }
    
    NSString *bookId = book.bookId;
    //NSLog(@"BookId: %i and Index: %i", bookId, index);
    NSDictionary *bookInfoFromCollection = [VSmartHelpers getBookInfoFromCollection: bookId inCollection:nil];
    NSLog(@"bookInfoFromCollection: %@", bookInfoFromCollection);
    
    if (bookInfoFromCollection) {
        popBookInfoViewController.authorText.text = [VSmartHelpers getAuthors:book.authors];
        popBookInfoViewController.publisherText.text = book.publisher;
        
        id dateAdded = [bookInfoFromCollection objectForKey:kBookDateAdded];
        
        NSString *dateAddedText = @"";
        if ([dateAdded isKindOfClass:[NSString class]]) {
            NSDate *dateObject = [NSDate dateFromString:dateAdded];
            dateAddedText = [NSDate stringFromDate:dateObject withFormat:kDateInfoFormat];
        }
        
        if ([dateAdded isKindOfClass:[NSDate class]]) {
            dateAddedText = [NSDate stringFromDate:dateAdded withFormat:kDateInfoFormat];
        }
        
        popBookInfoViewController.dateAddedLabel.text = dateAddedText;// [NSString stringWithFormat:@"Date Added: %@", dateAddedText];
        //NSLog(@"DateLastRead: %@", [bookInfoFromCollection objectForKey:kBookDateLastRead]);
        popBookInfoViewController.dateLastReadLabel.text = [bookInfoFromCollection objectForKey:kBookDateLastRead];
        popBookInfoViewController.bookId = bookId;
        popBookInfoViewController.index = (int)index;
        popBookInfoViewController.book = book;
        
        [popBookInfoViewController.readOrDeleteButton setTitle:buttonTextTitle forState: UIControlStateNormal]; //normal
        [popBookInfoViewController.readOrDeleteButton setTitle:buttonTextTitle forState: UIControlStateHighlighted];//pressed
        [popBookInfoViewController initButtons];
        
        [self presentPopupViewController:popBookInfoViewController animationType:PopupViewAnimationFade];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    NSInteger count = 0;
    if ( (self.frc != nil) && ([[self.frc sections] count] > 0)) {
        count = [[self.frc sections] count];
    }
    return count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger rowCount = 0;
    if ( self.frc != nil) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.frc sections][section];
        rowCount = [sectionInfo numberOfObjects];
        
    }
    return rowCount;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [cell layoutIfNeeded];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.frc objectAtIndexPath:indexPath];
    Book *book = (Book *)[mo valueForKey:@"book"];
    
    static NSString *CellIdentifier = @"cell_identifier";
    //    TextBookCell *itemCell = (TextBookCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    TextBookCollectionCell *itemCell = (TextBookCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier
                                                                                                           forIndexPath:indexPath];
    if (book != nil) {
        
        VSmartDownloadStatus downloadStatus = [[mo valueForKey:@"download_status"] intValue];
        
        //STYLING
        CALayer *bookLayer = itemCell.imageView.layer;
        bookLayer.shadowColor = [UIColor lightGrayColor].CGColor;
        bookLayer.shadowOpacity = 0.9;
        bookLayer.shadowRadius = 5;
        bookLayer.shadowOffset = CGSizeMake(-1,1);
        
        itemCell.book = book;
        itemCell.title = book.title;
        itemCell.author = [VSmartHelpers getAuthors:book.authors];
        
        
        NSString *thumb_url = [NSString stringWithFormat:@"%@",[mo valueForKey:@"thumbnail_url"]];
        [itemCell.imageView sd_setImageWithURL:[NSURL URLWithString:thumb_url]];
        
        
        NSData *binary_data = (NSData *)[mo valueForKey:@"thumbnail"];
        if (binary_data != nil) {
            itemCell.imageView.image = [UIImage imageWithData:binary_data];
        }
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        
        if(downloadStatus == VSmartDownloadNotStarted || downloadStatus == VSmartDownloadError || downloadStatus == VSmartDownloadCompleted) {
            [itemCell setProgressVisibility:NO];
            [itemCell setCloudVisibility:YES];
            [itemCell setBookProgressVisibility:NO];
        } else if (downloadStatus == VSmartDownloadInProgress || downloadStatus == VSmartDownloadZipExtraction) {
            [itemCell setProgressVisibility:YES];
            [itemCell setCloudVisibility:YES];
            [itemCell setBookProgressVisibility:NO];
            CGFloat progress = (CGFloat)[[mo valueForKey:@"progress"] floatValue];
            [itemCell setDownloadProgress:progress];
        } else if (downloadStatus == VSmartDownloadImported) {
            [itemCell setProgressVisibility:NO];
            [itemCell setCloudVisibility:NO];
            [itemCell setCompleted:YES];
        } else if (downloadStatus == VSmartDownloadNotStarted) {
            // Not Started
        } else if (downloadStatus == VSmartDownloadError) {
            // Error
            [itemCell setCompleted:NO];
            NSString *errorMessage = VS_FMT(MSG_BOOKS_DOWNLOAD_ERROR, book.title);
            [super alertMessage:errorMessage];
        } else if (downloadStatus == VSmartDownloadZipExtraction) {
            // Zip Extraction
        } else if (downloadStatus == VSmartDownloadCompleted) {
            // Completed
        } else if (downloadStatus == VSmartDownloadExtracted) {
            // check read state
            [itemCell setCompleted:YES];
            
            dispatch_async(queue, ^{
                VSmartDownloadBookStatus downloadBookStatus = [VSmartHelpers getBookReadingStatus:book.bookId];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [itemCell setReadStatus:downloadBookStatus == VSmartDownloadedBookUnread ? NO : YES];
                });
            });
        }
        
        [itemCell setAverageVisibility:(downloadStatus == VSmartDownloadExtracted || downloadStatus == VSmartDownloadImported) ];
        
        if (downloadStatus == VSmartDownloadExtracted || downloadStatus == VSmartDownloadImported) {
            
            dispatch_async(queue, ^{
                
                BOOL bookIsPdf = [book.mediaType isIn:kBookExtensionPdf, nil];
                [itemCell setBookProgressVisibility:!bookIsPdf];
                
                BOOL exerciseExists = [Utils isExerciseDataFileExists:VS_FMT(@"%@", book.bookId)];
                NSString *average = [self _computeAverage: book];
                
                NSDictionary *quizProgressData = nil;
                if(exerciseExists) {
                    quizProgressData = [self _getProgressCompletedQuiz:book];
                }
                
//                [itemCell setBookProgressVisibility:exerciseExists]; // MOVED TO MAIN QUEU
                NSDictionary *bookInfoFromCollection = [VSmartHelpers getBookInfoFromCollection: book.bookId inCollection:nil];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [itemCell setAverageText:average];
                    [itemCell setAverageVisibility:exerciseExists];
                    [itemCell setBookProgressVisibility:exerciseExists];
                    
                    NSString *lastReadLabel = NSLocalizedString(@"Last Read", nil);
                    [itemCell setDateLastRead:VS_FMT(@"%@: %@", lastReadLabel, [bookInfoFromCollection objectForKey:kBookDateLastRead])];
                    
                    if (exerciseExists) {
                        [itemCell setBookProgress:[[quizProgressData objectForKey:@"progress"] floatValue] withText:[quizProgressData objectForKey:@"text"]];
                    }
                });
            });
        }
        
        [itemCell setDateLastReadVisibility:(downloadStatus == VSmartDownloadExtracted || downloadStatus == VSmartDownloadImported)];
    }
    
    return itemCell;
}

#pragma mark - UICollectionViewDataSource

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index = indexPath.row;
    
    NSManagedObject *mo = [self.frc objectAtIndexPath:indexPath];
    
    Book *book = [mo valueForKey:@"book"];
    NSString *book_id_string = [NSString stringWithFormat:@"%@", book.bookId];
    NSString *sku_string = [NSString stringWithFormat:@"%@", [mo valueForKey:@"sku"]];
    
    NSLog(@"BOOK ID: %@", book_id_string);
    NSLog(@"BOOK SKU: %@", sku_string);
    
    VSmartDownloadStatus downloadStatus = [[mo valueForKey:@"download_status"] intValue];
    NSLog(@"download status : %@", @(downloadStatus) );
    
    if ((downloadStatus == VSmartDownloadImported) || (downloadStatus == VSmartDownloadExtracted) ) {
        [self showPopupBookInfo:book forIndex:index withBookState:kBookStateRead];
    }
    
    if (downloadStatus == VSmartDownloadNotStarted) {
        
        //VERY OLD IMPLEMENTATION
        
        //        NSMutableDictionary *dictWithProgress = [NSMutableDictionary dictionary];
        //        NSArray *attributes = [mo.entity.attributesByName allKeys];
        //        for (NSString *key in attributes) {
        //            id object = [mo valueForKey:key];
        //            if (object != nil) {
        //                [dictWithProgress setObject:[mo valueForKey:key] forKey:key];
        //            }
        //        }
        //        [dictWithProgress setObject:[NSNumber numberWithInteger:index] forKey:@"row"];
        //        // VLog(@"dictWithProgress: %@", dictWithProgress);
        //        // Localize string
        //        NSString *contactingServer = NSLocalizedString(@"contacting server...", nil); //checked
        //        [self.view makeToast:contactingServer duration:2.0f position:@"center"];
        //        // Ensure this MO in the FRC will have the value of VSmartDownloadInProgress
        //        NSNumber *progress_status = [NSNumber numberWithInt:VSmartDownloadInProgress];
        //        [mo setValue:progress_status forKeyPath:@"download_status"];
        //        [ctx save:nil];
        //        VS_NCPOST_OBJ(kNotificationQueueDownload, dictWithProgress)
        
        [self executeDownloadForBookID:book_id_string sku:sku_string];
    }
    
    //PAUSE
    if (downloadStatus == VSmartDownloadInProgress) {
        
        [self.rm pauseDownloadForBook:book_id_string];
    }
    
    //RESUME
    if (downloadStatus == VSmartDownloadPause) {
        
        [self.rm resumeDownloadForBook:book_id_string sku:sku_string];
    }
}

- (void)executeDownloadForBookID:(NSString *)bookid sku:(NSString *)sku {
    
    // Localize string
    NSString *contactingServer = NSLocalizedString(@"contacting server...", nil); //checked
    [self.view makeToast:contactingServer duration:2.0f position:@"center"];
    [self.rm updateDownloadFlagForBook:bookid];
    [self.rm requestBookID:bookid sku:sku];
}

#pragma mark - <NSFetchedResultsControllerDelegate>

- (NSFetchedResultsController *)configureFetch {
    
    if (_frc != nil) {
        return _frc;
    }
    
    NSManagedObjectContext *ctx = self.rm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTextBookEntity];
    
    // Create Predicate
    NSPredicate *predicate = [self.rm predicateForKeyPath:@"user_id" andValue:self.useridPATH];
    [fetchRequest setPredicate:predicate];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"book_id" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:ctx
                                                                                                  sectionNameKeyPath:nil
                                                                                                           cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.frc = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.frc performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _frc;
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    [self.gridView addChangeForSection:sectionInfo atIndex:sectionIndex forChangeType:type];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    
    [self.gridView addChangeForObjectAtIndexPath:indexPath forChangeType:type newIndexPath:newIndexPath];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    
    [self.gridView commitChanges];
}

//- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
//{
//    self.shouldReloadCollectionView = NO;
//    self.blockOperation = [[NSBlockOperation alloc] init];
//}
//
//- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
//           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
//{
//    __weak UICollectionView *collectionView = self.gridView;
//    if (collectionView != nil) {
//
//        switch (type) {
//            case NSFetchedResultsChangeInsert: {
//                [self.blockOperation addExecutionBlock:^{
//                    [collectionView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]];
//                }];
//                break;
//            }
//
//            case NSFetchedResultsChangeDelete: {
//                [self.blockOperation addExecutionBlock:^{
//                    [collectionView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]];
//                }];
//                break;
//            }
//
//            case NSFetchedResultsChangeUpdate: {
//                [self.blockOperation addExecutionBlock:^{
//                    [collectionView reloadSections:[NSIndexSet indexSetWithIndex:sectionIndex]];
//                }];
//                break;
//            }
//
//            default:
//                break;
//        }
//
//    }
//}
//
//- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
//{
//    __weak UICollectionView *collectionView = self.gridView;
//
//    if ( collectionView != nil ) {
//
//        switch (type) {
//            case NSFetchedResultsChangeInsert: {
//                if ([collectionView numberOfSections] > 0) {
//                    if ([collectionView numberOfItemsInSection:indexPath.section] == 0) {
//                        self.shouldReloadCollectionView = YES;
//                    } else {
//                        [self.blockOperation addExecutionBlock:^{
//                            [collectionView insertItemsAtIndexPaths:@[newIndexPath]];
//                        }];
//                    }
//                } else {
//                    self.shouldReloadCollectionView = YES;
//                }
//                break;
//            }
//
//            case NSFetchedResultsChangeDelete: {
//                if ([collectionView numberOfItemsInSection:indexPath.section] == 1) {
//                    self.shouldReloadCollectionView = YES;
//                } else {
//                    if ( [collectionView numberOfSections] > 0 ) {
//                        [self.blockOperation addExecutionBlock:^{
//                            [collectionView deleteItemsAtIndexPaths:@[indexPath]];
//                        }];
//                    }
//                }
//                break;
//            }
//
//            case NSFetchedResultsChangeUpdate: {
//                [self.blockOperation addExecutionBlock:^{
//                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
//                }];
//                break;
//            }
//
//            case NSFetchedResultsChangeMove: {
//                [self.blockOperation addExecutionBlock:^{
//                    [collectionView moveItemAtIndexPath:indexPath toIndexPath:newIndexPath];
//                }];
//                break;
//            }
//
//            default:
//                break;
//        }
//    }
//}
//
//- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
//
//    // Checks if we should reload the collection view to fix a bug @ http://openradar.appspot.com/12954582
//
//    __weak typeof(self) wo = self;
//
//    if (self.gridView != nil) {
//
//        if (self.shouldReloadCollectionView == YES) {
//            [self.gridView reloadData];
//            [self enableHomeButton:YES];
//        }
//
//        if (self.shouldReloadCollectionView == NO) {
//            [self.gridView performBatchUpdates:^{
//                [wo.blockOperation start];
//            } completion:^(BOOL finished) {
//                
//                if (wo.blockOperation.finished == YES) {
//                    
//                    [SVProgressHUD dismiss];
//                    [self enableHomeButton:YES];
//                }
//            }];
//        }
//    }
//}

@end
