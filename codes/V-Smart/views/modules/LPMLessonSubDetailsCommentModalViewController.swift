//
//  LPMLessonSubDetailsCommentModalViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 13/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMLessonSubDetailsCommentModalViewController: UIViewController {
    
    @IBOutlet fileprivate var titleLabel: UILabel!
    @IBOutlet fileprivate var image: UIImageView!
    @IBOutlet fileprivate var commentTextView: UITextView!
    @IBOutlet fileprivate var closeButton: UIButton!
    @IBOutlet fileprivate var saveButton: UIButton!
    
    var commentObject: NSManagedObject!
    
    fileprivate var lpid: String!
    fileprivate var template: String!
    
    // MARK: - Data Manager
    
    fileprivate lazy var lpmDataManager: LPMDataManager = {
        let lpdm = LPMDataManager.sharedInstance
        return lpdm
    }()
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let title = NSLocalizedString("Comment", comment: "")
        self.titleLabel.text = title
        
        let save = NSLocalizedString("Save", comment: "")
        self.saveButton.setTitle(save, for: UIControlState())
        self.saveButton.setTitle(save, for: .highlighted)
        self.saveButton.setTitle(save, for: .selected)
        
        let saveButtonAction = #selector(self.saveButtonAction(_:))
        self.saveButton.addTarget(self, action: saveButtonAction, for: .touchUpInside)
        
        let closeButtonAction = #selector(self.closeButtonAction(_:))
        self.closeButton.addTarget(self, action: closeButtonAction, for: .touchUpInside)
        
        self.lpid = self.lpmDataManager.stringValue(self.lpmDataManager.fetchUserDefaultsObject(forKey: "LPM_USER_DEFAULT_LESSON_PLAN_ID"))
        self.template = self.lpmDataManager.stringValue(self.lpmDataManager.fetchUserDefaultsObject(forKey: "LPM_USER_DEFAULT_TEMPLATE_ID"))
        
        if let comment = self.commentObject.value(forKey: "comment") as? String {
            self.commentTextView.text = comment
        }
        else {
            self.commentTextView.text = ""
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.preferredContentSize = CGSize(width: 600, height: 220)
    }
    
    // MARK:  - Button Event Handlers
    
    func saveButtonAction(_ sender: UIButton) {
        guard let commentID = self.commentObject.value(forKey: "id") else {
            return
        }
        
        self.view.makeToastActivity()
        self.view.isUserInteractionEnabled = false
        
        let body = ["comment_id": commentID, "comment": self.commentTextView.text, "is_comment": "1"]
        
        self.lpmDataManager.requestUpdateComment(body as [String : AnyObject]) { (doneBlock) in
            if doneBlock {
                self.lpmDataManager.requestDetailsForLessonPlanWithID(self.lpid, template: self.template, handler: { (doneBlock) in
                    DispatchQueue.main.async(execute: {
                        self.view.hideToastActivity()
                        self.view.isUserInteractionEnabled = true
                        self.dismiss(animated: true, completion: nil)
                    })
                })
            }
            else {
                DispatchQueue.main.async(execute: {
                    self.view.hideToastActivity()
                    self.view.isUserInteractionEnabled = true
                    
                    let message = NSLocalizedString("Comment was not successfully updated.", comment: "")
                    self.view.makeToast(message: message, duration: 5.0, position: "Center" as AnyObject)
                })
            }
        }
    }
    
    func closeButtonAction(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }
    
}
