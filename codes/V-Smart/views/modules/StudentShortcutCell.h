//
//  StudentShortcutCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 4/20/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@interface StudentShortcutCell : UITableViewCell

@property (strong, nonatomic) IBOutlet EGOImageView *profileImage;
@property (strong, nonatomic) IBOutlet UILabel *labelName;
@property (strong, nonatomic) IBOutlet UIView *labelStatus;

@end
