//
//  SSMainViewController.swift
//  V-Smart
//
//  Created by Ryan Migallos on 26/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
import AVFoundation

class SSMainViewController: UIViewController, Dimmable {
    
    @IBOutlet fileprivate var segmentedView: UIView!
    
    var dimView: UIView = UIView()
    
    fileprivate let segmentedControl = HMSegmentedControl()
    fileprivate let notification = NotificationCenter.default
    
    fileprivate var sections = [String]()
    fileprivate var currentSection = "0"
    
    fileprivate lazy var ssdm: SocialStreamDataManager = {
        let dataManager = SocialStreamDataManager.sharedInstance
        return dataManager
    }()
    
    var editMessageDict:[String:String]?
    var viewMessageDict:[String:String?]?
    
    var message_id = ""
    
    @IBOutlet weak var drawerButtonView: UIView!
    @IBOutlet weak var drawerButton: UIButton!
    @IBOutlet weak var groupTableContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create Data Manager Singleton
        DispatchQueue.main.async(execute: {
            let message = "\(NSLocalizedString("Loading", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: message)
        })
        
        ssdm.requestGroup { (done, error) in
            if error == nil {
                DispatchQueue.main.async(execute: {
                    self.setUpSegmentedViews()
                })
            }
            else {
                DispatchQueue.main.async(execute: {
                    self.view.hideToastActivity()
                })
                
                let alertView = UIAlertController(title: "", message: error, preferredStyle: UIAlertControllerStyle.alert)
                let closeAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.cancel) { (Alert) -> Void in
                    DispatchQueue.main.async(execute: { 
                        _ = self.navigationController?.popToRootViewController(animated: true)
                    })
                }

                alertView.addAction(closeAction)

                DispatchQueue.main.async {
                    HUD.hideUIBlockingIndicator()
                    self.present(alertView, animated: true, completion: nil)
                }
            }
        }
        
        self.setUpNotification()
        ssdm.requestBadwords(nil)
        
        self.drawerButtonView.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.teardownNotification()
        self.ssdm.disconnectSocket()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func drawerButtonAction(_ b: UIButton) {
        let hidden = (self.groupTableContainer.isHidden) ? false : true
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.allowUserInteraction], animations: {
            self.groupTableContainer.isHidden = hidden
            }, completion: { (animate) in
                
        })
    }
    
    func setUpNotification() {
        notification.addObserver(self, selector: #selector(self.createMessageButtonAction(_:)), name: NSNotification.Name(rawValue: "SS_NOTIFICATION_CREATE_MESSAGE"), object: nil)
        notification.addObserver(self, selector: #selector(self.createMessageButtonAction(_:)), name: NSNotification.Name(rawValue: "SS_NOTIFICATION_EDIT_MESSAGE"), object: nil)
        notification.addObserver(self, selector: #selector(self.showLikeAction(_:)), name: NSNotification.Name(rawValue: "SS_NOTIFICATION_SHOW_LIKE_MESSAGE"), object: nil)
        notification.addObserver(self, selector: #selector(self.commentAction(_:)), name: NSNotification.Name(rawValue: "SS_NOTIFICATION_COMMENT_MESSAGE"), object: nil)
        notification.addObserver(self, selector: #selector(self.viewMessageAction(_:)), name: NSNotification.Name(rawValue: "SS_NOTIFICATION_VIEW_MESSAGE"), object: nil)
        notification.addObserver(self, selector: #selector(self.accessibilityModeAction(_:)), name: NSNotification.Name(rawValue: "SS_NOTIFICATION_ACCESSIBILITY_MODE"), object: nil)
        notification.addObserver(self, selector: #selector(self.defaultViewAction(_:)), name: NSNotification.Name(rawValue: "SS_NOTIFICATION_DEFAULT_VIEW"), object: nil)
        notification.addObserver(self, selector: #selector(self.notificationMessageToastAction(_:)), name: NSNotification.Name(rawValue: "SS_NOTIFICATION_MESSAGE_TOAST"), object: nil)
        notification.addObserver(self, selector: #selector(self.drawerButtonAction(_:)), name: NSNotification.Name(rawValue: "SS_NOTIFICATION_SIDE_PANEL"), object: nil)
        
        notification.addObserver(self, selector: #selector(self.reconnectSocket), name: NSNotification.Name(rawValue: "GN_NOTIFICATION_SOCKET_CONNECT"), object: nil)
    }
    
    func reconnectSocket() {
        ssdm.connectSocket("8000")
    }
    
    func teardownNotification() {
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_CREATE_MESSAGE"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_EDIT_MESSAGE"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_SHOW_LIKE_MESSAGE"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_COMMENT_MESSAGE"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_VIEW_MESSAGE"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_ACCESSIBILITY_MODE"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_DEFAULT_VIEW"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_MESSAGE_TOAST"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_SIDE_PANEL"), object: nil)
        
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "GN_NOTIFICATION_SOCKET_CONNECT"), object: nil)
    }
    
    func createMessageButtonAction(_ notification: Notification) {
        let dict = notification.object as? [String:String]
        self.editMessageDict = dict;
        self.performSegue(withIdentifier: "SEGUE_CREATE_MESSAGE", sender: self)
    }
    
    func showLikeAction(_ notification: Notification) {
        let message_id = notification.object as? String
        self.message_id = message_id!
        self.performSegue(withIdentifier: "SEGUE_SHOW_LIKES", sender: self)
    }
    
    func commentAction(_ notification: Notification) {
        let message_id = notification.object as? String
        self.message_id = message_id!
        self.performSegue(withIdentifier: "SEGUE_SHOW_COMMENTS", sender: nil)
    }
    
    func viewMessageAction(_ notification: Notification) {
        let dict = notification.object as? [String:String?]
        self.viewMessageDict = dict;
        self.performSegue(withIdentifier: "SEGUE_VIEW_MESSAGE", sender: self)
    }
    
    func accessibilityModeAction(_ notification: Notification) {
        guard let accessible = notification.object as? String else {
            return
        }
        
        let enable = accessible == "1" ? true : false
        segmentedControl.isUserInteractionEnabled = enable
    }
    
    func defaultViewAction(_ notification: Notification) {
        self.loadGroupForSection(currentSection)
        self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_CANCEL_EDITING"), object: nil)
    }
    
    func notificationMessageToastAction(_ notification: Notification) {
        guard let message = notification.object as? String else {
            return
        }
        
        DispatchQueue.main.async(execute: {
            let soundID: SystemSoundID = 1016
            AudioServicesPlaySystemSound(soundID)
            
            let image = UIImage(named: "social-stream-student-30px.png")
            self.view.makeToast(message: message, duration: 3.0, position: HRToastPositionDefault as AnyObject, image: image!)
        })
    }
    
    // MARK: Segmented Controller
    
    fileprivate func setUpSegmentedViews() {
        guard let sectionList = self.ssdm.fetchSections() else {
            return
        }
        
        var sectionNames = [String]()
        
        if sectionList.count > 0 {
            for section in sectionList {
                if let id = section["group_id"] as? String, let name = section["name"] as? String, let specificSectionName = section["section"] as? String {
                    sections.append(id)
                    sectionNames.append("\(name) - \(specificSectionName)")
                }
            }
            
            // LOAD DEFAULT VALUE
            currentSection = sections[0]
            loadGroupForSection(currentSection)
        }
        
        // SET UP SEGMENTED VIEWS
        segmentedControl.sectionTitles = sectionNames
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.backgroundColor = UIColor(rgba: "#f5f4f4")
        segmentedControl.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(rgba: "#1d6483")]
        segmentedControl.selectedTitleTextAttributes = [NSForegroundColorAttributeName: UIColor(rgba: "#1d6483")]
        segmentedControl.selectionIndicatorColor = UIColor(rgba: "#00a9d5")
        segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox
        segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationUp
        
        // SWITCH VIEW
        self.segmentedControl.indexChangeBlock = { (index) in
            DispatchQueue.main.async(execute: {
                let message = "\(NSLocalizedString("Loading", comment: ""))..."
                HUD.showUIBlockingIndicator(withText: message)
            })
            
            self.currentSection = self.sections[index]
            self.loadGroupForSection(self.currentSection)
        }
        
        // EMBED VIEW
        segmentedView.addSubview(self.segmentedControl)
        
        // CONSTRAINTS
        segmentedControl.mas_makeConstraints { (make) in
            make?.edges.isEqual(self.segmentedView)
        }
        
        // START SOCKET SERVER LISTNER
        ssdm.connectSocket("8000")
    }
    
    fileprivate func loadGroupForSection(_ sectionid:String!) {
        ssdm.requestSubGroupForSection(sectionid) { (done) in
            self.ssdm.requestUserMessages(forGroup: "0", inSection: self.currentSection, handler: { (doneBlock) in
                DispatchQueue.main.async(execute: {
                    HUD.hideUIBlockingIndicator()
                })
                
                DispatchQueue.main.async(execute: {
                    self.initializeDefaults()
                    self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_CHANGE_GROUP"), object: nil)
                    self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_RELOAD_DATA"), object: nil)
                })
            })
        }
    }
    
    fileprivate func initializeDefaults() {
        let groupName = NSLocalizedString("All Students", comment: "")
        segmentedControl.isUserInteractionEnabled = true
        ssdm.save(object: currentSection as AnyObject!, forKey: "SS_SELECTED_SECTION_ID")
        ssdm.save(object: "0" as AnyObject!, forKey: "SS_SELECTED_GROUP_ID")
        ssdm.save(object: groupName as AnyObject!, forKey: "SS_SELECTED_GROUP_NAME")
        ssdm.save(object: "0" as AnyObject!, forKey: "SS_GROUP_IS_EDIT_MODE")
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sscmv = segue.destination as? SSCreateMessageViewController , segue.identifier == "SEGUE_CREATE_MESSAGE" {
            sscmv.delegate = self
            sscmv.editMessageDict = self.editMessageDict
        }
        
        if let ssslv = segue.destination as? SSShowLikesViewController , segue.identifier == "SEGUE_SHOW_LIKES" {
            ssslv.entity = SSMConstants.Entity.LIKES
            ssslv.predicate_value = self.message_id
            ssslv.predicate_keypath = "message_id"
        }
        
        if let sscv = segue.destination as? SSCommentViewController , segue.identifier == "SEGUE_SHOW_COMMENTS" {
            sscv.message_id = self.message_id
        }
        
        if let ssvm = segue.destination as? SSViewMessageViewController , segue.identifier == "SEGUE_VIEW_MESSAGE" {
            ssvm.viewMessageDict = self.viewMessageDict
            ssvm.delegate = self
        }
    }
    
}
