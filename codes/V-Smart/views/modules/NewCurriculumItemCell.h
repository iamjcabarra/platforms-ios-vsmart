//
//  NewCurriculumItemCell.h
//  V-Smart
//
//  Created by Julius Abarra on 16/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewCurriculumItemCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *curriculumTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *curriculumCreatorLabel;
@property (strong, nonatomic) IBOutlet UILabel *gradeLevelNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *effectivityLabel;
@property (strong, nonatomic) IBOutlet UILabel *curriculumDescriptionLabel;
@property (strong, nonatomic) IBOutlet UIButton *downloadButton;

@end
