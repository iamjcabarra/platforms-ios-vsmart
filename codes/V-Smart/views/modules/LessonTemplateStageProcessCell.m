//
//  LessonTemplateStageProcessCell.m
//  V-Smart
//
//  Created by Julius Abarra on 08/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "LessonTemplateStageProcessCell.h"

@implementation LessonTemplateStageProcessCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    NSString *title = NSLocalizedString(@"Uploading of file is not supported by this platform.", nil);
    [self.uploadFileButton setTitle:title forState:UIControlStateNormal];
    [self.uploadFileButton setTitle:title forState:UIControlStateSelected];
    [self.uploadFileButton setTitle:title forState:UIControlStateHighlighted];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)shouldHideProcessContentTextView:(BOOL)hide {
    self.processContentTextView.hidden = hide;
}

- (void)shouldHideUploadFileButton:(BOOL)hide {
    self.uploadFileButton.hidden = hide;
}

- (void)shouldHideClearFileButton:(BOOL)hide {
    self.clearFileButton.hidden = hide;
}

- (void)updateUploadButtonTitle:(NSString *)title {
    [self.uploadFileButton setTitle:title forState:UIControlStateNormal];
    [self.uploadFileButton setTitle:title forState:UIControlStateSelected];
    [self.uploadFileButton setTitle:title forState:UIControlStateHighlighted];
}

@end
