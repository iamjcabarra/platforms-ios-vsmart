//
//  StudentGradeBookViewController.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 08/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
import CoreData
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class StudentGradeBookViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, UIPopoverPresentationControllerDelegate, UISearchBarDelegate, StudentCourseListDelegate {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var courseLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var courseSelectionButton: UIButton!
    
    var sortFlag: Bool = false
    var activityType: String = "10"
    var course_id: String?
    
    var userID: String!
    
    // MARK: - Shared Data Manager
    fileprivate lazy var dataManager: GradeBookDataManager = {
        let tgdm = GradeBookDataManager.sharedInstance()
        return tgdm!
    }()
    
    var menuViewController : StudentCourseListController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.segmentedControl.selectedSegmentIndex = 0;
        self.tableView.estimatedRowHeight = 150;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.sectionHeaderHeight = 60;
        
        
        let sb : UIStoryboard = UIStoryboard(name: "StudentGradeBookStoryboard", bundle: nil)
        self.menuViewController = sb.instantiateViewController(withIdentifier: "StudentCourseListController") as? StudentCourseListController
        self.menuViewController!.modalPresentationStyle = UIModalPresentationStyle.popover
        self.menuViewController?.delegate = self
        
        self.userID = "\(self.dataManager.loginUser()!)"
        self.dataManager.requestCourseList(forUserID: self.userID) { (status) in
            let courseList = self.dataManager.fetchCourseList(forUserID: self.userID)
            if courseList?.count > 0 {
                let d = courseList?[0] as! NSMutableDictionary
                print("course data \(d)")
                let course_name = d.value(forKey: "course_name") as! String
                self.course_id = d.value(forKey: "cs_id") as? String
                
                DispatchQueue.main.async(execute: { 
                    self.courseLabel.text = course_name
                })
                
                self.dataManager.requestGradeList(forUser: self.userID, course: self.course_id, doneBlock: { (status) in
                    print("grade list done \(status)")
                })
            }
        }
        
        self.segmentedControl.addTarget(self, action: #selector(self.controlAction(_:)), for: .valueChanged)
        
        self.sortButton.addTarget(self, action: #selector(self.changeSortingOrder(_:)), for: .touchUpInside)
        
        self.courseSelectionButton.addTarget(self, action: #selector(self.courseSelection(_:)), for: .touchUpInside)
        
        self.searchBar.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func courseSelection(_ button: UIButton) {
        let popover: UIPopoverPresentationController = self.menuViewController!.popoverPresentationController!
        popover.sourceView = button
        popover.sourceRect = button.bounds
        popover.permittedArrowDirections = UIPopoverArrowDirection.up
        popover.delegate = self
        
        present(self.menuViewController!, animated: true, completion:nil)
    }
    
    func selectedClassData(_ option: NSDictionary) {
        let course_id = option["id"] as? String
        let course_name = option["course_name"] as? String
        self.courseLabel.text = course_name
        self.course_id = course_id
        
        self.dataManager.requestGradeList(forUser: self.userID, course: course_id) { (status) in
            print("STATUS!!!~ \(status)")
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func controlAction(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            self.activityType = "10"
            break;
        case 1:
            self.activityType = "8"
            break;
        default:
            break;
        }
        
        self.reloadFetchedResultsController()
    }
    
    func changeSortingOrder(_ sender:UIButton) {
        self.sortFlag = (self.sortFlag) ? false : true
        
        self.reloadFetchedResultsController()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let count = self.fetchedResultsController.sections?.count
        return count!;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        let count = sectionInfo.numberOfObjects
        return count;
    }
    
    func tableView(_ tbv: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView = tbv;
        let sgc = tbv.dequeueReusableCell(withIdentifier: "grade_book_cell_identifier", for: indexPath)
        self.configureCell(sgc, atIndexPath: indexPath)
        return sgc;
    }
    
    fileprivate func configureCell(_ cell: UITableViewCell, atIndexPath indexPath: IndexPath) {
        
        let gradeBookCell = cell as! StudentGradebookCell
        self.configureGradeBookCell(gradeBookCell, atIndexPath: indexPath)
    }
    
    fileprivate func configureGradeBookCell(_ cell: StudentGradebookCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        
        let activity_name = mo.value(forKey: "activity_name") as! String
        let date_performed = mo.value(forKey: "date_performed") as! String
        let date_time_object_string = date_performed
        
        let score = mo.value(forKey: "score") as! String
        
        let imageName = (self.segmentedControl.selectedSegmentIndex == 0) ? "img_test" : "img_quiz"
        cell.itemImageView.image = UIImage(named: imageName)
        
        cell.dateLabel.text = date_time_object_string
        cell.timeLabel.text = ""
        cell.nameLabel.text = activity_name
        cell.scoreLabel.text = score
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerIdentifier = "section_header"
        let header = tableView.dequeueReusableCell(withIdentifier: headerIdentifier) as! StudentGradeBookHeader
        
        header.nameLabel.text = "Name"
        header.dateLabel.text = "Date Performed"
        header.scoreLabel.text = "Score"
        _ = header.contentView.intrinsicContentSize
        
        return header.contentView
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = self.tableView.indexPathForSelectedRow
        self.tableView.deselectRow(at: indexPath!, animated: true)
        self.tableView.isUserInteractionEnabled = true
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        var error: NSError? = nil
        do {
            try _fetchedResultsController!.performFetch()
        } catch let error1 as NSError {
            error = error1
            print(error?.localizedDescription)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.reloadFetchedResultsController()
    }
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        // Set ManagedObjectContext
        let ctx = dataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: kGradeBookRecEntityV2)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kGradeBookRecEntityV2)
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 10
        
        // Predicate
        var predicate = self.predicateForKeyPath(keypath: "activity_type_id", value: self.activityType)
        
        if self.searchBar.text?.characters.count > 0 {
            let predicate_actType = self.predicateForKeyPath(keypath: "activity_type_id", value: self.activityType)
            let predicate_actName = self.predicateContaintsForKeyPath(keypath: "activity_name", value: self.searchBar.text!)
            predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate_actName, predicate_actType])
        }
        
        fetchRequest.predicate = predicate
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "date_performed", ascending: self.sortFlag)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest,
                                             managedObjectContext: ctx!,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil)
        frc.delegate = self
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
            
        case .insert:
            self.tableView.insertSections( IndexSet(integer: sectionIndex), with: .fade)
            
        case .delete:
            self.tableView.deleteSections( IndexSet(integer: sectionIndex), with: .fade)
            
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        let tbv = self.tableView
        switch type {
            
        case .insert:
            tbv?.insertRows( at: [newIndexPath!] , with: .fade)
            
        case .delete:
            tbv?.deleteRows( at: [indexPath!], with: .fade)
            
        case .update:
            self.configureCell( (tbv?.cellForRow(at: indexPath!)!)!, atIndexPath:indexPath!)
            
        case .move:
            tbv?.deleteRows(at: [indexPath!], with: .fade)
            tbv?.insertRows(at: [indexPath!], with: .fade)
        }
    }
    
    func predicateForKeyPath(keypath:String, value:String) -> NSPredicate {
        
        // create left and right expression
        let left = NSExpression(forKeyPath: keypath)
        let right = NSExpression(forConstantValue: value)
        
        // create predicate options
        let predicate = NSComparisonPredicate(leftExpression: left,
                                              rightExpression: right,
                                              modifier: .direct,
                                              type: NSComparisonPredicate.Operator.like,
                                              options: [NSComparisonPredicate.Options.diacriticInsensitive, NSComparisonPredicate.Options.caseInsensitive])
        return predicate
    }
    
    func predicateContaintsForKeyPath(keypath:String, value:String) -> NSPredicate {
        
        // create left and right expression
        let left = NSExpression(forKeyPath: keypath)
        let right = NSExpression(forConstantValue: value)
        
        // create predicate options
        let predicate = NSComparisonPredicate(leftExpression: left,
                                              rightExpression: right,
                                              modifier: .direct,
                                              type: NSComparisonPredicate.Operator.contains,
                                              options: [NSComparisonPredicate.Options.diacriticInsensitive, NSComparisonPredicate.Options.caseInsensitive])
        return predicate
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
