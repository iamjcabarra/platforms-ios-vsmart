//
//  CurriculumListView.m
//  V-Smart
//
//  Created by Ryan Migallos on 9/16/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "CurriculumListView.h"
#import "CurriculumListHeaderView.h"
#import "CurriculumItemCell.h"
#import "CurriculumDataManager.h"
#import "CurriculumDetailView.h"
#import "MainHeader.h"

#define ResultsTableView self.searchResultsTableViewController.tableView

@interface CurriculumListView () <NSFetchedResultsControllerDelegate, UISearchResultsUpdating, UISearchControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate> {
    
    NSManagedObject *mo_selected;
    UITableView *tableView_active;
}

@property (nonatomic, strong) CurriculumDataManager *cm;

@property (nonatomic, strong) UIRefreshControl *tableRefreshControl;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) UITableViewController *searchResultsTableViewController;
@property (nonatomic, strong) NSArray *results;
@property (nonatomic, assign) BOOL isAscending;
@property (nonatomic, strong) NSString *user_id;
@end

@implementation CurriculumListView

static NSString *kCellIdentifier = @"curriculum_item_cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    self.title = NSLocalizedString(@"Curriculum List", nil);
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.sectionHeaderHeight = 60;
    
    self.isAscending = NO;
    self.cm = [CurriculumDataManager sharedInstance];
    
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    SEL refreshAction = @selector(listAllTest);
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    [self listAllTest];
    [self setupRightBarButton];
    [self setupSearchCapabilities];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupRightBarButton {
    
    UIImage *sortImage = [UIImage imageNamed:@"sort_258x258"];
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sortButton.frame = CGRectMake(0, 0, 44, 44);
    sortButton.showsTouchWhenHighlighted = YES;
    [sortButton setImage:sortImage forState:UIControlStateNormal];
    [sortButton setImage:sortImage forState:UIControlStateHighlighted];
    [sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
}

- (void)setupSearchCapabilities {
    
    self.results = [[NSMutableArray alloc] init];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"CurriculumStoryboard" bundle:nil];
    NSString *controllerIdentifier = @"search_curriculum_table_view_controller";
    UITableViewController *srtvc = [sb instantiateViewControllerWithIdentifier:controllerIdentifier];
    srtvc.tableView.dataSource = self;
    srtvc.tableView.delegate = self;
    
    self.searchResultsTableViewController = srtvc;
    
    // Init a search controller with its table view controller for results.
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsTableViewController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    
    // Make an appropriate size for search bar and add it as a header view for initial table view.
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // Enable presentation context.
    self.definesPresentationContext = YES;
}

- (void)listAllTest {
    
    __weak typeof(self) wo = self;
    [self.cm requestCurriculumListWithBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.tableRefreshControl endRefreshing];
        });
    }];
}

- (IBAction)refreshTestList:(id)sender {
    
    [self listAllTest];
}

- (void)sortButtonAction:(id)sender {
    
    self.isAscending = (_isAscending) ? NO : YES;
    
    if ([tableView_active isEqual:ResultsTableView]) {
        [self reloadSearchResults];
    }
    
    if (![tableView_active isEqual:ResultsTableView]) {
        [self reloadFetchedResultsController];
    }
}

- (void)reloadSearchResults {
    
    NSArray *items = [NSArray arrayWithArray:self.results];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:self.isAscending];
    NSArray *sorted = [items sortedArrayUsingDescriptors:@[date_modified]];
    
    // Set up results.
    self.results = [NSArray arrayWithArray:sorted];
    
    // Reload search table view.
    [tableView_active reloadData];
}

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (NSManagedObject *)managedObjectFromButtonAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = 1;
    
    if ([tableView isEqual:ResultsTableView]) {
        return count;
    } else {
        return [[self.fetchedResultsController sections] count];
    }
    
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    
    return tableView.sectionHeaderHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *kHeaderIdentifier = @"section_header";
    
    CurriculumListHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kHeaderIdentifier];
    if(!headerView) {
        headerView = [[[NSBundle mainBundle] loadNibNamed:@"CurriculumListHeaderView" owner:self options:nil] objectAtIndex:0];
    }
    
    UIView *v = [[UIView alloc] initWithFrame:headerView.bounds];
    v.backgroundColor = [UIColor whiteColor];
    headerView.backgroundView = v;
    headerView.labelTitle.text = NSLocalizedString(@"Title", nil);
    headerView.labelOption.text = NSLocalizedString(@"Status", nil);
    
    if ([tableView isEqual:ResultsTableView]) {
        return v;
    }
    
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    if ([tableView isEqual:ResultsTableView]) {
        
        if (self.results) {
            return self.results.count;
        } else {
            return count;
        }
        
    } else {
        
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        count = [sectionInfo numberOfObjects];
        
    }
    
    return count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    tableView_active = tableView;
    mo_selected = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
    NSString *curriculumid = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"curriculum_id"] ];
    __weak typeof(self) wo = self;
    [self.cm requestCurriculumWithID:curriculumid doneBlock:^(BOOL status) {
        if (status == YES) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo performSegueWithIdentifier:@"show_curriculum_details" sender:nil];
            });
        }
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    if ([tableView isEqual:ResultsTableView]) {
        [self configureFilteredCell:cell indexPath:indexPath];
    }
    
    if (![tableView isEqual:ResultsTableView]) {
        [self configureCell:cell atIndexPath:indexPath];
    }
    
    return cell;
}

- (void)configureFilteredCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = self.results[indexPath.row];
    CurriculumItemCell *curriculumItemCell = (CurriculumItemCell *)cell;
    [self configureCell:curriculumItemCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    CurriculumItemCell *curriculumItemCell = (CurriculumItemCell *)cell;
    [self configureCell:curriculumItemCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(CurriculumItemCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *title = [NSString stringWithFormat:@"%@", [mo valueForKey:@"curriculum_title"] ];
    NSString *description = [NSString stringWithFormat:@"%@", [mo valueForKey:@"curriculum_description"] ];
    NSString *status = [NSString stringWithFormat:@"%@", [mo valueForKey:@"curriculum_status_remarks"] ];
    NSString *template = [NSString stringWithFormat:@"%@", [mo valueForKey:@"curriculum_template_name"] ];
    
    cell.labelTitle.text = [NSString stringWithFormat:@"%@ - %@", title, template];
    cell.labelDescription.text = description;
    cell.labelStatus.text = status;
    cell.labelStatus.textColor = [UIColor lightGrayColor];
    if ([status isEqualToString:@"Approved"]) {
        cell.labelStatus.textColor = UIColorFromHex(0x16a085);
    }
    
    SEL downloadSelector = @selector(downloadButtonAction:);
    [cell.buttonDownload addTarget:self action:downloadSelector forControlEvents:UIControlEventTouchUpInside];
    
    cell.buttonDownload.hidden = YES;
}

- (void)downloadButtonAction:(id)sender {
    
    //    mo_selected = [self managedObjectFromButtonAction:sender];
    //    NSString *question_title = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"name"]];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    if ([segue.identifier isEqualToString:@"show_curriculum_details"]) {
        CurriculumDetailView *detail = (CurriculumDetailView *)[segue destinationViewController];
        
        NSString *curriculum_id = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"curriculum_id"] ];
        NSPredicate *predicate = [self.cm predicateForKeyPath:@"curriculum_id" andValue:curriculum_id];
        detail.mo = [self.cm getEntity:kCurriculumTableEntity predicate:predicate];
    }
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }

    NSManagedObjectContext *ctx = self.cm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCurriculumListEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    //    NSPredicate *predicate_type = [self predicateForKeyPath:@"questionTypeName" value:@"Multiple Choice"];
    //    NSCompoundPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate_type]];
    //    [fetchRequest setPredicate:predicate];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *date_modified = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:self.isAscending];
    [fetchRequest setSortDescriptors:@[date_modified]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Search Results Updating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    UISearchBar *searchBar = searchController.searchBar;
    
    if (searchBar.text.length > 0) {
        
        NSString *text = searchBar.text;
        
        NSArray *fetchedObjects = self.fetchedResultsController.fetchedObjects;
        
        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(NSManagedObject *mo, NSDictionary *bindings) {
            
            NSString *object = [NSString stringWithFormat:@"%@", [mo valueForKey:@"search_string"]];
            NSStringCompareOptions options = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;
            NSRange range = [object rangeOfString:text options:options];
            
            return range.location != NSNotFound;
        }];
        
        // Set up results.
        self.results = [fetchedObjects filteredArrayUsingPredicate:predicate];;
        
        // Reload search table view.
        [self.searchResultsTableViewController.tableView reloadData];
    }
}

@end
