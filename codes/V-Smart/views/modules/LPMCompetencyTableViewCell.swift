//
//  LPMCompetencyTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 26/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMCompetencyTableViewCell: UITableViewCell {

    @IBOutlet var checkBoxImage: UIImageView!
    @IBOutlet var codeLabel: UILabel!
    @IBOutlet var competencyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.checkBoxImage.image = UIImage(named: "vibe_check_box_blank_75px.png")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func showSelected(_ selected: Bool) {
        let imageA = UIImage(named: "vibe_check_box_75px.png")
        let imageB = UIImage(named: "vibe_check_box_blank_75px.png")
        self.checkBoxImage.image = selected ? imageA : imageB
    }

}
