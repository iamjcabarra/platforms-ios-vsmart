//
//  GradeBookDataManager.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 27/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "GradeBookConstants.h"

static NSString *kCourseEntityV2 = @"GBStudentCourse";
static NSString *kGradeBookRecEntityV2 = @"GradeBookRec";


static NSString *kGBCourseEntityV2 = @"GBCourse";
static NSString *kGradeBookEntityV2 = @"GradeBook";

/////// BLOCK TYPES //////
typedef void (^GradeBookDoneBlock)(BOOL status);
typedef void (^GradeBookDataBlock)(NSDictionary *data);

typedef void (^PlayListProgressBlock)(CGFloat progress);
typedef void (^PlayListDoneProgressBlock)(NSString *status);
typedef void (^PlayListListBlock)(NSArray *list);

@interface GradeBookDataManager : NSObject

@property (nonatomic, copy, readwrite) PlayListProgressBlock progressBlock;
@property (nonatomic, copy, readwrite) PlayListDoneProgressBlock doneProgressBlock;
@property (nonatomic, readonly) NSManagedObjectContext *mainContext;
@property (nonatomic, readonly) NSManagedObjectContext *workerContext;
@property (nonatomic, readonly) NSURLSession *session;
@property (nonatomic, readonly) NSDateFormatter *formatter;

+ (instancetype)sharedInstance;
- (void)saveContext;
- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate;
- (BOOL)clearContentsForEntity:(NSString *)entity;

/////// PUBLIC METHODS //////
- (NSString *)loginUser;
- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value;
- (NSPredicate *)predicateForKeyPath:(NSString *)keypath object:(id)object;
- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value;
- (NSManagedObject *)getEntity:(NSString *)entity attribute:(NSString *)attribute parameter:(NSString *)parameter context:(NSManagedObjectContext *)context;
- (NSUInteger)fetchCountForEntity:(NSString *)entity;
- (NSArray *)fetchObjectsForEntity:(NSString *)entity;
- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate;
- (NSManagedObject *)insertNewRecordForEntity:(NSString *)entity;
- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context;
- (void)saveObject:(id)object forKey:(NSString *)key;
- (id)fetchObjectForKey:(NSString *)key;
- (BOOL)clearDataForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)ctx;
- (NSString *)stringValue:(id)object;
- (NSURL *)buildURL:(NSString *)string;
- (NSMutableURLRequest *)buildRequestWithMethod:(NSString *)method NSURL:(NSURL *)url body:(id)body;
- (BOOL)isArrayObject:(id)object;
- (void)saveTreeContext:(NSManagedObjectContext *)context;
//- (NSString *)urlEncode:(id<NSObject>)value;


// GRADEBOOK

- (void)requestCourseListForUserID:(NSString *)userid doneBlock:(GradeBookDoneBlock)doneBlock;
- (NSArray *)fetchCourseListForUserID:(NSString *)userid;
- (void)requestGradeListForUser:(NSString *)userid course:(NSString *)courseid doneBlock:(GradeBookDoneBlock)doneBlock;
- (void)requestCourseListForUser:(NSString *)userid dataBlock:(GradeBookDataBlock)dataBlock;
- (void)requestGradeBookForClassID:(NSString *)class_id isAscending:(BOOL)isAscending doneBlock:(GradeBookDoneBlock)doneBlock;

- (void)postGradeBookChangeScoreForData:(NSDictionary *)body doneBlock:(GradeBookDoneBlock)doneBlock;
- (void)postGradeBookSetScoreForData:(NSDictionary *)body doneBlock:(GradeBookDoneBlock)doneBlock;

- (NSUInteger)countRecordsForEntity:(NSString *)entity attribute:(NSString *)attribute value:(id)value;
@end
