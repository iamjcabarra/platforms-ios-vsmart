//
//  QuizListViewController.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/5/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "QuizListViewController.h"
#import "QuizListCell.h"
#import "QuizInformation.h"
//#import "QuizResultsController.h"
#import "QuizController.h"
#import "AppDelegate.h"
#import "ResourceManager.h"
#import "AccountInfo.h"
#import "Utils.h"
#import "VSmartValues.h"

//@interface QuizListViewController () <NSFetchedResultsControllerDelegate, QuizInformationDelegate, QuizControllerDelegate, QuizResultsControllerDelegate>

@interface QuizListViewController () <NSFetchedResultsControllerDelegate, QuizInformationDelegate, QuizControllerDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, weak) QuizController *quizController;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) ResourceManager *rm;

@property (strong, nonatomic) IBOutlet UILabel *courseTitle;
@property (strong, nonatomic) IBOutlet UILabel *courseCode;
@property (strong, nonatomic) IBOutlet UILabel *courseDescription;
@property (strong, nonatomic) IBOutlet UILabel *scheduleLabel;
@property (strong, nonatomic) IBOutlet UILabel *sectionLabel;
@property (strong, nonatomic) IBOutlet UILabel *courseInstructor;
@property (strong, nonatomic) IBOutlet UILabel *roomNumber;
@property (strong, nonatomic) IBOutlet UIButton *assessmentButton;

@end

@implementation QuizListViewController

static NSString *const kCouserID = @"9";
static NSString *const kCellIdentifier = @"cell_quiz_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Quiz List";
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.userid = [NSString stringWithFormat:@"%d",  account.user.id ];
    
    self.tableView.estimatedRowHeight = 120.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.rm = [AppDelegate resourceInstance];
    self.managedObjectContext = self.rm.mainContext;
    
    NSManagedObject *mo = [self.rm getEntity:kCourseEntity attribute:@"cs_id" parameter:self.courseid context:self.managedObjectContext];
    
    NSString *course_code = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"course_code"] ];
    NSString *course_name = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"course_name"] ];
    NSString *course_description = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"course_description"] ];
    NSString *course_schedule = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"schedule"] ];
    NSString *course_section = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"section_id"] ];
    NSString *course_venue = [NSString stringWithFormat:@"%@",  [mo valueForKey:@"venue"] ];
    
    self.courseTitle.text = course_name;
    self.courseDescription.text = course_description;
    self.courseCode.text = course_code;
    self.scheduleLabel.text = course_schedule;
    self.sectionLabel.text = course_section;
//    self.courseInstructor.text = @"Ms. Taberna"; //TEACHER API
    self.courseInstructor.text = course_venue;// PLEASE REFACTOR
    self.roomNumber.text = course_venue;
    
    [self.assessmentButton addTarget:self action:@selector(requestQuizList:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self executeLoadQuizList];
}

- (void)requestQuizList:(id)sender {
    [self executeLoadQuizList];
}

- (void)executeLoadQuizList {
    
    [self.rm requestQuizListForUser:self.userid course:self.courseid doneBlock:^(BOOL status) {
        NSLog(@"DONE FETCHING QUIZ...");
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"showQuizDetail"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        NSString *user_id = self.userid;
        NSString *quiz_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"quiz_id"] ];
        NSString *quiz_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"name"] ];
        NSString *course_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"cs_id"] ];

        QuizInformation *information = (QuizInformation *)segue.destinationViewController;
        information.userid = user_id;
        information.quizid = quiz_id;
        information.quizName = quiz_name;
        information.courseid = course_id;
        information.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"startExamination"]) {
        self.quizController = (QuizController *)segue.destinationViewController;
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        NSString *quiz_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"quiz_id"] ];
        NSString *quiz_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"name"] ];
        self.quizController.quizName = quiz_name;
        self.quizController.userid = self.userid;
        self.quizController.courseid = self.courseid;
        self.quizController.quizid = quiz_id;
        self.quizController.delegate = self;
    }
    
//    if ([segue.identifier isEqualToString:@"showResultBox"]) {
//        
//        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//        
//        NSString *user_id = self.userid;
//        NSString *quiz_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"quiz_id"] ];
//        NSString *course_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"cs_id"] ];
//        
//        QuizResultsController *resultsBox = (QuizResultsController *)segue.destinationViewController;
//        resultsBox.userid = user_id;
//        resultsBox.quizid = quiz_id;
//        resultsBox.courseid = course_id;
//        resultsBox.delegate = self;
//    }
    
}

//#pragma mark - <QuizControllerDelegate>
//
//- (void)didFinishSubmittingQuizAnswersForData:(NSDictionary *)data {
//    
//    //POP QUIZ VIEWCONTROLLER
//    [self.navigationController popViewControllerAnimated:YES];
//    
//    //SHOW QUIZ RESULTS VIEWCONTROLLER
//    [self performSegueWithIdentifier:@"showResultBox" sender:self];
//}
//
//- (void)didFinishTappingButtonType:(NSString *)type {
//    
//    NSLog(@"button type : %@", type);
//    
//    if ([type isEqualToString:@"retake"]) {
//        //POP QUIZ VIEWCONTROLLER
//        [self.navigationController popViewControllerAnimated:NO];
//        [self performSegueWithIdentifier:@"startExamination" sender:self];
//    }
//    
//    if ([type isEqualToString:@"review"]) {
//        //POP QUIZ VIEWCONTROLLER
//        [self.navigationController popViewControllerAnimated:NO];
//        [self performSegueWithIdentifier:@"startExamination" sender:self];
//    }
//    
//    if ([type isEqualToString:@"Quiz_List"]) {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self configureCellTable:tableView indexPath:indexPath];
}

- (id)configureCellTable:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    QuizListCell *cell = (QuizListCell *)[tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    
    NSString *attempts_string = [mo valueForKey:@"attempts"];
    NSString *attempts_count_string = [mo valueForKey:@"attempts_count"];
    
    NSInteger attempts_value = [attempts_string integerValue];
    NSInteger attempts_count_value = [attempts_count_string integerValue];
    
    NSString *attempt_message = @"";
    if (attempts_value == attempts_count_value) {
        attempt_message = [NSString stringWithFormat:@"no more attempts"];
    }
    
    cell.courseTitle.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"name"] ];
    cell.courseAttempts.text = attempt_message;
    cell.courseDescription.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"desc"] ];
    cell.courseDuration.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"time_limit"] ];
    
    return cell;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView = tableView;
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *quiz_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"quiz_id"] ];
    NSString *course_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"cs_id"] ];
    NSString *user_id = self.userid;
    
    __weak typeof(self) weakObject = self;
    
    [self.rm requestRunQuizDetailsForUser:user_id quiz:quiz_id course:course_id run:NO doneBlock:^(BOOL status) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            if (status == YES) {
                [weakObject performSegueWithIdentifier:@"showQuizDetail" sender:self];
            }
            
            if (status == NO) {
                NSString *message = @"Quiz attempts exceeded!";
                AlertWithMessageAndDelegate(@"Course", message, self);
            }
            
        });
        
    }];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:kQuizItemEntity inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sort_name_desc = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:NO];
    NSSortDescriptor *sort_date_desc = [NSSortDescriptor sortDescriptorWithKey:@"date_modified" ascending:NO];
    [fetchRequest setSortDescriptors:@[sort_name_desc, sort_date_desc]];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:self.managedObjectContext
                                                                                                  sectionNameKeyPath:nil
                                                                                                           cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView cellForRowAtIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}


#pragma mark - QuizInformation Delegate

- (void)buttonActionType:(NSString *)type {
    
    if ([type isEqualToString:@"start"]) {

        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        NSString *quiz_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"quiz_id"] ];
        NSString *course_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"cs_id"] ];
        NSString *user_id = self.userid;
        
        __weak typeof(self) weakObject = self;
        [self.rm requestRunQuizDetailsForUser:user_id quiz:quiz_id course:course_id run:YES doneBlock:^(BOOL status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakObject performSegueWithIdentifier:@"startExamination" sender:self];
            });

        }];

    }
}

@end
