//
//  SocialStreamGroupCell.m
//  V-Smart
//
//  Created by VhaL on 3/25/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "SocialStreamGroupCell.h"

@interface SocialStreamGroupCell ()
@property (nonatomic, assign) id delegate;

@end

@implementation SocialStreamGroupCell

- (void)initializeCell:(id)delegate{
    self.delegate = delegate;
    
}

@end
