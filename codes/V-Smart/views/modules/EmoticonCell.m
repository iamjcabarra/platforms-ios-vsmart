//
//  EmoticonCell.m
//  V-Smart
//
//  Created by VhaL on 3/24/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "EmoticonCell.h"

@interface EmoticonCell ()
@property (nonatomic, assign) id delegate;
@end

@implementation EmoticonCell

- (void)initializeCell:(id)delegate{
    self.delegate = delegate;
}

@end
