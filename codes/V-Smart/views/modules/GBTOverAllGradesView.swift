//
//  GBTOverAllGradesView.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 03/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTOverAllGradesView: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerLabel: UILabel!
    
    fileprivate let cellIdentifier = "gbt_learning_panel_cell"
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>?
    
    fileprivate var blockOperations: [BlockOperation] = []
    var scrollDelegate : ScrollCommunicationDelegate?
    
    var isAscending = true
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    fileprivate let notification = NotificationCenter.default
    var student_name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notification.addObserver(self, selector: #selector(self.studentSearchAction(_:)), name: NSNotification.Name(rawValue: "GBT_OVERALL_STUDENT_SEARCH"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "GBT_OVERALL_STUDENT_SEARCH"), object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func studentSearchAction(_ notification: Notification) {
        var name = notification.object as? String
        name = (name == nil) ? "" : name
        self.student_name = name!
        self.reloadFetchedResultsController()
    }
    
}

extension GBTOverAllGradesView: UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, NSFetchedResultsControllerDelegate {
    
    /// MARK : - COLLECTION VIEW DATASOURCE AND DELEGATE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! GBTLearningComponentCollectionCell
        
        let mo = self.fetchedResultsController.object(at: indexPath)
        let text = mo.value(forKey: "value") as! String
        cell.backgroundColor = UIColor.white
        cell.customLabel.textColor = UIColor.black
        cell.customLabel.font = UIFont(name: "Helvetica", size: 17)
        if (indexPath as NSIndexPath).section == 0 {
            cell.backgroundColor = UIColor(hex6: 0xecf0f1)
            cell.customLabel.font = UIFont(name: "Helvetica-Bold", size: 17)
            cell.customLabel.text = text
        } else {
            cell.backgroundColor = UIColor.white
            cell.customLabel.font = UIFont(name: "Helvetica", size: 17)
            cell.customLabel.text = "\(text)%"
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func adaptivePresentationStyleForPresentationController(_ controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = gbdm.getMainContext()
        let ascending = self.isAscending
        let entity = GBTConstants.Entity.OVERALLGRADES
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        
        fetchRequest.fetchBatchSize = 20
        
        var finalPredicate = [NSPredicate]()
        if self.student_name.characters.count > 0 {
            let predicateName = NSComparisonPredicate(keyPath: "name", withValue: self.student_name, isExact: false)
            let predicateHeader = NSComparisonPredicate(keyPath: "name", withValue: "00_AA_AA_AA_01", isExact: false)
            
            let namePredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicateName, predicateHeader])
            
            finalPredicate.append(namePredicate)
        }
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: finalPredicate)
        
        
        //TODO FILTER SORT SEGMENTED CONTROL
        let sortIndex = NSSortDescriptor(key: "index", ascending: ascending)
        
        let descriptorSelector = #selector(NSString.localizedCompare(_:))
        let sectionSort = NSSortDescriptor(key: "section_sort", ascending: ascending, selector:descriptorSelector)
        fetchRequest.sortDescriptors = [sectionSort, sortIndex]
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                   managedObjectContext: ctx!,
                                                                   sectionNameKeyPath: "section_sort",
                                                                   cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch _ as NSError {
            //            error = error1
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //println("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    fileprivate func addProcessingBlock(_ processingBlock:@escaping (Void)->Void) {
        blockOperations.append(BlockOperation(block: processingBlock))
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        blockOperations.removeAll(keepingCapacity: false)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
            
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.insertItems(at: [newIndexPath]) }
            
        case .update:
            guard let newIndexPath = newIndexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.reloadItems(at: [newIndexPath]) }
            
        case .move:
            guard let indexPath = indexPath else { return }
            guard let newIndexPath = newIndexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.moveItem(at: indexPath, to: newIndexPath) }
            
        case .delete:
            guard let indexPath = indexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.deleteItems(at: [indexPath]) }
            
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
            
        case .insert:
            addProcessingBlock { [weak self] in self?.collectionView.insertSections(IndexSet(integer: sectionIndex)) }
            
        case .update:
            addProcessingBlock { [weak self] in self?.collectionView.reloadSections(IndexSet(integer: sectionIndex)) }
            
        case .delete:
            addProcessingBlock { [weak self] in self?.collectionView.deleteSections(IndexSet(integer: sectionIndex)) }
            
        default: break
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView.performBatchUpdates({
            self.blockOperations.forEach { $0.start() }
        }, completion: { finished in
            self.blockOperations.removeAll(keepingCapacity: false)
        })
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.collectionView!.reloadData()
        
        
        do {
            try self.fetchedResultsController.performFetch()
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y;
        self.scrollDelegate?.gbScrollViewDidScroll(GBPanelType.LearningPanel, scrollTo: offsetY)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewWillBeginDragging(GBPanelType.LearningPanel)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.LearningPanel)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate == false {
            self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.LearningPanel)
        }
    }
}
