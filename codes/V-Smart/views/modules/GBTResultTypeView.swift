//
//  GBTResultTypeView.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 07/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
import CoreData


class GBTResultTypeView: UIViewController {
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>?
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var testTitleLbl: UILabel!
    
    var showResultTypes = true
    var showEditButton = true
    var dataDict:[String:AnyObject]?
    
    fileprivate let notification = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.editButton.addTarget(self, action: #selector(self.editButtonAction(_:)), for: .touchUpInside)
        self.tableView.tableHeaderView = headerView
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let height = self.tableView.contentSize.height
        self.preferredContentSize = CGSize(width: 150, height: height);
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        /*if showResultTypes == false {
            self.tableView.tableFooterView = footerView
        } else {
            self.tableView.tableFooterView = nil
        }*/
        
        let test_title: String! = self.dataDict!["test_title"] as! String
        self.testTitleLbl.text = test_title
        
        if showEditButton {
            self.tableView.tableFooterView = footerView
        } else {
            self.tableView.tableFooterView = nil
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func editButtonAction(_ b: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.notification.post(name: Notification.Name(rawValue: GBTConstants.Notification.BATCHEDITSCORE), object: self.dataDict)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension GBTResultTypeView: UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        if self.showResultTypes == false {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL_ID", for: indexPath)
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ cell: UITableViewCell, atIndexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: atIndexPath) 
        let name = self.gbdm.stringValue(mo.value(forKey: "name") as AnyObject?)
        
//        let keyPath = optionType.keyPath()
//        guard
//            let name = mo.valueForKey(keyPath) as? String else { return }
        cell.textLabel!.text = name
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*let dataDict: [String:AnyObject] = ["quiz_id":quiz_id as AnyObject,
                                            "component_id":self.component_id as AnyObject,
                                            "term_id":self.term_id as AnyObject,
                                            "index":index as AnyObject]*/
        
        
        let mo = fetchedResultsController.object(at: indexPath)
        
        let result_type_id: String! = self.gbdm.stringValue(mo.value(forKey: "id"))
        let quiz_id: String! = self.gbdm.stringValue(self.dataDict?["quiz_id"])
        let term_id: String! = self.gbdm.stringValue(self.dataDict?["term_id"])
        let component_id: String! = self.gbdm.stringValue(self.dataDict?["component_id"])
        
        let dict: [String:String] = ["result_type_id":result_type_id,
                                     "quiz_id":quiz_id,
                                     "term_id":term_id,
                                     "component_id":component_id]
        
        self.dismiss(animated: true) { 
            self.notification.post(name: Notification.Name(rawValue: "GBT_SORT_GRADEBOOK_CONTENT"), object: dict)//
        }
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = gbdm.getMainContext()
        let ascending = true
        let entity = GBTConstants.Entity.RESULT
        let keyPath = "name"
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
//        let fetchRequest = NSFetchRequest(entityName: entity)
        fetchRequest.fetchBatchSize = 20
        
        let descriptorSelector = #selector(NSString.localizedCompare(_:))
        let sortName = NSSortDescriptor(key: keyPath, ascending: ascending, selector:descriptorSelector)
        fetchRequest.sortDescriptors = [sortName]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        let tableViewController = tableView
        
        switch type {
        case .insert:
            tableViewController?.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableViewController?.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        let tableViewController = tableView
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableViewController?.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = tableViewController?.cellForRow(at: indexPath) {
                    configureCell(cell as! GBTStudentListCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableViewController?.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
