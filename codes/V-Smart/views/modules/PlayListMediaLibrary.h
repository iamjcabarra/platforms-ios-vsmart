//
//  PlayListMediaLibrary.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/26/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@protocol PlayListMediaLibraryDelegate <NSObject>
@optional
- (void)selectedAsset:(ALAsset *)asset;
@end

@interface PlayListMediaLibrary : UIViewController
@property (weak) id <PlayListMediaLibraryDelegate> delegate;
@end
