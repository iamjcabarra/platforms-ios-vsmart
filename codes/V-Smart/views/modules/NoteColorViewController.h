//
//  ColorPopOverViewController.h
//  V-Smart
//
//  Created by VhaL on 2/10/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NColor.h"
#import "NoteDataManager.h"
#import "NoteColorCell.h"

@interface NoteColorViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UIButton *buttonAllColor;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) id delegate;
@property (nonatomic, strong) id parent;
@property (nonatomic, strong) NSArray *colorArray;
@property (nonatomic, strong) Color *selectedColor;

@end
