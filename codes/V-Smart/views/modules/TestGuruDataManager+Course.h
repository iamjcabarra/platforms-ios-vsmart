//
//  TestGuruDataManager+Course.h
//  V-Smart
//
//  Created by Ryan Migallos on 28/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TestGuruDataManager.h"

static NSString *kCoursePlayerTestEntity = @"CoursePlayerTest";
static NSString *kCoursePlayerQuestionListEntity = @"CoursePlayerQuestionList";
static NSString *kCoursePlayerQuestionChoiceEntity = @"CoursePlayerQuestionChoice";
static NSString *kStudentSeatingArrangementEntity = @"StudentSeatingArrangement";

@interface TestGuruDataManager (Course)
- (void)requestRunOrPrerun:(NSString *)runType parameter:(NSDictionary *)parameter dataBlock:(TestGuruDataBlock)dataBlock;
- (void)requestCourseListForUser:(NSString *)userid doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestCourseListForUser:(NSString *)userid dataBlock:(TestGuruDataBlock)dataBlock;
- (void)executeUpdateForEntity:(NSString *)entity details:(NSDictionary *)dictionary predicate:(NSPredicate *)predicate doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestPaginatedCourseListForUser:(NSString *)userid
                withSettingsForPagination:(NSDictionary *)settings
                                dataBlock:(TestGuruDataBlock)dataBlock;
- (void)requestPaginatedTestListForCourse:(NSString *)courseid
                                  andUser:(NSString *)userid
                withSettingsForPagination:(NSDictionary *)settings
                                dataBlock:(TestGuruDataBlock)dataBlock;
//- (void)submitAnswers:(TestGuruDataBlock)dataBlock;
- (void)requestShowResult:(TestGuruDataBlock)dataBlock;
- (void)submitAnswersForQuestionID:(NSString *)question_id timeSpent:(NSString *)timeSpent isFinished:(NSString *)isFinish dataBlock:(TestGuruDataBlock)dataBlock;
- (NSArray *)choicesForQuestionID:(NSString *)question_id;
- (NSString *)stringFromSeconds:(NSInteger)interval;
- (void)saveTimeRemainingForTest:(NSString *)test_id timeRemaining:(NSString *)timeRemaining;
- (NSManagedObject *)fetchFillInTheBlanksAnswerForQuestionID:(NSString *)question_id;

/////// SEATING ARRANGEMENT AND ATTENDANCE //////
- (void)requestSeatingArrangementForCourseWithSectionID:(NSString *)sectionID doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestSeatingArrangement2ForCourseWithSectionID:(NSString *)sectionID dataBlock:(TestGuruDataBlock)dataBlock;
- (void)requestChangeAttendanceStatusWithPostBody:(NSDictionary *)body doneBlock:(TestGuruDoneBlock)doneBlock;
- (void)requestChangeSeatOrderForCourseWithSectionID:(NSString *)sectionID andTeacherID:(NSString *)teacherID withPostBody:(NSDictionary *)body doneBlock:(TestGuruDoneBlock)doneBlock;
- (NSInteger)getLateCount;
- (NSInteger)getAbsentCount;
- (NSInteger)getCountAll;

@end
