//
//  LPMLessonSubDetailsCommentViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 11/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMLessonSubDetailsCommentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {
    
    @IBOutlet fileprivate var commentTextView: UITextView!
    @IBOutlet fileprivate var sendCommentButton: UIButton!
    @IBOutlet fileprivate var tableView: UITableView!
    @IBOutlet fileprivate var emptyPlaceholderView: UIView!
    @IBOutlet fileprivate var emptyPlaceholderLabel: UILabel!
    
    fileprivate let kCellIdentifier = "comment_cell_identifier"
    fileprivate let kSegueIdentifier = "SHOW_COMMENT_MODAL_VIEW"
    
    fileprivate var lpid: String!
    fileprivate var template: String!
    fileprivate var commentModalView: LPMLessonSubDetailsCommentModalViewController!
    fileprivate var selectedCommentObject: NSManagedObject!
    
    // MARK: - Data Manager
    
    fileprivate lazy var lpmDataManager: LPMDataManager = {
        let lpdm = LPMDataManager.sharedInstance
        return lpdm
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 120.0;
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0)
        
        self.lpid = self.lpmDataManager.stringValue(self.lpmDataManager.fetchUserDefaultsObject(forKey: "LPM_USER_DEFAULT_LESSON_PLAN_ID"))
        self.template = self.lpmDataManager.stringValue(self.lpmDataManager.fetchUserDefaultsObject(forKey: "LPM_USER_DEFAULT_TEMPLATE_ID"))
        
        let sendCommentButtonAction = #selector(self.sendCommentButtonAction(_:))
        self.sendCommentButton.addTarget(self, action: sendCommentButtonAction, for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        self.shouldShowEmptyPlaceholderView(sectionData.numberOfObjects > 0 ? false : true)
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kCellIdentifier, for: indexPath) as! LPMLessonSubDetailsCommentTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: LPMLessonSubDetailsCommentTableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath) 
        let avatar = mo.value(forKey: "avatar") as! String
        let first_name = mo.value(forKey: "first_name") as! String
        let last_name = mo.value(forKey: "last_name") as! String
        let date_created = mo.value(forKey: "date_created") as! String
        let date_modified = mo.value(forKey: "date_modified") as! String
        let comment = mo.value(forKey: "comment") as! String
        let user_id = mo.value(forKey: "user_id") as! String
        
        cell.avatarImage.sd_setImage(with: URL(string: avatar))
        cell.nameLabel.text = "\(first_name) \(last_name)"
        cell.timestampLabel.text = "\(date_modified) \((date_created == date_modified) ? "" : NSLocalizedString("Edited", comment: ""))"
        cell.commentLabel.text = comment
        self.justifyLabel(cell.commentLabel)
        
        let own_user_id = self.lpmDataManager.logInUserID()
        let same_user = (user_id == own_user_id) ? true : false
        
        cell.editButton.isHidden = same_user ? false : true
        cell.deleteButton.isHidden = same_user ? false : true
        
        let editCommentButtonAction = #selector(self.editCommentButtonAction(_:))
        cell.editButton.addTarget(self, action: editCommentButtonAction, for: .touchUpInside)
        
        let deleteCommentButtonAction = #selector(self.deleteCommentButtonAction(_:))
        cell.deleteButton.addTarget(self, action: deleteCommentButtonAction, for: .touchUpInside)
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = self.lpmDataManager.getMainContext()
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: LPMConstants.Entity.LESSON_COMMENT)
//        let fetchRequest = NSFetchRequest(entityName: LPMConstants.Entity.LESSON_COMMENT)
        fetchRequest.fetchBatchSize = 20
        
        let sortDescriptor = NSSortDescriptor(key: "sort_date_modified", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.tableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! LPMLessonSubDetailsCommentTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    // MARK: - Button Event Handlers
    
    func sendCommentButtonAction(_ sender: UIButton) {
        if self.commentTextView.text != "" {
            self.view.makeToastActivity()
            self.view.isUserInteractionEnabled = false
    
            let user_id = self.lpmDataManager.logInUserID()
            let comment = self.commentTextView.text
            let body = ["user_id": user_id, "comment": comment!, "is_comment": "1"]
            
            self.lpmDataManager.requestAddCommentToLessonPlanWithID(self.lpid!, body: body as [String : AnyObject], handler: { (doneBlock) in
                if doneBlock {
                    self.lpmDataManager.requestDetailsForLessonPlanWithID(self.lpid, template: self.template, handler: { (doneBlock) in
                        DispatchQueue.main.async(execute: {
                            self.view.hideToastActivity()
                            self.view.isUserInteractionEnabled = true
                            self.commentTextView.text = ""
                        })
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        self.view.hideToastActivity()
                        self.view.isUserInteractionEnabled = true
                        let message = NSLocalizedString("Comment was not successfully posted.", comment: "")
                        self.view.makeToast(message: message, duration: 5.0, position: "Center" as AnyObject)
                    })
                }
            })
        }
    }
    
    func editCommentButtonAction(_ sender: UIButton) {
        self.selectedCommentObject = self.managedObjectFromButton(sender, inTableView: self.tableView)
        
        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: self.kSegueIdentifier, sender: nil)
        })
    }
    
    func deleteCommentButtonAction(_ sender: UIButton) {
        let mo = self.managedObjectFromButton(sender, inTableView: self.tableView)
        guard let commentID = mo.value(forKey: "id") as? String else { return }
        
        let message = NSLocalizedString("Are you sure you want to delete this comment?", comment: "")
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let negativeTitle = NSLocalizedString("No", comment: "")
        let positiveTitle = NSLocalizedString("Yes", comment: "")
        
        let negativeAction = UIAlertAction(title: negativeTitle, style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        let positiveAction = UIAlertAction(title: positiveTitle, style: .default) { (Alert) -> Void in
            self.view.makeToastActivity()
            self.view.isUserInteractionEnabled = false
            self.lpmDataManager.requestDeleteCommentWithID(commentID, handler: { (doneBlock) in
                if doneBlock {
                    self.lpmDataManager.requestDetailsForLessonPlanWithID(self.lpid, template: self.template, handler: { (doneBlock) in
                        DispatchQueue.main.async(execute: {
                            self.view.hideToastActivity()
                            self.view.isUserInteractionEnabled = true
                            alert.dismiss(animated: true, completion: nil)
                        })
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        self.view.hideToastActivity()
                        self.view.isUserInteractionEnabled = true
                        let message = NSLocalizedString("Comment was not successfully deleted.", comment: "")
                        self.view.makeToast(message: message, duration: 5.0, position: "Center" as AnyObject)
                        alert.dismiss(animated: true, completion: nil)
                    })
                }
            })
        }
        
        alert.addAction(negativeAction)
        alert.addAction(positiveAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Managed Object From Button Position
    
    fileprivate func managedObjectFromButton(_ button: UIButton, inTableView: UITableView) -> NSManagedObject {
        let buttonPosition = button.convert(CGPoint.zero, to: inTableView)
        let indexPath = inTableView.indexPathForRow(at: buttonPosition)
        let managedObject = self.fetchedResultsController.object(at: indexPath!)
        
        return managedObject
    }
    
    // MARK: - Empty Placeholder View
    
    fileprivate func shouldShowEmptyPlaceholderView(_ show: Bool) {
        if (show) {
            let message = NSLocalizedString("No available comment.", comment: "")
            self.emptyPlaceholderLabel.text = message
        }
        
        self.emptyPlaceholderView.isHidden = !show
        self.tableView.isHidden = show
    }
    
    // MARK: - UILabel Text Justification
    
    fileprivate func justifyLabel(_ label: UILabel) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        
        let attributedString = NSAttributedString(string: label.text!, attributes: [NSParagraphStyleAttributeName: paragraphStyle, NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)])
        label.attributedText = attributedString
        label.numberOfLines = 0
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.kSegueIdentifier {
            self.commentModalView = segue.destination as? LPMLessonSubDetailsCommentModalViewController
            self.commentModalView.commentObject = self.selectedCommentObject
        }
    }
    
}
