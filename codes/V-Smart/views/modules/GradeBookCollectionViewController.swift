//
//  GradeBookCollectionViewController.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 24/05/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GradeBookCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate, GBClassSelectionPopUpDelegate, UITextFieldDelegate{
    let gbCellIdentifier = "gradebook_cell_indentifier"
    
    // MARK: - Primary View Outlets
    
    @IBOutlet var classNameTextField: UITextField!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var dropDownButton: UIButton!
    @IBOutlet var sortButton: UIButton!
    @IBOutlet var emptyView: UIView!
    @IBOutlet var emptyViewLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Change Score View Outlets
    
    @IBOutlet var changeScoreView: UIView!
    @IBOutlet weak var nameChangeScoreLbl: UILabel!
    @IBOutlet weak var gradeChangeScoreLbl: UILabel!
    @IBOutlet weak var gradeChangeScoreTxtField: UITextField!
    @IBOutlet weak var remarksChangeScoreLbl: UILabel!
    @IBOutlet weak var remarksChangeScoreTxtField: UITextField!
    @IBOutlet weak var saveChangeScoreBtn: UIButton!
    @IBOutlet weak var cancelChangeScoreBtn: UIButton!
    
    // MARK: - Set Score View Outlets
    
    @IBOutlet var setScoreView: UIView!
    @IBOutlet weak var nameSetScoreLbl: UILabel!
    @IBOutlet weak var gradeSetScoreLbl: UILabel!
    @IBOutlet weak var gradeSetScoreTxtField: UITextField!
    @IBOutlet weak var remarksSetScoreLbl: UILabel!
    @IBOutlet weak var remarksSetScoreTxtField: UITextField!
    @IBOutlet weak var expectedSetScoreLbl: UILabel!
    @IBOutlet weak var expectedSetScoreTxtField: UITextField!
    @IBOutlet weak var saveSetScoreBtn: UIButton!
    @IBOutlet weak var cancelSetScoreBtn: UIButton!
    
    fileprivate var classSelectionPopUp: GBClassSelectionPopUp!
    fileprivate var popoverController: UIPopoverController!
    fileprivate var changeScorePopoverController: UIPopoverController!
    fileprivate var setScorePopoverController: UIPopoverController!
    fileprivate var blockOperations: [BlockOperation] = []
    fileprivate var formatter: NumberFormatter!
    
    var managedObjectContext: NSManagedObjectContext? = nil
    var shouldReloadCollectionView : Bool = false
    var selectedClassID: String! = ""
    var isAscending: Bool = true
    var temporarySearchKey: String! = ""
    var selectedData: [String:String]?
    
    // MARK: - Shared Data Manager
    
    fileprivate lazy var dataManager: GradeBookDataManager = {
        let tm = GradeBookDataManager.sharedInstance()
        return tm!
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Main Context
        self.managedObjectContext = self.dataManager.mainContext
        
        // Set Up UI Controls
        self.setUpUIControls()
        
        // Number Formatter
        formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.minimum = 0
        
        // Class Selection Popup
        self.classSelectionPopUp = GBClassSelectionPopUp (nibName: "GBClassSelectionPopUp", bundle: nil)
        self.classSelectionPopUp.delegate = self
        self.popoverController = UIPopoverController (contentViewController: self.classSelectionPopUp)
        self.popoverController.contentSize = CGSize(width: self.classNameTextField.frame.width, height: 300)
        
        // Change Score Popup
        let changeScoreViewController = UIViewController()
        changeScoreViewController.view = changeScoreView
        self.changeScorePopoverController = UIPopoverController (contentViewController: changeScoreViewController)
        self.changeScorePopoverController.contentSize = CGSize(width: 300, height: 250)
        
        // Set Score Popup
        let setScoreViewController = UIViewController()
        setScoreViewController.view = setScoreView
        self.setScorePopoverController = UIPopoverController (contentViewController: setScoreViewController)
        self.setScorePopoverController.contentSize = CGSize(width: 300, height: 317)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialView()
    }
    
    // MARK: - Class Primary Methods
    
    func setUpUIControls() {
        // Empty View
        self.emptyView.isHidden = true
        self.emptyViewLabel.text = NSLocalizedString("No available data.", comment: "")
        
        // Searh Bar
        self.searchBar.placeholder = NSLocalizedString("Search", comment: "")
        self.searchBar.delegate = self
        
        // Text Field Delegates
        self.gradeChangeScoreTxtField.delegate = self;
        self.gradeSetScoreTxtField.delegate = self;
        self.expectedSetScoreTxtField.delegate = self;
        
        // Text Field Keyboard Type
        self.gradeChangeScoreTxtField.keyboardType = UIKeyboardType.numberPad
        self.gradeSetScoreTxtField.keyboardType = UIKeyboardType.numberPad
        self.expectedSetScoreTxtField.keyboardType = UIKeyboardType.numberPad
        
        // Label and Button String Localization
        self.gradeSetScoreLbl.text = "\(NSLocalizedString("Grade", comment: "")) :"
        self.gradeChangeScoreLbl.text = "\(NSLocalizedString("Grade", comment: "")) :"
        self.remarksSetScoreLbl.text = "\(NSLocalizedString("Remarks", comment: "")) :"
        self.remarksChangeScoreLbl.text = "\(NSLocalizedString("Remarks", comment: "")) :"
        self.expectedSetScoreLbl.text = "\(NSLocalizedString("Expected Grade", comment: "")) :"
        
        self.saveSetScoreBtn.setTitle(NSLocalizedString("Save", comment: ""), for: UIControlState())
        self.saveSetScoreBtn.setTitle(NSLocalizedString("Save", comment: ""), for: .highlighted)
        self.saveChangeScoreBtn.setTitle(NSLocalizedString("Save", comment: ""), for: UIControlState())
        self.saveChangeScoreBtn.setTitle(NSLocalizedString("Save", comment: ""), for: .highlighted)
        self.cancelChangeScoreBtn.setTitle(NSLocalizedString("Cancel", comment: ""), for: UIControlState())
        self.cancelChangeScoreBtn.setTitle(NSLocalizedString("Cancel", comment: ""), for: .highlighted)
        self.cancelSetScoreBtn.setTitle(NSLocalizedString("Cancel", comment: ""), for: UIControlState())
        self.cancelSetScoreBtn.setTitle(NSLocalizedString("Cancel", comment: ""), for: .highlighted)
        
        // Drop Down
        let dropDownAction = #selector(self.dropDownButtonAction(_:))
        self.dropDownButton.addTarget(self, action: dropDownAction, for: .touchUpInside)
        
        // Sort
        let sortButtonAction = #selector(self.sortButtonAction(_:))
        self.sortButton.addTarget(self, action: sortButtonAction, for: .touchUpInside)
        
        let saveScoreAction = #selector(self.saveScoreButtonAction(_:))
        self.saveChangeScoreBtn.addTarget(self, action: saveScoreAction, for: .touchUpInside)
        self.saveSetScoreBtn.addTarget(self, action: saveScoreAction, for: .touchUpInside)
        
        let cancelScoreAction = #selector(self.cancelScoreButtonAction(_:))
        self.cancelChangeScoreBtn.addTarget(self, action: cancelScoreAction, for: .touchUpInside)
        
        let cancelSetScoreAction = #selector(self.cancelSetScoreButtonAction(_:))
        self.cancelSetScoreBtn.addTarget(self, action: cancelSetScoreAction, for: .touchUpInside)
    }
    
    func setUpInitialView() {
        let userid = self.dataManager.loginUser() as String
        
        self.dataManager.requestCourseList(forUser: userid) { (data) in
            if (data != nil) {
                let classid = data?["id"] as! String
                let section_name = data?["section_name"] as! String
                let course_name = data?["course_name"] as! String
                
                DispatchQueue.main.async(execute: {
                    self.classNameTextField.text = section_name + " - " + course_name
                self.selectedClassID = classid
                self.loadGradeBook(classid)
                })
                
                //self.selectedClassID = classid
                //self.loadGradeBook(classid)
            }
            else {
                self.dataManager.clearContents(forEntity: kGradeBookEntityV2, predicate: nil)
                
                DispatchQueue.main.async(execute: {
                    self.emptyView.isHidden = false
                })
            }
        }
    }
    
    func loadGradeBook(_ classID: String) {
        HUD.showUIBlockingIndicator(withText: "\(NSLocalizedString("Please wait", comment: ""))...")
        
        self.dataManager.requestGradeBook(forClassID: classID, isAscending: self.isAscending) { (doneBlock) in
            if (doneBlock) {
                DispatchQueue.main.async(execute: {
                    HUD.hideUIBlockingIndicator()
                    self.emptyView.isHidden = true
                    self.collectionView.reloadData()
                })
            }
            else {
                DispatchQueue.main.async(execute: {
                    HUD.hideUIBlockingIndicator()
                    self.emptyView.isHidden = false
                })
            }
        }
    }
    
    // MARK: - UI Control Actions
    
    func dropDownButtonAction(_ sender: UIButton) {
        _ = [self.popoverController .present(from: sender.bounds, in: sender, permittedArrowDirections: UIPopoverArrowDirection.right, animated: true)]
    }
    
    func sortButtonAction(_ sender: UIButton) {
        self.isAscending = self.isAscending ? false : true
        self.loadGradeBook(self.selectedClassID)
    }
    
    // MARK: - Search Bar Delegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.temporarySearchKey = searchBar.text
        self.reloadFetchedResultsController()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.temporarySearchKey = searchBar.text
        self.reloadFetchedResultsController()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.temporarySearchKey = ""
        self.reloadFetchedResultsController()
    }
    
    // MARK: - Class Selection PopUp Delegate
    
    func selectedClassData(_ option: NSDictionary) {
        let classid = option["id"] as! String
        let section_name = option["section_name"] as! String
        let course_name = option["course_name"] as! String
        
            self.classNameTextField.text = section_name + " - " + course_name
        self.selectedClassID = classid
            self.loadGradeBook(classid)
    }
    
    // MARK: - Collection View Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        let noItems = sectionInfo.numberOfObjects
        return noItems
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let object = self.fetchedResultsController.object(at: indexPath)
        let cell_value = object.value(forKey: "cell_value") as? String
        
        if (indexPath as NSIndexPath).section == 0 {
            let gbCell : GradeBookCollectionViewCell = collectionView .dequeueReusableCell(withReuseIdentifier: gbCellIdentifier, for: indexPath) as! GradeBookCollectionViewCell
            gbCell.cellLabel.font = UIFont.systemFont(ofSize: 13)
            gbCell.cellLabel.textColor = UIColor.white
            gbCell.cellLabel.text = cell_value
            gbCell.backgroundColor = UIColor(hex6: 0x4274b9)
            gbCell.cellLabel.textAlignment = .center
            gbCell.changeScoreButton.isHidden = true
            
            return gbCell
        }
        else {
            let gbCell : GradeBookCollectionViewCell = collectionView .dequeueReusableCell(withReuseIdentifier: gbCellIdentifier, for: indexPath) as! GradeBookCollectionViewCell
            gbCell.cellLabel.font = UIFont.systemFont(ofSize: 13)
            gbCell.cellLabel.textColor = UIColor.black
            gbCell.cellLabel.text = cell_value
            
            if (indexPath as NSIndexPath).section % 2 != 0 {
                gbCell.backgroundColor = UIColor(white: 242/255.0, alpha: 1.0)
            }
            else {
                gbCell.backgroundColor = UIColor.white
            }
            
            if (indexPath as NSIndexPath).row != 0 {
                gbCell.changeScoreButton.isHidden = false
                let changeScoreButtonAction = #selector(self.modifyScoreButtonAction(_:))
                gbCell.changeScoreButton.addTarget(self, action: changeScoreButtonAction, for: UIControlEvents.touchUpInside)
                gbCell.cellLabel.textAlignment = .center
            }
            
            if (indexPath as NSIndexPath).row == 0 {
                gbCell.cellLabel.textAlignment = .left
                gbCell.changeScoreButton.isHidden = true
            }
        
            return gbCell
        }
    }
    
    // MARK: - Score Management Related Methods
        
    func modifyScoreButtonAction(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.collectionView)
        let tempIndexPath = self.collectionView.indexPathForItem(at: buttonPosition)
        
        let incremIndexPath = self.incrementRow(tempIndexPath!)
        
        let object = self.fetchedResultsController.object(at: incremIndexPath)

        let class_id = (object.value(forKey: "class_id") as AnyObject).description
        let participant_id = (object.value(forKey: "participant_id") as AnyObject).description
        let deploy_id = (object.value(forKey: "deploy_id") as AnyObject).description
        let exp_score = (object.value(forKey: "exp_score") as AnyObject).description
        let took_test = (object.value(forKey: "took_test") as AnyObject).description
        
        self.selectedData = [
                            "class_id":class_id!,
                            "participant_id":participant_id!,
                            "deploy_id":deploy_id!,
                            "exp_score":exp_score!,
                            "took_test":took_test!
                            ]
        
        let student_name = (object.value(forKey: "student_name") as AnyObject).description
        let activity_score = (object.value(forKey: "score") as AnyObject).description
        let remark_activity = (object.value(forKey: "remark_activity") as AnyObject).description
        
        if (took_test == "0") {
            DispatchQueue.main.async {
                self.nameSetScoreLbl.text = student_name;
                self.gradeSetScoreTxtField.text = activity_score;
                self.remarksSetScoreTxtField.text = remark_activity;
                self.expectedSetScoreTxtField.text = exp_score;
            }
            
            _ = [self.setScorePopoverController.present(from: sender.bounds, in: sender, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)]
        }
        else {
            DispatchQueue.main.async {
                self.nameChangeScoreLbl.text = student_name;
                self.gradeChangeScoreTxtField.text = activity_score;
                self.remarksChangeScoreTxtField.text = remark_activity;
                self.expectedSetScoreTxtField.text = exp_score;
            }
            
            _ = [self.changeScorePopoverController.present(from: sender.bounds, in: sender, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)]
        }
    }
    
    func saveScoreButtonAction(_ sender: UIButton) {
        /*
        let took_test = self.selectedData!["took_test"] as String?
        HUD.showUIBlockingIndicator(withText: "\(NSLocalizedString("Loading", comment: ""))...")
        
        if took_test == "1" {
            self.changeScoreAction()
        } else {
            self.setScoreAction()
        }
        */
        
        DispatchQueue.main.async {
            let took_test = self.selectedData!["took_test"] as String?
            HUD.showUIBlockingIndicator(withText: "\(NSLocalizedString("Loading", comment: ""))...")
            
            if took_test == "1" {
                self.changeScoreAction()
            } else {
                self.setScoreAction()
            }
        }
    }
    
    func changeScoreAction() {
        let class_id = self.selectedData!["class_id"] as String?
        let participant_id = self.selectedData!["participant_id"] as String?
        let deploy_id = self.selectedData!["deploy_id"] as String?
        let score = self.gradeChangeScoreTxtField.text
        let expected_score = self.selectedData!["exp_score"] as String?
        let remarks = self.remarksChangeScoreTxtField.text
        
        let postBodyDict:[String:String] =
            [
                "class_id": class_id!,
                "participant_id": participant_id!,
                "deploy_id": deploy_id!,
                "score": (score!.characters.count == 0) ? "0" : score!,
                "expected_score": (expected_score!.characters.count == 0) ? "0" : expected_score!,
                "remarks": remarks!
        ]
        
        self.dataManager.postGradeBookChangeScore(forData: postBodyDict) { (status) in
            /*
            self.changeScorePopoverController.dismiss(animated: true)
            HUD.hideUIBlockingIndicator()
            
            if status {
                self.loadGradeBook(class_id!)
            } else {
                self.failAlert()
            }
            */
            
            DispatchQueue.main.async {
                self.changeScorePopoverController.dismiss(animated: true)
                HUD.hideUIBlockingIndicator()
                
                if status {
                    self.loadGradeBook(class_id!)
                } else {
                    self.failAlert()
                }
            }
        }
    }
    
    func setScoreAction() {
        let class_id = self.selectedData!["class_id"] as String?
        let participant_id = self.selectedData!["participant_id"] as String?
        let deploy_id = self.selectedData!["deploy_id"] as String?
        let score = self.gradeSetScoreTxtField.text
        let expected_score = self.expectedSetScoreTxtField.text
        let remarks = self.remarksSetScoreTxtField.text
        
        let postBodyDict:[String:String] =
            [
                "class_id": class_id!,
                "participant_id": participant_id!,
                "deploy_id": deploy_id!,
                "score": (score!.characters.count == 0) ? "0" : score!,
                "expected_score": (expected_score!.characters.count == 0) ? "0" : expected_score!,
                "remarks": remarks!
        ]
        
        self.dataManager.postGradeBookSetScore(forData: postBodyDict) { (status) in
            /*
            self.setScorePopoverController.dismiss(animated: true)
            HUD.hideUIBlockingIndicator()
            if status {
                self.loadGradeBook(class_id!)
            } else {
                self.failAlert()
            }
            */
            
            DispatchQueue.main.async {
            self.setScorePopoverController.dismiss(animated: true)
            HUD.hideUIBlockingIndicator()
            if status {
                self.loadGradeBook(class_id!)
            } else {
                self.failAlert()
            }
        }
    }
    }
    
    func cancelScoreButtonAction(_ sender: UIButton) {
        self.changeScorePopoverController.dismiss(animated: true);
    }
    
    func cancelSetScoreButtonAction(_ sender: UIButton) {
        self.setScorePopoverController.dismiss(animated: true);
    }
    
    func failAlert() {
        let errorAlert = UIAlertController(title: "", message: "Unable to update score.", preferredStyle: .alert)
        
        errorAlert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: { (Alert) -> Void in
            _ = self.navigationController?.popViewController(animated: true)
        }))
        self.present(errorAlert, animated: true, completion: nil)
    }
    
    func incrementRow(_ indexPath: IndexPath) -> IndexPath{
        var row = (indexPath as NSIndexPath).row
        let sec = (indexPath as NSIndexPath).section
        
        row += 1
        
        let newIndexPath = IndexPath(item: row, section: sec);
        
        return newIndexPath
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let countdots = textField.text!.components(separatedBy: ".").count - 1
        
        if countdots > 0 && string == "." {
            return false
        }
        
        // Create an `NSCharacterSet` set which includes everything *but* the digits
        let inverseSet = CharacterSet(charactersIn:"0123456789.").inverted
        
        // At every character in this "inverseSet" contained in the string,
        // split the string up into components which exclude the characters
        // in this inverse set
        let components = string.components(separatedBy: inverseSet)
        
        // Rejoin these components
        let filtered = components.joined(separator: "")  // use join("", components) if you are using Swift 1.2
        
        // If the original string is equal to the filtered string, i.e. if no
        // inverse characters were present to be eliminated, the input is valid
        // and the statement returns true; else it returns false
        return string == filtered
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        //let fetchRequest = NSFetchRequest()
        //let entity = NSEntityDescription.entity(forEntityName: kGradeBookEntityV2, in: self.managedObjectContext!)
        //
        //fetchRequest.entity = entity
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kGradeBookEntityV2)
        fetchRequest.fetchBatchSize = 20
        
        if (self.temporarySearchKey != "") {
            let predicate1 = self.dataManager.predicate(forKeyPathContains: "search_key", value: self.temporarySearchKey)
            let predicate2 = self.dataManager.predicate(forKeyPathContains: "search_key", value: "C02MM6N0FH01")
            let predicates = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate1!, predicate2!])
            fetchRequest.predicate = predicates
        }
        
        let section_index = NSSortDescriptor(key: "section_index", ascending: true)
        let index = NSSortDescriptor(key: "index", ascending: true)
        
        fetchRequest.sortDescriptors = [section_index, index]
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: "section_index", cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch _ as NSError {
            //            error = error1
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //println("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    
    
    fileprivate func addProcessingBlock(_ processingBlock:@escaping (Void)->Void) {
        blockOperations.append(BlockOperation(block: processingBlock))
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        blockOperations.removeAll(keepingCapacity: false)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
            
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.insertItems(at: [newIndexPath]) }
            
        case .update:
            guard let newIndexPath = newIndexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.reloadItems(at: [newIndexPath]) }
            
        case .move:
            guard let indexPath = indexPath else { return }
            guard let newIndexPath = newIndexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.moveItem(at: indexPath, to: newIndexPath) }
            
        case .delete:
            guard let indexPath = indexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.deleteItems(at: [indexPath]) }
            
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
                
        case .insert:
            addProcessingBlock { [weak self] in self?.collectionView.insertSections(IndexSet(integer: sectionIndex)) }
            
        case .update:
            addProcessingBlock { [weak self] in self?.collectionView.reloadSections(IndexSet(integer: sectionIndex)) }
            
        case .delete:
            addProcessingBlock { [weak self] in self?.collectionView.deleteSections(IndexSet(integer: sectionIndex)) }
            
        default: break
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView.performBatchUpdates({
            self.blockOperations.forEach { $0.start() }
            }, completion: { finished in
                self.blockOperations.removeAll(keepingCapacity: false)
        })
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.collectionView!.reloadData()
        
        do {
            try self.fetchedResultsController.performFetch()
        }
        catch {
            print(error.localizedDescription)
        }
    }
}
