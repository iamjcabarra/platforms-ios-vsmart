//
//  LPMHistoryTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 04/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMHistoryTableViewCell: UITableViewCell {
    
    @IBOutlet var userAvatarImage: UIImageView!
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var timeStampLabel: UILabel!
    @IBOutlet var historyLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
