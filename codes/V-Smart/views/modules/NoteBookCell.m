//
//  NoteBookCell.m
//  V-Smart
//
//  Created by VhaL on 3/1/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NoteBookCell.h"
#define kMaxLimit 20

@interface NoteBookCell()
@property (nonatomic, strong) NSArray *colors;
@end

@implementation NoteBookCell

@synthesize noteBookLabel, noteBookTextField, acceptButton, cancelButton, shareIndicatorImageView, delegate, imageView, selectedColorId;

-(void)initializeCell{
    [self hideEditControls:YES];
    [self hideShareIndicator:YES];
}

-(IBAction)acceptButtonTapped:(id)sender{
    
    NSString *newNotebookName = [noteBookTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([[newNotebookName lowercaseString] isEqualToString:@"general"]){
        
        /* localizable strings */
        NSString *nameCannotBeUsed = NSLocalizedString(@"This name cannot be used", nil); //checked
        [self makeToast:nameCannotBeUsed duration:2.0f position:@"center"];
        
        return;
    }
    
    [self hideEditControls:YES];
    
    noteBookLabel.text = newNotebookName;
    
    if ([delegate respondsToSelector:@selector(buttonEditNoteBookTapped:)])
    {
        [delegate performSelector:@selector(buttonEditNoteBookTapped:) withObject:self];
    }
}

-(IBAction)cancelButtonTapped:(id)sender{
    [self hideEditControls:YES];
}

-(void)hideEditControls:(bool)hide{
    acceptButton.hidden  = hide;
    cancelButton.hidden = hide;
    noteBookTextField.hidden = hide;
    noteBookLabel.hidden = !hide;
    
    if (hide){
        [noteBookTextField resignFirstResponder];
    }
    else{
        [noteBookTextField becomeFirstResponder];
    }
}

-(void)hideShareIndicator:(bool)hide{
    shareIndicatorImageView.hidden = hide;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self acceptButtonTapped:acceptButton];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)text{
    if(text.length == 0){
        if(textField.text.length != 0)
            return YES;
    }
    else if(kMaxLimit <= textField.text.length){
        return NO;
    }
    return YES;
}


-(void)updateNotebookColor{
    
    if (self.colors == nil)
        self.colors = [[NoteDataManager sharedInstance] getColors:YES useMainThread:YES];
    
    for (Color *color in self.colors) {
        if (color.colorId.intValue == selectedColorId){
            imageView.backgroundColor = [[NoteDataManager sharedInstance] getUIColorFromColorObject:color];
            break;
        }
    }
}
@end
