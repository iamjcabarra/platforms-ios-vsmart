//
//  CPMCourseCategoryTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 14/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class CPMCourseCategoryTableViewCell: UITableViewCell {
    
    @IBOutlet var courseCategoryNameLabel: UILabel!
    @IBOutlet var courseCategoryInitialLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
