//
//  CPEmbeddedStudentListView.h
//  V-Smart
//
//  Created by Julius Abarra on 30/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPEmbeddedStudentListView : UIViewController

@property (strong, nonatomic) NSManagedObject *courseObject;

@end
