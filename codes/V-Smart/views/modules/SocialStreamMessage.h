//
//  SocialStreamMessage.h
//  V-Smart
//
//  Created by Ryan Migallos on 1/22/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialStreamMessage : UIViewController

@property (nonatomic, assign) NSUInteger groupid;
@property (nonatomic, strong) NSMutableDictionary *mdictionary;
@property (nonatomic, strong) NSString *message_id;
@property (nonatomic, strong) NSString *icon_url;
@property (nonatomic, strong) NSString *icon_text;


- (IBAction)unwindFromConfirmationForm:(id)sender;
- (IBAction)sendUserMessage:(id)sender;

@end
