//
//  GBTeacherCustomGradeCell.m
//  V-Smart
//
//  Created by Julius Abarra on 24/05/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "GBTeacherCustomGradeCell.h"

@implementation GBTeacherCustomGradeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
