//
//  LPMLessonTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 22/06/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMLessonTableViewCell: UITableViewCell {
    
    @IBOutlet var lessonTitleLabel: UILabel!
    @IBOutlet var lessonTimeFrameLabel: UILabel!
    @IBOutlet var lessonStatusLabel: UILabel!
    @IBOutlet var showActionPopoverButton: UIButton!
    @IBOutlet var actionPopoverButtonExtension: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func changeLessonStatusLabel(_ status: String) {
        var statusString = ""
        
        if (status == "0") {
            statusString = NSLocalizedString("Draft", comment: "")
        }
        else if (status == "1") {
            statusString = NSLocalizedString("for Approval", comment: "")
        }
        else {
            statusString = NSLocalizedString("Approved", comment: "")
        }
        
        self.lessonStatusLabel.text = statusString
    }
}
