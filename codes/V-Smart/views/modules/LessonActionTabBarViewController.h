//
//  LessonActionTabBarViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 9/22/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonActionTabBarViewController : UIViewController

- (void)swapViewControllers:(NSString *)segueIdentifier;

@end
