//
//  Networkable.swift
//  V-Smart
//
//  Created by Julius Abarra on 09/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

protocol Networkable {
    var isProductionMode : Bool { get }
}

extension Networkable {
    
    // MARK: - Application Default URLs

    func appBaseURL() -> String! {
        if let server = UserDefaults.standard.string(forKey: "baseurl_preference") {
            return server.trimmingCharacters(in: CharacterSet.whitespaces)
        }
        
        return ""
    }
    
    func appServiceURL() -> String! {
        if let server = UserDefaults.standard.string(forKey: "serviceurl_preference") {
            return server.trimmingCharacters(in: CharacterSet.whitespaces)
        }
        
        return ""
    }
    
    func appBookURL() -> String! {
        if let server = UserDefaults.standard.string(forKey: "bookurl_preference") {
            return server.trimmingCharacters(in: CharacterSet.whitespaces)
        }
        
        return ""
    }
    
    // MARK: - Building Request Helpers
    
    func buildRequestURLFromEndPoint(_ endPoint: String) -> URL? {
        var path = "http://\(appBaseURL()!)\(endPoint)" as NSString
        
        if (path.range(of: "vsmart-rest-dev").length > 0) {
            if (isProductionMode) {
                path = path.replacingOccurrences(of: appBaseURL()!, with: appServiceURL()!) as NSString
                path = path.replacingOccurrences(of: "vsmart-rest-dev", with: "") as NSString
                path = path.replacingOccurrences(of: "//v1", with: "/v1") as NSString
                path = path.replacingOccurrences(of: "//v2", with: "/v2") as NSString
            }
        }
        
        if let url = URL(string: path as String) {
            print("Built url: \(url)")
            return url
        }
        
        return nil
    }
    
    func buildURLRequestForMethod(_ method: String, url: URL, body: [String: Any]?) -> URLRequest! {
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = method
        
        if let postBody = body {
            do {
                let data = try JSONSerialization.data(withJSONObject: postBody, options: JSONSerialization.WritingOptions(rawValue: 0))
                request.httpBody = data
                
                if let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    print("json string: <start>----\(jsonString)---<end>")
                }
                else {
                    print("Can't assemble json string!")
                }
            }
            catch let error {
                print("Error building url request with body: \(error)")
            }
        }
        
        return request as URLRequest
    }
    
}
