//
//  CurriculumDetailView.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/15/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurriculumDetailView : UIViewController

@property (nonatomic, strong) NSManagedObject *mo;

@end
