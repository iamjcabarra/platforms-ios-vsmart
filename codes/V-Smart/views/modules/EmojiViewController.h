//
//  EmoItemViewController.h
//  V-Smart
//
//  Created by VhaL on 3/24/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMSEmoItem.h"

@interface EmojiViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) NSArray *array;
@property (nonatomic, assign) id parent;
@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) JMSRecordEmoItem *emoItems;
-(void)loadData;
@end
