//
//  LPMSMDCollapsibleContentController.swift
//  V-Smart
//
//  Created by Julius Abarra on 13/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMSMDCollapsibleContentController: UITableViewController, UITextViewDelegate {
    
    @IBOutlet fileprivate var subDetailsView: UIView!
    @IBOutlet fileprivate var segmentedControl: UISegmentedControl!
    
    fileprivate let kViewSwapperSegueIdentifier = "SHOW_SUB_DETAILS_VIEW_SWAPPER"
    fileprivate let kCommentSegueIdentifier = "SHOW_COMMENT_VIEW"
    fileprivate let kHistorySegueIdentifier = "SHOW_HISTORY_VIEW"
    
    fileprivate var viewSwapper: LPMLessonSubDetailsViewSwapper!
    fileprivate var viewMode: String!
    fileprivate var tablePosition: CGPoint!

    // MARK: - Data Manager
    
    fileprivate lazy var lpmDataManager: LPMDataManager = {
        let lpdm = LPMDataManager.sharedInstance
        return lpdm
    }()
    
    // MARK: - Data Structure
    
    fileprivate struct Section {
        var header: String
        var contentObject: NSManagedObject
        var collapsed: Bool
        
        init(header: String, contentObject: NSManagedObject, collapsed: Bool = false) {
            self.header = header
            self.contentObject = contentObject
            self.collapsed = collapsed
        }
    }
    
    fileprivate var sections = [Section]()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedSectionHeaderHeight = 50.0
        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        
        self.tableView.estimatedRowHeight = 260.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.setUpSMDTemplateCollapsibleContents()
        self.viewMode = self.lpmDataManager.stringValue(self.lpmDataManager.fetchUserDefaultsObject(forKey: "LPM_USER_DEFAULT_IS_VIEW_MODE"))
        
        if self.viewMode == "1" {
            self.subDetailsView.isHidden = false
            self.setupSegmentedControl()
        }
        else {
            self.subDetailsView.isHidden = true
        }
        
        // Force calling viewWillAppear
        // to make UITableViewController
        // behave as expected (keyboard
        // should not cover editing
        // component
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    // MARK: Build Table View Data
    
    fileprivate func setUpSMDTemplateCollapsibleContents() {
        guard let contents = self.lpmDataManager.fetchSMDTemplateContents() else {
            print("can't retrieve smd template contents")
            return
        }
        
        self.sections.removeAll()
        
        for object in contents {
            if let stage_name = object.value(forKey: "stage_name") as? String {
                self.sections.append(Section(header: stage_name, contentObject: object, collapsed: false))
            }
        }
        
        DispatchQueue.main.async(execute: {
            self.view.endEditing(true)
            UIView.transition(with: self.tableView, duration: 0.3, options: .transitionCrossDissolve, animations: {self.tableView.reloadData()}, completion: nil)
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Segmented Control
    
    fileprivate func setupSegmentedControl() {
        self.segmentedControl.removeAllSegments()
        self.segmentedControl.tintColor = UIColor(rgba: "#B5F0FF")
        
        let titleA = NSLocalizedString("Comment", comment: "")
        let titleB = NSLocalizedString("History", comment: "")
        
        self.segmentedControl.insertSegment(withTitle: titleA, at: 0, animated: true)
        self.segmentedControl.insertSegment(withTitle: titleB, at: 1, animated: true)
        
        let attributeA = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16), NSForegroundColorAttributeName: UIColor(rgba: "#00AEEF")]
        self.segmentedControl.setTitleTextAttributes(attributeA, for: UIControlState.selected)
        
        let attributeB = [NSFontAttributeName: UIFont.systemFont(ofSize: 16), NSForegroundColorAttributeName: UIColor(rgba: "#00AEEF")]
        self.segmentedControl.setTitleTextAttributes(attributeB, for: UIControlState())
        
        let action = #selector(self.segmentedControlAction(_:))
        self.segmentedControl.addTarget(self, action: action, for: .valueChanged)
        
        self.segmentedControl.selectedSegmentIndex = 0
        self.switchSegmentedViewAtIndex(0)
    }
    
    fileprivate func switchSegmentedViewAtIndex(_ index: Int) {
        DispatchQueue.main.async(execute: {
            let svcSegueIdentifier = index == 0 ? self.kCommentSegueIdentifier : self.kHistorySegueIdentifier
            self.viewSwapper.swapToViewControllerWithSegueIdentifier(svcSegueIdentifier)
        })
    }
    
    func segmentedControlAction(_ sender: UISegmentedControl) {
        self.switchSegmentedViewAtIndex(sender.selectedSegmentIndex)
    }
    
    // MARK: - Table View Data Source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.sections[section].collapsed == false {
            return 0
        }
        else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contentCell = tableView.dequeueReusableCell(withIdentifier: "body_cell_identifier", for: indexPath) as! LPMSMDCollapsibleContentBodyCell
        let contentObject = self.sections[(indexPath as NSIndexPath).section].contentObject
        
        let description = contentObject.value(forKey: "content_description") as! String
        contentCell.descriptionLabel.text = description
        self.justifyLabel(contentCell.descriptionLabel)
        
        let content = contentObject.value(forKey: "content") as! String
        contentCell.contentTextView.text = content
        
        let id = contentObject.value(forKey: "id") as! Int
        contentCell.contentTextView.tag = id
        contentCell.contentTextView.delegate = self
        contentCell.enableEditing(self.viewMode == "1" ? false : true)
      
        return contentCell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "header_cell_identifier") as! LPMSMDCollapsibleContentHeaderCell
        let header = self.sections[section].header
        let collapsed: Bool = self.sections[section].collapsed
        
        headerCell.stageNameLabel.text = header
        headerCell.collapseButton.tag = section
        headerCell.collapseImage.image = collapsed ? UIImage(named: "Minus_Math_50.png") : UIImage(named: "Plus_Math_50.png")
        headerCell.collapseButton.addTarget(self, action: #selector(self.toggleCollapseButton(_:)), for: .touchUpInside)
        
        _ = headerCell.contentView.intrinsicContentSize
        headerCell.contentView.layer.borderColor = UIColor.white.cgColor
        headerCell.contentView.layer.borderWidth = 1.0
        
        return headerCell.contentView
    }
    
    // MARK: - Text View Delegate
    
    func textViewDidChange(_ textView: UITextView) {
        let predicate = NSComparisonPredicate(keyPath: "id", withValue: textView.tag, isExact: true)
        let data = ["content": textView.text as NSString]
        self.saveChangedData(data, predicate: predicate)
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.tablePosition = self.tableView.contentOffset
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        DispatchQueue.main.async {
            self.tableView.setContentOffset(self.tablePosition, animated: true)
        }
    }
    
    // MARK: - Collapse Button Event Handler
    
    func toggleCollapseButton(_ sender: UIButton) {
        let section = sender.tag
        let collapsed = self.sections[section].collapsed
        self.sections[section].collapsed = !collapsed
        
        DispatchQueue.main.async(execute: {
            self.view.endEditing(true)
            UIView.transition(with: self.tableView, duration: 0.0, options: .transitionCrossDissolve, animations: { self.tableView.reloadData() }, completion: nil)
        })
    }
    
    // MARK: - UILabel Text Justification
    
    fileprivate func justifyLabel(_ label: UILabel) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        
        let attributedString = NSAttributedString(string: label.text!, attributes: [NSParagraphStyleAttributeName: paragraphStyle, NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)])
        label.attributedText = attributedString
        label.numberOfLines = 0
    }
    
    // MARK: - Saving Local Changes
    
    fileprivate func saveChangedData(_ data: [String: AnyObject], predicate: NSPredicate) {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            _ = self.lpmDataManager.updateEntity(LPMConstants.Entity.SMD_TEMPLATE_CONTENT, predicate: predicate, data: data as NSDictionary)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.kViewSwapperSegueIdentifier {
            self.viewSwapper = segue.destination as? LPMLessonSubDetailsViewSwapper
        }
    }

}
