//
//  LPMConstants.swift
//  V-Smart
//
//  Created by Julius Abarra on 23/06/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import Foundation

// MARK: - General Settings

struct LPMConstants {
    
    struct DateFormat {
        static let SERVER: String = "yyyy-MM-dd HH:mm:ss"
        static let LESSON_DETAILS: String = "yyyy-MM-dd"
        static let LESSON_COMMENT: String = "MMM. dd, yyyy, hh:mm a"
        static let LESSON_HISTORY: String = "MMM. dd, yyyy, hh:mm a"
        static let LESSON_CREATOR: String = "EEEE, MMMM dd, yyyy"
    }
    
    struct Entity {
        static let SMD_TEMPLATE: String = "LPMSMDTemplate"
        static let SMD_TEMPLATE_CONTENT: String = "LPMSMDTemplateContent"
        static let DLL_TEMPLATE: String = "LPMDLLTemplate"
        static let DLL_TEMPLATE_CONTENT: String = "LPMDLLTemplateContent"
        static let DLL_TEMPLATE_CONTENT_DAY: String = "LPMDLLTemplateContentDay"
        static let DLL_TEMPLATE_CONTENT_ELEMENT: String = "LPMDLLTemplateContentElement"
        static let LESSON_OVERVIEW: String = "LPMLessonOverview"
        static let LESSON_COMMENT: String = "LPMLessonComment"
        static let LESSON_HISTORY: String = "LPMLessonHistory"
        static let PRE_ASSOCIATED_COMPETENCY: String = "LPMPreAssociatedCompetency"
        static let DOWNLOADED_FILE_MAPPER: String = "LPMDownloadedFileMapper"
    }
    
    struct Sandbox {
        static let MAX_DIRECTORY_SIZE: UInt = 167772160
        static let DIRECTORY_PREFIX: String = "LPM"
    }
    
    struct Server {
        static let MAX_VERSION: CGFloat = 2.5
    }
    
    struct Template {
        static let UBD: String = "1"
        static let SMD: String = "2"
        static let DLL: String = "3"
    }

}

// MARK: - Course List View

enum LPMCourseListRequestType: Int {
    case load = 1
    case search
    case refresh
}

// MARK: - Lesson List View

enum LPMLessonActionType: Int {
    case add = 1
    case edit
    case submit
    case download
    case delete
    case open
}

enum LPMLessonListRequestType: Int {
    case load = 1
    case search
    case refresh
}

// MARK: - Lesson Details

enum LPMLessonOverviewTimeFrame: Int {
    case startDate = 100
    case endDate = 200
}

protocol LPMRoutes: ParseProtocol, NetworkProtocol {
    func route(_ method: String, uri: String, body: [String:Any]?) -> URLRequest
    func route(_ method: String, uri: String, query: [String:AnyObject]?, body: [String: Any]?) -> URLRequest
    func routeForFormData(_ uri: String, body: [String: Any]) -> URLRequest
}

extension LPMRoutes {
    func route(_ method: String, uri: String, body: [String: Any]?) -> URLRequest {
        let endpoint = "vsmart-rest-dev" + "\(uri)"
        let url = buildURLFromRequestEndPoint(endpoint as NSString)
        let request = buildURLRequestWithMethod(method as NSString, url: url, andBody: body as AnyObject?)
        return request
    }
    
    func route(_ method: String, uri: String, query: [String: AnyObject]?, body: [String: Any]?) -> URLRequest {
        let endpoint = "vsmart-rest-dev" + "\(uri)"
        var url = buildURLFromRequestEndPoint(endpoint as NSString)
        
        if let dictionary = query {
            var queryVariables = [URLQueryItem]()
            
            for (name, object) in dictionary {
                let value = object as! String
                let queryObject = URLQueryItem(name: name, value: value)
                queryVariables.append(queryObject)
            }
            
            var urlComponents = URLComponents(string: url.absoluteString)!
            urlComponents.queryItems = queryVariables
            url = urlComponents.url!
        }
        
        let request = buildURLRequestWithMethod(method as NSString, url: url, andBody: body as AnyObject?)
        return request
    }
    
    func routeForFormData(_ uri: String, body: [String: Any]) -> URLRequest {
        let endpoint = "vsmart-rest-dev" + "\(uri)"
        let url = buildURLFromRequestEndPoint(endpoint as NSString)
        let request = buildFormDataURLRequestWithURL(url, body: body as AnyObject)
        return request
    }
    
}
