//
//  CurriculumBrowserView.h
//  V-Smart
//
//  Created by Julius Abarra on 18/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CurriculumBrowserViewDelegate <NSObject>

@required
- (void)updateDownloadedFileCount;

@end

@interface CurriculumBrowserView : UIViewController

@property (weak, nonatomic) id <CurriculumBrowserViewDelegate> delegate;
- (NSMutableArray *)getCurriculumCollection;

@end
