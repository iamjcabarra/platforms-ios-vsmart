//
//  TestPlayerList.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/3/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "TestPlayerList.h"
#import "QuizListViewController.h"
#import "AppDelegate.h"
#import "ResourceManager.h"
#import "CourseDataManager.h"
#import "TestPlayerListCell.h"
#import "StudentViewController.h"

#define ResultsTableView self.searchResultsTableViewController.tableView

@interface TestPlayerList ()

<NSFetchedResultsControllerDelegate, UISearchResultsUpdating, UISearchControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *navigationHeaderView;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) ResourceManager *rm;
@property (nonatomic, strong) CourseDataManager *cm;


////////// SEARCH CAPABILITIES //////////
@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) UITableViewController *searchResultsTableViewController;
@property (strong, nonatomic) NSArray *results;

@end

@implementation TestPlayerList

static NSString *const kCellIdentifier = @"cell_course_identifier";

- (AccountInfo *)getAccountObject {
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    return account;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.title = @"Course List";
    
    self.tableView.estimatedRowHeight = 44.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.rm = [AppDelegate resourceInstance];
    self.managedObjectContext = self.rm.mainContext;
 
    [self loadCourList];
    
    [self setupSearchCapabilities];
}

- (void)loadCourList {
    
    AccountInfo *account = [self getAccountObject];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
//    [self.rm requestCourseListForUserID:user_id doneBlock:^(BOOL status) {
//        //do nothing
//    }];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self customizeNavigationController];
}

- (void)customizeNavigationController {
    
    UIColor *color = UIColorFromHex(0xEC5466);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    } else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

- (void)setupSearchCapabilities {
    
    self.results = [[NSMutableArray alloc] init];
    
    // A table view for results.
    UITableView *searchResultsTableView = [[UITableView alloc] initWithFrame:self.tableView.frame];
    searchResultsTableView.dataSource = self;
    searchResultsTableView.delegate = self;
    
    // Registration of reuse identifiers.
    [searchResultsTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kCellIdentifier];
    
    // Init a search results table view controller and setting its table view.
    self.searchResultsTableViewController = [[UITableViewController alloc] init];
    self.searchResultsTableViewController.tableView = searchResultsTableView;
    
    // Init a search controller with its table view controller for results.
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsTableViewController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    
    [[UISearchBar appearance] setBackgroundColor:[UIColor whiteColor]];
    [[UISearchBar appearance] setBarTintColor:UIColorFromHex(0xE3E3E3)];
    
    // Make an appropriate size for search bar and add it as a header view for initial table view.
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // Enable presentation context.
    self.definesPresentationContext = YES;
}

#pragma mark - Search Results Updating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    UISearchBar *searchBar = searchController.searchBar;
    
    if (searchBar.text.length > 0) {
        
        NSString *text = searchBar.text;
        
        NSArray *fetchedObjects = self.fetchedResultsController.fetchedObjects;
        
        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(NSManagedObject *mo, NSDictionary *bindings) {
            
            NSString *object = [NSString stringWithFormat:@"%@", [mo valueForKey:@"search_string"]];
            NSStringCompareOptions options = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;
            NSRange range = [object rangeOfString:text options:options];
            
            return range.location != NSNotFound;
        }];
        
        // Set up results.
        self.results = [fetchedObjects filteredArrayUsingPredicate:predicate];;
        
        // Reload search table view.
        [self.searchResultsTableViewController.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSString *cs_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"cs_id"] ];
        NSString *user_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ];
    NSString *course_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"course_name"]];
    NSString *course_code = [NSString stringWithFormat:@"%@", [mo valueForKey:@"course_code"]];
    NSString *course_desc = [NSString stringWithFormat:@"%@", [mo valueForKey:@"course_description"]];
    NSString *section = [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_id"]];
    NSString *schedule = [NSString stringWithFormat:@"%@", [mo valueForKey:@"schedule"]];
    NSString *room = [NSString stringWithFormat:@"%@", [mo valueForKey:@"venue"]];

    if ([segue.identifier isEqualToString:@"showQuizList"]) {
        QuizListViewController *ql = (QuizListViewController *)segue.destinationViewController;
        ql.courseid = cs_id;
        ql.userid = user_id;
    }
    
    if ([segue.identifier isEqualToString:@"showStudentList"]) {
        NSDictionary *d = @{@"cs_id": cs_id,
                            @"course_name": course_name,
                            @"course_code": course_code,
                            @"course_desc": course_desc,
                            @"section": section,
                            @"schedule": schedule,
                            @"room": room
                            };
        
        StudentViewController *sl = (StudentViewController *)segue.destinationViewController;
        sl.courseDictionary = d;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = 1;
    
    if ([tableView isEqual:ResultsTableView]) {
        return count;
    } else {
        return [[self.fetchedResultsController sections] count];
    }
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    if ([tableView isEqual:ResultsTableView]) {
        
        if (self.results) {
            return self.results.count;
        } else {
            return count;
        }
        
    } else {
        
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        count = [sectionInfo numberOfObjects];
        
    }
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    if ([tableView isEqual:ResultsTableView]) {
        [self configureFilteredCell:cell indexPath:indexPath];
    }
    
    if (![tableView isEqual:ResultsTableView]) {
        [self configureCell:(TestPlayerListCell *)cell indexPath:indexPath];
    }
    
    return cell;
}

- (void)configureFilteredCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = self.results[indexPath.row];
    
    NSString *course_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"course_name"]];
    cell.textLabel.text = course_name;
}

- (void)configureCell:(TestPlayerListCell *)cell indexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.courseTitle.text = [mo valueForKey:@"course_name"];
    cell.courseDescription.text = [mo valueForKey:@"course_description"];
    cell.courseSchedule.text = [mo valueForKey:@"schedule"];
    cell.courseRoom.text = [mo valueForKey:@"venue"];
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.tableView = tableView;
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *user_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ];
    NSString *cs_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"cs_id"] ];
    
    AccountInfo *account = [self getAccountObject];
    NSString *position = [NSString stringWithFormat:@"%@", account.user.position];
    __weak typeof(self) weakObject = self;
    
    if ([position isEqualToString:@"teacher"]) {
        self.cm = [CourseDataManager sharedInstance];
        self.managedObjectContext = self.cm.mainContext;
        
        [self.cm requestStudentListForCourseWithID:cs_id doneBlock:^(BOOL status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakObject performSegueWithIdentifier:@"showStudentList" sender:self];
            });
        }];
    }
    
    if ([position isEqualToString:@"student"]) {
    [self.rm requestQuizListForUser:user_id course:cs_id doneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakObject performSegueWithIdentifier:@"showQuizList" sender:self];
        });
    }];
    }
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:kCourseEntity inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
        
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"course_name" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:self.managedObjectContext
                                                                                                  sectionNameKeyPath:nil
                                                                                                           cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView cellForRowAtIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
