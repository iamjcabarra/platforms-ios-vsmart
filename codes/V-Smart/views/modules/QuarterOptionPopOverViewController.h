//
//  QuarterOptionPopOverViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuarterOptionPopOverViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *butOption1;
@property (strong, nonatomic) IBOutlet UIButton *butOption2;
@property (strong, nonatomic) IBOutlet UIButton *butOption3;
@property (strong, nonatomic) IBOutlet UIButton *butOption4;

@end
