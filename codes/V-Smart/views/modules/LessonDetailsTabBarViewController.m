//
//  LessonDetailsTabBarViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/18/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "LessonDetailsTabBarViewController.h"
#import "LessonDetailsCommentViewController.h"
#import "LessonDetailsHistoryViewController.h"

#define SegueIdentifierComment @"embedCommentTab"
#define SegueIdentifierHistory @"embedHistoryTab"

@interface LessonDetailsTabBarViewController ()

@property (nonatomic, strong) NSString *currentSegueIdentifier;

@end

@implementation LessonDetailsTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set current segue identifier and perform initial segue using this identifier
    self.currentSegueIdentifier = SegueIdentifierComment;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:SegueIdentifierComment]) {
        LessonDetailsCommentViewController *comment = (LessonDetailsCommentViewController *)[segue destinationViewController];
        comment.lesson_mo = self.lesson_mo;
        
        if (self.childViewControllers.count > 0) {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
        }
        else {
            [self addChildViewController:segue.destinationViewController];
            ((UIViewController *)segue.destinationViewController).view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:((UIViewController *)segue.destinationViewController).view];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:SegueIdentifierHistory]) {
        LessonDetailsHistoryViewController *history = (LessonDetailsHistoryViewController *)[segue destinationViewController];
        history.lesson_mo = self.lesson_mo;
        
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController {
    
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
    }];
}

- (void)swapViewControllers:(NSString *)segueIdentifier {
    [self performSegueWithIdentifier:segueIdentifier sender:nil];
}

@end
