//
//  Topic.m
//  V-Smart
//
//  Created by Ryan Migallos on 10/3/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "Topic.h"
#import "Course.h"


@implementation Topic

@dynamic filename;
@dynamic index;
@dynamic title;
@dynamic course;

@end
