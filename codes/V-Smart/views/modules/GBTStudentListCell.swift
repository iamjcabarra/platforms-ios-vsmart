//
//  GBTStudentListCell.swift
//  V-Smart
//
//  Created by Ryan Migallos on 15/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTStudentListCell: UITableViewCell {

    @IBOutlet var customLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
