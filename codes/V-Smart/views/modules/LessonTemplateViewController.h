//
//  LessonTemplateViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonTemplateViewController : UIViewController

@property (strong, nonatomic) NSString *courseid;
@property (strong, nonatomic) NSString *gradeLevel;

@end
