//
//  SSGroupMemberListViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 01/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

// MARK: - Array Extension

extension Array where Element: Equatable {
    func arrayRemovingObject(_ object: Element) -> [Element] {
        return filter { $0 != object }
    }
}

class SSGroupMemberListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {
    
    @IBOutlet fileprivate var groupField: UITextField!
    @IBOutlet fileprivate var tableView: UITableView!
    @IBOutlet fileprivate var headerLabel: UILabel!
    @IBOutlet fileprivate var addPeopleView: UIView!
    @IBOutlet fileprivate var addPeopleLabel: UILabel!
    @IBOutlet fileprivate var addPeopleButton: UIButton!
    @IBOutlet fileprivate var cancelButton: UIButton!
    @IBOutlet fileprivate var saveButton: UIButton!
    
    var actionType: SSMGroupActionType!
    
    fileprivate let notification = NotificationCenter.default
    fileprivate let cellIdentifier = "group_member_cell_identifier"
    fileprivate let segueIdentifier = "SEGUE_PEOPLE_SELECTION"
    
    fileprivate var currentMembers = [String]()
    fileprivate var position = "teacher"
    fileprivate var isEditable = false
    
    fileprivate lazy var ssdm: SocialStreamDataManager = {
        let dataManager = SocialStreamDataManager.sharedInstance
        return dataManager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_ACCESSIBILITY_MODE"), object: "0")
        
        let groupName = ssdm.stringValue(ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_NAME"))
        var groupID = ssdm.stringValue(ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID"))
        groupID = actionType != .create ? groupID : ""
        
        position = self.ssdm.accountUserPosition().lowercased()
        isEditable = groupID != "0" && position != "student" ? true : false
        
        headerLabel.text = NSLocalizedString("Students", comment: "")
        groupField.placeholder = NSLocalizedString("Please enter group name", comment: "")
        groupField.text = actionType != .create ? groupName : ""
        groupField.isUserInteractionEnabled = isEditable
        
        let cancelText = NSLocalizedString("Cancel", comment: "").uppercased()
        cancelButton.setTitle(cancelText, for: UIControlState())
        cancelButton.setTitle(cancelText, for: .selected)
        cancelButton.setTitle(cancelText, for: .highlighted)
        
        let cancelButtonAction = #selector(cancelButtonAction(_:))
        cancelButton.addTarget(self, action: cancelButtonAction, for: .touchUpInside)
        
        saveButton.isHidden = !isEditable
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.allowUserInteraction], animations: {
            self.addPeopleView.isHidden = !self.isEditable
            }, completion: { (animate) in
        })
        
        if isEditable {setUpEditableUIObjects()}
        
        listMembersForGroup(groupID!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let cancelEditingAction = #selector(cancelEditingAction(_:))
        notification.addObserver(self, selector: cancelEditingAction, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_CANCEL_EDITING"), object: nil)
        
        let changeGroupButtonAction = #selector(changeGroupButtonAction(_:))
        notification.addObserver(self, selector: changeGroupButtonAction, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_CHANGE_GROUP_NAME"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_CANCEL_EDITING"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_CHANGE_GROUP_NAME"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Editable UI Objects
    
    fileprivate func setUpEditableUIObjects() {
        let saveText = NSLocalizedString("Save", comment: "")
        let updateText = NSLocalizedString("Update", comment: "")
        let title = actionType == .create ? saveText.uppercased() : updateText.uppercased()
        
        saveButton.setTitle(title, for: UIControlState())
        saveButton.setTitle(title, for: .selected)
        saveButton.setTitle(title, for: .highlighted)
        
        let saveButtonAction = #selector(saveButtonAction(_:))
        saveButton.addTarget(self, action: saveButtonAction, for: .touchUpInside)
        
        addPeopleLabel.text = NSLocalizedString("Add People", comment: "")
        let addPeopleButtonAction = #selector(addPeopleButtonAction(_:))
        addPeopleButton.addTarget(self, action: addPeopleButtonAction, for: .touchUpInside)
    }
    
    // MARK: - Group Member List
    
    fileprivate func listMembersForGroup(_ groupID: String) {
        if groupID != "0" {
            ssdm.requestMembersForGroup(groupID) { (doneBlock) in
                DispatchQueue.main.async(execute: {
                    self.currentMembers = self.retrieveGroupMembersNew(true)
                    self.reloadFetchedResultsController()
                })
            }
        }
        else {
            let sectionID = ssdm.stringValue(ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_SECTION_ID"))
            ssdm.requestStudentsForSection(sectionID!) { (doneBlock) in
                if doneBlock {
                    self.ssdm.deepCopyStudentEntity({ (doneBlock) in
                        if doneBlock {
                            DispatchQueue.main.async(execute: {
                                self.reloadFetchedResultsController()
                            })
                        }
                    })
                }
            }
        }
    }
    
    // MARK: - Button Event Handler
    
    func addPeopleButtonAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: segueIdentifier, sender: self)
    }
    
    func cancelButtonAction(_ sender: UIButton) {
        let groupID = ssdm.stringValue(ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID"))
        let displayConfirmation = actionType == .create ? position == "teacher" : (position == "teacher" && groupID != "0")
        
        if displayConfirmation {
            let actionText = actionType == .create ? NSLocalizedString("creating", comment: "") : NSLocalizedString("updating", comment: "")
            let questionA = NSLocalizedString("Are you sure you want to cancel", comment: "")
            let questionB = NSLocalizedString("this group?", comment: "")
            let questionC = "\(questionA) \(actionText) \(questionB)"
            
            self.showAlertViewWithPolarQuestion(questionC, completion: { (allow) in
                if allow {
                    DispatchQueue.main.async(execute: {
                        self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_ACCESSIBILITY_MODE"), object: "1")
                        self.ssdm.save(object: "0" as AnyObject!, forKey: "SS_GROUP_IS_EDIT_MODE")
                        _ = self.navigationController?.popViewController(animated: true)
                    })
                }
            })
        }
        else {
            DispatchQueue.main.async(execute: {
                self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_ACCESSIBILITY_MODE"), object: "1")
                self.ssdm.save(object: "0" as AnyObject!, forKey: "SS_GROUP_IS_EDIT_MODE")
                _ = self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    func saveButtonAction(_ sender: UIButton) {
        guard let groupName = groupField.text, groupName != "" else {
            groupField.layer.borderColor = UIColor.red.cgColor
            groupField.layer.borderWidth = 1.0
            return
        }
        
        let stringA = "\(NSLocalizedString("Creating", comment: ""))..."
        let stringB = "\(NSLocalizedString("Updating", comment: ""))..."
        let indicatorString = actionType == .create ?  stringA : stringB
        self.view.makeToastActivity(message: indicatorString)
        
        let userID = ssdm.accountUserID()
        var addedMembers = retrieveGroupMembersNew(true)
        let removedMembers = retrieveGroupMembersNew(false)
        
        // Remove current members from newly added members
        for nm in addedMembers {
            for cm in currentMembers {
                if nm == cm {
                    addedMembers = addedMembers.arrayRemovingObject(nm)
                }
            }
        }
        
        if actionType == .create {
            let groupID: String! = ssdm.stringValue(ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_SECTION_ID"))
            let body: [String: Any] = ["name": groupName, "user_id": userID, "group_id": groupID, "members": addedMembers]
            
            ssdm.requestCreateGroupWithPostBody(body, handler: { (doneBlock) in
                if doneBlock {
                    DispatchQueue.main.async(execute: {
                        self.view.hideToastActivity()
                        self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_ACCESSIBILITY_MODE"), object: "1")
                        self.ssdm.save(object: "0" as AnyObject!, forKey: "SS_GROUP_IS_EDIT_MODE")
                        _ = self.navigationController?.popViewController(animated: true)
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        self.view.hideToastActivity()
                        let message = NSLocalizedString("There was an error creating this group. Please try again later.", comment: "")
                        self.showAlertViewMessage(message)
                    })
                }
            })
        }
        
        if actionType == .update {
            let groupID = ssdm.stringValue(ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID"))
            let body: [String: AnyObject] = ["name": groupName as AnyObject, "user_id": userID as AnyObject, "members": addedMembers as AnyObject, "deleted_members": removedMembers as AnyObject]
            
            ssdm.requestUpdateGroup(groupID!, body: body, handler: { (doneBlock) in
                if doneBlock {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_ACCESSIBILITY_MODE"), object: "1")
                        self.ssdm.save(object: groupName as AnyObject!, forKey: "SS_SELECTED_GROUP_NAME")
                        self.ssdm.save(object: "0" as AnyObject!, forKey: "SS_GROUP_IS_EDIT_MODE")
                        _ = self.navigationController?.popViewController(animated: true)
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        let message = NSLocalizedString("There was an error updating this group. Please try again later.", comment: "")
                        self.showAlertViewMessage(message)
                    })
                }
            })
        }
    }
    
    // MARK: - Retrieve Group Members
    
    fileprivate func retrieveGroupMembersNew(_ new: Bool) -> [String] {
        let value = new ? "0" : "1"
        let predicate = ssdm.db.createPredicate(key: "is_removed", withValue: value, isExact: true)
        
        guard let members = ssdm.db.retrieveEntity(SSMConstants.Entity.MEMBER, properties: ["id"], filter: predicate, sortKey: "id", ascending: true) as [AnyObject]? else {
            return []
        }
        
        var ids = [String]()
        
        for member in members {
            if let id = member["id"] as? String {
                ids.append(id)
            }
        }
        
        return ids
    }
    
    // MARK: - Notification Center
    
    func changeGroupButtonAction(_ notification: Notification) {
        let groupName = ssdm.stringValue(ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_NAME"))
        self.groupField.text = groupName
    }
    
    func cancelEditingAction(_ notification: Notification) {
        self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_ACCESSIBILITY_MODE"), object: "1")
        self.ssdm.save(object: "0" as AnyObject!, forKey: "SS_GROUP_IS_EDIT_MODE")
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Alert View Message
    
    fileprivate func showAlertViewMessage(_ message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let title = NSLocalizedString("Okay", comment: "")
        
        let action = UIAlertAction(title: title, style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func showAlertViewWithPolarQuestion(_ question: String, completion: @escaping (_ allow: Bool) -> Void) {
        let alert = UIAlertController(title: "", message: question, preferredStyle: .alert)
        
        let positiveString = NSLocalizedString("Yes", comment: "")
        let negativeString = NSLocalizedString("No", comment: "")
        
        let positiveAction = UIAlertAction(title: NSLocalizedString(positiveString, comment: ""), style: .default) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                completion(true)
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        let negativeAction = UIAlertAction(title: NSLocalizedString(negativeString, comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                completion(false)
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(positiveAction)
        alert.addAction(negativeAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SSGroupMemberTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: SSGroupMemberTableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        
        guard
            let avatar = mo.value(forKey: "avatar") as? String,
            let first_name = mo.value(forKey: "first_name") as? String,
            let last_name = mo.value(forKey: "last_name") as? String
            else { return }
        
        cell.memberImage.sd_setImage(with: URL.init(string: avatar))
        cell.memberName.text = "\(first_name) \(last_name)"
        
        let removeButtonAction = #selector(removeButtonAction(_:))
        cell.removeButton.addTarget(self, action: removeButtonAction, for: .touchUpInside)
        cell.removeButton.isHidden = !isEditable
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = ssdm.getMainContext()
        
        //let fetchRequest = NSFetchRequest(entityName: SSMConstants.Entity.MEMBER)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: SSMConstants.Entity.MEMBER)
        fetchRequest.fetchBatchSize = 20
        
        let predicate = ssdm.db.createPredicate(key: "is_removed", withValue: "0", isExact: true)
        fetchRequest.predicate = predicate
        
        let descriptorSelector = #selector(NSString.localizedCaseInsensitiveCompare(_:))
        let sortDescriptor = NSSortDescriptor.init(key: "search_string", ascending: true, selector:descriptorSelector)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.tableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! SSGroupMemberTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Remove Member from Group
    
    func removeButtonAction(_ sender: UIButton) {
        guard let groupMemberObject = managedObjectFromButton(sender, inTableView: tableView) else {
            return
        }
        
        guard
            let id = groupMemberObject.value(forKey: "id") as? String,
            let firstName = groupMemberObject.value(forKey: "first_name") as? String,
            let lastName = groupMemberObject.value(forKey: "last_name") as? String
            else { return }
        
        let messageA = NSLocalizedString("Are you about to remove", comment: "")
        let messageB = NSLocalizedString("This action cannot be undone.", comment: "")
        let messageC = "\(messageA) \(firstName) \(lastName). \(messageB)"
        
        let positiveString = NSLocalizedString("Continue", comment: "")
        let negativeString = NSLocalizedString("Cancel", comment: "")
        
        let alert = UIAlertController(title: "", message: messageC, preferredStyle: .alert)
        
        let positiveAction = UIAlertAction(title: NSLocalizedString(positiveString, comment: ""), style: .default) { (Alert) -> Void in
            self.ssdm.removeGroupMember(id, handler: { (doneBlock) in
                DispatchQueue.main.async(execute: {
                    if doneBlock {
                        self.reloadFetchedResultsController()
                    }
                    else {
                        let message = NSLocalizedString("There was an error removing member from this group. Please try again.", comment: "")
                        self.showAlertViewMessage(message)
                    }
                    
                    alert.dismiss(animated: true, completion: nil)
                })
            })
        }
        
        let negativeAction = UIAlertAction(title: NSLocalizedString(negativeString, comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(positiveAction)
        alert.addAction(negativeAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func managedObjectFromButton(_ button: UIButton, inTableView: UITableView) -> NSManagedObject? {
        let buttonPosition = button.convert(CGPoint.zero, to: inTableView)
        
        guard let indexPath = inTableView.indexPathForRow(at: buttonPosition) else {
            return nil
        }
        
        /*guard let managedObject = self.fetchedResultsController.object(at: indexPath) else {
            return nil
        }*/
        
        let managedObject = self.fetchedResultsController.object(at: indexPath)
        
        return managedObject
    }
    
}
