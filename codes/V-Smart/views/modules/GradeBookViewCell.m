//
//  GradeBookViewCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/9/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "GradeBookViewCell.h"
#import <QuartzCore/QuartzCore.h>

@interface GradeBookViewCell()

@property (strong, nonatomic) IBOutlet UIView *viewCellFrame;

@end

@implementation GradeBookViewCell

- (void)awakeFromNib {
    // Initialization code
    
    CALayer *layer = self.viewCellFrame.layer;
    layer.borderColor = [UIColor lightGrayColor].CGColor;
    layer.borderWidth = 1.0f;
    
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
