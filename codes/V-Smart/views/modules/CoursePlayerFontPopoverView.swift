//
//  CoursePlayerFontPopoverView.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 02/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

//protocol CoursePlayerFontPopoverDelegate: class {
//    func selectedTestOption (option:NSInteger)
//}



class CoursePlayerFontPopoverView: UIViewController {
    

    @IBOutlet weak var fontSizeSlider: UISlider!
    @IBOutlet weak var fontAdjustmentLabel: UILabel!
//    weak var delegate:CoursePlayerFontPopoverDelegate?
    
    
    // MARK: - Shared Data Manager
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tm = TestGuruDataManager.sharedInstance()
        return tm!
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
//        [self.slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        self.fontSizeSlider.addTarget(self, action: #selector(self.fontSizeSliderAction(_:)), for: .valueChanged)
        self.fontSizeSlider.isContinuous = false
        
        
        // Do any additional setup after loading the view.
    }

    
    func fontSizeSliderAction(_ slider:UISlider) {
        
        print("FONT SIZE SET TO \(slider.value)")
        
        self.dataManager.save("\(slider.value)", forKey: kCP_SELECTED_FONT_SIZE)
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: kNotificationCoursePlayerFontSizeChanged), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
