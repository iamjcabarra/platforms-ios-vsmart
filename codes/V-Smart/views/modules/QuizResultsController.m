//
//  QuizResultsController.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/17/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "QuizResultsController.h"
#import "AppDelegate.h"
#import "AccountInfo.h"
#import "Utils.h"
#import "VSmartValues.h"

//// BUTTON CATEGORY ////
@interface UIButton (CUSTOM)
- (void)setColor:(UIColor *)color forState:(UIControlState)state;
@end

@implementation UIButton (CUSTOM)
- (void)setColor:(UIColor *)color forState:(UIControlState)state {
    
    UIView *colorView = [[UIView alloc] initWithFrame:self.frame];
    colorView.backgroundColor = color;
    UIGraphicsBeginImageContext(colorView.bounds.size);
    [colorView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:state];
}
@end
//// BUTTON CATEGORY ////

@interface QuizResultsController ()
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UILabel *attemptLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UIButton *buttonRetake;
@property (strong, nonatomic) IBOutlet UIButton *buttonReview;
@property (strong, nonatomic) ResourceManager *rm;

//You can take this exam 9 more times
//You took 15 mins to finish the exam

@end

@implementation QuizResultsController

- (AccountInfo *)getAccountObject {
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    return account;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self setupLeftButton];
    
    self.rm = [AppDelegate resourceInstance];
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.userid = [NSString stringWithFormat:@"%d",  account.user.id ];
    
    self.rm = [AppDelegate resourceInstance];
    NSMutableDictionary *d = (NSMutableDictionary *)[self.rm fetchQuizDataForQuizID:self.quizid];
    NSLog(@"NSMutableDictionary data : %@", d);
    
    NSString *attempts = [NSString stringWithFormat:@"%@", d[@"attempts"] ];
    NSLog(@"attempts : %@", attempts);
    NSString *attempts_count = [NSString stringWithFormat:@"%@", d[@"attempts_count"] ];
    NSLog(@"attempts_count : %@", attempts_count);
    NSString *time_limit = [NSString stringWithFormat:@"You have %@ to complete the exam", d[@"time_limit"] ];
    NSLog(@"time_limit : %@", time_limit);
    NSString *total_time_consumed = [NSString stringWithFormat:@"%@", d[@"total_time_consumed"] ];
    
    NSString *passing_rate = [NSString stringWithFormat:@"The passing score for this exam is %@%%", d[@"passing_rate"] ];
    NSLog(@"passing_rate : %@", passing_rate);
    NSString *total_score = [NSString stringWithFormat:@"%@", d[@"total_score"] ];
    NSLog(@"total_score : %@", total_score);
    NSString *total_points = [NSString stringWithFormat:@"%@", d[@"total_points"] ];
    NSLog(@"total_points : %@", total_points);
    
    
    NSString *message_status = @"You Failed!";
    float percent = ([total_score floatValue] / [total_points floatValue]) * 100;
    NSString *actual_passing_rate = [NSString stringWithFormat:@"%.0f", percent];
    
    NSInteger apr = [actual_passing_rate integerValue];
    NSInteger pr = [d[@"passing_rate"] integerValue];
    if (apr > pr) {
        message_status = @"You Passed!";
    }
    NSLog(@"message status : %@", message_status);

    self.scoreLabel.text = [NSString stringWithFormat:@"%@/%@", total_score, total_points];
    
    NSInteger remaining_attempt = [attempts integerValue] - [attempts_count integerValue];
    NSString *message;
    
    if (remaining_attempt > 1) {
        message = [NSString stringWithFormat:@"You can take this exam %ld more times", (long)remaining_attempt];
    }
    else if (remaining_attempt == 1) {
        message = [NSString stringWithFormat:@"You can take this exam %ld more time", (long)remaining_attempt];
    }
    else {
        message = @"You cannot take this exam anymore";
    }
    
    self.attemptLabel.text = message;
    self.messageLabel.text = message_status;
    self.timeLabel.text = [NSString stringWithFormat:@"You took %@ to finish the exam", total_time_consumed];
    
    [self.buttonRetake addTarget:self action:@selector(buttonQuizResultAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonReview addTarget:self action:@selector(buttonQuizResultAction:) forControlEvents:UIControlEventTouchUpInside];
    
//    self.buttonRetake.selected = YES;
//    self.buttonRetake.userInteractionEnabled = NO;
//    NSInteger attempt_count = [attempts integerValue];
    
    if (remaining_attempt > 0) {
        NSLog(@"marami pang attempt!!!");
        self.buttonRetake.selected = NO;
        self.buttonRetake.userInteractionEnabled = YES;
    }
    else {
        NSLog(@"wala ng attempt!!!");
        self.buttonRetake.selected = YES;
        self.buttonRetake.userInteractionEnabled = NO;
    }
    
    UIColor *active = UIColorFromHex(0x3498db);
    UIColor *inActive = [UIColor lightGrayColor];
    [self.buttonRetake setColor:active forState:UIControlStateNormal];
    [self.buttonRetake setColor:inActive forState:UIControlStateSelected];
}

- (void)setupLeftButton {
    
    self.navigationItem.hidesBackButton = YES;
    
    NSString *title_string = @"< Quiz List";
    UIBarButtonItem *backToListButton = [[UIBarButtonItem alloc] initWithTitle:title_string style:UIBarButtonItemStylePlain target:self action:@selector(buttonBackToQuizListAction:)];
    self.navigationItem.leftBarButtonItem = backToListButton;
}

- (void)buttonBackToQuizListAction:(id)sender {
    
    NSString *type = @"Quiz_List";
    if ([(NSObject *)self.delegate respondsToSelector:@selector(didFinishTappingButtonType:)]) {
        [self.delegate didFinishTappingButtonType:type];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buttonQuizResultAction:(id)sender {
    
    UIButton *b = (UIButton *)sender;
    
    NSString *type = @"review";
    
    if (b == self.buttonRetake) {
        type = @"retake";
        
        __weak typeof(self) wo = self;
        [self.rm requestRunQuizDetailsForUser:self.userid quiz:self.quizid course:self.courseid run:YES doneBlock:^(BOOL status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([wo.delegate respondsToSelector:@selector(didFinishTappingButtonType:)]) {
                    [wo.delegate didFinishTappingButtonType:type];
                }
            });
        }];
    }
    
    if (b == self.buttonReview) {
        type = @"review";
        
        if ([self.delegate respondsToSelector:@selector(didFinishTappingButtonType:)]) {
            [self.delegate didFinishTappingButtonType:type];
        }
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
