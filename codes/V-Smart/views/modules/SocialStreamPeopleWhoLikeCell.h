//
//  SocialStreamPeopleWhoLikeCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 2/3/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialStreamPeopleWhoLikeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *avaterImageView;
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;

- (void)showAlias;

@end
