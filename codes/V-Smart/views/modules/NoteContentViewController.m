//
//  NoteContentViewController.m
//  V-Smart
//
//  Created by VhaL on 2/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#import "NoteContentViewController.h"

#import "NoteContent1ViewCell.h"
#import "NoteContent2ViewCell.h"
#import "NoteContent3ViewCell.h"
#import "NoteContent4ViewCell.h"
#import "NoteContent5ViewCell.h"

#import "NotesViewController.h"
#import "NoteEntry.h"
#import "NoteDataManager.h"

@interface NoteContentViewController ()
@property (nonatomic, assign) CGFloat tableHeight;
@property (nonatomic, assign) CGFloat cellHeight1;
@property (nonatomic, assign) CGFloat cellHeight2;
@property (nonatomic, assign) CGFloat cellHeight3;
@property (nonatomic, assign) CGFloat cellHeight4;
@property (nonatomic, assign) CGFloat cellHeight5;
@property (nonatomic, assign) CGFloat cellHeight4Min;
@property (nonatomic, assign) CGFloat cellHeight4Max;

@property (nonatomic, strong) NoteContent1ViewCell *cell1;
@property (nonatomic, strong) NoteContent2ViewCell *cell2;
@property (nonatomic, strong) NoteContent3ViewCell *cell3;
@property (nonatomic, strong) NoteContent4ViewCell *cell4;
@property (nonatomic, strong) NoteContent5ViewCell *cell5;
@end

@implementation NoteContentViewController
@synthesize tableView, tableHeight, cellHeight1, cellHeight2, cellHeight3, cellHeight4, cellHeight5, cellHeight4Max, cellHeight4Min, noteObject, isDuplicateOperation, cell1, cell5, cell2, cell3, cell4, noteStringFromIdeaPad, delegate, currentNotebook;

-(void)viewDidLoad{
    [super viewDidLoad];

    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, 700, 750);
    
    self.view.layer.cornerRadius = 25;
    self.tableView.layer.cornerRadius = 25;
    
    cellHeight4Min = 162;
    cellHeight4Max = 375;
    
    cellHeight1 = 44;
    cellHeight2 = 44;
    cellHeight3 = 188;
    cellHeight4 = cellHeight4Min;
    cellHeight5 = 44;
    self.tableView.scrollEnabled = NO;
    
    tableHeight = cellHeight1 + cellHeight2 + cellHeight3 + cellHeight4 + cellHeight5;
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, tableHeight);
    self.tableView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, tableHeight);
    self.contentSizeForViewInPopover = CGSizeMake(self.view.frame.size.width, tableHeight);
}

-(IBAction)buttonFolderTapped:(id)sender{
    VLog(@"buttonFolderTapped respondSelector");
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [UIView animateWithDuration:0.45 animations:^{
        CGFloat diffHeight = (cellHeight4Max - cellHeight4Min)/2;
        
        if (cellHeight4 == cellHeight4Min)
        {
            cellHeight4 = cellHeight4Max;
            
            tableHeight = cellHeight1 + cellHeight2 + cellHeight3 + cellHeight4 + cellHeight5;
            self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-diffHeight, self.view.frame.size.width, tableHeight);
            self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, tableHeight);
        }
        else{
            cellHeight4 = cellHeight4Min;
            
            tableHeight = cellHeight1 + cellHeight2 + cellHeight3 + cellHeight4 + cellHeight5;
            self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+diffHeight, self.view.frame.size.width, tableHeight);
            self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, tableHeight);
        }

        self.contentSizeForViewInPopover = CGSizeMake(self.view.frame.size.width, tableHeight);
    } completion:^(BOOL finished) {
        [tableView beginUpdates];
        [tableView endUpdates];
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }];
}

-(IBAction)buttonColorTapped:(id)sender{
    UIButton *button = (UIButton*)sender;
    
    cell1.contentView.backgroundColor = [UIColor colorWithCGColor:button.layer.borderColor];
}

-(IBAction)buttonSaveTapped:(id)sender{
    
    UIViewController *vcContainer = (UIViewController*)self.delegate;
    
    [vcContainer dismissPopupViewControllerWithanimationType:PopupViewAnimationSlideBottomBottom];
    
    NoteEntry *entry = [NoteEntry new];
    entry.title = [cell2.textFieldTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] ;
    
    if ([entry.title isEqualToString:@""]){
        entry.title = [self getFirstTwoWordsForNoteTitle];
    }
    
    entry.description = cell3.textViewDescription.text;
    entry.tags = [cell4 getDelimitedTags];
    
    //NoteBook *folder = [[NoteDataManager sharedInstance] getNoteBookByNoteBookName:[cell4.buttonFolder titleForState:UIControlStateNormal] useMainThread:YES];
    
    // Bug Fix
    // jca-05052016
    // Get NoteBook by ID
    
    // Use General Notebook as default (to handle adding note via idea pad)
    NoteBook *folder = [[NoteDataManager sharedInstance] getNoteBookByNoteBookName:kNotebookGeneralName useMainThread:YES];
    
    // Notebook selected by the user
    NSString *noteBookID = [self stringValue:cell4.selectedNoteBookID];
    
    // In case the user does not select Notebook where to save the note (use current selected notebook)
    if ([noteBookID isEqualToString:@""] ) {
        noteBookID = [self stringValue:currentNotebook.noteBookID];
    }
    
    // If notebook exists
    if (![noteBookID isEqualToString:@""]) {
        folder = [[NoteDataManager sharedInstance] getNoteBookByNoteBookID:noteBookID useMainThread:YES];
    }
    
    // If notebook does not exist
    if (folder == nil) {
        NSLog(@"Error saving note...");
        return;
    }
    
    entry.noteBookName = folder.noteBookName;
    entry.color = cell4.selectedColorId;
    
    // Bug Fix
    // Use NoteBookID as a unique identifier
    entry.noteBookID = folder.noteBookID;
    
    NSArray *fontTypes = [[NoteDataManager sharedInstance] getFontTypes:NO useMainThread:YES];
    for (NFontType *fontType in fontTypes) {
        UIFont *font = [UIFont fontWithName:fontType.fontName size:10];
        if ([font.fontName isEqualToString:cell3.textViewDescription.font.fontName]){
            entry.fontTypeId = fontType.fontTypeId.intValue;
            break;
        }
    }

    NSArray *fontSizes = [[NoteDataManager sharedInstance] getFontSizes:NO useMainThread:YES];
    for (NFontSize *fontSize in fontSizes) {
        if (fontSize.fontSize.intValue == (int)cell3.textViewDescription.font.pointSize){
            entry.fontSizeId = fontSize.fontSizeId.intValue;
            break;
        }
    }

    if (noteObject)
        entry.isArchive = noteObject.isArchive.boolValue;
    
    if (noteObject == nil || isDuplicateOperation == YES){
        
        if (noteObject == nil)
            entry.isArchive = NO;
        
        [[NoteDataManager sharedInstance] addNote:entry useMainThread:YES];
    }
    else{
        [[NoteDataManager sharedInstance] editNote:entry sourceNote:noteObject useMainThread:YES];
    }
    
    if ([delegate respondsToSelector:@selector(buttonSaveTapped:)])
    {
        [delegate performSelector:@selector(buttonSaveTapped:) withObject:entry];
    }
}

-(NSString*)getFirstTwoWordsForNoteTitle{
    
    NSString *string = [cell3.textViewDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSArray *array = [string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"self != %@", @""];
    array = [array filteredArrayUsingPredicate:pred];
    
    NSString *returnString;
    
    if (array.count == 1)
        returnString = [NSString stringWithFormat:@"%@", [array objectAtIndex:0]];
    
    if (1 < array.count)
        returnString = [NSString stringWithFormat:@"%@ %@", [array objectAtIndex:0], [array objectAtIndex:1]];
    
    return returnString;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        return cellHeight1;
    }

    if (indexPath.row == 1){
        return cellHeight2;
    }
    
    if (indexPath.row == 2){
        return cellHeight3;
    }
    
    if (indexPath.row == 3){
        return cellHeight4;
    }
    
    if (indexPath.row == 4){
        return cellHeight5;
    }
    
    return 44;
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        static NSString *CellIdentifier = @"Cell1";
        cell1 = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell1 == nil) {
            NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NoteContent1ViewCell" owner:self options:nil];
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[NoteContent1ViewCell class]]) {
                    cell1 = (NoteContent1ViewCell *)currentObject;
                    break;
                }
            }
        }
        
        if (noteObject != nil){
            cell1.labelType.text = noteObject.title;
            
            Color *color = [[NoteDataManager sharedInstance] getColorByColorId:[noteObject.colorId intValue] useMainThread:YES];
            UIColor *backgroundColor = [[NoteDataManager sharedInstance] getUIColorFromColorObject:color];
            
            cell1.contentView.backgroundColor = backgroundColor;
        }
        
        return cell1;
    }
    
    if (indexPath.row == 1){
        static NSString *CellIdentifier = @"Cell2";
        cell2 = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell2 == nil) {
            NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NoteContent2ViewCell" owner:self options:nil];
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[NoteContent2ViewCell class]]) {
                    cell2 = (NoteContent2ViewCell *)currentObject;
                    break;
                }
            }
        }

        cell2.cell1Label = cell1.labelType;
        
        if (noteObject != nil){
            cell2.textFieldTitle.text = noteObject.title;
        }
        
        return cell2;
    }
    
    if (indexPath.row == 2){
        static NSString *CellIdentifier = @"Cell3";
        cell3 = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell3 == nil) {
            NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NoteContent3ViewCell" owner:self options:nil];
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[NoteContent3ViewCell class]]) {
                    cell3 = (NoteContent3ViewCell *)currentObject;
                    break;
                }
            }
        }
        
        [cell3 initializeCellWithDelegate:self];
        cell3.cell2Title = cell2.textFieldTitle;
        cell2.cell3TextViewDescription = cell3.textViewDescription;
        
        if (noteStringFromIdeaPad != nil && 0 < noteStringFromIdeaPad.length){
            cell3.textViewDescription.text = noteStringFromIdeaPad;
        }
        
        if (noteObject != nil){
            cell3.textViewDescription.text = noteObject.content;
            
            NFontType *fontType = [[NoteDataManager sharedInstance] getFontTypeByFontTypeId:noteObject.fontTypeId.intValue useMainThread:YES];
            NFontSize *fontSize = [[NoteDataManager sharedInstance] getFontSizeByFontSizeId:noteObject.fontSizeId.intValue useMainThread:YES];
            
            cell3.textViewDescription.font = [UIFont fontWithName:fontType.fontName size:fontSize.fontSize.intValue];
            cell3.sliderFontSize.value = fontSize.fontSize.intValue;
            
            [cell3 updateTextCount];
        }
        
        return cell3;
    }
    
    
    if (indexPath.row == 3){
        static NSString *CellIdentifier = @"Cell4";
        cell4 = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell4 == nil) {
            NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NoteContent4ViewCell" owner:self options:nil];
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[NoteContent4ViewCell class]]) {
                    cell4 = (NoteContent4ViewCell *)currentObject;
                    break;
                }
            }
        }
        
        [cell4 initializeCellWithDelegate:self];
        
        if (noteObject != nil){
            
            switch ([noteObject.colorId intValue]) {
                case 0:{
                    [cell4 buttonColorTapped:cell4.buttonRed];
                }
                    break;
                case 1:{
                    [cell4 buttonColorTapped:cell4.buttonBlue];
                }
                    break;
                case 2:{
                    [cell4 buttonColorTapped:cell4.buttonYellow];
                }
                    break;
                case 3:{
                    [cell4 buttonColorTapped:cell4.buttonGreen];
                }
                    break;
                case 4:{
                    [cell4 buttonColorTapped:cell4.buttonPurple];
                }
                    break;
                case 5:{
                    [cell4 buttonColorTapped:cell4.buttonWhite];
                }
                    break;
                default:
                    break;
            }
            
            [cell4.buttonFolder setTitle:noteObject.folder.noteBookName forState:UIControlStateNormal];
            
            for (int i=0; i < cell4.folderArray.count; i++) {
                NoteBook *folder = [cell4.folderArray objectAtIndex:i];
                
                ////this should be change to other way than notebook name
                //if ([folder.noteBookName isEqualToString:noteObject.folder.noteBookName]){
                //    [cell4.pickerFolder selectRow:i inComponent:0 animated:true];
                //    break;
                //}
                
                // Refactor
                // jca-05052016
                // Use NoteBookID instead
                if ([folder.noteBookID isEqualToString:noteObject.folder.noteBookID]) {
                    [cell4.pickerFolder selectRow:i inComponent:0 animated:true];
                    break;
                }
            }

            for (NoteTag *noteTag in noteObject.noteTags) {
                
                if ([noteTag.isRemoved isEqualToNumber:[NSNumber numberWithBool:YES]])
                    continue;
                
                cell4.textFieldTag.text = noteTag.tag.tagName;
                [cell4 buttonAddTapped:cell4.buttonAdd];

            }
        }
        else{
            for (int i=0; i < cell4.folderArray.count; i++) {
                NoteBook *folder = [cell4.folderArray objectAtIndex:i];
                
                ////this should be change to other way than notebook name
                //if ([folder.noteBookName isEqualToString:currentNotebook.noteBookName]){
                //    [cell4.pickerFolder selectRow:i inComponent:0 animated:true];
                //    [cell4.buttonFolder setTitle:currentNotebook.noteBookName forState:UIControlStateNormal];
                //    break;
                //}
                
                // Refactor
                // jca-05052016
                // Use NoteBookID instead
                if ([folder.noteBookID isEqualToString:currentNotebook.noteBookID]) {
                    [cell4.pickerFolder selectRow:i inComponent:0 animated:true];
                    [cell4.buttonFolder setTitle:currentNotebook.noteBookName forState:UIControlStateNormal];
                    break;
                }
            }
        }
        
        return cell4;
    }
    
    if (indexPath.row == 4){
        static NSString *CellIdentifier = @"Cell5";
        cell5 = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell5 == nil) {
            NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NoteContent5ViewCell" owner:self options:nil];
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[NoteContent5ViewCell class]]) {
                    cell5 = (NoteContent5ViewCell *)currentObject;
                    break;
                }
            }
        }
        
        [cell5 initializeCellWithDelegate:self];
        
        cell3.cell5SaveButton = cell5.buttonSave;
        cell2.cell5SaveButton = cell5.buttonSave;
        
        [cell3 updateTextCount];
        
        return cell5;
    }
    
    
    //default
    static NSString *CellIdentifier = @"NoteContentViewCell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.separatorInset = UIEdgeInsetsZero;
    }
    return cell;
}

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

@end
