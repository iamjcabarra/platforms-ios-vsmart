//
//  LPMDetailsViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 27/06/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

// MARK: - Toggle Button Animation

extension UIView {
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2, completionDelegate: CAAnimationDelegate? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.toValue = toValue
        rotateAnimation.duration = duration
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.fillMode = kCAFillModeForwards
        
        if let delegate: CAAnimationDelegate = completionDelegate {
            rotateAnimation.delegate = delegate
        }
        
        self.layer.add(rotateAnimation, forKey: nil)
    }
}

class LPMDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet fileprivate var headerCurriculumLabel: UILabel!
    @IBOutlet fileprivate var headerCourseLabel: UILabel!
    @IBOutlet fileprivate var headerLevelLabel: UILabel!
    @IBOutlet fileprivate var headerUnitLabel: UILabel!
    @IBOutlet fileprivate var headerStatusLabel: UILabel!
    @IBOutlet fileprivate var headerQuarterLabel: UILabel!
    @IBOutlet fileprivate var headerTeacherLabel: UILabel!
    @IBOutlet fileprivate var headerTimeFrameLabel: UILabel!
    @IBOutlet fileprivate var headerActCourseLabel: UILabel!
    @IBOutlet fileprivate var headerActLevelLabel: UILabel!
    @IBOutlet fileprivate var headerActUnitLabel: UILabel!
    @IBOutlet fileprivate var headerActStatusLabel: UILabel!
    @IBOutlet fileprivate var headerActQuarterLabel: UILabel!
    @IBOutlet fileprivate var headerActTeacherLabel: UILabel!
    @IBOutlet fileprivate var headerActTimeFrameLabel: UILabel!
    @IBOutlet fileprivate var headerLearningCompetencyLabel: UILabel!
    @IBOutlet fileprivate var collapsibleTableView: UITableView!
    @IBOutlet fileprivate var segmentedControl: UISegmentedControl!
    @IBOutlet fileprivate var curriculumButton: UIButton!
    @IBOutlet fileprivate var seeAllCompetenciesButton: UIButton!
    
    // MARK: - Table Data Structure
    
    fileprivate struct Section {
        var title: String!
        var contents: [AnyObject]!
        var collapsed: Bool!
        
        init(title: String, contents: [AnyObject], collapsed: Bool = false) {
            self.title = title
            self.contents = contents
            self.collapsed = collapsed
        }
    }
    
    var selectedCourseObject: NSManagedObject!
    var selectedLessonObject: NSManagedObject!
    
    // Just temporary
    var associatedCurriculum: NSDictionary?
    
    fileprivate let kHeaderCellIdentifier = "header_cell_identifier"
    fileprivate let kContentCellIdentifier = "content_cell_identifier"
    
    fileprivate var detailViewSwapperController: LPMDetailSVSwapper!
    fileprivate var competencyListView: LPMLessonCompetencyListViewController!
    
    fileprivate let kDetailViewSwapperSegueIdentifier = "SHOW_DETAILS_VIEW_SWAPPER"
    fileprivate let kCommentSVCSegueIdentifier = "SHOW_COMMENT_VIEW"
    fileprivate let kHistorySVCSegueIdentifier = "SHOW_HISTORY_VIEW"
    fileprivate let kCurriculumDetailsViewSegueIdentifier = "SHOW_CURRICULUM_DETAILS_VIEW"
    fileprivate let kCompetencLVSegueIdentifier = "SHOW_COMPETENCY_LIST_VIEW"
    
    fileprivate var sections = [Section]()
    
    // MARK: - Data Manager
    
    fileprivate lazy var lessonPlanDataManager: LessonPlanDataManager = {
        let lpdm = LessonPlanDataManager.sharedInstance()
        return lpdm!
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collapsibleTableView.estimatedRowHeight = 70.0;
        self.collapsibleTableView.rowHeight = UITableViewAutomaticDimension;
        
        self.setupHeaderViewContents()
        self.setupDataForCollapsibleTableView()
        self.setupSegmentedControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customizeNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Custom Navigation Bar
    
    func customizeNavigationBar() {
        self.title = self.selectedLessonObject.value(forKey: "name") as? String
        self.navigationController?.navigationBar.barTintColor = UIColor.darkGray
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
    }
    
    // MARK: - Header View Contents
    
    func setupHeaderViewContents() {
        self.headerCourseLabel.text = NSLocalizedString("Course", comment: "")
        self.headerLevelLabel.text = NSLocalizedString("Level", comment: "")
        self.headerUnitLabel.text = NSLocalizedString("Unit", comment: "")
        self.headerStatusLabel.text = NSLocalizedString("Status", comment: "")
        self.headerQuarterLabel.text = NSLocalizedString("Quarter", comment: "")
        self.headerTeacherLabel.text = NSLocalizedString("Teacher", comment: "")
        self.headerTimeFrameLabel.text = NSLocalizedString("Time Frame", comment: "")
        
        // Just temporary
        self.headerCurriculumLabel.text = "\(NSLocalizedString("Associated Curriculum", comment: "")) :"
        
        let course = self.selectedCourseObject.value(forKey: "course_name") as! String
        let level = self.selectedLessonObject.value(forKey: "level") as! String
        let unit = self.selectedLessonObject.value(forKey: "unit") as! String
        var status = self.selectedLessonObject.value(forKey: "status") as! String
        let quarter = self.selectedLessonObject.value(forKey: "quarter") as! String
        let teacher = self.selectedLessonObject.value(forKey: "teacher_name") as! String
        let start = self.selectedLessonObject.value(forKey: "start_date") as! String
        let end = self.selectedLessonObject.value(forKey: "end_date") as! String
        
        if (status == "0") {
            status = NSLocalizedString("Draft", comment: "")
        }
        else if (status == "1") {
            status = NSLocalizedString("for Approval", comment: "")
        }
        else {
            status = NSLocalizedString("Approved", comment: "")
        }
        
        self.headerActCourseLabel.text = ":\t\(course)"
        self.headerActLevelLabel.text = ":\t\(level)"
        self.headerActUnitLabel.text = ":\t\(unit)"
        self.headerActStatusLabel.text = ":\t\(status)"
        self.headerActQuarterLabel.text = ":\t\(quarter)"
        self.headerActTeacherLabel.text = ":\t\(teacher)"
        self.headerActTimeFrameLabel.text = ":\t\(start) \(NSLocalizedString("to", comment: "")) \(end)"
        self.prettifyLabel(self.headerActTimeFrameLabel)
        
        // Just temporary
        var curriculumTitle = ""
        
        if let curriculum = self.associatedCurriculum {
            if let title = curriculum["curriculum_title"] as? String {
                curriculumTitle = title
            }
        }
        
        self.curriculumButton.setTitle(curriculumTitle, for: UIControlState())
        self.curriculumButton.setTitle(curriculumTitle, for: .selected)
        self.curriculumButton.setTitle(curriculumTitle, for: .highlighted)
        
        let viewAssociatedCurriculumAction = #selector(self.viewAssociatedCurriculumAction(_:))
        self.curriculumButton.addTarget(self, action: viewAssociatedCurriculumAction, for: .touchUpInside)
        
        self.loadAssociatedLearningCompetencies()
    }

    func prettifyLabel(_ label: UILabel) {
        let paragraphStyle = NSMutableParagraphStyle.init()
        paragraphStyle.headIndent = 29
        label.attributedText = NSAttributedString.init(string: label.text!, attributes: [NSParagraphStyleAttributeName: paragraphStyle])
    }
    
    // MARK: - View Associated Curriculum
    
    func viewAssociatedCurriculumAction(_ sender: UIButton) {
        if (self.associatedCurriculum != nil) {
            DispatchQueue.main.async(execute: {
                self.performSegue(withIdentifier: self.kCurriculumDetailsViewSegueIdentifier, sender: nil)
            })
        }
    }
    
    // MARK: - Associated Learning Competencies
    
    fileprivate func loadAssociatedLearningCompetencies() {
        var count = 0
        var text: String! = ""
        
        if let competencies = self.lessonPlanDataManager.fetchPreAssociatedLearningCompetencies() {
            for competency in competencies {
                let content: String! = self.lessonPlanDataManager.stringValue((competency as AnyObject).value(forKey: "content") as AnyObject?)
                if count < 2 { text = "\(text!) \(content.replacingOccurrences(of: " \n", with: ","))" }
                count = count + 1
            }
        }
        
        self.headerLearningCompetencyLabel.isHidden = count > 0 ? false : true
        self.seeAllCompetenciesButton.isHidden = count > 0 ? false : true
        
        if count > 0 {
            self.headerLearningCompetencyLabel.text = count > 0 ? (count > 2 ? "(\(String(text.characters.dropLast()))...)" : String(text.characters.dropLast())) : text
            _ = self.headerLearningCompetencyLabel.intrinsicContentSize.width
            self.seeAllCompetenciesButton.isHidden = count > 2 ? false : true
            
            if count > 2 {
                let title = NSLocalizedString("See All", comment: "")
                self.seeAllCompetenciesButton.setTitle(title, for: UIControlState())
                self.seeAllCompetenciesButton.setTitle(title, for: .highlighted)
                self.seeAllCompetenciesButton.setTitle(title, for: .selected)
                
                let competencyListButtonAction = #selector(self.competencyListButtonAction(_:))
                self.seeAllCompetenciesButton.addTarget(self, action: competencyListButtonAction, for: .touchUpInside)
            }
        }
    }
    
    func competencyListButtonAction(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: self.kCompetencLVSegueIdentifier, sender: nil)
        })
    }
    
    // MARK: - Collapsible Table View Data
    
    fileprivate func setupDataForCollapsibleTableView() {
        let lpid = self.selectedLessonObject.value(forKey: "lp_id") as! String
        
        // Lesson Content
        let lessonContents = self.selectedLessonObject.value(forKey: "contents") as! NSSet
        let lessonStages = lessonContents.allObjects
        
        // Content Stages
        var lessonStageIDList = [String]()
        var lessonStageDetail = [String: String]()
        
        for lessonStageMO in lessonStages {
            let lessonStageID = (lessonStageMO as AnyObject).value(forKey: "stage_id") as! String
            let lessonStageName = (lessonStageMO as AnyObject).value(forKey: "stage_name") as! String
            
            lessonStageIDList.append(lessonStageID)
            lessonStageDetail[lessonStageID] = lessonStageName
        }
        
        // Stage Processes
        var lessonStageCount = 1
        let sortedLessonStageIDList = lessonStageIDList.sorted()
        
        for stageID in sortedLessonStageIDList {
            if let lessonStageProcesses = self.lessonPlanDataManager.getLessonStageProcessesForLesson(withID: lpid, andStageID: stageID) {
                let lessonStageTitle = "\(NSLocalizedString("Stage", comment: "")) \(lessonStageCount): \(lessonStageDetail[stageID]!)"
                self.sections.append(Section(title: lessonStageTitle, contents: lessonStageProcesses as [AnyObject], collapsed: true))
            }
            
            lessonStageCount += 1
        }
    }
    
    // MARK: - Segmented Control
    
    func setupSegmentedControl() {
        self.segmentedControl.removeAllSegments()
        
        let commentString = NSLocalizedString("Comment", comment: "")
        let historyString = NSLocalizedString("History", comment: "")
        
        self.segmentedControl.insertSegment(withTitle: commentString, at: 0, animated: true)
        self.segmentedControl.insertSegment(withTitle: historyString, at: 1, animated: true)
        
        self.segmentedControl.tintColor = UIColor(rgba: "#3498DB")
        self.segmentedControl.selectedSegmentIndex = 0
        
        let segmentedControlAction = #selector(self.segmentedControlAction(_:))
        self.segmentedControl.addTarget(self, action: segmentedControlAction, for: .valueChanged)
    }
    
    func segmentedControlAction(_ sender: UISegmentedControl) {
        DispatchQueue.main.async(execute: {
            let svcSegueIdentifier = sender.selectedSegmentIndex == 0 ? self.kCommentSVCSegueIdentifier : self.kHistorySVCSegueIdentifier
            self.detailViewSwapperController.swapToViewControllerWithSegueIdentifier(svcSegueIdentifier)
        })
    }

    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.sections[section].collapsed!) ? 0 : self.sections[section].contents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kContentCellIdentifier) as! LPMCollapsibleCellContent
        let processObject = self.sections[(indexPath as NSIndexPath).section].contents[(indexPath as NSIndexPath).row] as! NSManagedObject
        
        cell.processNameLabel.text = (processObject.value(forKey: "learning_process_name") as? String)?.uppercased()
        
        let fileFlag = processObject.value(forKey: "is_file") as? String
        let content = (fileFlag == "1") ? processObject.value(forKey: "orig_filename") as? String : processObject.value(forKey: "p_content") as? String
        cell.processContentLabel.text = content
        
        if fileFlag ==  "1" {
            cell.processContentLabel.textColor = UIColor.blue
            cell.processContentLabel.isUserInteractionEnabled = true
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.openFileOnTap(sender:)))
            tapGestureRecognizer.numberOfTapsRequired = 1
            cell.processContentLabel.addGestureRecognizer(tapGestureRecognizer)
            
            let sectionAndRow = indexPath.section * 100 + indexPath.row
            cell.processContentLabel.tag = sectionAndRow
        }
        else {
            cell.processContentLabel.textColor = UIColor.darkGray
            cell.processContentLabel.isUserInteractionEnabled = false
        }
        
        return cell
    }

    func openFileOnTap(sender: UITapGestureRecognizer) {
        /* Uncomment to enable viewing of attached file via web browser
        if let view = sender.view {
            print("TAP GESTURE SENDER TAG: \(view.tag)")
            
            let section = view.tag / 100
            let row = view.tag % 100
            
            if let object = self.sections[section].contents[row] as? NSManagedObject {
                let path = self.lessonPlanDataManager.stringValue(object.value(forKey: "file_path"))
                
                if let url = NSURL(string: path!) as? URL {
                    UIApplication.shared.openURL(url)
                }
                else {
                    let message = "Can't open link."
                    self.view.makeToast(message: message)
                }
            }
        }
        */
        
        // Comment out if you want to enable viewing of attached file via web browser
        let message = NSLocalizedString("To download, please use other platforms.", comment: "")
        let acTitle = NSLocalizedString("Okay", comment: "")
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: acTitle, style: .default) { (Alert) -> Void in alert.dismiss(animated: true, completion: nil) }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Table View Delegate
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: self.kHeaderCellIdentifier) as! LPMCollapsibleCellHeader
        
        header.toggleButton.tag = section
        header.headerTitleLabel.text = (self.sections[section].title).uppercased()
        header.toggleButton.rotate(self.sections[section].collapsed! ? 0.0 : CGFloat(M_PI_2))
        header.toggleButton.addTarget(self, action: #selector(self.toggleCollapse(_:)), for: .touchUpInside)
        
        _ = header.contentView.intrinsicContentSize
        header.contentView.layer.borderColor = UIColor.white.cgColor
        header.contentView.layer.borderWidth = 1.0
        
        return header.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    // MARK: - Collapse Event Handler
    
    func toggleCollapse(_ sender: UIButton) {
        let section = sender.tag
        let collapsed = self.sections[section].collapsed
        self.sections[section].collapsed = !collapsed!
        self.collapsibleTableView.reloadSections(IndexSet(integer: section), with: .none)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.kDetailViewSwapperSegueIdentifier {
            self.detailViewSwapperController = segue.destination as? LPMDetailSVSwapper
            if let lpid = self.selectedLessonObject.value(forKey: "lp_id") {
                self.detailViewSwapperController.lpid = lpid as! String
            }
        }
        
        if let curriculumDetailsView = segue.destination as? CPMCurriculumDetailsView , segue.identifier == self.kCurriculumDetailsViewSegueIdentifier {
            if let curriculum = self.associatedCurriculum {
                if let curriculumID = curriculum["curriculum_id"] as? String {
                    if let curriculumTitle = curriculum["curriculum_title"] as? String {
                        curriculumDetailsView.curriculumID = curriculumID
                        curriculumDetailsView.curriculumTitle = curriculumTitle
                        self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
                    }
                }
            }
        }
        
        if segue.identifier == self.kCompetencLVSegueIdentifier {
            self.competencyListView = segue.destination as? LPMLessonCompetencyListViewController
            self.competencyListView.isUBD = true
        }
    }
    
}
