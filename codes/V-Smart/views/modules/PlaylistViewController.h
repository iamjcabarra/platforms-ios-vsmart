//
//  PlaylistViewController.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 9/9/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "BaseViewController.h"

@interface PlaylistViewController : BaseViewController<UIWebViewDelegate>
@property (nonatomic, strong) UIWebView *theWebView;
@property (nonatomic, strong) UIImageView *theImage;
@end
