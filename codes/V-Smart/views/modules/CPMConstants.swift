//
//  CPMConstants.swift
//  V-Smart
//
//  Created by Julius Abarra on 15/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import Foundation

// MARK: General Settings

struct CPMConstants {
    struct Entity {
        static let COURSE_CATEGORY: String = "CPMCourseCategory"
        static let CURRICULUM: String = "CPMCurriculum"
        static let CURRICULUM_ASSOCIATION: String = "CPMCurriculumForAssociation"
        static let CURRICULUM_PERIOD: String = "CPMCurriculumPeriod"
        static let CURRICULUM_LEARNING_COMPETENCY: String = "CPMCurriculumLearningCompetency"
        static let FILE: String = "CPMFile"
    }
    
    struct Sandbox {
        static let MAX_DIRECTORY_SIZE: UInt = 167772160
        static let DIRECTORY_PREFIX: String = "CPM"
    }
    
    struct Server {
        static let MAX_VERSION: CGFloat = 2.5
    }
}

// MARK: Request End Points

func endPointForCourseCategoryListOfUserWithID(_ userID: String) -> String {
    return "/vsmart-rest-dev/v2/courses/category/user/\(userID)"
}

func endPointForCurriculumListForCourseCategoryWithID(_ categoryID: String, andUserWithID: String) -> String {
    return "/vsmart-rest-dev/v2/curriculum/list/course_category/\(categoryID)/user_id/\(andUserWithID)"
}

func endPointForDownloadingCurriculumWithID(_ curriculumID: String) -> String {
    return "/vsmart-rest-dev/v1/curriculum/\(curriculumID)/download/pdf"
}

func endPointForCurriculumDetails() -> String {
    return "/reports/curriculuminfo/"
}

func endPointForCurriculumListForCourseWithID(_ courseID: String) -> String {
    return "/vsmart-rest-dev/v2/curriculum/list/curriculum/course/\(courseID)"
}

func endPointForCurriculumPeriodListForCurriculumWithID(_ curriculumID: String) -> String {
    return "/vsmart-rest-dev/v2/curriculum/\(curriculumID)/overview"
}

func endPointForCurriculumLearningCompetencyListForCurriculumPeriodWithID(_ periodID: String) -> String {
    return "/vsmart-rest-dev/v2/curriculum/list/learningcompetencies/period/\(periodID)"
}

func endPointForCurriculumAssociationToLessonPlanWithID(_ lessonPlanID: String, andUserWithID: String) -> String {
    return "/vsmart-rest-dev/v2/curriculum/set/lessonplan/\(lessonPlanID)/user/\(andUserWithID)"
}

func endPointForUpdateCurriculumAssociationToLessonPlanWithID(_ lessonPlanID: String, andUserWithID: String) -> String {
    return "/vsmart-rest-dev/v2/curriculum/update/lessonplan/\(lessonPlanID)/user/\(andUserWithID)"
}

func endPointForCurriculumGeneration(_ curriculumID: String, courseCategoryID: String, uuid: String) -> String {
    return "/vsmart-rest-dev/v1/curriculum/\(curriculumID)/download/pdf/uuid/\(uuid)/coursecategory/\(courseCategoryID)?is_mobile"
}

// MARK: Course Category List View

enum CPMCourseCategoryListRequestType: Int {
    case load = 1
    case search
    case refresh
}

// MARK: Curriculum List View

enum CPMCurrculumListRequestType: Int {
    case load = 1
    case search
    case refresh
}
