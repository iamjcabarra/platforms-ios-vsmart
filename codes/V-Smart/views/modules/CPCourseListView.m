//
//  CPCourseListView.m
//  V-Smart
//
//  Created by Julius Abarra on 28/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "CPCourseListView.h"
#import "CPCourseCellItem.h"
#import "TestGuruDataManager.h"
#import "TestGuruDataManager+Course.h"
#import "CPStudentCourseDetailView.h"
#import "CPTeacherCourseDetailView.h"
#import "AccountInfo.h"
#import "HUD.h"

@interface CPCourseListView () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate>

@property (strong, nonatomic) TestGuruDataManager *tgdm;

@property (strong, nonatomic) UIRefreshControl *tableRefreshControl;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObject *selectedCourseObject;

@property (strong, nonatomic) IBOutlet UIView *emptyView;
@property (strong, nonatomic) IBOutlet UILabel *emptyMessageLabel;

@property (strong, nonatomic) IBOutlet UIView *visualEffectView;
@property (strong, nonatomic) IBOutlet UITableView *courseTable;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIButton *loadMoreButton;

@property (assign, nonatomic) NSInteger currentPage;
@property (assign, nonatomic) NSInteger totalFilteredTests;
@property (assign, nonatomic) NSInteger totalPages;
@property (assign, nonatomic) NSInteger totalItems;

@property (strong, nonatomic) NSString *searchKeyword;
@property (assign, nonatomic) BOOL isAscending;

@property (assign, nonatomic) NSInteger temporaryCourseCount;

@end

static NSString *kCourseCellIdentifier = @"courseCellIdentifier";
static NSString *kShowCourseDetailForStudent = @"SHOW_COURSE_DETAIL_VIEW_STUDENT";
static NSString *kShowCourseDetailForTeacher = @"SHOW_COURSE_DETAIL_VIEW_TEACHER";

@implementation CPCourseListView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Data Manager
    self.tgdm = [TestGuruDataManager sharedInstance];
    
    // Course Table
    [self setUpCourseTableView];
    
    // List Courses
    // NOTE: Uncomment if not paginated
    [self listAllCourses];
    
    // Search Default
    self.searchKeyword = @"";
    
    // Sort Default
    self.isAscending = YES;
    
    // Pagination Settings
    self.currentPage = 0;
    self.totalFilteredTests = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    
    // Pull to Refresh
    // NOTE: Uncomment if unpaginated
    SEL refreshAction = @selector(refreshCourseList);
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.courseTable addSubview:self.tableRefreshControl];
    
    // Load More
    self.loadMoreButton.hidden = YES;
    [self.loadMoreButton addTarget:self
                            action:@selector(loadMoreButtonAction:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    // Course Count (1 to avoid loading of empty view on first load)
    self.temporaryCourseCount = 1;
    
    // Empty View
    [self shouldShowEmptyView:NO];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelSearchWhenTapped)];
    [self.visualEffectView addGestureRecognizer:tap];
    
    // Initial Paginated Course List
    // NOTE: Comment if unpaginated
    // NSDictionary *settings = [self getSettingsForCoursePaginationIsReset:YES shouldLoadNextPage:NO];
    // [self listPaginatedCourseForSettings:settings];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Custom Navigation Bar
    [self customizeNavigationController];
    
    // Right Bar Buttons
    [self setUpRightBarButtons];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Navigation Bar

- (void)customizeNavigationController {
    UIColor *color = UIColorFromHex(0xEF4136);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    }
    else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationItem.titleView = nil;
    
    // Custom Navigation Title View
    self.searchBar.placeholder = NSLocalizedString(@"Search Course", nil);
    self.searchBar.delegate = self;
}

#pragma mark - Custom Right Bar Buttons

- (void)setUpRightBarButtons {
    // Spacer Button
    UIButton *spacerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    spacerButton.frame = CGRectMake(0, 0, 10, 20);
    
    // Search Button
    UIImage *searchImage = [UIImage imageNamed:@"search_white_48x48.png"];
    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    searchButton.frame = CGRectMake(0, 0, 20, 20);
    searchButton.showsTouchWhenHighlighted = YES;
    
    [searchButton setImage:searchImage forState:UIControlStateNormal];
    [searchButton setImage:searchImage forState:UIControlStateHighlighted];
    [searchButton addTarget:self action:@selector(searchButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Sort Button
    UIImage *sortImage = [UIImage imageNamed:@"sort_white48x48.png"];
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    sortButton.frame = CGRectMake(0, 0, 20, 20);
    sortButton.showsTouchWhenHighlighted = YES;
    
    [sortButton setImage:sortImage forState:UIControlStateNormal];
    [sortButton setImage:sortImage forState:UIControlStateHighlighted];
    [sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Right Button Items
    UIBarButtonItem *spacerButtonItem = [[UIBarButtonItem alloc] initWithCustomView:spacerButton];
    UIBarButtonItem *sortButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
    UIBarButtonItem *searchButtonItem = [[UIBarButtonItem alloc] initWithCustomView:searchButton];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:sortButtonItem, searchButtonItem, spacerButtonItem, nil];
}

#pragma mark - Course Table View

- (void)setUpCourseTableView {
    // Set Protocols
    self.courseTable.dataSource = self;
    self.courseTable.delegate = self;
    
    // Allow Selection
    self.courseTable.allowsSelection = YES;
    self.courseTable.allowsMultipleSelection = NO;
    
    // Default Height
    self.courseTable.estimatedRowHeight = 75.0f;
    self.courseTable.rowHeight = UITableViewAutomaticDimension;
    
    // Resusable Cell
    UINib *courseCellItemNib = [UINib nibWithNibName:@"CPCourseCellItem" bundle:nil];
    [self.courseTable registerNib:courseCellItemNib forCellReuseIdentifier:kCourseCellIdentifier];
}

#pragma mark - Paginated Course List

- (NSDictionary *)getSettingsForCoursePaginationIsReset:(BOOL)isReset shouldLoadNextPage:(BOOL)nextPage {
    NSString *limit = [self.tgdm stringValue:[self.tgdm fetchObjectForKey:kTG_PAGINATION_LIMIT]];
    NSString *current_page = @"1";
    NSString *sort_key = [NSString stringWithFormat:@"%@course_name", (self.isAscending) ? @"-" : @""];
    
    // Since API-driven sort is not yet implemented
    sort_key = @"";
    
    if ([limit integerValue] < 1) {
        limit = @"10";
    }
    
    if (!isReset) {
        NSInteger number = nextPage ? self.currentPage + 1 : self.currentPage;
        current_page = [NSString stringWithFormat:@"%zd", number];
    }
    
    return @{@"limit":limit, @"current_page":current_page, @"search_keyword":self.searchKeyword, @"sort_key":sort_key};
}

- (void)listPaginatedCourseForSettings:(NSDictionary *)settings {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Loading", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    NSString *userid = [self.tgdm loginUser];
    self.loadMoreButton.hidden = YES;
    
    [self.tgdm requestPaginatedCourseListForUser:userid withSettingsForPagination:settings dataBlock:^(NSDictionary *data) {
        if (data != nil) {
            self.currentPage = [data[@"current_page"] integerValue];
            self.totalItems = [data[@"total_items"] integerValue];
            self.totalFilteredTests = [data[@"total_filtered"] integerValue];
            self.totalPages = [data[@"total_pages"] integerValue];
        }
        
        __weak typeof(self) wo = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo reloadFetchedResultsController];
            
            BOOL hideButton = (wo.totalPages > wo.currentPage) ? NO : YES;
            wo.loadMoreButton.hidden = hideButton;
            
            BOOL shouldShow = (wo.totalFilteredTests == 0) ? YES :  NO;
            [wo shouldShowEmptyView:shouldShow];
            
            [HUD hideUIBlockingIndicator];
        });
    }];
}

- (void)loadMoreButtonAction:(id)sender {
    self.isAscending = NO;
    
    // NOTE: Uncomment if unpaginated
    [self reloadFetchedResultsController];
    
    // NOTE: Comment if unpaginated
    // NSDictionary *settings = [self getSettingsForCoursePaginationIsReset:NO shouldLoadNextPage:YES];
    // [self listPaginatedCourseForSettings:settings];
}

#pragma mark - Unpaginated Course List

- (void)listAllCourses {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Loading", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    NSString *userid = [self.tgdm loginUser];
    __weak typeof(self) wo = self;
    
    [self.tgdm requestCourseListForUser:userid dataBlock:^(NSDictionary *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD hideUIBlockingIndicator];
            [wo showErrorMessage:(data == nil) ? YES: NO];
            
            if (data != nil) {
                NSString *countString = [wo.tgdm stringValue:data[@"count"]];
                NSInteger count = [countString integerValue];
                [wo updateTitleViewWithCount:count];
                wo.temporaryCourseCount = count;
                [wo reloadFetchedResultsController];
            }
        });
    }];
}

- (void)refreshCourseList {
    NSString *userid = [self.tgdm loginUser];
    __weak typeof(self) wo = self;
    
    [self.tgdm requestCourseListForUser:userid dataBlock:^(NSDictionary *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.tableRefreshControl endRefreshing];
            [wo showErrorMessage:(data == nil) ? YES: NO];
            
            if (data != nil) {
                NSString *countString = [wo.tgdm stringValue:data[@"count"]];
                NSInteger count = [countString integerValue];
                [wo updateTitleViewWithCount:count];
                wo.temporaryCourseCount = count;
                [wo reloadFetchedResultsController];
            }
        });
    }];
}

#pragma mark - Actions for Buttons

- (void)searchButtonAction:(id)sender {
    self.navigationItem.titleView = self.searchBar;
    [self.searchBar becomeFirstResponder];
}

- (void)sortButtonAction:(id)sender {
    self.isAscending = (self.isAscending) ? NO : YES;
    
    // NOTE: Uncomment if unpaginated
    [self listAllCourses];
    
    // NOTE: Should be API-driven; change if already supported
    // [self reloadFetchedResultsController];
}

#pragma mark - Alert Messages

- (void)showErrorMessage:(BOOL)show {
    if (show) {
        NSString *butOkay = NSLocalizedString(@"Okay", nil);
        NSString *message = NSLocalizedString(@"There was an error loading this page. Please check your internet connection.", nil);
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *theAction = [UIAlertAction actionWithTitle:butOkay
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
        [alert addAction:theAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - Search Bar Delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    self.visualEffectView.hidden = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText isEqualToString:@""]) {
        self.searchKeyword = searchText;
        
        // NOTE: Uncomment if unpaginated
        [self listAllCourses];
        
        // NOTE: Comment if unpaginated
        // NSDictionary *settings = [self getSettingsForCoursePaginationIsReset:YES shouldLoadNextPage:NO];
        // [self listPaginatedCourseForSettings:settings];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.searchKeyword = searchBar.text;
    
    // NOTE: Uncomment if unpaginated
    [self listAllCourses];
    
    // NOTE: Comment if unpaginated
    // NSDictionary *settings = [self getSettingsForCoursePaginationIsReset:YES shouldLoadNextPage:NO];
    // [self listPaginatedCourseForSettings:settings];
    
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    searchBar.text = self.searchKeyword;
    self.navigationItem.titleView = nil;
    self.visualEffectView.hidden = YES;
}

- (void)cancelSearchWhenTapped {
    self.navigationItem.titleView = nil;
    self.visualEffectView.hidden = YES;
    [self.view endEditing:YES];
    self.searchBar.text = self.searchKeyword;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    
    // NOTE: Uncomment if unpaginated
    [self shouldShowEmptyView:(count > 0) ? NO : YES];
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.courseTable = tableView;
    
    CPCourseCellItem *cell = [tableView dequeueReusableCellWithIdentifier:kCourseCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    CPCourseCellItem *courseCellItem = (CPCourseCellItem *)cell;
    [self configureQuestionCell:courseCellItem managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(CPCourseCellItem *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    NSString *course_name = [mo valueForKey:@"course_name"];
    NSString *section_name = [mo valueForKey:@"section_name"];
    NSString *grade_level = [mo valueForKey:@"grade_level"];
    NSString *schedule = [mo valueForKey:@"schedule"];
    
    cell.courseNameLabel.text = course_name;
    cell.courseSectionLabel.text = [NSString stringWithFormat:@"%@ - %@", section_name, grade_level];
    cell.courseScheduleLabel.text = schedule;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedCourseObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    AccountInfo *account = [[VSmart sharedInstance] account];
    NSString *position = [NSString stringWithFormat:@"%@", account.user.position];
    
    NSString *cs_id = [NSString stringWithFormat:@"%@", [self.selectedCourseObject valueForKey:@"cs_id"]];
    [self.tgdm saveObject:cs_id forKey:kCP_SELECTED_CS_ID];
    
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Please wait", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    if ([position isEqualToString:kModeIsStudent]) {
        __weak typeof(self) wo = self;
        [self.tgdm requestPaginationSettings:^(BOOL status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [wo performSegueWithIdentifier:kShowCourseDetailForStudent sender:nil];
                [wo.courseTable deselectRowAtIndexPath:indexPath animated:YES];
            });
        }];
    }
    
    if ([position isEqualToString:kModeIsTeacher]) {
        __weak typeof(self) wo = self;
        [self.tgdm requestPaginationSettings:^(BOOL status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [wo performSegueWithIdentifier:kShowCourseDetailForTeacher sender:nil];
                [wo.courseTable deselectRowAtIndexPath:indexPath animated:YES];
            });
        }];
    }
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tgdm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCourseListEntity];
    [fetchRequest setFetchBatchSize:10];
    
    // NOTE: Uncomment if unpaginated
    if (![self.searchKeyword isEqualToString:@""]) {
        NSPredicate *predicate = [self predicateForKeyPathContains:@"search_string" value:self.searchKeyword];
        [fetchRequest setPredicate:predicate];
    }
    
    NSSortDescriptor *course_name = [NSSortDescriptor sortDescriptorWithKey:@"course_name"
                                                                  ascending:self.isAscending
                                                                   selector:@selector(caseInsensitiveCompare:)];
    [fetchRequest setSortDescriptors:@[course_name]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.courseTable beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.courseTable insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.courseTable deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.courseTable;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.courseTable endUpdates];
}

#pragma mark - Reload Fetched Results

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.courseTable reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error: %@", [err localizedDescription]);
    }
}

#pragma mark - Application Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kShowCourseDetailForStudent]) {
        CPStudentCourseDetailView *studentCourseDetailView = (CPStudentCourseDetailView *)[segue destinationViewController];
        studentCourseDetailView.courseObject = self.selectedCourseObject;
        [self hideNavigationBackButtonTitle];
    }
    
    if ([segue.identifier isEqualToString:kShowCourseDetailForTeacher]) {
        CPTeacherCourseDetailView *teacherCourseDetailView = (CPTeacherCourseDetailView *)[segue destinationViewController];
        teacherCourseDetailView.courseObject = self.selectedCourseObject;
        [self hideNavigationBackButtonTitle];
    }
}

- (void)hideNavigationBackButtonTitle {
    UIBarButtonItem *bbi = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = bbi;
}

#pragma mark - Custom Empty View

- (void)shouldShowEmptyView:(BOOL)show {
    // NOTE: Uncomment if unpaginated
    if ([self.searchBar.text isEqualToString:@""]) {
        show = (self.temporaryCourseCount > 0) ? NO : YES;
    }
    
    if (show) {
        NSString *message = NSLocalizedString(@"No available course yet.", nil);
        
        if (![self.searchBar.text isEqualToString:@""]) {
            message = NSLocalizedString(@"No results found for", nil);
            message =[NSString stringWithFormat:@"%@ \"%@\"", message, self.searchBar.text];
        }
        
        self.emptyMessageLabel.text = message;
    }
    
    self.emptyView.hidden = !show;
    self.courseTable.hidden = show;
}

#pragma mark - Title View Updating

- (void)updateTitleViewWithCount:(NSInteger)courseCount {
    NSString *title = NSLocalizedString(@"Course List", nil);
    self.title = [NSString stringWithFormat:@"%@ (%zd)", title, courseCount];
}

@end