//
//  GBTStudentListPanel.swift
//  V-Smart
//
//  Created by Ryan Migallos on 15/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTStudentListPanel: UIViewController {

    fileprivate let cellIdentifier = "gbt_student_panel_cell"
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var studentSearchBar: UISearchBar!
    @IBOutlet weak var sortSegmentedControl: UISegmentedControl!
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>?

    var isAscending = true
    
    var term_id = ""
    
    fileprivate let notification = NotificationCenter.default
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    var scrollDelegate : ScrollCommunicationDelegate?
    var student_name = ""
    var sort_by_gender = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cellNib = UINib(nibName: "GBTStudentListCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellIdentifier)
        
        self.studentSearchBar.delegate = self
        self.sortSegmentedControl.addTarget(self, action: #selector(self.sortSegmentedControlAction(_:)), for:.valueChanged)
    }
    
    func sortSegmentedControlAction(_ segment: UISegmentedControl) {
        if segment.selectedSegmentIndex == 0 {
            self.sort_by_gender = false
        } else {
            self.sort_by_gender = true
        }
        
        self.reloadFetchedResultsController()
        self.notification.post(name: Notification.Name(rawValue: "GBT_STUDENT_SORT"), object: self.sort_by_gender)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension GBTStudentListPanel: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, NSFetchedResultsControllerDelegate  {
    
    func reloadList(forTermID term_id: String) {
        self.term_id = term_id
        reloadFetchedResultsController()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! GBTStudentListCell
        cell.backgroundColor = UIColor.clear
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ cell: GBTStudentListCell, atIndexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: atIndexPath)
        guard
            let name = mo.value(forKey: "name") as? String,
            let gender = mo.value(forKey: "gender") as? String
            else { return }
        
        var mOrF = "n/a"
        
        if gender.contains("1_") {
            mOrF = " - M"
        } else if gender.contains("2_") {
            mOrF = " - F"
        }
        
        cell.customLabel.text = name + mOrF
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: - Fetched Results Controller
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = gbdm.getMainContext()
        let ascending = self.isAscending
        let entity = GBTConstants.Entity.STUDENT
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)//Event.fetchRequest()
        //let fetchRequest = NSFetchRequest(entityName: entity)
        fetchRequest.fetchBatchSize = 20
        
        //PREDICATE
        /*
         NOTE: GB_SELECTED_TERM_ID <- please declared this in the constants
         FOR NOW: we are hard coding the term_id value
         */
        
        var finalPredicate: NSPredicate
        
        let predicateTID = NSComparisonPredicate(keyPath: "term_id", withValue: self.term_id, isExact: true)
        
        if self.student_name.characters.count > 0 {
            let predicateName = NSComparisonPredicate(keyPath: "name", withValue: self.student_name, isExact: false)
            finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateTID, predicateName])
        } else {
            finalPredicate = predicateTID
        }
        
        fetchRequest.predicate = finalPredicate
        
        let descriptorSelector = #selector(NSString.localizedCompare(_:))
        if self.sort_by_gender {
            let sortGender = NSSortDescriptor(key: "gender", ascending: ascending, selector:descriptorSelector)
            fetchRequest.sortDescriptors = [sortGender]
        } else {
            let sortName = NSSortDescriptor(key: "name", ascending: ascending, selector:descriptorSelector)
            fetchRequest.sortDescriptors = [sortName]
        }
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        let tableViewController = tableView
        
        switch type {
        case .insert:
            tableViewController?.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableViewController?.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        let tableViewController = tableView
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableViewController?.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = tableViewController?.cellForRow(at: indexPath) {
                    configureCell(cell as! GBTStudentListCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableViewController?.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y;
        self.scrollDelegate?.gbScrollViewDidScroll(GBPanelType.StudentPanel, scrollTo: offsetY)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewWillBeginDragging(GBPanelType.StudentPanel)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.StudentPanel)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate == false {
            self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.StudentPanel)
        }
    }
    
}

extension GBTStudentListPanel: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("DID CHANGE \(searchText)")
        self.student_name = searchText
        self.reloadFetchedResultsController()
        self.notification.post(name: Notification.Name(rawValue: "GBT_STUDENT_SEARCH"), object: searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("CANCEL BUTTON")
    }
    
}

