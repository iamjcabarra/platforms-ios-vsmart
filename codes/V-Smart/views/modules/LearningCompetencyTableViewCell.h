//
//  LearningCompetencyTableViewCell.h
//  V-Smart
//
//  Created by Julius Abarra on 10/1/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LearningCompetencyTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *butCheckBox;
@property (strong, nonatomic) IBOutlet UILabel *lblLCCode;
@property (strong, nonatomic) IBOutlet UILabel *lblLCTitle;

@end
