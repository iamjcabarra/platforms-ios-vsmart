//
//  GBTCreateTestView.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 07/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTCreateTestView: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var highestScoreField: UITextField!
    @IBOutlet weak var testTitleField: UITextField!
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    var term_id = ""
    var component_id = ""
    
    fileprivate let notification = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.saveButton.addTarget(self, action: #selector(self.saveButtonAction(_:)), for: .touchUpInside)
        self.cancelButton.addTarget(self, action: #selector(self.cancelButtonAction(_:)), for: .touchUpInside)
    }
    
    func cancelButtonAction(_ b: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveButtonAction(_ b: UIButton) {
        
        let entry_title = "\(self.testTitleField.text!)"
        let highest_score = "\(self.highestScoreField.text!)"
        
        if entry_title.characters.count > 0 && highest_score.characters.count > 0 {
            let cs_id = self.gbdm.fetchUserDefaultsObject(forKey: GBTConstants.UserDefined.GBTCSID) as! String
            
//            gbdm.requestCreateGradeBookEntry(entry_title,
//                                             score: highest_score,
//                                             csid:cs_id,
//                                             termid: self.term_id, componentid: self.component_id) { done in
//                                                if done {
//                                                    self.notification.postNotificationName("GBT_RELOAD_GRADEBOOK_CONTENT", object: nil)
//                                                    // do nothing
//                                                } else {
//                                                    displayAlert(withTitle: "", withMessage: error)
//                                                }
//            }
            
            self.gbdm.requestCreateGradeBookEntry(entry_title, score: highest_score, csid: cs_id, termid: self.term_id, componentid: self.component_id, handler: { (done, error) in
                if error == nil {
                    DispatchQueue.main.async(execute: {
                        let dict: [String:String] = ["term_id": self.term_id,
                                                     "component_id": self.component_id]
                        self.notification.post(name: Notification.Name(rawValue: "GBT_RELOAD_GRADEBOOK_CONTENT"), object: dict)
                        self.dismiss(animated: true, completion: nil)
                    })
                } else {
                    self.displayAlert(withTitle: "", withMessage: error!)
                }
            })
            
        } else {
            displayAlert(withTitle: "", withMessage: "Title and highest possible score is required.")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.highestScoreField {
        let countdots = textField.text!.components(separatedBy: ".").count - 1
        
        if countdots > 0 && string == "." {
            return false
        }
        
        // Create an `NSCharacterSet` set which includes everything *but* the digits
        let inverseSet = CharacterSet(charactersIn:"0123456789.").inverted
        
        // At every character in this "inverseSet" contained in the string,
        // split the string up into components which exclude the characters
        // in this inverse set
        let components = string.components(separatedBy: inverseSet)
        
        // Rejoin these components
        let filtered = components.joined(separator: "")  // use join("", components) if you are using Swift 1.2
        
        // If the original string is equal to the filtered string, i.e. if no
        // inverse characters were present to be eliminated, the input is valid
        // and the statement returns true; else it returns false
        return string == filtered
        }
        
        return true
    }
    
    func displayAlert(withTitle title: String, withMessage message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.preferredContentSize = CGSize(width: 250, height: 150);
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
