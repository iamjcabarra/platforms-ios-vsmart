//
//  StudentShortcutCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 4/20/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "StudentShortcutCell.h"

@implementation StudentShortcutCell

- (void)awakeFromNib {
    // Initialization code
    
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    self.profileImage.clipsToBounds = YES;
    self.profileImage.layer.borderWidth = 1.5f;
    self.profileImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
