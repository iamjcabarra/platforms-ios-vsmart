//
//  NoteHelper.m
//  V-Smart
//
//  Created by Ryan Migallos on 2/6/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "NoteHelper.h"
#import "NNote.h"
#import "NNoteTag.h"
#import "NNoteBook.h"

@implementation NoteHelper

- (void)saveMessage:(NSString *)message doneBlock:(NoteHelperDoneBlock)doneBlock
{
    NSString *title_string = [self parseFirstTwoWordsFromDescription: message ];

    NoteEntry *entry = [NoteEntry new];
    entry.title = title_string;
    entry.description = [NSString stringWithFormat:@"%@", message ];
    entry.tags = @"Social Stream"; //default

    NoteDataManager *nm = [NoteDataManager sharedInstance];
    NoteBook *folder = [nm getNoteBookByNoteBookName:@"General" useMainThread:YES];
    entry.noteBookName = folder.noteBookName;
    entry.color = 5;
    entry.fontTypeId = 0;
    entry.fontSizeId = 10;
    entry.isArchive = NO;
    [nm addNote:entry useMainThread:YES];
    
    if (doneBlock) {
        doneBlock(YES);
    }
}

- (NSString *)trimWhiteSpaces:(NSString *)string {
    
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] ;
}

- (NSString *)parseFirstTwoWordsFromDescription:(NSString *)content {
    
    NSString *string = [self trimWhiteSpaces:content];
    NSArray *array = [string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self != %@", @""];
    array = [array filteredArrayUsingPredicate:predicate];
    
    if (array.count == 1) {
        string = [NSString stringWithFormat:@"%@", array[0]] ;
    }
    
    if (array.count > 1) {
        string = [NSString stringWithFormat:@"%@ %@", array[0], array[1] ];
    }
    
    return string;
}

@end
