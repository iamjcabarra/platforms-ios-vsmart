//
//  QuizListCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/5/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuizListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *courseTitle;
@property (strong, nonatomic) IBOutlet UILabel *courseDuration;
@property (strong, nonatomic) IBOutlet UILabel *courseAttempts;
@property (strong, nonatomic) IBOutlet UILabel *courseDescription;

@end
