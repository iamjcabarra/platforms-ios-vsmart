//
//  CurriculumListHeaderView.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/17/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurriculumListHeaderView : UITableViewHeaderFooterView

@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelOption;

@end
