//
//  ColorPopOverViewController.m
//  V-Smart
//
//  Created by VhaL on 2/10/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NoteColorViewController.h"
#define kRowHeight 55

@interface NoteColorViewController ()

@end

@implementation NoteColorViewController
@synthesize delegate, parent, buttonAllColor, selectedColor, colorArray;

-(void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    self.tableView.layer.borderWidth = 1;
    self.tableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [self loadData];
    
    UINib *cellNib = [UINib nibWithNibName:@"NoteColorCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"NoteColorCell"];
}

-(void)loadData{
    colorArray = [[NoteDataManager sharedInstance] getColors:NO useMainThread:YES];
    colorArray = [[colorArray reverseObjectEnumerator] allObjects]; // jca-08012017: Reverse!
    [self.tableView reloadData];
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kRowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [colorArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"NoteColorCell";
    NoteColorCell *cell = (NoteColorCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        cell = (NoteColorCell*)[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    Color *color = [colorArray objectAtIndex:indexPath.row];

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    //localize color name
    cell.colorLabel.text = [self localizeColorName:color.colorName];
    
//    cell.colorLabel.textColor = [UIColor whiteColor];
    cell.colorLabel.font = [UIFont systemFontOfSize:14];
//    cell.colorLabel.shadowColor = [UIColor grayColor];
//    cell.colorLabel.shadowOffset = CGSizeMake(1, 1);
    cell.colorBackground.backgroundColor  = [[NoteDataManager sharedInstance] getUIColorFromColorObject:color];
    cell.colorBackground.layer.cornerRadius = cell.colorBackground.frame.size.width/2;
    cell.colorBackground.layer.borderColor = [self darkerColorForColor:cell.colorBackground.backgroundColor].CGColor;
    cell.colorBackground.layer.borderWidth = 1;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedColor = [colorArray objectAtIndex:indexPath.row];
    
    if ([delegate respondsToSelector:@selector(buttonColorApplyTapped:)]){
        [delegate performSelector:@selector(buttonColorApplyTapped:) withObject:selectedColor];
    }
    
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.backgroundColor = [self darkerColorForColor:cell.backgroundColor];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.backgroundColor = [self lighterColorForColor:cell.backgroundColor];
}

- (UIColor *)lighterColorForColor:(UIColor *)color{
    CGFloat r, g, b, a;
    if ([color getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:MIN(r + 0.2, 1.0)
                               green:MIN(g + 0.2, 1.0)
                                blue:MIN(b + 0.2, 1.0)
                               alpha:a];
    return nil;
}

- (UIColor *)darkerColorForColor:(UIColor *)color{
    CGFloat r, g, b, a;
    if ([color getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:MAX(r - 0.2, 0.0)
                               green:MAX(g - 0.2, 0.0)
                                blue:MAX(b - 0.2, 0.0)
                               alpha:a];
    return nil;
}

-(IBAction)clearButtonTapped:(id)sender{
    
    [self.tableView reloadData];
    
    selectedColor = nil;
    
    if ([delegate respondsToSelector:@selector(buttonColorApplyTapped:)]){
        [delegate performSelector:@selector(buttonColorApplyTapped:) withObject:selectedColor];
    }
}

- (NSString *)localizeColorName:(NSString *)colorName
{
    /* localizable strings */
    NSString *redColor = NSLocalizedString(@"Red", nil); //checked
    NSString *blueColor = NSLocalizedString(@"Blue", nil); //checked
    NSString *yellowColor = NSLocalizedString(@"Yellow", nil); //checked
    NSString *greenColor = NSLocalizedString(@"Green", nil); //checked
    NSString *purpleColor = NSLocalizedString(@"Purple", nil); //checked
    NSString *whiteColor = NSLocalizedString(@"White", nil); //checked
    
    NSDictionary *colorData = @{@"Blue":blueColor,
                                @"Red":redColor,
                                @"Yellow":yellowColor,
                                @"Purple":purpleColor,
                                @"Green":greenColor,
                                @"White":whiteColor};
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF == %@", colorData];
    NSArray *result = [colorData.allKeys filteredArrayUsingPredicate:predicate];
    if (result.count > 0) {
        return colorData[colorName];
    }

    return colorData[colorName];
    //NOTE: return default color if the color name is not present
//    return colorData[@"Blue"];
}


@end
