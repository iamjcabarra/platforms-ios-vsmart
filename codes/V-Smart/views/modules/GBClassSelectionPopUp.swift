//
//  GBClassSelectionPopUp.swift
//  V-Smart
//
//  Created by Julius Abarra on 25/05/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol GBClassSelectionPopUpDelegate: class {
    func selectedClassData(_ option:NSDictionary)
}

class GBClassSelectionPopUp: UITableViewController, NSFetchedResultsControllerDelegate {
    weak var delegate:GBClassSelectionPopUpDelegate?
    var classes: NSArray! = []
    
    // MARK: - Shared Data Manager
    
    fileprivate lazy var dataManager: GradeBookDataManager = {
        let tm = GradeBookDataManager.sharedInstance()
        return tm!
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadClasses()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Class Primary Methods
    
    func loadClasses() {
        let userid = self.dataManager.loginUser() as String
        
        self.dataManager.requestCourseList(forUser: userid) { (data) in
//            if (data != nil) {
//                dispatch_async(dispatch_get_main_queue(), {
//                    self.classes = self.dataManager.getObjectsForEntity("GBCourse", predicate: nil)
//                    self.tableView.reloadData()
//                })
//            }
        }
    }
    
    // MARK: - Table View Data Source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "classCellIdentifier")
        let class_object = self.fetchedResultsController.object(at: indexPath) 
        
        let section_name = class_object.value(forKey: "section_name") as! String
        let course_name = class_object.value(forKey: "course_name") as! String
        
        cell.textLabel?.text = section_name + " - " + course_name
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let class_object = self.fetchedResultsController.object(at: indexPath) 
        
        let classid = class_object.value(forKey: "id") as! String
        let section_name = class_object.value(forKey: "section_name") as! String
        let course_name = class_object.value(forKey: "course_name") as! String
        
        let class_data: [String: String] = ["id": classid, "section_name": section_name, "course_name": course_name]
        
        self.dismiss(animated: true, completion: {
            self.delegate?.selectedClassData(class_data as NSDictionary)
        })
    }
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kGBCourseEntityV2)
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let context = self.dataManager.mainContext
        
        //let fetchRequest = NSFetchRequest()
        // Edit the entity name as appropriate.
        //let entity = NSEntityDescription.entity(forEntityName: kGBCourseEntityV2, in: context!)
        //fetchRequest.entity = entity
        
        // Filter by event type [current|past]
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "course_name", ascending: false)
        //        let sortDescriptors = [sortDescriptor]
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context!, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        //        var error: NSError? = nil
        do {
            try _fetchedResultsController!.performFetch()
        } catch _ as NSError {
            //            error = error1
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //println("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            print("update tableview")
            tableView.reloadData()
        //            self.configureCell(tableView.cellForRowAtIndexPath(indexPath!)!, atIndexPath: indexPath!)
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .fade)
            tableView.insertRows(at: [newIndexPath!], with: .fade)
            //        default:
            //            return
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
}
