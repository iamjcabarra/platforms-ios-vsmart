//
//  SocialStreamMessage.m
//  V-Smart
//
//  Created by Ryan Migallos on 1/22/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "SocialStreamMessage.h"
#import "SocialStreamMenuCollectionViewCell.h"
#import "MainHeader.h"
#import "ResourceManager.h"
#import "EGOImageView.h"

#import "SocialStreamIconPopViewController.h"
#import "SocialStreamStickerViewController.h"

#import "AppDelegate.h"

@interface SocialStreamMessage () <UICollectionViewDelegate, SocialStreamIconPopDelegate, SocialStreamStickerDelegate>

@property (nonatomic, strong) IBOutlet UITextView *textView;
@property (nonatomic, strong) IBOutlet UILabel *emotionField;
@property (weak, nonatomic) IBOutlet UIImageView *emotionImage;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint *constraintHeight;
@property (nonatomic, strong) IBOutlet UIButton *buttonEmoji;
@property (nonatomic, strong) IBOutlet UIButton *buttonEmoticon;

@property (nonatomic, strong) ResourceManager *rm;
@property (nonatomic, strong) NSArray *menuItems;

@property (weak) SocialStreamIconPopViewController *p;
@property (weak) SocialStreamStickerViewController *svc;

@property (nonatomic, strong) NSDictionary *emotionData;
@property (nonatomic, strong) NSDictionary *lookupBadWords;

@property (nonatomic, assign) BOOL didPickEmoticon;

@end

@implementation SocialStreamMessage

static NSString *kCellMenuIdentifier = @"cell_collection_menu";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.closeButton.hidden = YES;
    
    if (self.mdictionary) {
        if ([self.mdictionary[@"emoticon_id"] rangeOfString:@"null"].location == NSNotFound ) {
            [self processEditWithEmoticon];
            [self expandEmotionField];
        } else {
            [self shrinkEmotionField];
        }
        self.textView.text = [NSString stringWithFormat:@"%@", self.mdictionary[@"message"] ];
    }
    
    self.didPickEmoticon = NO;
    
    self.rm = [AppDelegate resourceInstance];
    self.lookupBadWords = [NSDictionary dictionaryWithDictionary:[self.rm fetchBadwords]];
    
    [self loadStickerMenus];
    
}

- (void)loadStickerMenus {
    
    __weak typeof(self) weakSelf = self;
    [self.rm requestSocialStreamStickers:^(NSArray *list) {
        if (list) {
            
//            NSLog(@"list : %@", list);
            weakSelf.menuItems = [NSArray arrayWithArray:list];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.collectionView reloadData];
            });
        }
    }];
}

//- (void)viewWillLayoutSubviews{
//    [super viewWillLayoutSubviews];
//    self.view.superview.bounds = CGRectMake(0, 0, 600, 300);
//}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.textView becomeFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)processEditWithEmoticon {
    self.emotionField.text = self.icon_text;
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", self.icon_url  ]];
    self.emotionImage.image = [[EGOImageLoader sharedImageLoader] imageForURL:imageURL shouldLoadWithObserver:nil];
    
//    NSString *emoticon_id = [NSString stringWithFormat:@"%@", self.emotionData[@"id"] ];
//    self.emotionData[@"id"] = emoticon_id;
}

- (NSNumber *)typeForMessage:(NSString *)message {
    NSNumber *type = @1;
    if (message != nil && (message.length > 1) ) {
        NSArray *tokens = [message componentsSeparatedByString:@" "];
        for (NSString *w in tokens) {
            if ([w hasPrefix:@"http://"] || [w hasPrefix:@"https://"] ) {
                if ([[NSURL URLWithString:w] host]) {
                    return @2;
                }
            }
        }
    }
    return type;
}

-(void) alertMessage: (NSString *) message {
    [self.view makeToast:message duration:2.0f position:@"center"];
}

- (IBAction)sendUserMessage:(id)sender {
    
    NSString *message = [NSString stringWithFormat:@"%@", self.textView.text];
    NSNumber *messageType = [self typeForMessage:message];
    __weak typeof(self) weakSelf = self;
//    BOOL status = [self analyzeString:message lookup:self.lookupBadWords];
    BOOL status = [Utils analyzeString:message lookup:self.lookupBadWords];
    if (status) {
    
        //EDIT
        if (self.mdictionary) {
            
            if (self.didPickEmoticon) {
            self.mdictionary[@"emoticon_id"] = [NSString stringWithFormat:@"%@", self.emotionData[@"id"] ];
            } else {
//                 do not change self.mdictionary[@"emoticon_id"];
                NSLog(@"ITO -> [%@]", self.mdictionary[@"emoticon_id"]);
            }
            
            if (self.closeButton.hidden) {
                self.mdictionary[@"emoticon_id"] = @"(null)";
            }
            
            self.mdictionary[@"message"] = message;
            self.mdictionary[@"is_message"] = messageType;
            self.mdictionary[@"message_id"] = self.message_id;
            
            [self.rm editMessage:self.mdictionary doneBlock:^(BOOL status) {
                if (status) {
                    NSLog(@"EDIT MESSAGE SUCCESSFUL");
                    [self unwindFromConfirmationForm:sender];
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf alertMessage:NSLocalizedString(@"Cannot post empty message", nil)];
                    });
                }
            }];
        }

        //NEW
        if (!self.mdictionary) {
            
            NSString *emotionId = [NSString stringWithFormat:@"%@", self.emotionData[@"id"] ];
            
            if (self.closeButton.hidden) {
                emotionId = @"(null)";
            }
            
            self.mdictionary = [ @{@"group_id":[NSNumber numberWithUnsignedInteger:self.groupid],
                                   @"emoticon_id":emotionId,
                                   @"is_message":messageType,
                                   @"message":message} mutableCopy];
            
            [self.rm postMessage:self.mdictionary doneBlock:^(BOOL status) {
                if (status) {
                    NSLog(@"POST MESSAGE SUCCESSFUL");
                    [self unwindFromConfirmationForm:sender];
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf alertMessage:NSLocalizedString(@"Cannot post empty message", nil)];
                    });
                    self.mdictionary = nil;
                }
            }];
        }
    }
    
}

//- (BOOL)analyzeString:(NSString *)string lookup:(NSDictionary *)hashmap {
//    
//    NSArray *words = [string componentsSeparatedByString:@" "];
//    
//    for (NSString *key in words) {
//        
//        NSString *comparedkey = [NSString stringWithFormat:@"%@", [key lowercaseString] ];
//        if ( [hashmap objectForKey:comparedkey] ) {
//            NSString *message = [NSString stringWithFormat:@"'%@' is bad word!!!", key ];
//            AlertWithMessageAndDelegate(@"Invalid Entry", message, self);
//            return NO;
//        }
//    }
//    return YES;
//}

- (IBAction)unwindFromConfirmationForm:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.menuItems.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SocialStreamMenuCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellMenuIdentifier forIndexPath:indexPath];
    
    NSDictionary *data = self.menuItems[indexPath.row];
    NSString *path = [NSString stringWithFormat:@"%@", data[@"cover_url"] ];
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", path ]];
    cell.iconImageView.image = [[EGOImageLoader sharedImageLoader] imageForURL:imageURL shouldLoadWithObserver:nil];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SocialStreamMenuCollectionViewCell *cell = (SocialStreamMenuCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"showStickerBox" sender:cell];
}

- (void)expandEmotionField {
    
    if (self.emotionField.bounds.size.height == 30) {
        return;
    }
    
    CGFloat height = 30; //(self.emotionField.bounds.size.height == 0) ? 30.0f : 0.0f;
    self.closeButton.hidden = NO;
    
    self.constraintHeight.constant = height;
    [self.emotionField setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:0.50f animations:^{
        [self.emotionField layoutIfNeeded];
        [self.textView layoutIfNeeded];
    }];
}

- (void)shrinkEmotionField {
    
    if (self.emotionField.bounds.size.height == 0) {
        return;
    }
    
    CGFloat height = 0; //(self.emotionField.bounds.size.height == 0) ? 30.0f : 0.0f;
    self.closeButton.hidden = YES;
    self.constraintHeight.constant = height;
    [self.emotionField setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:0.50f animations:^{
        [self.emotionField layoutIfNeeded];
        [self.textView layoutIfNeeded];
    }];
}

- (IBAction)closeEmotionView:(id)sender {
    
    [self shrinkEmotionField];
    
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    self.p = (SocialStreamIconPopViewController *)segue.destinationViewController;
    
    if ([segue.identifier isEqualToString:@"showEmoticonBox"]) {
//        [self expandEmotionField];
        self.p.type = @"emoticon";
        self.p.delegate = self;
    }

    if ([segue.identifier isEqualToString:@"showEmojiBox"]) {
        self.p.type = @"emoji";
        self.p.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"showStickerBox"]) {
        
        NSLog(@"show sticker Box");
        
        SocialStreamMenuCollectionViewCell *cell = (SocialStreamMenuCollectionViewCell *)sender;
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        
        NSDictionary *data = self.menuItems[indexPath.row];
        self.svc = (SocialStreamStickerViewController *)segue.destinationViewController;
        self.svc.menuItems = [NSArray arrayWithArray:data[@"stickers"]];
        self.svc.delegate = self;
    }
}

- (void)didFinishSelectingData:(NSDictionary *)data type:(NSString *)type {
    

    
    if ([type isEqualToString:@"emoticon"]) {
        
        [self expandEmotionField];
        NSLog(@"selected data : %@", data);
        
        self.didPickEmoticon = YES;
        
        self.emotionData = [NSDictionary dictionaryWithDictionary:data];
        
        /*
         chars = ":)";
         "icon_text" = "happy.";
         "icon_url" = "http://lms.vsmart.info/socialstream/img/emoji/smiley.png";
         id = 1;
         name = happy;
         */
        
        NSString *icon_url = [NSString stringWithFormat:@"%@", data[@"icon_url"] ];
        NSString *icon_text = [NSString stringWithFormat:@"%@", data[@"icon_text"] ];
        
//        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] init];
//        NSAttributedString *attrStringWithImage = [self setImageWithURL:icon_url toView:self.textView];
//        NSAttributedString *attrString = [self setTextString:icon_text];
//        [attributedString appendAttributedString:attrStringWithImage];
//        [attributedString appendAttributedString:attrString];
        self.emotionField.text = icon_text;
        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", icon_url  ]];
        self.emotionImage.image = [[EGOImageLoader sharedImageLoader] imageForURL:imageURL shouldLoadWithObserver:nil];
        
        self.p.delegate = nil;
        [self.p dismissViewControllerAnimated:YES completion:nil];
    }
    
    if ([type isEqualToString:@"sticker"]) {
        NSString *stickerIMG = [NSString stringWithFormat:@"%@", data[@"sticker"] ];
        self.svc.delegate = nil;
        [self.svc dismissViewControllerAnimated:YES completion:nil];        
        [self postStickerMessageWithData:stickerIMG];
    }

    
}

- (NSAttributedString *)setImageWithURL:(NSString *)urlstring toView:(UIView *)textfield {
    
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    UIImage *imageAttachment = [[EGOImageLoader sharedImageLoader] imageForURL:[NSURL URLWithString:urlstring] shouldLoadWithObserver:nil];
//    CGFloat oldWidth = textAttachment.image.size.width;
//    CGFloat scaleFactor = oldWidth / (textfield.frame.size.width - 10);
    imageAttachment = [UIImage imageWithCGImage:imageAttachment.CGImage scale:2 orientation:UIImageOrientationUp];
    NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
    
    return attrStringWithImage;
}

- (NSAttributedString *)setTextString:(NSString *)textstring {
    
  return [[NSAttributedString alloc] initWithString:textstring];
}

- (void)postStickerMessageWithData:(NSString *)stickerdata {
    
    //EDIT
    if (self.mdictionary) {
        self.mdictionary[@"message"] = stickerdata;
        self.mdictionary[@"emoticon_id"] = @0;
        self.mdictionary[@"is_message"] = @0;
        [self.rm editMessage:self.mdictionary doneBlock:^(BOOL status) {
            if (status) {
                NSLog(@"EDIT MESSAGE SUCCESSFUL");
                [self unwindFromConfirmationForm:self];
            }
        }];
    }
    
    //NEW
    if (!self.mdictionary) {
        self.mdictionary = [ @{@"group_id":@(self.groupid),
                               @"emoticon_id":@0,
                               @"is_message":@0,
                               @"message":stickerdata} mutableCopy];
        [self.rm postMessage:self.mdictionary doneBlock:^(BOOL status) {
            if (status) {
                NSLog(@"POST MESSAGE SUCCESSFUL");
                [self unwindFromConfirmationForm:self];
            }
        }];
    }
}

@end
