//
//  FolderViewController.m
//  V-Smart
//
//  Created by VhaL on 2/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "TagViewController.h"

#define kRowHeight 44
@interface TagViewController ()
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@end

@implementation TagViewController
@synthesize tagArray, textField, parent, delegate, selectedTag, clearButton;

-(void)viewDidLoad{
    [super viewDidLoad];
    self.tableView.layer.borderWidth = 1;
    self.tableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    UINib *cellNib = [UINib nibWithNibName:@"TagCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"TagCell"];
    
    [self loadData];
    [self addRefreshMode];
}

-(void)addRefreshMode{
    self.refreshControl = [[UIRefreshControl alloc] init];
    NSString *pullRefresh = NSLocalizedString(@"Pull to Refresh",nil);
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:pullRefresh];
    [self.refreshControl addTarget:self action:@selector(refreshTriggered) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshControl];
}

-(void)refreshTriggered{
    [self loadData];
    [self.refreshControl endRefreshing];
}

-(void)loadData{
    [self clearButtonTapped:nil];
    
    tagArray = [NSMutableArray arrayWithArray:[[NoteDataManager sharedInstance] getTags:NO useMainThread:YES]];
    [self.tableView reloadData];
}

-(IBAction)buttonAddTapped:(id)sender{
    
    [self clearButtonTapped:nil];
    
    Tag *result = [[NoteDataManager sharedInstance] addTag:textField.text useMainThread:YES];
    
    [tagArray addObject:result];
    
    NSSortDescriptor *sortByTagName = [NSSortDescriptor sortDescriptorWithKey:@"tagName" ascending:YES];
    NSArray *sortedArray = [tagArray sortedArrayUsingDescriptors:@[sortByTagName]];
    tagArray = [NSMutableArray arrayWithArray:sortedArray];
    
    [self.tableView reloadData];
    textField.text = @"";
    [textField resignFirstResponder];
}

-(IBAction)clearButtonTapped:(id)sender{
    
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
    
    selectedTag = nil;
    
    if ([delegate respondsToSelector:@selector(buttonFilterApplyTapped:)]){
        [delegate performSelector:@selector(buttonFilterApplyTapped:) withObject:selectedTag];
    }
}

-(IBAction)buttonEditTagTapped:(TagCell*)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    Tag *tag = [tagArray objectAtIndex:indexPath.row];
    
    tag.tagName = [sender.tagTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    tag.modified = [NSDate date];
    [[NoteDataManager sharedInstance] editTag:tag useMainThread:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self buttonAddTapped:nil];
    [self.textField resignFirstResponder];
    return YES;
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kRowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [tagArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"TagCell";
    TagCell *cell = (TagCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        cell = [[TagCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    TagCell __weak *weakCell = cell;
    
    [cell setAppearanceWithBlock:^{
        weakCell.leftUtilityButtons = [self leftButtons];
        weakCell.rightUtilityButtons = [self rightButtons];
        weakCell.delegate = self;
        weakCell.containingTableView = self.tableView;
    } force:NO];
    
    [cell setCellHeight:cell.frame.size.height];
    [cell initializeCell];
    cell.delegate = self;
    
    Tag *tag = [tagArray objectAtIndex:indexPath.row];
    
    cell.tagLabel.text = tag.tagName;
    cell.tagTextField.text = tag.tagName;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedTag = [tagArray objectAtIndex:indexPath.row];
    [textField resignFirstResponder];
    
    if ([delegate respondsToSelector:@selector(buttonFilterApplyTapped:)]){
        [delegate performSelector:@selector(buttonFilterApplyTapped:) withObject:selectedTag];
    }
}

#pragma mark - SWTableViewDelegate

- (NSArray *)rightButtons{
    UIImage *copyImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_flyout_rename.png"] scaledToSize:CGSizeMake(15, 15)];
//    UIImage *shareImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_notes_share.png"] scaledToSize:CGSizeMake(15, 15)];
    UIImage *trashImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_notes_trash.png"] scaledToSize:CGSizeMake(15, 15)];
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0] icon:copyImg];
//    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:0] icon:shareImg];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0] icon:trashImg];
//    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0] title:@""];
    return rightUtilityButtons;
}

- (NSArray *)leftButtons{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:0.07 green:0.75f blue:0.16f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"check.png"]];
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:1.0f green:1.0f blue:0.35f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"clock.png"]];
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:1.0f green:0.231f blue:0.188f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"cross.png"]];
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:0.55f green:0.27f blue:0.07f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"list.png"]];
    
    return leftUtilityButtons;
}

-(void)swipeableTableViewCell:(TagCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            NSLog(@"left button 0 was pressed");
            break;
        case 1:
            NSLog(@"left button 1 was pressed");
            break;
        case 2:
            NSLog(@"left button 2 was pressed");
            break;
        case 3:
            NSLog(@"left btton 3 was pressed");
        default:
            break;
    }
}

-(void)swipeableTableViewCell:(TagCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    switch (index) {
        case 0:
        {
            [cell hideEditControls:NO];
            [cell hideUtilityButtonsAnimated:YES];
            break;
        }
        case 1:
        {
            // Delete button was pressed
            
            Tag *tag = [tagArray objectAtIndex:indexPath.row];
            bool deleteSuccess = [[NoteDataManager sharedInstance] deleteTag:tag useMainThread:YES];
            
            if (deleteSuccess){
                [tagArray removeObjectAtIndex:indexPath.row];
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            }
            
            break;
        }
        default:
            break;
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(TagCell *)cell {
    return YES;
}



@end
