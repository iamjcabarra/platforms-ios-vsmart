//
//  PlayListDocumentViewer.h
//  V-Smart
//
//  Created by Ryan Migallos on 4/11/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayListDocumentViewer : UIViewController

@property (strong, nonatomic) NSData *contentData;
@property (strong, nonatomic) NSString *mimeType;

@end
