//
//  SocialStreamIconPopViewCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 2/3/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@interface SocialStreamIconPopViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet EGOImageView *imageView;

@end
