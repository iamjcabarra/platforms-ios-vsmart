//
//  ShowLikesViewController.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 05/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class SSShowLikesViewController: UIViewController, NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var predicate_value: String!
    var predicate_keypath: String!
    var entity: String!
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var emptyView: UIView!
    
    // MARK: - Singleton Method
    fileprivate lazy var ssdm: SocialStreamDataManager = {
        let dataManager = SocialStreamDataManager.sharedInstance
        return dataManager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonAction(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - Table View Data Source
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        self.emptyView.isHidden = (sectionCount > 0)
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        let count = sectionData.numberOfObjects
        self.emptyView.isHidden = (count > 0)
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SS_LIKE_CELL_ID", for: indexPath)
        cell.selectionStyle = .none
        
        
        
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: UITableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        let showLikesCell = cell as! SSShowLikesCell
//        avatar, first_name, last_name
        
        let avatar: String! = self.ssdm.stringValue(mo.value(forKey: "avatar"))
        
        let first_name: String! = self.ssdm.stringValue(mo.value(forKey: "first_name"))
        let last_name: String! = self.ssdm.stringValue(mo.value(forKey: "last_name"))
        
        showLikesCell.likerName.text = "\(first_name!) \(last_name!)"
        
        showLikesCell.tag = (indexPath as NSIndexPath).row
        
        let avatarURL = URL(string: avatar!)!
        self.getDataFromUrl(avatarURL) { (data, response, error) in
            guard let data = data, error == nil else { return }
            if showLikesCell.tag == (indexPath as NSIndexPath).row {
                DispatchQueue.main.async(execute: {
                    showLikesCell.likerImage.image = UIImage(data: data)
                })
            }
        }
        
    }
    
    func getDataFromUrl(_ url:URL, completion: @escaping ((_ data: Data?, _ response: URLResponse?, _ error: Error? ) -> Void)) {
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            completion(data, response, error)
            }) .resume()
    }
    
    
    // MARK: - Fetched Results Controller
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {

        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = ssdm.getMainContext()
        
        let ascending = false
        
        let entity = self.entity
        
//        let fetchRequest = NSFetchRequest(entityName: entity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity!)
        fetchRequest.fetchBatchSize = 20
        
        let predicate = NSComparisonPredicate(keyPath: predicate_keypath, withValue: predicate_value, isExact: true)
        fetchRequest.predicate = predicate
        
        let sortDescriptor = NSSortDescriptor(key: "date_modified_date", ascending: ascending)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        let tableViewController = tableView
        
        switch type {
        case .insert:
            tableViewController?.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableViewController?.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        let tableViewController = tableView
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableViewController?.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = tableViewController?.cellForRow(at: indexPath) {
                    configureCell(cell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableViewController?.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        tableView.reloadData()
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
