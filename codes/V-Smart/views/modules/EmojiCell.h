//
//  EmoItemCell.h
//  V-Smart
//
//  Created by VhaL on 3/24/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmojiCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UIImageView *imageView;

- (void)initializeCell:(id)delegate;
@end
