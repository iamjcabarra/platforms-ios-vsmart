//
//  LPMLessonCreatorViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 24/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMLessonCreatorViewController: UIViewController {
    
    @IBOutlet fileprivate var headerView: UIView!
    @IBOutlet fileprivate var lessonOverviewView: UIView!
    @IBOutlet fileprivate var bodyView: UIView!
    @IBOutlet fileprivate var footerView: UIView!
    
    @IBOutlet fileprivate var segmentedControl: UISegmentedControl!
    @IBOutlet fileprivate var segmentedControlImage: UIImageView!
    @IBOutlet fileprivate var previousButton: UIButton!
    @IBOutlet fileprivate var submitButton: UIButton!
    
    @IBOutlet fileprivate var courseLabel: UILabel!
    @IBOutlet fileprivate var unitLabel: UILabel!
    @IBOutlet fileprivate var statusLabel: UILabel!
    @IBOutlet fileprivate var quarterLabel: UILabel!
    @IBOutlet fileprivate var teacherLabel: UILabel!
    @IBOutlet fileprivate var timeLabel: UILabel!
    @IBOutlet fileprivate var competenciesLabel: UILabel!
    
    @IBOutlet fileprivate var actualCourseLabel: UILabel!
    @IBOutlet fileprivate var actualUnitLabel: UILabel!
    @IBOutlet fileprivate var actualStatusLabel: UILabel!
    @IBOutlet fileprivate var actualQuarterLabel: UILabel!
    @IBOutlet fileprivate var actualTeacherLabel: UILabel!
    @IBOutlet fileprivate var actualTimeLabel: UILabel!
    @IBOutlet fileprivate var actualCompetenciesLabel: UILabel!
    @IBOutlet fileprivate var competencyListButton: UIButton!

    var actionType: LPMLessonActionType!
    
    fileprivate let kViewSwapperSegueIdentifier = "SHOW_LESSON_CREATOR_VIEW_SWAPPER"
    fileprivate let kOverviewSVCSegueIdentifier = "SHOW_LESSON_OVERVIEW"
    fileprivate let kDetailsSVCSegueIdentifier = "SHOW_LESSON_DETAILS"
    fileprivate let kCompetencLVSegueIdentifier = "SHOW_COMPETENCY_LIST_VIEW"

    fileprivate var viewSwapper: LPMLessonCreatorViewSwapper!
    fileprivate var competencyListView: LPMLessonCompetencyListViewController!
    fileprivate var viewMode: String!
    
    // MARK: - Data Managers
    
    fileprivate lazy var lpmDataManager: LPMDataManager = {
        let lpdm = LPMDataManager.sharedInstance
        return lpdm
    }()
    
    fileprivate lazy var cpmDataManager: CPMDataManager = {
        let cpdm = CPMDataManager.sharedInstance
        return cpdm
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewMode = self.lpmDataManager.stringValue(self.lpmDataManager.fetchUserDefaultsObject(forKey: "LPM_USER_DEFAULT_IS_VIEW_MODE"))
        
        if self.viewMode == "1" {
            self.headerView.isHidden = true
            self.lessonOverviewView.isHidden = false
            self.footerView.isHidden = true
            self.setUpLessonPlanOverview()
            self.viewSwapper.swapToViewControllerWithSegueIdentifier(self.kDetailsSVCSegueIdentifier)
        }
        else {
            self.headerView.isHidden = false
            self.lessonOverviewView.isHidden = true
            self.footerView.isHidden = false
            self.setupSegmentedControl()
            self.switchSegmentedViewAtIndex(0)
            
            let previousButtonAction = #selector(self.previousButtonAction(_:))
            self.previousButton.addTarget(self, action: previousButtonAction, for: .touchUpInside)
            self.previousButton.isHidden = true
            
            let submitButtonAction = #selector(self.submitButtonAction(_:))
            self.submitButton.addTarget(self, action: submitButtonAction, for: .touchUpInside)
            self.submitButton.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.customizeNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Custom Navigation Bar
    
    func customizeNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "#343434")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        self.title = self.lpmDataManager.stringValue(self.lpmDataManager.fetchUserDefaultsObject(forKey: "LPM_USER_DEFAULT_NAVIGATION_TITLE"))
        
        let customBackButton = UIButton.init(type: UIButtonType.system)
        customBackButton.asBackButtonWithTitle("", color: UIColor.white)
        customBackButton.addTarget(self, action: #selector(self.customBackButtonAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: customBackButton)
    }
    
    func customBackButtonAction(_ sender: UIButton) {
        if self.viewMode != "1" {
            self.showAlertForCancelAction()
        }
        else {
            self.clearImmediateData()
            self.popToViewController(LPMLessonViewController.self)
        }
    }
    
    // MARK: - Navigation Button Setup
    
    fileprivate func setUpPreviousButton() {
        self.previousButton.isHidden = false
        let previousText = NSLocalizedString("Previous", comment: "")
        self.previousButton.setTitle(previousText, for: UIControlState())
        self.previousButton.setTitle(previousText, for: .highlighted)
        self.previousButton.setTitle(previousText, for: .selected)
    }
    
    fileprivate func setUpSubmitButton() {
        self.previousButton.isHidden = false
        self.submitButton.tag = 1
        //        let submitText = NSLocalizedString("Submit", comment: "")
        //        self.submitButton.setTitle(submitText, for: UIControlState())
        //        self.submitButton.setTitle(submitText, for: .highlighted)
        //        self.submitButton.setTitle(submitText, for: .selected)
        
        let text = (self.actionType == LPMLessonActionType.add) ? NSLocalizedString("Submit", comment: "") : NSLocalizedString("Update", comment: "")
        self.submitButton.setTitle(text, for: UIControlState())
        self.submitButton.setTitle(text, for: .highlighted)
        self.submitButton.setTitle(text, for: .selected)
    }
    
    fileprivate func setUpNextButton() {
        self.previousButton.isHidden = true
        self.submitButton.tag = 0
        let nextText = NSLocalizedString("Next", comment: "")
        self.submitButton.setTitle(nextText, for: UIControlState())
        self.submitButton.setTitle(nextText, for: .highlighted)
        self.submitButton.setTitle(nextText, for: .selected)
    }
    
    // MARK: - Navigation Button Handlers

    func previousButtonAction(_ sender: UIButton) {
        self.setUpNextButton()
        self.switchSegmentedViewAtIndex(0)
        self.segmentedControl.selectedSegmentIndex = 0
    }
    
    func submitButtonAction(_ sender: UIButton) {
        self.setUpPreviousButton()
        
        if sender.tag == 0 {
            self.setUpSubmitButton()
            self.switchSegmentedViewAtIndex(1)
            self.segmentedControl.selectedSegmentIndex = 1
        }
        else {
            let template = self.lpmDataManager.stringValue(self.lpmDataManager.fetchUserDefaultsObject(forKey: "LPM_USER_DEFAULT_TEMPLATE_ID"))
            self.actionType == LPMLessonActionType.add ? self.createLessonPlanUsingTemplate(template!) : self.updateLessonPlanUsingTemplate(template!)
        }
    }
    
    // MARK: - Segmented Control
    
    fileprivate func setupSegmentedControl() {
        self.segmentedControl.removeAllSegments()
        
        let titleA = NSLocalizedString("Overview", comment: "")
        let titleB = NSLocalizedString("Details", comment: "")
        
        self.segmentedControl.insertSegment(withTitle: titleA, at: 0, animated: true)
        self.segmentedControl.insertSegment(withTitle: titleB, at: 1, animated: true)
        
        let attributeA = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.white]
        let attributeB = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.white]
       
        self.segmentedControl.setTitleTextAttributes(attributeA, for: UIControlState.selected)
        self.segmentedControl.setTitleTextAttributes(attributeB, for: UIControlState())
        
        let action = #selector(self.segmentedControlAction(_:))
        self.segmentedControl.addTarget(self, action: action, for: .valueChanged)
        self.segmentedControl.selectedSegmentIndex = 0
    }
    
    fileprivate func switchSegmentedViewAtIndex(_ index: Int) {
        DispatchQueue.main.async(execute: {
            let svcSegueIdentifier = index == 0 ? self.kOverviewSVCSegueIdentifier : self.kDetailsSVCSegueIdentifier
            self.viewSwapper.swapToViewControllerWithSegueIdentifier(svcSegueIdentifier)
            index == 0 ? self.setUpNextButton() : self.setUpSubmitButton()
            self.segmentedControlImage.image = index == 0 ? UIImage(named: "segment-01.png") : UIImage(named: "segment-02.png")
        })
    }
    
    func segmentedControlAction(_ sender: UISegmentedControl) {
        self.switchSegmentedViewAtIndex(sender.selectedSegmentIndex)
    }
    
    // MAKR: - Set Up Lesson Plan Overview
    
    fileprivate func setUpLessonPlanOverview() {
        self.courseLabel.text = NSLocalizedString("Course", comment: "")
        self.unitLabel.text = NSLocalizedString("Unit", comment: "")
        self.statusLabel.text = NSLocalizedString("Status", comment: "")
        self.quarterLabel.text = NSLocalizedString("Quarter", comment: "")
        self.teacherLabel.text = NSLocalizedString("Teacher", comment: "")
        self.timeLabel.text = NSLocalizedString("Time Frame", comment: "")
        self.competenciesLabel.text = NSLocalizedString("Learning Competency", comment: "")
        
        guard let overview = self.lpmDataManager.fetchLessonOverview() else {
            print("can't parse lesson plan overview...")
            return
        }
        
        guard
            let unit = overview.value(forKey: "unit") as? String,
            let status = overview.value(forKey: "status") as? String,
            let quarter = overview.value(forKey: "quarter") as? String,
            let start_date = overview.value(forKey: "start_date") as? String,
            let end_date = overview.value(forKey: "end_date") as? String,
            let teacher_name = overview.value(forKey: "teacher_name") as? String
            else {
                print("can't render lesson plan overview...")
                return
        }
        
        let formatted_start_date = self.dateString(start_date, fromFormat: LPMConstants.DateFormat.LESSON_CREATOR, toFormat: LPMConstants.DateFormat.LESSON_DETAILS)
        let formatted_end_date = self.dateString(end_date, fromFormat: LPMConstants.DateFormat.LESSON_CREATOR, toFormat: LPMConstants.DateFormat.LESSON_DETAILS)
        
        self.actualUnitLabel.text = ": \(unit)"
        self.actualQuarterLabel.text = ": \(quarter)"
        self.actualTeacherLabel.text = ": \(teacher_name)"
        self.actualTimeLabel.text = ": \(formatted_start_date) \(NSLocalizedString("to", comment: "")) \(formatted_end_date)"
        
        var statusText = ""
        
        if status == "0" {
            statusText = NSLocalizedString("Draft", comment: "")
        }
        
        if status == "1" {
            statusText = NSLocalizedString("for Approval", comment: "")
        }
        
        if status == "2" {
           statusText = NSLocalizedString("Approved", comment: "")
        }
        
        self.actualStatusLabel.text = ": \(statusText)"
        self.loadAssociatedLearningCompetencies()
    }
    
    // MARK: - Associated Learning Competencies
    
    fileprivate func loadAssociatedLearningCompetencies() {
        var count = 0
        var text: String! = ""
        
        if let competencies = self.lpmDataManager.fetchPreAssociatedLearningCompetencies() {
            for competency in competencies {
                let content: String! = self.lpmDataManager.stringValue(competency.value(forKey: "content") as AnyObject?)
                if count < 2 { text = "\(text!) \(content.replacingOccurrences(of: " \n", with: ","))" }
                count = count + 1
            }
        }
        
        let label = count < 2 ? NSLocalizedString("Learning Competency", comment: "") : NSLocalizedString("Learning Competencies", comment: "")
        self.competenciesLabel.text = "\(label) :"
        
        self.actualCompetenciesLabel.text = count > 0 ? (count > 2 ? "\(String(text.characters.dropLast()))..." : String(text.characters.dropLast())) : text
        _ = self.actualCompetenciesLabel.intrinsicContentSize.width
        self.competencyListButton.isHidden = count > 2 ? false : true
        
        if count > 2 {
            let title = NSLocalizedString("See All", comment: "")
            self.competencyListButton.setTitle(title, for: UIControlState())
            self.competencyListButton.setTitle(title, for: .highlighted)
            self.competencyListButton.setTitle(title, for: .selected)
            
            let competencyListButtonAction = #selector(self.competencyListButtonAction(_:))
            self.competencyListButton.addTarget(self, action: competencyListButtonAction, for: .touchUpInside)
        }
    }
    
    func competencyListButtonAction(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: self.kCompetencLVSegueIdentifier, sender: nil)
        })
    }
    
    // MARK: - Date Formmatter
    
    fileprivate func dateString(_ string: String, fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        
        guard let showDate = dateFormatter.date(from: string) else {
            dateFormatter.dateFormat = toFormat
            return dateFormatter.string(from: Date())
        }
        
        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: showDate)
    }
    
    // MARK: - Create Lesson Plan
    
    fileprivate func createLessonPlanUsingTemplate(_ template: String) {
        let cannotProceed = self.doesContainEmptyRequiredField()
        
        if cannotProceed {
            let message = NSLocalizedString("Please fill in all required fields before you proceed.", comment: "")
            self.view.makeToast(message: message, duration: 5.0, position: "Center" as AnyObject)
        }
        else {
            self.view.isUserInteractionEnabled = false
            self.view.makeToastActivity()
            
            self.lpmDataManager.requestCreateLessonPlanUsingTemplate(template, handler: { (data) in
                if let d = data {
                    let competencies = self.cpmDataManager.fetchSelectedLearningCompetencies()
                        let lpid = d["lpid"] as! String
                        let userid = self.lpmDataManager.logInUserID()
                        let body = ["competencies": competencies]
                        
                    if competencies.count > 0 {
                        self.cpmDataManager.requestCreateCurriculumAssociationToLessonPlanWithID(lpid, andUserWithID: userid, postBody: body as NSDictionary, completionHandler: { (done) in
                            if done {
                                DispatchQueue.main.async(execute: {
                                    self.view.isUserInteractionEnabled = true
                                    self.view.hideToastActivity()
                                    let message = NSLocalizedString("Lesson plan was successfully created.", comment: "")
                                    self.showAlertViewMessage(message, handler: { (doneBlock) in
                                        self.popToViewController(LPMLessonViewController.self)
                                    })
                                })
                            }
                            else {
                                DispatchQueue.main.async(execute: {
                                    self.view.isUserInteractionEnabled = true
                                    self.view.hideToastActivity()
                                    let message = NSLocalizedString("Lesson plan was created but was not successfully associated to curriculum planner.", comment: "")
                                    self.showAlertViewMessage(message, handler: { (doneBlock) in
                                        self.popToViewController(LPMLessonViewController.self)
                                    })
                                })
                            }
                        })
                    }
                    else {
                        DispatchQueue.main.async(execute: {
                            self.view.isUserInteractionEnabled = true
                            self.view.hideToastActivity()
                            let message = NSLocalizedString("Lesson plan was successfully created.", comment: "")
                            self.showAlertViewMessage(message, handler: { (doneBlock) in
                                self.popToViewController(LPMLessonViewController.self)
                            })
                        })
                    }
                    
                    self.clearImmediateData()
                }
                else {
                    DispatchQueue.main.async(execute: {
                        self.view.isUserInteractionEnabled = true
                        self.view.hideToastActivity()
                        let message = NSLocalizedString("Lesson plan was not successfully created.", comment: "")
                        self.view.makeToast(message: message, duration: 5.0, position: "Center" as AnyObject)
                    })
                }
            })
        }
    }
    
    // MARK: - Update Lesson Plan
    
    fileprivate func updateLessonPlanUsingTemplate(_ template: String) {
        let cannotProceed = self.doesContainEmptyRequiredField()
        
        if cannotProceed {
            let message = NSLocalizedString("Please fill in all required fields before you proceed.", comment: "")
            self.view.makeToast(message: message, duration: 5.0, position: "Center" as AnyObject)
        }
        else {
            self.view.isUserInteractionEnabled = false
            self.view.makeToastActivity()
            
            let lpid: String! = self.lpmDataManager.stringValue(self.lpmDataManager.fetchUserDefaultsObject(forKey: "LPM_USER_DEFAULT_LESSON_PLAN_ID"))
            
            self.lpmDataManager.requestUpdateLessonPlanWithID(lpid!, template: template, handler: { (doneBlock) in
                if doneBlock {
                    let competencies = self.cpmDataManager.fetchSelectedLearningCompetencies()
                        let userid = self.lpmDataManager.logInUserID()
                        let body = ["competencies": competencies]
                        
                    self.cpmDataManager.requestUpdateCurriculumAssociationToLessonPlanWithID(lpid, andUserWithID: userid, postBody: body as NSDictionary, completionHandler: { (done) in
                            if done {
                                DispatchQueue.main.async(execute: {
                                    self.view.isUserInteractionEnabled = true
                                    self.view.hideToastActivity()
                                let message = NSLocalizedString("Lesson plan was successfully updated.", comment: "")
                                    self.showAlertViewMessage(message, handler: { (doneBlock) in
                                    self.popToViewController(LPMLessonViewController.self)
                                    })
                                })
                            }
                            else {
                                DispatchQueue.main.async(execute: {
                                    self.view.isUserInteractionEnabled = true
                                    self.view.hideToastActivity()
                                let message = NSLocalizedString("Lesson plan was updated but was not successfully associated to curriculum planner.", comment: "")
                                    self.showAlertViewMessage(message, handler: { (doneBlock) in
                                    self.popToViewController(LPMLessonViewController.self)
                                    })
                                })
                            }
                        })
                    
                    self.clearImmediateData()
                }
                else {
                    DispatchQueue.main.async(execute: {
                        self.view.isUserInteractionEnabled = true
                        self.view.hideToastActivity()
                        let message = NSLocalizedString("Lesson plan was not successfully updated.", comment: "")
                        self.view.makeToast(message: message, duration: 5.0, position: "Center" as AnyObject)
                    })
                }
            })
        }
    }
    
    // MARK: - Required Fields Validation
    
    fileprivate func doesContainEmptyRequiredField() -> Bool {
        
        guard let overview = self.lpmDataManager.fetchLessonOverview() else {
            return true
        }
        
        let keys = overview.entity.propertiesByName.keys
        let requiredFieldsKeys = ["name", "quarter", "start_date", "end_date"]
        
        for key in keys {
            if requiredFieldsKeys.contains(key) {
                let data = overview.value(forKey: key) as! String
                if data == "" { return true }
            }
        }
 
        return false
    }
    
    // MARK: - Alert View Message
    
    func showAlertForCancelAction() {
        let preString = NSLocalizedString("Are you sure you want to cancel", comment: "")
        let midTitleA = NSLocalizedString("creating", comment: "")
        let midTitleB = NSLocalizedString("updating", comment: "")
        let midString = (self.actionType == LPMLessonActionType.add) ? midTitleA : midTitleB
        let sufString = NSLocalizedString("this lesson plan?", comment: "")
        let avMessage = "\(preString) \(midString) \(sufString)"
        
        let alert = UIAlertController(title: "", message: avMessage, preferredStyle: .alert)
        
        let positiveAction = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                self.clearImmediateData()
                self.actionType == LPMLessonActionType.add ? self.popToViewController(LPMTemplateViewController.self) : self.popToViewController(LPMLessonViewController.self)
            })
        }
        
        let negativeAction = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(positiveAction)
        alert.addAction(negativeAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func showAlertViewMessage(_ message: String, handler: @escaping (_ doneBlock: Bool) -> Void) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let title = NSLocalizedString("Okay", comment: "")
        
        let action = UIAlertAction(title: title, style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
                handler(true)
            })
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Popping View Controller
    
    fileprivate func popToViewController(_ controller: AnyClass) {
        for vc in (self.navigationController?.viewControllers)! {
            if vc.isKind(of: controller)  {
                _ = self.navigationController?.popToViewController(vc, animated: true)
            }
        }
    }
    
    // MARK: - Delete Temporary Data
    
    fileprivate func clearImmediateData() {
        self.lpmDataManager.removeObjectForKey("LPM_USER_DEFAULT_TEMPLATE_ID")
        self.lpmDataManager.removeObjectForKey("LPM_USER_DEFAULT_DLL_TEMPLATE_DAY_ID")
        self.lpmDataManager.removeObjectForKey("LPM_USER_DEFAULT_LESSON_PLAN_ID")
        self.lpmDataManager.removeObjectForKey("LPM_USER_DEFAULT_NAVIGATION_TITLE")
        self.lpmDataManager.removeObjectForKey("LPM_USER_DEFAULT_IS_VIEW_MODE")
        _ = self.cpmDataManager.clearDataOfLearningCompetency()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == self.kViewSwapperSegueIdentifier {
            self.viewSwapper = segue.destination as? LPMLessonCreatorViewSwapper
        }
        
        if segue.identifier == self.kCompetencLVSegueIdentifier {
            self.competencyListView = segue.destination as? LPMLessonCompetencyListViewController
            self.competencyListView.isUBD = false
        }
    }

}
