//
//  LessonTemplateStageView.h
//  V-Smart
//
//  Created by Julius Abarra on 08/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonTemplateStageView : UIViewController

@property (strong, nonatomic) NSString *selectedContentStage;

@end
