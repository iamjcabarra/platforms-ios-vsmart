//
//  GBTConsolidatedGrades.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 26/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTConsolidatedGrades: UIViewController, GBMDropDownDelegate, UIPopoverPresentationControllerDelegate {

    var studentPanel:GBTCStudentListPanel!
    var learningComponentPanel:GBTCLearningComponents!
    var reportsComponent:GBTReportsComponent!
    var sectionMenuOption:GBMDropDown!
    var courseMenuOption:GBMDropDown!
    
    var scrollingView : UIScrollView?
    
    @IBOutlet weak var sectionNameLbl: UILabel!
    @IBOutlet weak var termButton: UIButton!
//    @IBOutlet weak var sectionButton: UIButton!
    @IBOutlet weak var consolidatedButton: UIButton!
    @IBOutlet weak var termField: UITextField!
    
//    @IBOutlet weak var sectionField: UITextField!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var is_quarter = false
    var header_name = ""
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.termField.isHidden = false
        self.termButton.isHidden = false
        
        self.consolidatedButton.addTarget(self, action: #selector(self.consolidatedButtonAction(_:)), for: .touchUpInside)
        
        self.setupOptionMenus()
        self.segmentedControl.addTarget(self, action: #selector(self.sortSegmentedControlAction(_:)), for:.valueChanged)
        
        setupTextFields()
        self.learningComponentPanel.reloadCollection(isQuarter: self.is_quarter, withHeaderName: self.header_name)
    }
    
    func setupTextFields() {
        
        ////////// section
        let mysection_id = gbdm.fetchUserDefaultsObject(forKey: GBTConstants.UserDefined.GBTSECTIONID) as! String
        let p1 = NSComparisonPredicate(keyPath: "section_id", withValue: mysection_id, isExact: true)
        let section_list = self.gbdm.db.retrieveEntity(GBTConstants.Entity.SECTION, properties: ["section","section_id"], filter: p1, sortKey: "section") as [AnyObject]?
        if section_list != nil {
            let value = section_list!.first as! [String:AnyObject]
            var section_name = self.gbdm.stringValue(value["section"])
            section_name = section_name?.replacingOccurrences(of: "*", with: "")
            self.sectionNameLbl.text = section_name
        }
        
        ////////// term
        let term_list = self.gbdm.db.retrieveEntity(GBTConstants.Entity.TERM, properties: ["name","id"], filter: nil, sortKey: "name") as [AnyObject]?
        if term_list != nil && (term_list?.count)! > 0 {
            let value = term_list!.first as! [String:AnyObject]
            let term_name = self.gbdm.stringValue(value["name"])
            self.termField.text = term_name
            self.header_name = term_name!
        }
    }
    
    func sortSegmentedControlAction(_ segment: UISegmentedControl) {
        
        if segment.selectedSegmentIndex == 0 {
            self.is_quarter = false
        } else {
            self.is_quarter = true
        }

//        if segment.selectedSegmentIndex == 0 {
//            self.performSegue(withIdentifier: "GBTC_REPORTS_COMPONENT", sender: nil);
//        }
//        
//        if segment.selectedSegmentIndex == 1 {
//            self.is_quarter = true
//        }
//        
//        if segment.selectedSegmentIndex == 2 {
//            self.is_quarter = false
//        }
        
        self.termField.isHidden = self.is_quarter
        self.termButton.isHidden = self.is_quarter
        
        self.studentPanel.reloadList(isQuarter: self.is_quarter)
        self.learningComponentPanel.reloadCollection(isQuarter: self.is_quarter, withHeaderName: self.header_name)
        
//        self.studentPanel.reloadFetchedResultsController()
//        self.learningComponentPanel.reloadFetchedResultsController()
    }
    
    func consolidatedButtonAction(_ button: UIButton) {
        DispatchQueue.main.async {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupOptionMenus() {
        
//        self.sectionMenuOption = optionMenu(.section, delegate:self)
//        self.setupButton(self.sectionButton)
        
        self.courseMenuOption = optionMenu(.term, delegate:self)
        self.setupButton(self.termButton)
    }
    
    func optionMenu(_ type:GBMDropDownMenuType, delegate:UIViewController? ) -> GBMDropDown {
        
        let menu = GBMDropDown(menu: type, delegate: self)
        menu.modalPresentationStyle = .popover
        
        return menu
    }
    
    func setupButton(_ button:UIButton) {
        let action = #selector(GBTMainViewController.optionAction(_:))
        let event = UIControlEvents.touchUpInside
        button.addTarget(self, action:action, for: event)
    }
    
    func optionAction(_ button:UIButton) {
        let menu = courseMenuOption// ( button == self.termButton ) ? sectionMenuOption : courseMenuOption
        
        // Popover setup
        let popController = (menu?.popoverPresentationController!)! as UIPopoverPresentationController
        popController.permittedArrowDirections = .unknown
        popController.delegate = self
        
        // Popover from an arbitrary anchor point
        popController.sourceView = button
        popController.sourceRect = CGRect(x: 0, y: 0, width: button.frame.size.width, height: button.frame.size.height)
        
        // present menu
        self.present(menu!, animated: true, completion: nil)
    }
    
    func didFinishSelecting(_ type:GBMDropDownMenuType, data: [String : AnyObject]) {
        
        let dataName = data["name"] as! String
        let dataID = data["id"] as! String
        print("\(#function) -- XXX -- \(dataName) = \(dataID)")
        
//        if type.description() == GBTConstants.Entity.SECTION {
//            self.sectionField.text = dataName
//            
//            gbdm.requestSummary(forSectionID: dataID) { done in
//                if done {
//                    print("done requesting summary...")
//                }
//            }
//        }
        
        if type.description() == GBTConstants.Entity.TERM {
            self.termField.text = dataName
            self.header_name = dataName
            self.learningComponentPanel.reloadCollection(withTermID: dataID, withHeaderName: dataName)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        if segue.identifier == "GBTC_STUDENT_LIST_PANEL_EMBED" {
            self.studentPanel = segue.destination as! GBTCStudentListPanel
            self.studentPanel.scrollDelegate = self
        }
        
        if segue.identifier == "GBTC_LEARNING_COMPONENT_PANEL_EMBED" {
            self.learningComponentPanel = segue.destination as! GBTCLearningComponents
            self.learningComponentPanel.scrollDelegate = self
        }
        
//        if segue.identifier == "GBTC_REPORTS_COMPONENT" {
//            self.reportsComponent = segue.destination as! GBTReportsComponent
//        }
        
    }
}



extension GBTConsolidatedGrades : ScrollCommunicationDelegate {
    
    func gbScrollViewDidScroll(_ panel: GBPanelType, scrollTo: CGFloat) {
        if self.scrollingView == self.studentPanel.tableView {
            let offSetX = self.learningComponentPanel.collectionView.contentOffset.x
            self.learningComponentPanel.collectionView.setContentOffset(CGPoint(x: offSetX, y: scrollTo), animated: false)
        }
        
        if self.scrollingView == self.learningComponentPanel?.collectionView {
            self.studentPanel.tableView.setContentOffset(CGPoint(x: 0, y: scrollTo), animated: false)
        }
    }
    
    func gbScrollViewWillBeginDragging(_ panel: GBPanelType) {
        
        if self.scrollingView == nil {
            switch panel {
            case .StudentPanel :
                self.scrollingView = self.studentPanel.tableView
                self.learningComponentPanel.collectionView.isUserInteractionEnabled = false
            case .LearningPanel :
                self.learningComponentPanel.collectionView.isUserInteractionEnabled = false
                self.studentPanel.tableView.isUserInteractionEnabled = false
            default :
                print("NO SCOLL RIGHT PANEL")
            }
        }
    }
    
    func gbScrollViewDidEndDecelerating(_ panel: GBPanelType) {
        self.scrollingView = nil
        self.learningComponentPanel.collectionView.isUserInteractionEnabled = true
        self.studentPanel.tableView.isUserInteractionEnabled = true
    }
}
