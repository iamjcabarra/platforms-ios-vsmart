//
//  CurriculumPeriodViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/30/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "CurriculumPeriodViewController.h"

@interface CurriculumPeriodViewController ()

@end

@implementation CurriculumPeriodViewController

@synthesize curriculumPeriodPicker = _curriculumPeriodPicker;
@synthesize delegate;

NSArray *cpList;
NSMutableArray *cpIDList;
NSMutableArray *cpNameList;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _curriculumPeriodPicker.delegate = self;
    _curriculumPeriodPicker.dataSource = self;
    
    cpList = [NSArray array];
    cpIDList = [NSMutableArray array];
    cpNameList = [NSMutableArray array];
    
    cpList = self.curriculumPeriodList;
    NSLog(@"cpList: %@", cpList);
    
    if (cpList.count > 0) {
        for (NSDictionary *d in cpList) {
            NSString *cpid = d[@"period_id"];
            NSString *name = d[@"name"];
            
            [cpIDList addObject:cpid];
            [cpNameList addObject:name];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.curriculumPeriodList count];
}

#pragma mark - UIPickerView Delegate

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30.0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [cpNameList objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSString *cpid = [cpIDList objectAtIndex:row];
    NSString *name = [cpNameList objectAtIndex:row];
    NSDictionary *d = @{@"period_id":cpid, @"name":name};

    [self.delegate periodSelected:d];
    
    NSLog(@"Chosen curriculum period: %@", d);
}

@end
