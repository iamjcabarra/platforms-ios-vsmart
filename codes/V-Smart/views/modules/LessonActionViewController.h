//
//  LessonActionViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonActionViewController : UIViewController

@property (strong, nonatomic) NSString *courseid;
@property (strong, nonatomic) NSString *gradeLevel;
@property (strong, nonatomic) NSString *templateid;
@property (strong, nonatomic) NSString *actionType;
@property (strong, nonatomic) NSManagedObject *lesson_mo;

@end
