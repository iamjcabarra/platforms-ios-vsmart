//
//  CoursePlayerShowResultsCell.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 04/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CoursePlayerShowResultsDelegate <NSObject>
@optional
- (void)transitionWithData:(NSDictionary *)previewData index:(NSInteger)index segueID:(NSString *)segueIdentifier;
@optional
- (void)refreshIndexPath:(NSIndexPath *)indexPath;
@end


@interface CoursePlayerShowResultsCell : UITableViewCell
- (void)displayObject:(NSManagedObject *)object;

@property (nonatomic, weak) id <CoursePlayerShowResultsDelegate> delegate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *questionAndImageTableHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *choiceTableHeight;

@property (strong, nonatomic) NSString *questionID;

@property (strong, nonatomic) NSString *show_correct_answer;
@property (strong, nonatomic) NSString *show_feedbacks;
@property (strong, nonatomic) NSString *show_score;


@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet UITableView *choicesTable;

@property (strong, nonatomic) NSIndexPath *currentIndex;

- (void)reloadFetchedResultsController;

@property (weak, nonatomic) IBOutlet UILabel *numberIndicator;

@end
