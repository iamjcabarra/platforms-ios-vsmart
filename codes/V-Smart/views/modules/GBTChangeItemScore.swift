//
//  GBTChangeItemScore.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 06/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


//protocol GBTChangeItemScoreDelegate {
//    func didEndChangeScore(didChangeScore: Bool)
//}

class GBTChangeItemScore: UIViewController, UITextFieldDelegate {
    
    var changeScoreData: [String:AnyObject]!
    var term_id = ""
    var component_id = ""
    var index: Double!
    var highestScore: String!
//    var delegate: GBTChangeItemScoreDelegate?
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var scoreTextField: UITextField!
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    fileprivate let notification = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.saveButton.addTarget(self, action: #selector(self.saveButtonAction(_:)), for: .touchUpInside)
        self.cancelButton.addTarget(self, action: #selector(self.cancelButtonAction(_:)), for: .touchUpInside)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let score = self.gbdm.formatStringNumber(self.changeScoreData["score"] as! String)
        self.scoreTextField.text = "\(score)"
        
        self.gbdm.fetchHighestScore(forComponentID: component_id, inTermID: term_id, withIndex: index) { (listBlock) in
            DispatchQueue.main.async(execute: {
                if listBlock?.count > 0 {
                    var highest_score = self.gbdm.stringValue(listBlock?.last!["actual_result"])
                    highest_score = self.gbdm.formatStringNumber(highest_score!)
                    self.highestScore = highest_score
                }
            })
        }
        
        
    }
    
    func cancelButtonAction(_ b: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func saveButtonAction(_ b: UIButton) {
        
        let oldScore = self.changeScoreData["score"] as! String
        let newScore = self.scoreTextField.text
        
        if newScore?.characters.count > 0 && Double(newScore!) != nil {
            if newScore == oldScore {
                self.dismiss(animated: true, completion: nil)
                return
            } else {
                self.changeScoreData["score"] = newScore as AnyObject?
                let data = [self.changeScoreData]
                self.gbdm.changeGradebookScore(withData: data, handler: { (done, error) in
                    if error == nil {
                        let dict: [String:String] = ["term_id": self.term_id,
                                                     "component_id": self.component_id]
                        self.notification.post(name: Notification.Name(rawValue: "GBT_RELOAD_GRADEBOOK_CONTENT"), object: dict)
//                        self.delegate?.didEndChangeScore(true)
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        self.displayAlert(withTitle: "Error", withMessage: error!)
                    }
                })
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let countdots = textField.text!.components(separatedBy: ".").count - 1
        
        if countdots > 0 && string == "." {
            return false
        }
        
        // Create an `NSCharacterSet` set which includes everything *but* the digits
        let inverseSet = CharacterSet(charactersIn:"0123456789.").inverted
        
        // At every character in this "inverseSet" contained in the string,
        // split the string up into components which exclude the characters
        // in this inverse set
        let components = string.components(separatedBy: inverseSet)
        
        // Rejoin these components
        let filtered = components.joined(separator: "")  // use join("", components) if you are using Swift 1.2
        
        var is_valid = string == filtered
//        if Double(string)
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        
        if String(txtAfterUpdate).characters.count > 0 && is_valid {
            if Double(txtAfterUpdate as String)! > Double(self.highestScore) {
                is_valid = false
                textField.text = self.highestScore
            }
        }
        
        // If the original string is equal to the filtered string, i.e. if no
        // inverse characters were present to be eliminated, the input is valid
        // and the statement returns true; else it returns false
        return is_valid
    }
    
    func displayAlert(withTitle title: String, withMessage message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.preferredContentSize = CGSize(width: 250, height: 125);
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
