
//
//  ShowLikesCell.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 05/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class SSShowLikesCell: UITableViewCell {

    @IBOutlet weak var likerImage: UIImageView!
    @IBOutlet weak var likerName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        likerName.text = ""
        likerImage.image = nil
    }

}
