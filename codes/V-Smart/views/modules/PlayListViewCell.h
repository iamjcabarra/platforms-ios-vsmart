//
//  PlayListViewCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/10/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface PlayListViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageViewFile;
@property (strong, nonatomic) IBOutlet UILabel *labelFilename;
@property (strong, nonatomic) IBOutlet UILabel *labelFileDescription;
@property (strong, nonatomic) IBOutlet UILabel *labelDate;
@property (strong, nonatomic) IBOutlet UILabel *labelFileSize;
//@property (strong, nonatomic) IBOutlet UILabel *sharedIndicator;
@property (strong, nonatomic) IBOutlet UIButton *buttonOption;
@property (strong, nonatomic) IBOutlet UIProgressView *progressIndicator;
@property (weak, nonatomic) IBOutlet UILabel *sharedToIndicator;
@property (weak, nonatomic) IBOutlet UIButton *sharedButton;

@end
