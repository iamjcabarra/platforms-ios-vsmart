//
//  TextBookCollectionCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 04/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TextBookCollectionCell.h"
#import "Book.h"
#import "BookProgress.h"
#import "PICircularProgressView.h"
#import "UIImageView+WebCache.h"
#import "Utils.h"

@interface TextBookCollectionCell ()

@property (strong, nonatomic) IBOutlet BookProgress *bookProgress;
@property (strong, nonatomic) IBOutlet PICircularProgressView *progressView;

@property (strong, nonatomic) IBOutlet UIView *overlayFrame;
@property (strong, nonatomic) IBOutlet UIImageView *cloudImageView;
@property (strong, nonatomic) IBOutlet UIImageView *unreadImageView;
@property (strong, nonatomic) IBOutlet UIButton *averageButtonView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *authorLabel;
@property (strong, nonatomic) IBOutlet UILabel *fileSizeLabel;
@property (strong, nonatomic) IBOutlet UILabel *bookProgressLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLastReadLabel;

@end

@implementation TextBookCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.titleLabel.text = @"";
    self.authorLabel.text = @"";
    self.fileSizeLabel.text = @"";
}

- (id)initWithFrame:(CGRect)aRect {
    
    if ((self = [super initWithFrame:aRect])) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder*)coder {
    
    if ((self = [super initWithCoder:coder])) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization {
    
    self.backgroundColor = [UIColor whiteColor];
    
    // CHECKED CODE
    self.contentView.backgroundColor = self.backgroundColor;
    self.imageView.backgroundColor = self.backgroundColor;
    self.titleLabel.backgroundColor = self.backgroundColor;
    
    // CHECKED CODE
    UIColor *customColor = [[UIColor darkGrayColor] colorWithAlphaComponent:0.5f];
    [_progressView setInnerBackgroundColor:customColor];
    
    // CHECKED NIB
    self.bookProgressLabel.text = NSLocalizedString(@"TOTAL QUIZ COMPLETED", nil);
    self.bookProgressLabel.tag = 10;
    
    // CHECKED NIB
    [_bookProgress setProgressColor:UIColorFromHex(0x00a0dc)]; //CODE

    // CHECKED
    SEL action = @selector(averageAction:);
    [_averageButtonView addTarget:self action:action forControlEvents:UIControlEventTouchDown];
    
    [_unreadImageView hide];
}

- (void)averageAction:(id)sender {
    VS_NCPOST_OBJ(kNotificationTextbookShelfSelection, _book);
}

- (void)setDownloadProgress:(CGFloat)value {
    _progressView.progress = value;
    [self setNeedsLayout];
}

- (void)setAverageText:(NSString *)text {
    [_averageButtonView setTitle:text forState:UIControlStateNormal];
    [self setNeedsLayout];
}

- (void)setAverageVisibility:(BOOL)visible {
    [_averageButtonView setHidden:!visible];
    [self setNeedsLayout];
}

- (UIImage *)image {
    return _imageView.image;
}

- (void)setCloudVisibility:(BOOL)isVisible {
    
    if (isVisible) {
        [_cloudImageView fadeIn];
        [_unreadImageView hide];
    }
}

- (void)setCompleted:(BOOL)completed {
    
    if (completed) {
        [_progressView fadeOut];
        [_overlayFrame fadeOut];
        [_cloudImageView fadeOut];
    }
}

- (void)setBookProgressVisibility:(BOOL)show {
    
//    Refactor
//    JCA-11232017
//    UI Blocking
    
//    UILabel *label = (UILabel *)[self viewWithTag:10];
//    if (!show) {
//        [_bookProgress fadeOut];
//        [label hide];
//    } else {
//        [_bookProgress fadeIn];
//        [label show];
//    }
//
////    [self setNeedsLayout];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UILabel *label = (UILabel *)[self viewWithTag:10];
        if (!show) {
            [_bookProgress fadeOut];
            [label hide];
        } else {
            [_bookProgress fadeIn];
            [label show];
        }
    });
}

- (void)setProgressVisibility:(BOOL)show {
    
    [_unreadImageView hide];
    [_progressView fadeOut];
    [_overlayFrame fadeOut];
    
    if (show) {
        if(_progressView.alpha == 0) {
            [_progressView fadeIn];
            [_overlayFrame fadeIn];
        }
    }
}

- (void)setBookProgress:(CGFloat)progress withText:(NSString *)progressText {
    [_bookProgress setProgress:progress];
    [_bookProgress setText:[progressText uppercaseString]];
}

- (void)setCover:(NSString *) coverUrl {
    [_imageView sd_setImageWithURL:[NSURL URLWithString:coverUrl]];
}

- (NSString *)dateLastRead {
    return _dateLastReadLabel.text;
}

- (void)setDateLastReadVisibility:(BOOL)visible {
    [_dateLastReadLabel setHidden:!visible];
}

- (void)setDateLastRead:(NSString *)dateLastRead {
    _dateLastReadLabel.text = dateLastRead;
}

- (NSString *)title {
    return _titleLabel.text;
}

- (void)setTitle:(NSString *)title {
    _titleLabel.text = title;
}

- (NSString *)author {
    return _authorLabel.text;
}

- (void)setAuthor:(NSString *)author {
    _authorLabel.text = author;
}

- (void)setReadStatus:(BOOL)isRead {
    
    if (isRead) {
        [_unreadImageView hide];
    } else {
        [_unreadImageView fadeIn];
    }
}

- (void)setBook:(Book *)book {
    if (book != nil) {
        NSString *fileSize = [Utils stringValue:book.fileSizeString];
        NSString *mediaType = [Utils stringValue:book.mediaType];
        self.fileSizeLabel.text = [NSString stringWithFormat:@"%@ %@", [mediaType uppercaseString], fileSize ];
    }
}

@end
