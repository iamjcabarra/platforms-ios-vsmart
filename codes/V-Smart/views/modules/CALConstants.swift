//
//  CALConstants.swift
//  V-Smart
//
//  Created by Julius Abarra on 09/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

struct CALConstants {
    struct Entity {
        static let EVENT: String = "Event"
        static let EVENT_COPY: String = "EventCopy"
        static let SECTION: String = "Section"
    }
    
    struct DateFormat {
        static let SERVER: String = "yyyy-MM-dd HH:mm:ss"
        static let EVENT_CREATOR: String = "EEEE, MMMM dd, yyyy"
    }
}
