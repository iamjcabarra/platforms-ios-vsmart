//
//  NotesChildViewController.m
//  V-Smart
//
//  Created by VhaL on 2/6/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NotesChildViewController.h"
#import "NotesViewController.h"
#import "NoteEntry.h"
#import "NoteDataManager.h"
#import "NoteBookModalView.h"

@interface NotesChildViewController () <NoteBookModalViewDelegate> {
    NSMutableArray *notesArray;
    NSMutableArray *workingNotesArray;
}
@property (nonatomic, strong) UIRefreshControl *refreshControlCollectionView;
@property (nonatomic, strong) UIRefreshControl *refreshControlTableView;
@property (nonatomic, weak) IBOutlet UIButton *buttonViewNotebooks;


//BUTTON VIEW NOTE
@property (nonatomic, strong) NSString *archivesTitle;
@property (nonatomic, strong) NSString *notebooksTitle;
@property (nonatomic, strong) NoteBookModalView *noteBookModalView;

// Bug Fix
// jca-05052016
// Fix displaying of deleted notes
@property (nonatomic, assign) BOOL isArchiveView;

@end

@implementation NotesChildViewController
@synthesize tableViewNotes, collectionViewNotes, buttonAddNew, buttonCollectionView, buttonTableView, noteContentViewController, colorPicker, folderPicker, filterPicker,noteSidePanelViewController, noteBookCollectionViewController, buttonViewNotebooks;

@synthesize delegate;

-(void)viewDidLoad{
    // Modify ButtonFolder UI
//    [buttonFolder setImageEdgeInsets:UIEdgeInsetsMake(-5.00, 90.00, 0.00, 0.00)];
//    [buttonFolder setTitleEdgeInsets:UIEdgeInsetsMake(0.00, -30.00, 0.00, 0.00)];
    
    CGRect tableAndCollectionRect = CGRectMake(20, 87, 728, 917);
    self.tableViewNotes.frame = tableAndCollectionRect;
    self.collectionViewNotes.frame = tableAndCollectionRect;
    
    noteBookCollectionViewController = [[NoteBookCollectionViewController alloc] init];
    noteBookCollectionViewController.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 41, self.view.frame.size.width, self.view.frame.size.height - 41);
    [self addChildViewController:noteBookCollectionViewController];
    [self.view addSubview:noteBookCollectionViewController.view];
    
    [self buttonCollectionViewTapped:buttonCollectionView];
    //[self buttonTableViewTapped:buttonTableView];
    
    /* localizable strings */
    self.archivesTitle = NSLocalizedString(@"Archives", nil); //checked
    self.notebooksTitle = NSLocalizedString(@"Notebooks", nil); //checked
    
    [buttonViewNotebooks setTitle:self.archivesTitle forState:UIControlStateNormal];
//    [buttonViewNotebooks setTitle:@"Archives" forState:UIControlStateNormal];

    // Bug Fix
    // jca-05052016
    // Fix displaying of deleted notes
    self.isArchiveView = NO;

    //initialize font sizes and font types
    [[NoteDataManager sharedInstance] getFontSizes:NO useMainThread:YES];
    [[NoteDataManager sharedInstance] getFontTypes:NO useMainThread:YES];
    
    [self setupSidePanel];
    [self setupNotesData];
    [self setupTableView];
    [self setupCollectionView];
    [self addRefreshMode];

    [[NoteDataManager sharedInstance] sync];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    VLog(@"closing notes: perform sync operation");
    [[NoteDataManager sharedInstance] sync];
}

-(void)addRefreshMode{
    
    NSString *pullRefresh = NSLocalizedString(@"Pull to Refresh",nil);
    
    self.refreshControlCollectionView = [[UIRefreshControl alloc] init];
    self.refreshControlCollectionView.attributedTitle = [[NSAttributedString alloc] initWithString:pullRefresh];
    [self.refreshControlCollectionView addTarget:self action:@selector(setupNotesData) forControlEvents:UIControlEventValueChanged];
    
    [self.collectionViewNotes addSubview:self.refreshControlCollectionView];
    self.collectionViewNotes.alwaysBounceVertical = YES;
    
    
    self.refreshControlTableView = [[UIRefreshControl alloc] init];
    self.refreshControlTableView.attributedTitle = [[NSAttributedString alloc] initWithString:pullRefresh];
    [self.refreshControlTableView addTarget:self action:@selector(setupNotesData) forControlEvents:UIControlEventValueChanged];
    
    [self.tableViewNotes addSubview:self.refreshControlTableView];
}

-(void)setupSidePanel{
    noteSidePanelViewController = [NoteSidePanelViewController new];
    noteSidePanelViewController.delegate = self;
    noteSidePanelViewController.view.hidden = YES;
    [self addChildViewController:noteSidePanelViewController];
    [self.view addSubview:noteSidePanelViewController.view];
    [noteSidePanelViewController didMoveToParentViewController:self];
    noteSidePanelViewController.view.frame = CGRectMake(0 - noteSidePanelViewController.view.frame.size.width, 42, self.view.frame.size.width, self.view.frame.size.height-42);
    
    [noteSidePanelViewController adjustFrame];
    
    folderPicker = noteSidePanelViewController.noteBookViewController;
    filterPicker = noteSidePanelViewController.tagViewController;
    colorPicker = noteSidePanelViewController.colorPopOverViewController;
    
    folderPicker.delegate = self;
    filterPicker.delegate = self;
    colorPicker.delegate = self;
    
    //add UI relationship for folder-related items
    folderPicker.otherNoteBookViewController = noteBookCollectionViewController;
    noteBookCollectionViewController.otherNoteBookViewController = folderPicker;
    [folderPicker castOtherController];
    [noteBookCollectionViewController castOtherController];
}

-(void)setupNotesData{
    notesArray = [NSMutableArray arrayWithArray:[[NoteDataManager sharedInstance] getNotes:NO useMainThread:YES]];
    
    //[self filterWorkingNotesArray];
    //[filterPicker loadData];

    // Bug Fix
    // jca-05052016
    // Fix displaying of deleted notes
    if (self.isArchiveView) {
        [self filterWorkingNotesArrayByArchives];
    }
    else {
    [self filterWorkingNotesArray];
    [filterPicker loadData];
    }
    
    [self.refreshControlCollectionView endRefreshing];
    [self.refreshControlTableView endRefreshing];
}

-(void)setupCollectionView{
    self.collectionViewNotes.delegate = self;
    self.collectionViewNotes.dataSource = self;
    self.collectionViewNotes.backgroundColor = [UIColor clearColor];
    
    UINib *cellNib = [UINib nibWithNibName:@"NotesCollectionViewCell" bundle:nil];
    [collectionViewNotes registerNib:cellNib forCellWithReuseIdentifier:@"NCCellAdd"];
    [collectionViewNotes registerNib:cellNib forCellWithReuseIdentifier:@"NCCellGeneral"];
    [collectionViewNotes registerNib:cellNib forCellWithReuseIdentifier:@"NCCellDefault"];
}

-(void)setupTableView{
    
    self.tableViewNotes.delegate = self;
    self.tableViewNotes.dataSource = self;
    self.tableViewNotes.rowHeight = 90;
    
    UINib *cellNib = [UINib nibWithNibName:@"UMTableViewCellDefault" bundle:nil];
    [tableViewNotes registerNib:cellNib forCellReuseIdentifier:@"UMCellDefault"];

    UINib *cellNib2 = [UINib nibWithNibName:@"UMTableViewCellArchived" bundle:nil];
    [tableViewNotes registerNib:cellNib2 forCellReuseIdentifier:@"UMCellArchived"];
    
    // If you set the seperator inset on iOS 6 you get a NSInvalidArgumentException...weird
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        self.tableViewNotes.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0); // Makes the horizontal row seperator stretch the entire length of the table view
    }
}

-(void)filterWorkingNotesArray{
    workingNotesArray = [NSMutableArray arrayWithArray:notesArray];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isArchive == %d", [[NSNumber numberWithBool:NO] intValue]];
    workingNotesArray = [NSMutableArray arrayWithArray:[workingNotesArray filteredArrayUsingPredicate:pred]];
    
    NoteBook *selectedFolder = folderPicker.selectedFolder;
    if (selectedFolder != nil){
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"folder ==[c] %@", selectedFolder];
        workingNotesArray = [NSMutableArray arrayWithArray:[workingNotesArray filteredArrayUsingPredicate:pred]];
    }

    Tag *selectedTag = filterPicker.selectedTag;
    if (selectedTag != nil){
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SUBQUERY (noteTags, $e, $e.tag == %@ && $e.isRemoved == %@).@count > 0", selectedTag, [NSNumber numberWithBool:NO]];
        workingNotesArray = [NSMutableArray arrayWithArray:[workingNotesArray filteredArrayUsingPredicate:pred]];
    }
    
    Color *selectedColor = colorPicker.selectedColor;
    if (selectedColor != nil){
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"colorId == %d", [selectedColor.colorId intValue]];
        workingNotesArray = [NSMutableArray arrayWithArray:[workingNotesArray filteredArrayUsingPredicate:pred]];
    }
}

-(void)filterWorkingNotesArrayByArchives{
    workingNotesArray = [NSMutableArray arrayWithArray:notesArray];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isArchive == %d", [[NSNumber numberWithBool:YES] intValue]];
    workingNotesArray = [NSMutableArray arrayWithArray:[workingNotesArray filteredArrayUsingPredicate:pred]];
    
    Tag *selectedTag = filterPicker.selectedTag;
    if (selectedTag != nil){
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SUBQUERY (noteTags, $e, $e.tag == %@ && $e.isRemoved == %@).@count > 0", selectedTag, [NSNumber numberWithBool:NO]];
        workingNotesArray = [NSMutableArray arrayWithArray:[workingNotesArray filteredArrayUsingPredicate:pred]];
    }
    
    Color *selectedColor = colorPicker.selectedColor;
    if (selectedColor != nil){
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"colorId == %d", [selectedColor.colorId intValue]];
        workingNotesArray = [NSMutableArray arrayWithArray:[workingNotesArray filteredArrayUsingPredicate:pred]];
    }
}

-(IBAction)sidePanelTapped:(id)sender{
    
    CGFloat xAxis = 0;
    
    if (noteSidePanelViewController.view.frame.origin.x == 0){
        //hide it if it is already visible
        xAxis = 0 - noteSidePanelViewController.view.frame.size.width;
    }
    else{
        noteSidePanelViewController.view.hidden = NO;
    }
    
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         noteSidePanelViewController.view.frame = CGRectMake(xAxis, noteSidePanelViewController.view.frame.origin.y, noteSidePanelViewController.view.frame.size.width,noteSidePanelViewController.view.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                         if (xAxis == 0)
                             noteSidePanelViewController.view.hidden = NO;
                         else
                             noteSidePanelViewController.view.hidden = YES;
                     }];
    
}

-(IBAction)buttonAddNewTapped:(id)sender{
    //noteContentViewController = [[NoteContentViewController alloc] init];
    //noteContentViewController.delegate = self;
    //noteContentViewController.currentNotebook = folderPicker.selectedFolder;
    //
    //[self presentPopupViewController:noteContentViewController animationType:PopupViewAnimationSlideBottomBottom];
    
    // Bug Fix
    // jca-05052016
    // Disable add note operation if in archive view
    if (!self.isArchiveView) {
    noteContentViewController = [[NoteContentViewController alloc] init];
    noteContentViewController.delegate = self;
    noteContentViewController.currentNotebook = folderPicker.selectedFolder;
    [self presentPopupViewController:noteContentViewController animationType:PopupViewAnimationSlideBottomBottom];
}
}

-(IBAction)buttonTableViewTapped:(id)sender{
    
    self.collectionViewNotes.hidden = YES;
    self.tableViewNotes.hidden = NO;
    buttonTableView.selected = YES;
    buttonCollectionView.selected = NO;
    
}

-(IBAction)buttonCollectionViewTapped:(id)sender{
    self.collectionViewNotes.hidden = NO;
    self.tableViewNotes.hidden = YES;
    buttonTableView.selected = NO;
    buttonCollectionView.selected = YES;
}

-(void)buttonSaveTapped:(NoteEntry*)entry{
    [self setupNotesData];
    [self.tableViewNotes reloadData];
    [self.collectionViewNotes reloadData];
    
    [noteBookCollectionViewController loadData];
}

-(IBAction)buttonViewNotebooksTapped:(id)sender{
    
    if ([buttonViewNotebooks.titleLabel.text isEqualToString:self.notebooksTitle]){
//    if ([buttonViewNotebooks.titleLabel.text isEqualToString:@"Notebooks"]){
        folderPicker.selectedFolder = nil;
        [self buttonFolderApplyTapped:nil];

        [buttonViewNotebooks setTitle:self.archivesTitle forState:UIControlStateNormal];
//        [buttonViewNotebooks setTitle:@"Archives" forState:UIControlStateNormal];
        
        // Bug Fix
        // jca-05052016
        // Fix displaying of deleted notes
        self.isArchiveView = NO;
        buttonAddNew.hidden = NO;
    }
    else{
        noteBookCollectionViewController.view.hidden = YES;
        noteSidePanelViewController.buttonTag.enabled = YES;
        noteSidePanelViewController.buttonColor.enabled = YES;
        
        [self filterWorkingNotesArrayByArchives];
        [self.tableViewNotes reloadData];
        [self.collectionViewNotes reloadData];
        
        [buttonViewNotebooks setTitle:self.notebooksTitle forState:UIControlStateNormal];
//        [buttonViewNotebooks setTitle:@"Notebooks" forState:UIControlStateNormal];
        
        // Bug Fix
        // jca-05052016
        // Fix displaying of deleted notes
        self.isArchiveView = YES;
        buttonAddNew.hidden = YES;
    }
}

-(IBAction)buttonFolderApplyTapped:(NoteBook*)sender{
    
//    if ([close boolValue] && noteSidePanelViewController.view.frame.origin.x == 0)
//        [self sidePanelTapped:nil];
    
    if (folderPicker.selectedFolder == nil){
        noteBookCollectionViewController.view.hidden = NO;
        noteSidePanelViewController.buttonTag.enabled = NO;
        noteSidePanelViewController.buttonColor.enabled = NO;
        
        [buttonViewNotebooks setTitle:self.archivesTitle forState:UIControlStateNormal];
//        [buttonViewNotebooks setTitle:@"Archives" forState:UIControlStateNormal];
        
        // Bug Fix
        // Updating of Note Books Collection
        [noteBookCollectionViewController loadData];
    }
    else{
        noteBookCollectionViewController.view.hidden = YES;
        noteSidePanelViewController.buttonTag.enabled = YES;
        noteSidePanelViewController.buttonColor.enabled = YES;
        
        [buttonViewNotebooks setTitle:self.notebooksTitle forState:UIControlStateNormal];
//        [buttonViewNotebooks setTitle:@"Notebooks" forState:UIControlStateNormal];
    }
    
    [self filterWorkingNotesArray];
    [self.tableViewNotes reloadData];
    [self.collectionViewNotes reloadData];
}

-(IBAction)cellFolderApplyTapped:(NoteBook*)sender{
    
    noteSidePanelViewController.buttonTag.enabled = YES;
    noteSidePanelViewController.buttonColor.enabled = YES;
    
    [buttonViewNotebooks setTitle:self.notebooksTitle forState:UIControlStateNormal];
//    [buttonViewNotebooks setTitle:@"Notebooks" forState:UIControlStateNormal];
    
    folderPicker.selectedFolder = sender;
    
    // Bug Fix
    // Updating of Notes Collection
    [self setupNotesData];
    
    [self filterWorkingNotesArray];
    [self.tableViewNotes reloadData];
    [self.collectionViewNotes reloadData];
}

-(IBAction)buttonFilterApplyTapped:(id)sender{
    [self filterWorkingNotesArray];
    [self.tableViewNotes reloadData];
    [self.collectionViewNotes reloadData];
}

-(IBAction)buttonColorApplyTapped:(id)sender{
    [self filterWorkingNotesArray];
    [self.tableViewNotes reloadData];
    [self.collectionViewNotes reloadData];
}

-(IBAction)reloadNoteBookViewInstancesFromThisClass:(NSString*)sender{
    if ([sender isEqualToString:@"NoteBookCollectionViewController"]){
        [noteSidePanelViewController.noteBookViewController loadData];
    }
    
    if ([sender isEqualToString:@"NoteBookViewController"]){
        [noteBookCollectionViewController loadData];
    }
}

-(IBAction)cellButtonActionApplyTapped:(NotesCollectionViewCell*)collectionViewCell withActionTitle:(NSString*)actionTitle{
    NSIndexPath *indexPath = [self.collectionViewNotes indexPathForCell:collectionViewCell];
    
    //if ([actionTitle isEqualToString:@"Duplicate"]){
    //    noteContentViewController = [[NoteContentViewController alloc] init];
    //    noteContentViewController.delegate = self;
    //    
    //    noteContentViewController.isDuplicateOperation = YES;
    //    noteContentViewController.noteObject = (Note*)[workingNotesArray objectAtIndex:indexPath.row];
    //    
    ////        NotesViewController *vc = (NotesViewController*)delegate;
    //    [self presentPopupViewController:noteContentViewController animationType:PopupViewAnimationSlideBottomBottom];
    //}
    //
    //if ([actionTitle isEqualToString:@"Share"]){
    //    
    //}
    //
    //if ([actionTitle isEqualToString:@"Restore"]){
    //    Note *note = [workingNotesArray objectAtIndex:indexPath.row];
    //    note.isArchive = [NSNumber numberWithBool:NO];
    //    [[NoteDataManager sharedInstance] editNote:note useMainThread:YES];
    //    
    //    [self filterWorkingNotesArrayByArchives];
    //    [self.tableViewNotes reloadData];
    //    [self.collectionViewNotes reloadData];
    //    [noteBookCollectionViewController loadData];
    //}
    //
    //if ([actionTitle isEqualToString:@"Archive"]){
    //    Note *note = [workingNotesArray objectAtIndex:indexPath.row];
    //    note.isArchive = [NSNumber numberWithBool:YES];
    //    [[NoteDataManager sharedInstance] editNote:note useMainThread:YES];
    //    
    //    [self filterWorkingNotesArray];
    //    [self.tableViewNotes reloadData];
    //    [self.collectionViewNotes reloadData];
    //    [noteBookCollectionViewController loadData];
    //}
    //
    //if ([actionTitle isEqualToString:@"Delete"]){
    //    // Delete button was pressed
    //    Note *note = [workingNotesArray objectAtIndex:indexPath.row];
    //    
    //    bool deleteSuccess = [[NoteDataManager sharedInstance] deleteNote:note useMainThread:YES];
    //    
    //    if (deleteSuccess){
    //        [workingNotesArray removeObjectAtIndex:indexPath.row];
    //        [self.tableViewNotes deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    //        [self.collectionViewNotes deleteItemsAtIndexPaths:@[indexPath]];
    //    }
    //}
 
    if ([actionTitle isEqualToString:@"Edit"]) {
        noteContentViewController = [[NoteContentViewController alloc] init];
        noteContentViewController.delegate = self;
        noteContentViewController.isDuplicateOperation = NO;
        
        Note *note = (Note *)[workingNotesArray objectAtIndex:indexPath.row];
        noteContentViewController.noteObject = note;
        noteContentViewController.currentNotebook = note.folder;
        
        [self presentPopupViewController:noteContentViewController animationType:PopupViewAnimationSlideBottomBottom];
    }
    
    if ([actionTitle isEqualToString:@"Duplicate"]){
        noteContentViewController = [[NoteContentViewController alloc] init];
        noteContentViewController.delegate = self;
        noteContentViewController.isDuplicateOperation = YES;
        
        Note *note = (Note *)[workingNotesArray objectAtIndex:indexPath.row];
        noteContentViewController.noteObject = note;
        noteContentViewController.currentNotebook = note.folder;
        
        [self presentPopupViewController:noteContentViewController animationType:PopupViewAnimationSlideBottomBottom];
    }
    
    if ([actionTitle isEqualToString:@"Delete"]){
        Note *note = (Note *)[workingNotesArray objectAtIndex:indexPath.row];
        
        // Enhancement #1306
        // jca-05042016
        
        [self shouldDeleteNote:note.title completion:^(BOOL delete) {
            if (delete) {
                bool deleteSuccess = [[NoteDataManager sharedInstance] deleteNote:note useMainThread:YES];
                
                if (deleteSuccess){
                    [workingNotesArray removeObjectAtIndex:indexPath.row];
                    [self.tableViewNotes deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                    [self.collectionViewNotes deleteItemsAtIndexPaths:@[indexPath]];
                }
                else {
                    NSString *message = NSLocalizedString(@"There was an error deleting note. Please try again later.", nil);
                    [self.view makeToast:message duration:2.0f position:@"center"];
                }
            }
        }];
    }
    
    if ([actionTitle isEqualToString:@"Move"]) {
        self.noteBookModalView = [[NoteBookModalView alloc] init];
        self.noteBookModalView.delegate = self;
        
        Note *note = (Note *)[workingNotesArray objectAtIndex:indexPath.row];
        self.noteBookModalView.note = note;
        self.noteBookModalView.currentNoteBook = note.folder;
        
        [self presentPopupViewController:self.noteBookModalView animationType:PopupViewAnimationSlideBottomBottom];
    }
    
    if ([actionTitle isEqualToString:@"Archive"]){
        Note *note = (Note *)[workingNotesArray objectAtIndex:indexPath.row];
        note.isArchive = [NSNumber numberWithBool:YES];
        [[NoteDataManager sharedInstance] editNote:note useMainThread:YES];
        
        [self filterWorkingNotesArray];
        [self.tableViewNotes reloadData];
        [self.collectionViewNotes reloadData];
        [noteBookCollectionViewController loadData];
    }
    
    if ([actionTitle isEqualToString:@"Restore"]){
        Note *note = (Note *)[workingNotesArray objectAtIndex:indexPath.row];
        note.isArchive = [NSNumber numberWithBool:NO];
        [[NoteDataManager sharedInstance] editNote:note useMainThread:YES];
        
        [self filterWorkingNotesArrayByArchives];
        [self.tableViewNotes reloadData];
        [self.collectionViewNotes reloadData];
        [noteBookCollectionViewController loadData];
    }
}

// Enhancement #1306
// jca-05042016

- (void)shouldDeleteNote:(NSString *)noteTitle completion:(void (^)(BOOL delete))response {
    NSString *avTitle = NSLocalizedString(@"Delete Note", nil);
    NSString *premstr = NSLocalizedString(@"Are you sure you want to delete", nil);
    NSString *message = [NSString stringWithFormat:@"%@ \"%@\"?", premstr, noteTitle];
    NSString *butNegR = NSLocalizedString(@"No", nil);
    NSString *butPosR = NSLocalizedString(@"Yes", nil);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:avTitle
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *negAction = [UIAlertAction actionWithTitle:butNegR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                          response(NO);
                                                      }];
    
    UIAlertAction *posAction = [UIAlertAction actionWithTitle:butPosR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                          response(YES);
                                                      }];
    [alert addAction:negAction];
    [alert addAction:posAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UIScrollViewDelegate

- (NSArray *)rightButtons:(BOOL)isArchived {
    
    //NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    //
    //UIImage *copyImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_notes_duplicate.png"] scaledToSize:CGSizeMake(30, 30)];
    //[rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0] icon:copyImg];
    //
    ////    UIImage *shareImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_notes_share.png"] scaledToSize:CGSizeMake(30, 30)];
    ////    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:0] icon:shareImg];
    //
    //if (isForArchive){
    //    UIImage *archivedImg = [UIImage imageWithImage:[UIImage imageNamed:@"ios-notes-archive.png"] scaledToSize:CGSizeMake(30, 30)];
    //    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:0] icon:archivedImg];
    //}
    //else{
    //    UIImage *unArchivedImg = [UIImage imageWithImage:[UIImage imageNamed:@"ios-notes-unarchive.png"] scaledToSize:CGSizeMake(30, 30)];
    //    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:0] icon:unArchivedImg];
    //}
    //
    //UIImage *trashImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_notes_trash.png"] scaledToSize:CGSizeMake(30, 30)];
    //[rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0] icon:trashImg];
    //
    ////padding
    //[rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0] title:@""];
    //
    //return rightUtilityButtons;
    
    // Feature
    // jca-05042016
    // Implement Edit, Duplicate and Move Options in Swipeable Cell
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    if (isArchived) {
        // Padding
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0] title:@""];
        
        // Padding
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0] title:@""];
        
        // Padding
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0] title:@""];
        
        // Delete
        UIImage *trashImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_notes_trash.png"] scaledToSize:CGSizeMake(30, 30)];
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0] icon:trashImg];
        
        // Restore
        UIImage *unArchivedImg = [UIImage imageWithImage:[UIImage imageNamed:@"ios-notes-unarchive.png"] scaledToSize:CGSizeMake(30, 30)];
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:0] icon:unArchivedImg];
    }
    else {
        // Edit
        UIImage *editImg = [UIImage imageWithImage:[UIImage imageNamed:@"edit48.png"] scaledToSize:CGSizeMake(30, 30)];
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0] icon:editImg];
        
        // Duplicate
        UIImage *duplicateImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_notes_duplicate.png"] scaledToSize:CGSizeMake(30, 30)];
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0] icon:duplicateImg];
        
        // Delete
        UIImage *trashImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn_notes_trash.png"] scaledToSize:CGSizeMake(30, 30)];
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0] icon:trashImg];
        
        // Move
        UIImage *moveImg = [UIImage imageWithImage:[UIImage imageNamed:@"icn-notes-move-100px.png"] scaledToSize:CGSizeMake(30, 30)];
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0] icon:moveImg];
        
        // Archive
        UIImage *archivedImg = [UIImage imageWithImage:[UIImage imageNamed:@"ios-notes-archive.png"] scaledToSize:CGSizeMake(30, 30)];
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:0] icon:archivedImg];
    }
    
    // Padding
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0] title:@""];
    
    return rightUtilityButtons;
}

- (NSArray *)leftButtons{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:0.07 green:0.75f blue:0.16f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"check.png"]];
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:1.0f green:1.0f blue:0.35f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"clock.png"]];
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:1.0f green:0.231f blue:0.188f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"cross.png"]];
    //    [leftUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:0.55f green:0.27f blue:0.07f alpha:1.0]
    //                                                icon:[UIImage imageNamed:@"list.png"]];
    
    return leftUtilityButtons;
}

#pragma mark - SWTableViewDelegate

-(void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    //switch (index) {
    //    case 0:
    //        NSLog(@"left button 0 was pressed");
    //        break;
    //    case 1:
    //        NSLog(@"left button 1 was pressed");
    //        break;
    //    case 2:
    //        NSLog(@"left button 2 was pressed");
    //        break;
    //    case 3:
    //        NSLog(@"left btton 3 was pressed");
    //    default:
    //        break;
    //}
}

-(void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    NSIndexPath *indexPath = [self.tableViewNotes indexPathForCell:cell];
    Note *note = (Note *)[workingNotesArray objectAtIndex:indexPath.row];
    BOOL isArchived = note.isArchive.boolValue;

    //switch (index) {
    //    case 0:
    //    {
    //        noteContentViewController = [[NoteContentViewController alloc] init];
    //        noteContentViewController.delegate = self;
    //        
    //        noteContentViewController.isDuplicateOperation = YES;
    //        noteContentViewController.noteObject = (Note*)[workingNotesArray objectAtIndex:indexPath.row];
    //        
    //        [self presentPopupViewController:noteContentViewController animationType:PopupViewAnimationSlideBottomBottom];
    //        
    //        [cell hideUtilityButtonsAnimated:YES];
    //        
    //        break;
    //    }
    ////        case 1:
    ////        {
    ////            NSLog(@"Share button was pressed");
    ////            UIAlertView *alertTest = [[UIAlertView alloc] initWithTitle:@"Hello" message:@"Share more more" delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles: nil];
    ////            [alertTest show];
    ////            
    ////            [cell hideUtilityButtonsAnimated:YES];
    ////            break;
    ////        }
    //    case 1:
    //    {
    //        note.isArchive = [NSNumber numberWithBool:!note.isArchive.boolValue];
    //        [[NoteDataManager sharedInstance] editNote:note useMainThread:YES];
    //        
    //        [workingNotesArray removeObjectAtIndex:indexPath.row];
    //        [self.tableViewNotes deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    //        [self.collectionViewNotes deleteItemsAtIndexPaths:@[indexPath]];
    //
    //        break;
    //    }
    //    case 2:
    //    {
    //        // Delete button was pressed
    //        
    //        bool deleteSuccess = [[NoteDataManager sharedInstance] deleteNote:note useMainThread:YES];
    //        
    //        if (deleteSuccess){
    //            [workingNotesArray removeObjectAtIndex:indexPath.row];
    //            [self.tableViewNotes deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    //            [self.collectionViewNotes deleteItemsAtIndexPaths:@[indexPath]];
    //        }
    //
    //        break;
    //    }
    //    default:
    //        break;
    //}
    
    // Feature
    // jca-05042016
    // Implement Edit, Duplicate and Move Options in Swipeable Cell
    
    if (isArchived) {
        switch (index) {
            // Padding
            case 0: case 1: case 2: {
                break;
            }
            // Delete
            case 3: {
                [self shouldDeleteNote:note.title completion:^(BOOL delete) {
                    if (delete) {
                        bool deleteSuccess = [[NoteDataManager sharedInstance] deleteNote:note useMainThread:YES];
                        
                        if (deleteSuccess){
                            [workingNotesArray removeObjectAtIndex:indexPath.row];
                            [self.tableViewNotes deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                            [self.collectionViewNotes deleteItemsAtIndexPaths:@[indexPath]];
                        }
                        else {
                            NSString *message = NSLocalizedString(@"There was an error deleting note. Please try again later.", nil);
                            [self.view makeToast:message duration:2.0f position:@"center"];
                        }
                    }
                }];
                
                break;
            }
            // Restore
            case 4: {
                note.isArchive = [NSNumber numberWithBool:NO];
                [[NoteDataManager sharedInstance] editNote:note useMainThread:YES];
                
                [self filterWorkingNotesArrayByArchives];
                [self.tableViewNotes reloadData];
                [self.collectionViewNotes reloadData];
                [noteBookCollectionViewController loadData];
                
                break;
            }
                
            default: {
                break;
            }
        }
    }
    else {
        switch (index) {
            // Edit
            case 0: {
                noteContentViewController = [[NoteContentViewController alloc] init];
                noteContentViewController.delegate = self;
                noteContentViewController.isDuplicateOperation = NO;
                noteContentViewController.noteObject = note;
                noteContentViewController.currentNotebook = note.folder;
                [self presentPopupViewController:noteContentViewController animationType:PopupViewAnimationSlideBottomBottom];
                
                break;
            }
            // Duplicate
            case 1: {
                noteContentViewController = [[NoteContentViewController alloc] init];
                noteContentViewController.delegate = self;
                noteContentViewController.isDuplicateOperation = YES;
                noteContentViewController.noteObject = note;
                noteContentViewController.currentNotebook = note.folder;
                [self presentPopupViewController:noteContentViewController animationType:PopupViewAnimationSlideBottomBottom];
                
                break;
            }
            // Delete
            case 2: {
                [self shouldDeleteNote:note.title completion:^(BOOL delete) {
                    if (delete) {
                        bool deleteSuccess = [[NoteDataManager sharedInstance] deleteNote:note useMainThread:YES];
                        
                        if (deleteSuccess){
                            [workingNotesArray removeObjectAtIndex:indexPath.row];
                            [self.tableViewNotes deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                            [self.collectionViewNotes deleteItemsAtIndexPaths:@[indexPath]];
                        }
                        else {
                            NSString *message = NSLocalizedString(@"There was an error deleting note. Please try again later.", nil);
                            [self.view makeToast:message duration:2.0f position:@"center"];
                        }
                    }
                }];
                
                break;
            }
            // Move
            case 3: {
                self.noteBookModalView = [[NoteBookModalView alloc] init];
                self.noteBookModalView.delegate = self;
                self.noteBookModalView.note = note;
                self.noteBookModalView.currentNoteBook = note.folder;
                
                [self presentPopupViewController:self.noteBookModalView animationType:PopupViewAnimationSlideBottomBottom];
                
                break;
            }
            // Archive
            case 4: {
                note.isArchive = [NSNumber numberWithBool:YES];
                [[NoteDataManager sharedInstance] editNote:note useMainThread:YES];
                
                [self filterWorkingNotesArray];
                [self.tableViewNotes reloadData];
                [self.collectionViewNotes reloadData];
                [noteBookCollectionViewController loadData];
                
                break;
            }
            default: {
                break;
            }
        }
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    return YES;
}

#pragma mark UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    Note *note = [workingNotesArray objectAtIndex:indexPath.row];
    NSString *cellId;
    
    if (note.isArchive.boolValue){
        cellId = @"UMCellArchived";
        
        UMTableViewCellArchived *cell = (UMTableViewCellArchived *)[self.tableViewNotes dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
        
        UMTableViewCellArchived __weak *weakCell = cell;
        
        [cell setAppearanceWithBlock:^{
            
            weakCell.leftUtilityButtons = [self leftButtons];
            weakCell.rightUtilityButtons = [self rightButtons:note.isArchive.boolValue];
            weakCell.delegate = self;
            weakCell.containingTableView = self.tableViewNotes;
        } force:NO];
        
        [cell setCellHeight:cell.frame.size.height];
        
        cell.labelTitle.text = note.title;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        cell.labelModifiedDate.text = [dateFormatter stringFromDate:note.modified];
        
        NSString *delimitedTags = @"";
        for (NoteTag *noteTag in note.noteTags) {
            if ([noteTag.isRemoved isEqualToNumber:[NSNumber numberWithBool:YES]])
                continue;
            
            if ([delimitedTags isEqualToString:@""])
                delimitedTags = noteTag.tag.tagName;
            else
                delimitedTags = [NSString stringWithFormat:@"%@, %@", delimitedTags, noteTag.tag.tagName];
        }
        
        cell.labelTags.text = delimitedTags;
        
        Color *color = [[NoteDataManager sharedInstance] getColorByColorId:[note.colorId intValue] useMainThread:YES];
        UIColor *backgroundColor = [[NoteDataManager sharedInstance] getUIColorFromColorObject:color];
        
        cell.labelImage.backgroundColor = backgroundColor;
        
        return cell;
    }
    
    else{
        cellId = @"UMCellDefault";
        
        UMTableViewCellDefault *cell = (UMTableViewCellDefault *)[self.tableViewNotes dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
        
        UMTableViewCellDefault __weak *weakCell = cell;
        
        [cell setAppearanceWithBlock:^{
            
            weakCell.leftUtilityButtons = [self leftButtons];
            weakCell.rightUtilityButtons = [self rightButtons:note.isArchive.boolValue];
            weakCell.delegate = self;
            weakCell.containingTableView = self.tableViewNotes;
        } force:NO];
        
        [cell setCellHeight:cell.frame.size.height];
        
        
        cell.labelTitle.text = note.title;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        cell.labelModifiedDate.text = [dateFormatter stringFromDate:note.modified];
        
        NSString *delimitedTags = @"";
        for (NoteTag *noteTag in note.noteTags) {
            if ([noteTag.isRemoved isEqualToNumber:[NSNumber numberWithBool:YES]])
                continue;
            
            if ([delimitedTags isEqualToString:@""])
                delimitedTags = noteTag.tag.tagName;
            else
                delimitedTags = [NSString stringWithFormat:@"%@, %@", delimitedTags, noteTag.tag.tagName];
        }
        
        cell.labelTags.text = delimitedTags;
        
        Color *color = [[NoteDataManager sharedInstance] getColorByColorId:[note.colorId intValue] useMainThread:YES];
        UIColor *backgroundColor = [[NoteDataManager sharedInstance] getUIColorFromColorObject:color];
        
        cell.labelImage.backgroundColor = backgroundColor;
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row % 2 == 0)
        cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
    else
        cell.backgroundColor = [UIColor whiteColor];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //noteContentViewController = [[NoteContentViewController alloc] init];
    //noteContentViewController.delegate = self;
    //
    //noteContentViewController.noteObject = (Note*)[workingNotesArray objectAtIndex:indexPath.row];
    //
    ////    NotesViewController *vc = (NotesViewController*)delegate;
    //[self presentPopupViewController:noteContentViewController animationType:PopupViewAnimationSlideBottomBottom];
    
    // Bug Fix
    // jca-05052016
    // NoteBook object becomes null (edit operation)
    
    if (!self.isArchiveView) {
    noteContentViewController = [[NoteContentViewController alloc] init];
    noteContentViewController.delegate = self;
        noteContentViewController.isDuplicateOperation = NO;

        Note *note = (Note *)[workingNotesArray objectAtIndex:indexPath.row];
        noteContentViewController.noteObject = note;
        noteContentViewController.currentNotebook = note.folder;
    
    [self presentPopupViewController:noteContentViewController animationType:PopupViewAnimationSlideBottomBottom];
}
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return workingNotesArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    if ([view isEqual:collectionViewNotes]){
        return workingNotesArray.count;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"NCCellDefault";

    if (indexPath.row == 0){
        cellIdentifier = @"NCCellAdd";
    }

    if (indexPath.row == 1){
        cellIdentifier = @"NCCellGeneral";
    }
    
    if ([collectionView isEqual:collectionViewNotes]){
        NotesCollectionViewCell *cell = (NotesCollectionViewCell *)[self.collectionViewNotes dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        Note *note = [workingNotesArray objectAtIndex:indexPath.row];
        cell.labelTitle.text = note.title;
        cell.textDescription.text = note.content;
        
        Color *color = [[NoteDataManager sharedInstance] getColorByColorId:[note.colorId intValue] useMainThread:YES];
        UIColor *backgroundColor = [[NoteDataManager sharedInstance] getUIColorFromColorObject:color];
        
        cell.delegate = self;
        cell.isArchived = note.isArchive.boolValue;
        [cell initializeCell];
        
        NSString *fontName = [[NoteDataManager sharedInstance] getFontTypeByFontTypeId:note.fontTypeId.intValue useMainThread:YES].fontName;
        
        cell.textDescription.font = [UIFont fontWithName:fontName size:14];
        
        cell.backgroundColor = backgroundColor;
        
        return cell;        
    }
    
    return 0;
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
        
    CGSize retval = CGSizeMake(175, 175);
    return retval;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 10, 0);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //if ([collectionView isEqual:collectionViewNotes]){
    //    noteContentViewController = [[NoteContentViewController alloc] init];
    //    noteContentViewController.delegate = self;
    //    
    //    noteContentViewController.noteObject = (Note*)[workingNotesArray objectAtIndex:indexPath.row];
    //    
    //    [self presentPopupViewController:noteContentViewController animationType:PopupViewAnimationSlideBottomBottom];
    //}
    
    // Bug Fix
    // jca-05052016
    // NoteBook object becomes null (edit operation)
    if ([collectionView isEqual:collectionViewNotes]){
        if (!self.isArchiveView) {
        noteContentViewController = [[NoteContentViewController alloc] init];
        noteContentViewController.delegate = self;
            noteContentViewController.isDuplicateOperation = NO;
        
            Note *note = (Note *)[workingNotesArray objectAtIndex:indexPath.row];
            noteContentViewController.noteObject = note;
            noteContentViewController.currentNotebook = note.folder;
        
        [self presentPopupViewController:noteContentViewController animationType:PopupViewAnimationSlideBottomBottom];
    }
    }
}

#pragma mark - NoteBookModalView Delegate

- (void)shouldReloadNoteCollection:(BOOL)reload {
    if (reload) {
        [self setupNotesData];
        [self dismissPopupViewControllerWithanimationType:PopupViewAnimationSlideBottomBottom];
    }
}

@end
