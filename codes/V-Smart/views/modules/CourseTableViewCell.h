//
//  CourseTableViewCell.h
//  V-Smart
//
//  Created by Julius Abarra on 9/1/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CourseTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblCourseTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblCourseDescription;
@property (strong, nonatomic) IBOutlet UILabel *lblCourseSection;
@property (strong, nonatomic) IBOutlet UILabel *lblCourseGradeLevel;

@end
