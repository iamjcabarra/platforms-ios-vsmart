//
//  GradeBookCourseList.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/13/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GradeBookCourseListDelegate <NSObject>
@required
- (void)didFinishSelectingCourseObject:(NSDictionary *)course;
@end

@interface GradeBookCourseList : UIViewController

@property (nonatomic, strong) NSString *userid;
@property (nonatomic, weak) id<GradeBookCourseListDelegate> delegate;
@end
