//
//  LessonPlanChildViewController.h
//  V-Smart
//
//  Created by VhaL on 5/8/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface LessonPlanChildViewController : UIViewController <UIWebViewDelegate>

@end
