//
//  SSPeopleTeacherViewController.m
//  V-Smart
//
//  Created by VhaL on 3/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "SSPeopleTeacherViewController.h"
#import "SSPeopleCell.h"
#import "SSPeopleClassesCell.h"

#import "SSPeopleItem.h"
#import "JMSGroupItem.h"
#import "JMSPeopleItem.h"


@interface SSPeopleTeacherViewController ()

@property (nonatomic, weak) IBOutlet UITableView *tableViewClasses;
@property (nonatomic, weak) IBOutlet UITableView *tableViewStudent;

@property (nonatomic, weak) IBOutlet UIButton *buttonBack;
@property (nonatomic, weak) IBOutlet UIView *viewDetail;
@property (nonatomic, weak) IBOutlet UIView *viewList;

@property (nonatomic, weak) IBOutlet UILabel *labelPostedTopics;
@property (nonatomic, weak) IBOutlet UILabel *labelRepliedTopics;
@property (nonatomic, weak) IBOutlet UILabel *labelLikedByMe;
@property (nonatomic, weak) IBOutlet UILabel *labelReceivedLikes;
@property (nonatomic, weak) IBOutlet UILabel *labelFullName;
@property (nonatomic, weak) IBOutlet UILabel *labelPosition;
@property (nonatomic, weak) IBOutlet UILabel *labelSubjects;
@property (nonatomic, weak) IBOutlet UILabel *labelAlias;
@property (nonatomic, weak) IBOutlet UIImageView *imageViewAvatar;

@property (nonatomic, strong) NSMutableArray *arrayClasses;
@property (nonatomic, strong) NSMutableArray *arrayStudents;
@property (nonatomic, strong) NSMutableArray *arrayStudentsWorking;

@property (nonatomic, assign) BOOL isClassesView;

@property (nonatomic, strong) UIRefreshControl *refreshControlTableView;

@end

@implementation SSPeopleTeacherViewController
@synthesize buttonBack, viewDetail, viewList, isClassesView;

@synthesize labelAlias, labelLikedByMe, labelPostedTopics, labelReceivedLikes, labelRepliedTopics, imageViewAvatar, labelFullName, labelPosition, labelSubjects;

-(AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    isClassesView = YES;

    imageViewAvatar.layer.cornerRadius = imageViewAvatar.frame.size.height/2;
    //    imageViewAvatar.layer.borderWidth = 1;
    imageViewAvatar.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    labelAlias.layer.cornerRadius = labelAlias.frame.size.height/2;
    //    labelAlias.layer.borderWidth = 1;
    labelAlias.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    UINib *cellNibStudent = [UINib nibWithNibName:@"SSPeopleCell" bundle:nil];
    [self.tableViewStudent registerNib:cellNibStudent forCellReuseIdentifier:@"SSPeopleCell"];

    UINib *cellNibClasses = [UINib nibWithNibName:@"SSPeopleClassesCell" bundle:nil];
    [self.tableViewClasses registerNib:cellNibClasses forCellReuseIdentifier:@"SSPeopleClassesCell"];
    
    [self initializeData];
    [self buttonBackTapped:buttonBack];
    [self addRefreshMode];
    
    self.tableViewStudent.hidden = YES;
    self.tableViewClasses.hidden = NO;
}

-(void)addRefreshMode{
    self.refreshControlTableView = [[UIRefreshControl alloc] init];
    NSString *pullRefresh = NSLocalizedString(@"Pull to Refresh",nil);
    self.refreshControlTableView.attributedTitle = [[NSAttributedString alloc] initWithString:pullRefresh];
    [self.refreshControlTableView addTarget:self action:@selector(initializeData) forControlEvents:UIControlEventValueChanged];
    
    [self.tableViewClasses addSubview:self.refreshControlTableView];
}

-(void)initializeData{
    self.arrayClasses = [[NSMutableArray alloc] init];
    self.arrayStudents = [[NSMutableArray alloc] init];
    
    NSString *peopleInfo = [NSString stringWithFormat:kEndPointGetPeopleInfo, [self account].user.id];
    NSString *url = [Utils buildUrl:peopleInfo];
    NSLog(@"getUserPeople url: %@", url);
    
    [JSONHTTPClient getJSONFromURLWithString:url completion:^(NSDictionary *json, JSONModelError *err) {
        
        if(err == nil){
            NSArray *arrayRecords = [json objectForKey:@"records"];
            
            NSError *parseError = nil;
            if ([[self account].user.position isEqualToString:kModeIsTeacher]){
                
//                NSArray *arrayClasses = [dictRecord objectForKey:@"Classes"];
                NSArray *arrayClasses = [self getTeachersRecord:arrayRecords];
                
                for (NSDictionary *dict in arrayClasses) {
                    [self.arrayClasses addObject:dict];
                    NSArray *studentsList = [dict objectForKey:@"Students"];
                    NSDictionary *studentsListToDict = @{@"records": studentsList};
                    
                    NSData *records = [NSJSONSerialization dataWithJSONObject:studentsListToDict options:NSJSONWritingPrettyPrinted error:&parseError];
                    NSString *jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
                    JMSRecordPeopleItem *students = [[JMSRecordPeopleItem alloc] initWithString:jsonString error:&parseError];
                    [self addPeopleFrom:students.records isTeacher:NO];
                }
                
                [self.tableViewClasses reloadData];
            }
        }
        else{
            /* localizable strings */
            NSString *connectionErrorMessage = NSLocalizedString(@"Please make sure you are connected to the server", nil); //checked
            [self.view makeToast:connectionErrorMessage duration:2.0f position:@"center"];
        }
        
        [self.refreshControlTableView endRefreshing];
    }];
}

- (NSString *)formatData:(NSString *)string {
    return [NSString stringWithFormat:@"%@",string];
}

- (NSArray *)getTeachersRecord:(NSArray *)records {
    
    NSArray *list = nil;
    NSMutableArray *listItem = [NSMutableArray array];
    
    for (NSDictionary *s in records) {
        
        NSString *section_id = [self formatData:s[@"section_id"]];
        
        //Section
        NSString *section_name = [self formatData:s[@"section_name"]];
        NSDictionary *section = @{@"name":section_name};

        NSString *course_id = [self formatData:s[@"course_id"]];
        NSString *course_name = [self formatData:s[@"course_name"]];
        NSString *course_code = [self formatData:s[@"course_code"]];
        NSString *course_description = [self formatData:s[@"course_description"]];
        
        NSMutableArray *studentList = [NSMutableArray array];
        for (NSDictionary *student in s[@"students"] ) {
            
            NSString *user_id = [self formatData:student[@"user_id"]];
            NSString *first_name = [self formatData:student[@"first_name"]];
            NSString *last_name = [self formatData:student[@"last_name"]];
            NSString *avatar = [self formatData:student[@"avatar"]];
            NSString *online = [self formatData:student[@"is_logged_in"]];
            NSString *type = [self formatData:kModeIsStudent];
            
            NSDictionary *object = @{@"user_id":user_id,
                                     @"course_id":course_id,
                                     @"course_name":course_name,
                                     @"course_code":course_code,
                                     @"course_description":course_description,
                                     @"section_id":section_id,
                                     @"section_name":section_name,
                                     @"first_name":first_name,
                                     @"last_name":last_name,
                                     @"avatar":avatar,
                                     @"type":type,
                                     @"online":online
                                     };
            [studentList addObject:object];
        }
        
        //Students
        NSArray *students = [NSArray arrayWithArray:studentList];
        
        NSDictionary *data =  @{@"Section": section,
                                @"Students": students,
                                @"section_id": section_id,
                                @"section_name": section_name,
                                @"course_id": course_id,
                                @"course_name": course_name,
                                @"course_code": course_code,
                                @"course_description": course_description
                                };
        
        [listItem addObject:data];
    }
    
    list = [NSArray arrayWithArray:listItem];
    return list;
}

//-(void)initializeData{
//    self.arrayClasses = [[NSMutableArray alloc] init];
//    self.arrayStudents = [[NSMutableArray alloc] init];
//    
//    NSDictionary *dictBody = @{@"user_id": [NSNumber numberWithInt:[self account].user.id], @"position":[self account].user.position};
//    NSString *stringBody = [VSmartHelpers jsonString:dictBody];
//    
//    /*
//     NOTE:
//     Get a pointer to the JSONHTTPClient headers
//     set the schoolcode_base64
//     */
//    NSMutableDictionary *headers = [JSONHTTPClient requestHeaders];
//    headers[@"code"] = [Utils getSettingsSchoolCodeBase64];
//    
//    [JSONHTTPClient postJSONFromURLWithString:[Utils buildUrl:kEndPointGetPeopleInfo] bodyString:stringBody completion:^(NSDictionary *json, JSONModelError *err) {
//        if(err == nil){
//            NSArray *arrayRecords = [json objectForKey:@"records"];
//            NSDictionary *dictRecord = [arrayRecords objectAtIndex:0];
//            
//            NSError *parseError = nil;
//            
//            if ([[self account].user.position isEqualToString:kModeIsTeacher]){
//                NSArray *arrayClasses = [dictRecord objectForKey:@"Classes"];
//                
//                for (NSDictionary *dict in arrayClasses) {
//                    [self.arrayClasses addObject:dict];
//                    
//                    NSArray *studentsList = [dict objectForKey:@"Students"];
//                    NSDictionary *studentsListToDict = @{@"records": studentsList};
//                    NSData *records = [NSJSONSerialization dataWithJSONObject:studentsListToDict options:NSJSONWritingPrettyPrinted error:&parseError];
//                    NSString *jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
//                    JMSRecordPeopleItem *students = [[JMSRecordPeopleItem alloc] initWithString:jsonString error:&parseError];
//                    [self addPeopleFrom:students.records isTeacher:NO];
//                }
//                
//                [self.tableViewClasses reloadData];
//            }
//        }
//        else{
//            /* localizable strings */
//            NSString *connectionErrorMessage = NSLocalizedString(@"Please make sure you are connected to the server", nil); //checked
//            [self.view makeToast:connectionErrorMessage duration:2.0f position:@"center"];
//        }
//        
//        [self.refreshControlTableView endRefreshing];
//    }];
//}

-(void)addPeopleFrom:(NSArray<JMSPeopleItem>*)records isTeacher:(BOOL)isTeacher{
    for (JMSPeopleItem *peopleItemInAPI in records) {
        SSPeopleItem *peopleItem = [[SSPeopleItem alloc] init];
        
        peopleItem.id = (int)peopleItemInAPI.userId;
        peopleItem.firstName = peopleItemInAPI.firstName;
        peopleItem.lastName = peopleItemInAPI.lastName;
        peopleItem.fullName = [NSString stringWithFormat:@"%@ %@", peopleItemInAPI.firstName, peopleItemInAPI.lastName];
        
        peopleItem.isTeacher = isTeacher;
        peopleItem.position = peopleItemInAPI.type;
        peopleItem.subjectsOrNickname = @"";
        peopleItem.urlImage = peopleItemInAPI.avatar;
        peopleItem.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[Utils buildUrl:peopleItem.urlImage]]]];
        
        peopleItem.sectionId = (int)peopleItemInAPI.sectionId;
        
        peopleItem.numLikedByMe = @"0";
        peopleItem.numPostedTopics = @"0";
        peopleItem.numReceivedLikes = @"0";
        peopleItem.numRepliedTopics = @"0";
        
        [self.arrayStudents addObject:peopleItem];
    }
}

-(IBAction)buttonBackTapped:(UIButton*)button{
    
    if (viewList.hidden == NO){
//        self.tableViewClasses.hidden = NO;
//        self.tableViewStudent.hidden = YES;
//        buttonBack.hidden = YES;
        
        /*To hide*/
        [UIView animateWithDuration:0.25 animations:^{
            [self.tableViewStudent setAlpha:0.0f];
            [self.buttonBack setAlpha:0.0f];
        } completion:^(BOOL finished) {
            [self.tableViewStudent setHidden:YES];
            [self.buttonBack setHidden:YES];
        }];
        
        /*To unhide*/
        [self.tableViewClasses setHidden:NO];
        [UIView animateWithDuration:0.25 animations:^{
            self.tableViewClasses.alpha = 1.0f;
        } completion:^(BOOL finished) {
        }];
    }
    
//    viewDetail.hidden = YES;
//    viewList.hidden = NO;
    
    /*To hide*/
    [UIView animateWithDuration:0.25 animations:^{
        [viewDetail setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [viewDetail setHidden:YES];
    }];
    
    /*To unhide*/
    [viewList setHidden:NO];
    [UIView animateWithDuration:0.25 animations:^{
        viewList.alpha = 1.0f;
    } completion:^(BOOL finished) {
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([tableView isEqual:self.tableViewStudent]) {
        return self.arrayStudentsWorking.count;
    }
    else{
        return self.arrayClasses.count;
    }
    
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView isEqual:self.tableViewStudent]) {
        SSPeopleCell *cell = (SSPeopleCell*)[tableView dequeueReusableCellWithIdentifier:@"SSPeopleCell"];
        if (cell==nil) {
            cell = [[SSPeopleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SSPeopleCell"];
        }
        
        [cell initializeCell];
        
        SSPeopleItem *peopleItem = [self.arrayStudentsWorking objectAtIndex:indexPath.row];
        cell.name.text = peopleItem.fullName;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if ([peopleItem.urlImage isEqualToString:@""]){
            cell.avatar.hidden = YES;
            cell.alias.hidden = NO;
            cell.alias.text = [VSmartHelpers aliasFromFirstName:peopleItem.firstName andLastName:peopleItem.lastName];;
        }
        else{
            cell.avatar.image = peopleItem.image;
            cell.avatar.hidden = NO;
            cell.alias.hidden = YES;
        }
        
        return cell;
    }
    else{
        SSPeopleClassesCell *cell = (SSPeopleClassesCell*)[tableView dequeueReusableCellWithIdentifier:@"SSPeopleClassesCell"];
        if (cell==nil) {
            cell = [[SSPeopleClassesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SSPeopleClassesCell"];
        }
        
        NSDictionary *dictSection = [self.arrayClasses[indexPath.row] objectForKey:@"Section"];
        cell.name.text = [dictSection objectForKey:@"name"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView isEqual:self.tableViewStudent]) {
//        buttonBack.hidden = NO;
//        viewDetail.hidden = NO;
//        viewList.hidden = YES;
        
        
        /*To hide*/
        [UIView animateWithDuration:0.25 animations:^{
            [viewList setAlpha:0.0f];
        } completion:^(BOOL finished) {
            [viewList setHidden:YES];
        }];
        
        /*To unhide*/
        [buttonBack setHidden:NO];
        [viewDetail setHidden:NO];
        [UIView animateWithDuration:0.25 animations:^{
            buttonBack.alpha = 1.0f;
            viewDetail.alpha = 1.0f;
        } completion:^(BOOL finished) {
        }];
        
        SSPeopleCell *cell = (SSPeopleCell*)[tableView cellForRowAtIndexPath:indexPath];
        
        SSPeopleItem *peopleItem = [self.arrayStudentsWorking objectAtIndex:indexPath.row];
        
        labelFullName.text = peopleItem.fullName;
        labelPosition.text = peopleItem.position;
        labelSubjects.text = peopleItem.subjectsOrNickname;
        
        labelLikedByMe.text = peopleItem.numLikedByMe;
        labelPostedTopics.text = peopleItem.numPostedTopics;
        labelReceivedLikes.text = peopleItem.numReceivedLikes;
        labelRepliedTopics.text = peopleItem.numRepliedTopics;
        
        /*
         NOTE:
         Get a pointer to the JSONHTTPClient headers
         set the schoolcode_base64
         */
        NSMutableDictionary *headers = [JSONHTTPClient requestHeaders];
        headers[@"code"] = [Utils getSettingsSchoolCodeBase64];
        
        NSString *url = VS_FMT([Utils buildUrl:kEndPointGetUserProfile], peopleItem.id);
        [JSONHTTPClient getJSONFromURLWithString:url completion:^(NSDictionary *json, JSONModelError *err) {
            if(err == nil){
                NSArray *arrayRecords = [json objectForKey:@"records"];
                NSDictionary *dictStatistics;
                for (NSDictionary *dict in arrayRecords) {
                    //Statistics
                    if ([dict objectForKey:@"Statistics"]) {
                        dictStatistics = [dict objectForKey:@"Statistics"];
                        
                        if ([dictStatistics objectForKey:@"count_liked"]){
                            NSString *countLike = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_liked"]];
                            labelLikedByMe.text = countLike;
                        }
                        
                        if ([dictStatistics objectForKey:@"count_posted"]){
                            NSString *countPosted = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_posted"]];
                            labelPostedTopics.text = countPosted;
                        }
                        
                        if ([dictStatistics objectForKey:@"count_received_likes"]){
                            NSString *countReceivedLikes = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_received_likes"]];
                            labelReceivedLikes.text = countReceivedLikes;
                        }
                        
                        if ([dictStatistics objectForKey:@"count_replied"]){
                            NSString *countReplied = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_replied"]];
                            labelRepliedTopics.text = countReplied;
                        }
                        
                        continue;
                    }
                }
            }
            else{
                /* localizable strings */
                NSString *connectionErrorMessage = NSLocalizedString(@"Please make sure you are connected to the server", nil); //checked
                [self.view makeToast:connectionErrorMessage duration:2.0f position:@"center"];
            }
        }];
        
        if ([peopleItem.urlImage isEqualToString:@""]){
            imageViewAvatar.hidden = YES;
            labelAlias.hidden = NO;
            labelAlias.backgroundColor = cell.alias.backgroundColor;
            labelAlias.text = [VSmartHelpers aliasFromFirstName:peopleItem.firstName andLastName:peopleItem.lastName];;
        }
        else{
            imageViewAvatar.image = peopleItem.image;
            imageViewAvatar.hidden = NO;
            labelAlias.hidden = YES;
        }
        
//        self.tableViewClasses.hidden = YES;
//        self.tableViewStudent.hidden = NO;
        
        /*To hide*/
        [UIView animateWithDuration:0.25 animations:^{
            [self.tableViewClasses setAlpha:0.0f];
        } completion:^(BOOL finished) {
            [self.tableViewClasses setHidden:YES];
        }];
        
        /*To unhide*/
        [self.tableViewStudent setHidden:NO];
        [UIView animateWithDuration:0.25 animations:^{
            self.tableViewStudent.alpha = 1.0f;
        } completion:^(BOOL finished) {
        }];
    }
    else{
        NSDictionary *dictSection = [self.arrayClasses[indexPath.row] objectForKey:@"Section"];
        NSString *sectionId = [dictSection objectForKey:@"section_id"];
        
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"sectionId == %d", [sectionId intValue]];
        self.arrayStudentsWorking = [NSMutableArray arrayWithArray:[self.arrayStudents filteredArrayUsingPredicate:pred]];
        
//        self.tableViewClasses.hidden = YES;
//        self.tableViewStudent.hidden = NO;
//        buttonBack.hidden = NO;
        
        /*To hide*/
        [UIView animateWithDuration:0.25 animations:^{
            [self.tableViewClasses setAlpha:0.0f];
        } completion:^(BOOL finished) {
            [self.tableViewClasses setHidden:YES];
        }];
        
        /*To unhide*/
        [self.tableViewStudent setHidden:NO];
        [buttonBack setHidden:NO];
        [UIView animateWithDuration:0.25 animations:^{
            self.tableViewStudent.alpha = 1.0f;
            buttonBack.alpha = 1.0f;
        } completion:^(BOOL finished) {
        }];
        
        [self.tableViewStudent reloadData];
    }
}

@end