//
//  LPMLessonCompetencyListTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 14/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMLessonCompetencyListTableViewCell: UITableViewCell {
    
    @IBOutlet var competencyCodeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
