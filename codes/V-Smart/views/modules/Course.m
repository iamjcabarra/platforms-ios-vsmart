//
//  Course.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/6/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "Course.h"
#import "Topic.h"


@implementation Course

@dynamic course_code;
@dynamic course_id;
@dynamic course_index;
@dynamic course_name;
@dynamic course_description;
@dynamic initial;
@dynamic cs_id;
@dynamic section_id;
@dynamic schedule;
@dynamic venue;
@dynamic topics;

@end
