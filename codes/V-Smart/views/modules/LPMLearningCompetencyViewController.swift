//
//  LPMLearningCompetencyViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 25/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMLearningCompetencyViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate {
    
    @IBOutlet fileprivate var searchBar: UISearchBar!
    @IBOutlet fileprivate var learningCompetencyTableView: UITableView!
    
    fileprivate let kLearningCompetencyCellIdentifier = "learning_competency_cell_identifier"
    fileprivate var searchKey = ""
    
    // MARK: - Data Managers
    
    fileprivate lazy var lessonPlanDataManager: LessonPlanDataManager = {
        let lpdm = LessonPlanDataManager.sharedInstance()
        return lpdm!
    }()
   
    fileprivate lazy var curriculumPlannerDataManager: CPMDataManager = {
        let cpdm = CPMDataManager.sharedInstance
        return cpdm
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.learningCompetencyTableView.estimatedRowHeight = 44.0;
        self.learningCompetencyTableView.rowHeight = UITableViewAutomaticDimension;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.searchBar.placeholder = NSLocalizedString("Search", comment: "")
        self.searchBar.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table View Data Source

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }

        return sectionCount
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }

        return sectionData.numberOfObjects
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kLearningCompetencyCellIdentifier, for: indexPath) as! LPMLearningCompetencyTableViewCell
        cell.selectionStyle = .none
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }

    func configureCell(_ cell: LPMLearningCompetencyTableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        
        cell.learningCompetencyCodeLabel.text = mo.value(forKey: "code") as? String
        cell.learningCompetencyLabel.text = mo.value(forKey: "competency") as? String
        self.justifyLabel(cell.learningCompetencyLabel)
        
        if let selected = mo.value(forKey: "is_selected") as? String {
            cell.showSelected(selected == "0" ? false : true)
        }
    }
    
    func justifyLabel(_ label: UILabel) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        
        let attributedString = NSAttributedString(string: label.text!, attributes: [NSParagraphStyleAttributeName: paragraphStyle, NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)])
        label.attributedText = attributedString
        label.numberOfLines = 0
    }

    // MARK: - Table View Delegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        let competencyID = mo.value(forKey: "competency_id") as! String
        let selected = mo.value(forKey: "is_selected") as! String
        
        let success = self.lessonPlanDataManager.removePreAssociatedLearningCompetency(competencyID)
        
        if success {
        let data = ["is_selected": selected == "0" ? "1" : "0"]
        let predicate = self.curriculumPlannerDataManager.predicateForKeyPath("competency_id", exactValue: competencyID)
        _ = self.curriculumPlannerDataManager.updateEntity(CPMConstants.Entity.CURRICULUM_LEARNING_COMPETENCY, filteredByPredicate: predicate, withData: data as NSDictionary)
        }
        
        self.learningCompetencyTableView.deselectRow(at: indexPath, animated: true)

    }
    
    // MARK: - Fetched Results Controller

    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }

        let ctx = self.curriculumPlannerDataManager.mainContext

        //let fetchRequest = NSFetchRequest(entityName: CPMConstants.Entity.CURRICULUM_LEARNING_COMPETENCY)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: CPMConstants.Entity.CURRICULUM_LEARNING_COMPETENCY)
        fetchRequest.fetchBatchSize = 20
        
        if (self.searchKey != "") {
            let predicate = self.curriculumPlannerDataManager.predicateForKeyPath("search_string", containsValue: self.searchKey)
            fetchRequest.predicate = predicate
        }

        let descriptorSelector = #selector(NSString.localizedCaseInsensitiveCompare(_:))
        let sortDescriptor = NSSortDescriptor.init(key: "code", ascending: true, selector:descriptorSelector)
        fetchRequest.sortDescriptors = [sortDescriptor]

        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self

        _fetchedResultsController = frc

        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }

        return _fetchedResultsController!
    }

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.learningCompetencyTableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {

        switch type {
        case .insert:
            self.learningCompetencyTableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.learningCompetencyTableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }

    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {

        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.learningCompetencyTableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.learningCompetencyTableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.learningCompetencyTableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! LPMLearningCompetencyTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.learningCompetencyTableView.deleteRows(at: [indexPath], with: .fade)
            }

            if let newIndexPath = newIndexPath {
                self.learningCompetencyTableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }

    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.learningCompetencyTableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.learningCompetencyTableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Search Bar Delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchKey = searchText
        self.reloadFetchedResultsController()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchKey = searchBar.text!
        self.reloadFetchedResultsController()
    }

}
