//
//  LPMSMDCollapsibleContentHeaderCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 13/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMSMDCollapsibleContentHeaderCell: UITableViewCell {
    
    @IBOutlet var stageNameLabel: UILabel!
    @IBOutlet var collapseImage: UIImageView!
    @IBOutlet var collapseButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
