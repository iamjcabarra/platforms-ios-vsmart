//
//  CourseCategoryListView.m
//  V-Smart
//
//  Created by Julius Abarra on 16/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "CourseCategoryListView.h"
#import "CourseCategoryItemCell.h"
#import "NewCurriculumListView.h"
#import "CurriculumDataManager.h"
#import "HUD.h"

@interface CourseCategoryListView () <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, UISearchControllerDelegate>

@property (strong, nonatomic) CurriculumDataManager *cpdm;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) UIRefreshControl *tableRefreshControl;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *results;
@property (strong, nonatomic) NSString *userid;
@property (strong, nonatomic) NSManagedObject *selectedObject;

@property (assign, nonatomic) BOOL isAscending;

@end

@implementation CourseCategoryListView

static NSString *kCourseCategotyCellIdentifier = @"courseCategoryCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Curriculum Planner Data Manager
    self.cpdm = [CurriculumDataManager sharedInstance];
    self.managedObjectContext = self.cpdm.mainContext;
    
    // Get User ID
    self.userid = [self.cpdm loginUser];
    
    // Protocols for UITableView
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Table Cell Height
    self.tableView.rowHeight = 44.0f;
    
    // Set initial result to descending order
    self.isAscending = YES;
    
    SEL refreshAction = @selector(listCourseCategory);
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    [self setupRightBarButton];
    [self setupSearchCapabilities];
    [self listCourseCategory];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Set navigation bar title
    self.title = NSLocalizedString(@"Course Category List", nil);
    
    // Decorate navigation bar
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    self.navigationController.navigationBar.barTintColor = UIColorFromHex(0x4475B7);
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - List Course Category

- (void)listCourseCategory {
    __weak typeof(self) wo = self;
    
    [self.cpdm requestCourseCategoryListForUser:self.userid doneBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo.tableRefreshControl endRefreshing];
                [wo reloadFetchedResultsController];
                [wo.tableView reloadData];
            });
        }
    }];
}

#pragma mark - Sort Button

- (void)setupRightBarButton {
    UIImage *sortImage = [UIImage imageNamed:@"sort_258x258"];
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    sortButton.frame = CGRectMake(0, 0, 44, 44);
    sortButton.showsTouchWhenHighlighted = YES;
    
    [sortButton setImage:sortImage forState:UIControlStateNormal];
    [sortButton setImage:sortImage forState:UIControlStateHighlighted];
    [sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
}

- (IBAction)sortButtonAction:(id)sender {
    self.isAscending = (self.isAscending) ? NO : YES;
    [self reloadFetchedResultsController];
}

#pragma mark - Search Bar

- (void)setupSearchCapabilities {
    // Allocate and initialize results
    self.results = [[NSMutableArray alloc] init];
    
    // Init a search controller reusing the current table view controller for results
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    
    // Do not dim and hide the navigation during presentation
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = YES;
    
    // Make an appropriate size for search bar and add it as a header view for initial table view
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // Enable presentation context
    self.definesPresentationContext = YES;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
        return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CourseCategoryItemCell *itemCell = [tableView dequeueReusableCellWithIdentifier:kCourseCategotyCellIdentifier forIndexPath:indexPath];
    [self configureCell:itemCell atIndexPath:indexPath];
    return itemCell;
}

- (void)configureFilteredCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = self.results[indexPath.row];
    CourseCategoryItemCell *itemCell = (CourseCategoryItemCell *)cell;
    [self configureCell:itemCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    CourseCategoryItemCell *itemCell = (CourseCategoryItemCell *)cell;
    [self configureCell:itemCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(CourseCategoryItemCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *name = [self stringValue:[mo valueForKey:@"name"]];
    NSString *desc = [self stringValue:[mo valueForKey:@"desc"]];
    
    cell.courseCategoryNameLabel.text = name;
    cell.courseCategoryDescLabel.text = desc;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Please wait", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    NSString *courseCategoryID = [self stringValue:[self.selectedObject valueForKey:@"id"]];
    __weak typeof(self) wo = self;
    
    [self.cpdm requestCurriculumListForCourseCategory:courseCategoryID andUser:self.userid doneBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [wo performSegueWithIdentifier:@"showCurriculumList" sender:nil];
                [wo.tableView deselectRowAtIndexPath:indexPath animated:YES];
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                
                NSString *message = NSLocalizedString(@"No curriculum plan associated with this course category.", nil);                
                [wo showMessage:message show:YES completion:^(BOOL okay) {}];
                
                [wo.tableView deselectRowAtIndexPath:indexPath animated:YES];
            });
        }
    }];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *lblHeaderA = [[UILabel alloc] initWithFrame:CGRectMake(025, 0, 125, 50)];
    UILabel *lblHeaderB = [[UILabel alloc] initWithFrame:CGRectMake(440, 0, 125, 50)];

    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    [headerView sizeToFit];
    
    lblHeaderA.text = NSLocalizedString(@"Name", nil);
    lblHeaderA.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    lblHeaderA.textColor = [UIColor darkGrayColor];
    lblHeaderA.backgroundColor = [UIColor clearColor];
    lblHeaderA.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    
     lblHeaderB.text = NSLocalizedString(@"Initial", nil);
     lblHeaderB.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
     lblHeaderB.textColor = [UIColor darkGrayColor];
     lblHeaderB.backgroundColor = [UIColor clearColor];
     lblHeaderB.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    
    [headerView addSubview:lblHeaderA];
    [headerView addSubview:lblHeaderB];
    
    return headerView;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0f;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:self.tableView.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCurriculumList"]) {
        NewCurriculumListView *curriculumListView = (NewCurriculumListView *)[segue destinationViewController];
        curriculumListView.courseCategoryObject = self.selectedObject;
    }
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCourseCategoryEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSCompoundPredicate *cp;
    
    // Like Predicate
    NSPredicate *predicate1 = [self predicateForKeyPath:@"userid"  value:self.userid];
    
    // Contains Predicate
    NSString *searchString = self.searchController.searchBar.text;
    NSPredicate *predicate2 = [self predicateForKeyPathContains:@"name" value:searchString];
    
    if (self.searchController.active == YES && ![searchString isEqualToString:@""]) {
        cp = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    }
    else {
        cp = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1]];
    }
    
    fetchRequest.predicate = cp;
    
    NSSortDescriptor *sortedByName = [NSSortDescriptor sortDescriptorWithKey:@"name"
                                                                   ascending:self.isAscending
                                                                    selector:@selector(caseInsensitiveCompare:)];
    [fetchRequest setSortDescriptors:@[sortedByName]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - Reloading of Data

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    [self reloadFetchedResultsController];
}

- (void)reloadSearchResults {
    NSArray *items = [NSArray arrayWithArray:self.results];
    NSSortDescriptor *name = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:self.isAscending];
    NSArray *sorted = [items sortedArrayUsingDescriptors:@[name]];
    
    self.results = [NSArray arrayWithArray:sorted];
}

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

#pragma mark - Class Helpers

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

- (void)showMessage:(NSString *)message show:(BOOL)show completion:(void (^)(BOOL okay))response {
    if (show) {
        NSString *butOkay = NSLocalizedString(@"Okay", nil);
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *theAction = [UIAlertAction actionWithTitle:butOkay
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                              response(YES);
                                                          }];
        [alert addAction:theAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

@end
