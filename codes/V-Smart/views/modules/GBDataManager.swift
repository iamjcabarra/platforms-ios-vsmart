//
//  GBDataManager.swift
//  V-Smart
//
//  Created by Ryan Migallos on 16/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

class GBDataManager : Routes {
    
    var db:VSCoreDataStack!
    
    // MARK: - Singleton Method
    static let sharedInstance: GBDataManager = {
        let singleton = GBDataManager()
        return singleton
    }()
    
    // VALUE TYPE Container
    struct GradeInfo {
        let name:String
        let gender:String
        let termid:String
        let componentid:String
    }
    
    
    // VALUE TYPE Container
    struct TermInfo {
        let studentName:String
        let gender:String
        let termid:String
        let for_quarter:String
    }
    
    init() {
        self.db = VSCoreDataStack(name:"GradeBookModel")
    }
    
    // ParseProtocol
    var isProductionMode: Bool = true
    
    // MARK: - Private Properties

    var dateFormatter: DateFormatter! = DateFormatter()
    var userDefaults: UserDefaults! = UserDefaults.standard
    
    fileprivate let session = URLSession.shared
    
    typealias GBDoneBlock = (_ doneBlock: Bool) -> Void
    typealias GBDictionaryBlock = (_ dictionary: [String:AnyObject]?) -> Void
    typealias GBListBlock = (_ listBlock: [[String:AnyObject]]?) -> Void
    typealias GBDoneBlockWithError = (_ done: Bool, _ error: String?) -> Void
    
    // MARK: - Utility Methods (Non-Swift Dependency)
    func accountUserID() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.id)"
    }
    
    func accountUserEmail() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.email)"
    }
    
    func accountUserPosition() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.position)"
    }
    
    fileprivate func accountUserUserName() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.email!)"
    }
    
    fileprivate func accountUserAvatar() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.avatar!)"
    }
    
    fileprivate func getContext() -> NSManagedObjectContext! {
        return self.db.objectContext()
    }
    
    func getMainContext() -> NSManagedObjectContext! {
        return self.db.objectMainContext()
    }
    
    func requestResultType(_ handler:@escaping GBDoneBlock) {
        
        // NETWORKING REQUEST
        let endpoint = "/v2/quizzes/list/result/types"
        let request = route("GET", uri: endpoint, body: nil)
        let ctx = self.getContext()
        _ = self.db.clearEntity(GBTConstants.Entity.RESULT, ctx: ctx!, filter: nil)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            let success = false
            
            if let e = error {
                print(e.localizedDescription)
                handler(false)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [[String:AnyObject]] else {
                    handler(false)
                    return
                }
                
                ctx?.performAndWait({
                    
                    for r in records {
                        print("result type: \(r)")
                        
                        /*
                         {
                         "id": "1",
                         "name": "Average"
                         }
                         */
                        
                        let result_type_id = self.stringValue(r["id"])
                        let result_type_name = self.stringValue(r["name"])
                        
                        let predicate = NSComparisonPredicate(keyPath: "id", withValue: result_type_id, isExact: true)
                        let mo = self.db.retrieveEntity(GBTConstants.Entity.RESULT, context: ctx!, filter: predicate)
                        mo.setValue(result_type_id, forKey: "id")
                        mo.setValue(result_type_name, forKey: "name")
                    }
                    
                    let success = self.db.saveObjectContext(ctx!)
                    handler(success)
                })
            }
            else {
                handler(success)
            }
        }) 
        
        task.resume()
    }
    
//    func insertHardcodedData(value:String, entity:String, index:Int, info:GradeInfo, context:NSManagedObjectContext) {
//        let mo = NSEntityDescription.insertNewObjectForEntityForName(entity, inManagedObjectContext: context)
//        
//        mo.setValue(value, forKey: "actual_result")
//        mo.setValue(index, forKey: "index")
//        
//        //MANDATORY
//        mo.setValue(index, forKey: "index")
//        mo.setValue(info.name, forKey: "name")
//        
//        //SECTION_NAME
//        mo.setValue(info.name, forKey: "section_name")
//        
//        mo.setValue(info.gender, forKey: "gender")
//        mo.setValue(info.termid, forKey: "term_id")
//        mo.setValue(info.componentid, forKey: "component_id")
//        
//        //FLAG
//        mo.setValue("0", forKey: "is_grade")
//    }
    
    func requestSection(_ handler:@escaping GBDoneBlock) {
        
        // NETWORKING REQUEST
        let userid = accountUserID()
        let endpoint = "/v2/sections/list/\(userid)"
        let request = route("GET", uri: endpoint, body: nil)
        
        let ctx = self.getContext()
        _ = self.db.clearEntity(GBTConstants.Entity.SECTION, ctx: ctx!, filter: nil)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                handler(false)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [[String:AnyObject]] else  {
                    handler(false)
                    return
                }
                
                ctx?.performAndWait({
                    //if status == true {
                    for r in records {
                        print("section: \(r)")
                        
                        /*
                         {
                         section_id: "1",
                         section: "VSmart Web Class 1",
                         grade_level_id: "5",
                         grade_level: "Grade 4"
                         },
                         */
                        
                        let section_id = self.stringValue(r["section_id"])
                        var section = self.stringValue(r["section"])
                        let grade_level_id = self.stringValue(r["grade_level_id"])
                        let grade_level = self.stringValue(r["grade_level"])
                        let is_advised = self.stringValue(r["is_advised"])
                        
                        if is_advised == "1" {
//                            section = section + " *"
                            let string = NSLocalizedString("My Class", comment: "")
                            section = section! + " (\(string))"
                        }
                        
                        let predicate = NSComparisonPredicate(keyPath: "section_id", withValue: section_id, isExact:true)
                        let mo = self.db.retrieveEntity(GBTConstants.Entity.SECTION, context: ctx!, filter: predicate)
                        
                        mo.setValue(section_id, forKey: "section_id")
                        mo.setValue(section, forKey: "section")
                        mo.setValue(grade_level_id, forKey: "grade_level_id")
                        mo.setValue(grade_level, forKey: "grade_level")
                        mo.setValue("0", forKey: "is_selected")
                        mo.setValue(is_advised, forKey: "is_advised")
                    }
                    
                    let success = self.db.saveObjectContext(ctx!)
                    handler(success)
                })
            }
            else {
                handler(false)
            }
        }) 
        
        task.resume()
    }
    
    func requestCourses(inSection sectionid:String, handler:@escaping GBDoneBlock) {
        
        // NETWORKING REQUEST
        let userid = accountUserID()
        let endpoint = "/v2/quizzes/courses/\(userid)/section/\(sectionid)"
        let request = route("GET", uri: endpoint, body: nil)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                handler(false)
            }
            
            let entity = GBTConstants.Entity.COURSE
            
            let ctx = self.getContext()
            //            let filter:NSPredicate? = nil
            let status = self.db.clearEntity(entity, ctx: ctx!, filter: nil)
            print("clear entity... \(status)")
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [[String:AnyObject]] else {
                    handler(false)
                    return
                }
                
                ctx?.performAndWait({
                    //if status == true {
                    for r in records {
                        print("courses: \(r)")
                        
                        /*
                         {
                         cs_id: "12",
                         course_id: "11",
                         section_id: "1",
                         details: null,
                         course_name: "Araling Panlipunan 4",
                         section_name: "VSmart Web Class 1",
                         grade_level_id: "5",
                         grade_level_name: "Grade 4",
                         is_teaching: 1                         },
                         */
                        
                        let cs_id = self.stringValue(r["cs_id"])
                        let course_id = self.stringValue(r["course_id"])
                        let section_id = self.stringValue(r["section_id"])
                        let details = self.stringValue(r["details"])
                        var course_name = self.stringValue(r["course_name"])
                        let section_name = self.stringValue(r["section_name"])
                        let grade_level_id = self.stringValue(r["grade_level_id"])
                        let grade_level_name = self.stringValue(r["grade_level_name"])
                        let is_teaching = self.stringValue(r["is_teaching"])
                        
                        let string = NSLocalizedString("My Course", comment: "")
                        course_name = (is_teaching == "1") ? "\(course_name!) (\(string))" : course_name!
                        
                        let predicate = NSComparisonPredicate(keyPath: "cs_id", withValue: cs_id, isExact:true)
                        let mo = self.db.retrieveEntity(entity, context: ctx!, filter: predicate)
                        
                        mo.setValue(cs_id, forKey: "cs_id")
                        mo.setValue(course_id, forKey: "course_id")
                        mo.setValue(section_id, forKey: "section_id")
                        mo.setValue(details, forKey: "details")
                        mo.setValue(course_name, forKey: "course_name")
                        mo.setValue(section_name, forKey: "section_name")
                        mo.setValue(grade_level_id, forKey: "grade_level_id")
                        mo.setValue(grade_level_name, forKey: "grade_level_name")
                        mo.setValue(is_teaching, forKey: "is_teaching")
                    }
                    
                    let success = self.db.saveObjectContext(ctx!)
                    handler(success)
                })
            }
            else {
                handler(false)
            }
        }) 
        
        task.resume()
    }
    
    //http://dev.api.vsmartschool.com/v2/quizzes/deployed/search/course_section/{cs_id}?quiz_id=&result_type_id=&search=
    func requestGradeBookContent(_ csId:String, quizId:String, resultTypeid:String, search:String, handler:@escaping GBDoneBlockWithError) {
        
        // ENDPOINT
        let endpoint = "/v2/quizzes/deployed/search/course_section/\(csId)"
        // QUERY PARAMETER
        let query = ["quiz_id":quizId,"result_type_id":resultTypeid,"search":search,"sort_by":""]
        // REQUEST
        let request = route("GET", uri:endpoint, query:query as [String : AnyObject]?, body: nil)
        print("GRADEBOOK CONTENT REQUEST \(String(describing: request.url))")
        // SESSION
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                handler(false, e.localizedDescription)
            }
            
            let entity = GBTConstants.Entity.TERM
            
            let ctx = self.getContext()
            let filter:NSPredicate? = nil
            let status = self.db.clearEntity(entity, ctx: ctx!, filter: filter)
            _ = self.db.clearEntity(GBTConstants.Entity.STUDENTGRADES, ctx: ctx!, filter: filter)
            _ = self.db.clearEntity(GBTConstants.Entity.STUDENT, ctx: ctx!, filter: filter)
            _ = self.db.clearEntity(GBTConstants.Entity.LEARNINGCOMPONENT, ctx: ctx!, filter: filter)
            _ = self.db.clearEntity(GBTConstants.Entity.OVERALLGRADES, ctx: ctx!, filter: filter)
            _ = self.db.clearEntity(GBTConstants.Entity.OVERALLGRADESSTUDENT, ctx: ctx!, filter: filter)
            
            print("clear entity... \(status)")
            
//            let bundle = NSBundle.mainBundle()
//            let path = bundle.pathForResource("payload", ofType: "json")
//            let content = NSData(contentsOfFile: path!)
//            guard let d = content, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }// dummy data
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else {
                handler(false, "Something went wrong")
                return
            }
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [[String:AnyObject]] else {
//                    handler(doneBlock: false)
                    let records = dictionary["records"] as! [String:String]
                    let message = self.stringValue(records["user_message"] as AnyObject?)
                    handler(false, message)
                    return
                }
                
                ctx?.performAndWait({
//                    print("records: \(records)")
                    
                    var arrayOfTerms: [[String:String]] = [[String:String]]()
                    for r in records {
                        
//                        print("courses: \(r)")
                        
                        if let term = r["term"] as! [String:AnyObject]? {
                            let term_id: String = self.stringValue(term["id"])
                            let term_name: String = self.stringValue(term["name"])
                            let date_start: String = self.stringValue(term["date_start"])
                            let date_end: String = self.stringValue(term["date_end"])
                            let is_locked: String = self.stringValue(term["locked"])
                            
                            let termDict: [String:String] = ["id":term_id,
                                                             "name":term_name];
                            arrayOfTerms.append(termDict)
                            
                            let predicate = NSComparisonPredicate(keyPath: "id", withValue: term_id, isExact:true)
                            let mo = self.db.retrieveEntity(entity, context: ctx!, filter: predicate)
                            
                            mo.setValue(term_id, forKey: "id")
                            mo.setValue(term_name, forKey: "name")
                            mo.setValue(date_start, forKey: "date_start")
                            mo.setValue(date_end, forKey: "date_end")
                            mo.setValue(is_locked, forKey: "is_locked")
                            mo.setValue(csId, forKey: "section_id")
                            
                            // BEGIN PARSING CONTENT
                            guard let content = r["content"] else { continue }
                            
                            // PROCESS STUDENTS, LEARNING COMPONENTS, HIGHEST_SCORES
                            guard
                                let students = content["students"] as? [[String:AnyObject]],
                                let component_list = content["component_list"] as? [[String:AnyObject]],
                                let highest_scores = content["highest_scores"] as? [[String:AnyObject]]
                                else { continue }
                            
                            // PROCESS STUDENT
                            defer {
                                self.processStudents(students, forTermID:term_id, context: ctx!)
                            }
                            
                            // HIGHEST SCORES
                            defer {
                                self.processHighestScores(highest_scores, forTermID: term_id, context: ctx!)
                            }
                            
                            // LEARNING COMPONENTS
                            defer {
                                self.processComponents(component_list, forTermID: term_id, context: ctx!)
                            }
                        }
                        
                        if let annual_grades = r["annual_grades"] as! [[String:Any]]? {
                            self.processOverAllGrades(annual_grades, forTerms: arrayOfTerms, context: ctx!)
                        }
                        
                    }
                    
                    let success = self.db.saveObjectContext(ctx!)
                    handler(success, nil)
                })
            }
            else {
                let records = dictionary["records"] as! [String:AnyObject]
                let message = self.stringValue(records["user_message"])
                handler(false, message)
            }
        })
        
        task.resume()
    }
    
    func processOverAllGrades(_ annual_grades: [[String:Any]], forTerms terms:[[String:String]], context:NSManagedObjectContext) {
        
        var term_index = 0
        if terms.count > 0 {
            for term in terms {
                
                let term_name: String = self.stringValue(term["name"])
                let term_id: String = self.stringValue(term["id"])
                
                let title_grade_mo = NSEntityDescription.insertNewObject(forEntityName: GBTConstants.Entity.OVERALLGRADES, into: context)
                
                title_grade_mo.setValue("00_AA_AA_AA_01", forKey: "section_sort")
                title_grade_mo.setValue(term_name, forKey: "value")
                title_grade_mo.setValue("00_AA_AA_AA_01", forKey: "name")
                
                title_grade_mo.setValue(term_index, forKey: "index")
                term_index += 1
            }
            
        }
        
        self.insertOverAllAverage("Overall Average", section_sort: "00_AA_AA_AA_01", name: "00_AA_AA_AA_01", index: term_index, context: context)
        
        var index : Int = 0
        for ag in annual_grades {
            
            // STUDENT NAME
            var name: String = self.stringValue(ag["name"])
            name = name.trimmingCharacters(
                in: CharacterSet.whitespacesAndNewlines
            )
            let section_sort: String = "\(name)_\(index)"
            
            let studentPredicate = NSComparisonPredicate(keyPath: "section_sort", withValue: section_sort, isExact: true)
            let student_mo = self.db.retrieveEntity(GBTConstants.Entity.OVERALLGRADESSTUDENT, context: context, filter: studentPredicate)
            
            student_mo.setValue(name, forKey: "name")
            student_mo.setValue(section_sort, forKey: "section_sort")
            
            
            /// GRADES!
            let grades: [String:Any] = ag["grades"] as! [String:Any]
            
            //self.db.retrieveObjects(forEntity: GBTConstants.Entity.TERM, withFilter: predicate)
            
            var overAllIndex = 0
            if terms.count > 0 {
                for term in terms{

                    let term_id: String = self.stringValue(term["id"])
                    let term_grade = self.formatStringNumber(self.stringValue(grades[term_id]))
                    
                    let grade_mo = NSEntityDescription.insertNewObject(forEntityName: GBTConstants.Entity.OVERALLGRADES, into: context)
                    
                    grade_mo.setValue(section_sort, forKey: "section_sort")
                    grade_mo.setValue(term_grade, forKey: "value")
                    grade_mo.setValue(name, forKey: "name")
                    
                    grade_mo.setValue(overAllIndex, forKey: "index")
                    overAllIndex += 1
                }
                
            }
            
            let annual_grade: String = self.formatStringNumber(self.stringValue(ag["annual_grade"]))
            self.insertOverAllAverage(annual_grade, section_sort: section_sort, name: name, index: overAllIndex, context: context)
            index += 1
        }
        
    }
    
    func insertOverAllAverage(_ value:String, section_sort: String, name: String, index:Int, context:NSManagedObjectContext) {
        let mo = NSEntityDescription.insertNewObject(forEntityName: GBTConstants.Entity.OVERALLGRADES, into: context)
        
        mo.setValue(value, forKey: "value")
        mo.setValue(section_sort, forKey: "section_sort")
        
        mo.setValue(name, forKey: "name")
        
        //MANDATORY
        mo.setValue(index, forKey: "index")
    }
    
    func processComponents(_ components:[[String:AnyObject]]?, forTermID term_id:String, context:NSManagedObjectContext) {
        
        context.performAndWait {
            
//            print("components : \(components)")
            
            for item in components! {
                
                guard let c = item["component"] as? [String:AnyObject] else { continue }
                /*
                 "course_id": "11",
                 "component_id": "32",
                 "name": "Project X213",
                 "percentage": "40",
                 "for_testguru": "1"
                 */
                
                let course_id: String = self.stringValue(c["course_id"])
                let component_id: String = self.stringValue(c["component_id"])
                let name: String = self.stringValue(c["name"])
                let percentage: String = self.stringValue(c["percentage"])
                let for_testguru: String = self.stringValue(c["for_testguru"])
                let sorter = Int(component_id)
                
                let predicateComponentID = NSComparisonPredicate(keyPath: "component_id", withValue: component_id, isExact:true)
                let predicateTermID = NSComparisonPredicate(keyPath: "term_id", withValue: term_id, isExact:true)
                let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateComponentID,predicateTermID])
                let mo = self.db.retrieveEntity(GBTConstants.Entity.LEARNINGCOMPONENT, context: context, filter: compoundPredicate)
                
                mo.setValue(course_id, forKey: "course_id")
                mo.setValue(component_id, forKey: "component_id")
                mo.setValue(name, forKey: "name")
                mo.setValue(percentage, forKey: "percentage")
                mo.setValue(for_testguru, forKey: "for_testguru")
                mo.setValue(term_id, forKey: "term_id")
                mo.setValue(sorter, forKey: "sorter")
                
                ///////////////////////////
                // PROCESS TEST_LIST
                ///////////////////////////
                //                guard let test_list = item["test_list"] as? [[String:AnyObject]] else { continue }
                let test_list = item["test_list"] as? [[String:AnyObject]]
                self.processTestList(test_list, forTermID: term_id, componentid: component_id, for_testguru: for_testguru, context: context)
                
            }
        }
    }
    
    func processTestList(_ testlist:[[String:AnyObject]]?, forTermID term_id:String, componentid:String, for_testguru: String, context:NSManagedObjectContext) {
        
        let entity = GBTConstants.Entity.STUDENTGRADES
        
        context.performAndWait {
            
            //VERY IMPORTANT
            let sequence_name = "00_AA_AA_AA_01"
            let gender = "0_\(sequence_name)"
            var index = 0 // INDEX INCREMENT
            let info = GradeInfo(name: sequence_name, gender: gender, termid: term_id, componentid: componentid)
            
            if testlist != nil && (testlist?.count)! > 0 {
                for item in testlist! {
                    
                    /*
                     "deployment_id": "45",
                     "quiz_id": "58",
                     "cs_id": "12",
                     "name": "AP Test",
                     "quiz_type_id": "32",
                     "total_score": "3.00",
                     "result_type_id": "2",
                     "result_type": "Highest"
                     */
                    
                    let deployment_id = self.stringValue(item["deployment_id"])
                    let quiz_id = self.stringValue(item["quiz_id"])
                    let quiz_type_id = self.stringValue(item["quiz_type_id"])
                    let actual_result = self.stringValue(item["name"])
                    
                    //TODO VERY IMPORTANT
                    //                let result_type_id = self.stringValue(item["result_type_id"])//FOR CACHING POPUP
                    //                let result_type = self.stringValue(item["result_type"])//FOR CACHING POPUP
                    
                    let mo = NSEntityDescription.insertNewObject(forEntityName: entity, into: context)
                    mo.setValue(sequence_name, forKey: "name")
                    mo.setValue(sequence_name, forKey: "sequence_name")
                    mo.setValue(actual_result, forKey: "actual_result")
                    mo.setValue(quiz_id, forKey: "quiz_id")
                    mo.setValue(quiz_type_id, forKey: "quiz_type_id")
                    mo.setValue(deployment_id, forKey: "deployment_id")
                    mo.setValue(for_testguru, forKey: "for_testguru")
                    
                    //MANDATORY
                    mo.setValue(term_id, forKey: "term_id")
                    mo.setValue(componentid, forKey: "component_id")
                    mo.setValue("0", forKey: "is_grade") //FLAG
                    mo.setValue(gender, forKey: "gender")
                    mo.setValue(index, forKey: "index")
                    index = index + 1
                }
            }
            
            // TOTAL
            let total = "Total"
            self.insertActualResult(total, entity: entity, index: index, info: info, context: context)
            
            // PS
            index = index + 1
            let ps = "PS"
            self.insertActualResult(ps, entity: entity, index: index, info: info, context: context)
            
            // WS
            index = index + 1
            let ws = "WS"
            self.insertActualResult(ws, entity: entity, index: index, info: info, context: context)
        }
    }
    
    func processHighestScores(_ highestscores:[[String:AnyObject]]?, forTermID term_id:String, context:NSManagedObjectContext) {
        
        let entity = GBTConstants.Entity.STUDENTGRADES
        
        context.performAndWait {
            
            let sequence_name = "00_AA_AA_AA_02"
            let gender = "0_\(sequence_name)"
            
            
            for item in highestscores! {
                
                let componentid = self.stringValue(item["component_id"])
                
                let info = GradeInfo(name: sequence_name, gender: gender, termid: term_id, componentid: componentid!)
                
                //                guard let score_list = item["scores"] as? [[String:AnyObject]] else { print("empty scores") }
                
                let score_list = item["scores"] as? [[String:AnyObject]]
                
                var index = 0 // INDEX INCREMENT
                
                for s in score_list! {
                    
                    let actual_result = self.stringValue(s["score"])
                    let deployment_id = self.stringValue(s["deployment_id"])
                    
                    let mo = NSEntityDescription.insertNewObject(forEntityName: entity, into: context)
                    mo.setValue(sequence_name, forKey: "name")
                    mo.setValue(sequence_name, forKey: "sequence_name")
                    mo.setValue(actual_result, forKey: "actual_result")
                    mo.setValue(deployment_id, forKey: "deployment_id")
                    
                    //MANDATORY
                    mo.setValue(term_id, forKey: "term_id")
                    mo.setValue(componentid, forKey: "component_id")
                    mo.setValue("0", forKey: "is_grade") //FLAG
                    mo.setValue(gender, forKey: "gender")
                    mo.setValue(index, forKey: "index")
                    index = index + 1
                }
                
                let totaln = item["total"] as? Double
                let psn = item["ps"] as? Double
                let wsn = item["ws"] as? Double
                
                let totalv: String! = (totaln != nil) ? "\(totaln!)" : self.stringValue(item["total"])
                let psv: String! = (psn != nil) ? "\(psn!)" : self.stringValue(item["ps"])
                let wsv: String! = (wsn != nil) ? "\(wsn!)" : self.stringValue(item["ws"])
                
                let total: String! = self.stringValue(totalv)
                let ps: String! = self.formatStringNumber(self.stringValue(psv)) + "%"
                let ws: String! = self.formatStringNumber(self.stringValue(wsv)) + "%"
                
                
                // TOTAL
//                let total = self.stringValue(item["total"])
                self.insertActualResult(total!, entity: entity, index: index, info: info, context: context)
                
                // PS
                index = index + 1
//                let ps = self.stringValue(item["ps"]) + "%"
                self.insertActualResult(ps, entity: entity, index: index, info: info, context: context)
                
                // WS
                index = index + 1
//                let ws = self.stringValue(item["ws"]) + "%"
                self.insertActualResult(ws, entity: entity, index: index, info: info, context: context)
            }
        }
    }
    
    func processStudents(_ students:[[String:AnyObject]]?, forTermID term_id:String, context:NSManagedObjectContext) {
        
        let entity = GBTConstants.Entity.STUDENT
        
        context.performAndWait {
            
            for item in students! {
                
                /*
                 user_id
                 participant_id
                 cs_id
                 avatar
                 name
                 initial_grade
                 quarterly_grade
                 */
                
                guard let info = item["student_info"] as? [String:AnyObject] else { continue }
                
                let user_id: String = self.stringValue( info["user_id"] )
                let participant_id: String = self.stringValue( info["participant_id"] )
                let cs_id: String = self.stringValue( info["cs_id"] )
                let avatar: String = self.stringValue( info["AVATAR"] )
                var name: String! = self.stringValue( info["NAME"] )
                var gender: String = self.stringValue( info["gender"] )
                
                name = name.trimmingCharacters(
                    in: CharacterSet.whitespacesAndNewlines
                )
                
                let initial_grade = self.stringValue( item["initial_grade"] )
                let quarterly_grade = self.stringValue( item["quarterly_grade"] )
                
                
                //                    print("user_id : \(user_id)")
                //                    print("participant_id : \(participant_id)")
                //                    print("cs_id : \(cs_id)")
                //                    print("avatar : \(avatar)")
                //                    print("name : \(name)")
                //                    print("initial_grade : \(initial_grade)")
                //                    print("quarterly_grade : \(quarterly_grade)")
                //                    print("--------------------")
                
                let predicateUserID = NSComparisonPredicate(keyPath: "user_id", withValue: user_id, isExact:true)
                let predicateTermID = NSComparisonPredicate(keyPath: "term_id", withValue: term_id, isExact:true)
                let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateUserID,predicateTermID])
                let mo = self.db.retrieveEntity(entity, context: context, filter: compoundPredicate)
                
                mo.setValue(user_id, forKey: "user_id")
                mo.setValue(participant_id, forKey: "participant_id")
                mo.setValue(cs_id, forKey: "cs_id")
                mo.setValue(avatar, forKey: "avatar")
                mo.setValue(name, forKey: "name")
                mo.setValue(initial_grade, forKey: "initial_grade")
                mo.setValue(quarterly_grade, forKey: "quarterly_grade")
                mo.setValue(term_id, forKey: "term_id")
                
                ///////////////////////////
                //TODO GENDER HARD CODING
                ///////////////////////////
                if gender == "" {
                    gender = "F"
                }
                
                //NOTE: ADDED PARTICIPANT ID IN NAME TO FIX ISSUE WITH STUDENTS WITH EXACTLY THE SAME NAME.
                let nameWithParticipantID = "\(name)_\(participant_id)"
                let sortGender = (gender == "F") ? "2_\(nameWithParticipantID)" : "1_\(nameWithParticipantID)"
                mo.setValue(sortGender, forKey: "gender")
                
                //                self.db.displayRecords(entity)
                
                
                ///////////////////////////
                // STUDENT GRADE PARSING
                ///////////////////////////
                //                guard let student_grades = item["student_grades"] as? [[String:AnyObject]] else { continue }
                let student_grades = item["student_grades"] as? [[String:AnyObject]]
                self.processStudentGrades(student_grades, forStudent:name, withParticipantID: participant_id, gender:sortGender, forTermID: term_id, context: context)
            }
        }
    }
    
    func processStudentGrades(_ student_grades:[[String:AnyObject]]?, forStudent name:String, withParticipantID participant_id: String, gender:String, forTermID term_id:String, context:NSManagedObjectContext) {
        
        let entity = GBTConstants.Entity.STUDENTGRADES
        
        context.performAndWait {
            
            for item in student_grades! {
                
                guard let grades = item["grades"] as? [[String:AnyObject]] else { continue }
                
                /*
                 "id": "64",
                 "actual_result": "3.00",
                 "expected_result": "3.00",
                 "remarks": "Quiz taken: 2016-05-23 03:42:00",
                 "deployment_id": "45",
                 "type_id": "32",
                 "quiz_id": "58",
                 "for_testguru": "1"
                 */
                
                let component_id = self.stringValue( item["component_id"] )
                
                // INDEX INCREMENT
                var index = 0
                
                //NOTE: ADDED PARTICIPANT ID IN NAME TO FIX ISSUE WITH STUDENTS WITH EXACTLY THE SAME NAME.
                let info = GradeInfo(name: "\(name)_\(participant_id)", gender: gender, termid: term_id, componentid: component_id!)
                
                for g in grades {
                    
                    let grade_id: String = self.stringValue(g["id"])
                    var actual_result: String = self.stringValue(g["actual_result"])
                    let expected_result: String = self.stringValue(g["expected_result"])
                    let remarks: String = self.stringValue(g["remarks"])
                    let deployment_id: String = self.stringValue(g["deployment_id"])
                    let type_id: String = self.stringValue(g["type_id"])
                    let quiz_id: String = self.stringValue(g["quiz_id"])
                    let for_testguru: String = self.stringValue(g["for_testguru"])
                    
                    actual_result = (actual_result == "") ? "0" : actual_result
                    
                    //                    let predicate = NSComparisonPredicate(keyPath: "id", withValue: grade_id, isExact:true)
                    let mo = NSEntityDescription.insertNewObject(forEntityName: entity, into: context)//self.db.retrieveEntity(entity, context: context, filter: predicate)
                    
                    mo.setValue(grade_id, forKey: "id")
                    
                    mo.setValue(expected_result, forKey: "expected_result")
                    mo.setValue(remarks, forKey: "remarks")
                    mo.setValue(deployment_id, forKey: "deployment_id")
                    mo.setValue(type_id, forKey: "type_id")
                    mo.setValue(quiz_id, forKey: "quiz_id")
                    mo.setValue(for_testguru, forKey: "for_testguru")
                    
                    mo.setValue(actual_result, forKey: "actual_result")
                    
                    //MANDATORY
                    mo.setValue(index, forKey: "index")
                    
                    //NOTE: ADDED PARTICIPANT ID IN NAME TO FIX ISSUE WITH STUDENTS WITH EXACTLY THE SAME NAME.
                    mo.setValue("\(name)_\(participant_id)", forKey: "name")
                    
                    mo.setValue(participant_id, forKey: "participant_id")
                    
                    //SEQUENCE
                    //mo.setValue(name, forKey: "section_name")
                    
                    mo.setValue(gender, forKey: "gender")
                    mo.setValue(term_id, forKey: "term_id")
                    mo.setValue(component_id, forKey: "component_id")
                    
                    //FLAG
                    mo.setValue("1", forKey: "is_grade")
                    
                    //INCREMENT INDEX
                    index = index + 1
                }
                
                let totaln = item["total"] as? Double
                let psn = item["ps"] as? Double
                let wsn = item["ws"] as? Double
                
                let totalv: String! = (totaln != nil) ? "\(totaln!)" : self.stringValue(item["total"])
                let psv: String! = (psn != nil) ? "\(psn!)" : self.stringValue(item["ps"])
                let wsv: String! = (wsn != nil) ? "\(wsn!)" : self.stringValue(item["ws"])
                
                let total: String! = self.stringValue(totalv)
                let ps: String! = self.formatStringNumber(self.stringValue(psv)) + "%"
                let ws: String! = self.formatStringNumber(self.stringValue(wsv)) + "%"

                
                // TOTAL
                index = index + 1
                self.insertActualResult(total!, entity: entity, index: index, info: info, context: context)
                
                // PS
                index = index + 1
                self.insertActualResult(ps, entity: entity, index: index, info: info, context: context)
                
                // WS
                index = index + 1
                self.insertActualResult(ws, entity: entity, index: index, info: info, context: context)
            }
        }
    }
    
    func insertActualResult(_ value:String, entity:String, index:Int, info:GradeInfo, context:NSManagedObjectContext) {
        let mo = NSEntityDescription.insertNewObject(forEntityName: entity, into: context)
        
        mo.setValue(value, forKey: "actual_result")
        mo.setValue(index, forKey: "index")
        
        //MANDATORY
        mo.setValue(index, forKey: "index")
        mo.setValue(info.name, forKey: "name")
        
        //SECTION_NAME
        mo.setValue(info.name, forKey: "section_name")
        
        mo.setValue(info.gender, forKey: "gender")
        mo.setValue(info.termid, forKey: "term_id")
        mo.setValue(info.componentid, forKey: "component_id")
        
        //FLAG
        mo.setValue("0", forKey: "is_grade")
    }
    
    func fetchComponent(forTermID term_id:String) -> (componentIDs: [String], componentData: [[String:String?]]) {
        
        let predicate = NSComparisonPredicate(keyPath: "term_id", withValue: term_id, isExact: true)
        let learningComponents = self.db.retrieveEntity(GBTConstants.Entity.LEARNINGCOMPONENT, properties: ["component_id","name", "for_testguru"], filter: predicate, sortKey: "sorter")//self.db.retrieveObjects(forEntity: GBTConstants.Entity.TERM, withFilter: predicate)
        
        guard learningComponents != nil else {
            return (componentIDs: [String](), componentData: [[String:String!]]())
        }
        
        
        var componentIDs = [String]()
        var componentData = [[String:String!]]()
        
        for component in learningComponents! {
            let c = component as! [String:AnyObject]
            
            let c_id = self.stringValue(c["component_id"])
            componentIDs.append(c_id!)
            
            let c_name: String = self.stringValue(c["name"])
            let c_for_testguru: String = self.stringValue(c["for_testguru"])
            
            let dict: [String:String] = ["name": c_name,
                        "for_testguru": c_for_testguru]
            
            componentData.append(dict)
            
        }
        
        return (componentIDs: componentIDs, componentData: componentData)
    }
    
        func fetchStudent(_ participant:String) -> [String:AnyObject] {
     
        let predicate = NSComparisonPredicate(keyPath: "participant_id", withValue: participant as AnyObject, isExact: true)
        
        let properties = ["user_id", "participant_id", "cs_id"] as [String]
        
        let students = self.db.retrieveEntity(GBTConstants.Entity.STUDENT, properties: properties, filter: predicate, sortKey: nil)
        
        guard students != nil else {
            return [String:AnyObject]()
        }
        
        return students!.last as! [String:AnyObject]
    }
    
    
    func fetchTerms(forSectionID section_id:String) -> [[String:String]] {
        let predicate = NSComparisonPredicate(keyPath: "section_id", withValue: section_id, isExact: true)
        let terms = self.db.retrieveEntity(GBTConstants.Entity.TERM, properties: ["id","name"], filter: predicate, sortKey: "date_start")//self.db.retrieveObjects(forEntity: GBTConstants.Entity.TERM, withFilter: predicate)
        
        guard terms != nil else {
            return [[String:String]]()
        }
        
        return terms as! [[String:String]]
    }
    
    func fetchComponentName(forComponentID component_id:String, inTermID term_id: String, completion: GBListBlock) {
        let p1 = NSComparisonPredicate(keyPath: "component_id", withValue: component_id, isExact: true)
        let p2 = NSComparisonPredicate(keyPath: "term_id", withValue: term_id, isExact: true)
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [p1, p2])
        let component = self.db.retrieveEntity(GBTConstants.Entity.LEARNINGCOMPONENT, properties: ["name"], filter: predicate, sortKey: "sorter")//self.db.retrieveObjects(forEntity: GBTConstants.Entity.TERM, withFilter: predicate)
        
        completion(component as? [[String:AnyObject]])
    }
    
    func fetchTestName(forComponentID component_id:String, inTermID term_id: String, withIndex index: Double, completion: GBListBlock) {
        let p1 = NSComparisonPredicate(keyPath: "component_id", withValue: component_id, isExact: true)
        let p2 = NSComparisonPredicate(keyPath: "term_id", withValue: term_id, isExact: true)
        let p3 = NSComparisonPredicate(keyPath: "index", withValue: index, isExact: true)
        let p4 = NSComparisonPredicate(keyPath: "name", withValue: "00_AA_AA_AA_01", isExact: true)
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [p1, p2, p3, p4])
        let component = self.db.retrieveEntity(GBTConstants.Entity.STUDENTGRADES, properties: ["actual_result"], filter: predicate, sortKey: nil)//self.db.retrieveObjects(forEntity: GBTConstants.Entity.TERM, withFilter: predicate)
        
        completion(component as? [[String:AnyObject]])
    }
    
    func fetchHighestScore(forComponentID component_id:String, inTermID term_id: String, withIndex index: Double, completion: GBListBlock) {
        let p1 = NSComparisonPredicate(keyPath: "component_id", withValue: component_id, isExact: true)
        let p2 = NSComparisonPredicate(keyPath: "term_id", withValue: term_id, isExact: true)
        let p3 = NSComparisonPredicate(keyPath: "index", withValue: index, isExact: true)
        let p4 = NSComparisonPredicate(keyPath: "name", withValue: "00_AA_AA_AA_02", isExact: true)
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [p1, p2, p3, p4])
        let component = self.db.retrieveEntity(GBTConstants.Entity.STUDENTGRADES, properties: ["actual_result", "name", "component_id", "term_id", "index"], filter: predicate, sortKey: nil)
        
        completion(component as? [[String:AnyObject]])
    }
    
    func fetchEditData(withPredicate predicate: NSPredicate, completion: GBListBlock) {
        let component = self.db.retrieveEntity(GBTConstants.Entity.STUDENTGRADES, properties: ["name", "actual_result", "quiz_id", "participant_id"], filter: predicate, sortKey: "name")//self.db.retrieveObjects(forEntity: GBTConstants.Entity.TERM, withFilter: predicate)
        print("HELLO ITO ~ \(component)")
        completion(component as? [[String:AnyObject]])
    }
    
    func formatStringNumber(_ stringNumber: String) -> String {
        var number = stringNumber
        
        let floatNumber = Float(stringNumber)
        
        if floatNumber != nil {
            let intNumber = Int(floatNumber!)
            
            let floatNumberString = self.stringValue(floatNumber)
            let intNumberString = self.stringValue(intNumber)
            
            if floatNumberString == intNumberString {
                number = intNumberString!
            } else {
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = NumberFormatter.Style.decimal
                numberFormatter.maximumFractionDigits = 2
                //                numberFormatter.roundingMode = NSNumberFormatterRoundingMode.RoundDown
                
                number = numberFormatter.string(from: NSNumber(value: floatNumber! as Float))!
            }
        }
        
        return number
    }
    
    func retrieveDefaultValue(_ entity:String, properties:[String], key:String) -> [String:AnyObject] {
        let list = self.db.retrieveEntity(entity, properties: properties, filter: nil, sortKey: key) as [AnyObject]!
        let value = list?.first as! [String:AnyObject]
        return value
    }
    
    // MARK: POST Operations
    
    ////////////////
    // CREATE TEST
    ////////////////
    func requestCreateGradeBookEntry(_ name:String, score:String, csid:String, termid:String, componentid:String, handler:@escaping GBDoneBlockWithError) {
        
        /*
         {
         "name": "TITLE_118142-150640-124657_RRM",
         "passing_rate": 50,
         "total_score" : 50,
         "course_period_id" : 1,
         "question_ids": [],
         "is_shuffle_answers": 1,
         "is_forced_complete": 0,
         "general_instruction": "INSTRUCTIONS_118142-150640-124657",
         "attempts": 0,
         "quiz_type_id" : 1,
         "result_type_id": 3,
         "show_feedbacks": 0,
         "password": "",
         "allow_review": 0,
         "is_shuffle_questions": 1,
         "show_correct_answers": 0,
         "show_score": 1,
         "show_result": 0,
         "is_graded": 1
         
         "time_limit": "00:00:00",
         "date_open": "2016-03-09 09:03:30",
         "date_close": "2016-03-10 09:03:30",
         
         }*/
        
        let question_ids: [String] = []
        let is_shuffle_answers = "1"
        let is_forced_complete = "0"
        let general_instruction = ""
        let attempts = "0"
        let quiz_type_id = componentid //
        let result_type_id = "3"//DEFAULT VALUE "LATEST"
        let show_feedbacks = "0"
        let password = ""
        let allow_review = "0"
        let is_shuffle_questions = "1"
        let show_correct_answers = "0"
        let show_score = "1"
        let show_result = "0"
        let is_graded = "1"
        let time_limit = "00:00:00"
        let object = dateOpenAndClose()
        let date_open = "\(object.open) \(time_limit)"
        let date_close = "\(object.close) \(time_limit)"
        let course_id = self.stringValue(self.fetchUserDefaultsObject(forKey: GBTConstants.UserDefined.GBTCOURSEID))//self.gbdm.save(object: course_id, forKey: GBTConstants.UserDefined.GBTCOURSEID)
        
        let body:[String:AnyObject] = [
                                       "name":name as AnyObject,
                                       "passing_rate":score as AnyObject,
                                       "total_score":score as AnyObject,
                                       "course_period_id":termid as AnyObject,
                                       "question_ids":question_ids as AnyObject,
                                       "is_shuffle_answers":is_shuffle_answers as AnyObject,
                                       "is_forced_complete":is_forced_complete as AnyObject,
                                       "general_instruction":general_instruction as AnyObject,
                                       "attempts":attempts as AnyObject,
                                       "quiz_type_id":quiz_type_id as AnyObject,
                                       "result_type_id":result_type_id as AnyObject,
                                       "show_feedbacks":show_feedbacks as AnyObject,
                                       "password":password as AnyObject,
                                       "allow_review":allow_review as AnyObject,
                                       "is_shuffle_questions": is_shuffle_questions as AnyObject,
                                       "show_correct_answers": show_correct_answers as AnyObject,
                                       "show_score": show_score as AnyObject,
                                       "show_result": show_result as AnyObject,
                                       "is_graded": is_graded as AnyObject,
                                       "time_limit": time_limit as AnyObject,
                                       "date_open": date_open as AnyObject,
                                       "date_close": date_close as AnyObject,
                                       "course_id": course_id as AnyObject
        ]
        
        print("----> body : \(body)")
        
        // USER ID
        let userid = accountUserID()
        
        // ENDPOINT
        let endpoint = "/v2/grades/new/quiz/user/\(userid)"//"/v2/testbank/new/user/\(userid)"
        
        // REQUEST
        let request = route("POST", uri: endpoint, body: body)
        
        // SESSION
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                handler(false, e.localizedDescription)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { handler(false, "Something went wrong")
                return }
            print("CREATE ITO response dictionary --> \(dictionary)")
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [String:AnyObject] else {
                    handler(false, "Something went wrong")
                    return
                }
                
                let userData = records["data"] as! [String:AnyObject]
                
                let quiz_id = self.stringValue(userData["id"])
                
                self.requestDeploy(quiz_id!, csid:csid, handler: handler)
            }
        }) 
        
        task.resume()
    }
    
    /////////////////////
    // DEPLOY TEST
    /////////////////////
    
    func requestDeploy(_ quizid:String, csid:String, handler:@escaping GBDoneBlockWithError) {
        
        let course_section_ids:[String] = [csid]
        
        let body:[String:AnyObject] = ["course_section_ids":course_section_ids as AnyObject]
        
        print("----> body : \(body)")
        
        // ENDPOINT
        let endpoint = "/v2/testbank/deploy/\(quizid)"
        
        // REQUEST
        let request = route("POST", uri: endpoint, body: body)
        
        // SESSION
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                handler(false, e.localizedDescription)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            print("DEPLOY ITO response dictionary --> \(dictionary)")
            
            handler(false, nil)
        }) 
        
        task.resume()
    }
    
    /////////////////////
    // UPDATE ADD SCORE
    /////////////////////
    func requestScoreUpdate(_ termid:String, handler:@escaping GBDoneBlock) {
        
        let entity = GBTConstants.Entity.SCORE
        let properties = ["participant_id","qid","score"]
        let filter = NSComparisonPredicate(keyPath: "termid", withValue: termid, isExact: true)
        let key = "qid"
        
        let list = self.db.retrieveEntity(entity, properties: properties, filter: filter, sortKey: key) as [AnyObject]!
        
        if (list?.count)! > 0 {
            
            let body:[String:AnyObject] = ["user_scores":list as AnyObject]
            print("----> body : \(body)")
            
            // ENDPOINT
            let endpoint = "/v2/quizzes/course/assessment/"
            
            // REQUEST
            let request = route("POST", uri: endpoint, body: body)
            
            // SESSION
            let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
                
                if let e = error {
                    print(e.localizedDescription)
                    handler(false)
                }
                
                guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
                print("response dictionary : %@", dictionary)
            }) 
            
            task.resume()
        }
    }
    
    func processTerms(_ terms:[[String:AnyObject]], context:NSManagedObjectContext) {
        
        let entity = GBTConstants.Entity.TERM
        
        for term in terms {
            
            let term_id = self.stringValue(term["id"])
            let term_name = self.stringValue(term["name"])
            let date_start = self.stringValue(term["date_start"])
            let date_end = self.stringValue(term["date_end"])
            let is_locked = self.stringValue(term["locked"])
            
            let predicate = NSComparisonPredicate(keyPath: "id", withValue: term_id, isExact: true)
            let mo = self.db.retrieveEntity(entity, context: context, filter: predicate)
            
            mo.setValue(term_id, forKey: "id")
            mo.setValue(term_name, forKey: "name")
            mo.setValue(date_start, forKey: "date_start")
            mo.setValue(date_end, forKey: "date_end")
            mo.setValue(is_locked, forKey: "is_locked")
        }
    }
    
//    func processCourses(courses:[[String:AnyObject]], context:NSManagedObjectContext) {
//        
//        let entity = GBTConstants.Entity.GRADESUMMARYGRADE
//        
//        for course in courses {
//            
//            let term_id = self.stringValue(course["cs_id"])
//            let term_name = self.stringValue(course["name"])
//            let date_start = self.stringValue(course["sys_id"])
//            
//            let predicate = NSComparisonPredicate(keyPath: "id", withValue: term_id, isExact: true)
//            let mo = self.db.retrieveEntity(entity, context: context, filter: predicate)
//        }
//    }
    
    func fetchTermIDForName(_ name:String) -> String {
        
        let predicate = NSComparisonPredicate(keyPath: "name", withValue: name, isExact: true)
        let term_mo = self.db.retrieveEntity(GBTConstants.Entity.TERM, filter: predicate )!
        let term_id = term_mo.value(forKey: "id") as! String
        
        return term_id
    }
    
    func requestSummary(forSectionID section_id: String, handler:@escaping GBDoneBlockWithError) {
        
        // ENDPOINT
        let endpoint = "/v2/quizzes/grades/summary/class/\(section_id)?sort_by="
        // REQUEST
        let request = route("GET", uri:endpoint, query:nil, body: nil)
        
        let ctx = self.getContext()
        _ = self.db.clearEntity(GBTConstants.Entity.GRADESUMMARYGRADE, ctx: ctx!, filter: nil)
        _ = self.db.clearEntity(GBTConstants.Entity.TERM, ctx: ctx!, filter: nil)
        _ = self.db.clearEntity(GBTConstants.Entity.GRADESUMMARYSTUDENT, ctx: ctx!, filter: nil)
        
        // SESSION
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                handler(false, e.localizedDescription)
            }
            
//            let bundle = NSBundle.mainBundle()
//            let path = bundle.pathForResource("summary", ofType: "json")
//            let content = NSData(contentsOfFile: path!)
//            guard let d = content, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }// dummy data
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else {
                handler(false, "Invalid response data")
                return
            }
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [String:AnyObject] else {
                    handler(false, "Invalid response data")
                    return
                }
                
                ctx?.performAndWait({
                    
                    //CHECKED
                    if let term_courses = records["term_courses"] as? [String:AnyObject] {
                        let terms = term_courses["terms"] as! [[String:AnyObject]]
                        self.processTerms(terms, context: ctx!)
                        
                        //process
//                        let courses = term_courses["courses"] as! [[String:AnyObject]]
//                        self.processCourses(courses, context: ctx)
                    }
                    
                    //CHECKED
                    let final_grades = records["final_grades"] as? [[String:AnyObject]]
                    guard final_grades != nil else {
                        handler(false, "Invalid response data")
                        return
                    }
                    
                    //CHECKED
                    let term_grades = records["term_grades"] as? [[String:AnyObject]]
                    guard term_grades != nil else {
                        handler(false, "Invalid response data")
                        return
                    }
                    
                    //--------------------------------------------------------------------

                    // PARSE GRADES BY QUARTER
                    self.parseQuarterlyGrades(forTermList: term_grades!, final_grades: final_grades!, ctx: ctx!)

                    // PARSE GRADES BY SUBJECT
                    self.parseSubjects(forTermList: term_grades!, ctx: ctx!)
                    
                })
                let success = self.db.saveObjectContext(ctx!)
                handler(success, nil)
            }
            else {
                let records = dictionary["records"] as! [String:AnyObject]
                let message: String! = self.stringValue(records["user_message"])
                handler(false, message)
            }
        }) 
        
        task.resume()
    }
    
    func parseQuarterlyGrades(forTermList term_grades:[[String:AnyObject]], final_grades:[[String:AnyObject]], ctx:NSManagedObjectContext) {
        
        let entity = GBTConstants.Entity.GRADESUMMARYGRADE
        
        // COLUMN INDEX
        var column_index = 0
        
        let term_section_key: String = "AA_AA_AA_AA_00"
        let term_gender_section: String = "00_\(term_section_key)"
        let term_id_section_key: String = "00_\(term_section_key)"
        let for_quarter: String = "1"
        
        for term_grade in term_grades {
            
            let term_name = self.stringValue(term_grade["term_name"])
            let term_id = self.fetchTermIDForName(term_name!)//THIS WILL BE USE LATER
            
            let mo = NSEntityDescription.insertNewObject(forEntityName: entity, into: ctx)
            
            mo.setValue(column_index, forKey: "index")
            mo.setValue(term_section_key, forKey: "name")
            mo.setValue(term_name, forKey: "quarterly_grade")
            mo.setValue(term_gender_section, forKey: "gender")
            mo.setValue(for_quarter, forKey: "for_quarter")
            
            if let students = term_grade["students"] as? [[String:AnyObject]] {
                
                let last_index = term_grades.count
                
                self.processSummaryStudent(students, column_index:column_index, term_id: term_id, final_grades:final_grades, last_index:last_index, context: ctx)
            }
            
            column_index += 1
        }
        
        // STATIC INSERT FOR AVERAGE
        let info = TermInfo(studentName: term_section_key, gender: "00_\(term_section_key)", termid:term_id_section_key, for_quarter: for_quarter)
        self.insertStaticData("Average", entity: entity, index: column_index, info: info, context: ctx)
    }
    
    func parseSubjects(forTermList term_grades:[[String:AnyObject]], ctx:NSManagedObjectContext) {
        
        let entity = GBTConstants.Entity.GRADESUMMARYSTUDENT
        
        for term_grade in term_grades {
            
            let term_name = self.stringValue(term_grade["term_name"])
            let term_id = self.fetchTermIDForName(term_name!)//THIS WILL BE USE LATER
            
            if let students = term_grade["students"] as? [[String:AnyObject]] {
                
                let for_quarterly: String = "0" //VERY IMPORTANT DO NOT CHANGE
                
                for student in students {
                    
                    let student_info = student["student_info"] as! [String:AnyObject]
                    
                    let student_id: String = self.stringValue(student_info["id"])
                    let last_name: String = self.stringValue(student_info["last_name"])
                    let first_name: String = self.stringValue(student_info["first_name"])
                    let student_name: String = "\(last_name), \(first_name)"
//                    student_name = "\(student_id)-\(student_name)"
                    var gender: String! = self.stringValue(student_info["gender"])
                    
                    let p1 = NSComparisonPredicate(keyPath: "id", withValue: student_id, isExact: true)
                    let p2 = NSComparisonPredicate(keyPath: "for_quarter", withValue: for_quarterly, isExact: true)
                    let p3 = NSComparisonPredicate(keyPath: "term_id", withValue: term_id, isExact: true)
                    let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [p1, p2, p3])
                    let mo = self.db.retrieveEntity(entity, context: ctx, filter: predicate)
                    
                    mo.setValue(student_id, forKey: "id")
                    mo.setValue(student_name, forKey: "name")
                    
                    //NOTE: ADDED STUDENT IN NAME TO FIX ISSUE WITH STUDENTS WITH EXACTLY THE SAME NAME.
                    let nameWithStudentID = "\(student_name)_\(student_id)"
                    gender = (gender == "M") ? "01_\(nameWithStudentID)" : "02_\(nameWithStudentID)"
                    
                    mo.setValue(gender, forKey: "gender")

//                    let term_id_section = "01_\(student_name)"//NOTE:Male Student

                    //FOR QUARTERLY FLAG
                    mo.setValue(term_id, forKey: "term_id")
                    mo.setValue(for_quarterly, forKey: "for_quarter")
                    
                    // PROCESS STUDENT GRADES
                    let student_grades = student["grades"] as! [[String:AnyObject]]
                    let student_average = self.stringValue(student["average"])
                    
                    //NOTE: ADDED STUDENT ID IN NAME TO FIX ISSUE WITH STUDENTS WITH EXACTLY THE SAME NAME.
                    let info = TermInfo(studentName: nameWithStudentID, gender: gender, termid: term_id, for_quarter: for_quarterly)
                    self.processStudentGrades(forGrades: student_grades, average: student_average!, studentInfo:info, context: ctx)
                }
            }
        }
    }
    
    func processStudentGrades(forGrades grades:[[String:AnyObject]], average:String, studentInfo:TermInfo, context:NSManagedObjectContext) {
        
        let entity = GBTConstants.Entity.GRADESUMMARYGRADE
        
        let term_section_key = "AA_AA_AA_AA_00"
        let term_gender_section = "00_\(term_section_key)"
        
        
        // COLUMN INDEX
        var column_index = 0
        
        let termid = studentInfo.termid
        let gender = studentInfo.gender
        let name = studentInfo.studentName
        let for_quarterly = studentInfo.for_quarter
        
        for grade in grades {

            // HEADER PARSING
            
            let course_name = self.stringValue(grade["course_name"])
            
            let p1 = NSComparisonPredicate(keyPath: "term_id", withValue: termid, isExact: true)
            let p2 = NSComparisonPredicate(keyPath: "for_quarter", withValue: for_quarterly, isExact: true)
            let p3 = NSComparisonPredicate(keyPath: "quarterly_grade", withValue: course_name, isExact: true)
            let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [p1,p2,p3])
            let mo = self.db.retrieveEntity(entity, context: context, filter: predicate)
            
            mo.setValue(column_index, forKey: "index")
            mo.setValue(term_gender_section, forKey: "gender")
            mo.setValue(term_section_key, forKey: "name")
            mo.setValue(course_name, forKey: "quarterly_grade")
            mo.setValue(termid, forKey: "term_id")
            mo.setValue(for_quarterly, forKey: "for_quarter")


            // GRADES PARSING
            let quarterly_grade = self.stringValue(grade["quarterly_grade"])
//            let initial_grade = self.stringValue(grade["initial_grade"])
            
            let grade_mo = NSEntityDescription.insertNewObject(forEntityName: entity, into: context)
            
            grade_mo.setValue(column_index, forKey: "index")
            grade_mo.setValue(gender, forKey: "gender")
            grade_mo.setValue(termid, forKey: "term_id")
            grade_mo.setValue(name, forKey: "name")
            grade_mo.setValue(quarterly_grade, forKey: "quarterly_grade")
            grade_mo.setValue(for_quarterly, forKey: "for_quarter")
            
            column_index += 1
        }
        
        
        // INSERT AVERAGE HEADER
        let infoAverageHeader = TermInfo(studentName: term_section_key, gender: term_gender_section, termid:termid, for_quarter: for_quarterly)
//        self.insertStaticData("Average", entity: entity, index: column_index, info: infoAverageHeader, context: context)
        let p1 = NSComparisonPredicate(keyPath: "term_id", withValue: termid, isExact: true)
        let p2 = NSComparisonPredicate(keyPath: "for_quarter", withValue: for_quarterly, isExact: true)
        let p3 = NSComparisonPredicate(keyPath: "quarterly_grade", withValue: "Average", isExact: true)
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [p1,p2,p3])
        let ah_mo = self.db.retrieveEntity(entity, context: context, filter: predicate)
        
        ah_mo.setValue(column_index, forKey: "index")
        ah_mo.setValue(infoAverageHeader.studentName, forKey: "name")
        ah_mo.setValue(infoAverageHeader.termid, forKey: "term_id")
        ah_mo.setValue(infoAverageHeader.gender, forKey: "gender")
        ah_mo.setValue(infoAverageHeader.for_quarter, forKey: "for_quarter")
        ah_mo.setValue("Average", forKey: "quarterly_grade")
        
        // INSERT AVERAGE VALUE
        let infoAverageValue = TermInfo(studentName: name, gender: gender, termid:termid, for_quarter: for_quarterly)
        self.insertStaticData(average, entity: entity, index: column_index, info: infoAverageValue, context: context)
    }
    
    func insertStaticData(_ value:String, entity:String, index:Int, info:TermInfo, context:NSManagedObjectContext) {
        
        let mo = NSEntityDescription.insertNewObject(forEntityName: entity, into: context)
        
        mo.setValue(index, forKey: "index")
        mo.setValue(info.studentName, forKey: "name")
        mo.setValue(info.termid, forKey: "term_id")
        mo.setValue(info.gender, forKey: "gender")
        mo.setValue(info.for_quarter, forKey: "for_quarter")
        mo.setValue(value, forKey: "quarterly_grade")
    }
    
    func processSummaryStudent(_ students:[[String:AnyObject]], column_index:Int, term_id: String, final_grades:[[String:AnyObject]], last_index:Int, context:NSManagedObjectContext) {
        
        let entity = GBTConstants.Entity.GRADESUMMARYSTUDENT
        let for_quarterly:String = "1"
        
        for student in students {
            
            let student_info = student["student_info"] as! [String:AnyObject]
            
            let student_id: String = self.stringValue(student_info["id"])
            let last_name: String = self.stringValue(student_info["last_name"])
            let first_name: String = self.stringValue(student_info["first_name"])
            let student_name: String = "\(last_name), \(first_name)"
            var gender: String = self.stringValue(student_info["gender"])
            gender = (gender == "M") ? "01_\(student_name)" : "02_\(student_name)"
            
            let p1 = NSComparisonPredicate(keyPath: "id", withValue: student_id, isExact: true)
            let p2 = NSComparisonPredicate(keyPath: "for_quarter", withValue: for_quarterly, isExact: true)
            let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [p1, p2])
            let mo = self.db.retrieveEntity(entity, context: context, filter: predicate)
            
            mo.setValue(student_id, forKey: "id")
            mo.setValue(student_name, forKey: "name")
            
            //TODO
//            var gender = "01_\(student_name)"//NOTE:Male Student
//            if student_name.containsString("1") || student_name.containsString("3") || student_name.containsString("5") {
//                gender = "02_\(student_name)"
//            }
            
            let term_id_section = "01_\(student_name)"//NOTE:Male Student
            mo.setValue(gender, forKey: "gender")
            
            //FOR QUARTERLY FLAG
            mo.setValue(term_id, forKey: "term_id")
            mo.setValue(for_quarterly, forKey: "for_quarter")

            //NOTE: ADDED STUDENT ID IN NAME TO FIX ISSUE WITH STUDENTS WITH EXACTLY THE SAME NAME.
            let nameWithStudentID: String = "\(student_name)_\(student_id)"
            
            //////////////////////////////////////////////////////////////////////////////////
            // INSERT STUDENT AVERAGE
            let average = self.stringValue( student["average"] )
            let info = TermInfo(studentName: nameWithStudentID, gender: gender, termid:term_id_section, for_quarter: "1")
            self.insertStaticData(average!, entity: GBTConstants.Entity.GRADESUMMARYGRADE,
                                  index: column_index,
                                  info: info,
                                  context: context)
            
            //////////////////////////////////////////////////////////////////////////////////
            // INSERT STUDENT FINAL AVERAGE
            let grade = final_grades.filter({ $0["id"]?.description == student_id }).last//[student_id] as! [String:AnyObject]
            let final_average = self.stringValue(grade!["average"])
            let finalAverageInfo = TermInfo(studentName: nameWithStudentID, gender: gender, termid:term_id_section, for_quarter: "1")
            self.insertStaticData(final_average!, entity: GBTConstants.Entity.GRADESUMMARYGRADE,
                                  index: last_index,
                                  info: finalAverageInfo,
                                  context: context)
            
            //////////////////////////////////////////////////////////////////////////////////
        }
    }
    
//    func processStudentGrades(grades grades:[[String:AnyObject]]?, withName name: String, withGender gender: String, forTermID term_id: String, final_average: String, context:NSManagedObjectContext) {
//        
//        let entity = GBTConstants.Entity.GRADESUMMARYGRADE
//        
//        if grades != nil {
//            
//            var column_index = 0
//            for grade in grades! {
//                let course_name = self.stringValue(grade["course_name"])
////                let initial_grade = self.stringValue(grade["initial_grade"])
//                let quarterly_grade = self.stringValue(grade["quarterly_grade"])
//                
//                let mo = NSEntityDescription.insertNewObjectForEntityForName(entity, inManagedObjectContext: context)
//                
//                mo.setValue(course_name, forKey: "course_name")
////                mo.setValue(initial_grade, forKey: "initial_grade")
//                mo.setValue(quarterly_grade, forKey: "quarterly_grade")
//                mo.setValue(name, forKey: "student_name")
//                mo.setValue(column_index, forKey: "index")
//                
//                mo.setValue("0", forKey: "for_quarter") // FOR TAB (QUARTER | SUBJECT)
//                mo.setValue(term_id, forKey: "term_id") // FOR FILTERING
//                
//                column_index = column_index + 1
//            }
//            
//            let finalAverageInfo = TermInfo(studentName: name, gender: gender, for_quarter: "0")
//            self.insertStaticData(final_average, entity: GBTConstants.Entity.GRADESUMMARYGRADE,
//                                  index: column_index,
//                                  info: finalAverageInfo,
//                                  context: context)
//        }
//    }
    
    
    func changeGradebookScore(withData data: [[String:AnyObject]?], handler: @escaping GBDoneBlockWithError) {
        
        let body = ["user_scores": data]
        
        // ENDPOINT
        let endpoint = "/v2/quizzes/course/assessment"
        
        // REQUEST
        let request = route("POST", uri: endpoint, body: body as [String : AnyObject]?)
        
        // SESSION
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                handler(false, e.localizedDescription)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:AnyObject] else { return }
            print("CREATE ITO response dictionary --> \(dictionary)")
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let _ = dictionary["records"] as? [String:AnyObject] else {
                    handler(false, "Something went wrong")
                    return
                }
                
                handler(true, nil)
            } else {
                handler(false, "Something went wrong")
            }
        }) 
        
        task.resume()
    }
    
    func dateOpenAndClose() -> (open:String, close:String) {
        
        let open_date = Date()
        let calendar = Calendar.current
        var components = (calendar as NSCalendar).components([.year, .month, .day], from: open_date)
        components.month! += 1
        let close_date = calendar.date(from: components)!
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let openDate = dateFormatter.string(from: open_date)
        let closeDate = dateFormatter.string(from: close_date)
        return (openDate, closeDate)
    }
    
    // MARK: - Debugging Helper
    fileprivate func prettyFunction(_ file: NSString = #file, function: String = #function, line: Int = #line) {
        print("<start>--- file: \(file.lastPathComponent) function:\(function) line:\(line) ---<end>")
    }
    
    // MARK: - Reports 
    
    func gradeBookReports() -> URLRequest! {
        
        // USERID
        let userid:String! = self.accountUserID()
        
        // BASE URL
        let baseurlString:String! = self.retrieveVSmartBaseURL()
        
        // ENDPOINT
        let path = "http://\(baseurlString!)/reports/webviewschoolgrades/\(userid!)"
        
        // REQUEST
//        let path = "http://dev.vsmartschool.com/reports/webviewschoolgrades/2"
        let url:URL! = URL(string: path)
        let request:URLRequest! = URLRequest(url: url)
        
        return request
    }
    
    
}
