//
//  CourseDataManager.m
//  V-Smart
//
//  Created by Julius Abarra on 09/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "CourseDataManager.h"
#import "Utils.h"
#import "AccountInfo.h"
#import "VSmartValues.h"
#import "NSDate+Helper.h"

static NSString *storeFilename = @"coursedata.sqlite";

@interface CourseDataManager()

#pragma mark - PROPERTIES

@property (nonatomic, readwrite) NSManagedObjectContext *masterContext;
@property (nonatomic, readwrite) NSManagedObjectContext *mainContext;
@property (nonatomic, readwrite) NSManagedObjectContext *workerContext;

@property (nonatomic, readwrite) NSManagedObjectModel *model;
@property (nonatomic, readwrite) NSPersistentStore *store;
@property (nonatomic, readwrite) NSPersistentStoreCoordinator *coordinator;

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSDateFormatter *formatter;

@end

@implementation CourseDataManager

#pragma mark - SINGLETON

+ (instancetype)sharedInstance {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    static CourseDataManager *singleton = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        singleton = [[self alloc] init];
    });
    
    return singleton;
}

#pragma mark - SETUP

- (id)init {
    NSLog(@"Running %s", __PRETTY_FUNCTION__);
    
    self = [super init];
    if (!self) { return nil; }
    
    // MANAGED OBJECT MODEL
    self.model = [self modelFromFrameWork:@"CourseDataModel"];
    
    // PERSISTENT STORE COORDINATOR
    self.coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    
    BOOL compatible = [self isCompatibleWithCoreDataMetadata:[self storeURL]];
    if (!compatible) {
        self.store = nil;
    }
    
    // CORE DATA 3-LAYER STACK for MASTER, MAIN, WORKER
    [self initializeManagedObjectsWithCoordinator:self.coordinator];
    
    // SESSION MANAGER
    self.session = [NSURLSession sharedSession];
    
    // DATE FORMATTER
    self.formatter = [[NSDateFormatter alloc] init];
    
    return self;
}

- (void)initializeManagedObjectsWithCoordinator:(NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // DATA FLUSHER
    // [WARNING: YOU ARE NOT ALLOWED TO USE THIS CONTEXT]
    // Core Data Stack for the Master Thread [MASTER] -> [PERSISTENTSTORE]
    _masterContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_masterContext performBlockAndWait:^{
        [_masterContext setPersistentStoreCoordinator:persistentStoreCoordinator];
        [_masterContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // UI RELATED CONTEXT
    // Core Data Stack for the Main Thread [MAIN] -> [MASTER]
    _mainContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_mainContext setParentContext:_masterContext];
    [_mainContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    
    // BACKGROUND CONTEXT
    // Core Data Stack for the Worker Thread [WORKER] -> [MAIN]
    _workerContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_workerContext performBlockAndWait:^{
        [_workerContext setParentContext:_mainContext];
        [_workerContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // LOAD STORE FILE
    [self loadStore];
}

- (void)loadStore {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSDictionary *sqliteConfig = @{@"journal_mode" : @"DELETE"};//@{@"journal_mode" : @"WAL"}
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption  : @YES ,
                              NSInferMappingModelAutomaticallyOption        : @YES , // Light Weight Migration
                              NSSQLitePragmasOption                         : sqliteConfig
                              };
    NSError *error = nil;
    _store = [_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:[self storeURL] options:options error:&error];
    
    if (!_store) {
        //abort();
    } else {
        NSLog(@"Successfully added store: %@", _store);
    }
}

#pragma mark - MODELS (Initialization)

- (NSManagedObjectModel *)modelFromFrameWork:(NSString *)name {
    NSBundle *appBundle = [NSBundle mainBundle];
    NSURL *modelFileURL = [appBundle URLForResource:name withExtension:@"momd"];
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelFileURL];
    
    return model;
}

#pragma mark - SAVING

- (void)saveContext {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSManagedObjectContext *ctx = _workerContext;
    
    [ctx performBlockAndWait:^{
        [self saveTreeContext:ctx];
    }];
}

- (void)saveTreeContext:(NSManagedObjectContext *)context {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    if (!context) {
        return;
    }
    
    NSError *error = nil;
    
    if (![context save:&error]) {
        NSLog(@"ERROR saving: %@", error);
    }
    
    if (context.parentContext) {
        [self saveTreeContext:context.parentContext];
    }
}

#pragma mark - PATHS

- (NSURL *)storeURL {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSURL *fileURL = [self applicationStoresDirectory];
    
    return [fileURL URLByAppendingPathComponent:storeFilename];
}

- (NSURL *)applicationStoresDirectory {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    NSString *filePath = [self applicationDocumentsDirectory];
    NSURL *storesDirectory = [[NSURL fileURLWithPath:filePath] URLByAppendingPathComponent:@"Stores"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtURL:storesDirectory withIntermediateDirectories:YES attributes:nil error:&error]) {
            NSLog(@"Successfully created Stores directory");
            if (error) {
                NSLog(@"Failed to create Stores directory : %@", error);
            }
        }
    }
    return storesDirectory;
}

- (NSString *)applicationDocumentsDirectory {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    return [NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - FAULTING (Memory Management)

- (void)faultObjectWithID:(NSManagedObjectID *)objectID inContext:(NSManagedObjectContext *)context {
    if (!objectID || !context) {
        return;
    }
    
    NSManagedObject *object = [context objectWithID:objectID];
    if (object.hasChanges) {
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"ERROR saving: %@", error);
        }
    }
    if (!object.isFault) {
        [context refreshObject:object mergeChanges:NO];
    }
    else {
        NSLog(@"Skipped faulting an object that is already a fault");
    }
    
    // Repeat the process if the context has a parent
    if (context.parentContext) {
        [self faultObjectWithID:objectID inContext:context.parentContext];
    }
}

#pragma mark - MIGRATION

- (BOOL)isCompatibleWithCoreDataMetadata:(NSURL *)storeUrl {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *filePath = [NSString stringWithFormat:@"%@", storeUrl.path];
    
    if ([fm fileExistsAtPath:filePath]) {
        NSLog(@"checking model for compatibility...");
        
        NSError *error = nil;
        NSDictionary *metaData = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
                                                                                            URL:storeUrl
                                                                                          error:&error];
        NSManagedObjectModel *model = self.coordinator.managedObjectModel;
        
        if ([model isConfiguration:nil compatibleWithStoreMetadata:metaData]) {
            NSLog(@"model is compatible...");
            return YES;
            
        } else {
            
            if ([fm removeItemAtPath:filePath error:NULL]) {
                NSLog(@"removed file : %@", filePath);
                NSLog(@"model is incompatible...");
                return NO;
            }// REMOVE THE STORE FILE
            
        }
    }
    
    NSLog(@"file does not exists..");
    return NO;
}

- (BOOL)migrateStore:(NSURL *)sourceStore {
    NSLog(@"SKIPPED MIGRATION: Source is already compatible");
    
    BOOL success = NO;
    NSError *error = nil;
    
    // STEP 1 - Gather the Source, Destination and Mapping Model
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:sourceStore error:&error];
    NSManagedObjectModel *sourceModel = [NSManagedObjectModel mergedModelFromBundles:nil forStoreMetadata:sourceMetadata];
    NSManagedObjectModel *destinModel = _model;
    NSMappingModel *mappingModel = [NSMappingModel mappingModelFromBundles:nil forSourceModel:sourceModel destinationModel:destinModel];
    
    // STEP 2 - Perform Migration, assuming the mapping model isn't null
    if (mappingModel) {
        NSMigrationManager *migrationManager = [[NSMigrationManager alloc] initWithSourceModel:sourceModel destinationModel:destinModel];
        
        // OBSERVER
        [migrationManager addObserver:self forKeyPath:@"migrationProgress" options:NSKeyValueObservingOptionNew context:NULL];
        
        NSURL *destinStore = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Temp.sqlite"];
        success = [migrationManager migrateStoreFromURL:sourceStore
                                                   type:NSSQLiteStoreType
                                                options:nil
                                       withMappingModel:mappingModel
                                       toDestinationURL:destinStore
                                        destinationType:NSSQLiteStoreType
                                     destinationOptions:nil
                                                  error:&error];
        if (success) {
            // STEP 3 - Replace the old store with the new migrated store
            if ([self replaceStore:sourceStore withStore:destinStore]) {
                NSLog(@"SUCCESSFULLY MIGRATED %@ to the Current Model", sourceStore.path);
                [migrationManager removeObserver:self forKeyPath:@"migrationProgress"];
            }
            // STEP 3
        }
        else {
            NSLog(@"FAILED MIGRATION : %@", error);
        }
    }
    else {
        NSLog(@"FAILED MIGRATION: Mapping Model is null");
    }
    
    return YES; // Indicates migration has finished, regardless of outcome
}

- (BOOL)replaceStore:(NSURL *)old withStore:(NSURL *)new {
    BOOL success = NO;
    NSError *Error = nil;
    if ([[NSFileManager defaultManager] removeItemAtURL:old error:&Error]) {
        
        Error = nil;
        if ([[NSFileManager defaultManager] moveItemAtURL:new toURL:old error:&Error]) {
            success = YES;
        }
        else {
            NSLog(@"FAILED to re-home new store %@", Error);
        }
    }
    else {
        NSLog(@"FAILED to remove old store %@: Error:%@", old, Error);
    }
    return success;
}

#pragma mark - WORKER

- (NSString *)baseURL {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *server = [NSString stringWithFormat:@"%@", [defaults stringForKey:@"baseurl_preference"] ];
    
    return server;
}

- (NSURL *)buildURL:(NSString *)string {
    NSString *path = [Utils buildUrl:string];
    NSLog(@"path : %@", path);
    return [NSURL URLWithString:path];
}

- (NSString *)emptyString:(NSString *)value {
    if ([value isEqualToString:@"(null)"]) {
        return @"";
    }
    return value;
}

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

- (BOOL)isArrayObject:(id)object {
    
    return [object isKindOfClass:[NSArray class]];
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value {
    // Create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // Predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption | NSCaseInsensitivePredicateOption;
    
    // Create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSManagedObject *)getEntity:(NSString *)entity attribute:(NSString *)attribute
                     parameter:(NSString *)parameter context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (parameter) {
        
        // Create predicate
        NSPredicate *predicate = [self predicateForKeyPath:attribute andValue:parameter];
        
        // Set predicate
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
}

- (NSManagedObject *)getEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"entity : %@", entity);
    NSLog(@"predicate : %@", predicate);
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return items;
    }
    
    return nil;
}

- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *lmo in items) {
            [context deleteObject:lmo];
        }
        [self saveTreeContext:context];
        
        return YES;
    }
    
    return NO;
}

- (BOOL)clearDataForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)ctx {
    // Clear contents
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate != nil) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *mo in items) {
            [ctx deleteObject:mo];
        }
        [self saveTreeContext:ctx];
    }
    
    return YES;
}

- (NSTimeInterval)parseTimeFromString:(NSString *)string {
    NSScanner *scanner = [NSScanner scannerWithString:string];
    NSInteger h, m, s;
    
    [scanner scanInteger:&h];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&m];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&s];
    
    return h * 3600 + m * 60 + s;
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (NSDate *)parseDateFromString:(NSString *)dateString {
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@".000Z" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" togo" withString:@""];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [self.formatter setTimeZone:gmt];
    NSString *template = @"yyyy-MM-dd hh:mm:ss";
    [self.formatter setDateFormat:template];
    NSDate *dateObject = [self.formatter dateFromString:dateString];
    return dateObject;
}

- (id)parseResponseData:(NSData *)data {
    
    if (data) {
        NSError *jsonError = nil;
        
        id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
        
        if (jsonError) {
            NSLog(@"JSON Error : %@", jsonError.localizedDescription);
        }
        
        if (!jsonError) {
            return object;
        }
    }
    
    return nil;
}

- (NSMutableURLRequest *)buildRequestWithMethod:(NSString *)method NSURL:(NSURL *)url body:(id)body {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:method];
    
    if (body) {
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:body options:0 error:&error];
        
        NSString *j_string = [NSString stringWithUTF8String:[postData bytes]];
        NSLog(@"j_string : <start>---- %@ ---<end>", j_string);
        [request setHTTPBody:postData];
    }
    
    return request;
}

- (BOOL)ownedByUser:(NSString *)userid {
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return [user_id isEqualToString:userid];
}

- (NSString *)generateBoundaryString {
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary parameters:(NSDictionary *)parameters {
    NSMutableData *httpBody = [NSMutableData data];
    
    // Add params (all params are strings)
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary parameters:(NSDictionary *)parameters file:(NSDictionary *)fileObject {
    NSMutableData *httpBody = [NSMutableData data];
    
    NSLog(@"FILE OBJECT: %@", fileObject);
    
    // Add params (all params are strings)
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    if (fileObject) {
        NSString *fieldname = @"file";
        NSString *filename = [NSString stringWithFormat:@"%@", fileObject[@"filename"]];
        NSURL *fileURL = [NSURL fileURLWithPath:fileObject[@"filepath"]];
        NSData *data = [NSData dataWithContentsOfURL:fileURL];
        NSString *mimetype  = [NSString stringWithFormat:@"%@", fileObject[@"mimetype"]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldname, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary file:(NSDictionary *)fileObject {
    NSMutableData *httpBody = [NSMutableData data];
    
    if (fileObject) {
        NSString *fieldname = @"file";
        NSString *filename = [NSString stringWithFormat:@"%@", fileObject[@"filename"]];
        NSURL *fileURL = [NSURL fileURLWithPath:fileObject[@"filepath"]];
        NSData *data = [NSData dataWithContentsOfURL:fileURL];
        NSString *mimetype  = [NSString stringWithFormat:@"%@", fileObject[@"mimetype"]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldname, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

#pragma mark - STUDENT API

- (void)requestStudentListForCourseWithID:(NSString *)courseid doneBlock:(CourseDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointStudentListv2, courseid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        NSLog(@"courseid: %@", courseid);
        [self clearContentsForEntity:kStudentEntity predicate:nil];
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock ) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            NSDictionary *parsedmeta = dictionary[@"_meta"];
            NSString *metastatus = parsedmeta[@"status"];
            
            NSLog(@"student dictionary: %@", dictionary);
            NSLog(@"student parsedmeta: %@", parsedmeta);
            NSLog(@"student metastatus: %@", metastatus);
            
            BOOL isSuccess = NO;
            
            if ([metastatus isEqualToString:@"SUCCESS"]) {
                isSuccess = YES;
            }
            
            if (isSuccess) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"student records: %@", records);
                
                if (records.count > 0) {
                    NSManagedObjectContext *ctx = self.workerContext;
                    
                    [ctx performBlock:^{
                        for (NSDictionary *d in records) {
                            // Parse data from API
                            NSString *sid = [self stringValue:d[@"id"]];
                            NSString *first_name = [self stringValue:d[@"first_name"]];
                            NSString *last_name = [self stringValue:d[@"last_name"]];
                            NSString *level = [self stringValue:d[@"level"]];
                            NSString *avatar = [NSString stringWithFormat:@"http://%@%@", [self baseURL], [self stringValue:d[@"avatar"]]];
                            NSString *contact_number = [self stringValue:d[@"contact_number"]];
                            NSString *email = [self stringValue:d[@"email"]];
                            NSString *search_string = [NSString stringWithFormat:@"%@%@", last_name, first_name];
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:kStudentEntity attribute:@"id" parameter:sid context:ctx];
                            
                            // Save data to core data
                            [mo setValue:sid forKey:@"id"];
                            [mo setValue:first_name forKey:@"first_name"];
                            [mo setValue:last_name forKey:@"last_name"];
                            [mo setValue:level forKey:@"level"];
                            [mo setValue:avatar forKey:@"avatar"];
                            [mo setValue:contact_number forKey:@"contact_number"];
                            [mo setValue:email forKey:@"email"];
                            [mo setValue:search_string forKey:@"search_string"];
                            
                        }//end for loop
                        
                        [self saveTreeContext:ctx];
                    }];//end context perform block
                }//end has record
                        if (doneBlock) {
                            doneBlock(YES);
                        }
            }//end success
            else {
                    if (doneBlock ) {
                        doneBlock(NO);
                    }
                }
        }//end not error
    }];//end task
    
    [task resume];
}

- (void)requestStudentListForCourseWithID:(NSString *)courseid dataBlock:(CourseDataBlock)dataBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointStudentListv2, courseid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        NSLog(@"courseid: %@", courseid);
        [self clearContentsForEntity:kStudentEntity predicate:nil];
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            NSDictionary *parsedmeta = dictionary[@"_meta"];
            NSString *metastatus = parsedmeta[@"status"];
            
            NSLog(@"student dictionary: %@", dictionary);
            NSLog(@"student parsedmeta: %@", parsedmeta);
            NSLog(@"student metastatus: %@", metastatus);
            
            BOOL isSuccess = NO;
            
            if ([metastatus isEqualToString:@"SUCCESS"]) {
                isSuccess = YES;
            }
            
            if (isSuccess) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"student records: %@", records);
                
                if (records.count > 0) {
                    NSManagedObjectContext *ctx = self.workerContext;
                    
                    [ctx performBlock:^{
                        for (NSDictionary *d in records) {
                            // Parse data from API
                            NSString *sid = [self stringValue:d[@"id"]];
                            NSString *first_name = [self stringValue:d[@"first_name"]];
                            NSString *last_name = [self stringValue:d[@"last_name"]];
                            NSString *level = [self stringValue:d[@"level"]];
                            NSString *avatar = [NSString stringWithFormat:@"http://%@%@", [self baseURL], [self stringValue:d[@"avatar"]]];
                            NSString *contact_number = [self stringValue:d[@"contact_number"]];
                            NSString *email = [self stringValue:d[@"email"]];
                            NSString *search_string = [NSString stringWithFormat:@"%@%@", last_name, first_name];
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:kStudentEntity attribute:@"id" parameter:sid context:ctx];
                            
                            // Defaults
                            if ([contact_number isEqualToString:@""]) {
                                contact_number = @"---";
                            }
                            
                            if ([email isEqualToString:@""]) {
                                email = @"---";
                            }
                            
                            // Save data to core data
                            [mo setValue:sid forKey:@"id"];
                            [mo setValue:first_name forKey:@"first_name"];
                            [mo setValue:last_name forKey:@"last_name"];
                            [mo setValue:level forKey:@"level"];
                            [mo setValue:avatar forKey:@"avatar"];
                            [mo setValue:contact_number forKey:@"contact_number"];
                            [mo setValue:email forKey:@"email"];
                            [mo setValue:search_string forKey:@"search_string"];
                            
                        }//end for loop
                        
                        [self saveTreeContext:ctx];
                    }];//end context perform block
                }//end has record
                if (dataBlock) {
                    NSDictionary *data = @{@"count":@(records.count)};
                    dataBlock(data);
                }
            }//end success
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }//end not error
    }];//end task
    
    [task resume];
}

- (void)downloadProfileImage:(NSManagedObject *)object binaryBlock:(CourseBinaryBlock)binaryBlock {
    if (object != nil) {
        // URL PATH CREATION
        NSString *image_url = [object valueForKey:@"avatar"];
        NSLog(@"IMAGE URL ->>>> [%@]", image_url);
        NSURLSession *s = [NSURLSession sharedSession];
        NSURL *imageURL = [NSURL URLWithString:image_url];
        NSURLSessionDownloadTask *dt = [s downloadTaskWithURL:imageURL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            
            if (location) {
                NSData *data = [NSData dataWithContentsOfURL:location];
                
                if (data) {
                    NSManagedObjectContext *ctx = object.managedObjectContext;
                    
                    [ctx performBlock:^{
                        [object setValue:data forKey:@"thumbnail"];
                        [self saveTreeContext:ctx];
                    }];
                    
                    if (binaryBlock) {
                        binaryBlock(data);
                    }
                }
            }
        }];
        
        [dt resume];
    }
}

- (void)requestStudentProfileWithID:(NSString *)studentid doneBlock:(CourseDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointStudentProfile, studentid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        NSLog(@"studentid: %@", studentid);
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            NSDictionary *parsedmeta = dictionary[@"_meta"];
            NSString *metastatus = parsedmeta[@"status"];
            
            NSLog(@"student profile dictionary: %@", dictionary);
            NSLog(@"student profile parsedmeta: %@", parsedmeta);
            NSLog(@"student profile metastatus: %@", metastatus);
            
            BOOL isSuccess = NO;
            
            if ([metastatus isEqualToString:@"SUCCESS"]) {
                isSuccess = YES;
            }
            
            if (isSuccess) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"student profile records: %@", records);
                
                // REFACTOR
                // jca-01-06-2016
                // Clear contents of student profile in core data
                
                [self clearContentsForEntity:kStudentProfileEntity predicate:nil];
                
                if (records.count > 0) {
                    NSManagedObjectContext *ctx = self.workerContext;
                    
                    [ctx performBlock:^{
                        
                        /*
                         "id": "3",
                         "user_type": "student",
                         "last_name": "Quitay",
                         "first_name": "Ally",
                         "mid_name": "",
                         "suffix_name": "",
                         "email": "aquitay@vsmartschool.me",
                         "user_name": "aquitay@vsmartschool.me",
                         "avatar": "/public/img/avatar/avatar-one.png",
                         "address": "",
                         "contact": "",
                         "contact_person": "",
                         "birth_date": "0000-00-00",
                         "birth_place": "",
                         "nationality": "",
                         "gender": "",
                         "title": "",
                         "nickname": "",
                         "spoken_language": "",
                         "about_me": "",
                         "hobbies": "",
                         "sports": "",
                         "facebook": "",
                         "twitter": "",
                         "instagram": "",
                         "pre_school_name": "vibal u",
                         "pre_school_address": "",
                         "pre_school_year": "",
                         "elem_school_name": "vibal u",
                         "elem_school_address": "",
                         "elem_school_year": "",
                         "high_school_name": "vubal u",
                         "high_school_address": "",
                         "high_school_year": "",
                         "info_percentage": "22"
                         */
                        
                        for (NSDictionary *d in records) {
                            // Parse data from API
                            NSString *sid = [self stringValue:d[@"id"]];
                            NSString *user_type = [self stringValue:d[@"user_type"]];
                            NSString *last_name = [self stringValue:d[@"last_name"]];
                            NSString *first_name = [self stringValue:d[@"first_name"]];
                            NSString *mid_name = [self stringValue:d[@"mid_name"]];
                            NSString *suffix_name = [self stringValue:d[@"suffix_name"]];
                            NSString *email = [self stringValue:d[@"email"]];
                            NSString *user_name = [self stringValue:d[@"user_name"]];
                            NSString *avatar = [NSString stringWithFormat:@"http://%@%@", [self baseURL], [self stringValue:d[@"avatar"]]];
                            NSString *address = [self stringValue:d[@"address"]];
                            NSString *contact = [self stringValue:d[@"contact"]];
                            NSString *contact_person = [self stringValue:d[@"contact_person"]];
                            NSString *birth_date = [self stringValue:d[@"birth_date"]];
                            NSString *birth_place = [self stringValue:d[@"birth_place"]];
                            NSString *nationality = [self stringValue:d[@"nationality"]];
                            NSString *gender = [self stringValue:d[@"gender"]];
                            NSString *title = [self stringValue:d[@"title"]];
                            NSString *nickname = [self stringValue:d[@"nickname"]];
                            NSString *spoken_language = [self stringValue:d[@"spoken_language"]];
                            NSString *about_me = [self stringValue:d[@"about_me"]];
                            NSString *hobbies = [self stringValue:d[@"hobbies"]];
                            NSString *sports = [self stringValue:d[@"sports"]];
                            NSString *facebook = [self stringValue:d[@"facebook"]];
                            NSString *twitter = [self stringValue:d[@"twitter"]];
                            NSString *instagram = [self stringValue:d[@"instagram"]];
                            NSString *pre_school_name = [self stringValue:d[@"pre_school_name"]];
                            NSString *pre_school_address = [self stringValue:d[@"pre_school_address"]];
                            NSString *pre_school_year = [self stringValue:d[@"pre_school_year"]];
                            NSString *elem_school_name = [self stringValue:d[@"elem_school_name"]];
                            NSString *elem_school_address = [self stringValue:d[@"elem_school_address"]];
                            NSString *elem_school_year = [self stringValue:d[@"elem_school_year"]];
                            NSString *high_school_name = [self stringValue:d[@"high_school_name"]];
                            NSString *high_school_address = [self stringValue:d[@"high_school_address"]];
                            NSString *high_school_year = [self stringValue:d[@"high_school_year"]];
                            NSString *info_percentage = [self stringValue:d[@"info_percentage"]];
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:kStudentProfileEntity attribute:@"id" parameter:sid context:ctx];
                            
                            // Save data to core data
                            [mo setValue:sid forKey:@"id"];
                            [mo setValue:user_type forKey:@"user_type"];
                            [mo setValue:last_name forKey:@"last_name"];
                            [mo setValue:first_name forKey:@"first_name"];
                            [mo setValue:mid_name forKey:@"mid_name"];
                            [mo setValue:suffix_name forKey:@"suffix_name"];
                            [mo setValue:email forKey:@"email"];
                            [mo setValue:user_name forKey:@"user_name"];
                            [mo setValue:avatar forKey:@"avatar"];
                            [mo setValue:address forKey:@"address"];
                            [mo setValue:contact forKey:@"contact"];
                            [mo setValue:contact_person forKey:@"contact_person"];
                            [mo setValue:birth_date forKey:@"birth_date"];
                            [mo setValue:birth_place forKey:@"birth_place"];
                            [mo setValue:nationality forKey:@"nationality"];
                            [mo setValue:gender forKey:@"gender"];
                            [mo setValue:title forKey:@"title"];
                            [mo setValue:nickname forKey:@"nickname"];
                            [mo setValue:spoken_language forKey:@"spoken_language"];
                            [mo setValue:about_me forKey:@"about_me"];
                            [mo setValue:hobbies forKey:@"hobbies"];
                            [mo setValue:sports forKey:@"sports"];
                            [mo setValue:facebook forKey:@"facebook"];
                            [mo setValue:twitter forKey:@"twitter"];
                            [mo setValue:instagram forKey:@"instagram"];
                            [mo setValue:pre_school_name forKey:@"pre_school_name"];
                            [mo setValue:pre_school_address forKey:@"pre_school_address"];
                            [mo setValue:pre_school_year forKey:@"pre_school_year"];
                            [mo setValue:elem_school_name forKey:@"elem_school_name"];
                            [mo setValue:elem_school_address forKey:@"elem_school_address"];
                            [mo setValue:elem_school_year forKey:@"elem_school_year"];
                            [mo setValue:high_school_name forKey:@"high_school_name"];
                            [mo setValue:high_school_address forKey:@"high_school_address"];
                            [mo setValue:high_school_year forKey:@"high_school_year"];
                            [mo setValue:info_percentage forKey:@"info_percentage"];
                        }//end for loop
                        
                        [self saveTreeContext:ctx];
                        
                        if (doneBlock) {
                            doneBlock(YES);
                        }
                        
                    }];//end context perform block
                }//end has record
            }//end success
        }//end not error
    }];//end task
    
    [task resume];
}

@end
