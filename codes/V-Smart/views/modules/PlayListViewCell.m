//
//  PlayListViewCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/10/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "PlayListViewCell.h"

@interface PlayListViewCell()
@property (strong, nonatomic) IBOutlet UIView *viewFrame;
@end

@implementation PlayListViewCell

- (void)awakeFromNib {
    // Initialization code
    
    CALayer *layer = self.viewFrame.layer;
    layer.borderColor = [UIColor lightGrayColor].CGColor;
    layer.borderWidth = 0.5f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
