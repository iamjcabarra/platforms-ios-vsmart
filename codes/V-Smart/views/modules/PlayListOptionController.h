//
//  PlayListOptionController.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/19/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PlayListOptionDelegate <NSObject>
@required
- (void)didFinishSelectingOption:(NSString *)option withData:(NSDictionary *)data;
@end

@interface PlayListOptionController : UITableViewController
@property (nonatomic, strong) NSDictionary *data;
@property (nonatomic, strong) NSString *shared;
@property (nonatomic, strong) NSArray *optionList;
@property (nonatomic, weak) id <PlayListOptionDelegate> delegate;
@end
