//
//  CoursePlayerShowResultsView.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 04/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

@objc class CoursePlayerShowResultsV2View: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, UIScrollViewDelegate, CoursePlayerShowResultsDelegate, UICollectionViewDelegateFlowLayout {
    
    fileprivate var fontView:CoursePlayerFontPopoverView!
    fileprivate var popoverController: UIPopoverController!
    
    //    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var testTitleLabel: UILabel!
    @IBOutlet weak var testScoreLabel: UILabel!
    @IBOutlet weak var testQuestionCountLabel: UILabel!
    
    fileprivate var imageIndex : Int?
    fileprivate var previewData: [AnyHashable: Any]?
    
    var testObject:NSManagedObject?
    var results:NSDictionary?
    
    // MARK: - Shared Data Manager
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tm = TestGuruDataManager.sharedInstance()
        return tm!
    }()
    
    // CELL IDENTIFIER
    let cellID = "YGTB_QUESTION_PREVIEW_COLLECTION_ITEM"
    let kModalSegue = "TGTB_PRESENT_CUSTOM_PAGE"
    let kSidePanelModelSegue = "TGTB_PRESENT_CUSTOM_SIDE_PANEL"
    let kModalSectionSelection = "SHOW_SECTION_SELECTION_VIEW"
    let kEditTestView = "SHOW_EDIT_TEST_VIEW"
    
    var index: IndexPath? = nil
    var totalItems: NSInteger? = 0
    
    var show_correct_answer: String = "0"
    var show_feedbacks: String = "0"
    var show_score: String = "0"
    
    let notif = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        self.collectionView.layer.border
        setUpResultSheet()
        
        self.dataManager.save("\(20)", forKey: kCP_SELECTED_FONT_SIZE);
        //        self.submitButton.addTarget(self, action: #selector(self.submitButtonAction(_:)), forControlEvents: .TouchUpInside)]
        //        UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collection.collectionViewLayout;
        //        layout.estimatedItemSize = CGSizeMake(50, 28);
        
        //        let flowLayout = self.collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        //        flowLayout.estimatedItemSize = CGSize(width: 600 , height: 50)
        tableView.estimatedRowHeight = 85
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeNavigationController()
    }
    
    func customizeNavigationController() {
        self.title = NSLocalizedString("TEST RESULT", comment: "")//localize
        let color = UIColor(hex6: 0x4274b9)
        
        if floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1 {
            self.navigationController?.navigationBar.tintColor = color
        } else {
            self.navigationController?.navigationBar.barTintColor = color;
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationController?.navigationBar.isTranslucent = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        _fetchedResultsController = nil
    }
    
    func setUpResultSheet() {
        let testTitle = self.testObject?.value(forKey: "name") as! String
        let questionCount = (self.testObject?.value(forKey: "questions") as? NSSet)!.count
        
        self.show_correct_answer = self.testObject?.value(forKey: "show_correct_answer") as! String
        self.show_feedbacks = self.testObject?.value(forKey: "show_feedbacks") as! String
        self.show_score = self.testObject?.value(forKey: "show_score") as! String
        
        let total_score = self.results?.value(forKey: "total_score") as! String
        let general_score = self.results?.value(forKey: "general_score") as! String
        let scoreStr = "\(total_score)/\(general_score)"
        self.testScoreLabel.text = "Score: \(scoreStr)"
        self.testScoreLabel.highlightString(scoreStr, size: 19, color: UIColor.black)
        
        self.testTitleLabel.text = testTitle
        self.testQuestionCountLabel.text = "\(questionCount) \(NSLocalizedString("Items", comment: ""))"
        
//        if show_score == "0" {
//            self.testScoreLabel.hidden = true
//        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // collection view data source
    //    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    //        guard let sectionData = fetchedResultsController.sections?[section] else {
    //            totalItems = 0
    //            return totalItems!
    //        }
    //        totalIt 3ems = sectionData.numberOfObjects
    //        return totalItems!;
    //    }
    //
    //    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    //        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellID, forIndexPath: indexPath)
    //        self.configureCell(cell, atIndexPath: indexPath)
    //        return cell
    //    }
    //
    //    private func configureCell(collectionCell: UICollectionViewCell!, atIndexPath indexPath: NSIndexPath) {
    //
    //        let cell = collectionCell as! CoursePlayerShowResultsCell
    //        cell.delegate = self;
    //
    //        cell.show_correct_answer = self.show_correct_answer as String
    //        cell.show_feedbacks = self.show_feedbacks as String
    //        cell.show_score = self.show_score as String
    //
    //        let mo = fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject
    //        let questionID = mo.valueForKey("question_id") as! String!
    //        cell.questionID = questionID
    //        cell.displayObject(mo)
    //        cell.reloadFetchedResultsController()
    //
    //        cell.currentIndex = indexPath;
    //
    //        cell.table.layoutIfNeeded()
    //        cell.choicesTable.layoutIfNeeded()
    //
    //        cell.questionAndImageTableHeight.constant = cell.table.contentSize.height
    //        cell.choiceTableHeight.constant = cell.choicesTable.contentSize.height
    //
    //        cell.numberIndicator.text = "\(indexPath.row + 1). "
    //    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        
        return sectionInfo.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PROTOTYPE_CELL", for: indexPath)
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ collectionCell: UITableViewCell!, atIndexPath indexPath: IndexPath) {
        let cell = collectionCell as! CoursePlayerShowResultsCell
        cell.delegate = self;
        
        cell.show_correct_answer = self.show_correct_answer as String
        cell.show_feedbacks = self.show_feedbacks as String
        cell.show_score = self.show_score as String
        
        let mo = fetchedResultsController.object(at: indexPath)
        let questionID = mo.value(forKey: "question_id") as! String!
        let question_type_id = mo.value(forKey: "question_type_id") as! String!
        
        cell.questionID = questionID
        cell.display(mo)
        cell.reloadFetchedResultsController()
        
        cell.currentIndex = indexPath;
        
        cell.table.isScrollEnabled = true
        cell.choicesTable.isScrollEnabled = true
        
        cell.table.layoutIfNeeded()
        cell.choicesTable.layoutIfNeeded()
        
        let tableContentSize = cell.table.contentSize.height
        let choicesContentSize = cell.choicesTable.contentSize.height
        
        cell.questionAndImageTableHeight.constant = tableContentSize
        cell.choiceTableHeight.constant = choicesContentSize
        
        cell.table.beginUpdates()
        cell.table.endUpdates()
        
        
        
//        cell.table.layoutIfNeeded()
//        cell.choicesTable.layoutIfNeeded()
        
        cell.numberIndicator.text = "\((indexPath as NSIndexPath).row + 1). "
    }
    
    //    func addCellButtonAction(button:UIButton) {
    //        self.performSegueWithIdentifier("TGTB_ASSIGN_QUESTION_VIEW", sender: self)
    //    }
    
    // flow layout
    //    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
    //        return 0.0
    //    }
    //    //
    //    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    //        return 0.0
    //    }
    //    //
    //    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    //        return UIEdgeInsetsMake(0, 0, 0, 0)
    //    }
    
    //    func collectionView
    //
    //    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    //
    ////        let cell = collectionView.cellForItemAtIndexPath(indexPath)
    //
    ////        let height = cell!.choicesTable.contentSize.height + cell!.table.contentSize.height
    //        let height = collectionView.frame.size.height
    //        let width = self.collectionView.bounds.size.width
    ////        UICollectires
    //
    //        return CGSizeMake(width, 10)
    //    }
    
    //    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    //        let height = collectionView.frame.size.height
    //        let width = collectionView.frame.size.width
    //        print("~>\(width)")
    //        return CGSizeMake(width, height)
    //    }
    //
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        print("---------------------------------------------> " + #function)
        resizeContents()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("---------------------------------------------> " + #function)
        resizeContents()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("---------------------------------------------> " + #function)
        resizeContents()
    }
    
    fileprivate func resizeContents() {
        //
        //        print("---------------------------------------------> " + #function)
        //
        //        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        //        let size = self.collectionView.bounds.size
        //        let w = size.width
        //        let h = size.height
        //
        //        layout.itemSize = CGSizeMake(w, h)
        //        layout.invalidateLayout()
    }
    
    func generateCustomButton(_ imageName: String, action: Selector) -> UIButton {
        
        let button = UIButton(type: UIButtonType.system)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.customSetImage(imageName)
        button.tintColor = UIColor.white
        button.addTarget(self, action: action, for: .touchUpInside)
        
        return button
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "SAMPLE_QUESTION_IMAGE_VIEW" {
            let qip = segue.destination as! QuestionImagePreview
            qip.previewData = self.previewData
            qip.index = self.imageIndex!
        }
        
    }
    /////////////////////////////////// NSFETCHRESULTSCONTROLLER ///////////////////////////////////
    
    /*
     NOTE: CUSTOM ANIMATION
     */
    // MARK: - Fetched results controller
    
    fileprivate lazy var customDescriptors: [NSSortDescriptor] = {
        
        let ascending = true
        let question_index = NSSortDescriptor(key: "index", ascending: ascending)
        
        return [question_index]
    }()
    
    //    private var fetchedResultsController: NSFetchedResultsController {
    //
    //        if _fetchedResultsController != nil {
    //            return _fetchedResultsController!
    //        }
    //
    //        // Set ManagedObjectContext
    //        let ctx = dataManager.mainContext
    //
    //        let fetchRequest = NSFetchRequest(entityName: kCoursePlayerQuestionListEntity)
    //        // Set the batch size to a suitable number.
    //        fetchRequest.fetchBatchSize = 20
    //
    //        // Edit the sort key as appropriate.
    //        fetchRequest.sortDescriptors = customDescriptors
    //
    //        // Edit the section name key path and cache name if appropriate.
    //        // nil for section name key path means "no sections".
    //        let frc = NSFetchedResultsController(fetchRequest: fetchRequest,
    //                                             managedObjectContext: ctx, sectionNameKeyPath: nil, cacheName: nil)
    //
    //        frc.delegate = self
    //
    //        _fetchedResultsController = frc
    //
    //        do {
    //            try _fetchedResultsController!.performFetch()
    //        } catch {
    //            abort()
    //        }
    //
    //        return _fetchedResultsController!
    //    }
    //    private var _fetchedResultsController: NSFetchedResultsController? = nil
    //
    //    private var blockOperation = NSBlockOperation()
    //    private var shouldReloadCollectionView = false
    //
    //    func controllerWillChangeContent(controller: NSFetchedResultsController) {
    //        self.shouldReloadCollectionView = false
    //        self.blockOperation = NSBlockOperation()
    //    }
    //
    //    func controller(controller: NSFetchedResultsController,
    //                    didChangeSection sectionInfo: NSFetchedResultsSectionInfo,
    //                                     atIndex sectionIndex: Int,
    //                                             forChangeType type: NSFetchedResultsChangeType) {
    //
    //        let collectionView = self.collectionView
    //        switch type {
    //        case NSFetchedResultsChangeType.Insert:
    //            self.blockOperation.addExecutionBlock({
    //                collectionView.insertSections( NSIndexSet(index: sectionIndex) )
    //            })
    //        case NSFetchedResultsChangeType.Delete:
    //            self.blockOperation.addExecutionBlock({
    //                collectionView.deleteSections( NSIndexSet(index: sectionIndex) )
    //            })
    //        case NSFetchedResultsChangeType.Update:
    //            self.blockOperation.addExecutionBlock({
    //                collectionView.reloadSections( NSIndexSet(index: sectionIndex ) )
    //            })
    //        default:()
    //        }
    //    }
    //
    //    func controller(controller: NSFetchedResultsController,
    //                    didChangeObject anObject: AnyObject,
    //                                    atIndexPath indexPath: NSIndexPath?,
    //                                                forChangeType type: NSFetchedResultsChangeType,
    //                                                              newIndexPath: NSIndexPath?) {
    //
    //        let collectionView = self.collectionView
    //
    //        switch type {
    //
    //        case NSFetchedResultsChangeType.Insert:
    //
    //            if collectionView.numberOfSections() > 0 {
    //
    //                if collectionView.numberOfItemsInSection( newIndexPath!.section ) == 0 {
    //                    self.shouldReloadCollectionView = true
    //                } else {
    //                    self.blockOperation.addExecutionBlock( {
    //                        collectionView.insertItemsAtIndexPaths( [newIndexPath!] )
    //                    } )
    //                }
    //
    //            } else {
    //                self.shouldReloadCollectionView = true
    //            }
    //
    //        case NSFetchedResultsChangeType.Delete:
    //
    //            if collectionView.numberOfItemsInSection( indexPath!.section ) == 1 {
    //                self.shouldReloadCollectionView = true
    //            } else {
    //                self.blockOperation.addExecutionBlock( {
    //                    collectionView.deleteItemsAtIndexPaths( [indexPath!] )
    //                } )
    //            }
    //
    //        case NSFetchedResultsChangeType.Update:
    //            self.blockOperation.addExecutionBlock({
    //                collectionView.reloadItemsAtIndexPaths( [indexPath!] )
    //            })
    //
    //        case NSFetchedResultsChangeType.Move:
    //            self.blockOperation.addExecutionBlock({
    //                collectionView.moveItemAtIndexPath( indexPath!, toIndexPath: newIndexPath! )
    //            })
    //
    //        }
    //    }
    //
    //    func controllerDidChangeContent(controller: NSFetchedResultsController) {
    //        // Checks if we should reload the collection view to fix a bug @ http://openradar.appspot.com/12954582
    //        if self.shouldReloadCollectionView {
    //            self.collectionView.reloadData()
    //        } else {
    //            self.collectionView.performBatchUpdates({ self.blockOperation.start() }, completion: nil )
    //        }
    //    }
    
    //    func transitionWithData(previewData: [NSObject : AnyObject]!, index: Int, segueID segueIdentifier: String!) {
    //        self.previewData = previewData
    //        self.imageIndex = index
    //        self.performSegueWithIdentifier(segueIdentifier, sender: self)
    //    }
    //
//        func refreshIndexPath(indexPath: NSIndexPath!) {
//            print("ROW!!!!! [\(indexPath.row)]")
//            //        self.view.layoutIfNeeded()
//            dispatch_async(dispatch_get_main_queue()) {
//    //            self.collectionView.reloadItemsAtIndexPaths([indexPath])
//                self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
//            }
//        }
    
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        var error: NSError? = nil
        do {
            try _fetchedResultsController!.performFetch()
        } catch let error1 as NSError {
            error = error1
            print(error?.localizedDescription)
        }
        
    }
    
    // MARK: - Fetched results controller
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kCoursePlayerQuestionListEntity)
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        // Set ManagedObjectContext
        let ctx = dataManager.mainContext
        
        
        //let fetchRequest = NSFetchRequest()
        // Edit the entity name as appropriate.
        //let entity = NSEntityDescription.entity(forEntityName: kCoursePlayerQuestionListEntity, in: ctx!)
        //fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        //        let sortDescriptors = [sortDescriptor]
        
        fetchRequest.sortDescriptors = customDescriptors
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        //        var error: NSError? = nil
        do {
            try _fetchedResultsController!.performFetch()
        } catch _ as NSError {
            //            error = error1
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //println("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadData()
        //            self.configureCell(tableView.cellForRowAtIndexPath(indexPath!)!, atIndexPath: indexPath!)
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .fade)
            tableView.insertRows(at: [newIndexPath!], with: .fade)
            //        default:
            //            return
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    
}
