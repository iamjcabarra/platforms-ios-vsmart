//
//  SocialStreamStickerViewController.m
//  V-Smart
//
//  Created by Ryan Migallos on 2/4/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "SocialStreamStickerViewController.h"
#import "SocialStreamStickerPopViewCell.h"

@interface SocialStreamStickerViewController ()

@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

- (IBAction)closeStickerView:(id)sender;

@end

@implementation SocialStreamStickerViewController

static NSString *kStickerCellIdentifier = @"cell_sticker_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeStickerView:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.menuItems.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SocialStreamStickerPopViewCell *cell = (SocialStreamStickerPopViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kStickerCellIdentifier forIndexPath:indexPath];
    
    NSDictionary *data = self.menuItems[indexPath.row];
    NSString *imagePath = [NSString stringWithFormat:@"%@", data[@"sticker_url"] ];
    NSURL *imageURL = [NSURL URLWithString:imagePath];
    cell.imageView.image = [[EGOImageLoader sharedImageLoader] imageForURL:imageURL shouldLoadWithObserver:nil];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *d = (NSDictionary *)self.menuItems[indexPath.row];

    NSLog(@"sticker object : %@", d);
    
    [self.delegate didFinishSelectingData:d type:@"sticker"];
}

@end
