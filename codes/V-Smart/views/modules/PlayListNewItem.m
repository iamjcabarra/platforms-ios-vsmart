//
//  PlayListNewItem.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/26/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "PlayListNewItem.h"
#import "PlayListMediaLibrary.h"
#import "AppDelegate.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import "CheckBoxCell.h"
#import "SelectedCollectionViewCell.h"
#import "HUD.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import <Photos/Photos.h>

#import "PlayListDataManager.h"

@interface PlayListNewItem () <NSFetchedResultsControllerDelegate, PlayListMediaLibraryDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) IBOutlet UITextField *titleLabel;
@property (strong, nonatomic) IBOutlet UITextField *descriptionLabel;
@property (strong, nonatomic) IBOutlet UITextField *tagLabel;
@property (strong, nonatomic) IBOutlet UIView *sharePanelView;
//@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIProgressView *progressIndicator;

@property (strong, nonatomic) IBOutlet UITextField *importLabel;
@property (strong, nonatomic) IBOutlet UIButton *importButton;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) PlayListDataManager *plm;

@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSDictionary *fileObject;

@property (weak, nonatomic) IBOutlet UILabel *maximumLbl;

@property (weak, nonatomic) IBOutlet UITableView *searchTable;
@property (strong, nonatomic) NSArray *searchResult;
@property (strong, nonatomic) NSMutableArray *shareList;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarHeight;
@property (weak, nonatomic) IBOutlet UILabel *shareToLabel;

@property (nonatomic, assign) BOOL isTyping;

@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) NSNumber *fileSizeNumber;

@end

@implementation PlayListNewItem

static NSString *const kCourseCellIdentifier = @"cell_course_identifier";
static NSString *const kUserCellIndentifier = @"cell_user_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.plm = [PlayListDataManager sharedInstance];
    self.managedObjectContext = self.plm.mainContext;
    
    [self setUpTableViews];
    self.shareList = [NSMutableArray array];
    
    AccountInfo *account = [VSmart sharedInstance].account;
    self.user_id = [NSString stringWithFormat:@"%d", account.user.id ];

    if (self.teachMode) {
        [self initiateSearchForString:@" "];
    } else {
        [self.plm requestListOfTeachers:^(NSArray *list) {
            if (list != nil) {
                if (list.count > 0) {//TODO: RYANM.
                    self.searchResult = list;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.searchTable reloadData];
                    });
                }
            }
        }];
        self.searchBarHeight.constant = 0;
        [self.view layoutIfNeeded];
    }
    
    [self.plm requestFileSizeLimit:^(NSString *error) {
        if (error == nil) {
            [self updateFileSizeNumber];
        } else {
            NSLog(@"File size limit error %@", error);
        }
    }];
    
    [self.saveButton addTarget:self action:@selector(updateAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.importButton addTarget:self action:@selector(importAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.titleLabel.text = @"";
    self.descriptionLabel.text = @"";
    self.tagLabel.text = @"";
    self.saveButton.enabled = YES;
    
    CGFloat version = [[Utils getServerInstanceVersion] floatValue];
    BOOL isVersion25 = (version >= VSMART_SERVER_MAX_VER);
    self.collectionView.hidden = (isVersion25) ? NO : YES;
    self.searchTable.hidden = (isVersion25) ? NO : YES;
    self.searchBar.hidden = (isVersion25) ? NO : YES;
    self.shareToLabel.hidden = (isVersion25) ? NO : YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.maximumLbl.text = NSLocalizedString(@"Maximum of 8MB", nil);
}

- (void)updateFileSizeNumber {
    NSString *fileSizeLimit = [self.plm fetchObjectForKey:kPL_FILESIZE_LIMIT_BYTES]; //1mb = 1048576b = 1024.00 kb
    NSNumber *fileSizeNumber = [NSNumber numberWithDouble:[fileSizeLimit doubleValue]];
    self.fileSizeNumber = fileSizeNumber;
    NSString *file_size = [Utils fileSizeToHumanReadable:fileSizeNumber];
    file_size = ([file_size isEqualToString:@"1024.00 KB"]) ? @"1 MB" : file_size;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.maximumLbl.text = [NSString stringWithFormat:@"Maximum of %@", file_size];
        [self.maximumLbl setHidden:!(fileSizeNumber > 0)];
    });
}

- (void)setUpTableViews {
    // Resusable Cell
    UINib *cellItemNib = [UINib nibWithNibName:@"CheckBoxCell" bundle:nil];
    [self.searchTable registerNib:cellItemNib forCellReuseIdentifier:kUserCellIndentifier];
    
    //UICollectionViewLeftAlignedLayout *layout = (UICollectionViewLeftAlignedLayout *)self.collectionView.collectionViewLayout;
    //layout.estimatedItemSize = CGSizeMake(50, 28);
    
    // Default Height
    self.searchTable.estimatedRowHeight = 44.0f;
    self.searchTable.rowHeight = UITableViewAutomaticDimension;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(170, 44);
}

- (void)importAction:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    //        picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSData *mediaData = nil;
    NSString *contentType = @"video/mp4";
    NSString *mediaURL = @"";
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
        NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
        mediaData = [NSData dataWithContentsOfURL:videoUrl];
        mediaURL = [videoUrl absoluteString];
        contentType = @"video/mp4";
    } else {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSURL *imageURL = [info objectForKey:UIImagePickerControllerReferenceURL];
        mediaData= [NSData dataWithData:UIImageJPEGRepresentation(image, 1)];
        mediaURL = [imageURL absoluteString];
        contentType = @"image/jpg";
    }
    
//    PHFetchResult<PHAsset *> *result = [PHAsset fetchAssetsWithALAssetURLs:@[[NSURL URLWithString:mediaURL]] options:nil];
//    PHAsset *asset = result.firstObject;
    NSString *fileName = [[NSURL URLWithString:mediaURL] lastPathComponent];//[asset valueForKey:@"filename"];
    
    NSInteger mediaSizeBytes = mediaData.length;
    NSInteger mediaSizeMB = mediaData.length/1024.0/1024.0;
    NSLog(@"FILE SIZE!!!!! %ld MB", (long)mediaSizeMB);
    if ((self.fileSizeNumber != nil) && (self.fileSizeNumber > 0) && ([self.fileSizeNumber integerValue] < mediaSizeBytes)) {
        NSString *file_size = [Utils fileSizeToHumanReadable:self.fileSizeNumber];
        file_size = ([file_size isEqualToString:@"1024.00 KB"]) ? @"1 MB" : file_size;
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error importing file"
                                                                           message:[NSString stringWithFormat:@"You can't upload this file. Its size is above the allowable size(%@).", file_size]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *closeAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction *action) {
                                                                    [alert dismissViewControllerAnimated:YES completion:nil];
                                                                }];
            [alert addAction:closeAction];
            [self presentViewController:alert animated:YES completion:nil];
        });
        
        self.importLabel.text = @"";
        self.fileObject = nil;
        return;
    }
    
    NSString *filePath = [NSString stringWithFormat:@"%@", mediaURL];
    self.importLabel.text = [NSString stringWithFormat:@"%@", mediaURL];
    self.fileObject = @{@"filename":fileName,
                        @"mimetype":contentType,
                        @"filepath":filePath,
                        @"filedata":mediaData};
}

- (void)updateAction:(id)sender {
    
    //TITLE
    NSString *title = [NSString stringWithFormat:@"%@", self.titleLabel.text];
    
    //DESCRIPTION
    NSString *file_desc = [NSString stringWithFormat:@"%@", self.descriptionLabel.text];
    
    //MODULE ID
    NSString *module_id = [NSString stringWithFormat:@"%@", self.module_id ];

    NSString *groups = @"";
    
    NSMutableArray *share_to_courses = [NSMutableArray array];
    
    for (NSDictionary *dict in self.shareList) {

        NSString *group_level_id = [NSString stringWithFormat:@"%@", [dict valueForKey:@"group_level_id"] ];
        NSString *context_id = [NSString stringWithFormat:@"%@", [dict valueForKey:@"context_id"] ];
        NSDictionary *d = @{@"group_level_id":group_level_id,
                            @"context_id":context_id};
        [share_to_courses addObject:d];
    }

    NSString* groups_string = [self jsonStringFromArrayObject:share_to_courses];
    
    groups = [NSString stringWithFormat:@"{\n\"groups\":%@}", groups_string];

    //TAG
    BOOL withTag = NO;
    NSMutableArray *tagObjects = [NSMutableArray array];
    NSString *tag_string = [NSString stringWithFormat:@"%@", self.tagLabel.text];
    NSString *trim_tag_string = [tag_string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (trim_tag_string.length > 0) {
        NSArray *tag_items = [trim_tag_string componentsSeparatedByString:@","];
        if ([tag_items count] > 0) {
            for (NSString *t in tag_items) {
                NSDictionary *d = @{ @"name": t};
                [tagObjects addObject:d];
            }
        }
        withTag = YES;
    }
    
    NSString *tags = @"";
    if (withTag) {
        tags = [self jsonStringFromArrayObject:tagObjects];
        tags = [NSString stringWithFormat:@"{\n\"tags\":%@}", tags];
    }

    //FILE
    
    //BUILD BODY
    NSMutableDictionary *meta = [NSMutableDictionary dictionary];
    [meta setValue:title forKey:@"title"];
    [meta setValue:file_desc forKey:@"description"];
    [meta setValue:module_id forKey:@"module_id"];
    
    //OPTIONAL GROUP PARAMETER
    if (groups.length > 0) {
        [meta setValue:groups forKey:@"groups"];
    }

    //OPTIONAL TAG PARAMETER
    if (tags.length > 0) {
        [meta setValue:tags forKey:@"tags"];
    }
    __weak typeof(self) wo = self;
    if (self.fileObject != nil) {
        self.saveButton.enabled = NO;
        NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Uploading", nil)];
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD showUIBlockingIndicatorWithText:indicatorString];
        });
        
        [self.plm requestUpdatePlayListItemForUploadID:self.user_id meta:meta file:self.fileObject errorBlock:^(NSString *error) {
            if (error != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                                   message:error
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *closeAction = [UIAlertAction actionWithTitle:@"Close"
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction *action) {
                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                      }];
                    [alert addAction:closeAction];
                    [self presentViewController:alert animated:YES completion:nil];
                });
            }
            
            [self.plm requestPlayListForUser:self.user_id doneBlock:^(BOOL status) {
                if([_delegate respondsToSelector:@selector(didFinishUploading)]){
                    [_delegate didFinishUploading];
                    //                [self dismissViewControllerAnimated:YES completion:nil];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [HUD hideUIBlockingIndicator];
                        [wo.navigationController popViewControllerAnimated:YES];
                    });
                }
            }];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.saveButton.enabled = YES;
            });
        }];
        
        
        
        
        
//        [self.plm requestUpdatePlayListItemForUploadID:self.user_id meta:meta file:self.fileObject doneBlock:^(BOOL status) {
//            [self.plm requestPlayListForUser:self.user_id doneBlock:^(BOOL status) {
//                if([_delegate respondsToSelector:@selector(didFinishUploading)]){
//                    [_delegate didFinishUploading];
//                    //                [self dismissViewControllerAnimated:YES completion:nil];
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [HUD hideUIBlockingIndicator];
//                       [wo.navigationController popViewControllerAnimated:YES];
//                    });
//                }
//            }];
//            self.saveButton.enabled = YES;
//        }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:@"Please attach a file."
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *theAction = [UIAlertAction actionWithTitle:@"Okay"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *action) {
                                                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                                              }];
            [alert addAction:theAction];
            [self presentViewController:alert animated:YES completion:nil]; 
        });
    }
    
    self.plm.progressBlock = ^(CGFloat progress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.progressIndicator.progress = progress;
        });
    };
}

- (NSString *)jsonStringFromArrayObject:(NSArray *)array {
    
    NSError *error = nil;
    NSData *dataObject = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    if (error) {
        NSLog(@"json error : %@", [error localizedDescription] );
    }
    NSString *jsonString = [[NSString alloc] initWithData:dataObject encoding:NSUTF8StringEncoding];
    
    return jsonString;
}

- (void)cancelAction:(id)sender {
//    [self dismissViewControllerAnimated:YES completion:nil];
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wo.navigationController popViewControllerAnimated:YES];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 #pragma mark - Navigation
 
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showMediaView"]) {
        PlayListMediaLibrary *library = (PlayListMediaLibrary *)segue.destinationViewController;
        library.delegate = self;
    }
    
}

- (void)initiateSearchForString:(NSString *)searchString {
    [self.plm requestListOfUserForSearchText:searchString listBlock:^(NSArray *list) {
        self.searchResult = list;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.searchTable reloadData];
        });
    }];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self initiateSearchForString:self.searchBar.text];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.7 target:self selector:@selector(observePause) userInfo:nil repeats:NO];
}

- (void)observePause {
    if (self.searchBar.text.length >= 3) {
        [self initiateSearchForString:self.searchBar.text];
    } else if (self.searchBar.text.length == 0) {
        [self initiateSearchForString:@" "];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchResult.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kUserCellIndentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    CheckBoxCell *itemCell = (CheckBoxCell *)cell;
    
    NSDictionary *userDict = self.searchResult[indexPath.row];
    NSString *name = [NSString stringWithFormat:@"%@", userDict[@"name"]];
    NSString *category = [NSString stringWithFormat:@"%@", userDict[@"category"]];
    
    BOOL contains = [self.shareList containsObject:userDict];
    if (contains) {
        itemCell.checkBoxButton.selected = YES;
    }
    
    itemCell.checkBoxLabel.text = name;
    itemCell.cellCategory.text = category;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CheckBoxCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    BOOL checkBoxState = cell.checkBoxButton.selected;
    dispatch_async(dispatch_get_main_queue(), ^{
       cell.checkBoxButton.selected = (checkBoxState) ? false : true;
    });
    
    NSDictionary *userDict = self.searchResult[indexPath.row];
    NSDictionary *dict = [NSDictionary dictionaryWithDictionary:userDict];
    
    (checkBoxState) ? [self.shareList removeObject:dict] : [self.shareList addObject:dict];
    
    [self resizeCollectionView];
    [self.collectionView reloadData];
}

- (void)resizeCollectionView {
    
    CGFloat height = 0;
    if (self.shareList.count > 0) {
        height = 50;
        if (self.shareList.count > 3) {
            height = 100;
        }
    }
    
    self.collectionViewHeight.constant = height;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.shareList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SelectedCollectionViewCell *cell = (SelectedCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"selectedCell" forIndexPath:indexPath];
    
    cell.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.layer.masksToBounds = NO;
    cell.layer.shadowOffset = CGSizeMake(0, 3);
    cell.layer.shadowOpacity = 0.2;
    cell.layer.shadowRadius = 2;
    cell.layer.cornerRadius = 5;
    
    NSDictionary *userDict = self.shareList[indexPath.row];
    NSString *name = [NSString stringWithFormat:@"%@", userDict[@"name"]];
    
    cell.nameLabel.text = name;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger i = indexPath.row;
    
    [self uncheckTableCell:i];
    
    [self.shareList removeObjectAtIndex:i];
    [self resizeCollectionView];
    [self.collectionView reloadData];
}

- (void)uncheckTableCell:(NSInteger )i {
    NSDictionary *shareUserDict = self.shareList[i];
    
    NSInteger index = [self.searchResult indexOfObject:shareUserDict];
    if (@(index) != nil) {
        
        NSArray *visibleIndexPaths = [self.searchTable indexPathsForVisibleRows];
        NSIndexPath *deselectIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
        
        if ([visibleIndexPaths containsObject:deselectIndexPath]) {
            CheckBoxCell *cell = [self.searchTable cellForRowAtIndexPath:deselectIndexPath];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.checkBoxButton.selected = NO;
            });
        }
    }
}

@end
