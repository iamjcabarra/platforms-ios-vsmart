//
//  GradeBookConstants.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 27/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

//#ifndef GradeBookConstants_h
//#define GradeBookConstants_h
//
//
//#endif /* GradeBookConstants_h */


#define kEndPointCourseList                         @"/vsmart-rest-dev/v1/courses/user/list/%@"
#define kEndPointGradeList                          @"/vsmart-rest-dev/v1/assessment/user/%@/%@"
#define kEndPointGBCourseList                       @"/vsmart-rest-dev/v1/quiz/courses/%@"
#define kEndPointGradeBook                          @"/vsmart-rest-dev/v1/assessment/class/%@"
#define kEndPointGradeBookChangeScore               @"/vsmart-rest-dev/v1/assessment/update/score/%@/%@"
#define kEndPointGradeBookSetScore                  @"/vsmart-rest-dev/v1/assessment/set/score/"