//
//  SocialStreamStickerPopViewCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 2/4/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@interface SocialStreamStickerPopViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet EGOImageView *imageView;

@end
