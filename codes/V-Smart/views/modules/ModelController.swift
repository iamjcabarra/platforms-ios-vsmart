//
//  ModelController.swift
//  PageTesting
//
//  Created by Carmelito Bayarcal on 18/08/2016.
//  Copyright © 2016 Carmelito Bayarcal. All rights reserved.
//

import UIKit

/*
 A controller object that manages a simple model -- a collection of month names.
 
 The controller serves as the data source for the page view controller; it therefore implements pageViewController:viewControllerBeforeViewController: and pageViewController:viewControllerAfterViewController:.
 It also implements a custom method, viewControllerAtIndex: which is useful in the implementation of the data source methods, and in the initial configuration of the application.
 
 There is no need to actually create view controllers for each page in advance -- indeed doing so incurs unnecessary overhead. Given the data model, these methods create, configure, and return a new view controller on demand.
 */


class ModelController: NSObject, UIPageViewControllerDataSource {
    
    var pageData_component_ids: [String] = []
//    var pageData_component_names: [String] = []
    var pageData_component_data: [[String : String?]] = []
    
    var term_id = ""
    var scrollingDelegate: ScrollCommunicationDelegate?
    
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    init(term_id : String, scrollingDelegate: ScrollCommunicationDelegate) {
        super.init()
        // Create the data model.
        
        let components = self.gbdm.fetchComponent(forTermID: term_id)
        self.pageData_component_ids = components.componentIDs
        self.pageData_component_data = components.componentData//self.gbdm.fetchComponentNames(forTermID: term_id)
        self.term_id = term_id
        self.scrollingDelegate = scrollingDelegate
    }
    
    func viewControllerAtIndex(_ index: Int, orIndexOfComponentID component_id: String?, storyboard: UIStoryboard) -> GBTLearningComponentPanel? {
        // Return the data view controller for the given index.
        if (self.pageData_component_ids.count == 0) || (index >= self.pageData_component_ids.count) {
            return nil
        }
        
        let newIndex = (component_id != nil) ? self.getIndex(component_id) : index
        
        // Create a new view controller and pass suitable data.
        let dataViewController = storyboard.instantiateViewController(withIdentifier: "GBT_LEARNING_COMPONENT_PANEL_SB") as! GBTLearningComponentPanel
        dataViewController.term_id = self.term_id
        dataViewController.component_id = self.pageData_component_ids[newIndex]
        dataViewController.componentData = self.pageData_component_data[newIndex]
        return dataViewController
    }
    
    func getIndex(_ component_id: String!) -> Int {
        var index = 0
        if self.pageData_component_ids.contains(component_id) {
            index = self.pageData_component_ids.index(of: component_id)!
        }
        
        return index
    }
    
    func indexOfViewController(_ viewController: GBTLearningComponentPanel) -> Int {
        // Return the index of the given data view controller.
        // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
        return pageData_component_ids.index(of: viewController.component_id) ?? NSNotFound
    }
    
    // MARK: - Page View Controller Data Source
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = self.indexOfViewController(viewController as! GBTLearningComponentPanel)
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index -= 1
        return self.viewControllerAtIndex(index, orIndexOfComponentID: nil, storyboard: viewController.storyboard!)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = self.indexOfViewController(viewController as! GBTLearningComponentPanel)
        if index == NSNotFound {
            return nil
        }
        
        index += 1
        if index == self.pageData_component_ids.count {
            return nil
        }
        return self.viewControllerAtIndex(index, orIndexOfComponentID: nil, storyboard: viewController.storyboard!)
    }
    
}

