//
//  CoursePlayer.m
//  V-Smart
//
//  Created by Ryan Migallos on 10/3/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CoursePlayer.h"

@interface CoursePlayer () <WKUIDelegate>
@property (nonatomic, strong) NSDictionary *data;
@property (nonatomic, strong) NSString *url;
@end

@implementation CoursePlayer

- (void)setTopicInfo:(NSDictionary *)object {
    if (self.data != object) {
        self.data = object;
        
        NSLog(@"data : %@", self.data);
        
        // Update the view.
        self.title = [NSString stringWithFormat:@"%@", self.data[@"title"] ];
        self.url = [NSString stringWithFormat:@"%@", self.data[@"url"] ];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.wkWebView = [[WKWebView alloc] initWithFrame:self.view.frame];
    
    NSURL *requestURL = [NSURL URLWithString:self.url];
    NSURLRequest *requestPath = [NSURLRequest requestWithURL:requestURL];
    [self.wkWebView loadRequest:requestPath];
    
    self.view = self.wkWebView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
