//
//  SelectedCollectionViewCell.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 19/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectedCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
