//
//  IdeaPadCell.h
//  V-Smart
//
//  Created by VhaL on 3/5/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface IdeaPadCell : SWTableViewCell
@property (nonatomic, strong) IBOutlet UILabel *labelContent;
@property (nonatomic, strong) IBOutlet UILabel *labelWordCount;
@property (nonatomic, strong) IBOutlet UILabel *labelDateModified;;

@property (nonatomic, assign) id delegate;
-(void)initializeCell:(id)withDelegate;
@end
