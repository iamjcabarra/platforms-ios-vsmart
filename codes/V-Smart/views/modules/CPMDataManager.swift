//
//  CPMDataManager.swift
//  V-Smart
//
//  Created by Julius Abarra on 13/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

class CPMDataManager: NSObject {
  
    fileprivate (set) var managedObjectModel: NSManagedObjectModel?
    fileprivate (set) var persistentStore: NSPersistentStore?
    fileprivate (set) var persistentStoreCoordinator: NSPersistentStoreCoordinator?

    fileprivate (set) var masterContext: NSManagedObjectContext!
    fileprivate (set) var mainContext: NSManagedObjectContext!
    fileprivate (set) var workerContext: NSManagedObjectContext!
    
    fileprivate (set) var session: URLSession!
    
    fileprivate let kPersistentStoreName = "cpmdata.sqlite"
    fileprivate let kCoreDataFWModelName = "CPMDataModel"
    
    fileprivate let networkRequestHelper = CPMNetworkRequestHelper()
    fileprivate let dataParserHelper = CPMParserHelper()
    
    typealias CPMDoneBlock = (_ doneBlock: Bool) -> Void
    typealias CPMDictionaryBlock = (_ dictionaryBlock: NSDictionary?) -> Void
    typealias CPMArrayBlock = (_ arrayBlock: NSArray?) -> Void
    typealias CPMStringDictionaryBlock = (_ stringDictionary: [String: String]?) -> Void
    
    var socketIOClient: SocketIOClient!

    // MARK: - Singleton
    
    static let sharedInstance: CPMDataManager = {
        let singleton = CPMDataManager()
        return singleton
    }()

    // MARK: - Initialization

    fileprivate override init() {
        super.init()
        
        // STEP 1
        // Managed object model
        self.managedObjectModel = self.managedObjectModelFromCoreDataModelName(self.kCoreDataFWModelName)

        // STEP 2
        // Persistent store coordinator
        if let model = self.managedObjectModel {
            self.persistentStoreCoordinator = NSPersistentStoreCoordinator.init(managedObjectModel: model)
        }
        
        // STEP 3
        // Managed object model compatibility test
        if let persistentStoreURL = self.retrievePersistentStoreURL() {
            let isCompatible = self.isCompatibleWithCoreDataMetaData(persistentStoreURL)
            
            if (!isCompatible) {
                self.persistentStore = nil
            }
        }
        
        // STEP 4
        // Core data 3-layer stack for master, main and worker contexts
        if let coordinator = self.persistentStoreCoordinator {
            self.setUpContextsForPersistentStoreCoordinator(coordinator)
        }
        
        // Session manager
        self.session = URLSession.shared
    }

    fileprivate func setUpContextsForPersistentStoreCoordinator(_ coordinator: NSPersistentStoreCoordinator) {
        // DATA FLUSHER
        // Core data stack for the master thread
        // [MASTER] -> [PERSISTENTSTORE]
        self.masterContext = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)
        self.masterContext.performAndWait {
            self.masterContext.persistentStoreCoordinator = coordinator
            self.masterContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        }

        // UI-RELATED CONTEXT
        // Core data stack for the main thread
        // [MAIN] -> [MASTER]
        self.mainContext = NSManagedObjectContext.init(concurrencyType: .mainQueueConcurrencyType)
        self.mainContext.parent = self.masterContext
        self.mainContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy

        // BACKGROUND CONTEXT
        // Core data stack for the worker thread
        // [WORKER] -> [MAIN]
        self.workerContext = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)
        self.workerContext.performAndWait {
            self.workerContext.parent = self.mainContext
            self.workerContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        }

        // Load persistent store file
        self.loadPersistentStore()
    }

    fileprivate func loadPersistentStore() {
        let sqliteConfiguration = ["journal_mode": "DELETE"]
        let options = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true, NSSQLitePragmasOption: sqliteConfiguration] as [String : Any]

        if let persistentStoreURL = self.retrievePersistentStoreURL() {
                do {
                    try self.persistentStore = self.persistentStoreCoordinator?.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: persistentStoreURL, options: options as [AnyHashable: Any])
                    print("Successful loading store...");
                }
                catch let error {
                    print("Error loading store... \(error)");
                }
        }
    }

    // MARK: - Modeling
    
    fileprivate func managedObjectModelFromCoreDataModelName(_ name: String) -> NSManagedObjectModel? {
        let appBundle = Bundle.main
        
        if let modelFileURL = appBundle.url(forResource: name, withExtension: "momd") {
            return NSManagedObjectModel.init(contentsOf: modelFileURL)
        }
    
        return nil
    }

    // MARK: - Saving

    fileprivate func saveContext() -> Bool {
        var success = false
        
        let ctx = self.workerContext

        ctx?.performAndWait({
            success = self.saveTreeContext(ctx!)
        })
        
        return success
    }

    fileprivate func saveTreeContext(_ ctx: NSManagedObjectContext) -> Bool {
        var success = false
        
        do {
            try ctx.save()
            print("Success saving tree context...");
            success = true
        }
        catch let error {
            print("Error saving tree context... \(error)");
        }
        
        if let parentContext = ctx.parent {
            success = self.saveTreeContext(parentContext)
        }
        
        return success
    }

    // MARK: - Pathing

    fileprivate func retrievePersistentStoreURL() -> URL? {
        if let fileURL = self.applicationStoresDirectory() {
            return fileURL.appendingPathComponent(self.kPersistentStoreName)
        }
        
        return nil
    }

    fileprivate func applicationStoresDirectory() -> URL? {
        let directoryPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectoryPath = directoryPaths[0]
        let storesDirectoryPath = (documentsDirectoryPath as NSString).appendingPathComponent("Stores")
        let fileManager = FileManager.default

        if (!fileManager.fileExists(atPath: storesDirectoryPath)) {
            do {
                try fileManager.createDirectory(atPath: storesDirectoryPath, withIntermediateDirectories: true, attributes: nil)
                print("Successful creating application stores directory...");
            }
            catch let error {
                print("Error creating application stores directory... \(error)");
                return nil
            }
        }

        return URL(fileURLWithPath: storesDirectoryPath)
    }

    // MARK: - Faulting

    fileprivate func faultObjectWithID(_ objectID: NSManagedObjectID?, inContext: NSManagedObjectContext?) {
        if (objectID == nil || inContext == nil) {
            return
        }

        if let object = inContext?.object(with: objectID!) {
            if (object.hasChanges) {
                do {
                    try inContext?.save()
                }
                catch let error {
                    print("Error saving... \(error)");
                }
            }

            if (!object.isFault) {
                inContext?.refresh(object, mergeChanges: false)
            }
            else {
                print("Skipped faulting an object that is already a fault...")
            }

            // Repeat the process if the context has a parent
            if ((inContext?.parent) != nil) {
                self.faultObjectWithID(objectID, inContext: inContext?.parent)
            }
        }
    }

    // MARK: - Migration

    fileprivate func isCompatibleWithCoreDataMetaData(_ storeURL: URL) -> Bool {
        let fileManager = FileManager.default
        let filePath = storeURL.path as String

        if (fileManager.fileExists(atPath: filePath)) {
            print("Checking model for compatibility...")

            do {
                let metaData = try NSPersistentStoreCoordinator.metadataForPersistentStore(ofType: NSSQLiteStoreType, at: storeURL, options: nil)

                if let model = self.persistentStoreCoordinator?.managedObjectModel {
                    if (model.isConfiguration(withName: nil, compatibleWithStoreMetadata: metaData)) {
                        print("Model is compatible...")
                        return true
                    }
                }
                else {
                    do {
                        try fileManager.removeItem(atPath: filePath)
                        print("Model is not compatible...")
                        print("Removed file at path \(filePath)")
                    }
                    catch let error {
                        print("Error removing file at path \(filePath)... \(error)");
                    }
                }
            }
            catch let error {
                print("Error checking for model compatibility... \(error)");
            }
        }

        print("File does not exist...")
        return false
    }
    
    fileprivate func migrateStore(_ sourceStore: URL) -> Bool {
        var success = false
        
        do {
            // STEP 1 - Gather the source, destination and mapping model
            let sourceMetadata = try NSPersistentStoreCoordinator.metadataForPersistentStore(ofType: NSSQLiteStoreType, at: sourceStore, options: nil)
            let sourceModel = NSManagedObjectModel.mergedModel(from: nil, forStoreMetadata: sourceMetadata)

            // STEP 2 - Perform migration, assuming the mapping model
            if let mappingModel = NSMappingModel.init(from: nil, forSourceModel: sourceModel, destinationModel: self.managedObjectModel) {
                let migrationManager = NSMigrationManager.init(sourceModel: sourceModel!, destinationModel: self.managedObjectModel!)
                migrationManager.addObserver(self, forKeyPath: "migrationProgress", options: .new , context: nil)

                let destinationStore = self.applicationStoresDirectory()!.appendingPathComponent("Temp.sqlite")

                do {
                    try migrationManager.migrateStore(from: sourceStore, sourceType: NSSQLiteStoreType, options: nil, with: mappingModel, toDestinationURL: destinationStore, destinationType: NSSQLiteStoreType, destinationOptions: nil)

                    // STEP 3 - Replace the old store with the new migrated store
                    if (self.replaceStore(sourceStore, new: destinationStore)) {
                        print("Successfully migrated \(sourceStore.path) to the current model...")
                        migrationManager.removeObserver(self, forKeyPath: "migrationProgress")
                        success = true
                    }
                    else {
                        print("Migration failed...")
                    }
                }
                catch let error {
                    print("Migration error... \(error)")
                }
            }
            else {
                print("Migration failed. Mapping model is null...")
            }
        }
        catch let error {
            print(error)
        }

        return success
    }

    fileprivate func replaceStore(_ old: URL, new: URL) -> Bool {
        var success = false

        do {
            try FileManager.default.removeItem(at: old)

            do {
                try FileManager.default.moveItem(at: new, to: old)
                success = true
            }
            catch let error {
                print("Failed to re-home new store... \(error)");
            }
        }
        catch let error {
            print("Failed to remove old store... \(error)");
        }

        return success
    }
    
    // MARK: - Worker
    
    fileprivate func retrieveEntity(_ entity: String, fromContext: NSManagedObjectContext, filteredByPredicate: NSPredicate?) -> NSManagedObject {
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        
        if let predicate = filteredByPredicate {
            fetchRequest.predicate = predicate
        }
        
        do {
            let items = try fromContext.fetch(fetchRequest)
            
            if (items.count > 0) {
                if let mo = items.last {
                   return mo
                }
            }
        }
        catch let error {
            print("Error retrieving data for entity \(entity): \(error)")
        }
        
        return NSEntityDescription.insertNewObject(forEntityName: entity, into: fromContext)
    }
    
    fileprivate func retrieveEntity(_ entity: String, filteredByPredicate: NSPredicate?) -> NSManagedObject? {
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        
        if let predicate = filteredByPredicate {
            fetchRequest.predicate = predicate
        }
        
        do {
            let items = try self.workerContext.fetch(fetchRequest)
            
            if (items.count > 0) {
                if let mo = items.last {
                    return mo
                }
            }
        }
        catch let error {
            print("Error retrieving data for entity \(entity): \(error)")
        }
        
        return nil
    }
    
    fileprivate func retrieveObjectsForEntity(_ entity: String, filteredByPredicate: NSPredicate?) -> [NSManagedObject]? {
        //let fetchRequest = NSFetchRequest.init(entityName: entity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        
        if let predicate = filteredByPredicate {
            fetchRequest.predicate = predicate
        }
        
        do {
            let items = try self.workerContext.fetch(fetchRequest)
            var objects = [NSManagedObject]()
            
            for mo in items {
//                if let object = mo {
                    objects.append(mo)
//                }
            }
            
            return objects
        }
        catch let error {
            print("Error retrieving data for entity \(entity): \(error)")
        }
        
        return nil
    }
    
    fileprivate func clearDataForEntity(_ entity: String, fromContext: NSManagedObjectContext, withPredicate: NSPredicate?) -> Bool {
        var success = false
        
//        let fetchRequest = NSFetchRequest.init(entityName: entity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        
        if let predicate = withPredicate {
            fetchRequest.predicate = predicate
        }
        
        do {
            let items = try fromContext.fetch(fetchRequest)
            
            for mo in items {
                fromContext.delete(mo)
            }
            
            success = self.saveTreeContext(fromContext)
        }
        catch let error {
            print("Error clearing data for entity \(entity): \(error)")
        }
        
        return success
    }
    
    func updateEntity(_ entity: String, filteredByPredicate: NSPredicate, withData: NSDictionary) -> Bool {
        if let managedObject = self.retrieveEntity(entity, filteredByPredicate: filteredByPredicate) {
            let keys = withData.allKeys
            
            for k in keys {
                if let kString = k as? String {
                    managedObject.setValue(withData[kString], forKey: kString)
                }
            }
            
            return self.saveTreeContext(managedObject.managedObjectContext!)
        }
        
        return false
    }
    
    // MARK: - Data Filters
    
    func predicateForKeyPath(_ keyPath: String, exactValue: String) -> NSPredicate {
        let leftExpression = NSExpression(forKeyPath: keyPath)
        let rightExpression = NSExpression(forConstantValue: exactValue)
        let options: NSComparisonPredicate.Options = [.diacriticInsensitive, .caseInsensitive]
        let predicate = NSComparisonPredicate(leftExpression: leftExpression, rightExpression: rightExpression, modifier: .direct, type: .equalTo,options: options)
        
        return predicate
    }
    
    func predicateForKeyPath(_ keyPath: String, containsValue: String) -> NSPredicate {
        let leftExpression = NSExpression(forKeyPath: keyPath)
        let rightExpression = NSExpression(forConstantValue: containsValue)
        let options: NSComparisonPredicate.Options = [.diacriticInsensitive, .caseInsensitive]
        let predicate = NSComparisonPredicate(leftExpression: leftExpression, rightExpression: rightExpression, modifier: .direct, type: .contains, options: options)
        
        return predicate
    }
    
    // MARK: - Course Category API
    
    func requestCourseCategoryListForUserWithID(_ userid: String, completionHandler: @escaping CPMDictionaryBlock) {
        let context = self.workerContext
        _ = self.clearDataForEntity(CPMConstants.Entity.COURSE_CATEGORY, fromContext: context!, withPredicate: nil)
        
        guard let url = self.networkRequestHelper.buildRequestURLFromEndPoint(endPointForCourseCategoryListOfUserWithID(userid)) else {
            completionHandler(nil)
            return
        }
        
        let request = self.networkRequestHelper.buildURLRequestForMethod("GET", url: url, body: nil)
        
        let task = self.session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                completionHandler(nil)
            }
            else {
                var success = false
                
                if let d = data {
                    if let dictionary = self.dataParserHelper.parseResponseData(d) as? NSDictionary {
                        let okay = self.dataParserHelper.okayToParseResponse(dictionary)
                        
                        if (okay) {
                            if let records = dictionary["records"] as? [[String:AnyObject]] {
                                print("course category records: \(records)")
                                
                                success = true
                                let count = records.count
                                
                                if (count > 0) {
                                    context?.perform({
                                        for d in records {
                                            let category_id = self.dataParserHelper.stringObject(d["id"])
                                            let code = self.dataParserHelper.stringObject(d["code"])
                                            let name = self.dataParserHelper.stringObject(d["name"])
                                            let description = self.dataParserHelper.stringObject(d["description"])
                                            
                                            let predicate = self.predicateForKeyPath("id", exactValue: category_id)
                                            let managedObject = self.retrieveEntity(CPMConstants.Entity.COURSE_CATEGORY, fromContext: context!, filteredByPredicate: predicate)
                                            
                                            managedObject.setValue(category_id, forKey: "id")
                                            managedObject.setValue(code, forKey: "code")
                                            managedObject.setValue(name, forKey: "name")
                                            managedObject.setValue(description, forKey: "category_description")
                                        }
                                        
                                        completionHandler(self.saveTreeContext(context!) ? ["count": count] : nil)
                                    })
                                }
                                else {
                                    completionHandler(["count": count])
                                }
                            }
                        }
                    }
                }
                
                if (!success) {
                    completionHandler(nil)
                }
            }
        }) 
        
        task.resume()
    }
    
    // MARK: - Curriculum API
    
    func requestCurriculumListForCourseCategoryWithID(_ categoryid: String, andUserWithID: String, completionHandler: @escaping CPMDictionaryBlock) {
//        guard let url = self.networkRequestHelper.buildRequestURLFromEndPoint(endPointForCurriculumListForCourseCategoryWithID(categoryid, andUserWithID: andUserWithID)) else {
//            completionHandler(nil)
//            return
//        }
//        
//        let request = self.networkRequestHelper.buildURLRequestForMethod("GET", url: url, body: nil)
//        
//        let task = self.session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
//            if let e = error {
//                print(e.localizedDescription)
//                completionHandler(nil)
//            }
//            else {
//                
//                let context = self.workerContext
//                var success = false
//                let status = self.clearDataForEntity(CPMConstants.Entity.CURRICULUM, fromContext: context!, withPredicate: nil)
//                
//                if status {
//                    if let d = data {
//                        if let dictionary = self.dataParserHelper.parseResponseData(d) as? NSDictionary {
//                            let okay = self.dataParserHelper.okayToParseResponse(dictionary)
//                            
//                            if (okay) {
//                                if let records = dictionary["records"] as? [[String:AnyObject]] {
//                                    print("curriculum records: \(records)")
//                                    
//                                    success = true
//                                    let count = records.count
//                                    
//                                    if (count > 0) {
//                                        
//                                        context?.perform({
//                                            for d in records {
//                                                let curriculum_id = self.dataParserHelper.stringObject(d["curriculum_id"])
//                                                let curriculum_territory_id = self.dataParserHelper.stringObject(d["curriculum_territory_id"])
//                                                let curriculum_territory_name = self.dataParserHelper.stringObject(d["curriculum_territory_name"])
//                                                let curriculum_title = self.dataParserHelper.stringObject(d["curriculum_title"])
//                                                let curriculum_description = self.dataParserHelper.stringObject(d["curriculum_description"])
//                                                let curriculum_status = self.dataParserHelper.stringObject(d["curriculum_status"])
//                                                let curriculum_status_remarks = self.dataParserHelper.stringObject(d["curriculum_status_remarks"])
//                                                let is_template = self.dataParserHelper.stringObject(d["is_template"])
//                                                let reference_id = self.dataParserHelper.stringObject(d["reference_id"])
//                                                let effectivity = self.dataParserHelper.stringObject(d["effectivity"])
//                                                let course_id = self.dataParserHelper.stringObject(d["course_id"])
//                                                let course_name = self.dataParserHelper.stringObject(d["course_name"])
//                                                let grade_level_id = self.dataParserHelper.stringObject(d["grade_level_id"])
//                                                let grade_level_name = self.dataParserHelper.stringObject(d["grade_level_name"])
//                                                let school_profile_id = self.dataParserHelper.stringObject(d["school_profile_id"])
//                                                let school_profile_name = self.dataParserHelper.stringObject(d["school_profile_name"])
//                                                let sy_id = self.dataParserHelper.stringObject(d["sy_id"])
//                                                let sy_name = self.dataParserHelper.stringObject(d["sy_name"])
//                                                let created_by = self.dataParserHelper.stringObject(d["created_by"])
//                                                let avatar = self.dataParserHelper.stringObject(d["avatar"])
//                                                let created_by_id = self.dataParserHelper.stringObject(d["created_by_id"])
//                                                let date_created = self.dataParserHelper.stringObject(d["date_created"])
//                                                let date_modified = self.dataParserHelper.stringObject(d["date_modified"])
//                                                
//                                                let predicate1 = self.predicateForKeyPath("curriculum_id", exactValue: curriculum_id)
//                                                let predicate2 = self.predicateForKeyPath("user_id", exactValue: andUserWithID)
//                                                let predicate3 = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1, predicate2])
//                                                let managedObject = self.retrieveEntity(CPMConstants.Entity.CURRICULUM, fromContext: context!, filteredByPredicate: predicate3)
//                                                
//                                                managedObject.setValue(curriculum_id, forKey: "curriculum_id")
//                                                managedObject.setValue(curriculum_territory_id, forKey: "curriculum_territory_id")
//                                                managedObject.setValue(curriculum_territory_name, forKey: "curriculum_territory_name")
//                                                managedObject.setValue(curriculum_title, forKey: "curriculum_title")
//                                                managedObject.setValue(curriculum_description, forKey: "curriculum_description")
//                                                managedObject.setValue(curriculum_status, forKey: "curriculum_status")
//                                                managedObject.setValue(curriculum_status_remarks, forKey: "curriculum_status_remarks")
//                                                managedObject.setValue(is_template, forKey: "is_template")
//                                                managedObject.setValue(reference_id, forKey: "reference_id")
//                                                managedObject.setValue(effectivity, forKey: "effectivity")
//                                                managedObject.setValue(course_id, forKey: "course_id")
//                                                managedObject.setValue(course_name, forKey: "course_name")
//                                                managedObject.setValue(grade_level_id, forKey: "grade_level_id")
//                                                managedObject.setValue(grade_level_name, forKey: "grade_level_name")
//                                                managedObject.setValue(school_profile_id, forKey: "school_profile_id")
//                                                managedObject.setValue(school_profile_name, forKey: "school_profile_name")
//                                                managedObject.setValue(sy_id, forKey: "sy_id")
//                                                managedObject.setValue(sy_name, forKey: "sy_name")
//                                                managedObject.setValue(created_by, forKey: "created_by")
//                                                managedObject.setValue(avatar, forKey: "avatar")
//                                                managedObject.setValue(created_by_id, forKey: "created_by_id")
//                                                managedObject.setValue(date_created, forKey: "date_created")
//                                                managedObject.setValue(date_modified, forKey: "date_modified")
//                                                managedObject.setValue(categoryid, forKey: "course_category_id")
//                                                managedObject.setValue(andUserWithID, forKey: "user_id")
//                                            }
//                                            
//                                            completionHandler(self.saveTreeContext(context!) ? ["count": count] : nil)
//                                        })
//                                    }
//                                    else {
//                                        completionHandler(["count": count])
//                                    }
//                                }
//                                else {
//                                    success = true
//                                    completionHandler(["count": 0])
//                                }
//                            }
//                        }
//                    }
//                }
//                
//                if (!success) {
//                    completionHandler(nil)
//                }
//            }
//        }) 
//        
//        task.resume()
        
        guard let url = self.networkRequestHelper.buildRequestURLFromEndPoint(endPointForCurriculumListForCourseCategoryWithID(categoryid, andUserWithID: andUserWithID)) else {
            completionHandler(nil)
            return
        }
        
        let request = self.networkRequestHelper.buildURLRequestForMethod("GET", url: url, body: nil)
        
        let task = self.session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                completionHandler(nil)
            }
            else {
                
                let context = self.workerContext
                var success = false
                
                    if let d = data {
                        if let dictionary = self.dataParserHelper.parseResponseData(d) as? NSDictionary {
                            let okay = self.dataParserHelper.okayToParseResponse(dictionary)
                            
                            if (okay) {
                                if let records = dictionary["records"] as? [[String:AnyObject]] {
                                    print("curriculum records: \(records)")
                                    
                                    success = true
                                    let count = records.count
                                    
                                    if (count > 0) {
                                        context?.perform({
                                        //var curriculum_list = [String]()
                                        
                                            for d in records {
                                                let curriculum_id = self.dataParserHelper.stringObject(d["curriculum_id"])
                                                let curriculum_territory_id = self.dataParserHelper.stringObject(d["curriculum_territory_id"])
                                                let curriculum_territory_name = self.dataParserHelper.stringObject(d["curriculum_territory_name"])
                                                let curriculum_title = self.dataParserHelper.stringObject(d["curriculum_title"])
                                                let curriculum_description = self.dataParserHelper.stringObject(d["curriculum_description"])
                                                let curriculum_status = self.dataParserHelper.stringObject(d["curriculum_status"])
                                                let curriculum_status_remarks = self.dataParserHelper.stringObject(d["curriculum_status_remarks"])
                                                let is_template = self.dataParserHelper.stringObject(d["is_template"])
                                                let reference_id = self.dataParserHelper.stringObject(d["reference_id"])
                                                let effectivity = self.dataParserHelper.stringObject(d["effectivity"])
                                                let course_id = self.dataParserHelper.stringObject(d["course_id"])
                                                let course_name = self.dataParserHelper.stringObject(d["course_name"])
                                                let grade_level_id = self.dataParserHelper.stringObject(d["grade_level_id"])
                                                let grade_level_name = self.dataParserHelper.stringObject(d["grade_level_name"])
                                                let school_profile_id = self.dataParserHelper.stringObject(d["school_profile_id"])
                                                let school_profile_name = self.dataParserHelper.stringObject(d["school_profile_name"])
                                                let sy_id = self.dataParserHelper.stringObject(d["sy_id"])
                                                let sy_name = self.dataParserHelper.stringObject(d["sy_name"])
                                                let created_by = self.dataParserHelper.stringObject(d["created_by"])
                                                let avatar = self.dataParserHelper.stringObject(d["avatar"])
                                                let created_by_id = self.dataParserHelper.stringObject(d["created_by_id"])
                                                let date_created = self.dataParserHelper.stringObject(d["date_created"])
                                                let date_modified = self.dataParserHelper.stringObject(d["date_modified"])
                                                
                                                let predicate1 = self.predicateForKeyPath("curriculum_id", exactValue: curriculum_id)
                                                let predicate2 = self.predicateForKeyPath("user_id", exactValue: andUserWithID)
                                                let predicate3 = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1, predicate2])
                                                let managedObject = self.retrieveEntity(CPMConstants.Entity.CURRICULUM, fromContext: context!, filteredByPredicate: predicate3)
                                                
                                                managedObject.setValue(curriculum_id, forKey: "curriculum_id")
                                                managedObject.setValue(curriculum_territory_id, forKey: "curriculum_territory_id")
                                                managedObject.setValue(curriculum_territory_name, forKey: "curriculum_territory_name")
                                                managedObject.setValue(curriculum_title, forKey: "curriculum_title")
                                                managedObject.setValue(curriculum_description, forKey: "curriculum_description")
                                                managedObject.setValue(curriculum_status, forKey: "curriculum_status")
                                                managedObject.setValue(curriculum_status_remarks, forKey: "curriculum_status_remarks")
                                                managedObject.setValue(is_template, forKey: "is_template")
                                                managedObject.setValue(reference_id, forKey: "reference_id")
                                                managedObject.setValue(effectivity, forKey: "effectivity")
                                                managedObject.setValue(course_id, forKey: "course_id")
                                                managedObject.setValue(course_name, forKey: "course_name")
                                                managedObject.setValue(grade_level_id, forKey: "grade_level_id")
                                                managedObject.setValue(grade_level_name, forKey: "grade_level_name")
                                                managedObject.setValue(school_profile_id, forKey: "school_profile_id")
                                                managedObject.setValue(school_profile_name, forKey: "school_profile_name")
                                                managedObject.setValue(sy_id, forKey: "sy_id")
                                                managedObject.setValue(sy_name, forKey: "sy_name")
                                                managedObject.setValue(created_by, forKey: "created_by")
                                                managedObject.setValue(avatar, forKey: "avatar")
                                                managedObject.setValue(created_by_id, forKey: "created_by_id")
                                                managedObject.setValue(date_created, forKey: "date_created")
                                                managedObject.setValue(date_modified, forKey: "date_modified")
                                                managedObject.setValue(categoryid, forKey: "course_category_id")
                                                managedObject.setValue(andUserWithID, forKey: "user_id")
                                            
                                            //curriculum_list.append(curriculum_id)
                                            }
                                            
                                        //print("CURRICULUM LIST TO REMOVE FROM DB: \(curriculum_list)")
                                        //let predicate = NSPredicate(format: "NOT (curriculum_id IN %@)", curriculum_list)
                                        //let cleared = self.clearDataForEntity(CPMConstants.Entity.CURRICULUM, fromContext: context!, withPredicate: predicate)
                                        //print("CLEARED CURRICULUM: \(cleared)")
                                        
                                            completionHandler(self.saveTreeContext(context!) ? ["count": count] : nil)
                                        })
                                    }
                                    else {
                                        completionHandler(["count": count])
                                    }
                                }
                                else {
                                    success = true
                                    completionHandler(["count": 0])
                                }
                            }
                        }
                    }
                
                if (!success) {
                    completionHandler(nil)
                }
            }
        }) 
        
        task.resume()
    }
    
    func requestDownloadCurriculumWithID(_ curriculumID: String, completionHandler: @escaping CPMDictionaryBlock) {
        guard let url = self.networkRequestHelper.buildRequestURLFromEndPoint(endPointForDownloadingCurriculumWithID(curriculumID)) else {
            completionHandler(nil)
            return
        }
        
        let request = self.networkRequestHelper.buildURLRequestForMethod("GET", url: url, body: nil)
        
        let task = self.session.downloadTask(with: request as URLRequest, completionHandler: { (location, response, error) in
            if let e = error {
                print(e.localizedDescription)
                completionHandler(nil)
            }
            else {
                var success = false
                
                if let r = response as? HTTPURLResponse {
                    let statusCode = self.dataParserHelper.stringObject(r.statusCode as AnyObject?)
                    
                    if (statusCode == "200") {
                        if let url = location {
                            let fileName = self.dataParserHelper.stringObject(r.suggestedFilename as AnyObject?)
                            let fileSize = NSNumber(value: r.expectedContentLength as Int64)
                            let fileData = try? Data(contentsOf: url)
                            
                            if let data = fileData {
                                completionHandler(["fileName": fileName, "fileSize": fileSize, "fileData": data])
                                success = true
                            }
                        }
                    }
                }
                
                if (!success) {
                    completionHandler(nil)
                }
            }
        }) 
        
        task.resume()
    }
    
    func downloadCurriculumFrom(url: URL, completionHandler: @escaping CPMDictionaryBlock) {
        let sessionConfig = URLSessionConfiguration.default
        let anotherSession = URLSession(configuration: sessionConfig)
        let request = self.networkRequestHelper.buildURLRequestForMethod("GET", url: url, body: nil)
        
        let task = anotherSession.downloadTask(with: request as URLRequest, completionHandler: { (location, response, error) in
            if let e = error {
                print(e.localizedDescription)
                completionHandler(nil)
            }
            else {
                var success = false
                
                if let r = response as? HTTPURLResponse {
                    let statusCode = self.dataParserHelper.stringObject(r.statusCode as AnyObject?)
                    
                    if (statusCode == "200") {
                        if let url = location {
                            let fileName = self.dataParserHelper.stringObject(r.suggestedFilename as AnyObject?)
                            let fileSize = NSNumber(value: r.expectedContentLength as Int64)
                            let fileData = try? Data(contentsOf: url)
                            
                            if let data = fileData {
                                completionHandler(["fileName": fileName, "fileSize": fileSize, "fileData": data])
                                success = true
                            }
                        }
                    }
                }
                
                if (!success) {
                    completionHandler(nil)
                }
            }
        })
        
        task.resume()
    }
    
    func saveFilesInCoreData(_ files: NSArray, completionHandler: @escaping CPMDoneBlock) {
        let ctx = self.workerContext
        _ = self.clearDataForEntity(CPMConstants.Entity.FILE, fromContext: ctx!, withPredicate: nil)
        
        if (files.count > 0) {
            print("Files to save in core data: \(files)")
            
            ctx?.perform({
                for fileObject in files {
                    let filesObjectDict = fileObject as! [String:AnyObject]
                    let fsfn = filesObjectDict["fsfn"] as! NSNumber
                    let sdui = self.dataParserHelper.stringObject(filesObjectDict["sdui"])
                    let name = self.dataParserHelper.stringObject(filesObjectDict["name"])
                    let type = self.dataParserHelper.stringObject(filesObjectDict["type"])
                    let path = self.dataParserHelper.stringObject(filesObjectDict["path"])
                    let size = filesObjectDict["size"] as! NSNumber
                    let date = filesObjectDict["date"] as! Date
                    
                    let predicate = self.predicateForKeyPath("fsfn", exactValue: "\(fsfn)")
                    let managedObject = self.retrieveEntity(CPMConstants.Entity.FILE, fromContext: ctx!, filteredByPredicate: predicate)
                    
                    managedObject.setValue(fsfn.intValue, forKey: "fsfn")
                    managedObject.setValue(sdui, forKey: "sdui")
                    managedObject.setValue(name, forKey: "name")
                    managedObject.setValue(type, forKey: "type")
                    managedObject.setValue(path, forKey: "path")
                    managedObject.setValue(size.intValue , forKey: "size")
                    managedObject.setValue(date, forKey: "date")
                    
                }
                
                let success = self.saveTreeContext(ctx!)
                completionHandler(success)
            })
        }
        else {
            completionHandler(false)
        }
        
    }
    
    // MARK: - Curriculum and Lesson Plan Integration API (See LPM)
    
    func requestCurriculumListForCourseWithID(_ courseID: String, completionHandler: @escaping CPMDoneBlock) {
        let context = self.workerContext
        _ = self.clearDataForEntity(CPMConstants.Entity.CURRICULUM_ASSOCIATION, fromContext: context!, withPredicate: nil)
        
        guard let url = self.networkRequestHelper.buildRequestURLFromEndPoint(endPointForCurriculumListForCourseWithID(courseID)) else {
            completionHandler(false)
            return
        }
        
        let request = self.networkRequestHelper.buildURLRequestForMethod("GET", url: url, body: nil)
        
        let task = self.session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                completionHandler(false)
            }
            else {
                var success = false
                
                if let d = data {
                    if let dictionary = self.dataParserHelper.parseResponseData(d) as? NSDictionary {
                        let okay = self.dataParserHelper.okayToParseResponse(dictionary)
                        
                        if (okay) {
                            if let records = dictionary["records"] as? [[String:AnyObject]] {
                                success = true
                                print("curriculum records: \(records)")
                                
                                if (records.count > 0) {
                                    let context = self.workerContext
                                    
                                    context?.perform({
                                        for d in records {
                                            let curriculum_id = self.dataParserHelper.stringObject(d["curriculum_id"])
                                            let curriculum_territory_id = self.dataParserHelper.stringObject(d["curriculum_territory_id"])
                                            let curriculum_territory_name = self.dataParserHelper.stringObject(d["curriculum_territory_name"])
                                            let curriculum_title = self.dataParserHelper.stringObject(d["curriculum_title"])
                                            let curriculum_description = self.dataParserHelper.stringObject(d["curriculum_description"])
                                            let curriculum_status = self.dataParserHelper.stringObject(d["curriculum_status"])
                                            let curriculum_status_remarks = self.dataParserHelper.stringObject(d["curriculum_status_remarks"])
                                            let grade_level_id = self.dataParserHelper.stringObject(d["grade_level_id"])
                                            let grade_level_name = self.dataParserHelper.stringObject(d["grade_level_name"])
                                            let school_profile_id = self.dataParserHelper.stringObject(d["school_profile_id"])
                                            let school_profile_name = self.dataParserHelper.stringObject(d["school_profile_name"])
                                            let sy_id = self.dataParserHelper.stringObject(d["sy_id"])
                                            let sy_name = self.dataParserHelper.stringObject(d["sy_name"])
                                            let created_by = self.dataParserHelper.stringObject(d["created_by"])
                                            let avatar = self.dataParserHelper.stringObject(d["avatar"])
                                            let created_by_id = self.dataParserHelper.stringObject(d["created_by_id"])
                                            let date_created = self.dataParserHelper.stringObject(d["date_created"])
                                            let date_modified = self.dataParserHelper.stringObject(d["date_modified"])
                                            
                                            let predicate = self.predicateForKeyPath("curriculum_id", exactValue: curriculum_id)
                                            let managedObject = self.retrieveEntity(CPMConstants.Entity.CURRICULUM_ASSOCIATION, fromContext: context!, filteredByPredicate: predicate)
                                            
                                            managedObject.setValue(curriculum_id, forKey: "curriculum_id")
                                            managedObject.setValue(curriculum_territory_id, forKey: "curriculum_territory_id")
                                            managedObject.setValue(curriculum_territory_name, forKey: "curriculum_territory_name")
                                            managedObject.setValue(curriculum_title, forKey: "curriculum_title")
                                            managedObject.setValue(curriculum_description, forKey: "curriculum_description")
                                            managedObject.setValue(curriculum_status, forKey: "curriculum_status")
                                            managedObject.setValue(curriculum_status_remarks, forKey: "curriculum_status_remarks")
                                            managedObject.setValue(grade_level_id, forKey: "grade_level_id")
                                            managedObject.setValue(grade_level_name, forKey: "grade_level_name")
                                            managedObject.setValue(school_profile_id, forKey: "school_profile_id")
                                            managedObject.setValue(school_profile_name, forKey: "school_profile_name")
                                            managedObject.setValue(sy_id, forKey: "sy_id")
                                            managedObject.setValue(sy_name, forKey: "sy_name")
                                            managedObject.setValue(created_by, forKey: "created_by")
                                            managedObject.setValue(avatar, forKey: "avatar")
                                            managedObject.setValue(created_by_id, forKey: "created_by_id")
                                            managedObject.setValue(date_created, forKey: "date_created")
                                            managedObject.setValue(date_modified, forKey: "date_modified")
                                        }
                                        
                                        _ = self.saveTreeContext(context!)
                                   })
                                }
                            }
                        }
                    }
                }
                
                completionHandler(success)
            }
        }) 
        
        task.resume()
    }
    
    func requestCurriculumPeriodListForCurriculumWithID(_ curriculumID: String, completionHandler: @escaping CPMDoneBlock) {
        let context = self.workerContext
        _ = self.clearDataForEntity(CPMConstants.Entity.CURRICULUM_PERIOD, fromContext: context!, withPredicate: nil)
        
        guard let url = self.networkRequestHelper.buildRequestURLFromEndPoint(endPointForCurriculumPeriodListForCurriculumWithID(curriculumID)) else {
            completionHandler(false)
            return
        }
        
        let request = self.networkRequestHelper.buildURLRequestForMethod("GET", url: url, body: nil)
        
        let task = self.session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                completionHandler(false)
            }
            else {
                var success = false
                
                if let d = data {
                    if let dictionary = self.dataParserHelper.parseResponseData(d) as? NSDictionary {
                        let okay = self.dataParserHelper.okayToParseResponse(dictionary)
                        
                        if (okay) {
                            if let records = dictionary["records"] as? NSDictionary {
                                if let periods = records["periods"] as? [[String:AnyObject]] {
                                    print("curriculum periods: \(periods)")
                                    success = true
                                    
                                    if (periods.count > 0) {
                                        let context = self.workerContext
                                        
                                        context?.perform({
                                            for d in periods {
                                                let period_id = self.dataParserHelper.stringObject(d["period_id"])
                                                let name = self.dataParserHelper.stringObject(d["name"])
                                                let date_created = self.dataParserHelper.stringObject(d["date_created"])
                                                let date_modified = self.dataParserHelper.stringObject(d["date_modified"])
                                                
                                                let predicate = self.predicateForKeyPath("period_id", exactValue: period_id)
                                                let managedObject = self.retrieveEntity(CPMConstants.Entity.CURRICULUM_PERIOD, fromContext: context!, filteredByPredicate: predicate)
                                                
                                                managedObject.setValue(Int(period_id), forKey: "period_id")
                                                managedObject.setValue(name, forKey: "name")
                                                managedObject.setValue(date_created, forKey: "date_created")
                                                managedObject.setValue(date_modified, forKey: "date_modified")
                                            }
                                            
                                            _ = self.saveTreeContext(context!)
                                        })
                                    }
                                }
                            }
                        }
                    }
                }
                
                completionHandler(success)
            }
        }) 
        
        task.resume()
    }
    
    func requestLearningCompetencyListForPeriodWithID(_ periodID: String, completionHandler: @escaping CPMDoneBlock) {
        let context = self.workerContext
        _ = self.clearDataForEntity(CPMConstants.Entity.CURRICULUM_LEARNING_COMPETENCY, fromContext: context!, withPredicate: nil)
        
        guard let url = self.networkRequestHelper.buildRequestURLFromEndPoint(endPointForCurriculumLearningCompetencyListForCurriculumPeriodWithID(periodID)) else {
            completionHandler(false)
            return
        }
        
        let request = self.networkRequestHelper.buildURLRequestForMethod("GET", url: url, body: nil)
        
        let task = self.session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                completionHandler(false)
            }
            else {
                var success = false
                
                if let d = data {
                    if let dictionary = self.dataParserHelper.parseResponseData(d) as? NSDictionary {
                        let okay = self.dataParserHelper.okayToParseResponse(dictionary)
                        
                        if (okay) {
                            if let records = dictionary["records"] as? NSDictionary {
                                if let competencies = records["competencies"] as? [[String:AnyObject]] {
                                    print("curriculum learning competencies: \(competencies)")
                                    success = true
                                    
                                    if (competencies.count > 0) {
                                        let context = self.workerContext
                                        
                                        context?.perform({
                                            for d in competencies {
                                                let period_id = self.dataParserHelper.stringObject(d["period_id"])
                                                let competency_id = self.dataParserHelper.stringObject(d["competency_id"])
                                                let domain_id = self.dataParserHelper.stringObject(d["domain_id"])
                                                let code = self.dataParserHelper.stringObject(d["code"])
                                                let competency = self.dataParserHelper.stringObject(d["competency"])
                                                
                                                let predicate = self.predicateForKeyPath("competency_id", exactValue: competency_id)
                                                let managedObject = self.retrieveEntity(CPMConstants.Entity.CURRICULUM_LEARNING_COMPETENCY, fromContext: context!, filteredByPredicate: predicate)
                                                
                                                let search_string = "\(code)\(competency)"
                                                
                                                managedObject.setValue(period_id, forKey: "period_id")
                                                managedObject.setValue(competency_id, forKey: "competency_id")
                                                managedObject.setValue(domain_id, forKey: "domain_id")
                                                managedObject.setValue(code, forKey: "code")
                                                managedObject.setValue(competency, forKey: "competency")
                                                managedObject.setValue(search_string, forKey: "search_string")
                                                managedObject.setValue("0", forKey: "is_selected")
                                            }
                                            
                                            _ = self.saveTreeContext(context!)
                                        })
                                    }
                                }
                            }
                        }
                    }
                }
                
                completionHandler(success)
            }
        }) 
        
        task.resume()
    }
    
    func requestCreateCurriculumAssociationToLessonPlanWithID(_ lessonPlanID: String, andUserWithID: String, postBody: NSDictionary, completionHandler: @escaping CPMDoneBlock) {
        guard let url = self.networkRequestHelper.buildRequestURLFromEndPoint(endPointForCurriculumAssociationToLessonPlanWithID(lessonPlanID, andUserWithID: andUserWithID)) else {
            completionHandler(false)
            return
        }
        
        let request = self.networkRequestHelper.buildURLRequestForMethod("POST", url: url, body: postBody)
        
        let task = self.session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                completionHandler(false)
            }
            else {
                var success =  false
                
                if let d = data {
                    if let dictionary = self.dataParserHelper.parseResponseData(d) as? NSDictionary {
                        let okay = self.dataParserHelper.okayToParseResponse(dictionary)
                        
                        if (okay) {
                            success = true
                        }
                    }
                }
                
                completionHandler(success)
            }
        }) 
        
        task.resume()
    }
    
    func requestUpdateCurriculumAssociationToLessonPlanWithID(_ lessonPlanID: String, andUserWithID: String, postBody: NSDictionary, completionHandler: @escaping CPMDoneBlock) {
        guard let url = self.networkRequestHelper.buildRequestURLFromEndPoint(endPointForUpdateCurriculumAssociationToLessonPlanWithID(lessonPlanID, andUserWithID: andUserWithID)) else {
            completionHandler(false)
            return
        }
        
        let request = self.networkRequestHelper.buildURLRequestForMethod("POST", url: url, body: postBody)
        
        let task = self.session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                completionHandler(false)
            }
            else {
                var success =  false
                
                if let d = data {
                    if let dictionary = self.dataParserHelper.parseResponseData(d) as? NSDictionary {
                        let okay = self.dataParserHelper.okayToParseResponse(dictionary)
                        
                        if (okay) {
                            success = true
                        }
                    }
                }
                
                completionHandler(success)
            }
        }) 
        
        task.resume()
    }
    
    func requestGenerationOfCurriculum(withID curriculumID: String, forCourseCategory courseCategoryID: String, completionHandler: @escaping CPMDoneBlock) {
        guard let url = self.networkRequestHelper.buildRequestURLFromEndPoint(endPointForCurriculumGeneration(curriculumID, courseCategoryID: courseCategoryID, uuid: self.deviceUUID())) else {
            completionHandler(false)
            return
        }
        
        let request = self.networkRequestHelper.buildURLRequestForMethod("GET", url: url, body: nil)
        
        let task = self.session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                completionHandler(false)
            }
            else {
                var success =  false
                
                if let d = data {
                    if let dictionary = self.dataParserHelper.parseResponseData(d) as? NSDictionary {
                        let okay = self.dataParserHelper.okayToParseResponse(dictionary)
                        
                        if (okay) {
                            if let records = dictionary["records"] as? [String: Any], let status = records["status"] as? String {
                                if (status.lowercased() == "success") {
                                    success = true
                                }
                            }
                        }
                    }
                }
                
                completionHandler(success)
            }
        })
        
        task.resume()
        
    }
    
    func fetchSelectedLearningCompetencies() -> [[String: Int]] {
        var competencyIDList = [[String: Int]]()
        let predicate = self.predicateForKeyPath("is_selected", exactValue: "1")
        
        if let competencies = self.retrieveObjectsForEntity(CPMConstants.Entity.CURRICULUM_LEARNING_COMPETENCY, filteredByPredicate: predicate) {
            for object in competencies {
                if let competencyID = object.value(forKey: "competency_id") as? String {
                    competencyIDList.append(["id": Int(competencyID)!])
                }
            }
        }
        
        return competencyIDList
    }
    
    func antiJoinPreAssociatedLearningCompetencies(_ competencies: [NSManagedObject]) -> Bool {
        let context = self.workerContext
        var success = false
        
        context?.performAndWait { 
            for competency in competencies {
                let ccc_id = self.dataParserHelper.stringObject(competency.value(forKey: "ccc_id") as AnyObject?)
                let predicate = self.predicateForKeyPath("competency_id", exactValue: ccc_id)
                
                if let mo = self.retrieveEntity(CPMConstants.Entity.CURRICULUM_LEARNING_COMPETENCY, filteredByPredicate: predicate) {
                    mo.setValue("1", forKey: "is_selected")
                }
            }
            
            success = self.saveTreeContext(context!)
        }
        
        return success
    }
    
    func clearDataOfLearningCompetency() -> Bool {
        let ctx = self.workerContext
        return self.clearDataForEntity(CPMConstants.Entity.CURRICULUM_LEARNING_COMPETENCY, fromContext: ctx!, withPredicate: nil)
    }
    
    // Non-Swift Dependency
    func retrieveUserID() -> String {
        if let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo {
            return "\(account.user.id)"
        }
        
        return ""
    }
    
    // Non-Swift Dependency
    func retrieveUserEmail() -> String {
        if let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo {
            return "\(account.user.email)"
        }
        
        return ""
    }
    
    // Non-Swift Dependency
    func deviceUUID() -> String {
        guard let uuid = Utils.deviceUUID() else { return "" }
        return "\(uuid)"
    }
    
    // MARK: - Socket IO Client
    
    func startSocketIOClient (forCurriculumWithID curriculumID: String, completionHandler: @escaping CPMStringDictionaryBlock) {
        guard let socketServerURL = URL(string: "http://\(self.networkRequestHelper.appBaseURL()):8888") else {
            print("Can't connect to unknown socket server!")
            return
        }
        
        self.socketIOClient = SocketIOClient.init(socketURL: socketServerURL, config: [.log(true), .forcePolling(true)])
        
        self.socketIOClient.on("connect", callback: {data, acknowledgement in
            let body = ["user_id": self.retrieveUserID(), "client_uuid": self.deviceUUID()];
            let data = try! JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            guard let jsonString = String(data: data, encoding: String.Encoding.utf8) else {
                print("Can't register client to socket server!")
                return
            }
            
            self.socketIOClient.emit("register_client", with: [jsonString])
            self.socketIOClient.onAny({ (event) in print("SOCKETIO EVENT: \(event)") })
            
            // LISTENERS
            
            self.socketIOClient.on("register_client") { (data, acknowledgement) in
                print("NOTIFICATION (REGISTER CLIENT) :\(data)")
                self.emitRequestForDownloadingCurriculum(curriculum: curriculumID)
            }
            
            self.socketIOClient.on("curriculum_pdf_download") { (data, acknowledgement) in
                print("NOTIFICATION (DOWNLOAD CURRICULUM) :\(data)")
                let result: [String: String]! = self.handleCurriculumDownloadListener(withData: data as [Any]?)
                let error: String! = result["error"]
                let file_path: String! = result["file_path"]
                
                if error == "0" && file_path != "" {
                    print("PATH OF FILE TO DOWNLOAD: \(file_path!)")
                    completionHandler(["error": "0", "file_path": file_path!])
                }
                
                if error == "1" {
                    completionHandler(["error": "1", "file_path": ""])
                }
            }
        })
        
        self.socketIOClient.connect()
    }
    
    func disconnectSocket() {
        if self.socketIOClient != nil {
            self.socketIOClient.disconnect()
        }
    }
    
    fileprivate func jsonToNSData(json: AnyObject) -> NSData? {
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData?
        }
        catch let myJSONError {
            print(myJSONError)
        }
        
        return nil;
    }
    
    fileprivate func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            }
            catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
    }
    
    func handleCurriculumDownloadListener(withData data: [Any]?) -> [String: String]? {
        if data != nil {
            guard let jsonString = data!.last as? String else {
                print("Response from server is not a JSON string!")
                return ["error": "1", "file_path": ""]
            }
            
            guard let dictionary = convertToDictionary(text: jsonString) else {
                print("Can't convert JSON string to dictionary!")
                return ["error": "1", "file_path": ""]
            }
            
            let status = self.dataParserHelper.stringObject(dictionary["status"] as AnyObject?)
            let is_error = self.dataParserHelper.stringObject(dictionary["is_error"] as AnyObject?)
            
            
            if is_error.lowercased() == "0" && status.lowercased() == "0" {
                let file_path = self.dataParserHelper.stringObject(dictionary["file_path"] as AnyObject?)
                return ["error": is_error, "file_path": "http://\(self.networkRequestHelper.appBaseURL())/\(file_path)"]
            }
            
            return ["error": is_error, "file_path": ""]
        }
        
        return ["error": "1", "file_path": ""]
    }
    
    func emitRequestForDownloadingCurriculum(curriculum: String) {
        let instance = self.networkRequestHelper.appBaseURL().components(separatedBy: ".").first
        let body = ["client_uuid": self.deviceUUID(), "user_id": self.retrieveUserID(), "instance": instance, "curr_id": curriculum]
        let data = try! JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue: 0))
        
        guard let jsonString = String(data: data, encoding: String.Encoding.utf8) else {
            print("Can't emit request for curriculum download to socket server!")
            return
        }
        
        self.socketIOClient.emit("curriculum_pdf_download", with: [jsonString])
    }
    
}
