//
//  LessonDetailsHistoryViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/18/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "LessonDetailsHistoryViewController.h"
#import "LessonDetailsHistoryTableViewCell.h"
#import "LessonDetailsTabBarViewController.h"
#import "LessonPlanDataManager.h"

@interface LessonDetailsHistoryViewController ()

<NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) LessonPlanDataManager *lm;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITextView *txtComment;

@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSString *lp_id;

@end

@implementation LessonDetailsHistoryViewController

static NSString *kHistoryCellIdentifier = @"historyCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set lesson plan id
    self.lp_id = [NSString stringWithFormat:@"%@", [self.lesson_mo valueForKey:@"id"]];
    
    // Set protocols for UITableView
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Dynamic table row height
    self.tableView.estimatedRowHeight = 115.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    // Set data manager and context
    self.lm = [LessonPlanDataManager sharedInstance];
    self.managedObjectContext = self.lm.mainContext;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)dateFromString:(NSString *)string parse:(NSString *)pattern display:(NSString *)format {
    string = [NSString stringWithFormat:@"%@ GMT", string];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:pattern];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSDate *date = [formatter dateFromString:string];
    [formatter setDateFormat:format];
    
    NSString *date_string = [formatter stringFromDate:date];
    return date_string;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    LessonDetailsHistoryTableViewCell *historyContentCell = [tableView dequeueReusableCellWithIdentifier:kHistoryCellIdentifier forIndexPath:indexPath];
    
    // Access data from selected managed object
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];

    NSString *firstname = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"]];
    NSString *lastname = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"]];
    NSString *dateModified = [NSString stringWithFormat:@"%@", [mo valueForKey:@"date_modified"]];
    NSString *history = [NSString stringWithFormat:@"%@", [mo valueForKey:@"history"]];
    NSString *name = [NSString stringWithFormat:@"%@ %@", firstname, lastname];
    
    // Format fields with date and time
    NSString *rfcFormat = @"yyyy-MM-dd HH:mm:ss zzz";
    NSString *formattedDateModified = [self dateFromString:dateModified parse:rfcFormat display:@"MMM. dd, yyyy hh:mm a"];
    
    // Render image from core data to history table view
    dispatch_queue_t queue = dispatch_queue_create("com.vibetech.lessonplan.IMAGE", DISPATCH_QUEUE_CONCURRENT);
    dispatch_barrier_async(queue, ^{
        NSString *imagePath = [NSString stringWithFormat:@"%@", [mo valueForKey:@"avatar"]];
        NSURL *url = [NSURL URLWithString:imagePath];
        NSURLSession *session =[NSURLSession sharedSession];
        NSURLSessionDownloadTask *task = [session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            NSData *imageData = [NSData dataWithContentsOfURL:location];
            UIImage *avatar = [UIImage imageWithData:imageData];
            dispatch_async(dispatch_get_main_queue(), ^{
                historyContentCell.imgAvatar.image = avatar;
                historyContentCell.imgAvatar.layer.cornerRadius = 37.5f;
            });
        }];
       
        [task resume];
    });
    
    // Configure the output of history (edited)
    history = [history stringByReplacingOccurrencesOfString:@"@#edited-" withString:@""];
    NSArray *stringComponents = [history componentsSeparatedByString:@","];
    NSString *fieldMessage = @"";
    NSString *editedFields = @"";
    
    if (stringComponents.count > 1) {
        for (int i = 0; i < stringComponents.count; i++) {
            NSString *str = [NSString stringWithFormat:@"\n\t • %@", [stringComponents objectAtIndex:i]];
            editedFields = [editedFields stringByAppendingString:str];
        }
        
        fieldMessage = NSLocalizedString(@"Edited the following fields", nil);
        history = [NSString stringWithFormat:@"%@:%@", fieldMessage, editedFields];
    }
    
    if (stringComponents.count == 1) {
        fieldMessage = NSLocalizedString(@"Edited", nil);
        editedFields = [stringComponents objectAtIndex:0];
        history = [NSString stringWithFormat:@"%@ %@", fieldMessage, editedFields];
    }
    
    historyContentCell.lblName.text = name;
    historyContentCell.lblDate.text = formattedDateModified;
    historyContentCell.lblHistory.text = history;
    
    return historyContentCell;
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kHistoryEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"lp_id == %@ AND lpc_is_deleted == 0", self.lp_id];
    fetchRequest.predicate = predicate;
    
    NSSortDescriptor *sort_date_created = [NSSortDescriptor sortDescriptorWithKey:@"date_created" ascending:NO];
    [fetchRequest setSortDescriptors:@[sort_date_created]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

@end
