//
//  CurriculumBrowserView.m
//  V-Smart
//
//  Created by Julius Abarra on 18/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>
#import "CurriculumBrowserView.h"
#import "CurriculumBrowserItemCell.h"

@interface CurriculumBrowserView () <UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate, UIDocumentInteractionControllerDelegate>

@property (strong, nonatomic) UIDocumentInteractionController *documentInteractionController;

@property (strong, nonatomic) IBOutlet UICollectionView *curriculumCollection;
@property (strong, nonatomic) IBOutlet UIImageView *headerIcon;
@property (strong, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *selectedFileDetailsLabel;
@property (strong, nonatomic) IBOutlet UILabel *collectionStatisticsLabel;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;

@property (strong, nonatomic) NSMutableArray *curriculumList;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@end

@implementation CurriculumBrowserView

static NSString *kCurriculumCellIdentifier = @"curriculumPDFCellIdentifier";
static unsigned long long int maxAllowableSizeForDirectory = 167772160;
static CGFloat kCellHeight = 125.0f;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Collection View Protocols
    self.curriculumCollection.dataSource = self;
    self.curriculumCollection.delegate = self;
    
    // Allow Sections in Collection View
    self.curriculumCollection.allowsSelection = YES;
    self.curriculumCollection.allowsMultipleSelection = NO;
    
    // Attach Long Press Gesture to Collection View
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                       action:@selector(handleLongPress:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [self.curriculumCollection addGestureRecognizer:lpgr];
    
    // Curriculum Collection Data
    self.curriculumList = [self getCurriculumCollection];
    
    // Default View
    [self setUpDefaultView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Default View

- (void)setUpDefaultView {
    // Title
    NSString *title = [NSLocalizedString(@"Curriculum Collection", nil) uppercaseString];
    self.headerTitleLabel.text = title;
    
    // Icon
    self.headerIcon.image = [UIImage imageNamed:@"yellow_folder48x48.png"];
    
    // Close Button
    [self.closeButton addTarget:self
                         action:@selector(closeCurriculumCollection:)
               forControlEvents:UIControlEventTouchUpInside];
    
    // Selected File Details
    self.selectedFileDetailsLabel.text = @"";
    
    if (self.curriculumList.count <= 0) {
        self.selectedFileDetailsLabel.text = NSLocalizedString(@"You have not downloaded any curriculum plan yet.", nil);
    }
    
    [self updateCollectionStatistics];
}

- (void)updateCollectionStatistics {
    // Number of Files
    NSString *stat1 = NSLocalizedString(@"Number of Files", nil);
    stat1 = [NSString stringWithFormat:@"%@: %lu", stat1, (unsigned long)self.curriculumList.count];
    
    // Used Space
    unsigned long long int totalUsedSpace = 0;
    for (NSDictionary *d in self.curriculumList) {
        totalUsedSpace += [d[@"size"] unsignedLongLongValue];
    }

    NSString *totalUsedSpaceString = [self transformedValue:totalUsedSpace];
    NSString *stat2 = NSLocalizedString(@"Used Space", nil);
    stat2 = [NSString stringWithFormat:@"%@: %@", stat2, totalUsedSpaceString];
    
    // Available Space
    unsigned long long int freeSpace = maxAllowableSizeForDirectory - totalUsedSpace;
    NSString *freeSpaceString = [self transformedValue:freeSpace];
    NSString *stat3 = NSLocalizedString(@"Free Space", nil);
    stat3 = [NSString stringWithFormat:@"%@: %@", stat3, freeSpaceString];
    
    
    NSString *statistics = [NSString stringWithFormat:@"%@   |   %@   |   %@", stat1, stat2, stat3];
    self.collectionStatisticsLabel.text = statistics;
}

#pragma mark - Locate Files From Directory

- (NSMutableArray *)getCurriculumCollection {
    // App Documents Directory
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *directoryPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [directoryPaths objectAtIndex:0];
    
    // Curriculum Planner Directory
    NSString *curriculumPlannerDirectory = [NSString stringWithFormat:@"%@/curriculum_planner", documentsDirectory];
    
    // Create Collection Objects for Files
    NSMutableArray *files = [NSMutableArray array];
    
    // Access Contents in Curriculum Planner Directory
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:curriculumPlannerDirectory error:nil];
 
    // Accessing Files
    for (NSString *file in contents){
        // Create Collection object for File's Details
        NSMutableDictionary *fileDetailsDictionary = [NSMutableDictionary dictionary];
        
        // Get File's Path
        NSString *path = [NSString stringWithFormat:@"%@/%@", curriculumPlannerDirectory, file];
        
        // Get File's Attributes
        NSDictionary *attributes = [fileManager attributesOfItemAtPath:path error:nil];
        
        NSString *date = @"";
        NSString *mime = @"";
        NSNumber *size = 0;
        
        if (attributes != nil) {
            // Get File's Date of Modification
            NSDate *dateObject = (NSDate *)[attributes objectForKey:NSFileModificationDate];
            date = [NSDateFormatter localizedStringFromDate:dateObject
                                                  dateStyle:NSDateFormatterLongStyle
                                                  timeStyle:NSDateFormatterMediumStyle];
            
            // Get File's Size
           size = [NSNumber numberWithUnsignedLongLong:[attributes fileSize]];
        }
        
        // Get File's MIME Type
        mime = [self mimeTypeForFileAtPath:path];
        
        // Add File's Details to Dictionary
        [fileDetailsDictionary setValue:file forKey:@"file"];
        [fileDetailsDictionary setValue:path forKey:@"path"];
        [fileDetailsDictionary setValue:size forKey:@"size"];
        [fileDetailsDictionary setValue:date forKey:@"date"];
        [fileDetailsDictionary setValue:mime forKey:@"mime"];
        
        // Add Dictionary to Array of Files
        [files addObject:fileDetailsDictionary];
    }
    
    return files;
}

- (NSString *)mimeTypeForFileAtPath:(NSString *)path {
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return nil;
    }
    
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[path pathExtension], NULL);
    CFStringRef mimeType = UTTypeCopyPreferredTagWithClass (UTI, kUTTagClassMIMEType);
    CFRelease(UTI);
    
    if (!mimeType) {
        return @"application/octet-stream";
    }
    
    return (__bridge NSString *) mimeType;
}

#pragma mark - Collection View Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.curriculumList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CurriculumBrowserItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCurriculumCellIdentifier forIndexPath:indexPath];
    
    NSInteger row = indexPath.row;
    NSDictionary *d = self.curriculumList[row];

    cell.filenameLabel.text = [self compressString:[self stringValue:d[@"file"]] maxLength:26];
    cell.filenameLabel.lineBreakMode = NSLineBreakByCharWrapping;
    cell.filenameLabel.textAlignment = NSTextAlignmentCenter;
    cell.filenameLabel.numberOfLines = 0;
    [cell.filenameLabel sizeToFit];
    
    if (self.selectedIndexPath) {
        [cell showSelection:[indexPath isEqual:self.selectedIndexPath]];
    }
    
    return cell;
}

#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionView *)cv didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CurriculumBrowserItemCell *cell = (CurriculumBrowserItemCell *)[cv cellForItemAtIndexPath:indexPath];
    [cell showSelection:![indexPath isEqual:self.selectedIndexPath]];
    self.selectedIndexPath = [indexPath isEqual:self.selectedIndexPath] ? nil : indexPath;
    
    NSInteger row = indexPath.row;
    NSDictionary *d = self.curriculumList[row];
    
    NSString *file = [self compressString:[self stringValue:d[@"file"]] maxLength:80];
    NSString *date = [self stringValue:d[@"date"]];
    NSString *size = [self stringValue:[self transformedValue:[d[@"size"] unsignedLongLongValue]]];
    
    NSString *downloadedString = [NSLocalizedString(@"was downloaded last", nil) lowercaseString];
    NSString *details = [NSString stringWithFormat:@"%@ (%@) %@ %@. ", file, size, downloadedString, date];
    self.selectedFileDetailsLabel.text = details;
}

- (void)collectionView:(UICollectionView *)cv didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    CurriculumBrowserItemCell *cell = (CurriculumBrowserItemCell *)[cv cellForItemAtIndexPath:indexPath];
    [cell showSelection:NO];
    self.selectedIndexPath = nil;
}

#pragma mark - Collection View FlowLayout Delegate

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 20;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = (collectionView.frame.size.width / 3) - 10;
    return CGSizeMake(width, kCellHeight);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeMake(0, 0);
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.curriculumCollection performBatchUpdates:nil completion:nil];
}

#pragma mark - Long Press Gesture

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    
    CGPoint p = [gestureRecognizer locationInView:self.curriculumCollection];
    NSIndexPath *indexPath = [self.curriculumCollection indexPathForItemAtPoint:p];
    
    if (indexPath != nil){
        NSInteger row = indexPath.row;
        NSDictionary *d = self.curriculumList[row];
        
        NSString *fileName = d[@"file"];
        NSString *filePath = d[@"path"];
        
        NSString *avTitle = fileName;
        NSString *butOpenCP = NSLocalizedString(@"Open", nil);
        NSString *butDelete = NSLocalizedString(@"Delete", nil);
        NSString *butCancel = NSLocalizedString(@"Cancel", nil);
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:avTitle
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *openCPAction = [UIAlertAction actionWithTitle:butOpenCP
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action) {
                                                                 __weak typeof (self) wo = self;
                                                                 dispatch_async (dispatch_get_main_queue(), ^{
                                                                     [wo openFileWithFilePath:filePath];
                                                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                                                 });
                                                             }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:butCancel
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action) {
                                                                [alert dismissViewControllerAnimated:YES completion:nil];
                                                             }];
        
        UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:butDelete
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action) {
                                                                 __weak typeof (self) wo = self;
                                                                 dispatch_async (dispatch_get_main_queue(), ^{
                                                                     NSError *error = nil;
                                                                     NSFileManager *fileManager = [NSFileManager defaultManager];
                                                                     BOOL deleted = [fileManager removeItemAtPath:filePath error:&error];
                                                                     
                                                                     if (deleted) {
                                                                         [wo.curriculumList removeObjectAtIndex:row];
                                                                         [wo.curriculumCollection reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.curriculumCollection.numberOfSections)]];
                                                                         wo.selectedFileDetailsLabel.text = @"";
                                                                         
                                                                         if (wo.curriculumList.count <= 0) {
                                                                             wo.selectedFileDetailsLabel.text = NSLocalizedString(@"You have not downloaded any curriculum plan yet.", nil);
                                                                         }
                                                                         
                                                                         [wo updateCollectionStatistics];
                                                                         [wo.delegate updateDownloadedFileCount];
                                                                     }
                                                                     
                                                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                                                 });
                                                             }];
        [alert addAction:openCPAction];
        [alert addAction:deleteAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)closeCurriculumCollection:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Open Curriculum As PDF File

- (void)openFileWithFilePath:(NSString *)filepath {
    NSURL *url = [NSURL fileURLWithPath:filepath];
    
    if (url) {
        // Initialize Document Interaction Controller
        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        
        // Configure Document Interaction Controller
        self.documentInteractionController.delegate = self;
        
        // Open PDF File
        [self.documentInteractionController presentPreviewAnimated:YES];
    }
    else {
        NSLog(@"Not a valid URL");
    }
}

#pragma mark - Document Interaction Controller Delegate

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return self;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller {
    return self.view;
}

#pragma mark - Class Helpers

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

- (NSString *)compressString:(NSString *)string maxLength:(int)length {
    if (string.length > length) {
        NSString *preString = [string substringToIndex:(length / 2)];
        NSString *posString = [string substringFromIndex:string.length - (length / 2)];
        NSString *finString = [NSString stringWithFormat:@"%@...%@", preString, posString];
        
        return finString;
    }
    else {
        return string;
    }
}

- (NSString *)transformedValue:(unsigned long long int)value {
    // Convert bits to bytes
    double convertedValue = (double)value / 8;
    
    // Units Collection
    int multiplyFactor = 0;
    
    NSArray *tokens = @[@"bytes", @"KB", @"MB", @"GB", @"TB"];
    
    while (convertedValue > 1024) {
        convertedValue /= 1024;
        multiplyFactor++;
    }
    
    return [NSString stringWithFormat:@"%4.2f %@", convertedValue, tokens[multiplyFactor]];
}

@end
