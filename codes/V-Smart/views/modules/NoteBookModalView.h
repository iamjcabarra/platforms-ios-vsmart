//
//  NoteBookModalView.h
//  V-Smart
//
//  Created by Julius Abarra on 03/05/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NoteBookModalViewDelegate <NSObject>

@required
- (void)shouldReloadNoteCollection:(BOOL)reload;

@end

@interface NoteBookModalView : UIViewController

@property (weak, nonatomic) id <NoteBookModalViewDelegate> delegate;
@property (strong, nonatomic) NoteBook *currentNoteBook;
@property (strong, nonatomic) Note *note;

@end
