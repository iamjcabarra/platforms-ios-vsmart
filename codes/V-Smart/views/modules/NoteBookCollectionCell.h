//
//  NoteBookCollectionCell.h
//  V-Smart
//
//  Created by VhaL on 3/6/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteBookCollectionCell : UICollectionViewCell <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UILabel *labelNotesCount;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UIImageView *imageViewAdd;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIButton *buttonAction;
@property (nonatomic, assign) id delegate;
@property (nonatomic, weak) IBOutlet UIView *defaultView;
@property (nonatomic, weak) IBOutlet UIView *editView;

@property (nonatomic, weak) IBOutlet UITextField *actionTextField;
@property (nonatomic, weak) IBOutlet UIButton *actionButtonOk;
@property (nonatomic, weak) IBOutlet UIButton *actionButtonCancel;

@property (nonatomic, assign) int selectedColorId;

@property (nonatomic, assign) BOOL addMode;

-(void)hideEditControls:(BOOL)hide;
-(IBAction)buttonActionTapped:(id)sender;
-(void)updateNotebookColor;
@end
