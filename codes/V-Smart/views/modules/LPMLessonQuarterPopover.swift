//
//  LPMLessonQuarterPopover.swift
//  V-Smart
//
//  Created by Julius Abarra on 07/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol LPMLessonQuarterPopoverDelegate: class {
    func selectedLessonQuarter(_ quarter: String)
}

class LPMLessonQuarterPopover: UITableViewController {
    
    weak var delegate: LPMLessonQuarterPopoverDelegate?
    
    fileprivate var quarterList = [String]()
    
    // MARK: - View Life Cycle
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.quarterList = ["1", "2", "3", "4"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table View Data Source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.quarterList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        cell.textLabel?.text = self.quarterList[(indexPath as NSIndexPath).row] 
        return cell
    }
    
    // MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: {
            self.delegate?.selectedLessonQuarter(self.quarterList[(indexPath as NSIndexPath).row])
        })
    }
}
