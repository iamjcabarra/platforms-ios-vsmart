//
//  CPMFileListViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 21/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol CPMFileListViewControllerDelegate: class {
    func selectedFileObject(_ fileObject: NSDictionary)
}

class CPMFileListViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, NSFetchedResultsControllerDelegate, UIDocumentInteractionControllerDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet fileprivate var headerLabel: UILabel!
    @IBOutlet fileprivate var closeButton: UIButton!
    @IBOutlet fileprivate var collectionView: UICollectionView!
    @IBOutlet fileprivate var directoryStatisticsLabel: UILabel!
    @IBOutlet fileprivate var emptyPlaceholderView: UIView!
    @IBOutlet fileprivate var emptyPlaceholderLabel: UILabel!
    
    weak var delegate:CPMFileListViewControllerDelegate?
    
    var subDirectoryName = ""
    var headerTitle = ""
    var isUploadAction = false
    
    fileprivate var sandboxHelper = VSSandboxHelper()
    fileprivate var documentController: UIDocumentInteractionController!
    fileprivate var blockOperations: [BlockOperation] = []
    fileprivate var selectedCellIndexPath: IndexPath? = nil
    fileprivate var subDirectoryPath = ""
    
    fileprivate let kFileCellIdentifier = "file_cell_identifier"
    
    // MARK: - Data Manager
    
    fileprivate lazy var curriculumPlannerDataManager: CPMDataManager = {
        let cpdm = CPMDataManager.sharedInstance
        return cpdm
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Header title
        self.headerLabel.text = self.headerTitle.uppercased()
        
        // Allow single selection
        self.collectionView.allowsSelection = true
        self.collectionView.allowsMultipleSelection = false
        
        // Set subdirectory path
        self.subDirectoryPath = self.sandboxHelper.makeSubdirectoryInDocumentsDirectory(self.subDirectoryName)
        
        // Fetch files from directory, save details to core data and render to collection view
        self.reloadDownloadedCurriculum()
        
        // Close button action
        let closeButtonAction = #selector(self.closeButtonAction(_:))
        self.closeButton.addTarget(self, action: closeButtonAction, for: .touchUpInside)
        
        // Long press gesture recognizer for delete file action
        let longPressAction = #selector(self.deleteFileObject(_:))
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: longPressAction)
        longPressGestureRecognizer.minimumPressDuration = 0.5
        longPressGestureRecognizer.delaysTouchesBegan = true
        longPressGestureRecognizer.delegate = self
        self.collectionView.addGestureRecognizer(longPressGestureRecognizer)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Directory Statistics
    
    func displayDirectoryStatistics() {
        // Count number of files (excluding directories)
        let numberOfFiles = self.sandboxHelper.retrieveFilesInDirectoryAtPath(self.subDirectoryPath).count
        
        // Empty placeholder view
        self.shouldShowEmptyPlaceholderView(numberOfFiles > 0 ? false : true)
        
        // Calculate used and free spaces
        let usedSpaceSize = self.sandboxHelper.calculateSizeOfDirectoryAtPath(self.subDirectoryPath)
        let freeSpaceSize = LPMConstants.Sandbox.MAX_DIRECTORY_SIZE - usedSpaceSize
        
        // Display statistics
        let numberOfFilesString = "\(NSLocalizedString("Number of Files", comment: "")): \(numberOfFiles)"
        let usedSpaceString = "\(NSLocalizedString("Used Space", comment: "")): \(self.sandboxHelper.formattedStringSize(usedSpaceSize))"
        let freeSpaceString = "\(NSLocalizedString("Free Space", comment: "")): \(self.sandboxHelper.formattedStringSize(freeSpaceSize))"
        self.directoryStatisticsLabel.text = "\(numberOfFilesString) | \(usedSpaceString) | \(freeSpaceString)"
    }
    
    // MARK: - Curriculum Collection
    
    func reloadDownloadedCurriculum() {
        var list = [[String: AnyObject]]()
        let contents = self.sandboxHelper.retrieveContentsOfDirectoryAtPath(self.subDirectoryPath)
        
        for item in contents {
            let itemPath = (self.subDirectoryPath as NSString).appendingPathComponent(item)
            let itemDetails = self.sandboxHelper.retrieveAttributesOfFileAtPath(itemPath)
            let isDirectory = self.sandboxHelper.isItemAtPathADirectory(itemPath)
            
            if (!isDirectory) {
                let fsfn = itemDetails[FileAttributeKey.systemFileNumber]
                let sdui = self.subDirectoryName
                let name = item
                let type = itemDetails[FileAttributeKey.type]
                let path = itemPath
                let size = itemDetails[FileAttributeKey.size]
                let date = itemDetails[FileAttributeKey.modificationDate]
                
                let dict = ["fsfn": fsfn!, "sdui": sdui, "name": name, "type": type!, "path": path, "size": size!, "date": date!]
                list.append(dict as [String : AnyObject])
            }
        }
        
        self.curriculumPlannerDataManager.saveFilesInCoreData(list as NSArray) { (success) in
            DispatchQueue.main.async(execute: {
                self.displayDirectoryStatistics()
                self.reloadFetchedResultsController()
            })
        }
    }
    
    // MARK: - Action for Selected File Object
    
    func openFileObject(_ fileObject: NSManagedObject) {
        let message = self.getDetailsOfFileObject(fileObject)
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let cancelTitle = NSLocalizedString("Cancel", comment: "")
        let openTitle = self.isUploadAction ? NSLocalizedString("Select", comment: "") : NSLocalizedString("Open", comment: "")
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        let openAction = UIAlertAction(title: openTitle, style: .default) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                let path = fileObject.value(forKey: "path") as! String
                let fileURL = URL(fileURLWithPath: path)
                
                self.documentController = UIDocumentInteractionController(url: fileURL)
                self.documentController.delegate = self
                self.documentController.presentPreview(animated: true)
                
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(cancelAction)
        alert.addAction(openAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteFileObject(_ gestureReconizer: UILongPressGestureRecognizer) {
        if (gestureReconizer.state == UIGestureRecognizerState.began) {
            let point = gestureReconizer.location(in: self.collectionView)
            let indexPath = self.collectionView.indexPathForItem(at: point)
            
            if (self.selectedCellIndexPath != nil && self.selectedCellIndexPath != indexPath) {
                let previouslySelectedCell = self.collectionView.cellForItem(at: self.selectedCellIndexPath!) as! CPMFileCollectionViewCell
                previouslySelectedCell.highlightSelectedCell(false)
            }
            
            let selectedCell = self.collectionView.cellForItem(at: indexPath!) as! CPMFileCollectionViewCell
            selectedCell.highlightSelectedCell(true)
            self.selectedCellIndexPath = indexPath!
            
            let fileObject = self.fetchedResultsController.object(at: indexPath!) 
            let message = self.getDetailsOfFileObject(fileObject)
            let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
            
            let cancelTitle = NSLocalizedString("Cancel", comment: "")
            let deleteTitle = NSLocalizedString("Delete", comment: "")
            
            let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (Alert) -> Void in
                DispatchQueue.main.async(execute: {
                    self.collectionView.reloadData()
                    alert.dismiss(animated: true, completion: nil)
                })
            }
            
            let deleteAction = UIAlertAction(title: deleteTitle, style: .default) { (Alert) -> Void in
                DispatchQueue.main.async(execute: {
                    let path = fileObject.value(forKey: "path") as! String
                    
                    self.sandboxHelper.removeFileAtPath(path, completion: { (success) in
                        if (success) {
                            self.selectedCellIndexPath = nil
                            self.reloadDownloadedCurriculum()
                            
                            let predicate = self.curriculumPlannerDataManager.predicateForKeyPath("downloaded_file_path", exactValue: path)
                            _ = self.curriculumPlannerDataManager.updateEntity(CPMConstants.Entity.CURRICULUM, filteredByPredicate: predicate, withData: ["downloaded_file_path": ""])
                        }
                        else {
                            let message = NSLocalizedString("There was an error deleting this file. Please try again later.", comment: "")
                            self.showNotificationMessage(message)
                            self.collectionView.reloadData()
                        }
                    })
                    
                    alert.dismiss(animated: true, completion: nil)
                })
            }
            
            alert.addAction(cancelAction)
            alert.addAction(deleteAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showNotificationMessage(_ message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: NSLocalizedString("Okay", comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    func closeButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - File Object Details
    
    func getDetailsOfFileObject(_ fileObject: NSManagedObject) -> String {
        let name = fileObject.value(forKey: "name") as! String
        let size = fileObject.value(forKey: "size") as! UInt
        let date = fileObject.value(forKey: "date") as! Date
        
        let sizeString = self.sandboxHelper.formattedStringSize(size)
        let dateString = self.stringLongStyleDateInDefaultTimeZone(date)
        
        return "\(name) (\(sizeString)) \(NSLocalizedString("was downloaded last", comment: "")) \(dateString)"
    }
    
    func stringLongStyleDateInDefaultTimeZone(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .medium
        return dateFormatter.string(from: date)
    }
    
    // MARK: - Empty Placeholder View
    
    func shouldShowEmptyPlaceholderView(_ show: Bool) {
        if (show) {
            let message = NSLocalizedString("No available file.", comment: "")
            self.emptyPlaceholderLabel.text = message
        }
        
        self.emptyPlaceholderView.isHidden = !show
        self.collectionView.isHidden = show
    }
    
    // MARK: - Collection View Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let fileObject = self.fetchedResultsController.object(at: indexPath) 
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: self.kFileCellIdentifier, for: indexPath) as! CPMFileCollectionViewCell
        
        let name = fileObject.value(forKey: "name") as! String
        cell.fileName.text = name
        
        let filePath = fileObject.value(forKey: "path") as! String
        let fileURL = URL(fileURLWithPath: filePath)
        cell.setIconOfFileWithExtension(fileURL.pathExtension)
        
        cell.highlightSelectedCell(indexPath == self.selectedCellIndexPath)
        
        return cell
    }
    
    // MARK: - Collection View Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CPMFileCollectionViewCell
        
        if (self.selectedCellIndexPath != nil) {
            let previouslySelectedCell = self.collectionView.cellForItem(at: self.selectedCellIndexPath!) as! CPMFileCollectionViewCell
            previouslySelectedCell.highlightSelectedCell(false)
        }
        
        cell.highlightSelectedCell(true)
        self.selectedCellIndexPath = indexPath
        
        let fileObject = self.fetchedResultsController.object(at: indexPath) 
        
        if (self.isUploadAction) {
            let fileName = fileObject.value(forKey: "name") as! String
            let filePath = fileObject.value(forKey: "path") as! String
            let fileObject = ["fileName": fileName, "filePath": filePath]
            
            self.dismiss(animated: true, completion: {
                self.delegate?.selectedFileObject(fileObject as NSDictionary)
            })
        }
        else {
            self.openFileObject(fileObject)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CPMFileCollectionViewCell
        cell.highlightSelectedCell(false)
        self.selectedCellIndexPath = nil
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: CPMConstants.Entity.FILE)
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let context = self.curriculumPlannerDataManager.mainContext
        
        //let fetchRequest = NSFetchRequest()
        //let entity = NSEntityDescription.entity(forEntityName: CPMConstants.Entity.FILE, in: context)
        
        let predicate = self.curriculumPlannerDataManager.predicateForKeyPath("sdui", exactValue: self.subDirectoryName)
        fetchRequest.predicate = predicate
        
        //fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20
        
        let date = NSSortDescriptor(key: "date", ascending: false)
        fetchRequest.sortDescriptors = [date]
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context!, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch _ as NSError {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    fileprivate func addProcessingBlock(_ processingBlock:@escaping (Void)->Void) {
        self.blockOperations.append(BlockOperation(block: processingBlock))
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.blockOperations.removeAll(keepingCapacity: false)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.insertItems(at: [newIndexPath]) }
            
        case .update:
            guard let newIndexPath = newIndexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.reloadItems(at: [newIndexPath]) }
            
        case .move:
            guard let indexPath = indexPath else { return }
            guard let newIndexPath = newIndexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.moveItem(at: indexPath, to: newIndexPath) }
            
        case .delete:
            guard let indexPath = indexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.deleteItems(at: [indexPath]) }
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.addProcessingBlock { [weak self] in self?.collectionView.insertSections(IndexSet(integer: sectionIndex)) }
            
        case .update:
            self.addProcessingBlock { [weak self] in self?.collectionView.reloadSections(IndexSet(integer: sectionIndex)) }
            
        case .delete:
            self.addProcessingBlock { [weak self] in self?.collectionView.deleteSections(IndexSet(integer: sectionIndex)) }
            
        default: break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.collectionView.performBatchUpdates({
            self.blockOperations.forEach { $0.start() }
            }, completion: { finished in
                self.blockOperations.removeAll(keepingCapacity: false)
        })
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.collectionView!.reloadData()
        
        do {
            try self.fetchedResultsController.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Document Interaction Controller Delegate
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }
    
}
