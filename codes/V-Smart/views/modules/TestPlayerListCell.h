//
//  TestPlayerListCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/4/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestPlayerListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *courseTitle;
@property (strong, nonatomic) IBOutlet UILabel *courseDescription;
@property (strong, nonatomic) IBOutlet UILabel *courseSchedule;
@property (strong, nonatomic) IBOutlet UILabel *courseRoom;
@end
