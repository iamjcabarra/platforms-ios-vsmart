//
//  CALEventViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 13/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class CALEventViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NSFetchedResultsControllerDelegate, UISearchBarDelegate {
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var collectionView: UICollectionView!
    
    fileprivate var dateHelper = DateHelper()
    fileprivate let notification = NotificationCenter.default
    
    fileprivate var blockOperations: [BlockOperation] = []
    fileprivate let cellIdentifier = "event_cell_identifier"
    fileprivate let eventModalViewSegueIdentifier = "SHOW_EVENT_MODAL_VIEW"
    
    fileprivate var addImage: UIImage!
    fileprivate var deleteImage: UIImage!
    fileprivate var isLandscape: Bool = false
    fileprivate var dateFilter: NSDate!
    fileprivate var searchKey = ""
    fileprivate var currentUser = ""
    
    // MARK: - Shared Data Manager
    
    fileprivate lazy var dataManager: CALDataManager = {
        let cmdm = CALDataManager.sharedInstance
        return cmdm
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Current user
        self.currentUser = self.dataManager.accountUserID()
        
        // Set action images
        self.addImage = UIImage(named: "addIcon")
        self.deleteImage = UIImage(named: "deleteIcon")
    
        // Start notifications
        self.setUpNotifications()
        
        // Render header view (current date)
        self.updateHeaderView(forDate: Date())
        
        // Initialize date filter (current date)
        self.updateFilter(forDate: Date())
        
        // Custom search bar
        self.searchBar.placeholder = NSLocalizedString("Search Event", comment: "")
        self.searchBar.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Destroy notifications
        self.tearDownNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Notifications
    
    fileprivate func setUpNotifications() {
        let notificationSelectorA = #selector(self.respondToDeviceOrientationChange(_:))
        let notificationNameA = NSNotification.Name(rawValue: "CAL_NOTIFICATION_DEVICE_ORIENTATION_CHANGE")
        self.notification.addObserver(self, selector: notificationSelectorA, name: notificationNameA, object: nil)
        
        let notificationSelectorB = #selector(self.respondToDaySelection(_:))
        let notificationNameB = NSNotification.Name(rawValue: "CAL_NOTIFICATION_DAY_SELECTION")
        self.notification.addObserver(self, selector: notificationSelectorB, name: notificationNameB, object: nil)
    }
    
    fileprivate func tearDownNotifications() {
        let notificationNameA = NSNotification.Name(rawValue: "CAL_NOTIFICATION_DEVICE_ORIENTATION_CHANGE")
        self.notification.removeObserver(self, name: notificationNameA, object: nil)
        
        let notificationNameB = NSNotification.Name(rawValue: "CAL_NOTIFICATION_DAY_SELECTION")
        self.notification.removeObserver(self, name: notificationNameB, object: nil)
    }
    
    func respondToDeviceOrientationChange(_ notification: NSNotification) {
        guard let object = notification.object as? [String: String] else {
            print("Can't parse notification object!")
            return
        }
        
        let orientation = self.dataManager.stringValue(object["orientation"])!
        self.isLandscape = orientation == "1" ? true : false
        self.loadEventList()
    }
    
    func respondToDaySelection(_ notification: NSNotification) {
        guard let object = notification.object as? [String: Date] else {
            print("Can't parse notification object!")
            return
        }
        
        let date = object["date"]!
        self.updateHeaderView(forDate: date)
        self.updateFilter(forDate: date)
        
        DispatchQueue.main.async(execute: {
            self.reloadFetchedResultsController()
        })
    }
    
    // MARK: - Request Event List
    
    fileprivate func loadEventList() {
        let userID = self.dataManager.accountUserID()
        self.dataManager.requestCalendarEventList(forUser: userID) { (success) in
            DispatchQueue.main.async(execute: {
                self.reloadFetchedResultsController()
                
                // Post notification
                let name = Notification.Name(rawValue: "CAL_NOTIFICATION_EVENT_LIST_CHANGE")
                self.notification.post(name: name, object: nil)
            })
        }
    }
    
    // MARK: - Updating Header View
    
    fileprivate func updateHeaderView(forDate date: Date) {
        
        let dateString = self.dateHelper.dateString(fromDate: date,
                                                    outFormat: "MMM dd, yyyy",
                                                    setToLocalTimeZone: true)
        
        let dayString = self.dateHelper.dateString(fromDate: date,
                                                   outFormat: "EEEE",
                                                   setToLocalTimeZone: true)
        
        DispatchQueue.main.async(execute: {
            let today = NSLocalizedString("Today", comment: "")
            self.dateLabel.text = date.isDateInToday ? today.uppercased() : dateString.uppercased()
            self.dayLabel.text = dayString.uppercased()
        })
    }
    
    // MARK: - Updating Date Filter
    
    fileprivate func updateFilter(forDate date: Date) {
        
        let dateString = self.dateHelper.dateString(fromDate: date,
                                                    outFormat: "yyyy-MM-dd HH:mm:ss",
                                                    setToLocalTimeZone: true)
        
        self.dateFilter = self.dateHelper.date(fromString: dateString) as NSDate!
        
    }
    
    // MARK: - Collection View Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else { return 0 }
        return sectionCount
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else { return 0 }
        return sectionData.numberOfObjects
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath) as! CALEventCollectionViewCell
        let mo = fetchedResultsController.object(at: indexPath)
        
        let user_id = self.dataManager.stringValue(mo.value(forKey: "user_id"))!
        let title = self.dataManager.stringValue(mo.value(forKey: "title"))!
        let event_start = self.dataManager.stringValue(mo.value(forKey: "event_start"))!
        let is_add_event = self.dataManager.stringValue(mo.value(forKey: "is_add_event"))!
        let background_color = self.dataManager.stringValue(mo.value(forKey: "background_color"))!
     
        cell.eventLabel.text = title
        cell.eventStartDateLabel.text = self.dateHelper.dateString(event_start,
                                                                   fromFormat: "yyyy-MM-dd HH:mm:ss",
                                                                   toFormat: "MM/dd/yyyy hh:mm a",
                                                                   fromTimeZone: "UTC",
                                                                   setToLocalTimeZone: true)
        cell.eventActionImage.image = is_add_event == "0" ? self.deleteImage : self.addImage
        cell.eventColorView.backgroundColor = UIColor(hexString: background_color)
        cell.eventActionButton.addTarget(self, action: #selector(self.cellButtonAction(_:)), for: .touchUpInside)
        
        cell.eventActionImage.isHidden = (user_id != self.currentUser) ? true : false
        cell.eventActionButton.isHidden = (user_id != self.currentUser) ? true : false
        
        return cell
    }
    
    // MARK: - Collectin View Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        let id = self.dataManager.stringValue(mo.value(forKey: "id"))!
        
        self.dataManager.deepCopyEvent(mo, doneBlock: { (success) in
            if success {
                if let copy = self.dataManager.fetchDeepCopiedEventObject(withID: id) {
                    DispatchQueue.main.async(execute: {
                        let data = ["eventObject": copy, "isCreate": "0"] as [String : Any]
                        self.performSegue(withIdentifier: self.eventModalViewSegueIdentifier, sender: data)
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        let message = NSLocalizedString("There was an error processing your request.", comment: "")
                        self.view.makeToast(message: message, duration: 0.3, position: "center" as AnyObject)
                    })
                }
            }
            else {
                DispatchQueue.main.async(execute: {
                    let message = NSLocalizedString("There was an error processing your request.", comment: "")
                    self.view.makeToast(message: message, duration: 0.3, position: "center" as AnyObject)
                })
            }
        })
    }
    
    // MARK: Collection View Flow Layout Delegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let numberOfRows = self.isLandscape == true ? 1 : 2
        let totalSpace = flowLayout.sectionInset.left + flowLayout.sectionInset.right + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfRows - 1))
        let width = (collectionView.bounds.width - totalSpace) / CGFloat(numberOfRows)
        return CGSize(width: width, height: 65.0)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
    
    // MARK: - Event Cell Button Action Event Handler
    
    func cellButtonAction(_ sender: UIButton) {
        guard let mo = self.managedObjectFromButton(sender, inCollectionView: self.collectionView) else {
            print("Error: Can't fetch event object!")
            return
        }
        
        let is_add_event = self.dataManager.stringValue(mo.value(forKey: "is_add_event"))!
        let title = self.dataManager.stringValue(mo.value(forKey: "title"))!
        let id = self.dataManager.stringValue(mo.value(forKey: "id"))!
        let user_id = self.dataManager.stringValue(mo.value(forKey: "user_id"))!
        
        if is_add_event == "1" {
            self.dataManager.deepCopyEvent(mo, doneBlock: { (success) in
                
                if success {
                    if let copy = self.dataManager.fetchDeepCopiedEventObject(withID: id) {
                        DispatchQueue.main.async(execute: {
                            let data = ["eventObject": copy, "isCreate": "1"] as [String : Any]
                            self.performSegue(withIdentifier: self.eventModalViewSegueIdentifier, sender: data)
                        })
                    }
                    else {
                        DispatchQueue.main.async(execute: {
                            let message = NSLocalizedString("There was an error processing your request.", comment: "")
                            self.view.makeToast(message: message, duration: 0.3, position: "center" as AnyObject)
                        })
                    }
                }
                else {
                    DispatchQueue.main.async(execute: {
                        let message = NSLocalizedString("There was an error processing your request.", comment: "")
                        self.view.makeToast(message: message, duration: 0.3, position: "center" as AnyObject)
                    })
                }
            })
        }
        else {
            let avTitle = NSLocalizedString("Delete", comment: "").uppercased()
            let message = NSLocalizedString("Are you sure you want to delete", comment: "")
            let alert = UIAlertController(title: avTitle, message: "\(message) \"\(title)\"?", preferredStyle: .alert)
            
            let actionA = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel) { (Alert) -> Void in
                DispatchQueue.main.async(execute: {
                    alert.dismiss(animated: true, completion: nil)
                })
            }
            
            let actionB = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { (Alert) -> Void in
                  self.dataManager.requestDeleteEvent(id, forUser: user_id, doneBlock: { (success) in
                    DispatchQueue.main.async(execute: {
                        if success {
                            self.loadEventList()
                        }
                        else {
                            let error = NSLocalizedString("There was an error deleting event. Please try again later.", comment: "")
                            self.view.makeToast(message: error, duration: 0.3, position: "Center" as AnyObject)
                        }
                        
                        alert.dismiss(animated: true, completion: nil)
                    })
                })
            }
            
            alert.addAction(actionA)
            alert.addAction(actionB)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    fileprivate func managedObjectFromButton(_ button: UIButton, inCollectionView: UICollectionView) -> NSManagedObject? {
        let buttonPosition = button.convert(CGPoint.zero, to: inCollectionView)
        guard let indexPath = inCollectionView.indexPathForItem(at: buttonPosition) else { return nil }
        let managedObject = self.fetchedResultsController.object(at: indexPath)
        return managedObject
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let context = self.dataManager.mainContext
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: CALConstants.Entity.EVENT)
        fetchRequest.fetchBatchSize = 20
        
        let predicateA = NSPredicate(format: "(%@ >= event_start_date) AND (%@ <= event_end_date)", self.dateFilter, self.dateFilter)
        let predicateB = NSPredicate(format: "search_key == 'C02MM6N0FH01'")
        var predicateC = NSCompoundPredicate(orPredicateWithSubpredicates: [predicateA, predicateB])
        
        if self.searchKey != "" {
            let predicateD = NSComparisonPredicate(keyPath: "search_key", withValue: self.searchKey, isExact: false)
            predicateC = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateC, predicateD])
        }
        
        fetchRequest.predicate = predicateC
        
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                   managedObjectContext: context(),
                                                                   sectionNameKeyPath: nil,
                                                                   cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch _ as NSError {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    fileprivate func addProcessingBlock(_ processingBlock:@escaping (Void)->Void) {
        self.blockOperations.append(BlockOperation(block: processingBlock))
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.blockOperations.removeAll(keepingCapacity: false)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.insertItems(at: [newIndexPath]) }
            
        case .update:
            guard let newIndexPath = newIndexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.reloadItems(at: [newIndexPath]) }
            
        case .move:
            guard let indexPath = indexPath else { return }
            guard let newIndexPath = newIndexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.moveItem(at: indexPath, to: newIndexPath) }
            
        case .delete:
            guard let indexPath = indexPath else { return }
            self.addProcessingBlock { [weak self] in self?.collectionView.deleteItems(at: [indexPath]) }
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.addProcessingBlock { [weak self] in self?.collectionView.insertSections(IndexSet(integer: sectionIndex)) }
            
        case .update:
            self.addProcessingBlock { [weak self] in self?.collectionView.reloadSections(IndexSet(integer: sectionIndex)) }
            
        case .delete:
            self.addProcessingBlock { [weak self] in self?.collectionView.deleteSections(IndexSet(integer: sectionIndex)) }
            
        default: break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.collectionView.performBatchUpdates({
            self.blockOperations.forEach { $0.start() }
        }, completion: { finished in
            self.blockOperations.removeAll(keepingCapacity: false)
        })
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.collectionView.reloadData()
        
        var error: NSError? = nil
        
        do {
            try self.fetchedResultsController.performFetch()
        }
        catch let error1 as NSError {
            error = error1
            print(error?.localizedDescription ?? "")
        }
    }
    
    // MARK: - Search Bar Delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchKey = searchText
        self.reloadFetchedResultsController()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchKey = searchBar.text!
        self.reloadFetchedResultsController()
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = self.searchKey
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.eventModalViewSegueIdentifier {
            guard let data = sender as? [String: Any], let object = data["eventObject"] as? NSManagedObject, let isCreate = data["isCreate"] as? String else { return }
            let eventModalView = segue.destination as! CALCreateEventModalViewController
            eventModalView.isCreate = isCreate
            eventModalView.eventObject = object
        }

    }

}
