//
//  SharedToViewController.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 20/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SharedToViewController : UIViewController

@property (nonatomic, strong) NSArray *sharedArray;

@end
