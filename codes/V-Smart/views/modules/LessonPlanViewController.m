//
//  LessonPlanViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 9/9/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "LessonPlanViewController.h"
#import "LessonPlanChildViewController.h"

@interface LessonPlanViewController ()
@property (nonatomic, strong) LessonPlanChildViewController *containerView;
@end

@implementation LessonPlanViewController
@synthesize containerView;

-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self setupChildViewController];
    
    [self _setupNotifications];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
}

-(void)setupChildViewController{
    
    float profileY = [super profileHeight];
    float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    //float gridHeight = 638 + [super profileSize].size.height;
    
    float gridY = [super headerSize].size.height + profileY;
    
    containerView = [[LessonPlanChildViewController alloc] init];
    containerView.view.frame = CGRectMake(0, gridY - 20, self.view.frame.size.width, gridHeight + 70);
    
    containerView.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	containerView.view.autoresizesSubviews = YES;
    
    [self addChildViewController:containerView];
    
    [self.view addSubview:containerView.view];
    [containerView didMoveToParentViewController:self];
    
    [self.view sendSubviewToBack:containerView.view];
}


-(void) _setupNotifications {
    VS_NCADD(kNotificationProfileHeight, @selector(_refreshController:))
}

- (NSString *) dashboardName {
    
    NSString *moduleName = NSLocalizedString(@"Lesson Plan", nil);
    return moduleName;

//    return kModuleLessonPlan;
}

#pragma mark - Post Notification Events

-(void) _refreshController: (NSNotification *) notification{
    VLog(@"Received: kNotificationProfileHeight");
    
    [UIView animateWithDuration:0.45 animations:^{
        float profileY = [super profileHeight];
        float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
        //float gridHeight = 638 + [super profileSize].size.height;
        
        float gridY = [super headerSize].size.height + profileY;
        [self.containerView.view setFrame:CGRectMake(0, gridY - 20, self.view.frame.size.width, gridHeight + 70)];
        [self.containerView.view setNeedsLayout];
        
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            [super adjustProfileHeight];
        }
        [super showOrHideMiniAvatar];
    } completion:^(BOOL finished) {
        
    }];
}



-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [SVProgressHUD dismiss];
}

@end
