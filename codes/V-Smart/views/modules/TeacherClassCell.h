//
//  TeacherClassCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 4/20/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeacherClassCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *sectionLabel;

- (void)highlight:(BOOL)flag;

@end
