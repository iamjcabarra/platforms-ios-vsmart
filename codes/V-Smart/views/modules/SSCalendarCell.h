//
//  SSCalendarCell.h
//  V-Smart
//
//  Created by VhaL on 3/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSCalendarCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *date;
@property (nonatomic, strong) IBOutlet UILabel *detail;
@property (nonatomic, strong) IBOutlet UILabel *duration;
@property (nonatomic, strong) IBOutlet UILabel *timeCoverage;
@end
