//
//  CALCreateEventTableViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 22/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class CALCreateEventTableViewController: UITableViewController, UITextFieldDelegate, UITextViewDelegate, LPMDatePickerPopoverDelegate {
    
    @IBOutlet var eventNameLabel: UILabel!
    @IBOutlet var startDateLabel: UILabel!
    @IBOutlet var endDateLabel: UILabel!
    @IBOutlet var colorLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    @IBOutlet var eventNameTextField: UITextField!
    @IBOutlet var startDateTextField: UITextField!
    @IBOutlet var endDateTextField: UITextField!
    @IBOutlet var descriptionTextView: UITextView!
    
    @IBOutlet var colorAView: UIView!
    @IBOutlet var colorBView: UIView!
    @IBOutlet var colorCView: UIView!
    @IBOutlet var colorDView: UIView!
    @IBOutlet var colorEView: UIView!
    @IBOutlet var colorFView: UIView!
    
    @IBOutlet var sdButton: UIButton!
    @IBOutlet var edButton: UIButton!
    
    @IBOutlet var colorAButton: UIButton!
    @IBOutlet var colorBButton: UIButton!
    @IBOutlet var colorCButton: UIButton!
    @IBOutlet var colorDButton: UIButton!
    @IBOutlet var colorEButton: UIButton!
    @IBOutlet var colorFButton: UIButton!
    
    @IBOutlet var postImage: UIImageView!
    @IBOutlet var optionAImage: UIImageView!
    @IBOutlet var optionBImage: UIImageView!
    @IBOutlet var optionCImage: UIImageView!
    
    @IBOutlet var postButton: UIButton!
    @IBOutlet var optionAButton: UIButton!
    @IBOutlet var optionBButton: UIButton!
    @IBOutlet var optionCButton: UIButton!
   
    var isCreate = ""
    var eventObject: NSManagedObject!
    
    fileprivate var showPostingOption = false
    fileprivate var showPostOptionA = true
    fileprivate var showPostOptionB = false
    fileprivate var showPostOptionC = false
    fileprivate var eventID = 0
    
    fileprivate let selectedViewColor = UIColor.black
    fileprivate let unselectedViewColor = UIColor.white
    
    fileprivate let checkImage = UIImage(named: "checkImage")
    fileprivate let uncheckImage = UIImage(named: "uncheckImage")
    fileprivate let onRadioButtonImage = UIImage(named: "onRadioButtonImage")
    fileprivate let offRadioButtonImage = UIImage(named: "offRadioButtonImage")
    
    fileprivate let notification = NotificationCenter.default
    fileprivate var datePickerView: LPMDatePickerPopover!
    
    fileprivate var timeFrameButtonTag = 100
    
    // MARK: - Shared Data Manager
    
    fileprivate lazy var dataManager: CALDataManager = {
        let cmdm = CALDataManager.sharedInstance
        return cmdm
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Color Views
        let views: [UIView] = [self.colorAView, self.colorBView, self.colorCView, self.colorDView, self.colorEView, self.colorFView]
        self.dropShadow(toViews: views)
        self.selectColorView(withTag: 100)
        
        // Event Colors
        let toggleColorButtonAction = #selector(self.toggleColorButton(_:))
        self.colorAButton.addTarget(self, action: toggleColorButtonAction, for: .touchUpInside)
        self.colorBButton.addTarget(self, action: toggleColorButtonAction, for: .touchUpInside)
        self.colorCButton.addTarget(self, action: toggleColorButtonAction, for: .touchUpInside)
        self.colorDButton.addTarget(self, action: toggleColorButtonAction, for: .touchUpInside)
        self.colorEButton.addTarget(self, action: toggleColorButtonAction, for: .touchUpInside)
        self.colorFButton.addTarget(self, action: toggleColorButtonAction, for: .touchUpInside)
        
        // Post Options
        let toggleOptionButtonAction = #selector(self.toggleOptionButton(_:))
        self.optionAButton.addTarget(self, action: toggleOptionButtonAction, for: .touchUpInside)
        self.optionBButton.addTarget(self, action: toggleOptionButtonAction, for: .touchUpInside)
        self.optionCButton.addTarget(self, action: toggleOptionButtonAction, for: .touchUpInside)
        
        // School Stream
        let togglePostButtoAction = #selector(self.togglePostButton(_:))
        self.postButton.addTarget(self, action: togglePostButtoAction, for: .touchUpInside)
        
        // Text field delegate
        self.eventNameTextField.delegate = self
        self.startDateTextField.delegate = self
        self.endDateTextField.delegate = self
        self.descriptionTextView.delegate = self
        
        // Date picker
        self.setupDatePickerView()
        
        // String localization
        self.eventNameLabel.text = NSLocalizedString("Event Name", comment: "")
        self.startDateLabel.text = NSLocalizedString("Start Date", comment: "")
        self.endDateLabel.text = NSLocalizedString("End Date", comment: "")
        self.colorLabel.text = NSLocalizedString("Color", comment: "")
        self.descriptionLabel.text = NSLocalizedString("Description", comment: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.renderContents()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Render Contents
    
    fileprivate func renderContents() {
        guard let mo = self.eventObject else {
            print("Error: Event object is nil!")
            DispatchQueue.main.async(execute: { self.dismiss(animated: true, completion: nil) })
            return
        }
        
        guard
            let id = mo.value(forKey: "id") as? Int,
            let title = mo.value(forKey: "title") as? String,
            let details = mo.value(forKey: "details") as? String,
            let event_start = mo.value(forKey: "event_start") as? String,
            let event_end = mo.value(forKey: "event_end") as? String
            else {
                print("Error: Can't parse event object!")
                DispatchQueue.main.async(execute: { self.dismiss(animated: true, completion: nil) })
                return
        }
        
        self.eventNameTextField.text = self.isCreate == "1" ? "" : title
        self.descriptionTextView.text = details
        self.startDateTextField.text = event_start
        self.endDateTextField.text = event_end
        
        self.eventID = id
    }
    
    // MARK: - Text Field Key Creator
    
    fileprivate func key(forTextField textField: UITextField) -> String {
        var key = ""
        
        switch textField {
        case self.eventNameTextField:
            key = "title"
            break
        case self.startDateTextField:
            key = "event_start"
            break
        case self.endDateTextField:
            key = "event_end"
            break
        default:
            key = ""
        }
        
        return key
    }
    
    fileprivate func key(forTextView textView: UITextView) -> String {
        var key = ""
        
        switch textView {
        case self.descriptionTextView:
            key = "details"
            break
        default:
            key = ""
        }
        
        return key
    }
    
    // MARK: - Text Field Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
        
        // Maximum of 50 characters only for lesson title
        if (textField == self.eventNameTextField) {
            if (newText.length > 50) {
                return false
            }
        }
        
        // Handle backspace event
        if ((string as NSString).length == 0) {
            let key = self.key(forTextField: textField)
            let data = [key: newText]
            self.saveChangedData(data, doneBlock: { (success) in })
        }
        
        // Saving local changes
        let key = self.key(forTextField: textField)
        let data = [key: newText]
        self.saveChangedData(data, doneBlock: { (success) in })
        
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let key = self.key(forTextView: textView)
        let data = [key: textView.text as NSString]
        self.saveChangedData(data, doneBlock: { (success) in })
    }
    
    // MARK: - Button Event Handlers
    
    func toggleColorButton(_ sender: UIButton) {
        self.selectColorView(withTag: sender.tag)
    }
    
    func togglePostButton(_ sender: UIButton) {
        self.showPostingOption = self.showPostingOption ? false : true
        self.showPostOptionA = self.showPostingOption ? self.showPostOptionA : true
        self.showPostOptionC = self.showPostingOption ? self.showPostOptionC : false
        
        if self.showPostingOption {
            let user = self.dataManager.accountUserID()
            self.dataManager.requestGroupList(forUser: user) { (success) in }
        }
        
        DispatchQueue.main.async(execute: {
            self.postImage.image = self.showPostingOption ? self.checkImage : self.uncheckImage
            self.optionAImage.image = self.onRadioButtonImage
            
            if self.showPostingOption == false {
                self.optionBImage.image = self.offRadioButtonImage
                self.optionCImage.image = self.offRadioButtonImage
            }
            
            self.tableView.reloadData()
            let data = ["post_flag": self.showPostingOption ? "1" : ""]
            self.saveChangedData(data as [String : NSString], doneBlock: { (success) in })
        })
    }
    
    func toggleOptionButton(_ sender: UIButton) {
        let tag = sender.tag
        
        if tag == 100 {
            self.showPostOptionA = true
            self.showPostOptionB = false
            self.showPostOptionC = false
            
            DispatchQueue.main.async(execute: {
                self.optionAImage.image = self.onRadioButtonImage
                self.optionBImage.image = self.offRadioButtonImage
                self.optionCImage.image = self.offRadioButtonImage
                self.tableView.reloadData()
                let data = ["post_flag": "1"]
                self.saveChangedData(data as [String : NSString], doneBlock: { (success) in })
            })
        }
        else if tag == 200 {
            self.showPostOptionB = true
            self.showPostOptionA = false
            self.showPostOptionC = false
            
            DispatchQueue.main.async(execute: {
                self.optionBImage.image = self.onRadioButtonImage
                self.optionAImage.image = self.offRadioButtonImage
                self.optionCImage.image = self.offRadioButtonImage
                self.tableView.reloadData()
                let data = ["post_flag": "2"]
                self.saveChangedData(data as [String : NSString], doneBlock: { (success) in })
            })
        }
        
        if tag == 300 {
            self.showPostOptionC = true
            self.showPostOptionA = false
            self.showPostOptionB = false
            
            let user = self.dataManager.accountUserID()
            self.dataManager.requestGroupList(forUser: user) { (success) in
                DispatchQueue.main.async(execute: {
                    self.optionCImage.image = self.onRadioButtonImage
                    self.optionAImage.image = self.offRadioButtonImage
                    self.optionBImage.image = self.offRadioButtonImage
                    self.tableView.reloadData()
                    let data = ["post_flag": "3"]
                    self.saveChangedData(data as [String : NSString], doneBlock: { (success) in })
                })
            }
        }
    }
    
    // MARK: - Lesson Date Picker
    
    func setupDatePickerView() {
        self.sdButton.tag = 100
        self.edButton.tag = 200
        
        let datePickerViewAction = #selector(self.showDatePickerView(_:))
        self.sdButton.addTarget(self, action: datePickerViewAction, for: .touchUpInside)
        self.edButton.addTarget(self, action: datePickerViewAction, for: .touchUpInside)
    }
    
    func showDatePickerView(_ sender: UIButton) {
        self.timeFrameButtonTag = sender.tag
        
        self.datePickerView = LPMDatePickerPopover.init(nibName: "LPMDatePickerPopover", bundle: nil)
        self.datePickerView.headerTitle = ""
        self.datePickerView.datePickerMode = UIDatePickerMode.dateAndTime
        self.datePickerView.dateFormat = CALConstants.DateFormat.SERVER
        self.datePickerView.delegate = self
        
        self.datePickerView.modalPresentationStyle = .popover
        self.datePickerView.preferredContentSize = CGSize(width: 300.0, height: 200.0)
        self.datePickerView.popoverPresentationController?.permittedArrowDirections = .any
        self.datePickerView.popoverPresentationController?.sourceView = sender
        self.datePickerView.popoverPresentationController?.sourceRect = sender.bounds
        self.present(self.datePickerView, animated: true, completion: nil)
    }
    
    func selectedDateString(_ dateString: String) {
        var textField = self.startDateTextField
        
        // Start date
        if (self.timeFrameButtonTag == 100) {
            self.startDateTextField.text = dateString
            textField = self.startDateTextField
        }
        
        // End date
        if (self.timeFrameButtonTag == 200) {
            self.endDateTextField.text = dateString
            textField = self.endDateTextField
        }
        
        // Saving local changes
        let key = self.key(forTextField: textField!)
        let data = [key: (dateString as NSString)]
        self.saveChangedData(data, doneBlock: { (success) in })
    }
    
    // MARK: - Updating UI Controls
    
    fileprivate func dropShadow(toViews views: [UIView]) {
        for view in views {
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 1
            view.layer.shadowOffset = CGSize.zero
            view.layer.shadowRadius = 10
            view.layer.shouldRasterize = true
        }
    }
    
    fileprivate func selectColorView(withTag tag: Int) {
        self.colorAView.backgroundColor = self.colorAView.tag == tag ? self.selectedViewColor : self.unselectedViewColor
        self.colorBView.backgroundColor = self.colorBView.tag == tag ? self.selectedViewColor : self.unselectedViewColor
        self.colorCView.backgroundColor = self.colorCView.tag == tag ? self.selectedViewColor : self.unselectedViewColor
        self.colorDView.backgroundColor = self.colorDView.tag == tag ? self.selectedViewColor : self.unselectedViewColor
        self.colorEView.backgroundColor = self.colorEView.tag == tag ? self.selectedViewColor : self.unselectedViewColor
        self.colorFView.backgroundColor = self.colorFView.tag == tag ? self.selectedViewColor : self.unselectedViewColor
        
        var background_color: NSString = ""
    
        switch tag {
            
        case 100: background_color = "#41AFD8"
            break
        case 200: background_color = "#8DBF57"
            break
        case 300: background_color = "#8D7BB6"
            break
        case 400: background_color = "#D74656"
            break
        case 500: background_color = "#F3BA4D"
            break
        default: background_color = "#FFFFFF"
            
        }
        
        let data = ["background_color": background_color]
        self.saveChangedData(data, doneBlock: { (success) in
            if success {
                DispatchQueue.main.async(execute: {
                    let name = Notification.Name(rawValue: "CAL_NOTIFICATION_EVENT_COLOR_CHANGE")
                    self.notification.post(name: name, object: nil)
                })
            }
        })
    }
    
    // MARK: - Table View Data Source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    // MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let row = indexPath.row
        var height: CGFloat = 75.0
        
        if row == 2 { height = 130.0 }
        if row == 3 { height = self.showPostingOption ? 105.0 : 0.0 }
        if row == 4 { height = self.showPostOptionC ? 105.0 : 0.0 }
        if row == 5 { height = 150.0 }
        
        return height
    }
    
    // MARK: - Saving Local Changes
    
    fileprivate func saveChangedData(_ data: [String: NSString], doneBlock: @escaping (_ success: Bool) -> Void) {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            let predicate = NSComparisonPredicate(keyPath: "id", withValue: self.eventID, isExact: true)
            let result = self.dataManager.updateEntity(CALConstants.Entity.EVENT_COPY, predicate: predicate, data: data as NSDictionary)
            doneBlock(result)
        }
    }
    
}
