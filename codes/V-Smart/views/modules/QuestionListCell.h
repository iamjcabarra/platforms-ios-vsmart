//
//  QuestionListCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/16/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *questionNumberLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;

@end
