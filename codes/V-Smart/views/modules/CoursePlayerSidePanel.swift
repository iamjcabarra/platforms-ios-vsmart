//
//  CoursePlayerSidePanel.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 29/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol CoursePlayerSidePanelDelegate: class {
    func didFinishSelectingObject(_ questionid:String, indexPath:IndexPath)
}

class CoursePlayerSidePanel: UICollectionViewController, UICollectionViewDelegateFlowLayout, NSFetchedResultsControllerDelegate, UIViewControllerTransitioningDelegate {
    
    weak var delegate:CoursePlayerSidePanelDelegate?
    
    var totalItems: NSInteger? = 0
    
    // CELL IDENTIFIER
    let cellID = "TGTB_SIDE_PANEL_CELL_IDENTIFIER"
    
    // HEADER IDENTIFIER
    let headerID = "TGTB_SIDE_PANEL_HEADER_IDENTIFIER"
    
    // MARK: - Shared Data Manager
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tm = TestGuruDataManager.sharedInstance()
        return tm!
    }()
    
    // MARK: - Designated Initializer
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!)  {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.commonInit()
    }
    
    fileprivate func commonInit() {
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            totalItems = 0
            return totalItems!
        }
        totalItems = sectionData.numberOfObjects
        return totalItems!;
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath)
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ collectionCell: UICollectionViewCell!, atIndexPath indexPath: IndexPath) {
        
        let cell = collectionCell as! CoursePlayerSidePanelCell
        
        let mo = fetchedResultsController.object(at: indexPath)
        
        let questionID = mo.value(forKey: "question_id") as! String
        
        let is_visited = mo.value(forKey: "is_visited") as! String
        
        let predicate = self.dataManager.predicate(forKeyPath: "question_id", object: questionID)
        let check = self.dataManager.getObjectsForEntity(kCoursePlayerQuestionChoiceEntity, predicate: predicate) as! [NSManagedObject]
        
        var answered : Bool = false
        for c_mo in check {
            
            let answer = c_mo.value(forKey: "answer") as! String
            
            if answer == "1" {
                answered = true
                break;
            }
            
        }
        
        cell.backView.backgroundColor = UIColor.white//UIColor(hex6: 0x68bf61)
        cell.textLabel.textColor = UIColor.blue
        
        if is_visited == "0" {
            cell.backView.backgroundColor = UIColor.lightGray
            cell.textLabel.textColor = UIColor.white
        }
        
        if answered {
            cell.backView.backgroundColor = UIColor(hex6: 0x68bf61)
            cell.textLabel.textColor = UIColor.white
        }
        
        let bullet_number = "\((indexPath as NSIndexPath).row + 1)"
        
        cell.textLabel.text = bullet_number
    }
    
    
    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
        UIView.animate(withDuration: 0.25, delay: 0.2, options: .curveEaseOut, animations: { () -> Void in
            cell.layer.transform = CATransform3DMakeScale(1,1,1)
            }, completion: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        let question_id = mo.value(forKey: "question_id") as! String
        delegate?.didFinishSelectingObject(question_id, indexPath: indexPath)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String,at indexPath: IndexPath) ->UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionHeader {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader,
                                                                                   withReuseIdentifier:headerID,
                                                                                   for: indexPath) as! TGTBSidePanelReusableView
            headerView.customTitle.text = NSLocalizedString("QUESTION LIST", comment: "")
            
            return headerView
        } else {
            return UICollectionReusableView()
        }
    }
    
    /////////////////////////////////// NSFETCHRESULTSCONTROLLER ///////////////////////////////////
    
    /*
     NOTE: CUSTOM ANIMATION
     */
    // MARK: - Fetched results controller
    
    fileprivate lazy var customDescriptors: [NSSortDescriptor] = {
        
        let ascending = true
        
        let questionIndex = NSSortDescriptor(key: "index", ascending: ascending)
        
        return [questionIndex]
    }()
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: kCoursePlayerQuestionListEntity)
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        // Set ManagedObjectContext
        let ctx = dataManager.mainContext
        
        //let fetchRequest = NSFetchRequest(entityName: kCoursePlayerQuestionListEntity)
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        fetchRequest.sortDescriptors = customDescriptors
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest,
                                             managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    fileprivate var blockOperation = BlockOperation()
    fileprivate var shouldReloadCollectionView = false
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.shouldReloadCollectionView = false
        self.blockOperation = BlockOperation()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange sectionInfo: NSFetchedResultsSectionInfo,
                                     atSectionIndex sectionIndex: Int,
                                             for type: NSFetchedResultsChangeType) {
        
        let cv = self.collectionView
        switch type {
        case NSFetchedResultsChangeType.insert:
            self.blockOperation.addExecutionBlock({
                cv!.insertSections( IndexSet(integer: sectionIndex) )
            })
        case NSFetchedResultsChangeType.delete:
            self.blockOperation.addExecutionBlock({
                cv!.deleteSections( IndexSet(integer: sectionIndex) )
            })
        case NSFetchedResultsChangeType.update:
            self.blockOperation.addExecutionBlock({
                cv!.reloadSections( IndexSet(integer: sectionIndex ) )
            })
        default:()
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                                    at indexPath: IndexPath?,
                                                for type: NSFetchedResultsChangeType,
                                                              newIndexPath: IndexPath?) {
        
        let cv = self.collectionView
        
        switch type {
            
        case NSFetchedResultsChangeType.insert:
            
            if cv!.numberOfSections > 0 {
                
                if cv!.numberOfItems( inSection: (newIndexPath! as NSIndexPath).section ) == 0 {
                    self.shouldReloadCollectionView = true
                } else {
                    self.blockOperation.addExecutionBlock( {
                        cv!.insertItems( at: [newIndexPath!] )
                    } )
                }
                
            } else {
                self.shouldReloadCollectionView = true
            }
            
        case NSFetchedResultsChangeType.delete:
            
            if cv!.numberOfItems( inSection: (indexPath! as NSIndexPath).section ) == 1 {
                self.shouldReloadCollectionView = true
            } else {
                self.blockOperation.addExecutionBlock( {
                    cv!.deleteItems( at: [indexPath!] )
                } )
            }
            
        case NSFetchedResultsChangeType.update:
            self.blockOperation.addExecutionBlock({
                cv!.reloadItems( at: [indexPath!] )
            })
            
        case NSFetchedResultsChangeType.move:
            self.blockOperation.addExecutionBlock({
                cv!.moveItem( at: indexPath!, to: newIndexPath! )
            })
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        // Checks if we should reload the collection view to fix a bug @ http://openradar.appspot.com/12954582
        
        let cv = self.collectionView
        
        if self.shouldReloadCollectionView {
            cv!.reloadData()
        } else {
            cv!.performBatchUpdates({ self.blockOperation.start() }, completion: nil )
        }
    }
    
    // flow layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //        return UIEdgeInsetsMake(0, 0, 0, 0)
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.frame.size.width / 3) - 10
        let height = width
        
        //        let width = collectionView.frame.size.width
        //        let height = collectionView.frame.size.height
        
        return CGSize(width: width, height: height)
    }
    
    //    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
    //        print("---------------------------------------------> " + __FUNCTION__)
    //        resizeContents()
    //    }
    //
    //    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    //        print("---------------------------------------------> " + __FUNCTION__)
    //        resizeContents()
    //    }
    //
    //    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    //        print("---------------------------------------------> " + __FUNCTION__)
    //        resizeContents()
    //    }
    //
    //    private func resizeContents() {
    //
    //        print("---------------------------------------------> " + __FUNCTION__)
    //
    //        let layout = self.collection.collectionViewLayout as! UICollectionViewFlowLayout
    //        let size = self.collection.bounds.size
    //        let w = size.width
    //        let h = size.height
    //
    //        layout.itemSize = CGSizeMake(w, h)
    //        layout.invalidateLayout()
    //    }
    
    // MARK: - UIViewControllerTransitioningDelegate
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        if presented == self {
            return CustomPresentationController(presentedViewController: presented, presenting: presenting)
        }
        
        return nil
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if presented == self {
            return CustomPresentationAnimationController(isPresenting: true)
        }
        else {
            return nil
        }
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if dismissed == self {
            return CustomPresentationAnimationController(isPresenting: false)
        }
        else {
            return nil
        }
    }
    
    /////////////////////////////////// UIPresentationController ///////////////////////////////////
    
    /// Custom Presentation Controller
    // MARK: - Custom Presentation Controller
    class CustomPresentationController: UIPresentationController {
        
        /**
         Dimming View - Creates a Fullscreen view
         */
        lazy var dimmingView :UIView = {
            
            // ATTACHED A GESTURE RECOGNIZER
            let action = #selector(CustomPresentationController.dimmingViewTapped(_:))
            let tap = UITapGestureRecognizer(target: self, action: action)
            
            let view = UIView(frame: self.containerView!.bounds)
            view.addGestureRecognizer(tap)
            
            view.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
            view.alpha = 0.0
            
            return view
        }()
        
        /**
         Removes the Dimming View when the user taps
         - Parameter gesture: user tap gesture
         */
        func dimmingViewTapped(_ gesture: UIGestureRecognizer) {
            if gesture.state == UIGestureRecognizerState.recognized {
                self.presentingViewController.dismiss(animated: true, completion: nil)
            }
        }
        
        override func presentationTransitionWillBegin() {
            
            let containerView = self.containerView
            let presentedViewController = self.presentedViewController
            
            // Make sure the dimming view is the size of the container's bounds, and fully transparent
            dimmingView.frame = containerView!.bounds
            dimmingView.alpha = 0.0
            
            // Insert the dimming view below everything else
            containerView!.insertSubview(self.dimmingView, at:0)
            
            // Fade in the dimming view alongside the transition
            if let transitionCoordinator = presentedViewController.transitionCoordinator {
                transitionCoordinator.animate( alongsideTransition: {(context: UIViewControllerTransitionCoordinatorContext!) -> Void in
                    self.dimmingView.alpha = 1.0
                    }, completion:nil)
            }
            self.dimmingView.alpha = 1.0
        }
        
        override func dismissalTransitionWillBegin()  {
            // Fade out the dimming view alongside the transition
            if let transitionCoordinator = presentedViewController.transitionCoordinator {
                transitionCoordinator.animate(alongsideTransition: {(context: UIViewControllerTransitionCoordinatorContext!) -> Void in
                    self.dimmingView.alpha  = 0.0
                    }, completion:nil)
            }
            self.dimmingView.alpha = 0.0
        }
        
        override var adaptivePresentationStyle : UIModalPresentationStyle {
            return UIModalPresentationStyle.overFullScreen
        }
        
        override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
            var decrement = 1.25 as CGFloat
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                decrement = 3
            }
            
            let w = parentSize.width / decrement
            let h = parentSize.height
            
            return CGSize(width: w, height: h)
        }
        
        override func containerViewDidLayoutSubviews() {
            self.dimmingView.frame = self.containerView!.bounds
            self.presentedView?.frame = self.frameOfPresentedViewInContainerView
        }
        
        override var shouldPresentInFullscreen : Bool {
            return true
        }
        
        override var frameOfPresentedViewInContainerView : CGRect {
            var presentedViewFrame = CGRect.zero
            let containerBounds = self.containerView?.bounds
            presentedViewFrame.size = size(forChildContentContainer: (self.presentedViewController as UIContentContainer), withParentContainerSize: containerBounds!.size)
            presentedViewFrame.origin.x = 0
            
            return presentedViewFrame
        }
    }
    
    /////////////////////////////////// UIViewControllerAnimatedTransitioning ///////////////////////////////////
    
    /*
     NOTE: CUSTOM ANIMATION
     */
    
    // MARK: - Custom Animation Transition
    
    class CustomPresentationAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
        
        let isPresenting :Bool
        let duration :TimeInterval = 0.5
        
        init(isPresenting: Bool) {
            self.isPresenting = isPresenting
            super.init()
        }
        
        // ---- UIViewControllerAnimatedTransitioning methods
        
        // CHECKED 1
        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
            return self.duration
        }
        
        // CHECKED 2
        func animateTransition(using transitionContext: UIViewControllerContextTransitioning)  {
            
            guard
                let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
                let fromView = fromVC.view,
                let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
                let toView = toVC.view
                //let containerView = transitionContext.containerView
                else {
                    return
            }
            
            let containerView = transitionContext.containerView
            
            if isPresenting {
                containerView.addSubview(toView)
            }
            
            let animatingVC = isPresenting ? toVC : fromVC
            let animatingView = animatingVC.view
            
            let appearedFrame = transitionContext.finalFrame(for: animatingVC)
            var dismissedFrame = appearedFrame;
            
            dismissedFrame.origin.x -= dismissedFrame.size.width
            
            let initialFrame = isPresenting ? dismissedFrame : appearedFrame
            let finalFrame = isPresenting ? appearedFrame : dismissedFrame
            
            animatingView?.frame = initialFrame
            
            UIView.animate(withDuration: self.duration,
                                       delay: 0.0,
                                       usingSpringWithDamping: 1.0,
                                       initialSpringVelocity: 0.0,
                                       options: [.allowUserInteraction, .beginFromCurrentState],
                                       animations: { animatingView?.frame = finalFrame },
                                       completion: { (completed: Bool) -> Void in
                                        if !self.isPresenting { fromView.removeFromSuperview() }
                                        transitionContext.completeTransition(true)
            })
        }
    }
}
