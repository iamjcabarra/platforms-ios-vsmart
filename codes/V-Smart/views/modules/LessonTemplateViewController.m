//
//  LessonTemplateViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "LessonTemplateViewController.h"
#import "LessonTemplateCollectionViewCell.h"
#import "LessonPlanDataManager.h"
#import "UBDContentManager.h"
#import "LessonActionViewController.h"
#import "CreateLessonPlanView.h"
#import "LessonPlanConstants.h"
#import "VSmartValues.h"
#import "HUD.h"

@interface LessonTemplateViewController () <UICollectionViewDataSource, UICollectionViewDelegate> {
    NSArray *objects;
    NSManagedObject *mo_selected;
}

@property (nonatomic, strong) LessonPlanDataManager *lm;
@property (nonatomic, strong) UBDContentManager *sharedUBDContentManager;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSManagedObject *copiedLessonObject;

@end

@implementation LessonTemplateViewController

static NSString *kTemplateCellIdentifier = @"templateCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set data manager and context
    self.lm = [LessonPlanDataManager sharedInstance];
    self.managedObjectContext = self.lm.mainContext;
    
    // Use content manager and clear its existing data
    self.sharedUBDContentManager = [UBDContentManager sharedInstance];
    [self.sharedUBDContentManager clearEntries];
    
    // Initialize collection object
    objects = [NSArray array];
    
    // Access managed objects for entity lesson template
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"is_deleted == 0"];
    objects = [self.lm getObjectsForEntity:kLessonTemplateEntity withPredicate:predicate andSortDescriptor:@"id"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Set navigation bar title
    self.title = NSLocalizedString(@"Lesson Template", nil);
    
    // Decorate navigation bar
    UIColor *navColor = UIColorFromHex(0x5ABE3E);
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    self.navigationController.navigationBar.barTintColor = navColor;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collection View Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [objects count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LessonTemplateCollectionViewCell *templateContentCell = [collectionView dequeueReusableCellWithReuseIdentifier:kTemplateCellIdentifier forIndexPath:indexPath];
    
    NSManagedObject *mo = (NSManagedObject *)[objects objectAtIndex:indexPath.row];
    NSString *template_name = [mo valueForKey:@"name"];

    templateContentCell.imgTemplate.image = [UIImage imageNamed:@"lesson_template_icn100x100.png"];
    templateContentCell.lblTemplateName.text = [NSString stringWithFormat:@"%@ Template", template_name];
    templateContentCell.backgroundColor = [UIColor whiteColor];
    templateContentCell.height = 170.0f;
    templateContentCell.width = 150.0f;
    
    return templateContentCell;
}

#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionViewCell *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    mo_selected = (NSManagedObject *)[objects objectAtIndex:indexPath.row];
    NSString *templateid = [mo_selected valueForKey:@"id"];
    
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Please wait", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    __weak typeof(self) wo = self;
    
    [self.lm requestLessonTemplateForLessonTemplateWithID:templateid doneBlock:^(BOOL status) {
        if (status) {
            NSManagedObject *templateObject = [self.lm getEntity:kTemplateEntity withPredicate:nil];
            
            if (templateObject != nil) {
                NSDictionary *data = @{@"course_id": self.courseid};
                
                [self.lm prepareLessonForAction:LPActionTypeCreate copyObject:templateObject additionalData:data objectBlock:^(NSManagedObject *object) {
                    if (object != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [HUD hideUIBlockingIndicator];
                            wo.copiedLessonObject = object;
                            NSLog(@"copied object: %@", object);
                            [wo performSegueWithIdentifier:@"showCreateLessonView" sender:nil];
                        });
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [HUD hideUIBlockingIndicator];
                            [wo.lm showErrorMessageDialog];
                        });
                    }
                }];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUD hideUIBlockingIndicator];
                    [wo.lm showErrorMessageDialog];
                });
            }
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [wo.lm showErrorMessageDialog];
            });
        }
    }];
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    // Set color with animation
    [UIView animateWithDuration:0.1
                          delay:0
                        options:(UIViewAnimationOptionAllowUserInteraction)
                     animations:^{
                         [cell setBackgroundColor:[UIColor lightGrayColor]];
                     }
                     completion:nil];
}

- (void)collectionView:(UICollectionView *)collectionView  didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    // Set color with animation
    [UIView animateWithDuration:0.1
                          delay:0
                        options:(UIViewAnimationOptionAllowUserInteraction)
                     animations:^{
                         [cell setBackgroundColor:[UIColor whiteColor]];
                     }
                     completion:nil ];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //NSString *templateid = [mo_selected valueForKey:@"id"];
    //if ([segue.identifier isEqualToString:@"showLessonAction"]) {
    //    if ([templateid isEqualToString:@"1"]) {
    //        LessonActionViewController *lessonActionView = (LessonActionViewController *)[segue destinationViewController];
    //        lessonActionView.courseid = self.courseid;
    //        lessonActionView.gradeLevel = self.gradeLevel;
    //        lessonActionView.templateid = templateid;
    //        lessonActionView.actionType = @"1";
    //    }
    //}
    
    if ([segue.identifier isEqualToString:@"showCreateLessonView"]) {
        CreateLessonPlanView *createLessonPlanView = (CreateLessonPlanView *)[segue destinationViewController];
        createLessonPlanView.actionType = LPActionTypeCreate;
        createLessonPlanView.copiedLessonObject = self.copiedLessonObject;
    }
}

@end
