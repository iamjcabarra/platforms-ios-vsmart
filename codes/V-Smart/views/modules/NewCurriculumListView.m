//
//  NewCurriculumListView.m
//  V-Smart
//
//  Created by Julius Abarra on 16/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "NewCurriculumListView.h"
#import "NewCurriculumItemCell.h"
#import "NewCurriculumDetailView.h"
#import "CurriculumBrowserView.h"
#import "CurriculumDataManager.h"
#import "HUD.h"

@interface NewCurriculumListView () <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, UISearchControllerDelegate, UIDocumentInteractionControllerDelegate, CurriculumBrowserViewDelegate>

@property (strong, nonatomic) CurriculumDataManager *cpdm;
@property (strong, nonatomic) CurriculumBrowserView *browserView;

@property (strong, nonatomic) UIDocumentInteractionController *documentInteractionController;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) UIRefreshControl *tableRefreshControl;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *openFolderButton;

@property (strong, nonatomic) NSArray *results;
@property (strong, nonatomic) NSString *userid;

@property (strong, nonatomic) NSString *selectedDownloadedFilePath;
@property (strong, nonatomic) NSManagedObject *selectedObject;

@property (assign, nonatomic) BOOL isAscending;

@end

@implementation NewCurriculumListView

static NSString *kCurriculumCellIdentifier = @"curriculumCellIdentifier";
static unsigned long long int maxAllowableSizeForDirectory = 167772160;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Curriculum Planner Data Manager
    self.cpdm = [CurriculumDataManager sharedInstance];
    self.managedObjectContext = self.cpdm.mainContext;
    
    // File Browser View
    self.browserView = [[CurriculumBrowserView alloc] init];
    
    // Get User ID
    self.userid = [self.cpdm loginUser];
    
    // Protocols for UITableView
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Dynamic Table Row Height
    self.tableView.estimatedRowHeight = 160.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    // Initial Result Order
    self.isAscending = NO;
    
    // Pull to Refresh
    SEL refreshAction = @selector(listCurriculum);
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    // Open Folder Button Action
    [self.openFolderButton addTarget:self
                              action:@selector(openCurriculumCollection:)
                    forControlEvents:UIControlEventTouchUpInside];

    [self setupRightBarButton];
    [self setupSearchCapabilities];
    [self updateFileDownloadedButtonTitle];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Set navigation bar title
    NSString *courseName = [self stringValue:[self.courseCategoryObject valueForKey:@"name"]];
    self.title = courseName;
    
    // Decorate navigation bar
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    self.navigationController.navigationBar.barTintColor = UIColorFromHex(0x4475B7);
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - List Curriculum

- (void)listCurriculum {
    NSString *courseCategoryID = [self stringValue:[self.courseCategoryObject valueForKey:@"id"]];
    __weak typeof(self) wo = self;
    
    [self.cpdm requestCurriculumListForCourseCategory:courseCategoryID andUser:self.userid doneBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wo.tableRefreshControl endRefreshing];
                [wo reloadFetchedResultsController];
                [wo.tableView reloadData];
            });
        }
    }];
}

#pragma mark - Sort Button

- (void)setupRightBarButton {
    UIImage *sortImage = [UIImage imageNamed:@"sort_258x258"];
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    sortButton.frame = CGRectMake(0, 0, 44, 44);
    sortButton.showsTouchWhenHighlighted = YES;
    
    [sortButton setImage:sortImage forState:UIControlStateNormal];
    [sortButton setImage:sortImage forState:UIControlStateHighlighted];
    [sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
}

- (void)sortButtonAction:(id)sender {
    self.isAscending = (self.isAscending) ? NO : YES;
    [self reloadFetchedResultsController];
}

#pragma mark - Search Bar

- (void)setupSearchCapabilities {
    // Allocate and initialize results
    self.results = [[NSMutableArray alloc] init];
    
    // Init a search controller reusing the current table view controller for results
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    
    // Do not dim and hide the navigation during presentation
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = YES;
    
    // Make an appropriate size for search bar and add it as a header view for initial table view
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // Enable presentation context
    self.definesPresentationContext = YES;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewCurriculumItemCell *itemCell = [tableView dequeueReusableCellWithIdentifier:kCurriculumCellIdentifier forIndexPath:indexPath];
    [self configureCell:itemCell atIndexPath:indexPath];
    return itemCell;
}

- (void)configureFilteredCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = self.results[indexPath.row];
    NewCurriculumItemCell *itemCell = (NewCurriculumItemCell *)cell;
    [self configureCell:itemCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NewCurriculumItemCell *itemCell = (NewCurriculumItemCell *)cell;
    [self configureCell:itemCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(NewCurriculumItemCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    NSString *curriculum_title = [self stringValue:[mo valueForKey:@"curriculum_title"]];
    NSString *created_by = [self stringValue:[mo valueForKey:@"created_by"]];
    NSString *curriculum_description = [self stringValue:[mo valueForKey:@"curriculum_description"]];
    NSString *grade_level_name = [self stringValue:[mo valueForKey:@"grade_level_name"]];
    NSString *effectivity = [self stringValue:[mo valueForKey:@"effectivity"]];

    cell.curriculumTitleLabel.text = curriculum_title;
    cell.curriculumCreatorLabel.text = created_by;
    cell.gradeLevelNameLabel.text = grade_level_name;
    cell.effectivityLabel.text = effectivity;
    
    cell.curriculumDescriptionLabel.numberOfLines = 0;
    [self justifyLabel:cell.curriculumDescriptionLabel string:curriculum_description];
    
    [cell.downloadButton addTarget:self
                            action:@selector(downloadCurriculumAction:)
                  forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *indicatorString = [NSString stringWithFormat:@"%@", NSLocalizedString(@"Loading", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [HUD hideUIBlockingIndicator];
    [self performSegueWithIdentifier:@"showCurriculumDetail" sender:nil];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    });
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:self.tableView.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

- (void)openCurriculumCollection:(id)sender {
    [self performSegueWithIdentifier:@"showCurriculumCollection" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCurriculumDetail"]) {
        NewCurriculumDetailView *curriculumDetailView = (NewCurriculumDetailView *)[segue destinationViewController];
        curriculumDetailView.curriculumObject = self.selectedObject;
    }
    
    if ([segue.identifier isEqualToString:@"showCurriculumCollection"]) {
        self.browserView = [segue destinationViewController];
        self.browserView.delegate = self;
    }
}

#pragma mark - Curriculum Browser Delegate

- (void)updateDownloadedFileCount {
    [self updateFileDownloadedButtonTitle];
}

#pragma mark - Download Curriculum

- (void)downloadCurriculumAction:(id)sender {
    self.selectedObject = [self managedObjectFromButtonAction:sender];
    NSString *curriculumid = [self stringValue:[self.selectedObject valueForKey:@"curriculum_id"]];
    NSString *curriculumTitle = [self stringValue:[self.selectedObject valueForKey:@"curriculum_title"]];
    
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Downloading", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    [self.cpdm requestDownloadCurriculumWithID:curriculumid contentBlock:^(NSArray *content) {
        if (content) {
            dispatch_async (dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
            });

            NSLog(@"dowloaded content: %@", content);

            // Get Downloaded File Details
            unsigned long long int fileSize = [content[0] unsignedLongLongValue];
            NSString *fileTemporaryPath = [self stringValue:content[1]];
            NSString *suggestedFilename = [self stringValue:content[2]];
            
            // Convert Downloaded File to Data
            NSURL *fileURL = [NSURL URLWithString:fileTemporaryPath];
            NSData *fileData = [NSData dataWithContentsOfURL:fileURL];
            
            // Check if directory will become full
            NSString *directory = [self createSubDirectoryInDocumentsDirectory:@"curriculum_planner"];
            BOOL willBecomeFull = [self willDirectoryBecomeFull:directory ifFileToDownloadWithSizeIsSaved:fileSize];
            
            if (!willBecomeFull) {
                if ([suggestedFilename isEqualToString:@""]) {
                    suggestedFilename = [NSString stringWithFormat:@"%@.pdf", curriculumTitle];
                }
                
                NSString *localFilePath = [NSString stringWithFormat:@"%@/%@", directory, suggestedFilename];
                
                // Check if file already exists
                if ([[NSFileManager defaultManager] fileExistsAtPath:localFilePath]) {
                    NSLog(@"file already exists");
                    
                    __weak typeof (self) wo = self;
                    dispatch_async (dispatch_get_main_queue(), ^{
                        [wo shouldReplaceFile:suggestedFilename completion:^(BOOL didReplace) {
                            NSString *filename = (didReplace) ? suggestedFilename : [wo changeNameOfFile:suggestedFilename];
                            [wo saveFile:filename andData:fileData];
                        }];
                    });
                    
                }
                else {
                    NSLog(@"file does not exist");
                    [self saveFile:suggestedFilename andData:fileData];
                }
            }
            else {
                __weak typeof (self) wo = self;
                dispatch_async (dispatch_get_main_queue(), ^{
                    [wo showErrorMessageDialogForError:VSCPErrorTypeDirectoryFullError];
                });
            }
        }
        else {
            __weak typeof (self) wo = self;
            dispatch_async (dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [wo showErrorMessageDialogForError:VSCPErrorTypeFileDownloadError];
            });
        }
    }];
}

- (void)shouldReplaceFile:(NSString *)filename completion:(void (^)(BOOL didReplace))response {
    NSString *premstr = NSLocalizedString(@"A file named", nil);
    NSString *sufmstr = NSLocalizedString(@"already exists. Do you want to replace it with the one you are downloading?", nil);
    NSString *message = [NSString stringWithFormat:@"%@ \"%@\" %@", premstr, filename, sufmstr];
    NSString *butPosR = NSLocalizedString(@"Replace", nil);
    NSString *butNegR = NSLocalizedString(@"Keep Both", nil);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *posAction = [UIAlertAction actionWithTitle:butPosR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                          response(YES);
                                                      }];
    
    UIAlertAction *negAction = [UIAlertAction actionWithTitle:butNegR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                          response(NO);
                                                      }];
    [alert addAction:posAction];
    [alert addAction:negAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)saveFile:(NSString *)name andData:(NSData *)data {
    // Save the file in a seperate thread
    dispatch_async(dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        __weak typeof (self) wo = self;
        
        if (data) {
            NSLog(@"saving started");
            
            // Create curriculum planner directory
            NSString *curriculumPlannerDirectory = [self createSubDirectoryInDocumentsDirectory:@"curriculum_planner"];
            
            // File to save in created directory
            NSString *localFilePath = [NSString stringWithFormat:@"%@/%@", curriculumPlannerDirectory, name];
            
            // Saving is done on main thread
            dispatch_async (dispatch_get_main_queue(), ^{
                NSError *writeError = nil;
                [data writeToFile:localFilePath options:NSDataWritingAtomic error:&writeError];
        
                if (writeError) {
                    NSLog(@"writing error");
                    [wo showErrorMessageDialogForError:VSCPErrorTypeFileWriteError];
                }
                else {
                    NSLog(@"saving finished");
                    [wo updateFileDownloadedButtonTitle];
                    [wo showDownloadConfirmationMessage:localFilePath filename:name];
                }
            });
        }
        else {
            NSLog(@"saving aborted");
            dispatch_async (dispatch_get_main_queue(), ^{
                [wo showErrorMessageDialogForError:VSCPErrorTypeFileWriteError];
            });
        }
    });
}

- (BOOL)doesFileExist:(NSString *)file {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory = [self createSubDirectoryInDocumentsDirectory:@"curriculum_planner"];
    return [fm fileExistsAtPath:[directory stringByAppendingPathComponent:file]];
}

- (NSString *)changeNameOfFile:(NSString *)file {
    NSString *filename = [[file lastPathComponent] stringByDeletingPathExtension];
    NSString *extension = [file pathExtension];
    
    NSString *suffix = NSLocalizedString(@"copy", nil);
    
    NSInteger i = 1;
    NSString *directory = [self createSubDirectoryInDocumentsDirectory:@"curriculum_planner"];
    NSInteger numberOfFiles = [[[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:nil] count];
    
    while (i <= numberOfFiles) {
        file = [NSString stringWithFormat:@"%@ %@ %zd.%@", filename, suffix, i, extension];
        
        if (![self doesFileExist:file]) {
            return file;
        }
        
        i++;
    }
    
    return file;
}

#pragma mark - Directory Testing and Configuration

- (NSString *)createSubDirectoryInDocumentsDirectory:(NSString *)name {
    name = [NSString stringWithFormat:@"/%@", name];
    
    // Access documents directory
    NSArray *directoryPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [directoryPaths objectAtIndex:0];
    
    // Name of subdirectory
    NSError *error;
    NSString *documentsSubdirectory = [documentsDirectory stringByAppendingPathComponent:name];
    
    // Create subdirectory if it does not exist
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsSubdirectory]) {
        // Create subdirectory in documents directory
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsSubdirectory
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:&error];
    }
    
    return documentsSubdirectory;
}

- (unsigned long long int)sizeOfDirectory:(NSString *)directory {
    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:nil];
    NSEnumerator *contentsEnumurator = [contents objectEnumerator];
    
    NSString *file;
    unsigned long long int directorySize = 0;
    
    while (file = [contentsEnumurator nextObject]) {
        NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[directory stringByAppendingPathComponent:file] error:nil];
        directorySize += [[fileAttributes objectForKey:NSFileSize] unsignedLongLongValue];
    }

    return directorySize;
}

- (BOOL)willDirectoryBecomeFull:(NSString *)directory ifFileToDownloadWithSizeIsSaved:(unsigned long long int)sizeOfFileToDownload {
    unsigned long long int directorySize = [self sizeOfDirectory:directory];
    unsigned long long int combinedSize = directorySize + sizeOfFileToDownload;

    if (combinedSize <= maxAllowableSizeForDirectory) {
        return NO;
    }
    
    return YES;
}

- (unsigned long long int)sizeOfLocalFile:(NSString *)localFilePath {
    unsigned long long int fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:localFilePath error:nil] fileSize];
    return fileSize;
}

- (void)updateFileDownloadedButtonTitle {
    NSArray *collection = [self.browserView getCurriculumCollection];
    NSInteger count = 0;
    
    if (collection != nil) {
        count = collection.count;
    }
    
    NSString *buttonTitle = NSLocalizedString(@"View Downloaded", nil);
    buttonTitle = [NSString stringWithFormat:@"%@ (%zd)", buttonTitle, count];
    [self.openFolderButton setTitle:buttonTitle forState:UIControlStateNormal];
    [self.openFolderButton setTitle:buttonTitle forState:UIControlStateSelected];
    [self.openFolderButton setTitle:buttonTitle forState:UIControlStateHighlighted];
}

#pragma mark - Alert Messages

- (void)showErrorMessageDialogForError:(NSInteger)errorNumber {
    NSString *avTitle = NSLocalizedString(@"Download Failed", nil);
    NSString *message = NSLocalizedString(@"There was an error downloading this file. Please try again later.", nil);
    NSString *butPosR = NSLocalizedString(@"Okay", nil);
    
    if (errorNumber == VSCPErrorTypeDirectoryFullError) {
        message = NSLocalizedString(@"Not enough space in device storage, delete some files from curriculum planner directory and try again.", nil);
    }
    
    if (errorNumber == VSCPErrorTypeFileWriteError) {
        message = NSLocalizedString(@"There was an error saving this file. Please try again later.", nil);
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:avTitle
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *posAction = [UIAlertAction actionWithTitle:butPosR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                      }];
    
    [alert addAction:posAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showDownloadConfirmationMessage:(NSString *)filepath filename:(NSString *)filename {
    self.selectedDownloadedFilePath = filepath;

    NSString *message = [NSString stringWithFormat:@"%@ %@.", filename, NSLocalizedString(@"was successfully downloaded and saved in your device", nil)];
    NSString *butPosR = NSLocalizedString(@"Open", nil);
    NSString *butNegR = NSLocalizedString(@"Not Now", nil);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *posAction = [UIAlertAction actionWithTitle:butPosR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          __weak typeof (self) wo = self;
                                                          dispatch_async (dispatch_get_main_queue(), ^{
                                                              [wo openFileWithFilePath:self.selectedDownloadedFilePath];
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                          });
                                                      }];
    
    UIAlertAction *negAction = [UIAlertAction actionWithTitle:butNegR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                      }];
    [alert addAction:posAction];
    [alert addAction:negAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Open Curriculum As PDF File

- (void)openFileWithFilePath:(NSString *)filepath {
    NSURL *url = [NSURL fileURLWithPath:filepath];
    
    if (url) {
        // Initialize Document Interaction Controller
        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        
        // Configure Document Interaction Controller
        self.documentInteractionController.delegate = self;
        
        // Open PDF File
        CGRect navigationRect = self.navigationController.navigationBar.frame;
        [self.documentInteractionController presentOptionsMenuFromRect:navigationRect inView:self.view animated:YES];
    }
    else {
        NSLog(@"Not a valid URL");
    }
}

#pragma mark - Document Interaction Controller Delegate

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return self;
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller {
    return self.view.frame;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller {
    return self.view;
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCurriculumListEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSCompoundPredicate *cp;
    
    // Like Predicate
    NSPredicate *predicate1 = [self predicateForKeyPath:@"user_id"  value:self.userid];
    
    // Contains Predicate
    NSString *searchString = self.searchController.searchBar.text;
    NSPredicate *predicate2 = [self predicateForKeyPathContains:@"curriculum_title" value:searchString];
    
    if (self.searchController.active == YES && ![searchString isEqualToString:@""]) {
        cp = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    }
    else {
        cp = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1]];
    }
    
    fetchRequest.predicate = cp;
    
    NSSortDescriptor *sortedByTitle = [NSSortDescriptor sortDescriptorWithKey:@"curriculum_title"
                                                                    ascending:self.isAscending
                                                                     selector:@selector(caseInsensitiveCompare:)];
    [fetchRequest setSortDescriptors:@[sortedByTitle]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - Reloading of Data

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    [self reloadFetchedResultsController];
}

- (void)reloadSearchResults {
    NSArray *items = [NSArray arrayWithArray:self.results];
    NSSortDescriptor *curriculum_title = [NSSortDescriptor sortDescriptorWithKey:@"curriculum_title" ascending:self.isAscending];
    NSArray *sorted = [items sortedArrayUsingDescriptors:@[curriculum_title]];
    
    self.results = [NSArray arrayWithArray:sorted];
}

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error: %@", [err localizedDescription]);
    }
}

#pragma mark - Class Helpers

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

- (NSManagedObject *)managedObjectFromButtonAction:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (void)justifyLabel:(UILabel *)label string:(NSString *)string {
    NSMutableParagraphStyle *paragraphStyles = [[NSMutableParagraphStyle alloc] init];
    paragraphStyles.alignment = NSTextAlignmentJustified;
    paragraphStyles.firstLineHeadIndent = 1.0;
    NSDictionary *attributes = @{NSParagraphStyleAttributeName:paragraphStyles};
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:string attributes:attributes];
    label.attributedText = attributedString;
}

@end