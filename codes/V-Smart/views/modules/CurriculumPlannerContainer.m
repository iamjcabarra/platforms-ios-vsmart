//
//  CurriculumPlannerContainer.m
//  V-Smart
//
//  Created by Ryan Migallos on 9/15/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "CurriculumPlannerContainer.h"
#import "AppDelegate.h"
#import "VSmartMacros.h"
#import "VSmartValues.h"

@class ResourceManager;

@interface CurriculumPlannerContainer ()

@property (nonatomic, strong) UIViewController *cv;

@end

@implementation CurriculumPlannerContainer

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupChildViewController];
    [self layoutNotifications];
    [super hideJumpMenu:NO];
    [super showOrHideMiniAvatar];
}

- (void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        dispatch_queue_t queue = dispatch_queue_create("com.vsmartchool.curriculum.ACTIVITY",DISPATCH_QUEUE_SERIAL);
        dispatch_async(queue, ^{
            NSString *details = @"Left the Curriculum module in Apple iPad";
            [[AppDelegate resourceInstance] requestLogActivityWithModuleType:@"22" details:details];
        });
    }
    
    [super viewWillDisappear:animated];
}

- (void)setupChildViewController {
//    NSString *storyboardName = @"CurriculumStoryboard";
    NSString *storyboardName = @"CPMStoryboard";
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    self.cv = [sb instantiateInitialViewController];
    [self initiateCustomLayoutFor:self.cv];
    self.cv.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.cv.view.autoresizesSubviews = YES;
    
    [self addChildViewController:self.cv];
    [self.view addSubview:self.cv.view];
    [self.cv didMoveToParentViewController:self];
    [self.view sendSubviewToBack:self.cv.view];
}

- (void)layoutNotifications {
    VS_NCADD(kNotificationProfileHeight, @selector(resizeContainerView:))
}

- (NSString *) dashboardName {
    NSString *moduleName = NSLocalizedString(@"Curriculum Planner", nil);
    return moduleName;
}

#pragma mark - Post Notification Events
- (void)resizeContainerView:(NSNotification *)notification {
    VLog(@"Received: kNotificationProfileHeight");
    [UIView animateWithDuration:0.45 animations:^{
        [self initiateCustomLayoutFor:self.cv];
        [self.cv.view setNeedsLayout];
        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            [super adjustProfileHeight];
        }
        
        [super showOrHideMiniAvatar];
    }];
}

- (void)initiateCustomLayoutFor:(UIViewController *)viewcontroller {
    float profileY = [super profileHeight];
    float headerDecrement = ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    float gridHeight = self.view.frame.size.height - headerDecrement;
    float gridY = [super headerSize].size.height + profileY;
    CGRect customFrame = CGRectMake(0, gridY, self.view.frame.size.width, gridHeight);
    [viewcontroller.view setFrame:customFrame];
}

@end
