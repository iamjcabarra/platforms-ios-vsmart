//
//  GradeBookCollectionViewCell.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 24/05/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GradeBookCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var changeScoreButton: UIButton!
}
