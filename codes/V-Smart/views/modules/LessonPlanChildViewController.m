//
//  PostStreamView.m
//  V-Smart
//
//  Created by VhaL on 3/13/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "LessonPlanChildViewController.h"
#import "JSONHTTPClient.h"

@interface LessonPlanChildViewController ()
@property (nonatomic, strong) IBOutlet UIWebView *webView;
@end

@implementation LessonPlanChildViewController

-(AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self setupWebview];
    [self loadWebview];
}

-(void)setupWebview{
    for (id subview in self.webView.subviews)
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            ((UIScrollView *)subview).bounces = NO;
    
    [self.view addSubview:self.webView];
}

-(void)loadWebview{

    int userid = [self account].user.id;
    NSString *lang = [VSmartHelpers getDeviceLocale];
    
    NSString *urlString = [Utils buildUrl:VS_FMT(kEndPointLessonPlan, userid, lang)];
    NSURLRequest *webRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [self.webView loadRequest:webRequest];
}

#pragma mark - UIWebView Delegate

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    //    [SVProgressHUD dismiss];
    VLog(@"Finished Loading View");
    [webView.scrollView setContentSize: CGSizeMake(webView.frame.size.width, webView.scrollView.contentSize.height)];
    
}

-(void)webView:(UIWebView *)theWebView didFailLoadWithError:(NSError *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    //    [SVProgressHUD dismiss];
	VLog(@"Error: %@", [error localizedDescription]);
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    //    [SVProgressHUD showWithStatus:MSG_LOADING_MODULE maskType:SVProgressHUDMaskTypeGradient];
}

-(void)dealloc{
    [self.webView stopLoading];
 	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.webView.delegate = nil;
}

@end
