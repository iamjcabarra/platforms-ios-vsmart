//
//  NotesCollectionViewCell.m
//  V-Smart
//
//  Created by VhaL on 2/12/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NotesCollectionViewCell.h"

@interface NotesCollectionViewCell()

@property (nonatomic, assign) BOOL isActionDisplayed;
@end

@implementation NotesCollectionViewCell
@synthesize labelTitle, textDescription, textViewBackground, optionArray, delegate, isActionDisplayed, isArchived;

-(IBAction)buttonActionTapped:(id)sender{
    NSLog(@"%@", @"buttonActionTapped");

    //CGFloat yAxis = self.frame.size.height - optionArray.count * 44;
    CGFloat yAxis = self.frame.size.height / 3.0f;
    
    if (isActionDisplayed){
        yAxis = self.frame.size.height;
    }

    [UIView animateWithDuration:0.2
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, yAxis, self.tableView.frame.size.width, self.tableView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                     }];
    
    isActionDisplayed = !isActionDisplayed;
    
    if (isActionDisplayed)
        [NSTimer scheduledTimerWithTimeInterval:5
                                         target:self
                                       selector:@selector(hideActionDisplay)
                                       userInfo:nil
                                        repeats:NO];
}

-(void)hideActionDisplay{
    if (isActionDisplayed)
        [self buttonActionTapped:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return optionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellId;
    if (isArchived)
        cellId = @"ArchivedCell";
    else
        cellId = @"NormalCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    NSDictionary *option = (NSDictionary *)optionArray[indexPath.row];
    cell.textLabel.text = option[@"option_title"];

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

-(void)initializeCell{

    /* localizable strings */
    //NSString *restoreOption = NSLocalizedString(@"Restore", nil); //checked
    //NSString *duplicateOption = NSLocalizedString(@"Duplicate", nil); //checked
    //NSString *archiveOption = NSLocalizedString(@"Archive", nil); //checked
    //NSString *deleteOption = NSLocalizedString(@"Delete", nil); //checked
    
    //if (isArchived) {
    //    optionArray = @[
    //                    @{@"option_name":@"Restore", @"option_title":restoreOption}
    //                    ];
    //} else {
    //    optionArray = @[
    //                    @{@"option_name":@"Duplicate", @"option_title":duplicateOption},
    //                    @{@"option_name":@"Archive", @"option_title":archiveOption},
    //                    @{@"option_name":@"Delete", @"option_title":deleteOption}
    //                    ];
    //}
    
    // Feature #885
    // jca-04272016
    // Add Edit and Move Options
    NSString *restoreOption = NSLocalizedString(@"Restore", nil);
    NSString *editOption = NSLocalizedString(@"Edit", nil);
    NSString *duplicateOption = NSLocalizedString(@"Duplicate", nil);
    NSString *moveOption = NSLocalizedString(@"Move", nil);
    NSString *deleteOption = NSLocalizedString(@"Delete", nil);
    NSString *archiveOption = NSLocalizedString(@"Archive", nil);
    
    
    if (isArchived) {
        // Enhancement #1307
        // jca-05042016
        // Add Delete Option
        optionArray = @[@{@"option_name":@"Restore", @"option_title":restoreOption},
                        @{@"option_name":@"Delete", @"option_title":deleteOption}
                        ];
    }
    else {
        optionArray = @[@{@"option_name":@"Edit", @"option_title":editOption},
                        @{@"option_name":@"Duplicate", @"option_title":duplicateOption},
                        @{@"option_name":@"Move", @"option_title":moveOption},
                        @{@"option_name":@"Delete", @"option_title":deleteOption},
                        @{@"option_name":@"Archive", @"option_title":archiveOption}
                        ];
    }
    
    [self.tableView reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self buttonActionTapped:nil];
    
    NSDictionary *option = (NSDictionary *)optionArray[indexPath.row];
    NSString *actionTitle = option[@"option_name"];
    
    if ([delegate respondsToSelector:@selector(cellButtonActionApplyTapped:withActionTitle:)]){
        [delegate performSelector:@selector(cellButtonActionApplyTapped:withActionTitle:) withObject:self withObject:actionTitle];
    }
}
@end
