//
//  Routable.swift
//  V-Smart
//
//  Created by Julius Abarra on 09/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

protocol Routable: Networkable, Parsable {
    func route(_ method: String, uri: String, body: [String: Any]?) -> URLRequest?
    func route(_ method: String, uri: String, query: [String: Any]?, body: [String: Any]?) -> URLRequest?
}

extension Routable {

    func route(_ method: String, uri: String, body: [String: Any]?) -> URLRequest? {
        let endPoint = "vsmart-rest-dev" + "\(uri)"
        guard let url = buildRequestURLFromEndPoint(endPoint) else { return nil }
        let request = buildURLRequestForMethod(method, url: url, body: body)
        return request
    }
    
    func route(_ method: String, uri: String, query: [String: Any]?, body: [String: Any]?) -> URLRequest? {
        let endPoint = "vsmart-rest-dev" + "\(uri)"
        guard var url = buildRequestURLFromEndPoint(endPoint) else { return nil }

        if let dictionary = query {
            var queryVariables = [URLQueryItem]()

            for (name, object) in dictionary {
                let value = object as! String
                let queryObject = URLQueryItem(name: name, value: value)
                queryVariables.append(queryObject)
            }

            guard var urlComponents = URLComponents(string: url.absoluteString) else { return nil }
            urlComponents.queryItems = queryVariables
            guard let theURL = urlComponents.url else { return nil }
            url = theURL
        }
        
        return buildURLRequestForMethod(method, url: url, body: body)
    }
}
