//
//  SSPeopleViewController.m
//  V-Smart
//
//  Created by VhaL on 3/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "SSPeopleViewController.h"
#import "SSPeopleStudentViewController.h"
#import "SSPeopleTeacherViewController.h"

@interface SSPeopleViewController ()
@property (nonatomic, strong) SSPeopleStudentViewController *studentController;
@property (nonatomic, strong) SSPeopleTeacherViewController *teacherController;
@end

@implementation SSPeopleViewController
@synthesize parent;
@synthesize studentController, teacherController;

-(AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
//    if ([[self account].user.position isEqualToString:kModeIsStudent]) {
//        studentController = [[SSPeopleStudentViewController alloc] init];
//        studentController.view.frame = self.view.frame;
//        studentController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
//        studentController.view.autoresizesSubviews = YES;
//        [self addChildViewController:studentController];
//        [self.view addSubview:studentController.view];
//        [studentController didMoveToParentViewController:self];
//    }
//    else{
//        teacherController = [SSPeopleTeacherViewController new];
//        teacherController.view.frame = self.view.frame;
//        teacherController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
//        teacherController.view.autoresizesSubviews = YES;
//        [self addChildViewController:teacherController];
//        [self.view addSubview:teacherController.view];
//        [teacherController didMoveToParentViewController:self];
//    }
}

-(void)viewWillAppear:(BOOL)animated {
    
//    if ([[self account].user.position isEqualToString:kModeIsStudent]) {
//        teacherController.view.hidden = YES;
//        studentController.view.hidden = NO;
//    }
//    else{
//        teacherController.view.hidden = NO;
//        studentController.view.hidden = YES;
//    }
    
    //Module Types
    NSString *position = [[VSmart sharedInstance] account].user.position;
    BOOL mode = [position isEqualToString:kModeIsTeacher];
    NSString *module = mode ? @"TeacherShortCut" : @"StudentShortCut";
    [self renderModuleWithIdentifier:module];
}

- (UIViewController *)loadControllerWithIdentifier:(NSString *)identifier {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SmartShorcutPeopleStoryBoard" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:identifier];
}

- (void)renderModuleWithIdentifier:(NSString *)module {
    
    UIViewController *vc = [self loadControllerWithIdentifier:module];
    vc.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    [self addChildViewController:vc];
    [self.view addSubview:vc.view];
    [vc didMoveToParentViewController:self];
}


@end