//
//  SocialStreamController.m
//  V-Smart
//
//  Created by Ryan Migallos on 1/20/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#define MAS_SHORTHAND
#import "Masonry.h"

#import "AppDelegate.h"
#import "SocialStreamController.h"
#import "SocialStreamOptionController.h"
#import "HMSegmentedControl.h"
#import "SocialStreamItemCell.h"
#import "SocialStreamStickerCell.h"
#import "SocialStreamPreviewCell.h"
#import "SocialStreamMessage.h"
#import "SocialStreamPeopleWhoLike.h"
#import "SocialStreamComment.h"
#import "JMSGroupItem.h"
#import <Parse/Parse.h>
#import <QuartzCore/QuartzCore.h>

#import "NoteHelper.h"
#import "MainHeader.h"

@interface SocialStreamController () <NSFetchedResultsControllerDelegate, UIScrollViewDelegate, UIPopoverControllerDelegate, SocialStreamOptionDelegate>

@property (nonatomic, strong) IBOutlet UIView *segmentedContainer;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, strong) IBOutlet UIButton *messageButton;
@property (nonatomic, strong) IBOutlet UIButton *showMoreButton;

@property (nonatomic, strong) JMSRecordGroupItem *groupItems;
@property (nonatomic, strong) JMSGroupItem *currentGroup;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) ResourceManager *rm;

@property (nonatomic, assign) CGFloat lastContentOffset;
@property (nonatomic, assign) BOOL loadingPrevData;
@property (nonatomic, assign) BOOL hideCommentBox;

@property (nonatomic, strong) UIRefreshControl *tableRefreshControl;

@property (nonatomic, strong) dispatch_queue_t queue;

@property (nonatomic, strong) HMSegmentedControl *sc;

@property (nonatomic, assign) BOOL likeEnabled;

@property (nonatomic, assign) NSInteger courseCount;
@property (strong, nonatomic) IBOutlet UIView *emptyView;
@property (strong, nonatomic) IBOutlet UILabel *emptyViewLabel;

@end

@implementation SocialStreamController

static NSString *kCellTextIdentifier = @"cell_ss_text_id";
static NSString *kCellStickerIdentifier = @"cell_ss_sticker_id";
static NSString *kCellPreviewIdentifier = @"cell_ss_preview_id";

- (AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

- (void)viewDidLoad
{
    self.likeEnabled = YES;
    VS_NCADD(kNotificationReceivedNewGroupMessage, @selector(notifyResouceManager) )
    VS_NCADD(kNotificationReceivedRealTimeDataLike, @selector(enableLikeButton) )
    NSLog(@"Social Stream Controller...");
    
    self.queue = dispatch_queue_create("com.vibetech.STICKERLIST", DISPATCH_QUEUE_SERIAL);
    
    //execute fetch request
    self.rm = [AppDelegate resourceInstance];
    self.managedObjectContext = self.rm.mainContext;
    
    // Enhancement #1354
    // jca-05172016
    // No course assigned to teacher or student
    self.courseCount = 0;
    self.emptyView.hidden = YES;
    self.emptyViewLabel.text = NSLocalizedString(@"No items to display.", nil);
    
    self.tableView.estimatedRowHeight = 235.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    self.tableView
    
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    SEL refreshAction = @selector(refreshSocialStream);
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    NSString *socketio = [NSString stringWithFormat:@"%@", [Utils getVibeServer] ];
    NSInteger port = 8000;
    [self.rm reconnectSocketWithURL:socketio port:port];
    
    self.sc = [[HMSegmentedControl alloc] init];
//    self.sc.touchEnabled = NO;

    //SOCKET IO
//    NSString *socketio = @"lms.vsmart.info";
//    NSInteger port = 8888;
    
//    NSString *socketio = @"vibe.vsmartschool.me";
    [self setupGroup];
    
    [self.messageButton addTarget:self action:@selector(actionShowMessageBox:) forControlEvents:UIControlEventTouchUpInside];

    [self setupOptionMenu];
}

- (void)refreshSocialStream {
//    [self setupGroup];
    [self loadStream];
}

- (void)setupOptionMenu
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SocialStreamStoryboard" bundle:nil];
    SocialStreamOptionController *oc = (SocialStreamOptionController *)[sb instantiateViewControllerWithIdentifier:@"socialStreamOptions"];
    oc.delegate = self;
    self.popover = [[UIPopoverController alloc] initWithContentViewController:oc];
    self.popover.delegate = self;
}

- (void)selectedMenuOption:(NSString *)option withData:(NSDictionary *)data point:(CGPoint)point {

    NSLog(@"option : %@ data : %@", option, data);
    [self.popover dismissPopoverAnimated:YES];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];

    BOOL owned = [[mo valueForKey:@"owned"] isEqualToString:@"1"];
    if (owned) {
        if ([option isEqualToString:@"edit"]) {
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [self performSegueWithIdentifier:@"showMessageBox" sender:cell];
        }
        
        if ([option isEqualToString:@"delete"]) {
            NSString *message_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];
            NSString *user_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ];
            NSMutableDictionary *d = [@{@"message_id":message_id, @"user_id":user_id } mutableCopy];
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete", nil) message:NSLocalizedString(@"Are you sure you want to delete?", nil) preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* noAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:noAlertAction];
            
            __weak typeof(self) wo = self;
            
            UIAlertAction* yesAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [wo.rm requestRemoveMessage:d doneBlock:^(BOOL status) {
                    if (status) {
                        NSLog(@"delete successful...");
                    }
                }];
            }];
            
            [alertController addAction:yesAlertAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    
    if ([option isEqualToString:@"add_to_note"]) {
        NSString *messageContent = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message"] ];
        __weak typeof (self) ws = self;
        NoteHelper *nh = [[NoteHelper alloc] init];
        [nh saveMessage:messageContent doneBlock:^(BOOL status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                AlertWithMessageAndDelegate(@"Note Book", @"Added Note", ws);
            });
        }];
    }

}

- (void) setupGroupSection
{
    NSMutableArray *items = [NSMutableArray array];
    for (JMSGroupItem *item in self.groupItems.records) {
//        NSString *firstLetter = [NSString stringWithFormat:@"%@",[item.name substringToIndex:1]];
        NSString *firstLetter = [NSString stringWithFormat:@"%@",item.name ];
        NSString *label = [NSString stringWithFormat:@"%@-%@", firstLetter, item.section];
//        NSString *label = [NSString stringWithFormat:@"%@", item.section];
        [items addObject:label];
    }
    
    /*
     * LEGACY CODE
     *
    
    HMSegmentedControl *sc = [[HMSegmentedControl alloc] init];
    [sc setSectionTitles:items];
    [sc setSelectedSegmentIndex:0];
    [sc setBackgroundColor:UIColorFromHex(0xf5f4f4)];
    [sc setTextColor:UIColorFromHex(0x1d6483)];
    [sc setSelectedTextColor:UIColorFromHex(0x1d6483)];
    [sc setSelectionIndicatorColor:UIColorFromHex(0x00a9d5)];
    [sc setSelectionStyle:HMSegmentedControlSelectionStyleBox];
    [sc setSelectionLocation:HMSegmentedControlSelectionLocationUp];
     */
    
//    HMSegmentedControl *sc = [[HMSegmentedControl alloc] init];
    self.sc.sectionTitles = items;
    self.sc.selectedSegmentIndex = 0;
    self.sc.backgroundColor = UIColorFromHex(0xf5f4f4);
    self.sc.titleTextAttributes = @{ NSForegroundColorAttributeName : UIColorFromHex(0x1d6483)};
    self.sc.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : UIColorFromHex(0x1d6483)};
    self.sc.selectionIndicatorColor = UIColorFromHex(0x00a9d5);
    self.sc.selectionStyle = HMSegmentedControlSelectionStyleBox;
    self.sc.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationUp;
    
    //Switch Content
    __weak typeof(self) weakSelf = self;
    [self.sc setIndexChangeBlock:^(NSInteger index) {
        weakSelf.currentGroup = [weakSelf.groupItems.records objectAtIndex:index];
        [weakSelf loadStream];
    }];
    
    [self.segmentedContainer addSubview:self.sc];
    
    //Apply Constraints
    [self.sc makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.segmentedContainer);
    }];
}

- (void)setupGroup
{
    NSString *urlString = [Utils buildUrl:VS_FMT(kEndPointGetGroups, [self account].user.id)];
    NSLog(@"group url string : %@", urlString);
    
    __weak typeof(self) wo = self;
    
    //[JSONHTTPClient getJSONFromURLWithString:urlString completion:^(NSDictionary *json, JSONModelError *err) {
    //    
    //    if(err == nil) {
    //        
    //        /*
    //         NOTE: we must handle the server reply even if the records are empty
    //         {"_meta":{"status":"SUCCESS","count":0},"records":{}}
    //         */
    //        
    //        NSArray *arrayRecords = [json objectForKey:@"records"];
    //        
    //        if ([arrayRecords count] > 0) {
    //            
    //            NSMutableArray *array = [NSMutableArray arrayWithArray:arrayRecords];
    //            NSDictionary *dict = @{@"records": array};
    //            NSError *parseError = nil;
    //            NSData *records = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
    //            NSString *jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
    //            
    //            self.groupItems = [[JMSRecordGroupItem alloc] initWithString:jsonString error:&parseError];
    //            if (0 < self.groupItems.records.count){
    //                self.currentGroup = [self.groupItems.records objectAtIndex:0];
    //                
    ////                    NSString *section_name = [NSString stringWithFormat:@"%@-%@", self.currentGroup.name, self.currentGroup.section];
    ////                    [Utils saveSectionName:section_name];
    ////                    
    ////                    NSUInteger section_id = self.currentGroup.groupId;
    ////                    [Utils saveSectionID:section_id];
    //            }
    //            
    //            // BUG FIX VIBALHP
    //            dispatch_async(dispatch_get_main_queue(), ^{
    //                [wo setupGroupSection];
    //                
    //                [VSmartHelpers pullCurrentSections];
    //                [wo loadStream];
    //                return;
    //            });
    //            
    //        }
    //        [wo loadStream];
    //    }
    //}];
    
    // Enhancement #1354
    // jca-05172016
    // No course assigned to teacher or student
    
    [JSONHTTPClient getJSONFromURLWithString:urlString completion:^(NSDictionary *json, JSONModelError *err) {
        
        if (err == nil) {
            NSArray *arrayRecords = [json objectForKey:@"records"];
            self.courseCount = arrayRecords.count;
        
            if ([arrayRecords count] > 0) {
                NSMutableArray *array = [NSMutableArray arrayWithArray:arrayRecords];
                NSDictionary *dict = @{@"records": array};
                NSError *parseError = nil;
                NSData *records = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
                NSString *jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
                
                self.groupItems = [[JMSRecordGroupItem alloc] initWithString:jsonString error:&parseError];
                
                if (0 < self.groupItems.records.count){
                    self.currentGroup = [self.groupItems.records objectAtIndex:0];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [wo setupGroupSection];
                    [VSmartHelpers pullCurrentSections];
                    [wo loadStream];
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [wo shouldShowEmptyView:YES];
                });
            }
        }
    }];
}

- (void)shouldShowEmptyView:(BOOL)show {
    self.tableView.hidden = show;
    self.emptyView.hidden = !show;
    
    if (show) {
        if (self.courseCount <= 0) {
            NSString *message = NSLocalizedString(@"No course available to the user.", nil);
            [self.view makeToast:message duration:2.0f position:@"center"];
        }
    }
    
    self.messageButton.hidden = self.courseCount > 0 ? NO : YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    //CLOSE SOCKET IO
    [self.rm closeSocket];
}

-(void)loadStream
{
    //VLog(@"%s", __PRETTY_FUNCTION__);
    //
    //self.sc.touchEnabled = NO;
    //
    //[Utils saveStreamType:@"social"];
    //
    ////    NSString *socketio = [NSString stringWithFormat:@"%@", [Utils getVibeServer] ];
    ////    NSInteger port = 8000;
    ////    [self.rm reconnectSocketWithURL:socketio port:port];
    //
    //
    //__weak typeof(self) wo = self;
    //NSString *groupid = [NSString stringWithFormat:@"%d", self.currentGroup.groupId]; // group id
    //
    //dispatch_async(self.queue, ^{
    //    [self.rm requestSocialStreamMessageWithGroupID:groupid doneBlock:^(BOOL status) {
    //        
    //        //            self.sc.enabled = YES;
    //        if (status) {
    //            NSLog(@" STATUS YES ");
    //            NSLog(@"requestSocialStreamMessageWithGroupID DONE!!!");
    //            [self.rm requestBadWordsDoneBlock:^(BOOL status) {
    //                NSLog(@"bad words loaded...");
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    wo.sc.touchEnabled = YES;
    //                    [wo.tableRefreshControl endRefreshing];
    //                    [wo.tableView reloadData];
    //                });
    //            }];
    //        }
    //        
    //        if (status == NO) {
    //            NSLog(@" STATUS NO ");
    //            dispatch_async(dispatch_get_main_queue(), ^{
    //                wo.sc.touchEnabled = YES;
    //                [wo.tableRefreshControl endRefreshing];
    //                [wo.tableView reloadData];
    //            });
    //            
    //        }
    //        
    //    }];
    //});
    
    // Enhancement #1354
    // jca-05172016
    // No course assigned to teacher or student
    
    //if (self.courseCount > 0) {
    //    self.sc.touchEnabled = NO;
    //    
    //    [Utils saveStreamType:@"social"];
    //    __weak typeof(self) wo = self;
    //    NSString *groupid = [NSString stringWithFormat:@"%d", self.currentGroup.groupId]; // group id
    //    
    //    dispatch_async(self.queue, ^{
    //        [self.rm requestSocialStreamMessageWithGroupID:groupid doneBlock:^(BOOL status) {
    //            if (status) {
    //                [self.rm requestBadWordsDoneBlock:^(BOOL status) {
    //                    dispatch_async(dispatch_get_main_queue(), ^{
    //                        wo.sc.touchEnabled = YES;
    //                        [wo.tableRefreshControl endRefreshing];
    //                        [wo.tableView reloadData];
    //                    });
    //                }];
    //            }
    //            
    //            if (status == NO) {
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    wo.sc.touchEnabled = YES;
    //                    [wo.tableRefreshControl endRefreshing];
    //                    [wo.tableView reloadData];
    //                });
    //            }
    //        }];
    //    });
    //}
    //else {
    //    __weak typeof(self) wo = self;
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //        wo.sc.touchEnabled = YES;
    //        [wo.tableRefreshControl endRefreshing];
    //    });
    //}
    //
    //[self shouldShowEmptyView:(self.courseCount > 0 ? NO : YES)];
    
    self.sc.touchEnabled = NO;
    
    [Utils saveStreamType:@"social"];
    __weak typeof(self) wo = self;
    NSString *groupid = [NSString stringWithFormat:@"%d", self.currentGroup.groupId];
    
    dispatch_async(self.queue, ^{
        [self.rm requestSocialStreamMessageWithGroupID:groupid dataBlock:^(NSDictionary *data) {
            if (data) {
                [self.rm requestBadWordsDoneBlock:^(BOOL status) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        wo.sc.touchEnabled = YES;
                        [wo.tableRefreshControl endRefreshing];
                        [wo.tableView reloadData];
                    });
                }];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    wo.sc.touchEnabled = YES;
                    [wo.tableRefreshControl endRefreshing];
                    [wo.tableView reloadData];
                });
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSInteger count = [data[@"count"] integerValue];
                BOOL show = (self.courseCount <= 0 || count <= 0) ? YES : NO;
                [wo shouldShowEmptyView:show];
            });
        }];
    });
}

- (NSString *)getTemporaryDeviceType {
    if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        return @"iPad";
    }
    else{
        return @"iPhone";
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"type"] ];
    
    NSString *identifier = kCellTextIdentifier;
    if ([type isEqualToString:@"message"]) {
        identifier = kCellTextIdentifier;
    }
    
    if ([type isEqualToString:@"sticker"]) {
        identifier = kCellStickerIdentifier;
    }

    if ([type isEqualToString:@"image"] || [type isEqualToString:@"video"]) {
        identifier = kCellPreviewIdentifier;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    [self configureCell:cell indexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)object indexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *avatar_image_url = [NSString stringWithFormat:@"%@", [mo valueForKey:@"avatar"] ];
    NSString *user_name = [NSString stringWithFormat:@"%@ %@", [mo valueForKey:@"first_name"], [mo valueForKey:@"last_name"] ];
    NSString *date_modified = [[mo valueForKey:@"date_modified"] description];
    NSString *message = [[mo valueForKey:@"message"] description];
    
    //TODO: CHECKING FOR OFFLINE AND ONLINE STATUS
    BOOL is_online = ([[mo valueForKey:@"status"] isEqualToString:@"1"]) ? YES : NO;
    NSString *statusMessage = @"offline";
    UIImage *onlineImage = [UIImage imageNamed:@"offline_image"];
    if (is_online) {
        statusMessage = [NSString stringWithFormat:@"online via %@", [mo valueForKey:@"model"] ];
        onlineImage = [UIImage imageNamed:@"online_image"];
    }
    
    if ([[mo valueForKey:@"user_id"] isEqualToString:[self.rm loginUser]]) {
        statusMessage = [NSString stringWithFormat:@"online via %@", [self getTemporaryDeviceType]];
        onlineImage = [UIImage imageNamed:@"online_image"];
    }
    
    BOOL hidden = ([[mo valueForKey:@"owned"] isEqualToString:@"1"]) ? NO : YES;
    BOOL is_liked = ([[mo valueForKey:@"is_liked"] isEqualToString:@"1"]) ? YES : NO;
    
    NSString *type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"type"] ];
    
    if ([type isEqualToString:@"message"]) {
        
        SocialStreamItemCell *cell = (SocialStreamItemCell *)object;
        
        //------------------ RESET ALL CONTENTS
        
        cell.avatarImage.image = nil;
        cell.userNameLabel.text = @"";
        cell.firstName = @"";
        cell.lastName = @"";
        cell.emotionLabel.text = @"";
        cell.likeCountLabel.text = @"";
        cell.commentCountLabel.text = @"";
        cell.dateLabel.text = @"";
        cell.messageLabel.text = @"";
        cell.statusImage.image = nil;
        cell.statusLabel.text = @"";
        cell.feelingImage.hidden = YES;
        cell.feelingImage.image = nil;
        //------------------

        cell.avatarImage.image = [self loadImageWithURL:avatar_image_url];
        
        NSString *icon_text = [mo valueForKey:@"icon_text"];
        
        if ([icon_text isEqualToString:@""] || [icon_text isEqualToString:@"<null>"] || [icon_text isEqualToString:@"(null)"]) {
            
        } else {
            icon_text = [NSString stringWithFormat:@"is feeling %@", icon_text];
            cell.feelingImage.hidden = NO;
        }
        
        cell.userNameLabel.text = user_name;
        cell.firstName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"] ];
        cell.lastName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"] ];
        cell.emotionLabel.text = icon_text;
        cell.likeCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"likeCount"] ];
        cell.commentCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"commentCount"] ];
        cell.feelingImage.image = [self loadImageWithURL:[mo valueForKey:@"icon_url"] ];
        
        cell.dateLabel.text = date_modified;
        cell.messageLabel.text = message;
        
        //ENABLE OPTION BUTTON
        [cell.optionButton addTarget:self action:@selector(actionOptionMenuButton:) forControlEvents:UIControlEventTouchUpInside];
//        cell.optionButton.hidden = hidden;
        
        //SHOW LIKE LIST
        [cell.peopleWhoLikeButton addTarget:self action:@selector(actionShowPeopleWhoLike:) forControlEvents:UIControlEventTouchUpInside];
        
        //SHOW COMMENT LIST
        [cell.peopleWhoComment addTarget:self action:@selector(actionShowWhoComment:) forControlEvents:UIControlEventTouchUpInside];
        
        //LIKE BUTTON ACTION
        [cell.likeButton addTarget:self action:@selector(actionLike:) forControlEvents:UIControlEventTouchUpInside];
        //SET SELECTED
        cell.likeButton.selected = is_liked;
        
//        //ADD TO NOTE
//        [cell.noteButton addTarget:self action:@selector(actionAddNote:) forControlEvents:UIControlEventTouchUpInside];
        
        //COMMENT BOX
        [cell.commentButton addTarget:self action:@selector(actionShowCommentBox:) forControlEvents:UIControlEventTouchUpInside];
        
        //ONLINE STATUS
        cell.statusImage.image = onlineImage;
        cell.statusLabel.text = statusMessage;
        
//        [cell showAlias];
        
        return;
    }
    
    if ([type isEqualToString:@"sticker"]) {
        
        SocialStreamStickerCell *cell = (SocialStreamStickerCell *)object;
        
        //------------------ RESET ALL CONTENTS
        
        cell.avatarImage.image = nil;
        cell.userNameLabel.text = @"";
        cell.firstName = @"";
        cell.lastName = @"";
        cell.emotionLabel.text = @"";
        cell.likeCountLabel.text = @"";
        cell.commentCountLabel.text = @"";
        cell.dateLabel.text = @"";
        cell.stickerImageView.image = nil;
        cell.statusImage.image = nil;
        cell.statusLabel.text = @"";
        
        //------------------

        cell.avatarImage.image = [self loadImageWithURL:avatar_image_url];
        
        cell.userNameLabel.text = user_name;
        cell.firstName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"] ];
        cell.lastName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"] ];
        cell.emotionLabel.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"icon_text"] ];
        cell.likeCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"likeCount"] ];
        cell.commentCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"commentCount"] ];
        cell.dateLabel.text = date_modified;

        NSString *sticker_message = [NSString stringWithFormat:@"%@", message];
//        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", message]];
//        cell.stickerImageView.image = [[EGOImageLoader sharedImageLoader] imageForURL:imageURL shouldLoadWithObserver:nil];
        //cell.stickerImageView.image = [self loadImageWithURL:sticker_message];
        [self loadStickerWithURL:sticker_message cell:cell];
        
        //ENABLE OPTION BUTTON
        [cell.optionButton addTarget:self action:@selector(actionOptionMenuButton:) forControlEvents:UIControlEventTouchUpInside];
        cell.optionButton.hidden = hidden;
        
        //SHOW LIKE LIST
        [cell.peopleWhoLikeButton addTarget:self action:@selector(actionShowPeopleWhoLike:) forControlEvents:UIControlEventTouchUpInside];
        
        //SHOW COMMENT LIST
        [cell.peopleWhoComment addTarget:self action:@selector(actionShowWhoComment:) forControlEvents:UIControlEventTouchUpInside];
        
        //LIKE BUTTON ACTION
        [cell.likeButton addTarget:self action:@selector(actionLike:) forControlEvents:UIControlEventTouchUpInside];
        //SET SELECTED
        cell.likeButton.selected = is_liked;
        
//        //ADD TO NOTE
//        [cell.noteButton addTarget:self action:@selector(actionAddNote:) forControlEvents:UIControlEventTouchUpInside];
        
        //COMMENT BOX
        [cell.commentButton addTarget:self action:@selector(actionShowCommentBox:) forControlEvents:UIControlEventTouchUpInside];
        
        //ONLINE STATUS
        cell.statusImage.image = onlineImage;
        cell.statusLabel.text = statusMessage;
        
//        [cell showAlias];
        
        return;
    }
    
    if ([type isEqualToString:@"image"] || [type isEqualToString:@"video"]) {
        
        SocialStreamPreviewCell *cell = (SocialStreamPreviewCell *)object;
        
        //------------------ RESET ALL CONTENTS
        
        cell.avatarImage.image = nil;
        
        cell.userNameLabel.text = @"";
        cell.firstName = @"";
        cell.lastName = @"";
        cell.emotionLabel.text = @"";
        cell.likeCountLabel.text = @"";
        cell.commentCountLabel.text = @"";
        cell.dateLabel.text = @"";
        cell.messageLabel.text = @"";
        cell.previewImageView.image = nil;
        cell.previewTitleLabel.text = @"";
        cell.previewDescriptionLabel.text = @"";
        cell.statusImage.image = nil;
        cell.statusLabel.text = @"";
        
        //------------------
        
        cell.avatarImage.image = [self loadImageWithURL:avatar_image_url];
        
        cell.userNameLabel.text = user_name;
        cell.firstName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"] ];
        cell.lastName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"] ];
        cell.emotionLabel.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"icon_text"] ];
        cell.likeCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"likeCount"] ];
        cell.commentCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"commentCount"] ];
        cell.dateLabel.text = date_modified;
        
        NSString *content_id = [mo valueForKey:@"message_id"];
        
        
        NSManagedObject *po = [self.rm getEntity:kSocialStreamPreviewEntity
                                       attribute:@"content_id"
                                       parameter:content_id
                                         context:mo.managedObjectContext];
        
        /*
         url_description
         url
         title
         message_type
         image_thumb
         image
         content_id
         */
        
        cell.messageLabel.text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message"] ];
        
        NSString *image_thumb = [NSString stringWithFormat:@"%@", [po valueForKey:@"image_thumb"] ];
//        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", image_thumb]];
//        cell.previewImageView.image = [[EGOImageLoader sharedImageLoader] imageForURL:imageURL shouldLoadWithObserver:nil];
        cell.previewImageView.image = [self loadImageWithURL:image_thumb];
        
        NSString *previewTitle = [NSString stringWithFormat:@"%@", [po valueForKey:@"title"] ];
        cell.previewTitleLabel.text = previewTitle;
        
        NSString *previewDescription = [NSString stringWithFormat:@"%@", [po valueForKey:@"url_description"] ];
        cell.previewDescriptionLabel.text = previewDescription;
        
        
        //ENABLE OPTION BUTTON
        [cell.optionButton addTarget:self action:@selector(actionOptionMenuButton:) forControlEvents:UIControlEventTouchUpInside];
        cell.optionButton.hidden = hidden;
        
        //SHOW LIKE LIST
        [cell.peopleWhoLikeButton addTarget:self action:@selector(actionShowPeopleWhoLike:) forControlEvents:UIControlEventTouchUpInside];
        
        //SHOW COMMENT LIST
        [cell.peopleWhoComment addTarget:self action:@selector(actionShowWhoComment:) forControlEvents:UIControlEventTouchUpInside];
        
        //LIKE BUTTON ACTION
        [cell.likeButton addTarget:self action:@selector(actionLike:) forControlEvents:UIControlEventTouchUpInside];
        //SET SELECTED
        cell.likeButton.selected = is_liked;
        
//        //ADD TO NOTE
//        [cell.noteButton addTarget:self action:@selector(actionAddNote:) forControlEvents:UIControlEventTouchUpInside];
        
        //COMMENT BOX
        [cell.commentButton addTarget:self action:@selector(actionShowCommentBox:) forControlEvents:UIControlEventTouchUpInside];
        
        //ONLINE STATUS
        cell.statusImage.image = onlineImage;
        cell.statusLabel.text = statusMessage;
//        [cell showAlias];
        
        return;
    }
}

- (UIImage *)loadImageWithURL:(NSString *)urlstring {
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", urlstring]];
    UIImage *image = [[EGOImageLoader sharedImageLoader] imageForURL:imageURL shouldLoadWithObserver:nil];
    return image;
}

- (void)loadStickerWithURL:(NSString *)urlstring cell:(SocialStreamStickerCell *)cell {
    BOOL isHTML = [urlstring containsString:@"http"];
    
    if (isHTML) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", urlstring]];
        UIImage *image = [[EGOImageLoader sharedImageLoader] imageForURL:url shouldLoadWithObserver:nil];
        cell.stickerImageView.image = image;
    }
    
    if (!isHTML) {
        [self.rm stringByStrippingHTML:urlstring contentBlock:^(NSString *content) {
            NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer]];
            NSString *imgurl = [homeurl stringByAppendingString:content];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", imgurl]];
            UIImage *image = [[EGOImageLoader sharedImageLoader] imageForURL:url shouldLoadWithObserver:nil];
            cell.stickerImageView.image = image;
        }];
    }
}

- (void)actionOptionMenuButton:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    UIButton *b = (UIButton *)sender;
    NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
    NSArray *keys = mo.entity.propertiesByName.allKeys;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    for (NSString *key in keys) {
        NSString *val = [NSString stringWithFormat:@"%@", [mo valueForKey:key] ];
        [data setValue:val forKey:key];
    }
    
    
    NSString *type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"type"]];
    BOOL isSticker = [type isEqualToString:@"sticker"];
    
    SocialStreamOptionController *oc = (SocialStreamOptionController *)self.popover.contentViewController;
    oc.point = [sender convertPoint:CGPointZero toView:self.tableView];
    oc.data = data;
    
    BOOL owned = [[mo valueForKey:@"owned"] isEqualToString:@"1"];
    if (owned == NO) {
//        oc.optionList = @[ @{@"title":@"Delete", @"type":@"delete"} ];
        
        if (!isSticker) {
            oc.optionList = @[ @{@"title":@"Add To Note", @"type":@"add_to_note"} ];
        }
    }
    
    if (owned == YES) {
        if (isSticker) {
//            oc.optionList = @[ @{@"title":@"Delete", @"type":@"delete"},
//                               @{@"title":@"Add To Note", @"type":@"add_to_note"}
//                               ];
            oc.optionList = @[ @{@"title":@"Delete", @"type":@"delete"}
                               ];
        }
        
        if (!isSticker) {
            oc.optionList = @[ @{@"title":@"Edit", @"type":@"edit"},
                               @{@"title":@"Delete", @"type":@"delete"},
                               @{@"title":@"Add To Note", @"type":@"add_to_note"}
                               ];
        }
    }
    
    CGFloat height = 44;
    
    height = height * oc.optionList.count;
    
    oc.preferredContentSize = CGSizeMake(150, height);
    
    if ([oc.optionList  isEqual: @[]]) {
        
    } else {
        [self.popover presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//    return [[mo valueForKey:@"owned"] isEqualToString:@"1"];
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
//        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//        NSMutableDictionary *data = [@{@"message_id":[mo valueForKey:@"message_id"],
//                                       @"user_id":[mo valueForKey:@"user_id"] } mutableCopy];
//        [self.rm requestRemoveMessage:data doneBlock:^(BOOL status) {
//            if (status) {
//                NSLog(@"delete successful...");
//            }
//        }];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
//    NSString *type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"type"] ];
//    
//    if ([[mo valueForKey:@"owned"] isEqualToString:@"1"]) {
//        if (![type isEqualToString:@"sticker"]) {
//            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//            [self performSegueWithIdentifier:@"showMessageBox" sender:cell];
//        }
//    }
}

- (NSManagedObject *)managedObjectFromButtonAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (void)actionShowMessageBox:(id)sender
{
    [self performSegueWithIdentifier:@"showMessageBox" sender:sender];
}

- (void)actionShowPeopleWhoLike:(id)sender
{
    [self performSegueWithIdentifier:@"showPeopleWhoLikeBox" sender:sender];
}

- (void)actionShowWhoComment:(id)sender
{
    self.hideCommentBox = YES;
    [self performSegueWithIdentifier:@"showCommentBox" sender:sender];
}

- (void)actionShowCommentBox:(id)sender
{
    self.hideCommentBox = NO;
    [self performSegueWithIdentifier:@"showCommentBox" sender:sender];
}
// BUG FIX
- (NSString *)getUserIDString {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return user_id;
}

- (void)actionLike:(id)sender
{
    if (self.likeEnabled) {
        self.likeEnabled = NO;
    UIButton *starButton = (UIButton *)sender;
    NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
    NSString *avatar = [NSString stringWithFormat:@"%@", [mo valueForKey:@"avatar"] ];
    NSString *username = [NSString stringWithFormat:@"%@", [mo valueForKey:@"username"] ];
    NSString *user_id = [self getUserIDString]; //[NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ]; // BUG FIX #124
    NSString *message_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];
    
    //LIKE MESSAGE
    NSMutableDictionary *d = [@{@"avatar":avatar,
                               @"username":username,
                               @"user_id":user_id,
                               @"content_id":message_id,
                               @"is_message":@1,
                               @"is_deleted":@0} mutableCopy];
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        starButton.selected = !starButton.selected ? YES : NO;
//    });
    
    if (!starButton.selected) {
        [self.rm postLikeMessage:d doneBlock:^(BOOL status) {
            if (status) {
                NSLog(@"success like message...");
                dispatch_async(dispatch_get_main_queue(), ^{
                    starButton.selected = YES;
                });
                } else {
                    self.likeEnabled = YES;
            }
        }];
    }
    
    if (starButton.selected) {
        [self.rm postUnlikeMessage:d doneBlock:^(BOOL status) {
            if (status) {
                NSLog(@"success unlike message...");
                dispatch_async(dispatch_get_main_queue(), ^{
                    starButton.selected = NO;
                });
                } else {
                    self.likeEnabled = YES;
            }
        }];
    }
}
}

- (void)enableLikeButton {
    self.likeEnabled = YES;
}

- (void)actionAddNote:(id)sender
{
    NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
    NSString *messageContent = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message"] ];
    
    __weak typeof (self) ws = self;
    NoteHelper *nh = [[NoteHelper alloc] init];
    [nh saveMessage:messageContent doneBlock:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            AlertWithMessageAndDelegate(@"Note Book", @"Added Note", ws);
        });
    }];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SocialStreamFeed" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Predicate
    NSPredicate *predicate = [self.rm predicateForKeyPath:@"is_deleted" andValue:@"0"];
    [fetchRequest setPredicate:predicate];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date_created" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:self.managedObjectContext
                                                                                                  sectionNameKeyPath:nil
                                                                                                           cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] indexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void) addDataToBottomOfTableView {
    
    if (self.loadingPrevData) {
        
        NSArray *rows = [self.tableView indexPathsForVisibleRows];
        NSIndexPath *indexPath = [rows lastObject];
        
        NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSString *message_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];
        int groupid = self.currentGroup.groupId; // group id
        int messageid = [message_id intValue];

        __weak typeof (self) weakSelf = self;
        dispatch_async(self.queue, ^{
            [self.rm requestSocialStreamPrevMessageWithGroupID:groupid messageID:messageid doneBlock:^(BOOL status) {
                if (status == NO) {
                }
                weakSelf.loadingPrevData = NO;
            }];
        });
        
    }
}

#pragma mark - UIScrollViewDelegate

typedef NS_ENUM (NSInteger, ScrollDirection) {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy
};

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    ScrollDirection scrollDirection;
    if (self.lastContentOffset > scrollView.contentOffset.y) {
        scrollDirection = ScrollDirectionUp;
    }
    
    if (self.lastContentOffset < scrollView.contentOffset.y) {
        scrollDirection = ScrollDirectionDown;
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    switch (scrollDirection) {
        case ScrollDirectionUp:
            NSLog(@"scrolling up...");

            // Change 10.0 to adjust the distance from bottom
            if (maximumOffset - currentOffset <= 10.0) {
                self.loadingPrevData = YES;
                if (self.tableRefreshControl.refreshing == NO) {
                    [self addDataToBottomOfTableView];
                }
            }
            
            [self enableMessageButton:YES];
            break;
            
        case ScrollDirectionDown:
            NSLog(@"scrolling down...");
            [self enableMessageButton:YES];
            break;

        default:
            break;
    }
}

- (void)enableMessageButton:(BOOL)status {
    
    [UIView animateWithDuration:1.0f
                          delay:0.5f
         usingSpringWithDamping:0.5f
          initialSpringVelocity:1.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         if (status) {
                             self.messageButton.alpha = 1.0f;
                             self.messageButton.hidden = NO;
                         } else {
                             self.messageButton.alpha = 0.0f;
                             self.messageButton.hidden = YES;
                         }
                         
                     } completion:^(BOOL finished) {
                         //do nothing
                     }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showMessageBox"]) {
        
        SocialStreamMessage *sm = (SocialStreamMessage *)segue.destinationViewController;
        sm.mdictionary = nil;//default value
        
        if (![sender isKindOfClass:[UIButton class]]) {
            NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
            
            NSString *user_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ];
            NSString *group_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"group_id"] ];
            NSString *emoticon_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"emoticon_id"] ];
            NSString *message = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message"] ];
            NSString *is_attached = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_attached"] ];
            NSString *is_deleted = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_deleted"] ];
            NSString *message_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];

            NSString *icon_url = [NSString stringWithFormat:@"%@", [mo valueForKey:@"icon_url"] ];
            NSString *icon_text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"icon_text"] ];

            sm.mdictionary = [@{@"user_id":user_id,
                                @"group_id":group_id,
                                @"emoticon_id":emoticon_id,
                                @"message":message,
                                @"is_attached":is_attached,
                                @"is_deleted":is_deleted,
                                @"message_id":message_id
                                } mutableCopy];
            sm.message_id = message_id;
            sm.icon_url = icon_url;
            sm.icon_text = icon_text;
        }
        
        NSString *groupid = [NSString stringWithFormat:@"%d", self.currentGroup.groupId];
        sm.groupid = (NSUInteger)[groupid integerValue];
    }
    
    if ([segue.identifier isEqualToString:@"showPeopleWhoLikeBox"]) {
        NSLog(@"show people who like box");
        SocialStreamPeopleWhoLike *sspwl = (SocialStreamPeopleWhoLike *)segue.destinationViewController;
        NSManagedObject *mo = [self managedObjectFromButtonAction:sender];

        NSString *groupid = [NSString stringWithFormat:@"%d", self.currentGroup.groupId];
        sspwl.groupid = (NSUInteger)[groupid integerValue];
        sspwl.messageid = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];
    }
    
    if ([segue.identifier isEqualToString:@"showCommentBox"]) {
        
        SocialStreamComment *sc = (SocialStreamComment *)segue.destinationViewController;
        NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
        
        //GROUP ID
        NSString *groupid = [NSString stringWithFormat:@"%d", self.currentGroup.groupId];
        sc.groupid = (NSUInteger)[groupid integerValue];
        sc.messageid = [NSString stringWithFormat:@"%@", [mo valueForKey:@"message_id"] ];
        sc.hideCommentBox = self.hideCommentBox;
    }
    
}

- (void)notifyResouceManager {
    NSString *groupid = [NSString stringWithFormat:@"%d", self.currentGroup.groupId];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReturnGroupIdResourceManager object:groupid];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self loadStream];
    });
}

@end
