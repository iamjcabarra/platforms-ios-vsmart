//
//  SocialStreamComment.m
//  V-Smart
//
//  Created by Ryan Migallos on 1/22/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "SocialStreamComment.h"
#import "SocialStreamCommentCell.h"
#import "ResourceManager.h"
#import "AppDelegate.h"
#import "SocialStreamOptionController.h"
#import "AccountInfo.h"
#import "Utils.h"
#import "VSmartValues.h"

@interface SocialStreamComment () <NSFetchedResultsControllerDelegate, SocialStreamOptionDelegate, UIPopoverControllerDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *keyboardHeight;
@property (nonatomic, strong) IBOutlet UITextField *customTextField;
@property (nonatomic, strong) IBOutlet UIView *postContainerView;
@property (nonatomic, strong) UIPopoverController *popover;

@property (nonatomic, strong) ResourceManager *rm;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, assign) BOOL editMode;
@property (nonatomic, strong) NSMutableDictionary *editedComment;
@end

@implementation SocialStreamComment

static NSString *kCellIdentifier = @"cell_ss_comment_id";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //execute fetch request
    self.rm = [AppDelegate resourceInstance];
    self.managedObjectContext = self.rm.mainContext;
    
    self.tableView.estimatedRowHeight = 44.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.postContainerView.hidden = self.hideCommentBox;
    
    self.editMode = NO;
    self.editedComment = nil;
    
    [self setupOptionMenu];
}

- (void)setupOptionMenu
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SocialStreamStoryboard" bundle:nil];
    SocialStreamOptionController *oc = (SocialStreamOptionController *)[sb instantiateViewControllerWithIdentifier:@"socialStreamOptions"];
    oc.preferredContentSize = CGSizeMake(200, 100);
    oc.delegate = self;
    self.popover = [[UIPopoverController alloc] initWithContentViewController:oc];
    self.popover.popoverContentSize = CGSizeMake(200, 100);
    self.popover.delegate = self;
}

- (void)selectedMenuOption:(NSString *)option withData:(NSDictionary *)data point:(CGPoint)point {
    
    NSLog(@"option : %@ data : %@", option, data);
    [self.popover dismissPopoverAnimated:YES];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    /*
     avatar = "http://asianate.vsmartschool.com/uploads/3/avatars/20150520/27f76dd6750f8712f5ac2c93ee8cf8bd.png";
     comment = comment;
     "comment_id" = 886;
     "current_date" = "2015-05-27 09:02:28";
     "date_created" = "2015-05-18 05:23:13";
     "date_modified" = "Last week";
     "first_name" = Alexis;
     "is_deleted" = 0;
     "is_liked" = "(null)";
     "last_name" = Johnson;
     likeCount = 0;
     "message_id" = 1431;
     owned = 1;
     "user_id" = 3;
     username = ajohnson;
     */
    
    BOOL owned = [[mo valueForKey:@"owned"] isEqualToString:@"1"];
    if (owned) {
        if ([option isEqualToString:@"edit"]) {
            
            self.editMode = YES;
            
            self.postContainerView.hidden = NO;
            self.customTextField.text = [NSString stringWithFormat:@"%@", data[@"comment"] ];
            [self.customTextField becomeFirstResponder];
            
            NSString *comment_id = [NSString stringWithFormat:@"%@", data[@"comment_id"] ];
            NSString *comment = [NSString stringWithFormat:@"%@", data[@"comment"] ];
            self.editedComment = [@{@"comment_id": comment_id, @"comment": comment } mutableCopy];
            
        }
        
        if ([option isEqualToString:@"delete"]) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete", nil)
                                                                                     message:NSLocalizedString(@"Are you sure you want to delete this comment?", nil)
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *yesAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                                                     style:UIAlertActionStyleCancel
                                                                   handler:^(UIAlertAction * _Nonnull action) {
            NSString *message_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"comment_id"] ];
            NSString *user_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ];
            NSMutableDictionary *d = [@{@"comment_id":message_id, @"user_id":user_id } mutableCopy];
            [self.rm requestRemoveComment:d doneBlock:^(BOOL status) {
                if (status) {
                    NSLog(@"delete successful...");
                }
            }];
                                                                   }];
            
            UIAlertAction *noAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:nil];
            [alertController addAction:yesAlertAction];
            [alertController addAction:noAlertAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

//- (void)viewWillLayoutSubviews{
//    [super viewWillLayoutSubviews];
//    self.view.superview.bounds = CGRectMake(0, 0, 500, 500);
//}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.preferredContentSize = CGSizeMake(500, 500);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// The callback for frame-changing of keyboard
- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *info = [notification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    NSLog(@"Updating constraints.");
    
    // Because the "space" is actually the difference between the bottom lines of the 2 views,
    // we need to set a negative constant value here.
    self.keyboardHeight.constant = -height;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary *info = [notification userInfo];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    self.keyboardHeight.constant = 0;
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

//- (IBAction)dismissKeyboard:(id)sender {
//    [self.textView resignFirstResponder];
//}


- (IBAction)unwindFromConfirmationForm:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)sendUserMessage:(id)sender {
    
    NSLog(@"post comment...");
    /*
     {
     username: username,
     avatar: avatar,
     user_id: userId,
     message_id: ID,
     comment:comment,
     is_deleted: 0
     }
     */
    
    __weak typeof(self) wo = self;
    
    if (self.editedComment != nil) {
        
        self.editedComment[@"comment"] = [NSString stringWithFormat:@"%@", self.customTextField.text];
        

        [self.rm editComment:self.editedComment doneBlock:^(BOOL status) {
            
            if (status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    wo.customTextField.text = @"";
                    [wo.customTextField resignFirstResponder];
                    wo.postContainerView.hidden = YES;
                    [wo.fetchedResultsController performFetch:nil];
                    
                });
                
                NSString *message_id = self.editedComment[@"message_id"];
                [self refreshPostWithMessageID:message_id];
            }
            
        }];
    }
    
    if (self.editedComment == nil) {
        
        NSManagedObject *mo = [self.rm getEntity:kSocialStreamFeedEntity
                                       attribute:@"message_id"
                                       parameter:self.messageid
                                         context:self.managedObjectContext];
        
        NSString *comment = [NSString stringWithFormat:@"%@", self.customTextField.text];
        
        NSString *username = [NSString stringWithFormat:@"%@", [mo valueForKey:@"username"] ];
        NSString *avatar = [NSString stringWithFormat:@"%@", [mo valueForKey:@"avatar"] ];
        NSString *user_id = [self getUserIDString];
        NSString *message_id = [NSString stringWithFormat:@"%@",[mo valueForKey:@"message_id"] ];
        
        NSMutableDictionary *d = [@{
                                   @"username":username,
                                   @"avatar":avatar,
                                   @"user_id":user_id,
                                   @"message_id":message_id,
                                   @"comment":comment,
                                   @"is_deleted":[NSNumber numberWithInt:0]} mutableCopy];
        
        [self.rm postComment:d doneBlock:^(BOOL status) {
            
            if (status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    wo.customTextField.text = @"";
                    [wo.customTextField resignFirstResponder];
                });
            }
            NSLog(@"done comment post");
            [self refreshPostWithMessageID:message_id];
        }];
        
    }
    
}

- (void)refreshPostWithMessageID:(NSString *)messageID {
    [self.rm requestNotificationDetailsWithMessageID:messageID doneBlock:^(BOOL status) {
        
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)object atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *user_name = [NSString stringWithFormat:@"%@ %@", [mo valueForKey:@"first_name"], [mo valueForKey:@"last_name"] ];
    NSString *date_modified = [[mo valueForKey:@"date_modified"] description];
    NSString *comment = [[mo valueForKey:@"comment"] description];
    
    BOOL ownedByUser = ([[mo valueForKey:@"owned"] isEqualToString:@"1"]);
    
    SocialStreamCommentCell *cell = (SocialStreamCommentCell *)object;
    
    cell.userNameLabel.text = @"";
    cell.firstName = @"";
    cell.lastName = @"";
    cell.commentLabel.text = @"";
    cell.likeCountLabel.text = @"";
    cell.dateLabel.text = @"";
    cell.commentOptionButton.hidden = YES;
    [cell isOwned:NO];

    NSString *avatar_url = [NSString stringWithFormat:@"%@", [mo valueForKey:@"avatar"] ];
    cell.avatarImage.image = [self loadImageWithURL:avatar_url];
    
    cell.userNameLabel.text = user_name;
    cell.firstName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"] ];
    cell.lastName = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"] ];
    
    cell.commentLabel.text = comment;
    
    cell.likeCountLabel.text = [NSString stringWithFormat:@"(%@)", [mo valueForKey:@"likeCount"] ];
    cell.dateLabel.text = date_modified;
    
    //OPTION BUTTON ACTION
    [cell.commentOptionButton addTarget:self
                                 action:@selector(actionCommentOptionMenuButton:)
                       forControlEvents:UIControlEventTouchUpInside];
    cell.commentOptionButton.hidden = (ownedByUser == YES) ? NO : YES;
    
    //LIKE BUTTON ACTION
    [cell.likeButton addTarget:self action:@selector(actionLike:) forControlEvents:UIControlEventTouchUpInside];
    
//    [cell showAlias];
    
    [cell isOwned:ownedByUser];
}

- (UIImage *)loadImageWithURL:(NSString *)urlstring
{
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", urlstring]];
    
    UIImage *image = [[EGOImageLoader sharedImageLoader] imageForURL:imageURL shouldLoadWithObserver:nil];
    
    return image;
}

- (NSString *)getUserIDString {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return user_id;
}

- (NSManagedObject *)managedObjectFromButtonAction:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (void)actionLike:(id)sender {
    
    NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
    NSString *avatar = [NSString stringWithFormat:@"%@", [mo valueForKey:@"avatar"] ];
    NSString *username = [NSString stringWithFormat:@"%@", [mo valueForKey:@"username"] ];
    NSString *user_id = [self getUserIDString]; //[NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ]; // BUG FIX #124
    NSString *comment_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"comment_id"] ];
    
    //LIKE MESSAGE
    NSMutableDictionary *d = [ @{ @"avatar"  : avatar,
                                  @"username" : username,
                                  @"user_id" : user_id,
                                  @"content_id" : comment_id,
                                  @"is_message" : @0,
                                  @"is_deleted" : @0
                                  } mutableCopy];
    [self.rm postLikeMessage:d doneBlock:^(BOOL status) {
        
        if (status) {
            NSLog(@"success like message...");
        }
        
    }];
    
}

- (void)actionCommentOptionMenuButton:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    UIButton *b = (UIButton *)sender;
    NSManagedObject *mo = [self managedObjectFromButtonAction:sender];
    NSArray *keys = mo.entity.propertiesByName.allKeys;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    for (NSString *key in keys) {
        NSString *val = [NSString stringWithFormat:@"%@", [mo valueForKey:key] ];
        [data setValue:val forKey:key];
    }
    
    SocialStreamOptionController *oc = (SocialStreamOptionController *)self.popover.contentViewController;
    oc.point = [sender convertPoint:CGPointZero toView:self.tableView];
    oc.data = data;
    
    BOOL owned = [[mo valueForKey:@"owned"] isEqualToString:@"1"];
    if (owned == YES) {
        oc.optionList = @[ @{@"title":@"Edit", @"type":@"edit"},
                           @{@"title":@"Delete", @"type":@"delete"},
                           ];
    }
    
    [self.popover presentPopoverFromRect:b.bounds inView:b permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];    
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:kSocialStreamCommentEntity inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    //Predicate
    NSPredicate *p1 = [self.rm predicateForKeyPath:@"message_id" andValue:self.messageid];
    NSPredicate *p2 = [self.rm predicateForKeyPath:@"is_deleted" andValue:@"0"];
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates: @[p1, p2]];
    [fetchRequest setPredicate:predicate];
    
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"comment_id" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                managedObjectContext:self.managedObjectContext
                                                                                                  sectionNameKeyPath:nil
                                                                                                           cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
