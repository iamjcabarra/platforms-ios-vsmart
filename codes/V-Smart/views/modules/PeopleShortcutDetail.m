//
//  PeopleShortcutDetail.m
//  V-Smart
//
//  Created by Ryan Migallos on 4/24/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "PeopleShortcutDetail.h"
#import "PICircularProgressView.h"
#import "UIImageView+WebCache.h"

@interface PeopleShortcutDetail ()

@property (strong, nonatomic) IBOutlet UIImageView *avatarImage;
@property (strong, nonatomic) IBOutlet UILabel *profileName;
@property (strong, nonatomic) IBOutlet UILabel *profileClassName;
@property (strong, nonatomic) IBOutlet UILabel *profileRole;

@property (strong, nonatomic) IBOutlet UILabel *postStat;
@property (strong, nonatomic) IBOutlet UILabel *replyStat;
@property (strong, nonatomic) IBOutlet UILabel *likeStat;
@property (strong, nonatomic) IBOutlet UILabel *receiveStat;

@property (strong, nonatomic) IBOutlet UILabel *profileCompletenessLabel;
@property (strong, nonatomic) IBOutlet PICircularProgressView *progressView;

@property (strong, nonatomic) NSTimer *timer;
@property (assign, nonatomic) CGFloat finalProgressValue;
@property (assign, nonatomic) BOOL startAnimate;

@end

@implementation PeopleShortcutDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Detail", nil);
    self.avatarImage.image = nil;
    
    self.startAnimate = YES;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.10
                                                  target:self
                                                selector:@selector(animateProgress:)
                                                userInfo:nil
                                                 repeats:YES];
    if (self.profileData != nil) {
        self.avatarImage.layer.cornerRadius = self.avatarImage.frame.size.width / 2;
        self.avatarImage.clipsToBounds = YES;
        self.avatarImage.layer.borderWidth = 1.5f;
        self.avatarImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
        //self.avatarImage.image = [UIImage imageWithData:self.profileData[@"thumbnail"]];
        
        // Bug #1151
        // jca-05112016
        // Fix disappearing of user's avatar
        NSString *avatar_url = [self stringValue:self.profileData[@"avatar_url"]];
        
        NSLog(@"PROFILE DATA: %@", self.profileData);
        NSLog(@"AVATAR: %@", avatar_url);
        
        [self.avatarImage sd_setImageWithURL:[NSURL URLWithString:avatar_url]
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image == nil || error != nil) {
                self.avatarImage.image = [UIImage imageNamed:@"circled_student_male.png"];
            }
        }];
        
        NSString *first_name = [self stringValue:self.profileData[@"first_name"]];
        NSString *last_name = [self stringValue:self.profileData[@"last_name"]];
        NSString *section_name = [self stringValue:self.profileData[@"section_name"]];
        NSString *user_type_id = [self stringValue:self.profileData[@"user_type_id"]];
        NSString *info_percentage = [self stringValue:self.profileData[@"info_percentage"]];
        NSString *count_posted = [self stringValue:self.profileData[@"count_posted"]];
        NSString *count_liked = [self stringValue:self.profileData[@"count_liked"]];
        NSString *count_received_likes = [self stringValue:self.profileData[@"count_received_likes"]];
        NSString *count_replied = [self stringValue:self.profileData[@"count_replied"]];
        
        self.profileName.text = [NSString stringWithFormat:@"%@ %@", first_name, last_name];
        self.profileClassName.text = [NSString stringWithFormat:@"%@", section_name];
        self.profileRole.text = [NSString stringWithFormat:@"%@", [self determineRoleType:user_type_id]];
        
        __weak typeof(self) wo = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.postStat.text = [NSString stringWithFormat:@"%@", count_posted];
            wo.replyStat.text = [NSString stringWithFormat:@"%@", count_replied];
            wo.likeStat.text = [NSString stringWithFormat:@"%@", count_liked];
            wo.receiveStat.text = [NSString stringWithFormat:@"%@", count_received_likes];
        });
        
        // Bug #1151
        // jca-04252016
        self.profileCompletenessLabel.text = NSLocalizedString(@"Profile Completeness", nil);
        self.progressView.textColor = [UIColor darkGrayColor];
        self.progressView.font = FONT_NEUE_THIN(15);
        self.progressView.thicknessRatio = 0.20f;
        self.progressView.progressFillColor = UIColorFromHex(0x56c877);
        self.progressView.roundedHead = 1;
        
        CGFloat percentage = [info_percentage floatValue];
        UIColor *innerColor = UIColorFromHex(0x32b6f4);
        
        if (percentage <= 40) {
            innerColor = UIColorFromHex(0xf49a86);
        }
        else if (percentage > 41 && percentage <= 75) {
            innerColor = UIColorFromHex(0xaacbf4);
        }
        else if (percentage > 75 && percentage <= 99) {
            innerColor = UIColorFromHex(0x71f49c);
        }
        
        self.progressView.innerBackgroundColor = innerColor;
        self.finalProgressValue = percentage / 100.0f;
    }
    
//    self.avatarImage.image = [UIImage imageWithData:self.profileData[@"thumbnail"] ];
//    
//    NSString *first_name = [NSString stringWithFormat:@"%@", self.profileData[@"first_name"] ];
//    NSString *last_name = [NSString stringWithFormat:@"%@", self.profileData[@"last_name"] ];
//    NSString *section_name = [NSString stringWithFormat:@"%@", self.profileData[@"section_name"] ];
//    NSString *user_type_id = [NSString stringWithFormat:@"%@", self.profileData[@"user_type_id"] ];
//    NSString *info_percentage = [NSString stringWithFormat:@"%@", self.profileData[@"info_percentage"] ];
//    NSLog(@"info_percentage : %@", info_percentage);
//    
//    NSString *count_posted = [NSString stringWithFormat:@"%@", self.profileData[@"count_posted"] ];
//    NSString *count_liked = [NSString stringWithFormat:@"%@", self.profileData[@"count_liked"] ];
//    NSString *count_received_likes = [NSString stringWithFormat:@"%@", self.profileData[@"count_received_likes"] ];
//    NSString *count_replied = [NSString stringWithFormat:@"%@", self.profileData[@"count_replied"] ];
//    
//    self.profileName.text = [NSString stringWithFormat:@"%@ %@", first_name, last_name];
//    self.profileClassName.text = [NSString stringWithFormat:@"%@", section_name];
//    self.profileRole.text = [NSString stringWithFormat:@"%@", [self determineRoleType:user_type_id] ];
//    
//    self.postStat.text = [NSString stringWithFormat:@"%@", count_posted];
//    self.replyStat.text = [NSString stringWithFormat:@"%@", count_replied];
//    self.likeStat.text = [NSString stringWithFormat:@"%@", count_liked];
//    self.receiveStat.text = [NSString stringWithFormat:@"%@", count_received_likes];
    
    /*
     
    NSString *infoPercentage = [NSString stringWithFormat:@"%@%%", [dictProfile objectForKey:@"info_percentage"]];
    viewStatistics.percentCompleted.text = infoPercentage;
    
    float percentage = 0;
    
    if ([infoPercentage rangeOfString:@"null"].location == NSNotFound) {
        percentage = [[dictProfile objectForKey:@"info_percentage"] doubleValue];
    }
    
    UIColor *innerColor = UIColorFromHex(0x32b6f4);
    if (percentage <= 40) {
        innerColor = UIColorFromHex(0xf49a86);
    } else if (percentage > 41 && percentage <= 75) {
        innerColor = UIColorFromHex(0xaacbf4);
    } else if (percentage > 75 && percentage <= 99) {
        innerColor = UIColorFromHex(0x71f49c);
    }
    self.progressView.innerBackgroundColor = innerColor;
    
    progressValueFinish = percentage / 100;
    //            self.progressView.progress = percentage / 100;
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)determineRoleType:(NSString *)userType {
    NSString *role = @"Student";
    
    if ([userType isEqualToString:@"3"]) {
        role = @"Teacher";
    }
    
    if ([userType isEqualToString:@"4"]) {
        role = @"Student";
    }
    
    return role;
}

- (void)animateProgress:(id)sender{
    if (self.startAnimate){
        if (self.finalProgressValue <= self.progressView.progress){
            self.startAnimate = NO;
        }
        else {
            self.progressView.progress += 0.01;
        }
    }
}

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

- (NSString *)normalizeString:(NSString *)value {
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"]) {
        return @"";
    }
    
    return value;
}

@end
