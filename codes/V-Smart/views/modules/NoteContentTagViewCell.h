//
//  NoteContentTagViewCell.h
//  V-Smart
//
//  Created by VhaL on 2/13/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteContentTagViewCell : UICollectionViewCell <UITextFieldDelegate>
@property (nonatomic, weak) IBOutlet UITextField *tagLabel;
@property (nonatomic, weak) IBOutlet UIButton *closeButton;
@property (nonatomic, strong) NSString *originalText;
@property (nonatomic, assign) id delegate;
-(IBAction)removeButtonTapped:(id)sender;
@end
