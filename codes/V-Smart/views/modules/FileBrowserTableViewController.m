//
//  FileBrowserTableViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 10/22/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>
#import "FileBrowserTableViewController.h"
#import "FileBrowserTableViewCell.h"

@interface FileBrowserTableViewController ()

@property (strong, nonatomic) IBOutlet UILabel *lblTitleView;
@property (strong, nonatomic) IBOutlet UIButton *butClose;
@property (strong, nonatomic) NSArray *files;

@end

@implementation FileBrowserTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Localized title of view
    NSString *title = NSLocalizedString(@"Select File to Upload", nil);
    self.lblTitleView.text = title;
    
    // Find files from app's directory
    NSArray *extensions = @[@"doc", @"docx", @"log", @"msg", @"odt", @"pages", @"rtf", @"tex", @"txt", @"wpd", @"wps", @"csv", @"dat", @"gbr", @"key", @"keychain", @"pps", @"ppt", @"pptx", @"sdf", @"tar", @"tax2012", @"tax2014", @"vcf", @"xml", @"bmp", @"gif", @"jpg", @"png", @"psd", @"pspimage", @"tga", @"thm", @"tif", @"tiff", @"yuv", @"indd", @"pct", @"pdf", @"xlr", @"xls", @"xlsx"];
    self.files = [self findFilesWithExtensions:extensions];
    
    // Add an action to
    [self.butClose addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Locate Files From Directory

- (NSArray *)findFilesWithExtensions:(NSArray *)extensions {
    // Access documents directory
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *directoryPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [directoryPaths objectAtIndex:0];
    
    // Create collection objects for files
    NSMutableArray *files = [NSMutableArray array];
    
    // Access contents of documents directory
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    NSString *strItem;
    
    // Loop through contents and check for valid files to display
    for (strItem in contents){
        BOOL match = NO;
        
        for (int i = 0; i < extensions.count; i++) {
            NSString *lowercase = [[strItem pathExtension] lowercaseString];
            
            if ([lowercase isEqualToString:extensions[i]]) {
                match = YES;
            }
        }
        
        if (match) {
            // Create collection object for file's details
            NSMutableDictionary *fileDetailsDictionary = [NSMutableDictionary dictionary];
            
            // Get file's path
            NSString *strPath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, strItem];
            
            // Get file's attributes
            NSDictionary* attributes = [fileManager attributesOfItemAtPath:strPath error:nil];
            
            NSString *strDate = @"";
            NSString *strSize = @"";
            NSString *strMime = @"";

            if (attributes != nil) {
                // Get file's date of modification
                NSDate *date = (NSDate *)[attributes objectForKey:NSFileModificationDate];
                strDate = [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle];
            
                // Get file's size
                NSNumber *size = [NSNumber numberWithUnsignedLongLong:[attributes fileSize]];
                strSize = [self transformedValue:size];
            }
            
            // Get file's MIME Type
            strMime = [self mimeTypeForFileAtPath:strPath];
            
            // Add file's details to dictionary
            [fileDetailsDictionary setValue:strItem forKey:@"filename"];
            [fileDetailsDictionary setValue:strPath forKey:@"filepath"];
            [fileDetailsDictionary setValue:strDate forKey:@"date_modified"];
            [fileDetailsDictionary setValue:strSize forKey:@"size"];
            [fileDetailsDictionary setValue:strMime forKey:@"mimetype"];
            
            // Add dictionary to array of files
            [files addObject:fileDetailsDictionary];
        }
    }
    
    return files;
}

- (id)transformedValue:(id)value {
    double convertedValue = [value doubleValue];
    int multiplyFactor = 0;
    
    NSArray *tokens = @[@"bytes", @"KB", @"MB", @"GB", @"TB"];
    
    while (convertedValue > 1024) {
        convertedValue /= 1024;
        multiplyFactor++;
    }
    
    return [NSString stringWithFormat:@"%4.2f %@", convertedValue, tokens[multiplyFactor]];
}

- (NSString *)mimeTypeForFileAtPath:(NSString *)path {
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return nil;
    }
    
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[path pathExtension], NULL);
    CFStringRef mimeType = UTTypeCopyPreferredTagWithClass (UTI, kUTTagClassMIMEType);
    CFRelease(UTI);
    
    if (!mimeType) {
        return @"application/octet-stream";
    }
    
    return (__bridge NSString *) mimeType;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.files.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FileBrowserTableViewCell *fileBrowserCell = [tableView dequeueReusableCellWithIdentifier:@"fileCellIdentifier" forIndexPath:indexPath];
    NSDictionary *fileDetails = [self.files objectAtIndex:indexPath.row];
    
    fileBrowserCell.lblFileName.text = fileDetails[@"filename"];
    fileBrowserCell.lblFileDateModified.text = fileDetails[@"date_modified"];
    fileBrowserCell.lblFileSize.text = fileDetails[@"size"];
    fileBrowserCell.lblFileKind.text = fileDetails[@"mimetype"];
    
    return fileBrowserCell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *fileDetails = [self.files objectAtIndex:indexPath.row];
    [self.delegate selectedFile:fileDetails];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Methods for Action Buttions

- (void)closeAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
