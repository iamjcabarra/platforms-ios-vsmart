//
//  CALSectionTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 21/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class CALSectionTableViewCell: UITableViewCell {
    
    @IBOutlet var checkImage: UIImageView!
    @IBOutlet var sectionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
