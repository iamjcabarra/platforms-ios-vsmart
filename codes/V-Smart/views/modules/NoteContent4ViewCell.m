//
//  NoteContent4ViewCell.m
//  V-Smart
//
//  Created by VhaL on 2/12/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#import "NoteContent4ViewCell.h"
//#import "NoteEntry.h"
#import "NoteDataManager.h"
#import "NNoteBook.h"

#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
#define MAX_TAG 50
#define kMaxLimit 25

@implementation NoteContent4ViewCell{
//    NoteContentTagViewCell *_sizingCell;
}

@synthesize buttonBlue, buttonGreen, buttonPurple, buttonRed, buttonWhite, buttonYellow, buttonFolder, pickerFolder, delegate, buttonAdd, collectionView, tagArray, textFieldTag, selectedColorId, folderArray;

-(void)initializeCellWithDelegate:(id)parent{
    delegate = parent;
    
    pickerFolder.delegate = self;
    pickerFolder.dataSource = self;
    
    collectionView.delegate = self;
    collectionView.dataSource = self;
    
    UINib *cellNib = [UINib nibWithNibName:@"NoteContentTagViewCell" bundle:nil];
	[self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"CellTag"];
//    self.collectionView.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
//    self.collectionView.layer.borderWidth = 1;
//    self.collectionView.layer.cornerRadius = 7;
    
    tagArray = [[NSMutableArray alloc] init];
    textFieldTag.delegate = self;
    
    [self initializeButton:buttonRed];
    [self initializeButton:buttonBlue];
    [self initializeButton:buttonYellow];
    [self initializeButton:buttonGreen];
    [self initializeButton:buttonPurple];
    [self initializeButton:buttonWhite];
    
    [self MakeButtonRound:buttonRed];
    [self MakeButtonRound:buttonBlue];
    [self MakeButtonRound:buttonYellow];
    [self MakeButtonRound:buttonGreen];
    [self MakeButtonRound:buttonPurple];
    [self MakeButtonRound:buttonWhite];
    
    [self buttonColorTapped:buttonRed];
    
    folderArray = [NSArray arrayWithArray:[[NoteDataManager sharedInstance] getNoteBooks:NO useMainThread:YES]];
    [buttonFolder setTitle:kNotebookGeneralName forState:UIControlStateNormal];
//    
//    // Bug Fix
//    // jca-05052016
//    // Set Initial Selected NoteBookID
//    if (folderArray.count > 0) {
//        NoteBook *folder = [folderArray objectAtIndex:0];
//        self.selectedNoteBookID = folder.noteBookID;
//    }
}

-(void)MakeButtonRound:(UIButton*)button{
    button.layer.cornerRadius = button.frame.size.height/2;
    button.clipsToBounds = YES;
}

-(void)initializeButton:(UIButton*)buttonOuter{
    UIButton *buttonInner = [[UIButton alloc] initWithFrame:
                             CGRectMake(5, 5, buttonOuter.frame.size.width-10, buttonOuter.frame.size.height-10)];
    buttonInner.backgroundColor = buttonOuter.backgroundColor;
    buttonInner.layer.cornerRadius = buttonInner.frame.size.width/2;
    buttonInner.userInteractionEnabled = NO;
    
    buttonOuter.backgroundColor = [UIColor clearColor];
    [buttonOuter addSubview:buttonInner];
    
    buttonOuter.layer.borderWidth = 0;
    buttonOuter.layer.borderColor = buttonInner.backgroundColor.CGColor;
}

-(IBAction)buttonColorTapped:(id)sender{
    VLog(@"buttonColorTapped");
    
    buttonRed.layer.borderWidth = 0;
    buttonBlue.layer.borderWidth = 0;
    buttonGreen.layer.borderWidth = 0;
    buttonYellow.layer.borderWidth = 0;
    buttonPurple.layer.borderWidth = 0;
    buttonWhite.layer.borderWidth = 0;
    
    UIButton *button = (UIButton*)sender;
    button.layer.borderWidth = 2;
    
    if (buttonRed.layer.borderWidth == 2) {
        selectedColorId = 0;
    }
    
    if (buttonBlue.layer.borderWidth == 2) {
        selectedColorId = 1;
    }
    
    if (buttonYellow.layer.borderWidth == 2) {
        selectedColorId = 2;
    }
    
    if (buttonGreen.layer.borderWidth == 2) {
        selectedColorId = 3;
    }
    
    if (buttonPurple.layer.borderWidth == 2) {
        selectedColorId = 4;
    }
    
    if (buttonWhite.layer.borderWidth == 2) {
        selectedColorId = 5;
    }
    
    if ([delegate respondsToSelector:@selector(buttonColorTapped:)])
    {
        [delegate performSelector:@selector(buttonColorTapped:) withObject:sender];
    }
}

-(IBAction)buttonFolderTapped:(id)sender{
    VLog(@"buttonFolderTapped");
    
    pickerFolder.hidden = !pickerFolder.hidden;
    
    if ([delegate respondsToSelector:@selector(buttonFolderTapped:)])
    {
        [delegate buttonFolderTapped:sender];
    }
}

-(IBAction)buttonAddTapped:(id)sender{
    NSLog(@"buttonAddTapped");

    if (MAX_TAG <= tagArray.count){
        
        /* localizable strings */
        NSString *maxTagReached = NSLocalizedString(@"max tags reached", nil); //checked
        [self makeToast:maxTagReached duration:2.0f position:@"center"];
        
        return;
    }
    
    bool isAlreadyExisting = NO;
    
    for (NSString *textInArray in tagArray) {
        if ([textFieldTag.text isEqualToString:textInArray]){
            isAlreadyExisting = YES;
            break;
        }
    }
    
    if (!isAlreadyExisting){
        [tagArray addObject:textFieldTag.text];
        [collectionView reloadData];
    }
    
    [textFieldTag resignFirstResponder];
    textFieldTag.text = @"";
}

-(IBAction)buttonTagRemoveTapped:(id)sender{
    UITextField *textField = (UITextField*)sender;
    NSLog(@"buttonTagRemoveTapped %@", textField.text);
    
    NSString *idToBeRemoved;
    
    for (NSString *textInArray in tagArray) {
        if ([textField.text isEqualToString:textInArray]){
            idToBeRemoved = textInArray;
            break;
        }
    }
    
    [tagArray removeObject:idToBeRemoved];
    [collectionView reloadData];
}

-(IBAction)textFieldTagUpdatedReturned:(id)sender withOriginalText:(NSString*)originalText{
    UITextField *textField = (UITextField*)sender;
    
    NSLog(@"textFieldTagUpdatedReturned to:%@ from:%@", textField.text, originalText);
    
    [tagArray replaceObjectAtIndex:[tagArray indexOfObject:originalText] withObject:textField.text];
    [collectionView reloadData];
}

-(CGSize)getRectFromTagEntry:(NSIndexPath *)indexPath{
    
    NSString *label = [tagArray objectAtIndex:indexPath.row];
    CGSize stringSize = [label sizeWithDefaultFont:[UIFont systemFontOfSize:16]];
    
    return stringSize;
}

-(NSString*)getDelimitedTags{
    
    NSString *tags = @"";
    
    for (int i = 0; i < [self.collectionView numberOfItemsInSection:0]; i++) {
        
        NoteContentTagViewCell *tagCell = (NoteContentTagViewCell*)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        
        if ([tags isEqualToString:@""])
            tags = [NSString stringWithFormat:@"%@", tagCell.tagLabel.text];
        else
            tags = [NSString stringWithFormat:@"%@;%@", tags, tagCell.tagLabel.text];
    }
    
    return [tags stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

#pragma mark - PickerView

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    NSString *title = [self pickerView:pickerView titleForRow:row forComponent:component];
    [buttonFolder setTitle:title forState:UIControlStateNormal];
    
    // Bug Fix
    // Use NoteBook ID
    NoteBook *folder = [folderArray objectAtIndex:row];
    self.selectedNoteBookID = folder.noteBookID;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return folderArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NoteBook *folder = [folderArray objectAtIndex:row];
    
    return folder.noteBookName;
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [tagArray count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NoteContentTagViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"CellTag" forIndexPath:indexPath];
    cell.delegate = self;
    NSString *labelText = [tagArray objectAtIndex:indexPath.row];
    UITextField *label = (UITextField*)[cell viewWithTag:1];
    label.text = labelText;
    [cell addSubview:label];
    return cell;
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize size = [self getRectFromTagEntry:indexPath];
    CGSize sizeNew = CGSizeMake(size.width + 30, size.height + 5);
    
    NSLog(@"cell width:%f height:%f", sizeNew.width, sizeNew.height);
    
    return sizeNew;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

#pragma mark - UITextField delegate

-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textFieldDidEndEditing %@", textField.text);
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    if ([self isTextFieldMaxLimitReached:textField])
        return NO;
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    if (![string isEqualToString:filtered])
        return NO;
        
    return YES;;
}

-(BOOL)isTextFieldMaxLimitReached:(UITextField*)textField{
    
    if(textField.text.length == 0){
        if(textField.text.length != 0)
            return NO;
    }
    else if(kMaxLimit <= textField.text.length){
        return YES;
    }
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self buttonAddTapped:buttonAdd];
    
    return true;
}

@end
