//
//  LessonDetailsViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/7/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "LessonDetailsViewController.h"
#import "LessonDetailsTableViewCell.h"
#import "LessonPlanDataManager.h"
#import "LessonDetailsTabBarViewController.h"
#import "TBClassHelper.h"
#import "VSmartValues.h"

@interface LessonDetailsViewController ()

<NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UIWebViewDelegate>

@property (strong, nonatomic) LessonPlanDataManager *lm;
@property (strong, nonatomic) LessonDetailsTabBarViewController *containerViewController;

@property (strong, nonatomic) TBClassHelper *classHelper;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *uivTabBarView;

@property (strong, nonatomic) IBOutlet UILabel *lblCapSubject;
@property (strong, nonatomic) IBOutlet UILabel *lblCapLevel;
@property (strong, nonatomic) IBOutlet UILabel *lblCapUnit;
@property (strong, nonatomic) IBOutlet UILabel *lblCapQuarter;
@property (strong, nonatomic) IBOutlet UILabel *lblCapTeacher;
@property (strong, nonatomic) IBOutlet UILabel *lblCapTimeFrame;
@property (strong, nonatomic) IBOutlet UILabel *lblCapStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblSubject;
@property (strong, nonatomic) IBOutlet UILabel *lblLevel;
@property (strong, nonatomic) IBOutlet UILabel *lblUnit;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblQuarter;
@property (strong, nonatomic) IBOutlet UILabel *lblTeacher;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeFrame;

@property (strong, nonatomic) IBOutlet UIButton *butComment;
@property (strong, nonatomic) IBOutlet UIButton *butHistory;

@property (strong, nonatomic) NSString *userid;
@property (strong, nonatomic) NSString *lpid;

@property (strong, nonatomic) NSArray *stageProcessObjects;
@property (strong, nonatomic) NSMutableArray *stageProcessObjectsCount;
@property (strong, nonatomic) NSMutableArray *stageProcessInformation;
@property (strong, nonatomic) NSMutableArray *buttonStates;
@property (strong, nonatomic) NSMutableSet *collapsedSections;
@property (strong, nonatomic) NSString *selectedFilename;
@property (strong, nonatomic) NSString *selectedFilepath;

- (IBAction)butCommentTabAction:(id)sender;
- (IBAction)butHistoryTabAction:(id)sender;

@end

@implementation LessonDetailsViewController

static NSString *kStageCellIdentifier = @"stageCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set the title of navigation bar
    NSString *navTitle = [NSString stringWithFormat:@"%@", [self.lesson_details_mo valueForKey:@"name"]];
    self.title = navTitle;
    
    // Set user id
    self.userid = [NSString stringWithFormat:@"%@", [self.course_mo valueForKey:@"user_id"]];
    
    // Set lesson plan id
    self.lpid = [NSString stringWithFormat:@"%@", [self.lesson_mo valueForKey:@"id"]];
    
    // Set protocols for UITableView
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Dynamic table row height
    self.tableView.estimatedRowHeight = 70.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    // Set data manager and context
    self.lm = [LessonPlanDataManager sharedInstance];
    self.managedObjectContext = self.lm.mainContext;
    
    // Class helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Localization of strings
    NSString *subject = NSLocalizedString(@"Subject", nil);
    NSString *level = NSLocalizedString(@"Level", nil);
    NSString *unit = NSLocalizedString(@"Unit", nil);
    NSString *status = NSLocalizedString(@"Status", nil);
    NSString *quarter = NSLocalizedString(@"Quarter", nil);
    NSString *teacher = NSLocalizedString(@"Teacher", nil);
    NSString *timeframe = NSLocalizedString(@"Time Frame", nil);
    
    NSString *unitCount = [self.lm stringValue:[self.lesson_details_mo valueForKey:@"unit"]];
    if ([unitCount integerValue] > 1) {
        unit = NSLocalizedString(@"Units", nil);
    }
    
    self.lblCapSubject.text = [NSString stringWithFormat:@"%@:", subject];
    self.lblCapLevel.text = [NSString stringWithFormat:@"%@:", level];
    self.lblCapUnit.text = [NSString stringWithFormat:@"%@:", unit];
    self.lblStatus.text = [NSString stringWithFormat:@"%@:", status];
    self.lblCapQuarter.text = [NSString stringWithFormat:@"%@:", quarter];
    self.lblCapTeacher.text = [NSString stringWithFormat:@"%@:", teacher];
    self.lblCapTimeFrame.text = [NSString stringWithFormat:@"%@:", timeframe];
    
    // Initial button (collapsible) states for all sections
    self.buttonStates = [NSMutableArray arrayWithObjects:@"0", @"0", @"0", nil];
    
    // Initialize collection objects
    self.stageProcessObjects = [NSArray array];
    self.stageProcessObjectsCount = [NSMutableArray array];
    self.stageProcessInformation = [NSMutableArray array];
    self.collapsedSections = [NSMutableSet set];

    // Invoke private methods
    [self displaySelectedLesson];
    [self retrieveLearningStageProcessManagedObject];
    [self setupTabBarView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Course Information

- (void)displaySelectedLesson {
    NSString *startDate = [self.lm stringValue:[self.lesson_details_mo valueForKey:@"start_date"]];
    NSString *endDate = [self.lm stringValue:[self.lesson_details_mo valueForKey:@"end_date"]];
    NSString *status = [self.lm stringValue:[self.lesson_details_mo valueForKey:@"status"]];
    
    self.lblSubject.text = [self.lm stringValue:[self.course_mo valueForKey:@"course_name"]];
    self.lblLevel.text = [self.lm stringValue:[self.lesson_details_mo valueForKey:@"level"]];
    self.lblUnit.text = [self.lm stringValue:[self.lesson_details_mo valueForKey:@"unit"]];
    self.lblQuarter.text = [self.lm stringValue:[self.lesson_details_mo valueForKey:@"quarter"]];
    self.lblTeacher.text = [self.lm stringValue:[self.lesson_details_mo valueForKey:@"teacher_name"]];
    
    NSString *timeFrame = [NSString stringWithFormat:@"%@ %@ %@", startDate, NSLocalizedString(@"to", nil), endDate];
    
    self.lblTimeFrame.numberOfLines = 0;
    [self.classHelper justifyLabel:self.lblTimeFrame string:timeFrame];

    if ([status isEqualToString:@"0"]) {
        status = NSLocalizedString(@"Draft", nil);;
    }
    else if ([status isEqualToString:@"1"]) {
        status = NSLocalizedString(@"for Approval", nil);
    }
    else if ([status isEqualToString:@"2"])  {
        status = NSLocalizedString(@"Approved", nil);
    }
    
    self.lblStatus.text = status;
}

#pragma mark - Core Data to Collection Objects

// Retrieve learning plan stage process managed objects
- (void)retrieveLearningStageProcessManagedObject {
    NSSet *set = (NSSet *)[self.lesson_details_mo valueForKey:@"contents"];
    NSArray *stages = [set allObjects];
    
    //for (NSInteger i = 1; i <= stages.count; i++) {
    //    NSString *sKeyValue = [NSString stringWithFormat:@"%zd", i];
    //    self.stageProcessObjects = [self.lm getObjectsForEntity:kProcessEntity
    //                                                   withFKey:@"lp_id"
    //                                               andFKeyValue:self.lpid
    //                                                    andSKey:@"stage_id"
    //                                               andSKeyValue:sKeyValue
    //                                                   sortedBy:@"lc_id"];
    //   
    //    // Count the number of managed objects for each learning plan stage
    //    [self.stageProcessObjectsCount addObject:[NSNumber numberWithLong:[self.stageProcessObjects count]]];
    //    
    //    // Invoke a method that will access and save data based on fetched objects
    //    [self accessLearningStageProcessManagedObjectData:self.stageProcessObjects];
    //}
    
    // Bug Fix
    // jca-05172016
    // Fix order of learning process content
    // Making learning process stage id safe when use
    
    //NSMutableArray *stageList = [NSMutableArray array];
    //
    //for (NSManagedObject *stageMO in stages) {
    //    NSString *stage_id = [self.lm stringValue:[stageMO valueForKey:@"stage_id"]];
    //    [stageList addObject:stage_id];
    //}
    //
    //stageList = (NSMutableArray *)[stageList sortedArrayUsingSelector:@selector(localizedCompare:)];
    //
    //for (NSString *sKeyValue in stageList) {
    //    // Get stage process objects
    //    self.stageProcessObjects = [self.lm getObjectsForEntity:kProcessEntity
    //                                                   withFKey:@"lp_id"
    //                                               andFKeyValue:self.lpid
    //                                                    andSKey:@"stage_id"
    //                                               andSKeyValue:sKeyValue
    //                                                   sortedBy:@"index"];
    //
    //    // Count the number of managed objects for each learning plan stage
    //    [self.stageProcessObjectsCount addObject:[NSNumber numberWithLong:[self.stageProcessObjects count]]];
    //
    //    // Invoke a method that will access and save data based on fetched objects
    //    [self accessLearningStageProcessManagedObjectData:self.stageProcessObjects];
    //}
    
    NSMutableArray *stageList = [NSMutableArray array];
    
    for (NSManagedObject *stageMO in stages) {
        NSString *stage_id = [self.lm stringValue:[stageMO valueForKey:@"stage_id"]];
        [stageList addObject:stage_id];
    }
    
    stageList = (NSMutableArray *)[stageList sortedArrayUsingSelector:@selector(localizedCompare:)];
    
    for (NSString *sKeyValue in stageList) {
        self.stageProcessObjects = [self.lm getObjectsForEntity:kProcessEntity
                                                       withFKey:@"lp_id"
                                                   andFKeyValue:self.lpid
                                                        andSKey:@"stage_id"
                                                   andSKeyValue:sKeyValue
                                                       sortedBy:@"index"];
    
        [self.stageProcessObjectsCount addObject:[NSNumber numberWithLong:[self.stageProcessObjects count]]];
        [self.stageProcessInformation addObject:self.stageProcessObjects];
    }
}

// Access data in the retrieved managed objects for each of the stages of learning plan content
// and save them in an array
//- (void)accessLearningStageProcessManagedObjectData:(NSArray *)stageProcessObjects {
//    if ([stageProcessObjects count] > 0) {
//        
//        // Initialize temporary array that will contain each of the processes in a learning plan stage
//        NSMutableArray *processes = [NSMutableArray array];
//        
//        for (NSInteger i = 0; i < [stageProcessObjects count]; i++) {
//            // Retrieve data in the managed objects
//            NSManagedObject *mo = (NSManagedObject *)[stageProcessObjects objectAtIndex:i];
//            
//            NSString *lc_id = [mo valueForKey:@"lc_id"];
//            NSString *learning_process_name = [mo valueForKey:@"learning_process_name"];
//            NSString *content = [mo valueForKey:@"p_content"];
//            NSString *file_path = [mo valueForKey:@"file_path"];
//            NSString *orig_filename = [mo valueForKey:@"orig_filename"];
//            NSString *is_file = [mo valueForKey:@"is_file"];
//            NSString *is_deleted = [mo valueForKey:@"is_deleted"];
//            
//            // Save retrieved data in temporary dictionary
//            NSMutableDictionary *d = [NSMutableDictionary dictionary];
//            
//            [d setValue:lc_id forKey:@"lc_id"];
//            [d setValue:learning_process_name forKey:@"learning_process_name"];
//            [d setValue:content forKey:@"p_content"];
//            [d setValue:file_path forKey:@"file_path"];
//            [d setValue:orig_filename forKey:@"orig_filename"];
//            [d setValue:is_file forKey:@"is_file"];
//            [d setValue:is_deleted forKey:@"is_deleted"];
//            
//            // Save temporary dictionary into a temporary array
//            [processes addObject:d];
//        }
//        
//        // Save temporary array to an array that will contain the complete processes for learning plan stages
//        [self.stageProcessInformation addObject:processes];
//    }
//}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rowCount = 0;
    
    if (self.stageProcessObjectsCount.count > 0) {
        rowCount = [[self.stageProcessObjectsCount objectAtIndex:section] integerValue];
    }
    
    return [self.collapsedSections containsObject:@(section)] ? 0 : rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LessonDetailsTableViewCell *lessonContentStageCell = [tableView dequeueReusableCellWithIdentifier:kStageCellIdentifier forIndexPath:indexPath];
    
    //NSArray *processes = [self.stageProcessInformation objectAtIndex:indexPath.section];
    //NSDictionary *specificProcess = [processes objectAtIndex:indexPath.row];
    //NSString *lpn = specificProcess[@"learning_process_name"];
    //NSString *lpc = specificProcess[@"p_content"];
    //
    //BOOL isFile = [specificProcess[@"is_file"] isEqualToString:@"1"];
    //
    //if (isFile) {
    //    NSString *filename = specificProcess[@"orig_filename"];
    //    
    //    if ([filename isEqualToString:@"<null>"]) {
    //        lpc = @"";
    //    }
    //    else {
    //        lpc = filename;
    //    }
    //    
    //    // Change the text color from black to blue to make it look like a link
    //    lessonContentStageCell.lblProcessContent.textColor = [UIColor blueColor];
    //    
    //    // Allow user interaction for process content label (tap gesture)
    //    lessonContentStageCell.lblProcessContent.userInteractionEnabled = YES;
    //    
    //    // Create gesture recognizer for process content label
    //    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openFileOnTap:)];
    //    tapGestureRecognizer.numberOfTapsRequired = 1;
    //    [lessonContentStageCell.lblProcessContent addGestureRecognizer:tapGestureRecognizer];
    //    
    //    // Use section and row as tag for process content label
    //    NSInteger sectionAndRow = indexPath.section * 100 + indexPath.row;
    //    lessonContentStageCell.lblProcessContent.tag = sectionAndRow;
    //}
    //else {
    //    lessonContentStageCell.lblProcessContent.textColor = [UIColor blackColor];
    //    lessonContentStageCell.lblProcessContent.userInteractionEnabled = NO;
    //}
    //
    //lessonContentStageCell.lblProcessName.text = [lpn uppercaseString];
    //lessonContentStageCell.lblProcessContent.text = lpc;
    //
    //return lessonContentStageCell;
    
    NSArray *processes = [self.stageProcessInformation objectAtIndex:indexPath.section];
    NSManagedObject *processObject = [processes objectAtIndex:indexPath.row];
    NSString *lpn = [self.lm stringValue:[processObject valueForKey:@"learning_process_name"]];
    NSString *lpc = [self.lm stringValue:[processObject valueForKey:@"p_content"]];
    NSString *isf = [self.lm stringValue:[processObject valueForKey:@"is_file"]];
        
    if ([isf isEqualToString:@"1"]) {
        lpc = [self.lm stringValue:[processObject valueForKey:@"orig_filename"]];
        
        // Change the text color from black to blue to make it look like a link
        lessonContentStageCell.lblProcessContent.textColor = [UIColor blueColor];
        
        // Allow user interaction for process content label (tap gesture)
        lessonContentStageCell.lblProcessContent.userInteractionEnabled = YES;
        
        // Create gesture recognizer for process content label
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openFileOnTap:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [lessonContentStageCell.lblProcessContent addGestureRecognizer:tapGestureRecognizer];
        
        // Use section and row as tag for process content label
        NSInteger sectionAndRow = indexPath.section * 100 + indexPath.row;
        lessonContentStageCell.lblProcessContent.tag = sectionAndRow;
    }
    else {
        lessonContentStageCell.lblProcessContent.textColor = [UIColor blackColor];
        lessonContentStageCell.lblProcessContent.userInteractionEnabled = NO;
    }
    
    lessonContentStageCell.lblProcessName.text = [lpn uppercaseString];
    lessonContentStageCell.lblProcessContent.text = lpc;
    
    return lessonContentStageCell;
}

#pragma mark - Table View Delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 50)];
    
    NSString *stageName = [[[self.fetchedResultsController sections] objectAtIndex:section] name];
    NSString *sectionHeaderName = [NSString stringWithFormat:@"Stage %ld: %@", (long)(section + 1), stageName];

    // Label for section header view
    UILabel *lblSection = [UILabel new];
    lblSection.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 50);
    lblSection.text = sectionHeaderName;
    lblSection.textColor = [UIColor whiteColor];
    lblSection.textAlignment = NSTextAlignmentCenter;
    lblSection.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    lblSection.backgroundColor = [UIColor colorWithRed:(38/255.f) green:(171/255.f) blue:(226/255.f) alpha:1.0f];
    lblSection.layer.borderColor = [[UIColor whiteColor] CGColor];
    lblSection.layer.borderWidth = 1.0f;

    // Button for section header view
    UIButton *butCollapse = [UIButton buttonWithType:UIButtonTypeCustom];
    butCollapse.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 50);
    butCollapse.backgroundColor = [UIColor clearColor];
    butCollapse.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    butCollapse.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    // Add action to button
    [butCollapse addTarget:self action:@selector(sectionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    butCollapse.selected = YES;
    butCollapse.tag = section;
    
    // Disable if no processes
    butCollapse.userInteractionEnabled = (self.stageProcessObjectsCount.count > 0) ? YES : NO;
    
    UIImage *imgNCollapsed = [UIImage imageNamed:@"Plus-12px.png"];
    UIImage *imgYCollapsed = [UIImage imageNamed:@"Minus-12px.png"];
    
    if ([self.buttonStates[section] isEqualToString:@"1"]) {
        [butCollapse setImage:imgNCollapsed forState:UIControlStateSelected];
    }
    else {
        [butCollapse setImage:imgYCollapsed forState:UIControlStateSelected];
    }

    // Adding label and button in section header view
    [sectionHeaderView addSubview:lblSection];
    [sectionHeaderView addSubview:butCollapse];

    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedTabViewContainer"]) {
        self.containerViewController = [segue destinationViewController];
        self.containerViewController.lesson_mo = self.lesson_mo;
    }
}

#pragma mark - Collapsible Table View Methods

- (void)sectionButtonTouchUpInside:(UIButton *)sender {
    UIButton *button = (UIButton *)sender;
    NSInteger section = sender.tag;
    bool shouldCollapse = ![self.collapsedSections containsObject:@(section)];
    
    [self.tableView beginUpdates];
    
    if (shouldCollapse) {
        NSInteger numOfRows = [self.tableView numberOfRowsInSection:section];
        NSArray *indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        
        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [self.collapsedSections addObject:@(section)];

        [button setImage:[UIImage imageNamed:@"Plus-12px.png"] forState:UIControlStateSelected];
        self.buttonStates[section] = @"1";
        
    }
    else {
        NSInteger numOfRows = [[self.stageProcessObjectsCount objectAtIndex:section] integerValue];
        NSArray *indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [self.collapsedSections removeObject:@(section)];

        [button setImage:[UIImage imageNamed:@"Minus-12px.png"] forState:UIControlStateSelected];
        self.buttonStates[section] = @"0";
    }
    
    [self.tableView endUpdates];
}

- (NSArray*)indexPathsForSection:(NSInteger)section withNumberOfRows:(NSInteger)numberOfRows {
    NSMutableArray* indexPaths = [NSMutableArray array];
    
    for (int i = 0; i < numberOfRows; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPaths addObject:indexPath];
    }
    
    return indexPaths;
}

#pragma mark - Tab Bar View Initial Set-up

- (void)setupTabBarView {
    [self.butComment setBackgroundColor:[UIColor whiteColor]];
    [self.butComment setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.butHistory setBackgroundColor:[UIColor colorWithRed:(38/255.f) green:(171/255.f) blue:(226/255.f) alpha:1.0f]];
    [self.butHistory setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

#pragma mark - Methods for Action Buttons

- (IBAction)butCommentTabAction:(id)sender {
    [self.containerViewController swapViewControllers:@"embedCommentTab"];
    [self.butComment setBackgroundColor:[UIColor whiteColor]];
    [self.butComment setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.butHistory setBackgroundColor:[UIColor colorWithRed:(38/255.f) green:(171/255.f) blue:(226/255.f) alpha:1.0f]];
    [self.butHistory setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)butHistoryTabAction:(id)sender {
    [self.containerViewController swapViewControllers:@"embedHistoryTab"];
    [self.butComment setBackgroundColor:[UIColor colorWithRed:(38/255.f) green:(171/255.f) blue:(226/255.f) alpha:1.0f]];
    [self.butComment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.butHistory setBackgroundColor:[UIColor whiteColor]];
    [self.butHistory setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

#pragma mark - Tap Gesture Recognizer Selector

- (void)openFileOnTap:(UITapGestureRecognizer *)tapGesture {
    NSLog(@"Sender: %li", (long)tapGesture.view.tag);
    
    NSInteger section;
    NSInteger row;
    NSArray *processes;
    NSDictionary *specificProcess;
    NSString *filename;
    NSString *filepath;
    
    section = tapGesture.view.tag / 100;
    row = tapGesture.view.tag % 100;
 
    processes = [self.stageProcessInformation objectAtIndex:section];
    specificProcess = [processes objectAtIndex:row];
    
    filename = specificProcess[@"orig_filename"];
    filepath = specificProcess[@"file_path"];
    
    NSString *openTitle = NSLocalizedString(@"Open", nil);
    NSString *cancelTitle = NSLocalizedString(@"Cancel", nil);
    
    // Set values to global variables
    self.selectedFilename = filename;
    self.selectedFilepath = filepath;
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                 message:filename
                                                delegate:self
                                       cancelButtonTitle:cancelTitle
                                       otherButtonTitles:openTitle, nil];
    
    [av show];
}

#pragma mark - Alert View Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 1:
            [self openFileWithFilePath:self.selectedFilepath andURLType:@"1"];
            break;
        default:
            break;
    }
}

#pragma mark - Render File in Web View

- (void)openFileWithFilePath:(NSString *)filepath andURLType:(NSString *)type {
    CGRect rect = [[UIScreen mainScreen] bounds];
    CGSize screenSize = rect.size;
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    [self setupActivityIndicator:webView];
    webView.delegate = self;
    
    NSURL *targetURL;
    
    if ([type isEqualToString:@"1"]) {
        // File server url (file is from the server)
        targetURL = [NSURL URLWithString:filepath];
    }
    else {
        // File device url (file is from the device)
        targetURL = [NSURL fileURLWithPath:filepath];
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [webView loadRequest:request];
    [self.view addSubview:webView];
}

#pragma mark - Activity Indicator

- (void)setupActivityIndicator:(UIWebView *)view {
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.frame = CGRectMake(0.0f, 0.0f, 40.0f, 40.0f);
    self.activityIndicator.center = CGPointMake(self.view.bounds.size.width / 2.0f, self.view.bounds.size.height / 2.0f);
    self.activityIndicator.autoresizingMask = (UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin);
    self.activityIndicator.backgroundColor = [UIColor whiteColor];
    self.activityIndicator.layer.cornerRadius = 5.0f;
    [view addSubview:self.activityIndicator];
    [self.activityIndicator bringSubviewToFront:view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
}

#pragma mark - Web View Delegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self.activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.activityIndicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSString *avTitle = NSLocalizedString(@"Error", nil);
    NSString *message = [NSString stringWithFormat:@"%@", error];
    NSString *okayTitle = NSLocalizedString(@"Okay", nil);
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:avTitle
                                                 message:message
                                                delegate:self
                                       cancelButtonTitle:nil
                                       otherButtonTitles:okayTitle, nil];
    [av show];
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCopyLessonContentEntity];
    [fetchRequest setFetchBatchSize:50];
    
    NSSortDescriptor *stage_id = [NSSortDescriptor sortDescriptorWithKey:@"stage_id.intValue"
                                                               ascending:YES
                                                                selector:@selector(caseInsensitiveCompare:)];
    [fetchRequest setSortDescriptors:@[stage_id]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:@"stage_name"
                                                                                     cacheName:nil];
    
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

@end
