//
//  IdeaPadViewController.h
//  V-Smart
//
//  Created by VhaL on 3/5/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IdeaPadCell.h"
#import "ShortcutDataManager.h"
#import <MessageUI/MessageUI.h>
#import "UIViewController+Popup.h"
#import "NoteContentViewController.h"

@interface IdeaPadViewController : UIViewController <UITextViewDelegate, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>
@property (nonatomic, strong) UIPopoverController *parent;
@end
