//
//  CurriculumDataManager.m
//  V-Smart
//
//  Created by Ryan Migallos on 9/15/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "CurriculumDataManager.h"
#import "Utils.h"
#import "AccountInfo.h"
#import "VSmartValues.h"
#import "NSDate+Helper.h"

static NSString *storeFilename = @"curriculumdata.sqlite";
static NSString *kCoreDataFrameworkModel = @"CurriculumDataModel";

@interface CurriculumDataManager()

#pragma mark - PROPERTIES

@property (nonatomic, readwrite) NSManagedObjectContext *masterContext;
@property (nonatomic, readwrite) NSManagedObjectContext *mainContext;
@property (nonatomic, readwrite) NSManagedObjectContext *workerContext;
@property (nonatomic, readwrite) NSManagedObjectModel *model;
@property (nonatomic, readwrite) NSPersistentStore *store;
@property (nonatomic, readwrite) NSPersistentStoreCoordinator *coordinator;
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSDateFormatter *formatter;

@end

@implementation CurriculumDataManager

#pragma mark - SINGLETON

+ (instancetype)sharedInstance {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    static CurriculumDataManager *singleton = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        singleton = [[self alloc] init];
    });
    return singleton;
}

#pragma mark - SETUP

- (id)init {
    NSLog(@"Running %s", __PRETTY_FUNCTION__);

    self = [super init];
    if (!self) { return nil; }
    
    // Managed Object Model
    self.model = [self modelFromFrameWork:kCoreDataFrameworkModel];
    // Persistent Store Coordinator
    self.coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    
    BOOL compatible = [self isCompatibleWithCoreDataMetadata:[self storeURL]];
    if (!compatible) {
        self.store = nil;
    }
    
    // CORE DATA 3-LAYER STACK for MASTER, MAIN, WORKER
    [self initializeManagedObjectsWithCoordinator:self.coordinator];
    
    //SESSION MANAGER
    self.session = [NSURLSession sharedSession];
    
    //DATE FORMATTER
    self.formatter = [[NSDateFormatter alloc] init];
    
    return self;
}

- (void)initializeManagedObjectsWithCoordinator:(NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // DATA FLUSHER
    // [WARNING: YOU ARE NOT ALLOWED TO USE THIS CONTEXT]
    // Core Data Stack for the Master Thread [MASTER] -> [PERSISTENTSTORE]
    _masterContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_masterContext performBlockAndWait:^{
        [_masterContext setPersistentStoreCoordinator:persistentStoreCoordinator];
        [_masterContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // UI RELATED CONTEXT
    // Core Data Stack for the Main Thread [MAIN] -> [MASTER]
    _mainContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_mainContext setParentContext:_masterContext];
    [_mainContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    
    // BACKGROUND CONTEXT
    // Core Data Stack for the Worker Thread [WORKER] -> [MAIN]
    _workerContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_workerContext performBlockAndWait:^{
        [_workerContext setParentContext:_mainContext];
        [_workerContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // LOAD STORE FILE
    [self loadStore];
}

- (void)loadStore {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSDictionary *sqliteConfig = @{@"journal_mode" : @"DELETE"};//@{@"journal_mode" : @"WAL"}
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption  : @YES ,
                              NSInferMappingModelAutomaticallyOption        : @YES , //Light Weight Migration
                              NSSQLitePragmasOption                         : sqliteConfig
                              };
    NSError *error = nil;
    _store = [_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:[self storeURL] options:options error:&error];
    if (!_store) {
        //abort();
    } else {
        NSLog(@"Successfully added store: %@", _store);
    }
}

#pragma mark - MODELS (Initialization)

- (NSManagedObjectModel *)modelFromFrameWork:(NSString *)name {
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSURL *modelFileURL = [appBundle URLForResource:name withExtension:@"momd"];
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelFileURL];
    
    return model;
}

#pragma mark - SAVING

- (void)saveContext {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSManagedObjectContext *ctx = _workerContext;
    [ctx performBlockAndWait:^{
        [self saveTreeContext:ctx];
    }];
}

- (void)saveTreeContext:(NSManagedObjectContext *)context {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    if (!context) {
        return;
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"ERROR saving: %@", error);
    }
    
    if (context.parentContext) {
        [self saveTreeContext:context.parentContext];
    }
}

#pragma mark - PATHS

- (NSURL *)storeURL {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSURL *fileURL = [self applicationStoresDirectory];
    
    return [fileURL URLByAppendingPathComponent:storeFilename];
}

- (NSURL *)applicationStoresDirectory {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    NSString *filePath = [self applicationDocumentsDirectory];
    NSURL *storesDirectory = [[NSURL fileURLWithPath:filePath] URLByAppendingPathComponent:@"Stores"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtURL:storesDirectory withIntermediateDirectories:YES attributes:nil error:&error]) {
            NSLog(@"Successfully created Stores directory");
            if (error) {
                NSLog(@"Failed to create Stores directory : %@", error);
            }
        }
    }
    return storesDirectory;
}

- (NSString *)applicationDocumentsDirectory {
    
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    return [NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - FAULTING (Memory Management)

- (void)faultObjectWithID:(NSManagedObjectID *)objectID inContext:(NSManagedObjectContext *)context {
    
    if (!objectID || !context) { return; }
    
    NSManagedObject *object = [context objectWithID:objectID];
    if (object.hasChanges) {
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"ERROR saving: %@", error);
        }
    }
    if (!object.isFault) {
        [context refreshObject:object mergeChanges:NO];
    }
    else {
        NSLog(@"Skipped faulting an object that is already a fault");
    }
    
    // Repeat the process if the context has a parent
    if (context.parentContext) {
        [self faultObjectWithID:objectID inContext:context.parentContext];
    }
}

#pragma mark - MIGRATION

- (BOOL)isCompatibleWithCoreDataMetadata:(NSURL *)storeUrl {
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *filePath = [NSString stringWithFormat:@"%@", storeUrl.path];
    
    if ([fm fileExistsAtPath:filePath]) {
        NSLog(@"checking model for compatibility...");
        
        NSError *error = nil;
        NSDictionary *metaData = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
                                                                                            URL:storeUrl
                                                                                          error:&error];
        NSManagedObjectModel *model = self.coordinator.managedObjectModel;
        
        if ([model isConfiguration:nil compatibleWithStoreMetadata:metaData]) {
            NSLog(@"model is compatible...");
            return YES;
            
        } else {
            
            if ([fm removeItemAtPath:filePath error:NULL]) {
                NSLog(@"removed file : %@", filePath);
                NSLog(@"model is incompatible...");
                return NO;
            }//REMOVE THE STORE FILE
            
        }
    }
    
    NSLog(@"file does not exists..");
    return NO;
}

- (BOOL)migrateStore:(NSURL *)sourceStore {
    NSLog(@"SKIPPED MIGRATION: Source is already compatible");
    
    BOOL success = NO;
    NSError *error = nil;
    
    //STEP 1 - Gather the Source, Destination and Mapping Model
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:sourceStore error:&error];
    NSManagedObjectModel *sourceModel = [NSManagedObjectModel mergedModelFromBundles:nil forStoreMetadata:sourceMetadata];
    NSManagedObjectModel *destinModel = _model;
    NSMappingModel *mappingModel = [NSMappingModel mappingModelFromBundles:nil forSourceModel:sourceModel destinationModel:destinModel];
    
    //STEP 2 - Perform Migration, assuming the mapping model isn't null
    if (mappingModel) {
        NSMigrationManager *migrationManager = [[NSMigrationManager alloc] initWithSourceModel:sourceModel destinationModel:destinModel];
        
        //OBSERVER
        [migrationManager addObserver:self forKeyPath:@"migrationProgress" options:NSKeyValueObservingOptionNew context:NULL];
        
        NSURL *destinStore = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Temp.sqlite"];
        success = [migrationManager migrateStoreFromURL:sourceStore
                                                   type:NSSQLiteStoreType
                                                options:nil
                                       withMappingModel:mappingModel
                                       toDestinationURL:destinStore
                                        destinationType:NSSQLiteStoreType
                                     destinationOptions:nil
                                                  error:&error];
        if (success) {
            //STEP 3 - Replace the old store with the new migrated store
            if ([self replaceStore:sourceStore withStore:destinStore]) {
                NSLog(@"SUCCESSFULLY MIGRATED %@ to the Current Model", sourceStore.path);
                [migrationManager removeObserver:self forKeyPath:@"migrationProgress"];
            }
            //STEP 3
        }
        else {
            NSLog(@"FAILED MIGRATION : %@", error);
        }
    }
    else {
        NSLog(@"FAILED MIGRATION: Mapping Model is null");
    }
    
    return YES;// indicates migration has finished, regardless of outcome
}

- (BOOL)replaceStore:(NSURL *)old withStore:(NSURL *)new {
    
    BOOL success = NO;
    NSError *Error = nil;
    if ([[NSFileManager defaultManager] removeItemAtURL:old error:&Error]) {
        
        Error = nil;
        if ([[NSFileManager defaultManager] moveItemAtURL:new toURL:old error:&Error]) {
            success = YES;
        }
        else {
            NSLog(@"FAILED to re-home new store %@", Error);
        }
    }
    else {
        NSLog(@"FAILED to remove old store %@: Error:%@", old, Error);
    }
    return success;
}

#pragma mark - WORKER

- (NSString *)baseURL {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *server = [NSString stringWithFormat:@"%@", [defaults stringForKey:@"baseurl_preference"] ];
    //    NSString *server = @"172.16.7.174"; //HARD CODING
    
    return server;
}

- (NSURL *)buildURL:(NSString *)string {
    
    NSString *path = [Utils buildUrl:string];
    NSLog(@"path : %@", path);
    return [NSURL URLWithString:path];
}

- (NSString *)emptyString:(NSString *)value {
    
    if ([value isEqualToString:@"(null)"]) {
        return @"";
    }
    return value;
}

- (NSString *)stringValue:(id)object {
    
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"]) {
        return @"";
    }
    
    return value;
}

- (BOOL)isArrayObject:(id)object {
    return [object isKindOfClass:[NSArray class]];
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption | NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSManagedObject *)getEntity:(NSString *)entity attribute:(NSString *)attribute
                     parameter:(NSString *)parameter context:(NSManagedObjectContext *)context {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (parameter) {
        
        // create predicate
        NSPredicate *predicate = [self predicateForKeyPath:attribute andValue:parameter];
        
        // set predicate
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
}

- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSManagedObjectContext *context = _workerContext;
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *lmo in items) {
            [context deleteObject:lmo];
        }
        [self saveTreeContext:context];
        
        return YES;
    }
    
    return NO;
}

- (BOOL)clearDataForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)ctx {
    
    //CLEAR CONTENTS
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate != nil) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *mo in items) {
            [ctx deleteObject:mo];
        }
        [self saveTreeContext:ctx];
    }
    
    return YES;
}

- (NSTimeInterval)parseTimeFromString:(NSString *)string {
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    
    NSInteger h, m, s;
    [scanner scanInteger:&h];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&m];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&s];
    
    return h * 3600 + m * 60 + s;
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (NSDate *)parseDateFromString:(NSString *)dateString {
    
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@".000Z" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" togo" withString:@""];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [self.formatter setTimeZone:gmt];
    NSString *template = @"yyyy-MM-dd hh:mm:ss";
    [self.formatter setDateFormat:template];
    NSDate *dateObject = [self.formatter dateFromString:dateString];
    return dateObject;
}

- (id)parseResponseData:(NSData *)data {
    
    
    if (data) {
        
        NSError *jsonError = nil;
        
        id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
        
        if (jsonError) {
            NSLog(@"JSON Error : %@", jsonError.localizedDescription);
        }
        
        if (!jsonError) {
            return object;
        }
        
    }
    
    
    return nil;
}

- (NSMutableURLRequest *)buildRequestWithMethod:(NSString *)method NSURL:(NSURL *)url body:(id)body {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:method];
    
    if (body) {
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:body options:0 error:&error];
        
        NSString *j_string = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        NSLog(@"j_string : <start>---- %@ ---<end>", j_string);
        [request setHTTPBody:postData];
    }
    
    return request;
}

- (BOOL)ownedByUser:(NSString *)userid {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return [user_id isEqualToString:userid];
}

#pragma mark - CURRICULUM 

- (void)requestCurriculumListWithBlock:(CurriculumDoneBlock)doneBlock {
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCurriculumPlannerList]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              
              if (dictionary) {
                  
                  if (dictionary[@"records"] != nil ) {
                      
                      NSArray *records = dictionary[@"records"];
                      if (records.count > 0) {
                          
                          NSManagedObjectContext *ctx = self.workerContext;
                          [ctx performBlock:^{
                              
                              [ctx performBlockAndWait:^{
                                  [self clearDataForEntity:kCurriculumListEntity withPredicate:nil context:ctx];
                              }];
                              
                              for (NSDictionary *d in records) {
                                  
                                  
                                  /*
                                   curriculum_id: 1,
                                   curriculum_template_id: 1,
                                   curriculum_template_name: "PH K-12",
                                   curriculum_title: "sample 1",
                                   curriculum_description: "sample 1 description",
                                   curriculum_status: 0,
                                   curriculum_status_remarks: "Draft",
                                   course_id: 1,
                                   course_name: "Mathematics",
                                   grade_level_id: 0,
                                   grade_level_name: null,
                                   school_profile_id: 1,
                                   school_profile_name: "Default School",
                                   sy_id: 0,
                                   sy_name: null,
                                   created_by: "School Admin",
                                   date_created: "2015-09-15 06:07:21",
                                   date_modified: "2015-09-15 06:07:21"
                                   */
                                  
                                  if (d != nil) {
                                      [self processCurriculumObject:d entity:kCurriculumListEntity context:ctx];
                                  }
                                  
//                                  [self processCurriculumObject:d entity:kCurriculumListEntity context:ctx];
                              }
                              
                              [self saveTreeContext:ctx];
                              
                              if (doneBlock) {
                                  doneBlock(YES);
                              }
                              
                          }];
                          
                      }
                      
                      if (doneBlock) {
                          doneBlock(NO);
                      }
                  }
              }
              

          }//end
          
      }];
    
    [task resume];
    
}

- (void)requestCurriculumWithID:(NSString *)curriculumid doneBlock:(CurriculumDoneBlock)doneBlock {
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCurriculumDetails, curriculumid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
          
          if (!error) {
              
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              
              if (dictionary[@"records"] != nil ) {
                  
                  NSDictionary *d = dictionary[@"records"];
                  
//                  NSLog(@"data : %@", d);
                  
                  NSManagedObjectContext *ctx = _workerContext;
                  [ctx performBlock:^{
                      
                      NSString *curriculum_id = [self stringValue:d[@"curriculum_id"]];
                      NSString *curriculum_template_id = [self stringValue:d[@"curriculum_template_id"]];
                      NSString *curriculum_template_name = [self stringValue:d[@"curriculum_template_name"]];
                      NSString *curriculum_title = [self stringValue:d[@"curriculum_title"]];
                      NSString *curriculum_description = [self stringValue:d[@"curriculum_description"]];
                      NSString *curriculum_status = [self stringValue:d[@"curriculum_status"]];
                      NSString *curriculum_status_remarks = [self stringValue:d[@"curriculum_status_remarks"]];
                      NSString *course_id = [self stringValue:d[@"course_id"]];
                      NSString *course_name = [self stringValue:d[@"course_name"]];
                      NSString *grade_level_id = [self stringValue:d[@"grade_level_id"]];
                      NSString *grade_level_name = [self stringValue:d[@"grade_level_name"]];
                      NSString *school_profile_id = [self stringValue:d[@"school_profile_id"]];
                      NSString *school_profile_name = [self stringValue:d[@"school_profile_name"]];
                      NSString *sy_id = [self stringValue:d[@"sy_id"]];
                      NSString *sy_name = [self stringValue:d[@"sy_name"]];
                      NSString *created_by = [self stringValue:d[@"created_by"]];
                      NSString *date_created = [self stringValue:d[@"date_created"]];
                      NSString *date_modified = [self stringValue:d[@"date_modified"]];
                      
                      NSManagedObject *mo = [self getEntity:kCurriculumTableEntity
                                                  attribute:@"curriculum_id"
                                                  parameter:curriculum_id
                                                    context:ctx];
                      
                      [mo setValue:curriculum_id forKey:@"curriculum_id"];
                      [mo setValue:curriculum_template_id forKey:@"curriculum_template_id"];
                      [mo setValue:curriculum_template_name forKey:@"curriculum_template_name"];
                      [mo setValue:curriculum_title forKey:@"curriculum_title"];
                      [mo setValue:curriculum_description forKey:@"curriculum_description"];
                      [mo setValue:curriculum_status forKey:@"curriculum_status"];
                      [mo setValue:curriculum_status_remarks forKey:@"curriculum_status_remarks"];
                      [mo setValue:course_id forKey:@"course_id"];
                      [mo setValue:course_name forKey:@"course_name"];
                      [mo setValue:grade_level_id forKey:@"grade_level_id"];
                      [mo setValue:grade_level_name forKey:@"grade_level_name"];
                      [mo setValue:school_profile_id forKey:@"school_profile_id"];
                      [mo setValue:school_profile_name forKey:@"school_profile_name"];
                      [mo setValue:sy_id forKey:@"sy_id"];
                      [mo setValue:sy_name forKey:@"sy_name"];
                      [mo setValue:created_by forKey:@"created_by"];
                      [mo setValue:date_created forKey:@"date_created"];
                      [mo setValue:date_modified forKey:@"date_modified"];
                      
                      NSString *search_string = [NSString stringWithFormat:@"%@ %@", curriculum_title, curriculum_description];
                      [mo setValue:[search_string lowercaseString] forKey:@"search_string"];
                      
                      if ( [self isArrayObject:d[@"periods"]] ) {
                          NSArray *periods = d[@"periods"];
                          [self processPeriods:periods managedObject:mo];
                      }
                      
                      //        if ( (d[@"comments"] != nil) && [self isArrayObject:d[@"comments"]] ) {
                      //            // do nothing
                      //        }
                      
                      //        if ( (d[@"histories"] != nil) && [self isArrayObject:d[@"histories"]] ) {
                      //            // do nothing
                      //        }
                      
                      [self saveTreeContext:ctx];
                      
                  }];

                  if (doneBlock) {
                      doneBlock(YES);
                  }
              }
          }//end
          
      }];
    
    [task resume];
    
}

- (void)processCurriculumObject:(NSDictionary *)d entity:(NSString *)entity context:(NSManagedObjectContext *)ctx {
    
    NSString *curriculum_id = [self stringValue:d[@"curriculum_id"]];
    NSString *curriculum_template_id = [self stringValue:d[@"curriculum_template_id"]];
    NSString *curriculum_template_name = [self stringValue:d[@"curriculum_template_name"]];
    NSString *curriculum_title = [self stringValue:d[@"curriculum_title"]];
    NSString *curriculum_description = [self stringValue:d[@"curriculum_description"]];
    NSString *curriculum_status = [self stringValue:d[@"curriculum_status"]];
    NSString *curriculum_status_remarks = [self stringValue:d[@"curriculum_status_remarks"]];
    NSString *course_id = [self stringValue:d[@"course_id"]];
    NSString *course_name = [self stringValue:d[@"course_name"]];
    NSString *grade_level_id = [self stringValue:d[@"grade_level_id"]];
    NSString *grade_level_name = [self stringValue:d[@"grade_level_name"]];
    NSString *school_profile_id = [self stringValue:d[@"school_profile_id"]];
    NSString *school_profile_name = [self stringValue:d[@"school_profile_name"]];
    NSString *sy_id = [self stringValue:d[@"sy_id"]];
    NSString *sy_name = [self stringValue:d[@"sy_name"]];
    NSString *created_by = [self stringValue:d[@"created_by"]];
    NSString *date_created = [self stringValue:d[@"date_created"]];
    NSString *date_modified = [self stringValue:d[@"date_modified"]];
    
    NSManagedObject *mo = [self getEntity:entity attribute:@"curriculum_id" parameter:curriculum_id context:ctx];
    
    [mo setValue:curriculum_id forKey:@"curriculum_id"];
    [mo setValue:curriculum_template_id forKey:@"curriculum_template_id"];
    [mo setValue:curriculum_template_name forKey:@"curriculum_template_name"];
    [mo setValue:curriculum_title forKey:@"curriculum_title"];
    [mo setValue:curriculum_description forKey:@"curriculum_description"];
    [mo setValue:curriculum_status forKey:@"curriculum_status"];
    [mo setValue:curriculum_status_remarks forKey:@"curriculum_status_remarks"];
    [mo setValue:course_id forKey:@"course_id"];
    [mo setValue:course_name forKey:@"course_name"];
    [mo setValue:grade_level_id forKey:@"grade_level_id"];
    [mo setValue:grade_level_name forKey:@"grade_level_name"];
    [mo setValue:school_profile_id forKey:@"school_profile_id"];
    [mo setValue:school_profile_name forKey:@"school_profile_name"];
    [mo setValue:sy_id forKey:@"sy_id"];
    [mo setValue:sy_name forKey:@"sy_name"];
    [mo setValue:created_by forKey:@"created_by"];
    [mo setValue:date_created forKey:@"date_created"];
    [mo setValue:date_modified forKey:@"date_modified"];
    
    NSString *search_string = [NSString stringWithFormat:@"%@ %@", curriculum_title, curriculum_description];
    [mo setValue:[search_string lowercaseString] forKey:@"search_string"];
    
    if ([entity isEqualToString:kContentTableEntity]) {

        if ( [self isArrayObject:d[@"periods"]] ) {
            NSArray *periods = [NSArray arrayWithArray: d[@"periods"] ];
            NSLog(@"periods : %@", periods);
            [self processPeriods:periods managedObject:mo];
        }

//        if ( (d[@"comments"] != nil) && [self isArrayObject:d[@"comments"]] ) {
//            // do nothing
//        }

//        if ( (d[@"histories"] != nil) && [self isArrayObject:d[@"histories"]] ) {
//            // do nothing
//        }
    }
}

- (void)processPeriods:(NSArray *)periods managedObject:(NSManagedObject *)object {
    
    if (periods.count > 0) {
        
        NSMutableSet *set = [NSMutableSet set];
        NSInteger count = 0;
        for (NSDictionary *d in periods) {
            
            /*
             "period_id": 3,
             "name": "First Grading Period",
             "date_created": "2015-09-15 06:16:33",
             "date_modified": "2015-09-15 06:16:33",
             */
            
            NSString *period_id = [self stringValue:d[@"period_id"]];
            NSString *name = [self stringValue:d[@"name"]];
            NSString *date_created = [self stringValue:d[@"date_created"]];
            NSString *date_modified = [self stringValue:d[@"date_modified"]];
            NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kPeriodTableEntity
                                                                inManagedObjectContext:object.managedObjectContext];
            [mo setValue:period_id forKey:@"period_id"];
            [mo setValue:name forKey:@"name"];
            [mo setValue:date_created forKey:@"date_created"];
            [mo setValue:date_modified forKey:@"date_modified"];

            NSArray *contents = d[@"content"];
            NSData *data = [NSJSONSerialization dataWithJSONObject:contents options:0 error:nil];
            [mo setValue:data forKey:@"content"];
            
            NSNumber *number = [NSNumber numberWithInteger:count];
            [mo setValue:[number stringValue] forKey:@"index"];
            
            
            //ADD PERIOD
            [set addObject:mo];
            ++count; //increment
        }
        [object setValue:set forKey:@"periods"];
    }
}


- (void)processContents:(NSArray *)periods managedObject:(NSManagedObject *)object {
    
    if (periods.count > 0) {
        NSMutableSet *set = [NSMutableSet set];
        for (NSDictionary *d in periods) {
            
            /*
             "content_id": 1,
             "domain": "A. Pagkilala sa Sarili",
             "date_created": "2015-09-17 02:16:14",
             "date_modified": "2015-09-17 02:16:14",
             "content_values": []
             */
            
            NSString *content_id = [self stringValue:d[@"content_id"]];
            NSString *domain = [self stringValue:d[@"domain"]];
            NSString *date_created = [self stringValue:d[@"date_created"]];
            NSString *date_modified = [self stringValue:d[@"date_modified"]];
            NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kContentTableEntity
                                                                inManagedObjectContext:object.managedObjectContext];
            [mo setValue:content_id forKey:@"content_id"];
            [mo setValue:domain forKey:@"domain"];
            [mo setValue:date_created forKey:@"date_created"];
            [mo setValue:date_modified forKey:@"date_modified"];
            
            if ( [self isArrayObject:d[@"content_values"]] ) {
                NSArray *content_values = [NSArray arrayWithArray:d[@"content_values"]];
                [self processContentValues:content_values managedObject:mo];
            }
            
            //ADD CONTENTS
            [set addObject:mo];
        }
        [object setValue:set forKey:@"contents"];
    }
}

- (void)processContentValues:(NSArray *)contentValues managedObject:(NSManagedObject *)object {
    
    if (contentValues.count > 0) {
        NSMutableSet *set = [NSMutableSet set];
        for (NSDictionary *d in contentValues) {
            
            /*
             "content_column_index": 1,
             "content_column_value_id": 1,
             "content_column_value": "Naipamamalas ang pag-unawa sa kahalagahan ng pagkilala sa sarili bilang Pilipino gamit ang konsepto ng pagpapatuloy at pagbabago",
             "content_column_name": "Content Standards",
             "date_created": "2015-09-17 02:16:14",
             "date_modified": "2015-09-17 02:16:14"
             */
            
            NSString *content_column_index = [self stringValue:d[@"content_column_index"]];
            NSString *content_column_value_id = [self stringValue:d[@"content_column_value_id"]];
            NSString *content_column_value = [self stringValue:d[@"content_column_value"]];
            NSString *content_column_name = [self stringValue:d[@"content_column_name"]];
            NSString *date_created = [self stringValue:d[@"date_created"]];
            NSString *date_modified = [self stringValue:d[@"date_modified"]];
            
            NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kContentValuesTableEntity
                                                                inManagedObjectContext:object.managedObjectContext];
            
            [mo setValue:content_column_index forKey:@"content_column_index"];
            [mo setValue:content_column_value_id forKey:@"content_column_value_id"];
            [mo setValue:content_column_value forKey:@"content_column_value"];
            [mo setValue:content_column_name forKey:@"content_column_name"];
            [mo setValue:date_created forKey:@"date_created"];
            [mo setValue:date_modified forKey:@"date_modified"];
            
            //ADD CONTENT VALUES
            [set addObject:mo];
        }
        [object setValue:set forKey:@"content_values"];
    }
}

#pragma mark - Curriculum Planner v2.0

- (NSString *)loginUser {
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *userid = [NSString stringWithFormat:@"%d", account.user.id];
    return userid;
}

- (NSString *)generateUniqueFileName:(NSString *)name {
    return [NSString stringWithFormat:@"%@_%@", name, [[NSUUID UUID] UUIDString]];
}

- (void)requestCourseCategoryListForUser:(NSString *)userid doneBlock:(CurriculumDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCourseCategoryList, userid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            NSManagedObjectContext *ctx = self.workerContext;
            
            [ctx performBlock:^{
                [self clearDataForEntity:kCourseCategoryEntity withPredicate:nil context:ctx];
            }];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"course category records: %@", records);
                
                if ([self isArrayObject:records] && records.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d in records) {
                            // Parse data
                            NSString *objid = [self stringValue:d[@"id"]];
                            NSString *code = [self stringValue:d[@"code"]];
                            NSString *name = [self stringValue:d[@"name"]];
                            NSString *description = [self stringValue:d[@"description"]];
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:kCourseCategoryEntity attribute:@"id" parameter:objid context:ctx];
                            
                            // Save to core data
                            [mo setValue:userid forKey:@"userid"];
                            [mo setValue:objid forKey:@"id"];
                            [mo setValue:code forKey:@"code"];
                            [mo setValue:name forKey:@"name"];
                            [mo setValue:description forKey:@"desc"];
                            
                            [self saveTreeContext:ctx];
                            
                            if (doneBlock) {
                                doneBlock(YES);
                            }
                        }
                    }];
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestCurriculumListForCourseCategory:(NSString *)course_category_id andUser:(NSString *)user_id doneBlock:(CurriculumDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCurriculumList, course_category_id, user_id]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            NSManagedObjectContext *ctx = self.workerContext;
            
            [ctx performBlock:^{
                [self clearDataForEntity:kCurriculumListEntity withPredicate:nil context:ctx];
            }];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"curriculum records: %@", records);
                
                if ([self isArrayObject:records] && records.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d in records) {
                            // Parse data
                            NSString *curriculum_id = [self stringValue:d[@"curriculum_id"]];
                            NSString *curriculum_territory_id = [self stringValue:d[@"curriculum_territory_id"]];
                            NSString *curriculum_territory_name = [self stringValue:d[@"curriculum_territory_name"]];
                            NSString *curriculum_title = [self stringValue:d[@"curriculum_title"]];
                            NSString *curriculum_description = [self stringValue:d[@"curriculum_description"]];
                            NSString *curriculum_status = [self stringValue:d[@"curriculum_status"]];
                            NSString *curriculum_status_remarks = [self stringValue:d[@"curriculum_status_remarks"]];
                            NSString *is_template = [self stringValue:d[@"is_template"]];
                            NSString *reference_id = [self stringValue:d[@"reference_id"]];
                            NSString *effectivity = [self stringValue:d[@"effectivity"]];
                            NSString *course_id = [self stringValue:d[@"course_id"]];
                            NSString *course_name = [self stringValue:d[@"course_name"]];
                            NSString *grade_level_id = [self stringValue:d[@"grade_level_id"]];
                            NSString *grade_level_name = [self stringValue:d[@"grade_level_name"]];
                            NSString *school_profile_id = [self stringValue:d[@"school_profile_id"]];
                            NSString *school_profile_name = [self stringValue:d[@"school_profile_name"]];
                            NSString *sy_id = [self stringValue:d[@"sy_id"]];
                            NSString *sy_name = [self stringValue:d[@"sy_name"]];
                            NSString *created_by = [self stringValue:d[@"created_by"]];
                            NSString *avatar = [self stringValue:d[@"avatar"]];
                            NSString *created_by_id = [self stringValue:d[@"created_by_id"]];
                            NSString *date_created = [self stringValue:d[@"date_created"]];
                            NSString *date_modified = [self stringValue:d[@"date_modified"]];
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:kCurriculumListEntity attribute:@"curriculum_id" parameter:curriculum_id context:ctx];
                            
                            // Save to core data
                            [mo setValue:user_id forKey:@"user_id"];
                            [mo setValue:course_category_id forKey:@"course_category_id"];
                            [mo setValue:curriculum_id forKey:@"curriculum_id"];
                            [mo setValue:curriculum_territory_id forKey:@"curriculum_territory_id"];
                            [mo setValue:curriculum_territory_name forKey:@"curriculum_territory_name"];
                            [mo setValue:curriculum_title forKey:@"curriculum_title"];
                            [mo setValue:curriculum_description forKey:@"curriculum_description"];
                            [mo setValue:curriculum_status forKey:@"curriculum_status"];
                            [mo setValue:curriculum_status_remarks forKey:@"curriculum_status_remarks"];
                            [mo setValue:is_template forKey:@"is_template"];
                            [mo setValue:reference_id forKey:@"reference_id"];
                            [mo setValue:effectivity forKey:@"effectivity"];
                            [mo setValue:course_id forKey:@"course_id"];
                            [mo setValue:course_name forKey:@"course_name"];
                            [mo setValue:grade_level_id forKey:@"grade_level_id"];
                            [mo setValue:grade_level_name forKey:@"grade_level_name"];
                            [mo setValue:school_profile_id forKey:@"school_profile_id"];
                            [mo setValue:school_profile_name forKey:@"school_profile_name"];
                            [mo setValue:sy_id forKey:@"sy_id"];
                            [mo setValue:sy_name forKey:@"sy_name"];
                            [mo setValue:created_by forKey:@"created_by"];
                            [mo setValue:avatar forKey:@"avatar"];
                            [mo setValue:created_by_id forKey:@"created_by_id"];
                            [mo setValue:date_created forKey:@"date_created"];
                            [mo setValue:date_modified forKey:@"date_modified"];
                            
                            [self saveTreeContext:ctx];
                        }
                        
                        if (doneBlock) {
                            doneBlock(YES);
                        }
                    }];
                }
                else {
                    if (doneBlock) {
                        doneBlock(NO);
                    }
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestDownloadCurriculumWithID:(NSString *)curriculumid contentBlock:(CurriculumArrayContent)contentBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCurriculumPDF, curriculumid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDownloadTask *downloadTask = [self.session downloadTaskWithRequest:request completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {

        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (contentBlock) {
                contentBlock(nil);
            }
        }
        
        if (!error) {
            NSLog(@"response: %@", response);
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            long int statusCode = (long)[httpResponse statusCode];
            
            if (statusCode == 200) {
                if (location) {
                    NSNumber *fileSize = [NSNumber numberWithUnsignedLongLong:[response expectedContentLength]];
                    NSString *fileTemporaryPath = [NSString stringWithFormat:@"%@", location];
                    NSString *filename = [self stringValue:[response suggestedFilename]];
                    
                    NSLog(@"suggested filename: %@", filename);
                    
                    NSArray *content = @[fileSize, fileTemporaryPath, filename];
                    
                    if (contentBlock) {
                        contentBlock(content);
                    }
                }
                else {
                    if (contentBlock) {
                        contentBlock(nil);
                    }
                }
            }
            else {
                if (contentBlock) {
                    contentBlock(nil);
                }
            }
        }
    }];
    
    [downloadTask resume];
}

#pragma mark - Parsing Helpers

- (BOOL)isItOkayToParseUsingThisResponseData:(NSDictionary *)response {
    NSLog(@"response: %@", response);
    
    if (response != nil) {
        NSDictionary *parsedmeta = response[@"_meta"];
        
        if (parsedmeta != nil) {
            if ([self doesKeyExistInDictionary:parsedmeta key:@"status"]) {
                NSString *status = [self stringValue:parsedmeta[@"status"]];
                
                if ([status isEqualToString:@"SUCCESS"]) {
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

- (BOOL)doesKeyExistInDictionary:(NSDictionary *)d key:(NSString *)key {
    NSArray *keys = [d allKeys];
    
    for (NSString *k in keys) {
        if ([k isEqualToString:key]) {
            return YES;
        }
    }
    
    return NO;
}

@end