//
//  SocialStreamCommentCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 1/29/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#define MAS_SHORTHAND
#import "Masonry.h"

#import "SocialStreamCommentCell.h"

@interface SocialStreamCommentCell()

@property (strong, nonatomic) IBOutlet UIView *frameBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIView *footerView;

@property (strong, nonatomic) UILabel *labelImage;

@end

@implementation SocialStreamCommentCell

- (void)awakeFromNib {
    // Initialization code
    NSLog(@"awake on nib custom cell");
    
    self.labelImage = [UILabel new];
    self.labelImage.textColor = [UIColor whiteColor];
    self.labelImage.font = [UIFont systemFontOfSize:25];
    self.labelImage.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.labelImage];
    [self.labelImage makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.avatarImage);
    }];
    
    self.avatarImage.layer.cornerRadius = self.avatarImage.frame.size.width / 2;
    self.avatarImage.clipsToBounds = YES;
    self.avatarImage.layer.borderWidth = 1.5f;
    self.avatarImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)showAlias {
    
    self.labelImage.text = [NSString stringWithFormat:@"%@%@",[self.firstName substringToIndex:1],[self.lastName substringToIndex:1]];
    UIImage *img = nil;
    UIGraphicsBeginImageContext(self.labelImage.bounds.size);
    [self.labelImage.layer renderInContext:UIGraphicsGetCurrentContext()];
    img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.avatarImage.image = img;
    
    [self setNeedsDisplay];
}

- (void)isOwned:(BOOL)status {
    
//    UIColor *color = [UIColor colorWithRed:255.0/255.0 green:197.0/255.0 blue:244.0/255.0 alpha:1.0];
    UIColor *color = [UIColor colorWithRed:195.0/255.0 green:234.0/255.0 blue:244.0/255.0 alpha:1.0];
    if (status) {
//        color = [UIColor colorWithRed:195.0/255.0 green:234.0/255.0 blue:244.0/255.0 alpha:1.0];
        color = [UIColor colorWithRed:255.0/255.0 green:197.0/255.0 blue:244.0/255.0 alpha:1.0];
    }
    
    
    self.userNameLabel.backgroundColor = color;
    self.dateLabel.backgroundColor = color;
    self.commentLabel.backgroundColor = color;
    self.likeCountLabel.backgroundColor = color;
    self.likeButton.backgroundColor = color;
    self.frameBackgroundView.backgroundColor = color;
    self.headerView.backgroundColor = color;
    self.footerView.backgroundColor = color;
}

@end
