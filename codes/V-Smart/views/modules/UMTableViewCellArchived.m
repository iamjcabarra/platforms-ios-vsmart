//
//  UMTableViewCell.m
//  SWTableViewCell
//
//  Created by Matt Bowman on 12/2/13.
//  Copyright (c) 2013 Chris Wendel. All rights reserved.
//

#import "UMTableViewCellArchived.h"

@implementation UMTableViewCellArchived
@synthesize labelTitle, labelImage, labelModifiedDate, labelTags;
@end
