//
//  LessonTemplateStageView.m
//  V-Smart
//
//  Created by Julius Abarra on 08/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "LessonTemplateStageView.h"
#import "LessonTemplateStageProcessCell.h"
#import "LessonPlanDataManager.h"
#import "LessonPlanConstants.h"

@interface LessonTemplateStageView () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UITextViewDelegate>

@property (strong, nonatomic) LessonPlanDataManager *lpdm;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) IBOutlet UITableView *processTableView;

@end

@implementation LessonTemplateStageView

static NSString *kProcessCellIdentifier = @"processCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Data Manager and Context
    self.lpdm = [LessonPlanDataManager sharedInstance];
    self.managedObjectContext = self.lpdm.mainContext;
    
    // Set Up Table View
    [self setUpProcessTableView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reloadFetchedResultsController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Process Table View

- (void)setUpProcessTableView {
    // Set Protocols
    self.processTableView.dataSource = self;
    self.processTableView.delegate = self;
    
    // Allow Selection
    self.processTableView.allowsSelection = NO;
    self.processTableView.allowsMultipleSelection = NO;
    
    // Default Height
    self.processTableView.rowHeight = 150.0f;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.processTableView = tableView;
    
    LessonTemplateStageProcessCell *cell = [tableView dequeueReusableCellWithIdentifier:kProcessCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    LessonTemplateStageProcessCell *processCell = (LessonTemplateStageProcessCell *)cell;
    [self configureQuestionCell:processCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(LessonTemplateStageProcessCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    NSString *learning_process_name = [self.lpdm stringValue:[mo valueForKey:@"learning_process_name"]];
    cell.processNameLabel.text = [learning_process_name uppercaseString];
    
    NSString *content = [self.lpdm stringValue:[mo valueForKey:@"content"]];
    cell.processContentTextView.text = content;
    cell.processContentTextView.tag = [[self.lpdm stringValue:[mo valueForKey:@"lc_id"]] integerValue];
    cell.processContentTextView.delegate = self;
    
    NSString *is_file = [self.lpdm stringValue:[mo valueForKey:@"is_file"]];
    BOOL hide = [is_file isEqualToString:@"1"];
    [cell shouldHideProcessContentTextView:hide];
    [cell shouldHideUploadFileButton:!hide];
    [cell shouldHideClearFileButton:hide];
    
    if (hide) {
        NSString *file_name = [self.lpdm stringValue:[mo valueForKey:@"orig_filename"]];
        
        if (![file_name isEqualToString:@""]) {
            [cell updateUploadButtonTitle:file_name];
        }
    }
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCopyLessonContentProcessEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"p_content.stage_id == %@", self.selectedContentStage];
    [fetchRequest setPredicate:predicate];
    
    NSSortDescriptor *lps_id = [NSSortDescriptor sortDescriptorWithKey:@"lps_id" ascending:YES];
    [fetchRequest setSortDescriptors:@[lps_id]];
    
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.processTableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.processTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.processTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.processTableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.processTableView endUpdates];
}

#pragma mark - Reload Fetched Results

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.processTableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error: %@", [err localizedDescription]);
    }
}

#pragma mark - Text View Delegate

- (void)textViewDidChange:(UITextView *)textView {
    NSString *lc_id = [NSString stringWithFormat:@"%zd", textView.tag];
    NSDictionary *userEntries = @{@"content":textView.text};
    NSPredicate *predicate = [self.lpdm predicateForKeyPath:@"lc_id" andValue:lc_id];
    [self.lpdm updateLessonPlanLocally:kCopyLessonContentProcessEntity predicate:predicate details:userEntries];
    [self postUpdateProgress];
}

#pragma mark - Post Notification

- (void)postUpdateProgress {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateProgress"
                                                        object:nil
                                                      userInfo:nil];
}

@end
