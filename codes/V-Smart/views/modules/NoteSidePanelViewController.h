//
//  NoteSidePanelViewController.h
//  V-Smart
//
//  Created by VhaL on 2/28/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "NoteBookViewController.h"
#import "TagViewController.h"
#import "NoteColorViewController.h"


@interface NoteSidePanelViewController : UIViewController
@property (nonatomic, strong) NoteBookViewController *noteBookViewController;
@property (nonatomic, strong) TagViewController *tagViewController;
@property (nonatomic, strong) NoteColorViewController *colorPopOverViewController;
@property (nonatomic, strong) IBOutlet UIView *containerView;
@property (nonatomic, strong) IBOutlet UIButton *buttonNoteBook;
@property (nonatomic, strong) IBOutlet UIButton *buttonTag;
@property (nonatomic, strong) IBOutlet UIButton *buttonColor;
@property (nonatomic, assign) id delegate;
-(void)adjustFrame;
@end
