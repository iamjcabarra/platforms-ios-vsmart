//
//  GBTLearningComponentPanel.swift
//  V-Smart
//
//  Created by Ryan Migallos on 15/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTLearningComponentPanel: UIViewController, UIPopoverPresentationControllerDelegate, GBMDropDownDelegate {

    fileprivate let cellIdentifier = "gbt_learning_panel_cell"
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet weak var componentLabel: UILabel!
    @IBOutlet weak var addTestButton: UIButton!
    
    var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var blockOperations: [BlockOperation] = []
    
    var scrollDelegate : ScrollCommunicationDelegate?
    
    var resultType:GBMDropDown = {
        let menu = GBMDropDown(menu: .result)
        menu.modalPresentationStyle = .popover
        return menu
    }()
    
    var isAscending = true
    var component_id = ""
    var componentData: [String : String?] = [:]
//    var component_name = ""
//    var componentDict: [String:AnyObject] = [:]
    
    var term_id = ""
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    fileprivate lazy var items:[String] = {
        let count = 500
        var list = [String]()
        for index in 0..<count {
            let text = "\(index)"
            list.append(text)
        }
        return list
    }()
    
    fileprivate let notification = NotificationCenter.default
    
    fileprivate lazy var scoreMenu:GBTScoreMenu = {
        let sb = UIStoryboard(name: "GBTChangeScore", bundle: nil)
        let menu = sb.instantiateInitialViewController() as! GBTScoreMenu
        menu.modalPresentationStyle = UIModalPresentationStyle.popover
        return menu
    }()
    
    fileprivate lazy var editScoreView:GBTChangeItemScore = {
        let sb = UIStoryboard(name: "GBTChangeScore", bundle: nil)
        let menu = sb.instantiateViewController(withIdentifier: "GBT_EDITSCORE_SB") as! GBTChangeItemScore
        menu.modalPresentationStyle = UIModalPresentationStyle.popover
        return menu
    }()
    
    fileprivate lazy var createNewTest:GBTCreateTestView = {
        let sb = UIStoryboard(name: "GBTChangeScore", bundle: nil)
        let menu = sb.instantiateViewController(withIdentifier: "GBT_CREATETEST_SB") as! GBTCreateTestView
        menu.modalPresentationStyle = UIModalPresentationStyle.popover
        return menu
    }()
    
    fileprivate lazy var resultTypeView:GBTResultTypeView = {
        let sb = UIStoryboard(name: "GBTChangeScore", bundle: nil)
        let menu = sb.instantiateViewController(withIdentifier: "GBT_RESULTTYPE_SB") as! GBTResultTypeView
        menu.modalPresentationStyle = UIModalPresentationStyle.popover
        return menu
    }()
    
    var student_name = ""
    var sort_by_gender = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.componentLabel.text = componentData["name"]!
        
//        print("VIEW DID LOAD \(component_name)")
        // Do any additional setup after loading the view.
        
        let is_teaching = self.gbdm.stringValue(self.gbdm.fetchUserDefaultsObject(forKey: GBTConstants.UserDefined.GBTCOURSEISTEACHING))
        var hidden = false
        if is_teaching == "1" {
            let for_testguru: String! = self.gbdm.stringValue(componentData["for_testguru"]!) // as! String
            if  for_testguru == "1" {
                hidden = true
            }
        } else {
            hidden = true
        }
        
        self.addTestButton.isHidden = hidden
        let buttonAction = #selector(GBTLearningComponentPanel.insertColumnAction(_:))
        self.addTestButton.addTarget(self, action: buttonAction, for: .touchUpInside)
    }
    
    func insertColumnAction(_ button:UIButton) {
        
        self.createNewTest.term_id = self.term_id
        self.createNewTest.component_id = self.component_id
        let popover: UIPopoverPresentationController = self.createNewTest.popoverPresentationController!
        popover.sourceRect = button.bounds
        popover.sourceView = button
        popover.delegate = self
        popover.permittedArrowDirections = UIPopoverArrowDirection.up
        present(self.createNewTest, animated: true, completion:nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.setUpNotification()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.teardownNotification()
    }
    
    func didFinishSelecting(_ type:GBMDropDownMenuType, data:[String:AnyObject]) {
        
        let quiz_id = data["quiz_id"] as! String
        let resultType_id = data["id"] as! String
        let cs_id = gbdm.fetchUserDefaultsObject(forKey: GBTConstants.UserDefined.GBTCSID) as! String
        
        gbdm.requestGradeBookContent(cs_id, quizId: quiz_id, resultTypeid: resultType_id, search: "", handler: { (done, error) in
            if error == nil {
                print("done selecting result type")
//                dispatch_async(dispatch_get_main_queue(), {
//                    self.setUpSegmentedViews(forCSID: cs_id, persistTermIndex: persisTermIndex, persistComponentIndex: persistComponentIndex)
//                })
            } else {
                self.displayAlert(withTitle: "", withMessage: error!)
            }
        })
        //            if flag {
//                print("done selecting result type")
//            }
//        })
    }
    
    func displayAlert(withTitle title: String, withMessage message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func showFilter(_ view:UIView, mo:NSManagedObject) {

        let quiz_id = mo.value(forKey: "quiz_id") as! String
        self.resultType.userData = ["quiz_id":quiz_id as AnyObject]
        
        // Delegate for GBMDropDownMenu
        self.resultType.delagate = self
        
        // Popover setup
        let popover = self.resultType.popoverPresentationController! as UIPopoverPresentationController
        popover.permittedArrowDirections = .unknown
        popover.delegate = self
        
        // Popover from an arbitrary anchor point
        popover.sourceRect = view.bounds
        popover.sourceView = view
        popover.delegate = self
        
        // Present Menu
        present(self.resultType, animated: true, completion:nil)
    }
}

extension GBTLearningComponentPanel: UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, NSFetchedResultsControllerDelegate {
    
    // MARK : - STUDENT SEARCH IMPLEMENTATION
    
//    func setUpNotification() {
//        notification.addObserver(self, selector: #selector(self.studentSearchAction(_:)), name: "GBT_STUDENT_SEARCH", object: nil)
//    }
//    
//    func teardownNotification() {
//        notification.removeObserver(self, name: "GBT_STUDENT_SEARCH", object: nil)
//    }
//    func didFinishSelecting(data: [String : AnyObject]?) {
////        let dataDict: [String:AnyObject] = ["quiz_id":quiz_id,
////                                            "component_id":self.component_id,
////                                            "term_id":self.term_id,
////                                            "index":index]
//        
////        let quiz_id = self.gbdm.stringValue(data!["quiz_id"])
////        let component_id = self.gbdm.stringValue(data!["component_id"])
////        let term_id = self.gbdm.stringValue(data!["term_id"])
////        let index = self.gbdm.stringValue(data!["index"])
//        
//        self.performSegueWithIdentifier(GBTConstants.SegueName.GBTBATCHEDIT, sender: nil)
//    }
    
    
    func studentSearchAction(_ notification: Notification) {
        var name = notification.object as? String
        name = (name == nil) ? "" : name
        self.student_name = name!
        self.reloadFetchedResultsController()
    }
    
    /// MARK : - COLLECTION VIEW DATASOURCE AND DELEGATE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! GBTLearningComponentCollectionCell
        
        let mo = self.fetchedResultsController.object(at: indexPath)
        var text = mo.value(forKey: "actual_result") as! String
        cell.backgroundColor = UIColor.white
        cell.customLabel.textColor = UIColor.black
        cell.customLabel.font = UIFont(name: "Helvetica", size: 17)
        if (indexPath as NSIndexPath).section == 0 {
            cell.backgroundColor = UIColor(hex6: 0xecf0f1)
            cell.customLabel.font = UIFont(name: "Helvetica-Bold", size: 17)
        } else if (indexPath as NSIndexPath).section == 1 {
            cell.backgroundColor = UIColor(hex6: 0xecf0f1)
            cell.customLabel.font = UIFont(name: "Helvetica-Bold", size: 17)
            cell.customLabel.textColor = UIColor.darkGray
            text = self.gbdm.formatStringNumber(text)
        } else {
            
            let for_testguru = gbdm.stringValue(mo.value(forKey: "for_testguru"))
            if for_testguru == "1" {
                cell.customLabel.textColor = UIColor.blue
            }
            
            text = self.gbdm.formatStringNumber(text)
        }
        
        cell.customLabel.text = text
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mo = self.fetchedResultsController.object(at: indexPath) 
        
        let for_testguru: String = self.gbdm.stringValue(mo.value(forKey: "for_testguru"))
        let quiz_id = self.gbdm.stringValue(mo.value(forKey: "quiz_id"))
        let paricipant_id = self.gbdm.stringValue(mo.value(forKey: "participant_id"))
        let index = mo.value(forKey: "index") as! Double
        let is_teaching: String! = self.gbdm.stringValue(self.gbdm.fetchUserDefaultsObject(forKey: GBTConstants.UserDefined.GBTCOURSEISTEACHING))
        
        let name : String = self.gbdm.stringValue(mo.value(forKey: "name"))
        print("NAME : \(name)")
        
        let totalindex = collectionView.numberOfItems(inSection: (indexPath as NSIndexPath).section) - 3
//        if is_teaching == "1" {
            if ((indexPath as NSIndexPath).section == 0) && ((indexPath as NSIndexPath).row < totalindex) {
                let actual_result = self.gbdm.stringValue(mo.value(forKey: "actual_result"))
                
                var showResultTypes = false
                if for_testguru == "1" && is_teaching == "1" {
                    showResultTypes = true
                }
                
                var showEditButton = false
                if is_teaching == "1" && for_testguru == "0"  {
                    showEditButton = true
                }
                
                
                let dataDict: [String:AnyObject] = ["quiz_id":quiz_id as AnyObject,
                                                    "component_id":self.component_id as AnyObject,
                                                    "term_id":self.term_id as AnyObject,
                                                    "index":index as AnyObject,
                                                    "test_title":actual_result as AnyObject]
                
                self.resultTypeView.dataDict = dataDict
                self.resultTypeView.showResultTypes = showResultTypes
                self.resultTypeView.showEditButton = showEditButton
                let popover: UIPopoverPresentationController = self.resultTypeView.popoverPresentationController!
                popover.sourceRect = (self.collectionView.cellForItem(at: indexPath)?.bounds)!
                popover.sourceView = self.collectionView.cellForItem(at: indexPath)
                popover.delegate = self
                popover.permittedArrowDirections = UIPopoverArrowDirection.up
                present(self.resultTypeView, animated: true, completion:nil)
                print("SHOW POPOVER!!!!!!!!")
                
                return
            }
            
            let is_grade = self.gbdm.stringValue(mo.value(forKey: "is_grade"))
            if is_grade == "0" {
                print("DONT SHOW POPOVER!!!!!!!!")
            } else {
                
                if for_testguru == "1" {
                    
                    //testing purposes
                    self.scoreMenu.quizID = quiz_id
                    self.scoreMenu.studentID = paricipant_id
                    
                    let popover: UIPopoverPresentationController = self.scoreMenu.popoverPresentationController!
                    popover.sourceRect = (self.collectionView.cellForItem(at: indexPath)?.bounds)!
                    popover.sourceView = self.collectionView.cellForItem(at: indexPath)
                    popover.delegate = self
                    present(self.scoreMenu, animated: true, completion:nil)
                } else {
                    let actual_result = self.gbdm.stringValue(mo.value(forKey: "actual_result"))
                    
    //                if self.editScoreView.delegate == nil {
    //                    self.editScoreView.delegate = self
    //                }
                    self.editScoreView.changeScoreData = ["qid": quiz_id as AnyObject,
                                                          "participant_id": paricipant_id as AnyObject,
                                                          "score":actual_result as AnyObject]
                    self.editScoreView.component_id = self.component_id
                    self.editScoreView.term_id = self.term_id
                    self.editScoreView.index = index
                    let popover: UIPopoverPresentationController = self.editScoreView.popoverPresentationController!
                    popover.sourceRect = (self.collectionView.cellForItem(at: indexPath)?.bounds)!
                    popover.sourceView = self.collectionView.cellForItem(at: indexPath)
                    popover.delegate = self
                    present(self.editScoreView, animated: true, completion:nil)
                }
            }
//        }
    }
    
    @objc(adaptivePresentationStyleForPresentationController:) func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    // MARK: - Fetched Results Controller
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
       
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = gbdm.getMainContext()
        let ascending = self.isAscending
        let entity = GBTConstants.Entity.STUDENTGRADES
        
         let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        
//        let fetchRequest = NSFetchRequest(entityName: entity)
        
        fetchRequest.fetchBatchSize = 20
        
        
//        let term_id = "1"//HARDCODING
//        let component_id = "32"//HARDCODING
//        let predicate1 = NSComparisonPredicate(keyPath: "term_id", withValue: self.term_id, isExact: true)
//        let predicate2 = NSComparisonPredicate(keyPath: "component_id", withValue: self.component_id, isExact: true)
////        let predicate3 = NSComparisonPredicate(keyPath: "name", withValue: self.student_name, isExact: false)
//        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2] )
//        fetchRequest.predicate = predicate
        
        
        var finalPredicate: NSPredicate
        
        let predicateTID = NSComparisonPredicate(keyPath: "term_id", withValue: self.term_id, isExact: true)
        let predicateCID = NSComparisonPredicate(keyPath: "component_id", withValue: self.component_id, isExact: true)
        
        if self.student_name.characters.count > 0 {
            let predicateName = NSComparisonPredicate(keyPath: "name", withValue: self.student_name, isExact: false)
            let predicateHeader = NSComparisonPredicate(keyPath: "name", withValue: "00_AA_AA_AA_", isExact: false)
            
            let idPredicates = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateTID, predicateCID])
            let namePredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicateName, predicateHeader])
            finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [idPredicates, namePredicate])
        } else {
            finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateTID, predicateCID])
        }
        
        fetchRequest.predicate = finalPredicate
        
        //TODO FILTER SORT SEGMENTED CONTROL
        var section_key_path = "name"
        
        let sortIndex = NSSortDescriptor(key: "index", ascending: ascending)
        
        let descriptorSelector = #selector(NSString.localizedCompare(_:))
        if self.sort_by_gender {
            let sortGender = NSSortDescriptor(key: "gender", ascending: ascending, selector:descriptorSelector)
            fetchRequest.sortDescriptors = [sortGender, sortIndex]
            section_key_path = "gender"
        } else {
            let sortName = NSSortDescriptor(key: "name", ascending: ascending, selector:descriptorSelector)
            fetchRequest.sortDescriptors = [sortName, sortIndex]
            section_key_path = "name"
        }
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                   managedObjectContext: ctx!,
                                                                   sectionNameKeyPath: section_key_path,
                                                                   cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch _ as NSError {
            //            error = error1
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //println("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    fileprivate func addProcessingBlock(_ processingBlock:@escaping (Void)->Void) {
        blockOperations.append(BlockOperation(block: processingBlock))
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        blockOperations.removeAll(keepingCapacity: false)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
            
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.insertItems(at: [newIndexPath]) }
            
        case .update:
            guard let newIndexPath = newIndexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.reloadItems(at: [newIndexPath]) }
            
        case .move:
            guard let indexPath = indexPath else { return }
            guard let newIndexPath = newIndexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.moveItem(at: indexPath, to: newIndexPath) }
            
        case .delete:
            guard let indexPath = indexPath else { return }
            addProcessingBlock { [weak self] in self?.collectionView.deleteItems(at: [indexPath]) }
            
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
            
        case .insert:
            addProcessingBlock { [weak self] in self?.collectionView.insertSections(IndexSet(integer: sectionIndex)) }
            
        case .update:
            addProcessingBlock { [weak self] in self?.collectionView.reloadSections(IndexSet(integer: sectionIndex)) }
            
        case .delete:
            addProcessingBlock { [weak self] in self?.collectionView.deleteSections(IndexSet(integer: sectionIndex)) }
            
        default: break
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView.performBatchUpdates({
            self.blockOperations.forEach { $0.start() }
            }, completion: { finished in
                self.blockOperations.removeAll(keepingCapacity: false)
        })
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.collectionView!.reloadData()
        
        do {
            try self.fetchedResultsController.performFetch()
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y;
        self.scrollDelegate?.gbScrollViewDidScroll(GBPanelType.LearningPanel, scrollTo: offsetY)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewWillBeginDragging(GBPanelType.LearningPanel)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.LearningPanel)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate == false {
            self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.LearningPanel)
        }
    }
}
