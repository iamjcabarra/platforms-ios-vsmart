//
//  GBTScrollingLearningComponent.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 18/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTScrollingLearningComponent: UIViewController, UIPageViewControllerDelegate {

    var scrollDelegate : ScrollCommunicationDelegate?
    var pageViewController: UIPageViewController?
    
    var displayedViewController: GBTLearningComponentPanel?
    
    var term_id = "1"
    
    fileprivate let notification = NotificationCenter.default
    var student_name = ""
    var sort_by_gender = false
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // Configure the page view controller and add it as a child view controller.
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpNotification()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.teardownNotification()
    }

    func reloadPage(forTermID term_id: String, component_id: String?) {
        _modelController = nil
        self.term_id = term_id
        setUpPageViewController(component_id)
    }
    
    func setUpPageViewController(_ component_id: String?) {
    
        if self.pageViewController == nil {
            self.pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)

            self.pageViewController!.delegate = self
            
            self.addChildViewController(self.pageViewController!)
            self.view.addSubview(self.pageViewController!.view)
            
            // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
            var pageViewRect = self.view.bounds
            if UIDevice.current.userInterfaceIdiom == .pad {
                pageViewRect = pageViewRect.insetBy(dx: 0, dy: 0)
            }
            self.pageViewController!.view.frame = pageViewRect
            self.pageViewController!.didMove(toParentViewController: self)
        }

        let startingViewController = self.modelController.viewControllerAtIndex(0, orIndexOfComponentID: component_id,storyboard: self.storyboard!) as GBTLearningComponentPanel?
        if startingViewController != nil {
            startingViewController?.student_name = self.student_name // for STUDENT SEARCH
            startingViewController?.sort_by_gender = self.sort_by_gender // for STUDENT SORT
            self.pageViewController?.view.isHidden = false
            let viewControllers = [startingViewController!]
            self.displayedViewController = startingViewController // SET UP DEFAULT
            self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: false, completion: {done in })
            self.pageViewController!.dataSource = self.modelController
        } else {
            self.pageViewController?.view.isHidden = true
//            self.displayedViewController?.componentLabel.text = "No Component Assigned"
//            let viewControllers = [startingViewController!]
//            self.displayedViewController = startingViewController // SET UP DEFAULT
//            self.pageViewController!.setViewControllers(viewControllers, direction: .Forward, animated: false, completion: {done in })
//            self.pageViewController!.dataSource = self.modelController
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var modelController: ModelController {
        // Return the model controller object, creating it if necessary.
        // In more complex implementations, the model controller may be passed to the view controller.
        if _modelController == nil {
            _modelController = ModelController(term_id: self.term_id, scrollingDelegate: self.scrollDelegate!)
        }
        return _modelController!
    }
    
    var _modelController: ModelController? = nil
    
    // MARK: - UIPageViewController delegate methods
    
    func pageViewController(_ pageViewController: UIPageViewController, spineLocationFor orientation: UIInterfaceOrientation) -> UIPageViewControllerSpineLocation {
        if (orientation == .portrait) || (orientation == .portraitUpsideDown) || (UIDevice.current.userInterfaceIdiom == .phone) {
            // In portrait orientation or on iPhone: Set the spine position to "min" and the page view controller's view controllers array to contain just one view controller. Setting the spine position to 'UIPageViewControllerSpineLocationMid' in landscape orientation sets the doubleSided property to true, so set it to false here.
            let currentViewController = self.pageViewController!.viewControllers![0]
            let viewControllers = [currentViewController]
            self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: true, completion: {done in })
            
            self.pageViewController!.isDoubleSided = false
            return .min
        }
        
        // In landscape orientation: Set set the spine location to "mid" and the page view controller's view controllers array to contain two view controllers. If the current page is even, set it to contain the current and next view controllers; if it is odd, set the array to contain the previous and current view controllers.
        let currentViewController = self.pageViewController!.viewControllers![0] as! GBTLearningComponentPanel
        var viewControllers: [UIViewController]
        
        let indexOfCurrentViewController = self.modelController.indexOfViewController(currentViewController)
        if (indexOfCurrentViewController == 0) || (indexOfCurrentViewController % 2 == 0) {
            let nextViewController = self.modelController.pageViewController(self.pageViewController!, viewControllerAfter: currentViewController)
            viewControllers = [currentViewController, nextViewController!]
        } else {
            let previousViewController = self.modelController.pageViewController(self.pageViewController!, viewControllerBefore: currentViewController)
            viewControllers = [previousViewController!, currentViewController]
        }
        self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: true, completion: {done in })
        
        return .mid
    }

    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let displayed = pageViewController.viewControllers?.last as! GBTLearningComponentPanel
        self.displayedViewController = displayed
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        let vcs = pendingViewControllers as! [GBTLearningComponentPanel]
        for vc in vcs {
//            print("WILL TRANSITION TO \(vc.component_name)--")
            vc.student_name = self.student_name
            vc.reloadFetchedResultsController()
        }
    }
    
    func setUpNotification() {
        notification.addObserver(self, selector: #selector(self.studentSearchAction(_:)), name: NSNotification.Name(rawValue: "GBT_STUDENT_SEARCH"), object: nil)
        notification.addObserver(self, selector: #selector(self.studentSortAction(_:)), name: NSNotification.Name(rawValue: "GBT_STUDENT_SORT"), object: nil)
        notification.addObserver(self, selector: #selector(self.batchEditScoreAction(_:)), name: NSNotification.Name(rawValue: GBTConstants.Notification.BATCHEDITSCORE), object: nil)
    }
    
    func teardownNotification() {
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "GBT_STUDENT_SEARCH"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "GBT_STUDENT_SORT"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: GBTConstants.Notification.BATCHEDITSCORE), object: nil)
    }
    
    func batchEditScoreAction(_ notification: Notification) {
        let dataDict: [String:AnyObject] = notification.object as! [String:AnyObject]
        
        let index =  dataDict["index"]!
        let component_id =  dataDict["component_id"]!
        let term_id =  dataDict["term_id"]!
        
        let pIndex = NSComparisonPredicate(keyPath: "index", withValue: index, isExact: true)
        let pComponent = NSComparisonPredicate(keyPath: "component_id", withValue: component_id, isExact: true)
        let pTerm = NSComparisonPredicate(keyPath: "term_id", withValue: term_id, isExact: true)
        
        let namePred = NSComparisonPredicate(keyPath: "name", withValue: "AA_AA_AA_AA_00", isExact: true)
        let namePred1 = NSComparisonPredicate(keyPath: "name", withValue: "00_AA_AA_AA_01", isExact: true)
        let namePred2 = NSComparisonPredicate(keyPath: "name", withValue: "00_AA_AA_AA_02", isExact: true)
        
        let notNamePred = NSCompoundPredicate(notPredicateWithSubpredicate: namePred)
        let notNamePred1 = NSCompoundPredicate(notPredicateWithSubpredicate: namePred1)
        let notNamePred2 = NSCompoundPredicate(notPredicateWithSubpredicate: namePred2)
        
        let finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [pIndex,pComponent,pTerm,notNamePred,notNamePred1,notNamePred2])
        
        self.gbdm.fetchEditData(withPredicate: finalPredicate) { (listBlock) in
            if listBlock != nil {
                let sender: [String:AnyObject] = ["dataDict": dataDict as AnyObject,
                              "editData": listBlock! as AnyObject]
                self.performSegue(withIdentifier: GBTConstants.SegueName.GBTBATCHEDIT, sender: sender)
            }
        }
        
        
        
//        let dataDict: [String:AnyObject] = ["quiz_id":quiz_id,
//                                            "component_id":self.component_id,
//                                            "term_id":self.term_id,
//                                            "index":index]
//
//        let quiz_id = self.gbdm.stringValue(data!["quiz_id"])
//        let component_id = self.gbdm.stringValue(data!["component_id"])
//        let term_id = self.gbdm.stringValue(data!["term_id"])
//        let index = self.gbdm.stringValue(data!["index"])
    }
    
    func studentSortAction(_ notification: Notification) {
//        var sort_by_gender = notification.object as? Bool
//        sort_by_gender = (sort_by_gender == nil) ? false : true
//        self.sort_by_gender = sort_by_gender!
//        self.reloadFetchedResultsController()
        let sort_by_gender = notification.object as! Bool
        self.sort_by_gender = sort_by_gender
        self.displayedViewController?.sort_by_gender = self.sort_by_gender
        self.displayedViewController?.reloadFetchedResultsController()
//        self.reloadFetchedResultsController()
    }
    
    func studentSearchAction(_ notification: Notification) {
        var name = notification.object as? String
        name = (name == nil) ? "" : name
        self.student_name = name!
        self.displayedViewController?.student_name = self.student_name
        self.displayedViewController?.reloadFetchedResultsController()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == GBTConstants.SegueName.GBTBATCHEDIT {
            let bev = segue.destination as! GBTBatchScoreEditView
            let data = sender as? [String:AnyObject]
            bev.dataDict = data!["dataDict"] as? [String:AnyObject]
            bev.editData = data!["editData"] as? [[String : AnyObject]?]
//            self.leftPanel.scrollDelegate = self
        }
    }

}
