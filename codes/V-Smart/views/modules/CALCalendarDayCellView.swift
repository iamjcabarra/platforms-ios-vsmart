//
//  CALCalendarDayCellView.swift
//  V-Smart
//
//  Created by Julius Abarra on 14/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import JTAppleCalendar

class CALCalendarDayCellView: JTAppleDayCellView {

    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var selectedView: UIView!
    @IBOutlet var eventCounterImageView: UIImageView!
    @IBOutlet var eventCounterLabel: UILabel!
    
}
