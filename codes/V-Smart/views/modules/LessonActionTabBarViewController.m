//
//  LessonActionTabBarViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/22/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "LessonActionTabBarViewController.h"
#import "UBDContentOverviewViewController.h"

#define SegueIdentifierOverview @"embedOverviewTab"
#define SegueIdentifierStage1 @"embedStage1Tab"
#define SegueIdentifierStage2 @"embedStage2Tab"
#define SegueIdentifierStage3 @"embedStage3Tab"

@interface LessonActionTabBarViewController () <UIScrollViewDelegate>

@property (nonatomic, strong) NSString *currentSegueIdentifier;

@end

@implementation LessonActionTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set current segue identifier and perform initial segue using this identifier
    self.currentSegueIdentifier = SegueIdentifierOverview;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:SegueIdentifierOverview]) {
        if (self.childViewControllers.count > 0) {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
        }
        else {
            [self addChildViewController:segue.destinationViewController];
            ((UIViewController *)segue.destinationViewController).view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:((UIViewController *)segue.destinationViewController).view];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:SegueIdentifierStage1]) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }
    else if ([segue.identifier isEqualToString:SegueIdentifierStage2]) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }
    else if ([segue.identifier isEqualToString:SegueIdentifierStage3]) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController {
    
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
    }];
}

- (void)swapViewControllers:(NSString *)segueIdentifier {
    [self performSegueWithIdentifier:segueIdentifier sender:nil];
}

@end
