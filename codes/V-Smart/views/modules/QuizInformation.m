//
//  QuizInformation.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/6/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "QuizInformation.h"
#import "AppDelegate.h"


@interface QuizInformation ()

@property (strong, nonatomic) IBOutlet UILabel *quizTitle;
@property (strong, nonatomic) IBOutlet UILabel *quizDescription;
@property (strong, nonatomic) IBOutlet UILabel *questionCount;

@property (strong, nonatomic) IBOutlet UILabel *attemptDescription;
@property (strong, nonatomic) IBOutlet UILabel *timelimitDescription;
@property (strong, nonatomic) IBOutlet UILabel *passingrateDescription;

@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *startButton;

@property (strong, nonatomic) ResourceManager *rm;

@end

@implementation QuizInformation

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.startButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    __weak typeof(self) weakObject = self;
    
    self.rm = [AppDelegate resourceInstance];
    NSMutableDictionary *d = (NSMutableDictionary *)[self.rm fetchQuizDataForQuizID:self.quizid];
    
    NSString *attempts = [NSString stringWithFormat:@"%@", d[@"attempts"] ];
    NSString *attempts_count = [NSString stringWithFormat:@"%@", d[@"attempts_count"] ];
    
    NSInteger attempts_value = [attempts integerValue];
    NSInteger attempts_count_value = [attempts_count integerValue];
    
    NSInteger total_attempts = attempts_value - attempts_count_value;
    
    NSString *attempt_message = [NSString stringWithFormat:@"You can take this exam %ld times", (long)total_attempts ];
    if ( total_attempts == 1 ) {
        attempt_message = [NSString stringWithFormat:@"You can take this exam once"];
    }

    if (attempts_value == attempts_count_value) {
        attempt_message = [NSString stringWithFormat:@"You cannot take this exam anymore"];
        self.startButton.userInteractionEnabled = NO;
        self.startButton.alpha = 50;
    }
    
    NSString *time_limit = [NSString stringWithFormat:@"You have %@ to complete the exam", d[@"time_limit"] ];
    NSString *passing_rate = [NSString stringWithFormat:@"The passing score for this exam is %@%%", d[@"passing_rate"] ];
    NSString *total_items = [NSString stringWithFormat:@"%@ items", d[@"total_items"] ];
    dispatch_async(dispatch_get_main_queue(), ^{
        weakObject.questionCount.text = total_items;
        weakObject.attemptDescription.text = attempt_message;
        weakObject.timelimitDescription.text = time_limit;
        weakObject.passingrateDescription.text = passing_rate;
    });

//    [self.rm requestPreRunQuizDetailsForUser:self.userid quiz:self.quizid course:self.courseid doneBlock:^(BOOL status) {
//        NSMutableDictionary *d = (NSMutableDictionary *)[self.rm fetchQuizDataForQuizID:self.quizid];
//        
//        NSString *attempt = [NSString stringWithFormat:@"You can take this exam %@ more times", d[@"attempts"] ];
//        NSString *time_limit = [NSString stringWithFormat:@"You have %@ to complete the exam", d[@"time_limit"] ];
//        NSString *passing_rate = [NSString stringWithFormat:@"The passing score for this exam is %@%%", d[@"passing_rate"] ];
//        NSString *total_items = [NSString stringWithFormat:@"%@ items", d[@"total_items"] ];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            weakObject.questionCount.text = total_items;
//            weakObject.attemptDescription.text = attempt;
//            weakObject.timelimitDescription.text = time_limit;
//            weakObject.passingrateDescription.text = passing_rate;
//        });
//    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)buttonAction:(id)sender {
    
    UIButton *b = (UIButton *)sender;
    
    if (b == _startButton) {
        if ([_delegate respondsToSelector:@selector(buttonActionType:)]) {
            [_delegate buttonActionType:@"start"];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
    if (b == _cancelButton) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

@end
