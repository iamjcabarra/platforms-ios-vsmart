//
//  LessonDetailsHistoryTableViewCell.h
//  V-Smart
//
//  Created by Julius Abarra on 9/21/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonDetailsHistoryTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgAvatar;

@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblHistory;

@end
