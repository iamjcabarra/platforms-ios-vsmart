//
//  ActionPopOverViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 9/22/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ActionPopOverViewControllerDelegate <NSObject>

@required
- (void)selectedAction:(NSInteger)action;

@end

@interface ActionPopOverViewController : UIViewController

typedef NS_ENUM (NSInteger, LPCrudActionType) {
    LPCrudActionTypeEdit = 1,
    LPCrudActionTypeSubmit,
    LPCrudActionTypeDownload,
    LPCrudActionTypeDelete
};

@property (strong, nonatomic) IBOutlet UIButton *butEditAction;
@property (strong, nonatomic) IBOutlet UIButton *butSubmitAction;
@property (strong, nonatomic) IBOutlet UIButton *butDownloadAction;
@property (strong, nonatomic) IBOutlet UIButton *butDeleteAction;

@property (weak, nonatomic) id <ActionPopOverViewControllerDelegate> delegate;

@end
