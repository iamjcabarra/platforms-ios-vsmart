//
//  LessonDetailsTabBarViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 9/18/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonDetailsTabBarViewController : UIViewController

@property (nonatomic, strong) NSManagedObject *lesson_mo;

- (void)swapViewControllers:(NSString *)segueIdentifier;

@end
