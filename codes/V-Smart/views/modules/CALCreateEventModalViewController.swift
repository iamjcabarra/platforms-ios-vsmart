//
//  CALCreateEventModalViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 22/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class CALCreateEventModalViewController: UIViewController {
    
    @IBOutlet var headerView: UIView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var headerTitleLabel: UILabel!
    @IBOutlet var processButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    
    var isCreate = ""
    var eventObject: NSManagedObject!
    
    fileprivate let eventFormSegueIdentifier = "EMBEDDED_EVENT_FORM"
    fileprivate let notification = NotificationCenter.default
    
    // MARK: - Shared Data Manager
    
    fileprivate lazy var dataManager: CALDataManager = {
        let cmdm = CALDataManager.sharedInstance
        return cmdm
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure header view
        self.updateHeaderView()
        
        let titleA = NSLocalizedString("Save", comment: "").uppercased()
        let titleB = NSLocalizedString("Update", comment: "").uppercased()
        let titleC = self.isCreate == "1" ? titleA : titleB
        
        self.processButton.setTitle(titleC, for: .normal)
        self.processButton.setTitle(titleC, for: .selected)
        self.processButton.setTitle(titleC, for: .highlighted)
        
        let cancel = NSLocalizedString("Cancel", comment: "").uppercased()
        
        self.cancelButton.setTitle(cancel, for: .normal)
        self.cancelButton.setTitle(cancel, for: .selected)
        self.cancelButton.setTitle(cancel, for: .highlighted)
        
        // Set up button actions
        let toggleProcessButtonAction = #selector(self.toggleProcessButton(_:))
        self.processButton.addTarget(self, action: toggleProcessButtonAction, for: .touchUpInside)
        
        let toggleCancelButtonAction = #selector(self.toggleCancelButton(_:))
        self.cancelButton.addTarget(self, action: toggleCancelButtonAction, for: .touchUpInside)
        
        // Set up notification
        self.setUpNotifications()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Destroy notifications
        self.tearDownNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Notifications
    
    fileprivate func setUpNotifications() {
        let selector = #selector(self.respondToDeviceOrientationChange(_:))
        let name = NSNotification.Name(rawValue: "CAL_NOTIFICATION_EVENT_COLOR_CHANGE")
        self.notification.addObserver(self, selector: selector, name: name, object: nil)
    }
    
    fileprivate func tearDownNotifications() {
        let name = NSNotification.Name(rawValue: "CAL_NOTIFICATION_EVENT_COLOR_CHANGE")
        self.notification.removeObserver(self, name: name, object: nil)
    }
    
    func respondToDeviceOrientationChange(_ notification: NSNotification) {
        DispatchQueue.main.async(execute: {
            self.updateHeaderView()
        })
    }
    
    // MARK: - Button Event Handlers
    
    func toggleProcessButton(_ sender: UIButton) {
        let hasEmptyField = self.doesContainEmptyRequiredField()
        
        if hasEmptyField {
            DispatchQueue.main.async(execute: {
                let message = NSLocalizedString("Please fill in all required fields.", comment: "")
                self.view.makeToast(message: message, duration: 0.3, position: "center" as AnyObject)
            })
        }
        else {
            let id = self.dataManager.stringValue(self.eventObject.value(forKey: "id"))!
            let user_id = self.dataManager.stringValue(self.eventObject.value(forKey: "user_id"))!
            
            guard let body = self.dataManager.assemblePostBody(forEvent: id, isCreate: self.isCreate == "1" ? true : false) else {
                DispatchQueue.main.async(execute: {
                    let message = NSLocalizedString("Saving was unsuccessful. Please try again.", comment: "")
                    self.view.makeToast(message: message, duration: 0.3, position: "center" as AnyObject)
                })
                
                return
            }
            
            if self.isCreate == "1" {
                self.dataManager.requestCreateEvent(withBody: body, doneBlock: { (success) in
                    DispatchQueue.main.async(execute: {
                        if !success {
                            let message = NSLocalizedString("Saving was unsuccessful. Please try again.", comment: "")
                            self.view.makeToast(message: message, duration: 0.3, position: "center" as AnyObject)
                        }
                        
                        if success {
                            self.dataManager.requestCalendarEventList(forUser: user_id, doneBlock: { (success) in
                                // Post notification
                                let isLandscape = UIDevice.current.orientation.isLandscape
                                let object = ["orientation": isLandscape ? "1" : "0"]
                                let name = Notification.Name(rawValue: "CAL_NOTIFICATION_DEVICE_ORIENTATION_CHANGE")
                                self.notification.post(name: name, object: object)
                                self.dismiss(animated: true, completion: nil)
                            })
                        }
                    })
                })
            }
            else {
                self.dataManager.requestUpdateEvent(id, withBody: body, doneBlock: { (success) in
                    DispatchQueue.main.async(execute: {
                        if !success {
                            let message = NSLocalizedString("Updating event was unsuccessful. Please try again.", comment: "")
                            self.view.makeToast(message: message, duration: 0.3, position: "center" as AnyObject)
                        }
                        
                        if success {
                            self.dataManager.requestCalendarEventList(forUser: user_id, doneBlock: { (success) in
                                // Post notification
                                let isLandscape = UIDevice.current.orientation.isLandscape
                                let object = ["orientation": isLandscape ? "1" : "0"]
                                let name = Notification.Name(rawValue: "CAL_NOTIFICATION_DEVICE_ORIENTATION_CHANGE")
                                self.notification.post(name: name, object: object)
                                self.dismiss(animated: true, completion: nil)
                            })
                        }
                    })
                })
            }
        }

    }
    
    func toggleCancelButton(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    // MARK: - Required Fields Validation
    
    fileprivate func doesContainEmptyRequiredField() -> Bool {
        let id = self.dataManager.stringValue(self.eventObject.value(forKey: "id"))!
        guard let copy = self.dataManager.fetchDeepCopiedEventObject(withID: id) else {
            print("Error: Can't fetch copied event object!")
            return true
        }
        
        let keys = copy.entity.propertiesByName.keys
        let requiredFieldsKeys = ["title", "details", "event_start", "event_end"]
        
        for key in keys {
            if requiredFieldsKeys.contains(key) {
                let data = copy.value(forKey: key) as! String
                if data == "" { return true }
            }
        }
        
        return false
    }
    
    // MARK: - Updating Header View
    
    fileprivate func updateHeaderView() {
        guard let mo = self.eventObject else {
            print("Error: Event object is nil!")
            DispatchQueue.main.async(execute: { self.dismiss(animated: true, completion: nil) })
            return
        }
        
        guard let title = mo.value(forKey: "title") as? String,
            let background_color = mo.value(forKey: "background_color") as? String
            else {
                print("Error: Can't parse event object!")
                DispatchQueue.main.async(execute: { self.dismiss(animated: true, completion: nil) })
                return
        }
        
        self.headerTitleLabel.text = title
        self.headerView.backgroundColor = UIColor(hexString: background_color)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == self.eventFormSegueIdentifier) {
            let eventFormView = segue.destination as! CALCreateEventTableViewController
            eventFormView.isCreate = self.isCreate
            eventFormView.eventObject = self.eventObject
        }
    }

}
