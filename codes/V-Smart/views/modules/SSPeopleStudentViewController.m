//
//  SSPeopleStudentViewController.m
//  V-Smart
//
//  Created by VhaL on 3/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "SSPeopleStudentViewController.h"
#import "SSPeopleCell.h"
#import "SSPeopleItem.h"
#import "JMSGroupItem.h"
#import "JMSPeopleItem.h"

@interface SSPeopleStudentViewController ()
@property (nonatomic, strong) NSMutableArray *entriesMain;
@property (nonatomic, strong) NSMutableArray *entriesMainWorking;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIButton *buttonAll;
@property (nonatomic, weak) IBOutlet UIButton *buttonClassmates;
@property (nonatomic, weak) IBOutlet UIButton *buttonTeachers;

@property (nonatomic, weak) IBOutlet UIButton *buttonBack;
@property (nonatomic, weak) IBOutlet UIView *viewDetail;
@property (nonatomic, weak) IBOutlet UIView *viewList;

@property (nonatomic, weak) IBOutlet UILabel *labelPostedTopics;
@property (nonatomic, weak) IBOutlet UILabel *labelRepliedTopics;
@property (nonatomic, weak) IBOutlet UILabel *labelLikedByMe;
@property (nonatomic, weak) IBOutlet UILabel *labelReceivedLikes;
@property (nonatomic, weak) IBOutlet UILabel *labelFullName;
@property (nonatomic, weak) IBOutlet UILabel *labelPosition;
@property (nonatomic, weak) IBOutlet UILabel *labelSubjects;
@property (nonatomic, weak) IBOutlet UILabel *labelAlias;
@property (nonatomic, weak) IBOutlet UIImageView *imageViewAvatar;

@property (nonatomic, strong) JMSRecordGroupItem *groupItems;

@property (nonatomic, strong) JMSRecordPeopleItem *teachers;
@property (nonatomic, strong) JMSRecordPeopleItem *classmates;
@property (nonatomic, strong) JMSRecordPeopleItem *peers;
@property (nonatomic, strong) JMSRecordPeopleItem *students;

@property (nonatomic, assign) int selectedButtonIndex;

@property (nonatomic, strong) UIRefreshControl *refreshControlTableView;

@end

@implementation SSPeopleStudentViewController
@synthesize buttonAll, buttonClassmates, buttonTeachers, entriesMain, entriesMainWorking, buttonBack, viewDetail, viewList;

@synthesize labelAlias, labelLikedByMe, labelPostedTopics, labelReceivedLikes, labelRepliedTopics, imageViewAvatar, labelFullName, labelPosition, labelSubjects;

@synthesize selectedButtonIndex;

-(AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    selectedButtonIndex = 0;

    
    imageViewAvatar.layer.cornerRadius = imageViewAvatar.frame.size.height/2;
    //    imageViewAvatar.layer.borderWidth = 1;
    imageViewAvatar.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    labelAlias.layer.cornerRadius = labelAlias.frame.size.height/2;
    //    labelAlias.layer.borderWidth = 1;
    labelAlias.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    UINib *cellNib = [UINib nibWithNibName:@"SSPeopleCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"SSPeopleCell"];
    
    [self initializeData];
    [self buttonTapped:buttonAll];
    [self buttonBackTapped:buttonBack];
    [self addRefreshMode];
}

-(void)addRefreshMode{
    self.refreshControlTableView = [[UIRefreshControl alloc] init];
    NSString *pullRefresh = NSLocalizedString(@"Pull to Refresh",nil);
    self.refreshControlTableView.attributedTitle = [[NSAttributedString alloc] initWithString:pullRefresh];
    [self.refreshControlTableView addTarget:self action:@selector(initializeData) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshControlTableView];
}

-(void)initializeData{
    
    entriesMain = [[NSMutableArray alloc] init];
    
    NSString *peopleInfo = [NSString stringWithFormat:kEndPointGetPeopleInfo, [self account].user.id];
    NSString *url = [Utils buildUrl:peopleInfo];
    NSLog(@"getUserPeople url: %@", url);

    [JSONHTTPClient getJSONFromURLWithString:url completion:^(NSDictionary *json, JSONModelError *err) {
        if(err == nil){
            NSArray *arrayRecords = [json objectForKey:@"records"];
            
            NSLog(@"array records : %@", arrayRecords);
            
            NSError *parseError = nil;
            
            if ([[self account].user.position isEqualToString:kModeIsStudent]){
                NSArray *arrayTeacher = [self getTeachersRecord:arrayRecords];
                NSArray *arrayClassmates = [self getClassmatesRecord:arrayRecords];
                
                NSDictionary *dictTeacher = @{@"records": arrayTeacher};
                NSDictionary *dictClassmates = @{@"records": arrayClassmates};
                
                NSData *records = [NSJSONSerialization dataWithJSONObject:dictTeacher options:NSJSONWritingPrettyPrinted error:&parseError];
                NSString *jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
                self.teachers = [[JMSRecordPeopleItem alloc] initWithString:jsonString error:&parseError];
                [self addPeopleFrom:self.teachers.records isTeacher:YES];
                
                records = [NSJSONSerialization dataWithJSONObject:dictClassmates options:NSJSONWritingPrettyPrinted error:&parseError];
                jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
                self.classmates = [[JMSRecordPeopleItem alloc] initWithString:jsonString error:&parseError];
                [self addPeopleFrom:self.classmates.records isTeacher:NO];
            }
        }
        else{
            /* localizable strings */
            NSString *connectionErrorMessage = NSLocalizedString(@"Please make sure you are connected to the server", nil); //checked
            [self.view makeToast:connectionErrorMessage duration:2.0f position:@"center"];
        }
        
        [self.refreshControlTableView endRefreshing];
        
        if (selectedButtonIndex == 0){
            [self buttonTapped:buttonAll];
        }
        
        if (selectedButtonIndex == 1){
            [self buttonTapped:buttonClassmates];
        }
        
        if (selectedButtonIndex == 2){
            [self buttonTapped:buttonTeachers];
        }
    }];
}

//-(void)initializeData{
//    
//    entriesMain = [[NSMutableArray alloc] init];
//    
//    NSDictionary *dictBody = @{@"user_id": [NSNumber numberWithInt:[self account].user.id], @"position":[self account].user.position};
//    NSString *stringBody = [VSmartHelpers jsonString:dictBody];
//    NSString *url = [Utils buildUrl:kEndPointGetPeopleInfo];
//
//    [JSONHTTPClient postJSONFromURLWithString:url bodyString:stringBody completion:^(NSDictionary *json, JSONModelError *err) {
//        if(err == nil){
//            NSArray *arrayRecords = [json objectForKey:@"records"];
//            NSDictionary *dictRecord = [arrayRecords objectAtIndex:0];
//            
//            NSError *parseError = nil;
//            
//            if ([[self account].user.position isEqualToString:kModeIsStudent]){
//                NSArray *arrayTeacher = [dictRecord objectForKey:@"Teachers"];
//                NSArray *arrayClassmates = [dictRecord objectForKey:@"Classmates"];
//                
//                NSDictionary *dictTeacher = @{@"records": arrayTeacher};
//                NSDictionary *dictClassmates = @{@"records": arrayClassmates};
//                
//                NSData *records = [NSJSONSerialization dataWithJSONObject:dictTeacher options:NSJSONWritingPrettyPrinted error:&parseError];
//                NSString *jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
//                self.teachers = [[JMSRecordPeopleItem alloc] initWithString:jsonString error:&parseError];
//                [self addPeopleFrom:self.teachers.records isTeacher:YES];
//                
//                records = [NSJSONSerialization dataWithJSONObject:dictClassmates options:NSJSONWritingPrettyPrinted error:&parseError];
//                jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
//                self.classmates = [[JMSRecordPeopleItem alloc] initWithString:jsonString error:&parseError];
//                [self addPeopleFrom:self.classmates.records isTeacher:NO];
//            }
//        }
//        else{
//            /* localizable strings */
//            NSString *connectionErrorMessage = NSLocalizedString(@"Please make sure you are connected to the server", nil); //checked
//            [self.view makeToast:connectionErrorMessage duration:2.0f position:@"center"];
//        }
//        
//        [self.refreshControlTableView endRefreshing];
//        
//        if (selectedButtonIndex == 0){
//            [self buttonTapped:buttonAll];
//        }
//        
//        if (selectedButtonIndex == 1){
//            [self buttonTapped:buttonClassmates];
//        }
//        
//        if (selectedButtonIndex == 2){
//            [self buttonTapped:buttonTeachers];
//        }
//    }];
//}

- (NSString *)formatData:(NSString *)string {
    return [NSString stringWithFormat:@"%@",string];
}

- (NSArray *)getTeachersRecord:(NSArray *)records {
    
    NSArray *list = nil;
    
    NSMutableArray *listItem = [NSMutableArray array];
    for (NSDictionary *d in records) {

        NSString *user_id = [self formatData:d[@"teacher_id"]];
        NSString *course_id = [self formatData:d[@"course_id"]];
        NSString *course_name = [self formatData:d[@"course_name"]];
        NSString *course_code = [self formatData:d[@"course_code"]];
        NSString *course_description = [self formatData:d[@"course_description"]];
        NSString *section_id = [self formatData:d[@"section_id"]];
        NSString *section_name = [self formatData:d[@"section_name"]];

        NSString *first_name = @"";
        NSString *last_name = @"";
        
        NSString *techerName = [self formatData:d[@"teacher"]];
        NSArray *tuple =[techerName componentsSeparatedByString:@" "];
        if (tuple.count > 1) {
            first_name = [NSString stringWithFormat:@"%@",tuple[0]];
            last_name = [NSString stringWithFormat:@"%@",tuple[1]];
        }
        
        NSString *avatar = [self formatData:d[@"teacher_avatar"]];
        NSString *type = [self formatData:kModeIsTeacher];
        NSString *online = @"0";
        
        NSDictionary *object = @{@"user_id":user_id,
                                 @"course_id":course_id,
                                 @"course_name":course_name,
                                 @"course_code":course_code,
                                 @"course_description":course_description,
                                 @"section_id":section_id,
                                 @"section_name":section_name,
                                 @"first_name":first_name,
                                 @"last_name":last_name,
                                 @"avatar":avatar,
                                 @"type":type,
                                 @"online":online
                                 };
        
        [listItem addObject:object];
        
        /*
         @property (nonatomic, assign) NSInteger userId;//checked
         @property (nonatomic, assign) NSInteger courseId;//checked
         @property (nonatomic, strong) NSString *courseName;//checked
         @property (nonatomic, strong) NSString *courseCode;//checked
         @property (nonatomic, strong) NSString *courseDescription;//checked
         @property (nonatomic, assign) NSInteger sectionId;//checked
         @property (nonatomic, strong) NSString *sectionName;//checked
         @property (nonatomic, strong) NSString *firstName; //checked
         @property (nonatomic, strong) NSString *lastName;//checked
         @property (nonatomic, strong) NSString *avatar;
         @property (nonatomic, strong) NSString *type;
         */
        
        /*
            course_id: 1,//checked
            section_id: 4,//checked
            section_name: "Bronze",//checked
            course_name: "English 6",//checked
            course_code: "ENGLISH",//checked
            course_description: "English",//checked
            teacher_id: 55,//checked
            teacher: "Janet Gaylord",//checked
            teacher_avatar: "/socialstream/public/img/avatar/generic-girl.png"
         */
        
        
        
    }
    list = [NSArray arrayWithArray:listItem];
    
    NSLog(@"get teacher list : %@", list);
    
    return list;
}

- (NSArray *)getClassmatesRecord:(NSArray *)records {
    
    NSArray *list = nil;
    NSMutableArray *listItem = [NSMutableArray array];
    
    for (NSDictionary *d in records) {
        
        NSString *course_id = [self formatData:d[@"course_id"]];
        NSString *course_name = [self formatData:d[@"course_name"]];
        NSString *course_code = [self formatData:d[@"course_code"]];
        NSString *course_description = [self formatData:d[@"course_description"]];
        NSString *section_id = [self formatData:d[@"section_id"]];
        NSString *section_name = [self formatData:d[@"section_name"]];
        
        NSArray *classmates = d[@"classmates"];
        for (NSDictionary *c in classmates) {
            NSString *user_id = [self formatData:c[@"user_id"]];
            NSString *first_name = [self formatData:c[@"first_name"]];
            NSString *last_name = [self formatData:c[@"last_name"]];
            NSString *avatar = [self formatData:c[@"avatar"]];
            NSString *online = [self formatData:c[@"is_logged_in"]];
            NSString *type = [self formatData:kModeIsStudent];
            
            
            NSDictionary *object = @{@"user_id":user_id,
                                     @"course_id":course_id,
                                     @"course_name":course_name,
                                     @"course_code":course_code,
                                     @"course_description":course_description,
                                     @"section_id":section_id,
                                     @"section_name":section_name,
                                     @"first_name":first_name,
                                     @"last_name":last_name,
                                     @"avatar":avatar,
                                     @"type":type,
                                     @"online":online
                                     };
            [listItem addObject:object];
        }//for
    }//for
    
    list = [NSArray arrayWithArray:listItem];
    
    NSLog(@"get classmate list : %@", list);
    
    return list;
}

-(void)addPeopleFrom:(NSArray<JMSPeopleItem>*)records isTeacher:(BOOL)isTeacher{
    for (JMSPeopleItem *peopleItemInAPI in records) {
        SSPeopleItem *peopleItem = [[SSPeopleItem alloc] init];
        
        peopleItem.id = (int)peopleItemInAPI.userId;
        peopleItem.firstName = peopleItemInAPI.firstName;
        peopleItem.lastName = peopleItemInAPI.lastName;
        peopleItem.fullName = [NSString stringWithFormat:@"%@ %@", peopleItemInAPI.firstName, peopleItemInAPI.lastName];
        
        peopleItem.isTeacher = isTeacher;
        peopleItem.position = peopleItemInAPI.type;
        peopleItem.subjectsOrNickname = @"";
        peopleItem.urlImage = peopleItemInAPI.avatar;
        peopleItem.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[Utils buildUrl:peopleItem.urlImage]]]];
        
        peopleItem.numLikedByMe = @"0";
        peopleItem.numPostedTopics = @"0";
        peopleItem.numReceivedLikes = @"0";
        peopleItem.numRepliedTopics = @"0";
        
        [entriesMain addObject:peopleItem];
    }
}

-(IBAction)buttonBackTapped:(UIButton*)button{
//    buttonBack.hidden = YES;
//    viewDetail.hidden = YES;
//    viewList.hidden = NO;
    
    /*To hide*/
    [UIView animateWithDuration:0.25 animations:^{
        [buttonBack setAlpha:0.0f];
        [viewDetail setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [buttonBack setHidden:YES];
        [viewDetail setHidden:YES];
    }];
    
    /*To unhide*/
    [viewList setHidden:NO];
    [UIView animateWithDuration:0.25 animations:^{
        viewList.alpha = 1.0f;
    } completion:^(BOOL finished) {
    }];
}

-(IBAction)buttonTapped:(UIButton*)button{
    buttonAll.backgroundColor = [UIColor whiteColor];
    buttonClassmates.backgroundColor = [UIColor whiteColor];
    buttonTeachers.backgroundColor = [UIColor whiteColor];
    
    button.backgroundColor = [UIColor clearColor];
    
    if ([button isEqual:buttonAll]){
        selectedButtonIndex = 0;
    }
    
    if ([button isEqual:buttonClassmates]){
        selectedButtonIndex = 1;
    }
    
    if ([button isEqual:buttonTeachers]){
        selectedButtonIndex = 2;
    }
    
    switch (selectedButtonIndex) {
        case 0:
        {
            entriesMainWorking = [NSMutableArray arrayWithArray:entriesMain];
        }
            break;
        case 1:
        {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"isTeacher == %d", NO];
            entriesMainWorking = [NSMutableArray arrayWithArray:[entriesMain filteredArrayUsingPredicate:pred]];
        }
            break;
        case 2:
        {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"isTeacher == %d", YES];
            entriesMainWorking = [NSMutableArray arrayWithArray:[entriesMain filteredArrayUsingPredicate:pred]];
        }
            break;
        default:
            break;
    }
    
    
    [self.tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return entriesMainWorking.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SSPeopleCell *cell = (SSPeopleCell*)[tableView dequeueReusableCellWithIdentifier:@"SSPeopleCell"];
    if (cell==nil) {
        cell = [[SSPeopleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SSPeopleCell"];
    }
    
    [cell initializeCell];
    
    SSPeopleItem *peopleItem = [entriesMainWorking objectAtIndex:indexPath.row];
    cell.name.text = peopleItem.fullName;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if ([peopleItem.urlImage isEqualToString:@""]){
        cell.avatar.hidden = YES;
        cell.alias.hidden = NO;
        cell.alias.text = [VSmartHelpers aliasFromFirstName:peopleItem.firstName andLastName:peopleItem.lastName];
    }
    else{
        cell.avatar.image = peopleItem.image;
        cell.avatar.hidden = NO;
        cell.alias.hidden = YES;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    /*To hide*/
    [UIView animateWithDuration:0.25 animations:^{
        [viewList setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [viewList setHidden:YES];
    }];

    /*To unhide*/
    [buttonBack setHidden:NO];
    [viewDetail setHidden:NO];
    [UIView animateWithDuration:0.25 animations:^{
        buttonBack.alpha = 1.0f;
        viewDetail.alpha = 1.0f;
    } completion:^(BOOL finished) {
    }];
    
//    buttonBack.hidden = NO;
//    viewDetail.hidden = NO;
//    viewList.hidden = YES;
    
    SSPeopleCell *cell = (SSPeopleCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    SSPeopleItem *peopleItem = [entriesMainWorking objectAtIndex:indexPath.row];
    
    labelFullName.text = peopleItem.fullName;
    labelPosition.text = peopleItem.position;
    labelSubjects.text = peopleItem.subjectsOrNickname;
    
    labelLikedByMe.text = peopleItem.numLikedByMe;
    labelPostedTopics.text = peopleItem.numPostedTopics;
    labelReceivedLikes.text = peopleItem.numReceivedLikes;
    labelRepliedTopics.text = peopleItem.numRepliedTopics;
    
    NSString *url = VS_FMT([Utils buildUrl:kEndPointGetUserProfile], peopleItem.id);
    [JSONHTTPClient getJSONFromURLWithString:url completion:^(NSDictionary *json, JSONModelError *err) {
        if(err == nil){
            NSArray *arrayRecords = [json objectForKey:@"records"];
            NSDictionary *dictStatistics;
            for (NSDictionary *dict in arrayRecords) {
                //Statistics
                if ([dict objectForKey:@"Statistics"]) {
                    dictStatistics = [dict objectForKey:@"Statistics"];
                    
                    if ([dictStatistics objectForKey:@"count_liked"]){
                        NSString *countLike = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_liked"]];
                        labelLikedByMe.text = countLike;
                    }
                    
                    if ([dictStatistics objectForKey:@"count_posted"]){
                        NSString *countPosted = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_posted"]];
                        labelPostedTopics.text = countPosted;
                    }
                    
                    if ([dictStatistics objectForKey:@"count_received_likes"]){
                        NSString *countReceivedLikes = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_received_likes"]];
                        labelReceivedLikes.text = countReceivedLikes;
                    }
                    
                    if ([dictStatistics objectForKey:@"count_replied"]){
                        NSString *countReplied = [NSString stringWithFormat:@"%@", [dictStatistics objectForKey:@"count_replied"]];
                        labelRepliedTopics.text = countReplied;
                    }
                    
                    continue;
                }
            }
        }
        else{
            /* localizable strings */
            NSString *connectionErrorMessage = NSLocalizedString(@"Please make sure you are connected to the server", nil); //checked
            [self.view makeToast:connectionErrorMessage duration:2.0f position:@"center"];
        }
    }];
    
    
    if ([peopleItem.urlImage isEqualToString:@""]){
        imageViewAvatar.hidden = YES;
        labelAlias.hidden = NO;
        labelAlias.backgroundColor = cell.alias.backgroundColor;
        labelAlias.text = [VSmartHelpers aliasFromFirstName:peopleItem.firstName andLastName:peopleItem.lastName];
    }
    else{
        imageViewAvatar.image = peopleItem.image;
        imageViewAvatar.hidden = NO;
        labelAlias.hidden = YES;
    }
}

@end