//
//  CurriculumDetailView.m
//  V-Smart
//
//  Created by Ryan Migallos on 9/15/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "CurriculumDetailView.h"
#import <WebKit/WebKit.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import "HMSegmentedControl.h"
#import "Masonry.h"
#import "MainHeader.h"

@interface CurriculumDetailView () <WKScriptMessageHandler, WKUIDelegate>

//Description
@property (strong, nonatomic) IBOutlet UILabel *labelDescription;
@property (strong, nonatomic) IBOutlet UILabel *fieldDescription;

//School Year
@property (strong, nonatomic) IBOutlet UILabel *labelSchoolYear;
@property (strong, nonatomic) IBOutlet UILabel *fieldSchoolYear;

//School Name
@property (strong, nonatomic) IBOutlet UILabel *labelSchoolName;
@property (strong, nonatomic) IBOutlet UILabel *fieldSchoolName;

//Segment Control
@property (strong, nonatomic) IBOutlet UIView *segmentContainer;

//WebView Container
@property (strong, nonatomic) IBOutlet UIView *webContainer;
@property (strong, nonatomic) NSMutableDictionary *lookup;
@property (strong, nonatomic) HMSegmentedControl *periodButton;
@property (strong, nonatomic) WKWebView *webView;

@end

@implementation CurriculumDetailView

- (NSString *)stringValue:(NSString *)key {
    
    NSString *value = [NSString stringWithFormat:@"%@", [self.mo valueForKey:key] ];
    
    if ([value isEqualToString:@"<null>"] || [value isEqualToString:@"(null)"]) {
        return @"";
    }
    
    return value;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.labelDescription.text = NSLocalizedString(@"Description", nil);
    self.labelSchoolYear.text = NSLocalizedString(@"School Year", nil);
    self.labelSchoolName.text = NSLocalizedString(@"School Name", nil);
    
    NSString *course_name = [self stringValue:@"course_name"];
    NSString *course_template_name = [self stringValue:@"curriculum_template_name"];
    self.title = [NSString stringWithFormat:@"%@ - %@", course_name, course_template_name];
    self.fieldDescription.text = [self stringValue:@"curriculum_description"];
    self.fieldSchoolYear.text = [self stringValue:@"sy_name"];
    self.fieldSchoolName.text = [self stringValue:@"school_profile_name"];
    
    [self loadSegmentedButtons: [self populateGradingPeriod] ];
    
    [self setupRightBarButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setupRightBarButton {
    
    UILabel *statusLabel = [[UILabel alloc] init];
    statusLabel.frame = CGRectMake(0, 0, 80, 30);
    statusLabel.textAlignment = NSTextAlignmentRight;
    NSString *curriculum_status = [self stringValue:@"curriculum_status_remarks"];
    statusLabel.text = curriculum_status;
    statusLabel.textColor = [UIColor lightGrayColor];
    if ([curriculum_status isEqualToString:@"Approved"]) {
        statusLabel.textColor = UIColorFromHex(0x16a085);
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:statusLabel];
}

#pragma mark - WebKit WebView

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    //
    //    // Check to make sure the name is correct
    //    if ([message.name isEqualToString:@"observe"]) {
    //        // Log out the message received
    //        NSLog(@"Received event %@", message.body);
    //
    //        // Then pull something from the device using the message body
    //        NSString *version = [[UIDevice currentDevice] valueForKey:message.body];
    //
    //        // Execute some JavaScript using the result
    //        NSString *exec_template = @"set_headline(\"received: %@\");";
    //        NSString *exec = [NSString stringWithFormat:exec_template, version];
    //        [_webView evaluateJavaScript:exec completionHandler:nil];
    //
//            NSString *javascriptString = @"alert(\"Hello! I am an alert box!!\")";
//            [_webView evaluateJavaScript:javascriptString completionHandler:nil];
    //    }
}

- (NSArray *)populateGradingPeriod {
    
    NSSet *set = [self.mo valueForKey:@"periods"];
    NSArray *periods = [set allObjects];
    
    NSSortDescriptor *indexSort = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
    NSArray *sorted_periods = [periods sortedArrayUsingDescriptors:@[indexSort]];
    
    self.lookup = [NSMutableDictionary dictionary];
    NSMutableArray *list = [NSMutableArray array];
    
    for (NSManagedObject *p in sorted_periods) {
        NSString *buttonName = [NSString stringWithFormat:@"%@", [p valueForKey:@"name"] ];
        [list addObject:buttonName];
        [self.lookup setValue:[p valueForKey:@"content"] forKey:buttonName];
    }
    
    return list;
}

- (void)loadSegmentedButtons:(NSArray *)items {
    
    if (items.count > 0) {
        
        _periodButton = [[HMSegmentedControl alloc] initWithFrame:self.segmentContainer.frame];
        _periodButton.sectionTitles = items;
        _periodButton.userDraggable = YES;
        _periodButton.selectedSegmentIndex = 0;
        _periodButton.selectionIndicatorHeight = 4.0f;
        _periodButton.backgroundColor = UIColorFromHex(0x3498db);
        _periodButton.titleTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor],
                                              NSFontAttributeName : FONT_NEUE(17) };
        
        _periodButton.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : UIColorFromHex(0x3498db) };
        _periodButton.selectionIndicatorColor = [UIColor whiteColor];
        _periodButton.selectionStyle = HMSegmentedControlSelectionStyleBox;
        _periodButton.selectionIndicatorBoxOpacity = 1.0f;
        _periodButton.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationUp;
        [self.segmentContainer addSubview:_periodButton];
        
        UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
        [_periodButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.segmentContainer).with.insets(padding);
        }];
        
        __weak typeof(self) wo = self;
        [_periodButton setIndexChangeBlock:^(NSInteger index) {
            NSString *buttonName =  items[index];
            [wo executeWebOperationWithButtonName:buttonName];
        }];
        
        //LOAD DEFAULTS
        NSInteger defaulIndex = _periodButton.selectedSegmentIndex;
        NSString *buttonName =  items[defaulIndex];
        [self executeWebOperationWithButtonName:buttonName];
    }
}

- (void)executeWebOperationWithButtonName:(NSString *)buttonName {
    
    NSData *contents = [self.lookup valueForKey:buttonName];
    [self loadWebViewWithContents:contents];
}

- (void)loadWebViewWithContents:(NSData *)data {
    
    // Create WKWebViewConfiguration instance
    WKWebViewConfiguration *configuration = [self loadConfiguration:data];
    
    // Initialize the WKWebView with the current frame and the configuration
    _webView = [[WKWebView alloc] initWithFrame:self.webContainer.frame configuration:configuration];
    [self.webContainer addSubview:_webView];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.webContainer).with.insets(padding);
    }];
    
    // load web view
    NSString *htmlString = [self resourceFile:@"webtableview.html"];
    [_webView loadHTMLString:htmlString baseURL:nil];
}

- (WKWebViewConfiguration *)loadConfiguration:(NSData *)data {
    
    // Create WKWebViewConfiguration instance
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    
    // Setup WKUserContentController instance for injecting user script
    WKUserContentController *controller = [[WKUserContentController alloc] init];
    
    [self setScript:@"jquery211.js" inController:controller injectionAtStart:YES];
    [self setScript:@"jquery.dataTables.js" inController:controller injectionAtStart:YES];
    [self setScript:@"curriculumscript.js" inController:controller injectionAtStart:YES];
    
    NSString *command = [self dataTableWithData:data];
    [self setCutomScript:command inController:controller injectionAtStart:NO];
    
    // Configure the WKWebViewConfiguration instance with the WKUserContentController
    configuration.userContentController = controller;
    
    return configuration;
}

- (NSString *)dataTableWithData:(NSData *)object {
    
    //default data values
    NSArray *dataSet = @[ @[@""] ];
    NSArray *columns = @[ @{@"title": @""} ];
    NSDictionary *dataObject = @{ @"data":dataSet, @"columns":columns };
    
    NSString *jsonString = [[NSString alloc] initWithData:object encoding:NSUTF8StringEncoding];
    if (jsonString.length > 2 ) {
        //NOTE: if the json string is not containing only new lines
        //      proceed with processing
        dataObject = [self javascriptCoreWithStringData:jsonString];
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataObject options:0 error:nil];
    NSString *string = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *command = [NSString stringWithFormat:@"populateDataTableWithCustomData( %@ )", string];
    return command;
    
//    NSArray *dataSet = @[ @[@"Ryan",@"Migallos",@"Oct 14"] ];
//    NSArray *columns = @[ @{@"title": @"First Name"}, @{@"title": @"Last Name"}, @{@"title": @"Date"} ];
//    NSDictionary *dataObject = @{ @"data":dataSet, @"columns":columns };
//    return dataObject;
}

- (void)setScript:(NSString *)script inController:(WKUserContentController *)controller injectionAtStart:(BOOL)start {
    
    WKUserScriptInjectionTime time = WKUserScriptInjectionTimeAtDocumentEnd;
    if (start) {
        time = WKUserScriptInjectionTimeAtDocumentStart;
    }
    WKUserScript *myScript = [self userScript:script injectionTime:time];
    [controller addUserScript:myScript];
}

- (void)setCutomScript:(NSString *)customScript inController:(WKUserContentController *)controller injectionAtStart:(BOOL)start {
    
    WKUserScriptInjectionTime time = WKUserScriptInjectionTimeAtDocumentEnd;
    if (start) {
        time = WKUserScriptInjectionTimeAtDocumentStart;
    }
    
    WKUserScript *userScript = [[WKUserScript alloc] initWithSource:customScript injectionTime:time forMainFrameOnly:NO];
    [controller addUserScript:userScript];
}

- (WKUserScript *)userScript:(NSString *)file injectionTime:(WKUserScriptInjectionTime)time {
    WKUserScript *userScript = [[WKUserScript alloc] initWithSource:[self resourceFile:file]
                                                      injectionTime:time
                                                   forMainFrameOnly:NO];
    return userScript;
}

- (NSString *)resourceFile:(NSString *)filename {
    
    NSString *name = [filename stringByDeletingPathExtension];
    NSString *extension = [filename pathExtension];
    NSBundle *bundle = [NSBundle mainBundle];
    NSURL *path = [bundle URLForResource:name withExtension:extension];
    NSString *string = [NSString stringWithContentsOfURL:path encoding:NSUTF8StringEncoding error:nil];
//    NSLog(@"resource content : %@", string);
    
    return string;
}

- (NSDictionary *)javascriptCoreWithStringData:(NSString *)stringdata {
    
    NSLog(@"%s - %@", __PRETTY_FUNCTION__, stringdata);
    
    // defining a JavaScript function
    NSString *columFunctionText = @"function columns(d){var c=JSON.parse(d);var b=[];var a=c[0];b.push({title:'Domain'});a.content_values.forEach(function(f,e){b.push({title:f.content_column_name})});return b};";
    
    NSArray *columnList = [[self arrayValueFromName:@"columns" functionBody:columFunctionText parameter:stringdata] toArray];
    
    NSString *recordFunctionText = @"function recordFromData(a){var a=JSON.parse(a);var b=[];a.forEach(function(e,c){var d=[];d.push(e.domain);e.content_values.forEach(function(g,f){d.push(g.content_column_value)});b.push(d)});return b};";
    NSArray *dataSet = [[self arrayValueFromName:@"recordFromData" functionBody:recordFunctionText parameter:stringdata] toArray];
    
    return @{ @"data":dataSet, @"columns":columnList };
}

- (JSValue *)arrayValueFromName:(NSString *)function functionBody:(NSString *)body parameter:(NSString *)stringdata {
    
    // getting a JSContext
    JSContext *context = [JSContext new];
    [context evaluateScript:body];
    JSValue *executeFunction = context[function];
    JSValue *functionValue = [executeFunction callWithArguments:@[ stringdata ]];
    return functionValue;
}

@end
