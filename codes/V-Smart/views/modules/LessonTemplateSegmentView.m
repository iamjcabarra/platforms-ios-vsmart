//
//  LessonTemplateSegmentView.m
//  V-Smart
//
//  Created by Julius Abarra on 08/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "LessonTemplateSegmentView.h"
#import "LessonTemplateOverviewView.h"
#import "LessonTemplateStageView.h"

@interface LessonTemplateSegmentView ()

@property (strong, nonatomic) LessonTemplateOverviewView *templateOverview;
@property (strong, nonatomic) LessonTemplateStageView *templateStageContentView;

@property (strong, nonatomic) NSString *currentSegueIdentifier;

@end

@implementation LessonTemplateSegmentView

static NSString *kShowLessonStageOverview = @"showLessonStageOverview";
static NSString *kShowStageContentView = @"showLessonStageContent";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.currentSegueIdentifier = kShowLessonStageOverview;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kShowLessonStageOverview]) {
        self.templateOverview = (LessonTemplateOverviewView *)[segue destinationViewController];
        self.templateOverview.copiedLessonObject = self.copiedLessonObject;
        
        if (self.childViewControllers.count > 0) {
            [self swapFromView:[self.childViewControllers objectAtIndex:0] toView:segue.destinationViewController];
        }
        else {
            [self addChildViewController:segue.destinationViewController];
            ((UIViewController *)segue.destinationViewController).view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:((UIViewController *)segue.destinationViewController).view];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else {
        self.templateStageContentView = (LessonTemplateStageView *)[segue destinationViewController];
        self.templateStageContentView.selectedContentStage = self.selectedContentStage;
        [self swapFromView:[self.childViewControllers objectAtIndex:0] toView:segue.destinationViewController];
    }
}

- (void)swapFromView:(UIViewController *)fromView toView:(UIViewController *)toView {
    toView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    UIViewAnimationOptions transition = UIViewAnimationOptionTransitionCrossDissolve;
    
    [fromView willMoveToParentViewController:nil];
    [self addChildViewController:toView];
    [self transitionFromViewController:fromView
                      toViewController:toView
                              duration:0.5
                               options:transition
                            animations:nil
                            completion:^(BOOL finished) {
                                [fromView removeFromParentViewController];
                                [toView didMoveToParentViewController:self];
                            }];
}

- (void)swapEmbeddedViews:(NSString *)segueIdentifier {
    [self performSegueWithIdentifier:segueIdentifier sender:nil];
}

@end
