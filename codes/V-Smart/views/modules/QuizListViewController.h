//
//  QuizListViewController.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/5/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuizListViewController : UIViewController

@property (nonatomic, strong) NSString *courseid;
@property (nonatomic, strong) NSString *userid;

@end
