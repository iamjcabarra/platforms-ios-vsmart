//
//  LPMMainActionViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 05/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMMainActionViewController: UIViewController {
    
    @IBOutlet fileprivate var progressView: UIProgressView!
    @IBOutlet fileprivate var segmentedControl: UISegmentedControl!
    @IBOutlet fileprivate var lessonStageNameLabel: UILabel!
    @IBOutlet fileprivate var nextButton: UIButton!
    @IBOutlet fileprivate var previousButton: UIButton!
    @IBOutlet fileprivate var finishButton: UIButton!
    
    var actionType: LPMLessonActionType!
    var lessonObject: NSManagedObject!
    
    fileprivate let kMainActionViewSwapperSegueIdentifier = "SHOW_MAIN_ACTION_SWAPPER_VIEW"
    fileprivate let kMainOverviewSTVCSegueIdentifier = "SHOW_MAIN_OVERVIEW_VIEW"
    fileprivate let kStageContentSTVCSegueIdentifier = "SHOW_STAGE_CONTENT_VIEW"

    fileprivate var mainActionViewSwapper: LPMMainActionSVSwapper!
    fileprivate var templateContentStages = [[String: String]]()
    fileprivate var selectedTemplateContentStageID = ""
    fileprivate var selectedSegmentedViewIndex = 0
    
    // MARK: - Data Managers
    
    fileprivate lazy var lessonPlanDataManager: LessonPlanDataManager = {
        let lpdm = LessonPlanDataManager.sharedInstance()
        return lpdm!
    }()
    
    fileprivate lazy var curriculumPlannerDataManager: CPMDataManager = {
        let cpdm = CPMDataManager.sharedInstance
        return cpdm
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Clear lesson content stage name
        self.lessonStageNameLabel.text = ""
        
        // Resize progress view
        self.progressView.transform = self.progressView.transform.scaledBy(x: 1.0, y: 2.0)
        
        // Segmented control initial set up
        self.segmentedControl.removeAllSegments()
        self.segmentedControl.tintColor = UIColor(rgba: "#00AEEF")
        self.segmentedControl.addTarget(self, action: #selector(self.segmentedControlAction(_:)), for: .valueChanged)
        
        // Initialize segmented views for segmented control
        self.setUpSegmentedViews()
        
        // Load initial segmented view (lesson content overview)
        self.loadSegmentedViewWithIndex(0)
        
        // Navigation button actions
        self.nextButton.addTarget(self, action: #selector(self.nextButtonAction(_:)), for: .touchUpInside)
        self.previousButton.addTarget(self, action: #selector(self.previousButtonAction(_:)), for: .touchUpInside)
        self.finishButton.addTarget(self, action: #selector(self.finishButtonAction(_:)), for: .touchUpInside)
        
        // Progress observer
        self.progressViewNotificationObserver(nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.customizeNavigationBar()
        
        // Add progress observer
        NotificationCenter.default.addObserver(self, selector: #selector(self.progressViewNotificationObserver(_:)), name: NSNotification.Name(rawValue: "updateProgressView"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        // Remove progress observer
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "updateProgressView"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Custom Navigation Bar
    
    func customizeNavigationBar() {
        // Decorate navigation bar
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "#5ABE3E")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        if (self.actionType == LPMLessonActionType.add) {
            self.title = NSLocalizedString("Add Lesson", comment: "")
        }
        
        if (self.actionType == LPMLessonActionType.edit) {
            self.title = NSLocalizedString("Edit Lesson", comment: "")
        }
      
        // Custom navigation back button
        let customBackButton = UIButton.init(type: UIButtonType.system)
        customBackButton.asBackButtonWithTitle("", color: UIColor.white)
        
        let customBackButtonAction = #selector(self.customBackButtonAction(_:))
        customBackButton.addTarget(self, action: customBackButtonAction, for: .touchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem.init(customView: customBackButton)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    // MARK: - Custom Segmented Views
    
    func setUpSegmentedViews() {
        // Make lesson content overview as the first segmented view
        let overviewString = NSLocalizedString("Overview", comment: "")
        self.segmentedControl.insertSegment(withTitle: overviewString, at: 0, animated: true)
        self.segmentedControl.selectedSegmentIndex = 0
        
        // Retrieve template objects from core data
        let lpid = self.lessonObject.value(forKey: "lp_id") as! String
        let predicate = NSPredicate(format: "lesson.lp_id == %@", lpid)
        
        if let templateContents = self.lessonPlanDataManager.getObjectsForEntity(kCopyLessonContentEntity, with: predicate, andSortDescriptor: "stage_id") {
            let stageString = NSLocalizedString("Stage", comment: "")
            var index = 1
            
            for mo in templateContents {
                // Add segmented view (lesson content stage) to segmented control
                self.segmentedControl.insertSegment(withTitle: "\(stageString) \(index)", at: index, animated: true)
                
                // Add lesson content stage details to array
                self.templateContentStages.append(["stage_id": (mo as AnyObject).value(forKey: "stage_id") as! String, "stage_name": (mo as AnyObject).value(forKey: "stage_name") as! String])
                
                index += 1
            }
        }
    }
    
    func loadSegmentedViewWithIndex(_ index: Int) {
        self.selectedSegmentedViewIndex = index
        
        // Lesson content overview
        var lessonContentStageName = ""
        var lessonContentStageID = ""
        var segueIdentifier = self.kMainOverviewSTVCSegueIdentifier
        
        // Lesson content stage
        if (index != 0) {
            let templateContentStage = self.templateContentStages[index - 1]
            lessonContentStageName = templateContentStage["stage_name"]!.uppercased()
            lessonContentStageID = templateContentStage["stage_id"]!
            segueIdentifier = self.kStageContentSTVCSegueIdentifier
        }
        
        DispatchQueue.main.async(execute: {
            self.segmentedControl.selectedSegmentIndex = index
            self.lessonStageNameLabel.text = lessonContentStageName
            
            // Swap to segmented view
            self.mainActionViewSwapper.lessonObject = self.lessonObject
            self.mainActionViewSwapper.lessonContentStageID = lessonContentStageID
            self.mainActionViewSwapper.swapToViewControllerWithSegueIdentifier(segueIdentifier)
            
            // Change navigation button attributes
            self.updateNavigationButtonStateForSegmentedViewWithIndex(index)
        })
    }
    
    // MARK: - Segmented Control Event Handler
    
    func segmentedControlAction(_ sender: UISegmentedControl) {
        self.loadSegmentedViewWithIndex(sender.selectedSegmentIndex)
    }
    
    // MARK: - Navigation Button Event Handler
    
    func nextButtonAction(_ sender: UIButton) {
        let nextSegmentedViewIndex = self.selectedSegmentedViewIndex + 1
        
        if (nextSegmentedViewIndex < self.segmentedControl.numberOfSegments) {
            self.loadSegmentedViewWithIndex(nextSegmentedViewIndex)
        }
        else {
            self.showAlertForCancelAction()
        }
    }
    
    func previousButtonAction(_ sender: UIButton) {
        let previousSegmentedViewIndex = self.selectedSegmentedViewIndex - 1;
        
        if (previousSegmentedViewIndex >= 0) {
            self.loadSegmentedViewWithIndex(previousSegmentedViewIndex)
        }
    }
    
    func finishButtonAction(_ sender: UIButton) {
        let indicatorString = (self.actionType == LPMLessonActionType.add) ? "\(NSLocalizedString("Saving", comment: ""))..." : "\(NSLocalizedString("Updating", comment: ""))..."
        HUD.showUIBlockingIndicator(withText: indicatorString)
        
        if (self.hasEmptyRequiredField()) {
            DispatchQueue.main.async(execute: {
                HUD.hideUIBlockingIndicator()
                self.showNotificationMessage(false)
            })
        }
        else {
            if (self.actionType == LPMLessonActionType.add) {
                if let postBody = self.lessonPlanDataManager.prepareLessonObject(self.lessonObject, forAction: LPMLessonActionType.add.rawValue) {
                    self.lessonPlanDataManager.requestCreateNewLessonPlan(withPostBody: postBody, dataBlock: { (data) in
                        let success = data != nil ? true : false
                        
                        if let newLPID = data?["lpid"] as? String {
                            let competencies = self.curriculumPlannerDataManager.fetchSelectedLearningCompetencies()
                                    let userid = self.lessonPlanDataManager.loginUser()
                        
                            if competencies.count > 0 {
                                self.curriculumPlannerDataManager.requestCreateCurriculumAssociationToLessonPlanWithID(newLPID, andUserWithID: userid!, postBody: ["competencies": competencies], completionHandler: { (success) in
                                        // TO-DO: Message here
                                    })
                                }
                            }
                        
                        DispatchQueue.main.async(execute: {
                            HUD.hideUIBlockingIndicator()
                            self.showNotificationMessage(success)
                        })
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        self.showNotificationMessage(false)
                    })
                }
            }
            else {
                if let postBody = self.lessonPlanDataManager.prepareLessonObject(self.lessonObject, forAction: LPMLessonActionType.edit.rawValue) {
                    guard let lpid = self.lessonObject.value(forKey: "lp_id") as? String else {
                        return
                    }
                    
                    self.lessonPlanDataManager.requestUpdateLessonPlan(withLessonPlanID: lpid, andPostBody: postBody, doneBlock: { (success) in
                        if (success) {
                            if let filePath = postBody["file_path"] as? String {
                                if (!filePath.hasPrefix("http") && filePath != "") {
                                    if let lcid = postBody["lc_id"] as? String {
                                        self.lessonPlanDataManager.requestUpdateLessonPlanFile(filePath, forLearningContentWithID: lcid, doneBlock: { (success) in
                                            // TO-DO: Message here
                                        })
                                    }
                                }
                            }
                            
                            let competencies = self.curriculumPlannerDataManager.fetchSelectedLearningCompetencies()
                                    let userid = self.lessonPlanDataManager.loginUser()
                                    
                            self.curriculumPlannerDataManager.requestUpdateCurriculumAssociationToLessonPlanWithID(lpid, andUserWithID: userid!, postBody: ["competencies": competencies], completionHandler: { (success) in
                                        // TO-DO: Message here
                                    })
                                }
                        
                        DispatchQueue.main.async(execute: {
                            HUD.hideUIBlockingIndicator()
                            self.showNotificationMessage(success)
                        })
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        self.showNotificationMessage(false)
                    })
                }
            }
        }
    }
    
    func customBackButtonAction(_ sender: UIButton) {
        self.showAlertForCancelAction()
    }
    
    // MARK: - Navigation Button State Update
    
    func updateNavigationButtonStateForSegmentedViewWithIndex(_ index: Int) {
        let segmentedViewsCount = self.segmentedControl.numberOfSegments
        let isLastSegmentedView = (segmentedViewsCount == index + 1) ? true : false
        
        if (isLastSegmentedView) {
            self.nextButton.isHidden = false
            self.previousButton.isHidden = segmentedViewsCount == 1 ? true : false
            self.finishButton.isHidden = false
            
            self.nextButton.isUserInteractionEnabled = true
            self.previousButton.isUserInteractionEnabled = true
            self.finishButton.isUserInteractionEnabled = true
            
            let cancelString = NSLocalizedString("Cancel", comment: "")
            self.nextButton.setTitle(cancelString, for: UIControlState())
            self.nextButton.setTitle(cancelString, for: .selected)
            self.nextButton.setTitle(cancelString, for: .highlighted)
            self.nextButton.backgroundColor = UIColor(rgba: "#999999")
            
            let previousString = NSLocalizedString("Previous", comment: "")
            self.previousButton.setTitle(previousString, for: UIControlState())
            self.previousButton.setTitle(previousString, for: .selected)
            self.previousButton.setTitle(previousString, for: .highlighted)
            self.previousButton.backgroundColor = UIColor(rgba: "#999999")
            
            let finishString = (self.actionType == LPMLessonActionType.add) ? NSLocalizedString("Save", comment: "") : NSLocalizedString("Update", comment: "")
            self.finishButton.setTitle(finishString, for: UIControlState())
            self.finishButton.setTitle(finishString, for: .selected)
            self.finishButton.setTitle(finishString, for: .highlighted)
            self.finishButton.backgroundColor = UIColor(rgba: "#5ABE65")
            
            if (self.hasEmptyRequiredField()) {
                self.finishButton.isUserInteractionEnabled = false
                self.finishButton.backgroundColor = UIColor(rgba: "#CCCCCC")
            }
        }
        else {
            self.nextButton.isHidden = false
            self.previousButton.isHidden = index == 0 ? true : false
            self.finishButton.isHidden = true
            
            self.nextButton.isUserInteractionEnabled = true
            self.previousButton.isUserInteractionEnabled = true
            self.finishButton.isUserInteractionEnabled = false
            
            let nextString = NSLocalizedString("Next", comment: "")
            self.nextButton.setTitle(nextString, for: UIControlState())
            self.nextButton.setTitle(nextString, for: .selected)
            self.nextButton.setTitle(nextString, for: .highlighted)
            self.nextButton.backgroundColor = UIColor(rgba: "#3498DB")
            
            let previousString = NSLocalizedString("Previous", comment: "")
            self.previousButton.setTitle(previousString, for: UIControlState())
            self.previousButton.setTitle(previousString, for: .selected)
            self.previousButton.setTitle(previousString, for: .highlighted)
            self.previousButton.backgroundColor = UIColor(rgba: "#999999")
        }
    }
    
    // MARK: - Alert View Message
    
    func showAlertForCancelAction() {
        let preString = NSLocalizedString("Are you sure you want to cancel", comment: "")
        let midString = (self.actionType == LPMLessonActionType.add) ? NSLocalizedString("creating", comment: "") : NSLocalizedString("updating", comment: "")
        let sufString = NSLocalizedString("this lesson plan?", comment: "")
        let avMessage = "\(preString) \(midString) \(sufString)"
        
        let alert = UIAlertController(title: "", message: avMessage, preferredStyle: .alert)
        
        let positiveAction = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { (Alert) -> Void in
            // NOTE: This is just temporary for cpm integration
            _ = self.curriculumPlannerDataManager.clearDataOfLearningCompetency()
            
            DispatchQueue.main.async(execute: {
                for vc in (self.navigationController?.viewControllers)! {
                    if vc.isKind(of: LPMLessonViewController.self)  {
                        _ = self.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            })
        }
        
        let negativeAction = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(positiveAction)
        alert.addAction(negativeAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showNotificationMessage(_ operationSuccessful: Bool) {
        var message = ""
        
        if (operationSuccessful) {
            let ms1 = NSLocalizedString("Lesson plan was successfully created.", comment: "")
            let ms2 = NSLocalizedString("Lesson plan was successfully updated.", comment: "")
            message = (self.actionType == LPMLessonActionType.add) ? ms1 : ms2
        }
        else {
            let ms1 = NSLocalizedString("Lesson plan was not successfully created.", comment: "")
            let ms2 = NSLocalizedString("Lesson plan was not successfully updated.", comment: "")
            message = (self.actionType == LPMLessonActionType.add) ? ms1 : ms2
        }
        
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: NSLocalizedString("Okay", comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                if (operationSuccessful) {
                    for vc in (self.navigationController?.viewControllers)! {
                        if vc.isKind(of: LPMLessonViewController.self)  {
                            _ = self.navigationController?.popToViewController(vc, animated: true)
                        }
                    }
                }
                
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Segmented View Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.kMainActionViewSwapperSegueIdentifier {
            self.mainActionViewSwapper = segue.destination as? LPMMainActionSVSwapper
        }
    }
    
    // MARK: - Required Field Validation
    
    func hasEmptyRequiredField() -> Bool {
        let keys = self.lessonObject.entity.propertiesByName.keys
        let requiredFieldKeys = ["name", "school_name", "school_address", "quarter", "level", "unit", "start_date", "end_date"]
        
        for key in keys where key != "contents" {
            if (requiredFieldKeys.contains(key)) {
                let data = self.lessonObject.value(forKey: key) as! String
                
                if (data == "") {
                    return true
                }
            }
        }
        
        return false
    }
    
    // MARK: - Progress Update Notification
    
    func progressViewNotificationObserver(_ notification: Notification?) {
        var progress = 0.0
        var lessonContentOverviewCount = 0
        var lessonContentStageCount = 0
        var voidDataCount = 0

        if (self.lessonObject != nil) {
            let lessonContentOverviewKeys = self.lessonObject.entity.propertiesByName.keys
            lessonContentOverviewCount = lessonContentOverviewKeys.count
            
            for k in lessonContentOverviewKeys where k != "contents" {
                if let data = self.lessonObject.value(forKey: k) {
                    voidDataCount = data as! String == "" ? voidDataCount + 1 : voidDataCount
                }
            }

            let lessonContentSet = self.lessonObject.value(forKey: "contents") as! NSSet
            let lessonContentObjects = lessonContentSet.allObjects
            
            for cmo in lessonContentObjects {
                let lessonContentStageProcessSet = (cmo as AnyObject).value(forKey: "processes") as! NSSet
                let lessonContentStageProcessObjects = lessonContentStageProcessSet.allObjects
                
                for pmo in lessonContentStageProcessObjects {
                    let lessonContentStageProcessObjectKeys = (pmo as! NSManagedObject).entity.propertiesByName.keys
                    
                    for k in lessonContentStageProcessObjectKeys where k == "is_file" {
                        lessonContentStageCount += 1
                        let isFile = (pmo as! NSManagedObject).value(forKey: k) as! String
                        
                        if (isFile == "1") {
                            let filePath = (pmo as! NSManagedObject).value(forKey: "file_path") as! String
                            voidDataCount = filePath == "" ? voidDataCount + 1 : voidDataCount
                        }
                        else {
                            let content = (pmo as! NSManagedObject).value(forKey: "content") as! String
                            voidDataCount = content == "" ? voidDataCount + 1 : voidDataCount
                        }
                    }
                }
            }
            
            let totalLessonContentCount = lessonContentOverviewCount + lessonContentStageCount
            
            if (totalLessonContentCount > 0) {
                progress = 1.0 - Double(voidDataCount) * (1.0 / Double(totalLessonContentCount))
            }
        }
        
        DispatchQueue.main.async(execute: {
            self.progressView.progress = Float(progress)
        })
    }

}
