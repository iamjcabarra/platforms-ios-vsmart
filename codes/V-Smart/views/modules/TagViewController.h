//
//  FilterViewController.h
//  V-Smart
//
//  Created by VhaL on 2/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NTag.h"
#import "NoteDataManager.h"
#import "TagCell.h"

@interface TagViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SWTableViewCellDelegate, UITextFieldDelegate>

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIButton *clearButton;
@property (nonatomic, strong) id delegate;
@property (nonatomic, strong) id parent;
@property (nonatomic, strong) NSMutableArray *tagArray;
@property (nonatomic, strong) IBOutlet UITextField *textField;
@property (nonatomic, strong) Tag *selectedTag;

-(void)loadData;
@end
