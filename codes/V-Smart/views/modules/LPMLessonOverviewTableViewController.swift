//
//  LPMLessonOverviewTableViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 24/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMLessonOverviewTableViewController: UITableViewController, UITextFieldDelegate, LPMDatePickerPopoverDelegate, LPMLessonQuarterPopoverDelegate, LPMCurriculumPlannerPopoverDelegate, LPMCurriculumPeriodPopoverDelegate {
    
    @IBOutlet fileprivate var titleLabel: UILabel!
    @IBOutlet fileprivate var unitLabel: UILabel!
    @IBOutlet fileprivate var quarterLabel: UILabel!
    @IBOutlet fileprivate var timeFrameLabel: UILabel!
    @IBOutlet fileprivate var associationLabel: UILabel!
    @IBOutlet fileprivate var curriculumLabel: UILabel!
    @IBOutlet fileprivate var curriculumPeriodLabel: UILabel!
    
    @IBOutlet fileprivate var titleTextField: UITextField!
    @IBOutlet fileprivate var unitTextField: UITextField!
    @IBOutlet fileprivate var quarterTextField: UITextField!
    @IBOutlet fileprivate var startDateTextField: UITextField!
    @IBOutlet fileprivate var endDateTextField: UITextField!
    @IBOutlet fileprivate var curriculumTextField: UITextField!
    @IBOutlet fileprivate var curriculumPeriodTextField: UITextField!

    @IBOutlet fileprivate var quarterButton: UIButton!
    @IBOutlet fileprivate var startDateButton: UIButton!
    @IBOutlet fileprivate var endDateButton: UIButton!
    @IBOutlet fileprivate var curriculumButton: UIButton!
    @IBOutlet fileprivate var curriculumPeriodButton: UIButton!
    
    @IBOutlet fileprivate var curriculumPeriodView: UIView!
    @IBOutlet fileprivate var competencyView: UIView!
    
    fileprivate var datePickerView: LPMDatePickerPopover!
    fileprivate var quarterPopover: LPMLessonQuarterPopover!
    fileprivate var curriculumPlannerPopover: LPMCurriculumPlannerPopover!
    fileprivate var curriculumPeriodPopover: LPMCurriculumPeriodPopover!
    fileprivate var timeFrameButtonTag = LPMLessonOverviewTimeFrame.startDate.rawValue
    
    fileprivate var lessonID = ""
    fileprivate var courseID = ""
    fileprivate var curriculumID = ""
    fileprivate var template = ""
    
    // MARK: - Data Managers
    
    fileprivate lazy var lpmDataManager: LPMDataManager = {
        let lpdm = LPMDataManager.sharedInstance
        return lpdm
    }()
    
    fileprivate lazy var cpmDataManager: CPMDataManager = {
        let cpdm = CPMDataManager.sharedInstance
        return cpdm
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.template = self.lpmDataManager.stringValue(self.lpmDataManager.fetchUserDefaultsObject(forKey: "LPM_USER_DEFAULT_TEMPLATE_ID"))
        
        self.titleTextField.delegate = self
        self.unitTextField.delegate = self
        
        let lessonQuarterPopoverAction = #selector(self.showLessonQuarterPopover(_:))
        self.quarterButton.addTarget(self, action: lessonQuarterPopoverAction, for: .touchUpInside)
        
        let curriculumPlannerPopoverAction = #selector(self.showCurriculumPlannerPopover(_:))
        self.curriculumButton.addTarget(self, action: curriculumPlannerPopoverAction, for: .touchUpInside)
        
        let curriculumPeriodPopoverAction = #selector(self.showCurriculumPeriodPopover(_:))
        self.curriculumPeriodButton.addTarget(self, action: curriculumPeriodPopoverAction, for: .touchUpInside)
        
        self.localizeStrings()
        self.setupDatePickerView()
        self.setupDefaultData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - String Localization
    
    fileprivate func localizeStrings() {
        self.titleLabel.text = NSLocalizedString("Title", comment: "")
        self.unitLabel.text = NSLocalizedString("Unit", comment: "")
        self.quarterLabel.text = NSLocalizedString("Quarter", comment: "")
        self.timeFrameLabel.text = NSLocalizedString("Time Frame", comment: "")
        self.associationLabel.text = NSLocalizedString("Associate Lesson Plan to Curriculum", comment: "")
        self.curriculumLabel.text = NSLocalizedString("Curriculum Planner", comment: "")
        self.curriculumPeriodLabel.text = NSLocalizedString("Period", comment: "")
    }
    
    // MARK: - Setup Default Data
    
    fileprivate func setupDefaultData() {
        guard let overview = self.lpmDataManager.fetchLessonOverview() else {
            print("can't parse lesson plan overview...")
            return
        }

        guard
            let lp_id = overview.value(forKey: "lp_id") as? String,
            let course_id = overview.value(forKey: "course_id") as? String,
            let curriculum_id = overview.value(forKey: "curriculum_id") as? String,
            let curriculum_title = overview.value(forKey: "curriculum_title") as? String,
            let curriculum_period_id = overview.value(forKey: "curriculum_period_id") as? String,
            let curriculum_period_name = overview.value(forKey: "curriculum_period_name") as? String,
            let name = overview.value(forKey: "name") as? String,
            let unit = overview.value(forKey: "unit") as? String,
            let quarter = overview.value(forKey: "quarter") as? String,
            let start_date = overview.value(forKey: "start_date") as? String,
            let end_date = overview.value(forKey: "end_date") as? String
        else {
            print("can't render lesson plan overview...")
            return
        }
        
        self.titleTextField.text = name
        self.unitTextField.text = unit
        self.quarterTextField.text = quarter == "" ? "1" : quarter
        self.startDateTextField.text = start_date
        self.endDateTextField.text = end_date
        
        let text = NSLocalizedString("Please Select", comment: "")
        
        let no_selected_curriculum = curriculum_id == "" ? true : false
        self.curriculumTextField.text = no_selected_curriculum ? text : curriculum_title
        
        let no_selected_curriculum_period = curriculum_period_id == "" ? true : false
        self.curriculumPeriodTextField.text = no_selected_curriculum_period ? text : curriculum_period_name
        
        self.curriculumPeriodView.isHidden = no_selected_curriculum
        self.competencyView.isHidden = no_selected_curriculum_period
        
        self.lessonID = lp_id
        self.courseID = course_id
        self.curriculumID = curriculum_id
        
        self.emphasizeRequiredFields()
    }
    
    // MARK: - Emphasize Required Fields
    
    fileprivate func emphasizeRequiredFields() {
        let validColor = UIColor.lightGray.cgColor
        let emptyColor = UIColor.red.cgColor
        
        if self.titleTextField.text == "" {
            self.titleTextField.layer.borderColor = emptyColor
            self.titleTextField.layer.borderWidth = 1.0
        }
        else {
            self.titleTextField.layer.borderColor = validColor
            self.titleTextField.layer.borderWidth = 0
        }
        
        if self.quarterTextField.text == "" {
            self.quarterTextField.layer.borderColor = emptyColor
            self.quarterTextField.layer.borderWidth = 1.0
        }
        else {
            self.quarterTextField.layer.borderColor = validColor
            self.quarterTextField.layer.borderWidth = 0
        }
        
        if self.startDateTextField.text == "" {
            self.startDateTextField.layer.borderColor = emptyColor
            self.startDateTextField.layer.borderWidth = 1.0
        }
        else {
            self.startDateTextField.layer.borderColor = validColor
            self.startDateTextField.layer.borderWidth = 0
        }
        
        if self.endDateTextField.text == "" {
            self.endDateTextField.layer.borderColor = emptyColor
            self.endDateTextField.layer.borderWidth = 1.0
        }
        else {
            self.endDateTextField.layer.borderColor = validColor
            self.endDateTextField.layer.borderWidth = 0
        }
    }
    
    // MARK: - Text Field Key Creator
    
    fileprivate func keyForTextField(_ textField: UITextField) -> String {
        var key = "name"
        
        switch textField {
        case self.titleTextField:
            key = "name"
            break
        case self.unitTextField:
            key = "unit"
            break
        case self.quarterTextField:
            key = "quarter"
            break
        case self.startDateTextField:
            key = "start_date"
            break
        case self.endDateTextField:
            key = "end_date"
            break
        default:
            key = ""
        }
        
        return key
    }
    
    // MARK: - Text Field Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
        
        // Maximum of 50 characters only for lesson title
        if (textField == self.titleTextField) {
            if (newText.length > 50) {
                return false
            }
        }
        
        // Only numeric data for unit
        if (textField == self.unitTextField) {
            let numberSet = CharacterSet.decimalDigits
            if ((string as NSString).rangeOfCharacter(from: numberSet.inverted).location != NSNotFound) {
                return false
            }
            
            if (newText.length > 2) {
                return false
            }
        }
        
        // Handle backspace event
        if ((string as NSString).length == 0) {
            let key = self.keyForTextField(textField)
            let data = [key: newText]
            self.saveChangedData(data)
        }
        
        // Saving local changes
        let key = self.keyForTextField(textField)
        let data = [key: newText]
        self.saveChangedData(data)
        
        return true
    }
    
    // MARK: - Lesson Date Picker
    
    func setupDatePickerView() {
        self.startDateButton.tag = LPMLessonOverviewTimeFrame.startDate.rawValue
        self.endDateButton.tag = LPMLessonOverviewTimeFrame.endDate.rawValue
        
        let datePickerViewAction = #selector(self.showDatePickerView(_:))
        self.startDateButton.addTarget(self, action: datePickerViewAction, for: .touchUpInside)
        self.endDateButton.addTarget(self, action: datePickerViewAction, for: .touchUpInside)
    }
    
    func showDatePickerView(_ sender: UIButton) {
        self.timeFrameButtonTag = sender.tag
        
        self.datePickerView = LPMDatePickerPopover.init(nibName: "LPMDatePickerPopover", bundle: nil)
        self.datePickerView.headerTitle = ""
        self.datePickerView.datePickerMode = UIDatePickerMode.dateAndTime
        self.datePickerView.dateFormat = LPMConstants.DateFormat.LESSON_CREATOR
        self.datePickerView.delegate = self
        
        self.datePickerView.modalPresentationStyle = .popover
        self.datePickerView.preferredContentSize = CGSize(width: 300.0, height: 200.0)
        self.datePickerView.popoverPresentationController?.permittedArrowDirections = .left
        self.datePickerView.popoverPresentationController?.sourceView = sender
        self.datePickerView.popoverPresentationController?.sourceRect = sender.bounds
        self.present(self.datePickerView, animated: true, completion: nil)
    }
    
    func selectedDateString(_ dateString: String) {
        
        if self.template == LPMConstants.Template.SMD {
            var textField = self.startDateTextField
            
            // Start date
            if (self.timeFrameButtonTag == LPMLessonOverviewTimeFrame.startDate.rawValue) {
                self.startDateTextField.text = dateString
                textField = self.startDateTextField
            }
            
            // End date
            if (self.timeFrameButtonTag == LPMLessonOverviewTimeFrame.endDate.rawValue) {
                self.endDateTextField.text = dateString
                textField = self.endDateTextField
            }
            
            // Saving local changes
            let key = self.keyForTextField(textField!)
            let data = [key: (dateString as NSString)]
            self.saveChangedData(data)
        }
        
        if self.template == LPMConstants.Template.DLL {
            let date = self.dateString(dateString, format: LPMConstants.DateFormat.LESSON_CREATOR)
            let isMonday = self.isDateMonday(date)
            let isFriday = self.isDateFriday(date)
            
            // Start date
            if (self.timeFrameButtonTag == LPMLessonOverviewTimeFrame.startDate.rawValue) {
                if isMonday {
                    self.startDateTextField.text = dateString
                    let key1 = self.keyForTextField(self.startDateTextField)
                    let data1 = [key1: (dateString as NSString)]
                    self.saveChangedData(data1)
                    
                    let endDate = self.addDays(4, toDate: date)
                    let formattedEndDate = self.stringDate(endDate, format: LPMConstants.DateFormat.LESSON_CREATOR, isServerTimeZone: false)
                    
                    self.endDateTextField.text = formattedEndDate
                    let key2 = self.keyForTextField(self.endDateTextField)
                    let data2 = [key2: (formattedEndDate as NSString)]
                    self.saveChangedData(data2)
                }
                else {
                    let message = NSLocalizedString("Start date should fall on Monday.", comment: "")
                    self.view.makeToast(message: message, duration: 3.0, position: "Center" as AnyObject)
                }
            }
            
            // End date
            if (self.timeFrameButtonTag == LPMLessonOverviewTimeFrame.endDate.rawValue) {
                if isFriday {
                    self.endDateTextField.text = dateString
                    let key1 = self.keyForTextField(self.endDateTextField)
                    let data1 = [key1: (dateString as NSString)]
                    self.saveChangedData(data1)
                    
                    let startDate = self.addDays(-4, toDate: date)
                    let formattedStartDate = self.stringDate(startDate, format: LPMConstants.DateFormat.LESSON_CREATOR, isServerTimeZone: false)
                    
                    self.startDateTextField.text = formattedStartDate
                    let key2 = self.keyForTextField(self.startDateTextField)
                    let data2 = [key2: (formattedStartDate as NSString)]
                    self.saveChangedData(data2)
                }
                else {
                    let message = NSLocalizedString("End date should fall on Friday.", comment: "")
                    self.view.makeToast(message: message, duration: 3.0, position: "Center" as AnyObject)
                }
            }
        }
    }
    
    // MARK: - Lesson Quarter Picker
    
    func showLessonQuarterPopover(_ sender: UIButton) {
        self.quarterPopover = LPMLessonQuarterPopover(nibName: "LPMLessonQuarterPopover", bundle: nil)
        self.quarterPopover.delegate = self
        
        self.quarterPopover.modalPresentationStyle = .popover
        self.quarterPopover.preferredContentSize = CGSize(width: 160.0, height: 132.0)
        self.quarterPopover.popoverPresentationController?.permittedArrowDirections = .right
        self.quarterPopover.popoverPresentationController?.sourceView = sender
        self.quarterPopover.popoverPresentationController?.sourceRect = sender.bounds
        self.present(self.quarterPopover, animated: true, completion: nil)
    }
    
    func selectedLessonQuarter(_ quarter: String) {
        self.quarterTextField.text = quarter
        let key = self.keyForTextField(self.quarterTextField)
        self.saveChangedData([key: quarter as NSString])
    }
    
    // MARK: - Curriculum Planner Picker
    
    func showCurriculumPlannerPopover(_ sender: UIButton) {
        self.curriculumPlannerPopover = LPMCurriculumPlannerPopover(nibName: "LPMCurriculumPlannerPopover", bundle: nil)
        self.curriculumPlannerPopover.courseID = courseID
        self.curriculumPlannerPopover.delegate = self
        
        self.curriculumPlannerPopover.modalPresentationStyle = .popover
        self.curriculumPlannerPopover.preferredContentSize = CGSize(width: 500.0, height: 150.0)
        self.curriculumPlannerPopover.popoverPresentationController?.permittedArrowDirections = .right
        self.curriculumPlannerPopover.popoverPresentationController?.sourceView = sender
        self.curriculumPlannerPopover.popoverPresentationController?.sourceRect = sender.bounds
        self.present(self.curriculumPlannerPopover, animated: true, completion: nil)
    }
    
    func selectedCurriculumObject(_ object: NSDictionary) {
        self.curriculumPeriodView.isHidden = true
        self.competencyView.isHidden = true
        
        let id = object.value(forKey: "id") as! String
        let title = object.value(forKey: "title") as! String
        
        let shouldHide = id == "" ? true : false
        self.curriculumPeriodView.isHidden = shouldHide
        
        let defaultText = NSLocalizedString("Please Select", comment: "")
        self.curriculumTextField.text = shouldHide ? defaultText : title
        self.curriculumPeriodTextField.text = defaultText
        
        let data = ["curriculum_id": id, "curriculum_title": title]
        self.saveChangedData(data as [String : NSString])
        self.curriculumID = id
    }
    
    // MARK: - Curriculum Period Picker
    
    func showCurriculumPeriodPopover(_ sender: UIButton) {
        self.curriculumPeriodPopover = LPMCurriculumPeriodPopover(nibName: "LPMCurriculumPeriodPopover", bundle: nil)
        self.curriculumPeriodPopover.curriculumID = self.curriculumID
        self.curriculumPeriodPopover.delegate = self
        
        self.curriculumPeriodPopover.modalPresentationStyle = .popover
        self.curriculumPeriodPopover.preferredContentSize = CGSize(width: 500.0, height: 150.0)
        self.curriculumPeriodPopover.popoverPresentationController?.permittedArrowDirections = .right
        self.curriculumPeriodPopover.popoverPresentationController?.sourceView = sender
        self.curriculumPeriodPopover.popoverPresentationController?.sourceRect = sender.bounds
        self.present(self.curriculumPeriodPopover, animated: true, completion: nil)
    }
    
    func selectedCurriculumPeriodObject(_ object: NSDictionary) {
        let id = object.value(forKey: "id") as! Int
        let name = object.value(forKey: "name") as! String
        
        let shouldHide = id == 0 ? true : false
        let defaultText = NSLocalizedString("Please Select", comment: "")
        self.curriculumPeriodTextField.text = shouldHide ? defaultText : name
    
        if (!shouldHide) {
            self.view.makeToastActivity()
            self.view.isUserInteractionEnabled = false
            
            self.cpmDataManager.requestLearningCompetencyListForPeriodWithID("\(id)", completionHandler: { (success) in
                var okay = success
                
                if okay {
                    if let competencies = self.lpmDataManager.fetchPreAssociatedLearningCompetencies() {
                        okay = self.cpmDataManager.antiJoinPreAssociatedLearningCompetencies(competencies)
                    }
                }
                
                DispatchQueue.main.async(execute: {
                    self.view.hideToastActivity()
                    self.view.isUserInteractionEnabled = true
                    self.competencyView.isHidden = !okay
                })
            })
        }
        
        let data = ["curriculum_period_id": "\(id)", "curriculum_period_name": name]
        self.saveChangedData(data as [String : NSString])
    }
    
    // MARK: - Saving Local Changes
    
    fileprivate func saveChangedData(_ data: [String: NSString]) {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            let predicate = NSComparisonPredicate(keyPath: "lp_id", withValue: self.lessonID, isExact: true)
            _ = self.lpmDataManager.updateEntity(LPMConstants.Entity.LESSON_OVERVIEW, predicate: predicate, data: data as NSDictionary)
            self.emphasizeRequiredFields()
        }
    }
    
    // MARK: Date-Related Utility Methods
    
    fileprivate func dateString(_ string: String, format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone.local
        guard let date = dateFormatter.date(from: string) else { return Date() }
        return date
    }
    
    fileprivate func isDateMonday(_ date: Date) -> Bool {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let components = (calendar as NSCalendar).components([.weekday], from: date)
        return components.weekday == 2 ? true : false
    }
    
    fileprivate func addDays(_ days: Int, toDate: Date) -> Date {
        guard let date = (Calendar.current as NSCalendar).date(byAdding: .day, value: days, to: toDate, options: []) else {
            return Date()
        }
        
        return date
    }
    
    fileprivate func isDateFriday(_ date: Date) -> Bool {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let components = (calendar as NSCalendar).components([.weekday], from: date)
        return components.weekday == 6 ? true : false
    }
    
    fileprivate func stringDate(_ date: Date, format: String, isServerTimeZone: Bool) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = NSTimeZone.local
        if isServerTimeZone { formatter.timeZone = TimeZone(identifier: "UTC") }
        return formatter.string(from: date)
    }
    
}
