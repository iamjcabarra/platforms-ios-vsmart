//
//  UBDContentOverviewViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/24/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "UBDContentOverviewViewController.h"
#import "QuarterOptionPopOverViewController.h"
#import "DatePickerViewController.h"
#import "CurriculumPeriodViewController.h"
#import "UBDContentManager.h"
#import "CurriculumViewController.h"
#import "LearningCompetencyTableViewCell.h"
#import "LessonPlanDataManager.h"

@interface UBDContentOverviewViewController () <CurriculumViewDelegate, CurriculumPeriodViewDelegate, UITabBarDelegate, UITableViewDataSource, UITextViewDelegate>

@property (nonatomic, strong) UBDContentManager *sharedUBDContentManager;
@property (nonatomic, strong) LessonPlanDataManager *lm;
@property (nonatomic, strong) DatePickerViewController *datePickerView;
@property (nonatomic, strong) CurriculumPeriodViewController *cpView;

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, retain) UIPopoverController *actionPopOverController;
@property (nonatomic, retain) UIPopoverController *datePickerPopOverController;
@property (nonatomic, retain) UIPopoverController *cpPopOverController;

@property (strong, nonatomic) IBOutlet UITableView *compentencyTableView;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblSchoolName;
@property (strong, nonatomic) IBOutlet UILabel *lblSchoolAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblUnit;
@property (strong, nonatomic) IBOutlet UILabel *lblQuarter;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeFrame;
@property (strong, nonatomic) IBOutlet UILabel *lblCurriculumPlanner;
@property (strong, nonatomic) IBOutlet UILabel *lblSelectedCurriculum;
@property (strong, nonatomic) IBOutlet UILabel *lblCurriculumQuarter;
@property (strong, nonatomic) IBOutlet UILabel *lblLearningCompetencies;
@property (strong, nonatomic) IBOutlet UILabel *lblLCCode;
@property (strong, nonatomic) IBOutlet UILabel *lblLCTitle;

@property (strong, nonatomic) IBOutlet UITextField *txtTitle;
@property (strong, nonatomic) IBOutlet UITextField *txtSchoolName;
@property (strong, nonatomic) IBOutlet UITextView *txtSchoolAddress;
@property (strong, nonatomic) IBOutlet UITextField *txtUnit;
@property (strong, nonatomic) IBOutlet UITextField *txtQuarter;
@property (strong, nonatomic) IBOutlet UITextField *txtStart;
@property (strong, nonatomic) IBOutlet UITextField *txtEnd;
@property (strong, nonatomic) IBOutlet UITextField *txtCurriculumQuarter;

@property (strong, nonatomic) IBOutlet UIButton *butQuarterOption;
@property (strong, nonatomic) IBOutlet UIButton *butStart;
@property (strong, nonatomic) IBOutlet UIButton *butEnd;
@property (strong, nonatomic) IBOutlet UIButton *butChooseCurriculum;
@property (strong, nonatomic) IBOutlet UIButton *butCurriculumPeriodOption;

@property (strong, nonatomic) NSArray *compentencyList;

@end

@implementation UBDContentOverviewViewController

NSArray *curriculumPeriodList;
NSInteger timeFrameButtonTag;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Use singleton class shared instance
    self.sharedUBDContentManager = [UBDContentManager sharedInstance];
    
    // Set data manager and context
    self.lm = [LessonPlanDataManager sharedInstance];
    self.managedObjectContext = self.lm.mainContext;
    
    // To dismiss keyboard when touching outside of text fields
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    // Set delegate
    self.txtSchoolAddress.delegate = self;
    
    // Add a "textFieldDidChange" notification method to the text field control
    [self.txtTitle addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    
    [self.txtSchoolName addTarget:self
                      action:@selector(textFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];
    
    [self.txtUnit addTarget:self
                           action:@selector(textFieldDidChange:)
                 forControlEvents:UIControlEventEditingChanged];
    
    [self.txtQuarter addTarget:self
                           action:@selector(textFieldDidChange:)
                 forControlEvents:UIControlEventEditingChanged];
    
    [self.txtStart addTarget:self
                           action:@selector(textFieldDidChange:)
                 forControlEvents:UIControlEventEditingChanged];
    
    [self.txtEnd addTarget:self
                           action:@selector(textFieldDidChange:)
                 forControlEvents:UIControlEventEditingChanged];
    
    // Implement action button
    [self.butQuarterOption addTarget:self
                              action:@selector(showPopOverQuarterOption:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self.butStart addTarget:self
                      action:@selector(showPopOverDatePicker:)
            forControlEvents:UIControlEventTouchUpInside];
    
    [self.butEnd addTarget:self
                    action:@selector(showPopOverDatePicker:)
          forControlEvents:UIControlEventTouchUpInside];
    
    [self.butChooseCurriculum addTarget:self
                                 action:@selector(showCurriculumList:)
                       forControlEvents:UIControlEventTouchUpInside];
    
    [self.butCurriculumPeriodOption addTarget:self
                                 action:@selector(showPopOverCurriculumPeriod:)
                       forControlEvents:UIControlEventTouchUpInside];
    
    curriculumPeriodList = [NSArray array];
    self.lblSelectedCurriculum.text = @"No chosen curriculum";
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self updateContentManager];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Get data from singleton class and render them to text fields
    self.txtTitle.text = [self.self.sharedUBDContentManager.contentOverview objectForKey:@"title"];
    self.txtSchoolName.text = [self.sharedUBDContentManager.contentOverview objectForKey:@"schoolName"];
    self.txtSchoolAddress.text = [self.sharedUBDContentManager.contentOverview objectForKey:@"schoolAddress"];
    self.txtUnit.text = [self.sharedUBDContentManager.contentOverview objectForKey:@"unit"];
    self.txtQuarter.text = [self.sharedUBDContentManager.contentOverview objectForKey:@"quarter"];
    self.txtStart.text = [self.sharedUBDContentManager.contentOverview objectForKey:@"start"];
    self.txtEnd.text = [self.sharedUBDContentManager.contentOverview objectForKey:@"end"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Handle Keyboard

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Pop Over Methods

- (void)showPopOverQuarterOption:(id)sender {
    // Instantiate pop over view controller class
    QuarterOptionPopOverViewController *quarterOption = [[QuarterOptionPopOverViewController alloc]initWithNibName:@"QuarterOptionPopOverViewController" bundle:nil];
    
    // Implement pop over view controller
    UIButton *button = (UIButton *)sender;
    
    self.actionPopOverController = [[UIPopoverController alloc] initWithContentViewController:quarterOption];
    self.actionPopOverController.popoverContentSize = CGSizeMake(160.0, 140.0);
    
    [self.actionPopOverController presentPopoverFromRect:button.bounds
                                                  inView:button
                                permittedArrowDirections:UIPopoverArrowDirectionDown
                                                animated:YES];
    // Implement action buttons
    [quarterOption.butOption1 addTarget:self
                                 action:@selector(optionAction:)
                       forControlEvents:UIControlEventTouchUpInside];
    
    [quarterOption.butOption2 addTarget:self
                                 action:@selector(optionAction:)
                       forControlEvents:UIControlEventTouchUpInside];
    
    [quarterOption.butOption3 addTarget:self
                                 action:@selector(optionAction:)
                       forControlEvents:UIControlEventTouchUpInside];
    
    [quarterOption.butOption4 addTarget:self
                                 action:@selector(optionAction:)
                       forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)showPopOverDatePicker:(id)sender {
    // Instantiate pop over view controller class
     self.datePickerView = [[DatePickerViewController alloc]initWithNibName:@"DatePickerViewController" bundle:nil];
    
    // Implement pop over view controller
    UIButton *button = (UIButton *)sender;
    timeFrameButtonTag = button.tag;
    
    self.datePickerPopOverController = [[UIPopoverController alloc] initWithContentViewController:self.datePickerView];
    self.datePickerPopOverController.popoverContentSize = CGSizeMake(325.0, 200.0);
    
    [self.datePickerPopOverController presentPopoverFromRect:button.bounds
                                                  inView:button
                                permittedArrowDirections:UIPopoverArrowDirectionDown
                                                animated:YES];
    // Implement action buttons
    [self.datePickerView.datePicker addTarget:self
                                       action:@selector(taskDatePicked:)
                             forControlEvents:UIControlEventValueChanged];
}

- (void)showPopOverCurriculumPeriod:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    if (curriculumPeriodList.count > 0) {
        self.cpView = [[CurriculumPeriodViewController alloc] initWithNibName:@"CurriculumPeriodViewController" bundle:nil];
        self.cpView.delegate = self;
        
        self.cpView.curriculumPeriodList = curriculumPeriodList;
        
        self.cpPopOverController = [[UIPopoverController alloc] initWithContentViewController:self.cpView];
        self.cpPopOverController.popoverContentSize = CGSizeMake(250.0, 200.0);
        
        [self.cpPopOverController presentPopoverFromRect:button.bounds
                                                  inView:button
                                permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

#pragma mark - Render Lesson Quarter

- (void)optionAction:(id)sender {
    UIButton *button = (UIButton *)sender;
    NSInteger tag = button.tag;
    NSString *option;
    
    switch (tag) {
        case 0:
            option = @"1";
            break;
        case 1:
            option = @"2";
            break;
        case 2:
            option = @"3";
            break;
        case 3:
            option = @"4";
            break;
        default:
            break;
    }
    
    self.txtQuarter.text = option;
    [self updateContentManager];
    
    [self.actionPopOverController dismissPopoverAnimated:YES];
    [self.actionPopOverController.delegate popoverControllerDidDismissPopover:self.actionPopOverController];
}

#pragma mark - Render Chosen Date

- (void)taskDatePicked:(id)sender {
    NSInteger textFieldTag = timeFrameButtonTag * 10;
    UITextField *textField = (UITextField *)[self.view viewWithTag:textFieldTag];
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    
    dateformatter.dateStyle = NSDateFormatterMediumStyle;
    dateformatter.dateFormat = @"YYYY-MM-dd";
    textField.text = [dateformatter stringFromDate:[self.datePickerView.datePicker date]];
    
    [self updateContentManager];
}

#pragma mark - List Curriculum

- (void)showCurriculumList:(id)sender {
    __weak typeof(self) wo = self;
    [self.lm requestApprovedCurriculumList:^(BOOL status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo performSegueWithIdentifier:@"showCurriculumList" sender:nil];
        });
    }];
}

#pragma mark - Update Content Manager

- (void)updateContentManager {
    NSString *title = self.txtTitle.text;
    NSString *schoolName = self.txtSchoolName.text;
    NSString *schoolAddress = self.txtSchoolAddress.text;
    NSString *unit = self.txtUnit.text;
    NSString *quarter = self.txtQuarter.text;
    NSString *start = self.txtStart.text;
    NSString *end = self.txtEnd.text;
    
    // Set data to singleton class collection property
    [self.sharedUBDContentManager.contentOverview setObject:title forKey:@"title"];
    [self.sharedUBDContentManager.contentOverview setObject:schoolName forKey:@"schoolName"];
    [self.sharedUBDContentManager.contentOverview setObject:schoolAddress forKey:@"schoolAddress"];
    [self.sharedUBDContentManager.contentOverview setObject:unit forKey:@"unit"];
    [self.sharedUBDContentManager.contentOverview setObject:quarter forKey:@"quarter"];
    [self.sharedUBDContentManager.contentOverview setObject:start forKey:@"start"];
    [self.sharedUBDContentManager.contentOverview setObject:end forKey:@"end"];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.compentencyList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *competency_mo = (NSManagedObject *)self.compentencyList[indexPath.row];
    
    NSString *lcTitle = [competency_mo valueForKey:@"domain"];
    NSString *lcContentID = [competency_mo valueForKey:@"content_id"];
    NSString *lcCode = [self getLearningCompetencyCodeWithID:lcContentID managedObject:competency_mo];
    
    LearningCompetencyTableViewCell *lcCell = [tableView dequeueReusableCellWithIdentifier:@"competencyCellIdentifier" forIndexPath:indexPath];
    
    lcCell.lblLCCode.text = lcCode;
    lcCell.lblLCTitle.text = lcTitle;
    lcCell.butCheckBox.imageView.image = [UIImage imageNamed:@"checkbox_unable32x32.png"];
    
    return lcCell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *object = (NSManagedObject *)self.compentencyList[indexPath.row];
    NSManagedObjectContext *ctx = object.managedObjectContext;
    [object setValue:@"1" forKey:@"selected"];
    
    NSError *error = nil;
    
    // Save the object to persistent store
    if (![ctx save:&error]) {
        NSLog(@"Can't update selected! %@ %@", error, [error localizedDescription]);
    }
    
    LearningCompetencyTableViewCell *lcCell = (LearningCompetencyTableViewCell *)[self.compentencyTableView cellForRowAtIndexPath:indexPath];
    lcCell.butCheckBox.imageView.image = [UIImage imageNamed:@"checkbox32x32.png"];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *object = (NSManagedObject *)self.compentencyList[indexPath.row];
    NSManagedObjectContext *ctx = object.managedObjectContext;
    [object setValue:@"0" forKey:@"selected"];
    
    NSError *error = nil;
    
    // Save the object to persistent store
    if (![ctx save:&error]) {
        NSLog(@"Can't update selected! %@ %@", error, [error localizedDescription]);
    }

    LearningCompetencyTableViewCell *lcCell = (LearningCompetencyTableViewCell *)[self.compentencyTableView cellForRowAtIndexPath:indexPath];
    lcCell.butCheckBox.imageView.image = [UIImage imageNamed:@"checkbox_unable32x32.png"];
}

- (NSString *)getLearningCompetencyCodeWithID:(NSString *)cid managedObject:(NSManagedObject *)mo {
    NSString *lcCode = @"";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"learning_competency.content_id == %@ AND content_column_name = %@", cid, @"Code"];
    NSArray *result = [NSArray array];
    
    result = [self.lm getObjectsForEntity:kLearningCompetencyContentEntity predicate:predicate context:mo.managedObjectContext];

    if (result.count > 0) {
        NSManagedObject *object = (NSManagedObject *)[result objectAtIndex:0];
        lcCode = [object valueForKey:@"content_column_value"];
    }

    return lcCode;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCurriculumList"]) {
        CurriculumViewController *curriculum = (CurriculumViewController *)[segue destinationViewController];
        curriculum.delegate = self;
    }
}

#pragma mark - CurriculumView Delegate

- (void)moSelected:(NSManagedObject *)mo {
    self.txtCurriculumQuarter.text = @"";
    self.compentencyList = nil;
    
    [self.compentencyTableView reloadData];
    
    if (mo != nil) {
        NSString *curriculum = [mo valueForKey:@"curriculum_title"];
        NSString *curriculum_id = [mo valueForKey:@"curriculum_id"];
        
        self.lblSelectedCurriculum.text = curriculum;
        NSLog(@"curriculum id: %@", curriculum_id);
        
        [self.lm requestPeriodsForCurriculum:mo doneBlock:^(BOOL status) {
            // Do nothing
        }];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"curriculum.curriculum_id == %@", curriculum_id];
        NSArray *result = [NSArray array];
        result = [self.lm getObjectsForEntity:kCurriculumPeriodEntity predicate:predicate context:mo.managedObjectContext];
        
        if (result.count > 0) {
            int i;
            
            NSMutableArray *cplist = [NSMutableArray array];
            
            for (i = 0; i < result.count; i++) {
                NSManagedObject *period = (NSManagedObject *)[result objectAtIndex:i];
                NSString *period_id = [period valueForKey:@"period_id"];
                NSString *period_name = [period valueForKey:@"name"];
                
                NSLog(@"period_id: %@", period_id);
                NSLog(@"period_name: %@", period_name);
                
                NSMutableDictionary *d = [NSMutableDictionary dictionary];
                [d setObject:period_id forKey:@"period_id"];
                [d setObject:period_name forKey:@"name"];
                
                [cplist addObject:d];
            }
            
            curriculumPeriodList = cplist;
            NSLog(@"curriculumPeriodList: %@", curriculumPeriodList);
        }
        else {
            curriculumPeriodList = nil;
        }
    }
}

#pragma mark - CurriculumViewPeriod Delegate

- (void)periodSelected:(NSDictionary *)d {
    if (d != nil) {
        NSString *cpid = d[@"period_id"];
        NSString *name = d[@"name"];
        
        self.txtCurriculumQuarter.text = name;
        
        NSPredicate *predicate = [self.lm predicateForKeyPath:@"period_id" andValue:cpid];
        NSManagedObject *periodObject = [self.lm getEntity:kCurriculumPeriodEntity withPredicate:predicate];
        
        if (periodObject != nil) {
            __weak typeof(self) wo = self;
            [self.lm requestCurriculumLearningCompetenciesForPeriodWithObject:periodObject dataBlock:^(NSDictionary *data) {
                if (data) {
                    BOOL status = [data[@"status"] boolValue];
                    NSSet *items = (NSSet *)data[@"competencies"];
                    
                    if ((status == YES) && (items.count > 0)) {
                        wo.compentencyList = [NSArray arrayWithArray:[items allObjects]];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [wo.compentencyTableView reloadData];
                        });
                    }
                }
            }];
        }
    }
}

#pragma mark - Text Field Did Change

- (void)textFieldDidChange:(id)sender {
    [self updateContentManager];
}

#pragma mark - Text View Delegate

- (void)textViewDidChange:(UITextView *)textView {
    [self updateContentManager];
}

@end
