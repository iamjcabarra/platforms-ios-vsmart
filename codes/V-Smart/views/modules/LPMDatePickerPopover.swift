//
//  LPMDatePickerPopover.swift
//  V-Smart
//
//  Created by Julius Abarra on 11/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol LPMDatePickerPopoverDelegate: class {
    func selectedDateString(_ dateString: String)
}

class LPMDatePickerPopover: UIViewController {

    @IBOutlet fileprivate var titleLabel: UILabel!
    @IBOutlet fileprivate var datePicker: UIDatePicker!
    @IBOutlet fileprivate var doneButton: UIButton!
    
    weak var delegate:LPMDatePickerPopoverDelegate?
    
    var headerTitle = ""
    var datePickerMode = UIDatePickerMode.date
    var dateFormat = "yyyy-mm-dd"
    
    fileprivate var returningDate: String!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let chageDateAction = #selector(self.changeDateAction(_:))
        self.datePicker.addTarget(self, action: chageDateAction, for: .valueChanged)
        
        let doneString = NSLocalizedString("Done", comment: "")
        self.doneButton.setTitle(doneString, for: UIControlState())
        let doneButtonAction = #selector(self.doneButtonAction(_:))
        self.doneButton.addTarget(self, action: doneButtonAction, for: .touchUpInside)
        
        self.titleLabel.text = self.headerTitle
        self.datePicker.datePickerMode = self.datePickerMode
        self.returningDate = self.formatDate(Date())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Event Handlers
    
    func changeDateAction(_ sender: UIDatePicker) {
        self.returningDate = self.formatDate(self.datePicker.date)
    }
    
    func doneButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.delegate?.selectedDateString(self.returningDate)
        })
    }
    
    // MARK: - Date Formatting
    
    func formatDate(_ date: Date) -> String {
        let formatter = DateFormatter.init()
        formatter.dateFormat = self.dateFormat
        formatter.timeZone = TimeZone.autoupdatingCurrent
        return formatter.string(from: date)
    }

}
