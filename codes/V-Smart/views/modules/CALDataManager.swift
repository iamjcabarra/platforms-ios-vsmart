//
//  CALDataManager.swift
//  V-Smart
//
//  Created by Julius Abarra on 09/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

class CALDataManager: Routable {
    
    var isProductionMode: Bool = true
    
    // MARK: - Singleton
    
    static let sharedInstance: CALDataManager = {
        let singleton = CALDataManager()
        return singleton
    }()
    
    // MARK: - Properties
    
    fileprivate let db = VSCoreDataStack(name: "CALDataModel")
    fileprivate let session = URLSession.shared
    fileprivate let dateHelper = DateHelper()
    fileprivate let userDefaults = UserDefaults()
    
    // MARK: - Closures
    
    typealias CALDoneBlock = (_ doneBlock: Bool) -> Void
    typealias CALDataBlock = (_ dataBlock: [String: Any]?) -> Void
    
    // MARK: - Shared Methods
    
    func mainContext() -> NSManagedObjectContext! {
        return self.db.objectMainContext()
    }
    
    func accountUserID() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.id)"
    }

    // MARK: - Calendar Events API
    
    func requestCalendarEventList(forUser userID: String, doneBlock: @escaping CALDoneBlock) {
        self.prettyFunction()
        
        guard let ctx = self.db.objectContext() else {
            print("Error: Can't create context!")
            doneBlock(false)
            return
        }
        
        let cleared = self.db.clearEntity(CALConstants.Entity.EVENT, ctx: ctx, filter: nil)
        let success = self.createAddEventObject()
        
        if !cleared || !success {
            doneBlock(false)
            return
        }

        let uri = "/v1/events/getuserevents/\(userID)"
        
        guard let request = self.route("GET", uri: uri, body: nil) else {
            print("Error: Can't create request!")
            doneBlock(false)
            return
        }
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if error != nil {
                print("Error: \(error!.localizedDescription)")
                doneBlock(false)
            }
            else {
                guard let d = data, let r = self.parseResponseData(d) as? [String: Any] else {
                    print("Error: Can't parse data!")
                    doneBlock(false)
                    return
                }
                
                if self.okayToParseResponse(r) {
                    guard let content = r["records"] as? [[String: Any]] else {
                        print("Error: Can't parse content!")
                        doneBlock(false)
                        return
                    }
                    
                    ctx.performAndWait {
                        for item in content {
                            let id = self.stringValue(item["id"])!
                            let user_id = self.stringValue(item["user_id"])!
                            let type_id = self.stringValue(item["type_id"])!
                            let announcement_id = self.stringValue(item["announcement_id"])!
                            let title = self.stringValue(item["title"])!
                            let details = self.stringValue(item["details"])!
                            let background_color = self.stringValue(item["background_color"])!
                            let event_start = self.stringValue(item["event_start"])!
                            let event_end = self.stringValue(item["event_end"])!
                            let is_deleted = self.stringValue(item["is_deleted"])!
                            let date_created = self.stringValue(item["date_created"])!
                            let date_modified = self.stringValue(item["date_modified"])!
                            let search_key = "\(title)\(details)"
                            
                            let event_start_string = self.dateHelper.dateString(event_start,
                                                                                fromFormat: "yyyy-MM-dd HH:mm:ss",
                                                                                toFormat: "yyyy-MM-dd",
                                                                                fromTimeZone: "UTC",
                                                                                setToLocalTimeZone: true)
                            
                            let event_end_string = self.dateHelper.dateString(event_end,
                                                                              fromFormat: "yyyy-MM-dd HH:mm:ss",
                                                                              toFormat: "yyyy-MM-dd",
                                                                              fromTimeZone: "UTC",
                                                                              setToLocalTimeZone: true)
                            
                            let event_start_date = self.dateHelper.date(fromString: event_start_string)
                            let event_end_date = self.dateHelper.date(fromString: event_end_string)

                            let predicate = self.db.createPredicate(key:"id", withValue: id, isExact: true)
                            let mo = self.db.retrieveEntity(CALConstants.Entity.EVENT, context: ctx, filter: predicate)
                            
                            mo.setValue(Int(id), forKey: "id")
                            mo.setValue(user_id, forKey: "user_id")
                            mo.setValue(type_id, forKey: "type_id")
                            mo.setValue(announcement_id, forKey: "announcement_id")
                            mo.setValue(title, forKey: "title")
                            mo.setValue(details, forKey: "details")
                            mo.setValue(background_color, forKey: "background_color")
                            mo.setValue(event_start, forKey: "event_start")
                            mo.setValue(event_end, forKey: "event_end")
                            mo.setValue(is_deleted, forKey: "is_deleted")
                            mo.setValue(date_created, forKey: "date_created")
                            mo.setValue(date_modified, forKey: "date_modified")
                            mo.setValue(search_key, forKey: "search_key")
                            mo.setValue(event_start_date, forKey: "event_start_date")
                            mo.setValue(event_end_date, forKey: "event_end_date")
                            mo.setValue("0", forKey: "is_add_event")
                        }
                        
                        let success = self.db.saveObjectContext(ctx)
                        doneBlock(success)
                    }
                }
                else {
                    doneBlock(false)
                }
            }
        })
        
        task.resume()
    }
    
    fileprivate func createAddEventObject() -> Bool {
        var status = false
        
        guard let ctx = self.db.objectContext() else {
            print("Error: Can't create context! Can't create add event object!")
            return status
        }
        
        ctx.performAndWait {
            let predicate = self.db.createPredicate(key:"id", withValue: "0", isExact: true)
            let mo = self.db.retrieveEntity(CALConstants.Entity.EVENT, context: ctx, filter: predicate)
            let title = NSLocalizedString("Add Event", comment: "")
            
            let user_id = self.accountUserID()
            let event_start = self.dateHelper.dateString(fromDate: Date(), outFormat: "yyyy-MM-dd HH:mm:ss", setToLocalTimeZone: true)
            let event_end = self.dateHelper.dateString(fromDate: Date(), outFormat: "yyyy-MM-dd HH:mm:ss", setToLocalTimeZone: true)
            
            mo.setValue(0, forKey: "id")
            mo.setValue(user_id, forKey: "user_id")
            mo.setValue("4", forKey: "type_id")
            mo.setValue("", forKey: "announcement_id")
            mo.setValue(title, forKey: "title")
            mo.setValue("", forKey: "details")
            mo.setValue("#41AFD8", forKey: "background_color")
            mo.setValue(event_start, forKey: "event_start")
            mo.setValue(event_end, forKey: "event_end")
            mo.setValue("0", forKey: "is_deleted")
            mo.setValue("", forKey: "date_created")
            mo.setValue("", forKey: "date_modified")
            mo.setValue("C02MM6N0FH01", forKey: "search_key")
            mo.setValue("1", forKey: "is_add_event")
            
            status = self.db.saveObjectContext(ctx)
        }
        
        return status
    }
    
    func requestDeleteEvent(_ eventID: String, forUser userID: String, doneBlock: @escaping CALDoneBlock) {
        self.prettyFunction()
        
        let uri = "/v1/events/removeevent"
        let body = ["user_id": userID, "event_id": eventID]
        
        guard let request = self.route("POST", uri: uri, body: body) else {
            print("Error: Can't create request!")
            doneBlock(false)
            return
        }
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if error != nil {
                print("Error: \(error!.localizedDescription)")
                doneBlock(false)
            }
            else {
                guard let d = data, let r = self.parseResponseData(d) as? [String: Any] else {
                    print("Error: Can't parse data!")
                    doneBlock(false)
                    return
                }
                
                let success = self.okayToParseResponse(r)
                doneBlock(success)
            }
        })
        
        task.resume()
    }
    
    func requestCreateEvent(withBody body: [String: Any], doneBlock: @escaping CALDoneBlock) {
        self.prettyFunction()
        
        let uri = "/v1/events/postevent"
        
        guard let request = self.route("POST", uri: uri, body: body) else {
            print("Error: Can't create request!")
            doneBlock(false)
            return
        }
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if error != nil {
                print("Error: \(error!.localizedDescription)")
                doneBlock(false)
            }
            else {
                guard let d = data, let r = self.parseResponseData(d) as? [String: Any] else {
                    print("Error: Can't parse data!")
                    doneBlock(false)
                    return
                }
                
                let success = self.okayToParseResponse(r)
                doneBlock(success)
            }
        })
        
        task.resume()
    }
    
    func requestUpdateEvent(_ event: String, withBody body: [String: Any], doneBlock: @escaping CALDoneBlock) {
        self.prettyFunction()
        
        let uri = "/v1/events/putevent/\(event)"
        
        guard let request = self.route("POST", uri: uri, body: body) else {
            print("Error: Can't create request!")
            doneBlock(false)
            return
        }
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if error != nil {
                print("Error: \(error!.localizedDescription)")
                doneBlock(false)
            }
            else {
                guard let d = data, let r = self.parseResponseData(d) as? [String: Any] else {
                    print("Error: Can't parse data!")
                    doneBlock(false)
                    return
                }
                
                let success = self.okayToParseResponse(r)
                doneBlock(success)
            }
        })
        
        task.resume()
    }
    
    func requestGroupList(forUser userID: String, doneBlock: @escaping CALDoneBlock) {
        self.prettyFunction()
        
        guard let ctx = self.db.objectContext() else {
            print("Error: Can't create context!")
            doneBlock(false)
            return
        }
        
        let cleared = self.db.clearEntity(CALConstants.Entity.SECTION, ctx: ctx, filter: nil)
        
        if !cleared {
            doneBlock(false)
            return
        }
        
        let uri = "/v1/stream/getusergroups/\(userID)"
        
        guard let request = self.route("GET", uri: uri, body: nil) else {
            print("Error: Can't create request!")
            doneBlock(false)
            return
        }
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if error != nil {
                print("Error: \(error!.localizedDescription)")
                doneBlock(false)
            }
            else {
                guard let d = data, let r = self.parseResponseData(d) as? [String: Any] else {
                    print("Error: Can't parse data!")
                    doneBlock(false)
                    return
                }
                
                if self.okayToParseResponse(r) {
                    guard let content = r["records"] as? [[String: Any]] else {
                        print("Error: Can't parse content!")
                        doneBlock(false)
                        return
                    }
                    
                    ctx.performAndWait {
                        for item in content {
                            
                            let id = self.stringValue(item["id"])!
                            let user_id = self.stringValue(item["user_id"])!
                            let group_id = self.stringValue(item["group_id"])!
                            let name = self.stringValue(item["name"])!
                            let date_created = self.stringValue(item["date_created"])!
                            let date_modified = self.stringValue(item["date_modified"])!
                            let section = self.stringValue(item["section"])!
                            
                            let predicate = self.db.createPredicate(key:"id", withValue: id, isExact: true)
                            let mo = self.db.retrieveEntity(CALConstants.Entity.SECTION, context: ctx, filter: predicate)
                            
                            mo.setValue(Int(id), forKey: "id")
                            mo.setValue(user_id, forKey: "user_id")
                            mo.setValue(group_id, forKey: "group_id")
                            mo.setValue(name, forKey: "name")
                            mo.setValue(date_created, forKey: "date_created")
                            mo.setValue(date_modified, forKey: "date_modified")
                            mo.setValue(section, forKey: "section")
                            mo.setValue("1", forKey: "selected")
                        }
                        
                        let success = self.db.saveObjectContext(ctx)
                        doneBlock(success)
                    }
                }
                else {
                    doneBlock(false)
                }
            }
        })
        
        task.resume()
    }
    
    func countEvent(forDate date: NSDate) -> Int {
        let dateString = self.dateHelper.dateString(fromDate: date as Date, outFormat: "yyyy-MM-dd HH:mm:ss", setToLocalTimeZone: true)
        let dateActual = self.dateHelper.date(fromString: dateString) as NSDate
        
        let entity = CALConstants.Entity.EVENT
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        let predicate = NSPredicate(format: "(%@ >= event_start_date) AND (%@ <= event_end_date)", dateActual, dateActual)
        fetchRequest.predicate = predicate
        
        guard let ctx = self.db.objectContext() else {
            print("Error: Can't create context!")
            return 0
        }
        
        do {
            let items = try ctx.fetch(fetchRequest)
            return items.count
        }
        catch let error {
            print("Error retrieving data for entity \(entity): \(error)")
        }
        
        return 0
    }
    
    // MARK: - Deep Copy Managed Object
    
    func deepCopyEvent(_ object: NSManagedObject, doneBlock: @escaping CALDoneBlock) {
        self.prettyFunction()
        
        guard let ctx = self.db.objectContext() else {
            print("Error: Can't create context!")
            doneBlock(false)
            return
        }
        
        let cleared = self.db.clearEntity(CALConstants.Entity.EVENT_COPY, ctx: ctx, filter: nil)
        
        if cleared {
            ctx.performAndWait {
                let id = self.stringValue(object.value(forKey: "id"))!
                let user_id = self.stringValue(object.value(forKey: "user_id"))!
                let type_id = self.stringValue(object.value(forKey: "type_id"))!
                let announcement_id = self.stringValue(object.value(forKey: "announcement_id"))!
                let title = self.stringValue(object.value(forKey: "title"))!
                let details = self.stringValue(object.value(forKey: "details"))!
                let background_color = self.stringValue(object.value(forKey: "background_color"))!
                let event_start = self.stringValue(object.value(forKey: "event_start"))!
                let event_end = self.stringValue(object.value(forKey: "event_end"))!
                let is_deleted = self.stringValue(object.value(forKey: "is_deleted"))!
                let date_created = self.stringValue(object.value(forKey: "date_created"))!
                let date_modified = self.stringValue(object.value(forKey: "date_modified"))!
                let search_key = self.stringValue(object.value(forKey: "search_key"))!
                let is_add_event = self.stringValue(object.value(forKey: "is_add_event"))!
                
                let predicate = self.db.createPredicate(key:"id", withValue: id, isExact: true)
                let mo = self.db.retrieveEntity(CALConstants.Entity.EVENT_COPY, context: ctx, filter: predicate)
                
                mo.setValue(Int(id), forKey: "id")
                mo.setValue(user_id, forKey: "user_id")
                mo.setValue(type_id, forKey: "type_id")
                mo.setValue(announcement_id, forKey: "announcement_id")
                mo.setValue(title, forKey: "title")
                mo.setValue(details, forKey: "details")
                mo.setValue(background_color, forKey: "background_color")
                mo.setValue(event_start, forKey: "event_start")
                mo.setValue(event_end, forKey: "event_end")
                mo.setValue(is_deleted, forKey: "is_deleted")
                mo.setValue(date_created, forKey: "date_created")
                mo.setValue(date_modified, forKey: "date_modified")
                mo.setValue(search_key, forKey: "search_key")
                mo.setValue(is_add_event, forKey: "is_add_event")

                let success = self.db.saveObjectContext(ctx)
                doneBlock(success)
            }
        }
        else {
            doneBlock(false)
        }
    }
    
    func fetchDeepCopiedEventObject(withID id: String) -> NSManagedObject? {
        let predicate = self.db.createPredicate(key:"id", withValue: id, isExact: true)
        let mo = self.db.retrieveEntity(CALConstants.Entity.EVENT_COPY, filter: predicate)
        return mo
    }
    
    // MARK: - Update Entity in Core Data
    
    func updateEntity(_ entity: String, predicate: NSPredicate, data: NSDictionary) -> Bool {
        var status = false
        
        guard let object = self.db.retrieveEntity(entity, filter: predicate) else {
            return status
        }
        
        if let ctx = object.managedObjectContext {
            
            ctx.performAndWait {
                let keys = data.allKeys
                
                for k in keys {
                    if let kString = k as? String {
                        object.setValue(data[kString], forKey: kString)
                    }
                }
                
                status = self.db.saveObjectContext(ctx)
            }
        }
        
        return status
    }
    
    func assemblePostBody(forEvent event: String, isCreate create: Bool) -> [String: Any]? {
        guard let copy = self.fetchDeepCopiedEventObject(withID: event) else {
            print("Can't fetch copied event object!")
            return nil
        }
        
        let id = self.stringValue(copy.value(forKey: "id"))!
        let user_id = self.stringValue(copy.value(forKey: "user_id"))!
        let type_id = self.stringValue(copy.value(forKey: "type_id"))!
        let announcement_id = self.stringValue(copy.value(forKey: "announcement_id"))!
        let title = self.stringValue(copy.value(forKey: "title"))!
        let details = self.stringValue(copy.value(forKey: "details"))!
        let background_color = self.stringValue(copy.value(forKey: "background_color"))!
        let event_start = self.stringValue(copy.value(forKey: "event_start"))!
        let event_end = self.stringValue(copy.value(forKey: "event_end"))!
        let is_deleted = self.stringValue(copy.value(forKey: "is_deleted"))!
        let date_created = self.stringValue(copy.value(forKey: "date_created"))!
        let date_modified = self.stringValue(copy.value(forKey: "date_modified"))!
        let post_flag = self.stringValue(copy.value(forKey: "post_flag"))!
        
        var cs_id_list = [String]()
        
        if create {
            if post_flag == "1" || post_flag == "3" {
                if let sections = self.db.retrieveObjects(forEntity: CALConstants.Entity.SECTION) {
                    for section in sections {
                        let cs_id = self.stringValue(section.value(forKey: "id"))!
                        cs_id_list.append(cs_id)
                    }
                }
            }
        }
        
        let teachers_only = post_flag == "2" ? "1" : "0"
        
        let body: [String: Any] = ["id": "0", "event_id": id, "user_id": user_id, "type_id": type_id, "cs_id": cs_id_list, "teachers_only": teachers_only,
                                   "announcement_id": announcement_id, "title": title, "details": details, "first_name": "", "last_name": "",
                                   "background_color": background_color, "event_start": event_start, "event_end": event_end, "is_deleted": is_deleted,
                                   "date_created": date_created, "date_modified": date_modified]
        return body
   
    }
    
    // MARK: - User Defaults Methods
    
    func saveUserDefault(object: Any!, forKey key: String!) {
        self.userDefaults.set(object, forKey: key)
        self.userDefaults.synchronize()
    }
    
    func fetchUserDefaultObject(forKey key: String!) -> Any! {
        let object = self.userDefaults.object(forKey: key)
        if object == nil { return "" as Any! }
        return object as Any!
    }
    
    // MARK: - Debugging Helper
    
    fileprivate func prettyFunction(_ file: NSString = #file, function: String = #function, line: Int = #line) {
        print("<start>--- file: \(file.lastPathComponent) function:\(function) line:\(line) ---<end>")
    }
    
}
