//
//  NoteContent1ViewCell.h
//  V-Smart
//
//  Created by VhaL on 2/12/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteContent1ViewCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *labelType;
@property (nonatomic, strong) IBOutlet UIView *backgroundView;
@end
