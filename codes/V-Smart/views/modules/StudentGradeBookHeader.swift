//
//  StudentGradeBookHeader.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 08/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class StudentGradeBookHeader: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
