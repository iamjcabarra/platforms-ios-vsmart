//
//  CheckBoxCell.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 14/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckBoxCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *checkBoxLabel;
@property (weak, nonatomic) IBOutlet UILabel *cellCategory;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton;

@end
