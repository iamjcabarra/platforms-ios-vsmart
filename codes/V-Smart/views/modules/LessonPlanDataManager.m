//
//  LessonPlanDataManager.m
//  V-Smart
//
//  Created by Julius Abarra on 9/1/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>
#import "LessonPlanDataManager.h"
#import "Utils.h"
#import "AccountInfo.h"
#import "VSmartValues.h"
#import "NSDate+Helper.h"
#import "LessonPlanConstants.h"
#import "TBClassHelper.h"

static NSString *storeFilename = @"lessonplandata.sqlite";

@interface LessonPlanDataManager()

#pragma mark - PROPERTIES

@property (nonatomic, readwrite) NSManagedObjectContext *masterContext;
@property (nonatomic, readwrite) NSManagedObjectContext *mainContext;
@property (nonatomic, readwrite) NSManagedObjectContext *workerContext;

@property (nonatomic, readwrite) NSManagedObjectModel *model;
@property (nonatomic, readwrite) NSPersistentStore *store;
@property (nonatomic, readwrite) NSPersistentStoreCoordinator *coordinator;

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSDateFormatter *formatter;

@property (nonatomic, strong) TBClassHelper *classHelper;

@end

@implementation LessonPlanDataManager

#pragma mark - SINGLETON

+ (instancetype)sharedInstance {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    static LessonPlanDataManager *singleton = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        singleton = [[self alloc] init];
    });
    
    return singleton;
}

#pragma mark - SETUP

- (id)init {
    NSLog(@"Running %s", __PRETTY_FUNCTION__);
    
    self = [super init];
    if (!self) { return nil; }
    
    // MANAGED OBJECT MODEL
    self.model = [self modelFromFrameWork:@"LessonPlannerModel"];
    
    // PERSISTENT STORE COORDINATOR
    self.coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    
    BOOL compatible = [self isCompatibleWithCoreDataMetadata:[self storeURL]];
    if (!compatible) {
        self.store = nil;
    }
    
    // CORE DATA 3-LAYER STACK for MASTER, MAIN, WORKER
    [self initializeManagedObjectsWithCoordinator:self.coordinator];
    
    // SESSION MANAGER
    self.session = [NSURLSession sharedSession];
    
    // DATE FORMATTER
    self.formatter = [[NSDateFormatter alloc] init];
    
    // CLASS HELPER
    self.classHelper = [[TBClassHelper alloc] init];
    
    return self;
}

- (void)initializeManagedObjectsWithCoordinator:(NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // DATA FLUSHER
    // [WARNING: YOU ARE NOT ALLOWED TO USE THIS CONTEXT]
    // Core Data Stack for the Master Thread [MASTER] -> [PERSISTENTSTORE]
    _masterContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_masterContext performBlockAndWait:^{
        [_masterContext setPersistentStoreCoordinator:persistentStoreCoordinator];
        [_masterContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // UI RELATED CONTEXT
    // Core Data Stack for the Main Thread [MAIN] -> [MASTER]
    _mainContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_mainContext setParentContext:_masterContext];
    [_mainContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    
    // BACKGROUND CONTEXT
    // Core Data Stack for the Worker Thread [WORKER] -> [MAIN]
    _workerContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_workerContext performBlockAndWait:^{
        [_workerContext setParentContext:_mainContext];
        [_workerContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // LOAD STORE FILE
    [self loadStore];
}

- (void)loadStore {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSDictionary *sqliteConfig = @{@"journal_mode" : @"DELETE"};//@{@"journal_mode" : @"WAL"}
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption  : @YES ,
                              NSInferMappingModelAutomaticallyOption        : @YES , // Light Weight Migration
                              NSSQLitePragmasOption                         : sqliteConfig
                              };
    NSError *error = nil;
    _store = [_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:[self storeURL] options:options error:&error];
    
    if (!_store) {
        //abort();
    } else {
        NSLog(@"Successfully added store: %@", _store);
    }
}

#pragma mark - MODELS (Initialization)

- (NSManagedObjectModel *)modelFromFrameWork:(NSString *)name {
    NSBundle *appBundle = [NSBundle mainBundle];
    NSURL *modelFileURL = [appBundle URLForResource:name withExtension:@"momd"];
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelFileURL];
    
    return model;
}

#pragma mark - SAVING

- (void)saveContext {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSManagedObjectContext *ctx = _workerContext;
    
    [ctx performBlockAndWait:^{
        [self saveTreeContext:ctx];
    }];
}

- (void)saveTreeContext:(NSManagedObjectContext *)context {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    if (!context) {
        return;
    }
    
    NSError *error = nil;
    
    if (![context save:&error]) {
        NSLog(@"ERROR saving: %@", error);
    }
    
    if (context.parentContext) {
        [self saveTreeContext:context.parentContext];
    }
}

#pragma mark - PATHS

- (NSURL *)storeURL {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    NSURL *fileURL = [self applicationStoresDirectory];
    
    return [fileURL URLByAppendingPathComponent:storeFilename];
}

- (NSURL *)applicationStoresDirectory {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    
    NSString *filePath = [self applicationDocumentsDirectory];
    NSURL *storesDirectory = [[NSURL fileURLWithPath:filePath] URLByAppendingPathComponent:@"Stores"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtURL:storesDirectory withIntermediateDirectories:YES attributes:nil error:&error]) {
            NSLog(@"Successfully created Stores directory");
            if (error) {
                NSLog(@"Failed to create Stores directory : %@", error);
            }
        }
    }
    return storesDirectory;
}

- (NSString *)applicationDocumentsDirectory {
    NSLog(@"Running - %s", __PRETTY_FUNCTION__);
    return [NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - FAULTING (Memory Management)

- (void)faultObjectWithID:(NSManagedObjectID *)objectID inContext:(NSManagedObjectContext *)context {
    if (!objectID || !context) {
        return;
    }
    
    NSManagedObject *object = [context objectWithID:objectID];
    if (object.hasChanges) {
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"ERROR saving: %@", error);
        }
    }
    if (!object.isFault) {
        [context refreshObject:object mergeChanges:NO];
    }
    else {
        NSLog(@"Skipped faulting an object that is already a fault");
    }
    
    // Repeat the process if the context has a parent
    if (context.parentContext) {
        [self faultObjectWithID:objectID inContext:context.parentContext];
    }
}

#pragma mark - MIGRATION

- (BOOL)isCompatibleWithCoreDataMetadata:(NSURL *)storeUrl {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *filePath = [NSString stringWithFormat:@"%@", storeUrl.path];
    
    if ([fm fileExistsAtPath:filePath]) {
        NSLog(@"checking model for compatibility...");
        
        NSError *error = nil;
        NSDictionary *metaData = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
                                                                                            URL:storeUrl
                                                                                          error:&error];
        NSManagedObjectModel *model = self.coordinator.managedObjectModel;
        
        if ([model isConfiguration:nil compatibleWithStoreMetadata:metaData]) {
            NSLog(@"model is compatible...");
            return YES;
            
        } else {
            
            if ([fm removeItemAtPath:filePath error:NULL]) {
                NSLog(@"removed file : %@", filePath);
                NSLog(@"model is incompatible...");
                return NO;
            }// REMOVE THE STORE FILE
            
        }
    }
    
    NSLog(@"file does not exists..");
    return NO;
}

- (BOOL)migrateStore:(NSURL *)sourceStore {
    NSLog(@"SKIPPED MIGRATION: Source is already compatible");
    
    BOOL success = NO;
    NSError *error = nil;
    
    // STEP 1 - Gather the Source, Destination and Mapping Model
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:sourceStore error:&error];
    NSManagedObjectModel *sourceModel = [NSManagedObjectModel mergedModelFromBundles:nil forStoreMetadata:sourceMetadata];
    NSManagedObjectModel *destinModel = _model;
    NSMappingModel *mappingModel = [NSMappingModel mappingModelFromBundles:nil forSourceModel:sourceModel destinationModel:destinModel];
    
    // STEP 2 - Perform Migration, assuming the mapping model isn't null
    if (mappingModel) {
        NSMigrationManager *migrationManager = [[NSMigrationManager alloc] initWithSourceModel:sourceModel destinationModel:destinModel];
        
        // OBSERVER
        [migrationManager addObserver:self forKeyPath:@"migrationProgress" options:NSKeyValueObservingOptionNew context:NULL];
        
        NSURL *destinStore = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Temp.sqlite"];
        success = [migrationManager migrateStoreFromURL:sourceStore
                                                   type:NSSQLiteStoreType
                                                options:nil
                                       withMappingModel:mappingModel
                                       toDestinationURL:destinStore
                                        destinationType:NSSQLiteStoreType
                                     destinationOptions:nil
                                                  error:&error];
        if (success) {
            // STEP 3 - Replace the old store with the new migrated store
            if ([self replaceStore:sourceStore withStore:destinStore]) {
                NSLog(@"SUCCESSFULLY MIGRATED %@ to the Current Model", sourceStore.path);
                [migrationManager removeObserver:self forKeyPath:@"migrationProgress"];
            }
            // STEP 3
        }
        else {
            NSLog(@"FAILED MIGRATION : %@", error);
        }
    }
    else {
        NSLog(@"FAILED MIGRATION: Mapping Model is null");
    }
    
    return YES; // Indicates migration has finished, regardless of outcome
}

- (BOOL)replaceStore:(NSURL *)old withStore:(NSURL *)new {
    BOOL success = NO;
    NSError *Error = nil;
    if ([[NSFileManager defaultManager] removeItemAtURL:old error:&Error]) {
        
        Error = nil;
        if ([[NSFileManager defaultManager] moveItemAtURL:new toURL:old error:&Error]) {
            success = YES;
        }
        else {
            NSLog(@"FAILED to re-home new store %@", Error);
        }
    }
    else {
        NSLog(@"FAILED to remove old store %@: Error:%@", old, Error);
    }
    return success;
}

#pragma mark - WORKER

- (NSString *)baseURL {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *server = [NSString stringWithFormat:@"%@", [defaults stringForKey:@"baseurl_preference"] ];

    return server;
}

- (NSURL *)buildURL:(NSString *)string {
    NSString *path = [Utils buildUrl:string];
    NSLog(@"path : %@", path);
    return [NSURL URLWithString:path];
}

- (NSString *)emptyString:(NSString *)value {
    if ([value isEqualToString:@"(null)"]) {
        return @"";
    }
    return value;
}

- (BOOL)isArrayObject:(id)object {
    return [object isKindOfClass:[NSArray class]];
}

- (BOOL)isDictionaryObject:(id)object {
    return [object isKindOfClass:[NSDictionary class]];
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value {
    // Create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // Predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption | NSCaseInsensitivePredicateOption;
    
    // Create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (NSManagedObject *)getEntity:(NSString *)entity attribute:(NSString *)attribute
                     parameter:(NSString *)parameter context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (parameter) {
        
        // Create predicate
        NSPredicate *predicate = [self predicateForKeyPath:attribute andValue:parameter];
        
        // Set predicate
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
}

- (NSManagedObject *)getEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"entity : %@", entity);
    NSLog(@"predicate : %@", predicate);
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSManagedObjectContext *context = _workerContext;
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return items;
    }
    
    return nil;
}

- (NSArray *)getObjectsForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate andSortDescriptor:(NSString *)descriptor {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"entity : %@", entity);
    NSLog(@"predicate : %@", predicate);
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:descriptor ascending:YES selector:@selector(localizedStandardCompare:)];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    NSError *error = nil;
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return items;
    }
    
    return nil;
}

- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate sortDescriptor:(NSSortDescriptor *)descriptor {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"entity : %@", entity);
    NSLog(@"predicate : %@", predicate);
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    [fetchRequest setSortDescriptors:@[descriptor]];
    
    NSError *error = nil;
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return items;
    }
    
    return nil;
}

- (NSArray *)getObjectsForEntity:(NSString *)entity withFKey:(NSString *)fKey andFKeyValue:(NSString *)fValue andSKey:(NSString *)sKey andSKeyValue:(NSString *)sValue sortedBy:(NSString *)sortDescriptor {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"entity : %@", entity);
    NSLog(@"fKey : %@", fKey);
    NSLog(@"fValue : %@", fValue);
    NSLog(@"sKey : %@", sKey);
    NSLog(@"sValue : %@", sValue);
    
    NSPredicate *p1 = [self predicateForKeyPath:fKey andValue:fValue];
    NSPredicate *p2 = [self predicateForKeyPath:sKey andValue:sValue];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[p1, p2]];
    
    NSArray *listOfObjects = [self getObjectsForEntity:entity
                                         withPredicate:predicate
                                     andSortDescriptor:sortDescriptor];
    
    if ([listOfObjects count] > 0) {
        return listOfObjects;
    }
    
    return nil;
}

- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *lmo in items) {
            [context deleteObject:lmo];
        }
        [self saveTreeContext:context];
        
        return YES;
    }
    
    return NO;
}

- (BOOL)clearDataForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)ctx {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate != nil) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *mo in items) {
            [ctx deleteObject:mo];
        }
        [self saveTreeContext:ctx];
    }
    
    return YES;
}

- (NSString *)loginUser {
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *userid = [NSString stringWithFormat:@"%d", account.user.id];
    return userid;
}

- (NSString *)userEmail {
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *email = [NSString stringWithFormat:@"%@", account.user.email];
    return email;
}

- (NSTimeInterval)parseTimeFromString:(NSString *)string {
    NSScanner *scanner = [NSScanner scannerWithString:string];
    NSInteger h, m, s;
    
    [scanner scanInteger:&h];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&m];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&s];
    
    return h * 3600 + m * 60 + s;
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (NSDate *)parseDateFromString:(NSString *)dateString {
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@".000Z" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" togo" withString:@""];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [self.formatter setTimeZone:gmt];
    NSString *template = @"yyyy-MM-dd hh:mm:ss";
    [self.formatter setDateFormat:template];
    NSDate *dateObject = [self.formatter dateFromString:dateString];
    return dateObject;
}

- (id)parseResponseData:(NSData *)data {
    
    if (data) {
        NSError *jsonError = nil;
        
        id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
        
        if (jsonError) {
            NSLog(@"JSON Error : %@", jsonError.localizedDescription);
        }
        
        if (!jsonError) {
            return object;
        }        
    }
 
    return nil;
}

- (NSMutableURLRequest *)buildRequestWithMethod:(NSString *)method NSURL:(NSURL *)url body:(id)body {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:method];
    
    if (body) {
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:body options:0 error:&error];
        
        NSString *j_string = [NSString stringWithUTF8String:[postData bytes]];
        NSLog(@"j_string : <start>---- %@ ---<end>", j_string);
        [request setHTTPBody:postData];
    }
    
    return request;
}

- (BOOL)ownedByUser:(NSString *)userid {
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return [user_id isEqualToString:userid];
}

- (NSString *)generateBoundaryString {
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary parameters:(NSDictionary *)parameters {
    NSMutableData *httpBody = [NSMutableData data];
    
    // Add params (all params are strings)
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary parameters:(NSDictionary *)parameters file:(NSDictionary *)fileObject {
    NSMutableData *httpBody = [NSMutableData data];
    
    NSLog(@"FILE OBJECT: %@", fileObject);
    
    // Add params (all params are strings)
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    if (fileObject) {
        NSString *fieldname = @"file";
        NSString *filename = [NSString stringWithFormat:@"%@", fileObject[@"filename"]];
        NSURL *fileURL = [NSURL fileURLWithPath:fileObject[@"filepath"]];
        NSData *data = [NSData dataWithContentsOfURL:fileURL];
        NSString *mimetype = [NSString stringWithFormat:@"%@", fileObject[@"mimetype"]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldname, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary file:(NSDictionary *)fileObject {
    NSMutableData *httpBody = [NSMutableData data];
    
    if (fileObject) {
        NSString *fieldname = @"file";
        NSString *filename = [NSString stringWithFormat:@"%@", fileObject[@"filename"]];
        NSURL *fileURL = [NSURL fileURLWithPath:fileObject[@"filepath"]];
        NSData *data = [NSData dataWithContentsOfURL:fileURL];
        NSString *mimetype  = [NSString stringWithFormat:@"%@", fileObject[@"mimetype"]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldname, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary andFileAtPath:(NSString *)filePath {
    NSMutableData *httpBody = [NSMutableData data];
    
    if (filePath != nil) {
        NSString *fileName = [filePath lastPathComponent];
        NSURL *fileURL = [NSURL fileURLWithPath:filePath];
        NSData *data = [NSData dataWithContentsOfURL:fileURL options:0 error:nil];
        NSString *mimetype  = [self mimeTypeForFileAtPath:filePath];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@\"\r\n", fileName] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

- (NSData *)createPostBodyWithBoundary:(NSString *)boundary parameters:(NSDictionary *)parameters andFileAtPath:(NSString *)filePath {
    NSMutableData *httpBody = [NSMutableData data];
   
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    if (filePath != nil) {
        NSString *fileName = [filePath lastPathComponent];
        NSURL *fileURL = [NSURL fileURLWithPath:filePath];
        NSData *data = [NSData dataWithContentsOfURL:fileURL options:0 error:nil];
        NSString *mimetype  = [self mimeTypeForFileAtPath:filePath];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@\"\r\n", fileName] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

- (BOOL)isItOkayToParseFromAPIUsingThisResponseData:(NSDictionary *)response {
    if (response != nil) {
        NSDictionary *parsedmeta = response[@"_meta"];
        NSLog(@"parsedmeta: %@", parsedmeta);
        
        if (parsedmeta != nil) {
            if ([self doesKeyExistInDictionary:parsedmeta key:@"status"]) {
                NSString *status = [self stringValue:parsedmeta[@"status"]];
                NSLog(@"metastatus: %@", status);
                
                if ([status isEqualToString:@"SUCCESS"]) {
                    if ([self doesKeyExistInDictionary:parsedmeta key:@"count"]) {
                        NSString *count = [self stringValue:parsedmeta[@"count"]];
                        NSLog(@"metacount: %@", count);
                        
                        if ([count integerValue] > 0) {
                            return YES;
                        }
                    }
                }
            }
        }
    }
    
    return NO;
}

- (BOOL)doesKeyExistInDictionary:(NSDictionary *)d key:(NSString *)key {
    NSArray *keys = [d allKeys];
    
    for (NSString *k in keys) {
        if ([k isEqualToString:key]) {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - COURSE API

- (void)requestCourseListForUser:(NSString *)userid dataBlock:(LessonPlanDataBlock)dataBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCourseList, userid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        NSLog(@"userid: %@", userid);
        
        NSManagedObjectContext *ctx = self.workerContext;
        [self clearDataForEntity:kCourseTableEntity withPredicate:nil context:ctx];
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseFromAPIUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"course records: %@", records);
                
                if (records.count > 0) {
                    [ctx performBlock:^{
                        
                        for (NSDictionary *d in records) {
                            NSLog(@"course data: %@", d);
                            
                            // Parse data from API
                            NSString *course_id = [self stringValue:d[@"course_id"]];
                            NSString *course_name = [self stringValue:d[@"course_name"]];
                            NSString *initial = [self stringValue:d[@"initial"]];
                            NSString *description = [self stringValue:d[@"description"]];
                            NSString *course_code = [self stringValue:d[@"course_code"]];
                            NSString *cs_id = [self stringValue:d[@"cs_id"]];
                            NSString *section_id = [self stringValue:d[@"section_id"]];
                            NSString *section_name = [self stringValue:d[@"section_name"]];
                            NSString *grade_level = [self stringValue:d[@"grade_level"]];
                            NSString *schedule = [self stringValue:d[@"schedule"]];
                            NSString *venue = [self stringValue:d[@"venue"]];
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:kCourseTableEntity attribute:@"course_id" parameter:course_id context:ctx];
                            
                            // Save data to core data
                            [mo setValue:userid forKey:@"user_id"];
                            [mo setValue:course_id forKey:@"course_id"];
                            [mo setValue:course_name forKey:@"course_name"];
                            [mo setValue:initial forKey:@"initial"];
                            [mo setValue:description forKey:@"course_desc"];
                            [mo setValue:course_code forKey:@"course_code"];
                            [mo setValue:cs_id forKey:@"cs_id"];
                            [mo setValue:section_id forKey:@"section_id"];
                            [mo setValue:section_name forKey:@"section_name"];
                            [mo setValue:grade_level forKey:@"grade_level"];
                            [mo setValue:schedule forKey:@"schedule"];
                            [mo setValue:venue forKey:@"venue"];
                            
                            // Search string
                            NSString *search_string = [NSString stringWithFormat:@"%@%@%@%@", course_name, section_name, grade_level, schedule];
                            
                            [mo setValue:search_string forKey:@"search_string"];
                            
                        }//end for loop
                        
                        [self saveTreeContext:ctx];
                        
                        NSDictionary *data = @{@"count":@(records.count)};
                        
                        if (dataBlock) {
                            dataBlock(data);
                        }
                        
                    }];//end context perform block
                }//end has record
                else {
                    if (dataBlock) {
                        dataBlock(nil);
                    }
                }//end has no record
            }//end okay to parse
            else {
                if (dataBlock) {
                    dataBlock (nil);
                }
            }//not okay to parse
        }//end not error
    }];//end task
    
    [task resume];
}

- (void)requestPaginatedCourseListForUser:(NSString *)userid withSettingsForPagination:(NSDictionary *)settings dataBlock:(LessonPlanDataBlock)dataBlock {
    NSString *limit = [self stringValue:settings[@"limit"]];
    NSString *current_page = [self stringValue:settings[@"current_page"]];
    NSString *search_keyword = [self stringValue:settings[@"search_keyword"]];
    NSString *enconded_search_keyword = [self urlEncode:search_keyword];
    NSString *sort_key = [self stringValue:settings[@"sort_key"]];
    
    NSManagedObjectContext *ctx = self.workerContext;
    
    if ([current_page isEqualToString:@"1"]) {
        [self clearDataForEntity:kCourseTableEntity withPredicate:nil context:ctx];
    }
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointLessonPlanPaginatedCourseList, userid, limit, current_page, enconded_search_keyword, sort_key]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseFromAPIUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSDictionary *records = dictionary[@"records"];
                NSArray *data = records[@"data"];
                
                NSMutableDictionary *pagination = [NSMutableDictionary dictionary];
                if (records[@"pagination"] != nil) {
                    NSDictionary *object = (NSDictionary *)records[@"pagination"];
                    for (NSString *k in object.allKeys) {
                        pagination[k] = [self stringValue:object[k]];
                    }
                }
                
                NSLog(@"records: %@", records);
                NSLog(@"pagination: %@", data);
                NSLog(@"data: %@", data);
                
                if ([self isArrayObject:data] && data.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d in data) {
                            NSString *course_id = [self stringValue:d[@"course_id"]];
                            NSString *course_name = [self stringValue:d[@"course_name"]];
                            NSString *initial = [self stringValue:d[@"initial"]];
                            NSString *course_description = [self stringValue:d[@"description"]];
                            NSString *course_code = [self stringValue:d[@"course_code"]];
                            NSString *course_banner = [self stringValue:d[@"course_banner"]];
                            NSString *grade_level_id = [self stringValue:d[@"grade_level_id"]];
                            NSString *school_level_id = [self stringValue:d[@"school_level_id"]];
                            NSString *cs_id = [self stringValue:d[@"cs_id"]];
                            NSString *section_id = [self stringValue:d[@"section_id"]];
                            NSString *schedule = [self stringValue:d[@"schedule"]];
                            NSString *venue = [self stringValue:d[@"venue"]];
                            NSString *section_name = [self stringValue:d[@"section_name"]];
                            NSString *grade_level = [self stringValue:d[@"grade_level"]];
                            NSString *school_level = [self stringValue:d[@"school_level"]];
                            NSString *search_string = [NSString stringWithFormat:@"%@%@%@%@", course_name, section_name, grade_level, schedule];
                            
                            NSManagedObject *mo = [self getEntity:kCourseTableEntity attribute:@"course_id" parameter:course_id context:ctx];
                            
                            [mo setValue:userid forKey:@"user_id"];
                            [mo setValue:course_id forKey:@"course_id"];
                            [mo setValue:course_name forKey:@"course_name"];
                            [mo setValue:initial forKey:@"initial"];
                            [mo setValue:course_description forKey:@"course_description"];
                            [mo setValue:course_code forKey:@"course_code"];
                            [mo setValue:course_banner forKey:@"course_banner"];
                            [mo setValue:grade_level_id forKey:@"grade_level_id"];
                            [mo setValue:school_level_id forKey:@"school_level_id"];
                            [mo setValue:cs_id forKey:@"cs_id"];
                            [mo setValue:section_id forKey:@"section_id"];
                            [mo setValue:schedule forKey:@"schedule"];
                            [mo setValue:venue forKey:@"venue"];
                            [mo setValue:section_name forKey:@"section_name"];
                            [mo setValue:grade_level forKey:@"grade_level"];
                            [mo setValue:school_level forKey:@"school_level"];
                            [mo setValue:search_string forKey:@"search_string"];
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                }
                
                if (dataBlock) {
                    dataBlock(pagination);
                }
            }
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
}

#pragma mark - LESSON API

- (void)requestLessonPlanListForUser:(NSString *)userid dataBlock:(LessonPlanDataBlock)dataBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointLessonPlanList, userid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        NSLog(@"Userid: %@", userid);
        
        NSManagedObjectContext *ctx = self.workerContext;
        [self clearDataForEntity:kLessonPlanEntity withPredicate:nil context:ctx];
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            NSDictionary *parsedmeta = dictionary[@"_meta"];
            NSString *metastatus = parsedmeta[@"status"];
            
            NSLog(@"lesson dictionary: %@", dictionary);
            NSLog(@"lesson parsedmeta: %@", parsedmeta);
            NSLog(@"lesson metastatus: %@", metastatus);
            
            BOOL isSuccess = NO;
            
            if ([metastatus isEqualToString:@"SUCCESS"]) {
                isSuccess = YES;
            }
            
            if (isSuccess) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"lesson records: %@", records);
                
                if (records.count > 0) {
                    [ctx performBlock:^{
                        for (NSDictionary *d1 in records) {
                            NSArray *learning_plans = d1[@"learning_plan"];
                            
                            if (learning_plans.count > 0) {
                                for (NSDictionary *d2 in learning_plans) {
                                    NSLog(@"lesson data: %@", d2);
                                    
                                    // Parse data from API
                                    NSString *course_id = [self stringValue:d2[@"course_id"]];
                                    NSString *lesson_id = [self stringValue:d2[@"id"]];
                                    NSString *user_id = [self stringValue:d2[@"user_id"]];
                                    NSString *name = [self stringValue:d2[@"name"]];
                                    NSString *school_name = [self stringValue:d2[@"school_name"]];
                                    NSString *school_address = [self stringValue:d2[@"school_address"]];
                                    NSString *level = [self stringValue:d2[@"level"]];
                                    NSString *unit = [self stringValue:d2[@"unit"]];
                                    NSString *quarter = [self stringValue:d2[@"quarter"]];
                                    NSString *start_date = [self stringValue:d2[@"start_date"]];
                                    NSString *end_date = [self stringValue:d2[@"end_date"]];
                                    NSString *status = [self stringValue:d2[@"status"]];
                                    NSString *template_id = [self stringValue:d2[@"template_id"]];
                                    NSString *is_deleted = [self stringValue:d2[@"is_deleted"]];
                                    NSString *date_created = [self stringValue:d2[@"date_created"]];
                                    NSString *date_modified = [self stringValue:d2[@"date_modified"]];
                                    
                                    // Create managed object
                                    NSManagedObject *mo = [self getEntity:kLessonPlanEntity attribute:@"id" parameter:lesson_id context:ctx];
                                    
                                    // Format dates (start and end)
                                    start_date = [self.classHelper transformDateString:start_date
                                                                            fromFormat:kLPServerDateFormat
                                                                              toFormat:KLPDisplayDateFormatShort];
                                    
                                    end_date = [self.classHelper transformDateString:end_date
                                                                          fromFormat:kLPServerDateFormat
                                                                            toFormat:KLPDisplayDateFormatShort];
                                    
                                    // Save data to core data
                                    [mo setValue:course_id forKey:@"course_id"];
                                    [mo setValue:lesson_id forKey:@"id"];
                                    [mo setValue:user_id forKey:@"user_id"];
                                    [mo setValue:name forKey:@"name"];
                                    [mo setValue:school_name forKey:@"school_name"];
                                    [mo setValue:school_address forKey:@"school_address"];
                                    [mo setValue:level forKey:@"level"];
                                    [mo setValue:unit forKey:@"unit"];
                                    [mo setValue:quarter forKey:@"quarter"];
                                    [mo setValue:start_date forKey:@"start_date"];
                                    [mo setValue:end_date forKey:@"end_date"];
                                    [mo setValue:status forKey:@"status"];
                                    [mo setValue:template_id forKey:@"template_id"];
                                    [mo setValue:is_deleted forKey:@"is_deleted"];
                                    [mo setValue:date_created forKey:@"date_created"];
                                    [mo setValue:date_modified forKey:@"date_modified"];
                                    [mo setValue:lesson_id forKey:@"lp_id"];
                                    
                                    // Sorted Date Modified
                                    NSDate *sorted_date_modified = [NSDate dateFromString:date_modified];
                                    [mo setValue:sorted_date_modified forKey:@"sorted_date_modified"];
                                    
                                    NSLog(@"date_modified: %@", date_modified);
                                    NSLog(@"sorted_date_modified: %@", sorted_date_modified);

                                }//end for loop
                            }//end has learning plan
                        }//end for loop
                        
                        [self saveTreeContext:ctx];
                        
                        NSDictionary *data = @{@"count":@(records.count)};
                        
                        if (dataBlock) {
                            dataBlock(data);
                        }
                        
                    }];//end context perform block
                }//end has record
                else {
                    NSDictionary *data = @{@"count":@(records.count)};
                    
                    if (dataBlock) {
                        dataBlock(data);
                    }
                }
            }//end success
            else {
                // TO-DO: Check perform segue from previous view (status should not be checked)
                // YES: To allow the user to navigate the lesson view even if it has an empty record
                // This will allow the user to create a lesson plan
                if (dataBlock) {
                    dataBlock(nil);
                }
            }//end not success
        }//end not error
    }];//end task
    
    [task resume];
}

- (void)requestLessonPlanListForUser:(NSString *)userid courseid:(NSString *)courseid dataBlock:(LessonPlanDataBlock)dataBlock {
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kLessonPlanEntity withPredicate:nil context:ctx];

    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointLessonPlanList, userid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        NSLog(@"Userid: %@", userid);
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            NSDictionary *parsedmeta = dictionary[@"_meta"];
            NSString *metastatus = parsedmeta[@"status"];
            
            NSLog(@"lesson dictionary: %@", dictionary);
            NSLog(@"lesson parsedmeta: %@", parsedmeta);
            NSLog(@"lesson metastatus: %@", metastatus);
            
            BOOL isSuccess = NO;
            
            if ([metastatus isEqualToString:@"SUCCESS"]) {
                isSuccess = YES;
            }
            
            if ([metastatus isEqualToString:@"ERROR"]) {
                
                isSuccess = NO;
                
                NSDictionary *data = @{@"count":@(0)};
                
                if (dataBlock) {
                    dataBlock(data);
                }
            }
            
            if (isSuccess) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"lesson records: %@", records);
                
                if (records.count > 0) {
                    [ctx performBlock:^{
                        NSInteger lesson_count = 0;
                        
                        for (NSDictionary *d1 in records) {
                            NSString *parsed_course_id = d1[@"course_id"];
                            
                            if ([parsed_course_id isEqualToString:courseid]) {
                                NSArray *learning_plans = d1[@"learning_plan"];
                                
                                if (learning_plans.count > 0) {
                                    lesson_count += learning_plans.count;
                                    
                                    for (NSDictionary *d2 in learning_plans) {
                                        NSLog(@"lesson data: %@", d2);
                                        
                                        // Parse data from API
                                        NSString *course_id = [self stringValue:d2[@"course_id"]];
                                        NSString *lesson_id = [self stringValue:d2[@"id"]];
                                        NSString *user_id = [self stringValue:d2[@"user_id"]];
                                        NSString *name = [self stringValue:d2[@"name"]];
                                        NSString *school_name = [self stringValue:d2[@"school_name"]];
                                        NSString *school_address = [self stringValue:d2[@"school_address"]];
                                        NSString *level = [self stringValue:d2[@"level"]];
                                        NSString *unit = [self stringValue:d2[@"unit"]];
                                        NSString *quarter = [self stringValue:d2[@"quarter"]];
                                        NSString *start_date = [self stringValue:d2[@"start_date"]];
                                        NSString *end_date = [self stringValue:d2[@"end_date"]];
                                        NSString *status = [self stringValue:d2[@"status"]];
                                        NSString *template_id = [self stringValue:d2[@"template_id"]];
                                        NSString *is_deleted = [self stringValue:d2[@"is_deleted"]];
                                        NSString *date_created = [self stringValue:d2[@"date_created"]];
                                        NSString *date_modified = [self stringValue:d2[@"date_modified"]];
                                        
                                        // Create managed object
                                        NSManagedObject *mo = [self getEntity:kLessonPlanEntity attribute:@"id" parameter:lesson_id context:ctx];
                                        
                                        // Format dates (start and end)
                                        start_date = [self.classHelper transformDateString:start_date
                                                                                fromFormat:kLPServerDateFormat
                                                                                  toFormat:KLPDisplayDateFormatShort];
                                        
                                        end_date = [self.classHelper transformDateString:end_date
                                                                              fromFormat:kLPServerDateFormat
                                                                                toFormat:KLPDisplayDateFormatShort];
                                        
                                        // Save data to core data
                                        [mo setValue:course_id forKey:@"course_id"];
                                        [mo setValue:lesson_id forKey:@"id"];
                                        [mo setValue:user_id forKey:@"user_id"];
                                        [mo setValue:name forKey:@"name"];
                                        [mo setValue:school_name forKey:@"school_name"];
                                        [mo setValue:school_address forKey:@"school_address"];
                                        [mo setValue:level forKey:@"level"];
                                        [mo setValue:unit forKey:@"unit"];
                                        [mo setValue:quarter forKey:@"quarter"];
                                        [mo setValue:start_date forKey:@"start_date"];
                                        [mo setValue:end_date forKey:@"end_date"];
                                        [mo setValue:status forKey:@"status"];
                                        [mo setValue:template_id forKey:@"template_id"];
                                        [mo setValue:is_deleted forKey:@"is_deleted"];
                                        [mo setValue:date_created forKey:@"date_created"];
                                        [mo setValue:date_modified forKey:@"date_modified"];
                                        [mo setValue:lesson_id forKey:@"lp_id"];
                                        
                                        // Sorted Date Created and Modified
                                        NSDate *sorted_date_created = [NSDate dateFromString:date_created];
                                        NSDate *sorted_date_modified = [NSDate dateFromString:date_modified];
                                        [mo setValue:sorted_date_created forKey:@"sorted_date_created"];
                                        [mo setValue:sorted_date_modified forKey:@"sorted_date_modified"];
                                        
                                    }//end for loop
                                }//end has learning plan
                            }
                        }//end for loop
                        
                        [self saveTreeContext:ctx];
                        
                        NSDictionary *data = @{@"count":@(lesson_count)};
                        
                        if (dataBlock) {
                            dataBlock(data);
                        }
                        
                    }];//end context perform block
                }//end has record
                else {
                    NSDictionary *data = @{@"count":@(0)};
                    
                    if (dataBlock) {
                        dataBlock(data);
                    }
                }
            }//end success
            else {
                // TO-DO: Check perform segue from previous view (status should not be checked)
                // YES: To allow the user to navigate the lesson view even if it has an empty record
                // This will allow the user to create a lesson plan
                NSDictionary *data = @{@"count":@(0)};
                
                if (dataBlock) {
                    dataBlock(data);
                }
            }//end not success
        }//end not error
    }];//end task
    
    [task resume];
}

- (void)requestLessonPlanDetailsForLessonPlanWithID:(NSString *)lessonplanid doneBlock:(LessonPlanDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointLessonPlanDetails, lessonplanid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        NSLog(@"lessonid: %@", lessonplanid);
        
        NSManagedObjectContext *ctx = self.workerContext;
        //[self clearDataForEntity:kLessonPlanDetailsEntity withPredicate:nil context:ctx];
        //[self clearDataForEntity:kContentEntity withPredicate:nil context:ctx];
        //[self clearDataForEntity:kProcessEntity withPredicate:nil context:ctx];
        //[self clearDataForEntity:kCommentEntity withPredicate:nil context:ctx];
        //[self clearDataForEntity:kHistoryEntity withPredicate:nil context:ctx];
        
        NSArray *entities = @[kLessonPlanDetailsEntity, kContentEntity, kProcessEntity, kCommentEntity, kHistoryEntity, kPreAssociatedCompetency];
        BOOL cleared = [self clearEntities:entities];
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            NSDictionary *parsedmeta = dictionary[@"_meta"];
            NSString *metastatus = parsedmeta[@"status"];
            
            NSLog(@"lesson details dictionary: %@", dictionary);
            NSLog(@"lesson details parsedmeta: %@", parsedmeta);
            NSLog(@"lesson details metastatus: %@", metastatus);
            
            BOOL isSuccess = NO;
            
            if ([metastatus isEqualToString:@"SUCCESS"]) {
                isSuccess = YES;
            }

            if (isSuccess && cleared) {
                NSDictionary *records = dictionary[@"records"];
                NSLog(@"lesson details records: %@", records);
                
                if (records.count > 0) {
                    [ctx performBlock:^{
                        // Parse data from API
                        NSString *name = [self stringValue:records[@"name"]];
                        NSString *template_id = [self stringValue:records[@"template_id"]];
                        NSString *template_name = [self stringValue:records[@"template_name"]];
                        NSString *teacher_name = [self stringValue:records[@"teacher_name"]];
                        NSString *school_name = [self stringValue:records[@"school_name"]];
                        NSString *school_address = [self stringValue:records[@"school_address"]];
                        NSString *course_id = [self stringValue:records[@"course_id"]];
                        NSString *level = [self stringValue:records[@"level"]];
                        NSString *unit = [self stringValue:records[@"unit"]];
                        NSString *quarter = [self stringValue:records[@"quarter"]];
                        NSString *start_date = [self stringValue:records[@"start_date"]];
                        NSString *end_date = [self stringValue:records[@"end_date"]];
                        NSString *status = [self stringValue:records[@"status"]];
                        
                        // Create managed object
                        NSManagedObject *mo = [self getEntity:kLessonPlanDetailsEntity attribute:@"lp_id" parameter:lessonplanid context:ctx];
                        
                        start_date = [self.classHelper transformDateString:start_date
                                                                fromFormat:kLPServerDateFormat
                                                                  toFormat:KLPDisplayDateFormatShort];
                        
                        end_date = [self.classHelper transformDateString:end_date
                                                              fromFormat:kLPServerDateFormat
                                                                toFormat:KLPDisplayDateFormatShort];
                        
                        // Save data to core data
                        [mo setValue:name forKey:@"name"];
                        [mo setValue:template_id forKey:@"template_id"];
                        [mo setValue:template_name forKey:@"template_name"];
                        [mo setValue:teacher_name forKey:@"teacher_name"];
                        [mo setValue:school_name forKey:@"school_name"];
                        [mo setValue:school_address forKey:@"school_address"];
                        [mo setValue:course_id forKey:@"course_id"];
                        [mo setValue:level forKey:@"level"];
                        [mo setValue:unit forKey:@"unit"];
                        [mo setValue:quarter forKey:@"quarter"];
                        [mo setValue:start_date forKey:@"start_date"];
                        [mo setValue:end_date forKey:@"end_date"];
                        [mo setValue:status forKey:@"status"];
                        [mo setValue:lessonplanid forKey:@"lp_id"];
                        
                        NSArray *contentlist = records[@"content"];
                        [self processContent:contentlist managedObject:mo lessonplanid:lessonplanid];
                        
                        NSArray *commentlist = records[@"comments"];
                        [self processComment:commentlist managedObject:mo];
                        
                        NSArray *historylist = records[@"history"];
                        [self processHistory:historylist managedObject:mo];
                        
                        NSArray *competencies = records[@"competencies"];
                        
                        for (NSDictionary *d in competencies) {
                            NSString *curriculum_id = [self stringValue:d[@"curriculum_id"]];
                            NSString *period_id = [self stringValue:d[@"period_id"]];
                            NSString *ccc_id = [self stringValue:d[@"ccc_id"]];
                            NSString *content = [self stringValue:d[@"content"]];
                            
                            NSManagedObject *cmo = [self getEntity:kPreAssociatedCompetency attribute:@"ccc_id" parameter:ccc_id context:ctx];
                            
                            [cmo setValue:curriculum_id forKey:@"curriculum_id"];
                            [cmo setValue:period_id forKey:@"period_id"];
                            [cmo setValue:@([ccc_id integerValue]) forKey:@"ccc_id"];
                            [cmo setValue:content forKey:@"content"];
                        }
                        
                        [self saveTreeContext:ctx];
                        
                        if (doneBlock) {
                            doneBlock(YES);
                        }
                        
                    }];//end context perform block
                }//end has record
            }//end success
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }//end not error
    }];//end task
    
    [task resume];
}

- (BOOL)clearEntities:(NSArray *)entities {
    NSManagedObjectContext *ctx = self.workerContext;
    
    for (NSString *entity in entities) {
        BOOL success = [self clearDataForEntity:entity withPredicate:nil context:ctx];
        if (!success) { return false; }
    }
    
    return true;
}

- (NSArray *)fetchPreAssociatedLearningCompetencies {
    NSArray *competencies = [self getObjectsForEntity:kPreAssociatedCompetency predicate:nil context:self.workerContext];
    return competencies;
}

- (BOOL)removePreAssociatedLearningCompetency:(NSString *)competency {
    NSPredicate *predicate = [self predicateForKeyPath:@"ccc_id" andValue:competency];
    BOOL removed = [self clearDataForEntity:kPreAssociatedCompetency withPredicate:predicate context:self.workerContext];
    return removed;
}

- (void)processContent:(NSArray *)contentlist managedObject:(NSManagedObject *)object lessonplanid:(NSString *)lp_id {
    // Use sent managed object context
    NSManagedObjectContext *ctx = object.managedObjectContext;
    NSMutableSet *set = [NSMutableSet set];
    
    if (contentlist.count > 0) {
        for (NSDictionary *content_item in contentlist) {
            NSLog(@"lesson details content data: %@", content_item);
            
            // Parse data from API
            NSString *stage_id = [self stringValue:content_item[@"stage_id"]];
            NSString *stage_name = [self stringValue:content_item[@"stage_name"]];
            
            // Create managed object
            NSManagedObject *ct_mo = [self getEntity:kContentEntity attribute:@"stage_id" parameter:stage_id context:ctx];
            
            // Save data to core data
            [ct_mo setValue:stage_id forKey:@"stage_id"];
            [ct_mo setValue:stage_name forKey:@"stage_name"];
            [ct_mo setValue:lp_id forKey:@"lp_id"];
            
            [set addObject:ct_mo];
    
            NSArray *processlist = content_item[@"process"];
            [self processContentProcess:processlist managedObject:ct_mo  lessonplanid:lp_id stageid:stage_id];
        }
        
        [object setValue:set forKey:@"contents"];
    }
}
- (void)processContentProcess:(NSArray *)processlist managedObject:(NSManagedObject *)object lessonplanid:(NSString *)lp_id stageid:(NSString *)stage_id {
    // Use sent managed object context
    NSManagedObjectContext *ctx = object.managedObjectContext;
    NSMutableSet *set = [NSMutableSet set];

    if (processlist.count > 0) {
        NSInteger index = 1;
        
        for (NSDictionary *process_item in processlist) {
            NSLog(@"lesson details content process data: %@", process_item);
            
            //TODO: Need further investigation with server side team
            NSString *addToFilePath = @"/public/img/uploads/rubrics/"; // Not included in API
            
            // Parse data from API
            NSString *learning_process_name = [self stringValue:process_item[@"learning_process_name"]];
            NSString *lc_id = [self stringValue:process_item[@"lc_id"]];
            NSString *content = [self stringValue:process_item[@"content"]];
            NSString *file_path = [self stringValue:process_item[@"file_path"]];
            NSString *orig_filename = [self stringValue:process_item[@"orig_filename"]];
            NSString *is_file = [self stringValue:process_item[@"is_file"]];
            NSString *is_deleted = [self stringValue:process_item[@"is_deleted"]];
            
            if (![file_path isEqualToString:@""]) {
                file_path = [NSString stringWithFormat:@"http://%@%@%@", [self baseURL], addToFilePath, file_path];
            }
            
            // Create managed object
            NSManagedObject *pr_mo = [self getEntity:kProcessEntity attribute:@"lc_id" parameter:lc_id context:ctx];
            
            // Save data to core data
            [pr_mo setValue:learning_process_name forKey:@"learning_process_name"];
            [pr_mo setValue:lc_id forKey:@"lc_id"];
            [pr_mo setValue:content forKey:@"p_content"];
            [pr_mo setValue:file_path forKey:@"file_path"];
            [pr_mo setValue:orig_filename forKey:@"orig_filename"];
            [pr_mo setValue:is_file forKey:@"is_file"];
            [pr_mo setValue:is_deleted forKey:@"is_deleted"];
            [pr_mo setValue:lp_id forKey:@"lp_id"];
            [pr_mo setValue:stage_id forKey:@"stage_id"];
            [pr_mo setValue:@(index) forKey:@"index"];
            
            index++;
            
            [set addObject:pr_mo];
        }
        
        [object setValue:set forKey:@"processes"];
    }
}

- (void)processComment:(NSArray *)commentlist managedObject:(NSManagedObject *)object {
    // Use sent managed object context
    NSManagedObjectContext *ctx = object.managedObjectContext;
    NSMutableSet *set = [NSMutableSet set];
    
    if (commentlist.count > 0) {
        for (NSDictionary *comment_item in commentlist) {
            NSLog(@"lesson details comment data: %@", comment_item);
            
            // Parse data from API
            NSString *cid = [self stringValue:comment_item[@"id"]];
            NSString *lp_id = [self stringValue:comment_item[@"lp_id"]];
            NSString *user_id = [self stringValue:comment_item[@"user_id"]];
            NSString *first_name = [self stringValue:comment_item[@"first_name"]];
            NSString *last_name = [self stringValue:comment_item[@"last_name"]];
            NSString *avatar = [NSString stringWithFormat:@"http://%@%@", [self baseURL], [self stringValue:comment_item[@"avatar"]]];
            NSString *comment = [self stringValue:comment_item[@"comment"]];
            NSString *lpc_is_deleted = [self stringValue:comment_item[@"lpc_is_deleted"]];
            NSString *date_created = [self stringValue:comment_item[@"date_created"]];
            NSString *date_modified = [self stringValue:comment_item[@"date_modified"]];
            
            // Create managed object
            NSManagedObject *cm_mo = [self getEntity:kCommentEntity attribute:@"id" parameter:cid context:ctx];
            
            // Sorted Date Modified
            NSDate *sorted_date_modified = [NSDate dateFromString:date_modified];
            [cm_mo setValue:sorted_date_modified forKey:@"sorted_date_modified"];
            
            // Save data to core data
            [cm_mo setValue:cid forKey:@"id"];
            [cm_mo setValue:lp_id forKey:@"lp_id"];
            [cm_mo setValue:user_id forKey:@"user_id"];
            [cm_mo setValue:first_name forKey:@"first_name"];
            [cm_mo setValue:last_name forKey:@"last_name"];
            [cm_mo setValue:avatar forKey:@"avatar"];
            [cm_mo setValue:comment forKey:@"comment"];
            [cm_mo setValue:lpc_is_deleted forKey:@"lpc_is_deleted"];
            [cm_mo setValue:date_created forKey:@"date_created"];
            [cm_mo setValue:date_modified forKey:@"date_modified"];
            
            [set addObject:cm_mo];
        }
        
        [object setValue:set forKey:@"comments"];
    }
}

- (void)processHistory:(NSArray *)historylist managedObject:(NSManagedObject *)object {
    // Use sent managed object context
    NSManagedObjectContext *ctx = object.managedObjectContext;
    NSMutableSet *set = [NSMutableSet set];
    
    if (historylist.count > 0) {
        for (NSDictionary *history_item in historylist) {
            NSLog(@"lesson details history data: %@", history_item);
            
            // Parse data from API
            NSString *hid = [self stringValue:history_item[@"id"]];
            NSString *lp_id = [self stringValue:history_item[@"lp_id"]];
            NSString *user_id = [self stringValue:history_item[@"user_id"]];
            NSString *first_name = [self stringValue:history_item[@"first_name"]];
            NSString *last_name = [self stringValue:history_item[@"last_name"]];
            NSString *avatar = [NSString stringWithFormat:@"http://%@%@", [self baseURL], [self stringValue:history_item[@"avatar"]]];
            NSString *history = [self stringValue:history_item[@"history"]];
            NSString *lpc_is_deleted = [self stringValue:history_item[@"lpc_is_deleted"]];
            NSString *date_created = [self stringValue:history_item[@"date_created"]];
            NSString *date_modified = [self stringValue:history_item[@"date_modified"]];
            
            // Create managed object
            NSManagedObject *h_mo = [self getEntity:kHistoryEntity attribute:@"id" parameter:hid context:ctx];
            
            // Sorted Date Modified
            NSDate *sorted_date_modified = [NSDate dateFromString:date_modified];
            [h_mo setValue:sorted_date_modified forKey:@"sorted_date_modified"];
            
            // Save data to core data
            [h_mo setValue:hid forKey:@"id"];
            [h_mo setValue:lp_id forKey:@"lp_id"];
            [h_mo setValue:user_id forKey:@"user_id"];
            [h_mo setValue:first_name forKey:@"first_name"];
            [h_mo setValue:last_name forKey:@"last_name"];
            [h_mo setValue:avatar forKey:@"avatar"];
            [h_mo setValue:history forKey:@"history"];
            [h_mo setValue:lpc_is_deleted forKey:@"lpc_is_deleted"];
            [h_mo setValue:date_created forKey:@"date_created"];
            [h_mo setValue:date_modified forKey:@"date_modified"];
            
            [set addObject:h_mo];
        }
        
        [object setValue:set forKey:@"histories"];
    }
}

- (void)requestUpdateLessonPlanStatus:(NSManagedObject *)mo doneBlock:(LessonPlanDoneBlock)doneBlock {
    // PARAMETERS
    NSString *lesson_id = [self stringValue:[mo valueForKey:@"id"]];
    NSString *lesson_status = @"1";
    
    NSLog(@"%s ---- PARAMETERS", __PRETTY_FUNCTION__);
    NSLog(@"lesson id : %@", lesson_id);
    NSLog(@"lesson status : %@", lesson_status);
    
    // REQUEST STRING
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointLessonPlanUpdateStatus, lesson_id]];
    NSLog(@"path : %@", query);
    
    // CONFIGURE REQUEST
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // CONTENT TYPE
    NSString *boundary = [self generateBoundaryString];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // BUILD FORM-DATA
    NSDictionary *meta = @{@"status": lesson_status};
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:meta];
    [request setHTTPBody:httpBody];
    
    // SESSION CONFIGURATION
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    [config setAllowsCellularAccess:YES];
    
    // SESSION OBJECT
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // SESSION TASK
    NSURLSessionDataTask *postTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error : %@", [error localizedDescription]);
            self.errorMessage = [error localizedDescription];
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (data) {
            NSDictionary *response = [self parseResponseData:data];
            
            if (response[@"_meta"] != nil) {
                NSDictionary *meta = response[@"_meta"];
                NSString *status_message = meta[@"status"];
                BOOL flag = [status_message isEqualToString:@"SUCCESS"];
                
                //update locally
                if (flag) {
                    NSPredicate *predicate = [self predicateForKeyPath:@"id" andValue:lesson_id];
                    NSManagedObject *mo = [self getEntity:kLessonPlanEntity predicate:predicate];
                    NSManagedObjectContext *ctx = mo.managedObjectContext;
                    
                    if (mo != nil) {
                        [mo setValue:lesson_status forKey:@"status"];
                        [self saveTreeContext:ctx];
                    }
                }
                
                if (doneBlock) {
                    doneBlock(flag);
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }//end data
    }];//end post task
    
    // START POSTING
    [postTask resume];
}

- (void)requestCreateNewLessonPlan:(NSDictionary *)meta file:(NSDictionary *)object doneBlock:(LessonPlanDoneBlock)doneBlock {
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointLessonPlanCreate]];
    
    NSLog(@"path : %@", query);
    NSLog(@"meta : %@", meta);
    NSLog(@"file : %@", object);
    
    // Configure the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // Boundary
    NSString *boundary = [self generateBoundaryString];
    
    // Set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // Create body
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:meta file:object];
    
    // Set post body
    [request setHTTPBody:httpBody];
    
    // Session configuration
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Use cellular data
    [config setAllowsCellularAccess:YES];
    
    // Session object
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Sesson task
    NSURLSessionDataTask *postTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"error : %@", [error localizedDescription]);
            self.errorMessage = [error localizedDescription];
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        else {
            if (data) {
                NSDictionary *response = [self parseResponseData:data];
                NSLog(@"XXX --- CREATE NEW LESSON PLAN RESPONSE --- XXX %@", response);
                
                if (response[@"_meta"] != nil) {
                    NSDictionary *meta = response[@"_meta"];
                    NSString *status_message = meta[@"status"];
                    BOOL flag = [status_message isEqualToString:@"SUCCESS"];
                    
                    if (doneBlock) {
                        doneBlock(flag);
                    }
                }
                else {
                    if (doneBlock) {
                        doneBlock(NO);
                    }
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    // Start posting
    [postTask resume];
}

- (void)requestCreateNewLessonPlanWithPostBody:(NSDictionary *)body doneBlock:(LessonPlanDoneBlock)doneBlock {
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointLessonPlanCreate]];
    
    NSLog(@"query : %@", query);
    NSLog(@"body : %@", body);
    
    NSDictionary *meta = (NSDictionary *)body[@"meta"];
    NSString *filePath = [self stringValue:body[@"file_path"]];
    
    if ([filePath isEqualToString:@""]) {
        filePath = nil;
    }
    
    // Configure the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // Boundary
    NSString *boundary = [self generateBoundaryString];
    
    // Set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // Create body
    NSData *httpBody = [self createPostBodyWithBoundary:boundary parameters:meta andFileAtPath:filePath];
    
    // Set post body
    [request setHTTPBody:httpBody];
    
    // Session configuration
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Use cellular data
    [config setAllowsCellularAccess:YES];
    
    // Session object
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Sesson task
    NSURLSessionDataTask *postTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"error : %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        else {
            if (data) {
                NSDictionary *response = [self parseResponseData:data];
                BOOL isOkayToParse = [self isItOkayToParseFromAPIUsingThisResponseData:response];
                
                if (isOkayToParse) {
                    NSLog(@"New lesson plan has been successfully created.\n%@", response);
                   
                    if (doneBlock) {
                        doneBlock(YES);
                    }
                }
                else {
                    NSLog(@"New lesson has been created with an error. Probably the file was not successfully uploaded.\n%@", response);
                    
                    if (doneBlock) {
                        doneBlock(NO);
                    }
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    // Start posting
    [postTask resume];
}

- (void)requestCreateNewLessonPlanWithPostBody:(NSDictionary *)body dataBlock:(LessonPlanDataBlock)dataBlock {
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointLessonPlanCreate]];
    
    NSLog(@"query : %@", query);
    NSLog(@"body : %@", body);
    
    NSDictionary *meta = (NSDictionary *)body[@"meta"];
    NSString *filePath = [self stringValue:body[@"file_path"]];
    
    if ([filePath isEqualToString:@""]) {
        filePath = nil;
    }
    
    // Configure the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // Boundary
    NSString *boundary = [self generateBoundaryString];
    
    // Set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // Create body
    NSData *httpBody = [self createPostBodyWithBoundary:boundary parameters:meta andFileAtPath:filePath];
    
    // Set post body
    [request setHTTPBody:httpBody];
    
    // Session configuration
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Use cellular data
    [config setAllowsCellularAccess:YES];
    
    // Session object
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Sesson task
    NSURLSessionDataTask *postTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"error : %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        else {
            if (data) {
                NSDictionary *response = [self parseResponseData:data];
                BOOL isOkayToParse = [self isItOkayToParseFromAPIUsingThisResponseData:response];
                
                if (isOkayToParse) {
                    NSLog(@"New lesson plan has been successfully created.\n%@", response);
                    NSDictionary *records = response[@"records"];
                    NSString *lpid = [self stringValue:records[@"id"]];
                    
                    if (dataBlock) {
                        dataBlock(@{@"lpid": lpid});
                    }
                }
                else {
                    NSLog(@"New lesson has been created with an error. Probably the file was not successfully uploaded.\n%@", response);
                    
                    if (dataBlock) {
                        dataBlock(nil);
                    }
                }
            }
            else {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    // Start posting
    [postTask resume];
}

- (void)requestUpdateLessonPlanWithLessonPlanID:(NSString *)lpid meta:(NSDictionary *)meta doneBlock:(LessonPlanDoneBlock)doneBlock {
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointLessonPlanUpdate, lpid]];
    
    NSLog(@"path : %@", query);
    NSLog(@"meta : %@", meta);
    
    // Configure the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // Boundary
    NSString *boundary = [self generateBoundaryString];
    
    // Set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // Create body
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:meta];
    
    // Set post body
    [request setHTTPBody:httpBody];
    
    // Session configuration
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Use cellular data
    [config setAllowsCellularAccess:YES];
    
    // Session object
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Sesson task
    NSURLSessionDataTask *postTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error : %@", [error localizedDescription]);
            self.errorMessage = [error localizedDescription];
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (data) {
            NSDictionary *response = [self parseResponseData:data];
            NSLog(@"XXX --- UPDATE LESSON PLAN RESPONSE --- XXX %@", response);
            
            if (response[@"_meta"] != nil) {
                NSDictionary *meta = response[@"_meta"];
                NSString *status_message = meta[@"status"];
                BOOL flag = [status_message isEqualToString:@"SUCCESS"];
                
                if (doneBlock) {
                    doneBlock(flag);
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
        else {
            if (doneBlock) {
                doneBlock(NO);
            }
        }
    }];
    
    // Start posting
    [postTask resume];
}

- (void)requestUpdateLessonPlanWithLessonPlanID:(NSString *)lpid andPostBody:(NSDictionary *)body doneBlock:(LessonPlanDoneBlock)doneBlock {
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointLessonPlanUpdate, lpid]];
    
    NSLog(@"query : %@", query);
    NSLog(@"body : %@", body);
    
    // Configure the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // Boundary
    NSString *boundary = [self generateBoundaryString];
    
    // Set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // Create body
    NSDictionary *meta = (NSDictionary *)body[@"meta"];
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:meta];
    
    // Set post body
    [request setHTTPBody:httpBody];
    
    // Session configuration
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Use cellular data
    [config setAllowsCellularAccess:YES];
    
    // Session object
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Sesson task
    NSURLSessionDataTask *postTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error : %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (data) {
            NSDictionary *response = [self parseResponseData:data];
            BOOL isOkayToParse = [self isItOkayToParseFromAPIUsingThisResponseData:response];
            
            if (isOkayToParse) {
                NSLog(@"Lesson plan has been successfully updated.\n%@", response);
                
                if (doneBlock) {
                    doneBlock(YES);
                }
            }
            else {
                NSLog(@"Lesson plan has been updated with an error. Probably the file was not successfully uploaded.\n%@", response);
                
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
        else {
            if (doneBlock) {
                doneBlock(NO);
            }
        }
    }];
    
    // Start posting
    [postTask resume];
}

- (void)requestUpdateLessonPlanFileWithLearningContentID:(NSString *)lcid file:(NSDictionary *)object doneBlock:(LessonPlanDoneBlock)doneBlock {
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointLessonPlanUpdateFile, lcid]];
    
    NSLog(@"path : %@", query);
    NSLog(@"file : %@", object);
    
    // Configure the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // Boundary
    NSString *boundary = [self generateBoundaryString];
    
    // Set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // Create body
    NSData *httpBody = [self createBodyWithBoundary:boundary file:object];
    
    // Set post body
    [request setHTTPBody:httpBody];
    
    // Session configuration
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Use cellular data
    [config setAllowsCellularAccess:YES];
    
    // Session object
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Sesson task
    NSURLSessionDataTask *postTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if (error) {
            NSLog(@"error : %@", [error localizedDescription]);
            self.errorMessage = [error localizedDescription];
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (data) {
            NSDictionary *response = [self parseResponseData:data];
            NSLog(@"XXX --- UPLOAD FILE RESPONSE --- XXX %@", response);
            
            if (response[@"_meta"] != nil) {
                NSDictionary *meta = response[@"_meta"];
                NSString *status_message = meta[@"status"];
                BOOL flag = [status_message isEqualToString:@"SUCCESS"];
                
                if (doneBlock) {
                    doneBlock(flag);
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
        else {
            if (doneBlock) {
                doneBlock(NO);
            }
        }
    }];
    
    // Start posting
    [postTask resume];
}

- (void)requestUpdateLessonPlanFile:(NSString *)newFileAtPath forLearningContentWithID:(NSString *)lcid doneBlock:(LessonPlanDoneBlock)doneBlock {
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointLessonPlanUpdateFile, lcid]];
    
    NSLog(@"query : %@", query);
    NSLog(@"file path : %@", newFileAtPath);
    
    if ([newFileAtPath isEqualToString:@""]) {
        newFileAtPath = nil;
    }
    
    // Configure the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // Boundary
    NSString *boundary = [self generateBoundaryString];
    
    // Set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // Create body
    NSData *httpBody = [self createBodyWithBoundary:boundary andFileAtPath:newFileAtPath];
    
    // Set post body
    [request setHTTPBody:httpBody];
    
    // Session configuration
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    // Use cellular data
    [config setAllowsCellularAccess:YES];
    
    // Session object
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // Sesson task
    NSURLSessionDataTask *postTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if (error) {
            NSLog(@"error : %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (data) {
            NSDictionary *response = [self parseResponseData:data];
            BOOL isOkayToParse = [self isItOkayToParseFromAPIUsingThisResponseData:response];
            
            if (isOkayToParse) {
                NSLog(@"File has been successfully updated.\n%@", response);
                
                if (doneBlock) {
                    doneBlock(YES);
                }
            }
            else {
                NSLog(@"File was not updated successfully.\n%@", response);
                
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
        else {
            if (doneBlock) {
                doneBlock(NO);
            }
        }
    }];
    
    // Start posting
    [postTask resume];
}

- (void)requestRemoveLessonPlan:(NSManagedObject *)mo doneBlock:(LessonPlanDoneBlock)doneBlock {
    NSString *lp_id = [self stringValue:[mo valueForKey:@"id"]];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointLessonPlanDelete, lp_id]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
    {
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            self.errorMessage = [error localizedDescription];
            
            if (doneBlock){
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isSuccess = [self isItOkayToParseFromAPIUsingThisResponseData:dictionary];

            if (isSuccess) {
                NSArray *records = dictionary[@"records"];
                
                if (records.count > 0) {
                    NSLog(@"deleted lesson records : %@", records);
                }
                
                //delete locally
                NSPredicate *predicate = [self predicateForKeyPath:@"id" andValue:lp_id];
                [self clearContentsForEntity:kLessonPlanEntity predicate:predicate];
                
                if (doneBlock) {
                    doneBlock(YES);
                }
                
            }//end success
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }//end not error
    }];//end task
    
    [task resume];
}

- (void)requestPostLessonPlanComment:(NSManagedObject *)mo comment:(NSString *)comment doneBlock:(LessonPlanDoneBlock)doneBlock {
    //PARAMETERS
    NSString *lesson_id = [self stringValue:[mo valueForKey:@"id"]];
    NSString *user_id = [self stringValue:[mo valueForKey:@"user_id"]];
    NSString *post_comment = comment;
    
    NSLog(@"%s ---- PARAMETERS", __PRETTY_FUNCTION__);
    NSLog(@"user id : %@", user_id);
    NSLog(@"comment : %@", post_comment);
    
    // REQUEST STRING
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointLessonPlanAddComment, lesson_id]];
    NSLog(@"path : %@", query);
    
    // CONFIGURE REQUEST
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // CONTENT TYPE
    NSString *boundary = [self generateBoundaryString];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // BUILD FORM-DATA
    NSDictionary *meta = @{@"user_id": user_id, @"comment": comment, @"is_comment": @"1"};
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:meta];
    [request setHTTPBody:httpBody];
    
    // SESSION CONFIGURATION
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    [config setAllowsCellularAccess:YES];
    
    // SESSION OBJECT
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // SESSION TASK
    NSURLSessionDataTask *postTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error : %@", [error localizedDescription]);
            self.errorMessage = [error localizedDescription];
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (data) {
            NSDictionary *response = [self parseResponseData:data];
            
            if (response[@"_meta"] != nil) {
                NSDictionary *meta = response[@"_meta"];
                NSString *status_message = meta[@"status"];
                BOOL flag = [status_message isEqualToString:@"SUCCESS"];
                
                if (flag == YES) {
                    if (doneBlock) {
                        doneBlock(flag);
                    }
                }
            }
        }//end data
    }];//end session data task
    
    // START POSTING
    [postTask resume];
}

- (void)requestPostComment:(NSDictionary *)commentData doneBlock:(LessonPlanDoneBlock)doneBlock {
    //PARAMETERS
    NSString *lesson_id = [self stringValue:commentData[@"lp_id"]];
    NSString *user_id = [self stringValue:commentData[@"user_id"]];
    NSString *comment = [self stringValue:commentData[@"comment"]];
    
    NSLog(@"%s ---- PARAMETERS", __PRETTY_FUNCTION__);
    NSLog(@"user id : %@", user_id);
    NSLog(@"comment : %@", comment);
    
    // REQUEST STRING
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointLessonPlanAddComment, lesson_id]];
    NSLog(@"path : %@", query);
    
    // CONFIGURE REQUEST
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // CONTENT TYPE
    NSString *boundary = [self generateBoundaryString];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // BUILD FORM-DATA
    NSDictionary *meta = @{@"user_id": user_id, @"comment": comment, @"is_comment": @"1"};
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:meta];
    [request setHTTPBody:httpBody];
    
    // SESSION CONFIGURATION
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    [config setAllowsCellularAccess:YES];
    
    // SESSION OBJECT
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // SESSION TASK
    NSURLSessionDataTask *postTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error : %@", [error localizedDescription]);
            self.errorMessage = [error localizedDescription];
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (data) {
            NSDictionary *response = [self parseResponseData:data];
            
            if (response[@"_meta"] != nil) {
                NSDictionary *meta = response[@"_meta"];
                NSString *status_message = meta[@"status"];
                BOOL flag = [status_message isEqualToString:@"SUCCESS"];
                
                if (flag == YES) {
                    if (doneBlock) {
                        doneBlock(flag);
                    }
                }
            }
        }//end data
    }];//end session data task
    
    // START POSTING
    [postTask resume];
}

- (void)requestUpdateLessonPlanComment:(NSManagedObject *)mo comment:(NSString *)comment doneBlock:(LessonPlanDoneBlock)doneBlock {
    //PARAMETERS
    NSString *comment_id = [self stringValue:[mo valueForKey:@"id"]];
    NSString *updated_comment = comment;
    
    NSLog(@"%s ---- PARAMETERS", __PRETTY_FUNCTION__);
    NSLog(@"comment id : %@", comment_id);
    NSLog(@"updated comment : %@", updated_comment);
    
    // REQUEST STRING
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointLessonPlanEditComment]];
    NSLog(@"path : %@", query);
    
    // CONFIGURE REQUEST
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // CONTENT TYPE
    NSString *boundary = [self generateBoundaryString];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // BUILD FORM-DATA
    NSDictionary *meta = @{@"comment_id": comment_id, @"comment": updated_comment, @"is_comment": @"1"};
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:meta];
    [request setHTTPBody:httpBody];
    
    // SESSION CONFIGURATION
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    [config setAllowsCellularAccess:YES];
    
    // SESSION OBJECT
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    NSURLSessionDataTask *postTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error : %@", [error localizedDescription]);
            self.errorMessage = [error localizedDescription];
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (data) {
            NSDictionary *response = [self parseResponseData:data];
            
            if (response[@"_meta"] != nil) {
                NSDictionary *meta = response[@"_meta"];
                NSString *status_message = meta[@"status"];
                BOOL flag = [status_message isEqualToString:@"SUCCESS"];
                
                if (flag) {
                    if (doneBlock) {
                        doneBlock(flag);
                    }
                }
            }
        }//end data
    }];//end session data task
    
    // START POSTING
    [postTask resume];
}

- (void)requestDeleteLessonPlanComment:(NSManagedObject *)mo doneBlock:(LessonPlanDoneBlock)doneBlock {
    NSString *commentid = [self stringValue:[mo valueForKey:@"id"]];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointLessonPlanDeleteComment, commentid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
         if (error) {
             NSLog(@"error %@", [error localizedDescription]);
             self.errorMessage = [error localizedDescription];
             
             if (doneBlock) {
                 doneBlock(NO);
             }
         }
         
         if (!error) {
             NSDictionary *dictionary = [self parseResponseData:responsedata];
             NSDictionary *parsedmeta = dictionary[@"_meta"];
             NSString *metastatus = parsedmeta[@"status"];
             
             NSLog(@"deleted lesson comment dictionary: %@", dictionary);
             NSLog(@"deleted lesson comment parsedmeta: %@", parsedmeta);
             NSLog(@"deleted lesson comment metastatus: %@", metastatus);
             
             BOOL isSuccess = NO;
             
             if ([metastatus isEqualToString:@"SUCCESS"]) {
                 isSuccess = YES;
             }
             
             if (isSuccess) {
                 NSArray *records = dictionary[@"records"];
                 NSPredicate *predicate = [self predicateForKeyPath:@"id" andValue:commentid];
                 [self clearContentsForEntity:kCommentEntity predicate:predicate];
                 
                 if (records.count > 0) {
                     NSLog(@"deleted lesson comment records : %@", records);
                     // NSManagedObjectContext *ctx = self.workerContext;
                     //
                     // [ctx performBlock:^{
                     //     [self saveTreeContext:ctx];
                     //
                     //     if (doneBlock) {
                     //         doneBlock(YES);
                     //     }
                     //
                     // }];//end context perform block
                 }//end has record
             }//end success
             
             if (doneBlock) {
                 doneBlock(isSuccess);
             }
         }//end not error
     }];//end task
    
    [task resume];
}

#pragma mark - DOWNLOAD FILE

- (void)requestDownloadPDFFileWithLessonPlanID:(NSString *)lessonplanid contentBlock:(LessonPlanContent)contentBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointLessonPlanDownload, lessonplanid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDownloadTask *downloadTask = [self.session downloadTaskWithRequest:request completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            self.errorMessage = [error localizedDescription];
            
            if (contentBlock) {
                contentBlock(nil);
            }
        }
        
        if (!error) {
            if (location) {
                NSString *strLocation = [NSString stringWithFormat:@"%@", location];
                NSString *filename = [NSString stringWithFormat:@"lessonplan_%@.pdf", lessonplanid];
                NSArray *content = @[strLocation, filename];
                
                if (contentBlock) {
                    contentBlock(content);
                }
            }
        }
        
    }];//end download task
    
    [downloadTask resume];
}

- (void)requestDownloadLessonPlanWithID:(NSString *)lessonid contentBlock:(LessonPlanContent)contentBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointLessonPlanDownload, lessonid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDownloadTask *downloadTask = [self.session downloadTaskWithRequest:request completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (contentBlock) {
                contentBlock(nil);
            }
        }
        
        if (!error) {
            NSLog(@"response: %@", response);
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            long int statusCode = (long)[httpResponse statusCode];
            
            if (statusCode == 200) {
                if (location) {
                    long long int imfSize = [response expectedContentLength];
                    NSNumber *fileSize = [NSNumber numberWithLongLong:imfSize];
                    
                    // Workaround: Unknown file size
                    if (imfSize < 0) {
                        fileSize = [NSNumber numberWithLongLong:0];
                    }
                    
                    NSString *filePath = [NSString stringWithFormat:@"%@", location];
                    NSData *fileData = [NSData dataWithContentsOfURL:[NSURL URLWithString:filePath]];
                    NSString *filename = [self stringValue:[response suggestedFilename]];
                    
                    if (contentBlock) {
                        contentBlock(@[fileSize, fileData, filename]);
                    }
                }
                else {
                    if (contentBlock) {
                        contentBlock(nil);
                    }
                }
            }
            else {
                if (contentBlock) {
                    contentBlock(nil);
                }
            }
        }
    }];
    
    [downloadTask resume];
}

- (NSString *)findExtensionOfFileInURL:(NSURL *)url {
    NSString *urlString = [url absoluteString];
    NSArray *componentsArray = [urlString componentsSeparatedByString:@"."];
    NSString *fileExtension = [self stringValue:[componentsArray lastObject]];
    return  fileExtension;
}

- (void)downloadRubricsWithURL:(NSString *)url contentID:(NSString *)lcid andLessonID:(NSString *)lpid {
    NSURL *link = [NSURL URLWithString:url];
    NSURLSessionDownloadTask *downloadTask = [self.session downloadTaskWithURL:link completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        
        if (location) {
            NSLog(@"%s link: %@", __PRETTY_FUNCTION__, location.absoluteString);
            NSData *data = [NSData dataWithContentsOfURL:location];
            NSManagedObjectContext *ctx = self.workerContext;
            
            // Create managed object
            NSManagedObject *mo = [self getEntity:kRubricsEntity attribute:@"lc_id" parameter:lcid context:ctx];
            
            if (mo) {
                if (data) {
                    // Sava data to core data
                    [mo setValue:lcid forKey:@"lc_id"];
                    [mo setValue:lpid forKey:@"lp_id"];
                    [mo setValue:data forKey:@"file"];
                    [self saveTreeContext:ctx];
                }//end data
            }//end mo
        }//end location
    }];//end download task
    
    [downloadTask resume];
}

- (void)saveFilesInCoreData:(NSArray *)files doneBlock:(LessonPlanDoneBlock)doneBlock {
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kFileEntity withPredicate:nil context:ctx];
    
    if (files.count > 0) {
        NSLog(@"files to save in core data : %@", files);
        
        [ctx performBlock:^{
            for (NSDictionary *d in files) {
                NSNumber *fsfn = d[@"fsfn"];
                NSString *sdui = [self stringValue:d[@"sdui"]];
                NSString *name = [self stringValue:d[@"name"]];
                NSString *type = [self stringValue:d[@"type"]];
                NSString *path = [self stringValue:d[@"path"]];
                NSNumber *size = d[@"size"];
                NSDate *date = d[@"date"];
                
                NSManagedObject *mo = [self getEntity:kFileEntity attribute:@"name" parameter:name context:ctx];
                
                [mo setValue:@([fsfn integerValue]) forKey:@"fsfn"];
                [mo setValue:sdui forKey:@"sdui"];
                [mo setValue:name forKey:@"name"];
                [mo setValue:type forKey:@"type"];
                [mo setValue:path forKey:@"path"];
                [mo setValue:@([size integerValue]) forKey:@"size"];
                [mo setValue:date forKey:@"date"];
            }
            
            [self saveTreeContext:ctx];
            
            if (doneBlock) {
                doneBlock(YES);
            }
        }];
    }
    else {
        if (doneBlock) {
            doneBlock(YES);
        }
    }
}

#pragma mark - TEMPLATE API

- (void)requestLessonTemplateList:(LessonPlanDoneBlock)doneBlock {
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kTemplateEntity withPredicate:nil context:ctx];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointLessonPlanTemplateList]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            self.errorMessage = [error localizedDescription];
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            NSDictionary *parsedmeta = dictionary[@"_meta"];
            NSString *metastatus = parsedmeta[@"status"];
            
            NSLog(@"lesson template dictionary: %@", dictionary);
            NSLog(@"lesson template parsedmeta: %@", parsedmeta);
            NSLog(@"lesson template metastatus: %@", metastatus);
            
            BOOL isSuccess = NO;
            
            if ([metastatus isEqualToString:@"SUCCESS"]) {
                isSuccess = YES;
            }

            if (isSuccess) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"lesson template records: %@", records);
                
                if (records.count > 0) {
                    [ctx performBlock:^{
                        
                        for (NSDictionary *d in records) {
                            NSLog(@"lesson template data: %@", d);
                            
                            // Parse data from API
                            NSString *template_id = [self stringValue:d[@"id"]];
                            NSString *template_name = [self stringValue:d[@"name"]];
                            NSString *is_deleted = [self stringValue:d[@"is_deleted"]];
                            NSString *date_created = [self stringValue:d[@"date_created"]];
                            NSString *date_modified = [self stringValue:d[@"date_modified"]];
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:kLessonTemplateEntity attribute:@"id" parameter:template_id context:ctx];
                            
                            // Save data to core data
                            [mo setValue:template_id forKey:@"id"];
                            [mo setValue:template_name forKey:@"name"];
                            [mo setValue:is_deleted forKey:@"is_deleted"];
                            [mo setValue:date_created forKey:@"date_created"];
                            [mo setValue:date_modified forKey:@"date_modified"];
                        }//end for loop
                        
                        [self saveTreeContext:ctx];
                        
                        if (doneBlock) {
                            doneBlock(YES);
                        }
                        
                    }];//end context perform block
                }//end has record
            }//end success
        }//end not error
    }];//end task
    
    [task resume];
}

- (void)requestLessonTemplateForLessonTemplateWithID:(NSString *)templateid doneBlock:(LessonPlanDoneBlock)doneBlock {
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kTemplateEntity withPredicate:nil context:ctx];
    [self clearDataForEntity:kTemplateContentEntity withPredicate:nil context:ctx];
    [self clearDataForEntity:kTemplateProcessEntity withPredicate:nil context:ctx];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointLessonPlanTemplateDetails, templateid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            self.errorMessage = [error localizedDescription];
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            NSDictionary *parsedmeta = dictionary[@"_meta"];
            NSString *metastatus = parsedmeta[@"status"];
            
            NSLog(@"lesson template details dictionary: %@", dictionary);
            NSLog(@"lesson template details parsedmeta: %@", parsedmeta);
            NSLog(@"lesson template details metastatus: %@", metastatus);
            
            BOOL isSuccess = NO;
            
            if ([metastatus isEqualToString:@"SUCCESS"]) {
                isSuccess = YES;
            }
            
            if (isSuccess) {
                NSDictionary *records = dictionary[@"records"];
                NSLog(@"lesson template details records: %@", records);
                
                if (records.count > 0) {
                    [ctx performBlock:^{
                        // Parse data from API
                        NSString *template_id = [self stringValue:records[@"template_id"]];
                        NSString *template_name = [self stringValue:records[@"template_name"]];
                        
                        // Create managed object
                        NSManagedObject *mo = [self getEntity:kTemplateEntity attribute:@"template_id" parameter:template_id context:ctx];
                        
                        // Save data to core data
                        [mo setValue:template_id forKey:@"template_id"];
                        [mo setValue:template_name forKey:@"template_name"];
                        
                        // Parse template contents from API
                        NSArray *contents = records[@"contents"];
                        [self processTemplateContents:contents managedObject:mo];
                        
                        [self saveTreeContext:ctx];
                        
                        if (doneBlock) {
                            doneBlock(YES);
                        }
                        
                    }];//end context perform block
                }//end has record
                else {
                    if (doneBlock) {
                        doneBlock(NO);
                    }
                }
            }//end success
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }//end not error
    }];//end task
    
    [task resume];
}

- (void)processTemplateContents:(NSArray *)contentlist managedObject:(NSManagedObject *)object {
    NSManagedObjectContext *ctx = object.managedObjectContext;
    
    if (contentlist.count > 0) {
        NSMutableSet *set = [NSMutableSet set];
        
        for (NSDictionary *content_item in contentlist ) {
            NSLog(@"lesson template details content data: %@", content_item);
            
            // Parse data from API
            NSString *stage_id = [self stringValue:content_item[@"stage_id"]];
            NSString *stage_name = [self stringValue:content_item[@"stage_name"]];
            
            // Create managed object
            NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:kTemplateContentEntity inManagedObjectContext:ctx];
            
            // Save data to core data
            [c_mo setValue:stage_id forKey:@"stage_id"];
            [c_mo setValue:stage_name forKey:@"stage_name"];
            
            // Parse template content processes from API
            NSArray *processes = content_item[@"process"];
            [self processTemplateProcesses:processes managedObject:c_mo];
            
            [set addObject:c_mo];
        }
        
        [object setValue:set forKey:@"contents"];
    }
}

- (void)processTemplateProcesses:(NSArray *)processlist managedObject:(NSManagedObject *)object {
    NSManagedObjectContext *ctx = object.managedObjectContext;
    
    if (processlist.count > 0) {
        NSMutableSet *set = [NSMutableSet set];
        
        for (NSDictionary *process_item in processlist ) {
            NSLog(@"lesson template details process data: %@", process_item);
            
            // Parse data from API
            NSString *lps_id = [self stringValue:process_item[@"learning_process_stage_id"]];
            NSString *process_id = [self stringValue:process_item[@"process_id"]];
            NSString *process_name = [self stringValue:process_item[@"process_name"]];
            NSString *is_file = [self stringValue:process_item[@"is_file"]];
            
            // Create managed object
            NSManagedObject *p_mo = [NSEntityDescription insertNewObjectForEntityForName:kTemplateProcessEntity inManagedObjectContext:ctx];
            
            // Save data to core data
            [p_mo setValue:lps_id forKey:@"lps_id"];
            [p_mo setValue:process_id forKey:@"process_id"];
            [p_mo setValue:process_name forKey:@"process_name"];
            [p_mo setValue:is_file forKey:@"is_file"];
            
            [set addObject:p_mo];
        }
        
        [object setValue:set forKey:@"processes"];
    }
}

#pragma mark - CURRICULUM API

- (void)requestApprovedCurriculumList:(LessonPlanDoneBlock)doneBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCurriculumApprovedList]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            NSDictionary *parsedmeta = dictionary[@"_meta"];
            NSString *metastatus = parsedmeta[@"status"];
            
            NSLog(@"approved curriculum dictionary: %@", dictionary);
            NSLog(@"approved curriculum parsedmeta: %@", parsedmeta);
            NSLog(@"approved curriculum metastatus: %@", metastatus);
            
            BOOL isSuccess = NO;
            
            if ([metastatus isEqualToString:@"SUCCESS"]) {
                isSuccess = YES;
            }
            
            if (isSuccess) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"approved curriculum records: %@", records);
                
                if (records.count > 0) {
                    NSManagedObjectContext *ctx = self.workerContext;
                    
                    [ctx performBlock:^{
                        
                        for (NSDictionary *d in records) {
                            NSLog(@"approved curriculum data: %@", d);
                            
                            // Parse data from API
                            NSString *template_type = [self stringValue:d[@"template_type"]];
                            NSString *curriculum_id = [self stringValue:d[@"curriculum_id"]];
                            NSString *curriculum_template_id = [self stringValue:d[@"curriculum_template_id"]];
                            NSString *curriculum_template_name = [self stringValue:d[@"curriculum_template_name"]];
                            NSString *curriculum_title = [self stringValue:d[@"curriculum_title"]];
                            NSString *curriculum_description = [self stringValue:d[@"curriculum_description"]];
                            NSString *curriculum_status = [self stringValue:d[@"curriculum_status"]];
                            NSString *curriculum_status_remarks = [self stringValue:d[@"curriculum_status_remarks"]];
                            NSString *course_id = [self stringValue:d[@"course_id"]];
                            NSString *course_name = [self stringValue:d[@"course_name"]];
                            NSString *grade_level_id= [self stringValue:d[@"grade_level_id"]];
                            NSString *grade_level_name = [self stringValue:d[@"grade_level_name"]];
                            NSString *school_profile_id = [self stringValue:d[@"school_profile_id"]];
                            NSString *school_profile_name = [self stringValue:d[@"school_profile_name"]];
                            NSString *sy_id = [self stringValue:d[@"sy_id"]];
                            NSString *sy_name = [self stringValue:d[@"sy_name"]];
                            NSString *created_by = [self stringValue:d[@"created_by"]];
                            NSString *created_by_id = [self stringValue:d[@"created_by_id"]];
                            NSString *date_created = [self stringValue:d[@"date_created"]];
                            NSString *date_modified = [self stringValue:d[@"date_modified"]];
                            
                            // Create managed object
                            NSManagedObject *mo = [self getEntity:kCurriculumEntity attribute:@"curriculum_id" parameter:curriculum_id context:ctx];
                            
                            // Save data to core data
                            [mo setValue:template_type forKey:@"template_type"];
                            [mo setValue:curriculum_id forKey:@"curriculum_id"];
                            [mo setValue:curriculum_template_id forKey:@"curriculum_template_id"];
                            [mo setValue:curriculum_template_name forKey:@"curriculum_template_name"];
                            [mo setValue:curriculum_title forKey:@"curriculum_title"];
                            [mo setValue:curriculum_description forKey:@"curriculum_description"];
                            [mo setValue:curriculum_status forKey:@"curriculum_status"];
                            [mo setValue:curriculum_status_remarks forKey:@"curriculum_status_remarks"];
                            [mo setValue:course_id forKey:@"course_id"];
                            [mo setValue:course_name forKey:@"course_name"];
                            [mo setValue:grade_level_id forKey:@"grade_level_id"];
                            [mo setValue:grade_level_name forKey:@"grade_level_name"];
                            [mo setValue:school_profile_id forKey:@"school_profile_id"];
                            [mo setValue:school_profile_name forKey:@"school_profile_name"];
                            [mo setValue:sy_id forKey:@"sy_id"];
                            [mo setValue:sy_name forKey:@"sy_name"];
                            [mo setValue:created_by forKey:@"created_by"];
                            [mo setValue:created_by_id forKey:@"created_by_id"];
                            [mo setValue:date_created forKey:@"date_created"];
                            [mo setValue:date_modified forKey:@"date_modified"];
                        }//end for loop
                        
                        [self saveTreeContext:ctx];
                        
                        if (doneBlock) {
                            doneBlock(YES);
                        }

                    }];//end context perform block
                }//end has record
            }//end success
        }//end not error
    }];//end task
    
    [task resume];
}

- (void)requestPeriodsForCurriculum:(NSManagedObject *)mo doneBlock:(LessonPlanDoneBlock)doneBlock {
    NSString *curriculum_id = [mo valueForKey:@"curriculum_id"];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCurriculumDetails, curriculum_id]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
          }
                                                     
          if (!error) {
              NSDictionary *dictionary = [self parseResponseData:responsedata];
              NSDictionary *parsedmeta = dictionary[@"_meta"];
              NSString *metastatus = parsedmeta[@"status"];
              
              NSLog(@"curriculum details dictionary: %@", dictionary);
              NSLog(@"curriculum details parsedmeta: %@", parsedmeta);
              NSLog(@"curriculum details metastatus: %@", metastatus);
              
              BOOL isSuccess = NO;
              
              if ([metastatus isEqualToString:@"SUCCESS"]) {
                  isSuccess = YES;
              }

              if (isSuccess) {
                  NSDictionary *d = dictionary[@"records"];
                  NSLog(@"curriculum details records: %@", d);
                  
                  // Use existing managed context
                  NSManagedObjectContext *ctx = mo.managedObjectContext;
                  
                  [ctx performBlock:^{
                      // Parse data from API
                      NSString *curriculum_id = [self stringValue:d[@"curriculum_id"]];
                      NSString *curriculum_template_id = [self stringValue:d[@"curriculum_template_id"]];
                      NSString *curriculum_template_name = [self stringValue:d[@"curriculum_template_name"]];
                      NSString *curriculum_title = [self stringValue:d[@"curriculum_title"]];
                      NSString *curriculum_description = [self stringValue:d[@"curriculum_description"]];
                      NSString *curriculum_status = [self stringValue:d[@"curriculum_status"]];
                      NSString *curriculum_status_remarks = [self stringValue:d[@"curriculum_status_remarks"]];
                      NSString *course_id = [self stringValue:d[@"course_id"]];
                      NSString *course_name = [self stringValue:d[@"course_name"]];
                      NSString *grade_level_id = [self stringValue:d[@"grade_level_id"]];
                      NSString *grade_level_name = [self stringValue:d[@"grade_level_name"]];
                      NSString *school_profile_id = [self stringValue:d[@"school_profile_id"]];
                      NSString *school_profile_name = [self stringValue:d[@"school_profile_name"]];
                      NSString *sy_id = [self stringValue:d[@"sy_id"]];
                      NSString *sy_name = [self stringValue:d[@"sy_name"]];
                      NSString *created_by = [self stringValue:d[@"created_by"]];
                      NSString *created_by_id = [self stringValue:d[@"created_by_id"]];
                      NSString *date_created = [self stringValue:d[@"date_created"]];
                      NSString *date_modified = [self stringValue:d[@"date_modified"]];
                      
                      // Save data to core data
                      [mo setValue:curriculum_id forKey:@"curriculum_id"];
                      [mo setValue:curriculum_template_id forKey:@"curriculum_template_id"];
                      [mo setValue:curriculum_template_name forKey:@"curriculum_template_name"];
                      [mo setValue:curriculum_title forKey:@"curriculum_title"];
                      [mo setValue:curriculum_description forKey:@"curriculum_description"];
                      [mo setValue:curriculum_status forKey:@"curriculum_status"];
                      [mo setValue:curriculum_status_remarks forKey:@"curriculum_status_remarks"];
                      [mo setValue:course_id forKey:@"course_id"];
                      [mo setValue:course_name forKey:@"course_name"];
                      [mo setValue:grade_level_id forKey:@"grade_level_id"];
                      [mo setValue:grade_level_name forKey:@"grade_level_name"];
                      [mo setValue:school_profile_id forKey:@"school_profile_id"];
                      [mo setValue:school_profile_name forKey:@"school_profile_name"];
                      [mo setValue:sy_id forKey:@"sy_id"];
                      [mo setValue:sy_name forKey:@"sy_name"];
                      [mo setValue:created_by forKey:@"created_by"];
                      [mo setValue:created_by_id forKey:@"created_by_id"];
                      [mo setValue:date_created forKey:@"date_created"];
                      [mo setValue:date_modified forKey:@"date_modified"];
                      
                      // Parse curriculum periods
                      NSArray *periods = d[@"periods"];
                      [self processPeriods:periods managedObject:mo];
                      
                }];//end context perform block
              }//end success
          }//end not error
        }];//end task
    
    [task resume];
}

- (void)processPeriods:(NSArray *)periodlist managedObject:(NSManagedObject *)object {
    NSManagedObjectContext *ctx = object.managedObjectContext;
    
    if (periodlist.count > 0) {
        NSMutableSet *set = [NSMutableSet set];
        
        for (NSDictionary *period_item in periodlist ) {
            NSLog(@"detailed curriculum periods data: %@", period_item);
            
            // Parse data from API
            NSString *period_id = [self stringValue:period_item[@"period_id"]];
            NSString *name = [self stringValue:period_item[@"name"]];
            NSString *date_created = [self stringValue:period_item[@"date_created"]];
            NSString *date_modified = [self stringValue:period_item[@"date_modified"]];
            
            // Create managed object
            NSManagedObject *p_mo = [NSEntityDescription insertNewObjectForEntityForName:kCurriculumPeriodEntity inManagedObjectContext:ctx];
            
            // Save data to core data
            [p_mo setValue:period_id forKey:@"period_id"];
            [p_mo setValue:name forKey:@"name"];
            [p_mo setValue:date_created forKey:@"date_created"];
            [p_mo setValue:date_modified forKey:@"date_modified"];

            // Parse curriculum period contents
            NSArray *contents = period_item[@"content"];
            [self processCurriculumContents:contents managedObject:p_mo];
            
            [set addObject:p_mo];
        }
        
        [object setValue:set forKey:@"periods"];
    }
}

- (void)processCurriculumContents:(NSArray *)contentlist managedObject:(NSManagedObject *)object {
    NSManagedObjectContext *ctx = object.managedObjectContext;
    
    if (contentlist.count > 0) {
        NSMutableSet *set = [NSMutableSet set];
        
        for (NSDictionary *content_item in contentlist) {
            NSLog(@"detailed curriculum contents data: %@", content_item);
            
            // Parse data from API
            NSString *content_id = [self stringValue:content_item[@"content_id"]];
            NSString *domain = [self stringValue:content_item[@"domain"]];
            NSString *date_created = [self stringValue:content_item[@"date_created"]];
            NSString *date_modified = [self stringValue:content_item[@"date_modified"]];
            
            // Create managed object
            NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:kCurriculumContentEntity inManagedObjectContext:ctx];
            
            // Save data to core data
            [c_mo setValue:content_id forKey:@"content_id"];
            [c_mo setValue:domain forKey:@"domain"];
            [c_mo setValue:date_created forKey:@"date_created"];
            [c_mo setValue:date_modified forKey:@"date_modified"];
            
            // Parse curriculum period lesson plans
            NSArray *lesson_plans = content_item[@"lesson_plans"];
            [self processLessonPlans:lesson_plans managedObject:c_mo];
            
            [set addObject:c_mo];
        }
        
        [object setValue:set forKey:@"contents"];
    }
}

- (void)processLessonPlans:(NSArray *)lessonlist managedObject:(NSManagedObject *)object {
    NSManagedObjectContext *ctx = object.managedObjectContext;
    
    if (lessonlist.count > 0) {
        NSMutableSet *set = [NSMutableSet set];
        
        for (NSDictionary *lesson_item in lessonlist) {
            NSLog(@"detailed curriculum lesson plans data: %@", lesson_item);
            
            // Parse data from API
            NSString *lid = [self stringValue:lesson_item[@"id"]];
            NSString *lesson_plan_id = [self stringValue:lesson_item[@"lesson_plan_id"]];
            
            // Create managed object
            NSManagedObject *l_mo = [NSEntityDescription insertNewObjectForEntityForName:kCurriculumLessonPlanEntity inManagedObjectContext:ctx];
            
            // Save data to core data
            [l_mo setValue:lid forKey:@"id"];
            [l_mo setValue:lesson_plan_id forKey:@"lesson_plan_id"];
    
            [set addObject:l_mo];
        }
        
        [object setValue:set forKey:@"lessons"];
    }
}

- (void)requestCurriculumLearningCompetenciesForPeriodWithObject:(NSManagedObject *)object dataBlock:(LessonPlanDataBlock)dataBlock {
    NSString *periodid = [self stringValue:[object valueForKey:@"period_id"]];
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointCurriculumLearningCompetencies, periodid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
    {
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            NSDictionary *parsedmeta = dictionary[@"_meta"];
            NSString *metastatus = parsedmeta[@"status"];
            
            NSLog(@"curriculum learning competencies dictionary: %@", dictionary);
            NSLog(@"curriculum learning competencies parsedmeta: %@", parsedmeta);
            NSLog(@"curriculum learning competencies metastatus: %@", metastatus);
            
            BOOL isSuccess = NO;
            
            if ([metastatus isEqualToString:@"SUCCESS"]) {
                isSuccess = YES;
            }

            if (isSuccess) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"curriculum learning competencies records: %@", records);
                
                if (records.count > 0) {
                    // Use existing managed context
                    NSManagedObjectContext *ctx = object.managedObjectContext;
                    
                    [ctx performBlock:^{
                        NSMutableSet *learningCompetencies = [NSMutableSet set];
                        
                        for (NSDictionary *d in records) {
                            NSLog(@"curriculum learning compentencies data: %@", d);
                            
                            // Parse data from API
                            NSString *content_id = [self stringValue:d[@"content_id"]];
                            NSString *domain = [self stringValue:d[@"domain"]];
                            NSString *date_created = [self stringValue:d[@"date_created"]];
                            NSString *date_modified = [self stringValue:d[@"date_modified"]];
                            NSString *selected = [self stringValue:d[@"selected"]];
                            
                            // Create managed object
                            NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kLearningCompentencyEntity inManagedObjectContext:ctx];
                            
                            // Save data to core data
                            [mo setValue:content_id forKey:@"content_id"];
                            [mo setValue:domain forKey:@"domain"];
                            [mo setValue:date_created forKey:@"date_created"];
                            [mo setValue:date_modified forKey:@"date_modified"];
                            [mo setValue:selected forKey:@"selected"];
                            
                            // Parse learning competencies content values
                            NSArray *content_values = d[@"content_values"];
                            [self processLCContentValues:content_values managedObject:mo];
                            
                            // Parse learning competencies lesson plans
                            NSArray *lesson_plans = d[@"lesson_plans"];
                            [self processLCLessonPlans:lesson_plans managedObject:mo];
                            
                            [learningCompetencies addObject:mo];
                        }
                        
                        // Save learning competency to core data
                        if (learningCompetencies.count > 0) {
                            [object setValue:learningCompetencies forKey:@"competencies"];
                        }
                        
                        [self saveTreeContext:ctx];
                        
                        NSDictionary *dataObject = @{@"status":@(YES), @"competencies":learningCompetencies};
                        
                        if (dataBlock) {
                            dataBlock(dataObject);
                        }
                        
                    }];//end context perform block
                }//end has record
            }//end success
        }//end not error
    }];//end task
    
    [task resume];
}

- (void)processLCContentValues:(NSArray *)contentlist managedObject:(NSManagedObject *)object {
    NSManagedObjectContext *ctx = object.managedObjectContext;
    
    if (contentlist.count > 0) {
        NSMutableSet *set = [NSMutableSet set];
        
        for (NSDictionary *content_item in contentlist ) {
            NSLog(@"lc content values data: %@", content_item);
            
            // Parse data from API
            NSString *content_column_value_id = [self stringValue:content_item[@"content_column_value_id"]];
            NSString *content_column_value = [self stringValue:content_item[@"content_column_value"]];
            NSString *content_column_name = [self stringValue:content_item[@"content_column_name"]];
            NSString *date_created = [self stringValue:content_item[@"date_created"]];
            NSString *date_modified = [self stringValue:content_item[@"date_modified"]];
            
            // Create managed object
            NSManagedObject *c_mo = [NSEntityDescription insertNewObjectForEntityForName:kLearningCompetencyContentEntity inManagedObjectContext:ctx];
            
            
            // Save data to core data
            [c_mo setValue:content_column_value_id forKey:@"content_column_value_id"];
            [c_mo setValue:content_column_value forKey:@"content_column_value"];
            [c_mo setValue:content_column_name forKey:@"content_column_name"];
            [c_mo setValue:date_created forKey:@"date_created"];
            [c_mo setValue:date_modified forKey:@"date_modified"];
            
            [set addObject:c_mo];
        }

        [object setValue:set forKey:@"content_values"];
    }
}

- (void)processLCLessonPlans:(NSArray *)lessonplanlist managedObject:(NSManagedObject *)object {
    NSManagedObjectContext *ctx = object.managedObjectContext;
    
    if (lessonplanlist.count > 0) {
        NSMutableSet *set = [NSMutableSet set];
        
        for (NSDictionary *lesson_plan_item in lessonplanlist ) {
            NSLog(@"lc lesson plan data: %@", lesson_plan_item);
            
            // Parse data from API
            NSString *lesson_id = [self stringValue:lesson_plan_item[@"id"]];
            NSString *lesson_plan_id = [self stringValue:lesson_plan_item[@"lesson_plan_id"]];
            
            // Create managed object
            NSManagedObject *l_mo = [NSEntityDescription insertNewObjectForEntityForName:kLearningCompetencyLessonEntity inManagedObjectContext:ctx];
            
            // Save data to core data
            [l_mo setValue:lesson_id forKey:@"id"];
            [l_mo setValue:lesson_plan_id forKey:@"lesson_plan_id"];
            
            [set addObject:l_mo];
        }

        [object setValue:set forKey:@"lessons"];
    }
}

#pragma mark - Associated Curriculum API (Just Temporary)

- (void)requestAssociatedCurriculumToLessonPlanWithID:(NSString *)lpid dataBlock:(LessonPlanDataBlock)dataBlock {
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointAssociatedCurriculumToLessonPlan, lpid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            BOOL hasAssociation = false;
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseFromAPIUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                if ([self isDictionaryObject:dictionary[@"records"]]) {
                    hasAssociation = true;
                    
                    NSDictionary *records = dictionary[@"records"];
                    NSLog(@"associated curriculum records: %@", records);
                    
                    NSString *curriculum_id = [self stringValue:records[@"curriculum_id"]];
                    NSString *curriculum_title = [self stringValue:records[@"curriculum_title"]];
                    
                    NSMutableArray *curriculum_periods = [NSMutableArray array];
                    
                    if ([self isArrayObject:records[@"periods"]]) {
                        for (NSDictionary *d in records[@"periods"]) {
                            NSString *period_id = [self stringValue:d[@"period_id"]];
                            NSString *period_name = [self stringValue:d[@"name"]];
                            [curriculum_periods addObject:@{@"period_id": period_id, @"period_name": period_name}];
                        }
                    }
                    
                    NSDictionary *data = @{@"curriculum_id": curriculum_id, @"curriculum_title": curriculum_title};
                    
                    if (curriculum_periods.count > 0) {
                        data = @{@"curriculum_id": curriculum_id, @"curriculum_title": curriculum_title, @"curriculum_periods": curriculum_periods};
                    }
                    
                    if (dataBlock) {
                        dataBlock(data);
                    }
                }
            }
            
            if (!hasAssociation) {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
}

#pragma mark - Error Alert View

- (void)showErrorMessageDialog {
    NSString *avTitle = NSLocalizedString(@"Error", nil);
    NSString *okayTitle = NSLocalizedString(@"Okay", nil);
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:avTitle
                                                 message:self.errorMessage
                                                delegate:self
                                       cancelButtonTitle:nil
                                       otherButtonTitles:okayTitle, nil];
    [av show];
}

#pragma mark - Preparation for Creating or Updating Lesson

- (void)prepareLessonForAction:(NSInteger)action copyObject:(NSManagedObject *)object additionalData:(NSDictionary *)data objectBlock:(LessonPlanManagedObjectBlock)objectBlock {
    
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kCopyLessonEntity withPredicate:nil context:ctx];
    [self clearDataForEntity:kCopyLessonContentEntity withPredicate:nil context:ctx];
    [self clearDataForEntity:kCopyLessonContentProcessEntity withPredicate:nil context:ctx];
    
    if (object != nil) {
        if (action == LPActionTypeCreate) {
            // Pre-Detail
            NSString *course_id = [self stringValue:data[@"course_id"]];
            NSString *template_id = [self stringValue:[object valueForKey:@"template_id"]];
            NSString *template_name = [self stringValue:[object valueForKey:@"template_name"]];
            NSString *start_date = [self.classHelper getDateTodayWithFormat:KLPDisplayDateFormatShort];
            NSString *end_date = [self.classHelper addDayToDate:[NSDate date]
                                                   numberOfDays:1
                                                 formatToReturn:KLPDisplayDateFormatShort];
            
            NSManagedObject *lmo = [NSEntityDescription insertNewObjectForEntityForName:kCopyLessonEntity
                                                                inManagedObjectContext:object.managedObjectContext];
            
            // NOTE: "0" means not applicable this time
            [lmo setValue:@"0" forKey:@"lp_id"];
            [lmo setValue:@"" forKey:@"name"];
            [lmo setValue:course_id forKey:@"course_id"];
            [lmo setValue:template_id forKey:@"template_id"];
            [lmo setValue:template_name forKey:@"template_name"];
            [lmo setValue:@"0" forKey:@"teacher_name"];
            [lmo setValue:@"" forKey:@"school_name"];
            [lmo setValue:@"" forKey:@"school_address"];
            [lmo setValue:@"" forKey:@"level"];
            [lmo setValue:@"" forKey:@"unit"];
            [lmo setValue:@"" forKey:@"quarter"];
            [lmo setValue:start_date forKey:@"start_date"];
            [lmo setValue:end_date forKey:@"end_date"];
            [lmo setValue:@"0" forKey:@"status"];
            
            // Contents
            NSSet *lmo_contents = (NSSet *)[object valueForKey:@"contents"];
            NSArray *contents = [lmo_contents allObjects];
            NSMutableSet *contents_set = [NSMutableSet set];
            
            for (NSManagedObject *cmo in contents) {
                NSString *stage_id = [self stringValue:[cmo valueForKey:@"stage_id"]];
                NSString *stage_name = [self stringValue:[cmo valueForKey:@"stage_name"]];
                
                // Processes
                NSSet *cmo_processes = (NSSet *)[cmo valueForKey:@"processes"];
                NSArray *processes = [cmo_processes allObjects];
                NSMutableSet *processes_set = [NSMutableSet set];
                
                for (NSManagedObject *pmo in processes) {
                    NSString *lps_id = [self stringValue:[pmo valueForKey:@"lps_id"]];
                    NSString *process_name = [self stringValue:[pmo valueForKey:@"process_name"]];
                    NSString *is_file = [self stringValue:[pmo valueForKey:@"is_file"]];
                    
                    NSManagedObject *content_process_item = [NSEntityDescription insertNewObjectForEntityForName:kCopyLessonContentProcessEntity
                                                                                          inManagedObjectContext:pmo.managedObjectContext];
                    
                    NSInteger lps_id_int = [lps_id integerValue];
                    
                    [content_process_item setValue:@(lps_id_int) forKey:@"lc_id"];                  // lps_id for lc_id
                    [content_process_item setValue:@(lps_id_int) forKey:@"lps_id"];                 // lps_id for lc_id
                    [content_process_item setValue:process_name forKey:@"learning_process_name"];   // process_name for learning_process_name
                    [content_process_item setValue:is_file forKey:@"is_file"];
                    [content_process_item setValue:@"" forKey:@"content"];
                    [content_process_item setValue:@"" forKey:@"file_path"];
                    [content_process_item setValue:@"" forKey:@"orig_filename"];
                    [content_process_item setValue:@"0" forKey:@"is_deleted"];
                    
                    // Add Process Object to Set
                    [processes_set addObject:content_process_item];
                }
                
                NSManagedObject *content_item = [NSEntityDescription insertNewObjectForEntityForName:kCopyLessonContentEntity
                                                                              inManagedObjectContext:cmo.managedObjectContext];
                
                [content_item setValue:stage_id forKey:@"stage_id"];
                [content_item setValue:stage_name forKey:@"stage_name"];
                [content_item setValue:[NSSet setWithSet:processes_set] forKey:@"processes"];
                
                // Add Content Object to Set
                [contents_set addObject:content_item];
            }
            
            // Add Contents with its Processes to Detail
            [lmo setValue:[NSSet setWithSet:contents_set] forKey:@"contents"];
            
            // Save Context
            [self saveTreeContext:ctx];
            
            if (objectBlock) {
                objectBlock (lmo);
            }
        }
        
        if (action == LPActionTypeUpdate) {
            // Get Template
            NSArray *template_processes = [self getObjectsForEntity:kTemplateProcessEntity predicate:nil context:ctx];
            
            // Pre-Detail
            NSString *lp_id = [self stringValue:[object valueForKey:@"lp_id"]];
            NSString *name = [self stringValue:[object valueForKey:@"name"]];
            NSString *template_id = [self stringValue:[object valueForKey:@"template_id"]];
            NSString *template_name = [self stringValue:[object valueForKey:@"template_name"]];
            NSString *teacher_name = [self stringValue:[object valueForKey:@"teacher_name"]];
            NSString *school_name = [self stringValue:[object valueForKey:@"school_name"]];
            NSString *school_address = [self stringValue:[object valueForKey:@"school_address"]];
            NSString *course_id = [self stringValue:[object valueForKey:@"course_id"]];
            NSString *level = [self stringValue:[object valueForKey:@"level"]];
            NSString *unit = [self stringValue:[object valueForKey:@"unit"]];
            NSString *quarter = [self stringValue:[object valueForKey:@"quarter"]];
            NSString *start_date = [self stringValue:[object valueForKey:@"start_date"]];
            NSString *end_date = [self stringValue:[object valueForKey:@"end_date"]];
            NSString *status = [self stringValue:[object valueForKey:@"status"]];
            
            NSManagedObject *lmo = [NSEntityDescription insertNewObjectForEntityForName:kCopyLessonEntity
                                                                 inManagedObjectContext:object.managedObjectContext];
            
            [lmo setValue:lp_id forKey:@"lp_id"];
            [lmo setValue:name forKey:@"name"];
            [lmo setValue:template_id forKey:@"template_id"];
            [lmo setValue:template_name forKey:@"template_name"];
            [lmo setValue:teacher_name forKey:@"teacher_name"];
            [lmo setValue:school_name forKey:@"school_name"];
            [lmo setValue:school_address forKey:@"school_address"];
            [lmo setValue:course_id forKey:@"course_id"];
            [lmo setValue:level forKey:@"level"];
            [lmo setValue:unit forKey:@"unit"];
            [lmo setValue:quarter forKey:@"quarter"];
            [lmo setValue:start_date forKey:@"start_date"];
            [lmo setValue:end_date forKey:@"end_date"];
            [lmo setValue:status forKey:@"status"];
            
            // Contents
            NSSet *lmo_contents = (NSSet *)[object valueForKey:@"contents"];
            NSArray *contents = [lmo_contents allObjects];
            NSMutableSet *contents_set = [NSMutableSet set];
            
            for (NSManagedObject *cmo in contents) {
                NSString *stage_id = [self stringValue:[cmo valueForKey:@"stage_id"]];
                NSString *stage_name = [self stringValue:[cmo valueForKey:@"stage_name"]];
                
                // Processes
                NSSet *cmo_processes = (NSSet *)[cmo valueForKey:@"processes"];
                NSArray *processes = [cmo_processes allObjects];
                NSMutableSet *processes_set = [NSMutableSet set];
                
                for (NSManagedObject *pmo in processes) {
                    NSString *learning_process_name = [self stringValue:[pmo valueForKey:@"learning_process_name"]];
                    NSString *lc_id = [self stringValue:[pmo valueForKey:@"lc_id"]];
                    NSString *content = [self stringValue:[pmo valueForKey:@"p_content"]];
                    NSString *file_path = [self stringValue:[pmo valueForKey:@"file_path"]];
                    NSString *orig_filename = [self stringValue:[pmo valueForKey:@"orig_filename"]];
                    NSString *is_file = [self stringValue:[pmo valueForKey:@"is_file"]];
                    NSString *is_deleted = [self stringValue:[pmo valueForKey:@"is_deleted"]];
                    
                    NSManagedObject *content_process_item = [NSEntityDescription insertNewObjectForEntityForName:kCopyLessonContentProcessEntity
                                                                                          inManagedObjectContext:pmo.managedObjectContext];
                    
                    NSInteger lc_id_int = [lc_id integerValue];
                    
                    [content_process_item setValue:learning_process_name forKey:@"learning_process_name"];
                    [content_process_item setValue:@(lc_id_int) forKey:@"lc_id"];
                    [content_process_item setValue:content forKey:@"content"];
                    [content_process_item setValue:file_path forKey:@"file_path"];
                    [content_process_item setValue:orig_filename forKey:@"orig_filename"];
                    [content_process_item setValue:is_file forKey:@"is_file"];
                    [content_process_item setValue:is_deleted forKey:@"is_deleted"];
                    
                    // Get lps_id from Template
                    for (NSManagedObject *tpmo in template_processes) {
                        NSString *process_name = [self stringValue:[tpmo valueForKey:@"process_name"]];
                        
                        if ([[learning_process_name uppercaseString] isEqualToString:[process_name uppercaseString]]) {
                            NSString *lps_id = [self stringValue:[tpmo valueForKey:@"lps_id"]];
                            NSInteger lps_id_int = [lps_id integerValue];
                            [content_process_item setValue:@(lps_id_int) forKey:@"lps_id"];
                        }
                    }
                    
                    // Add Process Object to Set
                    [processes_set addObject:content_process_item];
                }
                
                NSManagedObject *content_item = [NSEntityDescription insertNewObjectForEntityForName:kCopyLessonContentEntity
                                                                              inManagedObjectContext:cmo.managedObjectContext];
                
                [content_item setValue:stage_id forKey:@"stage_id"];
                [content_item setValue:stage_name forKey:@"stage_name"];
                [content_item setValue:[NSSet setWithSet:processes_set] forKey:@"processes"];
                
                // Add Content Object to Set
                [contents_set addObject:content_item];
            }
            
            // Add Contents with its Processes to Detail
            [lmo setValue:[NSSet setWithSet:contents_set] forKey:@"contents"];
            
            // Save Context
            [self saveTreeContext:ctx];
            
            if (objectBlock) {
                objectBlock (lmo);
            }
        }
    }
    else {
        if (objectBlock) {
            objectBlock(nil);
        }
    }
}

- (void)prepareLessonObjectForAction:(NSInteger)action
                          copyObject:(NSManagedObject *)object
                             addData:(NSDictionary *)data
                         objectBlock:(LessonPlanManagedObjectBlock)objectBlock {
    
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:kCopyLessonEntity withPredicate:nil context:ctx];
    [self clearDataForEntity:kCopyLessonContentEntity withPredicate:nil context:ctx];
    [self clearDataForEntity:kCopyLessonContentProcessEntity withPredicate:nil context:ctx];
    
    if (object != nil) {
        if (action == LPActionTypeCreate) {
            // Pre-Detail
            NSString *course_id = [self stringValue:data[@"course_id"]];
            NSString *template_id = [self stringValue:[object valueForKey:@"template_id"]];
            NSString *template_name = [self stringValue:[object valueForKey:@"template_name"]];
            NSString *start_date = [self.classHelper getDateTodayWithFormat:KLPDisplayDateFormatShort];
            NSString *end_date = [self.classHelper addDayToDate:[NSDate date]
                                                   numberOfDays:1
                                                 formatToReturn:KLPDisplayDateFormatShort];
            
            NSManagedObject *lmo = [NSEntityDescription insertNewObjectForEntityForName:kCopyLessonEntity
                                                                 inManagedObjectContext:object.managedObjectContext];
            
            // NOTE: "0" means not applicable this time
            [lmo setValue:@"0" forKey:@"lp_id"];
            [lmo setValue:@"" forKey:@"name"];
            [lmo setValue:course_id forKey:@"course_id"];
            [lmo setValue:template_id forKey:@"template_id"];
            [lmo setValue:template_name forKey:@"template_name"];
            [lmo setValue:@"0" forKey:@"teacher_name"];
            [lmo setValue:@"" forKey:@"school_name"];
            [lmo setValue:@"" forKey:@"school_address"];
            [lmo setValue:@"" forKey:@"level"];
            [lmo setValue:@"" forKey:@"unit"];
            [lmo setValue:@"1" forKey:@"quarter"];
            [lmo setValue:start_date forKey:@"start_date"];
            [lmo setValue:end_date forKey:@"end_date"];
            [lmo setValue:@"0" forKey:@"status"];
            
            // NOTE: This is just temporary for cpm integration
            // See CopyLesson entity: All curriculum-related attributes are just temporary
            [lmo setValue:@"" forKey:@"curriculum_id"];
            [lmo setValue:@"" forKey:@"curriculum_title"];
            [lmo setValue:@"" forKey:@"curriculum_period_id"];
            [lmo setValue:@"" forKey:@"curriculum_period_name"];
            
            // Contents
            NSSet *lmo_contents = (NSSet *)[object valueForKey:@"contents"];
            NSArray *contents = [lmo_contents allObjects];
            NSMutableSet *contents_set = [NSMutableSet set];
            
            for (NSManagedObject *cmo in contents) {
                NSString *stage_id = [self stringValue:[cmo valueForKey:@"stage_id"]];
                NSString *stage_name = [self stringValue:[cmo valueForKey:@"stage_name"]];
                
                // Processes
                NSSet *cmo_processes = (NSSet *)[cmo valueForKey:@"processes"];
                NSArray *processes = [cmo_processes allObjects];
                NSMutableSet *processes_set = [NSMutableSet set];
                
                for (NSManagedObject *pmo in processes) {
                    NSString *lps_id = [self stringValue:[pmo valueForKey:@"lps_id"]];
                    NSString *process_name = [self stringValue:[pmo valueForKey:@"process_name"]];
                    NSString *is_file = [self stringValue:[pmo valueForKey:@"is_file"]];
                    
                    NSManagedObject *content_process_item = [NSEntityDescription insertNewObjectForEntityForName:kCopyLessonContentProcessEntity
                                                                                          inManagedObjectContext:pmo.managedObjectContext];
                    
                    NSInteger lps_id_int = [lps_id integerValue];
                    
                    [content_process_item setValue:@(lps_id_int) forKey:@"lc_id"];                  // lps_id for lc_id
                    [content_process_item setValue:@(lps_id_int) forKey:@"lps_id"];                 // lps_id for lc_id
                    [content_process_item setValue:process_name forKey:@"learning_process_name"];   // process_name for learning_process_name
                    [content_process_item setValue:is_file forKey:@"is_file"];
                    [content_process_item setValue:@"" forKey:@"content"];
                    [content_process_item setValue:@"" forKey:@"file_path"];
                    [content_process_item setValue:@"" forKey:@"orig_filename"];
                    [content_process_item setValue:@"0" forKey:@"is_deleted"];
                    
                    // Add Process Object to Set
                    [processes_set addObject:content_process_item];
                }
                
                NSManagedObject *content_item = [NSEntityDescription insertNewObjectForEntityForName:kCopyLessonContentEntity
                                                                              inManagedObjectContext:cmo.managedObjectContext];
                
                [content_item setValue:stage_id forKey:@"stage_id"];
                [content_item setValue:stage_name forKey:@"stage_name"];
                [content_item setValue:[NSSet setWithSet:processes_set] forKey:@"processes"];
                
                // Add Content Object to Set
                [contents_set addObject:content_item];
            }
            
            // Add Contents with its Processes to Detail
            [lmo setValue:[NSSet setWithSet:contents_set] forKey:@"contents"];
            
            // Save Context
            [self saveTreeContext:ctx];
            
            if (objectBlock) {
                objectBlock (lmo);
            }
        }
        
        if (action == LPActionTypeUpdate) {
            // Get Template
            NSArray *template_processes = [self getObjectsForEntity:kTemplateProcessEntity predicate:nil context:ctx];
            
            // Pre-Detail
            NSString *lp_id = [self stringValue:[object valueForKey:@"lp_id"]];
            NSString *name = [self stringValue:[object valueForKey:@"name"]];
            NSString *template_id = [self stringValue:[object valueForKey:@"template_id"]];
            NSString *template_name = [self stringValue:[object valueForKey:@"template_name"]];
            NSString *teacher_name = [self stringValue:[object valueForKey:@"teacher_name"]];
            NSString *school_name = [self stringValue:[object valueForKey:@"school_name"]];
            NSString *school_address = [self stringValue:[object valueForKey:@"school_address"]];
            NSString *course_id = [self stringValue:[object valueForKey:@"course_id"]];
            NSString *level = [self stringValue:[object valueForKey:@"level"]];
            NSString *unit = [self stringValue:[object valueForKey:@"unit"]];
            NSString *quarter = [self stringValue:[object valueForKey:@"quarter"]];
            NSString *start_date = [self stringValue:[object valueForKey:@"start_date"]];
            NSString *end_date = [self stringValue:[object valueForKey:@"end_date"]];
            NSString *status = [self stringValue:[object valueForKey:@"status"]];
            
            NSManagedObject *lmo = [NSEntityDescription insertNewObjectForEntityForName:kCopyLessonEntity
                                                                 inManagedObjectContext:object.managedObjectContext];
            
            [lmo setValue:lp_id forKey:@"lp_id"];
            [lmo setValue:name forKey:@"name"];
            [lmo setValue:template_id forKey:@"template_id"];
            [lmo setValue:template_name forKey:@"template_name"];
            [lmo setValue:teacher_name forKey:@"teacher_name"];
            [lmo setValue:school_name forKey:@"school_name"];
            [lmo setValue:school_address forKey:@"school_address"];
            [lmo setValue:course_id forKey:@"course_id"];
            [lmo setValue:level forKey:@"level"];
            [lmo setValue:unit forKey:@"unit"];
            [lmo setValue:quarter forKey:@"quarter"];
            [lmo setValue:start_date forKey:@"start_date"];
            [lmo setValue:end_date forKey:@"end_date"];
            [lmo setValue:status forKey:@"status"];
            
            ///////////////////////////////////////////////////////////////////////////////
            // NOTE: This is just temporary for cpm integration
            // See CopyLesson entity: All curriculum-related attributes are just temporary
            NSString *curriculum_id = [self stringValue:data[@"curriculum_id"]];
            NSString *curriculum_title = [self stringValue:data[@"curriculum_title"]];
            
            [lmo setValue:curriculum_id forKey:@"curriculum_id"];
            [lmo setValue:curriculum_title forKey:@"curriculum_title"];
            [lmo setValue:@"" forKey:@"curriculum_period_id"];
            [lmo setValue:@"" forKey:@"curriculum_period_name"];
            ///////////////////////////////////////////////////////////////////////////////
            
            // Contents
            NSSet *lmo_contents = (NSSet *)[object valueForKey:@"contents"];
            NSArray *contents = [lmo_contents allObjects];
            NSMutableSet *contents_set = [NSMutableSet set];
            
            for (NSManagedObject *cmo in contents) {
                NSString *stage_id = [self stringValue:[cmo valueForKey:@"stage_id"]];
                NSString *stage_name = [self stringValue:[cmo valueForKey:@"stage_name"]];
                
                // Processes
                NSSet *cmo_processes = (NSSet *)[cmo valueForKey:@"processes"];
                NSArray *processes = [cmo_processes allObjects];
                NSMutableSet *processes_set = [NSMutableSet set];
                
                for (NSManagedObject *pmo in processes) {
                    NSString *learning_process_name = [self stringValue:[pmo valueForKey:@"learning_process_name"]];
                    NSString *lc_id = [self stringValue:[pmo valueForKey:@"lc_id"]];
                    NSString *content = [self stringValue:[pmo valueForKey:@"p_content"]];
                    NSString *file_path = [self stringValue:[pmo valueForKey:@"file_path"]];
                    NSString *orig_filename = [self stringValue:[pmo valueForKey:@"orig_filename"]];
                    NSString *is_file = [self stringValue:[pmo valueForKey:@"is_file"]];
                    NSString *is_deleted = [self stringValue:[pmo valueForKey:@"is_deleted"]];
                    
                    NSManagedObject *content_process_item = [NSEntityDescription insertNewObjectForEntityForName:kCopyLessonContentProcessEntity
                                                                                          inManagedObjectContext:pmo.managedObjectContext];
                    
                    NSInteger lc_id_int = [lc_id integerValue];
                    
                    [content_process_item setValue:learning_process_name forKey:@"learning_process_name"];
                    [content_process_item setValue:@(lc_id_int) forKey:@"lc_id"];
                    [content_process_item setValue:content forKey:@"content"];
                    [content_process_item setValue:file_path forKey:@"file_path"];
                    [content_process_item setValue:orig_filename forKey:@"orig_filename"];
                    [content_process_item setValue:is_file forKey:@"is_file"];
                    [content_process_item setValue:is_deleted forKey:@"is_deleted"];
                    
                    // Get lps_id from Template
                    for (NSManagedObject *tpmo in template_processes) {
                        NSString *process_name = [self stringValue:[tpmo valueForKey:@"process_name"]];
                        
                        if ([[learning_process_name uppercaseString] isEqualToString:[process_name uppercaseString]]) {
                            NSString *lps_id = [self stringValue:[tpmo valueForKey:@"lps_id"]];
                            NSInteger lps_id_int = [lps_id integerValue];
                            [content_process_item setValue:@(lps_id_int) forKey:@"lps_id"];
                        }
                    }
                    
                    // Add Process Object to Set
                    [processes_set addObject:content_process_item];
                }
                
                NSManagedObject *content_item = [NSEntityDescription insertNewObjectForEntityForName:kCopyLessonContentEntity
                                                                              inManagedObjectContext:cmo.managedObjectContext];
                
                [content_item setValue:stage_id forKey:@"stage_id"];
                [content_item setValue:stage_name forKey:@"stage_name"];
                [content_item setValue:[NSSet setWithSet:processes_set] forKey:@"processes"];
                
                // Add Content Object to Set
                [contents_set addObject:content_item];
            }
            
            // Add Contents with its Processes to Detail
            [lmo setValue:[NSSet setWithSet:contents_set] forKey:@"contents"];
            
            // Save Context
            [self saveTreeContext:ctx];
            
            if (objectBlock) {
                objectBlock (lmo);
            }
        }
    }
    else {
        if (objectBlock) {
            objectBlock(nil);
        }
    }
}

- (NSDictionary *)prepareLessonPlanDataForPostOperation:(NSManagedObject *)object action:(NSInteger)action {
    if (object != nil) {
        // Pre-Detail
        NSString *user_id = [self loginUser];
        NSString *name = [object valueForKey:@"name"];
        NSString *course_id = [object valueForKey:@"course_id"];
        NSString *level = [object valueForKey:@"level"];
        NSString *unit = [object valueForKey:@"unit"];
        NSString *quarter = [object valueForKey:@"quarter"];
        NSString *school_name = [object valueForKey:@"school_name"];
        NSString *school_address = [object valueForKey:@"school_address"];
        NSString *start_date = [object valueForKey:@"start_date"];
        NSString *end_date = [object valueForKey:@"end_date"];
        NSString *template_id = [object valueForKey:@"template_id"];
        
        start_date = [self.classHelper transformDateString:start_date
                                                fromFormat:KLPDisplayDateFormatShort
                                                  toFormat:kLPServerDateFormat];
        
        end_date = [self.classHelper transformDateString:end_date
                                              fromFormat:KLPDisplayDateFormatShort
                                                toFormat:kLPServerDateFormat];

        NSMutableArray *post_content = [NSMutableArray array];
        NSMutableArray *process_with_file = [NSMutableArray array];
        
        // Contents
        NSSet *lmo_contents = (NSSet *)[object valueForKey:@"contents"];
        NSArray *contents = [lmo_contents allObjects];
        
        for (NSManagedObject *cmo in contents) {
            NSSet *cmo_processes = (NSSet *)[cmo valueForKey:@"processes"];
            NSArray *processes = [cmo_processes allObjects];
            
            // Processes
            for (NSManagedObject *pmo in processes) {
                NSString *lps_id = [pmo valueForKey:@"lps_id"];
                NSString *content = [pmo valueForKey:@"content"];
                NSString *is_file = [pmo valueForKey:@"is_file"];
                NSString *file_path = [pmo valueForKey:@"file_path"];
                NSString *orig_filename = [pmo valueForKey:@"orig_filename"];
                
                // if file
                if ([is_file isEqualToString:@"1"] && ![file_path isEqualToString:@""]) {
                    [process_with_file addObject:@{@"lps_id":lps_id, @"file_path":file_path, @"orig_filename":orig_filename}];
                }
                // if not file
                else {
                    [post_content addObject:@{@"lps_id":lps_id, @"content":content}];
                }
            }
        }
        
        // Fix misorder of lesson content stages after posting
        NSSortDescriptor *sort_by_lps_id = [NSSortDescriptor sortDescriptorWithKey:@"lps_id" ascending:YES];
        NSArray *descriptors = [NSArray arrayWithObject:sort_by_lps_id];
        NSArray *sorted_p_content = [post_content sortedArrayUsingDescriptors:descriptors];
       
        NSDictionary *content = @{@"user_id":user_id,
                                  @"name":name,
                                  @"course_id":course_id,
                                  @"level":level,
                                  @"unit":unit,
                                  @"quarter":quarter,
                                  @"start_date":start_date,
                                  @"end_date":end_date,
                                  @"school_name":school_name,
                                  @"school_address":school_address,
                                  @"content":sorted_p_content
                                  };
        
        // Accept only one file for the mean time since
        // there's no support yet for multiple file upload
        
        NSString *file_learning_process_stage_id = @"";
        NSDictionary *file = nil;
        
        if (process_with_file.count == 1) {
            NSDictionary *d = [process_with_file objectAtIndex:0];
            file_learning_process_stage_id = [self stringValue:d[@"lps_id"]];
            
            NSString *file_path = [self stringValue:d[@"file_path"]];
            NSString *orig_filename = [self stringValue:d[@"orig_filename"]];
            NSString *mime_type = [self mimeTypeForFileAtPath:file_path];
            
            file = @{@"filepath": file_path, @"filename": orig_filename, @"mimetype": mime_type};
        }
        
        
        if (action == LPActionTypeCreate)  {
            NSDictionary *meta = @{@"content":[self convertDataToJSONString:content],
                                   @"file_learning_process_stage_id":file_learning_process_stage_id,
                                   @"template_id":template_id
                                   };
            
            if (file != nil) {
                meta = @{@"meta": meta, @"file": file};
            }
            
            return meta;
            
        }
        else {
            return @{@"content":[self convertDataToJSONString:content]};
        }
    }
    
    return nil;
}

- (NSDictionary *)prepareLessonObject:(NSManagedObject *)object forAction:(NSInteger)action {
    if (object != nil) {
        // Pre-Detail
        NSString *user_id = [self loginUser];
        NSString *name = [object valueForKey:@"name"];
        NSString *course_id = [object valueForKey:@"course_id"];
        NSString *level = [object valueForKey:@"level"];
        NSString *unit = [object valueForKey:@"unit"];
        NSString *quarter = [object valueForKey:@"quarter"];
        NSString *school_name = [object valueForKey:@"school_name"];
        NSString *school_address = [object valueForKey:@"school_address"];
        NSString *start_date = [object valueForKey:@"start_date"];
        NSString *end_date = [object valueForKey:@"end_date"];
        NSString *template_id = [object valueForKey:@"template_id"];
        
        start_date = [self.classHelper transformDateString:start_date
                                                fromFormat:KLPDisplayDateFormatShort
                                                  toFormat:kLPServerDateFormat];
        
        end_date = [self.classHelper transformDateString:end_date
                                              fromFormat:KLPDisplayDateFormatShort
                                                toFormat:kLPServerDateFormat];
        
        NSMutableArray *lesson_content_stage_process = [NSMutableArray array];
        NSMutableArray *lesson_content_stage_process_with_file = [NSMutableArray array];
        
        // Contents
        NSSet *lmo_contents = (NSSet *)[object valueForKey:@"contents"];
        NSArray *contents = [lmo_contents allObjects];
        
        for (NSManagedObject *cmo in contents) {
            NSSet *cmo_processes = (NSSet *)[cmo valueForKey:@"processes"];
            NSArray *processes = [cmo_processes allObjects];
            
            // Processes
            for (NSManagedObject *pmo in processes) {
                NSString *lps_id = [pmo valueForKey:@"lps_id"];
                NSString *lc_id = [pmo valueForKey:@"lc_id"];
                NSString *content = [pmo valueForKey:@"content"];
                NSString *is_file = [pmo valueForKey:@"is_file"];
                NSString *file_path = [pmo valueForKey:@"file_path"];
                
                // all should be added including lesson content stage process containing file
                [lesson_content_stage_process addObject:@{@"lps_id":lps_id, @"content":content}];
                
                // if file
                if ([is_file isEqualToString:@"1"] && ![file_path isEqualToString:@""]) {
                    [lesson_content_stage_process_with_file addObject:@{@"lps_id":lps_id, @"lc_id": lc_id, @"file_path":file_path}];
                }
            }
        }
        
        // Prevent misordering of lesson content stages after posting
        NSSortDescriptor *sort_by_lps_id = [NSSortDescriptor sortDescriptorWithKey:@"lps_id" ascending:YES];
        NSArray *descriptors = [NSArray arrayWithObject:sort_by_lps_id];
        NSArray *sorted_lesson_content_stage_process = [lesson_content_stage_process sortedArrayUsingDescriptors:descriptors];
        
        // Accept only one file for the mean time since
        // there's no support yet for multiple file upload
        NSString *file_learning_process_stage_id = @"";
        NSString *file_path = @"";
        NSString *lc_id = @"";
        
        if (lesson_content_stage_process_with_file.count == 1) {
            NSDictionary *d = [lesson_content_stage_process_with_file objectAtIndex:0];
            file_learning_process_stage_id = [self stringValue:d[@"lps_id"]];
            file_path = [self stringValue:d[@"file_path"]];
            lc_id = [self stringValue:d[@"lc_id"]];
        }
        
        if (action == LPActionTypeCreate) {
            NSDictionary *content = @{@"user_id":user_id,
                                      @"name":name,
                                      @"course_id":course_id,
                                      @"level":level,
                                      @"unit":unit,
                                      @"quarter":quarter,
                                      @"start_date":start_date,
                                      @"end_date":end_date,
                                      @"school_name":school_name,
                                      @"school_address":school_address,
                                      @"content":sorted_lesson_content_stage_process
                                      };
            
            NSDictionary *meta = @{@"content":[self generateJSONStringFromDictionary:content],
                                   @"file_learning_process_stage_id":file_learning_process_stage_id,
                                   @"template_id":template_id
                                   };
            
            return @{@"meta":meta, @"file_path":file_path};;
        }
        
        if (action == LPActionTypeUpdate) {
            NSDictionary *content = @{@"user_id":user_id,
                                      @"name":name,
                                      @"course_id":course_id,
                                      @"level":level,
                                      @"unit":unit,
                                      @"quarter":quarter,
                                      @"start_date":start_date,
                                      @"end_date":end_date,
                                      @"school_name":school_name,
                                      @"school_address":school_address,
                                      @"content":sorted_lesson_content_stage_process
                                      };
            
            NSDictionary *meta = @{@"content":[self generateJSONStringFromDictionary:content]};
            
            return @{@"meta":meta, @"lc_id":lc_id, @"file_path":file_path};;
        }
    }
    
    return nil;
}

#pragma mark - Parsing Helpers

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
//    if (value.length > 0) {
//        
//        NSLog(@"------- > value : %@", )
//        
//        NSRange range = NSMakeRange(0, value.length);
//        value = [value stringByReplacingOccurrencesOfString:@"Optional(^[0-9a-zA-Z])"
//                                                 withString:@""
//                                                    options:NSRegularExpressionSearch
//                                                      range:range];
//    }
    
    return value;
}

- (NSString *)getUTCFormateDate:(NSDate *)localDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:localDate];
    return dateString;
}

- (NSString *)convertDataToJSONString:(NSDictionary *)data {
    if (data) {
        NSError *err;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&err];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        return jsonString;
    }
    
    return @"";
}

- (NSString *)generateJSONStringFromDictionary:(NSDictionary *)dictionary {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
        return @"";
    }
    else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

- (NSString *)urlEncode:(id<NSObject>)value {
    if ([value isKindOfClass:[NSNumber class]]) {
        value = [(NSNumber*)value stringValue];
    }
    
    NSAssert([value isKindOfClass:[NSString class]], @"request parameters can be only of NSString or NSNumber classes. '%@' is of class %@.", value, [value class]);
    
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                 NULL,
                                                                                 (__bridge CFStringRef) value,
                                                                                 NULL,
                                                                                 (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                 kCFStringEncodingUTF8));
}

- (NSManagedObject *)getLessonDetailsObjectForLessonWithID:(NSString *)lpid {
    NSPredicate *predicate = [self predicateForKeyPath:@"lp_id" andValue:lpid];
    return [self getEntity:kLessonPlanDetailsEntity predicate:predicate];
}

- (NSArray *)getLessonStageProcessesForLessonWithID:(NSString *)lpid andStageID:(NSString *)stageid {
    NSPredicate *predicate1 = [self predicateForKeyPath:@"lp_id" andValue:lpid];
    NSPredicate *predicate2 = [self predicateForKeyPath:@"stage_id" andValue:stageid];
    NSCompoundPredicate *predicate3 = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
    return [self getObjectsForEntity:kProcessEntity predicate:predicate3 sortDescriptor:sortDescriptor];
}

- (NSString *)mimeTypeForFileAtPath:(NSString *)path {
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return @"";
    }
    
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[path pathExtension], NULL);
    CFStringRef mimeType = UTTypeCopyPreferredTagWithClass (UTI, kUTTagClassMIMEType);
    CFRelease(UTI);
    
    if (!mimeType) {
        return @"application/octet-stream";
    }
    
    return (__bridge NSString *) mimeType;
}

#pragma mark - Updating Core Data Objects

- (void)updateLessonPlanLocally:(NSString *)entity predicate:(NSPredicate *)predicate details:(NSDictionary *)details {
    NSManagedObject *mo = [self getEntity:entity predicate:predicate];
    NSManagedObjectContext *ctx = mo.managedObjectContext;
    
    if (mo != nil) {
        NSArray *keys = [details allKeys];
        
        for (NSString *k in keys) {
            [mo setValue:details[k] forKey:k];
        }
        
        [self saveTreeContext:ctx];
    }
}

- (void)updateEntity:(NSString *)entity predicate:(NSPredicate *)predicate withData:(NSDictionary *)data {
    NSManagedObject *mo = [self getEntity:entity predicate:predicate];
    NSManagedObjectContext *ctx = mo.managedObjectContext;
    
    if (mo != nil) {
        NSArray *keys = [data allKeys];
        
        for (NSString *k in keys) {
            [mo setValue:data[k] forKey:k];
        }
        
        [self saveTreeContext:ctx];
    }
}

@end
