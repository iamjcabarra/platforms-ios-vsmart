//
//  CPStudentCellItem.h
//  V-Smart
//
//  Created by Julius Abarra on 30/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPStudentCellItem : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *studentImage;
@property (strong, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *studentContactNumber;
@property (strong, nonatomic) IBOutlet UILabel *studentEmailAddress;

@end
