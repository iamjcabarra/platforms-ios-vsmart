//
//  CurriculumTableViewCell.m
//  V-Smart
//
//  Created by Julius Abarra on 9/28/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "CurriculumTableViewCell.h"

@implementation CurriculumTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
