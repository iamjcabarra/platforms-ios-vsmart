//
//  ReportsContainerController.h
//  V-Smart
//
//  Created by Ryan Migallos on 19/10/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ReportsContainerController : BaseViewController

@end
