//
//  NoteBookCell.h
//  V-Smart
//
//  Created by VhaL on 3/1/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface NoteBookCell : SWTableViewCell <UITextFieldDelegate>
@property (nonatomic, strong) IBOutlet UILabel *noteBookLabel;
@property (nonatomic, strong) IBOutlet UITextField *noteBookTextField;
@property (nonatomic, strong) IBOutlet UIButton *acceptButton;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (nonatomic, strong) IBOutlet UIImageView *shareIndicatorImageView;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) int selectedColorId;

-(void)hideEditControls:(bool)hide;
-(void)hideShareIndicator:(bool)hide;
-(void)initializeCell;
-(void)updateNotebookColor;
@end

