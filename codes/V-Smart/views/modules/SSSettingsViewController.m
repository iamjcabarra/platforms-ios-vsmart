//
//  SettingsViewController.m
//  V-Smart
//
//  Created by VhaL on 4/30/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "SSSettingsViewController.h"

@interface SSSettingsViewController ()

@end

@implementation SSSettingsViewController
@synthesize labelBuildDate, labelBuildNum, labelBuildTime;

-(void)viewDidLoad{
    NSString *dateStr = [NSString stringWithUTF8String:__DATE__];
    NSString *timeStr = [NSString stringWithUTF8String:__TIME__];
    
    labelBuildDate.text = dateStr;
    labelBuildTime.text = timeStr;
    
//    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
//    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];

    NSDictionary *appInfo = [[NSBundle mainBundle] infoDictionary];
    NSString *appVersion = [NSString stringWithFormat:@"%@ (%@)", appInfo[@"CFBundleShortVersionString"], appInfo[@"CFBundleVersion"]];
    
    labelBuildNum.text = appVersion;
}

@end
