//
//  StudentShortcut.m
//  V-Smart
//
//  Created by Ryan Migallos on 4/16/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "StudentShortcut.h"
#import "StudentShortcutCell.h"
#import "PeopleShortcutDetail.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"

@interface StudentShortcut () <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) ResourceManager *rm;
@property (strong, nonatomic) NSString *user_type_id;

@property (strong, nonatomic) dispatch_queue_t queue;

@end

@implementation StudentShortcut

static NSString *kCellIdentifier = @"student_shortcut_cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.rm = [AppDelegate resourceInstance];
    self.user_type_id = @"*";
    
    self.queue = dispatch_queue_create("com.vibetechonologies.people.SHORTCUT",DISPATCH_QUEUE_CONCURRENT);
    
    NSString *user_id = [NSString stringWithFormat:@"%d",[VSmart sharedInstance].account.user.id];
    
    [self.rm requestPeopleForUserID:user_id doneBlock:^(BOOL status) {
    }];
    
    [self.segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
    
    // Bug #1150
    // jca-04222016
    NSString *allString = NSLocalizedString(@"All", nil);
    [self.segmentedControl setTitle:allString forSegmentAtIndex:0];
    
    NSString *studentsString = NSLocalizedString(@"Students", nil);
    [self.segmentedControl setTitle:studentsString forSegmentAtIndex:1];
    
    NSString *teachersString = NSLocalizedString(@"Teachers", nil);
    [self.segmentedControl setTitle:teachersString forSegmentAtIndex:2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.tableView.allowsSelection = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)segmentAction:(id)sender {
    
    UISegmentedControl *s = (UISegmentedControl *)sender;    
    NSInteger selection = s.selectedSegmentIndex;
    
    switch (selection) {
        case 0:
            self.user_type_id = @"*";//STUDENTS
            break;
            
        case 1:
            self.user_type_id = @"4";//CLASSMATES
            break;
            
        case 2:
            self.user_type_id = @"3";//TEACHERS
            break;
            
        default:
            break;
    }
    
    [self reloadFetchedResultsController];
}

- (void)reloadFetchedResultsController {
    
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.tableView = tableView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)object atIndexPath:(NSIndexPath *)indexPath {
    StudentShortcutCell *cell = (StudentShortcutCell *)object;
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *name = [NSString stringWithFormat:@"%@ %@", [mo valueForKey:@"first_name"], [mo valueForKey:@"last_name"] ];
    NSString *flag = [NSString stringWithFormat:@"%@", [mo valueForKey:@"is_logged_in"] ];
    
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //        cell.profileImage.image = [self loadImageWithURL:avatar_url];
    //    });
    
    //if ([mo valueForKey:@"thumbnail"]) {
    //    cell.profileImage.image = [UIImage imageWithData:[mo valueForKey:@"thumbnail"]];
    //}
    //else {
    //    [self.rm downloadImagePeopleShortcut:mo dataBlock:^(NSData *binary) {
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            cell.profileImage.image = [UIImage imageWithData:binary];
    //        });
    //    }];
    //}
    
    cell.labelStatus.backgroundColor = [self loginStatus:flag];
    cell.labelName.text = name;
    
    // Bug #1151
    // jca-05112016
    // Fix disappearing of student's avatar
    NSString *avatar_url = [NSString stringWithFormat:@"%@", [mo valueForKey:@"avatar"]];
    [cell.profileImage sd_setImageWithURL:[NSURL URLWithString:avatar_url]];
}

- (UIImage *)loadImageWithURL:(NSString *)urlstring
{
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", urlstring]];
    UIImage *image = [[EGOImageLoader sharedImageLoader] imageForURL:imageURL shouldLoadWithObserver:nil];
    return image;
}

- (UIColor *)loginStatus:(NSString *)flag {
    
    UIColor *online = UIColorFromHex(0x00FF00);
    UIColor *offline = [UIColor whiteColor];
    UIColor *color = ([flag isEqualToString:@"1"]) ? online : offline;
    
    return color;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.tableView = tableView;
    
    self.tableView.allowsSelection = NO;
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *profile_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"] ];
    
    [self.rm requestUserProfileWithID:profile_id dataBlock:^(NSDictionary *data) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"showShortcutStudentDetail" sender:self];
        });
        
    }];
    
}

 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     
     if ([segue.identifier isEqualToString:@"showShortcutStudentDetail"]) {
         NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
         NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
         
         NSString *first_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"] ];
         NSString *last_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"] ];
         NSString *section_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"section_name"] ];
         NSString *user_type_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_type_id"] ];
         NSString *info_percentage = [NSString stringWithFormat:@"%@", [mo valueForKey:@"info_percentage"] ];
         NSString *count_liked = [NSString stringWithFormat:@"%@", [mo valueForKey:@"count_liked"] ];
         NSString *count_posted = [NSString stringWithFormat:@"%@", [mo valueForKey:@"count_posted"] ];
         NSString *count_received_likes = [NSString stringWithFormat:@"%@", [mo valueForKey:@"count_received_likes"] ];
         NSString *count_replied = [NSString stringWithFormat:@"%@", [mo valueForKey:@"count_replied"] ];
         NSData *thumbnail = [NSData dataWithData:[mo valueForKey:@"thumbnail"]];
         NSString *avatar_url = [NSString stringWithFormat:@"%@", [mo valueForKey:@"avatar"] ];
         
         NSDictionary *data = @{@"first_name":first_name,
                                @"last_name":last_name,
                                @"section_name":section_name,
                                @"user_type_id":user_type_id,
                                @"info_percentage":info_percentage,
                                @"count_liked":count_liked,
                                @"count_posted":count_posted,
                                @"count_received_likes":count_received_likes,
                                @"count_replied":count_replied,
                                @"thumbnail":thumbnail,
                                @"avatar_url":avatar_url};
         
         PeopleShortcutDetail *d = (PeopleShortcutDetail *)segue.destinationViewController;
         d.profileData = data;
         
         [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
     }
     
 }

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.rm.mainContext;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kShortcutPeopleEntity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Filter
    NSPredicate *predicate = [self predicateForKeyPath:@"user_type_id" value:self.user_type_id];
    [fetchRequest setPredicate:predicate];

    // Set Sort Key Descriptor
    NSSortDescriptor *user_type = [NSSortDescriptor sortDescriptorWithKey:@"user_type_id" ascending:YES];
    NSSortDescriptor *sort_name = [NSSortDescriptor sortDescriptorWithKey:@"first_name" ascending:YES];
    [fetchRequest setSortDescriptors:@[user_type,sort_name]];
    
    // Edit the section name key path and cache name if appropriate.
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // create predicate options
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
