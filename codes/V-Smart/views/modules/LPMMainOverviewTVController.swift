//
//  LPMMainOverviewTVController.swift
//  V-Smart
//
//  Created by Julius Abarra on 05/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMMainOverviewTVController: UITableViewController, UITextFieldDelegate, UITextViewDelegate, LPMDatePickerPopoverDelegate, LPMLessonQuarterPopoverDelegate, LPMCurriculumPlannerPopoverDelegate, LPMCurriculumPeriodPopoverDelegate {
   
    @IBOutlet fileprivate var curriculumPeriodView: UIView!
    @IBOutlet fileprivate var learningCompetencyView: UIView!
    
    @IBOutlet fileprivate var titleLabel: UILabel!
    @IBOutlet fileprivate var schoolNameLabel: UILabel!
    @IBOutlet fileprivate var schoolAddressLabel: UILabel!
    @IBOutlet fileprivate var gradeLevelLabel: UILabel!
    @IBOutlet fileprivate var unitLabel: UILabel!
    @IBOutlet fileprivate var quarterLabel: UILabel!
    @IBOutlet fileprivate var timeFrameLabel: UILabel!
    @IBOutlet fileprivate var associationSectionLabel: UILabel!
    @IBOutlet fileprivate var curriculumPlannerLabel: UILabel!
    @IBOutlet fileprivate var curriculumPeriodLabel: UILabel!
    
    @IBOutlet fileprivate var titleTextField: UITextField!
    @IBOutlet fileprivate var schoolNameTextField: UITextField!
    @IBOutlet fileprivate var schoolAddressTextField: UITextView!
    @IBOutlet fileprivate var gradeLevelTextField: UITextField!
    @IBOutlet fileprivate var unitTextField: UITextField!
    @IBOutlet fileprivate var quarterTextField: UITextField!
    @IBOutlet fileprivate var startDateTextField: UITextField!
    @IBOutlet fileprivate var endDateTextField: UITextField!
    @IBOutlet fileprivate var curriculumPlannerTextfield: UITextField!
    @IBOutlet fileprivate var curriculumPeriodTextfield: UITextField!

    @IBOutlet fileprivate var quarterButton: UIButton!
    @IBOutlet fileprivate var startDateButton: UIButton!
    @IBOutlet fileprivate var endDateButton: UIButton!
    @IBOutlet fileprivate var curriculumPlannerButton: UIButton!
    @IBOutlet fileprivate var curriculumPeriodButton: UIButton!
    
    var lessonObject: NSManagedObject!
    
    fileprivate var datePickerView: LPMDatePickerPopover!
    fileprivate var quarterPopover: LPMLessonQuarterPopover!
    fileprivate var curriculumPlannerPopover: LPMCurriculumPlannerPopover!
    fileprivate var curriculumPeriodPopover: LPMCurriculumPeriodPopover!
    fileprivate var timeFrameButtonTag = LPMLessonOverviewTimeFrame.startDate.rawValue
    
    // MARK: - Data Managers
    
    fileprivate lazy var lessonPlanDataManager: LessonPlanDataManager = {
        let lpdm = LessonPlanDataManager.sharedInstance()
        return lpdm!
    }()
    
    fileprivate lazy var curriculumPlannerDataManager: CPMDataManager = {
        let cpdm = CPMDataManager.sharedInstance
        return cpdm
    }()
    
    // MARK: - Version Check
    
    fileprivate lazy var isVersion25: Bool = {
        let version: CGFloat = Utils.getServerInstanceVersion() as CGFloat
        return (version >= LPMConstants.Server.MAX_VERSION)
    }()
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Localization
        self.titleLabel.text = NSLocalizedString("Title", comment: "").uppercased()
        self.schoolNameLabel.text = NSLocalizedString("School Name", comment: "").uppercased()
        self.schoolAddressLabel.text = NSLocalizedString("School Address", comment: "").uppercased()
        self.gradeLevelLabel.text = NSLocalizedString("Grade Level", comment: "").uppercased()
        self.unitLabel.text = NSLocalizedString("Unit", comment: "").uppercased()
        self.quarterLabel.text = NSLocalizedString("Quarter", comment: "").uppercased()
        self.timeFrameLabel.text = NSLocalizedString("Time Frame", comment: "").uppercased()
        self.associationSectionLabel.text = NSLocalizedString("Associate Lesson Plan to Curriculum", comment: "").uppercased()
        self.curriculumPlannerLabel.text = NSLocalizedString("Curriculum Planner", comment: "").uppercased()
        self.curriculumPeriodLabel.text = NSLocalizedString("Period", comment: "").uppercased()
        
        // Delegation
        self.titleTextField.delegate = self
        self.schoolNameTextField.delegate = self
        self.schoolAddressTextField.delegate = self
        self.gradeLevelTextField.delegate = self
        self.unitTextField.delegate = self
        
        // Lesson quarter popover
        let lessonQuarterPopoverAction = #selector(self.showLessonQuarterPopover(_:))
        self.quarterButton.addTarget(self, action: lessonQuarterPopoverAction, for: .touchUpInside)
        
        // Curriculum planner popover
        let curriculumPlannerPopoverAction = #selector(self.showCurriculumPlannerPopover(_:))
        self.curriculumPlannerButton.addTarget(self, action: curriculumPlannerPopoverAction, for: .touchUpInside)
        
        // Curriculum period popover
        let curriculumPeriodPopoverAction = #selector(self.showCurriculumPeriodPopover(_:))
        self.curriculumPeriodButton.addTarget(self, action: curriculumPeriodPopoverAction, for: .touchUpInside)
        
        // Default Data
        self.loadDefaultData()
        
        // Date Picker View
        self.setupDatePickerView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Render Default Data
    
    func loadDefaultData() {
        if self.lessonObject != nil {
            let title = self.lessonObject.value(forKey: "name") as! String
            let schoolName = self.lessonObject.value(forKey: "school_name") as! String
            let schoolAddress = self.lessonObject.value(forKey: "school_address") as! String
            let level = self.lessonObject.value(forKey: "level") as! String
            let unit = self.lessonObject.value(forKey: "unit") as! String
            let quarter = self.lessonObject.value(forKey: "quarter") as! String
            let startDate = self.lessonObject.value(forKey: "start_date") as! String
            let endDate = self.lessonObject.value(forKey: "end_date") as! String
            
            self.titleTextField.text = title
            self.schoolNameTextField.text = schoolName
            self.schoolAddressTextField.text = schoolAddress
            self.gradeLevelTextField.text = level
            self.unitTextField.text = unit
            self.startDateTextField.text = startDate
            self.endDateTextField.text = endDate
            self.quarterTextField.text = quarter == "" ? "1" : "\(quarter)"
            
            // NOTE: This is just temporary for cpm integration
            // See CopyLesson entity: All curriculum-related attributes are just temporary
            
            let curriculumID = self.lessonObject.value(forKey: "curriculum_id") as! String
            let curriculumPeriodID = self.lessonObject.value(forKey: "curriculum_period_id") as! String
            let curriculumTitle = self.lessonObject.value(forKey: "curriculum_title") as! String
            let curriculumPeriodName = self.lessonObject.value(forKey: "curriculum_period_name") as! String
            let defaultText = NSLocalizedString("Please Select", comment: "")
            
            let noSelectedCurriculum = curriculumID == "" ? true : false
            let noSelectedPeriod = curriculumPeriodID == "" ? true : false
            
            self.curriculumPlannerTextfield.text = noSelectedCurriculum ? defaultText : curriculumTitle
            self.curriculumPeriodTextfield.text = noSelectedPeriod ? defaultText : curriculumPeriodName
            
            self.curriculumPeriodView.isHidden = noSelectedCurriculum
            self.learningCompetencyView.isHidden = noSelectedPeriod
        }
    }
    
    // MARK: - Table View Data Source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        //return self.isVersion25 ? 2 : 1
        return 2
    }
    
    // MARK: - Text Field Key Creator
    
    func keyForTextField(_ textField: UITextField) -> String {
        
        if (textField == self.titleTextField) {
            return "name"
        }
        
        if (textField == self.schoolNameTextField) {
            return "school_name"
        }
        
        if (textField == self.gradeLevelTextField) {
            return "level"
        }
        
        if (textField == self.unitTextField) {
            return "unit"
        }
        
        if (textField == self.quarterTextField) {
            return "quarter"
        }
        
        if (textField == self.startDateTextField) {
            return "start_date"
        }
        
        if (textField == self.endDateTextField) {
            return "end_date"
        }
        
        return "name"
    }
    
    // MARK: - Text Field Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
        
        if (self.lessonObject != nil) {
            if (textField == self.titleTextField) {
                if (newText.length > 50) {
                    return false
                }
            }
            
            if (textField == self.unitTextField) {
                let numberSet = CharacterSet.decimalDigits
                if ((string as NSString).rangeOfCharacter(from: numberSet.inverted).location != NSNotFound) {
                    return false
                }
                
                if (newText.length > 2) {
                    return false
                }
            }
            
            if (textField == self.gradeLevelTextField) {
                let numberSet = CharacterSet.decimalDigits
                if ((string as NSString).rangeOfCharacter(from: numberSet.inverted).location != NSNotFound) {
                    return false
                }
                
                if (newText.length > 2) {
                    return false
                }
            }
            
            if ((string as NSString).length == 0) {
                let key = self.keyForTextField(textField)
                let data = [key: newText]
                self.saveChanges(data)
            }
        }
        
        let key = self.keyForTextField(textField)
        let data = [key: newText]
        self.saveChanges(data)
        
        return true
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let newText = textView.text as NSString
        let data = ["school_address": newText]
        self.saveChanges(data)
    }
    
    // MARK: - Date Picker View
    
    func setupDatePickerView() {
        self.startDateButton.tag = LPMLessonOverviewTimeFrame.startDate.rawValue
        self.endDateButton.tag = LPMLessonOverviewTimeFrame.endDate.rawValue
        
        let datePickerViewAction = #selector(self.showDatePickerView(_:))
        self.startDateButton.addTarget(self, action: datePickerViewAction, for: .touchUpInside)
        self.endDateButton.addTarget(self, action: datePickerViewAction, for: .touchUpInside)
    }
    
    func showDatePickerView(_ sender: UIButton) {
        self.timeFrameButtonTag = sender.tag
        
        self.datePickerView = LPMDatePickerPopover.init(nibName: "LPMDatePickerPopover", bundle: nil)
        self.datePickerView.headerTitle = ""
        self.datePickerView.datePickerMode = UIDatePickerMode.date
        self.datePickerView.dateFormat = KLPDisplayDateFormatShort
        self.datePickerView.delegate = self
        
        self.datePickerView.modalPresentationStyle = .popover
        self.datePickerView.preferredContentSize = CGSize(width: 300.0, height: 200.0)
        self.datePickerView.popoverPresentationController?.permittedArrowDirections = .right
        self.datePickerView.popoverPresentationController?.sourceView = sender
        self.datePickerView.popoverPresentationController?.sourceRect = sender.bounds
        self.present(self.datePickerView, animated: true, completion: nil)
    }
    
    func selectedDateString(_ dateString: String) {
        var textField = self.startDateTextField
        
        if (self.timeFrameButtonTag == LPMLessonOverviewTimeFrame.startDate.rawValue) {
            self.startDateTextField.text = dateString
            textField = self.startDateTextField
        }
        
        if (self.timeFrameButtonTag == LPMLessonOverviewTimeFrame.endDate.rawValue) {
            self.endDateTextField.text = dateString
            textField = self.endDateTextField
        }
        
        let key = self.keyForTextField(textField!)
        let data = [key: (dateString as NSString)]
        self.saveChanges(data)
    }
    
    // MARK: - Lesson Quarter Picker
    
    func showLessonQuarterPopover(_ sender: UIButton) {
        self.quarterPopover = LPMLessonQuarterPopover(nibName: "LPMLessonQuarterPopover", bundle: nil)
        self.quarterPopover.delegate = self
 
        self.quarterPopover.modalPresentationStyle = .popover
        self.quarterPopover.preferredContentSize = CGSize(width: 160.0, height: 132.0)
        self.quarterPopover.popoverPresentationController?.permittedArrowDirections = .right
        self.quarterPopover.popoverPresentationController?.sourceView = sender
        self.quarterPopover.popoverPresentationController?.sourceRect = sender.bounds
        self.present(self.quarterPopover, animated: true, completion: nil)
    }
    
    func selectedLessonQuarter(_ quarter: String) {
        self.quarterTextField.text = quarter

        let saveQuarter = quarter.replacingOccurrences(of: "Quarter", with: "") as NSString
        let key = self.keyForTextField(self.quarterTextField)
        self.saveChanges([key: saveQuarter])
    }
    
    // MARK: - Curriculum Planner Picker
    
    func showCurriculumPlannerPopover(_ sender: UIButton) {
        if let courseID = self.lessonObject.value(forKey: "course_id") as? String {
            self.curriculumPlannerPopover = LPMCurriculumPlannerPopover(nibName: "LPMCurriculumPlannerPopover", bundle: nil)
            self.curriculumPlannerPopover.courseID = courseID
            self.curriculumPlannerPopover.delegate = self
            
            self.curriculumPlannerPopover.modalPresentationStyle = .popover
            self.curriculumPlannerPopover.preferredContentSize = CGSize(width: 500.0, height: 150.0)
            self.curriculumPlannerPopover.popoverPresentationController?.permittedArrowDirections = .right
            self.curriculumPlannerPopover.popoverPresentationController?.sourceView = sender
            self.curriculumPlannerPopover.popoverPresentationController?.sourceRect = sender.bounds
            self.present(self.curriculumPlannerPopover, animated: true, completion: nil)
        }
    }
    
    func selectedCurriculumObject(_ object: NSDictionary) {
        self.curriculumPeriodView.isHidden = true
        self.learningCompetencyView.isHidden = true
        
        if let curriculumID = object.value(forKey: "id") as? String {
            if let curriculumTitle = object.value(forKey: "title") as? String {
                let shouldHide = curriculumID == "" ? true : false
                self.curriculumPeriodView.isHidden = shouldHide
                
                let defaultText = NSLocalizedString("Please Select", comment: "")
                self.curriculumPlannerTextfield.text = shouldHide ? defaultText : curriculumTitle
                self.curriculumPeriodTextfield.text = defaultText
                
                // NOTE: This is just temporary for cpm integration
                // See CopyLesson entity: All curriculum-related attributes are just temporary
                let data = ["curriculum_id": curriculumID, "curriculum_title": curriculumTitle]
                self.saveChanges(data as [String : NSString])
            }
        }
    }
    
    // MARK: - Curriculum Period Picker
    
    func showCurriculumPeriodPopover(_ sender: UIButton) {
        self.curriculumPeriodPopover = LPMCurriculumPeriodPopover(nibName: "LPMCurriculumPeriodPopover", bundle: nil)
        self.curriculumPeriodPopover.curriculumID = self.lessonObject.value(forKey: "curriculum_id") as! String
        self.curriculumPeriodPopover.delegate = self
        
        self.curriculumPeriodPopover.modalPresentationStyle = .popover
        self.curriculumPeriodPopover.preferredContentSize = CGSize(width: 500.0, height: 150.0)
        self.curriculumPeriodPopover.popoverPresentationController?.permittedArrowDirections = .right
        self.curriculumPeriodPopover.popoverPresentationController?.sourceView = sender
        self.curriculumPeriodPopover.popoverPresentationController?.sourceRect = sender.bounds
        self.present(self.curriculumPeriodPopover, animated: true, completion: nil)
    }
    
    func selectedCurriculumPeriodObject(_ object: NSDictionary) {
        if let curriculumPeriodID = object.value(forKey: "id") as? Int {
            if let curriculumPeriodName = object.value(forKey: "name") as? String {
                let shouldHide = curriculumPeriodID == 0 ? true : false
                self.curriculumPeriodTextfield.text = shouldHide ? NSLocalizedString("Please Select", comment: "") : curriculumPeriodName
                
                // NOTE: This is just temporary for cpm integration
                // See CopyLesson entity: All curriculum-related attributes are just temporary
                let data = ["curriculum_period_id": "\(curriculumPeriodID)", "curriculum_period_name": curriculumPeriodName]
                self.saveChanges(data as [String : NSString])
                
                //if (!shouldHide) {
                //    self.curriculumPlannerDataManager.requestLearningCompetencyListForPeriodWithID("\(curriculumPeriodID)", completionHandler: { (success) in
                //        dispatch_async(dispatch_get_main_queue(), {
                //            self.learningCompetencyView.hidden = !success
                //        })
                //    })
                //}
                
                if (!shouldHide) {
                    self.curriculumPlannerDataManager.requestLearningCompetencyListForPeriodWithID("\(curriculumPeriodID)", completionHandler: { (success) in
                        var okay = success
                        
                        if okay {
                            if let competencies = self.lessonPlanDataManager.fetchPreAssociatedLearningCompetencies() as? [NSManagedObject] {
                                okay = self.curriculumPlannerDataManager.antiJoinPreAssociatedLearningCompetencies(competencies)
                            }
                        }
                        
                        DispatchQueue.main.async(execute: {
                            self.learningCompetencyView.isHidden = !okay
                        })
                    })
                }
            }
        }
    }
    
    // MARK: - Saving of Updated Data
    
    func saveChanges(_ data: [String: NSString]) {
        let lpid = self.lessonObject.value(forKey: "lp_id") as! String
        let predicate = self.lessonPlanDataManager.predicate(forKeyPath: "lp_id", andValue: lpid)
        self.lessonPlanDataManager.updateEntity(kCopyLessonEntity, predicate: predicate, withData: data)
        self.progressViewPostNotification()
    }
    
    // MARK: - Progress View Post Notification
    
    func progressViewPostNotification() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateProgressView"), object: nil, userInfo: nil)
    }
}
