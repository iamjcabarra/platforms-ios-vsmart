//
//  NoteBookModalView.m
//  V-Smart
//
//  Created by Julius Abarra on 03/05/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "NoteBookModalView.h"
#import "NoteDataManager.h"

@interface NoteBookModalView () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) NoteDataManager *ndm;
@property (strong, nonatomic) NoteBook *selectedNoteBook;

@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *moveButton;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;

@property (strong, nonatomic) NSArray *noteBookList;

@end

@implementation NoteBookModalView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Title
    self.titleLabel.text = [NSLocalizedString(@"Select Notebook", nil) uppercaseString];

    // Data Manager
    self.ndm = [NoteDataManager sharedInstance];
    self.noteBookList = [NSArray arrayWithArray:[self.ndm getNoteBooks:NO useMainThread:YES]];
    
    // Picker View
    self.pickerView.hidden = NO;
    self.pickerView.userInteractionEnabled = YES;
    
    // Move Button
    NSString *buttonTitle = NSLocalizedString(@"Move", nil);
    
    self.moveButton.enabled = YES;
    self.moveButton.userInteractionEnabled = YES;
    
    [self.moveButton addTarget:self
                        action:@selector(moveButtonAction:)
              forControlEvents:UIControlEventTouchUpInside];
    
    if (self.noteBookList.count <= 1) {
        self.pickerView.hidden = YES;
        self.pickerView.userInteractionEnabled = NO;
        
        NSString *message = NSLocalizedString(@"No other available notebook where you can move this note to.", nil);        self.messageLabel.text = message;
        
        buttonTitle = NSLocalizedString(@"Okay", nil);
    }
    
    [self.moveButton setTitle:buttonTitle forState:UIControlStateNormal];
    [self.moveButton setTitle:buttonTitle forState:UIControlStateSelected];
    [self.moveButton setTitle:buttonTitle forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

- (void)moveButtonAction:(id)sender {
    if (self.selectedNoteBook == nil) {
        if (self.noteBookList.count > 0) {
            self.selectedNoteBook = [self.noteBookList objectAtIndex:0];
        }
    }
    
    if (self.selectedNoteBook != nil) {
        [self.ndm moveNote:self.note toNoteBook:self.selectedNoteBook useMainThread:YES];
        [self.delegate shouldReloadNoteCollection:YES];
    }
    else {
        NSString *message = NSLocalizedString(@"Please select a notebook.", nil);
        [self.view makeToast:message duration:2.0f position:@"center"];
    }
}

#pragma mark - Picker View Data Source

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView; {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.noteBookList.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NoteBook *noteBook = [self.noteBookList objectAtIndex:row];
    NSString *noteBookID = [self stringValue:noteBook.noteBookID];
    NSString *noteBookName = [self stringValue:noteBook.noteBookName];
    
    if ([noteBookID isEqualToString:self.currentNoteBook.noteBookID]) {
        return nil;
    }
    
    if ([self.currentNoteBook.noteBookName isEqualToString:@"General"]) {
        if ([noteBookName isEqualToString:self.currentNoteBook.noteBookName]) {
            return nil;
        }
    }
        
    return noteBookName;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.selectedNoteBook = [self.noteBookList objectAtIndex:row];
}

@end
