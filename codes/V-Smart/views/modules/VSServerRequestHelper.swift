//
//  VSServerRequestHelper.swift
//  V-Smart
//
//  Created by Julius Abarra on 20/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

protocol NetworkProtocol {
    var isProductionMode : Bool { get }
}

extension NetworkProtocol {
    func retrieveVSmartBaseURL() -> String {
        if let server = UserDefaults.standard.string(forKey: "baseurl_preference") {
            return server.trimmingCharacters(in: CharacterSet.whitespaces)
        }
        
        return ""
    }
    
    func retrieveVSmartBookURL() -> String {
        if let server = UserDefaults.standard.string(forKey: "bookurl_preference") {
            return server.trimmingCharacters(in: CharacterSet.whitespaces)
        }
        
        return ""
    }
    
    func retrieveVSmartAPIURL() -> String {
        if let server = UserDefaults.standard.string(forKey: "serviceurl_preference") {
            return server
        }
        
        return ""
    }
    
    func buildURLFromRequestEndPoint(_ endPoint: NSString) -> URL {
        var urlString = "http://\(self.retrieveVSmartBaseURL())\(endPoint)" as NSString
        
        if (urlString.range(of: "vsmart-rest-dev").length > 0) {
            if (self.isProductionMode) {
                let base = self.retrieveVSmartBaseURL() as String
                let api = self.retrieveVSmartAPIURL() as String
                
                urlString = urlString.replacingOccurrences(of: base, with: api) as NSString
                urlString = urlString.replacingOccurrences(of: "vsmart-rest-dev", with: "") as NSString
                urlString = urlString.replacingOccurrences(of: "//v1", with: "/v1") as NSString
                urlString = urlString.replacingOccurrences(of: "//v2", with: "/v2") as NSString
                urlString = urlString.replacingOccurrences(of: "http:/v", with: "http://v") as NSString
            }
        }
        
        let url = URL(string: urlString as String)!
        print("Built url: \(url)")
        
        return url
    }
    
    func buildURLRequestWithMethod(_ method: NSString, url: URL, andBody: Any?) -> URLRequest {
        let request = NSMutableURLRequest.init(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = method as String
        
        if let body = andBody {
            do {
                let postData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue:0))
                let jString = NSString.init(data: postData, encoding: String.Encoding.utf8.rawValue)
                print("jString: <start>----\(jString)---<end>")
                
                request.httpBody = postData
            }
            catch let error {
                print("Error building url request: \(error)")
            }
        }
        
        return request as URLRequest
    }
    
    func buildFormDataURLRequestWithURL(_ url: URL, body: AnyObject) -> URLRequest {
        let request = NSMutableURLRequest.init(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        let httpBody = NSMutableData()
        let boundary = "Boundary-\(UUID().uuidString)"
        
        body.enumerateKeysAndObjects { (key, value, stop) in
            httpBody.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            httpBody.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            httpBody.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        httpBody.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = httpBody as Data

        return request as URLRequest
    }
    
    func encode(string:String) -> String? {
        let customAllowedSet =  CharacterSet.urlQueryAllowed
        let escapedString = string.addingPercentEncoding(withAllowedCharacters: customAllowedSet)
        return escapedString
    }
}
