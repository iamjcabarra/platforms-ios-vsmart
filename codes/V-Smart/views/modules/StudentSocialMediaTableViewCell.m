//
//  StudentSocialMediaTableViewCell.m
//  V-Smart
//
//  Created by Julius Abarra on 10/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "StudentSocialMediaTableViewCell.h"

@implementation StudentSocialMediaTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
