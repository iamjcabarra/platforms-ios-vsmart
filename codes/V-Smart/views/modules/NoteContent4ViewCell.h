//
//  NoteContent4ViewCell.h
//  V-Smart
//
//  Created by VhaL on 2/12/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoteColorViewController.h"
#import "NoteContentTagViewCell.h"

@interface NoteContent4ViewCell : UITableViewCell <UIPickerViewDataSource, UIPickerViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIButton *buttonAdd;
@property (nonatomic, weak) IBOutlet UIButton *buttonBlue;
@property (nonatomic, weak) IBOutlet UIButton *buttonRed;
@property (nonatomic, weak) IBOutlet UIButton *buttonYellow;
@property (nonatomic, weak) IBOutlet UIButton *buttonPurple;
@property (nonatomic, weak) IBOutlet UIButton *buttonGreen;
@property (nonatomic, weak) IBOutlet UIButton *buttonWhite;
@property (nonatomic, weak) IBOutlet UIButton *buttonFolder;
@property (nonatomic, weak) IBOutlet UIPickerView *pickerFolder;
@property (nonatomic, weak) IBOutlet UITextField *textFieldTag;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *tagArray;
@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) int selectedColorId;
@property (nonatomic, strong) NSArray *folderArray;
@property (nonatomic, strong) NSString *selectedNoteBookID;

-(IBAction)buttonColorTapped:(id)sender;
-(IBAction)buttonFolderTapped:(id)sender;
-(IBAction)buttonAddTapped:(id)sender;
-(IBAction)buttonTagRemoveTapped:(id)sender;
-(IBAction)textFieldTagUpdatedReturned:(id)sender withOriginalText:(NSString*)originalText;
-(void)initializeCellWithDelegate:(id)parent;
-(NSString*)getDelimitedTags;
@end
