//
//  LPMDataManager.swift
//  V-Smart
//
//  Created by Julius Abarra on 23/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

class LPMDataManager: LPMRoutes {
    
    // MARK: - Singleton
    
    static let sharedInstance: LPMDataManager = {
        let singleton = LPMDataManager()
        return singleton
    }()
    
    // MARK: - Protocols
    
    var isProductionMode: Bool = true
    var dateFormatter: DateFormatter! = DateFormatter()
    var userDefaults: UserDefaults! = UserDefaults.standard
    
    // MARK: - Properties
    
    fileprivate let db = VSCoreDataStack(name: "LPMDataModel")
    fileprivate let session = URLSession.shared
    
    // MARK: - Closures
    
    typealias LPMDoneBlock = (_ doneBlock: Bool) -> Void
    typealias LPMDataBlock = (_ dataBlock: [String: AnyObject]?) -> Void
    typealias LPMObjectBlock = (_ objectBlock: NSManagedObject?) -> Void
    
    // MARK: - Lesson Plan Template API
    
    func requestDetailsForLessonTemplate(_ template: String, handler: @escaping LPMDoneBlock) {
        let endPoint = "/v1/learningplan/template/\(template)"
        let request = self.route("GET", uri: endPoint, body: nil)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                handler(false)
                return
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String: AnyObject] else {
                handler(false)
                return
            }
            
            var success = false
            
            if (self.okayToParseResponse(dictionary)) {
                guard let records = dictionary["records"] as? [String: AnyObject] else {
                    print("can't parse dll template records")
                    handler(false)
                    return
                }
                
                print("template records: \(records)")
                
                let ctx = self.db.objectContext()
                
                if (template == LPMConstants.Template.DLL) {
                    guard let contents = records["contents"] as? [String: AnyObject] else {
                        print("can't parse dll template records contents")
                        handler(false)
                        return
                    }
                    
                    let entities = [LPMConstants.Entity.LESSON_OVERVIEW,
                                    LPMConstants.Entity.DLL_TEMPLATE,
                                    LPMConstants.Entity.DLL_TEMPLATE_CONTENT,
                                    LPMConstants.Entity.DLL_TEMPLATE_CONTENT_DAY,
                                    LPMConstants.Entity.DLL_TEMPLATE_CONTENT_ELEMENT,
                                    LPMConstants.Entity.PRE_ASSOCIATED_COMPETENCY]
                    
                    let cleared = self.clearEntities(entities)
                    
                    if cleared {
                        ctx?.performAndWait({
                            let template_id = self.stringValue(records["template_id"])
                            let template_name = self.stringValue(records["template_name"])
                            
                            let predicate = NSComparisonPredicate(keyPath: "template_id", withValue: template_id!, isExact: true)
                            let template_mo = self.db.retrieveEntity(LPMConstants.Entity.DLL_TEMPLATE, context: ctx!, filter: predicate)
                            
                            self.relateDLLTemplateContents(contents, toTemplateObject: template_mo, templateID: template_id!)
                            
                            template_mo.setValue(template_id!, forKey: "template_id")
                            template_mo.setValue(template_name!, forKey: "template_name")
                            
                            success = self.db.saveObjectContext(ctx!
                            )
                        })
                    }
                }
                
                if (template == LPMConstants.Template.SMD) {
                    guard let contents = records["contents"] as? [[String: AnyObject]] else {
                        print("can't parse smd template records contents")
                        handler(false)
                        return
                    }
                    
                    let entities = [LPMConstants.Entity.LESSON_OVERVIEW,
                                    LPMConstants.Entity.SMD_TEMPLATE,
                                    LPMConstants.Entity.SMD_TEMPLATE_CONTENT,
                                    LPMConstants.Entity.PRE_ASSOCIATED_COMPETENCY]
                    
                    let cleared = self.clearEntities(entities)
                    
                    if cleared {
                        ctx?.performAndWait({
                            let template_id = self.stringValue(records["template_id"])
                            let template_name = self.stringValue(records["template_name"])
                            
                            let predicate = NSComparisonPredicate(keyPath: "template_id", withValue: template_id!, isExact: true)
                            let template_mo = self.db.retrieveEntity(LPMConstants.Entity.SMD_TEMPLATE, context: ctx!, filter: predicate)
                            
                            self.relateSMDTemplateContents(contents, toTemplateObject: template_mo)
                            
                            template_mo.setValue(template_id!, forKey: "template_id")
                            template_mo.setValue(template_name!, forKey: "template_name")
                            
                            success = self.db.saveObjectContext(ctx!)
                        })
                    }
                }
            }
            
            handler(success)
        }) 
        
        task.resume()
    }
    
    func requestDetailsForLessonPlanWithID(_ lpid: String, template: String, handler: @escaping LPMDoneBlock) {
        let endPoint = "/v1/learningplan/template/learningplan/id/\(lpid)"
        let request = self.route("GET", uri: endPoint, body: nil)
        
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                handler(false)
                return
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String: AnyObject] else {
                handler(false)
                return
            }
            
            var success = false
            
            if (self.okayToParseResponse(dictionary)) {
                guard let records = dictionary["records"] as? [String: AnyObject] else {
                    print("can't parse lesson plan details")
                    handler(false)
                    return
                }
                
                print("lesson plan details: \(records)")
                let ctx = self.db.objectContext()
                
                let entities = [LPMConstants.Entity.LESSON_OVERVIEW,
                                LPMConstants.Entity.DLL_TEMPLATE,
                                LPMConstants.Entity.DLL_TEMPLATE_CONTENT,
                                LPMConstants.Entity.DLL_TEMPLATE_CONTENT_DAY,
                                LPMConstants.Entity.DLL_TEMPLATE_CONTENT_ELEMENT,
                                LPMConstants.Entity.SMD_TEMPLATE,
                                LPMConstants.Entity.SMD_TEMPLATE_CONTENT,
                                LPMConstants.Entity.PRE_ASSOCIATED_COMPETENCY,
                                LPMConstants.Entity.LESSON_COMMENT,
                                LPMConstants.Entity.LESSON_HISTORY]
                
                let cleared = self.clearEntities(entities)
                
                if cleared {
                    ctx?.performAndWait({
                        
                        // Lesson Plan Overview
                        let name = self.stringValue(records["name"])
                        let template_id = self.stringValue(records["template_id"])
                        let template_name = self.stringValue(records["template_name"])
                        let teacher_name = self.stringValue(records["teacher_name"])
                        let school_name = self.stringValue(records["school_name"])
                        let school_address = self.stringValue(records["school_address"])
                        let course_id = self.stringValue(records["course_id"])
                        let level = self.stringValue(records["level"])
                        let unit = self.stringValue(records["unit"])
                        let quarter = self.stringValue(records["quarter"])
                        let start_date = self.stringValue(records["start_date"])
                        let end_date = self.stringValue(records["end_date"])
                        let status = self.stringValue(records["status"])
                        
                        let predicate_a = NSComparisonPredicate(keyPath: "lp_id", withValue: lpid, isExact: true)
                        let overview_mo = self.db.retrieveEntity(LPMConstants.Entity.LESSON_OVERVIEW, context: ctx!, filter: predicate_a)
                        
                        let fFormat = LPMConstants.DateFormat.SERVER
                        let tFormat = LPMConstants.DateFormat.LESSON_CREATOR
                        let formatted_start_date = self.dateString(start_date!, fromFormat: fFormat, toFormat: tFormat, fromTimeZone: "UTC", toTimeZone: "")
                        let formatted_end_date = self.dateString(end_date!, fromFormat: fFormat, toFormat: tFormat, fromTimeZone: "UTC", toTimeZone: "")
                        
                        overview_mo.setValue(lpid, forKey: "lp_id")
                        overview_mo.setValue(name!, forKey: "name")
                        overview_mo.setValue(template_id!, forKey: "template_id")
                        overview_mo.setValue(template_name!, forKey: "template_name")
                        overview_mo.setValue(teacher_name!, forKey: "teacher_name")
                        overview_mo.setValue(school_name!, forKey: "school_name")
                        overview_mo.setValue(school_address!, forKey: "school_address")
                        overview_mo.setValue(course_id!, forKey: "course_id")
                        overview_mo.setValue(level!, forKey: "level")
                        overview_mo.setValue(unit!, forKey: "unit")
                        overview_mo.setValue(quarter!, forKey: "quarter")
                        overview_mo.setValue(formatted_start_date, forKey: "start_date")
                        overview_mo.setValue(formatted_end_date, forKey: "end_date")
                        overview_mo.setValue(status!, forKey: "status")
                        overview_mo.setValue("", forKey: "curriculum_id")
                        overview_mo.setValue("", forKey: "curriculum_title")
                        overview_mo.setValue("", forKey: "curriculum_period_id")
                        overview_mo.setValue("", forKey: "curriculum_period_name")
                        
                        // Associated Learning Competencies
                        
                        if let competencies = records["competencies"] as? [[String: AnyObject]] {
                            for competency in competencies {
                                let curriculum_id = self.stringValue(competency["curriculum_id"])
                                let period_id = self.stringValue(competency["period_id"])
                                let ccc_id = self.stringValue(competency["ccc_id"])
                                let content = self.stringValue(competency["content"])
                                
                                let predicate_b = NSComparisonPredicate(keyPath: "ccc_id", withValue: ccc_id!, isExact: true)
                                let competency_mo = self.db.retrieveEntity(LPMConstants.Entity.PRE_ASSOCIATED_COMPETENCY, context: ctx!, filter: predicate_b)
                                
                                competency_mo.setValue(curriculum_id!, forKey: "curriculum_id")
                                competency_mo.setValue(period_id!, forKey: "period_id")
                                competency_mo.setValue(Int(ccc_id!), forKey: "ccc_id")
                                competency_mo.setValue(content!, forKey: "content")
                                
                                overview_mo.setValue(period_id!, forKey: "curriculum_period_id")
                            }
                        }
                        
                        // Lesson Plan Comments
                        
                        if let comments = records["comments"] as? [[String: AnyObject]] {
                            for comment in comments {
                                let id = self.stringValue(comment["id"])
                                let lp_id = self.stringValue(comment["lp_id"])
                                let user_id = self.stringValue(comment["user_id"])
                                let first_name = self.stringValue(comment["first_name"])
                                let last_name = self.stringValue(comment["last_name"])
                                let avatar = self.stringValue(comment["avatar"])
                                let actual_comment = self.stringValue(comment["comment"])
                                let lpc_is_deleted = self.stringValue(comment["lpc_is_deleted"])
                                let date_created = self.stringValue(comment["date_created"])
                                let date_modified = self.stringValue(comment["date_modified"])
                            
                                let predicate_c = NSComparisonPredicate(keyPath: "id", withValue: id!, isExact: true)
                                let comment_mo = self.db.retrieveEntity(LPMConstants.Entity.LESSON_COMMENT, context: ctx!, filter: predicate_c)
                                
                                let avatar_url = "http://\(self.retrieveVSmartBaseURL())/\(avatar!)"
                                let sort_date_modified = self.date(fromString: date_modified!, withFormat: LPMConstants.DateFormat.SERVER, convertToLocalTime: true)
                                
                                let fFormat = LPMConstants.DateFormat.SERVER
                                let tFormat = LPMConstants.DateFormat.LESSON_COMMENT
                                let formatted_date_created = self.dateString(date_created!, fromFormat: fFormat, toFormat: tFormat, fromTimeZone: "UTC", toTimeZone: "")
                                let formatted_date_modified = self.dateString(date_modified!, fromFormat: fFormat, toFormat: tFormat, fromTimeZone: "UTC", toTimeZone: "")
                                
                                comment_mo.setValue(id!, forKey: "id")
                                comment_mo.setValue(lp_id!, forKey: "lp_id")
                                comment_mo.setValue(user_id!, forKey: "user_id")
                                comment_mo.setValue(first_name!, forKey: "first_name")
                                comment_mo.setValue(last_name!, forKey: "last_name")
                                comment_mo.setValue(avatar_url, forKey: "avatar")
                                comment_mo.setValue(actual_comment!, forKey: "comment")
                                comment_mo.setValue(lpc_is_deleted!, forKey: "lpc_is_deleted")
                                comment_mo.setValue(formatted_date_created, forKey: "date_created")
                                comment_mo.setValue(formatted_date_modified, forKey: "date_modified")
                                comment_mo.setValue(sort_date_modified, forKey: "sort_date_modified")
                            }
                        }
                        
                        // Lesson Plan History
                        
                        if let histories = records["history"] as? [[String: AnyObject]] {
                            for history in histories {
                                let id = self.stringValue(history["id"])
                                let lp_id = self.stringValue(history["lp_id"])
                                let user_id = self.stringValue(history["user_id"])
                                let first_name = self.stringValue(history["first_name"])
                                let last_name = self.stringValue(history["last_name"])
                                let avatar = self.stringValue(history["avatar"])
                                let actual_history = self.stringValue(history["history"])
                                let lpc_is_deleted = self.stringValue(history["lpc_is_deleted"])
                                let date_created = self.stringValue(history["date_created"])
                                let date_modified = self.stringValue(history["date_modified"])
                                
                                let predicate_d = NSComparisonPredicate(keyPath: "id", withValue: id!, isExact: true)
                                let history_mo = self.db.retrieveEntity(LPMConstants.Entity.LESSON_HISTORY, context: ctx!, filter: predicate_d)
                                
                                let avatar_url = "http://\(self.retrieveVSmartBaseURL())/\(avatar!)"
                                let sort_date_modified = self.date(fromString: date_modified!, withFormat: LPMConstants.DateFormat.SERVER, convertToLocalTime: true)
                                
                                let fFormat = LPMConstants.DateFormat.SERVER
                                let tFormat = LPMConstants.DateFormat.LESSON_HISTORY
                                let formatted_date_created = self.dateString(date_created!, fromFormat: fFormat, toFormat: tFormat, fromTimeZone: "UTC", toTimeZone: "")
                                let formatted_date_modified = self.dateString(date_modified!, fromFormat: fFormat, toFormat: tFormat, fromTimeZone: "UTC", toTimeZone: "")
                                
                                history_mo.setValue(id!, forKey: "id")
                                history_mo.setValue(lp_id!, forKey: "lp_id")
                                history_mo.setValue(user_id!, forKey: "user_id")
                                history_mo.setValue(first_name!, forKey: "first_name")
                                history_mo.setValue(last_name!, forKey: "last_name")
                                history_mo.setValue(avatar_url, forKey: "avatar")
                                history_mo.setValue(actual_history!, forKey: "history")
                                history_mo.setValue(lpc_is_deleted!, forKey: "lpc_is_deleted")
                                history_mo.setValue(formatted_date_created, forKey: "date_created")
                                history_mo.setValue(formatted_date_modified, forKey: "date_modified")
                                history_mo.setValue(sort_date_modified, forKey: "sort_date_modified")
                            }
                        }
                        
                        // Lesson Plan Contents
                        
                        if template == LPMConstants.Template.DLL {
                            guard let content = records["content"] as? [String: AnyObject] else {
                                print("can't parse lesson plan details contents")
                                handler(false)
                                return
                            }
                            
                            let predicate_e = NSComparisonPredicate(keyPath: "template_id", withValue: template_id!, isExact: true)
                            let template_mo = self.db.retrieveEntity(LPMConstants.Entity.DLL_TEMPLATE, context: ctx!, filter: predicate_e)
                            self.relateDLLTemplateContents(content, toTemplateObject: template_mo, templateID: template_id!)
                            template_mo.setValue(template_id!, forKey: "template_id")
                            template_mo.setValue(template_name!, forKey: "template_name")
                            
                            if let content = content["content"] as? [[String: AnyObject]] {
                                for c in content {
                                    let element_id = self.stringValue(c["element_id"])
                                    let day_id = self.stringValue(c["day_id"])
                                    let actual_content = self.stringValue(c["content"])
                                    let element_id_day_id = "\(element_id!)-\(day_id!)"
                                    
                                    let predicate_d = NSComparisonPredicate(keyPath: "element_id_day_id", withValue: element_id_day_id, isExact: true)
                                    let element_mo = self.db.retrieveEntity(LPMConstants.Entity.DLL_TEMPLATE_CONTENT_ELEMENT, context: ctx!, filter: predicate_d)
                                    
                                    element_mo.setValue(actual_content!, forKey: "content")
                                }
                            }
                        }
                        
                        if template == LPMConstants.Template.SMD {
                            guard let contents = records["content"] as? [[String: AnyObject]] else {
                                print("can't parse lesson plan details contents")
                                handler(false)
                                return
                            }
                            
                            let predicate_e = NSComparisonPredicate(keyPath: "template_id", withValue: template_id!, isExact: true)
                            let template_mo = self.db.retrieveEntity(LPMConstants.Entity.SMD_TEMPLATE, context: ctx!, filter: predicate_e)
                            self.relateSMDTemplateContents(contents, toTemplateObject: template_mo)
                            template_mo.setValue(template_id!, forKey: "template_id")
                            template_mo.setValue(template_name!, forKey: "template_name")
                        }
                        
                        success = self.db.saveObjectContext(ctx!)
                    })
                }
            }
            
            handler(success)
        }) 
        
        task.resume()
    }
    
    // MARK: DLL Template Relational Schema
    
    fileprivate func relateDLLTemplateContents(_ contents: [String: AnyObject], toTemplateObject: NSManagedObject, templateID: String) {
        guard let days = contents["days"] as? [[String: AnyObject]],
            let elements = contents["elements"] as? [[String: AnyObject]],
            let ctx = toTemplateObject.managedObjectContext else {
                print("can't relate ddl template contents to template object")
                return
        }
        
        let predicate = NSComparisonPredicate(keyPath: "template_id", withValue: templateID, isExact: true)
        let content_mo = self.db.retrieveEntity(LPMConstants.Entity.DLL_TEMPLATE_CONTENT, context: ctx, filter: predicate)
        
        self.relateDLLTemplateContentDays(days, elements: elements, toContentObject: content_mo)
        
        content_mo.setValue(templateID, forKey: "template_id")
        toTemplateObject.setValue(content_mo, forKey: "content")
    }
    
    fileprivate func relateDLLTemplateContentDays(_ days: [[String: AnyObject]], elements: [[String: AnyObject]], toContentObject: NSManagedObject) {
        guard let ctx = toContentObject.managedObjectContext else {
            print("can't relate ddl template content days to content object")
            return
        }
        
        if days.count > 0 {
            var day_set = Set<NSManagedObject>()
            
            for day in days {
                let day_id = self.stringValue(day["id"])
                let day = self.stringValue(day["day"])
                
                let predicate = NSComparisonPredicate(keyPath: "id", withValue: day_id!, isExact: true)
                let day_mo = self.db.retrieveEntity(LPMConstants.Entity.DLL_TEMPLATE_CONTENT_DAY, context: ctx, filter: predicate)
                
                self.relateDLLTemplateContentElements(elements, toDayObject: day_mo, dayID: day_id!)
                
                day_mo.setValue(Int(day_id!), forKey: "id")
                day_mo.setValue(day!, forKey: "day")
                
                day_set.insert(day_mo)
            }
            
            toContentObject.setValue(day_set, forKey: "days")
        }
    }
    
    fileprivate func relateDLLTemplateContentElements(_ elements: [[String: AnyObject]], toDayObject: NSManagedObject, dayID: String) {
        guard let ctx = toDayObject.managedObjectContext else {
            print("can't relate ddl template content elements to day object")
            return
        }
        
        if elements.count > 0 {
            var element_set = Set<NSManagedObject>()
            var header_id = 0
            
            for element in elements {
                let element_id = self.stringValue(element["id"])
                let name = self.stringValue(element["name"])
                let description = self.stringValue(element["description"])
                let is_header = self.stringValue(element["is_header"])
                let is_content = self.stringValue(element["is_content"])
                let element_id_day_id = "\(element_id!)-\(dayID)"
                
                let predicate = NSComparisonPredicate(keyPath: "element_id_day_id", withValue: element_id_day_id, isExact: true)
                let element_mo = self.db.retrieveEntity(LPMConstants.Entity.DLL_TEMPLATE_CONTENT_ELEMENT, context: ctx, filter: predicate)
                
                element_mo.setValue(Int(element_id!), forKey: "id")
                element_mo.setValue(name!, forKey: "name")
                element_mo.setValue(description!, forKey: "element_description")
                element_mo.setValue(is_header!, forKey: "is_header")
                element_mo.setValue(is_content!, forKey: "is_content")
                
                // Work Around: Relating content to its header
                if is_header == "1" { header_id = Int(element_id!)! }
                
                element_mo.setValue(Int(dayID), forKey: "day_id")
                element_mo.setValue(element_id_day_id, forKey: "element_id_day_id")
                element_mo.setValue(header_id, forKey: "header_id")
                element_mo.setValue("", forKey: "content")
               
                element_set.insert(element_mo)
            }
            
            toDayObject.setValue(element_set, forKey: "elements")
        }
    }
    
    // MARK: SMD Template Relational Schema
    
    fileprivate func relateSMDTemplateContents(_ contents: [[String: AnyObject]], toTemplateObject: NSManagedObject) {
        guard let ctx = toTemplateObject.managedObjectContext else {
            print("can't relate smd template contents to template object")
            return
        }
        
        var content_set = Set<NSManagedObject>()
        
        for content in contents {
            let id = self.stringValue(content["id"])
            let stage_name = self.stringValue(content["stage_name"])
            let description = self.stringValue(content["description"])
            let content = self.stringValue(content["content"])
            
            let predicate = NSComparisonPredicate(keyPath: "id", withValue: id!, isExact: true)
            let content_mo = self.db.retrieveEntity(LPMConstants.Entity.SMD_TEMPLATE_CONTENT, context: ctx, filter: predicate)
            
            content_mo.setValue(Int(id!), forKey: "id")
            content_mo.setValue(stage_name!, forKey: "stage_name")
            content_mo.setValue(description!, forKey: "content_description")
            content_mo.setValue(content!, forKey: "content")
            
            content_set.insert(content_mo)
        }
        
        toTemplateObject.setValue(content_set, forKey: "contents")
    }
    
    // MARK: - Create Lesson Plan API
    
    func requestCreateLessonPlanUsingTemplate(_ template: String, handler: @escaping LPMDataBlock) {
        let endPoint = "/v1/learningplan/template/create"
        
        guard let body = self.generatePostBodyForLessonPlanUsingTemplate(template) else {
            handler(nil)
            return
        }
        
        let request = template != LPMConstants.Template.SMD ? self.route("POST", uri: endPoint, body: body) : self.routeForFormData(endPoint, body: body)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                handler(nil)
                return
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String: AnyObject], let records = dictionary["records"] as? [String: AnyObject], let lpid = records["id"] as? String else {
                handler(nil)
                return
            }
            
            print(dictionary)
            handler(["lpid": lpid as AnyObject])
        }) 
        
        task.resume()
    }
    
    // MARK: - Update Lesson Plan API
    
    func requestUpdateLessonPlanWithID(_ lpid: String, template: String, handler: @escaping LPMDoneBlock) {
        let endPoint = "/v1/learningplan/template/update/\(lpid)"
        
        guard let body = self.generatePostBodyForLessonPlanUsingTemplate(template) else {
            handler(false)
            return
        }
        
        let request = template != LPMConstants.Template.SMD ? self.route("POST", uri: endPoint, body: body) : self.routeForFormData(endPoint, body: body)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                handler(false)
                return
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String: AnyObject] else {
                handler(false)
                return
            }
            
            let success = self.okayToParseResponse(dictionary)
            handler(success)
        }) 
        
        task.resume()
    }
    
    // MARK: - Preparation for Lesson Plan Creation
    
    func setUpDefaultLessonOverviewForCourse(_ course: String, template: NSManagedObject, handler: @escaping LPMDoneBlock) {
        let ctx = self.getWorkerContext()
        let cleared = self.db.clearEntity(LPMConstants.Entity.LESSON_OVERVIEW, ctx: ctx!, filter: nil)
        
        if cleared {
            ctx?.performAndWait({
                let template_id = self.stringValue(template.value(forKey: "id"))
                let template_name = self.stringValue(template.value(forKey: "name"))
                
                                var formatted_start_date = self.stringDateTodayWithFormat(LPMConstants.DateFormat.LESSON_CREATOR, isServerTimeZone: false)
                var formatted_end_date = self.stringDateTodayWithFormat(LPMConstants.DateFormat.LESSON_CREATOR, isServerTimeZone: false)
                
                let predicate = NSComparisonPredicate(keyPath: "lp_id", withValue: "0", isExact: true)
                let mo = self.db.retrieveEntity(LPMConstants.Entity.LESSON_OVERVIEW, context: ctx!, filter: predicate)
                
                if template_id == LPMConstants.Template.DLL {
                    var new_start_date = Date()
                    var is_moday = self.isDateMonday(new_start_date)
                    
                    while !is_moday {
                        new_start_date = self.addDays(1, toDate: new_start_date)
                        is_moday = self.isDateMonday(new_start_date)
                    }
                    
                    let new_end_date = self.addDays(4, toDate: new_start_date)
                    formatted_start_date = self.stringDate(new_start_date, format: LPMConstants.DateFormat.LESSON_CREATOR, isServerTimeZone: false)
                    formatted_end_date = self.stringDate(new_end_date, format: LPMConstants.DateFormat.LESSON_CREATOR, isServerTimeZone: false)
                }
                
                mo.setValue("0", forKey: "lp_id")
                mo.setValue("", forKey: "name")
                mo.setValue(template_id!, forKey: "template_id")
                mo.setValue(template_name!, forKey: "template_name")
                mo.setValue("0", forKey: "teacher_name")
                mo.setValue("0", forKey: "school_name")
                mo.setValue("0", forKey: "school_address")
                mo.setValue(course, forKey: "course_id")
                mo.setValue("0", forKey: "level")
                mo.setValue("", forKey: "unit")
                mo.setValue("1", forKey: "quarter")
                mo.setValue(formatted_start_date, forKey: "start_date")
                mo.setValue(formatted_end_date, forKey: "end_date")
                mo.setValue("0", forKey: "status")
                
                let text = NSLocalizedString("Please Select", comment: "")
                mo.setValue("", forKey: "curriculum_id")
                mo.setValue(text, forKey: "curriculum_title")
                mo.setValue("", forKey: "curriculum_period_id")
                mo.setValue("", forKey: "curriculum_period_name")
                
                let success = self.db.saveObjectContext(ctx!)
                handler(success)
            })
        }
        else {
            handler(false)
        }
    }
    
    // MARK: - Preparation for Create or Update Post Operation
    
    fileprivate func generatePostBodyForLessonPlanUsingTemplate(_ template: String) -> [String: Any]? {
        
        // Lesson Overview
        guard let overview = self.db.retrieveEntity(LPMConstants.Entity.LESSON_OVERVIEW, filter: nil) else {
            print("can't parse lesson plan overview...")
            return nil
        }
        
        let user_id: String! = self.logInUserID()
        let template_id: String! = self.stringValue(overview.value(forKey: "template_id"))
        let name: String! = self.stringValue(overview.value(forKey: "name"))
        let course_id: String! = self.stringValue(overview.value(forKey: "course_id"))
        let level: String! = self.stringValue(overview.value(forKey: "level"))
        let unit: String! = self.stringValue(overview.value(forKey: "unit"))
        let quarter: String! = self.stringValue(overview.value(forKey: "quarter"))
        let start_date: String! = self.stringValue(overview.value(forKey: "start_date"))
        let end_date: String! = self.stringValue(overview.value(forKey: "end_date"))
        let school_name: String! = self.stringValue(overview.value(forKey: "school_name"))
        let school_address: String! = self.stringValue(overview.value(forKey: "school_address"))
        
        let fFormat = LPMConstants.DateFormat.LESSON_CREATOR
        let tFormat = LPMConstants.DateFormat.SERVER
        let formatted_start_date = self.dateString(start_date, fromFormat: fFormat, toFormat: tFormat, fromTimeZone: "", toTimeZone: "")
        let formatted_end_date = self.dateString(end_date, fromFormat: fFormat, toFormat: tFormat, fromTimeZone: "", toTimeZone: "")
        
        // Lesson Content
        if template == LPMConstants.Template.DLL {
            guard let elements = self.db.retrieveObjects(forEntity: LPMConstants.Entity.DLL_TEMPLATE_CONTENT_ELEMENT, predicate: nil, sortDescriptor: nil) else {
                print("can't parse lesson plan content elements...")
                return nil
            }
            
            var contents = [[String: AnyObject]]()
            
            for element in elements {
                let element_id = self.stringValue(element.value(forKey: "id"))
                let day_id = self.stringValue(element.value(forKey: "day_id"))
                let content = self.stringValue(element.value(forKey: "content"))
                let data = ["element_id": element_id!, "day_id": day_id!, "content": content!]
                contents.append(data as [String : AnyObject])
            }
            
            let body: [String: Any] = ["template_id": template_id!, "user_id": user_id, "name": name! , "course_id": course_id!, "level": level!,
                                       "unit": unit! , "quarter": quarter! , "start_date": formatted_start_date , "end_date": formatted_end_date,
                                       "school_name": school_name! , "school_address": school_address! , "content": contents]
            
            return body
        }
        
        if template == LPMConstants.Template.SMD {
            guard let elements = self.db.retrieveObjects(forEntity: LPMConstants.Entity.SMD_TEMPLATE_CONTENT, predicate: nil, sortDescriptor: nil) else {
                print("can't parse lesson plan content elements...")
                return nil
            }
            
            var contents = [[String: AnyObject]]()
            
            for element in elements {
                let content_id: String! = self.stringValue(element.value(forKey: "id"))
                let content: String! = self.stringValue(element.value(forKey: "content"))
                let data = ["learning_element_id": content_id!, "content": content!]
                contents.append(data as [String : AnyObject])
            }
            
            let object: [String: Any] = ["user_id": user_id, "name": name, "course_id": course_id, "level": level , "unit": unit,
                                         "quarter": quarter, "start_date": formatted_start_date, "end_date": formatted_end_date,
                                         "school_name": school_name, "school_address": school_address, "content": contents]
            
            guard let content = self.jsonStringObject(object) else { return nil }
            
            let body: [String: Any] = ["content": content, "template_id": template , "file_learning_process_stage_id": "" , "file": "" ]
            
            return body
        
        }
        
        return nil
    }
    
    func fetchLessonOverview() -> NSManagedObject? {
        guard let overview = self.db.retrieveEntity(LPMConstants.Entity.LESSON_OVERVIEW, filter: nil) else {
            print("can't fetch lesson overview")
            return nil
        }
        
        return overview
    }
    
    func fetchDLLTemplateContentDays() -> [AnyObject]? {
        let entity = LPMConstants.Entity.DLL_TEMPLATE_CONTENT_DAY
        let properties = ["id", "day"]
        
        guard let data = self.db.retrieveEntity(entity, properties: properties, filter: nil, sortKey: "id", ascending: true) else {
            print("can't fetch dll template days")
            return nil
        }
        
        return data as [AnyObject]?
    }
    
    func fetchDLLTemplateContentElementsForDay(_ day: Int, isHeader: Bool, headerID: Int?) -> [NSManagedObject]? {
        let predicateA = NSComparisonPredicate(keyPath: "day_id", withValue: day, isExact: true)
        let value = isHeader ? "1" : "0"
        
        let predicateB = NSComparisonPredicate(keyPath: "is_header", withValue: value, isExact: true)
        
        var predicateFinal = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicateA, predicateB])
        let sortDescriptor = NSSortDescriptor.init(key: "id", ascending: true)
        
        if headerID != nil {
            let predicateC = NSComparisonPredicate(keyPath: "header_id", withValue: headerID!, isExact: true)
            predicateFinal = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicateA, predicateB, predicateC])
        }
        
        if let contents = self.db.retrieveObjects(forEntity: LPMConstants.Entity.DLL_TEMPLATE_CONTENT_ELEMENT, predicate: predicateFinal, sortDescriptor: sortDescriptor) {
            return contents
        }
        
        return nil
    }
    
    func fetchSMDTemplateContents() -> [NSManagedObject]? {
        let descriptor = NSSortDescriptor(key: "id", ascending: true)
        return self.db.retrieveObjects(forEntity: LPMConstants.Entity.SMD_TEMPLATE_CONTENT, sortDescriptor: descriptor)
    }
    
    func fetchPreAssociatedLearningCompetencies() -> [NSManagedObject]? {
        guard let competencies = self.db.retrieveObjects(forEntity: LPMConstants.Entity.PRE_ASSOCIATED_COMPETENCY, withFilter: nil) else {
            print("can't fetch pre associate learning competencies")
            return nil
        }
        
        return competencies
    }
    
    func removePreAssociatedLearningCompetency(_ competency: String) -> Bool {
        let ctx = self.getWorkerContext()
        let filter = NSComparisonPredicate(keyPath: "ccc_id", withValue: competency as AnyObject, isExact: true)
        return self.db.clearEntity(LPMConstants.Entity.PRE_ASSOCIATED_COMPETENCY, ctx: ctx!, filter: filter)
    }
    
    fileprivate func jsonStringObject(_ object: [String: Any]) -> NSString? {
        do {
            let jsonObject = try JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue:0))
            return NSString.init(data: jsonObject, encoding: String.Encoding.utf8.rawValue)
        }
        catch let error {
            print("Error converting to json: \(error)")
            return nil
        }
    }
    
    // MARK: - Lesson Plan Comment API
    
    func requestAddCommentToLessonPlanWithID(_ lpid: String, body: [String: AnyObject], handler: @escaping LPMDoneBlock) {
        let endPoint = "/v1/learningplan/template/add/comment/\(lpid)"
        let request = self.routeForFormData(endPoint, body: body)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                handler(false)
                return
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String: AnyObject] else {
                handler(false)
                return
            }
            
            let success = self.okayToParseResponse(dictionary)
            handler(success)
        }) 
        
        task.resume()
    }
    
    func requestUpdateComment(_ body: [String: AnyObject], handler: @escaping LPMDoneBlock) {
        let endPoint = "/v1/learningplan/template/edit/comment"
        let request = self.routeForFormData(endPoint, body: body)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                handler(false)
                return
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String: AnyObject] else {
                handler(false)
                return
            }
            
            let success = self.okayToParseResponse(dictionary)
            handler(success)
        }) 
        
        task.resume()
    }
    
    func requestDeleteCommentWithID(_ commentID: String, handler: @escaping LPMDoneBlock) {
        let endPoint = "/v1/learningplan/template/delete/comment/\(commentID)"
        let request = self.route("POST", uri: endPoint, body: nil)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                handler(false)
                return
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String: AnyObject] else {
                handler(false)
                return
            }
            
            let success = self.okayToParseResponse(dictionary)
            handler(success)
        }) 
        
        task.resume()
    }
    
    // MARK: - Core Data Related Methods
    
    func getMainContext() -> NSManagedObjectContext! {
        return self.db.objectMainContext()
    }
    
    func getWorkerContext() -> NSManagedObjectContext! {
        return self.db.objectContext()
    }
    
    func updateEntity(_ entity: String, predicate: NSPredicate, data: NSDictionary) -> Bool {
        guard let object = self.db.retrieveEntity(entity, filter: predicate) else {
            return false
        }

        var status = false
        
        if let ctx = object.managedObjectContext {
            ctx.performAndWait {
                let keys = data.allKeys
                
                for k in keys {
                    if let kString = k as? String {
                        object.setValue(data[kString], forKey: kString)
                    }
                }
                
                status = self.db.saveObjectContext(ctx)
            }
        }
        
        return status
    }
    
    func fetchLastAssociatedCurriculumPeriod() ->  String? {
        guard let mo = self.db.retrieveEntity(LPMConstants.Entity.LESSON_OVERVIEW, filter: nil) else {
            return nil
        }
        
        let curriculum_period_id = self.stringValue(mo.value(forKey: "curriculum_period_id"))
        return curriculum_period_id
    }
    
    fileprivate func clearEntities(_ entities: [String]) -> Bool {
        let ctx = self.getWorkerContext()
        
        for entity in entities {
            let success = self.db.clearEntity(entity, ctx: ctx!, filter: nil)
            if !success { return false }
        }
        
        return true
    }
    
    // MARK: - Non-Swift Dependency
    
    func logInUserID() -> String {
        if let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo {
            return "\(account.user.id)"
        }
        
        return ""
    }
    
    // MARK: Date-Related Utility Methods
    
    fileprivate func isDateMonday(_ date: Date) -> Bool {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let components = (calendar as NSCalendar).components([.weekday], from: date)
        return components.weekday == 2 ? true : false
    }
    
    fileprivate func addDays(_ days: Int, toDate: Date) -> Date {
        guard let date = (Calendar.current as NSCalendar).date(byAdding: .day, value: days, to: toDate, options: []) else {
            return Date()
        }
        
        return date
    }
    
    fileprivate func isDateFriday(_ date: Date) -> Bool {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let components = (calendar as NSCalendar).components([.weekday], from: date)
        return components.weekday == 6 ? true : false
    }
    
    fileprivate func dateString(_ string: String, fromFormat: String, toFormat: String, fromTimeZone: String, toTimeZone: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = fromFormat
        formatter.timeZone = fromTimeZone == "" ? NSTimeZone.local : TimeZone(identifier: fromTimeZone)
        
        guard let date = formatter.date(from: string) else {
            formatter.timeZone = toTimeZone == "" ? NSTimeZone.local : TimeZone(identifier: toTimeZone)
            formatter.dateFormat = toFormat
            return formatter.string(from: Date())
        }
        
        formatter.timeZone = toTimeZone == "" ? NSTimeZone.local : TimeZone(identifier: toTimeZone)
        formatter.dateFormat = toFormat
        return formatter.string(from: date)
    }
    
    fileprivate func stringDate(_ date: Date, format: String, isServerTimeZone: Bool) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        if isServerTimeZone { formatter.timeZone = TimeZone(identifier: "UTC") }
        return formatter.string(from: date)
    }
        
    fileprivate func stringDateTodayWithFormat(_ format: String, isServerTimeZone: Bool) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        if isServerTimeZone { formatter.timeZone = TimeZone(identifier: "UTC") }
        return formatter.string(from: Date())
    }
    
    // MARK: - Downloaded File Mapper
    
    func mapDownloadedFile(_ file: [String: String]) -> Bool {
        var mapped = false
        let ctx = self.getWorkerContext()
        
        ctx?.performAndWait {
            let file_key: String! = file["file_key"]
            let file_name: String! = file["file_name"]
            
            let predicate = self.db.createPredicate(key: "file_key", withValue: file_key, isExact: true)
            let object = self.db.retrieveEntity(LPMConstants.Entity.DOWNLOADED_FILE_MAPPER, context: ctx!, filter: predicate)
            
            object.setValue(file_key, forKey: "file_key")
            object.setValue(file_name, forKey: "file_name")
            mapped = self.db.saveObjectContext(ctx!)
        }
        
        return mapped
    }
    
    func unmapDownloadedFileForKey(_ key: String, value: String) -> Bool {
        let ctx: NSManagedObjectContext! = self.getWorkerContext()
        let predicate = self.db.createPredicate(key: key, withValue: value, isExact: true)
        return self.db.clearEntity(LPMConstants.Entity.DOWNLOADED_FILE_MAPPER, ctx: ctx, filter: predicate)
    }
    
    func isFileDownloadedForKey(_ key: String, value: String) -> Bool {
        let predicate = self.db.createPredicate(key: key, withValue: value, isExact: true)
        let object = self.db.retrieveEntity(LPMConstants.Entity.DOWNLOADED_FILE_MAPPER, filter: predicate)
        return object != nil ? true : false
    }
    
    func fetchFileNameForFileKey(_ key: String) -> String? {
        let predicate = self.db.createPredicate(key: "file_key", withValue: key, isExact: true)
        
        guard
            let object = self.db.retrieveEntity(LPMConstants.Entity.DOWNLOADED_FILE_MAPPER, filter: predicate),
            let name = object.value(forKey: "file_name") as? String else {
            return nil
        }
        
        return name
    }

}
