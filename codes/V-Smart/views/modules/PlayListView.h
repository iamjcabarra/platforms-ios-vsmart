//
//  PlayListView.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/10/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface PlayListView : UIViewController

@end
