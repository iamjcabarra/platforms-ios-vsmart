//
//  LessonDetailsCommentViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/18/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "LessonDetailsCommentViewController.h"
#import "LessonDetailsCommentTableViewCell.h"
#import "LessonDetailsTabBarViewController.h"
#import "CommentModalViewController.h"
#import "LessonPlanDataManager.h"
#import "AccountInfo.h"
#import "Utils.h"

@interface LessonDetailsCommentViewController ()

<NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, CommentModalViewDelegate> {
    NSManagedObject *mo_selected;
}

@property (nonatomic, strong) LessonPlanDataManager *lm;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITextView *txtCommentToSend;
@property (strong, nonatomic) IBOutlet UIButton *butSendComment;

@property (nonatomic, strong) NSString *userid;
@property (nonatomic, strong) NSString *lpid;

@end

@implementation LessonDetailsCommentViewController

static NSString *kCommentCellIdentifier = @"commentCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Get currently logged user's id
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    self.userid = [NSString stringWithFormat:@"%d", account.user.id];

    // Set lesson plan id
    self.lpid = [NSString stringWithFormat:@"%@", [self.lesson_mo valueForKey:@"id"]];
    
    // Set protocols for UITableView
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Dynamic table row height
    self.tableView.estimatedRowHeight = 150.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    // Set data manager and context
    self.lm = [LessonPlanDataManager sharedInstance];
    self.managedObjectContext = self.lm.mainContext;
    
    // Comment text view decoration configuration
    self.txtCommentToSend.backgroundColor = [UIColor whiteColor];
    self.txtCommentToSend.layer.borderColor = [[UIColor grayColor] CGColor];
    self.txtCommentToSend.layer.borderWidth = 1.0f;
    self.txtCommentToSend.layer.cornerRadius = 6.0f;
    
    [self.butSendComment addTarget:self
                            action:@selector(sendComment:)
                  forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sendComment:(id)sender {
    BOOL hasComment = [self.txtCommentToSend.text isEqualToString:@""];
    
    if (!hasComment) {
        __weak typeof(self) wo = self;
        [self.lm requestPostLessonPlanComment:self.lesson_mo comment:self.txtCommentToSend.text doneBlock:^(BOOL status) {
            if (status) {
                [self.lm requestLessonPlanDetailsForLessonPlanWithID:self.lpid doneBlock:^(BOOL status) {
                    if (status) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            wo.txtCommentToSend.text = @"";
                            [wo reloadFetchedResultsController];
                            [wo.tableView reloadData];
                        });
                    }
                }];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [wo.lm showErrorMessageDialog];
                });
            }
        }];
        
        [self.txtCommentToSend endEditing:YES];
    }
}

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

- (IBAction)editCommentAction:(id)sender {
    // Get managed object through accessing action button position on the table view cell
    mo_selected = [self managedObjectFromButtonAction:sender];
    
    __weak typeof(self) wo = self;
    [wo performSegueWithIdentifier:@"showEditCommentView" sender:nil];
}

- (IBAction)deleteCommentAction:(id)sender {
    // Get managed object through accessing action button position on the table view cell
    mo_selected = [self managedObjectFromButtonAction:sender];
    
    NSString *message = NSLocalizedString(@"Are you sure you want to delete this comment?", nil);
    NSString *yesTitle = NSLocalizedString(@"Yes", nil);
    NSString *noTitle = NSLocalizedString(@"No", nil);
    NSString *avTitle = NSLocalizedString(@"Delete Comment", nil);
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:avTitle
                                                 message:message
                                                delegate:self
                                       cancelButtonTitle:noTitle
                                       otherButtonTitles:yesTitle, nil];
    [av show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
         __weak typeof(self) wo = self;
        
        [self.lm requestDeleteLessonPlanComment:mo_selected doneBlock:^(BOOL status) {
            if (status) {
                NSLog(@"Comment successfully deleted");
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [wo.lm showErrorMessageDialog];
                });
            }
        }];
    }
}

// Access managed object based on the position of action button from the table view cell
- (NSManagedObject *)managedObjectFromButtonAction:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showEditCommentView"]) {
        CommentModalViewController *commentModalView = (CommentModalViewController *)[segue destinationViewController];
        commentModalView.lesson_mo = self.lesson_mo;
        commentModalView.comment_mo = mo_selected;
        commentModalView.delegate = self;
    }
}

- (NSString *)dateFromString:(NSString *)string parse:(NSString *)pattern display:(NSString *)format {
    string = [NSString stringWithFormat:@"%@ GMT", string];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:pattern];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSDate *date = [formatter dateFromString:string];
    [formatter setDateFormat:format];

    NSString *date_string = [formatter stringFromDate:date];
    return date_string;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LessonDetailsCommentTableViewCell *commentContentCell = [tableView dequeueReusableCellWithIdentifier:kCommentCellIdentifier forIndexPath:indexPath];
    
    // Access data from selected managed object
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];

    NSString *uid = [NSString stringWithFormat:@"%@", [mo valueForKey:@"user_id"]];
    NSString *firstname = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"]];
    NSString *lastname = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"]];
    NSString *dateCreated = [NSString stringWithFormat:@"%@", [mo valueForKey:@"date_created"]];
    NSString *dateModified = [NSString stringWithFormat:@"%@", [mo valueForKey:@"date_modified"]];
    NSString *comment = [NSString stringWithFormat:@"%@", [mo valueForKey:@"comment"]];
    NSString *name = [NSString stringWithFormat:@"%@ %@", firstname, lastname];
    
    // Format fields with date and time
    NSString *rfcFormat = @"yyyy-MM-dd HH:mm:ss zzz";
    NSString *formattedDateModified = [self dateFromString:dateModified parse:rfcFormat display:@"MMM. dd, yyyy, hh:mm a"];
    
    // Identify if comment is edited or not
    NSString *strDisplay = @"";
    
    if (![dateCreated isEqualToString:dateModified]) {
        strDisplay =  NSLocalizedString(@"Edited", nil);
    }
    
    // Render image from core data to comment table view
    dispatch_queue_t queue = dispatch_queue_create("com.vibetech.lessonplan.IMAGE", DISPATCH_QUEUE_CONCURRENT);
    dispatch_barrier_async(queue, ^{
        NSString *imagePath = [NSString stringWithFormat:@"%@", [mo valueForKey:@"avatar"]];
        NSURL *url = [NSURL URLWithString:imagePath];
        NSURLSession *session =[NSURLSession sharedSession];
        NSURLSessionDownloadTask *task = [session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            NSData *imageData = [NSData dataWithContentsOfURL:location];
            UIImage *avatar = [UIImage imageWithData:imageData];
            dispatch_async(dispatch_get_main_queue(), ^{
                commentContentCell.imgAvatar.image = avatar;
                commentContentCell.imgAvatar.layer.cornerRadius = 37.5f;
            });
        }];
        
        [task resume];
    });

    commentContentCell.lblName.text = name;
    commentContentCell.lblDateTime.text = [NSString stringWithFormat:@"%@ %@", formattedDateModified, strDisplay];
    commentContentCell.lblComment.text = comment;

    if ([self.userid isEqual:uid]) {
        UIColor *bgColor = [UIColor colorWithRed:201.0f/255.0f green:232.0f/255.0f blue:249.0f/255.0f alpha:1.0f];
        commentContentCell.backgroundColor = bgColor;
    }
    else {
        commentContentCell.backgroundColor = [UIColor whiteColor];
    }
    
    [commentContentCell.butDeleteComment addTarget:self
                                          action:@selector(deleteCommentAction:)
                                forControlEvents:UIControlEventTouchUpInside];
    
    [commentContentCell.butEditComment addTarget:self
                              action:@selector(editCommentAction:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [commentContentCell layoutIfNeeded];
    return commentContentCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    mo_selected = [self.fetchedResultsController objectAtIndexPath:indexPath];
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCommentEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"lp_id == %@ AND lpc_is_deleted == 0", self.lpid];
    fetchRequest.predicate = predicate;
    
    NSSortDescriptor *sort_descriptor = [NSSortDescriptor sortDescriptorWithKey:@"sorted_date_modified" ascending:NO];
    [fetchRequest setSortDescriptors:@[sort_descriptor]];

    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - CommentModalView Delegate

- (void)reloadCommentTable {
    [self reloadFetchedResultsController];
    NSArray *indexes = [self.tableView indexPathsForVisibleRows];
    [self.tableView reloadRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationNone];
}

@end
