//
//  GlobalNotificationDetail.h
//  V-Smart
//
//  Created by Ryan Migallos on 2/26/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlobalNotificationDetail : UIViewController

@property (nonatomic, strong) NSString *group_id;
@property (nonatomic, strong) NSString *message_id;
@property (nonatomic, strong) NSString *user_id;

@end
