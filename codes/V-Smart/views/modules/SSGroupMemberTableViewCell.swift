//
//  SSGroupMemberTableViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 29/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class SSGroupMemberTableViewCell: UITableViewCell {
    
    @IBOutlet var memberImage: UIImageView!
    @IBOutlet var memberName: UILabel!
    @IBOutlet var memberType: UILabel!
    @IBOutlet var removeButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
