//
//  SSMConstants.swift
//  V-Smart
//
//  Created by Ryan Migallos on 29/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import Foundation

struct SSMConstants {
    
    struct Entity {
        static let SECTION = "SSMSection"
        static let GROUP = "SSMGroup"
        static let MESSAGEFEED = "SSMFeed"
        static let URLDATA = "SSMURLData"
        static let LIKES = "SSMLike"
        static let COMMENTS = "SSMComment"
        static let COMMENT_LIKES = "SSMCommentLike"
        static let BADWORDS = "SSBadwords"
        static let STUDENT = "SSMStudent"
        static let MEMBER = "SSMMember"
        static let MAPPING = "SSMMapping"
    }
    
    struct SocketIOListenerKey {
        static let GROUP_CREATE = "group_create"
        static let GROUP_UPDATE = "group_update"
        static let GROUP_REMOVE = "group_remove"
        static let GROUP_MEMBER_ADD = "group_member_add"
        static let GROUP_MEMBER_REMOVE = "group_member_remove"
        static let SOCKET_ERROR = "error"
    }
    
    struct SocketIOEmitterKey {
        static let GROUP_CREATE = "group_create"
        static let GROUP_UPDATE = "group_update"
        static let GROUP_REMOVE = "group_remove"
        static let GROUP_MEMBER_ADD = "group_member_add"
        static let GROUP_MEMBER_REMOVE = "group_member_remove"
    }
}

enum SSMGroupActionType: Int {
    case create = 1
    case update
    case delete
}
