//
//  SocialStreamIconPopViewController.h
//  V-Smart
//
//  Created by Ryan Migallos on 2/3/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SocialStreamIconPopDelegate <NSObject>
@optional
- (void)didFinishSelectingData:(NSDictionary *)data type:(NSString *)type;
@end

@interface SocialStreamIconPopViewController : UICollectionViewController
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, weak) id <SocialStreamIconPopDelegate> delegate;
@end
