//
//  GBMTestPaperView.swift
//  V-Smart
//
//  Created by Ryan Migallos on 23/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol TestPreviewProtocol : NetworkProtocol {
    func url(endpoint uri:String) -> URL
}

extension TestPreviewProtocol {
    func url(endpoint uri:String) -> URL {
        return buildURLFromRequestEndPoint(uri as NSString)
    }
}


class GBMTestPaperView: UIViewController, UIWebViewDelegate, TestPreviewProtocol {
    
    @IBOutlet var webView: UIWebView!
    
    // ParseProtocol
    var isProductionMode: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func loadWebContent(_ userid:String, csid:String, quizid:String) {

        let path = "/gradebook/review/\(userid)/\(csid)/\(quizid)?is_mobile=1"
        let request = URLRequest(url: url(endpoint: path))
        DispatchQueue.main.async {
            self.webView.loadRequest(request)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("done loading web page...")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
