//
//  StudentTableViewCell.m
//  V-Smart
//
//  Created by Julius Abarra on 09/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "StudentTableViewCell.h"

@implementation StudentTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
