//
//  CPTestPWValidationView.m
//  V-Smart
//
//  Created by Julius Abarra on 30/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "CPTestPWValidationView.h"

@interface CPTestPWValidationView ()

@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *unlockButton;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;

@end

@implementation CPTestPWValidationView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *passwordLabelString = NSLocalizedString(@"Please enter password", nil);
    self.passwordLabel.text = passwordLabelString;
    
    NSString *unlockButtonString = NSLocalizedString(@"Unlock", nil);
    [self.unlockButton setTitle:unlockButtonString forState:UIControlStateNormal];
    [self.unlockButton setTitle:unlockButtonString forState:UIControlStateSelected];
    [self.unlockButton setTitle:unlockButtonString forState:UIControlStateHighlighted];
    
    self.unlockButton.enabled = NO;
    
    [self.unlockButton addTarget:self
                          action:@selector(unlockButtonAction:)
                forControlEvents:UIControlEventTouchUpInside];
    
    [self.closeButton addTarget:self
                         action:@selector(closeButtonAction:)
               forControlEvents:UIControlEventTouchUpInside];
    
    [self.passwordTextField addTarget:self
                               action:@selector(textFieldDidChange:)
                     forControlEvents:UIControlEventEditingChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (void)unlockButtonAction:(id)sender {
    NSString *password = self.passwordTextField.text;
    BOOL doesMatch = [self doesPasswordMatch:password];
    
    if (doesMatch) {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
            [self.delegate shouldAllowToRunTest:doesMatch];
        }];
    }
    else {
        NSString *butOkay = NSLocalizedString(@"Okay", nil);
        NSString *message = NSLocalizedString(@"The password you entered is incorrect. Please try again.", nil);
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *theAction = [UIAlertAction actionWithTitle:butOkay
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              __weak typeof(self) wo = self;
                                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                                  [alert dismissViewControllerAnimated:YES completion:^{
                                                                      [wo.passwordTextField becomeFirstResponder];
                                                                  }];
                                                              });
                                                              
                                                          }];
        [alert addAction:theAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)closeButtonAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)textFieldDidChange:(id)sender {
    NSString *password = self.passwordTextField.text;
    self.unlockButton.enabled = YES;
    
    if ([password isEqualToString:@""]) {
        self.unlockButton.enabled = NO;
    }
}

#pragma mark - Class Private Methods

- (BOOL)doesPasswordMatch:(NSString *)password {
    NSString *actualPassword = [self stringValue:[self.testObject valueForKey:@"password"]];
    
    if ([actualPassword isEqualToString:password]) {
        return YES;
    }
    
    return NO;
}

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

@end
