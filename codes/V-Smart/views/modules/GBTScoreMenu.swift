//
//  GBTScoreMenu.swift
//  V-Smart
//
//  Created by Ryan Migallos on 23/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTScoreMenu: UITableViewController {

    @IBOutlet var viewTestButton: UIButton!

    internal var quizID:String?
    internal var studentID:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewTestButton.addTarget(self, action: #selector(GBTScoreMenu.buttonAction(_:)), for: .touchUpInside)

        loadEffect()
        
    }
    
    func loadEffect() {
        
        tableView.backgroundColor = UIColor.clear
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        tableView.backgroundView = blurEffectView
        tableView.separatorEffect = UIVibrancyEffect(blurEffect: blurEffect)
        
        
        self.view.backgroundColor = UIColor.clear
    }
    
    func buttonAction(_ button:UIButton) {
        self.dismiss(animated: true) { 
            self.showTestPaper()
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let height = self.tableView.contentSize.height
        self.preferredContentSize = CGSize(width: 250, height: height);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath as NSIndexPath).row == 0 {
            print("score")
        }
        
        if (indexPath as NSIndexPath).row == 1 {
            print("test")
        }
    }

    func showTestPaper() {
        let nc = NotificationCenter.default
        let event = GBTConstants.Notification.DISPLAYTEST
        let quiz_id = "\(self.quizID!)"
        let student_id = "\(self.studentID!)"
        let data:[String:String] = ["quiz_id":quiz_id,"student_id":student_id]
        nc.post(name: Notification.Name(rawValue: event), object: data)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
