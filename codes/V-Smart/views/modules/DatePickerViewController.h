//
//  DatePickerViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 9/30/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatePickerViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

@end
