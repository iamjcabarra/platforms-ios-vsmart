//
//  TestPlayerListCell.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/4/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "TestPlayerListCell.h"
#import <QuartzCore/QuartzCore.h>

@interface TestPlayerListCell()
@property (strong, nonatomic) IBOutlet UIView *viewFrame;
@end


@implementation TestPlayerListCell

- (void)awakeFromNib {
    // Initialization code
    
    CALayer *clayer = self.viewFrame.layer;
    clayer.borderWidth = 1;
    clayer.borderColor = [[UIColor lightGrayColor] CGColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
