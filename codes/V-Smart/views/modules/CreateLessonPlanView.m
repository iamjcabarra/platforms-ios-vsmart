//
//  CreateLessonPlanView.m
//  V-Smart
//
//  Created by Julius Abarra on 08/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "CreateLessonPlanView.h"
#import "LessonTemplateSegmentView.h"
#import "LessonViewController.h"
#import "LessonPlanDataManager.h"
#import "LessonPlanConstants.h"
#import "HUD.h"

@interface CreateLessonPlanView ()

@property (strong, nonatomic) LessonPlanDataManager *lpdm;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) LessonTemplateSegmentView *segmentView;

@property (strong, nonatomic) IBOutlet UIView *HeaderView;
@property (strong, nonatomic) IBOutlet UIView *footerView;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;

@property (strong, nonatomic) IBOutlet UIButton *previousButton;
@property (strong, nonatomic) IBOutlet UIButton *nextButton;
@property (strong, nonatomic) IBOutlet UIButton *finishButton;

@property (strong, nonatomic) IBOutlet UILabel *stageNameLabel;

@property (strong, nonatomic) NSArray *templateContents;
@property (strong, nonatomic) NSMutableArray *contentStages;
@property (strong, nonatomic) NSString *selectedContentStage;

@property (assign, nonatomic) NSInteger selectedSegmentIndex;

@end

static NSString *kProcessCellIdentifier = @"processCellIdentifier";
static NSString *kEmbeddedSegmentView = @"embedLessonStageView";
static NSString *kShowLessonStageOverview = @"showLessonStageOverview";
static NSString *kShowStageContentView = @"showLessonStageContent";

@implementation CreateLessonPlanView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Data Manager and Context
    self.lpdm = [LessonPlanDataManager sharedInstance];
    self.managedObjectContext = self.lpdm.mainContext;
    
    // Set Up Segmented Views
    [self.segmentedControl removeAllSegments];
    [self setUpSegmentedControl];
    self.stageNameLabel.text = @"";
    
    // Action for Buttons
    [self.previousButton addTarget:self
                            action:@selector(previousButtonAction:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    [self.finishButton addTarget:self
                          action:@selector(finishButtonAction:)
                forControlEvents:UIControlEventTouchUpInside];
    
    [self.nextButton addTarget:self
                        action:@selector(nextButtonAction:)
              forControlEvents:UIControlEventTouchUpInside];
    
    // Progress View
    self.progressView.transform = CGAffineTransformScale(self.progressView.transform, 1.0f, 2.0f);
    [self updateProgress:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    UIColor *color = UIColorFromHex(0x5ABE3E);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier
        self.navigationController.navigationBar.tintColor = color;
    }
    else {
        // iOS 7.0 or later
        self.navigationController.navigationBar.barTintColor = color;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    if (self.actionType == LPActionTypeCreate) {
        self.title = NSLocalizedString(@"Add Lesson", nil);
    }
    
    if (self.actionType == LPActionTypeUpdate) {
        self.title = NSLocalizedString(@"Edit Lesson", nil);
    }
    
    // Progress Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProgress:)
                                                 name:@"updateProgress"
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    // Remove Notification Observer
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"updateProgress"
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Segmented Views

- (void)setUpSegmentedControl {
    // Initial Segment (Overview)
    NSString *overview = NSLocalizedString(@"Overview",  nil);
    [self.segmentedControl insertSegmentWithTitle:overview atIndex:0 animated:YES];
    
    // Segment Title
    NSInteger index = 1;
    NSString *stage = NSLocalizedString(@"Stage", nil);
    
    // Get Selected Template Contents
    NSString *lp_id = [self.lpdm stringValue:[self.copiedLessonObject valueForKey:@"lp_id"]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"lesson.lp_id == %@", lp_id];
    self.templateContents = [self.lpdm getObjectsForEntity:kCopyLessonContentEntity
                                             withPredicate:predicate
                                         andSortDescriptor:@"stage_id"];
    
    // Content Stages
    self.contentStages = [NSMutableArray array];
    
    // Set Up Template Content Stages to Segmented Control
    for (NSManagedObject *mo in self.templateContents) {
        NSString *stage_id = [self.lpdm stringValue:[mo valueForKey:@"stage_id"]];
        NSString *stage_name = [self.lpdm stringValue:[mo valueForKey:@"stage_name"]];
        
        // Add Segment to Segmented Control
        NSString *segment_name = [NSString stringWithFormat:@"%@ %@", stage, stage_id];
        [self.segmentedControl insertSegmentWithTitle:segment_name atIndex:index animated:YES];
        index++;
        
        // Add Content Stage to Collection
        NSDictionary *detail = @{@"stage_id":stage_id, @"stage_name":stage_name};
        [self.contentStages addObject:detail];
    }
    
    // Segmented Control Additional Set Ups
    self.segmentedControl.tintColor = UIColorFromHex(0x00AEEF);
    self.segmentedControl.selectedSegmentIndex = 0;
    [self.segmentedControl addTarget:self
                              action:@selector(segmentedControlAction:)
                    forControlEvents:UIControlEventValueChanged];
    
    // Update Button State
    self.selectedSegmentIndex = self.segmentedControl.selectedSegmentIndex;
    [self updateButtonStateForSegmentIndex:self.selectedSegmentIndex];
}

- (void)segmentedControlAction:(id)sender {
    UISegmentedControl *sc = (UISegmentedControl *)sender;
    NSInteger index = sc.selectedSegmentIndex;
    [self changeSegmentViewToSegmentWithIndex:index];
}

- (void)changeSegmentViewToSegmentWithIndex:(NSInteger)index {
    // Lesson Template Overview
    if (index == 0) {
        __weak typeof(self) wo = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.segmentedControl.selectedSegmentIndex = index;
            wo.selectedSegmentIndex = index;
            
            wo.segmentView.selectedContentStage = @"";
            wo.stageNameLabel.text = @"";
            
            [wo.segmentView swapEmbeddedViews:kShowLessonStageOverview];
            [wo updateButtonStateForSegmentIndex:index];
        });
    }
    // Lesson Template Stage View
    else {
        NSDictionary *stageDetail = [self.contentStages objectAtIndex:(index - 1)];
        NSString *selContentStage = [self.lpdm stringValue:stageDetail[@"stage_id"]];
        NSString *stageNameString = [self.lpdm stringValue:stageDetail[@"stage_name"]];
        
        __weak typeof(self) wo = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.segmentedControl.selectedSegmentIndex = index;
            wo.selectedSegmentIndex = index;
            
            wo.segmentView.selectedContentStage = selContentStage;
            wo.stageNameLabel.text = [stageNameString uppercaseString];
            
            [wo.segmentView swapEmbeddedViews:kShowStageContentView];
            [wo updateButtonStateForSegmentIndex:index];
        });
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kEmbeddedSegmentView]) {
        self.segmentView = (LessonTemplateSegmentView *)[segue destinationViewController];
        self.segmentView.copiedLessonObject = self.copiedLessonObject;
    }
}

#pragma mark - Button State Updating

- (void)updateButtonStateForSegmentIndex:(NSInteger)index {
    NSInteger segmentCount = [self.segmentedControl numberOfSegments];
    BOOL isLastSegment = (segmentCount == index + 1) ? YES : NO;
    
    if (isLastSegment) {
        self.previousButton.userInteractionEnabled = YES;
        self.previousButton.hidden = NO;
        
        if (segmentCount ==  1) {
            self.previousButton.userInteractionEnabled = NO;
            self.previousButton.hidden = YES;
        }
        
        self.finishButton.userInteractionEnabled = YES;
        self.finishButton.hidden = NO;
        
        self.nextButton.userInteractionEnabled = YES;
        self.nextButton.hidden = NO;
        
        NSString *previous = NSLocalizedString(@"Previous", nil);
        [self.previousButton setTitle:previous forState:UIControlStateNormal];
        [self.previousButton setTitle:previous forState:UIControlStateSelected];
        [self.previousButton setTitle:previous forState:UIControlStateHighlighted];
        [self.previousButton setBackgroundColor:UIColorFromHex(0x999999)];
        
        NSString *save = NSLocalizedString(@"Save", nil);
        if (self.actionType == LPActionTypeUpdate) {
            save = NSLocalizedString(@"Update", nil);
        }
        
        [self.finishButton setTitle:save forState:UIControlStateNormal];
        [self.finishButton setTitle:save forState:UIControlStateSelected];
        [self.finishButton setTitle:save forState:UIControlStateHighlighted];
        [self.finishButton setBackgroundColor:UIColorFromHex(0x5ABE65)];
        
        if (![self isItOkayToPost]) {
            self.finishButton.userInteractionEnabled = NO;
            [self.finishButton setBackgroundColor:UIColorFromHex(0xCCCCCC)];
        }
        
        NSString *cancel = NSLocalizedString(@"Cancel", nil);
        [self.nextButton setTitle:cancel forState:UIControlStateNormal];
        [self.nextButton setTitle:cancel forState:UIControlStateSelected];
        [self.nextButton setTitle:cancel forState:UIControlStateHighlighted];
        [self.nextButton setBackgroundColor:UIColorFromHex(0x999999)];
    }
    else {
        if (index == 0) {
            self.previousButton.userInteractionEnabled = NO;
            self.previousButton.hidden = YES;
        }
        else {
            self.previousButton.userInteractionEnabled = YES;
            self.previousButton.hidden = NO;
        }
        
        self.finishButton.userInteractionEnabled = NO;
        self.finishButton.hidden = YES;
        
        self.nextButton.userInteractionEnabled = YES;
        self.nextButton.hidden = NO;
        
        NSString *previous = NSLocalizedString(@"Previous", nil);
        [self.previousButton setTitle:previous forState:UIControlStateNormal];
        [self.previousButton setTitle:previous forState:UIControlStateSelected];
        [self.previousButton setTitle:previous forState:UIControlStateHighlighted];
        [self.previousButton setBackgroundColor:UIColorFromHex(0x999999)];
        
        NSString *next = NSLocalizedString(@"Next", nil);
        [self.nextButton setTitle:next forState:UIControlStateNormal];
        [self.nextButton setTitle:next forState:UIControlStateSelected];
        [self.nextButton setTitle:next forState:UIControlStateHighlighted];
        [self.nextButton setBackgroundColor:UIColorFromHex(0x3498DB)];
    }
}

#pragma mark - Actions for Buttons

- (void)previousButtonAction:(id)sender {
    NSInteger previousIndex = self.selectedSegmentIndex - 1;
    
    if (previousIndex >= 0) {
        [self changeSegmentViewToSegmentWithIndex:previousIndex];
    }
}

- (void)nextButtonAction:(id)sender {
    NSInteger nextIndex = self.selectedSegmentIndex + 1;
    
    if (nextIndex < [self.segmentedControl numberOfSegments]) {
        [self changeSegmentViewToSegmentWithIndex:nextIndex];
    }
    else {
        [self showCancelAlertView];
    }
}

- (void)finishButtonAction:(id)sender {
    NSString *actionString = (self.actionType == LPActionTypeCreate) ? NSLocalizedString(@"Saving", nil) : NSLocalizedString(@"Updating", nil); ;
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", actionString];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    if ([self isItOkayToPost]) {
        NSDictionary *postBody = [self.lpdm prepareLessonPlanDataForPostOperation:self.copiedLessonObject action:self.actionType];
        
        if (self.actionType == LPActionTypeCreate) {
            [self.lpdm requestCreateNewLessonPlan:postBody file:nil doneBlock:^(BOOL status) {
                __weak typeof(self) wo = self;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUD hideUIBlockingIndicator];
                    [wo showConfirmationMessage:status];
                });
            }];
        }
        
        if (self.actionType == LPActionTypeUpdate) {
            NSString *lp_id = [self.copiedLessonObject valueForKey:@"lp_id"];
            NSDictionary *postBody = [self.lpdm prepareLessonPlanDataForPostOperation:self.copiedLessonObject action:self.actionType];
            
            [self.lpdm requestUpdateLessonPlanWithLessonPlanID:lp_id meta:postBody doneBlock:^(BOOL status) {
                __weak typeof(self) wo = self;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [HUD hideUIBlockingIndicator];
                    [wo showConfirmationMessage:status];
                });
            }];
        }
    }
    else {
        __weak typeof(self) wo = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD hideUIBlockingIndicator];
            [wo showConfirmationMessage:NO];
        });
    }
}

#pragma mark - Alert Controllers

- (void)showCancelAlertView {
    NSString *premstr = NSLocalizedString(@"Are you sure you want to cancel", nil);
    NSString *midmstr = NSLocalizedString(@"creating", nil);
    NSString *sufmstr = NSLocalizedString(@"this lesson plan?", nil);
    
    if (self.actionType == LPActionTypeUpdate) {
        midmstr = NSLocalizedString(@"updating", nil);
    }
    
    NSString *message =[NSString stringWithFormat:@"%@ %@ %@", premstr, midmstr, sufmstr];
    NSString *butNegR = NSLocalizedString(@"No", nil);
    NSString *butPosR = NSLocalizedString(@"Yes", nil);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *negAction = [UIAlertAction actionWithTitle:butNegR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                          
                                                      }];
    
    UIAlertAction *posAction = [UIAlertAction actionWithTitle:butPosR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          NSArray *viewControllers = self.navigationController.viewControllers;
                                                          
                                                          for (UIViewController *anVC in viewControllers) {
                                                              if ([anVC isKindOfClass:[LessonViewController class]]) {
                                                                  __weak typeof(self) wo = self;
                                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                                      [wo.navigationController popToViewController:anVC animated:YES];
                                                                  });
                                                              }
                                                          }
                                                          
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                    
                                                      }];
    
    [alert addAction:negAction];
    [alert addAction:posAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showConfirmationMessage:(BOOL)success {
    NSString *message = @"";
    NSString *butPosR = NSLocalizedString(@"Okay", nil);
    
    if (success) {
        if (self.actionType == LPActionTypeCreate) {
            message = NSLocalizedString(@"Lesson plan was successfully created.", nil);
        }
        else {
            message = NSLocalizedString(@"Lesson plan was successfully updated.", nil);
        }
    }
    else {
        if (self.actionType == LPActionTypeCreate) {
            message = NSLocalizedString(@"Lesson plan was not successfully created.", nil);
        }
        else {
            message = NSLocalizedString(@"Lesson plan was not successfully updated.", nil);
        }
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *posAction = [UIAlertAction actionWithTitle:butPosR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          if (success) {
                                                              NSArray *viewControllers = self.navigationController.viewControllers;
                                                              
                                                              for (UIViewController *anVC in viewControllers) {
                                                                  if ([anVC isKindOfClass:[LessonViewController class]]) {
                                                                      __weak typeof(self) wo = self;
                                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                                          [wo.navigationController popToViewController:anVC animated:YES];
                                                                      });
                                                                  }
                                                              }
                                                          }
                                                          
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                          
                                                      }];
    
    [alert addAction:posAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Post Action Validation

- (BOOL)isItOkayToPost {
    // Check if there's an empty data for each required field
    if (self.copiedLessonObject != nil) {
        NSArray *keys = self.copiedLessonObject.entity.propertiesByName.allKeys;
        
        for (NSString *k in keys) {
            NSString *value = [self.copiedLessonObject valueForKey:k];

            if ([k isEqualToString:@"name"]) {
                if ([value isEqualToString:@""]) {
                    return NO;
                }
            }
            
            if ([k isEqualToString:@"school_name"]) {
                if ([value isEqualToString:@""]) {
                    return NO;
                }
            }
            
            if ([k isEqualToString:@"school_address"]) {
                if ([value isEqualToString:@""]) {
                    return NO;
                }
            }
            
            if ([k isEqualToString:@"quarter"]) {
                if ([value isEqualToString:@""]) {
                    return NO;
                }
            }
            
            if ([k isEqualToString:@"level"]) {
                if ([value isEqualToString:@""]) {
                    return NO;
                }
            }
            
            if ([k isEqualToString:@"unit"]) {
                if ([value isEqualToString:@""]) {
                    return NO;
                }
            }
            
            if ([k isEqualToString:@"start_date"]) {
                if ([value isEqualToString:@""]) {
                    return NO;
                }
            }
            
            if ([k isEqualToString:@"end_date"]) {
                if ([value isEqualToString:@""]) {
                    return NO;
                }
            }
        }
    }
    
    return YES;
}

#pragma mark - Progress Updating

- (void)updateProgress:(NSNotification *)notification {
    NSLog(@"PROGRESS UPDATING");
    
    CGFloat progress = 0.0f;
    
    NSInteger overviewContentCount = 0;
    NSInteger stageContentCount = 0;
    NSInteger noDataCount = 0;
    
    if (self.copiedLessonObject != nil) {
        NSArray *overviewKeys = self.copiedLessonObject.entity.propertiesByName.allKeys;
        overviewContentCount = overviewKeys.count;
        
        for (NSString *k in overviewKeys) {
            NSString *value = [self.lpdm stringValue:[self.copiedLessonObject valueForKey:k]];
            
            if ([value isEqualToString:@""]) {
                noDataCount++;
            }
        }
        
        NSSet *contentSet = (NSSet *)[self.copiedLessonObject valueForKey:@"contents"];
        NSArray *contentSetObjects = [contentSet allObjects];
        
        for (NSManagedObject *cmo in contentSetObjects) {
            NSSet *processSet = (NSSet *)[cmo valueForKey:@"processes"];
            NSArray *processSetObjects = [processSet allObjects];
            
            for (NSManagedObject *pmo in processSetObjects) {
                NSArray *pmoKeys = pmo.entity.propertiesByName.allKeys;
                
                for (NSString *k in pmoKeys) {
                    if ([k isEqualToString:@"content"]) {
                        stageContentCount++;
                        
                        NSString *value = [self.lpdm stringValue:[pmo valueForKey:k]];
                        
                        if ([value isEqualToString:@""]) {
                            noDataCount++;
                        }
                    }
                }
            }
        }
        
        NSInteger totalContentCount = overviewContentCount + stageContentCount;
        
        if (totalContentCount > 0) {
            progress = 1.0f - noDataCount * (1.0f / totalContentCount);
        }
    }
    
    NSLog(@"progress: %f", progress);
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        wo.progressView.progress = progress;
    });
}

@end
