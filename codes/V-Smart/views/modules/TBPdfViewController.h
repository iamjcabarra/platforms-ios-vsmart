//
//  TBPdfViewController.h
//  V-Smart
//
//  Created by Ryan Migallos on 13/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBPdfViewController : UICollectionViewController

@property (strong, nonatomic) NSString *fileName;
@property (strong, nonatomic) NSString *passPhrase;
@property (strong, nonatomic) NSString *bookTitle;

//- (void)pdfFile:(NSString *)file password:(NSString *)key title:(NSString *)string;

@end
