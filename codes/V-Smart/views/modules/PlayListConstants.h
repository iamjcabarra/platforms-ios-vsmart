//
//  PlayListConstants.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 26/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kServerDateFormat                           @"yyyy-MM-dd HH:mm:ss"

#define kTGTBUIDateFormatVeryLongStyle              @"EEE, MMMM dd, yyyy hh:mm a"
#define kTGTBUIDateFormatLongStyle                  @"EEE. MMM. dd, yyyy hh:mm a"
#define kTGTBUIDateFormatMediumStyle                @"MMMM dd, yyyy hh:mm a"

///////////////////////////////
//      USER DEFAULTS KEY    //
///////////////////////////////
#define kTG_PAGINATION_LIMIT                        @"TG_PAGINATION_LIMIT"
#define kTGQB_SELECTED_PACKAGE_ID                   @"TGQB_SELECTED_PACKAGE_ID"
#define kTGQB_SELECTED_PACKAGE_NAME                 @"TGQB_SELECTED_PACKAGE_NAME"
#define kTGQB_SELECTED_PACKAGE_IMAGE_DATA           @"TGQB_SELECTED_PACKAGE_IMAGE_DATA"
#define kTGQB_SELECTED_PACKAGE_IMAGE_URL            @"TGQB_SELECTED_PACKAGE_IMAGE_URL"

#define kTGQB_SELECTED_COURSE_ID                    @"TGQB_SELECTED_COURSE_ID"
#define kTGQB_SELECTED_COURSE_NAME                  @"TGQB_SELECTED_COURSE_NAME"

#define kCP_SELECTED_CS_ID                          @"kCP_SELECTED_CS_ID"
#define kCP_SELECTED_QUIZ_ID                        @"kCP_SELECTED_QUIZ_ID"
#define kCP_SELECTED_FONT_SIZE                      @"kCP_SELECTED_FONT_SIZE"

#define kPL_FILESIZE_LIMIT_BYTES                          @"PL_FILESIZE_LIMIT_BYTES"

///////////////////////////
//      NOTIFICATION     //
///////////////////////////
#define kNotificationQuestionUpdate                 @"NOTIFICATION_QUESTION_UPDATE"
#define kNotificationQuestionDelete                 @"NOTIFICATION_QUESTION_DELETE"
#define kNotificationQuestionEditFromPreview        @"NOTIFICATION_QUESTION_EDIT_FROM_PREVIEW"
#define kNotificationTestCreateVerification         @"NOTIFICATION_TEST_CREATE_VERIFICATION"
#define kNotificationCoursePlayerAnswered           @"NOTIFICATION_TEST_CREATE_VERIFICATION"
#define kNotificationCoursePlayerFontSizeChanged    @"NOTIFICATION_FONT_SIZE_CHANGED"
#define kNotificationCoursePlayerConnectivity       @"VSMART_CONNECTIVITY_NOTIFICATION"


//////////////////////////////////
//      PAGINATION SETTINGS     //
//////////////////////////////////
#define kEndPointTGPagination                       @"/vsmart-rest-dev/v1/settings/get/setting_name/pagination"


/////////////////////////////////////////
// TEST GURU QUESTION BANK COURSE LIST //
/////////////////////////////////////////
#pragma mark - CURRICULUM
#define kEndPointTGQBCourseList                     @"/vsmart-rest-dev/v2/curriculum/list/curriculum/course/%@"
#define kEndPointTGQBCurriculumOverview             @"/vsmart-rest-dev/v2/curriculum/%@/overview"
#define kEndPointTGQBLearningCompetecies            @"/vsmart-rest-dev/v2/curriculum/list/learningcompetencies/period/%@"

/////////////////////////
// TEST GURU TEST BANK //
/////////////////////////
#define kEndPointTGTBPaginatedTestList              @"/vsmart-rest-dev/v2/testbank/list/course/%@/user/%@?limit=%@&current_page=%@&search_keyword=%@"
#define kEndPointTGTBPaginatedTestListWithFilter    @"/vsmart-rest-dev/v2/testbank/list/course/%@/user/%@?limit=%@&current_page=%@&search_keyword=%@&is_graded=%@"
#define kEndPointTGTBPaginatedTestGradingTypeList   @"/vsmart-rest-dev/v2/testbank/list/type/user/%@?limit=%@&current_page=%@&search_keyword=%@"
#define kEndPointTGTBDeleteTest                     @"/vsmart-rest-dev/v2/testbank/delete/%@"
#define kEndPointTGTBTestDetails                    @"/vsmart-rest-dev/v2/testbank/%@"
//#define kEndPointTGTBCourseSectionList              @"/vsmart-rest-dev/v2/courses/section/search/user/%@"
#define kEndPointTGTBCourseSectionList              @"/vsmart-rest-dev/v2/testbank/user/%@/test/%@/course/%@/sections" // lex debug
#define kEndPointTGTBDeployTest                     @"/vsmart-rest-dev/v2/testbank/deploy/%@"
#define kEndPointTGTBDeleteTest                     @"/vsmart-rest-dev/v2/testbank/delete/%@"

#define kEndPointTGTBCreateTest                     @"/vsmart-rest-dev/v2/testbank/new/user/%@"
#define kEndPointTGTBUpdateTest                     @"/vsmart-rest-dev/v2/testbank/user/%@/update/%@"

#define kEndPointTGTBGroupBy                        @"/vsmart-rest-dev/v2/testbank/list/type/user/%@?limit=%@&current_page=%@"
#define kEndPointTestGuruFilterAssignQuestionV2     @"/vsmart-rest-dev/v2/testbank/list/questions/available/course/%@/user/%@?limit=%@&current_page=%@&search_keyword=%@"
#define kEndPointTestGuruFilterAssignQuestionV3     @"/vsmart-rest-dev/v2/testbank/list/questions/available/course/%@/package/%@/user/%@?limit=%@&current_page=%@&search_keyword=%@"
#define kEndPointTestGuruFilterAssignQuestionV4     @"/vsmart-rest-dev/v2/testbank/list/questions/available/course/%@/package/%@/user/%@?limit=%@&current_page=%@&search_keyword=%@&sort=%@"

#define kEndPointTGTBPaginatedTestListComplete      @"/vsmart-rest-dev/v2/testbank/list/course/%@/package/%@/user/%@?limit=%@&current_page=%@&search_keyword=%@&sort=%@&is_graded=%@"
#define kEndPointTGTBPaginatedTestListNoFilter      @"/vsmart-rest-dev/v2/testbank/list/course/%@/package/%@/user/%@?limit=%@&current_page=%@&search_keyword=%@&sort=%@"

#define kEndPointTGTBSaveToPDF                      @"/vsmart-rest-dev/v2/testbank/user/%@/course/%@/save/pdf/%@"

/////////////////////////
//    COURSE PLAYER    //
/////////////////////////
#define kEndPointCPCourseListForUser                @"/vsmart-rest-dev/v2/courses/search/user/%@"
#define kEndPointCPPaginatedCourseList              @"/vsmart-rest-dev/v2/courses/list/user/%@?limit=%@&current_page=%@&search_keyword=%@"
#define kEndPointCPPaginatedCourseListV2            @"/vsmart-rest-dev/v2/courses/list/user/%@?limit=%@&current_page=%@&search_keyword=%@&sort=%@"

///courses/list/user/2?limit=20&current_page=1&search_keyword=
#define kEndPointCoursePlayerRunOrPrerun            @"/vsmart-rest-dev/v2/quizzes/%@/%@/search/user/%@/course_section/%@"
#define kEndPointCPPaginatedCourseTestList          @"/vsmart-rest-dev/v2/courses/test/list/course/%@/user/%@?limit=%@&current_page=%@&search_keyword=%@"
#define kEndPointCoursePlayerSubmitAnswers          @"/vsmart-rest-dev/v2/quizzes/%@/answers/add"
#define kEndPointCoursePlayerShowResults            @"/vsmart-rest-dev/v2/quizzes/%@/test/review/user/%@/course_section/%@"

//#endif /* TestGuruConstants_h */






/////////////////////////
//   PLAYLIST ENTITY   //
/////////////////////////

//#define kPlayListEntity                                 @"PlayList"
//#define kPlayListFileEntity                             @"PlayListFile"

#define kEndPointPlayListV2                             @"/vsmart-rest-dev/v1/playlist/list/%@"
#define kEndPointDownLoadPlayListItemV2                 @"/vsmart-rest-dev/v1/playlist/download/%@"
#define kEndPointPlayListDeleteV2                       @"/vsmart-rest-dev/v1/playlist/%@/delete"
#define kEndPointPlayListTeacherListV2                  @"/vsmart-rest-dev/v1/playlist/teacher_list/user_id/%@"

#define kEndPointPlayListUploadV2                       @"/vsmart-rest-dev/v1/playlist/upload/%@"

#define kEndPointPlayListTeacherListV2                  @"/vsmart-rest-dev/v1/playlist/teacher_list/user_id/%@"
#define kEndPointPlayListSearchUserV2                   @"/vsmart-rest-dev/v2/users/search/name/%@/user_id/%@"

#define kEndPointPlayListUpdateMetaV2                   @"/vsmart-rest-dev/v1/playlist/update_meta/%@"

#define kEndPointPlayListMaxUpload                      @"/vsmart-rest-dev/v1/settings/get/max_upload"




