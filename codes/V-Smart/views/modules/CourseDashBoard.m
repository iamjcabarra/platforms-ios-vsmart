//
//  CourseDashBoard.m
//  V-Smart
//
//  Created by Ryan Migallos on 10/2/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CourseDashBoard.h"
#import "CourseDashBoardCell.h"
#import "AppDelegate.h"
#import "Course.h"
#import "Topic.h"
#import "AccountInfo.h"

@interface CourseDashBoard () <UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) ResourceManager *rm;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSBlockOperation *blockOperation;

@property (nonatomic, assign) BOOL shouldReloadCollectionView;
@property (nonatomic, strong) AccountInfo *useraccount;

@end

@implementation CourseDashBoard

static NSString * const reuseIdentifier = @"cell_identifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Title
    self.title = @"Courses";
    
    // Initialize Resource Manager
    self.rm = [AppDelegate resourceInstance];
    
    // Initialize Fetch Results Controller
    [self configureFetch];
    
    [self networkRequest];
    
    // Uncomment the following line to preserve selection between presentations
     self.clearsSelectionOnViewWillAppear = NO;
    
    self.useraccount = [[VSmart sharedInstance] account];
}

- (void)networkRequest {
    
//    dispatch_queue_t queue = dispatch_queue_create("com.pearson.course",DISPATCH_QUEUE_SERIAL);
//    dispatch_async(queue, ^{
//        [self.rm requestCourseList:^(BOOL status) {
//            if (status) {
//            }
//        }];
//    });
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.

//    NSIndexPath *indexPath = (NSIndexPath *)[[self.collectionView indexPathsForSelectedItems] lastObject];
//    Course *mo = (Course *)[self.fetchedResultsController objectAtIndexPath:indexPath];
//    CourseTopics *course = [segue destinationViewController];
//    [course setCourseObject:mo];
//    
//    dispatch_queue_t queue = dispatch_queue_create("com.pearson.course.ATTEMPT",DISPATCH_QUEUE_SERIAL);
//    dispatch_async(queue, ^{
//        [self.rm readAnalytics:mo doneBlock:^(BOOL status) {
//            NSString *details = [NSString stringWithFormat:@"Opens %@ in Apple iPad", mo.course_name];
//            [self.rm requestLogActivityWithModuleType:@"6" details:details];
//        }];
//    });
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
//    return 1;
    return [[self.fetchedResultsController sections] count];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    return 50;
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CourseDashBoardCell *cell = (CourseDashBoardCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    Course *course = [self.fetchedResultsController objectAtIndexPath:indexPath];

    cell.textField.text = [NSString stringWithFormat:@"%@",course.course_name];
//    cell.textView.text = [NSString stringWithFormat:@"%@",course.course_de];
//    
//    cell.layer.cornerRadius = 5;
//    cell.layer.masksToBounds = YES;
//    cell.layer.shadowColor = [UIColor lightGrayColor].CGColor;
//    cell.layer.shadowOpacity = 0.8;
//    cell.layer.shadowRadius = 5.0;
//    cell.layer.shadowOffset = CGSizeMake(1.0, 1.0);
//    
//    [self updateImageView:cell.imageView withURL:course.course_cover];
    
    return cell;
}

#pragma mark - <UICollectionViewDelegateFlowLayout>

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(20, 60, 20, 60);
}

#pragma mark - <NSFetchedResultsControllerDelegate>

- (NSFetchedResultsController *)configureFetch {
    
    if (self.fetchedResultsController != nil) {
        return self.fetchedResultsController;
    }

    NSManagedObjectContext *context = self.rm.mainContext;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kCourseEntity];
    request.entity = [NSEntityDescription entityForName:kCourseEntity inManagedObjectContext:context];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"course_name" ascending:YES]];
    request.fetchBatchSize = 10;

    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context
                                                                          sectionNameKeyPath:nil cacheName:nil];
    self.fetchedResultsController.delegate = self;

    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate.
        // You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return self.fetchedResultsController;
}

/* Notifies the delegate that section and object changes are about to be processed and notifications will be sent.  Enables NSFetchedResultsController change tracking.
 Clients utilizing a UITableView may prepare for a batch of updates by responding to this method with -beginUpdates
 */
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    self.shouldReloadCollectionView = NO;
    self.blockOperation = [[NSBlockOperation alloc] init];
}

/* Notifies the delegate of added or removed sections.  Enables NSFetchedResultsController change tracking.
 
	controller - controller instance that noticed the change on its sections
	sectionInfo - changed section
	index - index of changed section
	type - indicates if the change was an insert or delete
 
	Changes on section info are reported before changes on fetchedObjects.
 */
- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    __weak UICollectionView *collectionView = self.collectionView;
    switch (type) {
        case NSFetchedResultsChangeInsert: {
            [self.blockOperation addExecutionBlock:^{
                [collectionView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]];
            }];
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [self.blockOperation addExecutionBlock:^{
                [collectionView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]];
            }];
            break;
        }
        case NSFetchedResultsChangeUpdate: {
            [self.blockOperation addExecutionBlock:^{
                [collectionView reloadSections:[NSIndexSet indexSetWithIndex:sectionIndex]];
            }];
            break;
        }
        default:
            break;
    }
}


/* Notifies the delegate that a fetched object has been changed due to an add, remove, move, or update. Enables NSFetchedResultsController change tracking.
	controller - controller instance that noticed the change on its fetched objects
	anObject - changed object
	indexPath - indexPath of changed object (nil for inserts)
	type - indicates if the change was an insert, delete, move, or update
	newIndexPath - the destination path for inserted or moved objects, nil otherwise
	
	Changes are reported with the following heuristics:
 
	On Adds and Removes, only the Added/Removed object is reported. It's assumed that all objects that come after the affected object are also moved, but these moves are not reported.
	The Move object is reported when the changed attribute on the object is one of the sort descriptors used in the fetch request.  An update of the object is assumed in this case, but no separate update message is sent to the delegate.
	The Update object is reported when an object's state changes, and the changed attributes aren't part of the sort keys.
 */
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    __weak UICollectionView *collectionView = self.collectionView;
    switch (type) {
        case NSFetchedResultsChangeInsert: {
            if ([self.collectionView numberOfSections] > 0) {
                if ([self.collectionView numberOfItemsInSection:indexPath.section] == 0) {
                    self.shouldReloadCollectionView = YES;
                } else {
                    [self.blockOperation addExecutionBlock:^{
                        [collectionView insertItemsAtIndexPaths:@[newIndexPath]];
                    }];
                }
            } else {
                self.shouldReloadCollectionView = YES;
            }
            break;
        }
        case NSFetchedResultsChangeDelete: {
            if ([self.collectionView numberOfItemsInSection:indexPath.section] == 1) {
                self.shouldReloadCollectionView = YES;
            } else {
                [self.blockOperation addExecutionBlock:^{
                    [collectionView deleteItemsAtIndexPaths:@[indexPath]];
                }];
            }
            break;
        }
        case NSFetchedResultsChangeUpdate: {
            [self.blockOperation addExecutionBlock:^{
                [collectionView reloadItemsAtIndexPaths:@[indexPath]];
            }];
            break;
        }
        case NSFetchedResultsChangeMove: {
            [self.blockOperation addExecutionBlock:^{
                [collectionView moveItemAtIndexPath:indexPath toIndexPath:newIndexPath];
            }];
            break;
        }
        default:
            break;
    }
}

/* Notifies the delegate that all section and object changes have been sent. Enables NSFetchedResultsController change tracking.
 Providing an empty implementation will enable change tracking if you do not care about the individual callbacks.
 */
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // Checks if we should reload the collection view to fix a bug @ http://openradar.appspot.com/12954582
    if (self.shouldReloadCollectionView) {
        [self.collectionView reloadData];
    } else {
        [self.collectionView performBatchUpdates:^{
            [self.blockOperation start];
        } completion:nil];
    }
}

- (void)updateImageView:(UIImageView *)imageView withURL:(NSString *)imageurl
{
    NSURLSession *session = [NSURLSession sharedSession];
    //1
    NSURL *url = [NSURL URLWithString:imageurl];
    // 2
    NSURLSessionDownloadTask *task = [session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        // 3
        NSData *imageData = [NSData dataWithContentsOfURL:location];
        UIImage *image = [UIImage imageWithData:imageData];
        dispatch_async(dispatch_get_main_queue(), ^{
            imageView.image = image;
        });
    }];
    // 4
    [task resume];
}

@end
