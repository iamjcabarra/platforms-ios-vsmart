//
//  SSGroupPanelViewController.swift
//  V-Smart
//
//  Created by Ryan Migallos on 28/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class SSGroupPanelViewController: UIViewController, NSFetchedResultsControllerDelegate {
    
    @IBOutlet var table: UITableView!
    @IBOutlet var groupHeaderLabel: UILabel!
    @IBOutlet var addGroupView: UIView!
    @IBOutlet var addButton: UIButton!
    
    fileprivate let cellIdentifier = "group_panel_cell_identifier"
    fileprivate let notification = NotificationCenter.default
    
    fileprivate var isAscending = true
    fileprivate var position = "teacher"
    
    @IBAction func addGroup(_ button: UIButton){
        DispatchQueue.main.async(execute: {
            self.ssdm.save(object: "1" as AnyObject!, forKey: "SS_GROUP_IS_EDIT_MODE")
            self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_CREATE_GROUP"), object: nil)
        })
    }
    
    fileprivate lazy var ssdm: SocialStreamDataManager = {
        let dataManager = SocialStreamDataManager.sharedInstance
        return dataManager
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        groupHeaderLabel.text = NSLocalizedString("Groups", comment: "").uppercased()
        
        let addGroupText = "     \(NSLocalizedString("Add Group", comment: ""))"
        addButton.setTitle(addGroupText, for: UIControlState())
        addButton.setTitle(addGroupText, for: .highlighted)
        addButton.setTitle(addGroupText, for: .selected)
        
        position = self.ssdm.accountUserPosition().lowercased()
        let hideAddButton = position == "teacher" ? false : true
        addGroupView.isHidden = hideAddButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let reloadFetchedResultsAction = #selector(reloadFetchedResultsAction(_:))
        notification.addObserver(self, selector: reloadFetchedResultsAction, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_RELOAD_DATA"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "SS_NOTIFICATION_RELOAD_DATA"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func reloadFetchedResultsAction(_ notification: Notification) {
        DispatchQueue.main.async(execute: {
            self.reloadFetchedResultsController()
        })
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SSGroupPanelTableViewCell
        cell.selectionStyle = .none
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAtIndexPath indexPath: IndexPath) -> Bool {
        let mo = fetchedResultsController.object(at: indexPath)
        guard let group_id = mo.value(forKey: "id") as? String else { return false }
        let isEditMode = ssdm.stringValue(ssdm.fetchUserDefaultsObject(forKey: "SS_GROUP_IS_EDIT_MODE"))
        return group_id != "0" && position != "student" && isEditMode != "1" ? true : false
    }
    
    func tableView(_ tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        
        guard
            let group_id = mo.value(forKey: "id") as? String,
            let name = mo.value(forKey: "name") as? String
            else { return }
        
        if editingStyle == .delete {
            let question = "\(NSLocalizedString("Are you sure you want to remove", comment: "")) \"\(name)\"?"
            self.showAlertViewWithPolarQuestion(question, completion: { (allow) in
                if allow {
                    let body: [String: AnyObject] = ["name": name as AnyObject, "user_id": self.ssdm.accountUserID() as AnyObject, "group_id": group_id as AnyObject]
                    self.ssdm.requestRemoveGroup(group_id, body: body, handler: { (doneBlock) in
                        if !doneBlock {
                            DispatchQueue.main.async(execute: {
                                let message = "\(NSLocalizedString("There was an error removing", comment: "")) \"\(name)\". \(NSLocalizedString("Please try again.", comment: ""))"
                                self.showAlertViewMessage(message)
                            })
                        }
                    })
                }
            })
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        let isEditMode = ssdm.stringValue(ssdm.fetchUserDefaultsObject(forKey: "SS_GROUP_IS_EDIT_MODE"))
        let previousGroupID = ssdm.stringValue(ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_GROUP_ID"))
        
        if isEditMode == "1" {
            if position == "teacher" && previousGroupID != "0" {
                let question = NSLocalizedString("Are you sure you want to cancel creating or updating a group?", comment: "")
                self.showAlertViewWithPolarQuestion(question, completion: { (allow) in
                    if allow {
                        DispatchQueue.main.async(execute: {
                            self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_CANCEL_EDITING"), object: nil)
                            self.changeMessageFeedForGroupObject(mo)
                        })
                    }
                })
            }
            else {
                DispatchQueue.main.async(execute: {
                    self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_CANCEL_EDITING"), object: nil)
                    self.changeMessageFeedForGroupObject(mo)
                })
            }
        }
        else {
            self.changeMessageFeedForGroupObject(mo)
        }
    }
    
    fileprivate func changeMessageFeedForGroupObject(_ object: NSManagedObject) {
        guard
            let group_id = object.value(forKey: "id") as? String,
            let section_id = object.value(forKey: "section_id") as? String,
            let name = object.value(forKey: "name") as? String
            else { return }
        
        // Update Group Selection
        self.ssdm.updateSelectionForGroup(group_id)
        
        // Update Message Feed
        self.ssdm.requestUserMessages(forGroup: group_id, inSection: section_id) { (doneBlock) in
            DispatchQueue.main.async(execute: {
                self.ssdm.save(object: group_id as AnyObject!, forKey: "SS_SELECTED_GROUP_ID")
                self.ssdm.save(object: name as AnyObject!, forKey: "SS_SELECTED_GROUP_NAME")
                self.notification.post(name: Notification.Name(rawValue: "SS_NOTIFICATION_CHANGE_GROUP"), object: nil)
            })
        }
    }
    
    fileprivate func configureCell(_ cell: SSGroupPanelTableViewCell, atIndexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: atIndexPath) 
        guard
            let name = mo.value(forKey: "name") as? String,
            let is_selected = mo.value(forKey: "is_selected") as? String,
            let notification_count = mo.value(forKey: "notification_count") as? Int
            else { return }
        
        cell.groupNameLabel.text = name
        
        let textColor = is_selected == "1" ? UIColor(rgba: "#1D6483") : UIColor.black
        cell.groupNameLabel.textColor = textColor
        
        let bgColor = is_selected == "1" ? UIColor(rgba: "#00A9D5").withAlphaComponent(0.3) : UIColor.clear
        cell.backgroundColor = bgColor
        
        let count = notification_count > 0 ? "(\(notification_count))" : ""
        cell.notificationLabel.text = count
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = ssdm.getMainContext()
        let ascending = self.isAscending
        let entity = SSMConstants.Entity.GROUP
        
        //let fetchRequest = NSFetchRequest(entityName: entity)
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
        fetchRequest.fetchBatchSize = 20
        
        let group_id = ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_SECTION_ID")
        let predicate = NSComparisonPredicate(keyPath: "group_id", withValue: group_id!, isExact: true)
        fetchRequest.predicate = predicate
        
        let descriptorSelector = #selector(NSString.localizedCompare(_:))
        let sortDescriptor = NSSortDescriptor.init(key: "id", ascending: ascending, selector:descriptorSelector)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        table.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        let tableViewController = table
        
        switch type {
        case .insert:
            tableViewController?.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableViewController?.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        let tableViewController = table
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableViewController?.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = tableViewController?.cellForRow(at: indexPath) {
                    configureCell(cell as! SSGroupPanelTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableViewController?.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        table.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        table.reloadData()
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Alert View Message
    
    fileprivate func showAlertViewMessage(_ message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let title = NSLocalizedString("Okay", comment: "")
        
        let action = UIAlertAction(title: title, style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func showAlertViewWithPolarQuestion(_ question: String, completion: @escaping (_ allow: Bool) -> Void) {
        let alert = UIAlertController(title: "", message: question, preferredStyle: .alert)
        
        let positiveString = NSLocalizedString("Yes", comment: "")
        let negativeString = NSLocalizedString("No", comment: "")
        
        let positiveAction = UIAlertAction(title: NSLocalizedString(positiveString, comment: ""), style: .default) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                completion(true)
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        let negativeAction = UIAlertAction(title: NSLocalizedString(negativeString, comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                completion(false)
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(positiveAction)
        alert.addAction(negativeAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}
