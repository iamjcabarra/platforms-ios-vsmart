//
//  UICollectionViewLeftAlignedLayout.swift
//  SwiftDemo
//
//  Created by fanpyi on 22/2/16.
//  Copyright © 2016 fanpyi. All rights reserved.
//  based on http://stackoverflow.com/questions/13017257/how-do-you-determine-spacing-between-cells-in-uicollectionview-flowlayout  https://github.com/mokagio/UICollectionViewLeftAlignedLayout

import UIKit


class UICollectionViewLeftAlignedLayoutSwift: UICollectionViewFlowLayout {
    
    var numberOfColumns: Int!
    var itemAttributes : NSMutableArray!
    var itemsSize : NSMutableArray!
    var contentSize : CGSize!
    
    
    var orientation: UIInterfaceOrientation!
    //    private lazy var dataManager: GradeBookDataManager = {
    //        let tm = GradeBookDataManager.sharedInstance()
    //        return tm
    //    }()
    
    override func prepare() {
        self.itemAttributes?.removeAllObjects()
        //        self.numberOfColumns = Int(self.dataManager.countRecordsForEntity(kGradeBookEntityV2, attribute: "section_index", value: 1))
        //
        if self.collectionView?.numberOfSections == 0 {
            return
        }
        
        //        self.collectionView?.numberOfSections()
        
        self.numberOfColumns = self.collectionView?.numberOfItems(inSection: 0)
                print("NUMBER OF COLUMNS \(self.numberOfColumns)")
        
        if (self.itemAttributes != nil && self.itemAttributes.count > 0) {
            for section in 0..<self.collectionView!.numberOfSections {
                if section != 0 && section != 1  {
                    continue
                }
                //                self.collectionView.indexPathfor
                //
                //                self.
                //
                //            let numberOfItems : Int = self.collectionView!.numberOfItemsInSection(section)
                
                //                let visibleIndexes = self.collectionView?.indexPathForVisi
                //
                //                let lastIndex = (visibleIndexes?.count)! - 1
                //                var prevIndex = (visibleIndexes?.first?.row)! - 1
                //                prevIndex = (prevIndex < 0) ? 0 : prevIndex
                //
                //                let attributesNext : UICollectionViewLayoutAttributes = self.layoutAttributesForItemAtIndexPath(NSIndexPath(forItem: lastIndex, inSection: section))!
                //
                //                let attributesPrev : UICollectionViewLayoutAttributes = self.layoutAttributesForItemAtIndexPath(NSIndexPath(forItem: lastIndex, inSection: section))!
                //
                //                if section == 0 {
                //                    var frameNext = attributesNext.frame
                //                    frameNext.origin.y = self.collectionView!.contentOffset.y
                //                    attributesNext.frame = frameNext
                //
                //                    var framePrev = attributesPrev.frame
                //                    framePrev.origin.y = self.collectionView!.contentOffset.y
                //                    attributesNext.frame = framePrev
                //                }
                //                if section == 1 {
                //                    var frameNext = attributesNext.frame
                //                    frameNext.origin.y = self.collectionView!.contentOffset.y + 44
                //                    attributesNext.frame = frameNext
                //
                //                    var framePrev = attributesPrev.frame
                //                    framePrev.origin.y = self.collectionView!.contentOffset.y + 44
                //                    attributesNext.frame = framePrev
                //                }
                
                
                
                
                //                let numberOfItems : Int = self.collectionView!.numberOfItemsInSection(section)
                
                
                //                let visibleIndexPaths = self.collectionView?.indexPathsForVisibleItems()
                //                let filteredVisible = visibleIndexPaths?.filter({
                //                    ($0.section) == 0
                //                })
                
                //                let filteredNumberOfItems =/
                
                
                
                let numberOfItems : Int = self.collectionView!.numberOfItems(inSection: section)
                for index in 0..<numberOfItems {
                    print("RETURN SECTION! \(section), INDEX! \(index)")
                    
                    let attributes : UICollectionViewLayoutAttributes = self.layoutAttributesForItem(at: IndexPath(item: index, section: section))!
                    if section == 0 {
                        var frame = attributes.frame
                        frame.origin.y = self.collectionView!.contentOffset.y
                        attributes.frame = frame
                    }
                    //                    if section == 1 {
                    //                        var frame = attributes.frame
                    //                        frame.origin.y = self.collectionView!.contentOffset.y + 44
                    //                        attributes.frame = frame
                    //                    }
                    //
                    //                    if index == 0 {
                    //                        var frame = attributes.frame
                    //                        frame.origin.x = self.collectionView!.contentOffset.x
                    //                        attributes.frame = frame
                    //                    }
                }
            }
            return
        }
        
        let currentOrientation = UIApplication.shared.statusBarOrientation
        if (self.itemsSize == nil || self.itemsSize.count != numberOfColumns || self.orientation != currentOrientation) {
            self.calculateItemsSize()
        }
        
        var column = 0
        var xOffset : CGFloat = 0
        var yOffset : CGFloat = 0
        var contentWidth : CGFloat = 0
        var contentHeight : CGFloat = 0
        
        for section in 0..<self.collectionView!.numberOfSections {
            let sectionAttributes = NSMutableArray()
            
            for index in 0..<numberOfColumns {
                let itemSize = (self.itemsSize[index] as AnyObject).cgSizeValue
                let indexPath = IndexPath(item: index, section: section)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = CGRect(x: xOffset, y: yOffset, width: (itemSize?.width)!, height: (itemSize?.height)!).integral
                
                //                print("SECTION! \(section), INDEX! \(index)")
                //
                //                if section == 0 && index == 0 {
                //                    attributes.zIndex = 1024;
                //                } else  if section == 0 || index == 0 {
                //                                    attributes.zIndex = 1023
                //                }
                //
                
                /*if section == 0 {
                    var frame = attributes.frame
                    frame.origin.y = self.collectionView!.contentOffset.y
                    attributes.frame = frame
                    attributes.zIndex = 1024
                }*/
                //                if section == 1 {
                //                    var frame = attributes.frame
                //                    frame.origin.y = self.collectionView!.contentOffset.y + 44
                //                    attributes.frame = frame
                //                    attributes.zIndex = 1024
                //                }
                
                /*if section != 1 && section != 0 {
                    attributes.zIndex = 1023
                }*/
                
                sectionAttributes.add(attributes)
                
                xOffset += (itemSize?.width)!
                column += 1
                
                if column == numberOfColumns {
                    if xOffset > contentWidth {
                        contentWidth = xOffset
                    }
                    
                    column = 0
                    xOffset = 0
                    yOffset += (itemSize?.height)!
                }
            }
            if (self.itemAttributes == nil) {
                self.itemAttributes = NSMutableArray(capacity: self.collectionView!.numberOfSections)
            }
            self.itemAttributes.add(sectionAttributes)
        }
        
        let attributes : UICollectionViewLayoutAttributes = (self.itemAttributes.lastObject as! NSMutableArray).lastObject as! UICollectionViewLayoutAttributes
        contentHeight = attributes.frame.origin.y + attributes.frame.size.height
        self.contentSize = CGSize(width: contentWidth, height: contentHeight)
    }
    
    override var collectionViewContentSize : CGSize {
        guard self.contentSize != nil else {
            return CGSize(width: 0, height: 0)
        }
        
        return self.contentSize
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let sectionAttributes = self.itemAttributes [(indexPath as NSIndexPath).section] as! [UICollectionViewLayoutAttributes]
        return sectionAttributes[(indexPath as NSIndexPath).row] as UICollectionViewLayoutAttributes
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var attributes = [UICollectionViewLayoutAttributes]()
        if self.itemAttributes != nil {
            for section in self.itemAttributes {
                
                let filteredArray  =  (section as! NSMutableArray).filtered(using: NSPredicate(block: { (evaluatedObject, bindings) -> Bool in
                    return rect.intersects((evaluatedObject as! UICollectionViewLayoutAttributes).frame)
                })
                    ) as! [UICollectionViewLayoutAttributes]
                
                
                attributes.append(contentsOf: filteredArray)
                
            }
        }
        
        return attributes
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    func sizeForItemWithColumnIndex(_ columnIndex: Int) -> CGSize {
        let height: CGFloat = 44
        let width: CGFloat = 200
        
        /*let orientation = UIApplication.shared.statusBarOrientation
        self.orientation = orientation// for checking purpose
        
        if orientation == .portrait {
            
            // 4 CELLS MAXIMUM ARE VISIBLE
            if self.numberOfColumns <= 6 {
                width = (self.collectionView?.frame.width)!/self.numberOfColumns
            }
            
        } else if orientation == .landscapeLeft || orientation == .landscapeRight {
            
            // 7 CELLS MAXIMUM ARE VISIBLE WHEN IN LANDSCAPE
            if self.numberOfColumns <= 9 {
                width = (self.collectionView?.frame.width)!/self.numberOfColumns
            }
            
        }*/
        
        return CGSize(width: width, height: height)
    }
    
    func calculateItemsSize() {
        self.itemsSize = NSMutableArray(capacity: numberOfColumns)
        for index in 0..<numberOfColumns {
            self.itemsSize.add(NSValue(cgSize: self.sizeForItemWithColumnIndex(index)))
        }
    }
}


/*extension UICollectionViewLayoutAttributes {
    func leftAlignFrameWithSectionInset(sectionInset:UIEdgeInsets){
        var frame = self.frame
        frame.origin.x = sectionInset.left
        self.frame = frame
    }
}

class UICollectionViewLeftAlignedLayoutSwift: UICollectionViewFlowLayout {
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var attributesCopy: [UICollectionViewLayoutAttributes] = []
        if let attributes = super.layoutAttributesForElements(in: rect) {
            attributes.forEach({ attributesCopy.append($0.copy() as! UICollectionViewLayoutAttributes) })
        }
        
        for attributes in attributesCopy {
            if attributes.representedElementKind == nil {
                let indexpath = attributes.indexPath
                if let attr = layoutAttributesForItem(at: indexpath) {
                    attributes.frame = attr.frame
                }
            }
        }
        return attributesCopy
    }
    
    func layoutAttributesForItemAtIndexPath(indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
        if let currentItemAttributes = super.layoutAttributesForItem(at: indexPath as IndexPath)?.copy() as? UICollectionViewLayoutAttributes {
            let sectionInset = self.evaluatedSectionInsetForItemAtIndex(index: indexPath.section)
            let isFirstItemInSection = indexPath.item == 0
            let layoutWidth = self.collectionView!.frame.width - sectionInset.left - sectionInset.right
            
            if (isFirstItemInSection) {
                currentItemAttributes.leftAlignFrameWithSectionInset(sectionInset: sectionInset)
                return currentItemAttributes
            }
            
            let previousIndexPath = NSIndexPath(item: indexPath.item - 1, section: indexPath.section)
            
            let previousFrame = layoutAttributesForItem(at: previousIndexPath as IndexPath)?.frame ?? CGRect.zero
            let previousFrameRightPoint = previousFrame.origin.x + previousFrame.width
            let currentFrame = currentItemAttributes.frame
            let strecthedCurrentFrame = CGRect(origin: CGPoint(x: sectionInset.left,y :currentFrame.origin.y), size: CGSize(width: layoutWidth, height: currentFrame.size.height)) /*CGRectMake(sectionInset.left,
                                                   currentFrame.origin.y,
                                                   layoutWidth,
                                                   currentFrame.size.height)*/
            // if the current frame, once left aligned to the left and stretched to the full collection view
            // widht intersects the previous frame then they are on the same line
            let isFirstItemInRow = !previousFrame.intersects(strecthedCurrentFrame)
            
            if (isFirstItemInRow) {
                // make sure the first item on a line is left aligned
                currentItemAttributes.leftAlignFrameWithSectionInset(sectionInset: sectionInset)
                return currentItemAttributes
            }
            
            var frame = currentItemAttributes.frame
            frame.origin.x = previousFrameRightPoint + evaluatedMinimumInteritemSpacingForSectionAtIndex(sectionIndex: indexPath.section)
            currentItemAttributes.frame = frame
            return currentItemAttributes
            
        }
        return nil
    }
    
    func evaluatedMinimumInteritemSpacingForSectionAtIndex(sectionIndex:Int) -> CGFloat {
        if let delegate = self.collectionView?.delegate as? UICollectionViewDelegateFlowLayout {
            if delegate.responds(to: #selector(UICollectionViewDelegateFlowLayout.collectionView(_:layout:minimumInteritemSpacingForSectionAt:))) {
                return delegate.collectionView!(self.collectionView!, layout: self, minimumInteritemSpacingForSectionAt: sectionIndex)
                
            }
        }
        return self.minimumInteritemSpacing
        
    }
    
    func evaluatedSectionInsetForItemAtIndex(index: Int) ->UIEdgeInsets {
        if let delegate = self.collectionView?.delegate as? UICollectionViewDelegateFlowLayout {
            if  delegate.responds(to: #selector(UICollectionViewDelegateFlowLayout.collectionView(_:layout:insetForSectionAt:))) {
                return delegate.collectionView!(self.collectionView!, layout: self, insetForSectionAt: index)
            }
        }
        return self.sectionInset
    }
}*/
