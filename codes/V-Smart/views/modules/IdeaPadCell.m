//
//  IdeaPadCell.m
//  V-Smart
//
//  Created by VhaL on 3/5/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "IdeaPadCell.h"

@interface IdeaPadCell ()

@end

@implementation IdeaPadCell
@synthesize delegate, labelContent, labelDateModified, labelWordCount;

-(void)initializeCell:(id)withDelegate{
    delegate = withDelegate;
}


@end
