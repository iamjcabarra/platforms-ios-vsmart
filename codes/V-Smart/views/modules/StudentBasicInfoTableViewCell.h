//
//  StudentBasicInfoTableViewCell.h
//  V-Smart
//
//  Created by Julius Abarra on 10/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentBasicInfoTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblNickname;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblBirthdate;
@property (strong, nonatomic) IBOutlet UILabel *lblBirthPlace;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblNationality;
@property (strong, nonatomic) IBOutlet UILabel *lblSpokenLanguages;
@property (strong, nonatomic) IBOutlet UILabel *lblHobbies;
@property (strong, nonatomic) IBOutlet UILabel *lblSports;
@property (strong, nonatomic) IBOutlet UILabel *lblActNickname;
@property (strong, nonatomic) IBOutlet UILabel *lblActEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblActBirthdate;
@property (strong, nonatomic) IBOutlet UILabel *lblActBirthPlace;
@property (strong, nonatomic) IBOutlet UILabel *lblActAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblActNationality;
@property (strong, nonatomic) IBOutlet UILabel *lblActSpokenLanguages;
@property (strong, nonatomic) IBOutlet UILabel *lblActHobbies;
@property (strong, nonatomic) IBOutlet UILabel *lblActSports;

@end
