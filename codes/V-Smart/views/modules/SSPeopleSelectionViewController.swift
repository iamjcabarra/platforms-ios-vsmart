//
//  SSPeopleSelectionViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 29/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class SSPeopleSelectionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet fileprivate var headerLabel: UILabel!
    @IBOutlet fileprivate var cancelButton: UIButton!
    @IBOutlet fileprivate var doneButton: UIButton!
    
    @IBOutlet fileprivate var collectionView: UICollectionView!
    @IBOutlet fileprivate var collectionViewHeight: NSLayoutConstraint!
    
    @IBOutlet fileprivate var tableView: UITableView!
    @IBOutlet fileprivate var searchBar: UISearchBar!
    
    fileprivate let tableCellIdentifier = "people_cell_identifier"
    fileprivate let collectionCellIdentifier = "selected_people_cell_identifier"
    
    fileprivate var searchKey = ""
    fileprivate var selectedPeopleList = [[String: String]]()
    
    fileprivate var newGroupMembers = [String]()
    fileprivate var allGroupMembers = [String]()
    
    // MARK: - Shared Data Manager
    
    fileprivate lazy var ssdm: SocialStreamDataManager = {
        let dataManager = SocialStreamDataManager.sharedInstance
        return dataManager
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerLabel.text = NSLocalizedString("Add More Students", comment: "")
        
        // Cancel button event handler
        let cancelText = NSLocalizedString("Cancel", comment: "").uppercased()
        cancelButton.setTitle(cancelText, for: UIControlState())
        cancelButton.setTitle(cancelText, for: .selected)
        cancelButton.setTitle(cancelText, for: .highlighted)
        
        let cancelButtonAction = #selector(self.cancelButtonAction(_:))
        cancelButton.addTarget(self, action: cancelButtonAction, for: .touchUpInside)
        
        // Done button event handler
        let doneText = NSLocalizedString("Done", comment: "").uppercased()
        doneButton.setTitle(doneText, for: UIControlState())
        doneButton.setTitle(doneText, for: .selected)
        doneButton.setTitle(doneText, for: .highlighted)
        
        let doneButtonAction = #selector(self.doneButtonAction(_:))
        doneButton.addTarget(self, action: doneButtonAction, for: .touchUpInside)
        
        // Search bar additional set up
        searchBar.placeholder = NSLocalizedString("Search Student", comment: "")
        searchBar.delegate = self
        
        // Set estimated size of collection view cell
        //if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
        //    layout.estimatedItemSize = CGSize(width: 104.0, height: 44.0)
        //}
        
        // Initialize current group members
        initializeCurrentGroupMembers()
        
        // Load all students for course section
        let sectionID = ssdm.stringValue(ssdm.fetchUserDefaultsObject(forKey: "SS_SELECTED_SECTION_ID"))
        listStudentsForSection(sectionID!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 170, height: 44)
    }
    
    // MARK: - Student List
    
    fileprivate func listStudentsForSection(_ sectionID: String) {
        ssdm.requestStudentsForSection(sectionID) { (doneBlock) in
            DispatchQueue.main.async(execute: {
                self.reloadFetchedResultsController()
            })
        }
    }
    
    // MARK: - Current Group Members
    
    fileprivate func initializeCurrentGroupMembers() {
        let predicate = ssdm.db.createPredicate(key: "is_removed", withValue: "0", isExact: true)
        guard let members = ssdm.db.retrieveEntity(SSMConstants.Entity.MEMBER, properties: ["id"], filter: predicate, sortKey: "id", ascending: true) as [AnyObject]? else {
            return
        }
        
        for member in members {
            if let id = member["id"] as? String {
                allGroupMembers.append(id)
            }
        }
    }
    
    // MARK: - Button Event Handler
    
    func cancelButtonAction(_ sender: UIButton) {
        let question = NSLocalizedString("Are you sure you want to cancel adding student(s) to group?", comment: "")
        showAlertViewWithPolarQuestion(question, completion: { (allow) in
            if (allow) { _ = self.navigationController?.popViewController(animated: true) }
        })
    }
    
    func doneButtonAction(_ sender: UIButton) {
        if newGroupMembers.count > 0 {
            ssdm.createGroupMembers(newGroupMembers, handler: { (doneBlock) in
                if doneBlock {
                    DispatchQueue.main.async(execute: {
                        _ = self.navigationController?.popViewController(animated: true)
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        let message = NSLocalizedString("There was an error adding selected student(s) to group. Please try again.", comment: "")
                        self.showAlertViewMessage(message)
                    })
                }
            })
        }
        else {
            let message = NSLocalizedString("You have not selected any student yet. Please select at least one (1) to continue.", comment: "")
            showAlertViewMessage(message)
        }
    }
    
    // MARK: - Alert View Message
    
    fileprivate func showAlertViewMessage(_ message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let title = NSLocalizedString("Okay", comment: "")
        
        let action = UIAlertAction(title: title, style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func showAlertViewWithPolarQuestion(_ question: String, completion: @escaping (_ allow: Bool) -> Void) {
        let alert = UIAlertController(title: "", message: question, preferredStyle: .alert)
        
        let positiveString = NSLocalizedString("Yes", comment: "")
        let negativeString = NSLocalizedString("No", comment: "")
        
        let positiveAction = UIAlertAction(title: NSLocalizedString(positiveString, comment: ""), style: .default) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                completion(true)
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        let negativeAction = UIAlertAction(title: NSLocalizedString(negativeString, comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                completion(false)
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(positiveAction)
        alert.addAction(negativeAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Search Bar Delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchKey = searchText
        reloadFetchedResultsController()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchKey = ""
        reloadFetchedResultsController()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let key = searchBar.text {
            searchKey = key
            reloadFetchedResultsController()
            searchBar.resignFirstResponder()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = self.searchKey
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier, for: indexPath) as! SSPeopleSelectionTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: SSPeopleSelectionTableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        guard
            let avatar = mo.value(forKey: "avatar") as? String,
            let first_name = mo.value(forKey: "first_name") as? String,
            let last_name = mo.value(forKey: "last_name") as? String
            else { return }
        
        cell.userImage.sd_setImage(with: URL(string: avatar))
        cell.userName.text = "\(first_name) \(last_name)"
    }
    
    // MARK: - Table View Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        guard
            let id = mo.value(forKey: "id") as? String,
            let first_name = mo.value(forKey: "first_name") as? String,
            let last_name = mo.value(forKey: "last_name") as? String
            else { return }
        
        // Consider selected people (students) as new members
        if !(newGroupMembers.contains(id)) {
            selectedPeopleList.append(["id": id, "name": "\(first_name) \(last_name)"])
        }
        
        // Update contents of collections
        allGroupMembers.contains(id) ?
            (allGroupMembers = allGroupMembers.arrayRemovingObject(id)) :
            (allGroupMembers.append(id))
        newGroupMembers.contains(id) ?
            (newGroupMembers = newGroupMembers.arrayRemovingObject(id)) :
            (newGroupMembers.append(id))
        
        // Update UI for changes
        self.resizeCollectionView()
        self.reloadFetchedResultsController()
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = self.ssdm.getMainContext()
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: SSMConstants.Entity.STUDENT)
        fetchRequest.fetchBatchSize = 20
        
        let predicate1 = NSPredicate(format: "NOT (id IN %@)", allGroupMembers)
        let predicate2 = self.ssdm.db.createPredicate(key: "search_string", withValue: searchKey, isExact: false)
        fetchRequest.predicate = searchKey != "" ? NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1, predicate2]) : predicate1
        
        let descriptorSelector = #selector(NSString.localizedCaseInsensitiveCompare(_:))
        let sortDescriptor = NSSortDescriptor.init(key: "search_string", ascending: true, selector:descriptorSelector)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.tableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! SSPeopleSelectionTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Collection View Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if selectedPeopleList.count > 0 {
            return 1
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedPeopleList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellIdentifier, for: indexPath) as! SSSelectedPeopleCollectionViewCell
        
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.masksToBounds = false
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowOpacity = 0.2
        cell.layer.shadowRadius = 2
        cell.layer.cornerRadius = 5
        
        let people = selectedPeopleList[(indexPath as NSIndexPath).row]
        if let name = people["name"] {
            cell.nameLabel.text = name
        }
        
        return cell
    }
    
    // MARK: - Collection View Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let people = selectedPeopleList[(indexPath as NSIndexPath).row]
        
        if let id = people["id"] {
            if (newGroupMembers.contains(id)) {
                newGroupMembers = newGroupMembers.arrayRemovingObject(id)
                selectedPeopleList.remove(at: (indexPath as NSIndexPath).row)
                resizeCollectionView()
            }
            if (allGroupMembers.contains(id)) {
                allGroupMembers = allGroupMembers.arrayRemovingObject(id)
                reloadFetchedResultsController()
            }
        }
    }
    
    // MARK: - Resizing Collection View
    
    fileprivate func resizeCollectionView() {
        var height: CGFloat = 0.0
        
        if selectedPeopleList.count > 0 {
            height = 50.0
        }
        
        if selectedPeopleList.count > 3 {
            height = 150.0
        }
        
        collectionViewHeight.constant = height
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            self.collectionView.reloadData()
        }) 
    }
    
}
