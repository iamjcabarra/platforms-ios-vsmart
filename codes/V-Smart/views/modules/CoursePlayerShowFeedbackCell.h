//
//  CoursePlayerShowFeedbackCell.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 05/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoursePlayerShowFeedbackCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *feedBackTextView;

@end
