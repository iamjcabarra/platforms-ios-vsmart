//
//  LearningCompetencyTableViewCell.m
//  V-Smart
//
//  Created by Julius Abarra on 10/1/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "LearningCompetencyTableViewCell.h"

@implementation LearningCompetencyTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
