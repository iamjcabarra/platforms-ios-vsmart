//
//  SocialStreamIconPopViewController.m
//  V-Smart
//
//  Created by Ryan Migallos on 2/3/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "SocialStreamIconPopViewController.h"
#import "SocialStreamIconPopViewCell.h"
#import "EGOImageLoader.h"
#import "ResourceManager.h"
#import "AppDelegate.h"

@interface SocialStreamIconPopViewController ()

@property (nonatomic, strong) ResourceManager *rm;

@end

@implementation SocialStreamIconPopViewController

static NSString * const reuseIdentifier = @"cell_pop_icon_id";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    self.rm = [AppDelegate resourceInstance];
    
    if ([self.type isEqualToString:@"emoticon"]) {
        [self fetchEmoticons];
    }

    if ([self.type isEqualToString:@"emoji"]) {
        [self fetchEmoji];
    }

}

- (void)fetchEmoticons {

    __weak typeof (self) weakSelf = self;
    
    [self.rm requestSocialStreamEmoticons:^(NSArray *list) {
        if (list) {
            NSLog(@"emoticons list : %@", list);
            
            weakSelf.items = [NSArray arrayWithArray:list];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.collectionView reloadData];
            });
        }
    }];
    
}

- (void)fetchEmoji {
    
    __weak typeof (self) weakSelf = self;
    
    [self.rm requestSocialStreamEmojis:^(NSArray *list) {
        if (list) {
            NSLog(@"emoji list : %@", list);
            
            weakSelf.items = [NSArray arrayWithArray:list];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.collectionView reloadData];
            });
        }
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)viewWillLayoutSubviews{
//    [super viewWillLayoutSubviews];
//    self.view.superview.bounds = CGRectMake(0, 0, 150, 150);
//}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SocialStreamIconPopViewCell *cell = (SocialStreamIconPopViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    NSDictionary *d = self.items[indexPath.row];
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", d[@"icon_url"]  ]];
    cell.imageView.image = [[EGOImageLoader sharedImageLoader] imageForURL:imageURL shouldLoadWithObserver:nil];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    NSDictionary *d = (NSDictionary *)self.items[indexPath.row];

    //Update Delegate
    [self.delegate didFinishSelectingData:d type:[NSString stringWithFormat:@"%@", self.type] ];
}

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
