//
//  CPAttendanceStatusSelectionView.swift
//  V-Smart
//
//  Created by Julius Abarra on 01/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

protocol CPAttendanceStatusSelectionViewDelegate: class {
    func refreshSeatingArrangementView()
}

class CPAttendanceStatusSelectionView: UIViewController {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var presentLabel: UILabel!
    @IBOutlet var lateLabel: UILabel!
    @IBOutlet var absentLabel: UILabel!
    @IBOutlet var presentButton: UIButton!
    @IBOutlet var lateButton: UIButton!
    @IBOutlet var absentButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    
    var seatObject: NSManagedObject!
    weak var delegate: CPAttendanceStatusSelectionViewDelegate?
    
    // MARK: - Shared Data Manager
    
    fileprivate lazy var dataManager: TestGuruDataManager = {
        let tgdm = TestGuruDataManager.sharedInstance()
        return tgdm!
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.text = NSLocalizedString("Attendance", comment: "").uppercased()
        self.presentLabel.text = NSLocalizedString("Present", comment: "").uppercased()
        self.lateLabel.text = NSLocalizedString("Late", comment: "").uppercased()
        self.absentLabel.text = NSLocalizedString("Absent", comment: "").uppercased()
        
        let cancelTitle = NSLocalizedString("Cancel", comment: "")
        self.cancelButton.setTitle(cancelTitle.uppercased(), for: .normal)
        self.cancelButton.setTitle(cancelTitle.uppercased(), for: .selected)
        self.cancelButton.setTitle(cancelTitle.uppercased(), for: .highlighted)
        
        self.cancelButton.addTarget(self, action: #selector(self.cancelButtonAction(sender:)), for: .touchUpInside)
        self.presentButton.addTarget(self, action: #selector(self.presentButtonAction(sender:)), for: .touchUpInside)
        self.lateButton.addTarget(self, action: #selector(self.lateButtonAction(sender:)), for: .touchUpInside)
        self.absentButton.addTarget(self, action: #selector(self.absentButtonAction(sender:)), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cancelButtonAction(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func presentButtonAction(sender: UIButton) {
        self.changeAttendanceStatus(forAction: "0")
    }
    
    func lateButtonAction(sender: UIButton) {
        self.changeAttendanceStatus(forAction: "1")
    }
    
    func absentButtonAction(sender: UIButton) {
        self.changeAttendanceStatus(forAction: "2")
    }
    
    fileprivate func changeAttendanceStatus(forAction action: String) {
        DispatchQueue.main.async(execute: {
            let indicatorString = "\(NSLocalizedString("Processing", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
        })
        
        if self.seatObject != nil {
            let cs_seatplan_id = self.dataManager.stringValue(self.seatObject.value(forKey: "csp_id"))
            let teacher_user_id = self.dataManager.loginUser()
            let is_absent = action == "2" ? "1" : "0"
            let is_late = action == "1" ? "1" : "0"
            let date_time = self.getStringDateToday(withFormat: "YYYY-MM-dd HH:mm:ss", localTimezone: false)
            
            let body = ["cs_seatplan_id": cs_seatplan_id!,
                        "teacher_user_id": teacher_user_id!,
                        "is_absent": is_absent,
                        "is_late": is_late,
                        "is_deleted": "0",
                        "date_time": date_time];
            
            self.dataManager.requestChangeAttendanceStatus(withPostBody: body, doneBlock: { (success) in
                if success {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        self.dismiss(animated: true, completion: {
                            self.delegate?.refreshSeatingArrangementView()
                        })
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        let message = NSLocalizedString("Sorry, there was an error updating the status. Please try again.", comment: "")
                        self.view.makeToast(message: message, duration: 2.0, position: "center" as AnyObject)
                    })
                }
            })
        }
        else {
            DispatchQueue.main.async(execute: {
                HUD.hideUIBlockingIndicator()
                let message = NSLocalizedString("Sorry, there was an error updating the status. Please try again.", comment: "")
                self.view.makeToast(message: message, duration: 2.0, position: "center" as AnyObject)
            })
        }
    }
    
    fileprivate func getStringDateToday(withFormat format: String, localTimezone: Bool = false) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        if localTimezone { dateFormatter.timeZone = TimeZone(identifier: "UTC") }
        return dateFormatter.string(from: Date())
    }
    
}
