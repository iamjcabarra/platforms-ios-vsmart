//
//  TagCell.h
//  V-Smart
//
//  Created by VhaL on 3/1/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface TagCell : SWTableViewCell <UITextFieldDelegate>
@property (nonatomic, strong) IBOutlet UILabel *tagLabel;
@property (nonatomic, strong) IBOutlet UITextField *tagTextField;
@property (nonatomic, strong) IBOutlet UIButton *acceptButton;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (nonatomic, strong) IBOutlet UIImageView *shareIndicatorImageView;
-(void)hideEditControls:(bool)hide;
-(void)hideShareIndicator:(bool)hide;
-(void)initializeCell;
@property (nonatomic, assign) id delegate;
@end
