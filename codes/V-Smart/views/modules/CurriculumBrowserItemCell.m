//
//  CurriculumBrowserItemCell.m
//  V-Smart
//
//  Created by Julius Abarra on 18/02/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "CurriculumBrowserItemCell.h"

@implementation CurriculumBrowserItemCell

- (void)prepareForReuse {
    [self showSelection:NO];
    [super prepareForReuse];
}

- (void)showSelection:(BOOL)selection {
    self.contentView.backgroundColor = selection ? UIColorFromHex(0xEFEFF4) : UIColorFromHex(0xFFFFFF);
}

@end
