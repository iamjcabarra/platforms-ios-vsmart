//
//  GBTGradePanel.swift
//  V-Smart
//
//  Created by Ryan Migallos on 15/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class GBTGradePanel: UIViewController {
    
    fileprivate let cellIdentifier = "gbt_grade_panel_cell"
    @IBOutlet var tableView: UITableView!
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>?
    
    var isAscending = true
    
    var term_id = ""
    
    fileprivate lazy var gbdm: GBDataManager = {
        let dataManager = GBDataManager.sharedInstance
        return dataManager
    }()
    
    var scrollDelegate : ScrollCommunicationDelegate?
    
    fileprivate let notification = NotificationCenter.default
    var student_name = ""
    var sort_by_gender = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cellNib = UINib(nibName: "GBTGradePanelCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellIdentifier)
        
        self.setUpNotification()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.teardownNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension GBTGradePanel: UITableViewDataSource, UIScrollViewDelegate, NSFetchedResultsControllerDelegate {
    
    func reloadList(forTermID term_id: String) {
        self.term_id = term_id
        reloadFetchedResultsController()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! GBTGradePanelCell
        cell.backgroundColor = UIColor.clear
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    fileprivate func configureCell(_ cell: GBTGradePanelCell, atIndexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: atIndexPath) 
        guard
            var initialGrade = mo.value(forKey: "initial_grade") as? String,
            var quarterlyGrade = mo.value(forKey: "quarterly_grade") as? String
            else { return }
        
        initialGrade = self.gbdm.formatStringNumber(initialGrade)
        quarterlyGrade = self.gbdm.formatStringNumber(quarterlyGrade)
        
        cell.initialGradeLabel.text = "\(initialGrade)%"
        cell.quarterlyGradeLabel.text = "\(quarterlyGrade)%"
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = gbdm.getMainContext()
        let ascending = self.isAscending
        let entity = GBTConstants.Entity.STUDENT
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: entity)
//        let fetchRequest = NSFetchRequest(entityName: entity)
        fetchRequest.fetchBatchSize = 20
        
        //PREDICATE
        /*
         NOTE: GB_SELECTED_TERM_ID <- please declared this in the constants
         FOR NOW: we are hard coding the term_id value
         */
        //        let term_id = gbdm.fetchUserDefaultsObject(forKey: "GB_SELECTED_TERM_ID")//TODO
//        let term_id = "1"//HARDCODING
//        let predicate = NSComparisonPredicate(keyPath: "term_id", withValue: self.term_id, isExact: true)
//        print("PREDICATE ~==== \(predicate)")
//        fetchRequest.predicate = predicate
        
        var finalPredicate: NSPredicate
        
        let predicateTID = NSComparisonPredicate(keyPath: "term_id", withValue: self.term_id, isExact: true)
        
        if self.student_name.characters.count > 0 {
            let predicateName = NSComparisonPredicate(keyPath: "name", withValue: self.student_name, isExact: false)
            finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateTID, predicateName])
        } else {
            finalPredicate = predicateTID
        }
        
        fetchRequest.predicate = finalPredicate
        
        let descriptorSelector = #selector(NSString.localizedCompare(_:))
        if self.sort_by_gender {
            let sortGender = NSSortDescriptor(key: "gender", ascending: ascending, selector:descriptorSelector)
            fetchRequest.sortDescriptors = [sortGender]
        } else {
        let sortName = NSSortDescriptor(key: "name", ascending: ascending, selector:descriptorSelector)
        fetchRequest.sortDescriptors = [sortName]
        }
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        let tableViewController = tableView
        
        switch type {
        case .insert:
            tableViewController?.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableViewController?.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        let tableViewController = tableView
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableViewController?.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = tableViewController?.cellForRow(at: indexPath) {
                    configureCell(cell as! GBTGradePanelCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                tableViewController?.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableViewController?.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y;
        self.scrollDelegate?.gbScrollViewDidScroll(GBPanelType.GradePanel, scrollTo: offsetY)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewWillBeginDragging(GBPanelType.GradePanel)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.GradePanel)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate == false {
            self.scrollDelegate?.gbScrollViewDidEndDecelerating(GBPanelType.GradePanel)
        }
    }
    
    func setUpNotification() {
        notification.addObserver(self, selector: #selector(self.studentSearchAction(_:)), name: NSNotification.Name(rawValue: "GBT_STUDENT_SEARCH"), object: nil)
        notification.addObserver(self, selector: #selector(self.studentSortAction(_:)), name: NSNotification.Name(rawValue: "GBT_STUDENT_SORT"), object: nil)
    }
    
    func teardownNotification() {
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "GBT_STUDENT_SEARCH"), object: nil)
        notification.removeObserver(self, name: NSNotification.Name(rawValue: "GBT_STUDENT_SORT"), object: nil)
    }
    
    func studentSortAction(_ notification: Notification) {
        let sort_by_gender = notification.object as! Bool
        self.sort_by_gender = sort_by_gender
        self.reloadFetchedResultsController()
    }
    
    func studentSearchAction(_ notification: Notification) {
        var name = notification.object as? String
        name = (name == nil) ? "" : name
        self.student_name = name!
        self.reloadFetchedResultsController()
    }
}

