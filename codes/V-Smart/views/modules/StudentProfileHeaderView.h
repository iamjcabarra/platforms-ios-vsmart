//
//  StudentProfileHeaderView.h
//  V-Smart
//
//  Created by Julius Abarra on 10/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentProfileHeaderView : UIView

@property (strong, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (strong, nonatomic) IBOutlet UILabel *lblStudentName;
@property (strong, nonatomic) IBOutlet UILabel *lblPosition;

@end
