//
//  CPMCurriculumDetailsView.swift
//  V-Smart
//
//  Created by Julius Abarra on 20/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit
import WebKit

class CPMCurriculumDetailsView: UIViewController, WKNavigationDelegate {
    
    var curriculumID = ""
    var curriculumTitle = ""
    
    fileprivate var webViewObject: WKWebView!
    fileprivate let networkRequestHelper = CPMNetworkRequestHelper()
    
    // MARK: - Data Manager
    
    fileprivate lazy var curriculumPlannerDataManager: CPMDataManager = {
        let cpdm = CPMDataManager.sharedInstance
        return cpdm
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupWebView()
        self.navigationItem.hidesBackButton = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customizeNavigationBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Custom Navigation Bar
    
    func customizeNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor(rgba: "#4475B7")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        self.title = self.curriculumTitle
    }
    
    // MARK: - Set Up WKWebView Object
    
    func setupWebView() {
        let configuration = WKWebViewConfiguration.init()
        self.webViewObject = WKWebView.init(frame: self.view.frame, configuration: configuration)
        self.webViewObject.navigationDelegate = self
        
        let baseURL = self.networkRequestHelper.appBaseURL()
        let urlString = "http://\(baseURL)\(endPointForCurriculumDetails())\(self.curriculumID)"
        
        if let url = URL(string: urlString) as URL? {
            let request = URLRequest(url: url)
            self.webViewObject.load(request)
            self.view.addSubview(self.webViewObject)
        }
        else {
            self.navigationItem.hidesBackButton = false
            let message = "Can't open requested page."
            self.showNotificationMessage(message)
        }
    }
    
    // MARK: - WKWebView Navigation Delegate
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        let indicatorString = "\(NSLocalizedString("Loading", comment: ""))..."
        HUD.showUIBlockingIndicator(withText: indicatorString)
        self.navigationItem.hidesBackButton = true
    }
    
    internal func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.navigationItem.hidesBackButton = false
        HUD.hideUIBlockingIndicator()
        self.showNotificationMessage(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        HUD.hideUIBlockingIndicator()
        self.navigationItem.hidesBackButton = false
    }
    
    // MARK: - Change Device Orientation
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.webViewObject.removeFromSuperview()
        self.webViewObject = nil
        self.setupWebView()
    }
    
    // MARK: - Alert View Message
    
    func showNotificationMessage(_ message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: NSLocalizedString("Okay", comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }

}
