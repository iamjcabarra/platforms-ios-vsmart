//
//  FileBrowserTableViewController.h
//  V-Smart
//
//  Created by Julius Abarra on 10/22/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

@protocol FileBrowserViewDelegate <NSObject>

@required
- (void)selectedFile:(NSDictionary *)d;

@end

#import <UIKit/UIKit.h>

@interface FileBrowserTableViewController : UITableViewController

@property (nonatomic, assign) id delegate;

@end
