//
//  LPMCurriculumPlannerPopover.swift
//  V-Smart
//
//  Created by Julius Abarra on 25/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

protocol LPMCurriculumPlannerPopoverDelegate: class {
    func selectedCurriculumObject(_ object: NSDictionary)
}

class LPMCurriculumPlannerPopover: UITableViewController, NSFetchedResultsControllerDelegate {
    
    weak var delegate: LPMCurriculumPlannerPopoverDelegate?
    var courseID = ""
    
    // MARK: - Data Manager
    
    fileprivate lazy var curriculumPlannerDataManager: CPMDataManager = {
        let cpdm = CPMDataManager.sharedInstance
        return cpdm
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadCurriculumList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - List Curriculum

    func loadCurriculumList() {
       self.curriculumPlannerDataManager.requestCurriculumListForCourseWithID(self.courseID) { (success) in
            DispatchQueue.main.async(execute: {
                self.reloadFetchedResultsController()
            })
        }
    }
    
    // MARK: - Table View Data Source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        return sectionData.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: UITableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath) 
        cell.textLabel?.text = mo.value(forKey: "curriculum_title") as? String
    }
    
    // MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath) 
        let curriculmID = mo.value(forKey: "curriculum_id") as! String
        let curriculumTitle = mo.value(forKey: "curriculum_title") as! String
        let curriculumObject = ["title": curriculumTitle, "id": curriculmID]
        
        self.dismiss(animated: true, completion: {
            self.delegate?.selectedCurriculumObject(curriculumObject as NSDictionary)
        })
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let ctx = self.curriculumPlannerDataManager.mainContext
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: CPMConstants.Entity.CURRICULUM_ASSOCIATION)
        
//        let fetchRequest = NSFetchRequest(entityName: CPMConstants.Entity.CURRICULUM_ASSOCIATION)
        fetchRequest.fetchBatchSize = 20
        
        let descriptorSelector = #selector(NSString.localizedCaseInsensitiveCompare(_:))
        let sortDescriptor = NSSortDescriptor.init(key: "curriculum_title", ascending: true, selector:descriptorSelector)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.tableView.cellForRow(at: indexPath) {
                    self.configureCell(cell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
}
