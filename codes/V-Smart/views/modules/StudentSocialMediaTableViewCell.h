//
//  StudentSocialMediaTableViewCell.h
//  V-Smart
//
//  Created by Julius Abarra on 10/11/2015.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentSocialMediaTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTwitter;
@property (strong, nonatomic) IBOutlet UILabel *lblFacebook;
@property (strong, nonatomic) IBOutlet UILabel *lblInstagram;
@property (strong, nonatomic) IBOutlet UILabel *lblActTwitter;
@property (strong, nonatomic) IBOutlet UILabel *lblActFacebook;
@property (strong, nonatomic) IBOutlet UILabel *lblActInstagram;

@end
