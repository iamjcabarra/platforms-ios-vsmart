//
//  SocialStreamComment.h
//  V-Smart
//
//  Created by Ryan Migallos on 1/22/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface SocialStreamComment : UIViewController

@property (nonatomic, assign) BOOL hideCommentBox;
@property (nonatomic, assign) NSUInteger groupid;
@property (nonatomic, strong) NSString *messageid;

- (IBAction)unwindFromConfirmationForm:(id)sender;
- (IBAction)sendUserMessage:(id)sender;

@end
