//
//  CALMainViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 13/02/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class CALMainViewController: UIViewController {
    
    @IBOutlet var topView: UIView!
    @IBOutlet var leftView: UIView!
    
    fileprivate let notification = NotificationCenter.default
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.relayoutMainView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.relayoutMainView()
    }
    
    // MARK: - Handle Changes in Device Orientation
    
    fileprivate func relayoutMainView() {
        let isLandscape = UIDevice.current.orientation.isLandscape
       
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: [UIViewAnimationOptions.curveEaseOut, UIViewAnimationOptions.allowUserInteraction], animations: {
            
            // Relayout views
            self.topView.isHidden = isLandscape ? true : false
            self.leftView.isHidden = isLandscape ? false : true
            
            // Post notification
            let object = ["orientation": isLandscape ? "1" : "0"]
            let name = Notification.Name(rawValue: "CAL_NOTIFICATION_DEVICE_ORIENTATION_CHANGE")
            self.notification.post(name: name, object: object)
            
        }, completion: { (animate) in })
    }
    
}
