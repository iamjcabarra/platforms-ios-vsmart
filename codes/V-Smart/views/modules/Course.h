//
//  Course.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/6/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Topic;

@interface Course : NSManagedObject

@property (nonatomic, retain) NSString * course_code;
@property (nonatomic, retain) NSString * course_id;
@property (nonatomic, retain) NSString * course_index;
@property (nonatomic, retain) NSString * course_name;
@property (nonatomic, retain) NSString * course_description;
@property (nonatomic, retain) NSString * initial;
@property (nonatomic, retain) NSString * cs_id;
@property (nonatomic, retain) NSString * section_id;
@property (nonatomic, retain) NSString * schedule;
@property (nonatomic, retain) NSString * venue;
@property (nonatomic, retain) NSSet *topics;
@end

@interface Course (CoreDataGeneratedAccessors)

- (void)addTopicsObject:(Topic *)value;
- (void)removeTopicsObject:(Topic *)value;
- (void)addTopics:(NSSet *)values;
- (void)removeTopics:(NSSet *)values;

@end
