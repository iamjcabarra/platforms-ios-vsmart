//
//  PlayListDataManager.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 26/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

//#import <Foundation/Foundation.h>
//
//@interface PlayListDataManager : NSObject
//
//@end

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "PlayListConstants.h"

static NSString *kPlayListEntityV2 = @"PlayListV2";
static NSString *kPlayListFileEntityV2 = @"PlayListFileV2";

/////// BLOCK TYPES //////
typedef void (^PlayListDoneBlock)(BOOL status);
typedef void (^PlayListErrorBlock)(NSString *error);
typedef void (^PlayListProgressBlock)(CGFloat progress);
typedef void (^PlayListDoneProgressBlock)(NSString *status);
typedef void (^PlayListListBlock)(NSArray *list);

@interface PlayListDataManager : NSObject

@property (nonatomic, copy, readwrite) PlayListProgressBlock progressBlock;
@property (nonatomic, copy, readwrite) PlayListDoneProgressBlock doneProgressBlock;
@property (nonatomic, readonly) NSManagedObjectContext *mainContext;
@property (nonatomic, readonly) NSManagedObjectContext *workerContext;
@property (nonatomic, readonly) NSURLSession *session;
@property (nonatomic, readonly) NSDateFormatter *formatter;

+ (instancetype)sharedInstance;
- (void)saveContext;
- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate;
- (BOOL)clearContentsForEntity:(NSString *)entity;

/////// PUBLIC METHODS //////
- (NSString *)loginUser;
- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value;
- (NSPredicate *)predicateForKeyPath:(NSString *)keypath object:(id)object;
- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value;
- (NSManagedObject *)getEntity:(NSString *)entity attribute:(NSString *)attribute parameter:(NSString *)parameter context:(NSManagedObjectContext *)context;
- (NSUInteger)fetchCountForEntity:(NSString *)entity;
- (NSArray *)fetchObjectsForEntity:(NSString *)entity;
- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate;
- (NSManagedObject *)insertNewRecordForEntity:(NSString *)entity;
- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context;
- (void)saveObject:(id)object forKey:(NSString *)key;
- (id)fetchObjectForKey:(NSString *)key;
- (BOOL)clearDataForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)ctx;
- (NSString *)stringValue:(id)object;
- (NSURL *)buildURL:(NSString *)string;
- (NSMutableURLRequest *)buildRequestWithMethod:(NSString *)method NSURL:(NSURL *)url body:(id)body;
- (BOOL)isArrayObject:(id)object;
- (void)saveTreeContext:(NSManagedObjectContext *)context;
- (NSString *)urlEncode:(id<NSObject>)value;


// PLAYLIST
- (void)requestPlayListForUser:(NSString *)userid doneBlock:(PlayListDoneBlock)doneBlock;
- (void)requestDeletePlayListItemWithUploadID:(NSString *)upload_id doneBlock:(PlayListDoneBlock)doneBlock;
- (void)updatePlayListFileForUUID:(NSString *)uuid rawFile:(NSData *)rawFile;
- (void)updateProgressForUUID:(NSString *)uuid progress:(NSString *)progress;

- (void)requestListOfTeachers:(PlayListListBlock)listBlock;
//- (void)requestUpdatePlayListItemForUploadID:(NSString *)uploadid meta:(NSDictionary *)meta file:(NSDictionary *)object doneBlock:(PlayListDoneBlock)doneBlock;
- (void)requestUpdatePlayListItemForUploadID:(NSString *)uploadid meta:(NSDictionary *)meta file:(NSDictionary *)object errorBlock:(PlayListErrorBlock)errorBlock;
- (void)requestFileSizeLimit:(PlayListErrorBlock)errorBlock;

- (void)requestListOfUserForSearchText:(NSString *)searchText listBlock:(PlayListListBlock)listBlock;
- (void)postUpdateOfUploadID:(NSString *)upload_id withPostBody:(NSDictionary *)postBody doneBlock:(PlayListDoneBlock)doneBlock;

@end
