//
//  TBPdfPageCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 13/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBPdfPageCell : UICollectionViewCell

- (void)renderPage:(CGPDFPageRef)pageref;

@end
