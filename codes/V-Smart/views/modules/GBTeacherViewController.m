//
//  GBTeacherViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 20/05/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "GBTeacherViewController.h"
#import "ResourceManager.h"
#import "AppDelegate.h"

@interface GBTeacherViewController ()

@property (nonatomic, strong) ResourceManager *rm;

@property (strong, nonatomic) IBOutlet UITextField *classNameTextField;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIButton *dropDownButton;
@property (strong, nonatomic) IBOutlet UIButton *sortButton;

@end

@implementation GBTeacherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.rm = [AppDelegate resourceInstance];
    
    [self.rm requestGradebookForClass:@"2" doneBlock:^(BOOL status) {
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
