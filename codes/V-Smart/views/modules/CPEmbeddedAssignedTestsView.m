//
//  CPEmbeddedAssignedTestsView.m
//  V-Smart
//
//  Created by Julius Abarra on 30/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "CPEmbeddedAssignedTestsView.h"
#import "TBNewTestItemCell.h"
#import "TestGuruDataManager.h"
#import "TestGuruDataManager+Course.h"
#import "TBClassHelper.h"
#import "V_Smart-Swift.h"
#import "HUD.h"

@interface CPEmbeddedAssignedTestsView () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) TestGuruDataManager *tgdm;
@property (strong, nonatomic) TBClassHelper *classHelper;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) IBOutlet UITableView *testTable;
@property (strong, nonatomic) IBOutlet UIButton *loadMoreButton;

@property (assign, nonatomic) NSInteger currentPage;
@property (assign, nonatomic) NSInteger totalFilteredTests;
@property (assign, nonatomic) NSInteger totalPages;
@property (assign, nonatomic) NSInteger totalItems;

@property (strong, nonatomic) NSString *searchKeyword;
@property (assign, nonatomic) BOOL isAscending;

@end

@implementation CPEmbeddedAssignedTestsView

static NSString *kTestCellIdentifier = @"testItemCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Data Manager
    self.tgdm = [TestGuruDataManager sharedInstance];
    
    // Class Helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Test Table View
    [self setUpTestTableView];
    
    // Search Default
    self.searchKeyword = @"";
    
    // Sort Default
    self.isAscending = YES;
    
    // Pagination Settings
    self.currentPage = 0;
    self.totalFilteredTests = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    
    // Load More Button
    self.loadMoreButton.hidden = YES;
    [self.loadMoreButton addTarget:self
                            action:@selector(loadMoreButtonAction:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    // Initial Paginated Test List
    NSDictionary *settings = [self getSettingsForTestPaginationIsReset:YES shouldLoadNextPage:NO];
    [self listPaginatedTestForSettings:settings];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Search Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processSearchAction:)
                                                 name:@"processSearchAction"
                                               object:nil];
    
    // Sort Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processSortAction:)
                                                 name:@"processSortAction"
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Remove Notification Observers
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"processSearchAction"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"processSortAction"
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Test Table View

- (void)setUpTestTableView {
    // Set Protocols
    self.testTable.dataSource = self;
    self.testTable.delegate = self;
    
    // Allow Selection
    self.testTable.allowsSelection = NO;
    self.testTable.allowsMultipleSelection = NO;
    
    // Default Height
    self.testTable.rowHeight = 125.0f;
    
    // Resusable Cell
    UINib *testCellItemNib = [UINib nibWithNibName:@"TBNewTestItemCell" bundle:nil];
    [self.testTable registerNib:testCellItemNib forCellReuseIdentifier:kTestCellIdentifier];
}

#pragma mark - Paginated Test List

- (NSDictionary *)getSettingsForTestPaginationIsReset:(BOOL)isReset shouldLoadNextPage:(BOOL)nextPage {
    NSString *limit = [self.tgdm stringValue:[self.tgdm fetchObjectForKey:kTG_PAGINATION_LIMIT]];
    NSString *current_page = @"1";
    
    if ([limit integerValue] < 1) {
        limit = @"10";
    }
    
    if (!isReset) {
        NSInteger number = nextPage ? self.currentPage + 1 : self.currentPage;
        current_page = [NSString stringWithFormat:@"%zd", number];
    }
    
    return @{@"limit":limit, @"current_page":current_page, @"search_keyword":self.searchKeyword};
}

- (void)listPaginatedTestForSettings:(NSDictionary *)settings {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Loading", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    NSString *courseid = [NSString stringWithFormat:@"%@", [self.courseObject valueForKey:@"course_id"]];
    NSString *userid = [self.tgdm loginUser];
    
    self.loadMoreButton.hidden = YES;
    
    [self.tgdm requestPaginatedTestListForCourse:courseid andUser:userid withSettingsForPagination:settings dataBlock:^(NSDictionary *data) {
        if (data != nil) {
            self.currentPage = [data[@"current_page"] integerValue];
            self.totalItems = [data[@"total_items"] integerValue];
            self.totalFilteredTests = [data[@"total_filtered"] integerValue];
            self.totalPages = [data[@"total_pages"] integerValue];
        }
        
        __weak typeof(self) wo = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo reloadFetchedResultsController];
            BOOL hideButton = (wo.totalPages > wo.currentPage) ? NO : YES;
            wo.loadMoreButton.hidden = hideButton;
            [HUD hideUIBlockingIndicator];
        });
    }];
}

- (void)loadMoreButtonAction:(id)sender {
    self.isAscending = NO;
    NSDictionary *settings = [self getSettingsForTestPaginationIsReset:NO shouldLoadNextPage:YES];
    [self listPaginatedTestForSettings:settings];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.testTable = tableView;
    
    TBNewTestItemCell *cell = [tableView dequeueReusableCellWithIdentifier:kTestCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    TBNewTestItemCell *testCellItem = (TBNewTestItemCell *)cell;
    [self configureQuestionCell:testCellItem managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(TBNewTestItemCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    NSString *name = [[mo valueForKey:@"name"] uppercaseString];
    NSString *formatted_date_open = [[mo valueForKey:@"formatted_date_open"] uppercaseString];
    NSString *formatted_date_close = [[mo valueForKey:@"formatted_date_close"] uppercaseString];
    NSString *item_string = [NSString stringWithFormat:@"%@", [mo valueForKey:@"item_count"]];
    NSString *point_string = [NSString stringWithFormat:@"%@", [mo valueForKey:@"total_score"]];
    NSString *is_graded = [mo valueForKey:@"is_graded"];
    NSString *image_string = [is_graded isEqualToString:@"1"] ? @"graded.png" : @"non-graded.png";
    
    if ([formatted_date_close isEqualToString:@""]) {
        formatted_date_close = NSLocalizedString(@"No Expiration", nil);
    }
    
    NSString *start_date_string = NSLocalizedString(@"Start Date", nil);
    NSString *end_date_string = NSLocalizedString(@"End Date", nil);
    
    cell.testTitle.text = name;
    cell.testStartDateLabel.text = [NSString stringWithFormat:@"%@: %@", start_date_string, formatted_date_open];
    cell.testEndDateLabel.text = [NSString stringWithFormat:@"%@: %@", end_date_string, formatted_date_close];
    cell.testStageTypeImage.image = [UIImage imageNamed:image_string];
    
    cell.itemsValueLabel.text = [NSString stringWithFormat:@"%@", item_string];
    [cell updateItemsLabel:item_string];
    
    cell.pointsValueLabel.text = [NSString stringWithFormat:@"%@", point_string];
    [cell updatePointsLabel:point_string];
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.tgdm.mainContext;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCourseTestListEntity];
    [fetchRequest setFetchBatchSize:10];
    
    if (![self.searchKeyword isEqualToString:@""]) {
        NSPredicate *predicate = [self predicateForKeyPathContains:@"search_string" value:self.searchKeyword];
        fetchRequest.predicate = predicate;
    }
    
    NSSortDescriptor *sort_date_created = [NSSortDescriptor sortDescriptorWithKey:@"sort_date_created" ascending:self.isAscending];
    [fetchRequest setSortDescriptors:@[sort_date_created]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.testTable beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.testTable insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.testTable deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.testTable;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.testTable endUpdates];
}

#pragma mark - Reload Fetched Results

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.testTable reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error: %@", [err localizedDescription]);
    }
}

#pragma mark - Actions for Notifications

- (void)processSortAction:(NSNotification *)notification {
    NSDictionary *info = notification.userInfo;
    
    if (info != nil) {
        self.isAscending = [info[@"isAscending"] boolValue];
        [self reloadFetchedResultsController];
    }
}

- (void)processSearchAction:(NSNotification *)notification {
    NSDictionary *info = notification.userInfo;
    
    if (info != nil) {
        self.searchKeyword = info[@"searchKeyword"];
        NSDictionary *settings = @{@"limit":@"1000", @"current_page":@"1", @"search_keyword":@""};
        
        if ([self.searchKeyword isEqualToString:@""]) {
            settings = [self getSettingsForTestPaginationIsReset:YES shouldLoadNextPage:NO];
        }
        
        [self listPaginatedTestForSettings:settings];
    }
}

@end