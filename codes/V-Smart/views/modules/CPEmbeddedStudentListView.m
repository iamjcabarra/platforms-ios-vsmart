//
//  CPEmbeddedStudentListView.m
//  V-Smart
//
//  Created by Julius Abarra on 30/03/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "CPEmbeddedStudentListView.h"
#import "CPStudentCellItem.h"
#import "StudentProfileTableViewController.h"
#import "CourseDataManager.h"
#import "UIImageView+WebCache.h"
#import "HUD.h"

@interface CPEmbeddedStudentListView () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) CourseDataManager *cdm;
@property (strong, nonatomic) UIRefreshControl *tableRefreshControl;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) IBOutlet UIView *emptyView;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *emptyMessageLabel;

@property (strong, nonatomic) NSString *searchKeyword;
@property (assign, nonatomic) BOOL isAscending;

@property (assign, nonatomic) NSInteger temporaryStudentCount;
@property (strong, nonatomic) NSString *studentid;

@end

@implementation CPEmbeddedStudentListView

static NSString *kStudentCellIdentifier = @"studentCellIdentifier";
static NSString *kShowStudentProfile = @"showStudentProfile";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Data Manager
    self.cdm = [CourseDataManager sharedInstance];
    
    // Default Search
    self.searchKeyword = @"";
    
    // Default Sort
    self.isAscending = YES;
    
    // Student Table
    [self setUpStudentTableView];
    
    // List Students
    [self listAllStudents];
    
    // Pull to Refresh
    SEL refreshAction = @selector(refreshStudentList);
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    // Student Count (1 to avoid loading of empty view on first load)
    self.temporaryStudentCount = 1;
    
    // Empty View
    [self shouldShowEmptyView:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Search Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processSearchAction:)
                                                 name:@"processSearchAction"
                                               object:nil];
    
    // Sort Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processSortAction:)
                                                 name:@"processSortAction"
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Remove Notification Observers
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"processSearchAction"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"processSortAction"
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Course Table View

- (void)setUpStudentTableView {
    // Set Protocols
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Allow Selection
    self.tableView.allowsSelection = YES;
    self.tableView.allowsMultipleSelection = NO;
    
    // Default Height
    self.tableView.rowHeight = 80.0f;
    
    // Resusable Cell
    UINib *studentCellItemNib = [UINib nibWithNibName:@"CPStudentCellItem" bundle:nil];
    [self.tableView registerNib:studentCellItemNib forCellReuseIdentifier:kStudentCellIdentifier];
}

#pragma mark - List All Students

- (void)listAllStudents {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Loading", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    NSString *courseid = [self.courseObject valueForKey:@"cs_id"];
    __weak typeof(self) wo = self;
    
    [self.cdm requestStudentListForCourseWithID:courseid dataBlock:^(NSDictionary *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD hideUIBlockingIndicator];
            NSString *message = NSLocalizedString(@"There was an error loading this page. Please check your internet connection.", nil);
            [wo showErrorMessage:message show:(data == nil) ? YES: NO];
            
            if (data != nil) {
                NSString *countString = [wo.cdm stringValue:data[@"count"]];
                NSInteger count = [countString integerValue];
                [wo postStudentCountNotification:count];
                wo.temporaryStudentCount = count;
                [wo reloadFetchedResultsController];
            }
        });
    }];
}

- (void)refreshStudentList {
    NSString *courseid = [self.courseObject valueForKey:@"cs_id"];
    __weak typeof(self) wo = self;
    
    [self.cdm requestStudentListForCourseWithID:courseid dataBlock:^(NSDictionary *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.tableRefreshControl endRefreshing];
            NSString *message = NSLocalizedString(@"There was an error loading this page. Please check your internet connection.", nil);
            [wo showErrorMessage:message show:(data == nil) ? YES: NO];
            
            if (data != nil) {
                NSString *countString = [wo.cdm stringValue:data[@"count"]];
                NSInteger count = [countString integerValue];
                [wo postStudentCountNotification:count];
                wo.temporaryStudentCount = count;
                [wo reloadFetchedResultsController];
            }
        });
    }];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    NSInteger count = [sectionInfo numberOfObjects];
    
    // NOTE: REMOVE IF PAGINATED
    [self shouldShowEmptyView:(count > 0) ? NO : YES];
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView = tableView;
    
    CPStudentCellItem *cell = [tableView dequeueReusableCellWithIdentifier:kStudentCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    CPStudentCellItem *studentCellItem = (CPStudentCellItem *)cell;
    [self configureQuestionCell:studentCellItem managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureQuestionCell:(CPStudentCellItem *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath {
    NSString *avatar_url = [self.cdm stringValue:[mo valueForKey:@"avatar"]];
    NSString *first_name = [self.cdm stringValue:[mo valueForKey:@"first_name"]];
    NSString *last_name = [self.cdm stringValue:[mo valueForKey:@"last_name"]];
    NSString *contact_number = [self.cdm stringValue:[mo valueForKey:@"contact_number"]];
    NSString *email = [self.cdm stringValue:[mo valueForKey:@"email"]];
    
    /*
    cell.studentImage.layer.cornerRadius = 25.0f;
    [cell.studentImage sd_setImageWithURL:[NSURL URLWithString:avatar_url]];
    cell.studentNameLabel.text = [NSString stringWithFormat:@"%@, %@", last_name, first_name];
    cell.studentContactNumber.text = contact_number;
    cell.studentEmailAddress.text = email;
     */
    
    cell.studentImage.layer.cornerRadius = 25.0f;
    cell.studentNameLabel.text = [NSString stringWithFormat:@"%@, %@", last_name, first_name];
    cell.studentContactNumber.text = contact_number;
    cell.studentEmailAddress.text = email;
    
    [cell.studentImage sd_setImageWithURL:[NSURL URLWithString:avatar_url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if ((image == nil) || (error != nil)) {
            cell.studentImage.image = [UIImage imageNamed:@"circled_student_male.png"];
        }
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    self.studentid = [self.cdm stringValue:[mo valueForKey:@"id"]];
    
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Please wait", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    __weak typeof(self) wo = self;
    
    [self.cdm requestStudentProfileWithID:self.studentid doneBlock:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [wo performSegueWithIdentifier:kShowStudentProfile sender:nil];
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
            });
        }
    }];
}

#pragma mark - Application Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kShowStudentProfile] && ![self.studentid isEqualToString:@""]) {
        StudentProfileTableViewController *studentProfile = (StudentProfileTableViewController *)[segue destinationViewController];
        studentProfile.studentid = self.studentid;
    }
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *ctx = self.cdm.mainContext;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kStudentEntity];
    [fetchRequest setFetchBatchSize:10];
    
    if (![self.searchKeyword isEqualToString:@""]) {
        NSPredicate *predicate = [self predicateForKeyPathContains:@"search_string" value:self.searchKeyword];
        fetchRequest.predicate = predicate;
    }
    
    NSSortDescriptor *last_name = [NSSortDescriptor sortDescriptorWithKey:@"last_name"
                                                                ascending:self.isAscending
                                                                 selector:@selector(caseInsensitiveCompare:)];
    
    NSSortDescriptor *first_name = [NSSortDescriptor sortDescriptorWithKey:@"first_name"
                                                                 ascending:self.isAscending
                                                                  selector:@selector(caseInsensitiveCompare:)];
    [fetchRequest setSortDescriptors:@[last_name, first_name]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:ctx
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - Reload Fetched Results

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error: %@", [err localizedDescription]);
    }
}

#pragma mark - Alert Messages

- (void)showErrorMessage:(NSString*)message show:(BOOL)show {
    if (show) {
        NSString *butOkay = NSLocalizedString(@"Okay", nil);
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *theAction = [UIAlertAction actionWithTitle:butOkay
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
        [alert addAction:theAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - Actions for Notifications

- (void)processSortAction:(NSNotification *)notification {
    NSDictionary *info = notification.userInfo;
    
    if (info != nil) {
        self.isAscending = [info[@"isAscending"] boolValue];
        [self listAllStudents];
    }
}

- (void)processSearchAction:(NSNotification *)notification {
    NSDictionary *info = notification.userInfo;
    
    if (info != nil) {
        self.searchKeyword = info[@"searchKeyword"];
        [self listAllStudents];
    }
}

- (void)postStudentCountNotification:(NSInteger)count {
    NSDictionary *dictionary = @{@"count":@(count)};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"processStudentCountAction"
                                                        object:nil
                                                      userInfo:dictionary];
}

#pragma mark - Custom Empty View

- (void)shouldShowEmptyView:(BOOL)show {
    // NOTE: REFACTOR IF PAGINATED
    if ([self.searchKeyword isEqualToString:@""]) {
        show = (self.temporaryStudentCount > 0) ? NO : YES;
    }
    
    if (show) {
        NSString *message = NSLocalizedString(@"No enrolled student.", nil);
        
        if (![self.searchKeyword isEqualToString:@""]) {
            message = NSLocalizedString(@"No results found for", nil);
            message =[NSString stringWithFormat:@"%@ \"%@\"", message, self.searchKeyword];
        }
        
        self.emptyMessageLabel.text = message;
    }
    
    self.emptyView.hidden = !show;
    self.tableView.hidden = show;
}

@end