//
//  TBPdfViewController.m
//  V-Smart
//
//  Created by Ryan Migallos on 13/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TBPdfViewController.h"
#import "TBPdfPageCell.h"

@interface TBPdfViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate> {
    CGPDFDocumentRef document;
    NSInteger totalPages;
}

@end

@implementation TBPdfViewController

static NSString * const reuseIdentifier = @"pdf_cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
//    NSString *password = @"6500ee4f138c27fc77af";
//    [self pdfDocument:@"test" password:password];
    [self pdfFile:self.fileName password:self.passPhrase title:self.bookTitle];
    
    [self setupRightBarButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupRightBarButton {
    
    SEL dismissAction = @selector(closeButtonAction:);
    
    UIBarButtonItem *dismissButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                   target:self
                                                                                   action:dismissAction];
    self.navigationItem.rightBarButtonItem = dismissButton;
}

- (void)closeButtonAction:(UIBarButtonItem *)button {
    
    CGPDFDocumentRelease(document);
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)pdfFile:(NSString *)file password:(NSString *)key title:(NSString *)string {
    
    self.title = [NSString stringWithFormat:@"%@", string ];
    NSLog(@"title : %@", self.title);
    
    CFURLRef inputUrl = (__bridge CFURLRef)[NSURL fileURLWithPath:file isDirectory:NO];
    document = CGPDFDocumentCreateWithURL(inputUrl);
    
    const char *text = [key UTF8String];
    BOOL encrypted = CGPDFDocumentIsEncrypted(document);
    if (encrypted) {
        NSLog(@"this is encrypted file...");
        BOOL unlock = CGPDFDocumentUnlockWithPassword(document, text);
        if (unlock) {
            NSLog(@"pdf was successfully unlock...");
        }
    }
    
    totalPages = (NSInteger)CGPDFDocumentGetNumberOfPages(document);
    [self.collectionView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return totalPages;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TBPdfPageCell *cell = (TBPdfPageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    NSInteger currentPage = indexPath.row + 1;
    CGPDFPageRef page = CGPDFDocumentGetPage(document, currentPage);
    [cell renderPage:page];
    return cell;
}

#pragma mark <UICollectionViewDelegateFlowLayout>

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = collectionView.bounds.size.width;
    CGFloat height = collectionView.bounds.size.height;
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)collectionView.collectionViewLayout;
    
    CGFloat width = self.view.frame.size.width;
    CGFloat itemWidth = flowLayout.itemSize.width;
    NSInteger numberOfCells = width / itemWidth;
    NSInteger edgeInsets = (width - (numberOfCells * itemWidth)) / (numberOfCells + 1);
    return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
}

- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator {
    [self resizeContents];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self resizeContents];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self resizeContents];
}

- (void)resizeContents {
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    CGSize size = self.collectionView.bounds.size;
    
    CGFloat w = size.width;
    CGFloat h = size.height;
    
    layout.itemSize = CGSizeMake(w, h);
    [layout invalidateLayout];
}

@end
