//
//  SchoolStreamChildViewController.h
//  V-Smart
//
//  Created by VhaL on 3/24/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface SchoolStreamChildViewController : UIViewController <UIWebViewDelegate, UITextViewDelegate>

@end
