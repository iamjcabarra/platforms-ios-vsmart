//
//  PlayListMediaCell.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/26/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface PlayListMediaCell : UICollectionViewCell

@property (strong, nonatomic) ALAsset *asset;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
