//
//  NoteBookCollectionViewController.m
//  V-Smart
//
//  Created by VhaL on 3/6/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NoteBookCollectionViewController.h"
#import "NoteBookCollectionCell.h"
#import "NNoteBook.h"
#import "NoteBookViewController.h"
#import "NoteEntry.h"

@interface NoteBookCollectionViewController ()
@property (nonatomic, strong) NoteBookViewController *otherView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@end

@implementation NoteBookCollectionViewController
@synthesize collectionView, noteBookArray, otherView;

-(void)viewDidLoad{
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    UINib *cellNib = [UINib nibWithNibName:@"NoteBookCollectionCell" bundle:nil];
    [collectionView registerNib:cellNib forCellWithReuseIdentifier:@"NoteBookCollectionCellDefault"];
    [collectionView registerNib:cellNib forCellWithReuseIdentifier:@"NoteBookCollectionCellAdd"];
    [collectionView registerNib:cellNib forCellWithReuseIdentifier:@"NoteBookCollectionCellGeneral"];
    
    // Enhancement
    // jca-05102016
    // Autoresize notebook collection view
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin |UIViewAutoresizingFlexibleRightMargin;
    
    [self loadData];
    [self addRefreshMode];
}

-(void)addRefreshMode{
    self.refreshControl = [[UIRefreshControl alloc] init];
    NSString *pullRefresh = NSLocalizedString(@"Pull to Refresh",nil);
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:pullRefresh];
    [self.refreshControl addTarget:self action:@selector(refreshTriggered) forControlEvents:UIControlEventValueChanged];
    
    [self.collectionView addSubview:self.refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
}

-(void)refreshTriggered{
    [self loadData];
    [otherView loadData];
    [self.refreshControl endRefreshing];
}

-(void)castOtherController{
    otherView = (NoteBookViewController*)self.otherNoteBookViewController;
}

-(void)moveGeneralNotebookToFirst{
    NoteBook *notebookGeneral;
    for (NoteBook *notebook in noteBookArray) {
        if ([notebook.noteBookName isEqualToString:kNotebookGeneralName]){
            notebookGeneral = notebook;
            break;
        }
    }
    
    if (notebookGeneral){
        [noteBookArray removeObject:notebookGeneral];
        [noteBookArray insertObject:notebookGeneral atIndex:0];
    }
}

-(void)loadData{
    noteBookArray = [NSMutableArray arrayWithArray:[[NoteDataManager sharedInstance] getNoteBooks:NO useMainThread:YES]];
    
    [self moveGeneralNotebookToFirst];
    [noteBookArray insertObject:[NSString stringWithFormat:@"New notebook"] atIndex:0];
    [self.collectionView reloadData];
}

-(IBAction)buttonAddNoteBookTapped:(NoteBookCollectionCell*)cell{
    
    NoteBook *result = [[NoteDataManager sharedInstance] addNoteBook:cell.actionTextField.text withColor:cell.selectedColorId useMainThread:YES];
    
    [self reflectAddedNoteBookInView:result];
    [otherView reflectAddedNoteBookInView:result];

}

-(IBAction)buttonEditNoteBookTapped:(NoteBookCollectionCell*)sender{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
    NoteBook *noteBook = [noteBookArray objectAtIndex:indexPath.row];
    
    noteBook.noteBookName = [sender.labelTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    noteBook.colorId = [NSNumber numberWithInt:sender.selectedColorId];
    noteBook.modified = [NSDate date];
    
    
    [[NoteDataManager sharedInstance] editNoteBook:noteBook useMainThread:YES];
    
    [self reflectModifiedNoteBookInView:noteBook inIndexPath:indexPath];
    [otherView reflectModifiedNoteBookInView:noteBook inIndexPath:[NSIndexPath indexPathForRow:(indexPath.row - 1) inSection:0]];
}

-(IBAction)cellButtonActionApplyTapped:(NoteBookCollectionCell*)collectionViewCell withActionTitle:(NSString*)actionTitle{
    
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:collectionViewCell];
    NoteBook *noteBook = [noteBookArray objectAtIndex:indexPath.row];
        
    if ([actionTitle isEqualToString:@"Share"]){

        [[NoteDataManager sharedInstance] shareFolder:noteBook isShared:![[noteBook isShared] boolValue] useMainThread:YES];
        [self reflectModifiedNoteBookInView:noteBook inIndexPath:indexPath];
        [otherView reflectModifiedNoteBookInView:noteBook inIndexPath:[NSIndexPath indexPathForRow:(indexPath.row - 1) inSection:0]];
        
    }
    
    if ([actionTitle isEqualToString:@"Delete"]){

        // JCA-02-16-2017
        // No need for this since action menu is disabled to General notebook
//        if ([noteBook.noteBookName isEqualToString:kNotebookGeneralName]){
//
//            /* localizable strings */
//            NSString *cannotDelete = NSLocalizedString(@"cannot be deleted", nil); //checked
//            [self.view makeToast:cannotDelete duration:2.0f position:@"center"];
//            
//            return;
//        }
    
        
        //bool deleteSuccess = [[NoteDataManager sharedInstance] deleteNoteBook:noteBook useMainThread:YES];
        //if (!deleteSuccess){
        //    return;
        //}
        //
        //[self reflectDeletedNoteBookInView:indexPath];
        //[otherView reflectDeletedNoteBookInView:[NSIndexPath indexPathForRow:(indexPath.row - 1) inSection:0]];
        
        // Enhancement #1305
        // jca-05042016
        
        [self shouldDeleteNoteBook:noteBook.noteBookName completion:^(BOOL delete) {
            if (delete) {
                bool deleteSuccess = [[NoteDataManager sharedInstance] deleteNoteBook:noteBook useMainThread:YES];
                
                if (!deleteSuccess){
                    NSString *message = NSLocalizedString(@"There was an error deleting notebook. Please try again later.", nil);
                    [self.view makeToast:message duration:2.0f position:@"center"];
                    
                    return;
                }
                
                [self reflectDeletedNoteBookInView:indexPath];
                [otherView reflectDeletedNoteBookInView:[NSIndexPath indexPathForRow:(indexPath.row - 1) inSection:0]];
            }
        }];
    }
    
    // Feature #884
    // jca-04272016
    // Implement Duplicate Notebook Operation
    
    if ([actionTitle isEqualToString:@"Duplicate"]) {
        NSString *noteBookName = [NSString stringWithFormat:@"%@", noteBook.noteBookName];
        NSNumber *colorCode = noteBook.colorId;
        
        // Create unique name for duplicated note book to avoid confusion
        NSInteger counter = 2;
        NSString *duplicatedNoteBookName = [NSString stringWithFormat:@"%@(%zd)", noteBookName, counter];
        BOOL doesExist = [self doesNoteBookExist:duplicatedNoteBookName];
        
        while (doesExist) {
            counter++;
            duplicatedNoteBookName = [NSString stringWithFormat:@"%@(%zd)", noteBookName, counter];
            doesExist = [self doesNoteBookExist:duplicatedNoteBookName];
        }
        
        // Create New Notebook (Duplicated)
        NoteBook *duplicatedNoteBook = [[NoteDataManager sharedInstance] addNoteBook:duplicatedNoteBookName
                                                                           withColor:[colorCode intValue]
                                                                       useMainThread:YES];
        // Copy Notes to New Notebook (Duplicated)
        NSArray *notes = [noteBook.notes allObjects];
        for (Note *note in notes) {
            BOOL isRemoved = [note.isRemoved boolValue];
            
            if (!isRemoved) {
                [self insertNoteToNoteBook:note noteBook:duplicatedNoteBook];
            }
        }
        
        // Reload Notebook Collection
        [self loadData];
        [otherView loadData];
    }
}

-(BOOL)doesNoteBookExist:(NSString *)noteBookName {
    NSArray *noteBookList = [NSMutableArray arrayWithArray:[[NoteDataManager sharedInstance] getNoteBooks:NO useMainThread:YES]];
    
    for (NSManagedObject *mo in noteBookList) {
        NSString *noteBookInStore = [self stringValue:[mo valueForKey:@"noteBookName"]];
        
        if ([noteBookName isEqualToString:noteBookInStore]) {
            return YES;
        }
    }
    
    return NO;
}

-(void)insertNoteToNoteBook:(Note *)note noteBook:(NoteBook *)noteBook {
    NoteEntry *entry = [NoteEntry new];
    entry.title = note.title;
    entry.description = note.content;
    entry.tags = [self getDelimitedTags:note.noteTags];
    entry.noteBookName = noteBook.noteBookName;
    entry.noteBookID = noteBook.noteBookID;
    entry.color = [note.colorId intValue];
    entry.fontTypeId = [note.fontTypeId intValue];
    entry.fontSizeId = [note.fontSizeId intValue];
    [[NoteDataManager sharedInstance] addNote:entry useMainThread:YES];
}

-(NSString *)getDelimitedTags:(NSSet *)setOfTags {
    NSArray *tagObjects = [setOfTags allObjects];
    NSString *tags = @"";
    
    for (NSManagedObject *mo in tagObjects) {
        NSManagedObject *tagMO = [mo valueForKey:@"tag"];
        
        if (tagMO != nil) {
            NSString *tagName = [self stringValue:[tagMO valueForKey:@"tagName"]];
            tags = [NSString stringWithFormat:@"%@;%@", tags, tagName];
        }
    }
    
    return [tags stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)stringValue:(id)object {
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] || [value isEqualToString:@"null"]) {
        return @"";
    }
    
    return value;
}

// Enhancement #1305
// jca-05042016

- (void)shouldDeleteNoteBook:(NSString *)noteBookName completion:(void (^)(BOOL delete))response {
    NSString *avTitle = NSLocalizedString(@"Delete Notebook", nil);
    NSString *premstr = NSLocalizedString(@"Are you sure you want to delete", nil);
    NSString *posmstr = NSLocalizedString(@"and its contents", nil);
    NSString *message = [NSString stringWithFormat:@"%@ \"%@\" %@?", premstr, noteBookName, posmstr];
    NSString *butNegR = NSLocalizedString(@"No", nil);
    NSString *butPosR = NSLocalizedString(@"Yes", nil);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:avTitle
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *negAction = [UIAlertAction actionWithTitle:butNegR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                          response(NO);
                                                      }];
    
    UIAlertAction *posAction = [UIAlertAction actionWithTitle:butPosR
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                          response(YES);
                                                      }];
    [alert addAction:negAction];
    [alert addAction:posAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)reflectAddedNoteBookInView:(NoteBook*)newNoteBook{
    
    [noteBookArray removeObjectAtIndex:0];
    
    [noteBookArray addObject:newNoteBook];
    
    NSSortDescriptor *sortByNoteBookName = [[NSSortDescriptor alloc] initWithKey:@"noteBookName" ascending:YES];
    noteBookArray = [NSMutableArray arrayWithArray:[noteBookArray sortedArrayUsingDescriptors:@[sortByNoteBookName]]];
    
    [self moveGeneralNotebookToFirst];
    
    [noteBookArray insertObject:[NSString stringWithFormat:@"New notebook"] atIndex:0];
    
    [self.collectionView reloadData];
}

-(void)reflectModifiedNoteBookInView:(NoteBook*)modifiedNoteBook inIndexPath:(NSIndexPath*)indexPath{
    
    NoteBookCollectionCell *cell = (NoteBookCollectionCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
    cell.labelTitle.text = modifiedNoteBook.noteBookName;
    
    if ([modifiedNoteBook.isShared boolValue]){
        cell.imageView.image = [UIImage imageNamed:@"icn_notebook-shared.png"];
    }
//    else{
//        cell.imageView.image = [UIImage imageNamed:@"icn_notebook.png"];
//    }
}

-(void)reflectDeletedNoteBookInView:(NSIndexPath*)indexPath{

    [noteBookArray removeObjectAtIndex:indexPath.row];
    [noteBookArray removeObjectAtIndex:0];
    
//    [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
    
    NSSortDescriptor *sortByNoteBookName = [[NSSortDescriptor alloc] initWithKey:@"noteBookName" ascending:YES];
    noteBookArray = [NSMutableArray arrayWithArray:[noteBookArray sortedArrayUsingDescriptors:@[sortByNoteBookName]]];
    
    [self moveGeneralNotebookToFirst];
    
    [noteBookArray insertObject:[NSString stringWithFormat:@"New notebook"] atIndex:0];
    
    [self.collectionView reloadData];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    if ([view isEqual:collectionView]){
        return noteBookArray.count;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)view cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = @"NoteBookCollectionCellDefault";
    
    if (indexPath.row == 0){
        cellIdentifier = @"NoteBookCollectionCellAdd";
    }
    
    if (indexPath.row == 1){
        cellIdentifier = @"NoteBookCollectionCellGeneral";
    }
    
    if ([view isEqual:collectionView]){
        NoteBookCollectionCell *cell = (NoteBookCollectionCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        NoteBook *noteBook = [noteBookArray objectAtIndex:indexPath.row];
        
        cell.layer.borderWidth = 1;
        cell.layer.borderColor = [UIColor colorWithRed:220/255.0 green:232/255.0 blue:242/255.0 alpha:1].CGColor;
        cell.delegate = self;
        
        if (indexPath.row == 0){
            cell.imageView.image = [UIImage imageNamed:@"icn_notebook-add.png"];
            cell.labelTitle.text = NSLocalizedString(@"New Notebook", nil);
            cell.labelNotesCount.text = @"";
            cell.buttonAction.hidden = YES;
            cell.imageViewAdd.hidden = NO;
            cell.imageView.hidden = YES;
        }
        else{
            // Bug #1141
            // jca-04222016
            if (indexPath.row == 1) {
                // Hide action button
                cell.buttonAction.hidden = YES;
            }
            
            cell.imageViewAdd.hidden = YES;
            cell.imageView.hidden = NO;
            
            cell.labelTitle.text = noteBook.noteBookName;
            
            int i = 0;
            for (Note *note in noteBook.notes) {
                if (note.isRemoved.boolValue == NO && note.isArchive.boolValue == NO) {
                    i++;
                }
            }
            
            int notesCount = i;
            
            if (0 < notesCount){
                cell.labelNotesCount.text = [NSString stringWithFormat:@"%d", notesCount];
                cell.labelNotesCount.hidden = NO;
            }
            else{
                cell.labelNotesCount.hidden = YES;
            }

            cell.delegate = self;
            cell.selectedColorId = noteBook.colorId.intValue;
            [cell updateNotebookColor];
            
            if ([noteBook.isShared boolValue]){
                cell.imageView.image = [UIImage imageNamed:@"icn_notebook-shared.png"];
            }
            //else{
            //    cell.imageView.image = [UIImage imageNamed:@"icn_notebook.png"];
            //}
            
        }

        return cell;
    }
    
    return 0;
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize retval = CGSizeMake(175, 175);
    return retval;
}

-(void)collectionView:(UICollectionView *)view didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([collectionView isEqual:collectionView]){
        NoteBookCollectionCell *cell = (NoteBookCollectionCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
        
        cell.addMode = NO;
        
        if (indexPath.row == 0){ //add mode
            NSLog(@"new selected");
            [cell hideEditControls:NO];
            cell.actionTextField.text = @"";
            cell.addMode = YES;
            return;
        }
        
        NoteBook *selectedFolder = [noteBookArray objectAtIndex:indexPath.row];
        self.view.hidden = YES;
        
        if ([self.parentViewController respondsToSelector:@selector(cellFolderApplyTapped:)]){
            [self.parentViewController performSelector:@selector(cellFolderApplyTapped:) withObject:selectedFolder];
        }
    }
}

@end
