//
//  LessonViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/1/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "LessonViewController.h"
#import "LessonTableViewCell.h"
#import "LessonDetailsViewController.h"
#import "LessonPlanDataManager.h"
#import "ActionPopOverViewController.h"
#import "LessonTemplateViewController.h"
#import "LessonActionViewController.h"
#import "CreateLessonPlanView.h"
#import "VSmartValues.h"
#import "TBClassHelper.h"
#import "V_Smart-Swift.h"
#import "HUD.h"

#define ResultsTableView self.searchResultsTableViewController.tableView

@interface LessonViewController ()

<NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, UISearchControllerDelegate, UIAlertViewDelegate, UIDocumentInteractionControllerDelegate, ActionPopOverViewControllerDelegate> {
    
    NSManagedObject *mo_selected;
    UITableView *tableView_active;
    NSString *selectedDownloadedFilePath;
    NSInteger buttonPressed;

}

@property (strong, nonatomic) LessonPlanDataManager *lm;
@property (strong, nonatomic) TBClassHelper *classHelper;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) UITableViewController *searchResultsTableViewController;
@property (strong, nonatomic) UIRefreshControl *tableRefreshControl;
@property (strong, nonatomic) UIPopoverController *actionPopOverController;
@property (strong, nonatomic) UIDocumentInteractionController *documentInteractionController;

@property (strong, nonatomic) IBOutlet UIView *emptyView;
@property (strong, nonatomic) IBOutlet UILabel *emptyMessageLabel;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *lblCourseTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblCourseDescription;
@property (strong, nonatomic) IBOutlet UIButton *butAddLesson;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *descriptionLabelHeight;

@property (strong, nonatomic) NSArray *results;
@property (assign, nonatomic) BOOL isAscending;

@property (strong, nonatomic) NSString *userid;
@property (strong, nonatomic) NSString *courseid;
@property (strong, nonatomic) NSString *courseTitle;
@property (strong, nonatomic) NSString *courseDescription;

@property (strong, nonatomic) NSManagedObject *copiedLessonObject;

@property (assign, nonatomic) NSInteger temporaryLessonCount;

@end

@implementation LessonViewController

static NSString *kLessonCellIdentifier = @"lessonCellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];

    // Use singleton class as data manager
    self.lm = [LessonPlanDataManager sharedInstance];
    self.managedObjectContext = self.lm.mainContext;
    
    // Set user id
    self.userid = [NSString stringWithFormat:@"%@", [self.course_mo valueForKey:@"user_id"]];
    
    // Class helper
    self.classHelper = [[TBClassHelper alloc] init];
    
    // Set protocols for UITableView
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Set initial result to descending order
    self.isAscending = NO;
    
    // Implement a selector for refresh action
    SEL refreshAction = @selector(refreshLessonList);
    
    // Implement refresh controller
    self.tableRefreshControl = [[UIRefreshControl alloc] init];
    [self.tableRefreshControl addTarget:self action:refreshAction forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    
    // Set needed data from course view to variables of lesson view
    self.courseid = [NSString stringWithFormat:@"%@", [self.course_mo valueForKey:@"course_id"]];
    self.courseTitle = [NSString stringWithFormat:@"%@", [self.course_mo valueForKey:@"course_name"]];
    self.courseDescription = [NSString stringWithFormat:@"%@", [self.course_mo valueForKey:@"course_description"]];
    
    self.lblCourseTitle.text = self.courseTitle;
    [self.classHelper justifyLabel:self.lblCourseDescription string:self.courseDescription];
    self.lblCourseDescription.numberOfLines = 0;
    [self updateViewWithDynamicHeight];
    
    // Add an action to add lesson button
    [self.butAddLesson addTarget:self action:@selector(addLessonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // Invoke private methods
    [self setupRightBarButton];
    [self setupSearchCapabilities];
    
    // Lesson Count (1 to avoid loading of empty view on first load)
    self.temporaryLessonCount = 1;
    
    // Empty View
    [self shouldShowEmptyView:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Set navigation bar title
    self.title = NSLocalizedString(@"Lesson List", nil);
    
    // Decorate navigation bar
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    self.navigationController.navigationBar.barTintColor = [UIColor darkGrayColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBarHidden = NO;
    
    // To update table after adding or editing lesson plan
    [self listAllLesson];
    
    // Animate add button
    [self.butAddLesson scaleAnimate:0.5f];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - List Lesson

- (void)listAllLesson {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Loading", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    __weak typeof(self) wo = self;
    
    [self.lm requestLessonPlanListForUser:self.userid courseid:self.courseid dataBlock:^(NSDictionary *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [HUD hideUIBlockingIndicator];
            
            BOOL show = (data == nil) ? YES: NO;
            NSString *message = NSLocalizedString(@"Sorry, there was an error loading this page. Please try again later.", nil);
            [wo showMessage:message show:show completion:^(BOOL okay) {}];
            
            if (data != nil) {
                NSString *lessonCount = [self.lm stringValue:data[@"count"]];
                wo.temporaryLessonCount = [lessonCount integerValue];
                [wo updateCourseTitleWithCount:wo.temporaryLessonCount];
                [wo reloadFetchedResultsController];
            }
        });
    }];
}

- (void)refreshLessonList {
    __weak typeof(self) wo = self;
    
    [self.lm requestLessonPlanListForUser:self.userid courseid:self.courseid dataBlock:^(NSDictionary *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [wo.tableRefreshControl endRefreshing];
            
            BOOL show = (data == nil) ? YES: NO;
            NSString *message = NSLocalizedString(@"Sorry, there was an error loading this page. Please try again later.", nil);
            [wo showMessage:message show:show completion:^(BOOL okay) {}];
            
            if (data != nil) {
                NSString *lessonCount = [self.lm stringValue:data[@"count"]];
                wo.temporaryLessonCount = [lessonCount integerValue];
                [wo updateCourseTitleWithCount:wo.temporaryLessonCount];
                [wo reloadFetchedResultsController];
            }
        });
    }];
}

#pragma mark - Sort Button

- (void)setupRightBarButton {
    UIImage *sortImage = [UIImage imageNamed:@"sort_white48x48.png"];
    UIButton *sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    sortButton.frame = CGRectMake(0, 0, 20, 20);
    sortButton.showsTouchWhenHighlighted = YES;
    
    [sortButton setImage:sortImage forState:UIControlStateNormal];
    [sortButton setImage:sortImage forState:UIControlStateHighlighted];
    [sortButton addTarget:self action:@selector(sortButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sortButton];
}

- (void)sortButtonAction:(id)sender {
    self.isAscending = (self.isAscending) ? NO : YES;
    [self listAllLesson];
}

#pragma mark - Search Bar

- (void)setupSearchCapabilities {
    // Allocate and initialize results
    self.results = [[NSMutableArray alloc] init];
    
    // Init a search controller reusing the current table view controller for results
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    
    // Do not dim and hide the navigation during presentation
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = YES;
    
    // Make an appropriate size for search bar and add it as a header view for initial table view
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // Enable presentation context
    self.definesPresentationContext = YES;
}

#pragma mark - Dynamic Height Updating

- (void)updateViewWithDynamicHeight {
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.25f animations:^{
            wo.descriptionLabelHeight.constant = [wo.classHelper getHeightOfLabel:wo.lblCourseDescription];
            [wo.view layoutIfNeeded];
        }];
    });
}

#pragma mark - Pop Over View

- (void)showPopOverAction:(id)sender {
    // Get managed object through accessing action button position on the table view cell
    mo_selected = [self managedObjectFromButtonAction:sender];
    
    // Identify current status of lesson plan
    NSString *lp_status = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"status"]];
    
    // Instantiate ActionPopOverViewController class
    ActionPopOverViewController *actionPopOverViewController = [[ActionPopOverViewController alloc] initWithNibName:@"ActionPopOverViewController" bundle:nil];
    actionPopOverViewController.delegate = self;
    
    // Implement pop over controller view
    UIButton *button = (UIButton *)sender;
    
    self.actionPopOverController = [[UIPopoverController alloc] initWithContentViewController: actionPopOverViewController];
    self.actionPopOverController.popoverContentSize = CGSizeMake(152.0f, 192.0f);
    
    [self.actionPopOverController presentPopoverFromRect:button.bounds
                                                  inView:button
                                permittedArrowDirections:UIPopoverArrowDirectionAny
                                                animated:YES];
    // Submit lesson plan
    if (![lp_status isEqualToString:@"0"]) {
        // Cannot submit lesson plan for it is for approval or has already been approved
        actionPopOverViewController.butSubmitAction.enabled = NO;
        actionPopOverViewController.butSubmitAction.alpha = 0.30f;
    }
    
    // Download lesson plan
    if (![lp_status isEqualToString:@"2"]) {
        // Cannot download lesson plan for it is not yet approved
        actionPopOverViewController.butDownloadAction.enabled = NO;
        actionPopOverViewController.butDownloadAction.alpha = 0.30f;
    }
}

// Access managed object based on the position of action button from the table view cell
- (NSManagedObject *)managedObjectFromButtonAction:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (void)selectedAction:(NSInteger)action {
    if (action == LPCrudActionTypeEdit) {
        [self editLessonAction:nil];
    }
    
    if (action == LPCrudActionTypeSubmit) {
        [self submitLessonAction:nil];
    }
    
    if (action == LPCrudActionTypeDownload) {
        [self downloadLessonAction:nil];
    }
    
    if (action == LPCrudActionTypeDelete) {
        [self deleteLessonAction:nil];
    }
}

#pragma mark - Implementation of CRUD Methods

- (void)addLessonAction:(id)sender {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Please wait", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    __weak typeof(self) wo = self;
    
    [self.lm requestLessonTemplateList:^(BOOL status) {
        if (status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [wo performSegueWithIdentifier:@"showLessonTemplates" sender:nil];
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                NSString *message = NSLocalizedString(@"You cannot create a new lesson plan this time. Please try again later.", nil);
                [wo showMessage:message show:YES completion:^(BOOL okay) {}];
            });
        }
    }];
}

- (void)editLessonAction:(id)sender {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Please wait", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    NSString *lpid = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"id"]];
    NSString *templateid = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"template_id"]];
    
    __weak typeof(self) wo = self;

    [self.lm requestLessonTemplateForLessonTemplateWithID:templateid doneBlock:^(BOOL status) {
        if (status) {
            [self.lm requestLessonPlanDetailsForLessonPlanWithID:lpid doneBlock:^(BOOL status) {
                if (status) {
                    NSPredicate *predicate = [self predicateForKeyPath:@"lp_id" value:lpid];
                    NSManagedObject *lesson_object = [self.lm getEntity:kLessonPlanDetailsEntity withPredicate:predicate];
                    NSDictionary *data = @{@"lp_id":lpid};
                    
                    [self.lm prepareLessonForAction:LPActionTypeUpdate copyObject:lesson_object additionalData:data objectBlock:^(NSManagedObject *object) {
                        if (object != nil) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [HUD hideUIBlockingIndicator];
                                wo.copiedLessonObject = object;
                                [wo performSegueWithIdentifier:@"showCreateLessonView" sender:nil];
                            });
                        }
                        else {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [HUD hideUIBlockingIndicator];
                                NSString *message = NSLocalizedString(@"You cannot edit this lesson plan this time. Please try again later.", nil);
                                [wo showMessage:message show:YES completion:^(BOOL okay) {}];
                            });
                        }
                    }];
                }
                else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [HUD hideUIBlockingIndicator];
                        NSString *message = NSLocalizedString(@"You cannot edit this lesson plan this time. Please try again later.", nil);
                        [wo showMessage:message show:YES completion:^(BOOL okay) {}];
                    });
                }
            }];
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                NSString *message = NSLocalizedString(@"You cannot edit this lesson plan this time. Please try again later.", nil);
                [wo showMessage:message show:YES completion:^(BOOL okay) {}];
            });
        }
    }];
}

- (void)submitLessonAction:(id)sender {
    buttonPressed = 1;
    
    NSString *lesson_name = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"name"]];
    NSString *message_string = NSLocalizedString(@"Are you sure you want to submit", nil);
    NSString *message = [NSString stringWithFormat:@"%@ %@?", message_string, lesson_name];
    NSString *yesTitle = NSLocalizedString(@"Yes", nil);
    NSString *noTitle = NSLocalizedString(@"No", nil);
    NSString *avTitle = NSLocalizedString(@"Lesson Plan for Approval", nil);
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:avTitle
                                                 message:message
                                                delegate:self
                                       cancelButtonTitle:noTitle
                                       otherButtonTitles:yesTitle, nil];
    [av show];
}

- (void)downloadLessonAction:(id)sender {
    buttonPressed = 2;
    
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Downloading", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    NSString *lpid = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"id"]];
    [self.lm requestDownloadPDFFileWithLessonPlanID:lpid contentBlock:^(NSArray *content) {
        if (content) {
            [self downloadFileWithFilePath:content[0] andFilename:content[1]];
        }
        else {
            __weak typeof (self) wo = self;
            dispatch_async (dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                NSString *message = NSLocalizedString(@"Sorry, there was an error downloading this lesson plan. Please try again later.", nil);
                [wo showMessage:message show:YES completion:^(BOOL okay) {}];
            });
        }
    }];
}

- (void)deleteLessonAction:(id)sender {
    buttonPressed = 3;

    NSString *lesson_name = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"name"]];
    NSString *message_string = NSLocalizedString(@"Are you sure you want to delete", nil);
    NSString *message = [NSString stringWithFormat:@"%@ %@?", message_string, lesson_name];
    NSString *yesTitle = NSLocalizedString(@"Yes", nil);
    NSString *noTitle = NSLocalizedString(@"No", nil);
    NSString *avTitle = NSLocalizedString(@"Delete", nil);
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:avTitle
                                                 message:message
                                                delegate:self
                                       cancelButtonTitle:noTitle
                                       otherButtonTitles:yesTitle, nil];
    [av show];
}

#pragma mark - File Download

- (void)downloadFileWithFilePath:(NSString *)urlFilePath andFilename:(NSString *)filename {
    // Download the file in a seperate thread
    dispatch_async(dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *url = [NSURL URLWithString:urlFilePath];
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        if (data) {
            // Access documents directory
            NSArray *directoryPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [directoryPaths objectAtIndex:0];
            
            // Name of subdirectory
            NSError *error;
            NSString *documentsSubdirectory = [documentsDirectory stringByAppendingPathComponent:@"/lessonplan"];
            
            // Create subdirectory if it does not exist
            if (![[NSFileManager defaultManager] fileExistsAtPath:documentsSubdirectory]) {
                // Create subdirectory in documents directory
                [[NSFileManager defaultManager] createDirectoryAtPath:documentsSubdirectory
                                          withIntermediateDirectories:NO
                                                           attributes:nil
                                                                error:&error];
            }
            
            // File to save in created subdirectory
            NSString *localFilePath = [NSString stringWithFormat:@"%@/%@", documentsSubdirectory, filename];
            
            // Saving is done on main thread
            __weak typeof (self) wo = self;
            dispatch_async (dispatch_get_main_queue(), ^{
                [data writeToFile:localFilePath atomically:YES];
                [HUD hideUIBlockingIndicator];
                [wo showDownloadConfirmationMessage:localFilePath filename:filename];
            });
        }
        else {
            __weak typeof (self) wo = self;
            dispatch_async (dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                NSString *message = NSLocalizedString(@"Sorry, there was an error downloading this lesson plan. Please try again later.", nil);
                [wo showMessage:message show:YES completion:^(BOOL okay) {}];
            });
        }
    });
}

- (void)showDownloadConfirmationMessage:(NSString *)filepath filename:(NSString *)filename {
    selectedDownloadedFilePath = filepath;
    
    NSString *avTitle = NSLocalizedString(@"Lesson Plan Downloaded", nil);
    NSString *openTitle = NSLocalizedString(@"Open", nil);
    NSString *cancelTitle = NSLocalizedString(@"Cancel", nil);
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:avTitle
                                                 message:filename
                                                delegate:self
                                       cancelButtonTitle:cancelTitle
                                       otherButtonTitles:openTitle, nil];
    [av show];
}

#pragma mark - Alert View Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (buttonPressed) {
        case 1:
            if (buttonIndex == 1) {
                NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Submitting", nil)];
                [HUD showUIBlockingIndicatorWithText:indicatorString];

                __weak typeof (self) wo = self;
                
                [self.lm requestUpdateLessonPlanStatus:mo_selected doneBlock:^(BOOL status) {
                    dispatch_async (dispatch_get_main_queue(), ^{
                        [HUD hideUIBlockingIndicator];
                        
                        NSString *posMessage = NSLocalizedString(@"Lesson plan was successfully submitted.", nil);
                        NSString *negMessage = NSLocalizedString(@"Sorry, there was an error submitting this lesson plan. Please try again later.", nil);
                        NSString *finmessage = (status) ? posMessage : negMessage;
                        
                        [wo showMessage:finmessage show:YES completion:^(BOOL okay) {
                            if (status) {
                                [wo refreshLessonList];
                            }
                        }];
                    });
                }];
            }
            break;
        case 2:
            if (buttonIndex == 1) {
                [self openFileWithFilePath:selectedDownloadedFilePath];
            }
            break;
        case 3:
            if (buttonIndex == 1) {
                NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Deleting", nil)];
                [HUD showUIBlockingIndicatorWithText:indicatorString];
                
                __weak typeof (self) wo = self;
                
                [self.lm requestRemoveLessonPlan:mo_selected doneBlock:^(BOOL status) {
                    dispatch_async (dispatch_get_main_queue(), ^{
                        [HUD hideUIBlockingIndicator];
                        
                        NSString *posMessage = NSLocalizedString(@"Lesson plan was successfully deleted.", nil);
                        NSString *negMessage = NSLocalizedString(@"Sorry, there was an error deleting this lesson plan. Please try again later.", nil);
                        NSString *finmessage = (status) ? posMessage : negMessage;
                        
                        [wo showMessage:finmessage show:YES completion:^(BOOL okay) {
                            if (status) {
                                [wo refreshLessonList];
                            }
                        }];
                    });
                }];
            }
            break;
        default:
            break;
    }
}

- (void)showMessage:(NSString *)message show:(BOOL)show completion:(void (^)(BOOL okay))response {
    if (show) {
        NSString *butOkay = NSLocalizedString(@"Okay", nil);
       
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *theAction = [UIAlertAction actionWithTitle:butOkay
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                               response(YES);
                                                          }];
        [alert addAction:theAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - Open File Using Document Interaction Controller

- (void)openFileWithFilePath:(NSString *)filepath {
    NSURL *url = [NSURL fileURLWithPath:filepath];
    
    if (url) {
        // Initialize Document Interaction Controller
        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        
        // Configure Document Interaction Controller
        self.documentInteractionController.delegate = self;
        
        // Open PDF File
        CGRect navigationRect = self.navigationController.navigationBar.frame;
        [self.documentInteractionController presentOptionsMenuFromRect:navigationRect inView:self.view animated:YES];
    }
    else {
        NSString *message = NSLocalizedString(@"Cannot open the file.", nil);
        [self showMessage:message show:YES completion:^(BOOL okay) {}];
    }
}

#pragma mark - Document Interaction Controller Delegate

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return self;
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller {
    return self.view.frame;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller {
    return self.view;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = 1;
    
    if ([tableView isEqual:ResultsTableView]) {
        return count;
    }
    else {
        return [[self.fetchedResultsController sections] count];
    }
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    
    if ([tableView isEqual:ResultsTableView]) {
        if (self.results) {
            count = self.results.count;
        }
    }
    else {
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        count = [sectionInfo numberOfObjects];
    }
    
    // NOTE: REMOVE IF PAGINATED
    [self shouldShowEmptyView:(count > 0) ? NO : YES];
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView_active = tableView;
    
    LessonTableViewCell *lessonContentCell = [tableView dequeueReusableCellWithIdentifier:kLessonCellIdentifier forIndexPath:indexPath];
    
    if ([tableView isEqual:ResultsTableView]) {
        [self configureFilteredCell:lessonContentCell indexPath:indexPath];
    }
    
    if (![tableView isEqual:ResultsTableView]) {
        [self configureCell:lessonContentCell atIndexPath:indexPath];
    }
    
    return lessonContentCell;
}

- (void)configureFilteredCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = self.results[indexPath.row];
    LessonTableViewCell *lessonContentCell = (LessonTableViewCell *)cell;
    [self configureCell:lessonContentCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    LessonTableViewCell *lessonContentCell = (LessonTableViewCell *)cell;
    [self configureCell:lessonContentCell managedObject:mo objectAtIndexPath:indexPath];
}

- (void)configureCell:(LessonTableViewCell *)cell managedObject:(NSManagedObject *)mo objectAtIndexPath:(NSIndexPath *)indexPath  {
    NSString *lessonTitle = [self.lm stringValue:[mo valueForKey:@"name"]];
    NSString *lessonStatus = [self.lm stringValue:[mo valueForKey:@"status"]];
    NSString *lessonStartDate = [self.lm stringValue:[mo valueForKey:@"start_date"]];
    NSString *lessonEndDate = [self.lm stringValue:[mo valueForKey:@"end_date"]];
    NSString *lessonSchoolName = [self.lm stringValue:[mo valueForKey:@"school_name"]];
    NSString *capTimeFrame = NSLocalizedString(@"Time Frame", nil);
    NSString *capSchoolName = NSLocalizedString(@"School Name", nil);

    if ([lessonStatus isEqualToString:@"0"]) {
        lessonStatus = NSLocalizedString(@"Draft", nil);;
    }
    else if ([lessonStatus isEqualToString:@"1"]) {
        lessonStatus = NSLocalizedString(@"for Approval", nil);
    }
    else if ([lessonStatus isEqualToString:@"2"])  {
        lessonStatus = NSLocalizedString(@"Approved", nil);
    }
    
    cell.lblLessonTitle.text = lessonTitle;
    cell.lblLessonStatus.text = lessonStatus;
    cell.lblLessonTimeFrame.text = [NSString stringWithFormat:@"%@: %@ %@ %@", capTimeFrame, lessonStartDate, NSLocalizedString(@"to", nil), lessonEndDate];
    cell.lblLessonSchoolName.text = [NSString stringWithFormat:@"%@: %@", capSchoolName, lessonSchoolName];
    [cell.butPopOverAction addTarget:self
                              action:@selector(showPopOverAction:)
                    forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Please wait", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];

    NSManagedObject *mo = nil;
    
    if ([tableView isEqual:ResultsTableView]) {
        mo = self.results[indexPath.row];
    }
    
    if (![tableView isEqual:ResultsTableView]) {
        mo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    mo_selected = mo;
    tableView_active = tableView;
    
    NSString *lesson_id = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"id"]];
    NSString *templateid = [NSString stringWithFormat:@"%@", [mo_selected valueForKey:@"template_id"]];
    
    __weak typeof(self) wo = self;
    
    [self.lm requestLessonTemplateForLessonTemplateWithID:templateid doneBlock:^(BOOL status) {
        if (status) {
            [self.lm requestLessonPlanDetailsForLessonPlanWithID:lesson_id doneBlock:^(BOOL status) {
                    if (status) {
                    NSPredicate *predicate = [self predicateForKeyPath:@"lp_id" value:lesson_id];
                    NSManagedObject *lesson_object = [self.lm getEntity:kLessonPlanDetailsEntity withPredicate:predicate];
                    NSDictionary *data = @{@"lp_id":lesson_id};
                    
                    [self.lm prepareLessonForAction:LPActionTypeUpdate copyObject:lesson_object additionalData:data objectBlock:^(NSManagedObject *object) {
                        if (object != nil) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [HUD hideUIBlockingIndicator];
                                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                                
                                wo.copiedLessonObject = object;
                                [wo performSegueWithIdentifier:@"showLessonDetails" sender:nil];
                            });
                        }
                        else {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [HUD hideUIBlockingIndicator];
                                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                                
                                NSString *message = NSLocalizedString(@"You cannot access this lesson plan right now. Please try again later.", nil);
                                [wo showMessage:message show:YES completion:^(BOOL okay) {}];
                            });
                        }
                    }];
                }
                else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [HUD hideUIBlockingIndicator];
                        [tableView deselectRowAtIndexPath:indexPath animated:YES];
                        
                        NSString *message = NSLocalizedString(@"You cannot access this lesson plan right now. Please try again later.", nil);
                        [wo showMessage:message show:YES completion:^(BOOL okay) {}];
                    });
                }
            }];
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [HUD hideUIBlockingIndicator];
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                
                NSString *message = NSLocalizedString(@"You cannot access this lesson plan right now. Please try again later.", nil);
                [wo showMessage:message show:YES completion:^(BOOL okay) {}];
            });
        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showLessonDetails"]) {
        LessonDetailsViewController *lessonDetailsView = (LessonDetailsViewController *)[segue destinationViewController];
        lessonDetailsView.course_mo = self.course_mo;
        lessonDetailsView.lesson_mo = mo_selected;
        lessonDetailsView.lesson_details_mo = self.copiedLessonObject;
    }
    
    if ([segue.identifier isEqualToString:@"showLessonTemplates"]) {
        LessonTemplateViewController *lessonTemplateView = (LessonTemplateViewController *)[segue destinationViewController];
        NSString *courseid = [NSString stringWithFormat:@"%@", [self.course_mo valueForKey:@"course_id"]];
        NSString *gradelevel = [NSString stringWithFormat:@"%@", [self.course_mo valueForKey:@"grade_level"]];

        lessonTemplateView.courseid = courseid;
        lessonTemplateView.gradeLevel = gradelevel;
    }
    
    if ([segue.identifier isEqualToString:@"showCreateLessonView"]) {
        CreateLessonPlanView *createLessonPlanView = (CreateLessonPlanView *)[segue destinationViewController];
        createLessonPlanView.actionType = LPActionTypeUpdate;
        createLessonPlanView.copiedLessonObject = self.copiedLessonObject;
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self updateViewWithDynamicHeight];
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kLessonPlanEntity];
    [fetchRequest setFetchBatchSize:10];
    
    NSCompoundPredicate *cp;
    
    // Like Predicate
    NSPredicate *predicate1 = [self predicateForKeyPath:@"user_id"  value:self.userid];
    NSPredicate *predicate2 = [self predicateForKeyPath:@"course_id" value:self.courseid];
    NSPredicate *predicate3 = [self predicateForKeyPath:@"is_deleted" value:@"0"];
    
    // Contains Predicate
    NSPredicate *predicate4 = [self predicateForKeyPathContains:@"name" value:self.searchController.searchBar.text];
    
    if (self.searchController.active == NO) {
        cp = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2, predicate3]];
    }
    else {
        cp = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2, predicate3, predicate4]];
    }
    
    fetchRequest.predicate = cp;
    
    NSSortDescriptor *sorted_date_modified = [NSSortDescriptor sortDescriptorWithKey:@"sorted_date_modified" ascending:self.isAscending];
    [fetchRequest setSortDescriptors:@[sorted_date_modified]];
    
    NSFetchedResultsController *frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                          managedObjectContext:self.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSLikePredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - Reloading of Data

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    [self reloadFetchedResultsController];
}

- (void)reloadSearchResults {
    NSArray *items = [NSArray arrayWithArray:self.results];
    
    // Edit the sort key as appropriate
    NSSortDescriptor *sorted_date_modified = [NSSortDescriptor sortDescriptorWithKey:@"sorted_date_modified" ascending:self.isAscending];
    NSArray *sorted = [items sortedArrayUsingDescriptors:@[sorted_date_modified]];
    
    // Set up results
    self.results = [NSArray arrayWithArray:sorted];
    
    // Reload search table view
    [tableView_active reloadData];
}

- (void)reloadFetchedResultsController {
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
    
    NSError *err = nil;
    [self.fetchedResultsController performFetch:&err];
    
    if (err) {
        NSLog(@"fetched error : %@", [err localizedDescription]);
    }
}

#pragma mark - Custom Empty View

- (void)shouldShowEmptyView:(BOOL)show {
    // NOTE: REFACTOR IF PAGINATED
    if ([self.searchController.searchBar.text isEqualToString:@""]) {
        show = (self.temporaryLessonCount > 0) ? NO : YES;
    }
    
    if (show) {
        NSString *message = NSLocalizedString(@"You have not created any lesson plan yet.", nil);
        
        if (![self.searchController.searchBar.text isEqualToString:@""]) {
            message = NSLocalizedString(@"No results found for", nil);
            message =[NSString stringWithFormat:@"%@ \"%@\"", message, self.searchController.searchBar.text];
        }
        
        self.emptyMessageLabel.text = message;
    }
    
    self.emptyView.hidden = !show;
    self.tableView.hidden = show;
}

- (void)updateCourseTitleWithCount:(NSInteger)count {
    self.lblCourseTitle.text = [NSString stringWithFormat:@"%@ (%zd)", self.courseTitle, count];
}

@end
