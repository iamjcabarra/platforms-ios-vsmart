//
//  LPMCommentModalController.swift
//  V-Smart
//
//  Created by Julius Abarra on 04/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMCommentModalController: UIViewController {
    
    @IBOutlet fileprivate var modalTitleLabel: UILabel!
    @IBOutlet fileprivate var commentText: UITextView!
    @IBOutlet fileprivate var closeButton: UIButton!
    @IBOutlet fileprivate var sendCommentButton: UIButton!
    
    var selectedCommentObject: NSManagedObject!
    
    // MARK: - Data Manager
    
    fileprivate lazy var lessonPlanDataManager: LessonPlanDataManager = {
        let lpdm = LessonPlanDataManager.sharedInstance()
        return lpdm!
    }()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.modalTitleLabel.text = NSLocalizedString("Comment", comment: "")
        self.commentText.text = self.selectedCommentObject.value(forKey: "comment") as! String
        
        let sendCommentButtonAction = #selector(self.sendCommentButtonAction(_:))
        self.sendCommentButton.addTarget(self, action: sendCommentButtonAction, for: .touchUpInside)
        
        let closeButtonAction = #selector(self.closeButtonAction(_:))
        self.closeButton.addTarget(self, action: closeButtonAction, for: .touchUpInside)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.preferredContentSize = CGSize(width: 600, height: 220)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Button Event Handlers
    
    func sendCommentButtonAction(_ sender: UIButton) {
        let comment = self.commentText.text
        
        if (comment != "") {
            let indicatorString = "\(NSLocalizedString("Updating", comment: ""))..."
            HUD.showUIBlockingIndicator(withText: indicatorString)
            
            self.lessonPlanDataManager.requestUpdateLessonPlanComment(self.selectedCommentObject, comment: comment, doneBlock: { (success) in
                if (success) {
                    let lpid = self.selectedCommentObject.value(forKey: "lp_id") as! String
                    
                    self.lessonPlanDataManager.requestLessonPlanDetailsForLessonPlan(withID: lpid, doneBlock: { (success) in
                        DispatchQueue.main.async(execute: {
                            HUD.hideUIBlockingIndicator()
                            
                            if (success) {
                                self.dismiss(animated: true, completion: nil)
                            }
                            else {
                                let message = NSLocalizedString("There was an error updating this comment. Please try again later.", comment: "")
                                self.showNotificationMessage(message)
                            }
                        })
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        HUD.hideUIBlockingIndicator()
                        let message = NSLocalizedString("There was an error updating this comment. Please try again later.", comment: "")
                        self.showNotificationMessage(message)
                    })
                }

            })
        }
    }
    
    func closeButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- Alert Message View
    
    func showNotificationMessage(_ message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: NSLocalizedString("Okay", comment: ""), style: .cancel) { (Alert) -> Void in
            DispatchQueue.main.async(execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}
