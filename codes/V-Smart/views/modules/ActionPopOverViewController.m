//
//  ActionPopOverViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/22/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "ActionPopOverViewController.h"

@interface ActionPopOverViewController ()

@end

@implementation ActionPopOverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
    [self.butEditAction addTarget:self
                           action:@selector(editButtonAction:)
                 forControlEvents:UIControlEventTouchUpInside];
    
    [self.butSubmitAction addTarget:self
                             action:@selector(submitButtonAction:)
                   forControlEvents:UIControlEventTouchUpInside];
    
    [self.butDownloadAction addTarget:self
                               action:@selector(downloadButtonAction:)
                     forControlEvents:UIControlEventTouchUpInside];
    
    [self.butDeleteAction addTarget:self
                             action:@selector(deleteButtonAction:)
                   forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)editButtonAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
        [self.delegate selectedAction:LPCrudActionTypeEdit];
    }];
}

- (void)submitButtonAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
        [self.delegate selectedAction:LPCrudActionTypeSubmit];
    }];
}

- (void)downloadButtonAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
        [self.delegate selectedAction:LPCrudActionTypeDownload];
    }];
}

- (void)deleteButtonAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
        [self.delegate selectedAction:LPCrudActionTypeDelete];
    }];
}

@end
