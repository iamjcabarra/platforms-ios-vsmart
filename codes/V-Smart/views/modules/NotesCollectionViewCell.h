//
//  NotesCollectionViewCell.h
//  V-Smart
//
//  Created by VhaL on 2/12/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotesCollectionViewCell : UICollectionViewCell <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UITextView *textDescription;
@property (nonatomic, weak) IBOutlet UIView *textViewBackground;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) BOOL isArchived;
@property (nonatomic, strong) NSArray *optionArray;

-(IBAction)buttonActionTapped:(id)sender;
-(void)initializeCell;
@end
