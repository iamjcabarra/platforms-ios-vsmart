//
//  TextbookViewController.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/28/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "AQGridView.h"
#import "BookInfoViewController.h"
#import "EPub.h"
#import "UIViewController+Popup.h"
#import "EPubViewController.h"
#import "QuizMedalViewController.h"

@class EPubViewController;

@interface TextbookViewController : BaseViewController<AQGridViewDelegate, AQGridViewDataSource, UIGestureRecognizerDelegate, BookInfoViewControllerDelegate, QuizMedalViewControllerDelegate, UIDocumentInteractionControllerDelegate>
{
    MBProgressHUD *mbHUD;
    
    QuizMedalViewController *popQuizMedalViewController;
    BookInfoViewController *popBookInfoViewController;
    EPubViewController *detailViewController;
}

@property (nonatomic, retain) AQGridView * gridView;
@property (nonatomic, strong) NSMutableArray *books;
@end
