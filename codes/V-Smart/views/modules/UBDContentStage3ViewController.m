//
//  UBDContentStage3ViewController.m
//  V-Smart
//
//  Created by Julius Abarra on 9/25/15.
//  Copyright © 2015 Vibe Technologies. All rights reserved.
//

#import "UBDContentStage3ViewController.h"
#import "LessonViewController.h"
#import "UBDContentManager.h"
#import "AccountInfo.h"
#import "Utils.h"

@interface UBDContentStage3ViewController () <UITextViewDelegate>

@property (strong, nonatomic) UBDContentManager *sharedUBDContentManager;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UILabel *lblContentTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDay1;
@property (strong, nonatomic) IBOutlet UILabel *lblDay2;
@property (strong, nonatomic) IBOutlet UILabel *lblDay3;
@property (strong, nonatomic) IBOutlet UILabel *lblDay4;
@property (strong, nonatomic) IBOutlet UILabel *lblDay5;

@property (strong, nonatomic) IBOutlet UITextView *txtDay1;
@property (strong, nonatomic) IBOutlet UITextView *txtDay2;
@property (strong, nonatomic) IBOutlet UITextView *txtDay3;
@property (strong, nonatomic) IBOutlet UITextView *txtDay4;
@property (strong, nonatomic) IBOutlet UITextView *txtDay5;

@property (strong, nonatomic) NSString *userid;
@property (strong, nonatomic) NSString *actionType;

@end

@implementation UBDContentStage3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Use singleton class shared instance
    self.sharedUBDContentManager = [UBDContentManager sharedInstance];
    
    // To dismiss keyboard when touching outside of text fields
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    // Set delegates
    self.txtDay1.delegate = self;
    self.txtDay2.delegate = self;
    self.txtDay3.delegate = self;
    self.txtDay4.delegate = self;
    self.txtDay5.delegate = self;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self updateContentManager];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Get data from singleton class and render them to text fields
    self.txtDay1.text = [self.sharedUBDContentManager.contentStage objectForKey:@"16"];
    self.txtDay2.text = [self.sharedUBDContentManager.contentStage objectForKey:@"17"];
    self.txtDay3.text = [self.sharedUBDContentManager.contentStage objectForKey:@"21"];
    self.txtDay4.text = [self.sharedUBDContentManager.contentStage objectForKey:@"22"];
    self.txtDay5.text = [self.sharedUBDContentManager.contentStage objectForKey:@"23"];
    
    // Keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Keyboard notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Handle Keyboard Movements

- (void)keyboardWillShow:(NSNotification *)notification {
    if ([self.txtDay5 isFirstResponder]) {
        CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        float newVerticalPosition = -keyboardSize.height;
        [self moveFrameToVerticalPosition:newVerticalPosition forDuration:0.3f];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [self moveFrameToVerticalPosition:0.0f forDuration:0.3f];
}

- (void)moveFrameToVerticalPosition:(float)position forDuration:(float)duration {
    CGRect frame = self.view.frame;
    frame.origin.y = position;
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Update Content Manager

- (void)updateContentManager {
    // Get input data from text fields
    NSString *day1 = self.txtDay1.text;
    NSString *day2 = self.txtDay2.text;
    NSString *day3 = self.txtDay3.text;
    NSString *day4 = self.txtDay4.text;
    NSString *day5 = self.txtDay5.text;
    
    // Set data to singleton class collection property
    [self.sharedUBDContentManager.contentStage setObject:day1 forKey:@"16"];
    [self.sharedUBDContentManager.contentStage setObject:day2 forKey:@"17"];
    [self.sharedUBDContentManager.contentStage setObject:day3 forKey:@"21"];
    [self.sharedUBDContentManager.contentStage setObject:day4 forKey:@"22"];
    [self.sharedUBDContentManager.contentStage setObject:day5 forKey:@"23"];
}

#pragma mark - Text View Delegate

- (void)textViewDidChange:(UITextView *)textView {
    [self updateContentManager];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self moveFrameToVerticalPosition:0.0f forDuration:0.3f];
}

@end
