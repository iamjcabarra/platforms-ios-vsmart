//
//  LPMFileCollectionViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 28/06/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import UIKit

class LPMFileCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var fileIcon: UIImageView!
    @IBOutlet var fileName: UILabel!
    
    fileprivate let extensions = ["xml": "icn_xml.png", "xlsx": "icn_xlsx.png", "xls": "icn_xls.png", "txt": "icn_txt.png",
                              "text": "icn_txt.png", "tiff": "icn_tiff.png", "rtf": "icn_rtf.png", "pptx": "icn_pptx.png",
                              "ppt": "icn_ppt.png", "png": "icn_png.png", "pdf": "icn_pdf.png", "mp4": "icn_mp4.png",
                              "mp3": "icn_mp3.png", "jpg": "icn_jpg.png", "jpeg": "icn_jpg.png", "html": "icn_html.png",
                              "gif": "icn_gif.png", "docx": "icn_docx.png", "doc": "icn_doc.png"]
    
    func highlightSelectedCell(_ highlight: Bool) {
        self.contentView.backgroundColor = highlight ? UIColor(rgba: "#EFEFF4") : UIColor(rgba: "#FFFFFF")
    }
    
    func setIconOfFileWithExtension(_ ext: String) {
        if let imageName = self.extensions[ext] {
            self.fileIcon.image = UIImage(named: imageName)
        }
        else {
            self.fileIcon.image = UIImage(named: "unknown-file-05.png")
        }
    }
    
}
