//
//  AboutViewController.m
//  V-Smart
//
//  Created by Ryan Migallos on 6/5/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController () <UIPopoverControllerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *labelVersion;
@property (strong, nonatomic) IBOutlet UILabel *labelBuild;
@property (strong, nonatomic) IBOutlet UILabel *labelCopyright;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.logoImageView.image = [UIImage imageNamed:@"Icon-76"];
    self.titleLabel.text = @"VSmart";
    self.labelVersion.text = @"Version -  Mariposa, 2.5.4";
    self.labelBuild.text = @"Build Date - November 23, 2017";
    self.labelCopyright.text = @"2017 Vibe Technologies Inc. \nAll Rights Reserved.";
    
    [self.closeButton addTarget:self action:@selector(closeActions:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)viewWillLayoutSubviews{
//    [super viewWillLayoutSubviews];
//    self.view.superview.bounds = CGRectMake(0, 0, 300, 300);
//}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.preferredContentSize = CGSizeMake(300, 300);
}

- (void)closeActions:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
