//
//  ImportViewController.h
//  Booklatan
//
//  Created by Earljon Hidalgo on 5/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImportViewController : UIViewController
{

}

@property (retain, nonatomic) IBOutlet UILabel *uuidLabel;
@property (retain, nonatomic) IBOutlet UILabel *accountLabel;

-(IBAction)closeView:(id)sender;
@end
