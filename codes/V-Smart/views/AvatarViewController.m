//
//  AvatarViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/21/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "AvatarViewController.h"
#import "PhotoGridViewCell.h"
#import "YKImageCropperViewController.h"
#import "JMAAvatar.h"
#import "AppDelegate.h"

@interface AvatarViewController ()
{
    PhotoGridViewCell *selectedCell;
    BOOL sourceIsPhoto;
}
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) UIImage *photo;
@property (nonatomic, strong) UIImage *cameraPhoto;
@property (nonatomic, strong) JMARecordAvatars *avatars;
@end

@implementation AvatarViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	self.gridView.autoresizesSubviews = YES;
	self.gridView.delegate = self;
	self.gridView.dataSource = self;
    
    sourceIsPhoto = NO;
    _photo = nil;
    
    [self _getOnlineAvatars];
    [self _refreshRecentPhotos];
}


-(void) _getOnlineAvatars{
    
    JSONModelError *jsonModelError;
    
    NSString *url = [Utils buildUrl:kEndPointGetGenericAvatars];
    
    NSData *responseData = [JSONHTTPClient syncRequestDataFromURL:[NSURL URLWithString:url] method:kHTTPMethodGET requestBody:nil headers:nil etag:nil error:&jsonModelError];
    
    //    NSString *jsonRaw = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    if (responseData == nil) {
        return;
    }
    
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    
    if (jsonModelError == nil){
        
        NSArray *array = [dict objectForKey:@"records"];
        
        if (array){
            NSDictionary *dictJSONP = @{@"records": array};

            self.avatars = [[JMARecordAvatars alloc] initWithDictionary:dictJSONP error:&error];
            
            
            
//            NSError *parseError = nil;
//            NSData *records = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&parseError];
//            jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
        }
    }
    else{
        VLog("%@", jsonModelError);
    }
}

-(void) _checkPhotoState {
    [self.uploadButton setEnabled:_photo != nil];
}

-(void) _refreshRecentPhotos {
    NSMutableArray *combinedImages = [[NSMutableArray alloc] initWithArray:[Utils recentlyUsedImages]];
    
    // BUG FIX
    for (JMAAvatar *avatar in self.avatars.records) {
        NSString *avatarUrl = [NSString stringWithFormat:@"/%@", avatar.avatarUrl];
        NSString *url = [Utils buildUrl:avatarUrl];
        [combinedImages addObject:url];
    }

    _items = [[NSArray alloc] initWithArray:combinedImages];
    [self.gridView reloadData];
    [self _checkPhotoState];
}

#pragma mark -
#pragma mark Grid View Data Source

- (NSUInteger) numberOfItemsInGridView: (AQGridView *) aGridView
{
    return ( [_items count] );
}

- (AQGridViewCell *) gridView: (AQGridView *) aGridView cellForItemAtIndex: (NSUInteger) index
{
    static NSString * PhotoCellIdentifier = @"PhotoGridCellIdentifier";
    
    AQGridViewCell * cell = nil;

    PhotoGridViewCell * photoCell = (PhotoGridViewCell *)[aGridView dequeueReusableCellWithIdentifier: PhotoCellIdentifier];
    if ( photoCell == nil )
    {
        photoCell = [[PhotoGridViewCell alloc] initWithFrame: CGRectMake(0.0, 0.0, 130.0, 150.0)
                                                 reuseIdentifier: PhotoCellIdentifier];
        photoCell.selectionGlowColor = UIColorFromHex(0x375493);
    }
    
    //photoCell.contentView.backgroundColor = UIColorFromHex(0x375493);
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^{
        
        NSString *imagePath = [_items objectAtIndex: index];
        UIImage *image;
        if ([imagePath rangeOfString:[Utils buildUrl:@""]].location == NSNotFound)
            image = [UIImage imageWithContentsOfFile:imagePath];
        else
            image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imagePath]]];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            photoCell.image = image;
        });
    });
    
    //photoCell.image = [UIImage imageWithContentsOfFile:[_items objectAtIndex: index]];
    cell = photoCell;
    return ( cell );
}

- (CGSize) portraitGridCellSizeForGridView: (AQGridView *) aGridView
{
    return ( CGSizeMake(150.0, 150.0) );
}

-(void) gridView:(AQGridView *)gridView didSelectItemAtIndex:(NSUInteger)index {
    VLog(@"Selected");
    
    PhotoGridViewCell *cell = (PhotoGridViewCell *)[gridView cellForItemAtIndex:index];
    _photo = cell.image;
    
    if (selectedCell) {
        selectedCell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    cell.contentView.backgroundColor = UIColorFromHex(0xcaccd3);
    selectedCell = cell;
    [self _checkPhotoState];
}

-(void) gridView: (AQGridView *) gridView didDeselectItemAtIndex: (NSUInteger) index {
    VLog(@"deSelected");
    _photo = nil;
    selectedCell.contentView.backgroundColor = [UIColor whiteColor];
    [self _checkPhotoState];
}

-(void) showImageCropper {
    YKImageCropperViewController *vc = [[YKImageCropperViewController alloc] initWithImage:self.cameraPhoto];
    vc.cancelHandler = ^() {
        NSLog(@"* Cancel");
        [self dismissViewControllerAnimated:YES completion:nil];
    };
    vc.doneHandler = ^(UIImage *editedImage) {
        NSLog(@"* Done");
        
        NSString *filePath2 = [NSString stringWithFormat:@"%@/%@.jpg", [Utils userPhotoDirectory], [Utils randomNumber]];
        NSData *imageData = UIImageJPEGRepresentation(editedImage, 1.0);
        [imageData writeToFile:filePath2 atomically:NO];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        [self _refreshRecentPhotos];
    };
    
    UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:vc];
    nvc.navigationBar.barStyle = UIBarStyleDefault;
    nvc.toolbar.barStyle = UIBarStyleDefault;
    
    [self presentViewController:nvc animated:YES completion:nil];
}

#pragma mark - UIImagePickerController Delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.cameraPhoto = chosenImage;
    
    VLog(@"ImageInfo: %@", info);
    
    if (sourceIsPhoto) {
        YKImageCropperViewController *vc = [[YKImageCropperViewController alloc] initWithImage:chosenImage];
        vc.cancelHandler = ^() {
            NSLog(@"* Cancel");
            [self dismissViewControllerAnimated:YES completion:nil];
        };
        vc.doneHandler = ^(UIImage *editedImage) {
            NSLog(@"* Done");
            NSLog(@"Original: %@, Edited: %@", NSStringFromCGSize(chosenImage.size), NSStringFromCGSize(editedImage.size));
            
            NSString *filePath2 = [NSString stringWithFormat:@"%@/%@.jpg", [Utils userPhotoDirectory], [Utils randomNumber]];
            NSData *imageData = UIImageJPEGRepresentation(editedImage, 1.0);
            [imageData writeToFile:filePath2 atomically:NO];
            
            [picker dismissViewControllerAnimated:YES completion:^{
                [self _refreshRecentPhotos];
            }];
            
            if (sourceIsPhoto) {
                [self dismissViewControllerAnimated:YES completion:^{
                    [self _refreshRecentPhotos];
                }];
            }
        };
        
        if (sourceIsPhoto) {
            [self.popoverImageViewController dismissPopoverAnimated:YES];
        }
        
        UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:vc];
        nvc.navigationBar.barStyle = UIBarStyleDefault;
        nvc.toolbar.barStyle = UIBarStyleDefault;
        
        [self presentViewController:nvc animated:YES completion:nil];
    } else {
        [picker dismissViewControllerAnimated:YES completion:^{
            [self showImageCropper];
        }];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if (sourceIsPhoto) {
        [self.popoverImageViewController dismissPopoverAnimated:YES];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)clearAllAction:(id)sender {

    [Utils removeUserPhotos];
    [self _refreshRecentPhotos];
    [self.gridView setNeedsDisplay];
    
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
//    dispatch_async(queue, ^{
//        [Utils removeUserPhotos];
//        
//        dispatch_sync(dispatch_get_main_queue(), ^{
//            [self _refreshRecentPhotos];
//            [self.gridView setNeedsDisplay];
//        });
//    });
}


- (void) sendActivityAvatarUpdate {
    
    dispatch_queue_t queue = dispatch_queue_create("com.pearson.course.UPLOADAVATAR",DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        ResourceManager *rm = [AppDelegate resourceInstance];
        NSString *details = @"New Profile image uploaded using Apple iPad";
        [rm requestLogActivityWithModuleType:@"2" details:details];
    });
}


-(IBAction)uploadImage:(id)sender {
    if (selectedCell) {
        
        NSString *uploadImage = NSLocalizedString(@"Uploading your image ...", nil);
        [SVProgressHUD showWithStatus:uploadImage maskType:SVProgressHUDMaskTypeGradient];
        
        [[VSmart sharedInstance] upload:selectedCell.image resultBlock:^(PhotoResponse *responseData) {
            VLog(@"Photo: %@", responseData.url);
            
//            NSString *url = [Utils buildUrl:responseData.url];
            
            [[VSmart sharedInstance] updateAvatar:responseData.url];
            
            VS_NCPOST(kNotificationProfileReload);
            
            NSString *avatarUpdateLabel = NSLocalizedString(@"Your avatar has been updated!", nil);
            [SVProgressHUD showSuccessWithStatus:avatarUpdateLabel];
            
            //PEARSON SPECIFIC
            [self sendActivityAvatarUpdate];
            
        } failureBlock:^(NSError *error, NSData *responseData) {
            VLog(@"Error Upload: %@", [error localizedDescription]);
            [SVProgressHUD dismiss];
        }];
    }
}

-(IBAction)avatarAction:(UIButton*)sender {
    int tag = (int)[sender tag];
    sourceIsPhoto = NO;

    if (tag == 0) {
//        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//        picker.delegate = self;
//        picker.allowsEditing = YES;
//        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//        
//        [self presentViewController:picker animated:YES completion:NULL];
        
        sourceIsPhoto = YES;
        
        UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
        [imgPicker setDelegate:self];
        [imgPicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [imgPicker setAllowsEditing:YES];
        //[imgPicker setModalPresentationStyle:UIModalPresentationCurrentContext];
        
        UIPopoverController *popOver = [[UIPopoverController alloc] initWithContentViewController:imgPicker];
        popOver.delegate = self;
        self.popoverImageViewController = popOver;

        CGRect buttonFrameInDetailView = [self.view convertRect:_photoButton.frame fromView:self.view];
        [self.popoverImageViewController presentPopoverFromRect:buttonFrameInDetailView inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else if (tag == 1) { // Camera
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            [imagePicker setDelegate:self];
            [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
            [imagePicker setAllowsEditing:YES];
            //[imagePicker setModalPresentationStyle:UIModalPresentationCurrentContext];
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
    }
}

-(IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
