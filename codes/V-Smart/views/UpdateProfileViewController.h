//
//  UpdateProfileViewController.h
//  V-Smart
//
//  Created by VhaL on 4/8/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+Popup.h"

@interface UpdateProfileViewController : UIViewController <UITextFieldDelegate>
@property (nonatomic, strong) IBOutlet UIButton *buttonUpdate;
@property (nonatomic, assign) id delegate;
@end
