//
//  GuestTableViewController.m
//  V-Smart
//
//  Created by VhaL on 3/26/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "GuestTableViewController.h"


#define kCellHeight 44

@interface GuestTableViewController ()
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSString *position;
@end

@implementation GuestTableViewController
@synthesize guestArray, delegate, parent;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGFloat tableViewHeight = 0;
    if (guestArray){
        tableViewHeight = kCellHeight * guestArray.count;
    }
    
    if (180 < tableViewHeight){
        tableViewHeight = 180;
    }
    
    self.view.frame = CGRectMake(0,
                                 0,
                                 187,
                                 30+tableViewHeight);
    
    self.tableView.frame = CGRectMake(0,
                                      31,
                                      187,
                                      tableViewHeight);
    [self.tableView reloadData];
}

-(void)initializeGuestList:(NSString*)kMode{

    NSData *data;
    if ([kMode isEqualToString:kModeIsTeacher]){
        data = [[NSUserDefaults standardUserDefaults] objectForKey:kGuestListKeyTeacher];
    }
    if ([kMode isEqualToString:kModeIsStudent]){
        data = [[NSUserDefaults standardUserDefaults] objectForKey:kGuestListKeyStudent];
    }

    self.position = kMode;
    
    NSArray *guestsRecord = [NSKeyedUnarchiver unarchiveObjectWithData:data];

    guestArray = [[NSArray alloc] initWithArray:guestsRecord];
}

-(IBAction)newGuestTapped:(id)sender{
    if ([self.delegate respondsToSelector:@selector(signInAsGuest:position:)]){
        [self.delegate performSelector:@selector(signInAsGuest:position:) withObject:nil withObject:self.position];
    }
    
    [self.parent dismissPopoverAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return guestArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DefaultCell"];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DefaultCell"];
    }
    
    NSDictionary *dict = [guestArray objectAtIndex:indexPath.row];
    
    GuestAccount *guest = [[GuestAccount alloc] initWithDictionary:dict error:nil];
    
    cell.textLabel.text = guest.name;
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:16];
    
//    NSString *avatarLink = [[[guest.value objectForKey:@"account"] objectForKey:@"user"] objectForKey:@"avatar"];
//    
//    if (0 < avatarLink.length){
//        cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
//        cell.imageView.layer.cornerRadius = cell.imageView.frame.size.height/2;
//        cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:avatarLink]]];
//    }
    
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [guestArray objectAtIndex:indexPath.row];
    GuestAccount *guest = [[GuestAccount alloc] initWithDictionary:dict error:nil];
    
    if ([self.delegate respondsToSelector:@selector(signInAsGuest:position:)]){
        [self.delegate performSelector:@selector(signInAsGuest:position:) withObject:guest.value withObject:self.position];
    }
    
    [self.parent dismissPopoverAnimated:YES];
}

@end
