//
//  ChangePasswordController.h
//  V-Smart
//
//  Created by Ryan Migallos on 6/10/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChangePasswordDelegate <NSObject>
@optional
- (void)didFinishChangedPasswordWithStatus:(BOOL)status;
@end

@interface ChangePasswordController : UIViewController
@property (nonatomic, weak) id <ChangePasswordDelegate> delegate;
@end
