//
//  BaseViewController.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/16/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarView.h"
#import "AccountInfo.h"
#import "VSmart.h"
#import "REMenu.h"
#import "PopoverView.h"
#import "EGOImageView.h"
#import "AvatarViewController.h"
#import "ProfileMedalViewController.h"
#import "UIViewController+Popup.h"
#import "AppDelegate.h"
#import "UpdateProfileViewController.h"

@interface BaseViewController : UIViewController<PopoverViewDelegate, ProfileMedalViewControllerDelegate, EGOImageViewDelegate>
{
    PopoverView *popView;
    EGOImageView *profileImageView;
    CGPoint popViewPoint;
    
    ProfileMedalViewController *popProfileMedalViewController;
    UpdateProfileViewController *updateProfileViewController;
}

//@property (strong, nonatomic) UIView *baseHeaderView;
@property (strong, nonatomic) UIView *baseProfileView;
@property (strong, nonatomic) UILabel *accountName;
@property (strong, nonatomic) UILabel *accountType;     // ENHANCEMENT#137: (120215)
@property (strong, nonatomic) UILabel *accountEmail;    // ENHANCEMENT#137: (120215)
@property (strong, nonatomic) AvatarView *avatarView;
@property (strong, nonatomic) AvatarView *avatarSmallView;
@property (strong, nonatomic) AccountInfo *account;

@property (strong, nonatomic) UIButton *jumpMenu;
@property (nonatomic, strong) REMenu *menu;

@property (nonatomic, assign) CGRect toolbarSize;
@property (nonatomic, assign) CGRect headerSize;
@property (nonatomic, assign) CGRect profileSize;
@property (nonatomic, assign) float profileHeight;


@property (strong, nonatomic) UIPopoverController *popoverController;

-(void) alertMessage: (NSString *) message;
-(void) hideJumpMenu: (BOOL) hide;
-(void) showOrHideMiniAvatar;
-(void) adjustProfileHeight;
-(void) postLogout;
-(void) reloadAvatar;
-(void) refreshMedals;
-(void) hideToolbar: (BOOL) hide;
-(void) enableHomeButton:(BOOL)enable;
-(void) updateNotifCount;
-(void) signout;

@end
