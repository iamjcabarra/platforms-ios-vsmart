//
//  AssessmentUnlockViewController.h
//  V-Smart
//
//  Created by VhaL on 4/5/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssessmentUnlockViewController : UIViewController
@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) NSString *exerId;
@property (nonatomic, strong) IBOutlet UITextField *textField;
@end
