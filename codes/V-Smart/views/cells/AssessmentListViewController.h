//
//  AssessmentListViewController.h
//  V-Smart
//
//  Created by VhaL on 4/4/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssessmentListViewController : UIViewController

-(void)initializeList:(NSString*)bookId exerId:(NSString*)exerId;
@property (nonatomic, strong) IBOutlet UILabel *bookTitle;
@end
