//
//  BookInfoViewController.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 8/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BookInfoViewController.h"
#import "UIImageView+Additions.h"
#import <QuartzCore/QuartzCore.h>
#import "CALayer+Additions.h"

@interface BookInfoViewController ()

@end

@implementation BookInfoViewController
@synthesize titleLabel, backgroundButton;
@synthesize readOrDeleteButton, cancelButton;
@synthesize delegate, book;
@synthesize authorsLabel, dateAddedLabel, dateLastReadLabel, bookImage;
@synthesize bookId, index;
@synthesize buttonStateText;


-(AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    [self initButtons];
    
    //self.view.backgroundColor = COLOR_BOOK_BG;
    self.view.layer.cornerRadius = 10;
    self.view.layer.masksToBounds = YES;

    UIRectCorner corners = UIRectCornerTopLeft;
    [self.bookImage maskRoundCorners:corners radius:10.0f];
    
    self.cancelButton.layer.mask = [CALayer maskLayerWithCorners:UIRectCornerBottomRight radii:CGSizeMake(10.0f, 10.0f) frame:self.cancelButton.bounds];
    self.readOrDeleteButton.layer.mask = [CALayer maskLayerWithCorners:UIRectCornerBottomLeft radii:CGSizeMake(10.0f, 10.0f) frame:self.readOrDeleteButton.bounds];
    self.blackBG.layer.mask = [CALayer maskLayerWithCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight radii:CGSizeMake(10.0f, 10.0f) frame:self.blackBG.bounds];
}

-(void) initButtons
{
//    [readOrDeleteButton setBackgroundImage:[[UIImage imageNamed:@"read_default"] stretchableImageWithLeftCapWidth:5.0 topCapHeight:0.0] forState:UIControlStateNormal];
//    [readOrDeleteButton setBackgroundImage:[[UIImage imageNamed:@"read_selected"] stretchableImageWithLeftCapWidth:5.0 topCapHeight:0.0] forState:UIControlStateHighlighted];
//    [cancelButton setBackgroundImage:[[UIImage imageNamed:@"read_default"] stretchableImageWithLeftCapWidth:5.0 topCapHeight:0.0] forState:UIControlStateNormal];
//    [cancelButton setBackgroundImage:[[UIImage imageNamed:@"read_selected"] stretchableImageWithLeftCapWidth:5.0 topCapHeight:0.0] forState:UIControlStateHighlighted];
    
//    bool isExerciseDataFileExists = [Utils isExerciseDataFileExists:VS_FMT(@"%i", bookId)];
    bool isExerciseDataFileExists = [Utils isExerciseDataFileExists:VS_FMT(@"%@", bookId)];
    
//    if ([[self account].user.position isEqualToString:kModeIsTeacher] && isExerciseDataFileExists){
//        self.readOrDeleteButton.frame = CGRectMake(0, 268, 150, 46);
//        self.cancelButton.frame = CGRectMake(358, 268, 150, 46);
//        self.assessmentButton.frame = CGRectMake(151, 268, 206, 46);
//    }
//    else{
        self.readOrDeleteButton.frame = CGRectMake(0, 268, 253, 46);
        self.cancelButton.frame = CGRectMake(254, 268, 254, 46);
        self.assessmentButton.hidden = YES;
//    }
    
}

-(IBAction)actionClicked:(id)sender
{
//    UIButton *button = (UIButton *)sender;
    ActionType action = CancelAction;//default
    
    //DONT USE BUTTON LABEL this will be used for localization
    //USING korean or thai language will be a mismatch
    
//    NSString *actionText = button.titleLabel.text;
    
    NSString *actionText = self.buttonStateText;
    VLog(@"Action: %@", actionText);

    if ([actionText isEqualToString:@"Read"]) {
        action = ReadAction;
    }
    if ([actionText isEqualToString:@"Delete"]) {
        action = DeleteAction;
    }
//    if ([actionText isEqualToString:@"Assessment"]) {
//        action = AssessmentAction;
//    }
    
    VLog(@"actionClicked: Book Id: %@ and Index: %i", bookId, index);
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionButtonClicked: withAction: withIndex: andBookId: forBook:)]) {
        [self.delegate actionButtonClicked:self withAction:action withIndex:index andBookId: bookId forBook:self.book];
    }
}

-(IBAction)cancelClicked:(id)sender
{
    ActionType action = CancelAction;//default
    
    VLog(@"actionClicked: Book Id: %@ and Index: %i", bookId, index);
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionButtonClicked: withAction: withIndex: andBookId: forBook:)]) {
        [self.delegate actionButtonClicked:self withAction:action withIndex:index andBookId: bookId forBook:self.book];
    }
}

-(void)viewDidUnload
{
    [self setTitleText:nil];
    [self setAuthorText:nil];
    [self setPublisherText:nil];
    [self setBlackBG:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(void)dealloc {

}
@end
