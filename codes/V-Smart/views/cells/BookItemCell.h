//
//  BookItemCell.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/28/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "AQGridViewCell.h"
#import "EGOImageView.h"
#import "Book.h"
#import "PICircularProgressView.h"
#import "BookProgress.h"

@interface BookItemCell : AQGridViewCell<EGOImageViewDelegate>
{
    //UIImageView * _imageView;
    UILabel * _title;
    UILabel * _author;
    UILabel * _dateLastRead;
    UIImageView * _cloudImageView;
    UIImageView * _downloadImageView;
    UIView *_overlayFrame;
    UIImageView *_unreadImageView;
    UIView *_errorFrame;
    UIButton *_averageButtonView;
}

@property (nonatomic, strong) Book *book;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *dateLastRead;
@property (nonatomic, strong) EGOImageView *imageView;
@property (nonatomic, assign) CGFloat downloadProgress;

//@property (nonatomic, strong) PICircularProgressView *progressView;

-(void) setBookProgressVisibility: (BOOL) show;
-(void) setBookProgress: (CGFloat) progress withText:(NSString*) progressText;
-(void) setCover:(NSString*) coverUrl;
-(void) setProgressVisibility: (BOOL) show;
-(void) setCompleted: (BOOL) completed;
-(void) setReadStatus: (BOOL) isRead;
-(void) setCloudVisibility: (BOOL) isVisible;
-(void) setAverageText: (NSString *) text;
-(void) setAverageVisibility: (BOOL) visible;
-(void) setDateLastReadVisibility:(BOOL) visible;
@end
