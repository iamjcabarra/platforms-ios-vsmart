//
//  ProfileMedalCell.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/23/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileMedalCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *chapterName;
@property (weak, nonatomic) IBOutlet UILabel *exerciseNumber;
@property (weak, nonatomic) IBOutlet UILabel *latestScore;
@property (weak, nonatomic) IBOutlet UILabel *dateTaken;

@end
