//
//  QuizResultCell.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/17/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuizResultCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *chapterName;
@property (weak, nonatomic) IBOutlet UILabel *exerciseNumber;
@property (weak, nonatomic) IBOutlet UILabel *latestScore;
@property (weak, nonatomic) IBOutlet UILabel *dateTaken;
@property (weak, nonatomic) IBOutlet UIImageView *medal;
@end
