//
//  BookHighlightCell.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/28/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "BookHighlightCell.h"

@implementation BookHighlightCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
