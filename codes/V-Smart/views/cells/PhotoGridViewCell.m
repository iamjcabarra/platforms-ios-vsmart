

#import "PhotoGridViewCell.h"

@implementation PhotoGridViewCell

- (id) initWithFrame: (CGRect) frame reuseIdentifier: (NSString *) aReuseIdentifier
{
    self = [super initWithFrame: frame reuseIdentifier: aReuseIdentifier];
    if ( self == nil )
        return ( nil );
    
    _imageView = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, 110.0, 110.0)];
    _imageView.contentMode = UIViewContentModeScaleToFill;
    
    CGRect rect = CGRectMake(0, 0, 120, 120);
    _avatarView = [[AvatarView alloc] initWithFrame:CGRectInset(rect, 4, 4) applyShadow:YES];
    _avatarView.backgroundColor = [UIColor clearColor];
    [_avatarView setImage:_imageView.image];
    _avatarView.center = self.center;
    
    [self.contentView addSubview: _avatarView];
    //[self.contentView addSubview: _imageView];
    
    return ( self );
}

- (CALayer *) glowSelectionLayer
{
    return ( _imageView.layer );
}

- (UIImage *) image
{
    return ( _imageView.image );
}

-(void) setImage: (UIImage *) anImage
{
    _imageView.image = anImage;
    [_avatarView setImage:_imageView.image];
    [self setNeedsLayout];
    [_avatarView setNeedsDisplay];
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    
//    CGSize imageSize = _imageView.image.size;
//    CGRect frame = _imageView.frame;
//    CGRect bounds = self.contentView.bounds;
//    
//    if ( (imageSize.width <= bounds.size.width) &&
//         (imageSize.height <= bounds.size.height) )
//    {
//        return;
//    }
    
    // scale it down to fit
//    CGFloat hRatio = bounds.size.width / imageSize.width;
//    CGFloat vRatio = bounds.size.height / imageSize.height;
//    CGFloat ratio = MAX(hRatio, vRatio);
//    
//    frame.size.width = floorf(imageSize.width * ratio);
//    frame.size.height = floorf(imageSize.height * ratio);
//    frame.origin.x = floorf((bounds.size.width - frame.size.width) * 0.5);
//    frame.origin.y = floorf((bounds.size.height - frame.size.height) * 0.5);
//    _imageView.frame = frame;
}

@end
