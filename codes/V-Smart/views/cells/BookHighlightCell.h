//
//  BookHighlightCell.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/28/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookHighlightCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIImageView *imageType;
@property (retain, nonatomic) IBOutlet UILabel *headerType;
@property (retain, nonatomic) IBOutlet UILabel *itemText;
@property (retain, nonatomic) IBOutlet UILabel *dateCreated;

@end
