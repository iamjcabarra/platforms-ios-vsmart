//
//  BookItemCell.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/28/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "BookItemCell.h"

@interface BookItemCell ()
{
    PICircularProgressView *_progressView;
    BookProgress *_bookProgress;
    UILabel *bookProgressLabel;
}

@property (nonatomic, strong) UIButton *_downloadButton;
@end

@implementation BookItemCell

- (id) initWithFrame: (CGRect) frame reuseIdentifier: (NSString *) aReuseIdentifier
{
    self = [super initWithFrame: frame reuseIdentifier: aReuseIdentifier];
    if ( self == nil )
        return ( nil );
    
    //VLogm();
    
    _imageView = [[EGOImageView alloc] initWithPlaceholderImage:[UIImage imageNamed:@"img_book-placeholder"]];
    _imageView.delegate = self;

    _title = [[UILabel alloc] initWithFrame: CGRectZero];
    //_title.highlightedTextColor = [UIColor whiteColor];
    //_title.font = [UIFont boldSystemFontOfSize: 12.0];
    _title.font = [UIFont fontWithName:@"HelveticaNeue" size:17.0f];
    _title.adjustsFontSizeToFitWidth = NO;
    _title.minimumScaleFactor = 10.0;

    _author = [[UILabel alloc] initWithFrame: CGRectZero];
    //_title.highlightedTextColor = [UIColor whiteColor];
    //_title.font = [UIFont boldSystemFontOfSize: 12.0];
    _author.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0f];
    _author.adjustsFontSizeToFitWidth = NO;
    _author.minimumScaleFactor = 10.0;
    
    //self.backgroundColor = [UIColor colorWithWhite: 0.95 alpha: 1.0];
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = self.backgroundColor;
    _imageView.backgroundColor = self.backgroundColor;
    _title.backgroundColor = self.backgroundColor;
    
    [self.contentView addSubview: _imageView];
    [self.contentView addSubview: _title];
    [self.contentView addSubview: _author];
    
    //self._downloadButton = [UIButton buttonWithLabel:@"DOWNLOAD" forTarget:self withAction:@selector(downloadBookAction:)];
    //self._downloadButton = [UIButton buttonWithImage:[UIImage imageNamed:@"icn_download"] forTarget:self withAction:@selector(downloadBookAction:)];
//    self._downloadButton = [UIButton buttonWithImageNamed:@"icn_download"];
//    
//    self._downloadButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15.0f];
//    [self._downloadButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];

    //[self._downloadButton setFrame:CGRectMake(17, 92, 135, 44)];
//    [self._downloadButton setBackgroundColor:[UIColor darkGrayColor]];
//    self._downloadButton.layer.borderColor = [UIColor blueColor].CGColor;
//    self._downloadButton.layer.borderWidth = 1;
    
    // create background overlay
    _overlayFrame = [[UIView alloc] initWithFrame:CGRectMake(17, 26, 135, 176)];
    _overlayFrame.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.60f];
    [self.contentView addSubview:_overlayFrame];

    _errorFrame = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _errorFrame.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.30f];
    [self.contentView addSubview:_errorFrame];
    [_errorFrame hide];
    
    // create cloud icon
    _cloudImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icn_cloud"]];
    [self.contentView addSubview:_cloudImageView];
    
    _progressView = [[PICircularProgressView alloc] init];
    [self.contentView addSubview:_progressView];
    
    [_progressView setInnerBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.5f]];
    
    _unreadImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_new-label"]];
    [self.contentView addSubview:_unreadImageView];
    //[_unreadImageView hide];
    
    bookProgressLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, 94, 224.0, 21.0)];
//    bookProgressLabel.text = @"TOTAL QUIZ COMPLETED";
    bookProgressLabel.text = NSLocalizedString(@"TOTAL QUIZ COMPLETED", nil);
    bookProgressLabel.tag = 10;
    [bookProgressLabel setFont:[UIFont fontWithName:@"Helvetica" size:10.0f]];
    [self.contentView addSubview:bookProgressLabel];
    
    _bookProgress = [[BookProgress alloc] initWithFrame:CGRectMake(160, 112, 213.0, 20.0)];
    [_bookProgress setBackgroundColor:UIColorFromHex(0x0d6ec1)];
    [_bookProgress setProgressColor:UIColorFromHex(0x00a0dc)];
    [_bookProgress setFont:[UIFont fontWithName:@"Helvetica" size:9.0f]];
    [_bookProgress setTextAlignment:NSTextAlignmentCenter];
    [_bookProgress setTextColor:[UIColor whiteColor]];
    [_bookProgress setUserInteractionEnabled:NO];
    
    [self.contentView addSubview:_bookProgress];
    
    _averageButtonView = [UIButton buttonWithType:UIButtonTypeCustom];
    [_averageButtonView addTarget:self
               action:@selector(averageAction:) forControlEvents:UIControlEventTouchDown];
    [_averageButtonView setBackgroundColor:UIColorFromHex(0xeeeeee)];
    [_averageButtonView setTitle:@"Show View" forState:UIControlStateNormal];
    _averageButtonView.frame = CGRectMake(160.0, 135.0, 213.0, 21.0);
    _averageButtonView.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:10.0f];
    [_averageButtonView setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.contentView addSubview:_averageButtonView];
    
    _dateLastRead = [[UILabel alloc] initWithFrame: CGRectZero];
    //_title.highlightedTextColor = [UIColor whiteColor];
    _dateLastRead.font = [UIFont fontWithName:@"Helvetica-Light" size:10.0f];
    _dateLastRead.adjustsFontSizeToFitWidth = NO;
    _dateLastRead.minimumScaleFactor = 10.0;
    
    [self.contentView addSubview:_dateLastRead];
    
    return ( self );
}

/*
- (void)prepareForReuse
{
    [super prepareForReuse];
    
    _title.text = @"";
    _author.text = @"";
    _dateLastRead.text = @"";
    _cloudImageView.image = nil;
    _downloadImageView.image = nil;
    _overlayFrame = nil;
    _unreadImageView.image = nil;
    _errorFrame = nil;
    _averageButtonView = nil;
    
    _progressView.progress = 0.0f;
    _bookProgress.progress = 0.0f;
    bookProgressLabel.text = @"";
}
*/

-(void) averageAction: (id) sender {
    VS_NCPOST_OBJ(kNotificationTextbookShelfSelection, _book);
}

-(void) setDownloadProgress: (CGFloat) value {
    _progressView.progress = value;
    [self setNeedsLayout];
}

-(void) setAverageText: (NSString *) text {
    [_averageButtonView setTitle:text forState:UIControlStateNormal];
    [self setNeedsLayout];
}

-(void) setAverageVisibility: (BOOL) visible {
    [_averageButtonView setHidden:!visible];
    [self setNeedsLayout];
}

#pragma mark - EGOImageView Delegate
-(void) imageViewLoadedImage:(EGOImageView *)imageView {
    //_imageView.image = [imageView.image imageToGrayscale];
    //[self setNeedsLayout];
}

- (UIImage *) image {
    return ( _imageView.image );
}

-(void) setImage: (UIImage *) anImage {
    _imageView.image = anImage;
    [self setNeedsLayout];
}

-(void) setCloudVisibility: (BOOL) isVisible {
    [_errorFrame fadeOut];
    
    if (isVisible) {
        [_cloudImageView fadeIn];
        [_unreadImageView hide];
    }
}

-(void) setCompleted: (BOOL) completed {
    if (completed) {
        [_progressView fadeOut];
        [_overlayFrame fadeOut];
        [_cloudImageView fadeOut];
    } else {
        UIView *view = _errorFrame;
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
            view.alpha = 1.0f;
        } completion:^(BOOL finished) {
            [_errorFrame fadeOut];
        }];
    }
}

-(void) setBookProgressVisibility: (BOOL) show {
    UILabel *label = (UILabel *)[self viewWithTag:10];
    if (!show) {
        [_bookProgress fadeOut];
        [label hide];
    } else {
        [_bookProgress fadeIn];
        [label show];
    }
    
    [self setNeedsLayout];
}

-(void) setProgressVisibility: (BOOL) show {
    [_unreadImageView hide];
    
    if (show) {
        if(_progressView.alpha == 0) {
            [_progressView fadeIn];
            [_overlayFrame fadeIn];
        }
    } else {
        [_progressView fadeOut];
        [_overlayFrame fadeOut];
    }
}

-(void) setBookProgress: (CGFloat) progress withText:(NSString*) progressText {
    [_bookProgress setProgress:progress];
    [_bookProgress setText:[progressText uppercaseString]];
}

-(void)setCover:(NSString*) coverUrl {
    _imageView.imageURL = [NSURL URLWithString:coverUrl];
}

- (NSString *) dateLastRead {
    return ( _dateLastRead.text );
}

-(void) setDateLastReadVisibility:(BOOL) visible {
    [_dateLastRead setHidden:!visible];
    [self setNeedsLayout];
}

-(void) setDateLastRead:(NSString *)dateLastRead {
    _dateLastRead.text = dateLastRead;
    [self setNeedsLayout];
}

- (NSString *) title {
    return ( _title.text );
}

-(void) setTitle: (NSString *) title {
    _title.text = title;
    [self setNeedsLayout];
}

- (NSString *) author {
    return ( _author.text );
}

-(void) setAuthor: (NSString *) author {
    _author.text = author;
    [self setNeedsLayout];
}

-(void) setReadStatus: (BOOL) isRead {
    if (isRead) {
        [_unreadImageView hide];
    } else {
        [_unreadImageView fadeIn];
    }
    [self setNeedsLayout];
}

-(void) layoutSubviews
{
    [super layoutSubviews];
        
    [_title sizeToFit];
    [_title setFrame:CGRectMake(160, 25, 224, 21)];

    [_author sizeToFit];
    [_author setFrame:CGRectMake(160, 49, 224, 21)];
    
    [_imageView sizeToFit];
    _imageView.frame = CGRectMake(17, 26, 135, 176);
    //[self._downloadButton setFrame:CGRectMake(17, 92, 135, 44)];
    //[self._downloadButton setFrame:CGRectMake(17, 26, 135, 176)];
    //[self._downloadButton setAlpha:0.60];
    
    [_cloudImageView setFrame:CGRectMake(107, 30, 42, 26)];
    //[_downloadImageView setFrame:CGRectMake(64, 93, 40, 42)];
    [_progressView setFrame:CGRectMake(44, 79, 80, 69)];
    [_unreadImageView setFrame:CGRectMake(109, 26, 43, 43)];
    [_averageButtonView setFrame:CGRectMake(160.0, 135.0, 213.0, 21.0)];
    [_dateLastRead setFrame:CGRectMake(160.0, 176.0, 224.0, 21.0)];
}

@end
