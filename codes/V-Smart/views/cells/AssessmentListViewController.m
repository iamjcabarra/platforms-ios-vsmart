//
//  AssessmentListViewController.m
//  V-Smart
//
//  Created by VhaL on 4/4/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "AssessmentListViewController.h"
#import "JMLockInOutResponse.h"
#import "AssessmentInfo.h"

@interface AssessmentListViewController ()
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *arrayList;

@end

@implementation AssessmentListViewController
@synthesize arrayList, bookTitle;

- (void)viewDidLoad
{
    [super viewDidLoad];
    bookTitle.text = @"";
}

-(void)initializeList:(NSString*)bookId exerId:(NSString*)exerId{
    
    arrayList = [[NSMutableArray alloc] init];
    
    NSDictionary *dictLockIO = [[NSUserDefaults standardUserDefaults] getCustomObjectForKey:kLockIOResponseKey];

    // If the result is not an instance of an NSArray data type exit gracefully
    if (![dictLockIO[@"books"] isKindOfClass:[NSArray class]]) {
        VLog(@"[WARNING] books content: %@", dictLockIO[@"books"] );
        return;
    }
    
    JSONModelError *error;
    JMLockInOutResponse *resp = [[JMLockInOutResponse alloc] initWithDictionary:dictLockIO error:&error];
    if (resp == nil)
        return;
    
    for (JMLockInOutBook *book in resp.books) {
        
        if (![book.bookUuid isEqualToString:bookId])
            continue;
        
        for (JMLockInOutBookData *data in book.data) {
            
            for (JMLockInOutBookExercise *exer in data.exercises) {

                if ([exer.exerId isEqualToString:exerId])
                {
                    AssessmentInfo *info = [AssessmentInfo new];
                    info.bookId = book.bookUuid;
                    info.section = data.section;
                    info.exerciseId = exer.exerId;
                    info.exerciseCode = exer.code;
                    
                    [arrayList addObject:info];
                }
            }
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DefaultCell"];
    
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"DefaultCell"];
    }
    
    AssessmentInfo *info = [self.arrayList objectAtIndex:indexPath.row];

    cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", info.section, info.exerciseId ];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", info.exerciseCode ];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}


@end
