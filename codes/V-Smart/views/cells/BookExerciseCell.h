//
//  BookExerciseCell.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/28/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookExerciseCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIImageView *imageType;
@property (retain, nonatomic) IBOutlet UILabel *chapterTitle;
@property (retain, nonatomic) IBOutlet UILabel *itemCount;
@property (retain, nonatomic) IBOutlet UILabel *exerciseName;

@end
