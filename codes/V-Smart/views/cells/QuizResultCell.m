//
//  QuizResultCell.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/17/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "QuizResultCell.h"

@implementation QuizResultCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
