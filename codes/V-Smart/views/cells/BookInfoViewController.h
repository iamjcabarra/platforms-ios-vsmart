//
//  BookInfoViewController.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 8/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Book.h"

typedef enum {
    ReadAction = 0,
    DeleteAction,
    CancelAction,
//    AssessmentAction
} ActionType;

@protocol BookInfoViewControllerDelegate;
@interface BookInfoViewController : UIViewController

@property (nonatomic, assign) id<BookInfoViewControllerDelegate> delegate;

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *authorsLabel;
@property (nonatomic, retain) IBOutlet UILabel *dateAddedLabel;
@property (nonatomic, retain) IBOutlet UILabel *dateLastReadLabel;
@property (nonatomic, retain) IBOutlet UIButton *backgroundButton;

@property (nonatomic, retain) IBOutlet UIButton *readOrDeleteButton;
@property (nonatomic, retain) IBOutlet UIButton *cancelButton;
@property (nonatomic, retain) IBOutlet UIButton *assessmentButton;

@property (nonatomic, retain) IBOutlet UIImageView *bookImage;
@property (nonatomic, retain) NSString *bookId;
@property (nonatomic, assign) int index;        
@property (nonatomic, retain) NSString *buttonStateText;
@property (nonatomic, retain) Book *book;
@property (retain, nonatomic) IBOutlet UITextView *titleText;
@property (retain, nonatomic) IBOutlet UITextView *authorText;
@property (retain, nonatomic) IBOutlet UITextView *publisherText;
@property (retain, nonatomic) IBOutlet UIButton *blackBG;

-(IBAction)actionClicked:(id)sender;
-(IBAction)cancelClicked:(id)sender;
-(void) initButtons;
@end

@protocol BookInfoViewControllerDelegate <NSObject>

-(void)actionButtonClicked:(BookInfoViewController *) popViewController withAction:(ActionType) actionType withIndex:(int) index andBookId:(NSString *)bookId forBook: (Book *) book;

@end