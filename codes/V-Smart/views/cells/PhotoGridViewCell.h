

#import <Foundation/Foundation.h>
#import "AQGridViewCell.h"
#import "AvatarView.h"

@interface PhotoGridViewCell : AQGridViewCell
{
    UIImageView * _imageView;
    AvatarView *_avatarView;
}
@property (nonatomic, retain) UIImage * image;
@end
