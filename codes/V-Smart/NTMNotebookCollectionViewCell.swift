//
//  NTMNotebookCollectionViewCell.swift
//  V-Smart
//
//  Created by Julius Abarra on 08/08/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class NTMNotebookCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var notebookImage: UIImageView!
    @IBOutlet var notebookNameLabel: UILabel!
    @IBOutlet var notebookCountLabel: UILabel!
    @IBOutlet var notebookActionButton: UIButton!
    
    func noteBookImage(forColorCode code: Int) -> UIImage {
        var image: UIImage!
        
        switch code {
        case 0:
            image = NTMConstants.NotebookImage.GENERAL
            break
        case 1:
            image = NTMConstants.NotebookImage.RED
            break
        case 2:
            image = NTMConstants.NotebookImage.BLUE
            break
        case 3:
            image = NTMConstants.NotebookImage.YELLOW
            break
        case 4:
            image = NTMConstants.NotebookImage.GREEN
            break
        case 5:
            image = NTMConstants.NotebookImage.PURPLE
            break
        case 6:
            image = NTMConstants.NotebookImage.WHITE
            break
        default:
            image = NTMConstants.NotebookImage.UNKNOWN
        }
        
        return image
    }
    
}
