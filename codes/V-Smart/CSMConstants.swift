//
//  CSMConstants.swift
//  V-Smart
//
//  Created by Julius Abarra on 22/08/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import Foundation

struct CSMConstants {
    struct Entity {
        static let COURSE: String = "CSMCourse"
    }
}

enum CSMCourseListRequestType: Int {
    case load = 1
    case search
    case refresh
}

protocol CSMRoutes: ParseProtocol, NetworkProtocol {
    func route(_ method:String, uri:String, body:[String:AnyObject]?) -> NSMutableURLRequest
    func route(_ method:String, uri:String, query:[String:AnyObject]?, body:[String:AnyObject]?) -> NSMutableURLRequest
}

extension CSMRoutes {
    func route(_ method:String, uri:String, body:[String:AnyObject]?) -> URLRequest {
        let endpoint = "vsmart-rest-dev" + "\(uri)"
        let url = buildURLFromRequestEndPoint(endpoint as NSString)
        let request = buildURLRequestWithMethod(method as NSString, url: url, andBody: body as AnyObject?)
        return request
    }
    
    func route(_ method:String, uri:String, query:[String:AnyObject]?, body:[String:AnyObject]?) -> URLRequest {
        let endpoint = "vsmart-rest-dev" + "\(uri)"
        var url = buildURLFromRequestEndPoint(endpoint as NSString)
        
        if let dictionary = query {
            var queryVariables = [URLQueryItem]()
            
            for (name, object) in dictionary {
                let value = object as! String
                let queryObject = URLQueryItem(name: name, value: value)
                queryVariables.append(queryObject)
            }
            
            var urlComponents = URLComponents(string: url.absoluteString)!
            urlComponents.queryItems = queryVariables
            url = urlComponents.url!
        }
        
        let request = buildURLRequestWithMethod(method as NSString, url: url, andBody: body as AnyObject?)
        return request
    }
}

