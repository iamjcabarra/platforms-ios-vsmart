//
//  AppDelegate.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/14/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "ImportViewController.h"
#import "DocumentsSupport.h"
#import "ResourceManager.h"
#import <Parse/Parse.h>
#import "MainHeader.h"

@interface AppDelegate () <DirectoryWatcherDelegate>

@property (nonatomic, strong) ResourceManager *resourceManager;

- (ResourceManager *)rm;

@end

@implementation AppDelegate
@synthesize ideaPadViewController, calendarViewController, peopleViewController, userProfileViewController, settingsViewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[ UIApplication sharedApplication ] setIdleTimerDisabled: YES ];
    //self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.

    [Utils copySettingsToDocuments];
    
    ideaPadViewController = [IdeaPadViewController new];
    calendarViewController = [SSCalendarViewController new];
    peopleViewController = [SSPeopleViewController new];
    userProfileViewController = [UserProfileViewController new];
    settingsViewController = [SSSettingsViewController new];

    self.viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.viewController];

    [self setupAppearance];

    [self registerDefaultsFromSettingsBundle];
    
    [Parse setApplicationId:kParseApplicationId clientKey:kParseClientKey];
    PFACL *defaultACL = [PFACL ACL];
    [defaultACL setPublicReadAccess:YES];
    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];
    
    //VLog(@"%@", [VSmartHelpers uniqueGlobalDeviceIdentifier]);
    
    directoryWatcher = [DirectoryWatcher watchFolderWithPath:[Utils appDocumentsDirectory] delegate:self];
    [[DocumentsSupport sharedInstance] queueDocumentsSupport];
    
    self.window.rootViewController = navController;

    //self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    [Utils setVendorCode:[VSmartHelpers uniqueGlobalDeviceIdentifier]];
    
    if (application.applicationState != UIApplicationStateBackground) {
        BOOL preBackgroundPush = ![application respondsToSelector:@selector(backgroundRefreshStatus)];
        BOOL oldPushHandlerOnly = ![self respondsToSelector:@selector(application:didReceiveRemoteNotification:fetchCompletionHandler:)];
        BOOL noPushPayload = ![launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (preBackgroundPush || oldPushHandlerOnly || noPushPayload) {
            [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
        }
    }
    
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound];
    
    return YES;
}

- (void)registerDefaultsFromSettingsBundle {
    NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    //VLog(@"BUNDLE: %@", settingsBundle);
    if(!settingsBundle)
    {
        //NSLog(@"Could not find Settings.bundle");
        return;
    }
    
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
    NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
    
    NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
    
    for(NSDictionary *prefSpecification in preferences)
    {
        NSString *key = prefSpecification[@"Key"];
        if(key)
        {
            NSString *value = prefSpecification[@"DefaultValue"];
            // PSToggleSwitchSpecifier
            NSString *type = prefSpecification[@"Type"];
            
            if ([type isEqualToString:@"PSToggleSwitchSpecifier"]) {
                
                NSLog(@"type: %@", type);
                NSLog(@"value : %@", value);
                
            } else {
                // NOTE: Trim leading and trailing white spaces on the string (e.g. " NEU  " into "NEU" )
                value = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                // one time registration for default values
                [defaultsToRegister setObject:value forKey:key];
            }
        }
    }
    [defaultsToRegister setObject:@"VIBAL" forKey:@"schoolcode_preference"];
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];

    //For the paranoid ensure everything is sychronize
    [[NSUserDefaults standardUserDefaults] synchronize];
            
    /*
     NOTE: we need to do a one-time registration for base64 format 
           rest-api -> schoolcode base64 format
           web-app  -> schoolcode human readable format
    */
    [Utils setSchoolCodeBase64representation];
}

-(void) setupAppearance {
    UIImage *toolbarImage = [UIImage imageNamed:@"toolbar_bg"];
    [[UIToolbar appearance] setBackgroundImage:toolbarImage forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];    
}

//-(void) setupToolbarItems: (UINavigationController *) controller {
//    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
//    
//    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [rightBtn setImage:TOOLBAR_CALENDAR forState:UIControlStateNormal];
//    rightBtn.frame = CGRectMake(0, 0, 70, 44 );
//    [rightBtn addTarget:self action:@selector(emptyAction:) forControlEvents:UIControlEventTouchUpInside];
//    
//    UIBarButtonItem *calendar = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
//    UIBarButtonItem *ideapad = [[UIBarButtonItem alloc] initWithImage:TOOLBAR_IDEAPAD style:UIBarButtonItemStylePlain target:self action:nil];
//    UIBarButtonItem *home = [[UIBarButtonItem alloc] initWithImage:TOOLBAR_HOME style:UIBarButtonItemStylePlain target:self action:nil];
//    UIBarButtonItem *profile = [[UIBarButtonItem alloc] initWithImage:TOOLBAR_PROFILE style:UIBarButtonItemStylePlain target:self action:nil];
//    UIBarButtonItem *settings = [[UIBarButtonItem alloc] initWithImage:TOOLBAR_SETTINGS style:UIBarButtonItemStylePlain target:self action:nil];
//    
//    NSArray *toolbarItems = @[flexibleItem, calendar, flexibleItem, ideapad, flexibleItem, home, flexibleItem, profile, flexibleItem, settings, flexibleItem];
//
//    [controller.toolbar setItems:toolbarItems];
//}

- (void)emptyAction:(id)sender {
    //do nothing
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    if (!url) {  return NO; }
    
    NSString *URLString = [url absoluteString];
    //VLog(@"Absolutue URL: %@", URLString);
    
    NSString *urlScheme = [NSString stringWithFormat:@"%@://import", [Utils getUrlScheme]];
    
    if ([URLString isEqualToString:urlScheme]) {
        
        [[ UIApplication sharedApplication ] setIdleTimerDisabled: YES ];
        
        ImportViewController *vc = [[ImportViewController alloc] initWithNibName:@"ImportViewController" bundle:nil];
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        vc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        
        [self.viewController presentViewController:vc animated:YES completion:nil];
        return YES;
    } else {
        [[DocumentsSupport sharedInstance] handleOpenURL:url];
    }
    return NO;
}

-(void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    //School Code State
//    [[self rm] setSchoolCodeValue:[Utils getSettingsSchoolCode] redirect:NO];
    
    [[ UIApplication sharedApplication ] setIdleTimerDisabled: NO ];
}

-(void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    // Save the Object Graph To ensure download is updated when going on background
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[self rm] backgroundSaveContext];
}

-(void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

-(void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[ UIApplication sharedApplication ] setIdleTimerDisabled: YES ];
    [[DocumentsSupport sharedInstance] queueDocumentsSupport]; // Queue a documents update
    
    // When active reload Application State
    [self reloadApplicationState];
}

-(void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

    // save the model state when app terminates (e.g. download progress)
    [[self rm] backgroundSaveContext];
}

- (void)dealloc {
    [directoryWatcherTimer invalidate];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken {
    [PFPush storeDeviceToken:newDeviceToken];
    // Store the deviceToken in the current Installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:newDeviceToken];
    [currentInstallation saveInBackground];
    [PFPush subscribeToChannelInBackground:@"" target:self selector:@selector(subscribeFinished:error:)];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if (error.code == 3010) {
        VLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        VLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
	}
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
    
    if (application.applicationState == UIApplicationStateInactive) {
        [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    if (application.applicationState == UIApplicationStateInactive) {
        [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
    }
}

- (void)subscribeFinished:(NSNumber *)result error:(NSError *)error {
    if ([result boolValue]) {
        VLog(@"ParseStarterProject successfully subscribed to push notifications on the broadcast channel.");
    } else {
        VLog(@"ParseStarterProject failed to subscribe to push notifications on the broadcast channel.");
    }
}

#pragma mark DirectoryWatcherDelegate methods

- (void)directoryDidChange:(DirectoryWatcher *)folderWatcher
{
	if (directoryWatcherTimer != nil) { [directoryWatcherTimer invalidate]; directoryWatcherTimer = nil; } // Invalidate and release previous timer
    
	directoryWatcherTimer = [NSTimer scheduledTimerWithTimeInterval:4.8 target:self selector:@selector(watcherTimerFired:) userInfo:nil repeats:NO];
}

- (void)watcherTimerFired:(NSTimer *)timer
{
	[directoryWatcherTimer invalidate]; directoryWatcherTimer = nil; // Invalidate and release timer
    [[DocumentsSupport sharedInstance] queueDocumentsSupport];
}

#pragma mark - Resource Manager

+ (ResourceManager *)resourceInstance
{
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] rm];
}

- (ResourceManager *)rm
{
    if (!self.resourceManager) {
        self.resourceManager = [[ResourceManager alloc] init];
//        [self.resourceManager setupCoreData];
    }
    return self.resourceManager;
}

- (void)reloadApplicationState
{
//    // Instantiate Resource Manager and Get Old School Code
//    NSString *oldSchoolCode = [[self rm] getSchoolCodeValue];
//    
//    // Refresh the User Defaults and invoke Synchronization
//    [self registerDefaultsFromSettingsBundle];
//    
//    // Get the New School Code
//    NSString *newSchoolCode = [Utils getSettingsSchoolCode];
//    
//    // Test for non-equality
//    if (![newSchoolCode isEqualToString:oldSchoolCode]) {
//        
//        //set redirect status to YES
//        [[self rm] setSchoolCodeValue:oldSchoolCode redirect:YES];
//        
//        //get a pointer to the navigation controller instance
//        UINavigationController *n = (UINavigationController *)self.window.rootViewController;
//        
//        //transition to root controller | TopViewController (a.k.a. ViewController)
//        [n popToRootViewControllerAnimated:NO];
//        
//        //get a pointer to the view controller
//        ViewController *v =  (ViewController *)n.topViewController;
//        
//        [v presentLoginViewController];
//    }
}

@end
