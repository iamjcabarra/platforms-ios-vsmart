//
//  ViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/14/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "VSReachability.h"
#import "GMGridView.h"
#import "DemoViewController.h"
#import "LoginViewController.h"
#import "JSONKit.h"
#import "GCNetworkKit.h"
#import "HUD.h"
#import "ImportViewController.h"
#import "DocumentsSupport.h"
#import "CourseContainerViewController.h"
#import "GradebookContainerController.h"
#import "PlayListContainerController.h"
#import "SocialStreamContainerController.h"
#import "ReportsContainerController.h"
#import "TestGuruContainer.h"
#import "LessonPlanContainer.h"
#import "CurriculumPlannerContainer.h"
#import "NSString+MD5Addition.h"
#import "MainHeader.h"
#import "AppDelegate.h"

//#import "TextbookViewController.h"
#import "TextBookController.h"

#import "V_Smart-Swift.h"

@interface ViewController ()<GMGridViewDataSource, GMGridViewSortingDelegate, GMGridViewActionDelegate>
{
    __gm_weak GMGridView *_gmGridView;
    
    NSMutableArray *_data;
    __gm_weak NSMutableArray *_currentData;
    
    dispatch_queue_t _mainQueue;
}

@property (nonatomic, strong) NSMutableArray *downloadQueue;
@property (nonatomic, strong) NSMutableArray *downloadQueueForRemoval;
@property (nonatomic, strong) NSMutableArray *arrayForQueueRemoval;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) BOOL loginVisible;
@property (nonatomic, assign) BOOL isConnnected;
@property (nonatomic, strong) ResourceManager *rm;

@property (nonatomic, strong) GNDataManager *gnm;


@property (assign, nonatomic) BOOL isVersion25;

@end


@implementation ViewController


- (id)init
{
    if ((self =[super init]))
    {

    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //RESOURCE MANAGER
    self.rm = [AppDelegate resourceInstance];
    [self.rm moveTextBookResources];
    
    self.gnm = [GNDataManager sharedInstance];
    
    _gmGridView.mainSuperView = self.navigationController.view;
    _mainQueue = dispatch_queue_create("com.vibetechnologies.gcd.local", NULL);
    
    [self _showUserInfo];
    
    self.downloadQueues = [[NSMutableArray alloc] init];
    [self setupValidateConnectivity];
    [self setupData];
    [self setupGrid];
    [self setupNotifications];
    
    [super hideJumpMenu:YES];
    [super showOrHideMiniAvatar];
    
    [self setupDownloadPoll];
    
    // BUG FIX
    if ([super account]) {
        
        // BACKWARD COMPATIBILITY IMPLEMENTATION
        [self.rm requestServerVersionBlock:^(BOOL status) {
            NSLog(@"VERSION CHECK...");
            
            CGFloat version = [[Utils getServerInstanceVersion] floatValue];
            
            NSLog(@"TEST GURU SERVER VERSION : %@", @(version) );
            
            self.isVersion25 = (version >= VSMART_SERVER_MAX_VER);
            
            if (self.isVersion25) {
                [self startPollingNotificationsV2];
            } else {
                [self startPollingNotifications:YES];
            }
            
        }];
        
    }
    
    self.arrayForQueueRemoval = [[NSMutableArray alloc] init];
}

-(void) loadView {
    [super loadView];
}

- (void) startPollingNotificationsV2
{
    // BUG FIX #124
    BOOL status = [Utils getUserUpdatsSettings];
    if (status) {
        [self.gnm connectGlobalNotificationSocketV2];
        
    }
}

- (void)navigateToModuleWithID:(NSNotification *)notif {
    
    NSString *module_type_id = (NSString *)notif.object;
    
    AccountInfo *account = [super account];
    NSArray<Module> *modules = account.userModules;
    
    
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        Module *module = (Module *)evaluatedObject;
        NSString *moduleTypeId = module.moduleTypeId;
        
        return [moduleTypeId isEqualToString:module_type_id];
    }];
    
    NSArray<Module> *filteredModules = (NSArray<Module> *)[modules filteredArrayUsingPredicate:predicate];
    if (filteredModules.count > 0) {
        // PROCEED TO NAVIGATE
        
        Module *selectedModule = (Module *)filteredModules.lastObject;
        
        [self pushToModuleWithTypeID:selectedModule.moduleTypeId];
    }
}

- (void) startPollingNotifications:(BOOL)clearData {
    // BUG FIX #124
    BOOL status = [Utils getUserUpdatsSettings];
    if (status) {
        dispatch_queue_t queue = dispatch_queue_create("com.vsmart.global.NOTIFICATION",DISPATCH_QUEUE_SERIAL);
        dispatch_async(queue, ^{
            [self.rm requestNotificationWithClearData:clearData contentBlock:^(NSString *content) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGlobalReadUpdates object:self];
                });
            }];
        });
    }
}

-(void)setupDownloadPoll{
    self.downloadQueue = [NSMutableArray new];
    self.downloadQueueForRemoval = [NSMutableArray new];
    
    self.timer = [NSTimer timerWithTimeInterval:2
                                         target:self
                                       selector:@selector(startDownloadPoll)
                                       userInfo:nil
                                        repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
}

- (void)startDownloadPoll {
    
    __weak typeof(self) wo = self;
//    NSMutableArray *arrayForQueueRemoval = [NSMutableArray new];
    
    if (self.arrayForQueueRemoval != nil) {
        [self.arrayForQueueRemoval removeAllObjects];
    }
    
    for (NSDictionary *item in self.downloadQueueForRemoval) {
        [self.downloadQueue removeObject:item];
        [self.arrayForQueueRemoval addObject:item];
    }
    
    for (NSDictionary *item in self.arrayForQueueRemoval) {
        [self.downloadQueueForRemoval removeObject:item];
    }
    
    for (NSDictionary *item in self.downloadQueue) {
        Book *book = (Book *) [item objectForKey:@"book"];
         [[VSmart sharedInstance] requestDowload:book.sku revision:1
                                withResultBlock:^(NSDictionary *responseData) {
                                    NSString *status = [responseData objectForKey:@"status"];
                                    
                                    if ([status isEqualToString:@"completed"]){
                                        
                                        NSString *link = [responseData objectForKey:@"link"];
                                        [wo.downloadQueueForRemoval addObject:item];
                                        if (wo.isConnnected) {
                                            [wo _downloadDataAtURLStart:link withItem:item];
                                        }
                                    }
                                    
                                    if (!status){ //there's an error
                                        int rowIndex = [[item objectForKey:@"row"] intValue];
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            NSDictionary *dict = [wo _createDownloadData:NO
                                                                                   forBook:book
                                                                        withDownloadStatus:VSmartDownloadError
                                                                              withProgress:0.0
                                                                                  forIndex:rowIndex];
                                            VS_NCPOST_OBJ(kNotificationUpdateQueue, dict)
                                        });
                                        [wo.downloadQueueForRemoval addObject:item];
                                    }
                                }
                                   failureBlock:^(NSError *error, NSData *responseData) {
                                       [wo.downloadQueueForRemoval addObject:item];
                                   }
         ];
    }
    
    //GLOBAL BACKEND NOTIFICATION
    if ([super account]) {
        if (self.isConnnected) {
            [self startPollingNotifications:NO];
        }
    }
}

- (void)displayError:(NSDictionary *)result {

    NSString *msg = [NSString stringWithFormat:@"%@", result[@"message"] ];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view makeToast:msg duration:2.0f position:@"bottom"];
    });
}

#pragma mark - Setup Configurations
-(void) setupNotifications {
    VS_NCADD(kNotificationQueueDownload, @selector(downloadDataForQueue:))
    VS_NCADD(kNotificationJumpMenuSelection, @selector(switchToModule:))
    VS_NCADD(kNotificationProfileHeight, @selector(_refreshGridSize:))
    VS_NCADD(kImportServerPostUpdate, @selector(refreshBooks:))
    VS_NCADD(kVSmartConnectivityNotif, @selector(controllerRefreshNotification:) )
    VS_NCADD(DocumentsUpdateNotification, @selector(syncBooksFromDocuments:))
    VS_NCADD(@"NOTIF_CHANGE_PASSWORD_DONE", @selector(changePasswordNotification:) )
    
    VS_NCADD(@"NOTIFICATION_NAVIGATE_NOTIFICATION", @selector(navigateToModuleWithID:) )
    
}

- (void)setupValidateConnectivity {
    
    NSString *hostName = [NSString stringWithFormat:@"%@", [Utils getVibeServer] ];
    NSLog(@"server: %@", hostName);
    
    // Allocate a reachability object
    VSReachability* reach = [VSReachability reachabilityWithHostname:hostName];
    
    // Tell the reachability that we DON'T want to be reachable on 3G/EDGE/CDMA
    reach.reachableOnWWAN = YES;
    
    __weak typeof(self) wo = self;
    // Set the blocks
    reach.reachableBlock = ^(VSReachability *reach) {
        // keep in mind this is called on a background thread
        // and if you are updating the UI it needs to happen
        // on the main thread, like this:

        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%@ reachable!", hostName);
            wo.isConnnected = YES;
            [wo vsmartControllerRefresh];
        });
    };
    
    reach.unreachableBlock = ^(VSReachability *reach) {
        NSLog(@"%@ unreachable!", hostName);
        dispatch_async(dispatch_get_main_queue(), ^{
            wo.isConnnected = NO;
            [wo vsmartControllerRefresh];
        });
    };
    
    // Start the notifier, which will cause the reachability object to retain itself!
    [reach startNotifier];
}

- (void)vsmartControllerRefresh {
    
    NSString *hidden = @"0";
    if (self.isConnnected == YES) {
        hidden = @"1";
        NSLog(@"%s isConnnected == YES ", __PRETTY_FUNCTION__);
        
    }
    
    if (self.isConnnected == NO) {
        hidden = @"0";
        NSString *msg = NSLocalizedString(@"no internet connection", nil);
        [self.view makeToast:msg duration:2.0f position:@"bottom"];
        NSLog(@"%s isConnnected == NO ", __PRETTY_FUNCTION__);
    }
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:kVSmartConnectivityNotif object:hidden];
}

- (void)controllerRefreshNotification:(NSNotification *)notification {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSString *status = [NSString stringWithFormat:@"%@", [notification object] ];
    NSLog(@"status : %@", status);
    if (![status isEqualToString:@""]) {
        BOOL ishidden = [status isEqualToString:@"1"];
        NSLog(@"hidden : %@", (ishidden) ? @"YES" : @"NO" );
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setupData];
            [_gmGridView reloadData];
        });
    }
}

- (void)setupData {
    
    //NSDictionary *item = @{@"module_name": @"Textbooks", @"image": @"icn_textbooks"};
    
    NSArray *modules = [self _buildModules];
    
    _data = [NSMutableArray arrayWithArray:modules];
    _currentData = _data;
}

-(void) setupGrid {
    VLog(@"setupGrid");
    
    self.view.backgroundColor = UIColorFromHex(0xeeeeee);

    float spacing = 1.2;
    float insets = 2.966102;
    float profileY = [super profileHeight];
    float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
    float gridY = [super headerSize].size.height + profileY;

    GMGridView *gmGridView = [[GMGridView alloc] initWithFrame:CGRectMake(0, gridY - 22, self.dashboardView.bounds.size.width, gridHeight + 44)];
    
    //VLog(@"Size: %@", NSStringFromCGSize(self.dashboardView.bounds.size));
    gmGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    gmGridView.backgroundColor = [UIColor clearColor];
    gmGridView.scrollEnabled = YES;
    [self.view addSubview:gmGridView];
    [self.view sendSubviewToBack:gmGridView];
    _gmGridView = gmGridView;

    _gmGridView.style = GMGridViewStyleSwap;
    _gmGridView.itemSpacing = spacing;
    _gmGridView.minEdgeInsets = UIEdgeInsetsMake(insets, insets, insets, insets);
    _gmGridView.centerGrid = NO;
    _gmGridView.actionDelegate = self;
    _gmGridView.sortingDelegate = self;
    _gmGridView.dataSource = self;
}

- (void) sendActivityLog:(NSDictionary *)activity
{
    dispatch_queue_t queue = dispatch_queue_create("com.pearson.course.ACTIVITYLOG",DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        NSString *type = activity[@"module_type"];
        NSString *details = activity[@"details"];
        [self.rm requestLogActivityWithModuleType:type details:details];
    });
}

//-(void) pushToModule: (NSString *) moduleName {
//    BaseViewController *vc = nil;
//    
//    NSString *type = @"0";
//    NSString *details = @"Open Module in Apple iPad";
//    
//    if ([moduleName isEqualToString:kModuleTextbooks]) {
////        vc = [[TextbookViewController alloc] init];
//        vc = [[TextBookController alloc] init];
//        type = @"8";
//        details = @"Opens Textbook module in Apple iPad";
//    } else if ([moduleName isEqualToString:kModuleGradebook]) {
////        vc = [[GradeBookViewController alloc] init];
//        vc = [[GradebookContainerController alloc] init];
//        type = @"9";
//        details = @"Opens Gradebook module in Apple iPad";
//    } else if ([moduleName isEqualToString:kModuleNotes]) {
//        vc = [[NotesViewController alloc] init];
//        type = @"7";
//        details = @"Opens the Notes module in Apple iPad";
//    } else if ([moduleName isEqualToString:kModuleQuizGuru]) {
////        vc = [[QuizGuruViewController alloc] init];
//        vc = [[TestGuruContainer alloc] init];
//        type = @"7";
//        details = @"Opens Test Guru module in Apple iPad";
//    } else if ([moduleName isEqualToString:kModuleCalendar]) {
//        vc = [[CalendarViewController alloc] init];
//        type = @"10";
//        details = @"Opens Calendar module in Apple iPad";
//    } else if ([moduleName isEqualToString:kModuleClasses]) {
//        vc = [[ClassesViewController alloc] init];
//    } else if ([moduleName isEqualToString:kModuleClassAnalytics]) {
//        vc = [[ClassAnalyticsViewController alloc] init];
//    } else if ([moduleName isEqualToString:kModuleSubjects]) {
//        vc = [[SubjectsViewController alloc] init];
//    } else if ([moduleName isEqualToString:kModulePlaylists]) {
////        vc = [[PlaylistViewController alloc] init];
//        vc = [[PlayListContainerController alloc] init];
//    } else if ([moduleName isEqualToString:kModuleEducationalApps]) {
//        vc = [[EducationalAppViewController alloc] init];
//        type = @"11";
//        details = @"Opens Educational Apps module in Apple iPad";
//    } else if ([moduleName isEqualToString:kModuleSocialStream]) {
//        vc = [[SocialStreamContainerController alloc] init];
////        vc = [[SocialStreamViewController alloc] init];
//        type = @"3";
//        details = @"Opens Social Stream module in Apple iPad";
//    } else if ([moduleName isEqualToString:kModuleSchoolStream]) {
//        vc = [[SchoolStreamViewController alloc] init];
//        type = @"4";
//        details = @"Opens School Stream module in Apple iPad";
//    } else if ([moduleName isEqualToString:kModuleLessonPlan]) {
//        vc = [[LessonPlanContainer alloc] init];
//        type = @"12";
//        details = @"Opens Lesson Plan module in Apple iPad";
//    } else if ([moduleName isEqualToString:kModuleAwardBadges]) {
//        vc = [[AwardBadgesViewController alloc] init];
//        details = @"Opens Award Badges in Apple iPad";
//    } else if ([moduleName isEqualToString:kModuleCourses]) {
//        vc = [[CourseContainerViewController alloc] init];
//        type = @"6";
//        details = @"Opens Course module in Apple iPad";
//    } else if ([moduleName isEqualToString:kModuleCurriculumPlanner]) {
//        vc = [[CurriculumPlannerContainer alloc] init];
//        type = @"22";
//        details = @"Opens Curriculum module in Apple iPad";
//    }
////    else if ( [moduleName isEqualToString:@"Grade Book Reports"] ) {
////        vc = [[ReportsContainerController alloc] init];
////        type = @"23";
////        details = @"Opens Reports module in Apple iPad";
////    }
//    
//    else if ( [moduleName isEqualToString:@"Gradebook Reports"] ) {
//        vc = [[ReportsContainerController alloc] init];
//        type = @"23";
//        details = @"Opens Reports module in Apple iPad";
//    }
//
//    NSDictionary *info = @{ @"module_type" : type, @"details" : details };
//    [self sendActivityLog:info];
//    
//    [self.navigationController pushViewController:vc animated:YES];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [HUD hideUIBlockingIndicator];
//    });
//    
//}


// ENHANCED NAVIGATION LOGIC
-(void) pushToModuleWithTypeID:(NSString *)moduleTypeID {
    BaseViewController *vc = nil;
    
    NSString *type = @"0";
    NSString *details = @"Open Module in Apple iPad";
    
    if ([moduleTypeID isEqualToString:@"8"]) {
        //        vc = [[TextbookViewController alloc] init];
        vc = [[TextBookController alloc] init];
        type = @"8";
        details = @"Opens Textbook module in Apple iPad";
    } else if ([moduleTypeID isEqualToString:@"9"]) {
        //        vc = [[GradeBookViewController alloc] init];
        vc = [[GradebookContainerController alloc] init];
        type = @"9";
        details = @"Opens Gradebook module in Apple iPad";
    } else if ([moduleTypeID isEqualToString:@"7"]) {
        vc = [[NotesViewController alloc] init];
        type = @"7";
        details = @"Opens the Notes module in Apple iPad";
    } else if ([moduleTypeID isEqualToString:@"5"]) {
        //        vc = [[QuizGuruViewController alloc] init];
        vc = [[TestGuruContainer alloc] init];
        type = @"5";
        details = @"Opens Test Guru module in Apple iPad";
    } else if ([moduleTypeID isEqualToString:@"10"]) {
        vc = [[CalendarViewController alloc] init];
        type = @"10";
        details = @"Opens Calendar module in Apple iPad";
        
//    } else if ([moduleName isEqualToString:kModuleClasses]) {
//        vc = [[ClassesViewController alloc] init];
        
    } else if ([moduleTypeID isEqualToString:@"13"]) {
        vc = [[ClassAnalyticsViewController alloc] init];
        type = @"13";
        details = @"Opens Class Analytics module in Apple iPad";
        
//    } else if ([moduleName isEqualToString:kModuleSubjects]) {
//        vc = [[SubjectsViewController alloc] init];
        
    } else if ([moduleTypeID isEqualToString:@"14"]) {
        vc = [[PlayListContainerController alloc] init];
        type = @"14";
        details = @"Open Playlist Module in Apple iPad";
    } else if ([moduleTypeID isEqualToString:@"11"]) {
        vc = [[EducationalAppViewController alloc] init];
        type = @"11";
        details = @"Opens Educational Apps module in Apple iPad";
    } else if ([moduleTypeID isEqualToString:@"3"]) {
        vc = [[SocialStreamContainerController alloc] init];
        type = @"3";
        details = @"Opens Social Stream module in Apple iPad";
    } else if ([moduleTypeID isEqualToString:@"4"]) {
        vc = [[SchoolStreamViewController alloc] init];
        type = @"4";
        details = @"Opens School Stream module in Apple iPad";
    } else if ([moduleTypeID isEqualToString:@"12"]) {
        vc = [[LessonPlanContainer alloc] init];
        type = @"12";
        details = @"Opens Lesson Plan module in Apple iPad";
        
//    } else if ([moduleName isEqualToString:kModuleAwardBadges]) {
//        vc = [[AwardBadgesViewController alloc] init];
//        details = @"Opens Award Badges in Apple iPad";
        
    } else if ([moduleTypeID isEqualToString:@"6"]) {
        vc = [[CourseContainerViewController alloc] init];
        type = @"6";
        details = @"Opens Course module in Apple iPad";
    } else if ([moduleTypeID isEqualToString:@"22"]) {
        vc = [[CurriculumPlannerContainer alloc] init];
        type = @"22";
        details = @"Opens Curriculum module in Apple iPad";
    }
    //    else if ( [moduleName isEqualToString:@"Grade Book Reports"] ) {
    //        vc = [[ReportsContainerController alloc] init];
    //        type = @"23";
    //        details = @"Opens Reports module in Apple iPad";
    //    }
    
    else if ( [moduleTypeID isEqualToString:@"23"] ) {
        vc = [[ReportsContainerController alloc] init];
        type = @"23";
        details = @"Opens Reports module in Apple iPad";
    }
    
    NSDictionary *info = @{ @"module_type" : type, @"details" : details };
    [self sendActivityLog:info];
    
    [self.navigationController pushViewController:vc animated:YES];
    dispatch_async(dispatch_get_main_queue(), ^{
        [HUD hideUIBlockingIndicator];
    });
    
}


-(void) testShowImport {
    if (kShowTestImportWindow == 1) {
        ImportViewController *vc = [[ImportViewController alloc] initWithNibName:@"ImportViewController" bundle:nil];
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        vc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        
        [self presentViewController:vc animated:YES completion:nil];
    }
}

#pragma mark - Notifications

-(void) refreshBooks:(NSNotification *) notification
{
    NSArray *books = [[notification object] copy];
    VLog(@"Books: \n%@", books);
    
    
    NSArray *importedBooks = [VSmartHelpers getImportedBooks];
    for (Book *book in importedBooks) {
        NSString *bookIdAsText = [NSString stringWithFormat:@"%@", book.bookId];
        [VSmartHelpers removeImportedBook:bookIdAsText];
    }
    
    if ([books count] > 0) {
        
        // The hud will dispable all input on the view (use the higest view possible in the view hierarchy)
        mbHUD = [[MBProgressHUD alloc] initWithView:self.view];
        
        // Add HUD to screen
        [self.view addSubview:mbHUD];
        
        // Regisete for HUD callbacks so we can remove it from the window at the right time
        mbHUD.delegate = self;
        
        [mbHUD showWhileExecuting:@selector(syncImportedBooks) onTarget:self withObject:nil animated:YES];
    }
}

-(void) syncImportedBooks {
    NSString *epubPath = [Utils appImportDirectory];
    //VLog(@"EPUB Dir: %@", epubPath);
    
    NSString *notitleLabel = NSLocalizedString(@"No Title", nil);
    NSString *unknownPublisherLabel = NSLocalizedString(@"<Unknown Publisher>", nil);
    NSString *importingBookLabel = NSLocalizedString(@"Importing books", nil);
    
    mbHUD.mode = MBProgressHUDModeDeterminate;
    mbHUD.labelText = importingBookLabel;
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *dirContents = [fm contentsOfDirectoryAtPath:epubPath error:nil];
    
    NSMutableArray *bookList = [[NSMutableArray alloc] init];
    
    for (NSString *subpath in dirContents) {
        if ( ![[subpath lastPathComponent] hasPrefix:@"."]) {
            if ([[[subpath pathExtension] lowercaseString] isIn:kBookExtensionVibe, kBookExtensionEpub, nil]) {
                [bookList addObject:subpath];
            }
        }
    }
    
    [bookList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *epubFile = [epubPath stringByAppendingFormat:@"/%@", obj];
        mbHUD.labelText = [NSString stringWithFormat:@"Adding [%@] book", obj];
        float currProgress = (float)((int) idx + 1) / (float)[bookList count];
        mbHUD.progress = currProgress;
        
        //NSString *uuid = [[NSProcessInfo processInfo] globallyUniqueString];
        //NSArray *splittedUUID = [uuid componentsSeparatedByString:@"-"];
        NSString *bookId = [NSString stringWithFormat:@"%@-%@-%@",[Utils randomNumber],[Utils randomNumber],[Utils randomNumber]];
        [Utils unzipEpubFile: bookId withFullPath:epubFile];
        
        EPub *epub = [[EPub alloc] initWithBookname:bookId];
        //NSDictionary *bookInfo = @{@"title": epub.title, @"authors_text": epub.authors, @"book_id": bookId};
        
        Book *book = [[Book alloc] init];
        book.bookId = bookId;
        book.title = epub.title;
        book.publisher = epub.publisher;
        book.purchaseDate = [[NSDate date] timeIntervalSinceNow];
        book.price = 0.0f;
        book.mediaType = kBookExtensionEpub;
        
        NSString *coverImagePath = [NSString stringWithFormat:@"%@/%@", epub.OPFRootPath, epub.coverImagePath];
        VLog(@"coverImagePath: %@", coverImagePath);
        book.cover = coverImagePath;
        
        NSArray<Author> *authors = (NSArray<Author>*)[NSArray arrayWithArray:epub.authorsList];
        book.authors = authors;
        
        VLog(@"Authors: %@", authors);
        
        //book._authorsText = epub.authors;
        
        NSInteger epubFileSize = [[Utils getFileSize:epubFile] integerValue];
        book.estimatedFileSize = epubFileSize;
        
        VLog(@"ImportedBook: %@", book);
        
        Metadata *meta = [[Metadata alloc] init];
        meta.title = IsEmpty(epub.title) ? notitleLabel : epub.title;
        meta.publisher = IsEmpty(epub.publisher) ? unknownPublisherLabel : epub.publisher;
        meta.authors = epub.authors;
        meta.coverImageUrl = coverImagePath;
        meta.uuid = epub.uuid;
        
        [VSmartHelpers addImportedBook:book];
        
        //AFTER DOWNLOAD SAVE ALL NECESSART INFORMATION
        [VSmartHelpers saveDRMinfoForBook:bookId drmversion:epub.drmVersion];
        
        [VSmartHelpers addBookToCloud:meta];
        [VSmartHelpers addBookExerciseToCloud:bookId forBook:meta.uuid];
        
        // Does this book has exercise data?
        NSDictionary *exerciseData = [Utils getExerciseFileContents:bookId];
        if (exerciseData != nil) {
            VLog(@"BOOK_HAS_EXERCISE");
            [VSmartHelpers saveBookExerciseData:bookId forExercise:exerciseData];
            //Test It
            //NSArray *allBookExercises = [VSmartHelpers getBookExercises];
            //VLog(@"Exercises: %@", allBookExercises);
        }
    }];

    // Test: Check Imported Books
    //VLog(@"Total Imported Books So Far: %i", [[VSmartHelpers getImportedBooks] count]);
    //[self refresh];
    [Utils clearImportDirectory];
    
    mbHUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    mbHUD.mode = MBProgressHUDModeCustomView;
    
    /* localizable strings */
    mbHUD.labelText = NSLocalizedString(@"Import Completed", nil); //checked
}

- (void) syncBooksFromDocuments: (NSNotification *) notification {
    
    NSMutableSet *books = [[notification object] copy];
    if ([books count] > 0) {
        
        dispatch_async(dispatch_get_main_queue(),^{
            NSString *epubPath = [Utils appDocumentsDirectory];
            
            [books enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
                NSString *epubFile = [epubPath stringByAppendingFormat:@"/%@", obj];
                NSString *bookId = [NSString stringWithFormat:@"%@-%@-%@",[Utils randomNumber],[Utils randomNumber],[Utils randomNumber]];
                
                /* localizable strings */
                NSString *importNewBooks = NSLocalizedString(@"Importing new book", nil); //checked
                [ProgressHUD show:importNewBooks Interaction:NO];
                
                NSString *extension = [[epubFile lowercaseString] pathExtension];
                if ([extension isIn:kBookExtensionEpub, kBookExtensionVibe, nil]) {
                    [self processEpubForExtraction:bookId forEpubPath:epubFile];
                } else if ([extension isIn:kBookExtensionPdf, nil]) {
                    [self processPdfForExtraction:bookId forPDFPath:epubFile];
                }
            }];

            /* localizable strings */
            NSString *importSuccess = NSLocalizedString(@"Book(s) imported successfully!", nil); //checked
            [ProgressHUD showSuccess:importSuccess];
        });
    }
}

- (void) processPdfForExtraction: (NSString *) folderName forPDFPath: (NSString *) pdfFile {
    NSURL *url = [NSURL fileURLWithPath:pdfFile];
    NSDictionary *data = [Utils getPdfMetadata:url forPDFFile:pdfFile withFolderName:folderName];
    VLog(@"PDF_DATA: %@", data);
    
    NSString *folderPath = [Utils createBookFolder:folderName];
    
    UIImage *pdfCover = [UIImage imageFromPDF:url];
    NSString *imagePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", folderName, @"png"]];
    [UIImagePNGRepresentation(pdfCover) writeToFile:imagePath atomically:NO];
    
    NSError *error = nil;
    NSString *pdfFilePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf", folderName]];
    [[NSFileManager defaultManager] copyItemAtPath:pdfFile toPath:pdfFilePath error:&error];
    
    Book *book = [[Book alloc] init];
    book.bookId = folderName;
    book.title = [data objectForKey:@"title"];
    book.publisher = @"<Unknown Publisher>";
    book.purchaseDate = [[NSDate date] timeIntervalSinceNow];
    book.price = 0.0f;    
    book.cover = imagePath;
    book.mediaType = kBookExtensionPdf;
    
    Author *author = [[Author alloc] init];
    author.authorName = [data objectForKey:@"authors_text"];
    //VLog(@"AUTHOR_NAME: %@", author.authorName);
    
    NSArray<Author> *defaultAuthor = (NSArray<Author>*)[NSArray arrayWithObject:author];
    book.authors = defaultAuthor;
    
    book.estimatedFileSize = [[Utils getFileSize:pdfFilePath] integerValue];
    
    if (error) {
        VLog(@"ERR: %@", [error localizedDescription]);
    } else {
        
        //UPDATE ON IMPORT ---------------------
        AccountInfo *account = [self account];
//        NSString *userid_path = VS_FMT(@"%@_%i", account.user.email, account.user.id);
        NSString *userid_path = VS_FMT(@"%@", account.user.email);
        NSDictionary *item = @{@"download_active": @NO,
                               @"book_id": book.bookId,
                               @"book": book,
                               @"progress": [NSNumber numberWithFloat:0],
                               @"download_status": [NSNumber numberWithUnsignedInteger:VSmartDownloadImported],
                               @"user_id" : userid_path};
        [self.rm updateResourceWithObject:item import:YES];
        //UPDATE ON IMPORT ---------------------
        
        [VSmartHelpers addImportedBook:book];
        [self deleteTMPFile:pdfFile];
    }
}

- (void) processPdfForExtraction: (NSString *) folderName forPDFPath: (NSString *) pdfFile book: (Book *)book {
    NSURL *url = [NSURL fileURLWithPath:pdfFile];
    NSDictionary *data = [Utils getPdfMetadata:url forPDFFile:pdfFile withFolderName:folderName];
    VLog(@"PDF_DATA: %@", data);
    
    NSString *folderPath = [Utils createBookFolder:folderName];
    
    UIImage *pdfCover = [UIImage imageFromPDF:url];
    NSString *imagePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", folderName, @"png"]];
    [UIImagePNGRepresentation(pdfCover) writeToFile:imagePath atomically:NO];
    
    NSError *error = nil;
    NSString *pdfFilePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf", folderName]];
    [[NSFileManager defaultManager] copyItemAtPath:pdfFile toPath:pdfFilePath error:&error];
        
    Author *author = [[Author alloc] init];
    author.authorName = [data objectForKey:@"authors_text"];    
    book.estimatedFileSize = [[Utils getFileSize:pdfFilePath] integerValue];
    
    if (error) {
        VLog(@"ERR: %@", [error localizedDescription]);
    } else {
        
        //UPDATE ON IMPORT ---------------------
        AccountInfo *account = [self account];
        //        NSString *userid_path = VS_FMT(@"%@_%i", account.user.email, account.user.id);
        NSString *userid_path = VS_FMT(@"%@", account.user.email);
        NSDictionary *item = @{@"download_active": @NO,
                               @"book_id": book.bookId,
                               @"book": book,
                               @"progress": [NSNumber numberWithFloat:0],
                               @"download_status": [NSNumber numberWithUnsignedInteger:VSmartDownloadImported],
                               @"user_id" : userid_path};
        [self.rm updateResourceWithObject:item import:YES];
        //UPDATE ON IMPORT ---------------------
        
        [VSmartHelpers addImportedBook:book];
        [self deleteTMPFile:pdfFile];
    }
}


- (void) processEpubForExtraction: (NSString *) folderName forEpubPath: (NSString *)epubFile {
    
    [Utils unzipEpubFile: folderName withFullPath:epubFile];
    
    /* localize string */
    NSString *notitleLabel = NSLocalizedString(@"No Title", nil);
    NSString *unknownPublisherLabel = NSLocalizedString(@"<Unknown Publisher>", nil);
    NSString *unknownAuthorLabel = NSLocalizedString(@"Author Unknown", nil);
    
    EPub *epub = [[EPub alloc] initWithBookname:folderName];
   
    Book *book = [[Book alloc] init];
    book.bookId = folderName;
    book.title = epub.title;
    book.publisher = epub.publisher;
    book.purchaseDate = [[NSDate date] timeIntervalSinceNow];
    book.price = 0.0f;
    book.mediaType = kBookExtensionEpub;
    book.sku = folderName;
    
    NSString *coverImagePath = [NSString stringWithFormat:@"%@/%@", epub.OPFRootPath, epub.coverImagePath];
    book.cover = coverImagePath;
    
    NSArray<Author> *authors = (NSArray<Author>*)[NSArray arrayWithArray:epub.authorsList];
    Author *author = [[Author alloc] init];
    author.authorName = unknownAuthorLabel;
    
    NSArray<Author> *defaultAuthor = (NSArray<Author>*)[NSArray arrayWithObject:author];
    book.authors = IsEmpty(authors) ? defaultAuthor : authors;
    
    book.estimatedFileSize = [[Utils getFileSize:epubFile] integerValue];
    
    Metadata *meta = [[Metadata alloc] init];
    meta.title = IsEmpty(epub.title) ? notitleLabel : epub.title;
    meta.publisher = IsEmpty(epub.publisher) ? unknownPublisherLabel : epub.publisher;
    meta.authors = epub.authors;
    meta.coverImageUrl = coverImagePath;
    meta.uuid = epub.uuid;
    
    //UPDATE ON IMPORT ---------------------
    AccountInfo *account = [self account];
//    NSString *userid_path = VS_FMT(@"%@_%i", account.user.email, account.user.id);
    NSString *userid_path = VS_FMT(@"%@", account.user.email);
    NSDictionary *item = @{@"download_active": @NO,
                           @"book_id": book.bookId,
                           @"book": book,
                           @"progress": [NSNumber numberWithFloat:0],
                           @"download_status": [NSNumber numberWithUnsignedInteger:VSmartDownloadImported],
                           @"user_id" : userid_path};
    [self.rm updateResourceWithObject:item import:YES];
    //UPDATE ON IMPORT ---------------------
    
    [VSmartHelpers addImportedBook:book];
    
    //AFTER DOWNLOAD SAVE ALL NECESSART INFORMATION
    [VSmartHelpers saveDRMinfoForBook:book.bookId drmversion:epub.drmVersion];
    
    [VSmartHelpers addBookToCloud:meta];
    [VSmartHelpers addBookExerciseToCloud:folderName forBook:meta.uuid];
    
    // Does this book has exercise data?
    NSDictionary *exerciseData = [Utils getExerciseFileContents:folderName];
    if (exerciseData != nil) {
        VLog(@"BOOK_HAS_EXERCISE");
        [VSmartHelpers saveBookExerciseData:folderName forExercise:exerciseData];
    }
    
    [self deleteTMPFile:epubFile];
}

-(void) _refreshGridSize: (NSNotification *) notification {
    VLog(@"_refreshGridSize");
    [UIView animateWithDuration:0.45 animations:^{
        float profileY = [super profileHeight];
        float gridHeight = self.view.frame.size.height - ([super headerSize].size.height + profileY + [super toolbarSize].size.height);
        
        float gridY = [super headerSize].size.height + profileY;
        [_gmGridView setFrame:CGRectMake(0, gridY - 22, self.dashboardView.bounds.size.width, gridHeight + 44)];
        [_gmGridView setNeedsLayout];

        if (![self.navigationController.topViewController isMemberOfClass:[self class]]) {
            CGRect frame = self.baseProfileView.frame;
            frame.origin.y = - (218.0f - 92.0f);
            
            if (profileY == 0.0) {
                [self.baseProfileView setFrame:frame];
            } else {
                [self.baseProfileView setFrame:CGRectMake(0, 92.0f, self.view.frame.size.width, 218.0)];
            }
        }
     
    } completion:^(BOOL finished) {
        [super showOrHideMiniAvatar];
    }];
}

-(void) switchToModule: (NSNotification *) notification {
    NSString *title = (NSString *)[notification object];
    
//    [self pushToModule:title];
}

-(void) downloadDataForQueue: (NSNotification *) notification {
    NSDictionary *item = (NSDictionary *) [notification object];
//    VLog(@"Received Notif: %@", item);
    [self _addItemToDownloadQueue:item];
}

#pragma mark - Private Methods

- (NSArray *) _buildModules {

    NSArray *currentModule = nil; //RETURN OBJECT
    
    /* localizable strings */
    NSString *textBookLabel = NSLocalizedString(@"Textbook", nil); //checked
    NSString *gradeBookLabel = NSLocalizedString(@"Gradebook", nil); //checked
    NSString *notesLabel = NSLocalizedString(@"Notes", nil); //checked
    NSString *socialStreamLabel = NSLocalizedString(@"Social Stream", nil); //checked
    NSString *schoolStreamLabel = NSLocalizedString(@"School Stream", nil); //checked
    NSString *calendarLabel = NSLocalizedString(@"Calendar", nil); //checked
    NSString *playlistLabel = NSLocalizedString(@"Playlist", nil); //checked
    NSString *educationalappLabel = NSLocalizedString(@"Educational Apps", nil); //checked
    NSString *testguruLabel = NSLocalizedString(@"Test Guru", nil); //checked
    NSString *curriculumLabel = NSLocalizedString(@"Curriculum Planner", nil); //checked
    NSString *lessonplanLabel = NSLocalizedString(@"Lesson Plan", nil); //checked
    NSString *classAnalyticsLabel = NSLocalizedString(@"Class Analytics", nil); //checked
    NSString *coursesLabel = NSLocalizedString(@"Course", nil); //checked
//    NSString *gradeBookReportLabel = NSLocalizedString(@"Grade Book Reports", nil); //checked
    NSString *gradeBookReportLabel = NSLocalizedString(@"Gradebook Reports", nil); //checked
    
    AccountInfo *account = [super account];
    
    if (account != nil) {

        NSMutableArray *modules = [NSMutableArray array];
        
//        for (Module *m in account.userModules) {
//            NSLog(@"-----------------------------------------");
//            NSLog(@"XXXX module name %@", m.name);
//            NSLog(@"XXXX module code %@", m.code);
//            NSLog(@"XXXX module active %@", m.isActive);
//            NSLog(@"XXXX module type %@", m.moduleTypeId);
//            NSLog(@"XXXX module ordering %@", m.ordering);
//        }
        
        NSNumber *enable = [NSNumber numberWithBool:self.isConnnected];
        
        for (Module *m in account.userModules) {

            //TESTGURU MODULE
            if ([m.moduleTypeId isEqualToString:@"5"] && [m.isActive isEqualToString:@"1"]) {
                NSDictionary *testgurumodule = @{@"module_name": m.name,//@"Test Guru",
                                                 @"module_title": testguruLabel,
                                                 @"image": @"icn_quiz-guru",
                                                 @"enable": enable,
                                                 @"order":@(m.ordering.integerValue),
                                                 @"module_type_id":m.moduleTypeId};
                [modules addObject:testgurumodule];
            }
            
            //CURRICULUM PLANNER MODULE
            if ([m.moduleTypeId isEqualToString:@"22"] && [m.isActive isEqualToString:@"1"]) {
                NSDictionary *curriculummodule = @{@"module_name": m.name,//@"Curriculum Planner",
                                                 @"module_title": curriculumLabel,
                                                 @"image": @"icn_curriculum-planner",
                                                 @"enable": enable,
                                                   @"order":@(m.ordering.integerValue),
                                                   @"module_type_id":m.moduleTypeId};
                [modules addObject:curriculummodule];
            }
            
            //LESSON PLANNER MODULE
            if ([m.moduleTypeId isEqualToString:@"12"] && [m.isActive isEqualToString:@"1"]) {
                NSDictionary *lessonmodule = @{@"module_name": m.name,//@"Lesson Plan",
                                                 @"module_title": lessonplanLabel,
                                                 @"image": @"icn_subject@2x",
                                                 @"enable": enable,
                                               @"order":@(m.ordering.integerValue),
                                               @"module_type_id":m.moduleTypeId};
                [modules addObject:lessonmodule];
            }
            
            //TEXTBOOK MODULE
            if ([m.moduleTypeId isEqualToString:@"8"] && [m.isActive isEqualToString:@"1"]) {
                NSDictionary *textbookmodule = @{@"module_name": m.name,//@"Textbooks",
                                                 @"module_title": textBookLabel,
                                                 @"image": @"icn_textbooks",
                                                 @"enable": [NSNumber numberWithBool:YES],
                                                 @"order":@(m.ordering.integerValue),
                                                 @"module_type_id":m.moduleTypeId};
                [modules addObject:textbookmodule];
            }
            
            //GRADEBOOK MODULE
            if ([m.moduleTypeId isEqualToString:@"9"] && [m.isActive isEqualToString:@"1"]) {
                NSDictionary *gradebookmodule = @{@"module_name": m.name,//@"Gradebook",
                                                  @"module_title": gradeBookLabel,
                                                  @"image": @"icn_gradebook",
                                                  @"enable": enable,
                                                  @"order":@(m.ordering.integerValue),
                                                  @"module_type_id":m.moduleTypeId};
                [modules addObject:gradebookmodule];
            }
            
            //NOTES MODULE
            if ([m.moduleTypeId isEqualToString:@"7"] && [m.isActive isEqualToString:@"1"]) {
                NSDictionary *notesmodule = @{@"module_name": m.name,//@"Notes",
                                              @"module_title": notesLabel,
                                              @"image": @"icn_notes",
                                              @"enable": enable,
                                              @"order":@(m.ordering.integerValue),
                                              @"module_type_id":m.moduleTypeId};
                [modules addObject:notesmodule];
            }
            
            //SOCIALSTREAM MODULE
            if ([m.moduleTypeId isEqualToString:@"3"] && [m.isActive isEqualToString:@"1"]) {
                NSDictionary *socialmodule = @{@"module_name": m.name,//@"Social Stream",
                                               @"module_title": socialStreamLabel,
                                               @"image": @"icn_social-stream-student",
                                               @"enable": enable,
                                               @"order":@(m.ordering.integerValue),
                                               @"module_type_id":m.moduleTypeId};
                [modules addObject:socialmodule];
            }
            
            //SCHOOLSTREAM MODULE
            if ([m.moduleTypeId isEqualToString:@"4"] && [m.isActive isEqualToString:@"1"]) {
                NSDictionary *schoolmodule = @{@"module_name": m.name,//@"School Stream",
                                               @"module_title": schoolStreamLabel,
                                               @"image": @"icn_school-stream",
                                               @"enable": enable,
                                               @"order":@(m.ordering.integerValue),
                                               @"module_type_id":m.moduleTypeId};
                [modules addObject:schoolmodule];
            }
            
            //CALENDAR MODULE
            if ([m.moduleTypeId isEqualToString:@"10"] && [m.isActive isEqualToString:@"1"]) {
                NSDictionary *calendarmodule = @{@"module_name": m.name,//@"Calendar",
                                                 @"module_title": calendarLabel,
                                                 @"image": @"icn_calendar",
                                                 @"enable": enable,
                                                 @"order":@(m.ordering.integerValue),
                                                 @"module_type_id":m.moduleTypeId};
                [modules addObject:calendarmodule];
            }
            
            //PLAYLIST MODULE
            if ([m.moduleTypeId isEqualToString:@"14"] && [m.isActive isEqualToString:@"1"]) {
                NSDictionary *playlistmodule = @{@"module_name": m.name,//@"Playlists",
                                                 @"module_title": playlistLabel,
                                                 @"image": @"icn_playlist",
                                                 @"enable": enable,
                                                 @"order":@(m.ordering.integerValue),
                                                 @"module_type_id":m.moduleTypeId};
                [modules addObject:playlistmodule];
            }
            
            //EDUCATIONAL APP MODULE
            if ([m.moduleTypeId isEqualToString:@"11"] && [m.isActive isEqualToString:@"1"]) {
                NSDictionary *edumodule = @{@"module_name": m.name,//@"Educational Apps",
                                            @"module_title": educationalappLabel,
                                            @"image": @"icn_educational-apps",
                                            @"enable": enable,
                                            @"order":@(m.ordering.integerValue),
                                            @"module_type_id":m.moduleTypeId};
                [modules addObject:edumodule];
            }
            
            //COURSE MODULE
            if ([m.moduleTypeId isEqualToString:@"6"] && [m.isActive isEqualToString:@"1"]) {
                
//                if (![account.user.position isEquaxlToString:@"teacher"]) {
                    NSDictionary *coursemodule = @{@"module_name": m.name,//kModuleCourses,
                                                   @"module_title": coursesLabel,
                                                   @"image": @"img_course",
                                                   @"enable": enable,
                                                   @"order":@(m.ordering.integerValue),
                                                   @"module_type_id":m.moduleTypeId};
                    [modules addObject:coursemodule];
//                }
            }

            //ANALYTICS MODULE
            if ([m.moduleTypeId isEqualToString:@"13"] && [m.isActive isEqualToString:@"1"]) {
                NSDictionary *analyticsmodule = @{@"module_name": m.name,//@"Class Analytics",
                                                  @"module_title": classAnalyticsLabel,
                                                  @"image": @"icn_class-analytic",
                                                  @"enable": enable,
                                                  @"order":@(m.ordering.integerValue),
                                                  @"module_type_id":m.moduleTypeId};
                [modules addObject:analyticsmodule];
            }

            //GRADE BOOK REPORTS MODULE
            
            /*
             code = GRDRP;
             "default_image_url" = "/img/icon/gradebookreports.png";
             description = "<null>";
             "is_active" = 1;
             "module_type_id" = 23;
             name = "Gradebook Reports";
             ordering = 0;
             "thumb_url" = "img/icons/gradebookreports.png";
             */
            
//            if ([m.moduleTypeId isEqualToString:@"23"] && [m.isActive isEqualToString:@"1"]) {
//                NSDictionary *analyticsmodule = @{@"module_name": @"Grade Book Reports",
//                                                  @"module_title": gradeBookReportLabel,
//                                                  @"image": @"dashboardReportIcon",
//                                                  @"enable": enable,
//                                                  @"order":@(m.ordering.integerValue)};
//                [modules addObject:analyticsmodule];
//            }
            
            if ([m.moduleTypeId isEqualToString:@"23"] && [m.isActive isEqualToString:@"1"]) {
                NSDictionary *analyticsmodule = @{@"module_name": m.name,//@"Gradebook Reports",
                                                  @"module_title": gradeBookReportLabel,
                                                  @"image": @"dashboardReportIcon",
                                                  @"enable": enable,
                                                  @"order":@(m.ordering.integerValue),
                                                  @"module_type_id":m.moduleTypeId};
                [modules addObject:analyticsmodule];
            }
            
        }//for
        
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"order" ascending:NO];
        [modules sortUsingDescriptors: @[descriptor] ];
        
//        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
//        [modules sortUsingDescriptors:@[descriptor]];
        
        return modules;
    }

    return currentModule;
}

- (NSDictionary *) _createDownloadData: (BOOL) isDownloading forBook: (Book *) book
                    withDownloadStatus:(VSmartDownloadStatus) downloadStatus withProgress:(float) progress
                              forIndex: (int) index {
    NSDictionary *dict = @{@"download_active": isDownloading ? @YES : @NO, @"book_id": book.bookId,
                           @"book": book, @"progress": [NSNumber numberWithFloat:progress / 100.0],
                           @"row": [NSNumber numberWithInt: index ], @"download_status": [NSNumber numberWithUnsignedInteger:downloadStatus]};
    
    return dict;
}

-(void) _addItemToDownloadQueue:(NSDictionary *) item
{
    if ([self.downloadQueue containsObject:item] == NO) {
        [self.downloadQueue addObject:item];
    }
}

-(void) _downloadDataAtURLStart:(NSString*)urlString withItem:(NSDictionary *) item{

    VLog(@"Item: %@", urlString);
    Book *book = (Book *) [item objectForKey:@"book"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    // http://www.vibeapi.net/book/download/epub/33?auth_token=
    //BookListCell *tableViewCell = (BookListCell *)[tableViewPurchases cellForRowAtIndexPath:indexPath];
    
    dispatch_queue_t queue = dispatch_queue_create("com.vibe.technology.WORKERQUEUE", DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        GCNetworkDownloadRequest *request = [GCNetworkDownloadRequest requestWithURL:url];
        request.autoDeleteTMPFile = NO;
        request.loadWhileScrolling = YES;
        request.timeoutInterval = 60;
        request.continueInBackground = YES;
        request.progressHandler = ^(CGFloat progress){
            
            NSDictionary *dict = [self _createDownloadData:YES forBook:book withDownloadStatus:VSmartDownloadInProgress
                                              withProgress:progress
                                                  forIndex:[[item objectForKey:@"row"] intValue]];
            VS_NCPOST_OBJ(kNotificationUpdateQueue, dict)
            
        };
        request.errorHandler = ^(NSError *error, NSData *responseData) {
            VLog(@"BooklatanViewController:downloadDataAtURL: Error: %@", [error localizedDescription]);
            NSDictionary *dict = [self _createDownloadData:NO forBook:book withDownloadStatus:VSmartDownloadError
                                              withProgress:0.0
                                                  forIndex:[[item objectForKey:@"row"] intValue]];
            VS_NCPOST_OBJ(kNotificationUpdateQueue, dict)
        };
        request.downloadCompletionHandler = ^(NSString *filePath){
            
            int fileSize = [VSmartHelpers getFileSize:filePath];
            NSString *fileExtension = [NSString stringWithFormat:@"%@", filePath.pathExtension ];
            VLog(@"BooklatanViewController: Downloaded data (%i) bytes and saved at: %@.", fileSize, filePath);
            
//            NSDictionary *dict = [self _createDownloadData:YES forBook:book withDownloadStatus:VSmartDownloadZipExtraction
//                                              withProgress:100.0
//                                                  forIndex:[[item objectForKey:@"row"] intValue]];
            
            if ([fileExtension isEqualToString:@"html"] || fileSize <= 100) {
                NSDictionary *dict = [self _createDownloadData:YES
                                                       forBook:book
                                            withDownloadStatus:VSmartDownloadError
                                                  withProgress:0
                                                      forIndex:[[item objectForKey:@"row"] intValue]];
                VS_NCPOST_OBJ(kNotificationUpdateQueue, dict)
                
            }

            //////////////////////////
            // [ VIBE | EPUB ] file
            //////////////////////////
            
            if ( [fileExtension isEqualToString:@"vibe"] || [fileExtension isEqualToString:@"epub"]  ) {
                
                NSDictionary *dict = [self _createDownloadData:YES forBook:book withDownloadStatus:VSmartDownloadZipExtraction
                                                  withProgress:100.0
                                                      forIndex:[[item objectForKey:@"row"] intValue]];
                
                
                VS_NCPOST_OBJ(kNotificationUpdateQueue, dict)
                
                dict = [self _createDownloadData:NO forBook:book withDownloadStatus:VSmartDownloadExtracted
                                    withProgress:100.0
                                        forIndex:[[item objectForKey:@"row"] intValue]];
                
                NSDictionary *data = @{@"cell_data": dict, @"book": book, @"filePath": filePath};
                
                [self processDownloadedFileForOperation:data];

            }
            
            ////////////////////////
            // PDF file
            ////////////////////////

            if ( [fileExtension isEqualToString:@"pdf"] ) {
                
                NSString *book_guid = [NSString stringWithFormat:@"%@", book.bookId];
//                NSString *book_guid = [NSString stringWithFormat:@"%@", [book.sku stringFromMD5]];
//                book.bookId = [NSString stringWithFormat:@"%@", book_guid];
                
                NSDictionary *dict = [self _createDownloadData:NO forBook:book withDownloadStatus:VSmartDownloadImported
                                                  withProgress:100.0
                                                      forIndex:[[item objectForKey:@"row"] intValue]];
                
                VS_NCPOST_OBJ(kNotificationUpdateQueue, dict)
                
                dispatch_async(_mainQueue, ^{
                    
                    dispatch_async(dispatch_get_main_queue(),^{
//                        [self processPdfForExtraction:book_guid forPDFPath:filePath];
                        [self processPdfForExtraction:book_guid forPDFPath:filePath book:book];
                    });
                    
                });
                
            }
            
//            else {
//                VS_NCPOST_OBJ(kNotificationUpdateQueue, dict)
//                dict = [self _createDownloadData:NO forBook:book withDownloadStatus:VSmartDownloadExtracted
//                                    withProgress:100.0
//                                        forIndex:[[item objectForKey:@"row"] intValue]];
//                
//                NSDictionary *data = @{@"cell_data": dict, @"book": book, @"filePath": filePath};
//                [self processDownloadedFileForOperation:data];
//            }
        };
        
        [request start];
    });
}


#pragma mark - Post Processing of Book Downlaods and NSOperation
-(void) processDownloadedFileDecompression: (id) data {
    VLog(@"processDownloadedFileOperation()");
    
    NSDictionary *info = (NSDictionary *) data;
    Book *book = (Book *)[info objectForKey:@"book"];
    
    NSString *filePath = [info objectForKey:@"filePath"];
    NSString *completedBookId = VS_FMT(@"%@", book.bookId);
    [Utils unzipEpubFile:completedBookId withFullPath:filePath];
    
    book.mediaType = kBookExtensionEpub;
    [VSmartHelpers addBook:book];
    
    EPub *epub = [[EPub alloc] initWithBookname:completedBookId];
    NSString *uuid = epub.uuid;
    if (!IsEmpty(uuid)) {
        Metadata *meta = [[Metadata alloc] init];
        meta.uuid = uuid;
        meta.title = book.title;
        meta.publisher = book.publisher;
        meta.authors = [VSmartHelpers getAuthors:book.authors];
        
        //AFTER DOWNLOAD SAVE ALL NECESSART INFORMATION
        [VSmartHelpers saveDRMinfoForBook:uuid drmversion:epub.drmVersion];
        
        [VSmartHelpers addBookToCloud:meta];
        [VSmartHelpers addBookExerciseToCloud:completedBookId forBook:uuid];
        
        NSDictionary *exerciseData = [Utils getExerciseFileContents:completedBookId];
        if (exerciseData != nil) {
            VLog(@"BOOK_HAS_EXERCISE_IN_DOWNLOAD");
            [VSmartHelpers saveBookExerciseData:completedBookId forExercise:exerciseData];
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self notifyAndRefreshPurchaseView:data];
    });

}

- (void)processDownloadedFileForOperation: (id) item {
    dispatch_async(_mainQueue, ^{
        [self processDownloadedFileDecompression:item];
    });
}

-(void) notifyAndRefreshPurchaseView: (id) data {
    VLog(@"notifyAndRefreshPurchaseView()");
    NSDictionary *info = (NSDictionary *) data;
    NSString *filePath = [info objectForKey:@"filePath"];
    NSString *cellData = [info objectForKey:@"cell_data"];
    
    [self deleteTMPFile:filePath];
    VS_NCPOST_OBJ(kNotificationUpdateQueue, cellData)
}

-(void)deleteTMPFile: (NSString *) filePath {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
        NSFileManager *manager = [[NSFileManager alloc] init];
        
        NSError *error = nil;
        [manager removeItemAtPath:filePath error:&error];
        
        if (error && [manager fileExistsAtPath:filePath]) {
            [manager createFileAtPath:filePath contents:[NSData data] attributes:nil];
            [manager removeItemAtPath:filePath error:nil];
        }
    });
}

- (void)postLogout {
    
    if ([self.navigationController.topViewController isMemberOfClass:[self class]]) {
        [self showLogin];
    }
}

- (void) showLogin {
    [self.timer invalidate];
    
    if(![super account]) {
        //NOTE: Making this block re-usable
        LoginViewController *vc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        vc.delegate = self;
        vc.previousAccount = (AccountInfo *)[Utils getArchive:kPreviousAccount];
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        vc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self presentViewController:vc animated:YES completion:nil];
    }
}

- (void)changePasswordNotification:(NSNotification *)notif {
    [super signout];
}

-(void) _showUserInfo {
    //AccountInfo *account = [super account];
    //VLog(@"Token: %@", account.device.token);
    
    //Account *account = [[VSmart sharedInstance] account];
    //VLog(@"UserDict: %@", [account toJSONString]);
    
    [self testShowImport];
}

// --------------------------
// PEARSON SPECIFIC
// --------------------------

- (void) downloadModuleTypes
{
    // REQUEST MODULE TYPES
    dispatch_queue_t queue = dispatch_queue_create("com.pearson.course.MODULES",DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        
        NSString *details = @"Login successful via Apple iPad";
        [self.rm requestLogActivityWithModuleType:@"2" details:details];
        
//        [self.rm requestModuleTypes:^(BOOL status) {
//            NSLog(@"[PEARSON] MODULES DOWNLOADED SUCCESSFULLY");
//            NSString *details = @"Login successful via Apple iPad";
//            [self.rm requestLogActivityWithModuleType:@"2" details:details];
//        }];
    });
}


#pragma mark - LoginViewController Delegate
-(void) didFinishLogin:(LoginViewController *)controller withStatus:(BOOL)success andResult:(id)result {
    //VS_SET_BOOL(kLoginState, success);
    //VS_SYNC;
    
    if (success) {
        
        [self.rm requestServerVersionBlock:^(BOOL status) {
            NSLog(@"VERSION CHECK...");
            CGFloat version = [[Utils getServerInstanceVersion] floatValue];
            NSLog(@"TEST GURU SERVER VERSION : %@", @(version) );
            self.isVersion25 = (version >= VSMART_SERVER_MAX_VER);
            
            if (self.isVersion25) {
                [self.gnm requestGlobalNotificationsV2WithPaginationWithLastID:0 :^(NSString *error) {
                    
                }];
            }
        }];
        
        [self downloadModuleTypes];
        [self setupData];
        [_gmGridView reloadData];
        self.loginVisible = NO;
        [controller dismissViewControllerAnimated:YES completion:^{
            [super reloadAvatar];
        }];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    VLog(@"viewWillAppear");
    
    // Bug #1152
    // jca-05112016
    // Update User Profile (BaseView)
    [super reloadAvatar];
    
    
    if (self.isVersion25) {
        [self startPollingNotificationsV2];
    } else {
        [self startPollingNotifications:NO];
    }
    
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self showLogin];    
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    _gmGridView = nil;
    // Dispose of any resources that can be recreated.
}

//////////////////////////////////////////////////////////////
#pragma mark GMGridViewDataSource
//////////////////////////////////////////////////////////////

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return [_currentData count];
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    //return CGSizeMake(253, 220);
    return CGSizeMake(253, 214);
}

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    //NSLog(@"Creating view indx %d", index);
    
    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    //VLog(@"Grid Size: %@", NSStringFromCGSize(size));
    GMGridViewCell *cell = [gridView dequeueReusableCell];
    NSDictionary *module = [_currentData objectAtIndex:index];

    NSString *moduleTitle = [module valueForKey:@"module_title"];
    
    if (!cell) {
        cell = [[GMGridViewCell alloc] init];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        view.layer.masksToBounds = NO;
        view.layer.cornerRadius = 2;
        cell.contentView = view;
    }
    
    BOOL enable = [module[@"enable"] boolValue];
    cell.contentView.backgroundColor = (enable) ? UIColorFromHex(0xffffff) : UIColorFromHex(0xfffdfb);
    
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
//    CGRect imageFrame = CGRectMake(0, 0, 253, 214);
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:imageFrame];
//    imageView.contentMode = UIViewContentModeCenter;
//    UIImage *moduleImage = [UIImage imageNamed:[module valueForKey:@"image"]];
//    [imageView setImage:moduleImage];
//    [cell.contentView addSubview:imageView];

    CGRect imageFrame = CGRectMake(0, 0, (size.width/2)+30, (size.height/2)+30);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:imageFrame];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIImage *moduleImage = [UIImage imageNamed:[module valueForKey:@"image"]];
    [imageView setCenter:CGPointMake(size.width/2, size.height/2)];
    [imageView setImage:moduleImage];
    [cell.contentView addSubview:imageView];

    
    UILabel *label = [[UILabel alloc] initWithFrame:cell.contentView.bounds];
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    label.text = moduleTitle;
    label.font = [UIFont fontWithName:@"HelveticaNeue-Regular" size:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = UIColorFromHex(0x575757);
    [cell.contentView addSubview:label];
    
    CGRect labelPosition = label.frame;
    labelPosition.origin.y = 80;
    label.frame = labelPosition;
    
    return cell;
}


- (BOOL)GMGridView:(GMGridView *)gridView canDeleteItemAtIndex:(NSInteger)index
{
    return NO; //index % 2 == 0;
}

//////////////////////////////////////////////////////////////
#pragma mark GMGridViewActionDelegate
//////////////////////////////////////////////////////////////

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    
    NSString *indicatorString = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Loading", nil)];
    [HUD showUIBlockingIndicatorWithText:indicatorString];
    
    GMGridViewCell *cell = [gridView cellForItemAtIndex:position];
    UIGraphicsBeginImageContext(cell.bounds.size);
    [cell.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // 2.
    // Add the UIImageView inside the UIButton
    UIImageView *zoomView = [[UIImageView alloc] initWithFrame:cell.bounds];
    zoomView.image = img;
    [cell addSubview:zoomView];
    [cell bringSubviewToFront:zoomView];
    
    NSDictionary *module = [_currentData objectAtIndex:position];

    BOOL enable = [module[@"enable"] boolValue];
    
    [UIView animateWithDuration:0.35
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:(void (^)(void)) ^{
                         zoomView.alpha = 0.0;
                         zoomView.transform = CGAffineTransformMakeScale(2, 2);
                     }
                     completion:^(BOOL finished){
                         zoomView.transform = CGAffineTransformIdentity;
                         [zoomView removeFromSuperview];
                         
                         NSString *moduleName = [module objectForKey:@"module_name"];
                         NSString *moduleTypeID = [module objectForKey:@"module_type_id"];
                         
//                         if (![moduleName isEqualToString:@"Playlists"] &&
                         if (![moduleName isEqualToString:@"Award Badges"] &&
                             ![moduleName isEqualToString:@"Educational Apps"] &&
//                             ![moduleTitle isEqualToString:@"Award Badges"] &&
                             ![moduleName isEqualToString:@"Class Analytics"] &&
                             ![moduleName isEqualToString:@"Analytics"]) {
                             
                             if (enable) {
                                 
                                 
                                 if ([moduleName isEqualToString:@"Textbooks"]) {
                                     __weak typeof(self) wo = self;
                                     [self.rm requestBookList:^(NSArray *list) {
                                         dispatch_async(dispatch_get_main_queue(), ^{
                                             [wo pushToModuleWithTypeID:moduleTypeID];
                                         });
                                     }];
                                 }
                                 
                                 if (![moduleName isEqualToString:@"Textbooks"]) {
                                     [self pushToModuleWithTypeID:moduleTypeID];
                                 }

//                                 //ORIGINAL
//                                 [self pushToModule:moduleName];
                                 
                             } else {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [HUD hideUIBlockingIndicator];
                                 });
                             }
                         }
                         else
                         {
                             /* localizable strings */
                             NSString *notAvailable = NSLocalizedString(@"Not available yet", nil); //checked
                             [self.view makeToast:notAvailable duration:2.0f position:@"center"];
                             
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [HUD hideUIBlockingIndicator];
                             });
                         }
                     }];
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    NSLog(@"Tap on empty space");
}

- (void)GMGridView:(GMGridView *)gridView processDeleteActionForItemAtIndex:(NSInteger)index
{
 
}

//////////////////////////////////////////////////////////////
#pragma mark GMGridViewSortingDelegate
//////////////////////////////////////////////////////////////

- (void)GMGridView:(GMGridView *)gridView didStartMovingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = UIColorFromHex(0x2eaee2);
                         cell.contentView.layer.shadowOpacity = 0.7;
                     }
                     completion:nil
     ];
}

- (void)GMGridView:(GMGridView *)gridView didEndMovingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = [UIColor whiteColor];
                         cell.contentView.layer.shadowOpacity = 0;
                     }
                     completion:nil
     ];
}

- (BOOL)GMGridView:(GMGridView *)gridView shouldAllowShakingBehaviorWhenMovingCell:(GMGridViewCell *)cell atIndex:(NSInteger)index
{
    return YES;
}

- (void)GMGridView:(GMGridView *)gridView moveItemAtIndex:(NSInteger)oldIndex toIndex:(NSInteger)newIndex
{
    NSObject *object = [_currentData objectAtIndex:oldIndex];
    [_currentData removeObject:object];
    [_currentData insertObject:object atIndex:newIndex];
}

- (void)GMGridView:(GMGridView *)gridView exchangeItemAtIndex:(NSInteger)index1 withItemAtIndex:(NSInteger)index2
{
    [_currentData exchangeObjectAtIndex:index1 withObjectAtIndex:index2];
}

@end
