//
//  NFontSize.m
//  V-Smart
//
//  Created by VhaL on 5/2/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NFontSize.h"


@implementation NFontSize

@dynamic fontSizeId;
@dynamic fontSize;
@dynamic created;
@dynamic isRemoved;
@dynamic modified;

@end
