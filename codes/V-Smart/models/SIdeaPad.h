//
//  SIdeaPad.h
//  V-Smart
//
//  Created by VhaL on 3/5/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SIdeaPad : NSManagedObject

@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic, retain) NSNumber * colorId;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSNumber * isRemoved;

@end
