//
//  GNListViewController.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 17/01/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class GNListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {

    // MARK: - Data Manager
    
    fileprivate lazy var gnDataManager: GNDataManager = {
        let gndm = GNDataManager.sharedInstance
        return gndm
    }()
    
//    fileprivate lazy var rmDataManager: ResourceManager = {
//        return ResourceManager.sharedInstance()
//    }()
    
    fileprivate lazy var ssdm: SocialStreamDataManager = {
        let dataManager = SocialStreamDataManager.sharedInstance
        return dataManager
    }()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    
    var isRequesting: Bool = false
    
    fileprivate var tableRefreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.preferredContentSize = CGSize(width: 350, height: 400)
        
        // Pull to refresh
        let refreshLessonListAction = #selector(self.requestFirstPage)
        self.tableRefreshControl = UIRefreshControl.init()
        self.tableRefreshControl.addTarget(self, action: refreshLessonListAction, for: .valueChanged)
        self.tableRefreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: ""))
        self.tableView.insertSubview(self.tableRefreshControl, belowSubview: self.tableView)
        self.tableRefreshControl.superview?.sendSubview(toBack: self.tableRefreshControl)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadFetchedResultsController()
        self.requestNotifications(withLastID: 0)
    }
    
    func requestNotifications(withLastID lastID: Int) {
        self.isRequesting = true
        
        self.gnDataManager.requestGlobalNotificationsV2WithPagination(lastID: lastID) { (error) in
            if self.tableRefreshControl.isRefreshing {
                self.tableRefreshControl.endRefreshing()
            }
            
            self.delay(1, closure: { 
                self.isRequesting = false
            })
        }
    }
    
    func requestFirstPage() {
        self.requestNotifications(withLastID: 0)
    }
    
    // MARK: - PAGINATION
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(tableView.contentOffset.y >= (tableView.contentSize.height - tableView.frame.size.height)) {
            if self.isRequesting == false {
                
                let numberOfRows: Int = self.tableView.numberOfRows(inSection: 0)
                let lastIndexPath: IndexPath = IndexPath(row: numberOfRows - 1, section: 0)
                
                let mo = fetchedResultsController.object(at: lastIndexPath)
                let sort_id: Int = Int(mo.value(forKeyPath: "sort_id") as! Double)
                
                self.requestNotifications(withLastID: sort_id)
                print("BOTTOM BOTTOM BOTTOM!!!")
            }
        }
    }
    
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        
        if sectionData.numberOfObjects == 0 {
            self.emptyView.isHidden = false;
        } else {
            self.emptyView.isHidden = true;
        }
        
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notif_cell_id", for: indexPath) as! GNListTableViewCell
        cell.selectionStyle = .none
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: GNListTableViewCell, atIndexPath indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        
        let owner_fname: String = self.gnDataManager.stringValue(mo.value(forKeyPath: "owner_fname"))
        let owner_lname: String = self.gnDataManager.stringValue(mo.value(forKeyPath: "owner_lname"))
        let notif_message: String = self.gnDataManager.stringValue(mo.value(forKeyPath: "notif_message")).trimmingCharacters(in: .whitespacesAndNewlines)
        let avatar: String = self.gnDataManager.stringValue(mo.value(forKeyPath: "avatar"))
        let thumb_url: String = self.gnDataManager.stringValue(mo.value(forKeyPath: "thumb_url"))
        let formatted_date_created: String = self.gnDataManager.stringValue(mo.value(forKeyPath: "formatted_date_created"))
        let is_seen: String = self.gnDataManager.stringValue(mo.value(forKeyPath: "is_seen"))
        let module_name: String = self.gnDataManager.stringValue(mo.value(forKeyPath: "module_name"))
        
        let module_type_id: String = self.gnDataManager.stringValue(mo.value(forKeyPath: "module_type_id"))
        
        
        let owner_fullname: String = "\(owner_fname) \(owner_lname)"
        var message = "<b>\(owner_fullname)</b>" + " \(notif_message) "// + " in <b>\(module_name)</b>."
        if module_type_id != "9" {
            message = message + " in <b>\(module_name)</b>."
        }
        
        let placeholderImage: UIImage = UIImage(named: "img_book-placeholder.png")!;
        cell.avatarImageView.clipsToBounds = true
        cell.avatarImageView.sd_setImage(with: URL(string: avatar), placeholderImage: placeholderImage)
        
        cell.notifMessageLabel.text = ""
        cell.notifMessageLabel.setHTMLFromString(text: message)
        cell.notifThumbnail.sd_setImage(with: URL(string: thumb_url))
        cell.notifDateTimeLabel.text = formatted_date_created
        
        cell.backgroundColor = (is_seen == "0") ? #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9450980392, alpha: 1) : UIColor.white;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mo = fetchedResultsController.object(at: indexPath)
        
        let is_seen: String = self.gnDataManager.stringValue(mo.value(forKeyPath: "is_seen"))
        
        if is_seen == "0" {
            let notif_id: String = self.gnDataManager.stringValue(mo.value(forKeyPath: "notif_id"))
            self.gnDataManager.postSeenNotification(withID: notif_id) { (error) in
                
            }
        }
        
        let module_type_id: String = self.gnDataManager.stringValue(mo.value(forKeyPath: "module_type_id"))
        
        if module_type_id == "1" { // MODAL SOCIAL STREAM/SCHOOL STREAM
            let reference_id: String = self.gnDataManager.stringValue(mo.value(forKeyPath: "reference_id"))
            
            self.ssdm.requestUserMessage(withMessageID: reference_id, clearEntities: false, handler: { (status) in
                if status {
                    self.performSegue(withIdentifier: "SEGUE_PREVIEW_POST", sender: reference_id)
                }
            })
//            self.rmDataManager.requestNotificationDetails(withMessageID: reference_id, doneBlock: { (status) in
//                self.performSegue(withIdentifier: "SEGUE_PREVIEW_POST", sender: reference_id)
//            })
        }
        
        if module_type_id != "1" { // OTHER MODULES
            self.dismiss(animated: true, completion: { 
                NotificationCenter.default.post(name: Notification.Name(rawValue: "NOTIFICATION_NAVIGATE_NOTIFICATION"), object: module_type_id)
            })
        }
        
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SEGUE_PREVIEW_POST" {
            let gnpp = segue.destination as? GNPreviewPostViewController
            gnpp?.message_id = sender as? String
        }
        
    }
    
    // MARK: - Fetched Results Controller
    
    fileprivate var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<NSManagedObject> = NSFetchRequest<NSManagedObject>(entityName: GNConstants.Entity.NOTIFICATION)
        
        let ctx = self.gnDataManager.getMainContext()
        
        //let fetchRequest = NSFetchRequest(entityName: kCoordinatorTestEntity)
        fetchRequest.fetchBatchSize = 20
        
        let loggedInUserID: String = self.gnDataManager.accountUserID()
        
        let predicate = NSComparisonPredicate(keyPath: "user_id", withValue: loggedInUserID, isExact: true)
        fetchRequest.predicate = predicate
        
        let sortDescriptor = NSSortDescriptor(key: "sort_id", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: ctx!, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        
        _fetchedResultsController = frc
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            abort()
        }
        
        return _fetchedResultsController!
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
        case .insert:
            self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                if let cell = self.tableView.cellForRow(at: indexPath) {
                    self.configureCell(cell as! GNListTableViewCell, atIndexPath: indexPath)
                }
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    func reloadFetchedResultsController() {
        self._fetchedResultsController = nil
        self.tableView.reloadData()
        
        do {
            try _fetchedResultsController!.performFetch()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
