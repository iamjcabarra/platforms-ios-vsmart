//
//  JMNNoteTag.h
//  V-Smart
//
//  Created by VhaL on 3/17/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"

@protocol JMNNoteTag
@end

@interface JMNNoteTag : JSONModel
@property (nonatomic, assign) int id;
@property (nonatomic, assign) int userNoteId;
@property (nonatomic, assign) int userTagId;
@property (nonatomic, assign) int isDeleted;
@property (nonatomic, strong) NSString *dateCreated;
@property (nonatomic, strong) NSString *dateModified;
@end

@interface JMNRecordNoteTag : JSONModel
@property (nonatomic, strong) NSArray<JMNNoteTag>* note_tags;
@end