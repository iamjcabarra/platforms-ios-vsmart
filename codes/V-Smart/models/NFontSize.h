//
//  NFontSize.h
//  V-Smart
//
//  Created by VhaL on 5/2/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface NFontSize : NSManagedObject

@property (nonatomic, retain) NSNumber * fontSizeId;
@property (nonatomic, retain) NSNumber * fontSize;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSNumber * isRemoved;
@property (nonatomic, retain) NSDate * modified;

@end
