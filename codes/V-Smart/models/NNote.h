//
//  Note.h
//  V-Smart
//
//  Created by VhaL on 2/26/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NoteBook, NoteTag;

@interface Note : NSManagedObject

@property (nonatomic, retain) NSNumber * colorId;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic, retain) NSNumber * syncId;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * isRemoved;
@property (nonatomic, retain) NSNumber * isShared;
@property (nonatomic, retain) NoteBook *folder;
@property (nonatomic, retain) NSSet *noteTags;

@property (nonatomic, retain) NSNumber * fontTypeId;
@property (nonatomic, retain) NSNumber * fontSizeId;
@property (nonatomic, retain) NSNumber * isArchive;

@end

@interface Note (CoreDataGeneratedAccessors)

-(void)addNoteTagsObject:(NoteTag *)value;
-(void)removeNoteTagsObject:(NoteTag *)value;
-(void)addNoteTags:(NSSet *)values;
-(void)removeNoteTags:(NSSet *)values;

@end
