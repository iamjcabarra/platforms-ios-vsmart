//
//  JMNNotebook.h
//  V-Smart
//
//  Created by VhaL on 3/17/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"

@protocol JMNNotebook
@end

@interface JMNNotebook : JSONModel
@property (nonatomic, assign) int id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) int bgColor;
@property (nonatomic, assign) int userId;
@property (nonatomic, assign) int isDeleted;
@property (nonatomic, strong) NSString *platform;
@property (nonatomic, strong) NSString *dateCreated;
@property (nonatomic, strong) NSString *dateModified;
@property (nonatomic, strong) NSString *noteBookID;
@end

@interface JMNRecordNotebook : JSONModel
@property (nonatomic, strong) NSArray<JMNNotebook>* notebooks;
@end