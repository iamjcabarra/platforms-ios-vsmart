//
//  JMNNote.m
//  V-Smart
//
//  Created by VhaL on 3/17/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JMNNote.h"

@implementation JMNNote
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end

@implementation JMNRecordNote
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end