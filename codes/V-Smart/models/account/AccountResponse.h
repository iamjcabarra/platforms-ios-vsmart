//
//  AccountResponse.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/30/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountInfo.h"
//#import "Error.h"
#import "ErrorModel.h"
#import "JSONModel.h"

@interface AccountResponse : JSONModel
@property (nonatomic, strong) AccountInfo *account;
//@property (nonatomic, strong) Error *error;
@property (nonatomic, strong) ErrorModel *error;
@end
