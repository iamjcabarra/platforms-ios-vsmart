//
//  Device.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/30/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "NSObject+Archiver.h"

@interface Device : JSONModel
@property (nonatomic, strong) NSString *deviceId;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, assign) int platformType;
@end
