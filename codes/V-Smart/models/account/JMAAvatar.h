//
//  Avatars.h
//  V-Smart
//
//  Created by VhaL on 4/21/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"

@protocol JMAAvatar
@end

@interface JMAAvatar : JSONModel
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *avatarUrl;
@property (nonatomic, strong) NSString *isDeleted;
@property (nonatomic, strong) NSString *dateCreated;
@property (nonatomic, strong) NSString *dateModified;
@end

@interface JMARecordAvatars : JSONModel
@property (nonatomic, strong) NSArray<JMAAvatar>* records;
@end