//
//  GuestAccount.h
//  V-Smart
//
//  Created by VhaL on 3/26/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"

@protocol GuestAccount
@end

@interface GuestAccount : JSONModel
@property (nonatomic, strong) NSDictionary *value;
@property (nonatomic, strong) NSString *name;
//@property (nonatomic, strong) NSString *key;
@end

//@interface RecordGuestAccount : JSONModel
//@property (nonatomic, strong) NSArray<GuestAccount>* records;
//@end