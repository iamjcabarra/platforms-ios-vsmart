//
//  GuestAccount.m
//  V-Smart
//
//  Created by VhaL on 3/26/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "GuestAccount.h"

@implementation GuestAccount
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
@end


//@implementation RecordGuestAccount
//+(JSONKeyMapper*)keyMapper
//{
//    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
//}
//@end