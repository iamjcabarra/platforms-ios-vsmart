//
//  Avatars.m
//  V-Smart
//
//  Created by VhaL on 4/21/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JMAAvatar.h"

@implementation JMAAvatar
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
@end

@implementation JMARecordAvatars
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
@end