//
//  User.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/30/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "NSObject+Archiver.h"

@interface User : JSONModel
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *firstname;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic, strong) NSString *middlename;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, strong) NSString *suffix;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, assign) int id;
@property (nonatomic, assign) int vibeId;
@property (nonatomic, assign) int status;
@property (nonatomic, strong) NSString *userSchoolyearId;
//@property (nonatomic, assign) int userSchoolyearId;
@property (nonatomic, strong) NSString *is_coordinator;
@end