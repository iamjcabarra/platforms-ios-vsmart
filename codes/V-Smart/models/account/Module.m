//
//  Module.m
//  V-Smart
//
//  Created by Ryan Migallos on 3/3/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import "Module.h"

@implementation Module
+ (JSONKeyMapper *)keyMapper {
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
@end