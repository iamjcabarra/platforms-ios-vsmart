//
//  Account.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 9/2/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "Device.h"
#import "User.h"
#import "Module.h"
#import "NSObject+Archiver.h"

@interface AccountInfo : JSONModel
@property (nonatomic, strong) Device *device;
@property (nonatomic, strong) User *user;
@property (nonatomic, strong) NSArray<Module>* userModules;
@end
