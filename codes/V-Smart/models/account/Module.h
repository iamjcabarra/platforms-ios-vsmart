//
//  Module.h
//  V-Smart
//
//  Created by Ryan Migallos on 3/3/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "NSObject+Archiver.h"

@protocol Module
@end

@interface Module : JSONModel

@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSString *defaultImageUrl;
@property (strong, nonatomic) NSString *isActive;
@property (strong, nonatomic) NSString *moduleTypeId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *ordering;
@property (strong, nonatomic) NSString *thumbUrl;

@end
