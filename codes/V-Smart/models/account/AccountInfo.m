//
//  Account.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 9/2/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "AccountInfo.h"

@implementation AccountInfo

+ (JSONKeyMapper *)keyMapper {
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}

@end
