//
//  JMNNotebook.m
//  V-Smart
//
//  Created by VhaL on 3/17/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JMNNotebook.h"

@implementation JMNNotebook
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end

@implementation JMNRecordNotebook
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end