//
//  Tag.m
//  V-Smart
//
//  Created by VhaL on 2/26/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NTag.h"
#import "NNoteTag.h"


@implementation Tag

@dynamic userId;
@dynamic created;
@dynamic modified;
@dynamic syncId;
@dynamic tagName;
@dynamic isRemoved;
@dynamic noteTags;

@end
