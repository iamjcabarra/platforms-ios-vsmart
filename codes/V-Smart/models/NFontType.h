//
//  NFontType.h
//  V-Smart
//
//  Created by VhaL on 5/2/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface NFontType : NSManagedObject

@property (nonatomic, retain) NSNumber * fontTypeId;
@property (nonatomic, retain) NSString * fontName;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSNumber * isRemoved;
@property (nonatomic, retain) NSDate * modified;

@end
