//
//  Color.m
//  V-Smart
//
//  Created by VhaL on 3/1/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NColor.h"


@implementation Color

@dynamic colorId;
@dynamic colorName;
@dynamic redValue;
@dynamic greenValue;
@dynamic blueValue;
@dynamic hexValue;
@dynamic created;
@dynamic modified;
@dynamic isRemoved;

@end
