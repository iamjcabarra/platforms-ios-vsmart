//
//  CustomJSONConverters.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/31/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "CustomJSONConverters.h"

@implementation JSONValueTransformer(NSDate)

-(NSDate*)NSDateFromNSNumber:(NSNumber *)number
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[number doubleValue]];
    return date;
}

-(id)JSONObjectFromNSDate:(NSDate*)date
{
    return @"date";
}

@end

@implementation JSONValueTransformer(NSString)
-(NSString*)NSStringFromNSNumber:(NSNumber*)number {
    NSString *fileSizeAsString = [Utils fileSizeToHumanReadable:number];
    return fileSizeAsString;
}

//-(id)JSONObjectFromNSString:(NSString*)filesize {
//    return @"filesize";
//}

@end