//
//  CustomJSONConverters.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/31/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONValueTransformer.h"

@interface JSONValueTransformer(NSDate)
-(NSDate*)NSDateFromNSNumber:(NSNumber*)number;
-(id)JSONObjectFromNSDate:(NSDate*)date;
@end

@interface JSONValueTransformer(NSString)
-(NSString*)NSStringFromNSNumber:(NSNumber*)number;
//-(id)JSONObjectFromNSString:(NSString*)filesize;
@end