//
//  NoteDataManager.h
//  V-Smart
//
//  Created by VhaL on 2/20/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NNote.h"
#import "NTag.h"
#import "NNoteBook.h"
#import "NNoteTag.h"
#import "NColor.h"
#import "NoteEntry.h"
#import "NFontType.h"
#import "NFontSize.h"

#import "JMNNotebook.h"
#import "JMNTag.h"
#import "JMNNote.h"
#import "JMNNoteTag.h"

extern NSString* const kNotebookGeneralName;

@interface NoteDataManager : NSObject


@property (nonatomic, assign) int isSyncing;
+ (NoteDataManager *) sharedInstance;

- (NSArray *)fetchObjectsForEntityName:(NSString *)entityName  useMainThread:(BOOL)useMainThread
                         withPredicate:(NSString*)stringFilter, ...;

-(void)sync;

////Online Calls
#pragma mark - Online API Notebooks
-(JMNRecordNotebook*)APIGetAllNotebooksByUserIdOutError:(NSError**)error statusCode:(NSNumber**)statusCode;
#pragma mark - Online API Tags
-(JMNRecordTag*)APIGetAllTagsByUserIdOutError:(NSError**)error statusCode:(NSNumber**)statusCode;
#pragma mark - Online API Notes
-(JMNRecordNote*)APIGetAllNotesByUserIdOutError:(NSError**)error statusCode:(NSNumber**)statusCode;

//Database Calls
#pragma mark - DB Notebooks
-(NSArray*)getNoteBooks:(BOOL)withRemovedItems useMainThread:(BOOL)useMainThread;
-(NoteBook*)getNoteBookByNoteBookName:(NSString*)folderName useMainThread:(BOOL)useMainThread;
-(NoteBook*)addNoteBook:(NSString*)folderName withColor:(int)colorId useMainThread:(BOOL)useMainThread;
-(BOOL)deleteNoteBook:(NoteBook*)folder useMainThread:(BOOL)useMainThread;
-(BOOL)editNoteBook:(NoteBook*)folder useMainThread:(BOOL)useMainThread;
-(BOOL)shareFolder:(NoteBook*)noteBook isShared:(BOOL)share useMainThread:(BOOL)useMainThread;
-(NoteBook *)getNoteBookByNoteBookID:(NSString *)noteBookID useMainThread:(BOOL)useMainThread;
-(void)moveNote:(Note *)note toNoteBook:(NoteBook *)noteBook useMainThread:(BOOL)useMainThread;

#pragma mark - DB Tag
-(NSArray*)getTags:(BOOL)withRemovedItems useMainThread:(BOOL)useMainThread;
-(Tag*)getTagByTagName:(NSString*)tagName useMainThread:(BOOL)useMainThread;
-(Tag*)addTag:(NSString*)tagName useMainThread:(BOOL)useMainThread;
-(BOOL)deleteTag:(Tag*)tag useMainThread:(BOOL)useMainThread;
-(BOOL)editTag:(Tag*)tag useMainThread:(BOOL)useMainThread;

#pragma mark - DB Note
-(NSArray*)getNotes:(BOOL)withRemovedItems useMainThread:(BOOL)useMainThread;
-(Note*)addNote:(NoteEntry*)entry useMainThread:(BOOL)useMainThread;
-(Note*)editNote:(NoteEntry*)entry sourceNote:(Note*)note useMainThread:(BOOL)useMainThread;
-(BOOL)deleteNote:(Note*)note useMainThread:(BOOL)useMainThread;
-(BOOL)editNote:(Note*)note useMainThread:(BOOL)useMainThread;

#pragma mark - DB Color
-(NSArray*)getColors:(BOOL)withRemovedItems useMainThread:(BOOL)useMainThread;
-(Color*)getColorByColorId:(int)colorId useMainThread:(BOOL)useMainThread;
-(Color*)addColor:(NSString*)colorName withColorId:(int)colorId withRed:(int)red withGreen:(int)green withBlue:(int)blue  withHex:(NSString*)hex useMainThread:(BOOL)useMainThread;
-(UIColor*)getUIColorFromColorObject:(Color*)color;

#pragma mark - Font Type
-(NSArray*)getFontTypes:(BOOL)withRemovedItems useMainThread:(BOOL)useMainThread;
-(NFontType*)getFontTypeByFontTypeId:(int)fontTypeId useMainThread:(BOOL)useMainThread;
-(NFontType*)addFontType:(NSString*)fontTypeName withFontTypeId:(int)fontTypeId useMainThread:(BOOL)useMainThread;

#pragma mark - Font Size
-(NSArray*)getFontSizes:(BOOL)withRemovedItems useMainThread:(BOOL)useMainThread;
-(NFontSize*)getFontSizeByFontSizeId:(int)fontSize useMainThread:(BOOL)useMainThread;
-(NFontSize*)addFontSize:(int)fontSizeValue withFontSizeId:(int)FontSizeId useMainThread:(BOOL)useMainThread;
@end
