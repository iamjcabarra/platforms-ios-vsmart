//
//  JMSEmoItem.h
//  V-Smart
//
//  Created by VhaL on 3/24/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"

@protocol JMSEmoItem
@end

@interface JMSEmoItem : JSONModel
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *chars;
@property (nonatomic, strong) NSString<Optional> *iconText;
@property (nonatomic, strong) NSString *iconUrl;
@property (nonatomic, strong) NSString *isDeleted;
@property (nonatomic, strong) NSString *dateCreated;
@property (nonatomic, strong) NSString *dateModified;
@end

@interface JMSRecordEmoItem : JSONModel
@property (nonatomic, strong) NSArray<JMSEmoItem>* records;
@end
