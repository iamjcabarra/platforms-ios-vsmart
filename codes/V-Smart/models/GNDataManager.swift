//
//  GNDataManager.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 16/01/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

@objc class GNDataManager : NSObject, Routes {
    
    var db:VSCoreDataStack!
    
    // MARK: - Singleton Method
    static let sharedInstance: GNDataManager = {
        let singleton = GNDataManager()
        return singleton
    }()
    
    override init() {
        self.db = VSCoreDataStack(name:"GlobalNotificationModel")
    }
    
    // ParseProtocol
    var isProductionMode: Bool = true
    
    // SOCKET IO
    var socket: SocketIOClient!
    
    // MARK: - Private Properties
    
    var dateFormatter: DateFormatter! = DateFormatter()
    var userDefaults: UserDefaults! = UserDefaults.standard
    
    fileprivate let session = URLSession.shared
    
    typealias GNDoneBlock = (_ doneBlock: Bool) -> Void
    typealias GNDictionaryBlock = (_ dictionary: [String:AnyObject]?) -> Void
    typealias GNListBlock = (_ listBlock: [[String:AnyObject]]?) -> Void
    typealias GNDoneBlockWithError = (_ done: Bool, _ error: String?) -> Void
    typealias GNErrorBlock = (_ error: String?) -> Void
    typealias GNDictionaryBlockWithError = (_ dictionary: [String:AnyObject]?, _ error: String?) -> Void
    
    // MARK: - Utility Methods (Non-Swift Dependency)
    func accountUserID() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.id)"
    }
    
    func accountUserEmail() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.email)"
    }
    
    func accountUserPosition() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.position)"
    }
    
    fileprivate func accountUserUserName() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.email!)"
    }
    
    fileprivate func accountUserAvatar() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.avatar!)"
    }
    
    fileprivate func getContext() -> NSManagedObjectContext! {
        return self.db.objectContext()
    }
    
    func getMainContext() -> NSManagedObjectContext! {
        return self.db.objectMainContext()
    }
    
    // MARK: - Socket IO
    func connectGlobalNotificationSocketV2() {
        
        if self.socket == nil {
            let url = URL(string: "http://\(self.retrieveVSmartBaseURL()):8000")!
            self.socket = SocketIOClient(socketURL: url, config: [.log(false), .forcePolling(true)])
        }
        
        if self.socket.status == .notConnected || self.socket.status == .disconnected {
        self.socket.on("connect") { data, ack in
            // notification
            self.socket.on("globalNotif") { (data, ack) in
                print("NOTIFICATION DAW :\(data)")
                self.handleGlobalNotificationListener(withData: data)
            }
        }
        
//        // debugger
//        self.socket.onAny { print("Got event: \($0.event), with items: \($0.items!)") }
        
        
        self.socket.connect()
        }
    }
    
    func handleGlobalNotificationListener(withData notifications: [Any]) {
        
        let notifDict = notifications.last as! [String:Any]
        let notifs: [[String:Any]] = notifDict["notifs"] as! [[String:Any]]
        
        let filteredNotifs = notifs.filter({
            let answer: String! = self.stringValue($0["user_id"])
            return answer == self.accountUserID()
        })
        
        if filteredNotifs.count > 0 {
            self.processArrayNotifications(notifications: filteredNotifs)
            
            if let unseen_count_int = self.fetchUserDefaultsObject(forKey: GNConstants.UserDefined.GNUNSEENCOUNT) as? Int {
                let incremented_unseen_count : Int = unseen_count_int + 1
                self.save(object: incremented_unseen_count, forKey: GNConstants.UserDefined.GNUNSEENCOUNT)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "NOTIFICATION_UPDATE_NOTIFICATION_BADGE"), object: nil)
            }
        }
    }
    
    func requestGlobalNotificationsV2(clearEntity: Bool, _ handler:@escaping GNErrorBlock) {
        
        let user_id: String = self.accountUserID()
        
        // NETWORKING REQUEST
        let endpoint = "/v2/notif/getnotif/user/\(user_id)"
        
        let request = route("GET", uri: endpoint, body: nil)
        if clearEntity {
            let ctx = self.getContext()
            _ = self.db.clearEntity(GNConstants.Entity.NOTIFICATION, ctx: ctx!, filter: nil)
        }
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                handler(e.localizedDescription)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:Any] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [[String:Any]] else {
                    handler("Something went wrong, please try again")
                    return
                }
                
                self.processArrayNotifications(notifications: records)
                handler(nil)
            }
            else {
                handler("Something went wrong, please try again")
            }
        })
        
        task.resume()
    }
    
    func requestGlobalNotificationsV2WithPagination(lastID: Int, _ handler:@escaping GNErrorBlock) {
        
        let user_id: String = self.accountUserID()
        
        // NETWORKING REQUEST
        let endpoint = "/v2/notif/getnotif/user/\(user_id)/\(lastID)"
        
        let request = route("GET", uri: endpoint, body: nil)
        
        if lastID == 0 {
            let ctx = self.getContext()
            _ = self.db.clearEntity(GNConstants.Entity.NOTIFICATION, ctx: ctx!, filter: nil)
        }
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                handler(e.localizedDescription)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:Any] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [String:Any] else {
                    handler("Something went wrong, please try again")
                    return
                }
                
                if let notifications = records["notifications"] as? [[String : Any]] {
                    self.processArrayNotifications(notifications: notifications)
                    handler(nil)
                }
                
                let unseen_count_string: String = self.stringValue(records["unseen"])
                let unseen_count_int: Int = Int(unseen_count_string)!
                self.save(object: unseen_count_int, forKey: GNConstants.UserDefined.GNUNSEENCOUNT)
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "NOTIFICATION_UPDATE_NOTIFICATION_BADGE"), object: nil)
                
            }
            else {
                handler("Something went wrong, please try again")
            }
        })
        
        task.resume()
    }
    
    func save(object: Any!, forKey key: String!) {
        self.userDefaults.set(object, forKey: key)
        self.userDefaults.synchronize()
    }
    
    func fetchUserDefaultsObject(forKey key: String!) -> Any? {
        let object = self.userDefaults.object(forKey: key)
        
        return object
    }
    
    func requestLoadMoreGlobalNotifications(lastID: Int, clearEntity: Bool, _ handler:@escaping GNErrorBlock) {
        
        let user_id: String = self.accountUserID()
        
        // NETWORKING REQUEST
        let endpoint = "/v2/notif/getprevnotif/user/\(user_id)/\(lastID)"
        
        let request = route("GET", uri: endpoint, body: nil)
        
        if clearEntity {
            let ctx = self.getContext()
            _ = self.db.clearEntity(GNConstants.Entity.NOTIFICATION, ctx: ctx!, filter: nil)
        }
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                handler(e.localizedDescription)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:Any] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                guard let records = dictionary["records"] as? [[String:Any]] else {
                    handler("Something went wrong, please try again")
                    return
                }
                
                self.processArrayNotifications(notifications: records)
                handler(nil)
            }
            else {
                handler("Something went wrong, please try again")
            }
        })
        
        task.resume()
    }
    
    func processArrayNotifications(notifications: [[String:Any]]) {
        
        if notifications.count > 0 {
            let ctx = self.getContext()
            
            let homeUrl = "http://\(self.retrieveVSmartBaseURL())"
            ctx?.performAndWait({
                for n in notifications {
                    
                    /*
                     "id": "5",
                     "user_id": "4",
                     "notif_id": "5",
                     "is_seen": "1",
                     "is_deleted": "0",
                     "date_created": "2017-01-16 11:40:34",
                     "formatted_date_created": "01/16/2017 11:40 AM",
                     "notif_message": " commented on this <b>post</b>",
                     "reference_id": "4",
                     "user_fname": "Leonil",
                     "user_lname": "Sauro",
                     "owner_fname": "Brian",
                     "owner_lname": "Marshall",
                     "avatar": "/public/img/avatar/avatar-one.png",
                     "module_type_id": "3",
                     "thumb_url": "img/icons/social.png",
                     "module_name": "Social Stream",
                     "module_code": "SOST",
                     "web_url": "#" */
                    
                    let notif_id = self.stringValue(n["id"])
                    let user_id = self.stringValue(n["user_id"])
                    let is_seen = self.stringValue(n["is_seen"])
                    let is_deleted = self.stringValue(n["is_deleted"])
                    let date_created = self.stringValue(n["date_created"])
                    let formatted_date_created = self.stringValue(n["formatted_date_created"])
                    let notif_message = self.stringValue(n["notif_message"])
                    let reference_id = self.stringValue(n["reference_id"])
                    let user_fname = self.stringValue(n["user_fname"])
                    let user_lname = self.stringValue(n["user_lname"])
                    let owner_fname = self.stringValue(n["owner_fname"])
                    let owner_lname = self.stringValue(n["owner_lname"])
                    var avatar = self.stringValue(n["avatar"])
                    avatar = homeUrl + avatar!
                    let module_type_id = self.stringValue(n["module_type_id"])
                    var thumb_url = self.stringValue(n["thumb_url"])
                    thumb_url = homeUrl + "/\(thumb_url!)"
                    let module_name = self.stringValue(n["module_name"])
                    let module_code = self.stringValue(n["module_code"])
                    let web_url = self.stringValue(n["web_url"])
                    
                    let sort_id: Double = Double(notif_id!)!
                    
                    let predicate = NSComparisonPredicate(keyPath: "notif_id", withValue: notif_id, isExact: true)
                    var mo: NSManagedObject? = self.db.retrieveEntity(GNConstants.Entity.NOTIFICATION, filter: predicate)
                    
                    if mo == nil {
                        mo = NSEntityDescription.insertNewObject(forEntityName: GNConstants.Entity.NOTIFICATION, into: ctx!)
                    }
                    
                    
                    mo?.setValue(is_seen, forKey: "is_seen")
                    
                    mo?.setValue(sort_id, forKey: "sort_id")
                    
                    mo?.setValue(notif_id, forKey: "notif_id")
                    mo?.setValue(user_id, forKey: "user_id")
                    mo?.setValue(is_deleted, forKey: "is_deleted")
                    mo?.setValue(date_created, forKey: "date_created")
                    mo?.setValue(formatted_date_created, forKey: "formatted_date_created")
                    mo?.setValue(notif_message, forKey: "notif_message")
                    mo?.setValue(reference_id, forKey: "reference_id")
                    mo?.setValue(user_fname, forKey: "user_fname")
                    mo?.setValue(user_lname, forKey: "user_lname")
                    mo?.setValue(owner_fname, forKey: "owner_fname")
                    mo?.setValue(owner_lname, forKey: "owner_lname")
                    mo?.setValue(avatar, forKey: "avatar")
                    mo?.setValue(module_type_id, forKey: "module_type_id")
                    mo?.setValue(thumb_url, forKey: "thumb_url")
                    mo?.setValue(module_name, forKey: "module_name")
                    mo?.setValue(module_code, forKey: "module_code")
                    mo?.setValue(web_url, forKey: "web_url")
                    
                }
                
                _ = self.db.saveObjectContext(ctx!)
            })
        }
        
    }
    
    func postSeenNotification(withID notif_id: String, handler:@escaping GNErrorBlock) {
        
        // NETWORKING REQUEST
        let endpoint = "/v2/notif/update/\(notif_id)"
        
        let request = route("POST", uri: endpoint, body: nil)
        let ctx = self.getContext()
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if let e = error {
                print(e.localizedDescription)
                handler(e.localizedDescription)
            }
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String:Any] else { return }
            
            if ( self.okayToParseResponse(dictionary) ) {
                
                ctx?.perform({ 
                    let predicate = NSComparisonPredicate(keyPath: "notif_id", withValue: notif_id, isExact: true)
                    let mo: NSManagedObject? = self.db.retrieveEntity(GNConstants.Entity.NOTIFICATION, filter: predicate)
                    
                    if mo != nil {
                        mo?.setValue("1", forKey: "is_seen")
                    }
                    
                    _ = self.db.saveObjectContext(ctx!)
                    
                    
                    guard let records = dictionary["records"] as? [String:Any] else {
                        handler("Something went wrong, please try again")
                        return
                    }
                    let unseen_count_string: String = self.stringValue(records["unseen"])
                    let unseen_count_int: Int = Int(unseen_count_string)!
                    self.save(object: unseen_count_int, forKey: GNConstants.UserDefined.GNUNSEENCOUNT)
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "NOTIFICATION_UPDATE_NOTIFICATION_BADGE"), object: nil)

                })
                
                
                handler(nil)
            }
            else {
                handler("Something went wrong, please try again")
            }
        })
        
        task.resume()
    }
    
    func getNumberOfUnseenPost() -> String {
//        let ctx = self.getContext()
        let predicate = NSComparisonPredicate(keyPath: "is_seen", withValue: "0", isExact: true)
        let mos: [NSManagedObject]? = self.db.retrieveObjects(forEntity: GNConstants.Entity.NOTIFICATION, withFilter: predicate) //self.db.retrieveEntity(GNConstants.Entity.NOTIFICATION, filter: predicate)
        
        var numberOfUnseenPost = "0"
        if mos != nil && (mos?.count)! > 0 {
            numberOfUnseenPost = "\(mos!.count)"
        }
        
        return numberOfUnseenPost
    }
    
}
