//
//  NoteEntry.h
//  V-Smart
//
//  Created by VhaL on 2/19/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoteEntry : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *tags;
@property (nonatomic, strong) NSString *noteBookName;
@property (nonatomic, strong) NSString *noteBookID;
@property (nonatomic, assign) int color;
@property (nonatomic, assign) int fontSizeId;
@property (nonatomic, assign) int fontTypeId;
@property (nonatomic, assign) int isArchive;

@end
