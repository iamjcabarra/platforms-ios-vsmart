//
//  Folder.m
//  V-Smart
//
//  Created by VhaL on 2/26/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NNoteBook.h"
#import "NNote.h"


@implementation NoteBook

@dynamic userId;
@dynamic colorId;
@dynamic created;
@dynamic noteBookName;
@dynamic modified;
@dynamic syncId;
@dynamic isRemoved;
@dynamic isShared;
@dynamic notes;

@end
