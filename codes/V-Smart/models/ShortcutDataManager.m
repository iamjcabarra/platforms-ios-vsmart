//
//  ShortcutDataManager.m
//  V-Smart
//
//  Created by VhaL on 3/5/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "ShortcutDataManager.h"
#define kTableSIdeaPad @"SIdeaPad"



@implementation ShortcutDataManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

-(AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

#pragma mark - Core Data
- (NSManagedObjectContext *) managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return _managedObjectContext;
}
- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return _managedObjectModel;
}
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"ModuleModel.sqlite"]];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:[self managedObjectModel]];
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil URL:storeUrl options:nil error:&error]) {
        /*Error for store creation should be handled in here*/
        
        
        //////////////////////////////////////
        // REMOVE THE INCOMPATIBLE STORE FILE
        //////////////////////////////////////
        
        NSFileManager *fm = [NSFileManager defaultManager];
        NSString *filePath = [NSString stringWithFormat:@"%@", storeUrl.path];
        
        if ([fm fileExistsAtPath:filePath]) {
            NSError *error = nil;
            NSDictionary *metaData = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:storeUrl options:nil error:&error];
            
            NSManagedObjectModel *model = self.persistentStoreCoordinator.managedObjectModel;
            if (![model isConfiguration:nil compatibleWithStoreMetadata:metaData]) {
                
                //REMOVE THE STORE FILE
                if ([fm removeItemAtPath:filePath error:NULL]) {
                    
                    //////////////////////////////////////
                    // REBUILD THE NEW STORE FILE
                    //////////////////////////////////////
                    
                    [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                              configuration:nil
                                                                        URL:storeUrl
                                                                    options:nil
                                                                      error:nil];
                }
            }
        }

        
        
        
    }
    
    return _persistentStoreCoordinator;
}
- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}
+ (id) sharedInstance {
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}
- (id)init {
    if ((self = [super init])) {
    }
    return self;
}
- (NSArray *)fetchObjectsForEntityName:(NSString *)entityName
                         withPredicate:(NSString*)stringFilter, ...{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    //add sorting mechanism (hardcoded 'modified' since all tables have that attribute)
    NSSortDescriptor *sortByModifiedDate = [[NSSortDescriptor alloc] initWithKey:@"modified" ascending:NO];
    [request setSortDescriptors:[NSArray arrayWithObject:sortByModifiedDate]];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.managedObjectContext];
    
    [request setEntity:entity];
    
    if (stringFilter){
        NSPredicate *predicate;
        va_list variadicArguments;
        va_start(variadicArguments, stringFilter);
        predicate = [NSPredicate predicateWithFormat:stringFilter arguments:variadicArguments];
        va_end(variadicArguments);
        
        [request setPredicate:predicate];
    }
    
    NSError *error;
    
    NSArray *array = [self.managedObjectContext executeFetchRequest:request error:&error];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isRemoved == %@", [NSNumber numberWithBool:NO]];
    array = [array filteredArrayUsingPredicate:pred];
    
    return array;
}

#pragma mark - IdeaPad
-(NSArray*)getIdeaPads{
    NSArray *result = [self fetchObjectsForEntityName:kTableSIdeaPad withPredicate:nil];

    NSPredicate *pred = [NSPredicate predicateWithFormat:@"userId == %i", [self account].user.id];
    result = [result filteredArrayUsingPredicate:pred];
    
    NSSortDescriptor *sortByTagName = [[NSSortDescriptor alloc] initWithKey:@"modified" ascending:NO];
    result = [result sortedArrayUsingDescriptors:@[sortByTagName]];
    
    
    
    return result;
}
-(SIdeaPad*)addIdeaPad:(NSString*)content withColorId:(int)colorId{
    
    SIdeaPad * ideaPad = [NSEntityDescription insertNewObjectForEntityForName:kTableSIdeaPad
                                              inManagedObjectContext:self.managedObjectContext];
    
    ideaPad.created = [NSDate date];
    ideaPad.modified = [NSDate date];
    ideaPad.content = content;
    ideaPad.isRemoved = [NSNumber numberWithBool:NO];
    ideaPad.userId = [NSNumber numberWithInt:[self account].user.id];
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"addIdeaPad, couldn't save: %@", [error localizedDescription]);
        
        // jca-05052016
        // For Error Message
        return nil;
    }
    
    return ideaPad;
}
-(BOOL)deleteIdeaPad:(SIdeaPad*)tag{
    bool deleteSuccess = true;
    
    tag.isRemoved = [NSNumber numberWithBool:YES];
    tag.modified = [NSDate date];
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"deleteIdeaPad, couldn't save: %@", [error localizedDescription]);
        deleteSuccess = false;
    }
    
    return deleteSuccess;
}
-(BOOL)editIdeaPad:(SIdeaPad*)tag{
    bool renameSuccess = true;
    
    tag.modified = [NSDate date];
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"editIdeaPad, couldn't save: %@", [error localizedDescription]);
        renameSuccess = false;
    }
    
    return renameSuccess;
}
@end
