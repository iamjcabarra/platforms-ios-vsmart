//
//  SSPeopleItem.m
//  V-Smart
//
//  Created by VhaL on 3/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "SSPeopleItem.h"

@implementation SSPeopleItem
@synthesize numLikedByMe, numPostedTopics, numReceivedLikes, numRepliedTopics, firstName, fullName, lastName, position, subjectsOrNickname, urlImage, isTeacher, image, sectionId, sectionName, id;

+(UIColor*)generateRandomColor{
    
    NSArray *colors = [[NoteDataManager sharedInstance] getColors:YES useMainThread:YES];
    
    NSUInteger r = arc4random_uniform(6);
    
    Color *color = [colors objectAtIndex:r];
    
    UIColor *returnColor = [[NoteDataManager sharedInstance] getUIColorFromColorObject:color];
    
    return returnColor;
}

@end
