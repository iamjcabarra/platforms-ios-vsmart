//
//  JMSPostMessage.h
//  V-Smart
//
//  Created by VhaL on 3/24/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"

@protocol JMSPostMessage
@end

@interface JMSPostMessage : JSONModel
@property (nonatomic, assign) int userId;
@property (nonatomic, assign) int groupId;
@property (nonatomic, assign) int emoticonId;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) int isAttached;
@end
