//
//  NoteTag.m
//  V-Smart
//
//  Created by VhaL on 2/26/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NNoteTag.h"
#import "NNote.h"
#import "NTag.h"


@implementation NoteTag

@dynamic created;
@dynamic modified;
@dynamic syncId;
@dynamic isRemoved;
@dynamic note;
@dynamic tag;

@end
