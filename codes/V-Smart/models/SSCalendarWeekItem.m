//
//  SSCalendarWeekItem.m
//  V-Smart
//
//  Created by VhaL on 3/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "SSCalendarWeekItem.h"

@implementation SSCalendarWeekItem
@synthesize week, weekEntries;

-(id)init{
    weekEntries = [[NSMutableArray alloc] init];
    return [super init];
}

@end
