//
//  SIdeaPad.m
//  V-Smart
//
//  Created by VhaL on 3/5/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "SIdeaPad.h"


@implementation SIdeaPad

@dynamic userId;
@dynamic content;
@dynamic modified;
@dynamic colorId;
@dynamic created;
@dynamic isRemoved;

@end
