//
//  JMSGroupItem.h
//  V-Smart
//
//  Created by VhaL on 3/25/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"

@protocol JMSGroupItem
@end

@interface JMSGroupItem : JSONModel
@property (nonatomic, assign) int id;
@property (nonatomic, assign) int userId;
@property (nonatomic, assign) int scsuId;
@property (nonatomic, assign) int groupId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *dateCreated;
@property (nonatomic, strong) NSString *dateModified;
@property (nonatomic, strong) NSString *section;
@end

@interface JMSRecordGroupItem : JSONModel
@property (nonatomic, strong) NSArray<JMSGroupItem>* records;
@end