//
//  Account.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/30/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Device.h"
#import "User.h"

@interface Account : NSObject
@property (nonatomic, strong) Device *device;
@property (nonatomic, strong) User *user;
@end
