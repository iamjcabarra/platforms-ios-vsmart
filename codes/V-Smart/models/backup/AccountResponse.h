//
//  AccountResponse.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/30/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Account.h"
#import "Error.h"

@interface AccountResponse : NSObject
@property (nonatomic, strong) Account *account;
@property (nonatomic, strong) Error *error;
@end
