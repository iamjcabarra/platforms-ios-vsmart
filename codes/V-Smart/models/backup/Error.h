//
//  Error.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/30/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Error : NSObject
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) int status;
@end
