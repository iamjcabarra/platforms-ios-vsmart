//
//  JMLockInOut.m
//  V-Smart
//
//  Created by VhaL on 4/4/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JMLockInOutResponse.h"

@implementation JMLockInOutResponse
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
@end

@implementation JMLockInOutBook
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
@end

@implementation JMLockInOutBookData
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
@end

@implementation JMLockInOutBookExercise
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
@end

