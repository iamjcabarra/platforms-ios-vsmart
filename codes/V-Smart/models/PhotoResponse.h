//
//  Photo.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/22/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface PhotoResponse : JSONModel

@property (nonatomic, strong) NSString *fileName;
@property (nonatomic, assign) NSString *fileSize;
@property (nonatomic, strong) NSString *fileType;
@property (nonatomic, strong) NSString *url;

@end
