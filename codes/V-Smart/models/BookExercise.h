//
//  BookExercise.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/15/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookExercise : NSObject
@property (nonatomic, strong) NSString *href;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) int exerciseId;
@property (nonatomic, assign) int totalItems;
@end
