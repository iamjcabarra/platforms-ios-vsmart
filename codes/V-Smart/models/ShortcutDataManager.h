//
//  ShortcutDataManager.h
//  V-Smart
//
//  Created by VhaL on 3/5/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SIdeaPad.h"

@interface ShortcutDataManager : NSObject

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator ;

+ (ShortcutDataManager *) sharedInstance;

- (NSArray *)fetchObjectsForEntityName:(NSString *)entityName
                         withPredicate:(NSString*)stringFilter, ...;

#pragma mark - IdeaPad
-(NSArray*)getIdeaPads;

-(SIdeaPad*)addIdeaPad:(NSString*)content withColorId:(int)colorId;

-(BOOL)deleteIdeaPad:(SIdeaPad*)tag;

-(BOOL)editIdeaPad:(SIdeaPad*)tag;

@end
