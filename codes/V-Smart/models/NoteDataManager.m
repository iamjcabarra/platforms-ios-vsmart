//
//  NoteTagDataManager.m
//  V-Smart
//
//  Created by VhaL on 2/20/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NoteDataManager.h"
#import "GCNetworkKit.h"

#define kTableTag @"NTag"
#define kTableNote @"NNote"
#define kTableNoteBook @"NNoteBook"
#define kTableNoteTag @"NNoteTag"
#define kTableColor @"NColor"
#define kTableFontType @"NFontType"
#define kTableFontSize @"NFontSize"

NSString* const kNotebookGeneralName = @"General";

@interface NoteDataManager()
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContextInBackground;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@end

@implementation NoteDataManager
//@synthesize managedObjectContext, managedObjectModel, persistentStoreCoordinator;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectContextInBackground = _managedObjectContextInBackground;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize isSyncing;

-(AccountInfo *) account {
    AccountInfo *account = [[VSmart sharedInstance] account];
    return account;
}

-(void)sync{

    if (isSyncing){
        VLog(@"synching already running");
    }
    else{
        [self startSync];
    }
    
}

-(void)startSync{
    isSyncing = YES;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
       
        VLog(@"synching - start");
        
        BOOL isSuccess = NO;
        isSuccess = [self syncNotebooks];
        
        if (isSuccess) //continue if there are no errors
            isSuccess = [self syncTags];
        
        if (isSuccess) //continue if there are no errors
            isSuccess = [self syncNotes];
        
        if (isSuccess) //continue if there are no errors
            isSuccess = [self syncNoteTags];
        

        dispatch_sync(dispatch_get_main_queue(), ^{
            isSyncing = NO;
        });

        VLog(@"synching - done");
    });
}

-(void)syncNotebookGeneral:(NSArray*)dbNotebooks fromAPINotebooks:(JMNRecordNotebook*)apiNotebooks{
    for (JMNNotebook *apiNotebook in apiNotebooks.notebooks) {
        for (NoteBook *dbNotebook in dbNotebooks) {

            if ([dbNotebook.noteBookName isEqualToString:kNotebookGeneralName] &&
                [apiNotebook.name isEqualToString:kNotebookGeneralName]){
                dbNotebook.syncId = [NSNumber numberWithInt:apiNotebook.id];
                [self editNoteBook:dbNotebook useMainThread:NO];
            }
        }
    }
}

-(BOOL)syncNotebooks{
    NSError *error;
    NSNumber *statusCode;
    JMNRecordNotebook *apiNotebooks;
    NSMutableArray *dbNotebooks;
    
//    NSUndoManager *undoManager = [[NSUndoManager alloc] init];
//    [givenManageObjectContext setUndoManager:undoManager];
//    [undoManager beginUndoGrouping];
//    [undoManager endUndoGrouping];
//    [undoManager undo];
    
    @try {
        apiNotebooks = [self APIGetAllNotebooksByUserIdOutError:&error statusCode:&statusCode];
        if (apiNotebooks || statusCode.intValue == 100){
            dbNotebooks = [NSMutableArray arrayWithArray:[self getNoteBooks:YES useMainThread:NO]];
            [self syncNotebookGeneral:dbNotebooks fromAPINotebooks:apiNotebooks];
            [self insertDBNotebooks:dbNotebooks fromAPINotebooks:apiNotebooks];
            [self insertAPINotebooks:apiNotebooks fromDBNotebooks:dbNotebooks];
        }
        else{ //do not continue
            return NO;
        }
    }
    @catch (NSException *exception) {
        VLog(@"%@", exception.debugDescription);
        return NO;
    }

    
    @try {
        apiNotebooks = [self APIGetAllNotebooksByUserIdOutError:&error statusCode:&statusCode];
        if (apiNotebooks || statusCode.intValue == 100){
            dbNotebooks = [NSMutableArray arrayWithArray:[self getNoteBooks:YES useMainThread:NO]];
            [self deleteDBNotebooks:dbNotebooks fromAPINotebooks:apiNotebooks];
            [self deleteAPINotebooks:apiNotebooks fromDBNotebooks:dbNotebooks];
        }
        else{ //do not continue
            return NO;
        }
    }
    @catch (NSException *exception) {
        VLog(@"%@", exception.debugDescription);
        return NO;
    }

    @try {
        apiNotebooks = [self APIGetAllNotebooksByUserIdOutError:&error statusCode:&statusCode];
        if (apiNotebooks || statusCode.intValue == 100){
            dbNotebooks = [NSMutableArray arrayWithArray:[self getNoteBooks:YES useMainThread:NO]];
            [self updateDBNotebooks:dbNotebooks fromAPINotebooks:apiNotebooks];
            [self updateAPINotebooks:apiNotebooks fromDBNotebooks:dbNotebooks];
        }
        else{ //do not continue
            return NO;
        }
    }
    @catch (NSException *exception) {
        VLog(@"%@", exception.debugDescription);
        return NO;
    }
    
    return YES;
}

-(BOOL)syncTags{
    NSError *error;
    NSNumber *statusCode;
    JMNRecordTag *apiTags;
    NSMutableArray *dbTags;

    @try {
        apiTags = [self APIGetAllTagsByUserIdOutError:&error statusCode:&statusCode];
        if (apiTags || statusCode.intValue == 100){
            dbTags = [NSMutableArray arrayWithArray:[self getTags:YES useMainThread:NO]];
            [self insertDBTags:dbTags fromAPITags:apiTags];
            [self insertAPITags:apiTags fromDBTags:dbTags];
        }
        else{ //do not continue
            return NO;;
        }
    }
    @catch (NSException *exception) {
        VLog(@"%@", exception.debugDescription);
        return NO;
    }
    
    @try {
        apiTags = [self APIGetAllTagsByUserIdOutError:&error statusCode:&statusCode];
        if (apiTags || statusCode.intValue == 100){
            dbTags = [NSMutableArray arrayWithArray:[self getTags:YES useMainThread:NO]];
            [self deleteDBTags:dbTags fromAPITags:apiTags];
            [self deleteAPITags:apiTags fromDBTags:dbTags];
        }
        else{ //do not continue
            return NO;;
        }
    }
    @catch (NSException *exception) {
        VLog(@"%@", exception.debugDescription);
        return NO;
    }

    @try {
        apiTags = [self APIGetAllTagsByUserIdOutError:&error statusCode:&statusCode];
        if (apiTags || statusCode.intValue == 100){
            dbTags = [NSMutableArray arrayWithArray:[self getTags:YES useMainThread:NO]];
            [self updateDBTags:dbTags fromAPITags:apiTags];
            [self updateAPITags:apiTags fromDBTags:dbTags];
        }
        else{ //do not continue
            return NO;;
        }
    }
    @catch (NSException *exception) {
        VLog(@"%@", exception.debugDescription);
        return NO;
    }
    
    return YES;
}

-(BOOL)syncNotes{
    NSError *error;
    NSNumber *statusCode;
    JMNRecordNote *apiNotes;
    NSMutableArray *dbNotes;
    
    @try {
        apiNotes = [self APIGetAllNotesByUserIdOutError:&error statusCode:&statusCode];
        if (apiNotes || statusCode.intValue == 100){
            dbNotes = [NSMutableArray arrayWithArray:[self getNotes:YES useMainThread:NO]];
            [self insertDBNotes:dbNotes fromAPINotes:apiNotes];
            [self insertAPINotes:apiNotes fromDBNotes:dbNotes];
        }
        else{ //do not continue
            return NO;;
        }
    }
    @catch (NSException *exception) {
        VLog(@"%@", exception.debugDescription);
        return NO;
    }
    
    @try {
        apiNotes = [self APIGetAllNotesByUserIdOutError:&error statusCode:&statusCode];
        if (apiNotes || statusCode.intValue == 100){
            dbNotes = [NSMutableArray arrayWithArray:[self getNotes:YES useMainThread:NO]];
            [self deleteDBNotes:dbNotes fromAPINotes:apiNotes];
            [self deleteAPINotes:apiNotes fromDBNotes:dbNotes];
        }
        else{ //do not continue
            return NO;;
        }
    }
    @catch (NSException *exception) {
        VLog(@"%@", exception.debugDescription);
        return NO;
    }
    
    @try {
        apiNotes = [self APIGetAllNotesByUserIdOutError:&error statusCode:&statusCode];
        if (apiNotes || statusCode.intValue == 100){
            dbNotes = [NSMutableArray arrayWithArray:[self getNotes:YES useMainThread:NO]];
            [self updateDBNotes:dbNotes fromAPINotes:apiNotes];
            [self updateAPINotes:apiNotes fromDBNotes:dbNotes];
        }
        else{ //do not continue
            return NO;;
        }
    }
    @catch (NSException *exception) {
        VLog(@"%@", exception.debugDescription);
        return NO;
    }
    
    return YES;
}

-(BOOL)syncNoteTags{
    NSError *error;
    NSNumber *statusCode;
    JMNRecordNoteTag *apiNoteTags;
    NSMutableArray *dbNoteTags;
    
    @try {
        apiNoteTags = [self APIGetAllNoteTagsByUserIdOutError:&error statusCode:&statusCode];
        if (apiNoteTags || statusCode.intValue == 100){
            dbNoteTags = [NSMutableArray arrayWithArray:[self getNoteTags:YES useMainThread:NO]];
            [self insertDBNoteTags:dbNoteTags fromAPINoteTags:apiNoteTags];
            [self insertAPINoteTags:apiNoteTags fromDBNoteTags:dbNoteTags];
        }
        else{ //do not continue
            return NO;;
        }
    }
    @catch (NSException *exception) {
        VLog(@"%@", exception.debugDescription);
        return NO;
    }
    
    
    @try {
        apiNoteTags = [self APIGetAllNoteTagsByUserIdOutError:&error statusCode:&statusCode];
        if (apiNoteTags || statusCode.intValue == 100){
            dbNoteTags = [NSMutableArray arrayWithArray:[self getNoteTags:YES useMainThread:NO]];
            [self deleteDBNoteTags:dbNoteTags fromAPINoteTags:apiNoteTags];
            [self deleteAPINoteTags:apiNoteTags fromDBNoteTags:dbNoteTags];
        }
        else{ //do not continue
            return NO;;
        }
    }
    @catch (NSException *exception) {
        VLog(@"%@", exception.debugDescription);
        return NO;
    }
    
    //Currently, there are no update methods available for note tags
//    apiNoteTags = [self APIGetAllNoteTagsByUserIdOutError:&error statusCode:&statusCode];
//    if (apiNoteTags || statusCode.intValue == 100){
//        dbNoteTags = [NSMutableArray arrayWithArray:[self getNoteTags:YES]];
//        [self updateDBNoteTags:dbNoteTags fromAPINoteTags:apiNoteTags];
//        [self updateAPINoteTags:apiNoteTags fromDBNoteTags:dbNoteTags];
//    }
    
    return YES;
}

-(void)insertDBNotebooks:(NSMutableArray*)dbNotebooks fromAPINotebooks:(JMNRecordNotebook*)apiNotebooks{
    
    NSMutableArray *arrayToBeAdded = [NSMutableArray new];
    
    for (JMNNotebook *apiNotebook in apiNotebooks.notebooks) {
        bool isExisting = false;
        for (NoteBook *dbNotebook in dbNotebooks) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbNotebook.syncId.intValue <= 0)
                continue;
            
            if (dbNotebook.syncId.intValue == apiNotebook.id){
                isExisting = true;
                break;
            }
        }
        
        if (!isExisting && ![apiNotebook.name isEqualToString:@"General"]){
            [arrayToBeAdded addObject:apiNotebook];
        }
    }
    
    for (JMNNotebook *apiNotebook in arrayToBeAdded) {
        
        //add entry to DB
        NoteBook *dbNotebook = [self addNoteBook:apiNotebook.name withColor:0 useMainThread:NO];
        
        //update entry in DB with date modify from api and sync id
        dbNotebook.syncId = [NSNumber numberWithInt:apiNotebook.id];
        dbNotebook.colorId = [NSNumber numberWithInt:apiNotebook.bgColor];
        dbNotebook.modified = [apiNotebook.dateModified dateFromString];
        [self editNoteBook:dbNotebook useMainThread:NO];
    }
}

-(void)insertAPINotebooks:(JMNRecordNotebook*)apiNotebooks fromDBNotebooks:(NSMutableArray*)dbNotebooks{
    
    for (NoteBook *dbNotebook in dbNotebooks) {
        if (0 < dbNotebook.syncId.intValue)
            continue;
        
        JMNNotebook *apiNotebook = [JMNNotebook new];
        apiNotebook.userId = [self account].user.id;
        apiNotebook.name = dbNotebook.noteBookName;
        apiNotebook.bgColor = dbNotebook.colorId.intValue;
        apiNotebook.platform = @"iOS";
        
        // jca-05062016
        // Add NoteBookID
        // apiNotebook.noteBookID = dbNotebook.noteBookID;
        
        NSString *jsonString = [apiNotebook toJSONString];

        JSONModelError *jsonModelError;
        NSNumber *numStatusCode;
        NSError *parseError;
        
        NSString *jsonOut = [self APICallFromURLString:[Utils buildUrl:kEndPointAddNotebook] withMethod:kHTTPMethodPOST withBody:jsonString withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"notebooks"];
        
        if (jsonModelError == nil){
            JMNRecordNotebook *apiNotebooks = [[JMNRecordNotebook alloc] initWithString:jsonOut error:&parseError];
            apiNotebook = [apiNotebooks.notebooks objectAtIndex:0];
            
            dbNotebook.modified = [apiNotebook.dateModified dateFromString];
            dbNotebook.syncId = [NSNumber numberWithInt:apiNotebook.id];
            [self editNoteBook:dbNotebook useMainThread:NO];
        }
    }
}

-(void)deleteDBNotebooks:(NSMutableArray*)dbNotebooks fromAPINotebooks:(JMNRecordNotebook*)apiNotebooks{

    NSMutableArray *arrayToBeDeleted = [NSMutableArray new];
    
    for (NoteBook *dbNotebook in dbNotebooks) {
        if (dbNotebook.isRemoved.boolValue == YES)
            continue;
        
        bool isExisting = false;
        
        for (JMNNotebook *apiNotebook in apiNotebooks.notebooks) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbNotebook.syncId.intValue <= 0)
                continue;
            
            if (dbNotebook.syncId.intValue == apiNotebook.id){
                isExisting = true;
                break;
            }
        }
        
        if (!isExisting){
            [arrayToBeDeleted addObject:dbNotebook];
        }
    }
    
    for (NoteBook *dbNotebook in arrayToBeDeleted) {
        [self deleteNoteBook:dbNotebook useMainThread:NO];
    }
}

-(void)deleteAPINotebooks:(JMNRecordNotebook*)apiNotebooks fromDBNotebooks:(NSMutableArray*)dbNotebooks{
    for (NoteBook *dbNotebook in dbNotebooks) {
        if (dbNotebook.syncId.intValue <= 0)
            continue;
        
        for (JMNNotebook *apiNotebook in apiNotebooks.notebooks) {
            if (dbNotebook.syncId.intValue == apiNotebook.id && dbNotebook.isRemoved.boolValue == YES){
                
                NSString *jsonString = [apiNotebook toJSONString];
                
                JSONModelError *jsonModelError;
                NSNumber *numStatusCode;
                NSError *parseError;
                
                NSString *jsonOut = [self APICallFromURLString:[Utils buildUrl:kEndPointDeleteNotebookById] withMethod:kHTTPMethodPOST withBody:jsonString withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"notebooks"];
                
                if (jsonModelError == nil){
                    JMNRecordNotebook *apiNotebooks = [[JMNRecordNotebook alloc] initWithString:jsonOut error:&parseError];
                    JMNNotebook *apiNotebook = [apiNotebooks.notebooks objectAtIndex:0];
                    
                    dbNotebook.modified = [apiNotebook.dateModified dateFromString];
                    [self editNoteBook:dbNotebook useMainThread:NO];
                }
            }
        }
    }
}

-(void)updateDBNotebooks:(NSMutableArray*)dbNotebooks fromAPINotebooks:(JMNRecordNotebook*)apiNotebooks{
    for (NoteBook *dbNotebook in dbNotebooks) {
        
        for (JMNNotebook *apiNotebook in apiNotebooks.notebooks) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbNotebook.syncId.intValue <= 0)
                continue;
            
            if (dbNotebook.syncId.intValue == apiNotebook.id && dbNotebook.isRemoved.boolValue == NO){
                
                //(dbNotebook.modified < [apiNotebook.dateModified dateFromString])
                if ([[apiNotebook.dateModified dateFromString] compare:dbNotebook.modified] == NSOrderedDescending){
                    // jca-05042016
                    // Add NoteBookID
                    // dbNotebook.noteBookID = apiNotebook.noteBookID;
                    
                    dbNotebook.noteBookName = apiNotebook.name;
                    dbNotebook.colorId = [NSNumber numberWithInt:apiNotebook.bgColor];
                    dbNotebook.modified = [apiNotebook.dateModified dateFromString];
                    [self editNoteBook:dbNotebook useMainThread:NO];
                }
                break;
            }
        }
    }
}

-(void)updateAPINotebooks:(JMNRecordNotebook*)apiNotebooks fromDBNotebooks:(NSMutableArray*)dbNotebooks{
    for (NoteBook *dbNotebook in dbNotebooks) {
        
        for (JMNNotebook *apiNotebook in apiNotebooks.notebooks) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbNotebook.syncId.intValue <= 0)
                continue;
            
            if (dbNotebook.syncId.intValue == apiNotebook.id && dbNotebook.isRemoved.boolValue == NO){
                
                if ([dbNotebook.modified compare:[apiNotebook.dateModified dateFromString]] == NSOrderedDescending){
                    // jca-05042016
                    // Add NoteBookID
                    // apiNotebook.noteBookID = dbNotebook.noteBookID;
                    
                    apiNotebook.name = dbNotebook.noteBookName;
                    apiNotebook.bgColor = dbNotebook.colorId.intValue;
                    
                    NSString *jsonString = [apiNotebook toJSONString];
                    
                    
                    JSONModelError *jsonModelError;
                    NSNumber *numStatusCode;
                    NSError *parseError;
                    
                    NSString *jsonOut = [self APICallFromURLString:[Utils buildUrl:kEndPointEditNotebookById] withMethod:kHTTPMethodPOST withBody:jsonString withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"notebooks"];
                    
                    if (jsonModelError == nil){
                        JMNRecordNotebook *apiNotebooks = [[JMNRecordNotebook alloc] initWithString:jsonOut error:&parseError];
                        JMNNotebook *apiNotebook = [apiNotebooks.notebooks objectAtIndex:0];
                        
                        dbNotebook.modified = [apiNotebook.dateModified dateFromString];
                        [self editNoteBook:dbNotebook useMainThread:NO];
                    }
                }
                break;
            }
        }
    }
}


-(void)insertDBTags:(NSMutableArray*)dbTags fromAPITags:(JMNRecordTag*)apiTags{
    NSMutableArray *arrayToBeAdded = [NSMutableArray new];
    
    for (JMNTag *apiTag in apiTags.user_tags) {
        bool isExisting = false;
        for (Tag *dbTag in dbTags) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbTag.syncId.intValue <= 0)
                continue;
            
            if (dbTag.syncId.intValue == apiTag.id){
                isExisting = true;
                break;
            }
        }
        
        if (!isExisting){
            [arrayToBeAdded addObject:apiTag];
        }
    }
    
    for (JMNTag *apiTag in arrayToBeAdded) {
        
        //add entry to DB
        Tag *dbTag = [self addTag:apiTag.name useMainThread:NO];
        
        //update entry in DB with date modify from api and sync id
        dbTag.syncId = [NSNumber numberWithInt:apiTag.id];
        dbTag.modified = [apiTag.dateModified dateFromString];
        [self editTag:dbTag useMainThread:NO];
    }
}

-(void)insertAPITags:(JMNRecordTag*)apiTags fromDBTags:(NSMutableArray*)dbTags{
    for (Tag *dbTag in dbTags) {
        if (0 < dbTag.syncId.intValue)
            continue;
        
        JMNTag *apiTag = [JMNTag new];
        apiTag.userId = [self account].user.id;
        apiTag.name = dbTag.tagName;
        
        NSString *jsonString = [apiTag toJSONString];
        
        
        JSONModelError *jsonModelError;
        NSNumber *numStatusCode;
        NSError *parseError;
        
        NSString *jsonOut = [self APICallFromURLString:[Utils buildUrl:kEndPointAddTag] withMethod:kHTTPMethodPOST withBody:jsonString withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"user_tags"];
        
        if (jsonModelError == nil){
            JMNRecordTag *apiTags = [[JMNRecordTag alloc] initWithString:jsonOut error:&parseError];
            JMNTag *apiTag = [apiTags.user_tags objectAtIndex:0];
            
            dbTag.modified = [apiTag.dateModified dateFromString];
            dbTag.syncId = [NSNumber numberWithInt:apiTag.id];
            [self editTag:dbTag useMainThread:NO];
        }

    }
}

-(void)deleteDBTags:(NSMutableArray*)dbTags fromAPITags:(JMNRecordTag*)apiTags{
    NSMutableArray *arrayToBeDeleted = [NSMutableArray new];
    
    for (Tag *dbTag in dbTags) {
        if (dbTag.isRemoved.boolValue == YES)
            continue;
        
        bool isExisting = false;
        
        for (JMNTag *apiTag in apiTags.user_tags) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbTag.syncId.intValue <= 0)
                continue;
            
            if (dbTag.syncId.intValue == apiTag.id){
                isExisting = true;
                break;
            }
        }
        
        if (!isExisting){
            [arrayToBeDeleted addObject:dbTag];
        }
    }
    
    for (Tag *dbTag in arrayToBeDeleted) {
        [self deleteTag:dbTag useMainThread:NO];
    }
}

-(void)deleteAPITags:(JMNRecordTag*)apiTags fromDBTags:(NSMutableArray*)dbTags{
    for (Tag *dbTag in dbTags) {
        if (dbTag.syncId.intValue <= 0)
            continue;
        
        for (JMNTag *apiTag in apiTags.user_tags) {
            if (dbTag.syncId.intValue == apiTag.id && dbTag.isRemoved.boolValue == YES){
                
                NSString *jsonString = [apiTag toJSONString];
                
                JSONModelError *jsonModelError;
                NSNumber *numStatusCode;
                NSError *parseError;
                
                NSString *jsonOut = [self APICallFromURLString:[Utils buildUrl:kEndPointDeleteTagById] withMethod:kHTTPMethodPOST withBody:jsonString withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"user_tags"];
                
                if (jsonModelError == nil){
                    JMNRecordTag *apiTags = [[JMNRecordTag alloc] initWithString:jsonOut error:&parseError];
                    JMNTag *apiTag = [apiTags.user_tags objectAtIndex:0];
                    
                    Tag *dbTag = [self getTagBySyncId:apiTag.id useMainThread:NO];
                    dbTag.modified = [apiTag.dateModified dateFromString];
                    [self editTag:dbTag useMainThread:NO];
                }
            }
        }
    }
}

-(void)updateDBTags:(NSMutableArray*)dbTags fromAPITags:(JMNRecordTag*)apiTags{
    for (Tag *dbTag in dbTags) {
        
        for (JMNTag *apiTag in apiTags.user_tags) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbTag.syncId.intValue <= 0)
                continue;
            
            if (dbTag.syncId.intValue == apiTag.id && dbTag.isRemoved.boolValue == NO){
                
//                if (dbTag.modified < [apiTag.dateModified dateFromString]){
                if ([[apiTag.dateModified dateFromString] compare:dbTag.modified] == NSOrderedDescending){
                    dbTag.tagName = apiTag.name;
                    dbTag.modified = [apiTag.dateModified dateFromString];
                    [self editTag:dbTag useMainThread:NO];
                }
                break;
            }
        }
    }
}

-(void)updateAPITags:(JMNRecordTag*)apiTags fromDBTags:(NSMutableArray*)dbTags{
    for (Tag *dbTag in dbTags) {
        
        for (JMNTag *apiTag in apiTags.user_tags) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbTag.syncId.intValue <= 0)
                continue;
            
            if (dbTag.syncId.intValue == apiTag.id && dbTag.isRemoved.boolValue == NO){
                
                if ([dbTag.modified compare:[apiTag.dateModified dateFromString]] == NSOrderedDescending){
                    
                    apiTag.name = dbTag.tagName;
                    
                    NSString *jsonString = [apiTag toJSONString];
                    
                    
                    JSONModelError *jsonModelError;
                    NSNumber *numStatusCode;
                    NSError *parseError;
                    
                    NSString *jsonOut = [self APICallFromURLString:[Utils buildUrl:kEndPointEditTagById] withMethod:kHTTPMethodPOST withBody:jsonString withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"user_tags"];
                    
                    if (jsonModelError == nil){
                        JMNRecordTag *apiTags = [[JMNRecordTag alloc] initWithString:jsonOut error:&parseError];
                        JMNTag *apiTag = [apiTags.user_tags objectAtIndex:0];
                        
                        Tag *dbTag = [self getTagBySyncId:apiTag.id useMainThread:NO];
                        dbTag.modified = [apiTag.dateModified dateFromString];
                        [self editTag:dbTag useMainThread:NO];
                    }
                }
                break;
            }
        }
    }
}


-(void)insertDBNotes:(NSMutableArray*)dbNotes fromAPINotes:(JMNRecordNote*)apiNotes{
    
    NSMutableArray *arrayToBeAdded = [NSMutableArray new];
    
    for (JMNNote *apiNote in apiNotes.user_notes) {
        bool isExisting = false;
        for (Note *dbNote in dbNotes) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbNote.syncId.intValue <= 0)
                continue;
            
            if (dbNote.syncId.intValue == apiNote.id){
                isExisting = true;
                break;
            }
        }
        
        if (!isExisting){
            [arrayToBeAdded addObject:apiNote];
        }
    }
    
    for (JMNNote *apiNote in arrayToBeAdded) {

        NoteBook *dbNotebook = [self getNoteBookBySyncId:apiNote.notebookId useMainThread:NO];
        
        if (dbNotebook == nil)
            continue;
        
        //add entry to DB
        NoteEntry *noteEntry = [NoteEntry new];
        
        // jca-05042016
        // Add NoteBookID
        noteEntry.noteBookID = dbNotebook.noteBookID;
        
        noteEntry.noteBookName = dbNotebook.noteBookName;
        noteEntry.title = apiNote.title;
        noteEntry.description = apiNote.content;
        noteEntry.color = apiNote.bgColor;
        noteEntry.fontSizeId = apiNote.fontSizeId;
        noteEntry.fontTypeId = apiNote.fontTypeId;
        noteEntry.isArchive = apiNote.isArchive;
        
        Note *dbNote = [self addNote:noteEntry useMainThread:NO];
        
        //update entry in DB with date modify from api and sync id
        dbNote.folder = dbNotebook;
        dbNote.syncId = [NSNumber numberWithInt:apiNote.id];
        dbNote.modified = [apiNote.dateModified dateFromString];
        [self editNote:noteEntry sourceNote:dbNote useMainThread:NO];
    }
}

-(void)insertAPINotes:(JMNRecordNote*)apiNotes fromDBNotes:(NSMutableArray*)dbNotes{
    
    for (Note *dbNote in dbNotes) {
        if (0 < dbNote.syncId.intValue)
            continue;
        
        JMNNote *apiNote = [JMNNote new];
        apiNote.userId = [self account].user.id;
        apiNote.notebookId = dbNote.folder.syncId.intValue;
        apiNote.title = dbNote.title;
        apiNote.content = dbNote.content;
        apiNote.bgColor = dbNote.colorId.intValue;
        apiNote.fontSizeId = dbNote.fontSizeId.intValue;
        apiNote.fontTypeId = dbNote.fontTypeId.intValue;
        
        // jca-05062016
        // Add NoteBookID
        // apiNote.dbNoteBookID = dbNote.folder.noteBookID;
        
        NSString *jsonString = [apiNote toJSONString];
        
        JSONModelError *jsonModelError;
        NSNumber *numStatusCode;
        NSError *parseError;
        
        NSString *jsonOut = [self APICallFromURLString:[Utils buildUrl:kEndPointAddNote] withMethod:kHTTPMethodPOST withBody:jsonString withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"user_notes"];
        
        if (jsonModelError == nil){
            JMNRecordNote *apiNotes = [[JMNRecordNote alloc] initWithString:jsonOut error:&parseError];
            JMNNote *apiNote = [apiNotes.user_notes objectAtIndex:0];
            
            dbNote.modified = [apiNote.dateModified dateFromString];
            dbNote.syncId = [NSNumber numberWithInt:apiNote.id];
            [self editNote:dbNote useMainThread:NO];
        }
    }
}

-(void)deleteDBNotes:(NSMutableArray*)dbNotes fromAPINotes:(JMNRecordNote*)apiNotes{
    
    NSMutableArray *arrayToBeDeleted = [NSMutableArray new];
    
    for (Note *dbNote in dbNotes) {
        if (dbNote.isRemoved.boolValue == YES)
            continue;
        
        bool isExisting = false;
        
        for (JMNNote *apiNote in apiNotes.user_notes) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbNote.syncId.intValue <= 0)
                continue;
            
            if (dbNote.syncId.intValue == apiNote.id){
                isExisting = true;
                break;
            }
        }
        
        if (!isExisting){
            [arrayToBeDeleted addObject:dbNote];
        }
    }
    
    for (Note *dbNote in arrayToBeDeleted) {
        
        if (dbNote.folder.userId.intValue == [self account].user.id)
            [self deleteNote:dbNote useMainThread:NO];
    }
}

-(void)deleteAPINotes:(JMNRecordNote*)apiNotes fromDBNotes:(NSMutableArray*)dbNotes{
    for (Note *dbNote in dbNotes) {
        if (dbNote.syncId.intValue <= 0)
            continue;
        
        for (JMNNote *apiNote in apiNotes.user_notes) {
            if (dbNote.syncId.intValue == apiNote.id && dbNote.isRemoved.boolValue == YES){
                
                NSString *jsonString = [apiNote toJSONString];
                
                JSONModelError *jsonModelError;
                NSNumber *numStatusCode;
                NSError *parseError;
                
                NSString *jsonOut = [self APICallFromURLString:[Utils buildUrl:kEndPointDeleteNoteById] withMethod:kHTTPMethodPOST withBody:jsonString withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"user_notes"];
                
                if (jsonModelError == nil){
                    JMNRecordNote *apiNotes = [[JMNRecordNote alloc] initWithString:jsonOut error:&parseError];
                    JMNNote *apiNote = [apiNotes.user_notes objectAtIndex:0];
                    
                    Note *dbNote = [self getNoteBySyncId:apiNote.id useMainThread:NO];
                    dbNote.modified = [apiNote.dateModified dateFromString];
                    [self editNote:dbNote useMainThread:NO];
                }
            }
        }
    }
}

-(void)updateDBNotes:(NSMutableArray*)dbNotes fromAPINotes:(JMNRecordNote*)apiNotes{
    for (Note *dbNote in dbNotes) {
        
        for (JMNNote *apiNote in apiNotes.user_notes) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbNote.syncId.intValue <= 0)
                continue;
            
            if (dbNote.syncId.intValue == apiNote.id && dbNote.isRemoved.boolValue == NO){
                
//                if (dbNote.modified < [apiNote.dateModified dateFromString]){
                if ([[apiNote.dateModified dateFromString] compare:dbNote.modified] == NSOrderedDescending){
                
                    dbNote.title = apiNote.title;
                    dbNote.content = apiNote.content;
                    dbNote.colorId = [NSNumber numberWithInt:apiNote.bgColor];
                    dbNote.fontTypeId = [NSNumber numberWithInt:apiNote.fontTypeId];
                    dbNote.fontSizeId = [NSNumber numberWithInt:apiNote.fontSizeId];
                    dbNote.isArchive = [NSNumber numberWithInt:apiNote.isArchive];
                    
                    // jca-05062016
                    // Add NoteBookID
                    // dbNote.folder.noteBookID = apiNote.dbNoteBookID;
                    
                    dbNote.modified = [apiNote.dateModified dateFromString];
                    [self editNote:dbNote useMainThread:NO];
                }
                break;
            }
        }
    }
}

-(void)updateAPINotes:(JMNRecordNote*)apiNotes fromDBNotes:(NSMutableArray*)dbNotes{
    for (Note *dbNote in dbNotes) {
        
        for (JMNNote *apiNote in apiNotes.user_notes) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbNote.syncId.intValue <= 0)
                continue;
            
            if (dbNote.syncId.intValue == apiNote.id && dbNote.isRemoved.boolValue == NO){
                
                if ([dbNote.modified compare:[apiNote.dateModified dateFromString]] == NSOrderedDescending){
                    
                    apiNote.notebookId = dbNote.folder.syncId.intValue;
                    apiNote.title = dbNote.title;
                    apiNote.content = dbNote.content;
                    apiNote.bgColor = dbNote.colorId.intValue;
                    apiNote.fontSizeId = dbNote.fontSizeId.intValue;
                    apiNote.fontTypeId = dbNote.fontTypeId.intValue;
                    apiNote.isArchive = dbNote.isArchive.intValue;
                    
                    // jca-05062016
                    // Add NoteBookID
                    // apiNote.dbNoteBookID = dbNote.folder.noteBookID;
                    
                    NSString *jsonString = [apiNote toJSONString];
                    
                    JSONModelError *jsonModelError;
                    NSNumber *numStatusCode;
                    NSError *parseError;
                    
                    NSString *jsonOut = [self APICallFromURLString:[Utils buildUrl:kEndPointEditNoteById] withMethod:kHTTPMethodPOST withBody:jsonString withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"user_notes"];
                    
                    if (jsonModelError == nil){
                        JMNRecordNote *apiNotes = [[JMNRecordNote alloc] initWithString:jsonOut error:&parseError];
                        JMNNote *apiNote = [apiNotes.user_notes objectAtIndex:0];
                        
                        Note *dbNote = [self getNoteBySyncId:apiNote.id useMainThread:NO];
                        dbNote.modified = [apiNote.dateModified dateFromString];
                        [self editNote:dbNote useMainThread:NO];
                    }
                }
                break;
            }
        }
    }
}


-(void)insertDBNoteTags:(NSMutableArray*)dbNoteTags fromAPINoteTags:(JMNRecordNoteTag*)apiNoteTags{
    
    NSMutableArray *arrayToBeAdded = [NSMutableArray new];
    
    for (JMNNoteTag *apiNoteTag in apiNoteTags.note_tags) {
        bool isExisting = false;
        for (NoteTag *dbNoteTag in dbNoteTags) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbNoteTag.syncId.intValue <= 0)
                continue;
            
            if (dbNoteTag.syncId.intValue == apiNoteTag.id){
                isExisting = true;
                break;
            }
        }
        
        if (!isExisting){
            [arrayToBeAdded addObject:apiNoteTag];
        }
    }
    
    for (JMNNoteTag *apiNoteTag in arrayToBeAdded) {
        
        //add entry to DB

        NoteTag *dbNoteTag = [self addNoteTagWithNoteSyncId:apiNoteTag.userNoteId andTagSyncId:apiNoteTag.userTagId useMainThread:NO];
        
        if (dbNoteTag == nil)
            continue;
        
        //update entry in DB with date modify from api and sync id
        dbNoteTag.syncId = [NSNumber numberWithInt:apiNoteTag.id];
        dbNoteTag.modified = [apiNoteTag.dateModified dateFromString];
        [self editNoteTag:dbNoteTag useMainThread:NO];
    }
}

-(void)insertAPINoteTags:(JMNRecordNoteTag*)apiNoteTags fromDBNoteTags:(NSMutableArray*)dbNoteTags{
    
    for (NoteTag *dbNoteTag in dbNoteTags) {
        if (0 < dbNoteTag.syncId.intValue)
            continue;
        
        JMNNoteTag *apiNoteTag = [JMNNoteTag new];
        apiNoteTag.userTagId = dbNoteTag.tag.syncId.intValue;
        apiNoteTag.userNoteId = dbNoteTag.note.syncId.intValue;
        
        NSString *jsonString = [apiNoteTag toJSONString];
        
        
        JSONModelError *jsonModelError;
        NSNumber *numStatusCode;
        NSError *parseError;
        
        NSString *jsonOut = [self APICallFromURLString:[Utils buildUrl:kEndPointAddNoteTag] withMethod:kHTTPMethodPOST withBody:jsonString withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"note_tags"];
        
        if (jsonModelError == nil){
            JMNRecordNoteTag *apiNoteTags = [[JMNRecordNoteTag alloc] initWithString:jsonOut error:&parseError];
            JMNNoteTag *apiNoteTag = [apiNoteTags.note_tags objectAtIndex:0];
            
            dbNoteTag.modified = [apiNoteTag.dateModified dateFromString];
            dbNoteTag.syncId = [NSNumber numberWithInt:apiNoteTag.id];
            [self editNoteTag:dbNoteTag useMainThread:NO];
        }
    }
}

-(void)deleteDBNoteTags:(NSMutableArray*)dbNoteTags fromAPINoteTags:(JMNRecordNoteTag*)apiNoteTags{
    
    NSMutableArray *arrayToBeDeleted = [NSMutableArray new];
    
    for (NoteTag *dbNoteTag in dbNoteTags) {
        if (dbNoteTag.isRemoved.boolValue == YES)
            continue;
        
        bool isExisting = false;
        
        for (JMNNoteTag *apiNoteTag in apiNoteTags.note_tags) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbNoteTag.syncId.intValue <= 0)
                continue;
            
            if (dbNoteTag.syncId.intValue == apiNoteTag.id){
                isExisting = true;
                break;
            }
        }
        
        if (!isExisting){
            [arrayToBeDeleted addObject:dbNoteTag];
        }
    }
    
    for (NoteTag *dbNoteTag in arrayToBeDeleted) {
        if (dbNoteTag.tag.userId.intValue == [self account].user.id)
            [self deleteNoteTag:dbNoteTag useMainThread:NO];
    }
}

-(void)deleteAPINoteTags:(JMNRecordNoteTag*)apiNoteTags fromDBNoteTags:(NSMutableArray*)dbNoteTags{
    for (NoteTag *dbNoteTag in dbNoteTags) {
        if (dbNoteTag.syncId.intValue <= 0)
            continue;
        
        for (JMNNoteTag *apiNoteTag in apiNoteTags.note_tags) {
            if (dbNoteTag.syncId.intValue == apiNoteTag.id && dbNoteTag.isRemoved.boolValue == YES){
                
                NSString *jsonString = [apiNoteTag toJSONString];
                
                JSONModelError *jsonModelError;
                NSNumber *numStatusCode;
                NSError *parseError;
                
                NSString *jsonOut = [self APICallFromURLString:[Utils buildUrl:kEndPointDeleteNoteTagById] withMethod:kHTTPMethodPOST withBody:jsonString withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"note_tags"];
                
                if (jsonModelError == nil){
                    JMNRecordNoteTag *apiNoteTags = [[JMNRecordNoteTag alloc] initWithString:jsonOut error:&parseError];
                    JMNNoteTag *apiNoteTag = [apiNoteTags.note_tags objectAtIndex:0];
                    
                    NoteTag *dbNoteTag = [self getNoteTagBySyncId:apiNoteTag.id useMainThread:NO];
                    dbNoteTag.modified = [apiNoteTag.dateModified dateFromString];
                    [self editNoteTag:dbNoteTag useMainThread:NO];
                }
            }
        }
    }
}

-(void)updateDBNoteTags:(NSMutableArray*)dbNoteTags fromAPINoteTags:(JMNRecordNoteTag*)apiNoteTags{
    for (NoteTag *dbNoteTag in dbNoteTags) {
        
        for (JMNNoteTag *apiNoteTag in apiNoteTags.note_tags) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbNoteTag.syncId.intValue <= 0)
                continue;
            
            if (dbNoteTag.syncId.intValue == apiNoteTag.id && dbNoteTag.isRemoved.boolValue == NO){
                
                //                if (dbNoteTag.modified < [apiNoteTag.dateModified dateFromString]){
                if ([[apiNoteTag.dateModified dateFromString] compare:dbNoteTag.modified] == NSOrderedDescending){
                    
                    dbNoteTag.modified = [apiNoteTag.dateModified dateFromString];
                    [self editNoteTag:dbNoteTag useMainThread:NO];
                }
                break;
            }
        }
    }
}

-(void)updateAPINoteTags:(JMNRecordNoteTag*)apiNoteTags fromDBNoteTags:(NSMutableArray*)dbNoteTags{
    for (NoteTag *dbNoteTag in dbNoteTags) {
        
        for (JMNNoteTag *apiNoteTag in apiNoteTags.note_tags) {
            //skip db entries that are not sync yet, as for sure there is no match in the API
            if (dbNoteTag.syncId.intValue <= 0)
                continue;
            
            if (dbNoteTag.syncId.intValue == apiNoteTag.id && dbNoteTag.isRemoved.boolValue == NO){
                
                if ([dbNoteTag.modified compare:[apiNoteTag.dateModified dateFromString]] == NSOrderedDescending){
                    
                    NSString *jsonString = [apiNoteTag toJSONString];
                    
                    JSONModelError *jsonModelError;
                    NSNumber *numStatusCode;
                    NSError *parseError;
                    
                    NSString *jsonOut = [self APICallFromURLString:[Utils buildUrl:kEndPointEditNoteTagById] withMethod:kHTTPMethodPOST withBody:jsonString withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"note_tags"];
                    
                    if (jsonModelError == nil){
                        JMNRecordNoteTag *apiNoteTags = [[JMNRecordNoteTag alloc] initWithString:jsonOut error:&parseError];
                        JMNNoteTag *apiNoteTag = [apiNoteTags.note_tags objectAtIndex:0];
                        
                        NoteTag *dbNoteTag = [self getNoteTagBySyncId:apiNoteTag.id useMainThread:NO];
                        dbNoteTag.modified = [apiNoteTag.dateModified dateFromString];
                        [self editNoteTag:dbNoteTag useMainThread:NO];
                    }
                }
                break;
            }
        }
    }
}

-(NSString*)APICallFromURLString:(NSString*)webUrl withMethod:(NSString*)method withBody:(NSString*)bodyString withHeaders:(NSDictionary*)headers outError:(JSONModelError**)jsonModelError vsStatusCode:(NSNumber**)vsStatusCode lookingFor:(NSString*)jsonNode{
    
    NSString *jsonString;
    
    //NOTE: Update the headers
    
    NSDictionary *headerObject = nil;
    
    if (headers) {
        
        NSMutableDictionary *data = [NSMutableDictionary dictionary];

        // set all previous headers
        for (NSString *key in [headers allKeys] ) {
            NSString *value = headers[key];
            [data setValue:value forKey:key];
        }
        
        // set school code in base64 representation
        [data setValue:[Utils getSettingsSchoolCodeBase64] forKey:@"code"];
        
        headerObject = [NSDictionary dictionaryWithDictionary:data];
        
    } else {
        
        //if header is nil just create a header
        headerObject = @{@"code":[Utils getSettingsSchoolCodeBase64]};
    }
    
    NSData *responseData = [JSONHTTPClient syncRequestDataFromURL:[NSURL URLWithString:webUrl] method:method requestBody:bodyString headers:headerObject etag:nil error:jsonModelError];
    
    //    NSString *jsonRaw = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    
    if (*jsonModelError == nil){
        
        NSDictionary *dictRecords = [dict objectForKey:@"records"];
        NSArray *array = [dictRecords objectForKey:jsonNode];
        *vsStatusCode = [dictRecords objectForKey:@"vs_status_code"];
        
        if (array){
            NSError *parseError = nil;
            NSData *records = [NSJSONSerialization dataWithJSONObject:dictRecords options:NSJSONWritingPrettyPrinted error:&parseError];
            jsonString = [[NSString alloc] initWithData:records encoding:NSUTF8StringEncoding];
        }
        else{
            if ([*vsStatusCode intValue] != 100)
                VLog("%@", *vsStatusCode);
        }
    }
    else{
        VLog("%@", *jsonModelError);
    }
    
    return jsonString;
}

//Online Calls
#pragma mark - Online API Notebooks
-(JMNRecordNotebook*)APIGetAllNotebooksByUserIdOutError:(NSError**)error statusCode:(NSNumber**)statusCode{

    JMNRecordNotebook *result;
    
    JSONModelError *jsonModelError;
    NSNumber *numStatusCode;
    NSError *parseError;
    
    NSString *jsonString = [self APICallFromURLString:VS_FMT([Utils buildUrl:kEndPointGetAllNotebooksByUserId], [self account].user.id) withMethod:kHTTPMethodGET withBody:nil withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"notebooks"];
    
    if (jsonModelError == nil || numStatusCode.intValue == 100)
        result = [[JMNRecordNotebook alloc] initWithString:jsonString error:&parseError];

    *statusCode = numStatusCode;
    
    return result;
}
#pragma mark - Online API Tags
-(JMNRecordTag*)APIGetAllTagsByUserIdOutError:(NSError**)error statusCode:(NSNumber**)statusCode{
    
    JMNRecordTag *result;
    
    JSONModelError *jsonModelError;
    NSNumber *numStatusCode;
    NSError *parseError;
    
    NSString *jsonString = [self APICallFromURLString:VS_FMT([Utils buildUrl:kEndPointGetAllTagsByUserId], [self account].user.id) withMethod:kHTTPMethodGET withBody:nil withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"user_tags"];
    
    if (jsonModelError == nil || numStatusCode.intValue == 100)
        result = [[JMNRecordTag alloc] initWithString:jsonString error:&parseError];
    
    *statusCode = numStatusCode;
    
    return result;
}
#pragma mark - Online API Notes
-(JMNRecordNote*)APIGetAllNotesByUserIdOutError:(NSError**)error statusCode:(NSNumber**)statusCode{
    
    JMNRecordNote *result;
    
    JSONModelError *jsonModelError;
    NSNumber *numStatusCode;
    NSError *parseError;
    
    NSString *jsonString = [self APICallFromURLString:VS_FMT([Utils buildUrl:kEndPointGetAllNotesByUserId], [self account].user.id) withMethod:kHTTPMethodGET withBody:nil withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"user_notes"];
    
    if (jsonModelError == nil || numStatusCode.intValue == 100)
        result = [[JMNRecordNote alloc] initWithString:jsonString error:&parseError];
    
    *statusCode = numStatusCode;
    
    return result;
}
#pragma mark - Online API NoteTags
-(JMNRecordNoteTag*)APIGetAllNoteTagsByUserIdOutError:(NSError**)error statusCode:(NSNumber**)statusCode{
    
    JMNRecordNoteTag *result;
    
    JSONModelError *jsonModelError;
    NSNumber *numStatusCode;
    NSError *parseError;
    
    NSString *jsonString = [self APICallFromURLString:VS_FMT([Utils buildUrl:kEndPointGetNoteTagByUserId], [self account].user.id) withMethod:kHTTPMethodGET withBody:nil withHeaders:nil outError:&jsonModelError vsStatusCode:&numStatusCode lookingFor:@"note_tags"];
    
    if (jsonModelError == nil || numStatusCode.intValue == 100)
        result = [[JMNRecordNoteTag alloc] initWithString:jsonString error:&parseError];
    
    *statusCode = numStatusCode;
    
    return result;
}

//DB Calls
#pragma mark - Core Data
-(NSManagedObjectContext *) managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return _managedObjectContext;
}

-(NSManagedObjectContext *) managedObjectContextInBackground {
    if (_managedObjectContextInBackground != nil) {
        return _managedObjectContextInBackground;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContextInBackground = [[NSManagedObjectContext alloc] init];
        [_managedObjectContextInBackground setPersistentStoreCoordinator: coordinator];
    }
    
    return _managedObjectContextInBackground;
}

-(NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return _managedObjectModel;
}
-(NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"ModuleModel.sqlite"]];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:[self managedObjectModel]];
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil
                                                            URL:storeUrl
                                                        options:nil
                                                          error:&error]) {
        /*Error for store creation should be handled in here*/
        
        //////////////////////////////////////
        // REMOVE THE INCOMPATIBLE STORE FILE
        //////////////////////////////////////
        
        NSFileManager *fm = [NSFileManager defaultManager];
        NSString *filePath = [NSString stringWithFormat:@"%@", storeUrl.path];
        
        if ([fm fileExistsAtPath:filePath]) {
            NSError *error = nil;
            NSDictionary *metaData = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
                                                                                                URL:storeUrl
                                                                                              error:&error];
            NSManagedObjectModel *model = self.persistentStoreCoordinator.managedObjectModel;
            if (![model isConfiguration:nil compatibleWithStoreMetadata:metaData]) {
                
                //REMOVE THE STORE FILE
                if ([fm removeItemAtPath:filePath error:NULL]) {

                    //////////////////////////////////////
                    // REBUILD THE NEW STORE FILE
                    //////////////////////////////////////
                    
                    [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                                       configuration:nil
                                                                                 URL:storeUrl
                                                                             options:nil
                                                                               error:nil];
                }
            }
        }
        
    }
    
    return _persistentStoreCoordinator;
}

-(NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

-(NSManagedObjectContext*)getManageObjectContextFromMainThread:(BOOL)useMainThread{
    if (useMainThread)
        return [self managedObjectContext];
    else
        return [self managedObjectContextInBackground];
}

+ (id) sharedInstance {
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}
-(id)init {
    if ((self = [super init])) {
        //register for this on the background thread
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self selector:@selector(mergeChanges:) name:NSManagedObjectContextDidSaveNotification object:[self managedObjectContextInBackground]];
    }
    return self;
}

- (void)mergeChanges:(NSNotification *)notification {
    
    //this tells the main thread moc to run on the main thread, and merge in the changes there
    [[self managedObjectContext] performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:) withObject:notification waitUntilDone:YES];
}

-(NSArray *)fetchObjectsForEntityName:(NSString *)entityName
                          useMainThread:(BOOL)useMainThread withPredicate:(NSString*)stringFilter, ...{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    //add sorting mechanism (hardcoded 'modified' since all tables have that attribute)
    NSSortDescriptor *sortByModifiedDate = [[NSSortDescriptor alloc] initWithKey:@"modified" ascending:NO];
    [request setSortDescriptors:[NSArray arrayWithObject:sortByModifiedDate]];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:[self getManageObjectContextFromMainThread:useMainThread]];
    
    [request setEntity:entity];
    
    if (stringFilter){
        NSPredicate *predicate;
        va_list variadicArguments;
        va_start(variadicArguments, stringFilter);
        predicate = [NSPredicate predicateWithFormat:stringFilter arguments:variadicArguments];
        va_end(variadicArguments);
        
        [request setPredicate:predicate];
    }
    
    NSError *error;
    
    NSArray *array = [[self getManageObjectContextFromMainThread:useMainThread] executeFetchRequest:request error:&error];
    
    return array;
}

#pragma mark - Notebook
-(NoteBook*)getNoteBookByNoteBookName:(NSString*)folderName useMainThread:(BOOL)useMainThread{
    NoteBook *returnObject;
    
    NSArray *result = [self fetchObjectsForEntityName:kTableNoteBook useMainThread:useMainThread withPredicate:nil];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"noteBookName == %@", folderName];
    result = [result filteredArrayUsingPredicate:pred];

    pred = [NSPredicate predicateWithFormat:@"userId == %i", [self account].user.id];
    result = [result filteredArrayUsingPredicate:pred];
    
    if (result != nil) {
        if (0 < result.count)
            returnObject = [result objectAtIndex:0];
    }

    return returnObject;
}
-(NoteBook*)getNoteBookBySyncId:(int)syncId useMainThread:(BOOL)useMainThread{
    NoteBook *returnObject;
    
    NSArray *result = [self fetchObjectsForEntityName:kTableNoteBook useMainThread:useMainThread withPredicate:nil];

    NSPredicate *pred = [NSPredicate predicateWithFormat:@"syncId == %@", [NSNumber numberWithInt:syncId]];
    result = [result filteredArrayUsingPredicate:pred];
    
    if (result != nil) {
        if (0 < result.count)
            returnObject = [result objectAtIndex:0];
    }
    
    return returnObject;
}
-(NSArray*)getNoteBooks:(BOOL)withRemovedItems useMainThread:(BOOL)useMainThread{
    
//    //create 'General' folder as a default folder one time
//    NoteBook *defaultNoteBook = [self getNoteBookByNoteBookName:kNotebookGeneralName useMainThread:useMainThread];
//    if (defaultNoteBook == nil || defaultNoteBook.noteBookID == nil){
//        defaultNoteBook = [self addNoteBook:kNotebookGeneralName withColor:0 useMainThread:useMainThread];
//    }
//    
//    if (defaultNoteBook.isRemoved.boolValue == YES){ //this is just mitigation, this should never happen
//        defaultNoteBook.isRemoved = [NSNumber numberWithBool:NO];
//        [self editNoteBook:defaultNoteBook useMainThread:useMainThread];
//    }
//    
//    NSArray *result = [self fetchObjectsForEntityName:kTableNoteBook useMainThread:useMainThread withPredicate:nil];
//    
//    if (!withRemovedItems){
//        NSPredicate *pred = [NSPredicate predicateWithFormat:@"isRemoved == %@", [NSNumber numberWithBool:NO]];
//        result = [result filteredArrayUsingPredicate:pred];
//    }
//    
//    NSPredicate *pred = [NSPredicate predicateWithFormat:@"userId == %i", [self account].user.id];
//    result = [result filteredArrayUsingPredicate:pred];
//    
//    NSSortDescriptor *sortByNoteBookName = [[NSSortDescriptor alloc] initWithKey:@"noteBookName" ascending:YES];
//    result = [result sortedArrayUsingDescriptors:@[sortByNoteBookName]];
//    
//    return result;
    
    NoteBook *defaultNoteBook = [self getNoteBookByNoteBookName:kNotebookGeneralName useMainThread:useMainThread];
    
    if (defaultNoteBook == nil) {
        defaultNoteBook = [self addNoteBook:kNotebookGeneralName withColor:0 useMainThread:useMainThread];
    }
    
    if (defaultNoteBook.isRemoved.boolValue == YES) {
        defaultNoteBook.isRemoved = [NSNumber numberWithBool:NO];
        [self editNoteBook:defaultNoteBook useMainThread:useMainThread];
    }
    
    NSArray *result = [self fetchObjectsForEntityName:kTableNoteBook useMainThread:useMainThread withPredicate:nil];
    
    if (!withRemovedItems) {
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"isRemoved == %@", [NSNumber numberWithBool:NO]];
        result = [result filteredArrayUsingPredicate:pred];
    }
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"userId == %i", [self account].user.id];
    result = [result filteredArrayUsingPredicate:pred];
    
    NSSortDescriptor *sortByNoteBookName = [[NSSortDescriptor alloc] initWithKey:@"noteBookName" ascending:YES];
    result = [result sortedArrayUsingDescriptors:@[sortByNoteBookName]];
    
    return result;
}
-(NoteBook*)addNoteBook:(NSString*)folderName withColor:(int)colorId useMainThread:(BOOL)useMainThread{
    NoteBook * noteBook = [NSEntityDescription insertNewObjectForEntityForName:kTableNoteBook
                                                   inManagedObjectContext:[self getManageObjectContextFromMainThread:useMainThread]];
    
    // Added Attribute to NoteBook Entity
    noteBook.noteBookID = [[NSUUID UUID] UUIDString];
    
    noteBook.created = [NSDate date];
    noteBook.noteBookName = folderName;
    noteBook.colorId = [NSNumber numberWithInt:colorId];
    noteBook.modified = [NSDate date];
    noteBook.isRemoved = [NSNumber numberWithBool:NO];
    noteBook.userId = [NSNumber numberWithInt:[self account].user.id];
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"addNoteBook, couldn't save: %@", [error localizedDescription]);
    }
    
    return noteBook;
}

-(BOOL)deleteNoteBook:(NoteBook*)folder useMainThread:(BOOL)useMainThread{
    bool deleteSuccess = true;

    folder.isRemoved = [NSNumber numberWithBool:YES];
    folder.modified = [NSDate date];
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"deleteNoteBook, couldn't save: %@", [error localizedDescription]);
        deleteSuccess = false;
    }
    
    return deleteSuccess;
}
-(BOOL)editNoteBook:(NoteBook*)folder useMainThread:(BOOL)useMainThread{
    bool renameSuccess = true;
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"editNoteBook, couldn't save: %@", [error localizedDescription]);
        renameSuccess = false;
    }
    
    return renameSuccess;
}
-(BOOL)shareFolder:(NoteBook*)noteBook isShared:(BOOL)share useMainThread:(BOOL)useMainThread{
    bool shareSuccess = true;
    
    noteBook.isShared = [NSNumber numberWithBool:share];
    noteBook.modified = [NSDate date];
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"shareFolder, couldn't save: %@", [error localizedDescription]);
        shareSuccess = false;
    }
    
    return shareSuccess;
    
}

- (NoteBook *)getNoteBookByNoteBookID:(NSString *)noteBookID useMainThread:(BOOL)useMainThread {
    NoteBook *returnObject;
    
    NSArray *result = [self fetchObjectsForEntityName:kTableNoteBook useMainThread:useMainThread withPredicate:nil];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"noteBookID == %@", noteBookID];
    result = [result filteredArrayUsingPredicate:pred];
    
    pred = [NSPredicate predicateWithFormat:@"userId == %i", [self account].user.id];
    result = [result filteredArrayUsingPredicate:pred];
    
    if (result != nil) {
        if (0 < result.count)
            returnObject = [result objectAtIndex:0];
    }
    
    return returnObject;
}

#pragma mark - Tag
-(NSArray*)getTags:(BOOL)withRemovedItems useMainThread:(BOOL)useMainThread{
    
    NSArray *result = [self fetchObjectsForEntityName:kTableTag useMainThread:useMainThread withPredicate:nil];
    
    if (!withRemovedItems){
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"isRemoved == %@", [NSNumber numberWithBool:NO]];
        result = [result filteredArrayUsingPredicate:pred];
    }
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"userId == %i", [self account].user.id];
    result = [result filteredArrayUsingPredicate:pred];
    
    NSSortDescriptor *sortByTagName = [[NSSortDescriptor alloc] initWithKey:@"tagName" ascending:YES];
    result = [result sortedArrayUsingDescriptors:@[sortByTagName]];
    
    return result;
}
-(Tag*)getTagByTagName:(NSString*)tagName useMainThread:(BOOL)useMainThread{
    Tag *returnObject;

    NSArray *result = [self fetchObjectsForEntityName:kTableTag useMainThread:useMainThread withPredicate:nil];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"tagName == %@", tagName];
    result = [result filteredArrayUsingPredicate:pred];

    pred = [NSPredicate predicateWithFormat:@"userId == %i", [self account].user.id];
    result = [result filteredArrayUsingPredicate:pred];
    
    if (result != nil) {
        if (0 < result.count)
            returnObject = [result objectAtIndex:0];
    }
    
    return returnObject;
}
-(Tag*)getTagBySyncId:(int)syncId useMainThread:(BOOL)useMainThread{
    Tag *returnObject;
    
    NSArray *result = [self fetchObjectsForEntityName:kTableTag useMainThread:useMainThread withPredicate:nil];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"syncId == %@", [NSNumber numberWithInt:syncId]];
    result = [result filteredArrayUsingPredicate:pred];

    
    if (result != nil) {
        if (0 < result.count)
            returnObject = [result objectAtIndex:0];
    }
    
    return returnObject;
}
-(Tag*)addTag:(NSString*)tagName useMainThread:(BOOL)useMainThread{

    Tag * tag = [NSEntityDescription insertNewObjectForEntityForName:kTableTag
                                                inManagedObjectContext:[self getManageObjectContextFromMainThread:useMainThread]];
    
    tag.created = [NSDate date];
    tag.modified = [NSDate date];
    tag.tagName = tagName;
    tag.isRemoved = [NSNumber numberWithBool:NO];
    tag.userId = [NSNumber numberWithInt:[self account].user.id];
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"addTag, couldn't save: %@", [error localizedDescription]);
    }
    
    return tag;
}
-(BOOL)deleteTag:(Tag*)tag useMainThread:(BOOL)useMainThread{
    bool deleteSuccess = true;

    tag.isRemoved = [NSNumber numberWithBool:YES];
    tag.modified = [NSDate date];
    
    for (NoteTag *noteTag in tag.noteTags) {
        noteTag.isRemoved = [NSNumber numberWithBool:YES];
        noteTag.modified = [NSDate date];
    }
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"deleteTag, couldn't save: %@", [error localizedDescription]);
        deleteSuccess = false;
    }
    
    return deleteSuccess;
}
-(BOOL)editTag:(Tag*)tag useMainThread:(BOOL)useMainThread{
    bool renameSuccess = true;
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"editTag, couldn't save: %@", [error localizedDescription]);
        renameSuccess = false;
    }
    
    return renameSuccess;
}

#pragma mark - Note
-(Note*)getNoteBySyncId:(int)syncId useMainThread:(BOOL)useMainThread{
    Note *returnObject;
    
    NSArray *result = [self fetchObjectsForEntityName:kTableNote useMainThread:useMainThread withPredicate:nil];

    NSPredicate *pred = [NSPredicate predicateWithFormat:@"syncId == %@", [NSNumber numberWithInt:syncId]];
    result = [result filteredArrayUsingPredicate:pred];
    
    if (result != nil) {
        if (0 < result.count)
            returnObject = [result objectAtIndex:0];
    }
    
    return returnObject;
}
-(NSArray*)getNotes:(BOOL)withRemovedItems useMainThread:(BOOL)useMainThread{
    
    NSArray *array = [self fetchObjectsForEntityName:kTableNote useMainThread:useMainThread withPredicate:nil];
    
    if (!withRemovedItems){
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"isRemoved == %@", [NSNumber numberWithBool:NO]];
        array = [array filteredArrayUsingPredicate:pred];
    }
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"folder.userId == %i", [self account].user.id];
    array = [array filteredArrayUsingPredicate:pred];
    
    return array;
}
-(Note*)addNote:(NoteEntry*)entry useMainThread:(BOOL)useMainThread{
    
    Note * note = [NSEntityDescription insertNewObjectForEntityForName:kTableNote
                                                   inManagedObjectContext:[self getManageObjectContextFromMainThread:useMainThread]];
    
    
    //NoteBook *folder = [self getNoteBookByNoteBookName:entry.noteBookName useMainThread:useMainThread];
    
    // Bug Fix
    // Filter by NoteBookID
    NoteBook *folder = [self getNoteBookByNoteBookID:entry.noteBookID useMainThread:useMainThread];
    
    // If General Notebook
    if ([entry.noteBookName isEqualToString:kNotebookGeneralName]) {
        folder = [self getNoteBookByNoteBookName:entry.noteBookName useMainThread:useMainThread];
    }
  
    note.colorId = [NSNumber numberWithInt:entry.color];
    note.content = entry.description;
    note.created = [NSDate date];
    note.folder = folder;
    note.title = entry.title;
    note.modified = [NSDate date];
    note.isRemoved = [NSNumber numberWithBool:NO];
    note.isShared = [NSNumber numberWithBool:NO];
    note.fontTypeId = [NSNumber numberWithInt:entry.fontTypeId];
    note.fontSizeId = [NSNumber numberWithInt:entry.fontSizeId];
    note.isArchive = [NSNumber numberWithBool:entry.isArchive];
    
    NSArray *tagArray = [entry.tags componentsSeparatedByString:@";"];
    
    for (NSString *entryTag in tagArray) {
        NSString *trimString = [entryTag stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

        if (trimString.length <= 0) {
            continue;
        }
        
        // Check if tag is already existing
        Tag *tag = [self getTagByTagName:entryTag useMainThread:useMainThread];
        
        if (tag == nil){
            tag = [self addTag:entryTag useMainThread:useMainThread];
        }
        
        NoteTag *noteTag = [self addNoteTagWithNote:note andTag:tag useMainThread:useMainThread];
        
        if (!noteTag){
            NSLog(@"no noteTag");
        }
    }
    
    NSError *error;
    
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"addNote, couldn't save: %@", [error localizedDescription]);
    }
    
    return note;
}
-(Note*)editNote:(NoteEntry*)entry sourceNote:(Note*)note useMainThread:(BOOL)useMainThread{

    //NoteBook *folder = [self getNoteBookByNoteBookName:entry.noteBookName useMainThread:useMainThread];
    
    // Bug Fix
    // Filter by NoteBookID
    NoteBook *folder = [self getNoteBookByNoteBookID:entry.noteBookID useMainThread:useMainThread];
    
    // If General Notebook
    if ([entry.noteBookName isEqualToString:kNotebookGeneralName]) {
        folder = [self getNoteBookByNoteBookName:entry.noteBookName useMainThread:useMainThread];
    }
    
    note.colorId = [NSNumber numberWithInt:entry.color];
    note.fontTypeId = [NSNumber numberWithInt:entry.fontTypeId];
    note.fontSizeId = [NSNumber numberWithInt:entry.fontSizeId];
    note.isArchive = [NSNumber numberWithBool:entry.isArchive];
    
    note.content = entry.description;
    note.created = [NSDate date];
    note.folder = folder;
    note.title = entry.title;
    note.modified = [NSDate date];
    
    NSArray *tagArray = [entry.tags componentsSeparatedByString:@";"];
    
    //NoteTags creation and update
    for (NSString *entryTag in tagArray) {
        
        NSString *tagString = [entryTag stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if ([tagString isEqualToString:@""])
            continue;
        
        //check if tag is already existing, if not create it
        Tag *tag = [self getTagByTagName:tagString useMainThread:useMainThread];
        if (tag == nil){
            tag = [self addTag:tagString useMainThread:useMainThread];
        }
        
        //create note tags relationship
        //but first check if tag is already mapped in note
        bool isNoteTagExisting = false;
        for (NoteTag *noteTag in note.noteTags) {
            if ([noteTag.tag.tagName isEqualToString:tagString]){
                isNoteTagExisting = true;
                break;
            }
        }
        
        if (!isNoteTagExisting){
            NoteTag *noteTag = [self addNoteTagWithNote:note andTag:tag useMainThread:useMainThread];
            if (!noteTag){
                NSLog(@"no noteTag");
            }
        }
    }
    
    //NoteTags soft deletion
    for (NoteTag *noteTag in note.noteTags) {
        bool isNoteTagExisting = false;
        
        for (NSString *entryTag in tagArray) {
            
            NSString *tagString = [entryTag stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if ([tagString isEqualToString:@""])
                continue;
            
            if ([noteTag.tag.tagName isEqualToString:tagString]){
                isNoteTagExisting = true;
                break;
            }
        }
        
        if (!isNoteTagExisting){
            noteTag.isRemoved = [NSNumber numberWithBool:YES];
        }
    }
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"editNote, couldn't save: %@", [error localizedDescription]);
    }
    
    return note;
}
-(BOOL)deleteNote:(Note*)note useMainThread:(BOOL)useMainThread{
    bool deleteSuccess = true;

    note.isRemoved = [NSNumber numberWithBool:YES];
    note.modified = [NSDate date];
    
    for (NoteTag *noteTag in note.noteTags) {
        noteTag.isRemoved = [NSNumber numberWithBool:YES];
        noteTag.modified = [NSDate date];
    }
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"deleteNote, couldn't save: %@", [error localizedDescription]);
        deleteSuccess = false;
    }
    
    return deleteSuccess;
}
-(BOOL)editNote:(Note*)note useMainThread:(BOOL)useMainThread{
    bool deleteSuccess = true;

    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"editNote, couldn't save: %@", [error localizedDescription]);
        deleteSuccess = false;
    }
    
    return deleteSuccess;
}

-(void)moveNote:(Note *)note toNoteBook:(NoteBook *)noteBook useMainThread:(BOOL)useMainThread {
    note.folder = noteBook;
 
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"moveNote, couldn't move: %@", [error localizedDescription]);
    }
}

#pragma mark - NoteTag
-(NoteTag*)addNoteTagWithNoteSyncId:(int)noteSyncId andTagSyncId:(int)tagSyncId useMainThread:(BOOL)useMainThread{
    
    NoteTag * noteTag = [NSEntityDescription insertNewObjectForEntityForName:kTableNoteTag
                                                inManagedObjectContext:[self getManageObjectContextFromMainThread:useMainThread]];
    
    
    Note *note = [self getNoteBySyncId:noteSyncId useMainThread:useMainThread];
    Tag *tag = [self getTagBySyncId:tagSyncId useMainThread:useMainThread];
    
    noteTag.created = [NSDate date];
    noteTag.modified = [NSDate date];
    noteTag.isRemoved = [NSNumber numberWithBool:NO];
    noteTag.note = note;
    noteTag.tag = tag;
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"addNoteTagWithNoteSyncId, couldn't save: %@", [error localizedDescription]);
    }
    
    return noteTag;
}
-(NoteTag*)addNoteTagWithNote:(Note*)note andTag:(Tag*)tag useMainThread:(BOOL)useMainThread{
    
    NoteTag * noteTag = [NSEntityDescription insertNewObjectForEntityForName:kTableNoteTag
                                                      inManagedObjectContext:[self getManageObjectContextFromMainThread:useMainThread]];
    
    noteTag.created = [NSDate date];
    noteTag.modified = [NSDate date];
    noteTag.isRemoved = [NSNumber numberWithBool:NO];
    noteTag.note = note;
    noteTag.tag = tag;
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"addNoteTagWithNote, couldn't save: %@", [error localizedDescription]);
    }
    
    return noteTag;
}
-(NSArray*)getNoteTags:(BOOL)withRemovedItems useMainThread:(BOOL)useMainThread{
    
    NSArray *array = [self fetchObjectsForEntityName:kTableNoteTag useMainThread:useMainThread withPredicate:nil];
    
    if (!withRemovedItems){
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"isRemoved == %@", [NSNumber numberWithBool:NO]];
        array = [array filteredArrayUsingPredicate:pred];
    }
    
    return array;
}
-(NoteTag*)getNoteTagBySyncId:(int)syncId useMainThread:(BOOL)useMainThread{
    NoteTag *returnObject;
    
    NSArray *result = [self fetchObjectsForEntityName:kTableNoteTag useMainThread:useMainThread withPredicate:nil];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"syncId == %@", [NSNumber numberWithInt:syncId]];
    result = [result filteredArrayUsingPredicate:pred];
    
    if (result != nil) {
        if (0 < result.count)
            returnObject = [result objectAtIndex:0];
    }
    
    return returnObject;
}
-(NoteTag*)getNoteTagByNoteSyncId:(int)noteSyncId useMainThread:(BOOL)useMainThread{
    NoteTag *returnObject;
    
    NSArray *result = [self fetchObjectsForEntityName:kTableNoteTag useMainThread:useMainThread withPredicate:nil];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"note.syncId == %@", [NSNumber numberWithInt:noteSyncId]];
    result = [result filteredArrayUsingPredicate:pred];
    
    if (result != nil) {
        if (0 < result.count)
            returnObject = [result objectAtIndex:0];
    }
    
    return returnObject;
}
-(NoteTag*)getNoteTagByTagSyncId:(int)tagSyncId useMainThread:(BOOL)useMainThread{
    NoteTag *returnObject;
    
    NSArray *result = [self fetchObjectsForEntityName:kTableNoteTag useMainThread:useMainThread withPredicate:nil];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"tag.syncId == %@", [NSNumber numberWithInt:tagSyncId]];
    result = [result filteredArrayUsingPredicate:pred];
    
    if (result != nil) {
        if (0 < result.count)
            returnObject = [result objectAtIndex:0];
    }
    
    return returnObject;
}
-(BOOL)deleteNoteTag:(NoteTag*)noteTag useMainThread:(BOOL)useMainThread{
    bool deleteSuccess = true;
    
    noteTag.isRemoved = [NSNumber numberWithBool:YES];
    noteTag.modified = [NSDate date];
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"deleteNoteTag, couldn't save: %@", [error localizedDescription]);
        deleteSuccess = false;
    }
    
    return deleteSuccess;
}
-(BOOL)editNoteTag:(NoteTag*)noteTag useMainThread:(BOOL)useMainThread{
    bool editSuccess = true;
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"editNoteTag, couldn't save: %@", [error localizedDescription]);
        editSuccess = false;
    }
    
    return editSuccess;
}

#pragma mark - Color
-(NSArray*)getColors:(BOOL)withRemovedItems useMainThread:(BOOL)useMainThread{
    
    //check if color exist
    Color *defaultColor = [self getColorByColorId:0 useMainThread:useMainThread];
    if (defaultColor == nil){
        [self addColor:@"Red" withColorId:0 withRed:226 withGreen:105 withBlue:96 withHex:@"E26960" useMainThread:useMainThread];
        [self addColor:@"Blue" withColorId:1 withRed:22 withGreen:153 withBlue:239 withHex:@"1699EF" useMainThread:useMainThread];
        [self addColor:@"Yellow" withColorId:2 withRed:203 withGreen:198 withBlue:119 withHex:@"CBC677" useMainThread:useMainThread];
        [self addColor:@"Green" withColorId:3 withRed:91 withGreen:188 withBlue:128 withHex:@"5BBC80" useMainThread:useMainThread];
        [self addColor:@"Purple" withColorId:4 withRed:158 withGreen:175 withBlue:222 withHex:@"9EAFDE" useMainThread:useMainThread];
        [self addColor:@"White" withColorId:5 withRed:198 withGreen:202 withBlue:204 withHex:@"C6CACC" useMainThread:useMainThread];
    }
    
    NSArray *returnArray = [self fetchObjectsForEntityName:kTableColor useMainThread:useMainThread withPredicate:nil];
    
    return returnArray;
}
-(Color*)getColorByColorId:(int)colorId useMainThread:(BOOL)useMainThread{
    NSArray *result = [self fetchObjectsForEntityName:kTableColor useMainThread:useMainThread withPredicate:@"colorId == %d", colorId];
    
    Color *returnObject;
    
    if (result != nil) {
        if (0 < result.count)
            returnObject = [result objectAtIndex:0];
    }
    
    return returnObject;
}
-(Color*)addColor:(NSString*)colorName withColorId:(int)colorId withRed:(int)red withGreen:(int)green withBlue:(int)blue  withHex:(NSString*)hex useMainThread:(BOOL)useMainThread{
    
    Color * color = [NSEntityDescription insertNewObjectForEntityForName:kTableColor
                                                        inManagedObjectContext:[self getManageObjectContextFromMainThread:useMainThread]];
    
    color.colorId = [NSNumber numberWithInt:colorId];
    color.colorName = colorName;
    color.redValue = [NSNumber numberWithInt:red];
    color.blueValue = [NSNumber numberWithInt:blue];
    color.greenValue = [NSNumber numberWithInt:green];
    color.hexValue = hex;

    color.isRemoved = [NSNumber numberWithBool:NO];
    color.created = [NSDate date];
    color.modified = [NSDate date];
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"addColor, couldn't save: %@", [error localizedDescription]);
    }
    
    return color;
}
-(UIColor*)getUIColorFromColorObject:(Color*)color{
    CGFloat red = [color.redValue floatValue] / 255;
    CGFloat green = [color.greenValue floatValue] / 255;
    CGFloat blue = [color.blueValue floatValue] / 255;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:1];
}


#pragma mark - Font Type
-(NSArray*)getFontTypes:(BOOL)withRemovedItems useMainThread:(BOOL)useMainThread{
    
    //check if color exist
    NFontType *defaultFontTypes = [self getFontTypeByFontTypeId:NO useMainThread:useMainThread];
    if (defaultFontTypes == nil){
        [self addFontType:@"Helvetica Neue" withFontTypeId:0 useMainThread:YES];
        [self addFontType:@"Rancho" withFontTypeId:1 useMainThread:YES];
        [self addFontType:@"Josefin Sans" withFontTypeId:2 useMainThread:YES];
        [self addFontType:@"Raleway" withFontTypeId:3 useMainThread:YES];
        [self addFontType:@"Ubuntu" withFontTypeId:4 useMainThread:YES];
    }
    
    NSArray *returnArray = [self fetchObjectsForEntityName:kTableFontType useMainThread:useMainThread withPredicate:nil];
    
    return returnArray;
}
-(NFontType*)getFontTypeByFontTypeId:(int)fontTypeId useMainThread:(BOOL)useMainThread{
    NSArray *result = [self fetchObjectsForEntityName:kTableFontType useMainThread:useMainThread withPredicate:@"fontTypeId == %d", fontTypeId];
    
    NFontType *returnObject;
    
    if (result != nil) {
        if (0 < result.count)
            returnObject = [result objectAtIndex:0];
    }
    
    return returnObject;
}
-(NFontType*)addFontType:(NSString*)fontTypeName withFontTypeId:(int)fontTypeId useMainThread:(BOOL)useMainThread{
    
    NFontType * fontType = [NSEntityDescription insertNewObjectForEntityForName:kTableFontType
                                                  inManagedObjectContext:[self getManageObjectContextFromMainThread:useMainThread]];
    
    fontType.fontTypeId = [NSNumber numberWithInt:fontTypeId];
    fontType.fontName = fontTypeName;
    
    fontType.isRemoved = [NSNumber numberWithBool:NO];
    fontType.created = [NSDate date];
    fontType.modified = [NSDate date];
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"addColor, couldn't save: %@", [error localizedDescription]);
    }
    
    return fontType;
}

#pragma mark - Font Size
-(NSArray*)getFontSizes:(BOOL)withRemovedItems useMainThread:(BOOL)useMainThread{
    
    //check if color exist
    NFontSize *defaultFontSizes = [self getFontSizeByFontSizeId:0 useMainThread:useMainThread];
    if (defaultFontSizes == nil){
        [self addFontSize:12 withFontSizeId:0 useMainThread:YES];
        [self addFontSize:13 withFontSizeId:1 useMainThread:YES];
        [self addFontSize:14 withFontSizeId:2 useMainThread:YES];
        [self addFontSize:15 withFontSizeId:3 useMainThread:YES];
        [self addFontSize:16 withFontSizeId:4 useMainThread:YES];
        [self addFontSize:17 withFontSizeId:5 useMainThread:YES];
        [self addFontSize:18 withFontSizeId:6 useMainThread:YES];
        [self addFontSize:19 withFontSizeId:7 useMainThread:YES];
        [self addFontSize:20 withFontSizeId:8 useMainThread:YES];
        [self addFontSize:21 withFontSizeId:9 useMainThread:YES];
        
        [self addFontSize:22 withFontSizeId:10 useMainThread:YES];
        [self addFontSize:23 withFontSizeId:11 useMainThread:YES];
        [self addFontSize:24 withFontSizeId:12 useMainThread:YES];
        [self addFontSize:25 withFontSizeId:13 useMainThread:YES];
        [self addFontSize:26 withFontSizeId:14 useMainThread:YES];
        [self addFontSize:27 withFontSizeId:15 useMainThread:YES];
        [self addFontSize:28 withFontSizeId:16 useMainThread:YES];
        [self addFontSize:29 withFontSizeId:17 useMainThread:YES];
        [self addFontSize:30 withFontSizeId:18 useMainThread:YES];
        [self addFontSize:31 withFontSizeId:19 useMainThread:YES];
    }
    
    NSArray *returnArray = [self fetchObjectsForEntityName:kTableFontSize useMainThread:useMainThread withPredicate:nil];
    
    return returnArray;
}
-(NFontSize*)getFontSizeByFontSizeId:(int)fontSize useMainThread:(BOOL)useMainThread{
    NSArray *result = [self fetchObjectsForEntityName:kTableFontSize useMainThread:useMainThread withPredicate:@"fontSizeId == %d", fontSize];
    
    NFontSize *returnObject;
    
    if (result != nil) {
        if (0 < result.count)
            returnObject = [result objectAtIndex:0];
    }
    
    return returnObject;
}
-(NFontSize*)addFontSize:(int)fontSizeValue withFontSizeId:(int)FontSizeId useMainThread:(BOOL)useMainThread{
    
    NFontSize * fontSize = [NSEntityDescription insertNewObjectForEntityForName:kTableFontSize
                                                         inManagedObjectContext:[self getManageObjectContextFromMainThread:useMainThread]];
    
    fontSize.fontSizeId = [NSNumber numberWithInt:FontSizeId];
    fontSize.fontSize = [NSNumber numberWithInt:fontSizeValue];
    
    fontSize.isRemoved = [NSNumber numberWithBool:NO];
    fontSize.created = [NSDate date];
    fontSize.modified = [NSDate date];
    
    NSError *error;
    if (![[self getManageObjectContextFromMainThread:useMainThread] save:&error]) {
        NSLog(@"addColor, couldn't save: %@", [error localizedDescription]);
    }
    
    return fontSize;
}
@end
