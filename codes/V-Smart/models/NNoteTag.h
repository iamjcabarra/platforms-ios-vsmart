//
//  NoteTag.h
//  V-Smart
//
//  Created by VhaL on 2/26/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Note, Tag;

@interface NoteTag : NSManagedObject

@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic, retain) NSNumber * syncId;
@property (nonatomic, retain) NSNumber * isRemoved;
@property (nonatomic, retain) Note *note;
@property (nonatomic, retain) Tag *tag;

@end
