//
//  SSCalendarWeekItem.h
//  V-Smart
//
//  Created by VhaL on 3/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SSCalendarWeekItem : NSObject
@property (nonatomic, strong) NSString *week;
@property (nonatomic, strong) NSMutableArray *weekEntries;
@end
