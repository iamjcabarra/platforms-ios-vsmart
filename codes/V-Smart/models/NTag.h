//
//  Tag.h
//  V-Smart
//
//  Created by VhaL on 2/26/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NoteTag;

@interface Tag : NSManagedObject

@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic, retain) NSNumber * syncId;
@property (nonatomic, retain) NSString * tagName;
@property (nonatomic, retain) NSNumber * isRemoved;
@property (nonatomic, retain) NSSet *noteTags;
@end

@interface Tag (CoreDataGeneratedAccessors)

-(void)addNoteTagsObject:(NoteTag *)value;
-(void)removeNoteTagsObject:(NoteTag *)value;
-(void)addNoteTags:(NSSet *)values;
-(void)removeNoteTags:(NSSet *)values;

@end
