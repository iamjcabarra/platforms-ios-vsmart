//
//  SSCalendarItem.h
//  V-Smart
//
//  Created by VhaL on 3/26/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SSCalendarItem : NSObject
@property (nonatomic, strong) NSDate *dateKey;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *detail;
@property (nonatomic, strong) NSString *duration;
@property (nonatomic, strong) NSString *timeCoverage;
@end
