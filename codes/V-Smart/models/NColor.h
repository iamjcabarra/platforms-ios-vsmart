//
//  Color.h
//  V-Smart
//
//  Created by VhaL on 3/1/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Color : NSManagedObject

@property (nonatomic, retain) NSNumber * colorId;
@property (nonatomic, retain) NSString * colorName;
@property (nonatomic, retain) NSNumber * redValue;
@property (nonatomic, retain) NSNumber * greenValue;
@property (nonatomic, retain) NSNumber * blueValue;
@property (nonatomic, retain) NSString * hexValue;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic, retain) NSNumber * isRemoved;

@end
