//
//  Note.m
//  V-Smart
//
//  Created by VhaL on 2/26/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NNote.h"
#import "NNoteBook.h"
#import "NNoteTag.h"


@implementation Note

@dynamic colorId;
@dynamic content;
@dynamic created;
@dynamic modified;
@dynamic syncId;
@dynamic title;
@dynamic isRemoved;
@dynamic isShared;
@dynamic folder;
@dynamic noteTags;
@dynamic fontTypeId;
@dynamic fontSizeId;
@dynamic isArchive;
@end
