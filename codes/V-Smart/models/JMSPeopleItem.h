//
//  JMSPeopleItem.h
//  V-Smart
//
//  Created by VhaL on 3/28/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"

@protocol JMSPeopleItem
@end

@interface JMSPeopleItem : JSONModel

/*
@property (nonatomic, assign) int sectionId;
@property (nonatomic, assign) int userId;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *type;
 */

@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger courseId;
@property (nonatomic, strong) NSString *courseName;
@property (nonatomic, strong) NSString *courseCode;
@property (nonatomic, strong) NSString *courseDescription;
@property (nonatomic, assign) NSInteger sectionId;
@property (nonatomic, strong) NSString *sectionName;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, assign) NSString *isLoggedIn;

@end

@interface JMSRecordPeopleItem : JSONModel
@property (nonatomic, strong) NSArray<JMSPeopleItem>* records;
@end