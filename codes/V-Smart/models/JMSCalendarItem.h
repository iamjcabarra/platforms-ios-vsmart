//
//  SSCalendarItem.h
//  V-Smart
//
//  Created by VhaL on 3/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JMSCalendarItem
@end

@interface JMSCalendarItem : JSONModel
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *typeId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *details;
@property (nonatomic, strong) NSString *backgroundColor;
@property (nonatomic, strong) NSString *eventStart;
@property (nonatomic, strong) NSString *eventEnd;
@property (nonatomic, strong) NSString *dateCreated;
@property (nonatomic, strong) NSString *dateModified;
@end

@interface JMSRecordCalendarItem : JSONModel
@property (nonatomic, strong) NSArray<JMSCalendarItem>* records;
@end