//
//  CloudBookExercise.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/22/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CloudBookExercise : NSManagedObject

@property (nonatomic, retain) NSString * chapterFile;
@property (nonatomic, retain) NSString * exerciseId;
@property (nonatomic, retain) NSString * exerciseTitle;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSNumber * lock;
@property (nonatomic, retain) NSNumber * itemCount;

@end
