//
//  VibeDataManager.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/1/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "VibeDataManager.h"

@interface VibeDataManager()

@property (strong, readwrite, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, readwrite, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, readwrite, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

static NSString *kPersistentStoreName = @"CloudData";

@implementation VibeDataManager

+ (id)sharedInstance
{
    //    NSLog(@"sharedManager...");
    static VibeDataManager *singletonInstance = nil;
    static dispatch_once_t singleToken;
    dispatch_once(&singleToken, ^{
        singletonInstance = [[self alloc] init];
    });
    return singletonInstance;
}

- (id)init
{
    if (self = [super init]) {
        //Initialize CoreData
        self.managedObjectContext = [self managedObjectContext];
    }
    
    return self;
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            VLog(@"Unresolved error %@, %@", error, [error userInfo]);
            // Remove on Production
            abort();
        }
    }
}

- (BOOL)coreDataSaveValues:(NSArray *)items forEntity:(NSString *)entity
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
    
    for (NSDictionary *data in items) {
        
        NSString *email = data[@"email"];
        NSManagedObject *mo = [self getEntity:[entityDesc name] attribute:@"email" parameter:email context:context];
        
        // new object
        if (mo == nil) {
            mo = [NSEntityDescription insertNewObjectForEntityForName:[entityDesc name] inManagedObjectContext:context];
        }
        
        //[mo setValue:data[@"accountId"] forKey:@"accountId"];
        [mo setValue:data[@"authKey"] forKey:@"authKey"];
        [mo setValue:email forKey:@"email"];
        
        [context refreshObject:mo mergeChanges:YES];
    }
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        VLog(@"Unresolved error %@, %@", error, [error userInfo]);
        return NO;//
    }
    
    return YES;
}

#pragma mark - CloudAccount Entity
- (BOOL) addCloudAccount: (NSDictionary *) account {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:kCloudAccountEntity inManagedObjectContext:context];
    NSString *email = account[kDBCloudAccountEmail];
    NSManagedObject *mo = [self getEntity:[entityDesc name] attribute:kDBCloudAccountEmail parameter:email context:context];
    
    // new object
    if (mo == nil) {
        mo = [NSEntityDescription insertNewObjectForEntityForName:[entityDesc name] inManagedObjectContext:context];
    }
    
    //[mo setValue:account[kDBCloudAccountId] forKey:kDBCloudAccountId];
    [mo setValue:account[kDBCloudAccountAuthKey] forKey:kDBCloudAccountAuthKey];
    [mo setValue:email forKey:kDBCloudAccountEmail];
    
    BOOL result = [self saveObject:mo forContext:context];
    return result;
}

- (void) removeCloudAccount: (NSString *) email {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email == %@", email];
    [self removeData:predicate forEntity:kCloudAccountEntity];
}

- (CloudAccount *) fetchCloudAccount: (NSString *) email {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email == %@", email];
    CloudAccount *account = (CloudAccount *)[self fetchSingle:kCloudAccountEntity withPredicate:predicate];
    return account;
}

- (NSArray *)fetchCloudAccounts {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCloudAccountEntity];
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (items != nil) {
        return items;
    }
    
    return nil;
}

#pragma mark - CloudBookMedia Entity
- (void) addCloudBookMedia: (NSArray *) contents forBook:(NSString *) uuid {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:kCloudBookMediaEntity inManagedObjectContext:context];
    NSManagedObject *managedObject = [self getEntity:[entityDesc name] attribute:kDBCloudUUID parameter:uuid context:context];
    
    if(managedObject) return;
    
    for (NSDictionary *item in contents) {
        
        NSString *chapterFile = [item objectForKey:kDBCloudHighlightChapterFile];
        NSArray *result = [item objectForKey:@"result"];
        
        for (NSDictionary *content in result) {
            NSString *mediaPath = [content objectForKey:kDBCloudBookMediaPath];
            NSString *mediaId = [content objectForKey:kDBCloudBookMediaId];
            NSString *mediaTitle = [content objectForKey:kDBCloudBookMediaTitle];
            NSString *mediaType = [content objectForKey:kDBCloudBookMediaType];
            
            NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:[entityDesc name] inManagedObjectContext:context];
            [mo setValue:mediaPath forKey:kDBCloudBookMediaPath];
            [mo setValue:mediaId forKey:kDBCloudBookMediaId];
            [mo setValue:mediaTitle forKey:kDBCloudBookMediaTitle];
            [mo setValue:mediaType forKey:kDBCloudBookMediaType];
            [mo setValue:chapterFile forKey:kDBCloudChapterFile];
            [mo setValue:uuid forKey:kDBCloudUUID];
            
            [self saveObject:mo forContext:context];
        }
    }
}

- (void) removeCloudBookMedia: (NSString *) uuid {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@", uuid];
    [self removeData:predicate forEntity:kCloudBookMediaEntity];
}

- (NSArray *)fetchCloudBookMedia:(NSString *) uuid {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCloudBookMediaEntity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@", uuid];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (items != nil) {
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:kDBCloudBookMediaTitle
                                                                     ascending:YES];
        NSArray *results = [items sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        return results;
    }
    
    return nil;
}

#pragma mark - CloudBookFigure Entity
- (void) addCloudBookFigure: (NSArray *) figures forBook:(NSString *) uuid {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:kCloudBookFigureEntity inManagedObjectContext:context];
    NSManagedObject *managedObject = [self getEntity:[entityDesc name] attribute:kDBCloudUUID parameter:uuid context:context];
    
    if(managedObject) return;
    
    for (NSDictionary *item in figures) {
        
        NSString *chapterFile = [item objectForKey:kDBCloudHighlightChapterFile];
        NSArray *result = [item objectForKey:@"result"];
        
        for (NSDictionary *content in result) {
            NSString *imagePath = [content objectForKey:@"filename"];
            NSString *imageId = [content objectForKey:@"imageId"];
            NSString *text = [content objectForKey:@"text"];

            NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:[entityDesc name] inManagedObjectContext:context];
            [mo setValue:imagePath forKey:kDBCloudBookFigureImagePath];
            [mo setValue:imageId forKey:kDBCloudBookFigureImageId];
            [mo setValue:text forKey:kDBCloudBookFigureCaption];
            [mo setValue:chapterFile forKey:kDBCloudChapterFile];
            [mo setValue:uuid forKey:kDBCloudUUID];
            
            [self saveObject:mo forContext:context];
        }
    }
}

- (void) removeCloudBookFigure: (NSString *) uuid {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@", uuid];
    [self removeData:predicate forEntity:kCloudBookFigureEntity];
}

- (NSArray *)fetchCloudBookFigure:(NSString *) uuid {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCloudBookFigureEntity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@", uuid];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (items != nil) {
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:kDBCloudBookFigureCaption
                                                                     ascending:YES];
        NSArray *results = [items sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        return results;
    }
    
    return nil;
}

#pragma mark - CloudBookEncyclopedia Entity
- (void) addCloudBookEncyclopedia: (NSArray *) encyclopedia forBook:(NSString *) uuid {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:kCloudBookEncyclopediaEntity inManagedObjectContext:context];
    NSManagedObject *managedObject = [self getEntity:[entityDesc name] attribute:kDBCloudUUID parameter:uuid context:context];
    
    if(managedObject) return;
    
    for (NSDictionary *item in encyclopedia) {
        
        NSString *chapterFile = [item objectForKey:kDBCloudHighlightChapterFile];
        NSArray *result = [item objectForKey:@"result"];
        
        for (NSDictionary *content in result) {
            
            NSString *filePath = [content objectForKey:@"filename"];
            NSString *text = [content objectForKey:kDBCloudBookEncyclopediaText];
            
            if (!IsEmpty(filePath)) {
                NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:[entityDesc name] inManagedObjectContext:context];
                [mo setValue:filePath forKey:kDBCloudBookEncyclopediaFilePath];
                [mo setValue:text forKey:kDBCloudBookEncyclopediaText];
                [mo setValue:chapterFile forKey:kDBCloudChapterFile];
                [mo setValue:uuid forKey:kDBCloudUUID];
                
                [self saveObject:mo forContext:context];
            }
        }
    }
}

- (void) removeCloudBookEncyclopedia: (NSString *) uuid {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@", uuid];
    [self removeData:predicate forEntity:kCloudBookEncyclopediaEntity];
}

- (NSArray *)fetchCloudBookEncyclopedia:(NSString *) uuid {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCloudBookEncyclopediaEntity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@", uuid];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (items != nil) {
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:kDBCloudBookEncyclopediaText
                                                                     ascending:YES];
        NSArray *results = [items sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        return results;
    }
    
    return nil;
}

#pragma mark - CloudBookExercise Entity
- (void) addCloudBookExercise: (NSDictionary *) exercise forBook:(NSString *) uuid {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:kCloudBookExerciseEntity inManagedObjectContext:context];
    NSManagedObject *mo = [self getEntity:[entityDesc name] attribute:kDBCloudUUID parameter:uuid context:context];
    
    if (mo) return;
    
    NSArray *exercises = [exercise objectForKey:@"exercises"];

    for (NSDictionary *item in exercises) {
        NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:[entityDesc name] inManagedObjectContext:context];
        
        NSString *href = [item objectForKey:@"href"];
        NSString *exerciseTitle = [item objectForKey:@"title"];
        
        NSString *passwordData = [NSString stringWithFormat:@"%@", [item objectForKey:@"password"] ];
        if ([passwordData isEqualToString:@"(null)"]) {
            passwordData = @"";
        }
        NSString *exercisePassword = [NSString stringWithFormat:@"%@", passwordData];

        
        int itemCount = [[item objectForKey:@"total_items"] intValue];
        
        NSArray *hrefArray = [href componentsSeparatedByString:@"#"];
        NSString *chapterFile = [hrefArray objectAtIndex:0];
        NSString *exerciseId = [hrefArray objectAtIndex:1];
        
        NSArray *exercise_component = [exerciseTitle componentsSeparatedByString:@" "];
        NSNumber *order = [NSNumber numberWithInteger:[exercise_component[1] integerValue]];
        [mo setValue:order forKey:@"order"];
        
        [mo setValue:exerciseId forKey:kDBCloudBookExerciseId];
        [mo setValue:exerciseTitle forKey:kDBCloudBookExerciseTitle];
        
        //ASIANATE IMPLEMENTATION
        [mo setValue:exercisePassword forKey:kDBCloudBookExercisePassword];
        NSNumber *lockFlag = [NSNumber numberWithBool:YES];//HARD CODING
        [mo setValue:lockFlag forKey:kDBCloudBookExerciseLock];
        //ASIANATE IMPLEMENTATION
        
        [mo setValue:[NSNumber numberWithInt:itemCount] forKey:kDBCloudBookExerciseItemCount];
        [mo setValue:chapterFile forKey:kDBCloudChapterFile];
        [mo setValue:uuid forKey:kDBCloudUUID];
    }
    
    [self saveObject:mo forContext:context];
}

- (void) removeCloudBookExercise: (NSString *) uuid {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@", uuid];
    [self removeData:predicate forEntity:kCloudBookExerciseEntity];
}

- (NSArray *)fetchCloudBookExercises:(NSString *) uuid {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@", uuid];
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCloudBookExerciseEntity];
    [fetchRequest setPredicate:predicate];
    
    /*
    chapterFile
    exerciseId
    exerciseTitle
     */
    
    NSSortDescriptor *exerciseTitle = [NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES];
    [fetchRequest setSortDescriptors:@[exerciseTitle]];
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (items != nil) {
        return items;
    }
    
    return nil;
}

#pragma mark - CloudBook Entity

- (BOOL) addCloudBook: (NSDictionary *) book {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:kCloudBookEntity inManagedObjectContext:context];
    NSString *uuid = book[kDBCloudUUID];
    NSManagedObject *mo = [self getEntity:[entityDesc name] attribute:kDBCloudUUID parameter:uuid context:context];
    
    // new object
    if (mo == nil) {
        mo = [NSEntityDescription insertNewObjectForEntityForName:[entityDesc name] inManagedObjectContext:context];
    }
    
    [mo setValue:book[kDBCloudBookTitle] forKey:kDBCloudBookTitle];
    [mo setValue:book[kDBCloudBookPublisher] forKey:kDBCloudBookPublisher];
    [mo setValue:book[kDBCloudBookAuthors] forKey:kDBCloudBookAuthors];
    [mo setValue:uuid forKey:kDBCloudUUID];
    
    BOOL result = [self saveObject:mo forContext:context];
    return result;
}

- (void) removeCloudBook: (NSString *) uuid {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@", uuid];
    [self removeData:predicate forEntity:kCloudBookEntity];
}

- (CloudBook *) fetchCloudBook: (NSString *) uuid {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@", uuid];
    CloudBook *book = (CloudBook *)[self fetchSingle:kCloudBookEntity withPredicate:predicate];
    return book;
}

- (NSArray *)fetchCloudBooks {
    NSManagedObjectContext *context = [self managedObjectContext];
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"accountId == %@", accountId];
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCloudBookEntity];
    //[fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (items != nil) {
        return items;
    }
    
    return nil;
}

#pragma mark - CloudHighlight Entity
- (BOOL) updateCloudHighlight: (CloudHighlight *) cloud {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:kCloudHighlightEntity inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@ AND email == %@ AND chapterFile == %@ AND highlightId == %d", cloud.uuid, cloud.email, cloud.chapterFile, [cloud.highlightId intValue] ];
    NSManagedObject *mo = (NSManagedObject *)[self fetchSingle:kCloudHighlightEntity withPredicate:predicate];
    
    // new object
    if (mo == nil) {
        mo = [NSEntityDescription insertNewObjectForEntityForName:[entityDesc name] inManagedObjectContext:context];
    }
    
    [mo setValue:cloud.email forKey:kDBCloudAccountEmail];
    [mo setValue:cloud.chapterTitle forKey:kDBCloudHighlightChapterTitle];
    [mo setValue:cloud.chapterFile forKey:kDBCloudHighlightChapterFile];
    [mo setValue:cloud.highlightedData forKey:kDBCloudHighlightData];
    [mo setValue:cloud.highlightedText forKey:kDBCloudHighlightText];
    [mo setValue:cloud.highlightId forKey:kDBCloudHighlightId];
    [mo setValue:cloud.hasNote forKey:kDBCloudHighlightHasNote];
    [mo setValue:cloud.note forKey:kDBCloudHighlightNote];
    [mo setValue:cloud.cssClass forKey:kDBCloudHighlightCss];
    [mo setValue:cloud.startRange forKey:kDBCloudHighlightStartRange];
    [mo setValue:cloud.endRange forKey:kDBCloudHighlightEndRange];
    [mo setValue:cloud.uuid forKey:kDBCloudUUID];
    
    BOOL result = [self saveObject:mo forContext:context];
    return result;
}

- (BOOL) addCloudHighlight: (NSDictionary *) highlight {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:kCloudHighlightEntity inManagedObjectContext:context];
    NSString *uuid = highlight[kDBCloudUUID];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@ AND email == %@ AND chapterFile == %@ AND highlightId == %@", uuid, highlight[kDBCloudAccountEmail], highlight[kDBCloudHighlightChapterFile], highlight[kDBCloudHighlightId]];
    NSManagedObject *mo = (NSManagedObject *)[self fetchSingle:kCloudHighlightEntity withPredicate:predicate];
    
    // new object
    if (mo == nil) {
        mo = [NSEntityDescription insertNewObjectForEntityForName:[entityDesc name] inManagedObjectContext:context];
    }
    
    [mo setValue:highlight[kDBCloudAccountEmail] forKey:kDBCloudAccountEmail];
    [mo setValue:highlight[kDBCloudHighlightChapterTitle] forKey:kDBCloudHighlightChapterTitle];
    [mo setValue:highlight[kDBCloudHighlightChapterFile] forKey:kDBCloudHighlightChapterFile];
    [mo setValue:highlight[kDBCloudHighlightData] forKey:kDBCloudHighlightData];
    [mo setValue:highlight[kDBCloudHighlightText] forKey:kDBCloudHighlightText];
    [mo setValue:highlight[kDBCloudHighlightId] forKey:kDBCloudHighlightId];
    [mo setValue:highlight[kDBCloudHighlightHasNote] forKey:kDBCloudHighlightHasNote];
    [mo setValue:highlight[kDBCloudHighlightNote] forKey:kDBCloudHighlightNote];
    [mo setValue:highlight[kDBCloudHighlightCss] forKey:kDBCloudHighlightCss];
    [mo setValue:highlight[kDBCloudHighlightStartRange] forKey:kDBCloudHighlightStartRange];
    [mo setValue:highlight[kDBCloudHighlightEndRange] forKey:kDBCloudHighlightEndRange];
    [mo setValue:uuid forKey:kDBCloudUUID];
    
    BOOL result = [self saveObject:mo forContext:context];
    return result;
}

- (void) removeCloudHighlightById: (NSString *) email withUUID: (NSString *) uuid forChapterFile:(NSString *) chapterFile andHighlightId:(int)highlightId {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@ AND email == %@ AND chapterFile == %@ AND highlightId == %d", uuid, email, chapterFile, highlightId];
    [self removeData:predicate forEntity:kCloudHighlightEntity];
}

- (CloudHighlight *) fetchCloudHighlightById: (NSString *) email withUUID: (NSString *) uuid forChapterFile:(NSString *) chapterFile andHighlightId:(int)highlightId {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@ AND email == %@ AND chapterFile == %@ AND highlightId == %d", uuid, email, chapterFile, highlightId];
    
    CloudHighlight *highlight = (CloudHighlight *)[self fetchSingle:kCloudHighlightEntity withPredicate:predicate];
    return highlight;
}

- (NSArray *)fetchCloudHighlightsForChapter: (NSString *) email withUUID: (NSString *) uuid forChapterFile:(NSString *) chapterFile {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCloudHighlightEntity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@ AND email == %@ AND chapterFile == %@", uuid, email, chapterFile];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (items != nil) {
        return items;
    }
    
    return nil;
}

- (NSArray *)fetchCloudHighlightsForBook: (NSString *) email withUUID: (NSString *) uuid {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCloudHighlightEntity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@ AND email == %@", uuid, email];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    //
    
    if (items != nil) {
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:kDBCloudDateAdded
                                                                     ascending:NO];
        NSArray *results = [items sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        return results;
    }
    
    return nil;
}

#pragma mark - Cloud Answer
- (void) addCloudAnswer: (NSString *) email forBook:(NSString *) uuid inChapterFile:(NSString *) chapterFile forBookAnswer:(NSDictionary *) answer {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:kCloudAnswerEntity inManagedObjectContext:context];
    NSString *exerId = answer[kDBCloudAnswerExerId];
    
    // remove all objects
    [self removeCloudAnswer:email forBook:uuid forExerciseId:exerId];
    
    NSArray *result = [answer objectForKey:@"result"];
    
    for (NSDictionary *item in result) {
        // new object
        NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:[entityDesc name] inManagedObjectContext:context];
        
        [mo setValue:item[kDBCloudAnswerExerType] forKey:kDBCloudAnswerExerType];
        [mo setValue:item[kDBCloudAnswerActualAnswer] forKey:kDBCloudAnswerActualAnswer];
        [mo setValue:item[kDBCloudAnswerAnswerStatus] forKey:kDBCloudAnswerAnswerStatus];
        [mo setValue:item[kDBCloudAnswerCorrectAnswer] forKey:kDBCloudAnswerCorrectAnswer];
        [mo setValue:item[kDBCloudAnswerItemNo] forKey:kDBCloudAnswerItemNo];
        [mo setValue:chapterFile forKey:kDBCloudHighlightChapterFile];
        [mo setValue:exerId forKey:kDBCloudAnswerExerId];
        [mo setValue:uuid forKey:kDBCloudUUID];
        [mo setValue:email forKey:kDBCloudAccountEmail];
        
        [self saveObject:mo forContext:context];
    }
}

- (void) removeCloudAnswer: (NSString *) email forBook:(NSString *) uuid forExerciseId:(NSString *) exerId {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email == %@ AND exer_id == %@ AND uuid == %@", email, exerId, uuid];
    [self removeData:predicate forEntity:kCloudAnswerEntity];
}

- (NSArray *)fetchCloudAnswer: (NSString *) email forBook:(NSString *) uuid inChapterFile:(NSString *) chapterFile {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCloudAnswerEntity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email == %@ AND uuid == %@ AND chapterFile == %@",email, uuid, chapterFile];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (items != nil) {
        return items;
    }
    
    return nil;
}

- (NSArray *)fetchCloudAnswer: (NSString *) email forBook:(NSString *) uuid inChapterFile:(NSString *) chapterFile forExerciseId: (NSString *) exerciseId {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCloudAnswerEntity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email == %@ AND uuid == %@ AND chapterFile == %@ AND exer_id == %@",email, uuid, chapterFile, exerciseId];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (items != nil) {
        return items;
    }
    
    return nil;
}

#pragma mark - Core Data Helpers
- (BOOL) saveObject:(NSManagedObject *)managedObject forContext:(NSManagedObjectContext *) context {
    [context refreshObject:managedObject mergeChanges:YES];
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        VLog(@"Unresolved error %@, %@", error, [error userInfo]);
        return NO;//
    }
    
    return YES;
}

- (id) fetchSingle:(NSString *) entity withPredicate:(NSPredicate *) predicate {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (items != nil) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSManagedObject *)getEntity:(NSString *)entity
                     attribute:(NSString *)attribute
                     parameter:(NSString *)parameter
                       context:(NSManagedObjectContext *)context
{
    
    NSString *entityObjectType = entity;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entityObjectType];
    
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:attribute];
    NSExpression *right = [NSExpression expressionForConstantValue:parameter];
    
    // create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:NSCaseInsensitivePredicateOption];
    // set predicate
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        VLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (void)removeContentsForEntity:(NSString *)entity withKey:(NSString *)key
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    [fetchRequest setIncludesPropertyValues:NO];
    
    NSManagedObjectContext *context = _managedObjectContext;
    NSArray *list = [context executeFetchRequest:fetchRequest error:nil];
    if ([list count] > 0) {
        
        [list enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [context deleteObject:obj];
        }];
        
        NSError *error = nil;
        if (![context save:&error]) {
            VLog(@"Error deleting\nerror:%@",[error localizedDescription]);
        }
    }
}

- (void)removeData:(NSPredicate *) predicate forEntity:(NSString *)entity {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    [fetchRequest setIncludesPropertyValues:NO];
    [fetchRequest setPredicate:predicate];
    
    NSManagedObjectContext *context = _managedObjectContext;
    NSArray *list = [context executeFetchRequest:fetchRequest error:nil];
    if ([list count] > 0) {
        
        [list enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [context deleteObject:obj];
        }];
        
        NSError *error = nil;
        if (![context save:&error]) {
            VLog(@"Error deleting\nerror:%@",[error localizedDescription]);
        }
    }
}

#pragma mark - Core Data
- (NSString *) getApplicationName {
    NSDictionary *applicationInfo = [[NSBundle mainBundle] infoDictionary];
    NSString *applicationName = [applicationInfo objectForKey:@"CFBundleDisplayName"];
    
    VLog(@"AppName: %@", applicationName);
    return applicationName;
}

- (NSString *) getStoreFile {
    //NSString *applicationName = [self getApplicationName];
    NSString *storeFile = [NSString stringWithFormat:@"%@.sqlite", kPersistentStoreName];
    
    VLog(@"storeFile: %@", storeFile);
    return storeFile;
}

- (BOOL)isPersistentStoreCompatible
{
    BOOL status = YES;

    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[self getStoreFile]];
    
    VLog(@"URL: %@", storeURL);
    
    NSPersistentStoreCoordinator *psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:nil])
    {
        status = NO;
    }
    
    return status;
}

- (void)rebuildCoreDataModel
{
    
    if (![self isPersistentStoreCompatible]) {
        
        //reset managed object context
        [_managedObjectContext reset];
        
        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[self getStoreFile]];
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        
        // clear manage object model
        _managedObjectModel = nil;
        
        // clear persistent store coordinator
        _persistentStoreCoordinator = nil;
        
        // clear manage object context
        _managedObjectContext = nil;
        
        VLog(@"Core Data Initialized");
    }
    
}

- (NSManagedObjectContext *)managedObjectContext
{
    //    NSLog(@"managedObjectContext...");
    
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:kPersistentStoreName withExtension:@"momd"];
    //VLog(@"URL: %@", modelURL);
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)store
{
    return [self persistentStoreCoordinator];
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[self getStoreFile]];
    VLog(@"URL: %@", storeURL);
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        VLog(@"Unresolved error %@, %@", error, [error userInfo]);
        
        [self rebuildCoreDataModel];
        
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Dummy Calls
- (BOOL)buildDummyDataForAccount
{
    //TEMPORARY REMOVE THIS LATER
    NSDictionary *r1 = @{@"accountId":[NSNumber numberWithInt:1],                         @"authKey":@"MDAwMDAwMjRlYXJsam9uaGlkYWxnb0BnbWFpbC5jb20wMDAwMDAzMmJjZmNiZDQxYmJlOWUzOTNkMmM4YmY5MzM4OGQwZWYxNDU0OTMyNjIyMTAwMA==",
                         @"email":@"earljon@outlook.com"};
    
    NSDictionary *r2 = @{@"accountId":[NSNumber numberWithInt:2],                         @"authKey":@"MDAwMDAwMjRlYXJsam9uaGlkYWxnb0BnbWFpbC5jb20wMDAwMDAzMmJjZmNiZDQxYmJlOWUzOTNkMmM4YmY5MzM4OGQwZWYxNDU0OTMyNjIyMTAwMA==",
                         @"email":@"earljon@gmail.com"};
    
    NSArray *results = @[r1, r2];
    
    return [self coreDataSaveValues:results forEntity:kCloudAccountEntity];
}
@end
