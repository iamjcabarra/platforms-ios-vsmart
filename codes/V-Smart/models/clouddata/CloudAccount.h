//
//  CloudAccount.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/10/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CloudAccount : NSManagedObject

@property (nonatomic, retain) NSString * authKey;
@property (nonatomic, retain) NSDate * dateCreated;
@property (nonatomic, retain) NSString * email;

@end
