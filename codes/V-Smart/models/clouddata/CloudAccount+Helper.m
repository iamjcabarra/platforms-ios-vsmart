//
//  CloudAccount+Helper.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/1/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CloudAccount+Helper.h"

@implementation CloudAccount (Helper)

- (void) awakeFromInsert
{
    [super awakeFromInsert];
    [self setValue:[NSDate date] forKey:@"dateCreated"];
}

@end
