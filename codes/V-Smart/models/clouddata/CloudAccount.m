//
//  CloudAccount.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/10/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CloudAccount.h"


@implementation CloudAccount

@dynamic authKey;
@dynamic dateCreated;
@dynamic email;

@end
