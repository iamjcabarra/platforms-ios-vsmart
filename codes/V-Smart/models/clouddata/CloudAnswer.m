//
//  CloudAnswer.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/20/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CloudAnswer.h"


@implementation CloudAnswer

@dynamic exer_id;
@dynamic exer_type;
@dynamic actual_answer;
@dynamic answer_status;
@dynamic correct_answer;
@dynamic item_no;
@dynamic uuid;
@dynamic chapterFile;
@dynamic email;

@end
