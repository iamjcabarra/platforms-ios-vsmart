//
//  CloudBookExercise.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/22/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CloudBookExercise.h"


@implementation CloudBookExercise

@dynamic chapterFile;
@dynamic exerciseId;
@dynamic exerciseTitle;
@dynamic uuid;
@dynamic password;
@dynamic lock;
@dynamic itemCount;

@end
