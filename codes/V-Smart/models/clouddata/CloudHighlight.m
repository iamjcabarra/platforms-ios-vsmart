//
//  CloudHighlight.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/10/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CloudHighlight.h"


@implementation CloudHighlight

@dynamic email;
@dynamic chapterFile;
@dynamic chapterTitle;
@dynamic cssClass;
@dynamic dateAdded;
@dynamic dateModified;
@dynamic endRange;
@dynamic hasNote;
@dynamic highlightedData;
@dynamic highlightedText;
@dynamic highlightId;
@dynamic note;
@dynamic startRange;
@dynamic uuid;

@end
