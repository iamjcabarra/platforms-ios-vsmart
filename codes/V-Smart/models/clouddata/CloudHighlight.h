//
//  CloudHighlight.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/10/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CloudHighlight : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * chapterFile;
@property (nonatomic, retain) NSString * chapterTitle;
@property (nonatomic, retain) NSString * cssClass;
@property (nonatomic, retain) NSDate * dateAdded;
@property (nonatomic, retain) NSDate * dateModified;
@property (nonatomic, retain) NSNumber * endRange;
@property (nonatomic, retain) NSNumber * hasNote;
@property (nonatomic, retain) NSString * highlightedData;
@property (nonatomic, retain) NSString * highlightedText;
@property (nonatomic, retain) NSNumber * highlightId;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSNumber * startRange;
@property (nonatomic, retain) NSString * uuid;

@end
