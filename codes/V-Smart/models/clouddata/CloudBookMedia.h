//
//  CloudBookMedia.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/25/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CloudBookMedia : NSManagedObject

@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSString * chapterFile;
@property (nonatomic, retain) NSString * mediaType;
@property (nonatomic, retain) NSString * mediaId;
@property (nonatomic, retain) NSString * mediaPath;
@property (nonatomic, retain) NSString * mediaTitle;

@end
