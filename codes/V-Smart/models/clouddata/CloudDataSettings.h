//
//  CloudDataSettings.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/1/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//


static NSString *kCloudAccountEntity                = @"CloudAccount";
static NSString *kCloudBookEntity                   = @"CloudBook";
static NSString *kCloudHighlightEntity              = @"CloudHighlight";
static NSString *kCloudAnswerEntity                 = @"CloudAnswer";
static NSString *kCloudBookExerciseEntity           = @"CloudBookExercise";
static NSString *kCloudBookEncyclopediaEntity       = @"CloudBookEncyclopedia";
static NSString *kCloudBookFigureEntity             = @"CloudBookFigure";
static NSString *kCloudBookMediaEntity              = @"CloudBookMedia";

#define kDBCloudAccountId                   @"accountId"
#define kDBCloudAccountEmail                @"email"
#define kDBCloudUUID                        @"uuid"
#define kDBCloudDateCreated                 @"dateCreated"
#define kDBCloudDateAdded                   @"dateAdded"
#define kDBCloudChapterFile                 @"chapterFile"

#define kDBCloudAccountAuthKey              @"authKey"
#define kDBCloudAccountEmail                @"email"

#define kDBCloudBookTitle                   @"title"
#define kDBCloudBookPublisher               @"publisher"
#define kDBCloudBookAuthors                 @"authors"

#define kDBCloudHighlightDateModified       @"dateModified"
#define kDBCloudHighlightChapterFile        @"chapterFile"
#define kDBCloudHighlightChapterTitle       @"chapterTitle"
#define kDBCloudHighlightId                 @"highlightId"
#define kDBCloudHighlightText               @"highlightedText"
#define kDBCloudHighlightData               @"highlightedData"
#define kDBCloudHighlightHasNote            @"hasNote"
#define kDBCloudHighlightNote               @"note"
#define kDBCloudHighlightCss                @"cssClass"
#define kDBCloudHighlightStartRange         @"startRange"
#define kDBCloudHighlightEndRange           @"endRange"

#define kDBCloudAnswerExerId                @"exer_id"
#define kDBCloudAnswerExerType              @"exer_type"
#define kDBCloudAnswerItemNo                @"item_no"
#define kDBCloudAnswerCorrectAnswer         @"correct_answer"
#define kDBCloudAnswerAnswerStatus          @"answer_status"
#define kDBCloudAnswerActualAnswer          @"actual_answer"

#define kDBCloudBookExerciseTitle           @"exerciseTitle"
#define kDBCloudBookExercisePassword        @"password"
#define kDBCloudBookExerciseLock            @"lock"
#define kDBCloudBookExerciseId              @"exerciseId"
#define kDBCloudBookExerciseItemCount       @"itemCount"

#define kDBCloudBookEncyclopediaFilePath    @"filePath"
#define kDBCloudBookEncyclopediaText        @"text"

#define kDBCloudBookFigureImagePath         @"imagePath"
#define kDBCloudBookFigureCaption           @"caption"
#define kDBCloudBookFigureImageId           @"imageId"

#define kDBCloudBookMediaPath               @"mediaPath"
#define kDBCloudBookMediaTitle              @"mediaTitle"
#define kDBCloudBookMediaType               @"mediaType"
#define kDBCloudBookMediaId                 @"mediaId"

#define kCloudHighlightPrefix               @"vibeHighlight"
#define kCloudHighlightTextPrefix           @"type:textContent"
