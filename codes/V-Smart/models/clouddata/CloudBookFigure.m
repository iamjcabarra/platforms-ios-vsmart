//
//  CloudBookFigure.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/22/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CloudBookFigure.h"


@implementation CloudBookFigure

@dynamic uuid;
@dynamic chapterFile;
@dynamic imagePath;
@dynamic imageId;
@dynamic caption;

@end
