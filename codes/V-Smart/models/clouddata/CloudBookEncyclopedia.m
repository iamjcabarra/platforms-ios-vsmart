//
//  CloudBookEncyclopedia.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/22/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CloudBookEncyclopedia.h"


@implementation CloudBookEncyclopedia

@dynamic uuid;
@dynamic chapterFile;
@dynamic filePath;
@dynamic text;

@end
