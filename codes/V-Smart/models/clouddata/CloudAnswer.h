//
//  CloudAnswer.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/20/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CloudAnswer : NSManagedObject

@property (nonatomic, retain) NSString * exer_id;
@property (nonatomic, retain) NSNumber * exer_type;
@property (nonatomic, retain) NSString * actual_answer;
@property (nonatomic, retain) NSString * answer_status;
@property (nonatomic, retain) NSString * correct_answer;
@property (nonatomic, retain) NSNumber * item_no;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSString * chapterFile;
@property (nonatomic, retain) NSString * email;

@end
