//
//  CloudHighlight+Helper.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/1/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CloudHighlight+Helper.h"

@implementation CloudHighlight (Helper)

- (void) awakeFromInsert
{
    [super awakeFromInsert];
    [self setValue:[NSDate date] forKey:@"dateAdded"];
    [self setValue:[NSDate date] forKey:@"dateModified"];
}
@end
