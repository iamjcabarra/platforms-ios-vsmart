//
//  CloudBook.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/10/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CloudBook : NSManagedObject

@property (nonatomic, retain) NSString * authors;
@property (nonatomic, retain) NSDate * dateCreated;
@property (nonatomic, retain) NSString * publisher;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * uuid;

@end
