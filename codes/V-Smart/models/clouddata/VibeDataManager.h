//
//  VibeDataManager.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/1/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CloudAccount.h"
#import "CloudBook.h"
#import "CloudHighlight.h"

@interface VibeDataManager : NSObject

+ (id)sharedInstance;

- (void)saveContext;
- (NSPersistentStoreCoordinator *)store;
- (NSManagedObjectContext *)managedObjectContext;

// Cloud Account
- (BOOL) addCloudAccount: (NSDictionary *) account;
- (void) removeCloudAccount: (NSString *) email;
- (CloudAccount *) fetchCloudAccount: (NSString *) email;
- (NSArray *)fetchCloudAccounts;

// Cloud Book Exercise
- (void) addCloudBookExercise: (NSDictionary *) exercise forBook:(NSString *) uuid;
- (void) removeCloudBookExercise: (NSString *) uuid;
- (NSArray *)fetchCloudBookExercises:(NSString *) uuid;

// Cloud Book Media
- (void) addCloudBookMedia: (NSArray *) contents forBook:(NSString *) uuid;
- (void) removeCloudBookMedia: (NSString *) uuid;
- (NSArray *)fetchCloudBookMedia:(NSString *) uuid;

// Cloud Book Figure
- (void) addCloudBookFigure: (NSArray *) figures forBook:(NSString *) uuid;
- (void) removeCloudBookFigure: (NSString *) uuid;
- (NSArray *)fetchCloudBookFigure:(NSString *) uuid;

// Cloud Book Encyclopedia
- (void) addCloudBookEncyclopedia: (NSArray *) encyclopedia forBook:(NSString *) uuid;
- (void) removeCloudBookEncyclopedia: (NSString *) uuid;
- (NSArray *)fetchCloudBookEncyclopedia:(NSString *) uuid;

// Cloud Book
- (BOOL) addCloudBook: (NSDictionary *) book;
- (void) removeCloudBook: (NSString *) uuid;
- (CloudBook *) fetchCloudBook: (NSString *) uuid;
- (NSArray *)fetchCloudBooks;

// Cloud Highlight
- (BOOL) updateCloudHighlight: (CloudHighlight *) cloud;
- (BOOL) addCloudHighlight: (NSDictionary *) highlight;
- (void) removeCloudHighlightById: (NSString *) email withUUID: (NSString *) uuid forChapterFile:(NSString *) chapterFile andHighlightId:(int)highlightId;
- (CloudHighlight *) fetchCloudHighlightById: (NSString *) email withUUID: (NSString *) uuid forChapterFile:(NSString *) chapterFile andHighlightId:(int)highlightId;
- (NSArray *)fetchCloudHighlightsForChapter: (NSString *) email withUUID: (NSString *) uuid forChapterFile:(NSString *) chapterFile;
- (NSArray *)fetchCloudHighlightsForBook: (NSString *) email withUUID: (NSString *) uuid;

// Cloud Answer
- (void) addCloudAnswer: (NSString *) email forBook:(NSString *) uuid inChapterFile:(NSString *) chapterFile forBookAnswer:(NSDictionary *) answer ;
- (void) removeCloudAnswer: (NSString *) email forBook:(NSString *) uuid forExerciseId:(NSString *) exerId;
- (NSArray *)fetchCloudAnswer: (NSString *) email forBook:(NSString *) uuid inChapterFile:(NSString *) chapterFile;
- (NSArray *)fetchCloudAnswer: (NSString *) email forBook:(NSString *) uuid inChapterFile:(NSString *) chapterFile forExerciseId: (NSString *) exerciseId;
// Composite Methods

// Helpers
- (id) fetchSingle:(NSString *) entity withPredicate:(NSPredicate *) predicate;

// Operations
- (void)removeData:(NSPredicate *) predicate forEntity:(NSString *)entity;

- (BOOL)buildDummyDataForAccount;
//- (NSArray *)fetchCloudAccounts;
@end
