//
//  CloudBook.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/10/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CloudBook.h"


@implementation CloudBook

@dynamic authors;
@dynamic dateCreated;
@dynamic publisher;
@dynamic title;
@dynamic uuid;

@end
