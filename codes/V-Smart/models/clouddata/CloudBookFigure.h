//
//  CloudBookFigure.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/22/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CloudBookFigure : NSManagedObject

@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSString * chapterFile;
@property (nonatomic, retain) NSString * imagePath;
@property (nonatomic, retain) NSString * imageId;
@property (nonatomic, retain) NSString * caption;

@end
