//
//  CloudBookMedia.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/25/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "CloudBookMedia.h"


@implementation CloudBookMedia

@dynamic uuid;
@dynamic chapterFile;
@dynamic mediaType;
@dynamic mediaId;
@dynamic mediaPath;
@dynamic mediaTitle;

@end
