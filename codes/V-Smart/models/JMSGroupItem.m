//
//  JMSGroupItem.m
//  V-Smart
//
//  Created by VhaL on 3/25/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JMSGroupItem.h"

@implementation JMSGroupItem
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end

@implementation JMSRecordGroupItem
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end
