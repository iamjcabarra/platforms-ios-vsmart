//
//  JMSEmoItem.m
//  V-Smart
//
//  Created by VhaL on 3/24/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JMSEmoItem.h"

@implementation JMSEmoItem
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end

@implementation JMSRecordEmoItem
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end