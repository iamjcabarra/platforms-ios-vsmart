//
//  GNConstants.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 16/01/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

struct GNConstants {
    
    struct Entity {
        
        static let NOTIFICATION = "GNNotification"
    }
    
    struct SegueName {
        
    }
    
    struct StoryBoard {
        
    }
    
    struct Notification {
        
    }
    
    struct UserDefined {
        static let GNUNSEENCOUNT = "GN_USER_DEFINED_UNSEEN_COUNT"
    }
}
