//
//  JMNTag.h
//  V-Smart
//
//  Created by VhaL on 3/17/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"

@protocol JMNTag
@end

@interface JMNTag : JSONModel
@property (nonatomic, assign) int id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) int userId;
@property (nonatomic, assign) int isDeleted;
@property (nonatomic, strong) NSString *dateCreated;
@property (nonatomic, strong) NSString *dateModified;
@end

@interface JMNRecordTag : JSONModel
@property (nonatomic, strong) NSArray<JMNTag>* user_tags;
@end