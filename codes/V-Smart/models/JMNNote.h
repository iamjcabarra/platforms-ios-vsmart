//
//  JMNNote.h
//  V-Smart
//
//  Created by VhaL on 3/17/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"

@protocol JMNNote
@end

@interface JMNNote : JSONModel
@property (nonatomic, assign) int id;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, assign) int userId;
@property (nonatomic, assign) int notebookId;
@property (nonatomic, assign) int bgColor;
@property (nonatomic, assign) int isDeleted;

@property (nonatomic, assign) int fontTypeId;
@property (nonatomic, assign) int fontSizeId;
@property (nonatomic, assign) int isArchive;

@property (nonatomic, strong) NSString *dateCreated;
@property (nonatomic, strong) NSString *dateModified;
//@property (nonatomic, strong) NSString *dbNoteBookID;

@end

@interface JMNRecordNote : JSONModel
@property (nonatomic, strong) NSArray<JMNNote>* user_notes;
@end
