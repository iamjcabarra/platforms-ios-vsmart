//
//  ErrorModel.h
//  V-Smart
//
//  Created by Carmelito Bayarcal on 16/09/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface ErrorModel : JSONModel
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) int status;
@end
