//
//  GNListTableViewCell.swift
//  V-Smart
//
//  Created by Carmelito Bayarcal on 17/01/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class GNListTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var notifMessageLabel: UILabel!
    @IBOutlet weak var notifDateTimeLabel: UILabel!
    
    @IBOutlet weak var notifThumbnail: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        self.notifMessageLabel.text = ""
        self.notifDateTimeLabel.text = ""
        self.avatarImageView.image = UIImage(named: "img_book-placeholder.png")!
        self.backgroundColor = UIColor.lightGray
    }

}
