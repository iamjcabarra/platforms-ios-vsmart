//
//  JMSPeopleItem.m
//  V-Smart
//
//  Created by VhaL on 3/28/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JMSPeopleItem.h"

@implementation JMSPeopleItem
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end


@implementation JMSRecordPeopleItem
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end