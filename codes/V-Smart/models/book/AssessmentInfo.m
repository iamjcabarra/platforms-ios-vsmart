//
//  AssessmentInfo.m
//  V-Smart
//
//  Created by VhaL on 4/4/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "AssessmentInfo.h"

@implementation AssessmentInfo
@synthesize bookId, section, exerciseCode, exerciseId;
@end
