//
//  BookCategory.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/31/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"

@interface BookCategory : JSONModel

//@property (nonatomic, assign) int categoryId;
@property (nonatomic, strong) NSString<Optional> *categoryName;
@property (nonatomic, strong) NSString<Optional> *name;
@end
