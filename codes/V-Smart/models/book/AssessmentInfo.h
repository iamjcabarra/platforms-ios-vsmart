//
//  AssessmentInfo.h
//  V-Smart
//
//  Created by VhaL on 4/4/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AssessmentInfo : NSObject
@property (nonatomic, strong) NSString *bookId;
@property (nonatomic, strong) NSString *section;
@property (nonatomic, strong) NSString *exerciseId;
@property (nonatomic, strong) NSString *exerciseCode;
@end
