//
//  BookListResponse.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/31/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"
#import "Book.h"
//#import "Error.h"
#import "ErrorModel.h"

@protocol Book @end

@interface BookListResponse : JSONModel
@property (nonatomic, assign) int count;
@property (nonatomic, strong) NSArray<Book>* result;
@property (nonatomic, assign) int success;
@end
