//
//  Chapter.m
//  AePubReader
//


#import "Chapter.h"
#import "NSString+HTML.h"
#import "UIWebView+Private.h"

@implementation Chapter

@synthesize chapterIndex, title, pageCount, spinePath, text, windowSize, fontPercentSize;
@synthesize webView, ncxElement, pagesInChapter, pageInBook;
//@synthesize vibeDRM;
@synthesize bookEncyclopedia, bookFigure, bookMedia;

- (id) initWithPath:(NSString*)theSpinePath title:(NSString*)theTitle chapterIndex:(int) theIndex{
    if((self=[super init])){
        spinePath = [NSString stringWithFormat:@"%@", theSpinePath];
        title = [NSString stringWithFormat:@"%@", theTitle];
        chapterIndex = theIndex;
        
        pageCount = 0;
    }
    
    return self;
}

- (void) loadChapterWithWindowSize:(CGRect)theWindowSize fontPercentSize:(int) theFontPercentSize withSecurity:(BOOL) fileSecured drm:(VibeDRM *)object {
    //VLog(@"Chapters: loadChapterWithWindowSize");
    fontPercentSize = theFontPercentSize;
    windowSize = theWindowSize;
    isChapterSecured = fileSecured;
    
    if (isChapterSecured) {
        self.vibeDRM = object;
    }
    
    webView = [[UIWebView alloc] initWithFrame:windowSize];
    [webView setDelegate:self];
        NSURLRequest* urlRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:spinePath]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [webView loadRequest:urlRequest];
    });
}

- (void) _extractVibeData: (UIWebView*) theView {
    NSString *jsonEncyclopedia = [theView evalJS:@"vibeGetEncyclopedicEntries()"];
    
    if(!IsEmpty(jsonEncyclopedia))
        self.bookEncyclopedia = [Utils toDictionary:jsonEncyclopedia];
    
    NSString *jsonFigure = [theView evalJS:@"vibeGetFigureEntries()"];
    if(!IsEmpty(jsonFigure))
        self.bookFigure = [Utils toDictionary:jsonFigure];

    NSString *jsonMedia = [theView evalJS:@"vibeGetMediaEntries()"];
    if(!IsEmpty(jsonMedia))
        self.bookMedia = [Utils toDictionary:jsonMedia];
    
    //VLog(@"JSON: %@", jsonMedia);
}

- (void)webView:(UIWebView *)theWebView didFailLoadWithError:(NSError *)error{
    VLog(@"%@", error);
	//[webView dealloc];
    
    // BUG FIX
    // jca-06-21-16
    // Release web view from memory in case of error
    //[webView release];
}

- (void) webViewDidFinishLoad:(UIWebView*) theView{
    //VLog(@"Chapters: webViewDidFinishLoad: %d", fontPercentSize);
    
    NSString *varMySheet = @"var mySheet = document.styleSheets[0];";
	NSString *addCSSRule =  @"function addCSSRule(selector, newRule) {"
	"if (mySheet.addRule) {"
    "mySheet.addRule(selector, newRule);"								// For Internet Explorer
	"} else {"
    "ruleIndex = mySheet.cssRules.length;"
    "mySheet.insertRule(selector + '{' + newRule + ';}', ruleIndex);"   // For Firefox, Chrome, etc.
    "}"
	"}";
	
    //	VLog(@"w:%f h:%f", webView.bounds.size.width, webView.bounds.size.height);
	
	//NSString *insertRule1 = [NcSString stringWithFormat:@"addCSSRule('html', 'padding: 10px; height: %fpx; -webkit-column-gap: 20px; -webkit-column-width: %fpx;')", webView.frame.size.height, webView.frame.size.width];
    NSString *insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'padding: 10px; height: %fpx; -webkit-column-gap: 20px; -webkit-column-width: %fpx; -webkit-column-fill: balance')", theView.bounds.size.height, theView.bounds.size.width];
	NSString *insertRule2 = [NSString stringWithFormat:@"addCSSRule('p', 'text-align: justify;')"];
	NSString *setTextSizeRule = [NSString stringWithFormat:@"addCSSRule('body', '-webkit-text-size-adjust: %d%%;')", fontPercentSize];
    NSString *setImageRule = [NSString stringWithFormat:@"addCSSRule('img', 'max-width: %fpx; height:auto;')", theView.bounds.size.width *0.75];
	[theView stringByEvaluatingJavaScriptFromString:varMySheet];
	[theView stringByEvaluatingJavaScriptFromString:addCSSRule];
	[theView stringByEvaluatingJavaScriptFromString:insertRule1];
	[theView stringByEvaluatingJavaScriptFromString:insertRule2];
    [theView stringByEvaluatingJavaScriptFromString:setTextSizeRule];
    [theView stringByEvaluatingJavaScriptFromString:setImageRule];
    
    [theView attachJS:@"json2"];
    [theView attachJS:@"vibebook-helpers"];
    
	int totalWidth = [[theView stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollWidth"] intValue];
    //    VLog(@"webViewBounds Width: %f", webView.bounds.size.width);
    //    float w = webView.bounds.size.width;
    //    VLog(@"Total Width of %i and %f is %f", totalWidth, w, totalWidth / w);
    //Total Width of 1456 and 728.000000 is 2.000000
	pageCount = (int)((float)totalWidth/theView.bounds.size.width);
	self.pagesInChapter = pageCount;
    
    //VLog(@"Chapter %d: %@ -> %d pages", chapterIndex, title, pageCount);
    
    [self _extractVibeData:theView];
    
    // BUG FIX
    // jca-06-21-16
    // Release web view from memory
    //[webView release];
    
    //[webView dealloc];
    [self.delegate chapterDidFinishLoad:self];
}

- (BOOL)webView:(UIWebView *) theWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSString *filePath = [[request URL] path];
    
    if ([[filePath pathExtension] isIn:@"xhtml", @"html", @"htm", nil] && [[[request URL] scheme] isEqualToString:@"file"]) {
        //NSLog(@"Chapter: shouldStartLoadWithRequest: filePath: %@ | spinePath: %@", filePath, spinePath);
        
        NSData *spineData = isChapterSecured ? [self.vibeDRM decrypt:[NSData dataWithContentsOfFile:spinePath]] : [NSData dataWithContentsOfFile:spinePath];
        NSURL *baseURL = [NSURL fileURLWithPath:[filePath stringByDeletingLastPathComponent] isDirectory:YES];
        NSString *html = [[NSString alloc] initWithData:spineData encoding:NSUTF8StringEncoding];
        //VLog(@"baseURL: %@", html);
        html = [html stringByReplacingOccurrencesOfString:@"<title></title>" withString:@"<title>Untitled</title>"];
        html = [html stringByReplacingOccurrencesOfString:@"<title/>" withString:@"<title>Untitled</title>"];
        html = [html stringByReplacingOccurrencesOfString:@"<title />" withString:@"<title>Untitled</title>"];
        
        text = [html stringByConvertingHTMLToPlainText];
        
        //VLog(@"%@", html);
        [theWebView loadHTMLString:html baseURL:baseURL];
        
        return NO;
    }
    
    return YES;
}

@end
