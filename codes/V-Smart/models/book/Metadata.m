//
//  Metadata.m
//  Booklatan
//
//  Created by Earljon Hidalgo on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Metadata.h"

@implementation Metadata
@synthesize publisher, authors, title, coverImageUrl, bookId, dateAdded, dateLastRead, isRead, uuid, bookType;

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.title = [decoder decodeObjectForKey:@"title"];
        self.authors = [decoder decodeObjectForKey:@"authors"];
        self.isRead = [decoder decodeBoolForKey:@"isRead"];
        self.bookId = [decoder decodeObjectForKey:@"bookId"];
        self.dateAdded = [decoder decodeObjectForKey:@"dateAdded"];
        self.dateLastRead = [decoder decodeObjectForKey:@"dateLastRead"];
        self.coverImageUrl = [decoder decodeObjectForKey:@"coverImageUrl"];
        self.publisher = [decoder decodeObjectForKey:@"publisher"];
        self.uuid = [decoder decodeObjectForKey:@"uuid"];
        self.bookType = [decoder decodeObjectForKey:@"bookType"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:title forKey:@"title"];
    [encoder encodeObject:authors forKey:@"authors"];
    [encoder encodeBool:isRead forKey:@"isRead"];
    [encoder encodeObject:bookId forKey:@"bookId"];
    [encoder encodeObject:dateAdded forKey:@"dateAdded"];
    [encoder encodeObject:dateLastRead forKey:@"dateLastRead"];   
    [encoder encodeObject:coverImageUrl forKey:@"coverImageUrl"];
    [encoder encodeObject:publisher forKey:@"publisher"];
    [encoder encodeObject:uuid forKey:@"uuid"];
    [encoder encodeObject:bookType forKey:@"bookType"];
}

@end
