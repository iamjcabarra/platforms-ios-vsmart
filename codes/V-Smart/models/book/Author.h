//
//  Author.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/31/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"

@interface Author : JSONModel

//@property (nonatomic, assign) int authorId;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *authorName;
@property (nonatomic, assign) int ordering;
@end
