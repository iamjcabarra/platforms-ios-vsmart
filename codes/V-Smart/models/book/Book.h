//
//  Book.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/31/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"
#import "Author.h"
#import "BookCategory.h"
//#import "CustomJSONConverters.h"
#import "NSObject+Archiver.h"

@protocol Author @end
@protocol BookCategory @end

@interface Book : JSONModel

@property (nonatomic, strong) NSArray<Author> *authors;
@property (nonatomic, strong) NSString *bookId;
//@property (nonatomic, strong) NSArray<BookCategory> *category;
@property (nonatomic, strong) NSString<Optional> *contentProtection;
@property (nonatomic, strong) NSString<Optional> *cover;
@property (nonatomic, strong) NSString<Optional> *currency;
@property (nonatomic, strong) NSString<Optional> *downloadLink;
@property (nonatomic, assign) NSInteger estimatedFileSize;
@property (nonatomic, assign) int expiryDate;
@property (nonatomic, strong) NSString<Optional> *image;
@property (nonatomic, strong) NSString<Optional> *isbn;
@property (nonatomic, strong) NSString<Optional> *mediaType;
@property (nonatomic, assign) int price;
@property (nonatomic, strong) NSString<Optional> *publisher;
@property (nonatomic, assign) int purchaseDate;
//@property (nonatomic, assign) int revision;
@property (nonatomic, strong) NSString<Optional> *sku;
@property (nonatomic, strong) NSString<Optional> *thumbnail;
@property (nonatomic, strong) NSString<Optional> *title;
@property (nonatomic, strong) NSString<Optional> *webLink;
@property (nonatomic, strong) NSString<Optional> *fileSizeString;

@end
