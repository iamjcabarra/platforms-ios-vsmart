//
//  Chapter.h

#import <Foundation/Foundation.h>
//#import "VibeCrypto.h"
#import "VibeDRM.h"

@class Chapter;
@class NCXElement;

@protocol ChapterDelegate <NSObject>
@required
- (void) chapterDidFinishLoad:(Chapter*)chapter;
@end

@interface Chapter : NSObject <UIWebViewDelegate>{
    NSString* spinePath;
    NSString* title;
	NSString* text;
//    id <ChapterDelegate> delegate;
    int pageCount;
    int chapterIndex;
    CGRect windowSize;
    int fontPercentSize;
    
    BOOL isChapterSecured;
}

@property (nonatomic, weak) id <ChapterDelegate> delegate;
@property (nonatomic, readonly) int pageCount, chapterIndex, fontPercentSize;
@property (nonatomic, retain) NSString *spinePath, *title, *text;
@property (nonatomic, readonly) CGRect windowSize;
@property (nonatomic, retain) UIWebView* webView;
@property (nonatomic, retain) NCXElement* ncxElement;
@property (nonatomic, assign) int pagesInChapter;
@property (nonatomic, assign) int pageInBook;
@property (nonatomic, strong) VibeDRM *vibeDRM;
@property (nonatomic, strong) NSString *bookName;
@property (nonatomic, retain) NSDictionary *bookEncyclopedia;
@property (nonatomic, retain) NSDictionary *bookFigure;
@property (nonatomic, retain) NSDictionary *bookMedia;

- (id) initWithPath:(NSString*)theSpinePath title:(NSString*)theTitle chapterIndex:(int) theIndex;
- (void) loadChapterWithWindowSize:(CGRect)theWindowSize fontPercentSize:(int) theFontPercentSize withSecurity:(BOOL) fileSecured drm:(VibeDRM *)object;

@end
