//
//  Author.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/31/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "Author.h"

@implementation Author

+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}

@end
