//
//  Metadata.h
//  Booklatan
//
//  Created by Earljon Hidalgo on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Metadata : NSObject<NSCoding>

@property (nonatomic, retain) NSString *publisher;
@property (nonatomic, retain) NSString *authors;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *coverImageUrl;
@property (nonatomic, retain) NSString *bookId;
@property (nonatomic) BOOL isRead;
@property (nonatomic, retain) NSString *dateLastRead;
@property (nonatomic, retain) NSDate *dateAdded;
@property (nonatomic, retain) NSString *uuid;
@property (nonatomic, retain) NSString *bookType;
@end
