//
//  JMLockInOut.h
//  V-Smart
//
//  Created by VhaL on 4/4/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JSONModel.h"

@protocol JMLockInOutResponse @end
@protocol JMLockInOutBook @end
@protocol JMLockInOutBookData @end
@protocol JMLockInOutBookExercise @end

@interface JMLockInOutResponse : JSONModel
//@property (nonatomic, strong) NSString *teacherId;
@property (nonatomic, strong) NSArray<JMLockInOutBook> *books;
@end


@interface JMLockInOutBook : JSONModel
@property (nonatomic, strong) NSString *bookUuid;
@property (nonatomic, strong) NSArray<JMLockInOutBookData> *data;
@end


@interface JMLockInOutBookData : JSONModel
@property (nonatomic, strong) NSString *section;
@property (nonatomic, strong) NSArray<JMLockInOutBookExercise> *exercises;
@end


@interface JMLockInOutBookExercise : JSONModel
@property (nonatomic, strong) NSString *exerId;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, assign) NSNumber<Optional> *isOpened;
@end