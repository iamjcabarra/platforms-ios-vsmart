//
//  SSPeopleItem.h
//  V-Smart
//
//  Created by VhaL on 3/11/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SSPeopleItem : NSObject
@property (nonatomic, assign) int id;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *urlImage;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *subjectsOrNickname;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, assign) BOOL isTeacher;
@property (nonatomic, assign) int sectionId;
@property (nonatomic, strong) NSString *sectionName;
@property (nonatomic, strong) NSString *numPostedTopics;
@property (nonatomic, strong) NSString *numRepliedTopics;
@property (nonatomic, strong) NSString *numLikedByMe;
@property (nonatomic, strong) NSString *numReceivedLikes;

+(UIColor*)generateRandomColor;
@end
