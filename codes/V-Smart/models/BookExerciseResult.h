//
//  BookExerciseResult.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/17/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BookExercise.h"

@interface BookExerciseResult : NSObject
@property (nonatomic, strong) BookExercise *bookExercise;
@property (nonatomic, strong) NSString *bookName;
@property (nonatomic, strong) NSString *bookTitle;
@property (nonatomic, strong) NSString *chapterName;
@property (nonatomic, assign) int result;
@property (nonatomic, strong) NSDate *dateCreated;
@end
