//
//  Folder.h
//  V-Smart
//
//  Created by VhaL on 2/26/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Note;

@interface NoteBook : NSManagedObject

@property (nonatomic, retain) NSString * noteBookID;
@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSNumber * colorId;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSString * noteBookName;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic, retain) NSNumber * syncId;
@property (nonatomic, retain) NSNumber * isRemoved;
@property (nonatomic, retain) NSNumber * isShared;
@property (nonatomic, retain) NSSet *notes;
@end

@interface NoteBook (CoreDataGeneratedAccessors)

-(void)addNotesObject:(Note *)value;
-(void)removeNotesObject:(Note *)value;
-(void)addNotes:(NSSet *)values;
-(void)removeNotes:(NSSet *)values;

@end
