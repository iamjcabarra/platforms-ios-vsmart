//
//  JMNTag.m
//  V-Smart
//
//  Created by VhaL on 3/17/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "JMNTag.h"

@implementation JMNTag
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end

@implementation JMNRecordTag
+(JSONKeyMapper*)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}
@end