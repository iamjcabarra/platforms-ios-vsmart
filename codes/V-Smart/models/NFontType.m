//
//  NFontType.m
//  V-Smart
//
//  Created by VhaL on 5/2/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NFontType.h"


@implementation NFontType

@dynamic fontTypeId;
@dynamic fontName;
@dynamic created;
@dynamic isRemoved;
@dynamic modified;

@end
