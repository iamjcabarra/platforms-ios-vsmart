//
//  NTMDataManager.swift
//  V-Smart
//
//  Created by Julius Abarra on 08/08/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

class NTMDataManager: Routes {
    
    // MARK: - Singleton
    
    static let sharedInstance: NTMDataManager = {
        let singleton = NTMDataManager()
        return singleton
    }()
    
    // MARK: - Properties
    
    fileprivate let db = VSCoreDataStack(name: "NTMDataModel")
    fileprivate let session = URLSession.shared
    
    var isProductionMode: Bool = true
    var dateFormatter: DateFormatter! = DateFormatter()
    var userDefaults: UserDefaults! = UserDefaults.standard
    
    // MARK: - Closures
    
    typealias NTMDoneBlock = (_ doneBlock: Bool) -> Void
    typealias NTMDataBlock = (_ dataBlock: [String: AnyObject]?) -> Void
    
}
