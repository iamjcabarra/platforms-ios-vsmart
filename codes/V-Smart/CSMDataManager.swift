//
//  CSMDataManager.swift
//  V-Smart
//
//  Created by Julius Abarra on 27/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

class CSMDataManager: Routes {
    
    // MARK: - Singleton
    
    static let sharedInstance: CSMDataManager = {
        let singleton = CSMDataManager()
        return singleton
    }()
    
    // MARK: - Properties
    
    fileprivate let db = VSCoreDataStack(name: "CSMDataModel")
    fileprivate let session = URLSession.shared
    
    var isProductionMode: Bool = true
    var dateFormatter: DateFormatter! = DateFormatter()
    var userDefaults: UserDefaults! = UserDefaults.standard
    
    // MARK: - Closures
    
    typealias CSMDoneBlock = (_ doneBlock: Bool) -> Void
    typealias CSMDataBlock = (_ dataBlock: [String: AnyObject]?) -> Void
    
    func getMainContext() -> NSManagedObjectContext! {
        return self.db.objectMainContext()
    }
    
    // MARK: - Utility Methods (Non-Swift Dependency)
    
    func accountUserID() -> String {
        guard let account = Utils.getArchive("GLOBAL_ACCOUNT") as? AccountInfo else { return "" }
        return "\(account.user.id)"
    }
    
    func requestCourseList(_ handler: @escaping CSMDataBlock) {
        let user = self.accountUserID()
        let endPoint = "/v2/courses/search/user/\(user)"
        let request = self.route("GET", uri: endPoint, body: nil)
        
        let task = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let e = error {
                print(e.localizedDescription)
                handler(nil)
                return
            }
            
            // SHOULD BE DONE AFTER CHECKING IF IT IS OKAY TO PARSE
            
            let ctx = self.db.objectContext()
            let status = self.db.clearEntity(CSMConstants.Entity.COURSE, ctx: ctx!, filter: nil)
            print("clear entity... \(status)")
            
            guard let d = data, let dictionary = self.parseResponseData(d) as? [String: AnyObject] else {
                handler(nil)
                return
            }
                
            if (self.okayToParseResponse(dictionary)) {
                guard let records = dictionary["records"] as? [[String: AnyObject]] else {
                    handler(nil)
                    return
                }
                
                print("course records: \(records)")
                
                ctx?.performAndWait({
                    for r in records {
                        let course_id = self.stringValue(r["course_id"])
                        let course_name = self.stringValue(r["course_name"])
                        let initial = self.stringValue(r["initial"])
                        let description = self.stringValue(r["course_description"])
                        let course_code = self.stringValue(r["course_code"])
                        let cs_id = self.stringValue(r["cs_id"])
                        let section_id = self.stringValue(r["section_id"])
                        let section_name = self.stringValue(r["section_name"])
                        let grade_level = self.stringValue(r["grade_level"])
                        let schedule = self.stringValue(r["schedule"])
                        let venue = self.stringValue(r["venue"])
                        let search_string = "\(course_name)\(section_name)\(grade_level)\(schedule)"
                        
                        let predicate = NSComparisonPredicate(keyPath: "cs_id", withValue: cs_id, isExact: true)
                        let mo = self.db.retrieveEntity(CSMConstants.Entity.COURSE, context: ctx!, filter: predicate)
                        
                        mo.setValue(course_id, forKey: "course_id")
                        mo.setValue(course_name, forKey: "course_name")
                        mo.setValue(initial, forKey: "initial")
                        mo.setValue(description, forKey: "course_description")
                        mo.setValue(course_code, forKey: "course_code")
                        mo.setValue(cs_id, forKey: "cs_id")
                        mo.setValue(section_id, forKey: "section_id")
                        mo.setValue(section_name, forKey: "section_name")
                        mo.setValue(grade_level, forKey: "grade_level")
                        mo.setValue(schedule, forKey: "schedule")
                        mo.setValue(venue, forKey: "venue")
                        mo.setValue(search_string, forKey: "search_string")
                    }
                    
                    let value: [String:AnyObject]? = (self.db.saveObjectContext(ctx!)) ? (["count": records.count as AnyObject]) as [String:AnyObject] : nil
                    
                    handler(value)
                })
            }
            else {
                handler(nil)
            }
        }) 
        
        task.resume()
    }
}
