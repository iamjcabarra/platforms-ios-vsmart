//
//  ViewController.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/14/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "LoginViewController.h"
//#import "TextbookViewController.h"
#import "NotesViewController.h"
#import "QuizGuruViewController.h"
#import "CalendarViewController.h"
#import "SubjectsViewController.h"
#import "ClassAnalyticsViewController.h"
#import "ClassesViewController.h"
#import "AwardBadgesViewController.h"
#import "EducationalAppViewController.h"
#import "SchoolStreamViewController.h"
#import "PlaylistViewController.h"
#import "SocialStreamViewController.h"
#import "LessonPlanViewController.h"
#import "VSmart.h"

@interface ViewController : BaseViewController<LoginViewControllerDelegate, MBProgressHUDDelegate>
{
    MBProgressHUD *mbHUD;
}

@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UIView *profileView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIView *dashboardView;

@property (nonatomic, strong) NSMutableArray *downloadQueues;

//- (void)presentLoginViewController;

@end
