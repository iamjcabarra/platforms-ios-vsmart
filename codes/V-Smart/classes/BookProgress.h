//
//  BookProgress.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/20/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookProgress : UITextField

@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, strong) UIColor *progressColor;
@end
