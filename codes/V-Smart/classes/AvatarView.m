//
//  AvatarView.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/26/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "AvatarView.h"
#import <QuartzCore/QuartzCore.h>

@interface AvatarView ()
{
    BOOL applyShadow;
}

@end

@implementation AvatarView
- (id)initWithFrame:(CGRect)frame applyShadow: (BOOL) withShadow
{
    self = [super initWithFrame:frame];
    if (self) {
        applyShadow = withShadow;
        if (withShadow) {
            self.layer.shadowColor = [UIColor blackColor].CGColor;
            self.layer.shadowOffset = CGSizeMake(0,2);
            self.layer.shadowRadius = 2;
            self.layer.shadowOpacity = 0.7f;
        }
        
        self.labelAlias = [UILabel new];
    }
    return self;
}

-(void) setImage:(UIImage *)image {
    self.isUsingAlias = NO;
    
    _image = image;
    [self setNeedsDisplay];
}

-(void)useAliasWithBigFont:(BOOL)bigFont{
    self.isUsingAlias = YES;
    
    self.labelAlias.frame = self.frame;
    
//    self.labelAlias.layer.cornerRadius = self.labelAlias.frame.size.height/2;
//    self.labelAlias.layer.borderWidth = 2;
//    self.labelAlias.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.labelAlias.textAlignment = NSTextAlignmentCenter;
    
    if (bigFont)
        self.labelAlias.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:50];
    else
        self.labelAlias.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    
    self.labelAlias.textColor = [UIColor whiteColor];
    
    UIGraphicsBeginImageContext(self.labelAlias.bounds.size);
    [self.labelAlias.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    _image = viewImage;
    [self setNeedsDisplay];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
-(void)drawRect:(CGRect)rect
{
//    // Drawing code
//    CGRect b = self.bounds;
//    CGContextRef ctx = UIGraphicsGetCurrentContext();
//    
//    CGContextSaveGState(ctx);
//    
//    CGPathRef circlePath = CGPathCreateWithEllipseInRect(b, 0);
//    CGMutablePathRef inverseCirclePath = CGPathCreateMutableCopy(circlePath);
//    CGPathAddRect(inverseCirclePath, nil, CGRectInfinite);
//    
//    CGContextSaveGState(ctx);
//    {
//        CGContextBeginPath(ctx);
//        CGContextAddPath(ctx, circlePath);
//        CGContextClip(ctx);
//        [_image drawInRect:b];
//    }
//    CGContextRestoreGState(ctx);
//    
//    CGContextSaveGState(ctx);
//    {
//        CGContextBeginPath(ctx);
//        CGContextAddPath(ctx, circlePath);
//        CGContextClip(ctx);
//        
//        if (applyShadow)
//            CGContextSetShadowWithColor(ctx, CGSizeMake(0, 0), 3.0f, [UIColor colorWithRed:0.994 green:0.989 blue:1.000 alpha:1.0f].CGColor);
//        
//        CGContextBeginPath(ctx);
//        CGContextAddPath(ctx, inverseCirclePath);
//        CGContextEOFillPath(ctx);
//    }
//    CGContextRestoreGState(ctx);
//    
//    CGPathRelease(circlePath);
//    CGPathRelease(inverseCirclePath);
//    
//    CGContextRestoreGState(ctx);
    
    /*
     * We will create the medallion effect inspired from the OS X login screen
     */
    
    ////////////////////////
    // CONSTANTS
    ////////////////////////
    
    // Width and Height of Rect
    CGFloat rectWidth = rect.size.width;
    CGFloat rectHeight = rect.size.height;
    
    UIColor *borderColor = [UIColor lightGrayColor];
    CGFloat borderWidth = 1.f;
    UIColor *shadowColor = [UIColor colorWithRed:0.25f green:0.25f blue:0.25f alpha:0.75f];
    CGSize shadowOffset = CGSizeMake(0, 0);
    CGFloat shadowBlur = 2.f;
    
    // Image rect
    CGRect imageRect = CGRectMake( borderWidth, borderWidth, (rectWidth - (borderWidth * 2)) , (rectHeight - (borderWidth * 2)) );
    
    ////////////////////////
    // START MASK CREATION
    ////////////////////////
    
    /// [ STEP 1 ] /////////////////////// CREATE THE GRAPHIC CONTEXT ///////////////////////////
    
    // Create color space specific for the device in this case gray
    CGColorSpaceRef maskColorSpaceRef = CGColorSpaceCreateDeviceGray();
    // Create bitmap context for both MAIN & SHINE mask in 8-bits with alpha 0
    CGContextRef mainMaskContextRef = CGBitmapContextCreate( NULL, rectWidth, rectHeight, 8, rectWidth, maskColorSpaceRef, 0);
    CGContextRef shineMaskContextRef = CGBitmapContextCreate( NULL, rectWidth, rectHeight, 8, rectWidth, maskColorSpaceRef, 0);
    
    // DEALLOC the mask color space
    CGColorSpaceRelease(maskColorSpaceRef);
    
    /// [ STEP 2 ] ///////////////////////// FILL CONTEXT WITH COLOR ////////////////////////////
    
    // First set black shading for both MAIN & SHINE mask
    CGContextSetFillColorWithColor(mainMaskContextRef, [UIColor blackColor].CGColor); //MAIN
    CGContextSetFillColorWithColor(shineMaskContextRef, [UIColor blackColor].CGColor);//SHINE
    // Paint the MAIN & SHINE graphic context on the RECT|canvas object
    CGContextFillRect(mainMaskContextRef, rect);
    CGContextFillRect(shineMaskContextRef, rect);
    // Second set white shading for both MAIN & SHINE mask
    CGContextSetFillColorWithColor(mainMaskContextRef, [UIColor whiteColor].CGColor);
    CGContextSetFillColorWithColor(shineMaskContextRef, [UIColor whiteColor].CGColor);
    
    /// [ STEP 3 ] //////////////////////// PAINT MAIN & SHINE SHADE ///////////////////////////
    
    // MAIN (dark) shade
    CGContextMoveToPoint(mainMaskContextRef, 0, 0);             // start at this point
    CGContextAddEllipseInRect(mainMaskContextRef, imageRect);   // create mask CIRCLE within bounds
    CGContextFillPath(mainMaskContextRef);                      // paint
    // SHINE shade
    CGContextTranslateCTM( shineMaskContextRef, -(rectWidth / 4), (rectHeight / 4 * 3) );         // change coordinate system
    CGContextRotateCTM( shineMaskContextRef, -45.f);                                              // 45 degree rotation of coordinate system
    CGContextMoveToPoint( shineMaskContextRef, 0, 0);                                             // move to this point
    CGContextFillRect( shineMaskContextRef, CGRectMake( 0, 0, (rectWidth / 8 * 5), rectHeight));  // paint
    
    /// [ STEP 5 ] /////////// CREATE ACTUAL IMAGE BASE ON THE MASK & SHINE CONTEXT /////////////
    
    // We now have an actual bitmap images base from the MAIN & SHINE graphic context
    CGImageRef mainMaskImageRef = CGBitmapContextCreateImage(mainMaskContextRef);
    CGImageRef shineMaskImageRef = CGBitmapContextCreateImage(shineMaskContextRef);
    // DEALLOC both MASK & SHINE context
    CGContextRelease(mainMaskContextRef);
    CGContextRelease(shineMaskContextRef);

    //  END MASK CREATION  //

    
    /////////////////////////////////
    //  START DRAWING THE MEDALLION
    /////////////////////////////////
    
    /// [ STEP 6 ] /////////////////// GET THE CURRENT GRAPHICS CONTEXT ////////////////////
    
    // Get current graphics context
    CGContextRef ctxRef = UIGraphicsGetCurrentContext();
    // Save the initial state of the graphics context
    CGContextSaveGState( ctxRef );
    // Combine User Image and Mask Bitmap image
    CGImageRef imageRef = CGImageCreateWithMask( self.image.CGImage, mainMaskImageRef );
    // Modify context
    CGContextTranslateCTM( ctxRef, 0, rectHeight );  //change coordinate system of the current graphics context
    CGContextScaleCTM( ctxRef, 1.0, -1.0 );          //resize the current graphics context
    CGContextSaveGState( ctxRef );                    //save the new state of the graphics context
    
    /// [ STEP 7 ] ///////////////////// DRAW BITMAP IMAGE WITH MASK //////////////////////
    
    // Draw image
    CGContextDrawImage( ctxRef, rect, imageRef );     // draw image within current context
    CGContextRestoreGState( ctxRef );                 // restore with latest state
    CGContextSaveGState( ctxRef );                    // save the new state of the graphics context
    
    /// [ STEP 8 ] ///////////////////// CLIP THE SHINE MASK //////////////////////
    
    // Clip to shine's mask
    CGContextClipToMask(ctxRef, self.bounds, mainMaskImageRef);     // dark shaded image
    CGContextClipToMask(ctxRef, self.bounds, shineMaskImageRef);    // light shaded image
    CGContextSetBlendMode(ctxRef, kCGBlendModeLighten);             // light blending for sutle effect
    
    /// [ STEP 9 ] /////////// FADE MAIN & SHINE MASK USING LINEAR GRADIENT //////////////////////
    
    // Create a gradient
    CGGradientRef gradient = [self createAlphaGradient];
    // Paint the gradient overlay on the context
    CGContextDrawLinearGradient(ctxRef, gradient, CGPointMake(0, 0), CGPointMake(0, self.bounds.size.height), 0);
    
    // DEALLOC the following items (MAIN mask, SHINE mask, Image, and Gradient)
    CGImageRelease(mainMaskImageRef);
    CGImageRelease(shineMaskImageRef);
    CGImageRelease(imageRef);
    CGGradientRelease(gradient);
    // Done with image
    
    /// [ STEP 10 ] /////////// ADD LIGHT GRAY BORDERLINE ARROUND THE CIRCLE //////////////////////

    // BORDERLINE
    CGContextRestoreGState(ctxRef);                                 // restore from the lates state
    CGContextSetLineWidth(ctxRef, borderWidth);                     // set the stroke
    CGContextSetStrokeColorWithColor(ctxRef, borderColor.CGColor);  // set the stroke color
    CGContextMoveToPoint(ctxRef, 0, 0);                             // move to this point
    CGContextAddEllipseInRect(ctxRef, imageRect);                   // combine round image with border line
    
    // SHADOW
    CGContextSetShadowWithColor(ctxRef, shadowOffset, shadowBlur, shadowColor.CGColor); //create a blurred drop shadow below the circle
    CGContextStrokePath(ctxRef);                                                        //paint a the line around the shadow
    CGContextRestoreGState(ctxRef);                                                     //restore on the latest state of the graphics context
    
    /////////////////////////////////
    //  END DRAWING THE MEDALLION
    /////////////////////////////////

}

// Function for create diagonal gradient
- (CGGradientRef)createAlphaGradient
{
    CGFloat colors[6] = {1.f, 0.75f, 1.f, 0.f, 0.f, 0.f};
    CGFloat colorStops[3] = {1.f, 0.35f, 0.f};
    CGColorSpaceRef grayColorSpace = CGColorSpaceCreateDeviceGray();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(grayColorSpace, colors, colorStops, 3);
    CGColorSpaceRelease(grayColorSpace);
    
    return gradient;
}

@end
