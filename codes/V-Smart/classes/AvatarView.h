//
//  AvatarView.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/26/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AvatarView : UIView
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) UILabel *labelAlias;
@property (assign, nonatomic) BOOL isUsingAlias;

- (id)initWithFrame:(CGRect)frame applyShadow: (BOOL) withShadow;

-(void)useAliasWithBigFont:(BOOL)bigFont;

@end
