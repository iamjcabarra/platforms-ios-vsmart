//
//  BookProgress.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/20/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "BookProgress.h"

@implementation BookProgress
@synthesize progress;
@synthesize progressColor;

- (void)setProgress:(CGFloat)aProgress {
    
    if ( aProgress < 0.0 || aProgress > 1.0 ) {
        return;
    }
    
    progress = aProgress;
    
    CGRect progressRect = CGRectZero;
    
    CGSize progressSize = CGSizeMake(progress * CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    progressRect.size = progressSize;
    
    // Create the background image
    UIGraphicsBeginImageContext(self.bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextFillRect(context, self.bounds);
    
    CGContextSetFillColorWithColor(context, [self progressColor].CGColor);
    CGContextFillRect(context, progressRect);
    
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [super setBackground:image];
}

- (void)setBackground:(UIImage *)background {

}

- (UIImage *)background {
    return nil;
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        [self setProgressColor:[UIColor lightGrayColor]];
        [self setBorderStyle:UITextBorderStyleNone];
    }
    return self;
}

@end
