var desiredHeight=914;
var desiredWidth=728;

$(document).ready(function() {
    $('img').each(function() {
        var maxWidth = desiredHeight; // Max width for the image
        var maxHeight = desiredWidth;    // Max height for the image
        var ratio = 0;  // Used for aspect ratio
        var width = $(this).width();    // Current image width
        var height = $(this).height();  // Current image height

        // Check if the current width is larger than the max
        if(width > maxWidth){
            ratio = maxWidth / width;   // get ratio for scaling image
            $(this).css("width", maxWidth); // Set new width
            $(this).css("height", height * ratio);  // Scale height based on ratio
            height = height * ratio;    // Reset height to match scaled image
            width = width * ratio;    // Reset width to match scaled image
        }

        // Check if current height is larger than max
        if(height > maxHeight){
            ratio = maxHeight / height; // get ratio for scaling image
            $(this).css("height", maxHeight);   // Set new height
            $(this).css("width", width * ratio);    // Scale width based on ratio
            width = width * ratio;    // Reset width to match scaled image
        }
    });
    relayout();
    document.body.scroll = "no";
});

function relayout() {
	var bodyID = document.getElementsByTagName('body')[0];
	var totalHeight = bodyID.offsetHeight;
	var pageCount = Math.floor(totalHeight/desiredHeight) + 1;
	bodyID.style.padding = 10; //(optional) prevents clipped letters around the edges
	bodyID.style.width = ((desiredWidth) * pageCount) + "px";
	bodyID.style.height = desiredHeight + "px";
	bodyID.style.WebkitColumnCount = pageCount;
	bodyID.style.WebkitColumnGap = "20px";
	bodyID.style.WebkitColumnFill = "balance";
}

function reset() {
	var bodyID = document.getElementsByTagName('body')[0];
	var totalHeight = bodyID.offsetHeight;
	var pageCount = Math.floor(totalHeight/desiredHeight) + 1;
	bodyID.style.padding = 10; //(optional) prevents clipped letters around the edges
	bodyID.style.width = "100%";
	bodyID.style.height = "auto";
	bodyID.style.WebkitColumnCount = 1;
	bodyID.style.WebkitColumnGap = "20px";
	bodyID.style.WebkitColumnFill = "balance";
}

var min=8;
var max=32;
function increaseFontSize() {
	reset();
  	var paras = document.getElementsByTagName('p');
  	var s, i, para_length = paras.length;
  	for(i=0; i < para_length ; i++) {
  	  if(paras[i].style.fontSize) {
      	 s = parseInt(paras[i].style.fontSize.replace("pt",""));
      } else {
         s = 12;
      }
      if(s!=max) {
         s += 1;
      }
      paras[i].style.fontSize = s+"pt";
   	}
   	relayout();
   	return s;
}
function decreaseFontSize() {
   reset();
   var paras = document.getElementsByTagName('p');
   var s;
   for(i=0;i<paras.length;i++) {
      if(paras[i].style.fontSize) {
         s = parseInt(paras[i].style.fontSize.replace("pt",""));
      } else {
         s = 12;
      }
      if(s!=min) {
         s -= 1;
      }
      paras[i].style.fontSize = s+"pt";
   }
   relayout();
   return s;
}


function initializeFontSize(init_size) {
	reset();
  	var paras = document.getElementsByTagName('p');
  	var s, i, para_length = paras.length;
  	for(i=0; i < para_length ; i++) {
		if(paras[i].style.fontSize) {
			s = parseInt(paras[i].style.fontSize.replace("pt",""));
		} else {
			s = 12;
		}
		s += init_size;
		paras[i].style.fontSize = s+"pt";
   	}
   	relayout();
   	return s;
}
