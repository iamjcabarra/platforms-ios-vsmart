var desiredHeight=864;
var desiredWidth=728;

$(document).ready(function() {
				  jQuery('img').each(function() {
								var maxWidth = desiredHeight; // Max width for the image
								var maxHeight = desiredWidth;    // Max height for the image
								var ratio = 0;  // Used for aspect ratio
								var width = jQuery(this).width();    // Current image width
								var height = jQuery(this).height();  // Current image height
								
								// Check if the current width is larger than the max
								if(width > maxWidth){
								ratio = maxWidth / width;   // get ratio for scaling image
								jQuery(this).css("width", maxWidth); // Set new width
								jQuery(this).css("height", height * ratio);  // Scale height based on ratio
								height = height * ratio;    // Reset height to match scaled image
								width = width * ratio;    // Reset width to match scaled image
								}
								
								// Check if current height is larger than max
								if(height > maxHeight){
								ratio = maxHeight / height; // get ratio for scaling image
								jQuery(this).css("height", maxHeight);   // Set new height
								jQuery(this).css("width", width * ratio);    // Scale width based on ratio
								width = width * ratio;    // Reset width to match scaled image
								}
								});
				  //relayout();
				  document.body.scroll = "no";
				  });

function relayout() {
	/*
	var bodyID = document.getElementsByTagName('article')[0];
	bodyID.style.backgroundColor = "#FFFFFF";
	var totalHeight = bodyID.offsetHeight;
	var pageCount = Math.floor(totalHeight/desiredHeight) + 1;
	//bodyID.style.padding = 10; //(optional) prevents clipped letters around the edges
	bodyID.style.width = ((desiredWidth) * pageCount) + "px";
	bodyID.style.height = desiredHeight + "px";
	
	bodyID.style.WebkitColumnCount = pageCount;
	bodyID.style.WebkitColumnGap = "20px";
	bodyID.style.WebkitColumnFill = "balance";
	
	var articleWidth = bodyID.offsetWidth;
	
	bodyID = document.body;
	bodyID.style.backgroundColor = "#FFFFFF";
	bodyID.offsetWidth = articleWidth;
	bodyID.style.width = articleWidth + "px";
	bodyID.style.height = desiredHeight + "px";
	 */
	
}

function reset() {
	/*
	var bodyID = document.getElementsByTagName('article')[0];
	var totalHeight = bodyID.offsetHeight;
	var pageCount = Math.floor(totalHeight/desiredHeight) + 1;
	//bodyID.style.padding = 10; //(optional) prevents clipped letters around the edges
	bodyID.style.width = "100%";
	bodyID.style.height = "auto";
	bodyID.style.backgroundColor = "#FFFFFF";
	bodyID.style.WebkitColumnCount = 1;
	bodyID.style.WebkitColumnGap = "20px";
	bodyID.style.WebkitColumnFill = "balance";
	
	bodyID = document.getElementsByTagName('body')[0];
	bodyID.style.width = "100%";
	bodyID.style.height = "auto";
	bodyID.style.backgroundColor = "#FFFFFF";
	 */
}

var min=8;
var max=32;
function increaseFontSize() {
	reset();
	var tags = ["p", "ul", "li", "h1", "h2", "h3", "h4", "table", "pre", "dl", "dt", "dd", "section", "figcaption"];
	for (var tag_index in tags) {
	  	var paras = document.getElementsByTagName(tags[tag_index]);
	  	var s, i, para_length = paras.length;
	  	for(i=0; i < para_length ; i++) {
			if(paras[i].style.fontSize) {
				s = parseFloat(paras[i].style.fontSize);
			} else {
				s = 12;
			}
			if(s!=max) {
				s += 1;
			}
			
			paras[i].style.fontSize = s+"pt";
	   	}
	}
	//relayout();
	
   	return s;
}
function decreaseFontSize() {
	reset();
	var tags = ["p", "ul", "li", "h1", "h2", "h3", "h4", "table", "pre", "dl", "dt", "dd", "section", "figcaption"];
	
	for (var tag_index in tags) {
		var paras = document.getElementsByTagName(tags[tag_index]);
		var s;
		for(i=0;i<paras.length;i++) {
			if(paras[i].style.fontSize) {
				s = parseFloat(paras[i].style.fontSize);
			} else {
				s = 12;
			}
			if(s!=min) {
				s -= 1;
			}
			paras[i].style.fontSize = s+"pt";
		}
	}
	//relayout();
	return s;
}


function initializeFontSize(init_size) {
	reset();
	
	var tags = ["p", "ul", "li", "h1", "h2", "h3", "h4", "table", "pre", "dl", "dt", "dd", "section", "figcaption"];
	
	for (var tag_index in tags) {
	  	var paras = document.getElementsByTagName(tags[tag_index]);
	  	var s, i, para_length = paras.length;
	  	for(i=0; i < para_length ; i++) {
			if(paras[i].style.fontSize) {
				s = parseFloat(paras[i].style.fontSize);
			} else {
				s = 12;
			}
			s += init_size;
			paras[i].style.fontSize = s+"pt";
	   	}
	}
   	//relayout();
   	return s;
}

