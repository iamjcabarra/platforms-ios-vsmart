/**
 * vibehelpers-helpers.js
 *
 * @author Earljon Hidalgo
 * @date March 2014
 * @desc Vibe Book Helpers
 * @copyright Vibe Technologies 2014
 */

/**
 * This method will try to get all encyclopedic entries on the currently loaded page.
 * @note All elements with tag named 'enc' will be fetched.
 * @example
 * // JSON formatted result
 * vibeGetEncyclopedicEntries();
 * @summary Collect all encyclopedic entries on the page
 */
function vibeGetEncyclopedicEntries() {
    var entries = document.getElementsByClassName('enc');
    var savedEntries = [];
    
    if(entries) {
        for(var i = 0; i < entries.length; i++) {
            var entry = entries[i];
            var data = {"filename": entry.pathname.split('/').pop(), "text": entry.textContent};
            savedEntries.push(data);
        }
    }
    
    if(savedEntries.length != 0)
        return JSON.stringify({"result": savedEntries});
    
    return undefined;
}

/**
 * This method will collect all media related tags including all video and audio tags.
 * @note All elements with tag named 'audio' and 'video' will be fetched.
 * @example
 * // JSON Formatted result
 * vibeGetMediaEntries();
 * @summary Collect all media entries on the page
 */
function vibeGetMediaEntries() {
    var savedEntries = [];
    
//    var nodesAudio = document.getElementsByClassName('audio');
//    if(nodesAudio) {
//        for(var i = 0; i < nodesAudio.length; i++) {
//            var audio = nodesAudio[i];
//            var data = {"mediaType": "audio", "mediaId": audio.id, "mediaPath": _extractBookPathOnly(audio.src), "mediaTitle": audio.title};
//            savedEntries.push(data);
//        }
//    }
//    
//    var nodesVideo = document.getElementsByClassName('video');
//    if(nodesVideo) {
//        for(var i = 0; i < nodesVideo.length; i++) {
//            var video = nodesVideo[i];
//            var data = {"mediaType": "audio", "mediaId": audio.id, "mediaPath": _extractBookPathOnly(audio.src), "mediaTitle": audio.title};
//            savedEntries.push(data);
//        }
//    }
    
    var nodesAudio = [].slice.call(document.getElementsByTagName('audio'));
    var nodesVideo = [].slice.call(document.getElementsByTagName('video'));
    
    var players = nodesVideo.concat(nodesAudio);
    
    if(players) {
        for(var i = 0; i < players.length; i++) {
            var player = players[i];
            var data = {"mediaType": player.tagName.toLowerCase(), "mediaId": player.id, "mediaPath": _extractBookPathOnly(player.src), "mediaTitle": player.title};
            savedEntries.push(data);
        }
    }
    
    if(savedEntries.length != 0)
        return JSON.stringify({"result": savedEntries});
    
    return undefined;
}

/**
 * This method will collect all images as figures on the loaded page.
 * @note All elements with tag named 'figure' that has no 'class' attributes
 * @example
 * // JSON Formatted result
 * vibeGetFigureEntries();
 * @summary Collect all figure entries on the page
 */
function vibeGetFigureEntries() {
    var entries = document.getElementsByTagName('figure');
    var savedEntries = [];
    
    if(entries) {
        for(var i = 0; i < entries.length; i++) {
            var entry = entries[i];
            
            if(!entry.hasAttribute('class')) {
                var captions = entry.getElementsByTagName('figcaption');
                if(captions.length > 0) {
                    var caption = captions[0];
                    var images = entry.getElementsByTagName('img');
                    var image = images[0];
                    
                    var data = {"filename": _extractBookPathOnly(image.src), "imageId": image.id.length > 0 ? image.id : "0", "text": caption.textContent.trim().replace(/(\r\n|\n|\r)/gm,"")};
                    savedEntries.push(data);
                }
            }
        }
    }
    
    if(savedEntries.length != 0)
        return JSON.stringify({"result": savedEntries});
    
    return undefined;
}

/**
 * This method will find the page associated on the provided filePath entry
 * @param filePath The saved path to compare
 * @param baseOffset The Webview width that will be used.
 * @example
 * // returns 2
 * vibeFindPageNumberForEncyclopedia('part55.xhtml', 768);
 * @summary Find page number association
 */
function vibeFindPageNumberForEncyclopedia(filePath, baseOffset) {
    var entries = document.getElementsByClassName('enc');
    var elem;
    
    if(entries) {
        for(var i = 0; i < entries.length; i++) {
            var entry = entries[i];
            var currentPath = entry.pathname.split('/').pop();
            if(currentPath == filePath) {
                elem = entry;
                break;
            }
        }
    }
    
    var _pageNumber = _getPageNumberForElement(elem, baseOffset);
    
    return _pageNumber;
}

/**
 * This method will find the page associated on the provided id
 * @param id The id associated on the node
 * @param baseOffset The Webview width that will be used.
 * @example
 * // returns 2
 * vibeFindPageNumber('fig445', 768);
 * @summary Find page number association
 */
function vibeFindPageNumber(id, baseOffset) {
    var elem = document.getElementById(id);
    var _pageNumber = _getPageNumberForElement(elem, baseOffset);
    
    return _pageNumber;
}

/* Private Helpers */
function _getPageNumberForElement(elem, baseOffset) {
    var rect = elem.getBoundingClientRect();
    var _offset = rect.left - (rect.left % baseOffset);
    var _pageNumber = _offset / baseOffset;
    
    return _pageNumber;
}

function _extractBookPathOnly (src) {
    var splits = src.split('/');
    var oebpsIndex = -1;
    var imagePath = src;
    
    for(var i = 0; i < splits.length; i++) {
        if(splits[i] == "OEBPS") {
            oebpsIndex = i;
            break;
        }
    }
    
    if(oebpsIndex != -1) {
        splits.splice(0, oebpsIndex + 1);
        imagePath = splits.join('/');
    }
    
    return imagePath;
}
