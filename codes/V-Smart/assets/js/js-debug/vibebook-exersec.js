console = new Object();
console.log = function(log) {
    var iframe = document.createElement("IFRAME");
    iframe.setAttribute("src", "ios-log:#iOS#" + log);
    document.documentElement.appendChild(iframe);
    iframe.parentNode.removeChild(iframe);
    iframe = null;
}
console.debug = console.log;
console.info = console.log;
console.warn = console.log;
console.error = console.log;

function notifyClientForSecurity(id) {
    window.location = 'notify://data/security/' + id;
}

var _json = [];

var imageLock_base64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAACACAYAAAABIucKAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpENEQwQkY2RkE4MDUxMUUzOTU0QkRCMDJCNjI1NjJCQSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpENEQwQkY3MEE4MDUxMUUzOTU0QkRCMDJCNjI1NjJCQSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkQ0RDBCRjZEQTgwNTExRTM5NTRCREIwMkI2MjU2MkJBIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkQ0RDBCRjZFQTgwNTExRTM5NTRCREIwMkI2MjU2MkJBIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+5l5x6gAAGF1JREFUeNrsXWmMXFV2Pu/Vq727uqsXd9vtHbvt2MbGKxDAwxYGwyTAaBIGiR9jQSaRgkQmkTL5ESmKImWSaJTMRFGkkWBQFAKDIpFAMjiQYSCGGRviBWzwuME2dru73Xvt69tyvvveK1cv1W5Ddy3Nu1K53VWv3rt9vnvO/c65554r/cvRy1TePLJEsaxKumGK/x86O0pBn5d29UToxGCStiwL03CqQIOJPPW0BG5oDXi/ejmRb1MN08dfl8ltlZrhlaXiqpbAZDyvvs7yO8/yo+5mP50ZzZTkmyuqdGDzspL8oyGv+H95U673yR5J2iBL0q1EZnQoVczxa09Akb/eE/FLrUGFIn6F+HMXIgcp06RkQaN4TqPBZME8M5Z5md9WWX4PspxiLM8j/Pu567nnvEFTZNrGarT1g+GUzzDMgz0twa07VjSr69pCodaQ0qLwqFA8shgdLmRXG3QEmqLpBmn8M57VHv5sMnvfh0Mp75VU4eORTJG8krSb5fsxX/rRAoEmIHjs1JV0IKMaT9/YFQ7sXhVZs6IlEGgOsGaxdvFoIUeBTdN0kZouQdvy4N8VrWZ0ZTQQvYnN4VAiHzl+Obni9Egmz/L9IX98I1/14ucGDRrj80grTEPblc3r21Ysi9xzYHPHjnXtQeppDQgTCNUXI8kGytWwCtpmy8lpLTzQMVcxeAGW5aYbV+Tovf7E750bTb7JV3+N5X6iSDR0XaCxqQtM5rTVI6nCfbLiO3jn+mhi58rIzb1MQtBUnTshW93Q+P9FzVL96ROm26YqAaYQH9tBxSOJgW4YRKuiQfHqjvhvPjkQyL59IXY/28znupr9b7QFlX7+an4GPuXaIfNNvWzrGIBVfWOZx1IF7aEDm9p33r9lGbROgOXY6bxqUIHBKqg65TWTPzPEy22zNy/P93gFWOJ+r4f8ivW7I1MoxNr20F1Br0xvnpuU4nmtfe/KyAuMB0iKaZQphJJnoQvTxoDl8wZGxLKzo5kDmaL+9Ue2Ltv+ld52YQaL9s3x5Sx/J8FsKFvUSmA7o8ltszdolq7pLDNTkMeQTxFmMsQAQn6QL2NIv3ljFxj4zlfOjHkYhxjL9CW2YCMBBtO0gVNG0qo1EngEnBnJhEdjqT0ef+AJ1jABGMyfgzE0aZLZTrqoC/BckK6DjJSZSTQM+DyD2OTzUFvYJ7QOhspgaUPu7CZsP9Q38cT5kfj5ZdHmt7d0hTOqZtpKwjeRLZou+z3SbzHVuX3/utYxmESjbAKFGRxOFARgkqtVCzLHQYKQJ+TqTC2m7dtB/sABeAAXqQyrUgSD2eDWrGoEe7tb796zquUuzGHOFIUbDif5xjxzulAtvAZCrkPxfAk4/ID8gQPwAC7Ap4SVhTrBNG6JZYtP71vdsnN9Z0h2JkgBGI8E3fW/Fn3OK9c4yB84AA/gAnw8torJTGLIJ8ub2Yb6tnY1h9Z3hEAgS6RjIl2kouGywmo0yBnydpgicAAewAX4ACfgpXw0nAHN38sIP7l3daSb/QOBOr4GlphRddckVtFUQt6Qe5NfETgAD+DSN5558qORNDTwrOxn1jicKrasbAnsZO+8iQG0UNcsWu+26jfIHfK3/DsJEagm4AOcgJe8MhLY4FOk/LbupmQkoAjmAmsIvwG01NWy6msb5A75AwfgAVyAD3ACXspkTn2gqJm7V0eDoSb+UDOsuaxgO91uq02D/OF4mwwjcAE+jNMdjFdIHkwWOsNe+fHWoBJ1CEhBs0JTsuuL1aRB7pB/wTaRwCUaUqLACXiBRJpsM9OwnVBFSdBPEus/bqtdg/yBg2Q73Ag2L4/40/gVoMmdYZ9YaoFpNG2qr9kAuq0285omuIXF4jXbZ0ZmgHDT8K+ftaw8LOW40R43baB2mlYWQjTJWiVAKkcpIuK2BpvzyjXLbfXdzHLQyt9wczzqDCjTpOmKpcw1GbqpcDUkI3MojzwTWVdg9aVp1wDNNY31bSIratpSacYSHoDKkvuL7Gl4ynxsLkHQDNv7nqKCjUJCykBCKA7dViQrWKDbkR0oHVaCS9rXACBOlz/wcfqvTJ/0jEYZlvw3Ia0MkXCfnUN4rYalfCTWYpEReZuN8KcCD9mUGtc8YiUXobUmv4fCfoDlmaJwwA1589I0hxQTOeLfTsJo2K+IRcZMQac0v5z7unPaIgCG7FssCAZYuyTHHJZdczmWo4+GUthSRAXWJD9f3xPx07YVzSL1egpJYcBFxi9fk8xrlOPrGwU4pdx+QhUdsy+VzRf10FpYO7AYaGmLBRZWd189NUL/fbqfMmMDlI2NUC4+Rmo2Raauk+RRyN/UQsG2bgpFuyjU1kXf2LOW7u7tEClqWMcA+NiilWbgkINYP8512ZRl/+LgpFSiyLigXshIM5vCSNArgAJgAOulE1foZyfP0uDxnzNYo+QLNlPr6l7q3LiL/M0tJMkeKmbTlGMg08OXaPzTDwSQ/3i6l37Su5Pu3rGRHt21XICH9M9o2MfmVaVUoU6Akyq7MEq9+zQOYEgdwyA6O5KmHxzup75Dz1EhnaBV++6jW7bsoL0rI9TR5KNkTiV7+Yli2H2Z2EAXY7spOzHEAI5RYvBT6nv9eRo4tpze6/8mPXX7atrc1WTlYvBz0OoGuAq+5wz2aM5C/2vVQDhabEHCjL1/MU5/9/Jb1H/kNerYuIP233E/fWtfD2HPHHbwjPBcdiqr0hX+CZaY4xf+lp6IjyiylmjdWhpYt4Vil/porO8YnfjJ39NfDjxETz90O+1d20owmHge/vp0HQAnFkEFUBXYo2GzrHpROmwJcgQIE3b43CT99T89Q7H+Ptp472P09IN7afeqFhpPF+l4f0Js3E/kddIAFHb5wEczrJ/w0ZzM3Y7mEK3fvYc+Xb6Grpz6BV3435fpLy730Xd//yDt39AmTC+ei+X+vFZ7YVh4mGTMhz3WMqkH81Y05LM6yGYRgHz/2X+l5NBF2vHoH9NfPbhRLL2fHEgKxgifC5oxlilSislEhl+5okamYeVtSrJCQR+TErEfzKDxjEGru9opcOv95GOiMvLxUfr+j1+gG777JPIwRNoFnj/O93PS46vuXM8hf2WqaaSa5+yD+wQUa8O9yI9gAT71gxdo/Nwp2vXN79D3HuwV1/zyQpzG0gUGyGDKrtKHgzG68uE7lB4bJK2QEy+P10+KP0iBSBt1bt5D0a6eUrQEILcEvOS96TaSPR6hdXjOS392UPQD16AfmqHX1PoIHxKG26xjPw1a1mTnQsBc/+2bF2ji3Ie0+tYH6A+YNGAEnrycpAmxT86gYwMJOvP6i0z1x8nQVWpatpKWb7+dwu0rKMPkY6zvOJOP85Qa7metitCaWw5QW0eXvQvTEHkX0TW/xlr8GcUv/Yr+5mfn6U/uWS/mUPSjIKIo9UXU6g40n+eqlmXZzB39n1fYzOn0+H230Q0dYTo9lBQaBpLxy0tx+tV/PsvUPknr9z9CT371ZtrUFRaAWLHHbQzMvdQ3kqFn3nifLh35KX32zitk3PY16uruEWw0p+rU3dFG6s6viPntyOv/Ttk7/lBETaxiAXIpRbuuQDPnMFXVnHChZSAd2EieKWj0Hx+OUGLgPG19+Nt0M7O7wXie+idzQsPePT9Gn7zxPOlagX73j/6c7uxtF1UXnL0I5a29yU9rHruTDu/dTs/8w/fo0i9+SuatB6h7xarSs0PsgHdsuIkmLpwWz/3tXd0COPTHa+83r7Y8ZrznzHd1ZRoxj3iteCLIxatvvEESzzff/vV1Ilnz09G0iCGeGkrRZ++yBvIvB5/6U3p0z3Jay7QfpEUVexB0EeHAT/yO9/E5gMD1EmvP0Mm3aSI2KfxAmMnWgEJt7A5Aq/FcEVAGi+X+eOss07quQBNlG+xM5yuJgpjLoms2iXjjhfEsDSeLNJEt0uCFs5QdH6YN9/wOfeuWHosw2GDBuY7zyyptpIrf8T4+x3W4/oY7v8GOeVxoMYiMbJMTb6iZvMEm8Vw8H/1QpuWEuqCVmQJYNQgHAsSG8P6YxQA7Nt5EfaMZ6hvLCFLwwZU0jX1yglpWbqDv/MaWUmY08t5TrF2IZoA4OC/8jvfxOa7D9fhea88GQTzGJsZF0BhrbwprdcuqXvFcPB/9cABF/+rFh5Utz9uOhJQ517DfMlXnJZ4lALOenWYtOT6Y5PcU6mjvFAKMZVTRv9SViyIc1b5hO21d3lwqlSGAQVkHiWa88D4+JzvIjO9F12+jfDJGmcmREiDw46KrNnI/POL56IcVh7X6V22ZlALGpmlHqq6hac4Xq/HCsxxNk2zWeP5Sv0UiQl7RcUTgsyg4w2ZN8QdKJEIAIbTIAmy2++N9ER0pY4H4Pnw4LZcpxfWwmOprioov4fnZskoOHqn6Mqn7OQ1myCn8hVHt8VoREQg7Z8cRc5ibinmS+bPNneHSdy1mJ13DaZemRDfwfdnrJV0tTFnN8NlECM93ohJSHa14zIyIUP2twEO7oAVgcx6fLDoqohy+Lzbe8H3cZ7YgbT01B5O6jojMBE1n0GQBXPlo/6IFFxq5YIMyVwxQqppJqDy64UOBqs8nZjnXUvtC/Cn1IpPSrhldsJT6Mw9f9malzln4mI1iHuc7Jq0Fw7m1ZKkMxyUBGuKD5Ffmd91SBK1Wo9FxXj8XaApyIBevb+WObrWZv3kt0IT9tNOPqzXhwtPH8oeI7ssW4QDxkD1e4Y9hmcUvFiMt51d8R0eNDfkLaQ++j/tYoMtWmVpRCkIWz8XzHQKEfiHUBbmgH9WQjWTXiEaegVRv5hGCCvk8okQsZISOnhvLUDGbEg5uMj5JacUnhvm4PyCi8B6fnz4eSdPKi/FSGOt6zSm+j/vgfufH06QX8kKNDK0onovnox9tYSsTDOkKcDkQ2tLNOjOPVVV9BiLEcxEAQ0o3wkbHLiXonw9/JGKMnZt2i1Vns8ypUvwhal6+nk4cPUzH31U//0hWvOI+AG2s78TV91mr8Fw8/0dveim/fxvtWdPCoFkDSxyOkNeqSP3rDDSfMH0WYMDl304M0yuHXmOBsKkKhEjxBfiiwKzf9fLnizqgGMz45U/pB8/30UMHHqDH9/WIfqK/Po9Bag1do1lBsyIPi9sp6A7mCdlejvlgMEGHPz4vUrnD7csZlDDVkqT7m6Ok5tKUmbgi+rW9p5l2rW4hTLvot8ZWYbEDt5XinUo5CXHq51arOYuL+PfEQJLibAp9oWaRf+8Nt1LYW7t4dmxyjHzhZlLzGdGvEwPraTeDVt7vRXesbSxkltCUEu+1JSFyiRnliqL8nQjiIk8x4rei7fPZd7YopruJNS2bFP0pphNW/+wB5rBc17kuUV25NJqRIjA6Ga/JBn7T1MW85vSnLp1rZwuvw9OUxWZH5vyym7RC9vNAv0ChAqmisy3T4m9x1syrB1iUT1sNUEdEmvd1km22DEMXL4fCN/LRRbPhsUSqG0gCJC2bIUMtlgCTZY+IbsB9kEXBvaURMl4SoBmGRsVUnHKJcTL1qcWyZcUncvl9za0CRBe0ugDMsACLj1k651FmOMkAE9oXii5bEqDJs4WWqsGXFmoSNzSV8snJq2lMs7EG7Fdj2q5rat31/1rgzMacrZVrp+yqnfdoEDVE0WlomV7IcZ/nl/DhpMs1hDax/A2biDhVVp2juBq4NpYVatO14vyZmGCVjV9Qu4FBu34maJhLowL6jJIU+LM89jheTBO5EAlEYIMIM+WZaEjXYIYwoVg1kBcwBLWY8nHu7OAxw7kuT4hstIp78MGw06Vi550yDgyuEgg3pHM9PZG44es9QtsCLR3kcdbdyoGz/4/PApH2BdWympvHRs919IUsTcsnJ8Q2pemaCMCENjZ4RMQoZ4+CXpYKjTSeiUQ6gh8RD8U31aaIiIif/JFoQwJWKk9pB/OdOXTJlMM1NI30Yk7QesfRBvlAos700NaSNI/OJr/FNJvO6q+5YKZDq+gaaMUCeRTvgjoai716PVseqDGbc62Xdh02pqZZA06eYTqNBQxfVds8Ao/pKXtLxzxiSUbXp1kMWQSKraTUpXMwhHxV9WxkywhJIzVdVysOV+M6Ql31pWlmaWNJefRt1qUZpw5wvUdEpmtaJedaXyTzuNgRkUoJsRXN42KvPCz0HyxYImKL5R0Hg0SQmEGTFvh5i70KMpf8Z57qRI15fInQpln6DSBRQtBswOi+Oe3guyVFRCoBdpWMGIL2Lzki0tCgFfLCDArzWIqwWi/nvUYlIxWdaycd3HmVJsKGMSOGKMhZyVXHss1Cm8fFlk1paaYMlyW159obahKRfGmOKL4ky0tL0xrexiO/0eehL0uTp/tOumG6pxXWjdkngceME7dc0SwR9liLnakaWUzPSocz7fqL0oI7xddFBmSp1CetButxlXBQKqlltdTf6VjUrgOCKnMQUiyRtCMatbLVkmCd6E95/6otn7ojIrrIZrYqg2OX5eHVm8QJF4o3QEowzDTeK+oN16RvqkpFHjiF5CRFuV/on1nW77pgj7UoG4ijQbDTE+Gazd1N9PC+zfRCbFTU0/eFW6yN8jVq0LBiJiH2XqNf6J8T3tP06obEpmNjrVzTVQeuVHLVKT6yiA2WTxMPlEWtjns3d/BcdgcdOn2ZspMjLLhc7Xy/QJhaVqynAzeuEv1C/1ADGf2thsUunWmHXc2yjQ2Zc5vHSvsZFrqpLAlZkXiil0TVgEdu6qb7t3bS+5/FaTxTuxXnjrCX9q1rpaDXIzRMMyyXCP2thlzmeoZSzYm1kuqjpqPXqhBMOr+Dskr7N7bV3k8iKpXQBWCoOF7L2czBSSnvYa3K4eoioKuTV9Snkkmvs1UUmERomFmjgWPSVGDqJoyFPuGgOpUMuwh1bbdblZb5ywK19dLq7nhJ06HU8OFqmPlcj5G8KcdLOkzRKX1gkJMn8uUNQtZ6WUqUDiQqMfrywttzskdZkshttQJtDndgOjNxA/z11cxp+EwBzW2N01zQGhU0w131bIg243DycpZiocnsRXLBrJ02SWW4SHMXNHNbg5hH1FUsHYthpwwaLo+srSkkO1fHhgEB64x9TilAM+J5TSzvO2fsXI1mu35arRxryF8k9EhXp6+MdXKiIULr41m1ydrTa12gonyRq2g1bZC/ai9uyrKlSPGcht3+ktwT8Y/ppvl8uqjHnGUIHICKGvkeV9Nq0iB3yN85yR64CHwYJ+AltwW9r/FF74ymitm8agGF9a2CarjSq2GD/IED8AAuwAc4AS95IJk/l9eMQH8iH8mpRulE9rymi1MC3Xmt+vMZ5J63D50FHsAF+AAn4CUXWAW7mnyJyZx6ciKjpqGKWMfCGS6xjOpKsQYNci/aOAAP4AJ8gBPwkrd1h2lbV9P/+WX5mU/GMsPxrCoO0BFshe1qusbnqnzZtAzyhtzFEc6MA/AALsAHOAEvGfy/aBhnQz5PsT9RyA6lCoK0wJZiMZJtqVhRdtviN8gZ8obcIX/gADyAC/ABToBCkHzkZGzpCp/pCHt/eH48e3IomTcc5oiqnkOJvPDj3LaIFN+w5OzU4of8gQPwAC7AR59+ojyr48d+j5QbiGV+fm4s+xajWlq9hX0diOfndQi42z6HI23L1zkLDhKG/IED8AAuwKcUxnJOJ0TkqqCbr5qG8e5Ho5nOkwNJK8GmHLhYXthcy+FzwfsizZEf5Am5lgMGuUP+wAF4CFxEyWYLK6WryaoZhR0ie1dGMtKqyLFPxrLPfjCUeiKgeLZvX9FcykgC+iNsc8NFD0VDXpFcilZeLc1tlTXK+mn9nmcaH2OSkSnoIvLhKAh+nhpKEcv/1A1twWd7O9uPgRNC1s5mFCXgvbqDEtm07BeM8n8PxfNa9Gh/XGd/YefOlRHy2jEuOHzJvCkcPgaV/HwzHA8J9678Xm4rD0lZsVwoEyIdcJzhh0G7nJKAIqjB4B27nABgJ1uD3ld6O0OHOpt8o6p9HdniVcxpkyEUhm9weVNn+MXxTHHi+GDqID8kcUN78K6e1iBmSDFZ4uEYLd6iLM7JxEMVj7vSU6lh0wbAQ2gKAx/WSxEH2FoyG4zn6PxE7i02iS29HaHnOsK+N4ADA2ZOJ4GzZmNphpmP+D2fdIRC6bPDiYunh1O3TuTU0Ia8dvPyZr8wjdAujACMDrXgrn7Pax4TGySssoMBBgvyY8WgK6kCnRvPvsfs8Yhp6Ec2d4ZOsBYOaRUYuzIXBS2ymyDJnqGgT2nmm5w72p8IdId9gd7O8JpoSAkAuKA4hFWq+imHjQiYZJvKHE8tExmd5zQtz47zpeFMkZVE+RHLOZ8r0n/BbEL+lWpK/r8AAwBx0GJsRSK4jQAAAABJRU5ErkJggg==";

var lock_teacher = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAIx5JREFUeNrsXQuQZFdZ/s999GNmZ9+bbB5LIGskmkQeQQIYXppIMD5iBCmpMlVZNUakqAgWWomFpQglKGiBCAqCICIQCyWWgkWZgBI0ApEoKRAMeZDNwr6yuzPT0497z/H7z+uee7tnd2a2e7on2zc5e3v63u4+9/zf/zz/+Y9QStH0OHOPaDoEUwBMjykApscUANNjCoDpMQXA9JgCYHpMATA9pgCYHmfGkZzuF3zo03eNrHNCCNvwh8L/HLQU+v/yfWTuUeaP4n1l39Mfphq+6xy8elIcid1S0Q7cOov76/jeFl4v4sZDUqn9uPnbuP8grkkR/KDy/5i3ZPgG3lHutSLTZ2H6LPWNo4m43nDNi8cLgEk9OMSNId8VCfHcKIpfkETRs+Io2hvH8a4oEvVIRJpIFlv6fiZULuUi2nfyXH49l/l/AhCfw7Uv4c7FPuRNJcCEEd1QfhZEf2maJK9Ee3E9TbbiTGkSUxLHBBBQpJuWG/6zkgGjNABmQfy9WZ7v7WXZtd0so24v+zZefyqT8q9x1+fxOTkFwMQRnnZBtN9US2u/1KinFzRqNaqnKdVAeAYAOJ+SgPiREBWJYaWAkgwCAggIIKBellM36+1pd7Ob2t0uWu/LAMM7lJQfh1RoTwEwfuJvSiLxmnot/bWZen1ns1GnBhNetzLnx47zA/EfgsipAWkkAUCQFyDoZdTp9QgguLzV7nxwqdO9FX//Dm7/qBiVgp8C4ORcj4H/8Wat9vbZZuOiGRC+WQfxa6nm/JSJz1wfR5r7Y1Ccub9kWHoIqIoUYCBI3TInCfICBPw7S53OUxeXOh9ptdu/CDVxCz75P4I2HhKSjUh5JWgriPu2mUZj36aZBs02GiBKTRPfcz4T3en8EvGtdzFYjWhL3gBBOqPQS4NuLadGL9USBmqG6qxm6ukPL7Ta9wAQbwBY3ibExsJAsrFor5h4l0O/f3hupnlxmfg1K/ITLfKZ8yNLfBb5kXbL+glPFba1hiTOkZYGMb+2QEhYJeD7Nch6bF8YsOHcXFiK/2C+1X5RL+vdiO84JDaIy7BhAMBcCdftFRD1f7F5dmYWACBIAJoB8euW80uWPhPbWvrO3vNEEQNA4Px3PmmgGGc+0hJHaTDx9xpw5fZsDMw0drZGcu2JxdYX2p3uy/A1900BMMyQZSRu2dRs/hGIT4b4dd3q1uAzIj/yoj5ytBZlCUK0OkXtPm8kSaxBxYakMyoTfbZ/4x607zkuxF1L3c7L8bF/mQJgOBHB2+aazd/bsmlWEx9GnzH4rLGXOOI7A4+WEfd0avqLZSKSLtZn1In9HQuGyLmXMV6jH3h/m1igT7Y6nVfgY/84ycbhRAOABw3c9XpD/E20eTYgfs0SP4qDwE6ZbZ1rN8jQW574oi/cLEQgTpS2Q9CvuPAoItciq2Z0m8U/H4fLeJ0k+sxUAqxF7Avxi5sazbds9pzfDMR+UkT1KtzuiK5UAYKiuTCxsmCgko3g7YbIzTGgaaL3q4aI59J8RDGMLHoAzuCjH1tst1+C8xefoAAYjbWLgf9R6Pw/nZsF8ZsznvNNdC/RHGiIXxiJpaCODego59Pb4I60Vj1H/Kpi3tgOwruNUcmFHOw+6ogi2wL8o4koSRlpJoK2oX0U6uBFepLpiQeA4Ws3DPOFcO3eD65PtauHxhG+uhX77N+7yRy1bDTPhHNdWDeXefG3BUKIGuGJLqxxZ+wK3aI4iCL2h5H1e7gv0YIgITf3ZwDA8YT8Qvz++zu93o/h7d5UBZz8iGHV//nc7Mx5s9bg03F9tNRyvtG1VcKTJroM4vgugpdlJpyb5ZmO7OU2yqdKALDWfByVXTzrWrrGQFBhUKkCoITK8wrSAq6Xy6uyhYU3ok+/OUkxgolSAVqKRtGtIPyPGH3PxK8b4seFzi/m+UOutwOdGaL3zCweWs+2zF7LdFQvNwDIWDOAeCla5Pz8VAd7+Mxh5SSYVwgijNbrCKWBsRn4Oxz348vrSoNOAzDLXz/fan0Wv/vpqQQYDKVnNRuNWx3xtc5nsa9n8wzxQ6XjdL0W75b4HUvwTpcnbtA6XZ7A4fd7AMW9IP6/gQD34rcehhVwHF/RwesZnLehXQQj8NkQ/y8A0b8XwBPsbbAEMlLIhpoTCZ1vJIISRjWUbAkNJIAS99bRv0xLAC2FRDfL3t7udP6dzG9vfACo4XF/hMH+Q4j8hpnYcQNudT4TP+A2rWMt12d6ooaJD2J3mPAdgvvFEzbcDnW72V9CHH8AhP+aju5VI0SF9/C5PFfvA6cmAMxz8B2/gj5cDyA22AaZyeB+1gE2nnBSqZVY/F1RyUA06iDWkoD7n8kaNa0a6mS97wMQb0OfXy+eEAAY0uJSEHhfs954YdNzfk3H2uMosWJWlPQ9T9a4mbpuz3D7Eri91W7T4hJaG0en806A5C34yBFn5a9IoQmR4d/Pw2b4fN7On9rpdt8EQPxMj9WIVR8s3rU7mbp4RRTMMpKNFbAkSKBG0E88D6ulTq/B0unVC63WR2CIfmXDA4B13hD4fxtE7K0zFeI7o0s40a9CsV+I/LYmfIeJTgutJQBg6d5u1tuHm+8LxfMaXFE+/S9+52XZYut6AODdAMBZbGTK3Lh5De5Tyggg7QmE5qk3DC0IejWWbABBo9cECH4bwP3plYDyCW0DWMPvZhD+KQ0/pZtQkkQurFpSNDZtS3OiI74m/NISLSy2aKHd/gBE+K9iXJeGNbbG3VSfgC9/L377o5AqV7CFr5zDZwVUKrQo87EJ0iEC61lAH9RqiZ5OZvW2VE+vy7Lsh/HpO8cabJsAw297rZa+qtEA8es1P6tn9H45IxdMp128nhf7BfHnQfz51tJbQfx9TPwRdfehdq/7khOLi5/m3+PfBSi0+oGJqftlgk5BrrCXApE2Zk0OQU1LOjzna8ddniEaN/dDv98AV+/8Ort7Sep9fe3ucfeU8/GN0actatal3YyW2l2t7+cXNQDehvd/Yx0Aexzi+2UnFlt3zWt109FSqAP7gOMNmbUPZBBnMoEi4zVwFJOjmeze1mq1a3HpeWeyBADDJ/vq1sVKa4kWlSbSV56R8aJf6/2clgLuh87/GxiDv76O/V4E2F4Be+N+/fttIwU6WaaN0pzVg6pECoVJTdOqgN3DOmcV1RkUN41TCkTD4OK1NtD3JSD+ZdrdY+6PQ5ev8PmZm3JZBFRglWvRa42+r0IdvMr1x0X4RhNtM5NJ1nA7hH7cuNhaanE/2APpsirQEUdpooCB9SJczkCc2EBT6uIKP4Wve9IZJwGYSOCGG2ouocOmcjl3ylv9LqKWF1b/EgZaW/2tdg/i+GbcciwEVm69hOH2l7xuD9oXOTu4BTXUslKAI45aCuTKuopOFQgbbg6jjXj2WroVz3u9XCMTbVgJgNHYkybpVTU9wZNq0RhZ7hch92vjj6NpubX8M238LXGgp9t5F/j97kH9kja7dxiHBpS2+vufA/16V6vdvrvVaWu11LEh58zNPFLFIIydKkj0c9f0sycv37gSYI0IAJmvxsNvTa2/X3B/VNb9Uvn0bJ2WzQGfNkf62o/lef5mYeZpB7Zc0emBQNsdFkgqEElBwy9lvV52mwakNgadGsi9MTjQFmApEBsQQBo8G99zyRmjAnS2bRz/eGITKjlQwqHTSHO+KFn+oe7vau7vOX37LtxzyLgHy7csM1Z54Zor3WJwdFRp7lrI+fzbpwIzAPo5eAJ/r70Brwak9wg8XmxQQYMgciDQEjCBgrhmAL5O2cYOgGKhxcobxPwOiPzn6ZSu1M6724ifsIn1xRRvMZnCVjaLf7QDIM77gvSrZRuTlAkpoEL4r0wk1BUpHU83+3Ys3ULzySbq4RpfZzCQZDGuViTkmKjg+HfqvvFklPMGcqMGJPXHBbQtwFPNqbEHwAwvGYczMK6EkGcC/Wdr0R+5cK9d4i2sf0BuiVaup2/N9C4GF4Pc63VvB18dXBFArUu2v7Gbjs/spG5U0+93o7TMCWyUqky/buZLtGPxAG3JHicJblUrCClCqt3Z7XXvgWdwRVfH/U1AK5flgJZbkCTs1HZi8w/ABJeLntqNq9/ZWJNBaxE7InpeEqZ12dQr/4XBunrmXj2VynP77GsDBOCsj65kAU6k8Lm4RgfmnkyPN3ea3D5rmEWq30tgCaDPkAgntszRruQx2gkgJDLTQDjVAZB+DH28wuUesBpIYlYDRTaRXtxCYWq5yy+It+P6ZWqdARCNRf9H0XPYH/ZBH2/8VaZ7SfmULh3+zXhgs/shGb54cteICZ3TkcbZ9MD2yzTxY0iSQXq+HOUz1xg4fP7Opj34/CW0ADUR4fOnUgXo66fQv243N7OGLigUzBr4yQWXN+AXm4Ah8N4VT3wvgGgWrtD3G8LHfiFHqLddNo1bm+dW6Nqsms/i/szm7vb9x6Jcipge3byXHt2yF6K+rom/hpCvVgmdpEkPbbuYDs+eq5eJRTbINOg/PN/XAYD/6fVMGprOQQziASZZVPihj4SRfmYNox6LZ6oBwYZRBgJOHwBidQ3EezIIf65P77Jr96q5/Mp6ACa/z+b4QQJIJf91Odwx1/eg4x8GwY7M7NacHLEJdhpBwUj7/4L2zz0FgLoQ9kBsJUn/47EaQ3+/kDkJ4BJQnSdQlQBWCjgQ4HwxvijdWKHg1bouJC7EgyZG7weiX5SJKZ0RaL0AC4IuXK77Bn1vBD29mM7RAzsuwXkTuD4b4gQQu40ZHWmeTQ9uv1jbFQyuQSAEQO8xCalF9rHxBFRplpBE4RI6EIgoPh9v7tpQkcBVu4BCXBS5zN7ILeWyLltIVBmoASsBcD6A+75ddfSY2Ednztacb4gzmgourBIYXA9s/346Ud8GUOSDbJyvc+zBi//crk+QFCxMsaFhEj44JAxDzOHinvUMBJw2AFwhhZU0dudw7Im8+C+434nGqgQoKnboNOtvYxBbhciX2jo/NHc+Pbb1QpL8vWp05Xuc95DFddgFT6XvzpzDA2CWk3uaqEfRz2O+roByIWFV4dwiYulAYPMe96ynBDhtN9Bl6q50CEH0PY7rvfEUSAC32KNcrcOv7jnACzBJG3tMiBo9tOUiatW3aBFN6zSvKuy8wGOwC3owEs+df8hGLjSAT6CvRwCCrTIAhlQht9kspz4JqaXA+SYWskHiANkqZt2YoLWIdgrH9cJOsdprzv936/hcqRbPRUod4sgsi95unNIjID7r/RQqYD0HzZGQQXcYdgH/8jnHH9QEBaGX0NfjfnVSkEAqlQpKESgvObxhCJUIaO3iMRUbBQCD9GCBcdEXQMEzzrl1du7B9YAEmkD5Ui2qtLgTA7ooMOi9KKVHt4LzQXz9+9H4Eiv599k45B7vZhCY/i5JKohbKIDlPKhilRHGZavoy2ZQkwuAw5svGGQZaqu82Z2nRm9Ri2v2zfEgNVycCXxC4xdXFJqouBnCrriMpMw6IqX9MPZatTkdnJmEg0FwdGa37vnuY99iFZEJVaw+riptA2qhG/Wrws0uKmoil3JFoejxAWDLk5Y3mCDKGgDBjoXHaFP7cZ0kC6KnZLL9KAVlOZOWF1ewajdz/+z/E3VB2w7ObQxSB3fnKoLYr8+yLw7i1zHorFYzGn/tBX6UGP2J4YnESsn83KXv1rsUo99chxbPwfrfAoAXkMZmQbH+ZC6olEWM8akJG7HsJQ1anN1JneZWW5RiAgGwHBc6qrRqm2lp+xzNdo7RWccfjmqqF+d4kuMqoaPdlLq9SKfwtgEWJnhPWRBoDqlThkZqE9HsDspno31ZlFybKlaSgtf1dYiXdglq26KNLbSlZVrPnl2HW2Qw54g44xia54Ps2Mzgu/FaNPV7+jU17VKyBq418LpuXwPHsj4/d4741uzOs7RRB+omC7i6yBfRhNQAaODXmviVGT7j52oyJiY5Ex4ubdKGfTO/+Rw6sWk3yaRmAKNnESdyNvDUkTQ+5hvbN3eSmZ+ZyxZ29upzIHRKeTvyBRoi4YouOIPIq0fdTWWotKkONAgMWp/BHxSGrnoTgd29MnkhKi9FMUlVLTQlBnxOaUzYCSLbga6zbdggBARVl++LQfQYoEipHs1QfWYW4zV71uObds9mteaizlOAKvVVSAba2/FkAwDHWXjK19REfmPcqJ+7hAeNBJfuLpJ3KCjaWBVz6mTU6TM6rTXZ93m18inMAXXkxAruW64OkFiuJJkKPCApoMug4pLtpJrbr4Tb8I1Y5h/ApXfg9oOjJM6oAXBjHNOb0zjarQso6YJKykcQS6lf7ixOTSQ1gAZq0PhSUPeHVluqSfTBR5xiPkycpK9eYqgiBuBsIVOLrpBeOJ0rpbgNttAvwKO4De+8f6MBgBn83Wks9iWJMNWznJyncty/j0eVWH4QT8KIyoaFl//sam3F5b9LnRQOy3kz1fcVBQFhr16EUBokLCXjiHbD1P0LKdUP4dZfsZpk4gHA5fI+nCbi5Smv7YvtAtKIPIkUhalWYuBwr5ZcYoVEGWZYePX9K6uFIgKuSFTBoIwbmLCrLMS+XKnNuOXnyHg+kwMA0a8L38rE14s7I7LWXRHfD/P9VBETG3rYQwyBwGLEfRqcwVgYxDq/QZgKZTAAXwbv6BFcft0kA+DH4O68NtareknX0iHra7kVPirwvVQQ+RtFzEuMidtXLAm8XVDJK3DBBY6NKBsl5FiJIl5MytVH/2lyVEDxRCn0/O+yztdBv3BmTxmiSyqvriFV1aurH/hxV9lYax/tNIh3gKIiKmykozDRwliXrXZrCvS08u8qU3iyN1kSQNH1kPqXCyvypXC18gRx6kRVCpQyZGgD77gwDEmgzHg57vfj4zaesiNkq9FcjtP1aB+bDAC4gE9EN7K1ryykmfA6tOuIr0wITlVS3kideSCoBo9EoAakUwEcMrbbpCkXgOL3dMIr3TgxALDHXoio5xfundLE1it7LAD8Yp0K4VdDdDUBIn+lKmGl/RSqDITIif+ASWInaZUBBtrz8Xov3nlgMiSAoCsh+mf09C+RFfuFzs8rxK+K/1EZXBPP/RUglJJhRDkk4tbL2PjZDEBwpZoEACiDyOc6vR8ae8z9eWAAOhvAL/6gM+9Qy8Uc7YUo5H5vKQtXLkFLCGkMBa4s8sFJMAIbeILLfMVOMkafUQGqJPpl6PKpMxMAJ5UKdkyiQN85zyC3NgKPq5kUEpfiJp6dXBorAHCcjc7scf69m9P3c/sDxP+ZaPWvFASClk/6jBwATEbsk3ADpyI9NF4VYGb7tmn9r1Qg/smvz1+p/p+CIEiVDN+1+xVk1ke0BuFW/LtrEgCwSyg2AMmK/8IG0ImRlUDQVPSvwKUuDbAyyaLKxgvsOOqNrw0Axu4GzklSkaSyAeiIXg0CTcX/KQAQGMh+vlCvdzQA0ClkypoHSs1NAgAaxWIOmwPvQaBK4d8pAFbuIpYrmhjJKgI7wLqGjbEDAERNtAtoU7jzStw/XBMnp+J/Vcag5X8/XexsK28rqNOn3zAkgK+h1C8BqE8CTAGwWgD0h4ll8b4cvwQQqkOe44XneF4wNEgCTFXAyYmvBgFAmulgGRJfA8CM/XglgKJFJcIt2orXXiJMAbAmG0BzvZ0qVoFqDWYMF8cOAGj+Y0oJXvtQdz6/UkWxZGcMTgGwFgCYPYwd10dCeKmKvzsY4GMTYASKwzi10Mm643wX/ctV/yzgpASCxID9BiciEGSlaVRVA577LSAUtfDy8AQYgeoI+ssd2RYu6w6XRauwVt6YASBiOC1pjWzFBl2eVmYZqV53IkCgKgSvhobdMlNpiH9k/BLAdGI/OnVRmOwhK62UDTSmwY1rDWofPUhHvvxvtPDI/5EC4Zvn7KEdz7ySZvfspbzbHqtIcBZ/JIq8yRAIJSNQqf3o6vgBADxybto3YAe8iCjU/aEUUH2zgWoMxD96/5fp4dv/jDpHDpJIEu2qHPmvu+nAnZ+k83/yBtr94p8k2e2MBaJ+9VngCriX2hBUwi8zt2OKMT/9vMBhFYr8ajUOUFIBwUTROAAQQeSfePj/6Ft/8yek8ozSHWfZwVWUcAGq1gI9+LF3U9SYoV3PvZryztJYAFA6W4NPW/zSZQmpooSuNGM+KQD4igiz/N3myQ7BsqS7TpoJPIqh5cXE3/nsHSR7PYqbM8FvM2vFlMxuBjByeuyfP05zl/4gxfWG/sw4iB8F6tL5/hEVRTOknRsQQ9pybhg2AB9fQ/8OoV+7SIUFEFQpGijHAAARR9Q+dpRa+x8iUasvo+IFxQBBG6qBbYPNFz+DZN4ZG/e7P4RykT9hsqxd5RSpeKy/NhEAsAN6GOf78NdVvt4PVbwBKnID1lMN8FRqDyI+B/czSy37m1Gsr/dOHDMRTanWHwDBC1HZbIIo2CdZqfuG4QIOBQB5ISnvRKeuKnWWyvltapmVQSMdXK62Ad0uEq5HkNl9CAfrMnYRo9ktfr+f9QZAFMRJqpnBrl4SmRqKQ9trcJhbx34GL99MIfc7v7UiBfrWBozyyHKKNm+n+nlPoYWv30tRvTlYlWVdbRzWcF/W6w5tS9wVuX6Bm+e5XwSqNLCvTGKI+szEACAvCjN+BabJV9H3S1UY8VGh9SqKoNC6xQTMVOq2K19KrQe/BkL3tDQoUx8c323T9pe+kqK5LfAC2uvr+1vxH1mPiUveKf/aDJZnGKm+ija0PYeHUCnUtwzEvUN4cld2/grmCOQAV3GULe92qX7+Xtr1U79A0cxmGHgyKEmrdAHoHVe/guae9SLK2p2R90f68eifPg/nUcIsWmUHCrbJHZLLMwaqdLwACB9K0u06a2lQGVtV1ANQ6zTAYcs6HZq97Aqa2XspqawI+zLn184+j7a98Ce0oVhKZV/H5jm+6mLbGolG9yvu3u3D3DNoCJNBpV58BR29C+erRGUJsLcFQms28CRGrgo4sLLUos6xQz4w5QDcOXSAuvPHScD/p/XU/db6M8vBRDFGdmWwoCATxLh/d+L6ULecH0ZKWPV4Lzp+VcUUL9XACfMFab2igzzA7RZlJx6nsN6aiGPK5o9R9/hRSILz0Zd8XYg/aJ1jUZFMFIsG3ckUzX7vsFdHDlsC8HEH2v1uHzxRqZBZqpq0jiDgcky9xXnKWwsaAD4YBf+f1UD36EFKd1+A5+mtD/GDaV8K9ktSg2pZGa65H6c7hj1CowAAm9DvxHO8R5QKIw2QAgNUwahAwLH13vHHtYXP/n6wj5vmrs7Bx6hxiRjabqOnDPi4KJ9Fgt9NbAD3W+Pvnfh76O7J6ccBBpuiH8LDvBqPcmm1Dk4p802V9f9I8wU4ysf6P8+I4qRMFkiE7qH9xiMYtQlQfvywRCK5BQEiwIAwbsL9GOe/GkV3Tj8OMJhcS3iAt+A5/kqoQg0IawsIH9aoRLlodFlDPJDdxw+ZSZ4qlZOUuocPUNbreMt7VEafM4eMqi+CPIIKnRDWDLLc//tkStvSBEoAtVxk9cMQu/vwEC8uslhF4egEocDQKlcjkgSCt547CgAM2v8vTik7doSy1qIOGw8bAKFd58O+QpRTgKuc77g/V3dhiD88KoE0Ci8gNF5ujQR9LjKVYfvKoVVLxI0MBDyy3Y72ANSgistJAuNwHjbCUUqbm4bqCQwy6iMhSs+uGcPXCrT5gAyIXHXB/beOUiONetew/4DuersrexaWQBO+MmbZM5CVtLIwHXrNDVyfwwXMF47rzZkG2Qeq04GEOEiSvQKioTQVJsN6Ziksfj8GAXPodX+8DkBqv//tegxHuHn0KLyA6vFGPNjVGPbLIxXkvSkrCUrBIh/vLi+QlKcnCfSU8Pxxku1F735W2ZR9kR4MwRqJZdXamjg/MHbd1rG++EOV862KjIyL9GXYRm8cdUxitCrAHJy+fLNWBVzbRvniocUyJyUo3FPTLHxVA2cM1ZoAEFF2/AgAsGQzggcXaukdfFRPBavTJHzJwvc6vVoetp/zI71trR6UFkB4sxqR4TdUANDKuOVLaK+LI3p3HqgBp+sit8hNKh/5VOEsIq29spgLRuXzjxOldaLa4AW1YnYL7jlOebdTDkqsxcMTZeK79B5X+oUlEm9Bq0vBsQ2qd0EzABBmQcXr8OpL6xGSHkJK2IoH6j24+bJYiFd5/U7kU51cUETYoEgxcVSuJr6mYBGImjz1cpp78iW0/L4rhmdlltNa9mapZvUWYChzuq74SZWysMpwPot+AOBP8ed71itjMlkfAeCPW/CwF8SCrnXLnGMqEh9FUIg9nBcr7761BmnAX8CJII3ZU91Ikje3XAX3e5EvwvKvogADFZXAXTxCb/9i9gsxhOcd1U0lzX/EWNxC63gMKydwpQcH2m+ANPwUQPBsR8zIA6GIlKhKXpyHhs2WKRFfFaJ3eaQOawadSjV+w0kdYakeuvch1/MzJoI88WPL+bG2/NV/4s8baEg1gNfPBlj9cRQPeh1QDxCIp3mXSZhB0XsGi2KFjLIFkkxqVDFZEu7EpwLXaj0iuYXCUIGdITwwDdGVX9DpgBCL0P4xxE+ktvo5yfM63HR0vYkxDgDwcQAUvRYc8A/g9GeE694KL035cCkFe+8W+lX4VCkKrhENtwi1OImRp7wkEEF9H1XodmFEfMzgtilfvJtGwoTnzSCk3lrvv/DyJyAlDoyDEBGN7+C1bdcA/XfrASFnEbu99QqxGZldx73xpAJdKyrcX81EJrWGLe5V5bM0uMC1//0gvu8JzyI+eCb9XGSJz3V1cnB+Lu+GxLuGx2JcRBgnAPg4iBEGCOTfphiUlLlC60d7tpssmgFUrlCycZeE9xMqtfUGraUtXq/kv0GfrVgigaFq+hIF/Yt138MGD5Qc10sQX4L46m/52fUYjPEYNwB4GBcwXj8LSfAmFompMi22+imxEkFzj+MmIYwFXRHPUYkrBQ0ne8Z8l/svqnyrIbiwfSLD4b6vpp+p43q0GJwvcvUmEP9n9bOPufx5QpNxsKT9LVjC94Jb3oNB2cWD3Qu4WgR19fNAObvqWSqouOynWlWhufv2AKpGg8Ug3S+KadyQ+63ODyOaMQXSygZ9YmXKeMVW58dSHRKSbsZbn5iUuveTAgBHlk9ggL6EQfwzUPoat6u2r5FLZsGkqFbNtIajDBIrHWiW25pmuVhPaZ2eUOUqHW4jJyp2OfWbO2gvRm+O7ImuVQFLNGPpfxq3/TI++cgkjXhEk3c8gnF7aSTpJgzc4ZpTC4FK4A2nzabTQm+rljgRbMWxNxpLjVbZjCEXfocR9daSJ/vbti8pmfecrmd7pgbU1jJ1GPr+Jn4m/WwTdkwiANzxXkiDp8VSvq8mVZbaQdWGojUWUzYWRdCsGDY2gxPNynPn6pryotwBLxbOJnG/pwrjzvUPRl4Nej7NZBbn+fugn57OzzKpgzzJAODjMYzxL5GUV8Bt+iSAIGtWGpSaCM4OAPZ17DlXeHDEy7TimvCt+n3+t6iQSsZwZY7XxJdxLj8Je+YK3fcxungb0AZY9rgXiv06qIUrIqFugTj+aSmonlmRnFvd7s4mCSRYjRwkXaqBRqAyexlTsTULEQ2YtCkntsQummcmcjqRUn+Ht/4Yl+6Z+M2NNhgA3HEPBvjnMNgXQU//PET0K2H47dV7E1hX0RFfUrHEWio/jVDk3YvB8b5wT+syAFQxfa2MhR8bQDwAbv8ILnHW7jfpVHMSUwAMxTX/JujxBqHUm8GFPwQivBwG2dWS1FPYZWdvoJhjqGQaq37PQATh3TA5s0jWsNO1Zmm2wu89iOufQbsdl+/GTW3aoEdCG/vggeetVP8FhKmDI38AxHgBqPg80OrpksR5AEC9v+CCGBiJCAszF2cW7bRfkF6T9wV8+F9x/m/iSp1PgCOhJ87BBPmiboreBgLWIR3Ow+vvBS0vBDWfjGvn4vVOUJt34m4ERrDUq24EnRCm9Aobnw+B6N/C629oQ048MQjeJ0yVmlbuPZOPaDoEUwBMjykApscUANNjCoDpceYd/y/AAEus2W3Pn4ydAAAAAElFTkSuQmCC";


var isTeacher = false;

$(document).ready(function() {
  console.log('+++++++++++==========VIBELOCKINOUT JS=============++++++++++++');
  
  /**
   * Sample JSON Array Data
   */
  // var lock = [{
  // exer_id : "exer1",
  // isOpened : 0
  // }, {
  // exer_id : "exer2",
  // isOpened : 0
  // }, {
  // exer_id : "exer3",
  // isOpened : 0
  // }, {
  // exer_id : "exer4",
  // isOpened : 0
  // }, {
  // exer_id : "exer5",
  // isOpened : 0
  // }, {
  // exer_id : "exer6",
  // isOpened : 0
  // }, {
  // exer_id : "exer7",
  // isOpened : 0
  // }, {
  // exer_id : "exer8",
  // isOpened : 0
  // }, {
  // exer_id : "exer9",
  // isOpened : 0
  // }];
  // lockExercise(JSON.stringify(lock));
});


function lockExercise(json, isteacher) {
    console.log('+++++++++++==========lockExercise=============++++++++++++' + json + '++++++++++++' + isteacher + '++++++++++++');
    isTeacher = isteacher;

    if (isTeacher == "true") {
        setTeacherMode();
    } else {
        setStudentMode(json);
    }
}

function setTeacherMode() {
    console.log('+++++++++++==========TEACHER MODE=============++++++++++++');
    $('.exercise').each(function (index, element) {
                        
                        console.log('+++++++++++==========ADDING_DIV=============++++++++++++');
                        
                        var lockCode = $("<div class='lock-code'><span class='img-lock-teacher'><img class='img-lock' src='" + lock_teacher + "'/></span></div>");
                        $(element).prepend(lockCode);
                        
                        $(element).css({
                                       'position': 'relative'
                                       });
                        
                        $(lockCode).css({
                                        'position': 'absolute'
                                        });
                        
                        $('.img-lock-teacher').css({
                                                   'position': 'absolute',
                                                   'float': 'right',
                                                   'top': '-48px',
                                                   'left': '-21px'
                                                   });
                        
                        $('.img-lock').css({
                                           'width': '70px',
                                           'height': '90px'
                                           });
                        });
    
    $('.img-lock-teacher').click(function(e) {
                                 // $('.img-lock-teacher').on('touchstart', function(e) {
                                 var target = e.currentTarget;
                                 var parent = $(target).parent();
                                 var exer_parent = $(parent).parent();
                                 var exerid = $(exer_parent).attr('id');
                                 notifyClientForSecurity(exerid);
                                 });
}

function setStudentMode(json) {
    console.log('+++++++++++==========STUDENT_MODE=============++++++++++++');
    
    _json = JSON.parse(json);
    console.log('JSONLENGTH:' + _json.length);
    
    for (var i = 0; i < _json.length; i++) {
        executeLock(i);
    }
    
    function executeLock(i) {
       var isOpen = _json[i].isOpened;
       var exer_id = _json[i].exer_id;
       console.log('_____EXERID::' + exer_id + '______ISOPENED::' + isOpen);
       
       if (isOpen == 0) {
       var exerNum = exer_id.replace("exer", "");
       var exercise = $("#" + exer_id);
       // var exercise = $("#exer8");
       var lockid = "lock_" + exer_id;
       var idWrapper = "lock-wrapper" + exerNum;
       console.log($(exercise));
       
       var elemLock = $("<div class='lockinout' id='" + lockid + "'></div>");
       $(exercise).wrap(elemLock);
       
       var width = $('#' + lockid).width();
       var height = $('#' + lockid).height();
       console.log('WIDTH:_' + width);
       console.log('HEIGHT:_' + height);
       
       var lock = $("<div onclick=\"notifyClientForSecurity('" + exer_id + "')\" class='lockinout' id='" + idWrapper + "'><span id='lock-img'><img alt='' src='" + imageLock_base64 + "'/></span></div>");
       $('#' + lockid).prepend(lock);
       
       $('#' + lockid).css({
                           'position' : 'relative'
                           });
       
       $(lock).css({
                   'opacity' : '1',
                   'position' : 'absolute',
                   'z-index' : '999',
                   'background-color' : '#BBD1F6',
                   'width' : '100%',
                   'height' : '100%'
                   });
       
       $('#lock-img').css({
                          'position' : 'inherit',
                          'margin-left' : '20px',
                          'margin-top' : '20px'
                          });
       } else {
       var exerNum = exer_id.replace("exer", "");
       var exercise = $("#" + exer_id);
       
       var lockid = "lock_" + exer_id;
       var idWrapper = "lock-wrapper" + exerNum;
       
       $(exercise).unwrap();
       $("#" + idWrapper).remove();
       }
    }
}