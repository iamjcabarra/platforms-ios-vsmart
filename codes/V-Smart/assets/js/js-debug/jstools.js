var mySheet = document.styleSheets[0];

function addCSSRule(selector, newRule) {
    if (mySheet.addRule) {
        mySheet.addRule(selector, newRule);
    } else {
        ruleIndex = mySheet.cssRules.length;
        mySheet.insertRule(selector + '{' + newRule + ';}', ruleIndex);
    }
};

function getDocHeight() {
    var D = document;
    return Math.max(
                    D.body.scrollHeight, D.documentElement.scrollHeight,
                    D.body.offsetHeight, D.documentElement.offsetHeight,
                    D.body.clientHeight, D.documentElement.clientHeight
                    );
}

function stopPlayer() {
    var nodesAudio = [].slice.call(document.getElementsByTagName('audio'));
    var nodesVideo = [].slice.call(document.getElementsByTagName('video'));
    
    var players = nodesVideo.concat(nodesAudio);

    if(players) {
        for(var i = 0; i < players.length; i++) {
            var player = players[i];
            
            // reset to default
            var isPlayed = player.played.length == 0 ? false : true;
            if(isPlayed) {
                player.pause();
                player.currentTime = 0;
            }
        }
    }
}

function pageScroll(xOffset){
    window.scroll(xOffset,0);
}

function applyFontFamily(fontfamily) {
    var tags = 'h1, h2, h3, h4, h5, h6, p, span, div, li, pre, dl, dt, section, figcaption, table';
    $(tags).css('font-family', fontfamily);
}

function applyLastChild() {
    $('p:last-child').css('margin-right', '100px').css('padding-right', '200px');
}

function injectFile(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script')
        fileref.setAttribute("type","text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref!="undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}

function applyColor(color) {
    var bodyTag = document.getElementsByTagName("body")[0];
    var tags = 'h1, h2, h3, h4, h5, h6, p, span, div, li, pre, dl, dt, section, figcaption, table';
    
    var theColor = "white";
    var theTextColor = "";
    
    if(color == "sepia") {
        theColor = "rgb(236,226,198)";
    } else if (color == "black") {
        theColor = "rgb(58,61,67)";
        theTextColor = "white";
    }
    
    bodyTag.style.backgroundColor = theColor;
    $(tags).css('-webkit-text-fill-color', theTextColor);
}

function VRGetSectionClassAtPoint(x,y) {
    var tags = "";
    var e = document.elementFromPoint(x,y);
//    while (e) {
//        if (e.tagName == "SECTION") {
//            if(e.className == "exercise")
//                tags = e.className;
//        }
//        e = e.parentNode;
//    }
    tags = e.className + ',' + e.tagName;
    return tags;
}

function MyAppGetHTMLElementsAtPoint(x,y) {
    var tags = ",";
    var e = document.elementFromPoint(x,y);
    while (e) {
        if (e.tagName) {
            tags += e.tagName + ',';
        }
        e = e.parentNode;
    }
    return tags;
}

function MyAppGetLinkSRCAtPoint(x,y) {
    var tags = "";
    var e = document.elementFromPoint(x,y);
    while (e) {
        if (e.src) {
            tags += e.src;
            break;
        }
        e = e.parentNode;
    }
    return tags;
}

function MyAppGetLinkHREFAtPoint(x,y) {
    var tags = "";
    var e = document.elementFromPoint(x,y);
    while (e) {
        if (e.href) {
            tags += e.href;
            break;
        }
        e = e.parentNode;
    }
    return tags;
}

function documentCoordinateToViewportCoordinate(x,y) {
    var coord = new Object();
    coord.x = x - window.pageXOffset;
    coord.y = y - window.pageYOffset;
    return coord;
}

function viewportCoordinateToDocumentCoordinate(x,y) {
    var coord = new Object();
    coord.x = x + window.pageXOffset;
    coord.y = y + window.pageYOffset;
    return coord;
}

function elementFromPointIsUsingViewPortCoordinates() {
    if (window.pageYOffset > 0) {     // page scrolled down
        return (window.document.elementFromPoint(0, window.pageYOffset + window.innerHeight -1) == null);
    } else if (window.pageXOffset > 0) {   // page scrolled to the right
        return (window.document.elementFromPoint(window.pageXOffset + window.innerWidth -1, 0) == null);
    }
    return false; // no scrolling, don't care
}

function elementFromDocumentPoint(x,y) {
    if (elementFromPointIsUsingViewPortCoordinates()) {
        var coord = documentCoordinateToViewportCoordinate(x,y);
        return window.document.elementFromPoint(coord.x,coord.y);
    } else {
        return window.document.elementFromPoint(x,y);
    }
}

function elementFromViewportPoint(x,y) {
    if (elementFromPointIsUsingViewPortCoordinates()) {
        return window.document.elementFromPoint(x,y);
    } else {
        var coord = viewportCoordinateToDocumentCoordinate(x,y);
        return window.document.elementFromPoint(coord.x,coord.y);
    }
}

function getHashPage(id, desiredWidth, desiredHeight) {
	if(id != '') {
		var offset = 0;
		var pos = $('#' + id).offset();
		if(pos!=null){
			offset = pos.left - (pos.left % desiredWidth);
			if (pos.top < desiredHeight / 15 && offset > 0) {
				offset = offset - desiredWidth;
			}
		}
		var pageNumber = offset / desiredWidth;
        return pageNumber;
	}
    return 0;
}

function getElementTopLeft(id, desiredWidth, desiredHeight) {
    var offset = 0;
    var pos = document.getElementById(id);
    
    if(pos!=null){
        offset = pos.offsetLeft - (pos.left % desiredWidth);
        if (pos.offsetTop < desiredHeight / 10 && offset > 0) {
            offset = offset - desiredWidth;
        }
    }
    var pageNumber = offset; /// desiredWidth;
    //return pageNumber.toString();
    return pos.offsetTop.toString() + "," + pos.offsetLeft.toString();
    //return { top: top, left: left };
}
