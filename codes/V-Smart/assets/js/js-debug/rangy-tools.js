// Rangy Tools
//
// Vibe Technologies 2014
//
// Changes:
// Earljon Hidalgo  March 2014
//

console = new Object();
console.log = function(log) {
    var iframe = document.createElement("IFRAME");
    iframe.setAttribute("src", "ios-log:#iOS#" + log);
    document.documentElement.appendChild(iframe);
    iframe.parentNode.removeChild(iframe);
    iframe = null;
}
console.debug = console.log;
console.info = console.log;
console.warn = console.log;
console.error = console.log;

var highlighter;
var serializedHighlights;

var selectedRange;

$(document).ready(function() {
                  rangy.init();
                  
                  highlighter = rangy.createHighlighter();

                  highlighter.addClassApplier(rangy.createCssClassApplier("vibeHighlight", {
                                                                          ignoreWhiteSpace : true,
                                                                          tagNames : ["span", "a"],
                                                                          elementProperties : {
                                                                          onclick : function() {
                                                                                var highlight = highlighter.getHighlightForElement(this);
                                                                                console.log('onclick: ' + highlight.id);
                                                                                //return false;
                                                                            }
                                                                          }
                                                                          }));
                  highlighter.addClassApplier(rangy.createCssClassApplier("vibeHighlightYellow", {
                                                                          ignoreWhiteSpace : true,
                                                                          tagNames : ["span", "a"],
                                                                          elementProperties : {
                                                                          onclick : function() {
                                                                                var highlight = highlighter.getHighlightForElement(this);
                                                                                console.log('onclick: ' + highlight.id);
                                                                                notifyApp('color', highlight.id);
                                                                                //return false;
                                                                            }
                                                                          }
                                                                          }));
                  highlighter.addClassApplier(rangy.createCssClassApplier("vibeHighlightRed", {
                                                                          ignoreWhiteSpace : true,
                                                                          tagNames : ["span", "a"],
                                                                          elementProperties : {
                                                                          onclick : function() {
                                                                          var highlight = highlighter.getHighlightForElement(this);
                                                                          console.log('onclick: ' + highlight.id);
                                                                          notifyApp('color', highlight.id);
                                                                          //return false;
                                                                          }
                                                                          }
                                                                          }));
                  
                  highlighter.addClassApplier(rangy.createCssClassApplier("vibeHighlightGreen", {
                                                                          ignoreWhiteSpace : true,
                                                                          tagNames : ["span", "a"],
                                                                          elementProperties : {
                                                                          onclick : function() {
                                                                          var highlight = highlighter.getHighlightForElement(this);
                                                                          console.log('onclick: ' + highlight.id);
                                                                          notifyApp('color', highlight.id);
                                                                          //return false;
                                                                          }
                                                                          }
                                                                          }));
                  
                  highlighter.addClassApplier(rangy.createCssClassApplier("vibeHighlightBlue", {
                                                                          ignoreWhiteSpace : true,
                                                                          tagNames : ["span", "a"],
                                                                          elementProperties : {
                                                                          onclick : function() {
                                                                          var highlight = highlighter.getHighlightForElement(this);
                                                                          console.log('onclick: ' + highlight.id);
                                                                          notifyApp('color', highlight.id);
                                                                          //return false;
                                                                          }
                                                                          }
                                                                          }));
                  
                  highlighter.addClassApplier(rangy.createCssClassApplier("vibeHighlightNote", {
                                                                          ignoreWhiteSpace : true,
                                                                          tagNames : ["span", "a"],
                                                                          elementProperties : {
                                                                          onclick : function() {
                                                                          var highlight = highlighter.getHighlightForElement(this);
                                                                          console.log('onclick: ' + highlight.id);
                                                                          var rect = this.getBoundingClientRect();
                                                                          console.log('rect: ' + "{{" + rect.left + "," + rect.top + "}, {" + rect.width + "," + rect.height + "}}");
                                                                          
                                                                          notifyNote(highlight.id, rect.left, rect.top, rect.width, rect.height);
                                                                          //return false;
                                                                          }
                                                                          }
                                                                          }));
                  });


function rangyTestSelection(){
    var selection = rangy.getSelection();
    console.log('Selected: ' + selection);
    return selObj;
}

function rangySerializeSelection() {
    //highlighter.removeHighlights(highlighter.highlights);
    var sel = rangy.getSelection();
    console.log('rangySerializeSelection: ' + sel);
    
    //var highlights = rangy.serializeSelection(sel, true);
    //var highlights = highlighter.serialize();
    serializedHighlights = highlights;
    
    highlights = highlightSelectedText();
    console.log('rangySerializeSelectionHighlights: ' + highlights);
    return highlights;
}

function rangyDeserializeSelection(value) {
    var serializedText = value;
    console.log('rangyDeserializeSelection: ' + serializedText);
    //rangy.deserializeSelection(serializedText);
    highlighter.deserialize(serializedText);
    //rangyTestSelection();
    //highlightSelectedText();
}

var each = [].each ?
    function(arr, func) {
        arr.forEach(func);
    } :
    function(arr, func) {
        for (var i = 0, len = arr.length; i < len; ++i) {
            func( arr[i] );
    }
};

function getJSONForHighlight (highlight) {
    var json;
    var serializeValue, startRange, endRange;
    var textValue, highlightId, cssClass;
    
    var characterRange = highlight.characterRange;
    var parts = [
         characterRange.start,
         characterRange.end,
         highlight.id,
         highlight.classApplier.cssClass,
         highlight.containerElementId
    ];
    serializeValue = parts.join("$");
    textValue = highlight.getText();
    highlightId = highlight.id;
    startRange = characterRange.start;
    endRange = characterRange.end;
    cssClass = highlight.classApplier.cssClass;
    
    json = JSON.stringify({"id": highlightId, "textValue": textValue, "startRange": startRange, "endRange": endRange, "cssClass": cssClass, "serializedValue": serializeValue});
    return json;
}

function vibeRevertSelection(noteId, serializedText) {
    highlighter.deserialize(serializedText);
    var currentHighlight = [];
    each(highlighter.highlights, function(selected) {
         if(selected.id == noteId) {
            currentHighlight.push(selected);
         }
    });
    
    highlighter.removeHighlights(currentHighlight);
}

function vibeRemoveAllHighlights() {
    highlighter.removeAllHighlights();
}

function vibeHighlightStoredData(storedData) {
    var serializedText = storedData;
    highlighter.deserialize(serializedText);
}

function vibeUnhighlightSelected() {
    var h = highlighter.unhighlightSelection();
    var highlights = [];
    if (h) {
        if (h.length > 0) {
            each(h, function(selected) {
                 var merged = getJSONForHighlight(selected);
                 highlights.push(merged);
            });
        };
    };
    
    return JSON.stringify({"result": highlights});
}

function vibeHighlightSelected(cssClass) {
    var currentSelection = rangy.getSelection();
    var mergedSelections = [];
    
    selectedRange = currentSelection;
    
    // get current highlights for the current selection
    var highlightsInSelection = getHighlightsInSelection();
    if (highlightsInSelection) {
        if (highlightsInSelection.length > 0) {
            each(highlightsInSelection, function(selected) {
                 var merged = getJSONForHighlight(selected);
                 mergedSelections.push(merged);
            });
        };
    };
    
    var json;
    
    var highlightObj = highlighter.highlightSelection(cssClass, currentSelection);
    
    if (highlightsInSelection.length > 0) {
        var s = highlighter.serialize();
        console.log('SER: ' + s);
        
        var arrSerialized = s.split('|');
        // clone array
        var serializedData = arrSerialized.slice(0);
        var lastId = getIndexOfLastHighlight(serializedData);
        var serializedText = [arrSerialized[0], arrSerialized[lastId + 1]].join('|');
        
        highlighter.unhighlightSelection(currentSelection);
        
        highlighter.deserialize(serializedText);
        each(highlighter.highlights, function(selected) {
             json = getJSONForHighlight(selected);
        });
    } else {
        if (highlightObj) {
            var highlight = highlightObj[0];
            json = getJSONForHighlight(highlight);
        };
    }
    
    return JSON.stringify({"result": json, "mergedData": mergedSelections});
}

function getHighlightsInSelection () {
    var highlights = highlighter.getHighlightsInSelection();
    return highlights;
}

function getIndexOfLastHighlight (data) {
    var notes = [];
    var idx;
    var origData = data;
    
    data.shift();
    
    each(data, function(selected) {
         var splits = selected.split('$');
         var highlightId = splits[2];
         notes.push(highlightId);
    });
    
    var highest = Math.max.apply(Math, notes);
    var i = 0;
    each(notes, function(note) {
         if (note == highest) {
            index = i;
         };
         i += 1;
    });
    
    return index
}

function findIndexOfGreatest(array) {
    var greatest;
    var indexOfGreatest;
    for (var i = 0; i < array.length; i++) {
        if (!greatest || array[i] > greatest) {
            greatest = array[i];
            indexOfGreatest = i;
        }
    }
    return indexOfGreatest;
}

//function vibeHighlightSelected(cssClass) {
//    var highlightObj = highlighter.highlightSelection(cssClass);
//    var serializeValue, startRange, endRange;
//    var textValue, highlightId;
//    
//    if (highlightObj) {
//        var highlight = highlightObj[0];
//        
//        var characterRange = highlight.characterRange;
//        var parts = [
//             characterRange.start,
//             characterRange.end,
//             highlight.id,
//             highlight.classApplier.cssClass,
//             highlight.containerElementId
//        ];
//        serializeValue = parts.join("$");
//        textValue = highlight.getText();
//        highlightId = highlight.id;
//        startRange = characterRange.start;
//        endRange = characterRange.end;
//    };
//    return JSON.stringify({"id": highlightId, "textValue":textValue, "startRange": startRange, "endRange": endRange, "cssClass": cssClass, "serializedValue": serializeValue});
//}

function vibeGetHighlights () {
    var h = getHighlightsInSelection();
    var highlights = 0;
    var notesCount = 0;
    var cssClasses = ['vibeHighlightRed', 'vibeHighlightGreen', 'vibeHighlightBlue', 'vibeHighlightYellow'];
    var cssNotes = ['vibeHighlightNote'];
    var noteId = 0;
    
    if (h.length > 0) {
        each(h, function(selected) {
             var css = selected.classApplier.cssClass;
             
             var cssIndex = indexInArray(css, cssClasses);
             if (cssIndex > -1) highlights += 1;
        });
        each(h, function(selected) {
             var css = selected.classApplier.cssClass;
             var cssNoteIndex = indexInArray(css, cssNotes);
             if (cssNoteIndex > -1) {
                notesCount += 1;
                noteId = selected.id;
             }
        });
    };
    
    return JSON.stringify({"highlightCount": highlights, "noteCount": notesCount, "noteId": noteId});
}

function indexInArray (str, strArray) {
    for (var j=0; j<strArray.length; j++) {
        if (strArray[j].match(str)) return j;
    }
    return -1;
}

function getHtmlTextFromSelection () {
    var obj = rangy.getSelection();
    var html = obj.focusNode.parentNode.innerHTML;
    console.log('HTML: ' + html);
    return html;
}

function highlightSelectedText() {
    //var highlighted = rangy.getSelection();
    highlighter.highlightSelection("vibeHighlight");
    var text = highlighter.serialize();
    console.log('highlightSelectedText(): ' + text);
    return text;
}

function scrollIntoView(eleID) {
    var e = document.getElementById(eleID);
    console.log('scrollIntoView: ' + e);
    
    if (!!e && e.scrollIntoView) {
        e.scrollIntoView();
    }
}

function findPos(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        do {
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return [curtop];
    }
}

function vibeFindElementPosition(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        do {
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return [curtop];
    }
}

function vibeFindOffset (pos, baseOffset) {
    var y = 0;
    
    for(var i = 0; i < baseOffset; i++) {
        var x = i * baseOffset;
        console.log('X: ' + x);

        if(pos <= x) {
            console.log('EQ: ' + pos + ' <= ' + y);
            return y;
        }
        y = x;
    }
    
    return y;
}

function vibeFindPageIndex(pos, baseOffset) {
    var y = 0;
    
    for(var i = 0; i < baseOffset; i++) {
        var x = i * baseOffset;
        
        if(pos <= x) {
            return y;
        }
        y = i;
    }
    return y;
}

function vibeFindPageTest(elem, baseOffset) {
    var e = document.getElementsByClassName(elem);
    console.log('vibeScrollCount:' + e.length);
    var pos = findPos(e[0]);
    //console.log('pos: ' + pos);
    var pageIndex = vibeFindPageIndex(pos - baseOffset, baseOffset);
    console.log('vibeFindPage:' + pageIndex);
    //console.log('e:#' + e[3] + '#vibeScroll: ' + elem + ' pos: ' + pos);
    //window.scrollTo(offset, 0);
    return pageIndex;
}

function vibeFindPage(startRange, endRange, baseOffset) {
    var h = highlighter.highlights;
    var foundHighlight, foundElem;
    
    for(var i = 0; i < h.length; i++) {
        var highlight = h[i];
        
        if(highlight.characterRange.start == startRange && highlight.characterRange.end == endRange) {
            foundHighlight = highlight;
            break;
        }
    }
    
    if(foundHighlight) {
        var elems = foundHighlight.getHighlightElements();
        // elems may contain 1 or more result, just get the first item in the array.
        foundElem = elems[0];
        
        //console.log('textContent:' + foundElem.textContent);
        var pos = vibeFindElementPosition(foundElem);
        //var rect = foundElem.getBoundingClientRect();

        //var textRect = "{{" + rect.left + "," + rect.top + "}, {" + rect.width + "," + rect.height + "}}";
        console.log('vibePos:' + pos);
        var pageIndex = vibeFindPageIndex(pos - baseOffset, baseOffset);
        return pageIndex;
    }
}

function getRectForSelectedText() {
    var selection = rangy.getSelection();
    var range = selection.getRangeAt(0);
    var rect = range.getBoundingClientRect();
    return "{{" + rect.left + "," + rect.top + "}, {" + rect.width + "," + rect.height + "}}";
}

function notifyApp(type, id) {
    window.location = 'notify://data/' + type + '/' + id;
}

function notifyNote(id, left, top, width, height) {
    window.location = 'notify://data/note/' + id + '/' + left + '/' + top + '/' + width + '/' + height;
}
