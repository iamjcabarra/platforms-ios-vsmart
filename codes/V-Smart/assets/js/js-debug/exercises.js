// global variable for matching type reset
$q = null;

$(document).ready(function() {
                  
                  $('.exercise').each(function(){
                                      $(this).find('.check').attr('eid',  $(this).attr('id')  );
                                      });
                  
                  // CHOOSE FROM THE BOX
                  // check if answer span exists
                  
                  $('.ex-choose-q li').click(function() {
                                             // remove focus on previously clicked element
                                             $q = $(this);
                                             $('.select').removeClass('select');
                                             $q.addClass('select');
                                             
                                             $('.ex-choose-a li').click(function() {
                                                                        if ( $q != null ) {
                                                                        $a = $(this);
                                                                        $('.ex-choose-a .select').removeClass('select');
                                                                        $a.addClass('select');
                                                                        $q.children('.answer').text( $a.text().replace(/(<([^>]+)>)/ig,"") ); // strip HTML tags
                                                                        notifyClient();
                                                                        }
                                                                        });
                                             });
                  
                  // CHOOSE FROM THE BOX	//TWO ANSWERS
                  
                  // for questions with two answers
                  $('.x2 > li').addClass('q');
                  $('.q > ol').addClass('ex-choose-a');
                  $('.q > ol > li').addClass('a');
                  
                  // check if answer span exists
                  $('.ex-choose-q li.q .answer').click(function() {
                                                       // remove focus on previously clicked element
                                                       $blank = $(this);
                                                       $q = $(this).parent('.q');
                                                       $('.select').removeClass('select');
                                                       $q.addClass('select');
                                                       notifyClient();
                                                       
                                                       $q.find('.a').click(function() {
                                                                           $a = $(this);
                                                                           
                                                                           if ( $q != null ) {
                                                                           $q.removeClass('select');
                                                                           $a.addClass('select');
                                                                           notifyClient();
                                                                           $blank.text( $a.text().replace(/(<([^>]+)>)/ig,"") ); // strip HTML tags
                                                                           }
                                                                           });
                                                       });
                  
                  // MATCHING TYPE
                  
                  $('.ex-match-q li').click(function() {
                                            
                                            // remove focus on previously clicked element
                                            $q = $(this);
                                            $('.select').removeClass('select');
                                            $q.addClass('select');
                                            notifyClient();
                                            
                                            $('.ex-match-a li').click(function() {
                                                                      $a = $(this);
                                                                      $('.ex-match-a .select').removeClass('select');
                                                                      $a.addClass('select');
                                                                      if ( $q != null ) {
                                                                      // * 1 to int
                                                                      // +65 to ASCII number
                                                                      // fromCharchode to literal literal
                                                                      $q.children('.answer').text( String.fromCharCode( ($a.index()*1+65) ) );
                                                                      }
                                                                      notifyClient();
                                                                      });
                                            });
                  
                  
                  
                  
                  // TRUE OR FALSE OR TWO CHOICES
                  
                  var choice1 =  $('ol.ex-tf').attr('choice1');
                  var choice2 = $('ol.ex-tf').attr('choice2');
                  var	choice1in;
                  var choice2in;
                  
                  if(choice1==undefined || choice2==undefined ){
                  choice1='T';
                  choice2='F';
                  }
                  
                  $('.ex-tf > li').each(function() {
                                        //set choice 1 and 2 if each question has specific answer choices
                                        choice1in = $(this).attr('choice1');
                                        choice2in = $(this).attr('choice2');
                                        
                                        if(choice1in!=undefined) {choice1=choice1in;}
                                        if(choice2in!=undefined) {choice2=choice2in;}
                                        
                                        if(($(this).parent().first().hasClass('choicex2')) ){
                                        $(this).find('span.answer').first().prepend('<span class="answer-t">' + choice1 + '</span> <span class="answer-f">' + choice2 + '</span>')
                                        }
                                        else {
                                        $(this).prepend('<span class="answer"><span class="answer-t">' + choice1 + '</span> <span class="answer-f">' + choice2 + '</span></span>');
                                        }
                                        });
                  
                  $('.ex-tf li .answer span').bind('click', function(event) {
                                                   $q = $(this);
                                                   $q.toggleClass('select');
                                                   $q.siblings().removeClass('select');
                                                   });
                  
                  // MULTIPLE CHOICE
                  $('.ex-multi li').each(function() {
                                         //$(this).prepend('<span class="answer"></span>');
                                         });
                  $('.ex-multi li li').click(function() {
                                             $q = $(this);
                                             $q.parent().parent().find('blank').first().text( $q.text() );
                                             $q.toggleClass('select');
                                             $q.siblings().removeClass('select');
                                             notifyClient();
                                             });
                  
                  // WORD JUMBLE
                  $('.ex-jumble li').each(function() {
                                          $(this).append('<input type="text" class="answer" />');
                                          });
                  
                  // CHECKING BUTTONS
                  $('.check').click(function() {
                                    $score = 0;
                                    $choosefromthebox = $(this).parent('.buttons').siblings('.ex-choose-q').first();
                                    $matchingtype = $(this).parent('.buttons').siblings('.ex-match-q').first();
                                    $trueorfalse = $(this).parent('.buttons').siblings('.ex-tf').first();
                                    $multiplechoice = $(this).parent('.buttons').siblings('.ex-multi').first();
                                    $wordjumble = $(this).parent('.buttons').siblings('.ex-jumble').first();
                                    $twoanswers = $(this).parent('.buttons').siblings('.x2').first();
                                    
                                    if ( $twoanswers.length > 0 ) {
                                    $totalquestions = 0;
                                    $twoanswers.siblings().children('.q').removeClass('select');	
                                    $twoanswers.children('.q').each(function(i) {
                                                                    $q = $(this);
                                                                    
                                                                    $q.children('.answer').each( function(i){
                                                                                                $(this).removeClass('select correct incorrect');
                                                                                                $totalquestions++;
                                                                                                
                                                                                                if ( $(this).text() != ""  && $(this).text().trim().toUpperCase() == $(this).attr('data-key').toUpperCase() ) {
                                                                                                $score++;
                                                                                                
                                                                                                $(this).addClass('correct');
                                                                                                } else {
                                                                                                $(this).addClass('incorrect');
                                                                                                }
                                                                                                
                                                                                                } );
                                                                    
                                                                    });
                                    unselect($twoanswers.siblings('.check'));
                                    //alert('Your score is ' + $score + ' out of ' + $totalquestions);
                                    //window.location = 'vibereader://message/' + 'Your score is ' + $score + ' out of ' + $totalquestions;
                                    //alert('Interactive Exercise ' + $(this).attr('eid').match(/\d+/) + "\nScore: " + $score + '/' + $totalquestions);
                                    var exerNum = $(this).attr('eid').match(/\d+/);
                                    window.location = 'vibereader://message/' + exerNum + ',' + $score + ',' + $totalquestions;
                                    }	
                                    
                                    if ( $choosefromthebox.length > 0 ) {
                                    $choosefromthebox.children('li').removeClass('select correct incorrect');
                                    $choosefromthebox.siblings().children('li').removeClass('select');	
                                    $choosefromthebox.children('li').each(function(i) {
                                                                          if ( $(this).children('.answer').text().toUpperCase() == $(this).attr('data-key').toUpperCase() ) {
                                                                          $score++;
                                                                          $(this).addClass('correct');
                                                                          } else {
                                                                          $(this).addClass('incorrect');
                                                                          }
                                                                          });
                                    score($score, $choosefromthebox);
                                    }
                                    
                                    if ( $matchingtype.length > 0 ) {
                                    $matchingtype.children('li').removeClass('select correct incorrect');	
                                    $matchingtype.siblings().children('li').removeClass('select');	
                                    $matchingtype.children('li').each(function(i) {
                                                                      if ( $(this).children('.answer').text().toUpperCase() == $(this).attr('data-key').toUpperCase() ) {
                                                                      $score++;
                                                                      $(this).addClass('correct');
                                                                      } else {
                                                                      $(this).addClass('incorrect');
                                                                      }
                                                                      });		
                                    score($score, $matchingtype);
                                    $q = null;
                                    }
                                    if ( $trueorfalse.length > 0 ) {
                                    $trueorfalse.children('li').removeClass('select correct incorrect');
                                    $trueorfalse.children('li').each(function(i) {
                                                                     // uppercased to be sure
                                                                     if ( $(this).find('.select').text().toUpperCase() == $(this).attr('data-key').toUpperCase() ) {
                                                                     $score++;
                                                                     $(this).addClass('correct');
                                                                     } else {
                                                                     $(this).addClass('incorrect');
                                                                     }
                                                                     });
                                    score($score, $trueorfalse);
                                    }
                                    if ( $multiplechoice.length > 0 ) {
                                    $multiplechoice.children('li').removeClass('select correct incorrect');
                                    $multiplechoice.children('li').each(function(i){				
                                                                        // get numerical value of letter and the difference from value of 'A' (65)
                                                                        // indices: 0 1 2 3 [a b c d] etc.
                                                                        // alphabet: 65 66 67 68 [A B C D] etc.
                                                                        if ( $(this).find('.select').index() == ( $(this).attr('data-key').toUpperCase().charCodeAt(0) - 65 ) ) {
                                                                        $score++;
                                                                        $(this).addClass('correct');
                                                                        } else {
                                                                        $(this).addClass('incorrect');
                                                                        }
                                                                        });
                                    score($score, $multiplechoice);
                                    }
                                    if ( $wordjumble.length > 0 ) {
                                    $wordjumble.children('li').removeClass('select correct incorrect');
                                    $wordjumble.children('li').each(function() {
                                                                    // uppercased and trimmed to be sure
                                                                    if ( $.trim( $(this).find('input').val().toUpperCase() ) == $(this).attr('data-key').toUpperCase() ) {
                                                                    $score++;
                                                                    $(this).addClass('correct');
                                                                    } else {
                                                                    $(this).addClass('incorrect');
                                                                    }
                                                                    });
                                    score($score, $wordjumble);
                                    }
                                    
                                    
                                    
                                    //reset();
                                    });
                  $('.reset').click(function() {
                                    reset($(this));
                                    });
                  });

function notifyClient() {
    window.location = 'vibereader://message/exercise';
}

function score($score, $exercise) {
	unselect($exercise.siblings('.check'));
	//alert('Your score is ' + $score + ' out of ' + $exercise.children('li').length);
    //var msg = 'Your score is ' + $score + ' out of ' + $exercise.children('li').length;
	//window.location = 'vibereader://message/' + msg;
    var exerNum = $exercise.parent().find('.check').first().attr('eid').match(/\d+/);
    window.location = 'vibereader://message/' + exerNum + ',' + $score + ',' + $exercise.children('li').length;
    //alert('Interactive Exercise '  + $exercise.parent().find('.check').first().attr('eid').match(/\d+/) + "\nScore: " + $score + '/' + $exercise.children('li').length);
}

function unselect($button) {
	$button.parent('.buttons').siblings('.ex-choose-q, .ex-choose-a, .ex-match-q, .ex-match-a, .ex-tf, .ex-multi').find('.select').removeClass('select');
}

function reset($button) {
	unselect($button);
	$button.parent('.buttons').siblings('ol').children('li').removeClass('correct incorrect');
	$button.parent('.buttons').siblings('.ex-match-q').find('.answer').html('&nbsp;');
	$button.parent('.buttons').siblings('.ex-choose-q').find('.answer').html('&nbsp;');	
	$button.parent('.buttons').siblings('.ex-jumble').find('input').val('');
	$q = null;
	$a = null;
}
