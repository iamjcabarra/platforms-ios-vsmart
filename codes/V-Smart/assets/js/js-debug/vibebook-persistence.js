var _jsonCache;
var _jsonResult;

var exer_type = {
	Choose_The_Correct_Word : 0,
	Fill_in_the_blanks : 1,
	SelectTheCorrectAnswer : 2,
	Multiple_Choice : 3,
	Select_ActionWords : 4,
	True_or_False : 5,
	Multiple_choice_no_pic : 6,
	Matching_type : 7
};

$(document).ready(function() {

	$('.reset.button').click(function(e) {
		var button_parent = $(e.target).parent();
		var exercise_parent = $(button_parent).parent();

		console.log("[Exer_ID]" + $(exercise_parent).attr('id'));
        var exerId = $(exercise_parent).attr('id');
        notifyClientApp(exerId);
	});

	$('.check.button').click(function(e) {
		var button_parent = $(e.target).parent();
		var exercise_parent = $(button_parent).parent();

		$(exercise_parent).children().each(function(index, element) {
			// console.log(element);
			if ($(element).hasClass("ex-choose-q x2")) {//Choose the correct word
				parseChooseTheCorrectWord(exercise_parent);
			} else if ($(element).hasClass("ex-jumble")) {// Fill in the blanks
				parseFillInTheBlank(exercise_parent);
			} else if ($(element).hasClass("ex-multi pic")) {// Multiple Choice
				parseMultipleChoice(exercise_parent);
			} else if ($(element).hasClass("ex-choose-q")) {// Select the correct answer
				parseSelectTheCorrectAnswer(exercise_parent);
			} else if ($(element).hasClass("ex-tf choicex2 inline")) {// Select the correct answer
				parseActionWords(exercise_parent);
			} else if ($(element).hasClass("ex-tf")) {// Select the correct answer
				parseTrueOrFalse(exercise_parent);
			} else if ($(element).hasClass("ex-multi")) {// Multiple Choice
				parseMultipleChoiceWithoutPIC(exercise_parent);
			} else if ($(element).hasClass("ex-match-q")) {// Multiple Choice
				parseMatchingType(exercise_parent);
			}
            //notifyApp(1);
		});
	});
});

function parseChooseTheCorrectWord(exercise_parent) {
	console.log("Choose The Correct Word", exercise_parent);
	var child = $(exercise_parent).find(".ex-choose-q.x2");
	console.log(child);
    var exerId;
    
	var jsonArray = [];
	$(child).children().each(function(index, element) {
		if ($(element).attr('class') == "q" || $(element).attr('class') == "q select") {
			var temp_answer = $(element).find('span');
			var answer = "";
			var answer_status = "";
			for (var i = 0; i < $(temp_answer).length; i++) {
				answer += $(temp_answer[i]).text() + "|";
				answer_status += $(temp_answer[i]).attr('class') + "|";
			}
            
            exerId = $(exercise_parent).attr('id');

			// console.log(answer);
			jsonArray.push({
				item_no : index,
				exer_type : exer_type.Choose_The_Correct_Word,
				exer_id : $(exercise_parent).attr('id'),
				correct_answer : "",
				actual_answer : $.trim(answer.substring(0, answer.length - 1)),
				answer_status : answer_status.substring(0, answer_status.length - 1)
			});
		}
	});
	//console.log(jsonArray);
	//console.log("[JSON-ARRAY]" + JSON.stringify(jsonArray));
    _jsonResult = JSON.stringify({"exer_id": exerId, "result": jsonArray});
};

function parseFillInTheBlank(exercise_parent) {
	console.log("Fill in the blanks", exercise_parent);
	var child = $(exercise_parent).find(".ex-jumble");
	var jsonArray = [];
    var exerId;
	$(child).children().each(function(index, element) {
		if ($(element).attr('data-key') != undefined) {
            exerId = $(exercise_parent).attr('id');
			
            jsonArray.push({
				item_no : index,
				exer_type : exer_type.Fill_in_the_blanks,
				exer_id : $(exercise_parent).attr('id'),
				correct_answer : $(element).attr('data-key'),
				actual_answer : $(element).find('.answer').val(),
				answer_status : $(element).attr('class')
			});
		}
	});
	console.log(jsonArray);
	console.log("[JSON-ARRAY]" + JSON.stringify(jsonArray));
    _jsonResult = JSON.stringify({"exer_id": exerId, "result": jsonArray});
};

function parseSelectTheCorrectAnswer(exercise_parent) {
	console.log("SelectTheCorrectAnswer", exercise_parent);
	var child = $(exercise_parent).find(".ex-choose-q");
	// != undefined ? $(exercise_parent).find(".ex-choose-q") : $(element).find(".ex-choose-q.x2");
	console.log(child);
	var jsonArray = [];
    var exerId;
	$(child).children().each(function(index, element) {
		if ($(element).attr('data-key') != undefined) {
            exerId = $(exercise_parent).attr('id');
			jsonArray.push({
				item_no : index,
				exer_type : exer_type.SelectTheCorrectAnswer,
				exer_id : $(exercise_parent).attr('id'),
				correct_answer : $(element).attr('data-key'),
				actual_answer : $.trim($(element).find('.answer').text()),
				answer_status : $(element).attr('class')
			});
		}
	});
	console.log(jsonArray);
	console.log("[JSON-ARRAY]" + JSON.stringify(jsonArray));
    _jsonResult = JSON.stringify({"exer_id": exerId, "result": jsonArray});
};

function parseMultipleChoiceWithoutPIC(exercise_parent) {
	console.log("Multiple Choice", exercise_parent);
	var child = $(exercise_parent).find(".ex-multi > li");
	var jsonArray = [];
    var exerId;
	$(child).each(function(index, element) {
		if ($(element).attr('data-key') != undefined) {
            exerId = $(exercise_parent).attr('id');
			jsonArray.push({
				item_no : index,
				exer_type : exer_type.Multiple_choice_no_pic,
				exer_id : $(exercise_parent).attr('id'),
				correct_answer : $(element).attr('data-key'),
				actual_answer : $.trim($(element).find('.select').text()),
				answer_status : $(element).attr('class')
			});
		}
	});
	console.log(jsonArray);
	console.log("[JSON-ARRAY]" + JSON.stringify(jsonArray));
    _jsonResult = JSON.stringify({"exer_id": exerId, "result": jsonArray});
};

function parseMultipleChoice(exercise_parent) {
	console.log("Multiple Choice", exercise_parent);
	var child = $(exercise_parent).find(".ex-multi.pic > li");
	var jsonArray = [];
    var exerId;
	$(child).each(function(index, element) {
		if ($(element).attr('data-key') != undefined) {
            exerId = $(exercise_parent).attr('id');
			jsonArray.push({
				item_no : index,
				exer_type : exer_type.Multiple_Choice,
				exer_id : $(exercise_parent).attr('id'),
				correct_answer : $(element).attr('data-key'),
				actual_answer : $.trim($(element).find('.select').text()),
				answer_status : $(element).attr('class')
			});
		}
	});
	console.log(jsonArray);
	console.log("[JSON-ARRAY]" + JSON.stringify(jsonArray));
    _jsonResult = JSON.stringify({"exer_id": exerId, "result": jsonArray});
};

function parseActionWords(exercise_parent) {
	console.log("Select Action Words", exercise_parent);
	var child = $(exercise_parent).find(".ex-tf.choicex2.inline");
	var jsonArray = [];
    var exerId;
	$(child).children().each(function(index, element) {
		if ($(element).attr('data-key') != undefined) {
			var _answer;
			var selected_answer = $(element).find('.answer');
			$(selected_answer).children().each(function(ind, elem) {
				var select = $(elem).attr('class');
				var contains = select.indexOf('select');
				if (contains >= 0) {
					_answer = $(elem).text();
				}
			});
            exerId = $(exercise_parent).attr('id');
			jsonArray.push({
				item_no : index,
				exer_type : exer_type.Select_ActionWords,
				exer_id : $(exercise_parent).attr('id'),
				correct_answer : $(element).attr('data-key'),
				actual_answer : _answer,
				answer_status : $(element).attr('class')
			});
		}
	});
	console.log(jsonArray);
	console.log("[JSON-ARRAY]" + JSON.stringify(jsonArray));
    _jsonResult = JSON.stringify({"exer_id": exerId, "result": jsonArray});
};

function parseTrueOrFalse(exercise_parent) {
	console.log("True or False", exercise_parent);
	var child = $(exercise_parent).find(".ex-tf > li");
	var jsonArray = [];
    var exerId;
	$(child).each(function(index, element) {
		if ($(element).attr('data-key') != undefined) {
			var _answer = "";
			var selected_answer = $(element).find('.answer');
			$(selected_answer).children().each(function(ind, elem) {
				var select = $(elem).attr('class');
				var contains = select.indexOf('select');
				if (contains >= 0) {
					_answer = $(elem).text();
				}
			});
            exerId = $(exercise_parent).attr('id');
			jsonArray.push({
				item_no : index,
				exer_type : exer_type.True_or_False,
				exer_id : $(exercise_parent).attr('id'),
				correct_answer : $(element).attr('data-key'),
				actual_answer : _answer,
				answer_status : $(element).attr('class')
			});
		}
	});
	console.log(jsonArray);
	console.log("[JSON-ARRAY]" + JSON.stringify(jsonArray));
    _jsonResult = JSON.stringify({"exer_id": exerId, "result": jsonArray});
};

function parseMatchingType(exercise_parent) {
	console.log("Matching Type", exercise_parent);
	var child = $(exercise_parent).find(".ex-match-q > li");
	var jsonArray = [];
    var exerId;
	$(child).each(function(index, element) {
		if ($(element).attr('data-key') != undefined) {
            exerId = $(exercise_parent).attr('id');
			jsonArray.push({
				item_no : index,
				exer_type : exer_type.Matching_type,
				exer_id : $(exercise_parent).attr('id'),
				correct_answer : $(element).attr('data-key'),
				actual_answer : $(element).find('.answer').text(),
				answer_status : $(element).attr('class')
			});
		}
	});
	console.log(jsonArray);
	console.log("[JSON-ARRAY]" + JSON.stringify(jsonArray));
    _jsonResult = JSON.stringify({"exer_id": exerId, "result": jsonArray});
}

function vibeGetBookAnswers () {
    return _jsonResult;
}

function notifyClientApp(id) {
    window.location = 'notify://data/answer/' + id;
}

/********method called by the client/Platforms********************/
function setJsonAnswer(answers) {
	_jsonCache = answers;
	setTimeout(function() {
		loadAnswer();
	}, 250);
}

/*****************************************************************/
function loadAnswer() {
	try {
		var jsonAnswer = JSON.parse(_jsonCache);

		for (var i = 0; i < jsonAnswer.length; i++) {
			// var _index = jsonAnswer[i].itemNo;
			// var index = parseInt(_index);
			//
			// var title = jsonAnswer[i].bookTitle;
			// var type = jsonAnswer[i].exerType;
			// var exer_id = jsonAnswer[i].exerId;
			// var data_key = jsonAnswer[i].correctAnswer;
			// var answer_status = jsonAnswer[i].answerStatus;
			// var exer_answer = jsonAnswer[i].actualAnswer;

			var _index = jsonAnswer[i].item_no;
			var index = parseInt(_index);

			var title = jsonAnswer[i].book_title;
			var type = jsonAnswer[i].exer_type;
			var exer_id = jsonAnswer[i].exer_id;
			var data_key = jsonAnswer[i].correct_answer;
			var answer_status = jsonAnswer[i].answer_status;
			var exer_answer = jsonAnswer[i].actual_answer;

			if ($('body').find('#' + exer_id) != undefined || $('body').find('#' + exer_id) != null) {
				var exerid = $('body').find('#' + exer_id);
				if (type == exer_type.SelectTheCorrectAnswer) {
					var ques_list = $(exerid).find('.ex-choose-q >li');

					$(ques_list[index]).attr('class', answer_status);
					$(ques_list[index]).find('.answer').text(exer_answer);
				} else if (type == exer_type.Select_ActionWords) {
					var ques_list = $(exerid).find('.ex-tf.choicex2.inline > li');

					$(ques_list[index]).attr('class', answer_status);
					var answer = $(ques_list[index]).find('.answer');
					$(answer).children().each(function(index, element) {
						var answer_text = $(element).text();
						if (answer_text == exer_answer) {
							var att = $(element).attr('class');
							$(element).attr('class', att + ' select');
						}
					});
				} else if (type == exer_type.Choose_The_Correct_Word) {
					var ques_list = $(exerid).find('.ex-choose-q.x2 > li');
					var answer = exer_answer.split("|");
					var status = answer_status.split("|");
					if (answer.length > 1) {
						var list = $(ques_list[index]).find('span');
						// console.log("ANSWERLIST COUNT: ", answer_list);
						$(list).each(function(ind, elem) {
							$(elem).text(answer[ind]);
							$(elem).attr('class', status[ind]);
						});
					} else {
						var elem = $(ques_list[index]).find('span');
						$(elem).text(answer[0]);
						$(elem).attr('class', status[0]);
					}
				} else if (type == exer_type.True_or_False) {
					var ques_list = $(exerid).find('.ex-tf > li');
					var answer = $(ques_list[index]).find('span.answer');
					$(ques_list[index]).attr('class', answer_status);
					$(answer).children().each(function(ind, elem) {
						var curr = $(elem).text();
						console.log(ind);
						if (curr == exer_answer) {
							var att = $(elem).attr('class');
							$(elem).attr('class', att + ' select');
						}
					});
				} else if (type == exer_type.Multiple_Choice) {
					var ques_list = $(exerid).find('.ex-multi.pic > li');
					var answer = $(ques_list[index]);
					$(answer).attr('class', answer_status);
					$(answer).each(function(ind, elem) {
						var ol = $(elem).find('ol');
						$(ol).children().each(function(i, el) {
							var curr = $(el).text();
							if (curr == exer_answer) {
								$(el).addClass('select');
							}
						});
					});
				} else if (type == exer_type.Fill_in_the_blanks) {
					var ques_list = $(exerid).find('.ex-jumble > li');
					var answer = $(ques_list[index]);
					$(answer).attr('class', answer_status);
					$(answer).find('.answer').val(exer_answer);
				} else if (type == exer_type.Multiple_choice_no_pic) {
					var ques_list = $(exerid).find('.ex-multi > li');
					var answer = $(ques_list[index]);
					$(answer).attr('class', answer_status);
					$(answer).each(function(ind, elem) {
						var ol = $(elem).find('ol');
						$(ol).children().each(function(i, el) {
							var curr = $(el).text();
							if (curr == exer_answer) {
								$(el).addClass('select');
							}
						});
					});
				} else if (type == exer_type.Matching_type) {
					var ques_list = $(exerid).find('.ex-match-q > li');
					var answer = $(ques_list[index]);
					$(answer).attr('class', answer_status);
					$(answer).each(function(ind, elem) {
						var answer_field = $(elem).find('.answer');
						$(answer_field).text(exer_answer);
					});
				}
			} else {
				continue;
			}
		}
	} catch (e) {
		console.log(e);
	}
}
