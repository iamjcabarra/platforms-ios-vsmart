/**
* vibehelpers-rangy.js
*
* @author Earljon Hidalgo
* @date March 2014
* @desc Webview Highlight Helpers
* @copyright Vibe Technologies 2014
*/

console = new Object();
console.log = function(log) {
    var iframe = document.createElement("IFRAME");
    iframe.setAttribute("src", "ios-log:#iOS#" + log);
    document.documentElement.appendChild(iframe);
    iframe.parentNode.removeChild(iframe);
    iframe = null;
}
console.debug = console.log;
console.info = console.log;
console.warn = console.log;
console.error = console.log;

var highlighter;
var serializedHighlights;

var selectedRange;

/*
  REQUIRED CSS CLASSES:

  .vibeHighlightYellow {
      background-color: #fdfeab;
  }
  .vibeHighlightRed {
      background-color: #ffb1ad;
  }
  .vibeHighlightGreen {
      background-color: #a1ffa2;
  }
  .vibeHighlightBlue {
      background-color: #3fcfff;
  } 
  .vibeHighlightNote {
      border-bottom:: 2px solid #FE2E64;
  }   
*/

$(document).ready(function() {
  rangy.init();

  highlighter = rangy.createHighlighter();

  highlighter.addClassApplier(rangy.createCssClassApplier("vibeHighlightYellow", {
                                                          ignoreWhiteSpace : true,
                                                          tagNames : ["span", "a"],
                                                          elementProperties : {
                                                          onclick : function() {
                                                                var highlight = highlighter.getHighlightForElement(this);
                                                                console.log('onclick: ' + highlight.id);
                                                                _notifyApp('color', highlight);
                                                            }
                                                          }
                                                          }));
  highlighter.addClassApplier(rangy.createCssClassApplier("vibeHighlightRed", {
                                                          ignoreWhiteSpace : true,
                                                          tagNames : ["span", "a"],
                                                          elementProperties : {
                                                          onclick : function() {
                                                                var highlight = highlighter.getHighlightForElement(this);
                                                                _notifyApp('color', highlight);
                                                            }
                                                          }
                                                          }));

  highlighter.addClassApplier(rangy.createCssClassApplier("vibeHighlightGreen", {
                                                          ignoreWhiteSpace : true,
                                                          tagNames : ["span", "a"],
                                                          elementProperties : {
                                                          onclick : function() {
                                                                var highlight = highlighter.getHighlightForElement(this);
                                                                _notifyApp('color', highlight);
                                                            }
                                                          }
                                                          }));

  highlighter.addClassApplier(rangy.createCssClassApplier("vibeHighlightBlue", {
                                                          ignoreWhiteSpace : true,
                                                          tagNames : ["span", "a"],
                                                          elementProperties : {
                                                          onclick : function() {
                                                                var highlight = highlighter.getHighlightForElement(this);
                                                                _notifyApp('color', highlight);
                                                            }
                                                          }
                                                          }));

  highlighter.addClassApplier(rangy.createCssClassApplier("vibeHighlightNote", {
                                                          ignoreWhiteSpace : true,
                                                          tagNames : ["span", "a"],
                                                          elementProperties : {
                                                          onclick : function() {
                                                                var highlight = highlighter.getHighlightForElement(this);
                                                                var rect = this.getBoundingClientRect();                                                          
                                                                _notifyNote(highlight, rect);
                                                            }
                                                          }
                                                          }));
                  });

var each = [].each ?
    function(arr, func) {
        arr.forEach(func);
    } :
    function(arr, func) {
        for (var i = 0, len = arr.length; i < len; ++i) {
            func( arr[i] );
    }
};

/* Public Functions */

/**
 * This method will revert the changes made to the currently highlighted text. This is
 * used in a scenario wherein the user cancelled the note creation.
 * @param {number} noteId
 * @param {string} serializedText
 * @example
 * // returns void
 * vibeRevertSelection(5, 'type:textContent|468$475$6$vibeHighlightNote$');
 * @summary Revert highlighted text
 */
function vibeRevertSelection(noteId, serializedText) {
    highlighter.deserialize(serializedText);
    var currentHighlight = [];
    each(highlighter.highlights, function(selected) {
         if(selected.id == noteId) {
            currentHighlight.push(selected);
         }
    });
    
    highlighter.removeHighlights(currentHighlight);
}

/**
 * This method will remove all highlights currently loaded on the page
 * @param {number} startRange
 * @param {number} endRange
 * @param {number} baseOffset
 * @example
 * // returns void
 * vibeRemoveAllHighlights();
 * @summary Remove all highlights
 */
function vibeRemoveAllHighlights() {
    highlighter.removeAllHighlights();
}

/**
 * This method will apply all saved rangy data from the database to the loaded page.
 * The saved data is row based (collections), you have to iterate and form a data stream
 * 
 * @param {string} storedData
 * @example
 * // returns void
 * vibeHighlightStoredData('type:textContent|28577$28646$5$vibeHighlightGreen$|30942$30993$10$vibeHighlightBlue$');
 * @summary Load all highlights stored in DB
 */
function vibeHighlightStoredData(storedData) {
    var serializedText = storedData;
    highlighter.deserialize(serializedText);
}

/**
 * This method will apply the cssClass to the selected text by the user.
 * The mergeData on the JSON result will be used to remove the saved IDs from the database. Merge data
 * occurs when the currently selected text contains one or more CSS classes of type 'vibeHighlight<type>'.
 * @param {string} cssClass
 * @example
 * // returns JSON formatted result with the highlighted data and merged data
 * vibeHighlightSelected('vibeHighlightRed');
 * JSON:
 {"result":"{\"id\":10,\"textValue\":\"that runs like a malignant fluid through your veins\",\"startRange\":30942,\"endRange\":30993,\"cssClass\":\"vibeHighlightBlue\",\"serializedValue\":\"30942$30993$10$vibeHighlightBlue$\"}","mergedData":["{\"id\":7,\"textValue\":\"through\",\"startRange\":30975,\"endRange\":30982,\"cssClass\":\"vibeHighlightYellow\",\"serializedValue\":\"30975$30982$7$vibeHighlightYellow$\"}","{\"id\":6,\"textValue\":\"malignant\",\"startRange\":30959,\"endRange\":30968,\"cssClass\":\"vibeHighlightGreen\",\"serializedValue\":\"30959$30968$6$vibeHighlightGreen$\"}"]}
 * @summary Highlight selected text
 */
function vibeHighlightSelected(cssClass) {
    var currentSelection = rangy.getSelection();
    var mergedSelections = [];
    
    selectedRange = currentSelection;
    
    // get current highlights for the current selection
    var highlightsInSelection = _getHighlightsInSelection();
    if (highlightsInSelection) {
        if (highlightsInSelection.length > 0) {
            each(highlightsInSelection, function(selected) {
                 var merged = _getJSONForHighlight(selected);
                 mergedSelections.push(merged);
            });
        };
    };
    
    var json;
    var highlightObj = highlighter.highlightSelection(cssClass, currentSelection);
    
    if (highlightsInSelection.length > 0) {
        var s = highlighter.serialize();
        
        var arrSerialized = s.split('|');
        var serializedData = arrSerialized.slice(0);
        var lastId = _getIndexOfLastHighlight(serializedData);
        var serializedText = [arrSerialized[0], arrSerialized[lastId + 1]].join('|');
        
        highlighter.unhighlightSelection(currentSelection);
        
        highlighter.deserialize(serializedText);
        each(highlighter.highlights, function(selected) {
             json = _getJSONForHighlight(selected);
        });
    } else {
        if (highlightObj) {
            var highlight = highlightObj[0];
            json = _getJSONForHighlight(highlight);
        };
    }
    
    return JSON.stringify({"result": json, "mergedData": mergedSelections});
}

/**
 * This method will try to give you total count on how many highlights and notes 
 * available on the currently loaded page. This is useful to determine what action should your
 * display to the client (UI) if the user selects a paragraph with highlights and notes on it.
 * Conditions:
 * If the noteId has a value of 1, pop the 'Edit Note' and apply the CSS for note with its new
 * selection.
 * If the noteId has a value greater than 1, pop the 'Add Note' and merge all notes into the newly
 * selected text.
 * If the noteId has a value of 0, pop the 'Change Color' action.
 * @example
 * // returns JSON formatted result
 * vibeGetHighlights();
 * JSON:
 * {"highlightCount":2,"noteCount":0,"noteId":0}
 * @summary Get highlights/notes statistics on a current page
 */
function vibeGetHighlights () {
    var h = _getHighlightsInSelection();
    var highlights = 0;
    var notesCount = 0;
    var cssClasses = ['vibeHighlightRed', 'vibeHighlightGreen', 'vibeHighlightBlue', 'vibeHighlightYellow'];
    var cssNotes = ['vibeHighlightNote'];
    var noteId = 0;
    
    if (h.length > 0) {
        each(h, function(selected) {
             var css = selected.classApplier.cssClass;
             
             var cssIndex = _indexInArray(css, cssClasses);
             if (cssIndex > -1) highlights += 1;
        });
        each(h, function(selected) {
             var css = selected.classApplier.cssClass;
             var cssNoteIndex = _indexInArray(css, cssNotes);
             if (cssNoteIndex > -1) {
                notesCount += 1;
                noteId = selected.id;
             }
        });
    };
    
    return JSON.stringify({"highlightCount": highlights, "noteCount": notesCount, "noteId": noteId});
}

/**
 * This method will try to search the location of the saved rangy data (note or highlight) and
 * gives you the possible index on where the highlight/note is found.
 * @note This is a little buggy as sometimes it finds the wrong page index. #FIXME
 * @param {number} startRange
 * @param {number} endRange
 * @param {number} baseOffset
 * @example
 * // returns 3
 * vibeFindPage(468, 475, 768);
 * @summary Find the page index of a certain highlight.
 */
function vibeFindPage(startRange, endRange, baseOffset) {
    var h = highlighter.highlights;
    var foundHighlight, foundElem;
    
    for(var i = 0; i < h.length; i++) {
        var highlight = h[i];
        
        if(highlight.characterRange.start == startRange && highlight.characterRange.end == endRange) {
            foundHighlight = highlight;
            break;
        }
    }
    
    if(foundHighlight) {
        var elems = foundHighlight.getHighlightElements();
        // elems may contain 1 or more result, just get the first item in the array.
        foundElem = elems[0];
        
        //var pos = _vibeFindElementPosition(foundElem);
        var rect = foundElem.getBoundingClientRect();
        var _offset = rect.left - (rect.left % baseOffset);
		var _pageNumber = _offset / baseOffset;
        //var pageIndex = _vibeFindPageIndex(pos - baseOffset, baseOffset);
        //return pageIndex;
        return _pageNumber;
    }
}

/* Private Functions */
function _getJSONForHighlight (highlight) {
    var json;
    var serializeValue, startRange, endRange;
    var textValue, highlightId, cssClass;
    
    var characterRange = highlight.characterRange;
    var parts = [
         characterRange.start,
         characterRange.end,
         highlight.id,
         highlight.classApplier.cssClass,
         highlight.containerElementId
    ];
    serializeValue = parts.join("$");
    textValue = highlight.getText();
    highlightId = highlight.id;
    startRange = characterRange.start;
    endRange = characterRange.end;
    cssClass = highlight.classApplier.cssClass;
    
    json = JSON.stringify({"id": highlightId, "textValue": textValue, "startRange": startRange, "endRange": endRange, "cssClass": cssClass, "serializedValue": serializeValue});
    return json;
}

function _getHighlightsInSelection () {
    var highlights = highlighter.getHighlightsInSelection();
    return highlights;
}

function _getIndexOfLastHighlight (data) {
    var notes = [];
    var idx;
    var origData = data;
    
    data.shift();
    
    each(data, function(selected) {
         var splits = selected.split('$');
         var highlightId = splits[2];
         notes.push(highlightId);
    });
    
    var highest = Math.max.apply(Math, notes);
    var i = 0;
    each(notes, function(note) {
         if (note == highest) {
            index = i;
         };
         i += 1;
    });
    
    return index
}

function _indexInArray (str, strArray) {
    for (var j=0; j<strArray.length; j++) {
        if (strArray[j].match(str)) return j;
    }
    return -1;
}

function _vibeFindElementPosition(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        do {
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return [curtop];
    }
}

function _vibeFindOffset (pos, baseOffset) {
    var y = 0;
    
    for(var i = 0; i < baseOffset; i++) {
        var x = i * baseOffset;

        if(pos <= x) {
            return y;
        }
        y = x;
    }
    
    return y;
}

function _vibeFindPageIndex(pos, baseOffset) {
    var y = 0;
    
    for(var i = 0; i < baseOffset; i++) {
        var x = i * baseOffset;
        
        if(pos <= x) {
            return y;
        }
        y = i;
    }
    return y;
}

function _notifyApp(type, highlight) {
    window.location = 'notify://data/' + type + '/' + highlight.id;
}

function _notifyNote(highlight, rect) {
    window.location = 'notify://data/note/' + highlight.id + '/' + rect.left + '/' + rect.top + '/' + rect.width + '/' + rect.height;
}
