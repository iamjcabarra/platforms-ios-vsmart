//
//  NTMConstants.swift
//  V-Smart
//
//  Created by Julius Abarra on 08/08/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import Foundation

struct NTMConstants {
    
    struct CellIdentifier {
        static let NOTEBOOK = "notebook_cell_identifier"
    }
    
    struct NotebookImage {
        static let GENERAL = UIImage(named: "icn_notebook_general")
        static let RED = UIImage(named: "icn_notebook_red")
        static let BLUE = UIImage(named: "icn_notebook_blue")
        static let YELLOW = UIImage(named: "icn_notebook_yellow")
        static let GREEN = UIImage(named: "icn_notebook_green")
        static let PURPLE = UIImage(named: "icn_notebook_purple")
        static let WHITE = UIImage(named: "icn_notebook_white")
        static let UNKNOWN = UIImage(named: "icn_notebook_general")
    }
}

protocol NTMRoutes: ParseProtocol, NetworkProtocol {
    func route(_ method: String, uri: String, body: [String: AnyObject]?) -> NSMutableURLRequest
    func route(_ method: String, uri: String, query: [String: AnyObject]?, body: [String: AnyObject]?) -> NSMutableURLRequest
}

extension NTMRoutes {
    func route(_ method: String, uri: String, body: [String: AnyObject]?) -> URLRequest {
        let endpoint = "vsmart-rest-dev" + "\(uri)"
        let url = buildURLFromRequestEndPoint(endpoint as NSString)
        let request = buildURLRequestWithMethod(method as NSString, url: url, andBody: body as AnyObject?)
        return request
    }
    
    func route(_ method: String, uri: String, query: [String: AnyObject]?, body: [String: AnyObject]?) -> URLRequest {
        let endpoint = "vsmart-rest-dev" + "\(uri)"
        var url = buildURLFromRequestEndPoint(endpoint as NSString)
        
        if let dictionary = query {
            var queryVariables = [URLQueryItem]()
            
            for (name, object) in dictionary {
                let value = object as! String
                let queryObject = URLQueryItem(name: name, value: value)
                queryVariables.append(queryObject)
            }
            
            var urlComponents = URLComponents(string: url.absoluteString)!
            urlComponents.queryItems = queryVariables
            url = urlComponents.url!
        }
        
        let request = buildURLRequestWithMethod(method as NSString, url: url, andBody: body as AnyObject?)
        return request
    }
}
