//
//  VSSandboxHelper.swift
//  V-Smart
//
//  Created by Julius Abarra on 28/07/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

import Foundation

class VSSandboxHelper: NSObject {
    
    fileprivate let fileManager = FileManager.default
    
    func makeSubdirectoryInDocumentsDirectory(_ name: String) -> String {
        let directoryPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectoryPath = directoryPaths[0]
        
        let subdirectoryName = name.hasPrefix("/") ? name : "/\(name)"
        let subdirectoryPath = (documentsDirectoryPath as NSString).appendingPathComponent(subdirectoryName)
        
        if (!self.fileManager.fileExists(atPath: subdirectoryPath)) {
            do {
                try self.fileManager.createDirectory(atPath: subdirectoryPath, withIntermediateDirectories: true, attributes: nil)
            }
            catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        
        return subdirectoryPath
    }
    
    func retrieveContentsOfDirectoryAtPath(_ path: String) -> Array<String> {
        var contents = [String]()
        
        do {
            contents = try self.fileManager.contentsOfDirectory(atPath: path)
        }
        catch let error as NSError {
            print(error.localizedDescription);
        }
        
        return contents
    }
    
    func retrieveFilesInDirectoryAtPath(_ path: String) -> Array<String> {
        let contents = self.retrieveContentsOfDirectoryAtPath(path)
        var files = [String]()
        
        for item in contents {
            let itemPath = (path as NSString).appendingPathComponent(item)
            let isDirectory = self.isItemAtPathADirectory(itemPath)
            
            if (!isDirectory) {
                files.append(itemPath)
            }
        }
        
        return files
    }
    
    func retrieveAttributesOfFileAtPath(_ path: String) -> [FileAttributeKey : Any] {
        var attributes = [FileAttributeKey : Any]()
        
        do {
            attributes = try self.fileManager.attributesOfItem(atPath: path)
        }
        catch let error as NSError {
            print(error.localizedDescription);
        }
        
        return attributes
    }
    
    func calculateSizeOfDirectoryAtPath(_ path: String) -> UInt {
        let contents = self.retrieveContentsOfDirectoryAtPath(path)
        var totalSize: UInt = 0
        
        for item in contents {
            let itemPath = (path as NSString).appendingPathComponent(item)
            totalSize += self.calculateSizeOfFileAtPath(itemPath)
        }
        
        return totalSize
    }
    
    func calculateSizeOfFileAtPath(_ path: String) -> UInt {
        let fileAttributes = self.retrieveAttributesOfFileAtPath(path)
        return fileAttributes[FileAttributeKey.size] as! UInt
    }
    
    func saveFileData(_ data: Data, atPath: String, completion: (_ success: Bool) -> Void) {
        do {
            try data.write(to: URL(fileURLWithPath: atPath), options: NSData.WritingOptions.atomicWrite)
            completion(true)
        }
        catch let error as NSError {
            print(error.localizedDescription);
            completion(false)
        }
    }
    
    func removeFileAtPath(_ path: String, completion: (_ success: Bool) -> Void) {
        do {
            try self.fileManager.removeItem(atPath: path)
            completion(true)
        }
        catch let error as NSError {
            print(error.localizedDescription);
            completion(false)
        }
    }
    
    func doesFileExistAtPath(_ path: String) -> Bool {
        return self.fileManager.fileExists(atPath: path)
    }
    
    func isItemAtPathADirectory(_ path: String) -> Bool {
        var isDirectory: ObjCBool = ObjCBool(false)
        
        if self.fileManager.fileExists(atPath: path, isDirectory:&isDirectory) {
            return isDirectory.boolValue
        }
        
        return isDirectory.boolValue
    }
    
    func autoChangeFileName(_ name: String, atPath: String) -> String {
        let fileExtension = (name as NSString).pathExtension
        let fileName: NSString = ((name as NSString).lastPathComponent as NSString).deletingPathExtension as NSString
        
        do {
            let contents = try self.fileManager.contentsOfDirectory(atPath: atPath)
            var counter: Int = 1
            var newFileName = ""
            let suffix = NSLocalizedString("copy", comment: "")
            
            while (counter <= contents.count) {
                newFileName = "\(fileName) \(suffix) \(counter).\(fileExtension)"
                
                if (!self.doesFileExistAtPath("\(atPath)/\(newFileName)")) {
                    return newFileName
                }
                
                counter += 1
            }
        }
        catch let error as NSError {
            print(error.localizedDescription);
        }
        
        return name
    }
    
    func formattedStringSize(_ size: UInt) -> String {
        var convertedSize = Double(size) / 8
        var factorMultiplier = 0
        let tokens = ["bytes", "KB", "MB", "GB", "TB"]
        
        while (convertedSize > 1024) {
            convertedSize /= 1024
            factorMultiplier += 1
        }
        
        let convertedSizeString = String(format: "%.2f", convertedSize)
        return "\(convertedSizeString) \(tokens[factorMultiplier])"
    }

}
