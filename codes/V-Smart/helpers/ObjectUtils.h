//
//  ObjectUtils.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/31/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjectUtils : NSObject
+ (NSDictionary *)propertiesForClass:(Class)cls;
@end
