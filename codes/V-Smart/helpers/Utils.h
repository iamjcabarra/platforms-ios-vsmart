//
//  Utils.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/31/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface Utils : NSObject
+ (NSString *) fileSizeToHumanReadable:(NSNumber *) value;
+ (NSNumber *)getFileSize:(NSString *)fileName;
+ (NSString *) randomNumber;
+ (void) saveArchive: (id) object forKey: (NSString *) key;
+ (id) getArchive: (NSString *) key;

+ (NSString*) appCacheDirectory;
+ (NSString *) appDownloadDirectory;
+ (NSArray *) importedBooks;
+ (NSString *) appImportDirectory;
+ (void) clearImportDirectory;
+ (NSString *) appDocumentsDirectory;
+ (NSString *) appLibraryBooksDirectory;

+ (NSString *) getBookService;
+ (NSString *) getVibeServer;
+ (NSString *) getServiceAPI;
+ (NSString *) getSettingsSchoolCode;
+ (NSString *) getSettingsSchoolCodeBase64;
+ (void) setSchoolCodeBase64representation;
+ (void) setVendorCode:(NSString *)code;
+ (void)unzipEpubFile: (NSString *) epubName withFullPath:(NSString*)filepath;
+ (int) getFontSize;
+ (void) setFontSize: (int) fontSize;

+ (void) saveLoginState: (BOOL) state;
+ (void) saveReaderState: (BOOL) state;

+ (void) replaceExerciseFile: (NSString *) bookName;
+ (NSDictionary *) getExerciseFileContents: (NSString *) bookName;
+ (BOOL) isExerciseDataFileExists: (NSString *) bookName;

+ (NSArray *) getBookmarkForBook:(NSString *) bookName;
+ (void) addBookmark:(NSString *) bookname withChapter: (NSDictionary *) chapter;
+ (void) removeBookmark:(NSString *) bookname withChapter: (NSDictionary *) chapter;
+ (BOOL) isBookmarkAdded:(NSArray *) bookmark andChapter: (NSDictionary *) chapter;
+ (NSMutableArray *) getBookmark;

// Hash
+ (void) saveHash: (NSString *) hash;
+ (void) clearHash;
+ (NSString *) getHash;
+ (NSDictionary *) getBookSetings;
+ (void) saveBookSettings: (NSMutableDictionary *) dict;

+ (BOOL) isSecurityEnabled: (NSArray *) encryptedFiles forFile: (NSString *) filePath;
+ (NSString *) getUrlScheme;
+ (NSString *) prepareJSFile:(NSString *) filename;

+ (void) removeUserPhotos;
+ (NSString *) userPhotoDirectory;
+ (NSArray *) recentlyUsedImages;

+ (NSString *) toJSON: (NSDictionary *) data;
+ (NSDictionary *) toDictionary: (NSString *) json;
+ (void) tableViewFadeAnimation: (UITableView *) sender;

+ (BOOL) getBookPageNumberSettings;
+ (BOOL) getUserUpdatsSettings;
void AlertWithMessageAndDelegate(NSString *title, NSString *message, id theDelegate);
void AlertWithErrorAndDelegate(NSError *error, id theDelegate);

+ (NSString *) buildUrl: (NSString *) endpoint;
+ (void) playSoundFile: (NSString *) audioName withType: (NSString *) soundType;
+ (void) stopSound;

+ (NSString *) getUserId;
+ (void) saveUserId: (NSString *) userId;

+ (NSString *)memoryFormatter:(long long)diskSpace;

+ (NSString *)totalDiskSpace;

+ (NSString *)freeDiskSpace;

+ (NSString *) createBookFolder: (NSString *) folderName;
+ (void) copySettingsToDocuments ;
+ (void) moveFilesFromCacheToApplicationSupport;
+ (NSDictionary *) getPdfMetadata: (NSURL *) filePath forPDFFile:(NSString *) pdfFile withFolderName:(NSString *) bookId;

+ (void) copySamplersToDocuments;
+ (void)ListFonts;

+(void)adjustBorder:(UITextField*)textField;
+(void)adjustBorder2:(UITextView*)textView;

+ (BOOL)analyzeString:(NSString *)string lookup:(NSDictionary *)hashmap;


+ (void) saveSections:(NSString *)sections;
+ (NSString *) getSections;
+ (void) saveSectionName:(NSString *)name;
+ (NSString *) getSectionName;
+ (void) saveStreamType:(NSString *)data;
+ (NSString *) getStreamType;
+ (NSString *) getAccessToken;

// IMPLEMENTATION FOR PDF SECURITY
+ (void) setServer:(NSString *)version;
+ (NSString *)getServerVersion;

// IMPLEMENTATION SERVER INSTANCE VERSION CHECK
+ (void) setServerInstanceVersion:(NSString *)version;
+ (NSNumber *)getServerInstanceVersion;

+ (NSString *)deviceUUID;

+ (NSString *)stringValue:(id)object;

@end
