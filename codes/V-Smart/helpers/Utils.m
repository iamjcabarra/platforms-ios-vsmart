//
//  Utils.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/31/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "Utils.h"
#import "ZipArchive.h"
#include <sys/xattr.h>
#import "JSONKit.h"
#import "NSDataAdditions.h"

#define MB (1024*1024)
#define GB (MB*1024)

@implementation Utils

#pragma mark - Common Helpers

+ (NSString *) fileSizeToHumanReadable:(NSNumber *) value {
    float convertedValue = [value floatValue];
    int multiplyFactor = 0;
    
    NSArray *tokens = [NSArray arrayWithObjects:@"bytes",@"KB",@"MB",@"GB",@"TB",nil];
    
    while (convertedValue > 1024) {
        convertedValue /= 1024;
        multiplyFactor++;
    }
    
    return [NSString stringWithFormat:@"%4.2f %@", convertedValue, [tokens objectAtIndex:multiplyFactor]];
}

+ (NSNumber *) getFileSize:(NSString *)fileName {
    NSError *error = nil;
    NSDictionary *attributes = [[NSFileManager defaultManager]
                                attributesOfItemAtPath:fileName error:&error];
    
    if (!error) {
        NSNumber *size = [attributes objectForKey:NSFileSize];
        return size;
    } else {
        return [NSNumber numberWithFloat:-1];
    }    
}

+ (NSString *) randomNumber {
    return [NSString stringWithFormat:@"1%i%i%i%i%i",
            arc4random() % 9,
            arc4random() % 9,
            arc4random() % 9,
            arc4random() % 9,
            arc4random() % 9];
}

void AlertWithMessageAndDelegate(NSString *title, NSString *message, id theDelegate)
{
    NSString *okButton = NSLocalizedString(@"OK", nil);
    
	/* Show an alert with an OK button */
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
													message:message
												   delegate:theDelegate
										  cancelButtonTitle:okButton
										  otherButtonTitles: nil];
	[alert show];
}

void AlertWithErrorAndDelegate(NSError *error, id theDelegate)
{
    NSString *sorryLabel = NSLocalizedString(@"Sorry, ", nil);
    NSString *errorLabel = NSLocalizedString(@"Error", nil);
	NSString *message = [sorryLabel stringByAppendingString:[error localizedDescription]];
	AlertWithMessageAndDelegate(errorLabel, message, theDelegate);
}

+ (void) saveArchive: (id) object forKey: (NSString *) key {
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setCustomObject:object forKey:key];
}

+ (id) getArchive: (NSString *) key {
    
//    if ([key isEqualToString:@"GLOBAL_ACCOUNT"])
//        VLog(@"getArchive - %@", key);
    
    id object = VS_GET_CUSTOM_OBJECT(key);
    return object;
}

+ (NSString *)appDocumentsDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSLog(@"base path : %@", basePath);
    return basePath;
}

+ (NSString *) appLibraryBooksDirectory {
    
    // For V-Smart, we should really be adding username as folder: Format is USERNAME_USERID
    AccountInfo *account = [[VSmart sharedInstance] account];
//    NSString *userFolder = VS_FMT(@"%@_%i", account.user.email, account.user.id);
    NSString *userFolder = VS_FMT(@"%@", account.user.email);
    
//    NSString* path = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//    NSString *importDirectory = [NSString stringWithFormat:@"%@/Caches/Books/%@", path, userFolder];
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *importDirectory = [NSString stringWithFormat:@"%@/Books/%@", path, userFolder];
    
    VLog(@"importDirectory: %@", importDirectory);
    NSFileManager *fileManager= [NSFileManager defaultManager];
    BOOL isDir;
    
    if(![fileManager fileExistsAtPath:importDirectory isDirectory:&isDir]){
        if(![fileManager createDirectoryAtPath:importDirectory withIntermediateDirectories:YES attributes:nil error:NULL]){
            VLog(@"Error: Create folder failed %@", importDirectory);
        }
        [self addSkipBackupAttributeToItemAtURL:[NSURL URLWithString:importDirectory]];
    }
    
    return importDirectory;
}

#pragma mark - Book Import Helpers
+ (void) clearImportDirectory {
    // Clear files in Vibe_Import folder
    NSFileManager *fileMgr = [[NSFileManager alloc] init];
    NSError *error = nil;
    NSArray *directoryContents = [fileMgr contentsOfDirectoryAtPath:[self appImportDirectory] error:&error];
    if (error == nil) {
        for (NSString *path in directoryContents) {
            NSString *fullPath = [[self appImportDirectory] stringByAppendingPathComponent:path];
            VLog(@"Deleting Path: %@", fullPath);
            BOOL removeSuccess = [fileMgr removeItemAtPath:fullPath error:&error];
            if (!removeSuccess) {
                // Error handling
                VLog(@"Failed");
            }
        }
    } else {
        // Error handling
    }
}

+ (BOOL)addSkipBackupAttributeToItemAtURL: (NSURL *)URL {
    const char* filePath = [[URL path] fileSystemRepresentation];
    
    const char* attrName = "com.apple.MobileBackup";
    //u_int8_t attrValue = 1;
    
    int result = getxattr(filePath, attrName, NULL, sizeof(u_int8_t), 0, 0);
    if (result != -1) {
        // The attribute exists, we need to remove it
        int removeResult = removexattr(filePath, attrName, 0);
        if (removeResult == 0) {
            VLog(@"Removed extended attribute on file %@", URL);
        }
    }
    // Set the new key
    return [URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:nil];
//    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
//    return result == 0;

}

+ (NSString *) appImportDirectory {
//    NSString* path = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//    NSString *appLibraryImportCacheDir = [NSString stringWithFormat:@"%@/Caches/Books/VIBE_IMPORT", path];
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *appLibraryImportCacheDir = [NSString stringWithFormat:@"%@/Books/VIBE_IMPORT", path];
    return appLibraryImportCacheDir;
}

+ (NSArray *) importedBooks {
    NSString *epubPath = [self appImportDirectory];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *dirContents = [fm contentsOfDirectoryAtPath:epubPath error:nil];
    
    NSMutableArray *bookList = [[NSMutableArray alloc] init];
    
    for (NSString *subpath in dirContents) {
        if ( ![[subpath lastPathComponent] hasPrefix:@"."]) {
            [bookList addObject:subpath];
        }
    }
    
    return bookList;
}

+ (NSString *) appDownloadDirectory {
//    NSString* path = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//    NSString *appLibraryCacheDir = [NSString stringWithFormat:@"%@/Caches/%@", path, kDownloadedFolder];
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *appLibraryCacheDir = [NSString stringWithFormat:@"%@/%@", path, kDownloadedFolder];
    
    NSFileManager *fileManager= [NSFileManager defaultManager];
    BOOL isDir;
    
    if(![fileManager fileExistsAtPath:appLibraryCacheDir isDirectory:&isDir]){
        if(![fileManager createDirectoryAtPath:appLibraryCacheDir withIntermediateDirectories:YES attributes:nil error:NULL]){
            VLog(@"Error: Create folder failed %@", appLibraryCacheDir);
        }
        
        [self addSkipBackupAttributeToItemAtURL:[NSURL URLWithString:appLibraryCacheDir]];
    }
    
    return appLibraryCacheDir;
}

+ (NSString*) appCacheDirectory {
    
//    NSString* path = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//    NSString *appLibraryCacheDir = [NSString stringWithFormat:@"%@/Caches/%@", path, kAppName];
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *appLibraryCacheDir = [NSString stringWithFormat:@"%@/%@", path, kAppName];
    
    NSFileManager *fileManager= [NSFileManager defaultManager];
    BOOL isDir;
    
    if(![fileManager fileExistsAtPath:appLibraryCacheDir isDirectory:&isDir]){
        if(![fileManager createDirectoryAtPath:appLibraryCacheDir withIntermediateDirectories:YES attributes:nil error:NULL]){
            VLog(@"Error: Create folder failed %@", appLibraryCacheDir);
        }
        
        [self addSkipBackupAttributeToItemAtURL:[NSURL URLWithString:appLibraryCacheDir]];
    }
    
    return appLibraryCacheDir;
}

+ (void)unzipEpubFile: (NSString *) epubName withFullPath:(NSString*)filepath {
	
	ZipArchive* za = [[ZipArchive alloc] init];
    NSString *bookPath = [Utils appLibraryBooksDirectory];
    
	if( [za UnzipOpenFile:filepath]){
		NSString *strPath = [NSString stringWithFormat:@"%@/%@", bookPath, epubName];
        
        //VLog(@"%@", filepath);
		//Delete all the previous files
		NSFileManager *filemanager = [[NSFileManager alloc] init];
		if ([filemanager fileExistsAtPath:strPath]) {
			NSError *error;
			[filemanager removeItemAtPath:strPath error:&error];
		}
		filemanager=nil;
        
		BOOL ret = [za UnzipFileTo:[NSString stringWithFormat:@"%@/", strPath] overWrite:YES];
		if( NO==ret ){
            
            NSString *errorLabel = NSLocalizedString(@"Error", nil);
            NSString *errorMessage = NSLocalizedString(@"Error while unzipping the epub", nil);
            NSString *okButton = NSLocalizedString(@"OK", nil);
            
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:errorLabel
                                                            message:errorMessage
                                                           delegate:self
                                                  cancelButtonTitle:okButton
                                                  otherButtonTitles:nil];
			[alert show];
			alert=nil;
		}
		[za UnzipCloseFile];
	}
}

+ (BOOL) isSecurityEnabled: (NSArray *) encryptedFiles forFile: (NSString *) filePath {
    // SPINEPATH: /Users/earljon/Library/Application Support/iPhone Simulator/6.1/Applications/8707769C-6058-4D3D-81C6-EF82094BD91E/Documents/VIBAL-LANGLIT-II/OEBPS/ENC/enc6.xhtml
    NSRange hashTagRange = [filePath rangeOfString:@"#" options:NSCaseInsensitiveSearch];
    NSString *filename = [filePath lastPathComponent];
    
    if (hashTagRange.location != NSNotFound) {
        NSArray *splittedUrl = [filePath componentsSeparatedByString:@"#"];
        filename = [[splittedUrl objectAtIndex:0] lastPathComponent];
    }
    
    //VLog(@"Filename: %@", filename);
    if ([encryptedFiles containsObject:filename]) {
        return YES;
    }
    
    return NO;
}

+ (NSString *) getUrlScheme {
    NSString *urlScheme = @"";
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
    //NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
    NSArray* urlBundleArray = [infoDict objectForKey:@"CFBundleURLTypes"];
    
    NSDictionary *urlType = [urlBundleArray objectAtIndex:0];
    NSArray *urlSchemes =  [urlType objectForKey:@"CFBundleURLSchemes"];
    urlScheme = [urlSchemes objectAtIndex:0];
    
    return urlScheme;
}

+ (void) replaceExerciseFile: (NSString *) bookName {
    NSString *exerciseFile = [NSString stringWithFormat:@"%@/%@/OEBPS/JS/exercises.js", [Utils appLibraryBooksDirectory], bookName];
    VLog(@"Replace exercises.js!: %@", exerciseFile);
    
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:exerciseFile];
    if (exists) {
        NSFileManager *fm = [[NSFileManager alloc] init];
        [fm removeItemAtPath:exerciseFile error:nil];
        
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"exercises" ofType:@"js"];
        NSError *error;
        if(![fm copyItemAtPath:filePath toPath:exerciseFile error:&error]) {
            VLog(@"Error creating the database: %@", [error description]);
        }
    }
}

+ (NSDictionary *) getExerciseFileContents: (NSString *) bookName {
    // book name is the folder name

//    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:exerciseFile];

    BOOL exists = [self isExerciseDataFileExists:bookName];
    if (!exists) return nil;
    
    NSString *exerciseFile = [NSString stringWithFormat:@"%@/%@/OEBPS/JS/exercises.json", [Utils appLibraryBooksDirectory], bookName];
    NSString *fileContents = [NSString stringWithContentsOfFile:exerciseFile encoding:NSUTF8StringEncoding error:nil];
    
    VLog(@"FileContents: %@", fileContents);
    NSData *data = [fileContents dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    //VLog(@"FileDict: %@", jsonResponse);
    return jsonResponse;
}

+ (BOOL) isExerciseDataFileExists: (NSString *) bookName {
    NSString *exerciseFile = [NSString stringWithFormat:@"%@/%@/OEBPS/JS/exercises.json", [Utils appLibraryBooksDirectory], bookName];
    VLog(@"%@", exerciseFile);
    
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:exerciseFile];
    return exists;
}

+ (NSString *) prepareJSFile:(NSString *) filename {
    NSString *js = [NSString stringWithContentsOfFile:[[NSBundle mainBundle]
                                                       pathForResource:filename ofType:@"js"]
                                             encoding:NSUTF8StringEncoding error:nil];
    
    return js;
}

+ (NSString *) getBookService {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *server = [defaults stringForKey:@"bookurl_preference"];
    server = [server stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    return server;
}

+ (NSString *) getVibeServer {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *server = [defaults stringForKey:@"baseurl_preference"];
    server = [server stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    return server;
}

+ (NSString *) getServiceAPI {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *server = [defaults stringForKey:@"serviceurl_preference"];
    return server;
}

+ (NSString *) getSettingsSchoolCode {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *code = [defaults stringForKey:@"schoolcode_preference"];
    return code;
}

+ (NSString *) getSettingsSchoolCodeBase64 {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *code = [defaults stringForKey:@"schoolcode_base64"];
    return code;
}

+ (void) setSchoolCodeBase64representation
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *code = [defaults valueForKey:@"schoolcode_preference"];
    NSString *base64 = [NSData base64String:code];
    [defaults setValue:base64 forKeyPath:@"schoolcode_base64"];
    [defaults synchronize];
}

+ (void) setVendorCode:(NSString *)code
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *vendor = [NSString stringWithFormat:@"%@", code]; //ensure to copy value
    [defaults setValue:vendor forKeyPath:@"vendor_preference"];
    [defaults synchronize];
}

+ (int) getFontSize {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (![userDefaults valueForKey:@"FONT_SIZE"]) {
        return 100;
    }
    int fontSize = [[userDefaults valueForKey:@"FONT_SIZE"] intValue];
    
    return fontSize;
}

+ (void) setFontSize: (int) fontSize {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:[NSNumber numberWithInt:fontSize] forKey:@"FONT_SIZE"];
    [userDefaults synchronize];
}

+ (void) saveLoginState: (BOOL) state {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:state forKey:@"LOGIN_STATE"];
    [userDefaults synchronize];
}

+ (void) saveReaderState: (BOOL) state {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:state forKey:kReaderIsActive];
    [userDefaults synchronize];
}

#pragma mark - Bookmark related
+ (NSArray *) getBookmarkForBook:(NSString *) bookName
{
    NSArray *bookmarkItem;
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedDict = [currentDefaults objectForKey:bookName];
    
    if (dataRepresentingSavedDict != nil)
    {
        NSArray *oldSavedDict = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedDict];
        if (oldSavedDict != nil)
        {
            //bookmarkItem = [NSDictionary dictionaryWithDictionary:oldSavedDict];
            bookmarkItem = oldSavedDict;
            
            return bookmarkItem;
        }
    }
    
    return nil;
}

+ (void) addBookmark:(NSString *) bookname withChapter: (NSDictionary *) chapter
{
    NSArray *bookmarkArray = [self getBookmarkForBook:bookname];
    NSMutableArray *chapterArray;
    
    if (bookmarkArray) {
        chapterArray = [[NSMutableArray alloc] initWithArray:bookmarkArray];
        [chapterArray addObject:chapter];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:chapterArray]
                                                  forKey:bookname];
    }
    else
    {
        chapterArray = [[NSMutableArray alloc] init];
        [chapterArray addObject:chapter];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:chapterArray]
                                                  forKey:bookname];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void) removeBookmark:(NSString *) bookname withChapter: (NSDictionary *) chapter
{
    NSArray *bookmarkArray = [self getBookmarkForBook:bookname];
    
    if (bookmarkArray) {
        NSArray *origChapterArray = [[NSArray alloc] initWithArray:bookmarkArray];
        NSMutableArray *chapterArray = [[NSMutableArray alloc] init];
        
        for(NSDictionary *item in origChapterArray)
        {
            int currentSpineIndex = [[item objectForKey:kChapterSpineIndex] integerValue];
            int currentPageInSpineIndex = [[item objectForKey:kChapterPageIndexInSpine] integerValue];
            
            int _currentSpineIndex = [[chapter objectForKey:kChapterSpineIndex] integerValue];
            int _currentPageInSpineIndex = [[chapter objectForKey:kChapterPageIndexInSpine] integerValue];
            
            if (currentPageInSpineIndex == _currentPageInSpineIndex && currentSpineIndex == _currentSpineIndex) {
                continue;
            }
            else {
                [chapterArray addObject:item];
            }
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:chapterArray]
                                                  forKey:bookname];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL) isBookmarkAdded:(NSArray *) bookmark andChapter: (NSDictionary *) chapter
{
    BOOL isFound = NO;
    //VLog(@"isBookmarkAdded: %@", bookmark);
    //NSArray *bookmarkArray = (NSArray *)[bookmark objectForKey:kChapterArray];
    
    for(NSDictionary *item in bookmark)
    {
        int currentSpineIndex = [[item objectForKey:kChapterSpineIndex] integerValue];
        int currentPageInSpineIndex = [[item objectForKey:kChapterPageIndexInSpine] integerValue];
        
        int _currentSpineIndex = [[chapter objectForKey:kChapterSpineIndex] integerValue];
        int _currentPageInSpineIndex = [[chapter objectForKey:kChapterPageIndexInSpine] integerValue];
        
        if (currentPageInSpineIndex == _currentPageInSpineIndex && currentSpineIndex == _currentSpineIndex) {
            isFound = YES;
            break;
        }
    }
    
    //VLog(@"isBookmarkAdded: %@: %@", bookmark, isFound ? @"YES" : @"NO");
    //VLog(@"isBookmarkAdded: %@", chapter);
    return isFound;
}

+ (NSMutableArray *) getBookmark
{
    /*
     - NSArray
     - NSDictionary
     - chapterName
     - currentTextSize
     - currentSpineIndex
     - pagesInCurrentSpineCount
     - currentPageInSpineIndex
     */
    NSMutableArray *bookmarksArray;
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [currentDefaults objectForKey:kBookmarks];
    if (dataRepresentingSavedArray != nil)
    {
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        if (oldSavedArray != nil)
            bookmarksArray = [[NSMutableArray alloc] initWithArray:oldSavedArray];
        else
            bookmarksArray = [[NSMutableArray alloc] init];
    }
    else {
        bookmarksArray = [[NSMutableArray alloc] init];
    }
    
    return bookmarksArray;
}

#pragma mark - Book Related Helpers

+ (void) saveHash: (NSString *) hash {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:hash forKey:kBookPageHash];
    [userDefaults synchronize];
}

+ (void) clearHash {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:nil forKey:kBookPageHash];
    [userDefaults synchronize];
}

+ (NSString *) getHash {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *hash = [userDefaults valueForKey:kBookPageHash];
    
    return hash;
}

+ (NSDictionary *) getBookSetings {
    NSString *storePath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:@"BookSettings.plist"];
    NSDictionary *plistData = [NSDictionary dictionaryWithContentsOfFile:storePath];
    return plistData;
}

+ (void) saveBookSettings: (NSMutableDictionary *) dict {
    NSString *storePath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:@"BookSettings.plist"];
    [dict writeToFile:storePath atomically:YES];
}

+ (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

+ (void) removeUserPhotos {
    [VSmartHelpers deleteTMPFile:[self userPhotoDirectory]];
}

+ (NSString *) userPhotoDirectory {
    NSString *userFolder = [VSmartHelpers _getUserDefaultKey:@""];
    
	NSString *photoPath = [self applicationDocumentsDirectory];
    photoPath = [photoPath stringByAppendingFormat:@"/%@", userFolder];
    
    NSFileManager *filemanager = [[NSFileManager alloc] init];
    if (![filemanager fileExistsAtPath:photoPath]) {
        NSError *error;
        [filemanager createDirectoryAtPath:photoPath withIntermediateDirectories:YES attributes:nil error:&error];
    }
    filemanager=nil;
    
    return photoPath;
}

+ (NSArray *) recentlyUsedImages {
    
	NSString *bookPath = [self userPhotoDirectory];
    //VLog(@"bookPath: %@", bookPath);
    
    NSMutableArray *bookList = [NSMutableArray array];
	NSFileManager *fm = [NSFileManager defaultManager];
	NSArray *dirContents = [fm contentsOfDirectoryAtPath:bookPath error:nil];

    for (NSString *subpath in dirContents) {
        if ( ![[subpath lastPathComponent] hasPrefix:@"."]) {
            if ([subpath hasSuffix:@".png"] || [subpath hasSuffix:@".jpg"]) {
                NSString *imagePath = VS_FMT(@"%@/%@", bookPath, subpath);
                [bookList addObject:imagePath];
            }
        }
    }
    
    return bookList;
}

+ (NSString *) toJSON: (NSDictionary *) data {
    NSString *json = [data JSONString];
    return json;
}

+ (NSDictionary *) toDictionary: (NSString *) json {
    NSDictionary *dict = [json objectFromJSONString];
    return dict;
}

+ (void) tableViewFadeAnimation: (UITableView *) sender {
    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionFade];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setFillMode:kCAFillModeBoth];
    [animation setDuration:.3];
    [[sender layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
}

+ (BOOL) getBookPageNumberSettings {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL switchValue = [defaults boolForKey:@"page_number_preference"];
    
    return switchValue;
}

+ (BOOL) getUserUpdatsSettings {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL switchValue = [defaults boolForKey:@"global_notification_preference"];
    
    return switchValue;
}

+ (NSString *) buildUrl: (NSString *) endpoint {
    
    NSString *url = [NSString stringWithFormat:@"http://%@%@", VSMART_BASE_URL, endpoint];
    
    BOOL productionMode = YES;
    if ([url rangeOfString:@"vsmart-rest-dev"].length > 0) {
        //perform the routines
        if (productionMode) {
            NSString *base = VSMART_BASE_URL;
            NSString *api = VSMSRT_API_URL;
            
            url = [url stringByReplacingOccurrencesOfString:base withString:api];
            url = [url stringByReplacingOccurrencesOfString:@"vsmart-rest-dev" withString:@""];
            url = [url stringByReplacingOccurrencesOfString:@"//v1" withString:@"/v1"];
            url = [url stringByReplacingOccurrencesOfString:@"//v2" withString:@"/v2"];
            url = [url stringByReplacingOccurrencesOfString:@"http:/v" withString:@"http://v"];
        }
    }
    
    //VLog(@"URL: %@", url);
    
    return url;
}

+ (void) playSoundFile: (NSString *) audioName withType: (NSString *) soundType {
    [[JSQSystemSoundPlayer sharedPlayer] playSoundWithName:audioName
                                                 extension:soundType
                                                completion:^{
                                                    VLog(@"Long sound complete!");
                                                }];
}

+ (void) stopSound {
    [[JSQSystemSoundPlayer sharedPlayer] stopAllSounds];
}

+ (NSString *) getUserId {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *userId = [userDefaults valueForKey:@"USER_ID"];
    
    return userId;
}

+ (void) saveUserId: (NSString *) userId {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:userId forKey:@"USER_ID"];
    [userDefaults synchronize];
}

+ (NSString *)memoryFormatter:(long long)diskSpace {
    NSString *formatted;
    double bytes = 1.0 * diskSpace;
    double megabytes = bytes / MB;
    double gigabytes = bytes / GB;
    if (gigabytes >= 1.0)
        formatted = [NSString stringWithFormat:@"%.2f GB", gigabytes];
    else if (megabytes >= 1.0)
        formatted = [NSString stringWithFormat:@"%.2f MB", megabytes];
    else
        formatted = [NSString stringWithFormat:@"%.2f bytes", bytes];
    
    return formatted;
}

+ (NSString *)totalDiskSpace {
    long long space = [[[[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:nil] objectForKey:NSFileSystemSize] longLongValue];
    return [self memoryFormatter:space];
}

+ (NSString *)freeDiskSpace {
    long long freeSpace = [[[[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:nil] objectForKey:NSFileSystemFreeSize] longLongValue];
    return [self memoryFormatter:freeSpace];
}

+ (void) copySettingsToDocuments {
    NSString *storePath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:@"BookSettings.plist"];
    //VLog(@"StorePath: %@", storePath);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:storePath]) {
        NSString *defaultStorePath = [[NSBundle mainBundle] pathForResource:@"BookSettings" ofType:@"plist"];
        if (defaultStorePath) {
            [fileManager copyItemAtPath:defaultStorePath toPath:storePath error:NULL];
        }
    } else {
        NSDictionary *settings = [self getBookSetings];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:settings];
        [dict setValue:[NSNumber numberWithFloat:0.5] forKey:@"brightness_level"];
        
        [self saveBookSettings:dict];
    }
}

+ (void) moveFilesFromCacheToApplicationSupport {
	NSString *sourcePath = [self appLibraryBooksDirectoryCompatibility];
    NSString *destPath = [self appBooksDirectory];
    
	NSFileManager *fm = [NSFileManager defaultManager];
	NSArray *dirContents = [fm contentsOfDirectoryAtPath:sourcePath error:nil];
    
    for (NSString *subpath in dirContents) {
        if ( ![[subpath lastPathComponent] hasPrefix:@"."]) {
            NSString *fromPath = [sourcePath stringByAppendingPathComponent:subpath];
            NSString *toPath = [destPath stringByAppendingPathComponent:subpath];
            [[NSFileManager defaultManager] moveItemAtPath: fromPath toPath:toPath error:nil];
        }
    }
}

+ (NSString *)appLibraryBooksDirectoryCompatibility {
    
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *directory = [NSString stringWithFormat:@"%@/Caches/Books", path];
    
    [self createBookFolderToDirectory:directory];
    return directory;
}

+ (NSString *)appBooksDirectory {
    
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *directory = [NSString stringWithFormat:@"%@/Books", path];
    
    [self createBookFolderToDirectory:directory];
    return directory;
}

+ (void) createBookFolderToDirectory: (NSString *) directory {
    NSFileManager *fileManager= [NSFileManager defaultManager];
    BOOL isDir;
    
    if(![fileManager fileExistsAtPath:directory isDirectory:&isDir]){
        if(![fileManager createDirectoryAtPath:directory withIntermediateDirectories:YES attributes:nil error:NULL]){
            VLog(@"Error: Create folder failed %@", directory);
        }
        [self addSkipBackupAttributeToItemAtURL:[NSURL URLWithString:directory]];
    }
}

+ (void) copySamplersToDocuments {
    
//    NSString *storePath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:@"owm5sample.epub"];
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    NSString *defaultStorePath = [[NSBundle mainBundle] pathForResource:@"owm5sample" ofType:@"epub"];
//    if (defaultStorePath) {
//        [fileManager copyItemAtPath:defaultStorePath toPath:storePath error:NULL];
//    }

    NSArray *epubFiles = [self samplerEpubFiles];
    if ([epubFiles count] > 0) {
        for (NSString *f in epubFiles) {
            BOOL status = [self importSamplerFile:f];
            NSString *message = (status) ? @"OK": @"NOK";
            VLog(@"copy file : %@ status : %@", f, message);
        }
    }
}

+(BOOL)importSamplerFile:(NSString *)filePath
{
    BOOL status = NO;
    NSArray *components = [filePath componentsSeparatedByString:@"."];
    if ([components count] > 1) {
        NSString *fileName = components[0];
        NSString *fileExtension = components[1];
        if (filePath) {
            NSString *storePath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:filePath];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSString *defaultStorePath = [[NSBundle mainBundle] pathForResource:fileName ofType:fileExtension];
            NSError *error = nil;
            [fileManager copyItemAtPath:defaultStorePath toPath:storePath error:&error];
            if (error) {
                VLog(@"file copy error : %@", [error localizedDescription]);
                return NO;
            }
            return YES;
        }
    }
    
    return status;
}

+ (NSMutableArray *)samplerEpubFiles
{
    NSMutableArray *items = [NSMutableArray array];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *bundleURL = [[NSBundle mainBundle] bundleURL];
    NSArray *contents = [fileManager contentsOfDirectoryAtURL:bundleURL
                                   includingPropertiesForKeys:@[]
                                                      options:NSDirectoryEnumerationSkipsHiddenFiles
                                                        error:nil];
    // Filter files with extension name .epub
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pathExtension == 'epub'"];
    for (NSURL *fileURL in [contents filteredArrayUsingPredicate:predicate]) {
        // Enumerate each .epub file in directory
        NSString *epubFileName = [NSString stringWithFormat:@"%@",[[fileURL path] lastPathComponent]];
        [items addObject:epubFileName];// add epub file
    }
    return items;
}

+ (NSString *) createBookFolder: (NSString *) folderName {
    NSString *bookPath = [Utils appLibraryBooksDirectory];
    NSString *folderPath = [NSString stringWithFormat:@"%@/%@", bookPath, folderName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return folderPath;
}

+ (NSString *)getKey:(char *)key from:(CGPDFDictionaryRef)dict {
    NSString *value = nil;
    CGPDFStringRef cfValue;
    if (CGPDFDictionaryGetString(dict, key, &cfValue))
        value = CFBridgingRelease(CGPDFStringCopyTextString(cfValue));
    return value;
}

+ (NSDictionary *) getPdfMetadata: (NSURL *) filePath forPDFFile:(NSString *) pdfFile withFolderName:(NSString *) bookId {
    NSURL* pdfFileUrl = filePath;
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfFileUrl);
    CGPDFDictionaryRef dict = CGPDFDocumentGetInfo(pdf);
    
    //VLog(@"ABS_URL: %@", [[pdfFileUrl absoluteString] lastPathComponent]);
    NSDictionary *data = nil;
    if (dict != NULL) {
        NSString *title = [self getKey:"Title" from:dict];
        NSString *authors = [self getKey:"Author" from:dict];
        
        data = @{@"title": IsEmpty(title) ? [[pdfFile lastPathComponent] stringByDeletingPathExtension] : title, @"authors_text": IsEmpty(authors) ? @"Author Unknown" : authors, @"book_id": bookId};
    } else {
        data = @{@"title": [[pdfFile lastPathComponent] stringByDeletingPathExtension], @"authors_text": @"Author Unknown", @"book_id": bookId};
    }
    CGPDFDocumentRelease(pdf);
    
    return data;
}

+ (void)ListFonts{
    NSArray *familyNames = [UIFont familyNames];
    for (NSString *aFamilyName in familyNames) {
        NSArray *fontNames = [UIFont fontNamesForFamilyName:aFamilyName];
        for (NSString *aFontName in fontNames) {
            NSLog(@"%@", aFontName);
        }
    }
}

+(void)adjustBorder:(UITextField*)textField{
    textField.layer.masksToBounds=YES;
    textField.layer.borderColor=[[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1]CGColor];
    textField.layer.borderWidth= 1.0f;
}

+(void)adjustBorder2:(UITextView*)textView{
    textView.layer.masksToBounds=YES;
    textView.layer.borderColor=[[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1]CGColor];
    textView.layer.borderWidth= 1.0f;
}

+ (BOOL)analyzeString:(NSString *)string lookup:(NSDictionary *)hashmap {
    
    NSArray *words = [string componentsSeparatedByString:@" "];
    
    for (NSString *key in words) {
        
        NSString *comparedkey = [NSString stringWithFormat:@"%@", [key lowercaseString] ];
        if ( [hashmap objectForKey:comparedkey] ) {
            NSString *message = [NSString stringWithFormat:@"'%@' is bad word!!!", key ];
            AlertWithMessageAndDelegate(@"Invalid Entry", message, self);
            return NO;
        }
    }
    return YES;
}

+ (void) saveSections:(NSString *) sections {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:sections forKey:@"SECTION_ID_SS"];
    [userDefaults synchronize];
}

+ (NSString *) getSections {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *sections = [userDefaults stringForKey:@"SECTION_ID_SS"];
    return sections;
}

+ (void) saveSectionName:(NSString *)name {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:name forKey:@"SECTION_NAME_SS"];
    [userDefaults synchronize];
}

+ (NSString *) getSectionName {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *section_name = [NSString stringWithFormat:@"%@", [userDefaults stringForKey:@"SECTION_NAME_SS"]];
    return section_name;
}

+ (void) saveStreamType:(NSString *)data {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:data forKey:@"STREAM_TYPE"];
    [userDefaults synchronize];
}

+ (NSString *) getStreamType {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *data = [NSString stringWithFormat:@"%@", [userDefaults stringForKey:@"STREAM_TYPE"]];
    return data;
}

+ (NSString *) getAccessToken {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *access_token = [NSString stringWithFormat:@"%@", [userDefaults stringForKey:kOAuthResponseKey]];
    return access_token;
}

// IMPLEMENTATION FOR PDF SECURITY
+ (void) setServer:(NSString *)version {
    NSString *server_version = [NSString stringWithFormat:@"%@", version];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:server_version forKey:kServerVersionFlag];
    [userDefaults synchronize];
}

+ (NSString *)getServerVersion {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *server_version = [NSString stringWithFormat:@"%@", [userDefaults stringForKey:kServerVersionFlag]];
    
    if ([server_version isEqualToString:@"<null>"] || [server_version isEqualToString:@"(null)"] || [server_version isEqualToString:@"null"] ) {
        return @"1";
    }
    
    return server_version;
}


// server side version checker
+ (void) setServerInstanceVersion:(NSString *)version {
    NSString *server_version = [NSString stringWithFormat:@"%@", version];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:server_version forKey:kServerIstanceVersion];
    [userDefaults synchronize];
}

+ (NSNumber *)getServerInstanceVersion {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *server_version = [NSString stringWithFormat:@"%@", [userDefaults stringForKey:kServerIstanceVersion]];
    
    // NULL VALUES
    if ([server_version isEqualToString:@"<null>"] || [server_version isEqualToString:@"(null)"] || [server_version isEqualToString:@"null"] ) {
        server_version = @"2.1";
    }
    
    // LESS THAN 2.1
    CGFloat value = [server_version floatValue];
    if (value == 0) {
        value = 2.1;
    }
    
    return [NSNumber numberWithFloat:value];
}

+ (NSString *)deviceUUID {
    return [NSString stringWithFormat:@"%@", [[VSmart sharedInstance] uuid] ];
}

+ (NSString *)stringValue:(id)object {
    
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] ) {
        return @"";
    }
    
    return value;
}


@end
