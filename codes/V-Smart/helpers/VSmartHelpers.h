//
//  VSmartHelpers.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/30/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Metadata.h"
#import "EPub.h"
#import "Book.h"
#import "BookExercise.h"
#import "BookExerciseResult.h"
#import "STKeychain.h"
#import "JMLockInOutResponse.h"

@interface VSmartHelpers : NSObject
+ (NSString *) uniqueGlobalDeviceIdentifier;
+ (NSString *) jsonString: (NSDictionary *) dict;
+ (void) saveAuthToken: (NSString *) token;
+ (NSString *) getAuthToken;
+ (void) saveEmail: (NSString *) email;
+ (NSString *) getEmail;
+ (int) getFileSize: (NSString *) file;
+ (BOOL) extractArchive: (NSString *) epubName sourceFilePath:(NSString *) sourceFilePath
      destinationFolder:(NSString*)destinationFolderPath;
+ (void)deleteTMPFile: (NSString *) filePath;
+ (void) clearSession;

+ (VSmartDownloadBookStatus) getBookReadingStatus:(NSString *) bookId;
+ (UIImage *) getBookshelfCover:(UIImage *) coverImage withBookId: (NSString *) bookId andImageShadow: (BOOL) applyShadow
                withRibbonImage:(UIImage *) image atPoint: (CGPoint) point;
+ (UIImage *) getBookshelfCover:(UIImage *) coverImage withBookId: (NSString *) bookId andImageShadow: (BOOL) applyShadow;
+ (NSDictionary *) getBookInfoFromCollection: (NSString *) bookId inCollection: (NSArray *) bookCollection ;

+ (NSString *) getAuthors: (NSArray *) authorDict;
+ (BOOL) isBookOnShelf:(NSString *) currentBookId;
+ (BOOL) isBookOnShelf:(NSString *) currentBookId thruAction:(NSString *) action;

+ (NSArray *) getBookCollection;
+ (NSArray *) prepareBooks;
+ (NSArray *) getDownloadedBooks;
+ (void) addImportedBook: (Book *) book;
+ (void) updateBookToCollection: (NSString *) theBookId;
+ (void) removeBookToCollection: (NSString *) theBookId;
+ (void) removeImportedBook: (NSString *) bookTitleAsId;
+ (void) clearImportedBooks;
+ (void) removeBook:(NSString *) bookTitleAsId;
+ (NSArray *) getImportedBooks;
+ (void) addBook:(Book *) book;

+ (void) saveBookExerciseData: (NSString *) bookNameAsFolder forExercise:(NSDictionary *) data;
+ (NSArray *) getBookExercises;
+ (NSDictionary *) getBookExerciseData: (NSString *) bookNameAsFolder;
+ (NSDictionary *) getBookExerciseData: (NSString *) bookNameAsFolder inCollection: (NSArray *) source;
+ (void) removeExerciseData: (NSString *) bookNameAsFolder;
+ (BookExercise *) getBookExercise: (NSString *) bookNameAsFolder withExerciseId: (int) exerciseId;

+ (NSArray *) getBookExerciseResults: (NSString *) bookNameAsId;
+ (NSArray *) getBookExerciseResults;
+ (void) saveBookExerciseResult: (BookExerciseResult *) data;
+ (BookExerciseResult *) getBookExerciseResult: (BookExerciseResult *) data inCollection: (NSArray *) exerciseResultCollection;
+ (void) removeExerciseResultData: (NSString *) bookNameAsFolder;

+ (void) addBookToCloud: (Metadata *) metadata;
+ (void) addBookExerciseToCloud: (NSString *) bookName forBook:(NSString *) uuid;

+ (NSDictionary *) getMedals;
+ (void) saveCurrentSpineState: (NSString *) bookIdentifer forSpineData: (NSDictionary *) spineData;
+ (NSDictionary *) getCurrentSpineState: (NSString *) bookIdentifer;
+ (void) saveTempHighlightNote: (NSDictionary *) highlight;
+ (NSDictionary *) getTempHighlightNote;
+ (NSString *) _getUserDefaultKey: (NSString *) baseKey;

+(JMLockInOutBook*)getAssessments:(NSString*)bookId;
+(NSString*)getAssessmentsAsJSONforExerJS:(NSString*)bookId optionUnlock:(BOOL)unlock;
+(BOOL)validateAssessment:(NSString*)bookId withExerciseId:(NSString*)exerciseId withCode:(NSString*)code;

+(NSString*)aliasFromFirstName:(NSString*)firstName andLastName:(NSString*)lastName;

+(NSArray*)loadLastUserDefaultProfile;
+(void)saveLastUserDefaultProfile:(NSArray*)record;

+ (NSString *)getDeviceLocale;//get current device language
+ (NSString *)getVersionCode;

+ (void) pullCurrentSections;

#pragma mark - DRM Manager
+ (void)saveDRMinfoForBook:(NSString *)uuid drmversion:(NSString *)drmversion;
+ (NSDictionary *)getDRMInfo:(NSString *)uuid;
+ (NSString *)getUserIdWithBookUUID:(NSString *)string;

@end
