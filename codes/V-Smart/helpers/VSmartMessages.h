//
//  VSmartMessages.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/30/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MSG_LOGIN_START     @"Signing-in to V-Smart"
#define MSG_BOOKS_FETCHING  @"Fetching Books"
#define MSG_BOOKS_FAILED    @"Download Failed."
#define MSG_BOOKS_DOWNLOAD_ERROR    @"Failed to download %@.\nPlease try again later."
#define MSG_LOADING_MODULE  @"Loading ..."