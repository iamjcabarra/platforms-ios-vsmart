//
//  VSmartHelpers.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/30/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "VSmartHelpers.h"
#import "NSString+MD5Addition.h"

#include <sys/socket.h> // Per msqr
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>
#import "ZipArchive.h"
#include <sys/xattr.h>

#import "DRMManager.h"

@implementation VSmartHelpers
////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Private Methods

// Return the local MAC addy
// Courtesy of FreeBSD hackers email list
// Accidentally munged during previous update. Fixed thanks to erica sadun & mlamb.
+ (NSString *) macaddress{
    
    int                 mib[6];
    size_t              len;
    char                *buf;
    unsigned char       *ptr;
    struct if_msghdr    *ifm;
    struct sockaddr_dl  *sdl;
    
    mib[0] = CTL_NET;
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_LINK;
    mib[4] = NET_RT_IFLIST;
    
    if ((mib[5] = if_nametoindex("en0")) == 0) {
        printf("Error: if_nametoindex error\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 1\n");
        return NULL;
    }
    
    if ((buf = malloc(len)) == NULL) {
        printf("Could not allocate memory. error!\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 2");
        free(buf);
        return NULL;
    }
    
    ifm = (struct if_msghdr *)buf;
    sdl = (struct sockaddr_dl *)(ifm + 1);
    ptr = (unsigned char *)LLADDR(sdl);
    NSString *outstring = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                           *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    free(buf);
    
    return outstring;
}

#pragma mark - Public Internal Static Methods

+ (BOOL) extractArchive: (NSString *) epubName sourceFilePath:(NSString *) sourceFilePath
      destinationFolder:(NSString*)destinationFolderPath {
	
	ZipArchive* za = [[ZipArchive alloc] init];
    BOOL ret = NO;
    
	if([za UnzipOpenFile:sourceFilePath]){
		NSString *strPath = [NSString stringWithFormat:@"%@/%@", destinationFolderPath, epubName];
        
		NSFileManager *filemanager = [[NSFileManager alloc] init];
		if ([filemanager fileExistsAtPath:strPath]) {
			NSError *error;
			[filemanager removeItemAtPath:strPath error:&error];
		}
        
		filemanager = nil;
        
		ret = [za UnzipFileTo:[NSString stringWithFormat:@"%@/", strPath] overWrite:YES];
		if( NO == ret ){
		}
		[za UnzipCloseFile];
	}
	//[za release];
    
    return ret;
}

+ (void)deleteTMPFile: (NSString *) filePath {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
        NSFileManager *manager = [[NSFileManager alloc] init];
        
        NSError *error = nil;
        [manager removeItemAtPath:filePath error:&error];
        
        if (error && [manager fileExistsAtPath:filePath]) {
            [manager createFileAtPath:filePath contents:[NSData data] attributes:nil];
            [manager removeItemAtPath:filePath error:nil];
        }
    });
}

+ (NSString *) uniqueGlobalDeviceIdentifier {
    NSString *value = [STKeychain getPasswordForUsername:kSystemUsername andServiceName:kSystemServiceName error:nil];
    if (IsEmpty(value)) {
        NSString *uuid = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
        BOOL storeSuccess = [STKeychain storeUsername:kSystemUsername andPassword:uuid
                                       forServiceName:kSystemServiceName updateExisting:YES error:nil];
        if (storeSuccess) {
            value = [STKeychain getPasswordForUsername:kSystemUsername andServiceName:kSystemServiceName error:nil];
        }
    }
    
//    VLog(@"DeviceId: %@", value);
//    value = @"bc995eb514ff600c591761181a66242b";
    
    return value;
}

+ (NSString *) jsonString: (NSDictionary *) dict {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:0 // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

+ (void) saveAuthToken: (NSString *) token
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:token forKey:@"AUTH_TOKEN"];
    [userDefaults synchronize];
}

+ (void) saveEmail: (NSString *) email
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:email forKey:@"LOGIN_EMAIL"];
    [userDefaults synchronize];
}

+ (NSString *) getAuthToken
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *auth_token = [userDefaults valueForKey:@"AUTH_TOKEN"];
    
    return auth_token;
}

+ (NSString *) getEmail
{
    NSString *email = [[VSmart sharedInstance] email];
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //NSString *email = [userDefaults valueForKey:@"LOGIN_EMAIL"];
    
    return email;
}

+ (void) clearSession {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:nil forKey:@"LOGIN_EMAIL"];
    [userDefaults setValue:nil forKey:@"AUTH_TOKEN"];
    
    [userDefaults synchronize];
}

+ (int) getFileSize: (NSString *) file {
    NSError *error = nil;
    NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:file error:&error];
    
    if (!error) {
        NSNumber *size = [attributes objectForKey:NSFileSize];
        return [size intValue];
    }
    
    return 0;
}

+ (NSString *) getUrlScheme {
    NSString *urlScheme = @"";
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
    //NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
    NSArray* urlBundleArray = [infoDict objectForKey:@"CFBundleURLTypes"];
    
    NSDictionary *urlType = [urlBundleArray objectAtIndex:0];
    NSArray *urlSchemes =  [urlType objectForKey:@"CFBundleURLSchemes"];
    urlScheme = [urlSchemes objectAtIndex:0];
    
    return urlScheme;
}

+ (VSmartDownloadBookStatus) getBookReadingStatus:(NSString *) bookId {
    VSmartDownloadBookStatus bookReadStatus = VSmartDownloadedBookRead;
    
    NSDictionary *bookInfoFromCollection = [self getBookInfoFromCollection: bookId inCollection:nil];
    NSString *bookStateText = [bookInfoFromCollection objectForKey:@"BOOK_DATE_LAST_READ"];
    if([bookStateText isEqualToString:@"Unread"]) {
        bookReadStatus = VSmartDownloadedBookUnread;
    }
    
    return bookReadStatus;
}

+ (UIImage *) getBookshelfCover:(UIImage *) coverImage withBookId: (NSString *) bookId andImageShadow: (BOOL) applyShadow {
    
    CGPoint point = CGPointMake(215, 5);
    UIImage *finalImage = [self getBookshelfCover:coverImage withBookId:bookId
                                   andImageShadow:applyShadow
                                  withRibbonImage:[UIImage imageNamed:@"img_new-label@2x.png"] atPoint:point];
    return finalImage;
}

+ (UIImage *) getBookshelfCover:(UIImage *) coverImage withBookId: (NSString *) bookId andImageShadow: (BOOL) applyShadow
                withRibbonImage:(UIImage *) image atPoint: (CGPoint) point {
    
    NSDictionary *bookInfoFromCollection = [self getBookInfoFromCollection: bookId inCollection:nil];
    
    UIImage *finalImage = coverImage;
    
    NSString *bookStateText = [bookInfoFromCollection objectForKey:@"BOOK_DATE_LAST_READ"];
    VLog(@"Book Status Text: %@", bookStateText);
    
    if([bookStateText isEqualToString:@"Unread"]) {
        //CGPoint point = CGPointMake(0, 0);
        //CGPoint point = CGPointMake(29, -1);
        UIImage *overlayedImage = [coverImage overlayImage:image atPoint:point];
        //UIImage *overlayedImage = [coverImage overlayImage:[UIImage imageNamed:@"new-ribbon-small.png"] atPoint:point];
        finalImage = overlayedImage;
    }
    
    if(applyShadow)
        return [finalImage imageWithShadow];
    
    return finalImage;
}

+ (void) addBookToCloud: (Metadata *) metadata {
    
    if (!IsEmpty(metadata.uuid)) {
        if (![metadata.uuid isEqualToString:@"book_id"]) {
            NSDictionary *book = @{kDBCloudUUID: metadata.uuid, 
                              kDBCloudBookTitle: metadata.title, 
                              kDBCloudBookPublisher: metadata.publisher, 
                              kDBCloudBookAuthors: metadata.authors};
            
            VLog(@"ADD_BOOK_TO_CLOUD: %@", book);
            [[VibeDataManager sharedInstance] addCloudBook:book];
        }
    }
}

+ (void) addBookExerciseToCloud: (NSString *) bookName forBook:(NSString *) uuid {
    if (!IsEmpty(uuid)) {
        if ([Utils isExerciseDataFileExists:bookName]) {
            NSDictionary *exercise = [Utils getExerciseFileContents:bookName];
            VLog(@"EXERCISES: %@", exercise);
            [[VibeDataManager sharedInstance] addCloudBookExercise:exercise forBook:uuid];
        }
    }
}

+ (NSArray *) prepareBooks {
    NSMutableArray *bookNames = [[NSMutableArray alloc] init];
    
	NSString *bookPath = [Utils appLibraryBooksDirectory];
    
    NSMutableArray *bookList = [NSMutableArray array];
	NSFileManager *fm = [NSFileManager defaultManager];
	NSArray *dirContents = [fm contentsOfDirectoryAtPath:bookPath error:nil];
    for (NSString *subpath in dirContents) {
        if ( ![[subpath lastPathComponent] hasPrefix:@"."]) {
            [bookList addObject:subpath];
        }
    }
    
    [bookList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (![[obj lowercaseString] isEqualToString:@"vibe_import"]) {
            EPub *epub = [[EPub alloc] initWithBookname:obj];
            NSString *image = epub.coverImagePath;
            
            Metadata *meta = [[Metadata alloc] init];
            meta.title =  IsEmpty(epub.title)  ? @"No Title" : epub.title;
            meta.publisher = IsEmpty(epub.publisher) ? @"<not specified>" : epub.publisher;
            meta.authors = epub.authors;
            meta.coverImageUrl = image;
            meta.uuid = epub.uuid;
            
            [self addBookToCloud:meta];
            [self addBookExerciseToCloud:obj forBook:meta.uuid];
            
            NSString *rootPath = epub.rootPath;
            
            NSString *customPath = [NSString stringWithFormat:@"/%@/%@/%@", obj, rootPath, image];
            NSString *coverPath = [bookPath stringByAppendingFormat:@"%@", [rootPath isEqualToString:@""] ? [NSString stringWithFormat: @"/%@/%@", obj, image] : customPath];
            NSDictionary *bookItem = [NSDictionary dictionaryWithObjectsAndKeys: meta.title, @"book_title", coverPath, @"coverPath", obj, @"title", meta, @"metadata", nil];
            [bookNames addObject:bookItem];
        }
    }];

    return bookNames;
}

#pragma mark - Book Exercise
+ (NSArray *) _getExercises: (NSDictionary *) data {
    NSArray *exercises = [data objectForKey:@"exercises"];
    VLog(@"Exercises: %@", exercises);
    
    NSMutableArray *exerciseData = [[NSMutableArray alloc] initWithCapacity:[exercises count]];
    for (NSDictionary *item in exercises) {
        BookExercise *bookExercise = [[BookExercise alloc] init];
        bookExercise.href = [item objectForKey:@"href"];
        bookExercise.title = [item objectForKey:@"title"];
        bookExercise.totalItems = [[item objectForKey:@"total_items"] intValue];
                
        NSArray *hrefArray = [bookExercise.href componentsSeparatedByString:@"#"];
        NSString *exer = [hrefArray objectAtIndex:1];
        
        int exerNum = [[exer stringByReplacingOccurrencesOfString:@"exer" withString:@""] intValue];
        //int exerNum = [[bookExercise.title stringByReplacingOccurrencesOfString:@"Exercise " withString:@""] intValue];
        bookExercise.exerciseId = exerNum;
        
        [exerciseData addObject:bookExercise];
    }
    
    return exerciseData;
}

+ (void) saveBookExerciseData: (NSString *) bookNameAsFolder forExercise:(NSDictionary *) data {
    NSArray *booksArray = [self getBookExercises];
    
    NSMutableArray *newBookArray;
    
    if (booksArray) {
        newBookArray = [[NSMutableArray alloc] initWithArray:booksArray];
    } else {
        newBookArray = [[NSMutableArray alloc] init];
    }
    
    NSArray *exercises = [self _getExercises: data];
    NSDictionary *item = @{@"BookId": bookNameAsFolder, @"Exercises": exercises};
    [newBookArray addObject:item];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newBookArray]
                                              forKey:[self _getUserDefaultKey:kExerciseBooks]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSArray *) getBookExercises {
    NSArray *items;
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedDict = [currentDefaults objectForKey:[self _getUserDefaultKey:kExerciseBooks]];
    
    if (dataRepresentingSavedDict != nil)
    {
        NSArray *oldSavedDict = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedDict];
        if (oldSavedDict != nil)
        {
            items = oldSavedDict;
            return items;
        }
    }
    
    return nil;
}

+ (BookExercise *) getBookExercise: (NSString *) bookNameAsFolder withExerciseId: (int) exerciseId {
    BookExercise *exercise = nil;
    
    NSDictionary *exerciseData = [self getBookExerciseData:bookNameAsFolder];
    for (BookExercise *exer in [exerciseData objectForKey:@"Exercises"]) {
        if (exer.exerciseId == exerciseId) {
            exercise = exer;
            break;
        }
    }
    return exercise;
}

+ (NSDictionary *) getBookExerciseData: (NSString *) bookNameAsFolder {
    NSArray *source = [self getBookExercises];
    NSDictionary *exerciseData = nil;
    
//    for(NSDictionary *item in source)
//    {
//        if ([[item objectForKey:@"BookId"] isEqualToString:bookNameAsFolder]) {
//            exerciseData = item;
//            break;
//        }
//    }

    exerciseData = [self getBookExerciseData:bookNameAsFolder inCollection:source];
    return exerciseData;
}

+ (NSDictionary *) getBookExerciseData: (NSString *) bookNameAsFolder inCollection: (NSArray *) source {
    NSDictionary *exerciseData = nil;
    
//    for(NSDictionary *item in source)
//    {
//        NSString *bookIdentifier = [NSString stringWithFormat:@"%@", item[@"BookId"] ];
//        
//        if ([bookIdentifier isEqualToString:bookNameAsFolder]) {
//            exerciseData = item;
//            break;
//        }
//    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"BookId", bookNameAsFolder];
    NSArray *filteredList = [source filteredArrayUsingPredicate:predicate];
    if (filteredList != nil) {
        exerciseData = (NSDictionary *)[filteredList firstObject];
    }
    
    return exerciseData;
}

+ (void) removeExerciseData: (NSString *) bookNameAsFolder {
    NSArray *bookExerciseCollection = [self getBookExercises];
    NSMutableArray *newBookExerciseCollection;
    
    if (bookExerciseCollection) {
        newBookExerciseCollection = [[NSMutableArray alloc] initWithArray:bookExerciseCollection];
        
        NSDictionary *bookData = [self getBookExerciseData: bookNameAsFolder inCollection:bookExerciseCollection];
        if(bookData != nil) {
            [newBookExerciseCollection removeObject:bookData];
            [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newBookExerciseCollection]
                                                      forKey:[self _getUserDefaultKey:kExerciseBooks]];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

#pragma mark - Book Exercise Result
+ (NSArray *) getBookExerciseResults: (NSString *) bookNameAsId {
    
    NSArray *savedResults = [self getBookExerciseResults];
    //VLog(@"BookName: %@ Results: %i", bookNameAsId, [savedResults count]);
    
    if ([savedResults count] == 0) return nil;
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    for (BookExerciseResult *result in savedResults) {
        //VLog(@"%@ == %@", result.bookName, bookNameAsId);
        
        if ([result.bookName isEqualToString:bookNameAsId]) {
            [items addObject:result];
        }
    }
    
    return items;
}

+ (NSArray *) getBookExerciseResults {
    NSArray *items;
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedDict = [currentDefaults objectForKey:[self _getUserDefaultKey:kExerciseBooksResult]];
    
    if (dataRepresentingSavedDict != nil)
    {
        NSArray *oldSavedDict = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedDict];
        if (oldSavedDict != nil)
        {
            items = oldSavedDict;
            return items;
        }
    }
    
    return nil;
}

+ (void) saveBookExerciseResult: (BookExerciseResult *) data {
    NSArray *bookExercisesArray = [self getBookExerciseResults];
    NSMutableArray *newBookArray;
    
    if (bookExercisesArray) {
        newBookArray = [[NSMutableArray alloc] initWithArray:bookExercisesArray];
    } else {
        newBookArray = [[NSMutableArray alloc] init];
    }
    
    if ([newBookArray count] != 0) {
        BookExerciseResult *oldResult = [self getBookExerciseResult:data inCollection:bookExercisesArray];
        [newBookArray removeObject:oldResult];
    }
    
    [newBookArray addObject:data];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newBookArray]
                                              forKey:[self _getUserDefaultKey:kExerciseBooksResult]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void) removeExerciseResultData: (NSString *) bookNameAsFolder {
    NSArray *exerciseResultCollection = [self getBookExerciseResults];
    NSMutableArray *newBookArray = [[NSMutableArray alloc] init];
    
    for(BookExerciseResult *item in exerciseResultCollection)
    {
        if (![item.bookName isEqualToString:bookNameAsFolder]) {
            [newBookArray addObject:item];
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newBookArray]
                                              forKey:[self _getUserDefaultKey:kExerciseBooksResult]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BookExerciseResult *) getBookExerciseResult: (BookExerciseResult *) data inCollection: (NSArray *) exerciseResultCollection {
    BookExerciseResult *theBook = nil;
    
    for(BookExerciseResult *item in exerciseResultCollection)
    {
        if ([item.bookName isEqualToString:data.bookName]) {
            if (item.bookExercise.exerciseId == data.bookExercise.exerciseId) {
                theBook = item;
                break;
            }
        }
    }
    
    return theBook;
}

+ (NSDictionary *) getMedals {
    NSArray *results = [self getBookExerciseResults];
    
    int goldCount = 0, silverCount = 0, bronzeCount = 0, platinumCount = 0;
    
    for (BookExerciseResult *result in results) {
        float grade = ((float)(result.result)/(float)result.bookExercise.totalItems) * 50 + 50;
        
        //VLog(@"BookName: %@: Result: %i/%i: Grade: %0.2f", result.bookTitle, result.result, result.bookExercise.totalItems, grade);
        
        if (grade >= 85 && grade < 90) {
            bronzeCount += 1;
        } else if (grade >= 90 && grade < 95) {
            silverCount += 1;
        } else if (grade >= 95 && grade < 98) {
            goldCount += 1;
        } else if (grade >= 98) {
            platinumCount += 1;
        }
    }       
    
    NSDictionary *medals = @{@"gold":[NSNumber numberWithInt:goldCount], 
                             @"platinum":[NSNumber numberWithInt:platinumCount], 
                             @"silver": [NSNumber numberWithInt:silverCount], 
                             @"bronze": [NSNumber numberWithInt:bronzeCount]};
    return medals;
}

#pragma mark - Book Management
+ (NSString *) getAuthors: (NSArray *) authorDict {
    NSMutableString *authorList = [[NSMutableString alloc] init];
    
//    for (int i = 0; i < [authorDict count]; i++) {
//        NSDictionary *item = (NSDictionary *) [authorDict objectAtIndex:i];
//        NSString *authorText = [item objectForKey:@"author_name"];
//        
//        if (IsEmpty(authorText) || [authorText isIn:@"N/A", @"Author Unknown", @"et al", @"et.al.", nil]) {
//            continue;
//        }
//        [authorList appendFormat:@",%@", authorText];
//    }
    
    for (int i = 0; i < [authorDict count]; i++) {
        Author *item = (Author *) [authorDict objectAtIndex:i];
        NSString *authorText = item.authorName;
        
        if (IsEmpty(authorText) || [authorText isIn:@"N/A", @"Author Unknown", @"et al", @"et.al.", nil]) {
            continue;
        }
        [authorList appendFormat:@",%@", authorText];
    }
    
    NSString *authors = [NSString stringWithFormat: @"%@", authorList];
    if ([authors isEqualToString:@""]) {
        authors = @"No Author";
    }
    else {
        authors = [[authors substringFromIndex:1] copy];
    }
    
    return authors;
}

+ (BOOL) isBookOnShelf:(NSString *) currentBookId {
    NSArray *sourceLibrary = [self getDownloadedBooks];
    BOOL isFound = NO;
    
    if (sourceLibrary) {
//        for(Book *book in sourceLibrary)
//        {
//            NSString *bookIdentifier = [NSString stringWithFormat:@"%@", book.bookId];
//            
//            if ([bookIdentifier isEqualToString:currentBookId]) {
//                BOOL bookExists = [self isBookExistsAsFile:[NSString stringWithFormat:@"%@", currentBookId]];
//                if (!bookExists) {
//                    [self removeBookToCollection:currentBookId];
//                }
//                
//                isFound = bookExists;
//                break;
//            }
//        }
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"bookId", currentBookId];
        NSArray *filteredList = [sourceLibrary filteredArrayUsingPredicate:predicate];
        if (filteredList != nil) {
            BOOL bookExists = [self isBookExistsAsFile:[NSString stringWithFormat:@"%@", currentBookId]];
            if (!bookExists) {
                [self removeBookToCollection:currentBookId];
            }
            isFound = bookExists;
        }
    }
    
    return isFound;
}

+ (BOOL) isBookOnShelf:(NSString *) currentBookId thruAction:(NSString *) action {
    NSArray *sourceLibrary = nil;
    
    // TO-DO: Clarify if all books are added to book collection after downloading or importing it
    // If yes, then use [self getBookCollection] instead of [self getDownloadedBooks] and [self getImportedBooks] both for epub and pdf
    
    // Download
    if ([action isEqualToString:@"1"]) {
        sourceLibrary = [self getDownloadedBooks];
    }
    
    // Import
    if ([action isEqualToString:@"2"]) {
        sourceLibrary = [self getImportedBooks];
    }
    
    NSLog(@"sourceLibrary: %@", sourceLibrary);
    
    BOOL isFound = NO;
    if (sourceLibrary) {
//        for(Book *book in sourceLibrary)
//        {
//            NSString *bookIdentifier = [NSString stringWithFormat:@"%@", book.bookId];
//            
//            if ([bookIdentifier isEqualToString:currentBookId]) {
//                BOOL bookExists = [self isBookExistsAsFile:[NSString stringWithFormat:@"%@", currentBookId]];
//                if (!bookExists) {
//                    [self removeBookToCollection:currentBookId];
//                }
//                
//                isFound = bookExists;
//                break;
//            }
//        }
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"bookId", currentBookId];
        NSArray *filteredList = [sourceLibrary filteredArrayUsingPredicate:predicate];
        if (filteredList != nil) {
            BOOL bookExists = [self isBookExistsAsFile:[NSString stringWithFormat:@"%@", currentBookId]];
            if (!bookExists) {
                [self removeBookToCollection:currentBookId];
                
            }
            isFound = bookExists;
        }
    }
    
    return isFound;
}

+ (BOOL) isBookAdded:(NSArray *) sourceLibrary forBook:(NSDictionary *) book {
    //VLog(@"BookDict: %@", book);
    BOOL isFound = NO;
    NSString *bookIdAsString = @"";
    NSString *currentBookId = @"0";
    
    if ([[book objectForKey:@"book_id"] isKindOfClass:[NSString class]]) {
        bookIdAsString = [book objectForKey:@"book_id"];
        currentBookId = bookIdAsString;
    } else {
        bookIdAsString = [NSString stringWithFormat:@"%d", [[book objectForKey:@"book_id"] intValue]];
        currentBookId = [book objectForKey:@"book_id"];
    }
    
    if (sourceLibrary) {
        for(NSDictionary *item in sourceLibrary)
        {
            //VLog(@"item: %@", item);
            //int bookId = [bookIdAsString intValue];
            NSString *bookId = @"";
            if ([[item objectForKey:@"book_id"] isKindOfClass:[NSString class]]) {
                bookId = [item objectForKey:@"book_id"];
            } else {
                bookId = [item objectForKey:@"book_id"];
            }
            
            if ([bookId caseInsensitiveCompare:bookIdAsString] == NSOrderedSame) {
                BOOL bookExists = [self isBookExistsAsFile:bookIdAsString];
                if (!bookExists) {
                    [self removeBookToCollection:currentBookId];
                }
                
                isFound = bookExists;
                break;
            }
        }
    }
    
    return isFound;
}

+ (BOOL) isBookExistsAsFile: (NSString *) bookId {
    NSString *bookFolder = [NSString stringWithFormat:@"%@/%@", [Utils appLibraryBooksDirectory], bookId];
    
    BOOL isDir;
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:bookFolder isDirectory:&isDir];
    
    if (exists) {
        /* file exists */
        if (isDir) {
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL) isBookAdded:(NSDictionary *) book {
    NSArray *booksArray = [self getDownloadedBooks];
    return [self isBookAdded:booksArray forBook:book];
}

+ (void) addImportedBook: (Book *) book {
    NSArray *booksArray = [self getImportedBooks];
    NSMutableArray *newBookArray;
    
    if (booksArray) {
        newBookArray = [[NSMutableArray alloc] initWithArray:booksArray];
    } else {
        newBookArray = [[NSMutableArray alloc] init];
    }
    
    [newBookArray addObject:book];
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newBookArray]
                                              forKey:[self _getUserDefaultKey:kImportedBooks]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Central Book Collection
    // This will store metadata for Popup Book Info use
    [self addBookToCollection:book];
}

+ (void) removeImportedBook: (NSString *) bookTitleAsId {
    //NSString *bookPath = [Utils appLibraryBooksDirectory];
    //bookPath = [NSString stringWithFormat:@"%@/%@", bookPath, bookTitleAsId];
    //VLog(@"BookPath: %@", bookPath);
    //[[NSFileManager defaultManager] removeItemAtPath:bookPath error:nil];
    [self removeImportedBookToCollection:bookTitleAsId];
}

+ (void) addBook:(Book *) book {
    NSArray *booksArray = [self getDownloadedBooks];
    NSMutableArray *newBookArray;
    
    if (booksArray) {
        newBookArray = [[NSMutableArray alloc] initWithArray:booksArray];
    }
    else
    {
        newBookArray = [[NSMutableArray alloc] init];
    }
    
    [newBookArray addObject:book];
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newBookArray]
                                              forKey:[self _getUserDefaultKey:kDownloadedBooks]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Central Book Collection
    // This will store metadata for Popup Book Info use
    [self addBookToCollection:book];
}

+ (void) clearImportedBooks {
    NSMutableArray *newBookArray = [[NSMutableArray alloc] init];
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newBookArray]
                                              forKey:[self _getUserDefaultKey:kImportedBooks]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSArray *) getImportedBooks {
    NSArray *items;
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedDict = [currentDefaults objectForKey:[self _getUserDefaultKey:kImportedBooks]];
    
    if (dataRepresentingSavedDict != nil)
    {
        NSArray *oldSavedDict = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedDict];
        if (oldSavedDict != nil)
        {
            items = oldSavedDict;
            return items;
        }
    }
    
    return nil;
}

+ (NSArray *) getDownloadedBooks {
    NSArray *items;
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedDict = [currentDefaults objectForKey:[self _getUserDefaultKey:kDownloadedBooks]];
    
    if (dataRepresentingSavedDict != nil)
    {
        NSArray *oldSavedDict = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedDict];
        if (oldSavedDict != nil)
        {
            items = oldSavedDict;
            return items;
        }
    }
    
    return nil;
}

+ (void) removeBook:(NSString *) bookTitleAsId {
    NSString *bookPath = [Utils appLibraryBooksDirectory];
    bookPath = [NSString stringWithFormat:@"%@/%@", bookPath, bookTitleAsId];
    VLog(@"BookPath: %@", bookPath);
    [[NSFileManager defaultManager] removeItemAtPath:bookPath error:nil];
    
    //[self removeBookToCollection:[bookTitleAsId intValue]];
}

+ (void) savePurchasedBooks: (NSArray *) books {
    // 1. collect all book_id first
    NSMutableArray *uniqueBooks = [NSMutableArray array];
    NSMutableSet *seenItems = [NSMutableSet set];
    
    for (NSDictionary *item in books) {
        NSString *bookId = [item objectForKey:@"book_id"];
        if (![seenItems containsObject:bookId]) {
            [uniqueBooks addObject:item];
            [seenItems addObject:bookId];
        }
    }
    
    // sort before saving
    //VLog(@"savePurchasedBooks: %@", books);
    if (books != nil) {
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"purchase_date"
                                                      ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        books = [uniqueBooks sortedArrayUsingDescriptors:sortDescriptors];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:books]
                                              forKey:[self _getUserDefaultKey:kPurchasedBooks]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSArray *) getPurchasedBooks {
    NSArray *items = [[NSArray alloc] init];
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedDict = [currentDefaults objectForKey:[self _getUserDefaultKey:kPurchasedBooks]];
    
    if (dataRepresentingSavedDict != nil) {
        NSArray *oldSavedDict = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedDict];
        if (oldSavedDict != nil) {
            items = oldSavedDict;
        }
    }
    
    return items;
}

+ (NSArray *) getBookCollection
{
    NSArray *items = [[NSArray alloc] init];
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedDict = [currentDefaults objectForKey:[self _getUserDefaultKey:kBookCollection]];
    
    if (dataRepresentingSavedDict != nil)
    {
        NSArray *oldSavedDict = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedDict];
        if (oldSavedDict != nil)
        {
            items = oldSavedDict;
        }
    }
    
    return items;
}

+ (void) addBookToCollection : (Book *) book
{
    //    Metadata *meta = [[Metadata alloc] init];
    //    meta.dateAdded = [NSDate date];
    //    meta.isRead = NO;
    //    meta.dateLastRead = @"Unread";
    //    meta.bookId = [[book objectForKey:@"book_id"] intValue];
    //    meta.title = [book objectForKey:@"title"];
    //    meta.authors = [self getAuthors:[book objectForKey:@"authors"]];
    
    NSArray *booksArray = [self getBookCollection];
    NSMutableArray *newBookArray;
    
    NSString *authors = @"";
    authors = [self getAuthors:book.authors];
    
    NSDictionary *bookInfo = [NSDictionary dictionaryWithObjectsAndKeys:book.bookId, kBookId,
                              @"Unread", kBookDateLastRead, book.title, kBookTitle,
                              [NSDate date], kBookDateAdded, authors, kBookAuthors,
                              [NSNumber numberWithBool:NO], kBookIsRead, nil];
    
    if (booksArray) {
        newBookArray = [[NSMutableArray alloc] initWithArray:booksArray];
        [newBookArray addObject:bookInfo];
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:newBookArray];
        [[NSUserDefaults standardUserDefaults] setObject:data
                                                  forKey:[self _getUserDefaultKey:kBookCollection]];
    }
    else
    {
        newBookArray = [[NSMutableArray alloc] init];
        [newBookArray addObject:bookInfo];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newBookArray]
                                                  forKey:[self _getUserDefaultKey:kBookCollection]];
    }
    
    //[meta release];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void) updateBookToCollection: (NSString *) theBookId
{
    if (theBookId) {
        
        NSArray *bookCollection = [self getBookCollection];
        //VLog(@"updateBookToCollection: %@", bookCollection);
        
        NSDictionary *theBook = nil;
        NSDictionary *oldDictionary = nil;
        
        if(bookCollection) {
            
            for(NSDictionary *item in bookCollection)
            {
                //bookId = [[[item allKeys] objectAtIndex:0] intValue];
                
                NSString *bookIdentifier = [NSString stringWithFormat:@"%@", [item objectForKey:kBookId] ];
                
                if ([theBookId isEqualToString:bookIdentifier]) {
                    //theBook = [item objectForKey:[NSString stringWithFormat:@"%d", bookId]];
                    oldDictionary = item;
                    
                    theBook = [NSDictionary dictionaryWithObjectsAndKeys:bookIdentifier, kBookId,
                               [NSDate stringFromDate:[NSDate date] withFormat:kDateInfoFormat2], kBookDateLastRead, [item objectForKey:kBookTitle], kBookTitle,
                               [item objectForKey:kBookDateAdded], kBookDateAdded, [item objectForKey:@"authors"], kBookAuthors,
                               [NSNumber numberWithBool:YES], kBookIsRead, nil];
                    break;
                }
            }
        }
        
        // Update Item
        NSMutableArray *newBookCollection;
        
        if (bookCollection) {
            newBookCollection = [[NSMutableArray alloc] initWithArray:bookCollection];
            
            [newBookCollection removeObject:oldDictionary];
            [newBookCollection addObject:theBook];
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newBookCollection]
                                                      forKey:[self _getUserDefaultKey:kBookCollection]];
        }
        
        if(bookCollection) {
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }

}

+ (void) removeImportedBookToCollection: (NSString *) theBookId {
    NSArray *bookCollection = [self getBookCollection];
    
    NSMutableArray *newBookCollection;
    
    if (bookCollection) {
        newBookCollection = [[NSMutableArray alloc] initWithArray:bookCollection];
        
        NSDictionary *bookData = [self getBookInfoFromCollection: theBookId inCollection:bookCollection];
        [newBookCollection removeObject:bookData];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newBookCollection]
                                                  forKey:[self _getUserDefaultKey:kBookCollection]];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    bookCollection = [self getImportedBooks];
    if (bookCollection) {
        newBookCollection = [[NSMutableArray alloc] initWithArray:bookCollection];
        
        Book *bookData = [self getImportedBookInfo: theBookId inCollection:bookCollection];
        //VLog(@"removeImportedBookToCollection: %@", bookData);
        //VLog(@"Before: Import Book Count: %i", [newBookCollection count]);
        [newBookCollection removeObject:bookData];
        //VLog(@"After: Import Book Count: %i", [newBookCollection count]);
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newBookCollection]
                                                  forKey:[self _getUserDefaultKey:kImportedBooks]];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void) removeBookToCollection: (NSString *) theBookId {
    NSArray *bookCollection = [self getBookCollection];
    
    VLog(@"removeBookToCollection: %@", theBookId);
    NSMutableArray *newBookCollection;
    
    if (bookCollection) {
        newBookCollection = [[NSMutableArray alloc] initWithArray:bookCollection];
        
        NSDictionary *bookData = [self getBookInfoFromCollection: theBookId inCollection:bookCollection];
        [newBookCollection removeObject:bookData];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newBookCollection]
                                                  forKey:[self _getUserDefaultKey:kBookCollection]];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    bookCollection = [self getDownloadedBooks];
    if (bookCollection) {
        newBookCollection = [[NSMutableArray alloc] initWithArray:bookCollection];
        
        //NSDictionary *bookData = [self getDownloadedBookInfo: theBookId inCollection:bookCollection];
        Book *bookData = [self getDownloadedBookInfo: theBookId inCollection:bookCollection];
        //VLog(@"DownloadedBookData: %@", bookData);
        [newBookCollection removeObject:bookData];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newBookCollection]
                                                  forKey:[self _getUserDefaultKey:kDownloadedBooks]];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (Book *) getImportedBookInfo: (NSString *) bookId inCollection: (NSArray *) bookCollection {
    Book *theBook = nil;
    
//    for(Book *item in bookCollection)
//    {
//        NSString *bookIdentifier = [NSString stringWithFormat:@"%@", item.bookId];
//        
//        if ([bookIdentifier isEqualToString:bookId]) {
//            theBook = item;
//            break;
//        }
//    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"bookId", bookId];
    NSArray *filteredList = [bookCollection filteredArrayUsingPredicate:predicate];
    if (filteredList != nil) {
        theBook = (Book *)[filteredList firstObject];
    }
    
    return theBook;
}

+ (Book *) getDownloadedBookInfo: (NSString *) bookId inCollection: (NSArray *) bookCollection{
//    NSDictionary *theBook = nil;
//    
//    for(NSDictionary *item in bookCollection)
//    {
//        int itemBookId = [[item objectForKey:@"book_id"] intValue];
//        
//        if (itemBookId == bookId) {
//            theBook = item;
//            //VLog(@"Found!");
//            break;
//        }
//    }
    Book *theBook = nil;
    
//    for(Book *item in bookCollection)
//    {
//        NSString *bookIdentifier = [NSString stringWithFormat:@"%@", item.bookId];
//        
//        if ([bookIdentifier isEqualToString:bookId]) {
//            theBook = item;
//            break;
//        }
//    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"bookId", bookId];
    NSArray *filteredList = [bookCollection filteredArrayUsingPredicate:predicate];
    if (filteredList != nil) {
        theBook = (Book *)[filteredList firstObject];
    }
    return theBook;
}

+ (NSDictionary *) getBookInfoFromCollection: (NSString *) bookId inCollection: (NSArray *) bookCollection  {
    if (bookCollection == nil) {
        bookCollection = [self getBookCollection];
    }
    //NSArray *bookCollection = [self getBookCollection];
    NSDictionary *theBook = nil;
    
//    for(NSDictionary *item in bookCollection)
//    {
//        NSString *itemBookId = [NSString stringWithFormat:@"%@",[item objectForKey:kBookId]];
//        
//        if ([itemBookId isEqualToString:bookId]) {
//            theBook = item;
//            break;
//        }
//    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kBookId, bookId];
    NSArray *filteredList = [bookCollection filteredArrayUsingPredicate:predicate];
    if (filteredList != nil) {
        theBook = (NSDictionary *)[filteredList firstObject];
    }
    
    return theBook;
}

+ (void) saveCurrentSpineState: (NSString *) bookIdentifer forSpineData: (NSDictionary *) spineData {
    //VLog(@"bookID: %@", bookIdentifer);
    NSString *key = [NSString stringWithFormat:@"%@_%@", kCurrentSpineState, bookIdentifer];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:spineData forKey:key];
    [userDefaults synchronize];
}

+ (NSDictionary *) getCurrentSpineState: (NSString *) bookIdentifer {
    NSString *key = [NSString stringWithFormat:@"%@_%@", kCurrentSpineState, bookIdentifer];
    NSDictionary *spineData = [[NSUserDefaults standardUserDefaults] dictionaryForKey:key];
    return spineData;
}

+ (void) saveTempHighlightNote: (NSDictionary *) highlight {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:highlight forKey:@"HIGHLIGHT_TEST"];
    [userDefaults synchronize];
}

+ (NSDictionary *) getTempHighlightNote {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *highlight = (NSDictionary *)[userDefaults valueForKey:@"HIGHLIGHT_TEST"];
    return highlight;
}

+(JMLockInOutBook*)getAssessments:(NSString*)bookId{
    NSDictionary *dictLockIO = [[NSUserDefaults standardUserDefaults] getCustomObjectForKey:kLockIOResponseKey];
    JSONModelError *error;
    
    JMLockInOutResponse *resp = [[JMLockInOutResponse alloc] initWithDictionary:dictLockIO error:&error];
    
    
    for (JMLockInOutBook *book in resp.books) {
        
        if ([book.bookUuid isEqualToString:bookId])
            return book;
    }
    
    return nil;
}

+(NSString*)getAssessmentsAsJSONforExerJS:(NSString*)bookId optionUnlock:(BOOL)unlock {
    
/*
    NSDictionary *dictLockIO = [[NSUserDefaults standardUserDefaults] getCustomObjectForKey:kLockIOResponseKey];
    NSLog(@"dictionary lockIO : %@", dictLockIO);
    
    JSONModelError *error;
    
    if (! [[dictLockIO objectForKey:@"books"] isKindOfClass:[NSArray class]]) {
        return nil;
    }
    
    JMLockInOutResponse *resp = [[JMLockInOutResponse alloc] initWithDictionary:dictLockIO error:&error];
    if (resp == nil) {
        return nil;
    }
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (JMLockInOutBook *book in resp.books) {
        
        if (![book.bookUuid isEqualToString:bookId]) {
            continue;
        }
        
        //get the first section only
        JMLockInOutBookData *data = book.data[0];
        for (JMLockInOutBookExercise *exer in data.exercises) {
            
            if ([[VSmart sharedInstance].account.user.position isEqualToString:kModeIsTeacher])
            {
                exer.isOpened = [NSNumber numberWithInt:1];
            }
            else
            {
                //Defensive Programming
                if (exer.isOpened == nil) {
                    exer.isOpened = [NSNumber numberWithInt:0];
                }
            
//                NOTE: <dc:lock>
//                don't do anything if imported books are in <dc:unlock>false</dc:unlock>
                if (unlock) {
                    exer.isOpened = [NSNumber numberWithInt:1];
                }

                NSDictionary *dict = @{@"exer_id": exer.exerId, @"isOpened": exer.isOpened};
                [array addObject:dict];
            }
        }
        
    }
*/
    
    NSMutableArray *array = [NSMutableArray array];
    
    NSArray *cloudDataExercises = [[VibeDataManager sharedInstance] fetchCloudBookExercises:bookId];
    
    for (CloudBookExercise *exer in cloudDataExercises) {
        
        BOOL isOpened = NO;
        
        isOpened = ([exer.lock boolValue]) ? NO : YES;
        
        if ([[VSmart sharedInstance].account.user.position isEqualToString:kModeIsTeacher]) {
            isOpened = YES;
        }
        
        NSDictionary *dict = @{@"exer_id": exer.exerciseId, @"isOpened": [NSNumber numberWithBool:isOpened]};
        [array addObject:dict];
        
    }
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:kNilOptions error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    return jsonString;
}

+(BOOL)validateAssessment:(NSString*)bookId withExerciseId:(NSString*)exerciseId withCode:(NSString*)code{
    
//    NSDictionary *dictLockIO = [[NSUserDefaults standardUserDefaults] getCustomObjectForKey:kLockIOResponseKey];
//    JSONModelError *error;
//    
//    JMLockInOutResponse *resp = [[JMLockInOutResponse alloc] initWithDictionary:dictLockIO error:&error];
//    
//    BOOL returnValue = NO;
//    
//    if (resp == nil) {
//        return NO;
//    }
//
//    for (JMLockInOutBook *book in resp.books) {
//        
//        if (![book.bookUuid isEqualToString:bookId])
//            continue;
//        
//        for (JMLockInOutBookData *data in book.data) {
//            
//            for (JMLockInOutBookExercise *exer in data.exercises) {
//                
//                if (!exer.isOpened)
//                    exer.isOpened = [NSNumber numberWithInt:0];
//                
//                if ([exer.exerId isEqualToString:exerciseId]){
//                    if ([exer.code isEqualToString:code]){
//                        exer.isOpened = [NSNumber numberWithInt:1];
//                        returnValue = YES;
//                    }
//                }
//            }
//        }
//    }
//    
//    if (!returnValue){
//        return returnValue;
//    }
//    
//    NSDictionary *dict = [resp toDictionary];
//    
//    [[NSUserDefaults standardUserDefaults] setCustomObject:dict forKey:kLockIOResponseKey];

    BOOL returnValue = NO;
    NSArray *cloudDataExercises = [[VibeDataManager sharedInstance] fetchCloudBookExercises:bookId];
    for (CloudBookExercise *exer in cloudDataExercises) {
        
//        NSLog(@"XXXXXX parameter exercise id: [%@]", exerciseId);
//        NSLog(@"XXXXXX parameter exercise code: [%@]", code);
//        NSLog(@"XXXXXX parameter CloudBookExercise exercise id : [%@]", exer.exerciseId);
//        NSLog(@"XXXXXX parameter CloudBookExercise exercise pass : [%@]", exer.password);
        
        if ([exer.exerciseId isEqualToString:exerciseId] && [exer.password isEqualToString:code] ) {
            NSManagedObjectContext *ctx = exer.managedObjectContext;
            exer.lock = [NSNumber numberWithBool:NO];
            [ctx refreshObject:exer mergeChanges:YES];
            NSError *error = nil;
            if (![ctx save:&error]) {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            }
            
            returnValue = YES;
            break;
        }
    }
    
    return returnValue;
}

+(NSArray*)loadLastUserDefaultProfile{
    NSString *userKey = [NSString stringWithFormat:@"%@%d", kUserDefaultLastProfileInfo, [VSmart sharedInstance].account.user.id];
    NSArray *record = [[NSUserDefaults standardUserDefaults] getCustomObjectForKey:userKey];
    return record;
}

+(void)saveLastUserDefaultProfile:(NSArray*)record{
    NSString *userKey = [NSString stringWithFormat:@"%@%d", kUserDefaultLastProfileInfo, [VSmart sharedInstance].account.user.id];
    [[NSUserDefaults standardUserDefaults] setCustomObject:record forKey:userKey];
}

#pragma mark - Private Methods
+ (NSString *) _getUserDefaultKey: (NSString *) baseKey {
    AccountInfo *account = [[VSmart sharedInstance] account];
//    NSString *userKey = VS_FMT(@"%@_%i", [account.user.email uppercaseString], account.user.id);
    NSString *userKey = VS_FMT(@"%@", [account.user.email uppercaseString]);
    return VS_FMT(@"%@%@", userKey, baseKey);
}

+(NSString*)aliasFromFirstName:(NSString*)firstName andLastName:(NSString*)lastName{
    
    if ([firstName length] == 0) {
        firstName = @" ";
    }
    
    if ([lastName length] == 0) {
        lastName = @" ";
    }
    
    NSString *returnValue = [NSString stringWithFormat:@"%@%@", [firstName substringToIndex:1], [lastName substringToIndex:1]];
    
    returnValue = [returnValue uppercaseString];
    
    return returnValue;
}

+ (NSString *)getDeviceLocale
{
    /* NOTE: get the current device language selected by the user in the Settings Application
     the return value will be used as parameter for localization for web components
     such as calendar school stream, social stream, test guru
     */
    
    return [[NSLocale preferredLanguages] firstObject];
}

+ (NSString *)getVersionCode
{
    NSString *version_string = @"Chrysalis,1.0.8,20150626";
    NSString *versionFilePath = [[NSBundle mainBundle] pathForResource:@"VERSION" ofType:nil];
    if (versionFilePath) {
        NSString *versionContent = [NSString stringWithContentsOfFile:versionFilePath encoding:NSUTF8StringEncoding error:NULL];
        version_string = [NSString stringWithFormat:@"%@", versionContent];
    }
    return version_string;
}

+ (void) pullCurrentSections {
    AccountInfo *account = [[VSmart sharedInstance] account];
    NSString *urlString = [Utils buildUrl:VS_FMT(kEndPointGetSections, account.user.id)];
    
    VLog(@"PULL_CURRENT_SECTIONS: %@", urlString);
    [JSONHTTPClient getJSONFromURLWithString:urlString completion:^(NSDictionary *json, JSONModelError *err) {
        if(err == nil){
            
            NSArray *arrayRecords = [json objectForKey:@"records"];
            VLog(@"PULL_CURRENT_SECTIONS_DATA: %@", arrayRecords);
            if ([arrayRecords count] > 0) {
                NSMutableArray *sections = [[NSMutableArray alloc] init];
                for (NSDictionary *item in arrayRecords) {
                    NSString *sectionId = [item objectForKey:@"section_id"];
                    [sections addObject:sectionId];
                }
                NSString *jsonSections = [sections JSONString];
                VLog(@"JSON_PULL_CURRENT_SECTIONS: %@", jsonSections);
                [Utils saveSections: jsonSections];
                //VLog(@"PULL_CURRENT_SECTIONS: %@", [Utils getSections]);
            }
        }
    }];
}

#pragma mark - DRM Manager

+ (void)saveDRMinfoForBook:(NSString *)uuid drmversion:(NSString *)drmversion {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%@", [Utils getUserId] ];
    NSString *user_name = [NSString stringWithFormat:@"%@", account.user.email];
    NSString *book_uuid = [NSString stringWithFormat:@"%@", uuid];
    NSString *drm_version = [NSString stringWithFormat:@"%@", drmversion];
    
    NSDictionary *data = @{@"userid": user_id,
                           @"username": user_name,
                           @"uuid": book_uuid,
                           @"drmversion": drm_version};
    
    [[DRMManager sharedInstance] insertBookData:data];
}

+ (NSDictionary *)getDRMInfo:(NSString *)uuid {
    
    NSDictionary *info = nil;
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_name = [NSString stringWithFormat:@"%@", account.user.email];
    NSString *book_uuid = [NSString stringWithFormat:@"%@", uuid];
    
    NSDictionary *data = @{@"username": user_name,
                           @"uuid": book_uuid};
    
    DRMManager *dm = [DRMManager sharedInstance];
    NSDictionary *d = [dm getBookData:data];
    if (d) {
        
        NSLog(@"%s %@", __PRETTY_FUNCTION__, d);
        
        info = [NSDictionary dictionaryWithDictionary: d ];
        
        
    }
    
    return info;
}

+ (NSString *)getUserIdWithBookUUID:(NSString *)string {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSString *userId = [Utils getUserId];
    
    NSDictionary *d = [VSmartHelpers getDRMInfo:string];
    if (d) {
        userId = [NSString stringWithFormat:@"%@", d[@"userid"] ];
        NSLog(@"using DRM logged user : %@", userId);
        return userId;
    }
    NSLog(@"using ONLINE log user : %@", userId);
    
    return userId;
}

@end
