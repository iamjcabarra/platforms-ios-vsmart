//
//  VSmartMacros.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/16/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, VSmartDownloadBookStatus) {
    VSmartDownloadedBookUnread = 1,
    VSmartDownloadedBookRead
};

// compiler help
#define STRING_IS_EMPTY_OR_NIL( _STRING ) ( _STRING == nil || [_STRING isEmptyOrWhitespace] )

// http://www.wilshipley.com/blog/2005/10/pimp-my-code-interlude-free-code.html
static inline BOOL IsEmpty(id thing) {
    return thing == nil ||
    ([thing isEqual:[NSNull null]]) ||
    ([thing respondsToSelector:@selector(length)] && [(NSData *)thing length] == 0) ||
    ([thing respondsToSelector:@selector(count)] && [(NSArray *)thing count] == 0);
}

static NSString *kQuizResultPerfectScore = @"perfect_score";
static NSString *kQuizResultAverageScore = @"average_score";
static NSString *kQuizResultFailedScore = @"failed_score";

#define VS_ENABLE_TAP(view, delegate, selector) do {\
view.userInteractionEnabled = YES;\
[view addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:delegate action:selector]];\
} while(0)

// collection shortcuts
#define VS_DEFAULT(_value, _default) ([[NSNull null] isEqual:(_value)] ? (_default) : (_value))
#define VS_FMT(...) [NSString stringWithFormat: __VA_ARGS__]
#define VS_ARRAY(...) [NSArray arrayWithObjects: __VA_ARGS__, nil]
#define VS_DICT(...) [NSDictionary dictionaryWithObjectsAndKeys: __VA_ARGS__, nil]
#define VS_MARRAY(...) [NSMutableArray arrayWithObjects: __VA_ARGS__, nil]
#define VS_MDICT(...) [NSMutableDictionary dictionaryWithObjectsAndKeys: __VA_ARGS__, nil]

// notification center
#define VS_NC [NSNotificationCenter defaultCenter]
#define VS_NCADD(n,sel) [[NSNotificationCenter defaultCenter] addObserver:self selector:sel name:n object:nil];
#define VS_NCREMOVE [[NSNotificationCenter defaultCenter] removeObserver:self];
#define VS_NCPOST(name) [[NSNotificationCenter defaultCenter] postNotificationName:name object:self];
#define VS_NCPOST_OBJ(name,obj) [[NSNotificationCenter defaultCenter] postNotificationName:name object:obj];

// user defaults
#define VS_DEL_OBJECT(k) [[NSUserDefaults standardUserDefaults] removeObjectForKey:k];
#define VS_GET_OBJECT(v) [[NSUserDefaults standardUserDefaults] objectForKey:v]
#define VS_SET_OBJECT(k,v) [[NSUserDefaults standardUserDefaults] setObject:v forKey:k];
#define VS_SET_CUSTOM_OBJECT(k,v) [[NSUserDefaults standardUserDefaults] setCustomObject:v forKey:k];
#define VS_GET_CUSTOM_OBJECT(v) [[NSUserDefaults standardUserDefaults] getCustomObjectForKey:v]
#define VS_GET_STRING(v) [[NSUserDefaults standardUserDefaults] stringForKey:v]
#define VS_SET_STRING(k,v) [[NSUserDefaults standardUserDefaults] setObject:v forKey:k];
#define VS_GET_FLOAT(v) [[NSUserDefaults standardUserDefaults] floatForKey:v]
#define VS_SET_FLOAT(k,v) [[NSUserDefaults standardUserDefaults] setFloat:v forKey:k];
#define VS_GET_BOOL(v) [[NSUserDefaults standardUserDefaults] boolForKey:v]
#define VS_SET_BOOL(k,v) [[NSUserDefaults standardUserDefaults] setBool:v forKey:k];
#define VS_GET_INT(v) [[NSUserDefaults standardUserDefaults] integerForKey:v]
#define VS_SET_INT(k,v) [[NSUserDefaults standardUserDefaults] setInteger:v forKey:k];
#define VS_SYNC [[NSUserDefaults standardUserDefaults] synchronize];

