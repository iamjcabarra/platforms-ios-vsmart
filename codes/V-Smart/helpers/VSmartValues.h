//
//  VSmartValues.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/28/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

// Temp Data
// earljon@outlook.com token
#define kToken @"MDAwMDAwMTllYXJsam9uQG91dGxvb2suY29tMDAwMDAwMzIyMGExZmRiN2UyYzJlYjA2YzVlNzIyN2IzOWI4MzMzNTQ1MzI5MTg3ODQwMDA="

#define VSMART_SERVER_MAX_VER       2.5

#define DRM_SALT_KEY                @"_vibetech2012_drm_v1_"
#define DRM_KEY                     [NSString stringWithFormat:@"%@%@%@", @"%@", DRM_SALT_KEY, @"%@"]

// Parse Analytics & Push Notifications
#define kParseApplicationId         @"dgsGBfZaO47rCsjyWhvtkDso96gXbTjtAvKXgdx8"
#define kParseClientKey             @"lUQkqmYIjVGIriWQUA0oNpQ5pFbkMx2BRt0bs8aJ"

// For Keychain access
#define kSystemUsername             @"VSMARTUSER"
#define kSystemServiceName          @"VSMARTIOS"

#define kShowTestImportWindow       0

// Fonts
#define FONT_NEUE(s) [UIFont fontWithName:@"HelveticaNeue" size:s]
#define FONT_NEUE_LIGHT(s) [UIFont fontWithName:@"HelveticaNeue-Light" size:s]
#define FONT_NEUE_THIN(s) [UIFont fontWithName:@"HelveticaNeue-Thin" size:s]
#define FONT_NEUE_DEFAULT [UIFont fontWithName:@"HelveticaNeue" size:15.0]

//static const int DOWNLOAD_BUTTON = 1, DOWNLOAD_INDICATOR = 2, DOWNLOAD_STATUS_LABEL = 3, DOWNLOAD_PROGRESS = 4, DOWNLOAD_STATIC_LABEL = 5;
#define kDownloadButton             1
#define kDownloadIndicator          2
#define kDownloadStatusLabel        3
#define kDownloadProgress           4
#define kDownloadStaticLabel        5
#define kDownloadStatusFileSizeLabel        6

// Settings
#define kProfileHeight              @"SETTINGS_PROFILE_HEIGHT"
#define kServerVersionFlag          @"SERVER_VERSION_FLAG"
#define kServerIstanceVersion       @"SERVER_INSTANCE_VERSION"
#define kBookmarks                  @"BOOKMARKS"
#define kBookDictionary             @"BOOKDICT"
#define kBookname                   @"BOOKNAME"
#define kChapterArray               @"CHAPTER"
#define kChapterDictionary          @"CHAPTER_ROOT"
#define kChapterName                @"CHAPTER_NAME"
#define kChapterTextSize            @"CHAPTER_TEXT_SIZE"
#define kChapterSpineIndex          @"CHAPTER_SPINE_INDEX"
#define kChapterPageCountInSpine    @"CHAPTER_PAGE_COUNT_IN_SPINE"
#define kChapterPageIndexInSpine    @"CHAPTER_PAGE_INDEX_IN_SPINE"

#define kCurrentSpineState          @"CURRENT_SPINE_STATE"
#define kCurrentSpineIndex          @"CURRENT_SPINE_INDEX"
#define kCurrentPageInSpineIndex    @"CURRENT_PAGE_IN_SPINE_INDEX"

// Medals
#define kMedalPlatinum              @"platinum"
#define kMedalGold                  @"gold"
#define kMedalSilver                @"silver"
#define kMedalBronze                @"bronze"

// Modules
#define kModuleDashboard            @"Dashboard"
#define kModuleHome                 @"Home"

//#define kModuleTextbooks            @"Textbooks"
//#define kModuleGradebook            @"Gradebook"
//#define kModuleNotes                @"Notes"
//#define kModuleSocialStream         @"Social Stream"
//#define kModuleSubjects             @"Subjects"
//#define kModuleSchoolStream         @"School Stream"
//#define kModuleCalendar             @"Calendar"
//#define kModulePlaylists            @"Playlists"
//#define kModuleEducationalApps      @"Educational Apps"
//#define kModuleClasses              @"Classes"
//#define kModuleClassAnalytics       @"Class Analytics"
//#define kModuleQuizGuru             @"Test Guru"
//#define kModuleAwardBadges          @"Award Badges"
//#define kModuleLessonPlan           @"Lesson Plan"
//#define kModuleCurriculumPlanner    @"Curriculum Planner"
//#define kModuleCourses              @"COURSES"

#define kModuleTextbooksJumpIcon            @"icn_jump_menu_textbooks"
#define kModuleGradebookJumpIcon            @"icn_jump_menu_gradebook"
#define kModuleNotesJumpIcon                @"icn_jump_menu_notes"
#define kModuleSocialStreamJumpIcon         @"icn_jump_menu_social-stream"
#define kModuleSubjectsJumpIcon             @"icn_jump_menu_subject"
#define kModuleSchoolStreamJumpIcon         @"icn_jump_menu_school-stream"
#define kModuleCalendarJumpIcon             @"icn_jump_menu_calendar"
#define kModulePlaylistsJumpIcon            @"icn_jump_menu_playlist"
#define kModuleEducationalAppsJumpIcon      @"icn_jump_menu_educational-apps"
#define kModuleClassesJumpIcon              @"icn_jump_menu_subject"
#define kModuleClassAnalyticsJumpIcon       @"icn_jump_menu_class-analytic"
#define kModuleQuizGuruJumpIcon             @"icn_jump_menu_quiz-guru"
#define kModuleAwardBadgesJumpIcon          @"icn_jump_menu_award-badges"
#define kModuleHomeJumpIcon                 @"icn_jump_menu_home"

// States
#define kLoginState                         @"LOGIN_STATE"

// Objects
#define kGlobalAccount                      @"GLOBAL_ACCOUNT"
#define kGlobalBooks                        @"GLOBAL_BOOKS_%@"
#define kPreviousAccount                    @"PREVIOUS_ACCOUNT"

// Notifications
#define kNotificationQueueDownload          @"NOTIF_QUEUE_DOWNLOAD"
#define kNotificationUpdateQueue            @"NOTIF_UPDATE_QUEUE"
#define kNotificationJumpMenuSelection      @"NOTIF_JUMP_MENU_SELECTION"
#define kNotificationProfileHeight          @"NOTIF_PROFILE_HEIGHT"
#define kNotificationProfileReload          @"NOTIF_RELOAD_AVATAR"
#define kNotificationGlobalReadUpdates      @"NOTIF_GLOBAL_READ_UPDATES"
#define kNotificationTextbookShelfSelection @"NOTIF_TEXTBOOK_AVERAGE_SELECTION"
//BUG FIX 104
#define kNotificationHideProfileView        @"NOTIF_HIDE_PROFILE_VIEW"

//RECEIVED NEW MESSAGE // BUG FIX
#define kNotificationReceivedNewGroupMessage        @"NOTIF_RECEIVED_NEW_GROUP_MESSAGE"
#define kNotificationReturnGroupIdResourceManager   @"NOTIF_RETURN_GROUP_ID_RESOURCE_MANAGER"
#define kNotificationReceivedRealTimeDataLike       @"NOTIF_RECEIVED_REAL_TIME_LIKE"
#define kNofiticationBookDownloadError              @"NOTIF_BOOK_DOWNLOAD_ERROR"

// Account Mode
#define kModeIsStudent              @"student"
#define kModeIsTeacher              @"teacher"

// Book Info
#define kBookPageHash               @"PAGE_HASH"
#define kBookStateRead              @"Read"
#define kBookStateDelete            @"Delete"
#define kBookDateAdded              @"BOOK_DATE_ADDED"
#define kBookDateLastRead           @"BOOK_DATE_LAST_READ"
#define kBookId                     @"BOOK_ID"
#define kBookIsRead                 @"BOOK_ISREAD"
#define kBookTitle                  @"BOOK_TITLE"
#define kBookAuthors                @"BOOK_AUTHORS"
#define kDownloadedBooks            @"DOWNLOADED_BOOKS"
#define kRecentPhotos               @"RECENT_PHOTOS"
#define kExerciseBooks              @"EXERCISE_BOOKS"
#define kExerciseBooksResult        @"EXERCISE_BOOKS_RESULT"
#define kImportedBooks              @"IMPORTED_BOOKS"
#define kPurchasedBooks             @"PURCHASED_BOOKS"
#define kImportedSamplers           @"IMPORT_SAMPLER"
#define kSingleBookDownloadedNotification            @"SINGLE_BOOK_DOWNLOADED_NOTIFICATION"
#define kSingleBookDownloadProgressNotification            @"SINGLE_BOOK_DOWNLOAD_PROGRESS_NOTIFICATION"
#define kMainRotateNotification     @"MAIN_VIEW_ROTATE_NOTIFICATION"
#define kBookstoreNotification      @"BOOKSTORE_NOTIFICATION"
#define kBookListNotification      @"BOOKLIST_NOTIFICATION"
#define kLoginCloseNotification      @"CLOSE_LOGIN_NOTIFICATION"
#define kLoginBindNotification      @"BOOKSTORE_LOGINBIND_NOTIFICATION"
#define kNoteDataNotification       @"BOOKSTORE_NOTEDATA_NOTIFICATION"
#define kImportServerPostUpdate     @"IMPORT_SERVER_POST_UPDATE"
#define kImportServerDictionary     @"IMPORT_SERVER_DICTIONARY"
#define kVSmartConnectivityNotif    @"VSMART_CONNECTIVITY_NOTIFICATION"
#define kReaderIsActive             @"READER_IS_ACTIVE"
#define kBookCollection             @"BOOKINFO_COLLECTION"
#define kAppName                    @"V-Smart"
#define kDownloadedFolder           @"Downloaded"
#define kDateInfoFormat             @"EEE. MMM dd, ''yy"
#define kDateInfoFormat2            @"dd MMM yyyy hh:mm a"
#define kImportServerName           @"Vibe Reader Uploader"
#define kImportServerPassword       @"IMPORT_SERVER_PASSWORD"
#define kImportLabelHeader          @"To transfer books between a computer on your local network and \"%@\", open a web browser and go to:"

#define kBookExtensionVibe          @"vibe"
#define kBookExtensionEpub          @"epub"
#define kBookExtensionPdf           @"pdf"

#define kFetchListMessage           @"Getting books from the server. \nPlease wait..."
#define kFetchListErrorMessage      @"Error fetching books. Please try again."
#define kPendingDownloadMessage     @"Please wait to finish your download(s)."
#define kLoadingBookMessage         @"Loading book"
#define kRemovingBookMessage        @"Removing book"
#define kDatePurchasedMessage        @"Date Purchased"

#define kHighlightRed               @"Red"
#define kHighlightGreen             @"Green"
#define kHighlightBlue              @"Blue"
#define kHighlightYellow            @"Yellow"
#define kHighlightNote              @"Note"
#define kHighlightDelete            @"Delete"

#define kTabTableOfContents         @"TABLE OF CONTENTS"
#define kTabBookmarks               @"BOOKMARKS"
#define kTabDrawings                @"DRAWINGS"
#define kTabAnnotations             @"ANNOTATIONS"
#define kTabExercises               @"EXERCISES"
#define kTabEncyclopedia            @"ENCYCLOPEDIA ENTRIES"
#define kTabFigures                 @"LIST OF FIGURES"
#define kTabMedia                   @"AUDIO & VIDEO FILES"

#define VSMART_BOOK_URL BOOK_SERVICE
#define VSMART_BASE_URL APP_SERVER
#define VSMSRT_API_URL API_SERVICE

// Profile Tab
#define kProfileTabBasic            @"Basic"
#define kProfileTabOther            @"Other Info"
#define kProfileTabSchools          @"Schools"
#define kProfileTabStatistics       @"Statistics"

// Social Media URL
#define kSocialInstagram            @"http://www.instagram.com/%@"
#define kSocialFacebook             @"http://www.facebook.com/%@"
#define kSocialTwitter              @"http://www.twitter.com/%@"

#pragma mark - Common
#define kEndPointGetEmojis @"/vsmart-rest-dev/v1/stream/getemojis"
#define kEndPointGetEmoticons @"/vsmart-rest-dev/v1/stream/getemoticons"
#define kEndPointGetStickers @"/vsmart-rest-dev/v1/stream/getstickers"
#define kEndPointGetGroups @"/vsmart-rest-dev/v1/stream/getusergroups/%i"
#define kEndPointGetGroupMessage @"/vsmart-rest-dev/v1/stream/getgroupmessages/%@"
#define kEndPointGetSections @"/vsmart-rest-dev/v1/sections/list/%i"

#pragma mark - User Profile
#define kEndPointGetUserProfile @"/vsmart-rest-dev/v1/shortcut/getuserprofile/%i"
#define kEndPointUserProfile @"/vsmart-rest-dev/v1/shortcut/getuserprofile/%@"

//#define kEndPointAvatarUpload @"/profile?id=%i"
#define kEndPointAvatarUpload @"/vsmart-rest-dev/v1/users/%i/avatarupload"
#define kEndPointAvatarUploadV2 @"/vsmart-rest-dev/v1/users/%@/avatarupload"
#define kEndPointUpdateProfile @"/vsmart-rest-dev/v1/users/update/%i"
#define kEndPointGetGenericAvatars @"/vsmart-rest-dev/v1/stream/getavatars"

#pragma mark - Book API
#define kEndPointBooklist @"/book?authKey=%@"

#pragma mark - Module API
#define kEndPointDashboardModule @"/vsmart-rest-dev/v1/v1/module/enabled"

#pragma mark - Service URLs
#define kServiceURLOAuthToken VS_FMT(@"http://%@/oauth2/token", VSMART_BOOK_URL)
#define kServiceURLUserInfo VS_FMT(@"http://%@/oauth2/userinfo", VSMART_BOOK_URL)
#define kServiceURLBind VS_FMT(@"http://%@/device/bind", VSMART_BOOK_URL)
#define kServiceURLBookList VS_FMT(@"http://%@/storeapi/product/orders", VSMART_BOOK_URL)
#define kServiceURLServerVersion VS_FMT(@"http://%@/storeapi/application/check", VSMART_BOOK_URL)
#define kEndPointServiceInstanceVersion VS_FMT(@"http://%@/version/get", VSMSRT_API_URL)
#define kServiceURLRequestDL VS_FMT(@"http://%@/storeapi/product/download/sku/", VSMART_BOOK_URL)
#define kServiceURLRequestDLB VS_FMT(@"http://%@/storeapi/product/downloadbook/sku/", VSMART_BOOK_URL)

#pragma mark - Login
#define kEndPointLogin @"/vsmart-rest-dev/v1/users/login"
#define kEndPointLoginAsGuest @"/vsmart-rest-dev/v1/users/guest"

#pragma mark - LockInOut
#define kEndPointLockInOutTeacher @"/vsmart-rest-dev/v1/textbooks/subjects/list/teacher_textbook/%i"
#define kEndPointLockInOutStudent @"/vsmart-rest-dev/v1/textbooks/subjects/list/student_textbook/%i"

#pragma mark - Notebooks
#define kEndPointGetAllNotebooksByUserId @"/vsmart-rest-dev/v1/notebooks/getAll/%d"
//#define kEndPointGetNotebookByUserIdAndId @"/vsmart-rest-dev/v1/notebooks/get/%d/%d"
//#define kEndPointGetNotebookById @"/vsmart-rest-dev/v1/notebooks/getById/%d"
//#define kEndPointGetNotebookByName @"/vsmart-rest-dev/v1/notebooks/getByName/%@"
#define kEndPointAddNotebook @"/vsmart-rest-dev/v1/notebooks/add"
#define kEndPointEditNotebookById @"/vsmart-rest-dev/v1/notebooks/editById"
#define kEndPointDeleteNotebookById @"/vsmart-rest-dev/v1/notebooks/deleteById"

#pragma mark - Tags
#define kEndPointGetAllTagsByUserId @"/vsmart-rest-dev/v1/user_tags/getAll/%d"
//#define kEndPointGetTagByUserIdAndId @"/vsmart-rest-dev/v1/user_tags/get/%d/%d"
//#define kEndPointGetTagById @"/vsmart-rest-dev/v1/user_tags/getById/%d"
//#define kEndPointGetTagByName @"/vsmart-rest-dev/v1/user_tags/getByName/%@"
#define kEndPointAddTag @"/vsmart-rest-dev/v1/user_tags/add"
#define kEndPointEditTagById @"/vsmart-rest-dev/v1/user_tags/editById"
#define kEndPointDeleteTagById @"/vsmart-rest-dev/v1/user_tags/deleteById"

#pragma mark - Notes
#define kEndPointGetAllNotesByUserId @"/vsmart-rest-dev/v1/user_notes/getAll/%d"
//#define kEndPointGetNoteByUserIdAndId @"/vsmart-rest-dev/v1/user_notes/get/%d/%d"
//#define kEndPointGetNoteById @"/vsmart-rest-dev/v1/user_notes/getById/%d"
//#define kEndPointGetNoteByName @"/vsmart-rest-dev/v1/user_notes/getByTitle/%@"
#define kEndPointAddNote @"/vsmart-rest-dev/v1/user_notes/add"
#define kEndPointEditNoteById @"/vsmart-rest-dev/v1/user_notes/editById"
#define kEndPointDeleteNoteById @"/vsmart-rest-dev/v1/user_notes/deleteById"

#pragma mark - Note Tag
#define kEndPointGetNoteTagByUserId @"/vsmart-rest-dev/v1/note_tag_jt/getAll/%d"
//#define kEndPointGetNoteTagByUserIdAndId @"/vsmart-rest-dev/v1/note_tag_jt/get/%d/%d"
//#define kEndPointGetNoteTagById @"/vsmart-rest-dev/v1/note_tag_jt/getById/%d"
//#define kEndPointGetNoteTagByNoteId @"/vsmart-rest-dev/v1/note_tag_jt/getByNoteId/%d"
//#define kEndPointGetNoteTagByTagId @"/vsmart-rest-dev/v1/note_tag_jt/getByTagId/%d"
#define kEndPointAddNoteTag @"/vsmart-rest-dev/v1/note_tag_jt/add"
#define kEndPointEditNoteTagById @""
#define kEndPointDeleteNoteTagById @"/vsmart-rest-dev/v1/note_tag_jt/deleteById"

#pragma mark - Calendar
#define kEndPointCalendar @"/vs.calendar/calendar.html#/%d"
//#define kEndPointCalendarLocalize @"/calendar.html#/?mobile=1&userid=%i&role=%i&lang=%@"
#define kEndPointCalendarLocalize @"/calendar?user_id=%d&type_id=%d&is_mobile=1"

#pragma mark - Subjects
#define kEndPointSubjects @"/vsmart-class/subjects?id=%i"

#pragma mark - School Stream
//#define kEndPointSchoolStream @"/socialstream/%i/1?lang=%@"
#define kEndPointSchoolStream @"/schoolstream?is_mobile=1&user_id=%d&type_id=%d"
#define kEndPointPostSchoolStream @"/vsmart-rest-dev/v1/stream/postmessage"

#pragma mark - Social Stream
#define kEndPointSocialStream @"/socialstream/%i/%i"
#define kEndPointSocialStreamLocalize @"/socialstream/%i/%i?lang=%@"
#define kEndPointPostSocialStream @"/vsmart-rest-dev/v1/stream/postmessage"
#define kEndPointPostEditSocialStream @"/vsmart-rest-dev/v1/stream/putmessage/%@"
#define kEndPointRemoveSocialStream @"/vsmart-rest-dev/v1/stream/removemessage/%@"
#define kEndPointGetBadWords @"/vsmart-rest-dev/v1/stream/getbadwords"
#define kEndPointPostEditCommentSocialStream @"/vsmart-rest-dev/v1/stream/putcomment/%@"
#define kEndPointPostRemoveCommentSocialStream @"/vsmart-rest-dev/v1/stream/removecomment/%@"

#define kEndPointPostCommentSocialStream @"/vsmart-rest-dev/v1/stream/postcomment"
#define kEndPointPostSocialStreamLikeMessage @"/vsmart-rest-dev/v1/stream/postlike"
#define kEndPointPostSocialStreamUnLikeMessage @"/vsmart-rest-dev/v1/stream/removelike/%@"

#pragma mark - Test Player
#define kEndPointCourseList @"/vsmart-rest-dev/v1/courses/user/list/%@"
#define kEndPointQuizList @"/vsmart-rest-dev/v1/quiz/list/myquizzes/%@/%@"
#define kEndPointPreRunQuizDetail @"/vsmart-rest-dev/v1/quiz/prerun/%@/%@/%@"
#define kEndPointStartQuiz @"/vsmart-rest-dev/v1/quiz/run/%@/%@/%@"
#define kEndPointSubmitQuiz @"/vsmart-rest-dev/v1/quiz/add/attempt/answers/%@/%@/%@"
#define kEndPointStudentList @"/vsmart-rest-dev/v1/courses/studentsbycsid/%@"
#define kEndPointStudentProfile @"/vsmart-rest-dev/v1/users/%@/profile"
#define kEndPointStudentListv2 @"/vsmart-rest-dev/v2/courses/studentsbycsid/%@"
#define kEndPointSeatingArrangement @"/vsmart-rest-dev/v2/courses/seatplan/cs_id/%@"
#define kEndPointChangeAttendanceStatus @"/vsmart-rest-dev/v2/courses/mark_attendance"
#define kEndPointChangeSeatingOrder @"/vsmart-rest-dev/v2/courses/assign/seatplan/cs_id/%@/teacher_id/%@"

#pragma mark - Test Guru
#define kEndPointTestGuruQuestionCount @"/vsmart-rest-dev/v1/shortcut/question_count/%@"
#define kEndPointTestGuruTestCount @"/vsmart-rest-dev/v1/shortcut/quiz_count/%@"
#define kEndPointTestGuruDeployedTestCount @"/vsmart-rest-dev/v1/shortcut/quiz_deployed_count/%@"
#define kEndPointTestGuruImageUploadForUserID @"/vsmart-rest-dev/v1/quiz/upload/%@"

//#define kEndPointTestGuruPackageType @"/vsmart-rest-dev/v1/quiz/packageType"
#define kEndPointTestGuruPackageType @"/vsmart-rest-dev/v2/quizzes/package"
#define kEndPointTestGuruCourseList @"/vsmart-rest-dev/v2/courses/list/user/%@?limit=%@&current_page=%@&search_keyword=%@"
#define kEndPointTestGuruDashboardStatistics @"/vsmart-rest-dev/v2/quizzes/stats/course/%@/package/%@/user/%@"
#define kEndPointTestGuruQuestionType @"/vsmart-rest-dev/v2/quizzes/questions/types"


#define kEndPointTestGuruQuestionFilterOptions @"/vsmart-rest-dev/v2/quizzes/%@"


//#define kEndPointTestGuruQuestionType @"/vsmart-rest-dev/v1/quiz/list/qtypes" //Dead
#define kEndPointTestGuruGradedType @"/vsmart-rest-dev/v1/quiz/list/stages"
#define kEndPointTestGuruResultType @"/vsmart-rest-dev/v1/quiz/list/resulttype"
#define kEndPointTestGuruQuizCategory @"/vsmart-rest-dev/v1/quiz/list/categories"
#define kEndPointTestGuruTestType @"/vsmart-rest-dev/v1/quiz/list/quiz/types"
#define kEndPointTestGuruProficiencyLevel @"/vsmart-rest-dev/v1/quiz/list/proficiency"
#define kEndPointTestGuruDifficultyLevel @"/vsmart-rest-dev/v1/quiz/list/difficulty"
#define kEndPointTestGuruLearningSkill @"/vsmart-rest-dev/v1/quiz/list/learning"
#define kEndPointTestGuruQuestionList @"/vsmart-rest-dev/v1/quiz/question/list/%@"

#define kEndPointTestGuruQuestionListByTags @"/vsmart-rest-dev/v1/quiz/question/group/tags/%@"
#define kEndPointTestGuruQuestionListByQuestionType @"/vsmart-rest-dev/v1/quiz/question/group/types/%@"
#define kEndPointTestGuruQuestionListByDifficultyType @"/vsmart-rest-dev/v1/quiz/question/group/difficultylevel/%@"
#define kEndPointTestGuruQuestionListByLearningSkill @"/vsmart-rest-dev/v1/quiz/question/group/learningskill/%@"
#define kEndPointTestGuruQuestionListBySharedStatus @"/vsmart-rest-dev/v1/quiz/question/group/sharedstatus/%@"

#define kEndPointTestGuruQuestionDetails @"/vsmart-rest-dev/v1/quiz/question/info/%@/%@"
#define kEndPointTestGuruQuestionUpdate @"/vsmart-rest-dev/v1/quiz/question/update/%@"
#define kEndPointTestGuruQuestionNew @"/vsmart-rest-dev/v1/quiz/question/new/%@"

//NEW CREATE QUESTION
#define kEndPointTestGuruQuestionNewV2 @"/vsmart-rest-dev/v2/quizzes/questions/add/%@"
#define kEndPointTestGuruQuestionUpdateV2 @"/vsmart-rest-dev/v2/quizzes/questions/update/%@"

//#define kEndPointTestGuruQuestionRemove @"/vsmart-rest-dev/v1/quiz/question/delete/%@/%@"
#define kEndPointTestGuruQuestionDeleteV2 @"/vsmart-rest-dev/v2/quizzes/questions/delete/%@/user/%@"

#define kEndPointTestGuruTestList @"/vsmart-rest-dev/v1/quiz/listAll/%@"

#define kEndPointTestGuruTestListByTags @"/vsmart-rest-dev/v1/quiz/group/tags/%@"
#define kEndPointTestGuruTestListByCategory @"/vsmart-rest-dev/v1/quiz/group/category/%@"
#define kEndPointTestGuruTestListByTestType @"/vsmart-rest-dev/v1/quiz/group/types/%@"
#define kEndPointTestGuruTestListByGradeRating @"/vsmart-rest-dev/v1/quiz/group/stages/%@"

//TODO
//#define kEndPointTestGuruTestListByRating @"/vsmart-rest-dev/v1/quiz/question/group/sharedstatus/%@"

#define kEndPointTestGuruTestRemove @"/vsmart-rest-dev/v1/quiz/delete/%@"
#define kEndPointTestGuruTestDetails @"/vsmart-rest-dev/v1/quiz/info/%@"
#define kEndPointTestGuruTestUpdate @"/vsmart-rest-dev/v1/quiz/update/%@"
#define kEndPointTestGuruTestCreate @"/vsmart-rest-dev/v1/quiz/create/%@"
#define kEndPointTestGuruTestDeploy @"/vsmart-rest-dev/v1/quiz/deploy"
#define kEndPointTestGuruTestSections @"/vsmart-rest-dev/v1/quiz/courses/%@"
#define kEndPointTestGuruPreviewTest @"/vsmart-rest-dev/v1/quiz/preview/%@/%@"

// TOTO
//#define kEndPointTestGuruFilterQuestionBank @"/vsmart-rest-dev/v1/quiz/question/user/%@/filter/limit/%@/page/%@" // OLD
//#define kEndPointTestGuruFilterQuestionBank @"/vsmart-rest-dev/v1/quiz/question/list/filtered/package/%@/user/%@" // New dead
#define kEndPointTestGuruFilterQuestionBankV2 @"/vsmart-rest-dev/v2/quizzes/question/list/course/%@/package/%@/user/%@?limit=%@&current_page=%@&search_keyword=%@" // New
#define kEndPointTestGuruFilterQuestionBankV3 @"/vsmart-rest-dev/v2/quizzes/question/list/course/%@/package/%@/user/%@?limit=%@&current_page=%@&search_keyword=%@&sort=%@" // New

////////////////////////Group by Tags/////////////////////////
#define kEndPointTestGuruQuestionTags @"/vsmart-rest-dev/v1/quiz/question/list/tags/package/%@/user/%@" //GET TAGS DEAD
#define kEndPointTestGuruQuestionsForTagId @"/vsmart-rest-dev/v1/quiz/question/list/package/%@/user/%@/tag/%@" // DEAD

//#define kEndPointTestGuruQuestionGroupByV2 @"/vsmart-rest-dev/v2/quizzes/question/list/%@/course/%@/package/%@/user/%@" //GET Headers
//#define kEndPointTestGuruQuestionsForIdV2 @"/vsmart-rest-dev/v2/quizzes/question/list/course/%@/package/%@/user/%@/%@/%@" //GET Question for headers

#define kEndPointTestGuruQuestionGroupByV2 @"/vsmart-rest-dev/v2/quizzes/question/list/%@/course/%@/package/%@/user/%@?limit=%@&current_page=%@" //GET Headers
#define kEndPointTestGuruQuestionsForIdV2 @"/vsmart-rest-dev/v2/quizzes/question/list/course/%@/package/%@/user/%@/%@/%@?limit=%@&current_page=%@&search_keyword=%@" //GET Question for headers

//////////////////////////Group by Question Types/////////////////////////
//#define kEndPointTestGuruQuestionTypes @"/vsmart-rest-dev/v1/quiz/question/list/types/package/%@/user/%@" //GET QT
//#define kEndPointTestGuruQuestionsForQTId @"/vsmart-rest-dev/v1/quiz/question/list/package/%@/user/%@/type/%@" //question for qt id
//
//////////////////////////Group by Difficulty levels/////////////////////////
//#define kEndPointTestGuruDifficultyLevels @"/vsmart-rest-dev/v1/quiz/list/difficulty/package/%@/user/%@" // GET DL
//#define kEndPointTestGuruQuestionsForDLId @"/vsmart-rest-dev/v1/quiz/question/list/package/%@/user/%@/difficultylevel/%@" //question of dl id
//
//////////////////////////Group by Learning Skills/////////////////////////
//#define kEndPointTestGuruLearningSkills @"/vsmart-rest-dev/v1/quiz/list/learningskills/package/%@/user/%@" // GET LS
//#define kEndPointTestGuruQuestionsForLSId @"/vsmart-rest-dev/v1/quiz/question/list/package/%@/user/%@/learningskill/%@" //question of ls id
//
//////////////////////////Group by Shared Status/////////////////////////
//#define kEndPointTestGuruSharedStatus @"/vsmart-rest-dev/v1/quiz/question/list/sharedstatus/package/%@/user/%@" // GET SS
//#define kEndPointTestGuruQuestionsForSSId @"/vsmart-rest-dev/v1/quiz/question/list/package/%@/user/%@/sharedstatus/%@" //question of ss id

#pragma mark - Test Bank v2.0
#define kEndPointTestCategoryList  @"/vsmart-rest-dev/v2/quizzes/categories"
#define kEndPointTestResultTypeList @"/vsmart-rest-dev/v2/quizzes/result_types"
#define kEndPointTestTypeList @"/vsmart-rest-dev/v2/quizzes/types"
#define kEndPointTestDifficultyLevelList @"/vsmart-rest-dev/v2/quizzes/proficiency"
#define kEndPointTestLearningSkillList @"/vsmart-rest-dev/v2/quizzes/learning"
#define kEndPointTestCreate @"/vsmart-rest-dev/v1/quiz/create/%@"//@"/vsmart-rest-dev/v2/quizzes/add"
#define kEndPointTestUpdateV1 @"/vsmart-rest-dev/v1/quiz/update/%@"
#define kEndPointTestUpdate @"/vsmart-rest-dev/v2/quizzes/%@/edit"
#define kEndPointTestDelete @"/vsmart-rest-dev/v2/quizzes/%@/remove"

#pragma mark - Curriculum Planner
#define kEndPointCurriculumPlannerList @"/vsmart-rest-dev/v1/curriculum/list/curriculum/approved"
#define kEndPointCurriculumDetails @"/vsmart-rest-dev/v1/curriculum/%@/details"

#pragma mark - Lesson Plan
#define kEndPointLessonPlanList @"/vsmart-rest-dev/v1/learningplan/template/learningplan/user/%@"
#define kEndPointLessonPlanDetails @"/vsmart-rest-dev/v1/learningplan/template/learningplan/id/%@"
#define kEndPointLessonPlanTemplateList @"/vsmart-rest-dev/v1/learningplan/templates"
#define kEndPointLessonPlanTemplateDetails @"/vsmart-rest-dev/v1/learningplan/template/%@"
#define kEndPointLessonPlanUpdateStatus @"/vsmart-rest-dev/v1/learningplan/template/update/status/%@"
#define kEndPointLessonPlanAddComment @"/vsmart-rest-dev/v1/learningplan/template/add/comment/%@"
#define kEndPointLessonPlanEditComment @"/vsmart-rest-dev/v1/learningplan/template/edit/comment"
#define kEndPointLessonPlanDeleteComment @"/vsmart-rest-dev/v1/learningplan/template/delete/comment/%@"
#define kEndPointLessonPlanCreate @"/vsmart-rest-dev/v1/learningplan/template/create"
#define kEndPointLessonPlanUpdate @"/vsmart-rest-dev/v1/learningplan/template/update/%@"
#define kEndPointLessonPlanUpdateFile @"/vsmart-rest-dev/v1/learningplan/template/update/file/%@"
#define kEndPointLessonPlanDelete @"/vsmart-rest-dev/v1/learningplan/template/delete/learningplan/%@"
#define kEndPointLessonPlanDownload @"/vsmart-rest-dev/v1/learningplan/template/download/pdf/%@"
#define kEndPointCurriculumApprovedList @"/vsmart-rest-dev/v1/curriculum/list/curriculum/approved"
#define kEndPointCurriculumDetails @"/vsmart-rest-dev/v1/curriculum/%@/details"
#define kEndPointCurriculumLearningCompetencies @"/vsmart-rest-dev/v1/curriculum/list/learningcompetencies/period/%@"
#define kEndPointLessonPlanPaginatedCourseList @"/vsmart-rest-dev/v2/courses/list/user/%@?limit=%@&current_page=%@&search_keyword=%@&sort=%@"
#define kEndPointAssociatedCurriculumToLessonPlan @"/vsmart-rest-dev/v2/curriculum/list/lessonplan/%@"

#pragma mark - Grade Book
#define kEndPointGradeList @"/vsmart-rest-dev/v1/assessment/user/%@/%@"
#define kEndPointPostGradeAssessment @"/vsmart-rest-dev/v1/assessment/book"

#pragma mark - Play List
#define kEndPointPlayList @"/vsmart-rest-dev/v1/playlist/list/%@"
#define kEndPointPlayListUpload @"/vsmart-rest-dev/v1/playlist/upload/%@"
#define kEndPointPlayListUpdateMeta @"/vsmart-rest-dev/v1/playlist/update_meta/%@"
#define kEndPointDownLoadPlayListItem @"/vsmart-rest-dev/v1/playlist/download/%@"
#define kEndPointPlayListDelete @"/vsmart-rest-dev/v1/playlist/%@/delete"
#define kEndPointPlayListSearchUser @"/vsmart-rest-dev/v2/users/search/name/%@/user_id/%@"
#define kEndPointPlayListTeacherList @"/vsmart-rest-dev/v1/playlist/teacher_list/user_id/%@"


#pragma mark - Notifcations 
#define kEndPointGlobalNotification @"/vsmart-rest-dev/v1/notification/%@/%@"
#define kEndPointNotificationDetail @"/vsmart-rest-dev/v1/stream/getmessage/%@"

#pragma mark - Smart Shortcut - Calendar
#define kEndPointSSCalendar @"/vsmart-rest-dev/v1/shortcut/getusercalendarevents/%i"

#pragma mark - Smart Shortcut - People
#define kEndPointGetSectionStudentTeachers @"/vsmart-rest-dev/v1/shortcut/getsectionteachers/%i"
#define kEndPointGetSectionStudentClassmates @"/vsmart-rest-dev/v1/shortcut/getsectionstudents/%i"
#define kEndPointGetSectionTeacherStudents @"/vsmart-rest-dev/v1/shortcut/getsectionteachers/%i"
#define kEndPointGetSectionTeacherPeers @"/vsmart-rest-dev/v1/shortcut/getsectionstudents/%i"
#define kEndPointGetPeopleInfo @"/vsmart-rest-dev/v1/shortcut/getuserpeople/%i"
#define kEndPointGetPeopleForUser @"/vsmart-rest-dev/v1/shortcut/getuserpeople/%@"

#pragma mark - Change Password
#define kEndPointChangePasswordForUser @"/vsmart-rest-dev/v1/users/changepwd"

#pragma mark - Gradebook
//#define kEndPointGradebookTeacher @"/gradebook.html#/overview?userid=%i&role=0&mobile=1&lang=%@"
#define kEndPointGradebookTeacher @"/gradebook.html#/?mobile=1&userid=%i&role=0&lang=%@"

//#define kEndPointGradebookStudent @"/gradebook-student.html#/overview?userid=%i&role=1&mobile=1&lang=%@"
#define kEndPointGradebookStudent @"/gradebook-student.html#/?mobile=1&userid=%i&role=1&lang=%@"
#define kEndPointClassGradebook @"/vsmart-rest-dev/v1/assessment/class/%@"
#define kEndPointGBCourseList       @"/vsmart-rest-dev/v1/quiz/courses/%@"

#pragma mark - Test
//#define kEndPointTestGuru @"/quiz-guru-editor.html#/question-bank/?mobile=1&role=0&userid=%i&lang=%@"
#define kEndPointTestGuru @"/quiz-guru-editor.html#/cs?mobile=1&userid=%i&role=0&lang=%@"

#pragma mark - Lesson Plan
//#define kEndPointLessonPlan @"/vsmart-class/lesson-plan.html?id=%i"
#define kEndPointLessonPlan @"/vsmart-class/lesson-plan.html?id=%i&lang=%@"

#pragma Other Declarations
#define kUserDefaultFontSizeKey @"DefaultFontSize"
#define kUserDefaultFontNameKey @"DefaultFontName"
#define kUserDefaultLastProfileInfo @"LastProfileInfo"
#define kOAuthResponseKey @"OAuthResponseKey"
#define kBindResponseKey @"BindResponseKey"
#define kLockIOResponseKey @"LockIOResponseKey"

#pragma mark - Curriculum Planner v2.0
#define kEndPointCourseCategoryList @"/vsmart-rest-dev/v2/courses/category/user/%@"
#define kEndPointCurriculumList @"/vsmart-rest-dev/v2/curriculum/list/course_category/%@/user_id/%@"
#define kEndPointCurriculumPDF @"/vsmart-rest-dev/v1/curriculum/%@/download/pdf"
#define kEndPointCurriculumDetail @"/reports/curriculuminfo/"

// GRADEBOOK TESTING
#define kEndPointGradeBook                          @"/vsmart-rest-dev/v1/assessment/class/%@"
#define kEndPointGradeBookChangeScore               @"/vsmart-rest-dev/v1/assessment/update/score/%@/%@"
#define kEndPointGradeBookSetScore               @"/vsmart-rest-dev/v1/assessment/set/score/"
