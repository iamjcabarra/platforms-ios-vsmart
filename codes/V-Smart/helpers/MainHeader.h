//
//  MainHeader.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/15/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "UIDevice+IdentifierAddition.h"
#import "SVProgressHUD.h"
#import "NSDate+Helper.h"
#import "MBProgressHUD.h"
#import "NSString+Additions.h"
#import "UIColor+MoreColors.h"
#import "CategoryDefines.h"
#import "VSmartMacros.h"
#import "VSmartValues.h"
#import "VSmartMessages.h"
#import "Helpers.h"
#import "VSmart.h"
#import "SVPullToRefresh.h"
#import "JSONHTTPClient.h"
#import "JSONKit.h"
#import "VibeDataManager.h"
#import "CloudModel.h"
#import "CloudDataSettings.h"
#import "RIButtonItem.h"
#import "JSQSystemSoundPlayer.h"
#import "ProgressHUD.h"
#import "ResourceManager.h"

#define UIColorFromRGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define UIColorFromHex(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//RGB color macro with alpha
#define UIColorFromHexWithAlpha(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue &0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#define IS_LANDSCAPE [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight

#define ARC4RANDOM_MAX 0×100000000
#define RND ((double)arc4random() / ARC4RANDOM_MAX);

#define DEFAULT_FONT_PERCENTAGE 100
#define APP_VERSION [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey]

#define BOOK_SERVICE [Utils getBookService]
#define APP_SERVER [Utils getVibeServer]
#define API_SERVICE [Utils getServiceAPI]

// IMAGES
#define VSMART_LOGO [UIImage imageNamed:@"icn_vsmart"]
#define IMAGE_DEFAULT_PROFILE [UIImage imageNamed:@"default-photo"]
#define IMAGE_HEADER [UIImage imageNamed:@"icn_top_header"]

// TOOLBARS
#define TOOLBAR_CALENDAR [UIImage imageNamed:@"icn_toolbar_calendar"]
#define TOOLBAR_NOTIFICATION [UIImage imageNamed:@"icn_toolbar_notification"]
#define TOOLBAR_IDEAPAD [UIImage imageNamed:@"icn_toolbar_ideapad"]
#define TOOLBAR_HOME [UIImage imageNamed:@"icn_toolbar_home"]
#define TOOLBAR_PROFILE [UIImage imageNamed:@"icn_toolbar_profile"]
#define TOOLBAR_SETTINGS [UIImage imageNamed:@"icn_toolbar_setting"]