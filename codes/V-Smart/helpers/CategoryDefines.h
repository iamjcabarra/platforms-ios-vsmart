//
//  CategoryDefines.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/16/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIView+Additions.h"
#import "UIButton+Additions.h"
#import "UIImage+Addons.h"
#import "NSUserDefaults+Helpers.h"
#import "Toast+UIView.h"