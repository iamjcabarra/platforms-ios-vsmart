//
//  DRMManager.h
//  V-Smart
//
//  Created by Ryan Migallos on 9/3/15.
//  Copyright (c) 2015 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

static NSString *kDRMEntity = @"DRMTable";

/////// BLOCK TYPES //////
typedef void (^DRMManagerDoneBlock)(BOOL status);
typedef void (^DRMManagerDataBlock)(NSDictionary *data);

@interface DRMManager : NSObject

@property (nonatomic, readonly) NSManagedObjectContext *mainContext;

+ (instancetype)sharedInstance;
- (void)saveContext;
- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate;
- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value;
- (NSManagedObject *)getEntity:(NSString *)entity attribute:(NSString *)attribute parameter:(NSString *)parameter context:(NSManagedObjectContext *)context;

/////// PUBLIC FUNCTIONS ///////
- (void)insertBookData:(NSDictionary *)bookInfo;
- (NSDictionary *)getBookData:(NSDictionary *)info;

@end