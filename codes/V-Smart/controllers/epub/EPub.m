//
//  EPub.m

#import "EPub.h"
#import "ZipArchive.h"
#import "Chapter.h"
#import "EpubParser.h"
#import "NCXElement.h"

@interface EPub()

- (void) parseEpub;
- (void) parseMetadata;
- (void) unzipAndSaveFileNamed:(NSString*)fileName;
- (NSString*) applicationDocumentsDirectory;
- (NSString*) parseManifestFile;
- (void) parseOPF:(NSString*)opfPath;
- (NSString *) getElementValue: (CXMLDocument *) parentXml withNodePath: (NSString *) nodePath;

@end

@implementation EPub

@synthesize spineArray, tocArray, encryptedFilesArray;
@synthesize publisher, authors, title, rootPath, coverContent;
@synthesize opfFilePath, coverImagePath, isFixedLayout, coverPageUrl, uuid;
@synthesize baseDirectory, authorsList, isUnlock, isValid;
@synthesize drmVersion;

- (id) initWithEPubPath:(NSString *)path{
	if((self=[super init])){
        epubFilePath = [NSString stringWithFormat:@"%@", path];
		spineArray = [[NSMutableArray alloc] init];
        self.opfFilePath = [self parseManifestFile];
        self.baseDirectory = [self.opfFilePath stringByDeletingLastPathComponent];
//        NSLog(@"BASEDIR_FILE_PATH: %@", self.baseDirectory);
        [self populateEpubVersion];
		[self parseEpub];
        [self parseRightsWithParser:epubFilePath];
	}
	return self;
}

- (id) initWithBookname:(NSString*)bookname
{
	if((self=[super init])){
        epubFilePath = [NSString stringWithFormat:@"%@", bookname];
		//[self parseMetadata];
        self.opfFilePath = [self parseManifestFile];
        [self populateEpubVersion];
        [self parseOPF];
        [self parseRightsWithParser:epubFilePath];
	}
	return self;
}

- (void) parseMetadata
{
//    self.rootPath = @"";
//    NSString* opfPath = [self parseManifestFile];
//    //CXMLDocument* opfFile = [[CXMLDocument alloc] initWithContentsOfURL:[NSURL fileURLWithPath:opfPath] options:0 error:nil];
//    CXMLDocument* opfFile = [[CXMLDocument alloc] initWithContentsOfURL:[NSURL fileURLWithPath:opfPath] encoding:NSISOLatin1StringEncoding options:0 error:nil];
//    
//    self.title = [self getElementValue:opfFile withNodePath:@"title"];
//    self.publisher = [self getElementValue:opfFile withNodePath:@"publisher"];
//    self.authors = [self getElementValue:opfFile withNodePath:@"creator"];
//    self.coverContent = [self getElementValueForCover:opfFile withNodePath:@"meta"];
//    
//    [opfFile release];
}

- (NSString *) getElementValueForCover: (CXMLDocument *) parentXml withNodePath: (NSString *) nodePath {
    
//    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
//                          @"http://purl.org/dc/elements/1.1/",
//                          @"dc",
//                          @"http://www.idpf.org/2007/opf",
//                          @"opf",
//                          nil];
    NSDictionary *dict = @{@"dc": @"http://purl.org/dc/elements/1.1/", @"opf": @"http://www.idpf.org/2007/opf"};
    ////*[@style]
    NSString *xpath = @"//*[@name='cover']";
    //[NSString stringWithFormat: @"//dc:%@[@name]", nodePath]
    NSArray* metaArray = [parentXml nodesForXPath:xpath namespaceMappings:dict error:nil];
    //VLog(@"metaArray: %i", [metaArray count]);
    
    if ([metaArray count] == 0) {
        return @"";
    }
    else
    {
        //VLog(@"metaArray: %@", metaArray);
        CXMLElement* element = [metaArray objectAtIndex:0];
        //VLog(@"Value: %@", [element stringValue]);
        return [[element attributeForName:@"content"] stringValue];
    }
}

- (NSString *) getElementValue: (CXMLDocument *) parentXml withNodePath: (NSString *) nodePath
{
    NSArray* metaArray = [parentXml nodesForXPath:[NSString stringWithFormat: @"//dc:%@", nodePath] namespaceMappings:[NSDictionary dictionaryWithObject:@"http://purl.org/dc/elements/1.1/" forKey:@"dc"] error:nil];
    
    if ([metaArray count] == 0) {
        return @"";
    }
    else
    {
        CXMLElement* element = [metaArray objectAtIndex:0];
        //VLog(@"Value: %@", [element stringValue]);
        return [element stringValue];
    }
}

- (void) parseEpub{
	//[self unzipAndSaveFileNamed:epubFilePath];
    
	//NSString* opfPath = [self parseManifestFile];
	[self parseOPF:self.opfFilePath];
}

- (void) _buildAuthors {
    NSMutableArray *theAuthors = [[NSMutableArray alloc] init];
    NSArray *csvAuthors = [self.authors componentsSeparatedByString:@","];
    
    for (NSString *authorItem in csvAuthors) {
        Author *author = [[Author alloc] init];
        author.authorName = authorItem;
        [theAuthors addObject:author];
    }
    
    self.authorsList = [NSArray arrayWithArray:theAuthors];
}

- (NSString *) OPFRootPath {
    NSString *opfRoothPath = self.opfFilePath;
    return [opfRoothPath stringByDeletingLastPathComponent];
}

- (void)unzipAndSaveFileNamed:(NSString*)fileName{
	
//	ZipArchive* za = [[ZipArchive alloc] init];
//	if( [za UnzipOpenFile:epubFilePath]){
//		NSString *strPath=[NSString stringWithFormat:@"%@/UnzippedEpub",[self applicationDocumentsDirectory]];
//        
//		NSFileManager *filemanager=[[NSFileManager alloc] init];
//		if ([filemanager fileExistsAtPath:strPath]) {
//			NSError *error;
//			[filemanager removeItemAtPath:strPath error:&error];
//		}
//		[filemanager release];
//		filemanager=nil;
//		//start unzip
//		BOOL ret = [za UnzipFileTo:[NSString stringWithFormat:@"%@/",strPath] overWrite:YES];
//		if( NO==ret ){
//			// error handler here
//			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error"
//														  message:@"Error while unzipping the epub"
//														 delegate:self
//												cancelButtonTitle:@"OK"
//												otherButtonTitles:nil];
//			[alert show];
//			[alert release];
//			alert=nil;
//		}
//		[za UnzipCloseFile];
//	}
//	[za release];
}

- (NSString *)applicationDocumentsDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

- (void) populateEpubVersion {
    NSData *xmlData = [NSData dataWithContentsOfFile:opfFilePath];
    TBXML *tbxml = [TBXML tbxmlWithXMLData:xmlData];
    if (tbxml.rootXMLElement) {
        NSString *epubVersion = [TBXML valueOfAttributeNamed:@"version" forElement:tbxml.rootXMLElement];
        self.version = [epubVersion floatValue];
    }
}

- (void) parseOPF {
    NSData *xmlData = [NSData dataWithContentsOfFile:opfFilePath];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLData:xmlData];
    if (tbxml.rootXMLElement)
    {
        TBXMLElement *metaElement = [TBXML childElementNamed:@"metadata" parentElement:tbxml.rootXMLElement];
        [self traverseManifestElement:metaElement];
    }
}

- (void)parseRightsWithParser:(NSString *)epubfile {
    
    self.drmVersion = @"1"; //default
    
    //Check First Rights Files
    NSString *rightsFilePath = [NSString stringWithFormat:@"%@/%@/META-INF/rights.xml", [Utils appLibraryBooksDirectory], epubfile];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:rightsFilePath] == YES) {
        NSData *xmlData = [NSData dataWithContentsOfFile:rightsFilePath];
        if (xmlData == nil) {
            NSLog(@"%s: No rights XML file found", __PRETTY_FUNCTION__);
            return;
        }
        
        TBXML *tbxml = [TBXML tbxmlWithXMLData:xmlData];
        if (tbxml.rootXMLElement) {
            TBXMLElement *root = tbxml.rootXMLElement;
            NSString *elementName = [TBXML elementName:root];
            if ([elementName isEqualToString:@"rights"]) {
            //  <rights xmlns="http://www.vibebookstore.com/xmlrights-epub" version="1">
                self.drmVersion = [NSString stringWithFormat:@"%@", [TBXML valueOfAttributeNamed:@"version" forElement:root] ];
                NSLog(@"DRM VERSION : %@", self.drmVersion);
                
            }
        }
    }
}

- (void) traverseManifestElement:(TBXMLElement *)element {
    do {
        if (element->firstChild) {
            [self traverseManifestElement:element->firstChild];
        }
        
        // Metadata Start
        if ([[TBXML elementName:element] isEqualToString:@"dc:title"]) {
            self.title = [TBXML textForElement:element];
            if(!self.title) {
                // try other encoding
                self.title = [TBXML textForElement:element withEncoding:NSISOLatin1StringEncoding];
            }
            self.title = [self.title stringByDecodingXMLEntities];
            //VLog(@"Title: %@", self.title);
        }
        
        if ([[TBXML elementName:element] isEqualToString:@"dc:magazine"]) {
            NSString *fixedLayout = [TBXML textForElement:element];
            //VLog(@"FixedLayout: %@", fixedLayout);
            self.isFixedLayout = [[fixedLayout lowercaseString] isIn:@"true", @"1", nil] ? YES : NO;
        }

        self.isUnlock = YES;
        if ([[TBXML elementName:element] isEqualToString:@"dc:lock"]) {
            NSString *dataIsUnlock = [TBXML textForElement:element];
            self.isUnlock = [[dataIsUnlock lowercaseString] isIn:@"true", @"1", nil] ? NO : YES;
        }
        
        if ([[TBXML elementName:element] isEqualToString:@"dc:creator"]) {
            self.authors = [TBXML textForElement:element];
            if (!self.authors) {
                self.authors = [TBXML textForElement:element withEncoding:NSISOLatin1StringEncoding];
            }
            [self _buildAuthors];
            //VLog(@"Authors: %@", self.authors);
        }

        if ([[TBXML elementName:element] isEqualToString:@"dc:identifier"]) {
            self.uuid = [TBXML textForElement:element];
            if (!self.uuid) {
                self.uuid = [TBXML textForElement:element withEncoding:NSISOLatin1StringEncoding];
            }
            //VLog(@"UUID: %@", self.uuid);
        }
        
        if ([[TBXML elementName:element] isEqualToString:@"dc:publisher"]) {
            self.publisher = [TBXML textForElement:element];
            if (!self.publisher) {
                self.publisher = [TBXML textForElement:element withEncoding:NSISOLatin1StringEncoding];
            }
            
            self.publisher = [self.publisher stringByDecodingXMLEntities];
            //self.publisher = [NSString stringWithCString:[self.publisher cStringUsingEncoding:NSUTF8StringEncoding]
            //                   encoding:NSNonLossyASCIIStringEncoding];
        }
        
        if ([[TBXML elementName:element] isEqualToString:@"meta"]) {
            NSString *metaCover = [TBXML valueOfAttributeNamed:@"content" forElement:element];
            NSString *metaName = [TBXML valueOfAttributeNamed:@"name" forElement:element];
            if ([metaName isIn:@"cover", nil]) {
                self.coverContent = metaCover;
                //VLog(@"Cover Content: %@", self.coverContent);
            }
        }
        // Metadata End
        
        // Guide Start
        if ([[TBXML elementName:element] isEqualToString:@"reference"]) {
            NSString *guideType = [TBXML valueOfAttributeNamed:@"type" forElement:element];
            //NSString *itemHref = [TBXML valueOfAttributeNamed:@"href" forElement:element];
            if ([[guideType lowercaseString] isIn:@"cover", nil]) {
                // check if href content is type of image
//                if ([[[itemHref lowercaseString] pathExtension] isIn:@"jpeg", @"jpg", @"png", nil]) {
//                    self.coverPageUrl = [TBXML valueOfAttributeNamed:@"href" forElement:element];
//                }
                self.coverPageUrl = [[TBXML valueOfAttributeNamed:@"href" forElement:element] lastPathComponent];
                //VLog(@"Cover Page: %@", self.coverPageUrl);
            }
        }
        // Guide End
        
        if ([[TBXML elementName:element] isEqualToString:@"item"]) {
            NSString *itemId = [TBXML valueOfAttributeNamed:@"id" forElement:element];
            NSString *itemHref = [TBXML valueOfAttributeNamed:@"href" forElement:element];
            
            if (!self.coverContent) {
                // Important: temporary placeholder so that coverContent will not be nil!
                self.coverContent = @"vibe-image";
            }
            if([[itemId lowercaseString] isIn:@"cover", @"cover.jpg", @"cover.jpeg", @"xcover-jpeg", @"cover_fmt", self.coverContent, @"cover-image", nil]) {
                NSString *mediaType = [TBXML valueOfAttributeNamed:@"media-type" forElement:element];
                //VLog(@"itemId: %@ | Cover Image Path: %@", itemId, itemHref);
                
                if ([mediaType isIn:@"image/jpeg", @"image/jpg", @"image/png", nil]) {
                    self.coverImagePath = itemHref;
                    //VLog(@"Cover Image Path: %@", self.coverImagePath);
                    //break;
                }
            }
        }
    } while ((element = element->nextSibling));
}

//- (NSString*) parseManifestFile{
//    NSString *booksDirectory = [Utils appLibraryBooksDirectory];
//
//    NSString* manifestFilePath = [NSString stringWithFormat:@"%@/%@/META-INF/container.xml", booksDirectory, epubFilePath];
//	NSFileManager *fileManager = [NSFileManager defaultManager];
//	if ([fileManager fileExistsAtPath:manifestFilePath]) {
//	    NSURL *fileURL = [NSURL fileURLWithPath:manifestFilePath];
//	    CXMLDocument* manifestFile = [[CXMLDocument alloc] initWithContentsOfURL:fileURL options:0 error:nil]; //WE MANAGED THIS
//	    CXMLNode* opfPath = [manifestFile nodeForXPath:@"//@full-path[1]" error:nil];
//        
//        self.rootPath = [[opfPath stringValue] stringByDeletingLastPathComponent];
//        
//        NSString *manifestFullPath = [NSString stringWithFormat:@"%@/%@/%@", booksDirectory, epubFilePath, [opfPath stringValue]];
//        self.isValid = YES;
//        return manifestFullPath;
//	} else {
//		VLog(@"ERROR: ePub not Valid");
//        self.isValid = NO;
//		return nil;
//	}
//}

- (NSString *)parseManifestFile {
    NSString *booksDirectory = [Utils appLibraryBooksDirectory];
    NSString *manifestFilePath = [NSString stringWithFormat:@"%@/%@/META-INF/container.xml", booksDirectory, epubFilePath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:manifestFilePath]) {
        NSURL *fileURL = [NSURL fileURLWithPath:manifestFilePath];
        CXMLDocument *manifestFile = [[CXMLDocument alloc] initWithContentsOfURL:fileURL options:0 error:nil];
        CXMLNode *opfPath = [manifestFile nodeForXPath:@"//@full-path[1]" error:nil];
        NSString *manifestFullPath = [NSString stringWithFormat:@"%@/%@/%@", booksDirectory, epubFilePath, [opfPath stringValue]];
        
        self.rootPath = [[opfPath stringValue] stringByDeletingLastPathComponent];
        self.isValid = YES;
        
        return manifestFullPath;
    }
    else {
        VLog(@"ERROR: ePub not Valid");
        self.isValid = NO;
        
        return nil;
    }
}

- (NSString *) coverImage:(NSString *) bookname
{
    NSString* imageName;
//    epubFilePath = bookname;
//    NSString* opfPath = [self parseManifestFile];
//    
//    CXMLDocument* opfFile = [[CXMLDocument alloc] initWithContentsOfURL:[NSURL fileURLWithPath:opfPath] options:0 error:nil];
//	NSArray* itemsArray = [opfFile nodesForXPath:@"//opf:item" namespaceMappings:[NSDictionary dictionaryWithObject:@"http://www.idpf.org/2007/opf" forKey:@"opf"] error:nil];
//    
//    //VLog(@"CoverContent: %@", self.coverContent);
//    
//    //NSMutableDictionary* itemDictionary = [[NSMutableDictionary alloc] init];
//	for (CXMLElement* element in itemsArray) {
//		//[itemDictionary setValue:[[element attributeForName:@"href"] stringValue] forKey:[[element attributeForName:@"id"] stringValue]];
//        
//        NSString *coverId = [[[element attributeForName:@"id"] stringValue] lowercaseString];
//        
//        if([coverId isIn:@"cover", @"xcover-jpeg", @"cover_fmt", self.coverContent, @"cover-image", nil]) {
//            // check media type also
//            NSString *mediaType = [[[element attributeForName:@"media-type"] stringValue] lowercaseString];
//            if ([mediaType isIn:@"image/jpeg", @"image/jpg", @"image/png", nil]) {
//                imageName = [[element attributeForName:@"href"] stringValue];
//                
//                break;
//            }
//        }
//    }
    
    return imageName;
}

- (NSDictionary *) getChapterWithHash: (NSDictionary *) ncxMaster withChapterUrl:(NSString *) chapterUrl{
    NSDictionary *ncx = [[NSDictionary alloc] init];
    for (NSString *item in ncxMaster) {
        
        NSString *url = item;
        NSRange hashTagRange = [url rangeOfString:@"#" options:NSCaseInsensitiveSearch];
        
        if (hashTagRange.location != NSNotFound) {
            NSArray *splittedUrl = [url componentsSeparatedByString:@"#"];
            NSString *htmlName = [splittedUrl objectAtIndex:0];
            if ([chapterUrl isEqualToString:htmlName]) {
                NSDictionary *dict = (NSDictionary *)[ncxMaster valueForKey:item];
                return dict;
            }
        }
    }
    
    return ncx;
}

- (void) parseOPF:(NSString*)opfPath{
    VLog(@"ParseOPF started: %@", opfPath);
    
    NSUInteger lastSlash = [opfPath rangeOfString:@"/" options:NSBackwardsSearch].location;
	NSString* ebookBasePath = [opfPath substringToIndex:(lastSlash +1)];
    NSMutableArray* tmpArray = [NSMutableArray array];
    
    EpubParser *parser = [[EpubParser alloc] init];
    
    //Check First Encryption Files
    NSString *encryptionFilePath = [NSString stringWithFormat:@"%@/%@/META-INF/encryption.xml", [Utils appLibraryBooksDirectory], epubFilePath];
    [parser readEncryptionFile:encryptionFilePath];
    self.encryptedFilesArray = [NSArray arrayWithArray:parser.encryptedFiles];
    
    //Check First Rights Files
    [self parseRightsWithParser:epubFilePath];
//    [self parseRightsWithParser:parser];
    
//    [parser initializeBook:ebookBasePath withOPFFilePath:opfPath andEpubVersion:self.version];
    [parser initializeBook:ebookBasePath withOPFFilePath:opfPath andEpubVersion:self.version uuid:self.uuid];
    
    for (int i = 0; i < [parser.spines count]; i++) {
        NSString *idRef = [parser.spines objectAtIndex:i];
        NSString *chapUrl = [parser.spineDict valueForKey:idRef];
        
        NSDictionary *ncxDict = (NSDictionary *)[parser.ncxDict valueForKey:chapUrl];
        if (ncxDict == nil) {
            ncxDict = [self getChapterWithHash:parser.ncxDict withChapterUrl:chapUrl];
        }
        
        NSString *spinePath = [NSString stringWithFormat:@"%@%@", ebookBasePath, chapUrl];
        NSString *theTitle = [ncxDict objectForKey:@"title"];
        
        //Chapter* tmpChapter = [[Chapter alloc] init];
        Chapter* tmpChapter = [[Chapter alloc] initWithPath:spinePath title:theTitle chapterIndex:i];
        tmpChapter.title = theTitle;
        tmpChapter.spinePath = spinePath;
        //VLog(@"spinePath: %@", tmpChapter.spinePath);
        tmpChapter.ncxElement = (NCXElement *)[ncxDict objectForKey:@"NCX"];
        
        [tmpArray addObject:tmpChapter];
    }
    
    //NSArray* reversedArray = [[parser.files reverseObjectEnumerator] allObjects];
    //self.tocArray = reversedArray;
    self.tocArray = [NSArray arrayWithArray:parser.files];
    
//    NSString *encryptionFilePath = [NSString stringWithFormat:@"%@/%@/META-INF/encryption.xml", [Utils appLibraryBooksDirectory], epubFilePath];
//    [parser readEncryptionFile:encryptionFilePath];
//    self.encryptedFilesArray = [NSArray arrayWithArray:parser.encryptedFiles];
    
    //[self parseMetadata];
    [self parseOPF];
	
    //VLog(@"eBookBasePath: %@", ebookBasePath);
	self.spineArray = [NSArray arrayWithArray:tmpArray];
    if ([self.spineArray count] > 0) {
        if (!self.coverPageUrl) {
            // Let's assume the first spine item is our cover page
            Chapter* tmpChapter = (Chapter *)[self.spineArray objectAtIndex:0];
            self.coverPageUrl = [tmpChapter.spinePath lastPathComponent];
        }
    }
    VLog(@"ParseOPF ended");
}

@end
