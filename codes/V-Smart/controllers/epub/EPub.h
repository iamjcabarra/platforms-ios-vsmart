//
//  EPubParser.h

#import <Foundation/Foundation.h>
#import "TouchXML.h"


@interface EPub : NSObject {
	NSArray *spineArray;
    NSArray *tocArray;
	NSString *epubFilePath;
}

@property(nonatomic, retain) NSArray *spineArray;
@property(nonatomic, retain) NSArray *tocArray;
@property(nonatomic, retain) NSArray *encryptedFilesArray;
@property(nonatomic, retain) NSArray *authorsList;
@property (nonatomic, retain) NSString *uuid;
@property (nonatomic, retain) NSString *publisher;
@property (nonatomic, retain) NSString *authors;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *rootPath;
@property (nonatomic, retain) NSString *coverContent;
@property (nonatomic, retain) NSString *opfFilePath;
@property (nonatomic, retain) NSString *coverImagePath;
@property (nonatomic, retain) NSString *coverPageUrl;
@property (nonatomic, retain) NSString *baseDirectory;
@property (nonatomic, retain) NSString *OPFRootPath;
@property (nonatomic, assign) NSString *drmVersion;
@property (nonatomic, assign) BOOL isFixedLayout;
@property (nonatomic, assign) BOOL isUnlock;
@property (nonatomic, assign) BOOL isValid;
@property (nonatomic, assign) float version;

- (id) initWithEPubPath:(NSString*)path;
- (id) initWithBookname:(NSString*)bookname;
- (NSString *) coverImage:(NSString *) bookname;

@end
