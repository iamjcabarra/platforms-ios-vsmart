//
//  NCXElement.h
//  NCXParsing
//
//  Created by Earljon Hidalgo on 2/14/13.
//
//

#import <Foundation/Foundation.h>

@interface NCXElement : NSObject
{
    
}
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, assign) int playOrder;
@property (nonatomic, assign) int indent;
@property (nonatomic, assign) int parentPlayOrder;
@end
