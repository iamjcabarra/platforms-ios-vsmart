//
//  EpubParser.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/15/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "EpubParser.h"
#import "TBXML.h"
#import "NCXElement.h"
#import "VSmartHelpers.h"
#import "Utils.h"
#import "VibeDRM.h"

@interface EpubParser()
@property (strong, nonatomic) NSString *book_uuid;
@end

@implementation EpubParser
{
    BOOL ncxIsAvailable;
    NSString *backwardNCXFilename;
}

@synthesize spineDict, ncxDict, files, spines, encryptedFiles;

- (void) initializeBook: (NSString *) ncxBasePath withOPFFilePath:(NSString *) opfFilePath andEpubVersion:(float) version {
    
    playOrderAtZero = NO;
    currentIndent = 0;
    currentPlayOrderIndex = 0;
    playOrder = 0;
    epubVersion = version;
    ncxIsAvailable = NO;
    backwardNCXFilename = @"";
    
    [self loadOPFFile:opfFilePath];
    
    NSString *ncxPath = [NSString stringWithFormat:@"%@%@", ncxBasePath, ncxFilename];
    
    if (epubVersion == 2.0) {
        [self loadNCXFile:ncxPath];
    } else {
        [self loadEPUB3:ncxPath];
    }
}

- (void) initializeBook: (NSString *) ncxBasePath withOPFFilePath:(NSString *) opfFilePath andEpubVersion:(float) version uuid:(NSString *)uuid {
    
    playOrderAtZero = NO;
    currentIndent = 0;
    currentPlayOrderIndex = 0;
    playOrder = 0;
    epubVersion = version;
    ncxIsAvailable = NO;
    backwardNCXFilename = @"";
    self.book_uuid = [NSString stringWithFormat:@"%@", uuid];
    
    [self loadOPFFile:opfFilePath];
    
    NSString *ncxPath = [NSString stringWithFormat:@"%@%@", ncxBasePath, ncxFilename];
    
    if (epubVersion == 2.0) {
        [self loadNCXFile:ncxPath];
    } else {
        [self loadEPUB3:ncxPath];
    }
}

- (void) loadEPUB3:(NSString *)ncxFilePath {
    
    NSString *text = [NSString stringWithContentsOfFile:ncxFilePath encoding:NSUTF8StringEncoding error:nil];
    
    if (self.encryptedFiles.count > 0) {
        NSString *toc_FileName = ncxFilePath.lastPathComponent;
        for (NSString *fileName in self.encryptedFiles) {
            if ([fileName isEqualToString:toc_FileName]) {
                NSString *deviceId = [VSmartHelpers uniqueGlobalDeviceIdentifier];
                NSString *userId = [VSmartHelpers getUserIdWithBookUUID:self.book_uuid];
                NSString *drmKey = [NSString stringWithFormat:DRM_KEY, userId, deviceId];
                VibeDRM *vibeDRM = [[VibeDRM alloc] initWithKey:drmKey];
                [vibeDRM decrypt:[NSData dataWithContentsOfFile:ncxFilePath]];
                
                NSData *ncxData = [vibeDRM decrypt:[NSData dataWithContentsOfFile:ncxFilePath]];
                text = [[NSString alloc] initWithData:ncxData encoding:NSUTF8StringEncoding];
                break;
            }
        }
    }
    
    text = [self stringByRemovingCodeTag:text];
    
    //VLog(@"PATH: %@", ncxFilePath);
    self.files = [NSMutableArray array];
    
    TBXML *tbxml = [TBXML tbxmlWithXMLString:text];
    
    if (tbxml.rootXMLElement) {
        self.ncxDict = [[NSMutableDictionary alloc] init];
        [self traverseEPUB3Element:tbxml.rootXMLElement];
    }
}

- (void)loadNCXFile:(NSString *)ncxFilePath {
    NSData *xmlData = [NSData dataWithContentsOfFile:ncxFilePath];
    TBXML *tbxml = [TBXML tbxmlWithXMLData:xmlData];
    self.files = [NSMutableArray array];
    
    if (tbxml.rootXMLElement)
    {
        TBXMLElement *navMap = [TBXML childElementNamed:@"navMap" parentElement:tbxml.rootXMLElement];
        TBXMLElement *currElem = [TBXML childElementNamed:@"navPoint" parentElement:navMap];
        if (currElem == nil) {
            currElem = [TBXML childElementNamed:@"navpoint" parentElement:navMap];
        }
        
        self.ncxDict = [[NSMutableDictionary alloc] init];
        if (currElem != nil) {
            [self traverseNCXElementAdvanced:currElem];
        }
    }
    
}

- (void) loadOPFFile: (NSString *) opfFilePath {
    NSData *xmlData = [NSData dataWithContentsOfFile:opfFilePath];
    TBXML *tbxml = [TBXML tbxmlWithXMLData:xmlData];
    self.spineDict = [NSMutableDictionary dictionary];
    self.spines = [NSMutableArray array];
    //[self.spineDict retain];
    
    if (tbxml.rootXMLElement)
    {
        TBXMLElement *manifestElement = [TBXML childElementNamed:@"manifest" parentElement:tbxml.rootXMLElement];
        TBXMLElement *currElem = [TBXML childElementNamed:@"item" parentElement:manifestElement];
        
        [self traverseManifestElement:currElem];
        
        TBXMLElement *spineElement = [TBXML childElementNamed:@"spine" parentElement:tbxml.rootXMLElement];
        TBXMLElement *itemRefElement = [TBXML childElementNamed:@"itemref" parentElement:spineElement];
        [self traverseSpineElement:itemRefElement];
    }
}

#pragma mark - Encryption Methods
- (void) readEncryptionFile:(NSString *)filePath {
    
    self.encryptedFiles = [NSMutableArray array];
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists) {
        
        NSData *xmlData = [NSData dataWithContentsOfFile:filePath];
        if (xmlData == nil) {
            VLog(@"readEncryptionFile: No XML File found");
            self.encryptedFiles = nil;
            return;
        }
        
        self.encryptedFiles = [NSMutableArray array];
        TBXML *tbxml = [TBXML tbxmlWithXMLData:xmlData];
        
        if (tbxml.rootXMLElement)
        {
            [self traverseEncryptionElement:tbxml.rootXMLElement];
        }
    }
}

- (void) traverseEncryptionElement:(TBXMLElement *)element {
    do {
        if (element->firstChild) {
            [self traverseEncryptionElement:element->firstChild];
        }
        
        if ([[TBXML elementName:element] isEqualToString:@"CipherData"]) {
            
            // <CipherReference URI="OEBPS/gabay_sa_paggamit.xhtml"/>
            TBXMLElement *currElem = [TBXML childElementNamed:@"CipherReference" parentElement:element];
            NSString *itemUrl = [TBXML valueOfAttributeNamed:@"URI" forElement:currElem];
            [self.encryptedFiles addObject:[itemUrl lastPathComponent]];
        }
    } while ((element = element->nextSibling));
}

#pragma mark - Rights Methods
- (void) parseRights:(NSString *)filePath {

    self.drmversion = @"1.0"; //DEFAULT VALUE
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists) {
        
        NSData *xmlData = [NSData dataWithContentsOfFile:filePath];
        if (xmlData == nil) {
            VLog(@"%s: No rights XML file found", __PRETTY_FUNCTION__);
            return;
        }
        
        TBXML *tbxml = [TBXML tbxmlWithXMLData:xmlData];
        if (tbxml.rootXMLElement)
        {
            [self traverseDRMVersionElement:tbxml.rootXMLElement];
        }
    }
    
}

- (void) traverseDRMVersionElement:(TBXMLElement *)element {
    do {
        if (element->firstChild) {
            [self traverseDRMVersionElement:element->firstChild];
        }
        
        if ([[TBXML elementName:element] isEqualToString:@"version"]) {
            //  <rights xmlns="http://www.vibebookstore.com/xmlrights-epub" version="1">
            TBXMLElement *currElem = [TBXML childElementNamed:@"rights" parentElement:element];
            self.drmversion = [NSString stringWithFormat:@"%@", [TBXML valueOfAttributeNamed:@"version" forElement:currElem] ];
        }
    } while ((element = element->nextSibling));
}

int currentIndent = 0;
int currentPlayOrderIndex = 0;
BOOL playOrderAtZero = NO;

- (void) processNCXAdvanced:(TBXMLElement *) element isElementParent:(BOOL) isParent andParentOrder:(int) order withIndent:(int) indent
{
//    NSString *playOrder = [TBXML valueOfAttributeNamed:@"playOrder" forElement:element];
//    if (playOrder == nil) {
//        playOrder = [TBXML valueOfAttributeNamed:@"playorder" forElement:element];
//    }
    
    // Get the current element's name first
    TBXMLElement *navLabel = [TBXML childElementNamed:@"navLabel" parentElement:element];
    if (navLabel == nil) {
        navLabel = [TBXML childElementNamed:@"navlabel" parentElement:element];
    }
    
    TBXMLElement *textXML =[TBXML childElementNamed:@"text" parentElement:navLabel];
    NSString *stringLabel = [TBXML textForElement:textXML];
    
    TBXMLElement *navContent = [TBXML childElementNamed:@"content" parentElement:element];
    NSString *url = [TBXML valueOfAttributeNamed:@"src" forElement:navContent];
    
//    if(order != 0)
//        VLog(@"%@%@: %@ (%i) I: %i", isParent ? @"" : @"\t", playOrder, stringLabel, order, indent);
//    else {
//        VLog(@"%@%@: %@ I: %i", isParent ? @"" : @"\t", playOrder, stringLabel, indent);
//    }
    
    //int playOrderIndex = [playOrder intValue] - 1;
//    if (!playOrderAtZero) {
//        playOrderAtZero = [playOrder intValue] == 0;
//    }
    
    //int playOrderIndex = -1;//= [playOrder intValue] - 1;
    int playOrderValue = currentPlayOrderIndex + 1;
    
//    if(playOrderAtZero)
//        playOrderIndex = [playOrder intValue];
//    else
//        playOrderIndex = [playOrder intValue] - 1;
    
    NCXElement *elem = [[NCXElement alloc] init];
    elem.text = stringLabel;
    elem.url = url;
    elem.playOrder = playOrderValue;
    elem.parentPlayOrder = order;
    elem.indent = indent;
    
    NSDictionary *item = [NSDictionary dictionaryWithObjectsAndKeys:elem, @"NCX", stringLabel, @"title", nil];
    [files insertObject:[item objectForKey:@"NCX"] atIndex:currentPlayOrderIndex];
    [self.ncxDict setObject:item forKey:url];
    currentPlayOrderIndex = currentPlayOrderIndex + 1;
    
    TBXMLElement *childElem = [TBXML childElementNamed:@"navPoint" parentElement:element];
    if (childElem == nil) {
        childElem = [TBXML childElementNamed:@"navpoint" parentElement:element];
    }
    
	while (childElem) {
        currentIndent = indent + 1;
        
		[self processNCXAdvanced:childElem isElementParent:NO andParentOrder:playOrderValue withIndent:currentIndent];
		childElem = [TBXML nextSiblingNamed:@"navPoint" searchFromElement:childElem];
	}
}

int playOrder = 0;

- (void) processEPUB3:(TBXMLElement *) element isElementParent:(BOOL)isParent andParentOrder:(int)order withIndent:(int)indent
{
    int currentPlayOrder = playOrder;
    
    NSString *name = [NSString stringWithFormat:@"%s", element->name];
    if ([name isEqualToString:@"li"]) {
        playOrder += 1;
        TBXMLElement *hrefElement = [TBXML childElementNamed:@"a" parentElement:element];
        //NSLog(@"%i:%i-%i|%s", playOrder,order, indent, hrefElement->text);
        
        NSString *stringLabel = [NSString stringWithFormat:@"%s", hrefElement->text];
        NSString *url = [TBXML valueOfAttributeNamed:@"href" forElement:hrefElement];
        
        NCXElement *elem = [[NCXElement alloc] init];
        elem.text = stringLabel;
        elem.url = url;
        elem.playOrder = playOrder;
        elem.parentPlayOrder = order;
        elem.indent = indent;
        
        NSDictionary *item = [NSDictionary dictionaryWithObjectsAndKeys:elem, @"NCX", stringLabel, @"title", nil];
        [files insertObject:elem atIndex:playOrder - 1];
        [self.ncxDict setObject:item forKey:url];
        
        TBXMLElement *olElement = [TBXML childElementNamed:@"ol" parentElement:element];
        if (olElement) {
            if(olElement->firstAttribute == nil) {
                [self processEPUB3:olElement isElementParent:NO andParentOrder:playOrder withIndent:currentIndent];
            }
        }
    }
    
    TBXMLElement *childElem = [TBXML childElementNamed:@"li" parentElement:element];
	
	while (childElem) {
        currentIndent = indent + 1;
		[self processEPUB3:childElem isElementParent:NO andParentOrder:currentPlayOrder withIndent:currentIndent];
		childElem = [TBXML nextSiblingNamed:@"li" searchFromElement:childElem];
	}
}

-(void)traverseNCXElementAdvanced:(TBXMLElement *) element
{
    do {
        [self processNCXAdvanced:element isElementParent:YES andParentOrder:0 withIndent:0];
    } while ((element = element->nextSibling));
}

- (void) traverseManifestElement:(TBXMLElement *)element {
    //VLog(@"Traversing ...");
    do {
        if (element->firstChild) {
            //VLog(@"firstChild: %@", element);
            [self traverseManifestElement:element->firstChild];
        }
        //VLog(@"Checking ...");
        
        //VLog(@"traverseElement: %@", element);
        if ([[TBXML elementName:element] isEqualToString:@"item"]) {
            NSString *itemId = [TBXML valueOfAttributeNamed:@"id" forElement:element];
            NSString *itemHref = [TBXML valueOfAttributeNamed:@"href" forElement:element];
            //VLog(@"traverseElement: %@: %@", itemId, itemHref);
            NSString *mediaType = [TBXML valueOfAttributeNamed:@"media-type" forElement:element];
            
            if (epubVersion == 2.0) {
                if([mediaType isEqualToString:@"application/x-dtbncx+xml"]) ncxFilename = itemHref;
            } else {
                if ([mediaType isEqualToString:@"application/xhtml+xml"]) {
                    NSString *propertyName = [TBXML valueOfAttributeNamed:@"properties" forElement:element];
                    if (!IsEmpty(propertyName)) {
                        if ([propertyName isEqualToString:@"nav"]) {
                            ncxFilename = itemHref;
                        }
                    }
                } else if ([mediaType isEqualToString:@"application/x-dtbncx+xml"]) {
                    ncxIsAvailable = YES;
                    backwardNCXFilename = itemHref;
                }
            }
            
            [self.spineDict setObject:itemHref forKey:itemId];
        }
    } while ((element = element->nextSibling));
}

- (void) traverseSpineElement:(TBXMLElement *)element {
    do {
        if (element->firstChild) {
            //VLog(@"firstChild: %@", element);
            [self traverseSpineElement:element->firstChild];
        }
        //VLog(@"Checking ...");
        
        //VLog(@"traverseElement: %@", element);
        if ([[TBXML elementName:element] isEqualToString:@"itemref"]) {
            NSString *idRef = [TBXML valueOfAttributeNamed:@"idref" forElement:element];
            //VLog(@"Checking ... %@", idRef);
            [self.spines addObject:idRef];
        }
    } while ((element = element->nextSibling));
}

- (void) traverseEPUB3Element:(TBXMLElement *)element {
    
    do {
        NSString *navType = [TBXML valueOfAttributeNamed:@"epub:type" forElement:element];
        if ([navType isEqualToString:@"toc"]) {
            TBXMLElement *olElem = [TBXML childElementNamed:@"ol" parentElement:element];
            
//            [self processEPUB3:olElem isElementParent:YES andParentOrder:0 withIndent:0];
            
            // WORKAROUND: Does child element null???
            if (olElem != NULL) {
                [self processEPUB3:olElem isElementParent:YES andParentOrder:0 withIndent:0];
            }
        }
        if (element->firstChild) {
            [self traverseEPUB3Element:element->firstChild];
        }
    } while ((element = element->nextSibling));
}

#pragma mark - Private Helpers
- (NSString *) stringByReplacingString:(NSString *) source replaceWith:(NSString *)replaceText applyRegex:(NSString *) regexText {
    NSError* error;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexText
                                                                           options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSString *modifiedString = [regex stringByReplacingMatchesInString:source
                                                               options:0
                                                                 range:NSMakeRange(0, [source length])
                                                          withTemplate:replaceText];
    
    return modifiedString;
}

- (NSString *) stringByRemovingCodeTag: (NSString *) text {
    NSString* codeClass = @"<code.*?>";
    NSString* endTag = @"</code>";
    
    if ( [text containsString:endTag] ) {//Turn-On for future migration
        text = [self stringByReplacingString:text replaceWith:@"" applyRegex:codeClass];
        text = [self stringByReplacingString:text replaceWith:@"" applyRegex:endTag];
    }
    
    codeClass = @"<em>";
    endTag = @"</em>";
    
    if ( [text containsString:endTag] ) {//Turn-On for future migration
        text = [self stringByReplacingString:text replaceWith:@"" applyRegex:codeClass];
        text = [self stringByReplacingString:text replaceWith:@"" applyRegex:endTag];
    }
    
    codeClass = @"<span class=.*?>";
    endTag = @"</span>";
    if ( [text containsString:endTag] ) {//Turn-On for future migration
        text = [self stringByReplacingString:text replaceWith:@"" applyRegex:codeClass];
        text = [self stringByReplacingString:text replaceWith:@"" applyRegex:endTag];
    }

    return text;
}

@end
