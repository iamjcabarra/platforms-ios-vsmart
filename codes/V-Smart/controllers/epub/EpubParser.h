//
//  EpubParser.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/15/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBXML.h"

@interface EpubParser : NSObject
{
    NSString *ncxFilename;
    float epubVersion;
}

@property(nonatomic,retain) NSMutableArray *files;
@property(nonatomic,retain) NSMutableArray *spines;
@property(nonatomic,retain) NSMutableDictionary *spineDict;
@property(nonatomic,retain) NSMutableDictionary *ncxDict;
@property(nonatomic,retain) NSMutableArray *encryptedFiles;
@property(nonatomic,retain) NSString *drmversion;

- (void) initializeBook: (NSString *) ncxBasePath withOPFFilePath:(NSString *) opfFilePath andEpubVersion:(float) version;
- (void) initializeBook: (NSString *) ncxBasePath withOPFFilePath:(NSString *) opfFilePath andEpubVersion:(float) version uuid:(NSString *)uuid;
- (void) readEncryptionFile:(NSString *)filePath;
- (void) parseRights:(NSString *)filePath;

@end
