//
//  VSmart.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/16/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "VSmart.h"
#import "GCNetworkKit.h"
#import "VSmartHelpers.h"
#import "GuestAccount.h"
#import "SSPeopleItem.h"
#import <Parse/Parse.h>

@interface VSmart ()  {
@private
    NSString *_uuid;
    NSString *_authKey;
    NSString *_email;
    
    VSmartLoginRequestCompletionBlock _loginResultBlock;
    VSmartServiceResponseCompletionBlock _serviceRequestBlock;
    VSmartListRequestCompletionBlock _listResultBlock;
    VSmartRequestErrorBlock _failureResultBlock;
    VSmartPlatformType _platformType;
    
    AccountInfo *_account;
}
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) BOOL isPerformingRequiredOperation;
@property (nonatomic, assign) BOOL isAuthenticated;
@end

@implementation VSmart
@synthesize isPerformingRequiredOperation, aliasBackgroundColor, isAuthenticated;

+ (id) sharedInstance {
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}

- (id)init {
    if ((self = [super init])) {
        [[GCNetworkRequestQueue sharedQueue] setMaxConcurrentRequests:2];
        [self polling];
        [self pollingStart]; //run this once
        
        aliasBackgroundColor = [SSPeopleItem generateRandomColor];
    }
    
    return self;
}

- (NSString *)uuid {
    return [VSmartHelpers uniqueGlobalDeviceIdentifier];
}

- (NSString *)authKey {
    return [self account].device.token;
}

- (NSString *)email {
    return [self account].user.email;
}

- (AccountInfo *) account {
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    //VLog(@" The Account: %@", account);
    
    //RYAN
    [self performRequiredOperations];

    return account;
}

-(void)performRequiredOperations{
    if (isPerformingRequiredOperation)
        return;
    
    isPerformingRequiredOperation = YES;
    
    if (!self.account){
        isPerformingRequiredOperation = NO;
        return;
    }

    /*
     * moving this block to performRequiredOperationsWithfailureBlock
     * this is called in the text book module
     */
    //check if lockIO codes are already fetched from server
//    NSDictionary *lockCodes = [[NSUserDefaults standardUserDefaults] getCustomObjectForKey:kLockIOResponseKey];
//    if (lockCodes == nil) {
//        //fetch
//        [self requestExerciseCodesWithResultBlock:^(NSDictionary *responseData) {
//            VLog(@"success lockCodes");
//            NSLog(@"----- END ---- lockCodes Success");
//        } failureBlock:^(NSError *error, NSData *responseData) {
//            VLog(@"failed lockCodes");
//        }];
//    }
    
    //check if device is already binded
    BOOL isSuccess = NO;
    NSDictionary *bindResponse = [[NSUserDefaults standardUserDefaults] valueForKey:kBindResponseKey];
    
    if (bindResponse){
        isSuccess = (BOOL)[bindResponse valueForKey:@"success"];
    }
    
    if (!isSuccess){
        bool isRequestOAuthSuccess = [self requestOAuth];
        
        if (isRequestOAuthSuccess)
        {
            bool isRequestBindSuccess = [self requestBind];
            if (!isRequestBindSuccess)
            {
                //VLog(@"failed isRequestBindSuccess");
                isPerformingRequiredOperation = NO;
                return;
            }
        }
        else{
            //VLog(@"failed isRequestOAuthSuccess");
            isPerformingRequiredOperation = NO;
            return;
        }
    }
    
    //check if userId is existing
    NSString *userId = [Utils getUserId];
    if (userId == nil || userId.length <= 0) {
        bool isRequestUserIdSuccess = [self requestUserId];
        
        if (!isRequestUserIdSuccess){
            //VLog(@"failed isRequestUserIdSuccess");
            isPerformingRequiredOperation = NO;
            return;
        }
    }
    
    //VLog(@"success performRequiredOperations");
    isPerformingRequiredOperation = NO;
}

/* perform required operation with failure block 
 * called by the fetchBooks function
 */
-(void)performRequiredOperationsWithfailureBlock:(VSmartRequestErrorBlock)failureBlock
{
    if (isPerformingRequiredOperation)
        return;
    
    isPerformingRequiredOperation = YES;
    
    if (!self.account){
        isPerformingRequiredOperation = NO;
        return;
    }
    
//    //check if lockIO codes are already fetched from server
//    [self requestLockCodes];
    
//    NSDictionary *lockCodes = [[NSUserDefaults standardUserDefaults] getCustomObjectForKey:kLockIOResponseKey];
//    if (lockCodes == nil) {
//        //fetch
//        [self requestExerciseCodesWithResultBlock:^(NSDictionary *responseData) {
//            VLog(@"success lockCodes");
//        } failureBlock:^(NSError *error, NSData *responseData) {
//            VLog(@"failed lockCodes");
//        }];
//    }
    
    //check if device is already binded
    BOOL isSuccess = NO;
    NSDictionary *bindResponse = [[NSUserDefaults standardUserDefaults] valueForKey:kBindResponseKey];
    if (bindResponse){
        isSuccess = (BOOL)[bindResponse valueForKey:@"success"];
    }
    
    if (!isSuccess){
        
        NSDictionary *info = nil;
        NSError *error = nil;
        
        bool isRequestOAuthSuccess = [self requestOAuth];
        
        if (isRequestOAuthSuccess)
        {
            bool isRequestBindSuccess = [self requestBind];
            if (!isRequestBindSuccess)
            {
                //VLog(@"failed isRequestBindSuccess");
                isPerformingRequiredOperation = NO;
                
                /* we need to return a failure block to dismiss the HUD
                 * we also need inform the user
                 */
                info = @{NSLocalizedDescriptionKey:@"Failed Binding"};
                error = [NSError errorWithDomain:@"com.vibebookstore.vsmart" code:0 userInfo:info];
                
                if (failureBlock) {
                    failureBlock(error,nil);
                }
                
                return;
            }
        }
        else{
            //VLog(@"failed isRequestOAuthSuccess");
            isPerformingRequiredOperation = NO;
            
            /* we need to return a failure block to dismiss the HUD
             * we also need inform the user
             */
            info = @{NSLocalizedDescriptionKey:@"Failed Authentication"};
            error = [NSError errorWithDomain:@"com.vibebookstore.vsmart" code:0 userInfo:info];
            
            if (failureBlock) {
                failureBlock(error,nil);
            }
            
            return;
        }
    }
    
    //check if userId is existing
    NSString *userId = [Utils getUserId];
    if (userId == nil || userId.length <= 0) {
        bool isRequestUserIdSuccess = [self requestUserId];
        
        if (!isRequestUserIdSuccess){
            //VLog(@"failed isRequestUserIdSuccess");
            isPerformingRequiredOperation = NO;
            return;
        }
    }
    
    //VLog(@"success performRequiredOperations");
    isPerformingRequiredOperation = NO;
}

- (void)requestLockCodes {
    //check if lockIO codes are already fetched from server
    NSDictionary *lockCodes = [[NSUserDefaults standardUserDefaults] getCustomObjectForKey:kLockIOResponseKey];
    if (lockCodes == nil) {
        //fetch
        [self requestExerciseCodesWithResultBlock:^(NSDictionary *responseData) {
            //VLog(@"success lockCodes");
        } failureBlock:^(NSError *error, NSData *responseData) {
            //VLog(@"failed lockCodes");
        }];
    }
}

- (BOOL)successfullyAuthenticated
{
    return self.isAuthenticated;
}

-(void) fetchBooks: (VSmartBooklistRequestCompletionBlock) resultBlock failureBlock:(VSmartRequestErrorBlock) failureBlock {
    
//    BOOL isSuccess = NO;
    
//    //check if device is already binded
//    NSDictionary *bindResponse = [[NSUserDefaults standardUserDefaults] valueForKey:kBindResponseKey];
//    if (bindResponse){
//        isSuccess = (BOOL)[bindResponse valueForKey:@"success"];
//    }
//    
//    //check if userId is existing
//    NSString *userId = [Utils getUserId];
//    if (userId == nil || userId.length <= 0 || !isSuccess) {
//        //[self performRequiredOperations];
//        // We must return a failure block to dismiss the progress HUD
//        [self performRequiredOperationsWithfailureBlock:failureBlock];
//    }
//    else{
        [self fetchBooks2:resultBlock failureBlock:failureBlock];
//    }
}

-(void) fetchBooks2: (VSmartBooklistRequestCompletionBlock) resultBlock failureBlock:(VSmartRequestErrorBlock) failureBlock {

    NSURL *booklistURL = [NSURL URLWithString:kServiceURLBookList];
    GCNetworkRequest *request = [GCNetworkRequest requestWithURL:booklistURL];
    
    NSDictionary *oAuthResponse = [[NSUserDefaults standardUserDefaults] valueForKey:kOAuthResponseKey];
    NSString *oAuthToken = [oAuthResponse valueForKey:@"access_token"];
    NSString *authorization = [NSString stringWithFormat:@"Bearer %@", oAuthToken];
    
    NSLog(@"book list URL : %@", booklistURL);
    NSLog(@"oauth token : %@", oAuthToken);
    NSLog(@"authorization : %@", authorization);
    
    request.requestMethod = GCNetworkRequestMethodGET;
    [request setHeaderValue:authorization forField:@"Authorization"];

    //NOTE: no need to set school code header in this moonlight request
    
    request.completionHandler = ^(NSData *responseData){
        TransformJSONDataToNSObject(responseData, ^(id object, NSError *error){
            // Error
            if (error && error.code != GCNetworkRequestUserDidCancelErrorCode) {
                VLog(@"An error occured: %@", error);
                if(failureBlock) failureBlock(error, responseData);
            } else {
                NSDictionary *results = (NSDictionary *)object;
                VLog(@"Fetch Result: %@", results);
                if(resultBlock) {
                    int success = [[results valueForKeyPath:@"error.status"] intValue];
                    
                    if(success == 0) {
                        BookListResponse *response = [self _buildBooklistResponse:results];
                        resultBlock(response);
                    }
                    else {
                        resultBlock(nil);
                        if(failureBlock) {
                            failureBlock([self _buildError:results], responseData);
                        }
                    }
                }
            }
        });
    };
    request.errorHandler = ^(NSError *error, NSData *responseData) {
        VLog(@"Error: %@", [error localizedDescription]);
        if(failureBlock) failureBlock(error, responseData);
    };
    // Add request to the queue and return its hash
    [[GCNetworkRequestQueue sharedQueue] addRequest:request];
}

-(void) upload: (UIImage *) image resultBlock: (VSmartUploadCompletionBlock) resultBlock
   failureBlock: (VSmartRequestErrorBlock) failureBlock {
    
    /*
     {"Name":"f9df7aafac6e0526d52977cf77591fee.png","Size":1024,"ContentType":"png","Location":"http:\/\/vsmart.vibeapi.net\/public\/profile\/test.jpeg"}
     */
    
    NSString *code = [Utils getSettingsSchoolCode];
    NSDictionary *dimensions = @{@"user": [VSmart sharedInstance].account.user.email, @"code": code};
    [PFAnalytics trackEvent:@"avatar_update" dimensions:dimensions];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.8f);
    NSString *webQuery = [Utils buildUrl:VS_FMT(kEndPointAvatarUpload, [self account].user.id)];
    
    GCNetworkFormRequest *request = [GCNetworkFormRequest requestWithURL:[NSURL URLWithString:webQuery]];
    [request addData:imageData forKey:@"data" contentType:@"image/jpg"];
    
    //NOTE: For service end points we must set schoolcode preference
    [request setHeaderValue:[Utils getSettingsSchoolCodeBase64] forField:@"code"];
    
    request.errorHandler = ^(NSError *error, NSData *responseData) {
        VLog(@"Error: %@", [error localizedDescription]);
        if(failureBlock) failureBlock(error, responseData);
    };
    request.completionHandler = ^(NSData *responseData){
        
        TransformJSONDataToNSObject(responseData, ^(id object, NSError *error) {
            if (error && error.code != GCNetworkRequestUserDidCancelErrorCode) {
                VLog(@"An error occured: %@", responseData);
                if(failureBlock) failureBlock(error, responseData);
            }
            else {
                NSDictionary *results = (NSDictionary *)object;
                VLog(@"Upload Result: %@", results);
                
                if(resultBlock) {
                    NSDictionary *dict = [[[results objectForKey:@"records"] objectForKey:@"data"] objectAtIndex:0];
                    
                    PhotoResponse *resp = [[PhotoResponse alloc] initWithDictionary:dict error:nil];
                    resultBlock(resp);
                }
            }
        });
    };
    
    request.responseHandler = ^(NSHTTPURLResponse *response){
        VLog(@"Uploaded grab to URL: %@", [response allHeaderFields]);
        VLog(@"Status Code: %li", response.statusCode);
    };
    
    [[GCNetworkRequestQueue sharedQueue] addRequest:request];
}

-(void) connect: (NSString *) email
    withPassword: (NSString *) password
 andPlatformType: (VSmartPlatformType) platformType
     resultBlock: (VSmartLoginRequestCompletionBlock) resultBlock
    failureBlock: (VSmartRequestErrorBlock) failureBlock {
    
    if(resultBlock) _loginResultBlock = resultBlock;
    if(failureBlock) _failureResultBlock = failureBlock;
    
    NSString *webQuery = [Utils buildUrl:kEndPointLogin];
    NSLog(@"login web query : %@", webQuery);
//    NSString *jsonRequest = [self _buildLoginRequest:email withPassword:password andPlatformType:platformType];
    
    NSDictionary *device = @{@"platform_type": [NSNumber numberWithInt:platformType], @"device_id": [self uuid], @"email": email, @"password": password};
    NSLog(@"device object : %@", device);
    NSString *jsonRequest = [VSmartHelpers jsonString:device];
    
    NSString *code = [Utils getSettingsSchoolCode];
    NSDictionary *dimensions = @{@"uuid": [self uuid] , @"email": email, @"code": code};
    [PFAnalytics trackEvent:@"connect" dimensions:dimensions];
    
    VLog(@"jsonRequestLogin: %@", jsonRequest);
    
    GCNetworkRequest *request = [GCNetworkRequest requestWithURL:[NSURL URLWithString:webQuery]];
    request.requestMethod = GCNetworkRequestMethodPOST;
    request.bodyContent = [jsonRequest dataUsingEncoding:NSUTF8StringEncoding];
    
    //NOTE: For service end points we must set schoolcode preference
    NSString *school_code = [Utils getSettingsSchoolCodeBase64];
    VLog(@"SCHOOL CODE : %@", school_code);
    [request setHeaderValue:school_code forField:@"code"];
    
    VLog(@"HEADER : %@", [request allHeaderValuesAndFields]);
    
    request.completionHandler = ^(NSData *responseData){
        
        TransformJSONDataToNSObject(responseData, ^(id object, NSError *error){
            // Error
            if (error && error.code != GCNetworkRequestUserDidCancelErrorCode) {
                VLog(@"An error occured: %@", error);
                VLog(@"An error occured: %@", responseData);
                if(failureBlock) failureBlock(error, responseData);
            }
            else {
                NSMutableDictionary *results = (NSMutableDictionary *)object;
                VLog(@"Login Result: %@", results);

                if(resultBlock) {
                    int success = [[results valueForKeyPath:@"error.status"] intValue];
                    
                    if(success == 0) {
                        
                        //IMPORT SAMPLERS
//                        [self initiateImportStamplers];
                        
//                        NSDictionary *dictRecords = @{@"account": [results[@"records"] objectAtIndex:0]};
//                        NSDictionary *dictError = [results[@"records"] objectAtIndex:1];
                        NSMutableDictionary *dictRecords = [@{@"account": (results[@"records"])[0]} mutableCopy];
                        
                        // BACKGROUND COMPATIBILITY
                        NSMutableDictionary *user = (NSMutableDictionary *)(dictRecords[@"account"])[@"user"];
                        if (user[@"is_coordinator"] == nil) {
                            user[@"is_coordinator"] = @"0";
                        }
                        
                        NSDictionary *dictError = (results[@"records"])[1];
                        
                        NSMutableDictionary *dictConform = [[NSMutableDictionary alloc] init];
                        [dictConform addEntriesFromDictionary:dictRecords];
                        [dictConform addEntriesFromDictionary:dictError];
                        AccountResponse *response = [self _buildLoginResponse:dictConform];
                        
//                        AccountResponse *response = [self _buildLoginResponse:results];
                        _loginResultBlock(response);
                        
                    }
                    else {
                        resultBlock(nil);
                        if(failureBlock) {
                            failureBlock([self _buildError:results], responseData);
                        }
                    }
                }
            }
        });
    };
    request.errorHandler = ^(NSError *error, NSData *responseData) {
        
        NSLog(@"LOGIN ERROR : %@", [error localizedDescription]);
        
        VLog(@"connect errorHandler: %@", responseData);
        if(failureBlock) failureBlock(error, responseData);
    };
    
    [[GCNetworkRequestQueue sharedQueue] addRequest:request];
}

-(void)initiateImportStamplers {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *flag = [defaults objectForKey:kImportedSamplers];
    if (![flag isEqualToString:@"1"]) {
        [Utils copySamplersToDocuments];//copy books
        [defaults setObject:@"1" forKey:kImportedSamplers];
        [defaults synchronize];
    }
}

-(void)connectAsExistingGuest:(NSDictionary*)results resultBlock:(VSmartLoginRequestCompletionBlock)resultBlock failureBlock:(VSmartRequestErrorBlock) failureBlock{
    
    if(resultBlock) _loginResultBlock = resultBlock;
    if(failureBlock) _failureResultBlock = failureBlock;
    
    AccountResponse *response = [[AccountResponse alloc] initWithDictionary:results error:nil];
    
    [self connect:response.account.user.email withPassword:response.account.user.password andPlatformType:VSmartPlatformTypeiPad resultBlock:resultBlock failureBlock:failureBlock];
    
//    AccountResponse *response = [self _buildLoginResponse:results];
//    _loginResultBlock(response);
}

-(void)connectAsGuestWithPosition:(NSString*)position
    platformType: (VSmartPlatformType) platformType
    resultBlock: (VSmartLoginRequestCompletionBlock) resultBlock
   failureBlock: (VSmartRequestErrorBlock) failureBlock {
    
    if(resultBlock) _loginResultBlock = resultBlock;
    if(failureBlock) _failureResultBlock = failureBlock;
    
    NSDictionary *device = @{@"platform_type": [NSNumber numberWithInt:platformType], @"device_id": [self uuid], @"position" : position};
    NSString *jsonRequest = [VSmartHelpers jsonString:device];

    VLog(@"jsonRequestLogin: %@", jsonRequest);
    
    GCNetworkRequest *request = [GCNetworkRequest requestWithURL:[NSURL URLWithString:[Utils buildUrl:kEndPointLoginAsGuest]]];
    request.requestMethod = GCNetworkRequestMethodPOST;
    request.bodyContent = [jsonRequest dataUsingEncoding:NSUTF8StringEncoding];
    
    //NOTE: For service end points we must set schoolcode preference
    [request setHeaderValue:[Utils getSettingsSchoolCodeBase64] forField:@"code"];
    
    VLog(@"HEADER : %@", [request allHeaderValuesAndFields]);
    
    request.completionHandler = ^(NSData *responseData){

        TransformJSONDataToNSObject(responseData, ^(id object, NSError *error){
            // Error
            if (error && error.code != GCNetworkRequestUserDidCancelErrorCode) {
                VLog(@"An error occured: %@", error);
                VLog(@"An error occured: %@", responseData);
                if(failureBlock) failureBlock(error, responseData);
            }
            else {
                NSDictionary *results = (NSDictionary *)object;
                VLog(@"Login Result: %@", results);
                
                if(resultBlock) {
                    int success = [[results valueForKeyPath:@"error.status"] intValue];
                    
                    if(success == 0) {
                        NSDictionary *dictRecords = @{@"account": [[results objectForKey:@"records"] objectAtIndex:0]};
                        NSDictionary *dictError = [[results objectForKey:@"records"] objectAtIndex:1];
                        
                        NSMutableDictionary *dictConform = [[NSMutableDictionary alloc] init];
                        [dictConform addEntriesFromDictionary:dictRecords];
                        [dictConform addEntriesFromDictionary:dictError];
                        
//                        NSData *stringData = [NSJSONSerialization dataWithJSONObject:final options:NSJSONWritingPrettyPrinted error:nil];
//                        
//                        NSString *stringTest = [[NSString alloc] initWithData:stringData encoding:NSUTF8StringEncoding];
//                        
//                        NSLog(@"%@", stringTest);
                        
                        AccountResponse *response = [self _buildLoginResponse:dictConform];
                        _loginResultBlock(response);
                        
                        //save to guest list: get first the list
                        
                        NSData *data;
                        
                        if ([position isEqualToString:kModeIsStudent])
                            data = [[NSUserDefaults standardUserDefaults] objectForKey:kGuestListKeyStudent];

                        if ([position isEqualToString:kModeIsTeacher])
                            data = [[NSUserDefaults standardUserDefaults] objectForKey:kGuestListKeyTeacher];
                        
                        
                        NSArray *guestsRecord = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                        
                        if (guestsRecord == nil)
                            guestsRecord = [[NSMutableArray alloc] init];
                        
                        //check if the name already exist, if not, save it, else just proceed to login
                        bool isExisting = NO;
                        for (NSDictionary *item in guestsRecord) {
                            GuestAccount *guestItem = [[GuestAccount alloc] initWithDictionary:item error:nil];
                            if ([guestItem.name isEqualToString:response.account.user.email]){
                                isExisting = YES;
                                break;
                            }
                        }
                        
                        if (!isExisting){
                            NSMutableArray *workingArray = [guestsRecord mutableCopy];

                            GuestAccount *guest = [GuestAccount new];
                            guest.name = response.account.user.email;
                            guest.value = dictConform;
                            
                            NSDictionary *dict = [guest toDictionary];
                            [workingArray addObject:dict];
                            
                            data = [NSKeyedArchiver archivedDataWithRootObject:workingArray];
                            
                            if ([position isEqualToString:kModeIsStudent])
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:data forKey:kGuestListKeyStudent];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                            }
                            
                            if ([position isEqualToString:kModeIsTeacher])
                            {
                                [[NSUserDefaults standardUserDefaults] setObject:data forKey:kGuestListKeyTeacher];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                            }
                            
                            [Utils copySamplersToDocuments];
                        }
                    }
                    else {
                        resultBlock(nil);
                        if(failureBlock) {
                            failureBlock([self _buildError:results], responseData);
                        }
                    }
                }
            }
        });
    };
    request.errorHandler = ^(NSError *error, NSData *responseData) {
        VLog(@"connectAsGuestWithPlatformType errorHandler: %@", responseData);
        if(failureBlock) failureBlock(error, responseData);
    };
    
    [[GCNetworkRequestQueue sharedQueue] addRequest:request];
}

-(BOOL)requestOAuth{
    
//    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSString *username = self.account.user.email;
    NSString *password = self.account.user.password;

    
    JSONModelError *jsonError;
    NSError *error;
    
    self.isAuthenticated = NO; //default value
    
    NSDictionary *params = @{@"grant_type": @"password", @"username" : username, @"password" : password};
    NSDictionary *headers = @{@"Authorization": @"Basic MTAwMDAwMTpzMmFMd3o4a0FuRW01MlNqT1BnVmViblI2NlI1cXZLYlF6Q3ozZDkzaDJEV0pkcEl3VGN5aEwwUXlCelloRHdXUjA1V2FRc1RjeGNoNmpwSQ=="};
    
    NSURL *oauthURL = [NSURL URLWithString:kServiceURLOAuthToken];
//    NSLog(@"REQUEST OAUTH : %@", oauthURL.absoluteString);
//    NSLog(@"REQUEST HEADER : %@", headers);
//    NSLog(@"REQUEST PARAMS : %@", params);
    
    NSData *responseData = [JSONHTTPClient syncRequestDataFromURL:oauthURL method:kHTTPMethodPOST params:params headers:headers etag:nil error:&jsonError];
    
    if (responseData == nil) {
        return NO;
    }
    
    if (jsonError) {
        return NO;
    }
    
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    VLog(@"requestOAuth responseDict: %@", responseDict);
    
    NSString *access_token = [NSString stringWithFormat:@"%@", responseDict[@"access_token"] ];
    NSString *expires_in = [NSString stringWithFormat:@"%@", responseDict[@"expires_in"] ];
    NSString *refresh_token = [NSString stringWithFormat:@"%@", responseDict[@"refresh_token"] ];
    NSString *scope = [NSString stringWithFormat:@"%@", responseDict[@"scope"] ];
    NSString *token_type = [NSString stringWithFormat:@"%@", responseDict[@"token_type"] ];
    
    /*
    "access_token" = MjM5YmEzMTM3NDlmYjUwZTM3Nzk5ZTMxYjA1YTY5MDRiM2IzOGNlZWM1Yjk1MWEyNmFjNDU5OGEwYTU2NmRjYg;
    "expires_in" = 3600;
    "refresh_token" = NjM0NjNlZTU4Y2E5YmRlMzYzMGUzMDgyODUxNDI0OGMwNDU2MzQ4MWFlZDY0ZDRkODEyMDYwZmI3ZGY5ZmUzNA;
    scope = "<null>";
    "token_type" = bearer;
     */
    
    NSDictionary *result = @{@"access_token":access_token,
                             @"expires_in":expires_in,
                             @"refresh_token":refresh_token,
                             @"scope":scope,
                             @"token_type":token_type};
    if (error) {
        return NO;
    }

    [[NSUserDefaults standardUserDefaults] setValue:result forKey:kOAuthResponseKey];
    self.isAuthenticated = YES;//success state
    
    return YES;
}

-(BOOL)requestUserId{
    
    JSONModelError *jsonError;
    NSError *error;
    
    NSDictionary *oAuthResponse = [[NSUserDefaults standardUserDefaults] valueForKey:kOAuthResponseKey];
    NSString *oAuthToken = [oAuthResponse valueForKey:@"access_token"];
    
    NSDictionary *headers = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", oAuthToken]};
    
    NSData *responseData = [JSONHTTPClient syncRequestDataFromURL:[NSURL URLWithString:kServiceURLUserInfo] method:kHTTPMethodGET params:nil headers:headers etag:nil error:&jsonError];
    
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    VLog(@"requestUserId responseDict: %@", responseDict);
    NSLog(@"%s %@", __PRETTY_FUNCTION__, responseDict);
    /*
    firstname = ajohnson;
    id = 3;
    lastname = ajohnson;
    name = "ajohnson ajohnson";
    username = ajohnson;
     */

    if (jsonError) {
        return NO;
    }
    
    if (error) {
        return NO;
    }
    
//    NSString *userId = [responseDict objectForKey:@"id"];
    NSString *userId = [NSString stringWithFormat:@"%@", responseDict[@"id"] ];
    [Utils saveUserId:userId];
    
    return YES;
}

-(BOOL)requestBind{
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    JSONModelError *jsonError;
    NSError *error;
    
    NSDictionary *oAuthResponse = [[NSUserDefaults standardUserDefaults] valueForKey:kOAuthResponseKey];
    NSString *oAuthToken = [oAuthResponse valueForKey:@"access_token"];
    
    NSDictionary *params = @{@"operating_system": @"iOS",
                             @"system_version" : [[UIDevice currentDevice] systemVersion],
                             @"manufacturer" : @"Apple",
                             @"model": [[UIDevice currentDevice] model],
                             @"serial_number" : @"na",
                             @"wifi_address" : @"na",
                             @"bluetooth" : @"na",
                             @"storage_capacity" : @"0",
                             @"uuid" : [VSmartHelpers uniqueGlobalDeviceIdentifier],
                             };
    
    NSDictionary *headers = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", oAuthToken]};
    
    NSData *responseData = [JSONHTTPClient syncRequestDataFromURL:[NSURL URLWithString:kServiceURLBind] method:kHTTPMethodPOST params:params headers:headers etag:nil error:&jsonError];

    if (responseData == nil) {
        return NO;
    }
    
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error];
    NSLog(@"requestBind responseDict: %@", responseDict);
    if (jsonError) {
        return NO;
    }
    
    if (error) {
        return NO;
    }
    
    NSNumber *success = [responseDict objectForKey:@"success"];
    if ([success boolValue] == NO){
        
        if (responseDict != nil) {
            if (responseDict[@"error"] != nil) {
                NSDictionary *error_info = responseDict[@"error"];
                NSString *message = [self stringValue: error_info[@"message"] ];
                NSLog(@"error : %@", message);
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [ProgressHUD showError:message];
//                });
            }
        }
        
        return NO;
    }
    
    if (responseDict != nil) {
        NSArray *current_devices = [NSArray arrayWithArray: responseDict[@"current_devices"] ];
    
        NSMutableArray *a = [NSMutableArray array];
        for (NSDictionary *d in current_devices) {
            NSString *manufacturer = [self stringValue: d[@"manufacturer"] ];
            NSString *model = [self stringValue: d[@"model"] ];
            NSString *operating_system = [self stringValue: d[@"operating_system"] ];
            NSString *uuid = [self stringValue: d[@"uuid"] ];
            
            NSDictionary *data = @{ @"manufacturer" : manufacturer,
                                    @"model" : model,
                                    @"operating_system" : operating_system,
                                    @"uuid" : uuid };
            
            [a addObject:data];
        }
        
        responseDict[@"current_devices"] = a;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:responseDict forKey:kBindResponseKey];
        [defaults synchronize];
    }
    
    return YES;
}

- (NSString *)stringValue:(id)object {
    
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] ) {
        return @"";
    }
    
    return value;
}

-(void)requestDowload:(NSString*)sku
             revision:(int)revision
      withResultBlock: (VSmartServiceResponseCompletionBlock) resultBlock
         failureBlock: (VSmartRequestErrorBlock) failureBlock {
    
    if(resultBlock) _serviceRequestBlock = resultBlock;
    if(failureBlock) _failureResultBlock = failureBlock;
    
    
    NSString *url = [NSString stringWithFormat:@"%@%@", kServiceURLRequestDL, sku];
    
    GCNetworkRequest *request = [GCNetworkRequest requestWithURL:[NSURL URLWithString:url]];
    
    NSDictionary *oAuthResponse = [[NSUserDefaults standardUserDefaults] valueForKey:kOAuthResponseKey];
    NSString *oAuthToken = [oAuthResponse valueForKey:@"access_token"];
    [request setHeaderValue:[NSString stringWithFormat:@"Bearer %@", oAuthToken] forField:@"Authorization"];
    
    request.requestMethod = GCNetworkRequestMethodGET;
    
    //NOTE: no need to set school code header in this moonlight request
    
    request.completionHandler = ^(NSData *responseData){
        
        TransformJSONDataToNSObject(responseData, ^(id object, NSError *error){
            // Error
            if (error && error.code != GCNetworkRequestUserDidCancelErrorCode) {
                VLog(@"An error occured: %@", error);
                VLog(@"An error occured: %@", responseData);
                if(failureBlock) failureBlock(error, responseData);
            }
            else {
                NSDictionary *results = (NSDictionary *)object;
                VLog(@"requestDowload Result: %@", results);
                
                if(resultBlock) {
                    _serviceRequestBlock(results);
                }
                else {
                    resultBlock(nil);
                    if(failureBlock) {
                        failureBlock([self _buildError:results], responseData);
                    }
                }
            }
        });
    };
    request.errorHandler = ^(NSError *error, NSData *responseData) {
        NSString* newStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        VLog(@"errorHandler: %@", newStr);
        if(failureBlock) failureBlock(error, responseData);
    };
    
    [[GCNetworkRequestQueue sharedQueue] addRequest:request];
}

-(void)requestExerciseCodesWithResultBlock: (VSmartServiceResponseCompletionBlock) resultBlock
                              failureBlock: (VSmartRequestErrorBlock) failureBlock{
    
    if(resultBlock) _serviceRequestBlock = resultBlock;
    if(failureBlock) _failureResultBlock = failureBlock;
    
    NSString *url;
    
    int schoolyear = [([self account].user.userSchoolyearId) intValue];
    
    if ([[self account].user.position isEqualToString:kModeIsStudent]) {
        url = VS_FMT(kEndPointLockInOutStudent, schoolyear);
    } else if ([[self account].user.position isEqualToString:kModeIsTeacher]) {
        url = VS_FMT(kEndPointLockInOutTeacher, schoolyear);
    }
    
    url = [Utils buildUrl:url];
    
    GCNetworkRequest *request = [GCNetworkRequest requestWithURL:[NSURL URLWithString:url]];
    request.requestMethod = GCNetworkRequestMethodGET;
    
    request.completionHandler = ^(NSData *responseData){
        
        TransformJSONDataToNSObject(responseData, ^(id object, NSError *error){
            // Error
            if (error && error.code != GCNetworkRequestUserDidCancelErrorCode) {
                VLog(@"An error occured: %@", error);
                VLog(@"An error occured: %@", responseData);
                if(failureBlock) failureBlock(error, responseData);
            }
            else {
                NSDictionary *results = (NSDictionary *)object;
                VLog(@"requestExerciseCodesWithResultBlock Result: %@", @"success"); //results
                
                if(resultBlock) {
                    
                    NSDictionary *records = [results objectForKey:@"records"];
                    [[NSUserDefaults standardUserDefaults] setCustomObject:records forKey:kLockIOResponseKey];
                    _serviceRequestBlock(results);
                }
                else {
                    resultBlock(nil);
                    if(failureBlock) {
                        failureBlock([self _buildError:results], responseData);
                    }
                }
            }
        });
    };
    request.errorHandler = ^(NSError *error, NSData *responseData) {
        NSString* newStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        VLog(@"requestExerciseCodesWithResultBlock errorHandler: %@", newStr);
        if(failureBlock) failureBlock(error, responseData);
    };
    
    [[GCNetworkRequestQueue sharedQueue] addRequest:request];
}


-(void) logout {
    [self _clearUserInfo];
}

-(void) updateAvatar: (NSString *) avatarUrl {
    AccountInfo *account = [self account];
    account.user.avatar = avatarUrl;
    
    [self _saveAccount:account];
}

#pragma mark - Private Helpers
-(void) _saveAccount: (AccountInfo *) account {
    [Utils saveArchive:account forKey:kGlobalAccount];
}

- (void) copyAccount:(AccountInfo *) account {
    [Utils saveArchive:account forKey:kPreviousAccount];
}

-(void) _saveBookList: (NSArray *) books {
//    NSString *userBooks = VS_FMT(kGlobalBooks, [self account].user.id);
    NSString *userBooks = VS_FMT(kGlobalBooks, [self account].user.email);
    [Utils saveArchive:books forKey:userBooks];
}

-(void) _clearUserInfo {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kGlobalAccount];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kBindResponseKey];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kLockIOResponseKey];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kOAuthResponseKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [Utils saveUserId:nil];
}

-(AccountResponse *) _buildLoginResponse: (NSDictionary *)results {
    
    JSONModelError *error;
    AccountResponse *response = [[AccountResponse alloc] initWithDictionary:results error:&error];
    //VLog(@"Account: %@", response.account);
    AccountInfo *account = response.account;
    [self _saveAccount:account];
    [self copyAccount:account];
    
    return response;
}

-(BookListResponse *) _buildBooklistResponse: (NSDictionary *) results {
    BookListResponse *response = [[BookListResponse alloc] initWithDictionary:results error:nil];
    [self _saveBookList:response.result];
    return response;
}

- (NSString *) _buildLoginRequest: (NSString *) email withPassword: (NSString *) password
                  andPlatformType: (VSmartPlatformType) platformType  {
    
    NSDictionary *device = @{@"platform_type": [NSNumber numberWithInt:platformType], @"device_id": [self uuid]};
    NSDictionary *bodyContent = @{@"email":email, @"password":password, @"device":device};
    
    //NSString *jsonRequest = [NSString stringWithFormat:@"%@", [VSmartHelpers jsonString:bodyContent]];
    NSString *jsonRequest = [VSmartHelpers jsonString:bodyContent];
    //VLogv(jsonRequest);
    return jsonRequest;
}

- (NSError *) _buildError: (NSDictionary*) results {
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    NSString *errorMessage = [results valueForKeyPath:@"error.message"];
    
    [details setValue:errorMessage forKey:NSLocalizedDescriptionKey];
    int errorCode = [[results valueForKeyPath:@"error.status"] intValue];
    
    NSError *_error = [NSError errorWithDomain:@"com.vibebookstore.vsmart" code:errorCode userInfo:details];
    return _error;
}

#pragma mark - Polling
-(void)polling{

    // single fuse switch for enabling|disabling background daemon
    BOOL status = [Utils getUserUpdatsSettings];
    if (status) {
        self.timer = [NSTimer timerWithTimeInterval:60 //60 second is 1 min = times 5 to become 5 minutes
                                             target:self
                                           selector:@selector(pollingStart)
                                           userInfo:nil
                                            repeats:YES];
        
        [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
    }
}

- (void)pollingStart {
    
    if ([self account]) {
        NSLog(@"Note Manager synching...");
        [[NoteDataManager sharedInstance] sync];
    }
}

@end
