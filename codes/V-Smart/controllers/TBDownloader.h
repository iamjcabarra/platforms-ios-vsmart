//
//  TBDownloader.h
//  V-Smart
//
//  Created by Ryan Migallos on 21/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TBDownloaderDelegate <NSObject>
@optional
- (void)didFinishDownloadingBook:(NSString *)bookid;
@end

@interface TBDownloader : NSObject

@property (nonatomic, readonly) NSString *skuString;
@property (nonatomic, readonly) NSString *bookID;
@property (nonatomic, readonly) NSString *userID;
@property (nonatomic, readonly) NSString *downloadURL;
@property (nonatomic, readonly) BOOL active;
@property (nonatomic, weak) id <TBDownloaderDelegate> delegate;

- (instancetype)initWithBook:(NSDictionary *)info url:(NSURLRequest *)url queue:(NSOperationQueue *)queue;
- (void)resume:(NSData *)data;
- (void)cancel;
- (void)pause;
- (void)start;

@end
