//
//  VSmart.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 8/16/13.
//  Copyright (c) 2013 VSmart Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountResponse.h"
#import "BookListResponse.h"
#import "PhotoResponse.h"

#define kGuestListKeyStudent @"GuestListKeyStudent"
#define kGuestListKeyTeacher @"GuestListKeyTeacher"

enum {
	VSmartRequestTypeLogin = 0,
	VSmartRequestTypeList = 1,
};
typedef int VSmartRequestType;

enum {
    VSmartPlatformTypeiPad = 2,
    VSmartPlatformTypeiPhone = 3,
    VSmartPlatformTypeMac = 4
};
typedef int VSmartPlatformType;

typedef NS_ENUM(NSUInteger, VSmartContentType) {
    VSmartContentTypeBook = 1,
    VSmartContentTypePdf,
    VSmartContentTypeRss,
    VSmartContentTypeMagazine
};

typedef NS_ENUM(NSUInteger, VSmartDownloadStatus) {
    VSmartDownloadInProgress = 1,
    VSmartDownloadCompleted,
    VSmartDownloadError,
    VSmartDownloadZipExtraction,
    VSmartDownloadExtracted,
    VSmartDownloadNotStarted,
    VSmartDownloadImported,
    VSmartDownloadPause
};

@class VSmart;

typedef void (^VSmartFileExtractCompletionBlock) (NSDictionary *bookData);
typedef void (^VSmartLoginRequestCompletionBlock) (AccountResponse *accountResponse);
typedef void (^VSmartBooklistRequestCompletionBlock) (BookListResponse *booklistResponse);
typedef void (^VSmartListRequestCompletionBlock) (NSDictionary *responseData);
typedef void (^VSmartRequestErrorBlock) (NSError *error, NSData *responseData);
typedef void (^VSmartUploadCompletionBlock) (PhotoResponse *responseData);
typedef void (^VSmartServiceResponseCompletionBlock) (NSDictionary *responseData);

@interface VSmart : NSObject

+ (VSmart *) sharedInstance;

@property (nonatomic, readonly) NSString *uuid;
@property (nonatomic, readonly) NSString *authKey;
@property (nonatomic, readonly) NSString *email;
@property (nonatomic, retain) UIColor *aliasBackgroundColor;
@property (nonatomic, readonly) AccountInfo *account;

-(void) upload: (UIImage *) image resultBlock: (VSmartUploadCompletionBlock) resultBlock
   failureBlock: (VSmartRequestErrorBlock) failureBlock;

-(void) connect: (NSString *) email withPassword: (NSString *) password andPlatformType:(VSmartPlatformType) platformType
    resultBlock:(VSmartLoginRequestCompletionBlock) resultBlock
   failureBlock:(VSmartRequestErrorBlock) failureBlock;

-(void)connectAsExistingGuest:(NSDictionary*)results resultBlock:(VSmartLoginRequestCompletionBlock)resultBlock failureBlock:(VSmartRequestErrorBlock) failureBlock;

-(void)connectAsGuestWithPosition:(NSString*)position
                     platformType: (VSmartPlatformType) platformType
                      resultBlock: (VSmartLoginRequestCompletionBlock) resultBlock
                     failureBlock: (VSmartRequestErrorBlock) failureBlock  ;

-(void) fetchBooks: (VSmartBooklistRequestCompletionBlock) resultBlock failureBlock:(VSmartRequestErrorBlock) failureBlock;

-(void)requestDowload:(NSString*)sku
             revision:(int)revision
      withResultBlock: (VSmartServiceResponseCompletionBlock) resultBlock
         failureBlock: (VSmartRequestErrorBlock) failureBlock;

-(void) logout;
-(void) updateAvatar: (NSString *) avatarUrl;
-(BOOL) successfullyAuthenticated;
@end
