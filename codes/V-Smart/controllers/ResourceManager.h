//
//  ResourceManager.h
//  V-Smart
//
//  Created by Ryan Migallos on 5/6/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Course.h"
#import "Topic.h"

static NSString *kTextBookEntity = @"TextBook";
static NSString *kSchoolCodeEntity = @"SchoolCode";
static NSString *kCourseEntity = @"Course";
static NSString *kModuleEntity = @"Module";
static NSString *kTopicEntity = @"Topic";
static NSString *kBadWordsEntity = @"BadWords";
static NSString *kQuizItemEntity = @"QuizItem";
static NSString *kQuestionItemEntity = @"QuestionItem";
static NSString *kQuestionChoiceItemEntity = @"QuestionChoiceItem";
static NSString *kQuizDetailsEntity = @"QuizDetails";
static NSString *kGradeBookEntity = @"GradeBookRecord";
static NSString *kPlayListEntity = @"PlayList";
static NSString *kPlayListFileEntity = @"PlayListFile";
static NSString *kShortcutPeopleEntity = @"People";
static NSString *kSocialStreamFeedEntity = @"SocialStreamFeed";
static NSString *kSocialStreamCommentEntity = @"SocialStreamComment";
static NSString *kSocialStreamLikeEntity = @"SocialStreamLike";
static NSString *kSocialStreamPreviewEntity = @"SocialStreamPreviewData";
static NSString *kGlobalNotificationEntity = @"GlobalNotification";

static NSString *kNotificationProfileReloadV2 = @"NOTIF_RELOAD_AVATAR";

/////// BLOCK TYPES //////
typedef void (^ResourceManagerDoneBlock)(BOOL status);
typedef void (^ResourceManagerDoneProgressBlock)(NSString *status);
typedef void (^ResourceManagerListBlock)(NSArray *list);
typedef void (^ResourceManagerContent)(NSString *content);
typedef void (^ResourceManagerDataBlock)(NSDictionary *data);
typedef void (^ResourceManagerDownloadBlock)(CGFloat progress);
typedef void (^ResourceManagerBinaryBlock)(NSData *binary);

@interface ResourceManager : NSObject

@property (nonatomic, readonly) NSManagedObjectContext *mainContext;
@property (nonatomic, copy, readwrite) ResourceManagerDownloadBlock progressBlock;
@property (nonatomic, copy, readwrite) ResourceManagerDoneProgressBlock doneProgressBlock;
@property (nonatomic, assign) BOOL isTextBookModuleFirstLoaded;
@property (nonatomic, readwrite) NSManagedObjectContext *workerContext;

+ (instancetype)sharedInstance;
- (void)saveContext;
- (void)backgroundSaveContext;

/////// PUBLIC METHODS //////
- (NSString *)baseURL;
- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value;
- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value;
- (void)updateResourceWithObject:(NSDictionary *)data;
- (void)updateResourceWithObject:(NSDictionary *)data import:(BOOL)flag;
- (void)deleteManagedObject:(NSManagedObject *)object;
- (NSManagedObject *)getEntity:(NSString *)entity attribute:(NSString *)attribute parameter:(NSString *)parameter context:(NSManagedObjectContext *)context;
- (NSUInteger)countRecordsForEntity:(NSString *)entity;
- (void)moveTextBookResources;

- (void)setSchoolCodeValue:(NSString *)code redirect:(BOOL)redirect;
- (NSString *)getSchoolCodeValue;
- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate;

/////// PEARSON //////
- (void)requestModuleTypes:(ResourceManagerDoneBlock)doneBlock;
//- (void)requestCourseListForUserID:(NSString *)userid doneBlock:(ResourceManagerDoneBlock)doneBlock;
//- (NSArray *)fetchCourseListForUserID:(NSString *)userid;

/////// PEARSON V-SMART ACTIVITY LOG API ///////
- (void)requestLogActivityWithModuleType:(NSString *)type details:(NSString *)details;
- (void)requestLogoutWithBlock:(ResourceManagerDoneBlock)doneBlock;

/////// PEARSON ANALYTICS //////
- (void)readAnalytics:(Course *)course doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)exitTopicAnalytics:(Course *)course doneBlock:(ResourceManagerDoneBlock)doneBlock;

/////// SOCIAL STREAM //////
- (void)stringByStrippingHTML:(NSString *)htmlString contentBlock:(ResourceManagerContent)contentBlock;
- (void)requestSocialStreamStickers:(ResourceManagerListBlock)listBlock;
- (void)requestSocialStreamEmojis:(ResourceManagerListBlock)listBlock;
- (void)requestSocialStreamEmoticons:(ResourceManagerListBlock)listBlock;
- (NSString *)loginUser;
- (void)requestSocialStreamMessageWithGroupID:(NSString *)groupid doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)requestSocialStreamPrevMessageWithGroupID:(NSUInteger)groupid messageID:(NSUInteger)messageid doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)requestSocialStreamGroups:(ResourceManagerListBlock)listBlock;
- (void)requestBadWordsDoneBlock:(ResourceManagerDoneBlock)doneBlock;
- (NSDictionary *)fetchBadwords;
- (void)postMessage:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)editMessage:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)requestRemoveMessage:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)postComment:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)editComment:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)requestRemoveComment:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)postLikeMessage:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)postUnlikeMessage:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)requestSocialStreamMessageWithGroupID:(NSString *)groupid dataBlock:(ResourceManagerDataBlock)dataBlock;

/////// AUTHENTICATION //////
- (void)requestAuthentication:(ResourceManagerDoneBlock)doneBlock;
- (void)requestDeviceBind:(ResourceManagerDoneBlock)doneBlock;

/////// SOCKET IO //////
- (void)reconnectSocketWithURL:(NSString *)connect port:(NSInteger)port;
- (void)closeSocket;

/////// SOCKET IO NEW (Swift)//////
- (void)startSocketIOClient;
- (void)socketEvent:(NSString *)event info:(NSDictionary *)info dataBlock:(ResourceManagerDataBlock)dataBlock;
- (void)stopSocketIOClient;

///////// TEXTBOOK ////////
- (void)requestBookList:(ResourceManagerListBlock)listBlock;
- (void)requestBookID:(NSString *)bookid sku:(NSString *)sku;
- (void)pauseDownloadForBook:(NSString *)bookid;
- (void)resumeDownloadForBook:(NSString *)bookid sku:(NSString *)sku;
- (void)updateDownloadFlagForBook:(NSString *)bookid;
- (void)requestVersionCheckBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)requestServerVersionBlock:(ResourceManagerDoneBlock)doneBlock;

/////// TEST PLAYER //////
- (NSArray *)generateCourseList;
- (void)requestQuizListForUser:(NSString *)userid course:(NSString *)courseid doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)requestPreRunQuizDetailsForUser:(NSString *)userid quiz:(NSString *)quizid course:(NSString *)courseid doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)requestRunQuizDetailsForUser:(NSString *)userid quiz:(NSString *)quizid course:(NSString *)courseid run:(BOOL)run doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)submitQuizForUser:(NSDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock;

- (NSDictionary *)fetchQuizDataForQuizID:(NSString *)quizid;
- (void)setAnswerObject:(NSDictionary *)answer doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (NSArray *)fetchQuestionsForQuizID:(NSString *)quizid;
- (NSArray *)fetchChoicesForQuestionID:(NSString *)questionid;

/////// TEST GURU //////
- (void)requestQuestionCountForUser:(NSString *)userid dataBlock:(ResourceManagerDataBlock)dataBlock;
- (void)requestTestCountForUser:(NSString *)userid dataBlock:(ResourceManagerDataBlock)dataBlock;
- (void)requestDeployedTestCountForUser:(NSString *)userid dataBlock:(ResourceManagerDataBlock)dataBlock;
- (void)requestQuestionListForUser:(NSString *)userid doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)downloadImageTestPreview:(NSString *)image_url_string dataBlock:(ResourceManagerBinaryBlock)dataBlock;

/////// PLAYLIST //////
//- (void)requestPlayListForUser:(NSString *)userid doneBlock:(ResourceManagerDoneBlock)doneBlock;
//- (void)requestUpdatePlayListItemWithMeta:(NSDictionary *)meta doneBlock:(ResourceManagerDoneBlock)doneBlock;
//- (void)requestUpdatePlayListItemForUploadID:(NSString *)uploadid meta:(NSDictionary *)meta file:(NSDictionary *)object doneBlock:(ResourceManagerDoneBlock)doneBlock;
//- (void)updateProgressForUUID:(NSString *)uuid progress:(NSString *)progress;
//- (void)updatePlayListFileForUUID:(NSString *)uuid rawFile:(NSData *)rawFile;
//- (void)requestDeletePlayListItemWithUploadID:(NSString *)upload_id doneBlock:(ResourceManagerDoneBlock)doneBlock;

/////// PEOPLESHORTCUT //////
- (void)requestUserProfileWithID:(NSString *)usedid dataBlock:(ResourceManagerDataBlock)dataBlock;
- (void)requestPeopleForUserID:(NSString *)userid doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)requestPeopleForUserID:(NSString *)userid dataBlock:(ResourceManagerDataBlock)dataBlock;
- (void)downloadImagePeopleShortcut:(NSManagedObject *)object dataBlock:(ResourceManagerBinaryBlock)dataBlock;

/////// GLOBAL NOTIFICATIONS ////////
//- (void)requestNotificationWithBlock:(ResourceManagerContent)contentBlock;
- (void)requestNotificationWithClearData:(BOOL)clearData contentBlock:(ResourceManagerContent)contentBlock;
- (void)requestNotificationDetailsWithMessageID:(NSString *)messageid doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (NSString *)readCountFromNotifications;
- (void)requestPreviewURL:(NSString *)url doneBlock:(ResourceManagerDoneBlock)doneBlock;

/////// CHANGE PASSWORD //////
- (void)requestNewPasswordForUser:(NSString *)userid withPassword:(NSString *)oldpassword
                      newPassword:(NSString *)newPassword doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)setNotificationAsSeen:(NSString *)notification_id;

/////// COURSE /////////
- (void)downloadImageFromQuizID:(NSDictionary *)data dataBlock:(ResourceManagerBinaryBlock)dataBlock;

/////// GRADEBOOK JULIUS //////
//- (void)requestGradeListForUser:(NSString *)userid course:(NSString *)courseid doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)submitAssessmentForUser:(NSString *)userid withData:(NSDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)requestGradebookForClass:(NSString *)classCode doneBlock:(ResourceManagerDoneBlock)doneBlock;
//- (void)requestCourseListForUser:(NSString *)userid dataBlock:(ResourceManagerDataBlock)dataBlock;
- (void)updateSortKeyOfGradeBookSectionHeader:(BOOL)isAscending;

/////// GRADEBOOK CARMELITO ///////
- (void)requestGradeBookForClassID:(NSString *)class_id doneBlock:(ResourceManagerDoneBlock)doneBlock;
//- (void)requestGradeBookForClassID:(NSString *)class_id isAscending:(BOOL)isAscending doneBlock:(ResourceManagerDoneBlock)doneBlock;
//- (void)postGradeBookChangeScoreForData:(NSDictionary *)body doneBlock:(ResourceManagerDoneBlock)doneBlock;
//- (void)postGradeBookSetScoreForData:(NSDictionary *)body doneBlock:(ResourceManagerDoneBlock)doneBlock;
- (void)requestProfileAvatars:(ResourceManagerListBlock)listBlock;
- (void)uploadAvatarImage:(UIImage *)image dataBlock:(ResourceManagerDataBlock)dataBlock;
//- (NSUInteger)countRecordsForEntity:(NSString *)entity attribute:(NSString *)attribute value:(id)value;
- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate;

//- (void)postUpdateOfUploadID:(NSString *)upload_id withPostBody:(NSDictionary *)postBody doneBlock:(ResourceManagerDoneBlock)doneBlock;
//- (void)requestListOfUserForSearchText:(NSString *)searchText listBlock:(ResourceManagerListBlock)listBlock;
//- (void)requestListOfTeachers:(ResourceManagerListBlock)listBlock;

@end
