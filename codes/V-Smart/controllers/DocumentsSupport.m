//
//  DocumentsSupport.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 4/16/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "DocumentsSupport.h"

@implementation DocumentsSupport
{
    NSOperationQueue *workQueue;
}

#pragma mark DocumentsSupport class methods

+ (DocumentsSupport *)sharedInstance
{
	static dispatch_once_t predicate = 0;
	static DocumentsSupport *object = nil; // Object
	dispatch_once(&predicate, ^{ object = [self new]; });
    
	return object; // DocumentsSupport singleton
}

+ (NSString *)documentsPath
{
	NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [documentsPaths objectAtIndex:0];
}

#pragma mark DocumentsUpdate instance methods

- (id)init
{
	if ((self = [super init]))
	{
		workQueue = [NSOperationQueue new];
		[workQueue setName:@"DocumentsSupportWorkQueue"];
		[workQueue setMaxConcurrentOperationCount:1];
	}
    
	return self;
}

- (void)cancelAllOperations
{
	[workQueue cancelAllOperations];
}

- (void)queueDocumentsSupport
{
	if (workQueue.operationCount < 1)
	{
		DocumentsSupportOperation *updateOp = [DocumentsSupportOperation new]; [updateOp setThreadPriority:0.25];
		[workQueue addOperation:updateOp];
	}
}

- (BOOL)handleOpenURL:(NSURL *) theURL {
	BOOL handled = NO; // Handled flag
    
	if ([theURL isFileURL] == YES) // File URLs only
	{
		NSString *inboxFilePath = [theURL path]; // File path string
        
		NSString *inboxPath = [inboxFilePath stringByDeletingLastPathComponent];
        
		if ([[inboxPath lastPathComponent] isEqualToString:@"Inbox"]) // Inbox test
		{
			NSString *documentFile = [inboxFilePath lastPathComponent]; // File name
			NSString *documentsPath = [DocumentsSupport documentsPath]; // Documents path
			NSString *documentFilePath = [documentsPath stringByAppendingPathComponent:documentFile];
            
			NSFileManager *fileManager = [NSFileManager new]; // File manager instance
			[fileManager moveItemAtPath:inboxFilePath toPath:documentFilePath error:NULL]; // Move
            
			[fileManager removeItemAtPath:inboxPath error:NULL]; // Delete Inbox directory
		}
	}
    
	return handled;
}

@end

@implementation DocumentsSupportOperation

#pragma mark DocumentsSupportOperation methods

- (void)main
{
	__autoreleasing NSError *error = nil;
    
	NSString *documentsPath = [DocumentsSupport documentsPath];
    
	NSFileManager *fileManager = [NSFileManager new];
	NSArray *fileList = [fileManager contentsOfDirectoryAtPath:documentsPath error:&error];
    
	if (fileList != nil)
	{
		NSMutableSet *fileSet = [NSMutableSet new];
        
		for (NSString *fileName in fileList)
		{
			if ([[fileName pathExtension] isIn:kBookExtensionEpub, kBookExtensionVibe, kBookExtensionPdf, nil])
			{
                VLog(@"DOCU_FILE: %@", fileName);
				[fileSet addObject:fileName];
			}
		}
        if ([fileSet count] > 0) {
            NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
            [notificationCenter postNotificationName:DocumentsUpdateNotification object:fileSet userInfo:nil];
        }
	}
	else // Log any errors
	{
		NSLog(@"%s %@", __FUNCTION__, error); assert(NO);
	}
}

#pragma mark Notification name strings

NSString *const DocumentsUpdateNotification = @"DocumentsUpdateNotification";

@end
