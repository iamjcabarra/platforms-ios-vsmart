//
//  QuizResultViewController.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 8/1/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "QuizResultViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "CALayer+Additions.h"

//////////////////////////// UIKitDynamics iOS7 ////////////////////////////
@interface VSmartDynamicBehavior : UIDynamicBehavior
- (instancetype)initWithView:(id <UIDynamicItem>)source point:(CGPoint)sourcePoint replaceWithView:(id <UIDynamicItem>)destination snapPoint:(CGPoint)point;
@end

@implementation VSmartDynamicBehavior
- (instancetype)initWithView:(id <UIDynamicItem>)source point:(CGPoint)sourcePoint replaceWithView:(id <UIDynamicItem>)destination snapPoint:(CGPoint)point
{
    if (!(self = [super init])) return nil;
    
    UISnapBehavior *snapA = [[UISnapBehavior alloc] initWithItem:source snapToPoint:sourcePoint];
    snapA.damping = 0.65f;
    [self addChildBehavior:snapA];
    
    UISnapBehavior *snapB = [[UISnapBehavior alloc] initWithItem:destination snapToPoint:point];
    snapB.damping = 0.65f;
    [self addChildBehavior:snapB];
    
    //return animation behavior
    return self;
}
@end
//////////////////////////// UIKitDynamics iOS7 ////////////////////////////

@interface QuizResultViewController ()

//SURVEY RELATED PROPERTIES
@property (strong, nonatomic) IBOutlet UIView *exerciseContainer;
@property (strong, nonatomic) IBOutlet UISlider *slider;
@property (strong, nonatomic) IBOutlet UILabel *surveyRatingDisplay;
@property (strong, nonatomic) IBOutlet UIView *surveyContainerView;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) IBOutlet UIImageView *emotionImageView;

@property (strong, nonatomic) NSArray *surveyValues;

@property (strong, nonatomic) ResourceManager *rm;

@end

@implementation QuizResultViewController
@synthesize delegate;
@synthesize displayResult, result;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated {
    //displayResult.text = @"";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    displayResult.text = result;
    
    self.rm = [AppDelegate resourceInstance];
    
    NSLog(@"XXXXXX RESULT OBJECT : %@", self.assessmentData);
    
    [self performSelector:@selector(animateText) withObject:nil afterDelay:.50];
    
    self.view.layer.cornerRadius = 10;
    self.view.layer.masksToBounds = YES;
    
    self.headerLabel.layer.mask = [CALayer maskLayerWithCorners:UIRectCornerTopLeft | UIRectCornerTopRight radii:CGSizeMake(10.0f, 10.0f) frame:self.headerLabel.bounds];
    self.blackBackgroundLabelTop.layer.mask = [CALayer maskLayerWithCorners:UIRectCornerTopLeft | UIRectCornerTopRight radii:CGSizeMake(10.0f, 10.0f) frame:self.blackBackgroundLabelTop.bounds];
    
    self.closeWindowButton.layer.mask = [CALayer maskLayerWithCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight radii:CGSizeMake(10.0f, 10.0f) frame:self.closeWindowButton.bounds];
    self.blackBackgroundLabelBottom.layer.mask = [CALayer maskLayerWithCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight radii:CGSizeMake(10.0f, 10.0f) frame:self.blackBackgroundLabelBottom.bounds];

    // Set Survey View
    [self setupSurveyView];
}

- (void) animateText {
    CAKeyframeAnimation *scaleAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    NSArray *scaleValues                = [NSArray arrayWithObjects:
                                           [NSValue valueWithCATransform3D:CATransform3DScale(self.view.layer.transform, 1, 1, 1)],
                                           [NSValue valueWithCATransform3D:CATransform3DScale(self.view.layer.transform, 1.1, 1.1, 1)],
                                           [NSValue valueWithCATransform3D:CATransform3DScale(self.view.layer.transform, 1, 1, 1)], nil];
    [scaleAnimation setValues:scaleValues];
    scaleAnimation.fillMode             = kCAFillModeForwards;
    scaleAnimation.removedOnCompletion  = NO;
    
    [self.displayResult.layer addAnimation:scaleAnimation forKey:@"scale"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeWindow:(id)sender {
    
//    if (self.delegate && [self.delegate respondsToSelector:@selector(didCloseQuizResultWindow:)]) {
//        [self.delegate didCloseQuizResultWindow:self];
//    }
    
    [self setupVSmartDynamicBehavior];
}

- (void)dealloc {
    [displayResult release];
    [_blackBackgroundLabelBottom release];
    [_blackBackgroundLabelTop release];
    [_closeWindowButton release];
    [_headerLabel release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setBlackBackgroundLabelBottom:nil];
    [self setBlackBackgroundLabelTop:nil];
    [self setCloseWindowButton:nil];
    [self setHeaderLabel:nil];
    [super viewDidUnload];
}

/////////////////////////////////////////////////////////////////////////
///////////////////////////// SURVEY RATING /////////////////////////////
/////////////////////////////////////////////////////////////////////////

- (void)setupSurveyView
{
    self.view.backgroundColor = [UIColor clearColor];
    
    self.exerciseContainer.layer.cornerRadius = 10;
    self.exerciseContainer.layer.masksToBounds = YES;
    
    self.surveyContainerView.layer.cornerRadius = 10;
    self.surveyContainerView.layer.masksToBounds = YES;
    
    // Set initial position for the container
    self.surveyContainerView.frame = CGRectMake(-1024, 1100, 508, 314);
    
    // UI
    [self setupSlider];
}
- (void)setupSlider
{
    /* localizable strings */
    NSString *frustratedLabel = NSLocalizedString(@"frustrated", nil);
    NSString *pitifulLabel = NSLocalizedString(@"pitiful", nil);
    NSString *sadLabel = NSLocalizedString(@"sad", nil);
    NSString *neutralLabel = NSLocalizedString(@"neutral", nil);
    NSString *satisfiedLabel = NSLocalizedString(@"satisfied", nil);
    NSString *happyLabel = NSLocalizedString(@"happy", nil);
    NSString *ecstaticLabel = NSLocalizedString(@"ecstatic", nil);
    
    self.surveyValues = @[
                          @{ @"state"       : frustratedLabel,
                             @"image"       : @"01_survey_frustrated.png",
                             @"trackimage"  : @"slider_red_track.png",
                             @"color"       : @0,
                             @"rate"        : @0 },
                          
                          @{ @"state"       : pitifulLabel,
                             @"image"       : @"02_survey_pitiful.png",
                             @"trackimage"  : @"slider_red_track.png",
                             @"color"       : @0,
                             @"rate"        : @1 },
                          
                          @{ @"state"       : sadLabel,
                             @"image"       : @"03_survey_sad.png",
                             @"trackimage"  : @"slider_red_track.png",
                             @"color"       : @0,
                             @"rate"        : @2 },
                          
                          @{ @"state"       : neutralLabel,
                             @"image"       : @"04_survey_neutral.png",
                             @"trackimage"  : @"slider_gray_track.png",
                             @"color"       : @1,
                             @"rate"        : @3 },
                          
                          @{ @"state"       : satisfiedLabel,
                             @"image"       : @"05_survey_satisfied.png",
                             @"trackimage"  : @"slider_green_track.png",
                             @"color"       : @2,
                             @"rate"        : @4 },
                          
                          @{ @"state"       : happyLabel,
                             @"image"       : @"06_survey_happy.png",
                             @"trackimage"  : @"slider_green_track.png",
                             @"color"       : @2,
                             @"rate"        : @5 },
                          
                          @{ @"state"       : ecstaticLabel,
                             @"image"       : @"07_survey_ecstatic.png",
                             @"image"       : @"slider_green_track.png",
                             @"color"       : @3,
                             @"rate"        : @6 }];
    
    // Set min and max values
    self.slider.minimumValue = 0;
    self.slider.maximumValue = ( (float)[self.surveyValues count] - 1);
    
    // Set Initial Index
    NSUInteger index = (NSUInteger)((self.slider.maximumValue / 2) + 0.5);

    [self updateComponentsWithIndex:index];

    // Add action
    [self.slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    // Set up submit button
    [self setupSubmitButton];
}

- (void)sliderValueChanged:(UISlider *)sender
{
    NSUInteger index = (NSUInteger)(self.slider.value + 0.5); // Round the number.
    
    [self updateComponentsWithIndex:index];
}

- (void)updateComponentsWithIndex:(NSUInteger)index
{
    [self.slider setValue:index animated:NO];
    
    NSDictionary *data  = (NSDictionary *)self.surveyValues[index]; // <-- This is the number you want.
    VLog(@"data : %@", data);
    
    NSArray *color = @[[UIColor redColor], [UIColor darkGrayColor], [UIColor greenColor], [UIColor blueColor]];
    
    
    self.emotionImageView.image = [UIImage imageNamed:data[@"image"]];
    
    int colorIndex = [ data[@"color"] intValue];
    self.surveyRatingDisplay.textColor = color[colorIndex];
    
    NSString *trackImageString = data[@"trackimage"];
    
    [self.slider setMinimumTrackImage:[UIImage imageNamed:trackImageString] forState:UIControlStateNormal];
//    [self.slider setMaximumTrackImage:[UIImage imageNamed:trackImageString] forState:UIControlStateNormal];
    
    self.surveyRatingDisplay.text = data[@"state"];
}

- (void)setupSubmitButton
{
    [self.submitButton addTarget:self action:@selector(submitButtonAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)submitButtonAction:(id)sender
{
    //TODO
    //Perform caching from slider results
    /*
     exercise id
     book UUID
     subject id
     account id
     test rating
     */
    
    NSString *user_id = self.assessmentData[@"user_id"];

    
    [self.rm submitAssessmentForUser:user_id withData:self.assessmentData doneBlock:^(BOOL status) {
        //Dismiss the modal view
        if (self.delegate && [self.delegate respondsToSelector:@selector(didCloseQuizResultWindow:)]) {
            [self.delegate didCloseQuizResultWindow:self];
        }
    }];
    
//    //Dismiss the modal view
//    if (self.delegate && [self.delegate respondsToSelector:@selector(didCloseQuizResultWindow:)]) {
//        [self.delegate didCloseQuizResultWindow:self];
//    }
}

- (void)setupVSmartDynamicBehavior
{
    // Create UIKit Dynamic Animator Object iOS 7
    UIDynamicAnimator *animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    CGPoint pointA = CGPointMake(-1024, 1100);
    CGPoint pointB = CGPointMake((self.view.bounds.size.width/2), (self.view.bounds.size.height/2));
    
    // Instantiate VSmartDynamicBehavior
    VSmartDynamicBehavior *uidynamics = [[VSmartDynamicBehavior alloc] initWithView:self.exerciseContainer
                                                                              point:pointA
                                                                    replaceWithView:self.surveyContainerView
                                                                          snapPoint:pointB];
    [animator addBehavior:uidynamics];
}

@end
