//
//  SettingsViewController.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 7/30/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "SettingsViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface SettingsViewController ()
{
    int appearanceIndex;
    int fontNameIndex;
    int fontSize;
    int savedFontSize;
    float brightness;
    
    NSArray *fontOptions;
    NSArray *themeOptions;
    NSArray *headerItems;
    UISlider *slider;
}
@end

@implementation SettingsViewController
@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView = nil;
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"p6"]];
//    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"p6"]];
    //self.view.backgroundColor = [UIColor colorWithRed:241/255.0 green:237/255.0 blue:219/255.0 alpha:1.0];
    //self.tableView.backgroundColor = [UIColor colorWithRed:245/255 green:239/255 blue:222/255 alpha:1.0];
    self.view.backgroundColor = UIColorFromHex(0xfcfcfc);
    
    NSDictionary *settings = [Utils getBookSetings];
    
    [self setupOptions];
    
    appearanceIndex = [[settings objectForKey:@"book_appearance_index"] intValue];
    fontNameIndex = [[settings objectForKey:@"font_name_index"] intValue];
    savedFontSize = [[settings objectForKey:@"font_size"] intValue];
    fontSize = [[settings objectForKey:@"font_size"] intValue];
    brightness = [[settings objectForKey:@"brightness_level"] floatValue];
}

- (void) setupOptions {
    fontOptions = [[NSArray alloc] initWithObjects:@{@"name": @"Default", @"style":@"TimesNewRomanPSMT"}, @{@"name": @"Thonburi", @"style":@"Thonburi"},
                   @{@"name": @"Helvetica Neue Light", @"style":@"HelveticaNeue-Light"}, @{@"name": @"Georgia", @"style":@"Georgia"},
                   @{@"name": @"Cochin", @"style":@"Cochin"}, nil];
    
    //fontOptions = @[@{@"name": @"Default", @"style":@"TimesNewRomanPSMT"}, @{@"name": @"Thonburi", @"style":@"Thonburi"},
    //                @{@"name": @"Helvetica Neue Light", @"style":@"HelveticaNeue-Light"}, @{@"name": @"Georgia", @"style":@"Georgia"},
    //                @{@"name": @"Cochin", @"style":@"Cochin"}];
    
    themeOptions = [[NSArray alloc] initWithObjects:@"Default", @"Sepia", @"Night", nil];
    //themeOptions = @[@"Default", @"Sepia", @"Night"];
    
    
    /* localize string */
    NSString *fontStyleLabel = NSLocalizedString(@"Font Style", nil);
    NSString *brightnessLabel = NSLocalizedString(@"Brightness Level", nil);
    NSString *fontAdjustmentLabel = NSLocalizedString(@"Font Adjustment", nil);
    NSString *bookAppearanceLabel = NSLocalizedString(@"Book Appearance", nil);
    
    headerItems = [[NSArray alloc] initWithObjects:fontStyleLabel, brightnessLabel, fontAdjustmentLabel, bookAppearanceLabel, nil];
//    headerItems = [[NSArray alloc] initWithObjects:@"Font Style", @"Brightness Level", @"Font Adjustment", @"Book Appearance", nil];
    //headerItems = @[@"Font Adjustment", @"Font Style", @"Book Appearance"];
}

#pragma mark - Custom Methods
- (void) adjustFontSize: (UISlider *) sender {
    VLog(@"adjustFontSize value = %li", lroundf(sender.value));
    if (fontSize != savedFontSize) {
        //fontSize = lroundf(sender.value);
        [self saveSettings];
    }
}

- (void) changeSliderValue: (UISlider *) sender {
    //VLog(@"changeSliderValue value = %li", lroundf(sender.value));
    fontSize = lroundf(sender.value);
}

- (void) changeBrightnessSliderValue: (UISlider *) sender {
    brightness = (float)(sender.value);
    //VLog(@"changeBrightnessSliderValue value = %.02f", (float)(sender.value));
    [self saveSettings];
}

- (void) saveSettings {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    dict[@"font_size"] = [NSNumber numberWithInt: fontSize];
    dict[@"book_appearance_index"] = [NSNumber numberWithInt: appearanceIndex];
    dict[@"font_name_index"] = [NSNumber numberWithInt: fontNameIndex];
    dict[@"font_name"] = [[fontOptions objectAtIndex:fontNameIndex] objectForKey:@"style"];
    dict[@"book_appearance_name"] = [themeOptions objectAtIndex:appearanceIndex];
    dict[@"brightness_level"] = [NSNumber numberWithFloat: brightness];
    
    [Utils saveBookSettings:dict];
    [self.tableView reloadData];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didFinishChangeSettings:withValues:)]) {
        NSDictionary *values = @{@"font_size": [NSNumber numberWithInt: fontSize],
                                 @"font_name": dict[@"font_name"],
                                 @"theme": dict[@"book_appearance_name"],
                                 @"brightness_level": [NSNumber numberWithFloat:brightness]};
        [self.delegate didFinishChangeSettings:self withValues:values];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (UISlider *) createSlider {
//    slider = [[UISlider alloc] initWithFrame:CGRectMake(10, 0, 300, 44)];
//    slider.maximumValue = 10;
//    slider.minimumValue = 0;
//    
//    UIImage *sliderLeftTrackImage = [UIImage imageNamed: @"icn_font_down"];
//    UIImage *sliderRightTrackImage = [UIImage imageNamed: @"icn_font_up"];
//    [slider setMinimumValueImage:sliderLeftTrackImage];
//    [slider setMaximumValueImage:sliderRightTrackImage];
//
//}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return 5;
    }
    else if (section == 1) {
        return 1;
    } else if (section == 2) {
        return 1;
    }
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        slider = [[UISlider alloc] initWithFrame:CGRectMake(10, 0, 300, 44)];
        slider.maximumValue = 10;
        slider.minimumValue = 0;
        slider.tag = 11;
        
        UIImage *sliderLeftTrackImage = [UIImage imageNamed: @"icn_font_down"];
        UIImage *sliderRightTrackImage = [UIImage imageNamed: @"icn_font_up"];
        [slider setMinimumValueImage:sliderLeftTrackImage];
        [slider setMaximumValueImage:sliderRightTrackImage];
        //[slider setValue:fontSize];
        
        [slider addTarget:self action:@selector(changeSliderValue:) forControlEvents:UIControlEventValueChanged];
        [slider addTarget:self action:@selector(adjustFontSize:) forControlEvents:UIControlEventTouchUpInside];
        
        //if(indexPath.section == 0)[cell.contentView addSubview:slider];
        
        slider.bounds = CGRectMake(0, 0, cell.contentView.bounds.size.width - 10, slider.bounds.size.height);
        slider.center = cell.contentView.center;
        slider.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        
//        [slider setThumbImage:[UIImage imageNamed:@"sliderKnob"] forState:UIControlStateNormal];
//        [slider setMinimumTrackImage:[[UIImage imageNamed:@"sliderBar"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
//        [slider setMaximumTrackImage:[[UIImage imageNamed:@"sliderDefault"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
        
        UISlider *brightnessSlider = [[UISlider alloc] initWithFrame:CGRectMake(10, 0, 300, 44)];
        brightnessSlider.maximumValue = 1;
        brightnessSlider.minimumValue = 0;
        brightnessSlider.tag = 10;
        
        UIImage *brightnessLeftTrackImage = [UIImage imageNamed: @"btn_light_l"];
        UIImage *brightnessRightTrackImage = [UIImage imageNamed: @"btn_light_r"];
        [brightnessSlider setMinimumValueImage:brightnessLeftTrackImage];
        [brightnessSlider setMaximumValueImage:brightnessRightTrackImage];
        //[slider setValue:fontSize];
        
        [brightnessSlider addTarget:self action:@selector(changeBrightnessSliderValue:) forControlEvents:UIControlEventValueChanged];
        //[brightnessSlider addTarget:self action:@selector(adjustFontSize:) forControlEvents:UIControlEventTouchUpInside];
        
        //if(indexPath.section == 0)[cell.contentView addSubview:brightnessSlider];
        
        brightnessSlider.bounds = CGRectMake(0, 0, cell.contentView.bounds.size.width - 10, brightnessSlider.bounds.size.height);
        brightnessSlider.center = cell.contentView.center;
        brightnessSlider.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        if (indexPath.section == 1) {
            [cell.contentView addSubview:brightnessSlider];
            [brightnessSlider release];
        } else if (indexPath.section == 2) {
            [cell.contentView addSubview:slider];
        }
    }
    
    NSString *fontStyle = @"";
    NSString *fontName = @"";
    
    if (indexPath.section == 1) {
        [(UISlider *)[cell.contentView viewWithTag:10] setValue:brightness];
    } else if (indexPath.section == 2) {
        [slider setValue:fontSize];
        tableView.separatorColor = [UIColor clearColor];
    } else if (indexPath.section == 0) {
        
        NSDictionary *fontSettings = [fontOptions objectAtIndex:indexPath.row];
        fontStyle = [fontSettings objectForKey:@"style"];
        fontName = [fontSettings objectForKey:@"name"];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.textColor = [UIColor blackColor];
        if (indexPath.row == fontNameIndex) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            fontStyle = [NSString stringWithFormat:@"%@-Bold", fontStyle];
            //cell.textLabel.textColor = [UIColor orangeColor];
        }
        cell.textLabel.text = fontName;
        cell.textLabel.font = [UIFont fontWithName:fontStyle size:16];
        //tableView.separatorColor = UIColorFromHex(0xc3c3c3);
    } else if (indexPath.section == 3) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        fontStyle = @"HelveticaNeue";
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.text = [themeOptions objectAtIndex:indexPath.row];

        if (indexPath.row == appearanceIndex) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            fontStyle = [NSString stringWithFormat:@"%@-Bold", fontStyle];
            //cell.textLabel.textColor = [UIColor orangeColor];
        }
        cell.textLabel.font = [UIFont fontWithName:fontStyle size:16];
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSString *sectionName;
    
    sectionName = [headerItems objectAtIndex:section];
    UIView *customTitleView = [ [UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 44)];
    
    UILabel *titleLabel = [ [UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 44)];
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
    titleLabel.text = [sectionName uppercaseString];
    titleLabel.textColor = [UIColor darkGrayColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    [customTitleView addSubview:titleLabel];
    
    return customTitleView;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *customTitleView = [ [UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
//    customTitleView.layer.shadowColor = [UIColor blackColor].CGColor;
//    customTitleView.layer.shadowOffset = CGSizeMake(0, -2);
//    customTitleView.layer.shadowOpacity = 0.75f;
    //customTitleView.backgroundColor = [UIColor blueColor];
    
    UILabel *titleLabel = [ [UILabel alloc] initWithFrame:CGRectMake(0, 18, 320, 2)];
    titleLabel.backgroundColor = UIColorFromHex(0xc3c3c3);
    [customTitleView addSubview:titleLabel];
    
    return customTitleView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 20.0f;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        fontNameIndex = indexPath.row;
    } else if (indexPath.section == 1) {

    } else if (indexPath.section == 3) {
        appearanceIndex = indexPath.row;
    }
    
    [self saveSettings];
}

@end
