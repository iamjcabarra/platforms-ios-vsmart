//
//  QuizMedalViewController.m
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/17/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "QuizMedalViewController.h"
#import "CALayer+Additions.h"
#import "QuizResultCell.h"
#import "BookExerciseResult.h"

@interface QuizMedalViewController ()
{
    NSDateFormatter *formatter;
}
@end

@implementation QuizMedalViewController
@synthesize delegate;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.layer.cornerRadius = 10;
    self.view.layer.masksToBounds = YES;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    self.bookTitle.layer.mask = [CALayer maskLayerWithCorners:UIRectCornerTopLeft | UIRectCornerTopRight radii:CGSizeMake(10.0f, 10.0f) frame:self.bookTitle.bounds];
//
    self.closeButton.layer.mask = [CALayer maskLayerWithCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight radii:CGSizeMake(10.0f, 10.0f) frame:self.closeButton.bounds];
    
    TapDetectingWindow *tapWindow = (TapDetectingWindow *)[[UIApplication sharedApplication].windows objectAtIndex:0];
    tapWindow.controllerThatObserves = nil;
    tapWindow.viewToObserve = nil;
}

- (NSString *) _getMedalSetsForIndex: (NSInteger) index {
    /*
     ((3/4)*50)+50 = 87.5 = Bronze
     ((4/5)*50)+50 = 90   = Silver
     ((3/3)*50)+50 = 100  = Platinum
     
     ave: ((10/12)*50)+50 = 91.67 = Silver
     
     The following are the values of medals:
     Platinum: 98 - 100 %
     Gold: 95 - 97.99 %
     Silver: 90 - 94.99 %
     Bronze: 85 - 89.99 %
     */
    BookExerciseResult *result = [_items objectAtIndex:index];
    NSString *imageSetName = @"no_medal";
    
    float grade = ((float)(result.result)/(float)result.bookExercise.totalItems) * 50 + 50;
    
    if (grade >= 85 && grade < 90) {
        // bronze
        imageSetName = @"bronze";
    } else if (grade >= 90 && grade < 95) {
        // gold
        imageSetName = @"silver";
    } else if (grade >= 95 && grade < 98) {
        imageSetName = @"gold";
    } else if (grade >= 98) {
        // platinum
        imageSetName = @"platinum";
    }
    
    return imageSetName;
}

- (NSArray *) _getImageArrayForSet: (NSString *) imageSetName {
    NSMutableArray *imageArray = [[NSMutableArray alloc] init];
    NSString *imageName = @"";
    
    for (int i = 0; i < 12; i++) {
        imageName = [NSString stringWithFormat:@"%@%i.png", imageSetName, i + 1];
        [imageArray addObject:[UIImage imageNamed:imageName]];
    }
    return imageArray;
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"QuizResultCellIdentifier";
    static NSString *CellNib = @"QuizResultCell";
    
    QuizResultCell *cell = (QuizResultCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    BookExerciseResult *result = [_items objectAtIndex:indexPath.row];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellNib owner:self options:nil];
        cell = (QuizResultCell *)[nib objectAtIndex:0];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //cell.contentView.backgroundColor = [UIColor colorWithRed:220/255.0 green:237/255.0 blue:245/255.0 alpha:1.0];
        self.bookTitle.text = result.bookTitle;
    }
    
    cell.chapterName.text = result.chapterName;
    cell.latestScore.text = [NSString stringWithFormat:@"%i/%i", result.result, result.bookExercise.totalItems];
    cell.exerciseNumber.text = [NSString stringWithFormat:@"Exercise %i", result.bookExercise.exerciseId];
    
    [formatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
    NSString *dateString = [formatter stringFromDate:result.dateCreated];
    cell.dateTaken.text = dateString;
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSString *imageSet = [self _getMedalSetsForIndex:indexPath.row];
        //VLog(@"ImageSet: %@", imageSet);
        
        BOOL isAnimated = ![imageSet isEqualToString:@"no_medal"];
        NSArray *imageArray = nil;
        if (isAnimated) {
            imageArray = [self _getImageArrayForSet:imageSet];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if(imageArray) {
                cell.medal.animationImages = imageArray;
                cell.medal.animationDuration = 1.0f;
                cell.medal.animationRepeatCount = 0;
                [cell.medal startAnimating];
            } else {
                cell.medal.image = [UIImage imageNamed:imageSet];
            }
        });
    });
    
	return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    UIColor *color = ((indexPath.row % 2) == 0) ? UIColorFromHex(0xf4f4f4) : [UIColor whiteColor];
    cell.contentView.backgroundColor = color;
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)close:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCloseQuizMedalWindow:)]) {
        [self.delegate didCloseQuizMedalWindow:self];
    }
}
@end
