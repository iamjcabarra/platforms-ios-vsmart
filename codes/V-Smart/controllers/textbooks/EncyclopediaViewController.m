//
//  EncyclopediaViewController.m
//  Booklatan
//
//  Created by Earljon Hidalgo on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EncyclopediaViewController.h"

#define BLACK_BAR_COMPONENTS				{ 0.22, 0.22, 0.22, 1.0, 0.07, 0.07, 0.07, 1.0 }

@implementation EncyclopediaViewController
@synthesize baseURL, filePath, urlRequest;
@synthesize vibeDRM, webView, fileUrl;

#pragma mark - View lifecycle

- (id)initWithFrame:(CGRect)frame title:(NSString *)title andFilePath:(NSString *) htmlPath andUrl:(NSString *) url{
	if ((self = [super initWithFrame:frame])) {
		
		//CGFloat colors[8] = BLACK_BAR_COMPONENTS;
		//[self.titleBar setColorComponents:colors];
		self.headerLabel.text = title;
        self.contentColor = [UIColor blueColor];// [UIColor colorWithPatternImage:[UIImage imageNamed:@"img_login_bg"]];
        
        // The line mode of the gradient view (top, bottom, both, none). Top is a white line, bottom is a black line.
        //[[self titleBar] setLineMode: pow(2, (arc4random() % 3))];
        //[[self titleBar] setLineMode:UAGradientBackgroundStyleLinearReversed];
        
        // The noise layer opacity. Default = 0.4
        //[[self titleBar] setNoiseOpacity:(((arc4random() % 10) + 1) * 0.1)];
        
        // The header label, a UILabel with the same frame as the titleBar
        [self headerLabel].font = [UIFont boldSystemFontOfSize:floor(self.titleBarHeight / 2.0)];
        
		//UIWebView *wv = [[[UIWebView alloc] initWithFrame:CGRectZero] autorelease];
        //wv.delegate = self;
        //wv.scalesPageToFit = YES;
        self.webView = [[UIWebView alloc] initWithFrame:CGRectZero];
        [self.webView setDelegate:self];
        [self.webView setScalesPageToFit:YES];
        
        // Initialize DRM
//#if defined (CONSUMER)
//        NSString *drmKey = [NSString stringWithFormat:DRM_KEY, [Utilities getEmail], [Utilities getDeviceId]];
//        vibeDRM = [[VibeCrypto alloc] initWithKey:drmKey];
//#else
//        vibeDRM = [[VibeCrypto alloc] initWithKey:DRM_KEY];
//#endif
        self.fileUrl = url;
        self.filePath = htmlPath;
        VLog(@"Ency filePath: %@", self.filePath);
        
        //VLog(@"EncyclopediaViewController: FILEPATH: %@", self.filePath);
        NSURLRequest *currRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:htmlPath]];
        //NSData *htmlData = [NSData dataWithContentsOfFile:self.filePath];
        //NSString *currData = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
        //[wv loadHTMLString:currData baseURL:self.baseURL];
        //[currData release];
		//NSFileManager *fm  = [NSFileManager defaultManager];
        
        //[wv loadRequest:currRequest];
        [self.webView loadRequest:currRequest];
		
        //[wv loadRequest:self.url];
		//[wv loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://urbanapps.com/product_list"]]];
        //        for (id subview in wv.subviews)
        //            if ([[subview class] isSubclassOfClass: [UIScrollView class]])
        //                ((UIScrollView *)subview).bounces = NO;
        //
        //		v = [wv retain];
        
        for (id subview in self.webView.subviews) {
            if ([[subview class] isSubclassOfClass: [UIScrollView class]]) {
                ((UIScrollView *)subview).bounces = NO;
            }
        }
        
		v = [self.webView retain];
		[self.contentView addSubview:v];
		
	}	
	return self;
}

- (id)initWithFrame:(CGRect)frame title:(NSString *)title andHtmlString:(NSString *) html withBaseURL:(NSURL *)theBaseURL {
	if ((self = [super initWithFrame:frame])) {
		
		//CGFloat colors[8] = BLACK_BAR_COMPONENTS;
		//[self.titleBar setColorComponents:colors];
		self.headerLabel.text = title;
        self.contentColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"img_login_bg"]];

        // The line mode of the gradient view (top, bottom, both, none). Top is a white line, bottom is a black line.
        //[[self titleBar] setLineMode: pow(2, (arc4random() % 3))];
        //[[self titleBar] setLineMode:UAGradientBackgroundStyleLinearReversed];
        
        // The noise layer opacity. Default = 0.4
        //[[self titleBar] setNoiseOpacity:(((arc4random() % 10) + 1) * 0.1)];
        
        // The header label, a UILabel with the same frame as the titleBar
        [self headerLabel].font = [UIFont boldSystemFontOfSize:floor(self.titleBarHeight / 2.0)];
        
		UIWebView *wv = [[[UIWebView alloc] initWithFrame:CGRectZero] autorelease];
        wv.delegate = self;
        wv.scalesPageToFit = YES;
        
        //VLog(@"EncyclopediaViewController: FILEPATH: %@", self.filePath);
        //NSURLRequest *currRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:htmlPath]];
        //NSData *htmlData = [NSData dataWithContentsOfFile:self.filePath];
        //NSString *currData = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
        //[wv loadHTMLString:currData baseURL:self.baseURL];
        //[currData release];
		//NSFileManager *fm  = [NSFileManager defaultManager];
        
        [wv loadHTMLString:html baseURL:theBaseURL];
		
        //[wv loadRequest:self.url];
		//[wv loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://urbanapps.com/product_list"]]];
        for (id subview in wv.subviews) {
            if ([[subview class] isSubclassOfClass: [UIScrollView class]]) {
                ((UIScrollView *)subview).bounces = NO;
            }
        }

		v = [wv retain];
		[self.contentView addSubview:v];
		
	}
	return self;
}

- (void)dealloc {
    [vibeDRM release];
    [filePath release];
    [baseURL release];
    [urlRequest release];
    [v release];
    self.webView.delegate = nil;
    [webView release];
    [super dealloc];
}

- (void)layoutSubviews {
	[super layoutSubviews];	
	[v setFrame:self.contentView.bounds];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - UIWebView Delegate
- (void)webViewDidFinishLoad:(UIWebView *)theWebView
{
    VLog(@"Finished Loading Encyclopedia");
}

- (void)webView:(UIWebView *)theWebView didFailLoadWithError:(NSError *)error 
{
	VLog(@"Error: %@", [error localizedDescription]);
}

- (BOOL)webView:(UIWebView *)theWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    VLog(@"Encyclopedia: shouldStartLoadWithRequest");
//    if ([[filePath pathExtension] isEqualToString:@"xhtml"] && [[[request URL] scheme] isEqualToString:@"file"]) {
//        NSString *spinePath = fileUrl;
//        NSData *toWrite = nil;
//        
//        VLog(@"shouldStartLoadWithRequest: %@", spinePath);
//        
//        if (ENABLE_DRM_SECURITY) {
//            toWrite = [vibeDRM decrypt:[NSData dataWithContentsOfFile:spinePath]];
//        }
//        else {
//            toWrite = [NSData dataWithContentsOfFile:spinePath];
//        }
//        
//        if (toWrite) {
//            
//            NSString *currWrite = [[[NSString alloc] initWithData:toWrite encoding:NSUTF8StringEncoding] autorelease];
//            //currWrite = [currWrite stringByReplacingOccurrencesOfString:@"xlink:href" withString:@"src"];
//            //currWrite = [currWrite stringByReplacingOccurrencesOfString:@"<article>" withString:@""];
//            //currWrite = [currWrite stringByReplacingOccurrencesOfString:@"<article >" withString:@""];
//            //currWrite = [currWrite stringByReplacingOccurrencesOfString:@"</article>" withString:@""];
//            currWrite = [currWrite stringByReplacingOccurrencesOfString:@"<title></title>" withString:@"<title>Untitled</title>"];
//            currWrite = [currWrite stringByReplacingOccurrencesOfString:@"<title/>" withString:@"<title>Untitled</title>"];
//            currWrite = [currWrite stringByReplacingOccurrencesOfString:@"<title />" withString:@"<title>Untitled</title>"];
//            //../styles.css
//            currWrite = [currWrite stringByReplacingOccurrencesOfString:@"../styles.css" withString:@"styles.css"];
//            //VLog(@"shouldStartLoadWithRequest Full URL: \n%@", currWrite);
//            NSURL *theBaseURL = [NSURL fileURLWithPath:[spinePath stringByDeletingLastPathComponent] isDirectory:YES];
//            VLog(@"********\n%@\n**********", theBaseURL);
//            // DRM
//            [self.webView loadHTMLString:currWrite baseURL:nil];
//            //[self.webView loadHTMLString:currWrite];
//        }
//        else {
//            [SVProgressHUD dismiss];
//            AlertWithMessageAndDelegate(@"Unknown Error", @"Content cannot be displayed.", nil);
//        }
//        
//        return NO;
//    }
//    
	if ([[[request URL] scheme] isEqualToString:@"http"] || [[[request URL] scheme] isEqualToString:@"https"]){
		[[UIApplication sharedApplication] openURL:[request URL]];
		return NO;
	}
    
    return YES;
}
@end
