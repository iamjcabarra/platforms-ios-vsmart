//
//  NoteViewController.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/10/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIBarButtonItem *saveButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *trashButton;
@property (nonatomic, retain) IBOutlet UITextView *noteText;
@property (nonatomic, assign) int highlightId;
@property (nonatomic, retain) NSDictionary *bookData;
-(IBAction)buttonAction:(id)sender;
@end
