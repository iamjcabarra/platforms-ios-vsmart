//
//  BibleInfoViewController.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/12/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BibleInfoViewControllerDelegate;
@interface BibleInfoViewController : UIViewController<UIWebViewDelegate>
{
    
}
@property (nonatomic, assign) id<BibleInfoViewControllerDelegate> delegate;
@property (nonatomic, retain) NSString *htmlText;
@property (nonatomic, retain) NSURL *baseURL;
@property (nonatomic, retain) IBOutlet UIWebView *theWebView;

- (IBAction)close:(id)sender;
@end

@protocol BibleInfoViewControllerDelegate <NSObject>

- (void)didFinishedLoadingBibleLink:(BibleInfoViewController *) popViewController;

@end