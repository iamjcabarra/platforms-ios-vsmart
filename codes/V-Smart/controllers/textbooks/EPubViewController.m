//
//  DetailViewController.m

#import "EPubViewController.h"
//#import "ChapterListViewController.h"
#import "SearchResultsViewController.h"
#import "SearchResult.h"
#import "UIWebView+Private.h"
#import "Chapter.h"
#import <QuartzCore/QuartzCore.h>
#import "SearchTextViewController.h"
//#import "PopupViewController.h"
#import "EncyclopediaViewController.h"
#import "ZoomImageViewController.h"
#import "UIImage+Addons.h"
#import "UIColor+MoreColors.h"
#import "UIColor-Expanded.h"
#import "BibleInfoViewController.h"
#import "UIViewController+Popup.h"
#import "NCXElement.h"
#import "Toast+UIView.h"
#import "GIKPopoverBackgroundView.h"
#import "QuizResultViewController.h"
#import "HMSegmentedControl.h"
#import "HUD.h"
#import "ContextMenuHelper.h"
#import "TKEmptyView.h"
#import "BookHighlightCell.h"
#import "BookExerciseCell.h"
#import "EPubViewController+Private.h"
#import "JMLockInOutResponse.h"

#define RGBFromColor(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]

@interface EPubViewController()
{
    BOOL needsPromptForIOS7;
    BOOL needsReloadForHighlight;
    BOOL needsReloadForBookExercise;
    BOOL needsReloadForBookEncyclopedia;
    BOOL needsReloadForBookFigure;
    BOOL needsReloadForBookMedia;
    BOOL needsHideContext;
    UIStatusBarStyle statusBarStyle;
    
    TKEmptyView *emptyView;
}

- (void) gotoNextSpine;
- (void) gotoPrevSpine;
- (void) gotoNextPage;
- (void) gotoPrevPage;
- (int) getGlobalPageCount;
- (void) gotoPageInCurrentSpine: (int)pageIndex;
- (void) updatePagination;
- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex;
- (void) animateNext;
- (void) animatePrevious;
- (BOOL) isEmpty: (id) thing;
- (void) loadSubViews;
- (void) updateSpinePageLabel: (int) pageIndex;
- (void) checkBookmark;
- (void) clearSelection;
- (void) checkSelection:(id)sender;
- (void) closePopup;
@end

@implementation EPubViewController

#define TAP_AREA_SIZE 40.0f
#define IOS7_HEIGHT 20.0f

@synthesize loadedEpub, toolbar, webView;
@synthesize chapterListButton, decTextSizeButton, incTextSizeButton;
@synthesize currentPageLabel, pageSlider, searching, chapterSlider;
@synthesize currentSearchResult;
@synthesize menuHeader, menuTOC, tocTable, bookmarkButton;
@synthesize currentTitleLabel, currentPageInSpineLabel;
@synthesize searchButton, showTOCButton, showBookmarkButton;
@synthesize currentChapterForBookmark, currentBookmark;
@synthesize paginating, fontListButton, tocShowButton;
@synthesize annotator, btnAnnotate, greenColorButton, blueColorButton, redColorButton;
@synthesize btnUndoDraw, btnResetDraw, btnDrawStylePencil, btnDrawStyleHighlight;
@synthesize slideTOCButton, slideDrawButton, slideBookmarkButton, showAnnotateButton;
@synthesize colorRedButton, colorBlueButton, colorGreyButton, colorPinkButton, colorBrownButton;
@synthesize colorGreenButton, colorWhiteButton, colorIndigoButton, colorOrangeButton, colorYellowButton, colorRandomColorButton;
@synthesize vibeDRM, fontSettingName, fontSizeSettings, themeNameSettings;
@synthesize mainColor, mainTextColor, btnBookmarkLarge;
@synthesize bookIdentifier, drmKey, chapters, segmentedLabel, chaptersForSlider;
@synthesize currentChapter, bookHighlights, selectedCloudHighlight;
@synthesize bookExercises, selectedCloudBookExercise;
@synthesize bookEncyclopedia, bookFigure, bookEncyclopediaEntries, selectedCloudBookEncyclopedia;
@synthesize bookFigureEntries, selectedCloudBookFigure;
@synthesize bookMediaEntries, selectedCloudBookMedia, bookMedia;

#pragma mark - Private Methods

- (void) updateSpinePageLabel: (int) pageIndex
{
    /* localizable strings */
    NSString *pageLeftInThisChapter = NSLocalizedString(@"page left in this chapter", nil); //checked
    NSString *lastPageInThisChapter = NSLocalizedString(@"last page in this chapter", nil); //checked
    NSString *globalPageOFtotalPage = NSLocalizedString(@"of", nil); //checked
    
    NSString *pageLabel = @"";
    [currentPageInSpineLabel setText:pageLabel];
    int currentPageNumber = pagesInCurrentSpineCount - (pageIndex + 1);
    if (currentPageNumber == 1) {
        pageLabel = [NSString stringWithFormat:@"1 %@", pageLeftInThisChapter];
    }
    else if (currentPageNumber == 0) {
        pageLabel = lastPageInThisChapter;
    }
    else {
        pageLabel = [NSString stringWithFormat:@"%d %@", currentPageNumber, pageLeftInThisChapter];
    }

    isLastPageOfChapter = currentPageNumber == 0;
    [currentPageInSpineLabel setText:pageLabel];
    
    //    VLog(@"currentTextSize             : %d", currentTextSize);
    //    VLog(@"currentSpineIndex           : %d", currentSpineIndex);
    //    VLog(@"pagesInCurrentSpineCount    : %d", pagesInCurrentSpineCount);
    //    VLog(@"currentPageInSpineIndex     : %d", currentPageInSpineIndex);
    
    NSString *title = [[loadedEpub.spineArray objectAtIndex:currentSpineIndex] title];
    self.bookChapterLabel.text = title;
    
    NSMutableDictionary *itemChapterForBookmark = [[NSMutableDictionary alloc] init];
    [itemChapterForBookmark setValue:title forKey:kChapterName];
    [itemChapterForBookmark setObject:[NSNumber numberWithInt: currentTextSize] forKey:kChapterTextSize];
    [itemChapterForBookmark setObject:[NSNumber numberWithInt: currentSpineIndex] forKey:kChapterSpineIndex];
    [itemChapterForBookmark setObject:[NSNumber numberWithInt: currentPageInSpineIndex] forKey:kChapterPageIndexInSpine];
    //[itemChapterForBookmark setObject:[NSNumber numberWithInt: [self getGlobalPageCount]] forKey:kChapterPageIndexInSpine];
    [itemChapterForBookmark setObject:[NSNumber numberWithInt: pagesInCurrentSpineCount] forKey:kChapterPageCountInSpine];
    
    self.currentChapterForBookmark = [NSDictionary dictionaryWithDictionary:itemChapterForBookmark];
    [itemChapterForBookmark release];
    
    // ejh 02202014
    // saving for next load of book to this current page
    [self prepareCurrentSpineState];
    
    //[currentPageLabel setText:[NSString stringWithFormat:@"%d/%d", (pageIndex + 1), pagesInCurrentSpineCount]];
    //[pageSlider setValue:(float)100*(float)(pageIndex + 1)/(float) pagesInCurrentSpineCount animated:YES];
    [currentPageLabel setText:[NSString stringWithFormat:@"%d %@ %d",[self getGlobalPageCount], globalPageOFtotalPage, totalPagesCount]];
    [pageSlider setValue:(float)100*(float)[self getGlobalPageCount]/(float)totalPagesCount animated:YES];

    if(sideBarSelectedType == UITableOfContent)
        [self.tocTable reloadData];
    
    VLog(@"Current Book Page: %d", [self getGlobalPageCount]);
    //VLog(@"updateSpinePageLabel:currentPageInSpineIndex: %d", currentPageInSpineIndex);
    //VLog(@"Chapter Slider: %0.2f", (float)100*(float)(currentSpineIndex + 1)/(float) [loadedEpub.spineArray count]);
    
    if (self.loadedEpub.isFixedLayout) {
        [currentPageLabel setAlpha:0];
        [currentPageInSpineLabel setAlpha:0];
    }
    
    CGFloat alpha = ([self showPageNumber]) ? 1.0 : 0;
    [self.currentPageLabel setAlpha:alpha];
}

- (void) showLoading : (NSString *) title{
    NSString *loadingLabel = NSLocalizedString(@"Loading", nil);
    NSString *statusMessage = [NSString stringWithFormat:@"%@ %@", loadingLabel, title];
    
    [SVProgressHUD showWithStatus:statusMessage maskType:SVProgressHUDMaskTypeNone];
}

- (void) displayHUD: (NSString *) message {
    [ProgressHUD show:message Interaction:NO];
}

- (BOOL) showPageNumber {
    BOOL show = [Utils getBookPageNumberSettings];
    return show;
}

- (void) prepareCurrentSpineState {
    // earljon@gmail.com_123344_spine_index
    NSDictionary *spineDataDict = @{kCurrentSpineIndex: [NSNumber numberWithInt:currentSpineIndex], kCurrentPageInSpineIndex: [NSNumber numberWithInt:currentPageInSpineIndex]};
    [VSmartHelpers saveCurrentSpineState:self.bookIdentifier forSpineData:spineDataDict];
}

- (void) loadCurrentSpineState {
    NSDictionary *spineData = [VSmartHelpers getCurrentSpineState:self.bookIdentifier];
    currentSpineIndex = 0;
    currentPageInSpineIndex = 0;

    if (spineData) {
        currentSpineIndex = [[spineData objectForKey:kCurrentSpineIndex] intValue];
        currentPageInSpineIndex = [[spineData objectForKey:kCurrentPageInSpineIndex] intValue];
    }
}

- (NSString *)drmsaltForBook:(NSString *)book {
    
    NSString *deviceId = [VSmartHelpers uniqueGlobalDeviceIdentifier];
    NSString *userId = [VSmartHelpers getUserIdWithBookUUID:book];
    NSString *email = [VSmartHelpers getEmail];
    BOOL flag = [self.loadedEpub.drmVersion isEqualToString:@"1"];
    NSString *drmprefix = flag ? userId : email;
    return [NSString stringWithFormat:DRM_KEY, drmprefix, deviceId];
}

- (void) loadEpubBook:(NSString*) epubName
{
    VLog(@"loadEpub: %@", epubName);
    [PSMenuItem installMenuHandlerForObject:self];
    
    self.bookIdentifier = [NSString stringWithFormat:@"%@_%@", [[VSmartHelpers getEmail] stringByReplacingOccurrencesOfString:@"@" withString:@"_" ], epubName];
    loadChapterContent = NO;
    
    // ejh 02202014
    // Commented in favor of 2 methods below it.
    //currentSpineIndex = 0;
    //currentPageInSpineIndex = 0;
    
    [self loadCurrentSpineState];
    [self prepareCurrentSpineState];
    
    currentSelectedSegmentIndex = 0;
    pagesInCurrentSpineCount = 0;
    totalPagesCount = 0;
	searching = NO;
    epubLoaded = NO;
    
    self.fontSettingName = @"";
    self.themeNameSettings = @"";
    self.currentBookname = @"";
    
    [Utils clearHash];
    [Utils replaceExerciseFile: epubName];
    [self loadBookSettings];
    
    self.currentBookname = epubName;
    needsPromptForIOS7 = NO;

    [self buildMenuHeadeForTableOfContents];
    
    self.bookEncyclopedia = [NSMutableArray array];
    self.bookFigure = [NSMutableArray array];
    self.bookMedia = [NSMutableArray array];
    
    self.loadedEpub = [[EPub alloc] initWithEPubPath:epubName];
    epubLoaded = YES;
    bookUUIDEmpty = IsEmpty(self.loadedEpub.uuid);
    
//    NSString *deviceId = [VSmartHelpers uniqueGlobalDeviceIdentifier];
//    NSString *userId = [VSmartHelpers getUserIdWithBookUUID:epubName];
//    NSString *email = [VSmartHelpers getEmail];
//    BOOL flag = [self.loadedEpub.drmVersion isEqualToString:@"1"];
//    NSString *drmprefix = flag ? userId : email;
    self.drmKey = [self drmsaltForBook:epubName];
    VLog(@"DRM: %@", self.drmKey);
    self.vibeDRM = [[VibeDRM alloc] initWithKey:self.drmKey];
    
    // load interactive book extra details
    if (!bookUUIDEmpty) {
        self.bookExercises = [[VibeDataManager sharedInstance] fetchCloudBookExercises:self.loadedEpub.uuid];
        self.bookEncyclopediaEntries = [[VibeDataManager sharedInstance] fetchCloudBookEncyclopedia:self.loadedEpub.uuid];
        self.bookFigureEntries = [[VibeDataManager sharedInstance] fetchCloudBookFigure:self.loadedEpub.uuid];
        self.bookMediaEntries = [[VibeDataManager sharedInstance] fetchCloudBookMedia:self.loadedEpub.uuid];
    }
    
    //self.menuTOC.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_slide.png"]];
    self.menuTOC.backgroundColor = UIColorFromHex(0xfcfcfc);
    self.tocTable.backgroundColor = [UIColor clearColor];
    //self.tocTable.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_cell.png"]];
    self.tocTable.backgroundColor = UIColorFromHex(0xfcfcfc);
    self.tocTable.tableFooterView = [UIView new];
    
    // Values for Chapter Slider
    self.chapterSlider.maximumValue = [[self.loadedEpub spineArray] count];
    self.chapterSlider.chapters = [[self.loadedEpub spineArray] copy];
    
    currentChapterForBookmark = [[NSDictionary alloc] init];
    //[self checkBookmark];
    VLog(@"self.currentBookname: %@", self.currentBookname);
    exerciseFileExists = [Utils isExerciseDataFileExists:self.currentBookname];
    NSLog(@"exercise file exist...");
    
    //[self.tocTable reloadData];
	[self updatePagination];
    [self.tocTable reloadData];
    [self setNeedsStatusBarAppearanceUpdate];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

#pragma mark - Private Methods for Empty View
- (void) _loadEmptyViewToTableView {
    [self _removeEmptyViewToTableView];
    
    int itemCount = 0;
    if (sideBarSelectedType == UITableOfContent)    itemCount = [self.loadedEpub.tocArray count];
    if (sideBarSelectedType == UIBookmark)          itemCount =  [self.currentBookmark count];
    if (sideBarSelectedType == UIAnnotate)          itemCount =  [annotator countItems:_currentBookname];
    if (sideBarSelectedType == UIHighlight)         itemCount =  [self.bookHighlights count];
    if (sideBarSelectedType == UIExercise)          itemCount =  self.bookExercises != nil ? [self.bookExercises count] : 0;
    if (sideBarSelectedType == UIEncyclopedia)      itemCount =  self.bookEncyclopediaEntries != nil ? [self.bookEncyclopediaEntries count] : 0;
    if (sideBarSelectedType == UIFigure)            itemCount =  self.bookFigureEntries != nil ? [self.bookFigureEntries count] : 0;
    if (sideBarSelectedType == UIMedia)             itemCount =  self.bookMediaEntries != nil ? [self.bookMediaEntries count] : 0;
    
    if (itemCount == 0) {
        [self _addEmptyViewToTableView];
    } else {
        [self _removeEmptyViewToTableView];
    }
}

- (void) _loadEmptyViewWithTransition: (NSString *) transition {
    // transition is not used.
    [Utils tableViewFadeAnimation:self.tocTable];
    [self _loadEmptyViewToTableView];
}

- (void) _addEmptyViewToTableView {
    
    
    /* localizable strings */
    
    /* Table of Content */
    NSString *tableOfContentsPlaceholder = NSLocalizedString(@"Table of Contents Not Available", nil); //checked
    NSString *tableOfContentsDescription = NSLocalizedString(@"No available data to display", nil); //checked
    
    /* Bookmark */
    NSString *bookmarksPlaceholder = NSLocalizedString(@"BOOKMARKS", nil); //checked
    NSString *bookmarksDescription = NSLocalizedString(@"To add a bookmark, simply tap the ribbon icon.", nil); //checked
    
    /* Annotation */
    NSString *annotationPlaceholder = NSLocalizedString(@"User Drawings", nil); //checked
    NSString *annotationDescription = NSLocalizedString(@"To create drawing, set to default font first.", nil); //checked
    
    /* Notes Highlight */
    NSString *highlightPlaceholder = NSLocalizedString(@"Notes and Highlights", nil); //checked
    NSString *highlightDescription = NSLocalizedString(@"Just select a text and choose a color or create a note.", nil); //checked
    
    /* Exercises */
    NSString *exercisePlaceholder = NSLocalizedString(@"Exercise/Assessment Items", nil); //checked
    NSString *exerciseDescription = NSLocalizedString(@"Interactive Quizzes on each chapters.", nil); //checked

    /* Encyclodia */
    NSString *encyclopediaPlaceholder = NSLocalizedString(@"Encyclopedia Entries.", nil); //checked
    NSString *encyclopediaDescription = NSLocalizedString(@"Offline information for a specific item.", nil); //checked
    
    /* Image Figure */
    NSString *imageFigurePlaceholder = NSLocalizedString(@"Image/Figure entries.", nil); //checked
    NSString *imageFigureDescription = NSLocalizedString(@"Figures with captions is not available", nil); //checked

    /* Audio Video */
    NSString *audioVideoPlaceholder = NSLocalizedString(@"Audio/Video Contents", nil); //checked
    NSString *audioVideoDescription = NSLocalizedString(@"List of available media contents.", nil); //checked
    
    NSString *emptyLabel = @"";
    NSString *subtitle = @"";
    
    TKEmptyViewImage image = TKEmptyViewImageStar;
    
    if (sideBarSelectedType == UITableOfContent) {
        emptyLabel = tableOfContentsPlaceholder;//checked
        subtitle = tableOfContentsDescription;//checked
        image = TKEmptyViewImageTableOfContent;
    }
    if (sideBarSelectedType == UIBookmark) {
        emptyLabel = bookmarksPlaceholder;//checked
        subtitle = bookmarksDescription;//checked
        image = TKEmptyViewImageBookmark;
    }
    if (sideBarSelectedType == UIAnnotate){
        emptyLabel = annotationPlaceholder;//checked
        subtitle = annotationDescription;//checked
        image = TKEmptyViewImageDrawing;
    }
    if (sideBarSelectedType == UIHighlight){
        emptyLabel = highlightPlaceholder;//checked
        subtitle = highlightDescription;//checked
        image = TKEmptyViewImageAnnotation;
    }
    if (sideBarSelectedType == UIExercise) {
        emptyLabel = exercisePlaceholder;//checked
        subtitle = exerciseDescription;//checked
        image = TKEmptyViewImageExercise;
    }
    if (sideBarSelectedType == UIEncyclopedia){
        emptyLabel = encyclopediaPlaceholder;//checked
        subtitle = encyclopediaDescription;//checked
        image = TKEmptyViewImageEncyclopedia;
    }
    if (sideBarSelectedType == UIFigure){
        emptyLabel = imageFigurePlaceholder;//checked
        subtitle = imageFigureDescription;//checked
        image = TKEmptyViewImageFigure;
    }
    if (sideBarSelectedType == UIMedia){
        emptyLabel = audioVideoPlaceholder;//checked
        subtitle = audioVideoDescription;//checked
        image = TKEmptyViewImageMedia;
    }
    emptyView = [[TKEmptyView alloc] initWithFrame:self.tocTable.bounds
                                    emptyViewImage:image
                                             title:emptyLabel
                                          subtitle:subtitle];
	
	emptyView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	[self.tocTable addSubview:emptyView];
}

- (void) _removeEmptyViewToTableView {
    if (emptyView) {
        [emptyView removeFromSuperview];
    }
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    
    //VLog(@"statusBarStyle: %i", statusBarStyle);
    if ([self.themeNameSettings isEqualToString:@"black"]) {
        //VLog(@"Theme is Black!");
        return UIStatusBarStyleLightContent;
    }
    
    return UIStatusBarStyleDefault;
}

- (BOOL) isBookSecured: (NSString *) filePath {
    if (self.loadedEpub.encryptedFilesArray == nil) {
        return NO;
    }
    
    BOOL isSecured = [Utils isSecurityEnabled:self.loadedEpub.encryptedFilesArray forFile:filePath];
    return isSecured;
}

- (void) checkBookmark {
    self.currentBookmark = [Utils getBookmarkForBook:self.currentBookname];
    //VLog(@"currentBookmark: %@", self.currentBookmark);
}

- (void) setBookmarkState {
    BOOL isBookmarked = (currentBookmark == nil) ? NO : [Utils isBookmarkAdded:self.currentBookmark andChapter:currentChapterForBookmark];
    [bookmarkButton setSelected:isBookmarked];
    //VLog(@"Bookmarked: %@", isBookmarked ? @"YES" : @"NO");
    
    [self hideLargeBookmark:!isBookmarked];
}

#pragma mark - Chapter Delegate when UIWebView finished loading. See Chapter.m
- (void) chapterDidFinishLoad:(Chapter *)chapter{
    //VLog(@"EPubViewController: chapterDidFinishLoad Chapter %d PageCount: %d",chapter.chapterIndex, chapter.pageCount);
    
    if (chapter.chapterIndex == 0) {
        chapter.pageInBook = 1;
    } else {
        chapter.pageInBook = totalPagesCount + 1;
    }
    
    if (!IsEmpty(chapter.title)) {
        //VLog(@"Book Title: %@", chapter.title);
        [self.chapters setValue:chapter forKey:chapter.title];
    }
    
    [self.chaptersForSlider addObject:chapter];
    self.chapterSlider.chapters = self.chaptersForSlider;
    
    totalPagesCount += chapter.pageCount;
    //totalPagesCount = chapter.pageCount;
    //VLog(@"%@: Pages: %i Total Pages: %d",chapter.title, chapter.pageCount, totalPagesCount);

    NSString *chapterPath = [chapter.spinePath lastPathComponent];
    if (chapter.bookEncyclopedia) {
        //VLog(@"Encyclopedia: %@", chapter.bookEncyclopedia);
        NSArray *encyclopediaEntries = [chapter.bookEncyclopedia objectForKey:@"result"];
        NSDictionary *chapterEntry = @{@"chapterFile": chapterPath, @"result": encyclopediaEntries};
        
        [self.bookEncyclopedia addObject:chapterEntry];
    }
    if (chapter.bookFigure) {
        //VLog(@"Figure: %@", chapter.bookFigure);
        NSArray *imageFigures = [chapter.bookFigure objectForKey:@"result"];
        NSDictionary *chapterEntry = @{@"chapterFile": chapterPath, @"result": imageFigures};
        [self.bookFigure addObject:chapterEntry];
    }
    if (chapter.bookMedia) {
        //VLog(@"Figure: %@", chapter.bookFigure);
        NSArray *mediaEntries = [chapter.bookMedia objectForKey:@"result"];
        NSDictionary *chapterEntry = @{@"chapterFile": chapterPath, @"result": mediaEntries};
        [self.bookMedia addObject:chapterEntry];
    }
    
    if(chapter.chapterIndex + 1 < [loadedEpub.spineArray count]){
        //VLog(@"loadChapterContent");
        BOOL spinePathSecured = [self isBookSecured:[[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1] spinePath]];
        
        [[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1] setDelegate:self];
        [[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1] loadChapterWithWindowSize:webView.bounds
                                                                                fontPercentSize:self.fontSizeSettings
                                                                                   withSecurity:spinePathSecured
                                                                                            drm:self.vibeDRM];
        
        [currentPageLabel setText:[NSString stringWithFormat:@"? of %d", totalPagesCount]];
        if(sideBarSelectedType == UITableOfContent)
            [self.tocTable reloadData];
    } else {
        VLog(@"Total Pages: %i", totalPagesCount);
        [currentPageLabel setText:[NSString stringWithFormat:@"%d of %d",[self getGlobalPageCount], totalPagesCount]];
        [pageSlider setValue:(float)100*(float)[self getGlobalPageCount]/(float)totalPagesCount animated:YES];
        paginating = NO;
        [self updateSpinePageLabel:currentPageInSpineIndex];
        
        //VLog(@"ENCYCLOPEDIA ENTRIES:\n%@", self.bookEncyclopedia);
        dispatch_queue_t _mainQueue = dispatch_queue_create("com.vibebookstore.gdc.epub", NULL);
        dispatch_async(_mainQueue, ^{
            if (!bookUUIDEmpty) {
                if([self.bookEncyclopedia count] > 0) {
                    [[VibeDataManager sharedInstance] addCloudBookEncyclopedia:self.bookEncyclopedia forBook:self.loadedEpub.uuid];
                    [[VibeDataManager sharedInstance] addCloudBookFigure:self.bookFigure forBook:self.loadedEpub.uuid];
                    [[VibeDataManager sharedInstance] addCloudBookMedia:self.bookMedia forBook:self.loadedEpub.uuid];
                }
            }
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                if (!bookUUIDEmpty) {
                    self.bookEncyclopediaEntries = [[VibeDataManager sharedInstance] fetchCloudBookEncyclopedia:self.loadedEpub.uuid];
                    self.bookFigureEntries = [[VibeDataManager sharedInstance] fetchCloudBookFigure:self.loadedEpub.uuid];
                    self.bookMediaEntries = [[VibeDataManager sharedInstance] fetchCloudBookMedia:self.loadedEpub.uuid];
                }
            });
        });
        
        //[SVProgressHUD dismiss];
        VLog(@"PAGINATION_ENDED");
    }
}

- (int) getGlobalPageCount{
	int pageCount = 0;
	for(int i = 0; i < currentSpineIndex; i++){
		pageCount += [[loadedEpub.spineArray objectAtIndex:i] pageCount];
        //VLog(@"pageCount: %d", pageCount);
	}
	pageCount += currentPageInSpineIndex + 1;
	return pageCount;
}

- (void) saveDataForFootnote: (NSString *) key forValue:(int) value
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:[NSNumber numberWithInt:value] forKey:key];
    [userDefaults synchronize];
}

- (void) clearDataForFootnote
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:nil forKey:@"SPINE_INDEX"];
    [userDefaults setValue:nil forKey:@"PAGE_INDEX"];
    [userDefaults synchronize];
}

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex {
    isLastPageOfChapter = NO;
	[self loadSpine:spineIndex atPageIndex:pageIndex highlightSearchResult:nil];
}

- (void) loadPage: (NSString *) pathUrl {
    //VLog(@"loadPage: Compare: %@", pathUrl);
    
    int spineIndex = -1;
    //int pageIndex = 0;
    NSString *spinePath = @"";
    //NSURL* url;
    
    for (int i = 0; i < [loadedEpub.spineArray count]; i++) {
        spineIndex = i;
        spinePath = [[loadedEpub.spineArray objectAtIndex:spineIndex] spinePath];
        if ([pathUrl isEqualToString:spinePath]) {
            //VLog(@"FOUND IT at %i", spineIndex);
            //url = [NSURL fileURLWithPath:[[loadedEpub.spineArray objectAtIndex:spineIndex] spinePath]];
            break;
        }
    }
    
    [self saveDataForFootnote:@"SPINE_INDEX" forValue:spineIndex];
    // since we just save the hash tag, let's default the page to 0
    //[self loadSpine:spineIndex atPageIndex:0];
}

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex highlightSearchResult:(SearchResult*)theResult{
	
    VLog(@"Loading Spine: spineIndex: %i atPageIndex: %i", spineIndex, pageIndex);
    //VLog(@"spineArray: %i, spineIndex: %i", [loadedEpub.spineArray count] - 1, spineIndex);
    
    if ( !(spineIndex < 0) && !(spineIndex > [self.loadedEpub.spineArray count]) ) {
    
        isLastChapter = ([loadedEpub.spineArray count] - 1) == spineIndex;
        webView.hidden = YES;
        
        self.currentSearchResult = theResult;
        
        [chaptersPopover dismissPopoverAnimated:YES];
        [searchResultsPopover dismissPopoverAnimated:YES];
        [searchTextPopover dismissPopoverAnimated:YES];
        [fontPopover dismissPopoverAnimated:YES];
        
        self.currentChapter = (Chapter *) [loadedEpub.spineArray objectAtIndex:spineIndex];
        
        NSString *title = [[loadedEpub.spineArray objectAtIndex:spineIndex] title];
        VLog(@"Title: %@", title);
        currentSpineIndex = spineIndex;
        
        self.currentTitleLabel.text = IsEmpty(title) ? @"Book Cover" : title;
        
        NSURL* url = [NSURL fileURLWithPath:[[loadedEpub.spineArray objectAtIndex:spineIndex] spinePath]];
        
        //VLog(@"SpinePath: %@", [[loadedEpub.spineArray objectAtIndex:spineIndex] spinePath]);
        //VLog(@"Total Spine Array: %d", [loadedEpub.spineArray count]);
        //VLog(@"Spine Index: %d", spineIndex + 1);
        self.chapterSlider.value = spineIndex + 2;
        //VLog(@"loadSpine:url: %@", url);
        
        currentPageInSpineIndex = pageIndex;
        
        [webView loadRequest:[NSURLRequest requestWithURL:url]];
    }
}

- (int) getHashPage: (NSString *) hashId {
    NSString *hashCommand = [NSString stringWithFormat:@"vibeFindPageNumber('%@',%i)", hashId, [webView width]];
    int hashPage = [[webView evalJS: hashCommand] intValue];
    return hashPage;
}

- (void) gotoPageInCurrentSpine:(int)pageIndex{
	if(pageIndex >= pagesInCurrentSpineCount){
		pageIndex = pagesInCurrentSpineCount - 1;
		currentPageInSpineIndex = pagesInCurrentSpineCount - 1;
        VLog(@"pageIndex >= pagesInCurrentSpineCount: %i", currentPageInSpineIndex);
	}
	
	//float pageOffset = pageIndex * webView.bounds.size.width;
    float pageOffset = pageIndex * webView.frame.size.width;
    //VLog("PAGE_INDEX: %d", pageIndex);
    NSString* goToOffsetFunc = [NSString stringWithFormat:@" function pageScroll(xOffset){ window.scroll(xOffset,0); } "];
    NSString* goTo =[NSString stringWithFormat:@"pageScroll(%f)", pageOffset];
    
    hashTag = [Utils getHash];
    if (hashTag) {
        VLog(@"gotoPageInCurrentSpine: HASHTAG");
        //[webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.location.href = '#%@';", hashTag]];
        //NSString *hashCommand = [NSString stringWithFormat:@"document.getElementByName('%@').focus()", hashTag];
        
        //NSString *hashCommand = [NSString stringWithFormat:@"window.location.hash = '%@'", hashTag];
        //[webView stringByEvaluatingJavaScriptFromString:hashCommand];
        NSString *pathJQ = [[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"];
        NSString *jQuery = [NSString stringWithContentsOfFile:pathJQ encoding:NSUTF8StringEncoding error:nil];
        [webView evalJS: jQuery];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"jstools" ofType:@"js"];
        NSString *jsCode = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        [webView evalJS: jsCode];
        
        NSString *hashCommand1 = [NSString stringWithFormat:@"getHashPage('%@',%i,%i)",
                                  hashTag, (NSInteger)webView.bounds.size.width,(NSInteger)webView.bounds.size.height];
        //        int hashPos = [[webView stringByEvaluatingJavaScriptFromString: hashCommand] intValue];
        VLog(@"hashCommand: %@", hashCommand1);
        
        //NSString *hashCommand = [NSString stringWithFormat:@"getElementTopLeft('%@')", hashTag];
        //int hashOffset = [[webView stringByEvaluatingJavaScriptFromString: hashCommand] intValue];
        NSString *hashPos = [webView evalJS: hashCommand1];
        VLog(@"hashCommand: %@", hashPos);
        
        int hashPage = [hashPos intValue];
        [Utils clearHash];
        
        //        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        //        int spineIndex = [[userDefaults valueForKey:@"SPINE_INDEX"] intValue];
        //        if(spineIndex < 0) return;
        
        currentPageInSpineIndex = hashPage;
        //[self loadSpine:spineIndex atPageIndex:hashPage];
        
        goTo = [NSString stringWithFormat:@"pageScroll(%f)", hashPage * webView.bounds.size.width];
        
        //[self gotoPageInCurrentSpine:hashPage];
        //[self updateSpinePageLabel: hashPage];
        //        VLog(@"goTo Command: %@", goTo);
        //
        [webView evalJS:goToOffsetFunc];
        [webView evalJS:goTo];
    } else {
        //NSString* goToOffsetFunc = [NSString stringWithFormat:@" function pageScroll(xOffset){ window.scroll(xOffset,0); } "];
        //NSString* goTo =[NSString stringWithFormat:@"pageScroll(%f)", pageOffset];
        
        VLog(@"goToNormal: %@", goTo);
        
        [webView evalJS:goToOffsetFunc];
        [webView evalJS:goTo];
        
        //[self getHashPage:@"bodyftn83"];
    }
    
	if(!paginating){
        [self updateSpinePageLabel: pageIndex];
        [self setBookmarkState];
        //		[currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",[self getGlobalPageCount], totalPagesCount]];
        //		[pageSlider setValue:(float)100*(float)[self getGlobalPageCount]/(float)totalPagesCount animated:YES];
	}
	
    //[webView stringByEvaluatingJavaScriptFromString:@"window.location.href = '#hashtag';"];
    
	webView.hidden = NO;
	//VLog(@"DONE: gotoPageInCurrentSpine: %i", pageIndex);
}

- (void) gotoNextSpine {
	if(!paginating){
		if(currentSpineIndex + 1 < [loadedEpub.spineArray count]){
            [self animateNext];
			[self loadSpine:++currentSpineIndex atPageIndex:0];
		}
        if (isLastChapter && isLastPageOfChapter) {
            
            /* localizable strings */
            NSString *endOfBook = NSLocalizedString(@"End of book reached", nil); //checked
            [self.view makeToast:endOfBook duration:.7 position:@"center"];
        }

        [self setBookmarkState];
	}
}

- (void) gotoPrevSpine {
	if(!paginating){
		if(currentSpineIndex-1>=0){
			[self loadSpine:--currentSpineIndex atPageIndex:0];
		}
        [self setBookmarkState];
	}
}

- (void) gotoNextPage {
    //VLog(@"gotoNextPage Init()");
	if(!paginating){
        //[self animateNext];
		if(currentPageInSpineIndex + 1 < pagesInCurrentSpineCount){
            [self animateNext];
			[self gotoPageInCurrentSpine: ++currentPageInSpineIndex];
            //[self gotoPageInCurrentSpine:20];
            //VLog(@"gotoPageInCurrentSpine: ");
            [webView stopVideoPlayer];
		} else {
			[self gotoNextSpine];
            //VLog(@"gotoNextSpine: ");
		}
        [self setBookmarkState];
	}
}

- (void) gotoPrevPage {
	if (!paginating) {
        //VLog(@"PageIndex: %i", currentPageInSpineIndex - 1);
        
        loadLastChapter = NO;
        
		if(currentPageInSpineIndex-1>=0){
            [self animatePrevious];
			[self gotoPageInCurrentSpine:--currentPageInSpineIndex];
            [webView stopVideoPlayer];
		} else {
			if(currentSpineIndex!=0){
                [self animatePrevious];
                loadLastChapter = YES;
				int targetPage = [[loadedEpub.spineArray objectAtIndex:(currentSpineIndex-1)] pageCount];
				[self loadSpine:--currentSpineIndex atPageIndex:targetPage-1];
			}
		}
        
        [self setBookmarkState];
	}
}

- (void) adjustBrightness {
    [[UIScreen mainScreen] setBrightness:brightnessLevel];
}

- (void) handleSwipeGesture:(UISwipeGestureRecognizer*)swipeGesture {
    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self gotoPrevPage];
    }
    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionRight) {
        [self gotoNextPage];
    }
    if(swipeGesture.direction == UISwipeGestureRecognizerDirectionUp || swipeGesture.direction == UISwipeGestureRecognizerDirectionDown) {
        if (swipeGesture.direction == UISwipeGestureRecognizerDirectionUp) {
            if(brightnessLevel != 1.0 ) {
                brightnessLevel += 0.2;
            }
        }
        if (swipeGesture.direction == UISwipeGestureRecognizerDirectionDown) {
            if (brightnessLevel != 0) {
                brightnessLevel -= 0.2;
            }
        }
        [self adjustBrightness];
    }
}

- (void) adjustBrightness:(UISwipeGestureRecognizer*)swipeGesture {
    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionUp) {
        if(brightnessLevel != 1.0 ) {
            brightnessLevel += 0.2;
        }
    }
    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionDown) {
        if (brightnessLevel != 0) {
            brightnessLevel -= 0.2;
        }
    }
    [self adjustBrightness];
}

#pragma mark - Tap Recognizer Selectors
//- (void)handleZoomImage:(UITapGestureRecognizer *)recognizer
//{
//    VLog(@"DoubleTap Hit");
//    int scrollPositionY = [[self.webView stringByEvaluatingJavaScriptFromString:@"window.pageYOffset"] intValue];
//    int scrollPositionX = [[self.webView stringByEvaluatingJavaScriptFromString:@"window.pageXOffset"] intValue];
//
//    int displayWidth = [[self.webView stringByEvaluatingJavaScriptFromString:@"window.outerWidth"] intValue];
//    CGFloat scale = webView.frame.size.width / displayWidth;
//
//    CGPoint pt = [recognizer locationInView:self.webView];
//    pt.x *= scale;
//    pt.y *= scale;
//    pt.x += scrollPositionX;
//    pt.y += scrollPositionY;
//
//    NSString *js = [NSString stringWithFormat:@"document.elementFromPoint(%f, %f).tagName", pt.x, pt.y];
//    NSString * tagName = [self.webView stringByEvaluatingJavaScriptFromString:js];
//    VLog(@"Tag Name: %@", tagName);
//
//    if ([[tagName lowercaseString] isEqualToString:@"img"]) {
//        NSString *imgURL = [NSString stringWithFormat:@"document.elementFromPoint(%f, %f).src", pt.x, pt.y];
//        NSString *urlToSave = [self.webView stringByEvaluatingJavaScriptFromString:imgURL];
//
//        VLog(@"Image SRC: %@", urlToSave);
//        ZoomImageViewController *modalPanel = [[[ZoomImageViewController alloc] initWithFrame:self.view.bounds
//                                                                                              title:@"Full Image View"
//                                                                                        andImagePath:urlToSave] autorelease];
//
//        modalPanel.onClosePressed = ^(UAModalPanel* panel) {
//			// [panel hide];
//			[panel hideWithOnComplete:^(BOOL finished) {
//				[panel removeFromSuperview];
//			}];
//		};
//
//        [self.view addSubview:modalPanel];
//        [modalPanel showFromPoint:currentPoint];
//    }
//}

- (void)handleLongPress:(UILongPressGestureRecognizer *)recognizer {
    CGPoint location = [(UILongPressGestureRecognizer*)recognizer locationInView:recognizer.view];
    currentPoint = location;
    VLog(@"currentPoint: %@", NSStringFromCGPoint(currentPoint));
}

- (void)handleZoomImage:(UITapGestureRecognizer *)recognizer
{
    //CGPoint point = [recognizer locationInView:self.webView];
    
    //VLog(@"DoubleTap Hit");
    int scrollPositionY = [[self.webView stringByEvaluatingJavaScriptFromString:@"window.pageYOffset"] intValue];
    int scrollPositionX = [[self.webView stringByEvaluatingJavaScriptFromString:@"window.pageXOffset"] intValue];
    
    int displayWidth = [[self.webView stringByEvaluatingJavaScriptFromString:@"window.outerWidth"] intValue];
    CGFloat scale = webView.frame.size.width / displayWidth;
    
    CGPoint pt = [recognizer locationInView:self.webView];
    pt.x *= scale;
    pt.y *= scale;
    pt.x += scrollPositionX;
    pt.y += scrollPositionY;
    
    // get the Tags at the touch location
    NSString *tags = [webView stringByEvaluatingJavaScriptFromString:
                      [NSString stringWithFormat:@"MyAppGetHTMLElementsAtPoint(%i,%i);",(NSInteger)currentPoint.x,(NSInteger)currentPoint.y]];
    
    NSString *tagsHREF = [webView stringByEvaluatingJavaScriptFromString:
                          [NSString stringWithFormat:@"MyAppGetLinkHREFAtPoint(%i,%i);",(NSInteger)currentPoint.x,(NSInteger)currentPoint.y]];
    
    NSString *tagsSRC = [webView stringByEvaluatingJavaScriptFromString:
                         [NSString stringWithFormat:@"MyAppGetLinkSRCAtPoint(%i,%i);",(NSInteger)currentPoint.x,(NSInteger)currentPoint.y]];
    
    NSString *url = nil;
    if ([tags rangeOfString:@",IMG,"].location != NSNotFound) {
        url = tagsSRC;
    }
    if ([tags rangeOfString:@",A,"].location != NSNotFound) {
        url = tagsHREF;
    }
    VLog(@"handleZoomImage url : %@",url);
    
    NSArray *urlArray = [[url lowercaseString] componentsSeparatedByString:@"/"];
    NSString *urlBase = nil;
    if ([urlArray count] > 2) {
        urlBase = [urlArray objectAtIndex:2];
    }
    
    if ((url != nil) &&
        ([url length] != 0)) {
        
        ZoomImageViewController *modalPanel = [[[ZoomImageViewController alloc] initWithFrame:self.view.bounds
                                                                                        title:@"Full Image View"
                                                                                 andImagePath:url] autorelease];
        modalPanel.isFixedLayout = self.loadedEpub.isFixedLayout;
        
        modalPanel.onClosePressed = ^(UAModalPanel* panel) {
            // [panel hide];
            [panel hideWithOnComplete:^(BOOL finished) {
                [panel removeFromSuperview];
            }];
        };
        
        [self.view addSubview:modalPanel];
        [modalPanel showFromPoint:currentPoint];
    }
}

- (void)handleTapFrom:(UITapGestureRecognizer *)recognizer {
    
    VLog(@"handleTapFrom");
    if (recognizer.state == UIGestureRecognizerStateRecognized)
	{
        CGPoint point = [recognizer locationInView:webView];
        
        // convert point from view to HTML coordinate system
        CGSize viewSize = [webView frame].size;
        CGSize windowSize = [webView windowSize];
        
        CGFloat f = windowSize.width / viewSize.width;
        if ([[UIDevice currentDevice].systemVersion doubleValue] >= 5.) {
            point.x = point.x * f;
            point.y = point.y * f;
        }
        
        currentPoint = point;
    }
}

- (void) animateNext {
    [self.webView hide];
    [self.webView pageCurlAnimation:kCAFillModeBackwards withDelegate:self];
}

- (void) animatePrevious {
    [self.webView pageCurlAnimation:kCAFillModeForwards withDelegate:self];
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    if(flag) [self.webView fadeIn];
}

-(void)changeFontSize:(int)fontSize{
	currentTextSize = fontSize;
	[self updatePagination];
}

- (void) hideLargeBookmark: (BOOL) hide {
    float y = -self.btnBookmarkLarge.frame.size.height;
    float alpha = 1.0f;
    //self.btnBookmarkLarge.alpha = 1.0;
    
    if (!hide) {
        y = 0 + IOS7_HEIGHT;
        alpha = 0;
    }
    
    [UIView animateWithDuration:0.50 animations:^{
        CGRect frame = self.btnBookmarkLarge.frame;
        frame.origin.y = y - IOS7_HEIGHT;
        [self.btnBookmarkLarge setFrame:frame];
        //self.bookmarkButton.alpha = alpha;
    } completion:^(BOOL finished) {
        if (hide) {
            [self.bookmarkButton fadeIn];
        } else {
            [self.bookmarkButton fadeOut];
        }
    }];
}

- (IBAction) bookmarkLargeAction:(id)sender {
    VLog(@"bookmarkLargeAction");

    [Utils removeBookmark:_currentBookname withChapter:self.currentChapterForBookmark];
    
    [self hideLargeBookmark:YES];
    [self.bookmarkButton setSelected:NO];
    [self updateBookmarkActionState];
}

- (IBAction) fontSizeClicked:(id)sender
{
//    if (fontPopover == nil) {
//		if (fontView == nil) {
//			fontView = [[FontViewController alloc] initWithNibName:@"FontViewController" bundle:nil];
//			[fontView setEpubViewController:self];
//		}
//		
//		fontPopover = [[UIPopoverController alloc] initWithContentViewController:fontView];
//		[fontPopover setPopoverContentSize:CGSizeMake(146, 56)];
//	}
//	
//	if (![fontPopover isPopoverVisible]) {
//		[fontView setFontSize:currentTextSize];
//		
//        CGRect buttonFrameInDetailView = [self.view convertRect:fontListButton.frame fromView:menuHeader];
//        [fontPopover presentPopoverFromRect:buttonFrameInDetailView
//                                     inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//	}
    if (fontPopover == nil) {
		if (settingsView == nil) {
			settingsView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
            settingsView.delegate = self;
		}
		
		fontPopover = [[UIPopoverController alloc] initWithContentViewController:settingsView];
        fontPopover.popoverBackgroundViewClass = [GIKPopoverBackgroundView class];
		[fontPopover setPopoverContentSize:CGSizeMake(320, 570)];
	}
	
	if (![fontPopover isPopoverVisible]) {
		//[fontView setFontSize:currentTextSize];
		
        CGRect buttonFrameInDetailView = [self.view convertRect:fontListButton.frame fromView:menuHeader];
        [fontPopover presentPopoverFromRect:buttonFrameInDetailView
                                     inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	}
}

- (IBAction) increaseTextSizeClicked:(id)sender{
	if(!paginating){
		if(currentTextSize + 25 <= 200){
			currentTextSize += 25;
			[self updatePagination];
			if(currentTextSize == 200){
				[incTextSizeButton setEnabled:NO];
			}
			[decTextSizeButton setEnabled:YES];
		}
	}
}

- (IBAction) decreaseTextSizeClicked:(id)sender{
	if(!paginating){
		if(currentTextSize - 25 >= 50){
			currentTextSize -= 25;
			[self updatePagination];
			if(currentTextSize == 50){
				[decTextSizeButton setEnabled:NO];
			}
			[incTextSizeButton setEnabled:YES];
		}
	}
}

- (IBAction) doneClicked:(id)sender{
    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) closePopup
{
    //[self dismissModalViewControllerAnimated:YES];
}

- (void) updateBookmarkActionState {
    
    isTOC = YES;
    [self checkBookmark];
    [tocTable reloadData];
    //[self checkBookmark];
    [self setBookmarkState];
}

- (int) _currentTargetPage {
    int targetPage = (int)((pageSlider.value/(float)100)*(float)pagesInCurrentSpineCount);
    if (targetPage == 0) {
        targetPage++;
    }
    return targetPage;
}

- (IBAction) slidingStarted:(id)sender{
    int targetPage = [self _currentTargetPage];
	//[currentPageLabel setText:[NSString stringWithFormat:@"%d/%d", targetPage, totalPagesCount]];
    [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d", targetPage, pagesInCurrentSpineCount]];
}

- (IBAction) slidingEnded:(id)sender{
    
    int targetPage = [self _currentTargetPage];
    
    //VLog(@"Target Page: %i", targetPage);
    
    //VLog(@"targetPage: %d\tcurrentSpineIndex: %d", targetPage, currentPageInSpineIndex);
    [self gotoPageInCurrentSpine:targetPage - 1];
    currentPageInSpineIndex = targetPage - 1;
	//[self loadSpine:chapterIndex atPageIndex:pageIndex];
}

- (IBAction) showChapterIndex:(id)sender{
//	if(chaptersPopover==nil){
//		ChapterListViewController* chapterListView = [[ChapterListViewController alloc] initWithNibName:@"ChapterListViewController" bundle:[NSBundle mainBundle]];
//		[chapterListView setEpubViewController:self];
//		chaptersPopover = [[UIPopoverController alloc] initWithContentViewController:chapterListView];
//		[chaptersPopover setPopoverContentSize:CGSizeMake(400, 600)];
//		[chapterListView release];
//	}
//	if ([chaptersPopover isPopoverVisible]) {
//		[chaptersPopover dismissPopoverAnimated:YES];
//	}else{
//		[chaptersPopover presentPopoverFromBarButtonItem:chapterListButton
//                                permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//	}
}

- (IBAction) searchClicked:(id)sender
{
    if(searchTextPopover==nil){
		searchTextPopover = [[UIPopoverController alloc] initWithContentViewController:searchTextViewController];
		[searchTextPopover setPopoverContentSize:CGSizeMake(400, 600)];
	}
	if (![searchTextPopover isPopoverVisible]) {
        [searchTextPopover presentPopoverFromRect:searchButton.bounds inView:searchButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	}
    //	VLog(@"Searching for %@", [searchBar text]);
	if(!searching){
		searching = YES;
		//[searchResViewController searchString:[searchBar text]];
	}
}

- (IBAction) bookmarkCliked:(id)sender {
//    if ([bookmarkButton isSelected]) {
//        [Utilities removeBookmark:currentBookname withChapter:self.currentChapterForBookmark];
//    }
//    else {
//        [Utilities addBookmark:currentBookname withChapter:self.currentChapterForBookmark];
//    }
    [Utils addBookmark:_currentBookname withChapter:self.currentChapterForBookmark];
    [self updateBookmarkActionState];
}

- (IBAction) titleCliked:(id)sender
{
    if (self.annotator.isActive) {
        return;
    }
    
    [self showSlidingTOC];
}

- (void) showSlidingTOC {
    isTOC = YES;
	CGRect tocRect = menuTOC.frame;
    //VLog(@"titleCliked: Origin.X: %.2f", tocRect.origin.x);
    //VLog(@"titleCliked: Origin.Y: %.2f", tocRect.origin.y);
    //VLog(@"titleCliked: tocRect Width: %.2f", tocRect.size.width);
    tocRect.origin.y = 0 + IOS7_HEIGHT;
    
    //UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    if (IS_LANDSCAPE) {
        if (tocRect.origin.x == 1024) {
            menuTOC.hidden = NO;
            tocRect.origin.x = 1024 - tocRect.size.width;
            [showTOCButton setSelected:YES];
            [showBookmarkButton setSelected:NO];
            [tocTable reloadData];
        }
        else {
            tocRect.origin.x = 1024;
        }
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5f];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        if (tocRect.origin.x == 1024) {
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(animDone:finished:context:)];
        }
        tocRect.size.height = self.view.frame.size.height;
        menuTOC.frame = tocRect;
        [UIView commitAnimations];
        
        return;
    }
    
	if (tocRect.origin.x == 768) {
		menuTOC.hidden = NO;
		tocRect.origin.x = 768 - tocRect.size.width;
        [showTOCButton setSelected:YES];
        [showBookmarkButton setSelected:NO];
        [tocTable reloadData];
	}
	else {
        tocRect.origin.x = 768;
    }
    
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.5f];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	if (tocRect.origin.x == 768) {
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDidStopSelector:@selector(animDone:finished:context:)];
	}
    tocRect.size.height = self.view.frame.size.height;
	menuTOC.frame = tocRect;
	[UIView commitAnimations];
    
//    if (sideBarSelectedType == UITableOfContent) {
//        [self.tocTable selectRowAtIndexPath:[self getCurrentChapterIndexPath] animated:NO scrollPosition:UITableViewScrollPositionNone];
//    }
}

- (IBAction) closeTOC:(UIButton *) trigger {
	[self titleCliked:trigger];
}

- (IBAction) backToLibrary:(UIButton *)trigger
{
    [timer invalidate];
    tapWindow.controllerThatObserves = nil;
    tapWindow = nil;
    [tapWindow release];
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification
                                                            notificationWithName:@"org.vibalfoundation.vibereader.close"
                                                            object:nil]];
}

- (IBAction) changeActionMode:(UIButton *) trigger
{
//    [showTOCButton setSelected:NO];
//	[showBookmarkButton setSelected:NO];
//	
//    [slideTOCButton setSelected:NO];
//    [slideBookmarkButton setSelected:NO];
//    [slideDrawButton setSelected:NO];
//    
//	[trigger setSelected:YES];
//	
//	//isTOC = (trigger == showTOCButton);
//	//[tocTable reloadData];
//    
//    //int tag = (int)((UIButton *)trigger).tag;
//    
//    [self initSlideHeader];
//    
//    if (trigger == slideTOCButton)
//    {
//        //[slideTOCButton setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"slideHeader"]]];
//        sideBarSelectedType = UITableOfContent;
//    }
//    if (trigger == slideBookmarkButton)
//    {
//        //[slideBookmarkButton setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"slideHeader"]]];
//        sideBarSelectedType = UIBookmark;
//    }
//    if (trigger == slideDrawButton)
//    {
//        //[slideDrawButton setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"slideHeader"]]];
//        sideBarSelectedType = UIAnnotate;
//    }
//    
//    [tocTable reloadData];
}

-(void)animDone:(NSString*) animationID finished:(BOOL) finished
		context:(void*) context
{
	menuTOC.hidden = YES;
}

- (void) updatePagination {
    
	if(epubLoaded){
        if(!paginating){
            webView.hidden = YES;
            //[SVProgressHUD showWithStatus:@"Loading Chapters ..." maskType:SVProgressHUDMaskTypeClear];
            
            //VLog(@"Pagination Started!");
            //paginating = YES;
            
            self.chapters = [[NSMutableDictionary alloc] init];
            self.chaptersForSlider = [[NSMutableArray alloc] init];
            
            [self loadSpine:currentSpineIndex atPageIndex:currentPageInSpineIndex];
            
            totalPagesCount = 0;
            pagesInCurrentSpineCount = 0;
            
            //VLog(@"loadChapterContent: %i, CurrentTextSize: %i Saved Text Size: %i", [loadedEpub.spineArray count], currentTextSize, self.fontSizeSettings);
            
                // For Chapter loading ...
                NSLog(@"For Chapter Loading...");
                BOOL spinePathSecured = [self isBookSecured:[[loadedEpub.spineArray objectAtIndex:0] spinePath]];
                [[loadedEpub.spineArray objectAtIndex:0] setDelegate:self];
                
                [[loadedEpub.spineArray objectAtIndex:0] loadChapterWithWindowSize:webView.bounds
                                                                   fontPercentSize:self.fontSizeSettings
                                                                      withSecurity:spinePathSecured
                                                                               drm:self.vibeDRM];
            
            [currentPageLabel setText:@"?/?"];
            [currentPageInSpineLabel setText:@""];
            
            //update bookmark too!
            [Utils setFontSize:currentTextSize];
            
            // ejh 02192014
            // Hard setting to NO to already access the pages while still loading the rest of the files.
            paginating = NO;
            [self updateSpinePageLabel:currentPageInSpineIndex];
            
            [self checkBookmark];
            [self setBookmarkState];
            
            [SVProgressHUD dismiss];
            VLog(@"Pagination Ended!");
        }
	}
}

- (void) updateOffsetPadding {
    offsetPadding = 30;
    
    if (IS_LANDSCAPE) {
        offsetPadding = 50;
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
	if(searchResultsPopover==nil){
		searchResultsPopover = [[UIPopoverController alloc] initWithContentViewController:searchResViewController];
		[searchResultsPopover setPopoverContentSize:CGSizeMake(400, 600)];
	}
	if (![searchResultsPopover isPopoverVisible]) {
		[searchResultsPopover presentPopoverFromRect:searchBar.bounds inView:searchBar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	}
    //	VLog(@"Searching for %@", [searchBar text]);
	if(!searching){
		searching = YES;
		[searchResViewController searchString:[searchBar text]];
        [searchBar resignFirstResponder];
	}
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void) receivedNoteNotification: (NSNotification *) notification {
    NSDictionary *data = (NSDictionary *)[notification object];
    //VLog(@"Data: %@", data);
    int highlightId = [[data objectForKey:@"id"] intValue];
    if ([[data objectForKey:@"action"] isEqualToString:@"DELETE"]) {
        [self _removeHighlightToDbById:highlightId];
    } else {
        NSMutableString *textNotes = [[NSMutableString alloc] init];
        NSString *text = [data objectForKey:@"text"];
        [textNotes appendString:text];
        
        if (!IsEmpty(text)) {
            // save note data
            if (highlightId == 0) {
                // add
                NSDictionary *json = [VSmartHelpers getTempHighlightNote];
                //VLog(@"jsonSaving...:%@", json);
                [self _saveHighlightToDb:json];
                NSDictionary *result = [Utils toDictionary:[json objectForKey:@"result"]];
                highlightId = [[result objectForKey:@"id"] intValue];
                
                if(!bookUUIDEmpty) {
                    NSArray *mergedItems = (NSArray *)[json objectForKey:@"mergedData"];
                    int mergedCount = [mergedItems count];
                    if (mergedCount > 0) {
                        NSString *notes = [self _extractHighlightNotes:mergedItems];
                        [textNotes appendString:notes];
                        
                        // remove all merged
                        [self _removeHighlightToDb:mergedItems];
                    }
                }
            }
            [self _updateHighlightToDb:highlightId withNote:textNotes];
        }
    }
    
    [self clearSelection];
    [self _loadHighlights];
    [notePopover dismissPopoverAnimated:YES];
}

#pragma mark - Rotation support
- (void)didChangeRotation:(NSNotification *)notification {
    //push the related controller
    //VLog(@"EPubViewController: didChangeRotation");
    [self closeTOC:nil];
    [self updatePagination];
    
//    dispatch_queue_t _mainQueue = dispatch_queue_create("com.vibetechnologies.gcd.local", NULL);
//    dispatch_async(_mainQueue, ^{
//        
//        
//        dispatch_sync(dispatch_get_main_queue(), ^{
//
//        });
//    });
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    //VLog(@"didRotateFromInterfaceOrientation");
    //[self updatePagination];
	//[self figureOrientation];
}

// Ensure that the view controller supports rotation and that the split view can therefore show in both portrait and landscape.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    VLog(@"EPubViewController: shouldAutorotate");
    //[self updatePagination];
	return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    VLog(@"willAnimateRotationToInterfaceOrientation");
}

#pragma mark -
#pragma mark View lifecycle

- (void) initSlideHeader
{
    //[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_cell.png"]]
    //[slideTOCButton setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_cell.png"]]];
    //[slideBookmarkButton setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_cell.png"]]];
    //[slideDrawButton setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_cell.png"]]];
}

- (void) initColors
{
    // blue, green, red, brown, orange, pink, indigo, grey, yellow, white
    [colorBlueButton setImage:[UIImage imageNamed:@"Blue" withColor:[UIColor blueColor]] forState:UIControlStateNormal];
    [colorGreenButton setImage:[UIImage imageNamed:@"Green" withColor:[UIColor greenColor]] forState:UIControlStateNormal];
    [colorRedButton setImage:[UIImage imageNamed:@"Red" withColor:[UIColor redColor]] forState:UIControlStateNormal];
    [colorBrownButton setImage:[UIImage imageNamed:@"Brown" withColor:[UIColor brown]] forState:UIControlStateNormal];
    [colorOrangeButton setImage:[UIImage imageNamed:@"Orange" withColor:[UIColor orangeColor]] forState:UIControlStateNormal];
    [colorPinkButton setImage:[UIImage imageNamed:@"Pink" withColor:[UIColor pink]] forState:UIControlStateNormal];
    [colorIndigoButton setImage:[UIImage imageNamed:@"Indigo" withColor:[UIColor indigo]] forState:UIControlStateNormal];
    [colorGreyButton setImage:[UIImage imageNamed:@"Grey" withColor:[UIColor grayColor]] forState:UIControlStateNormal];
    [colorYellowButton setImage:[UIImage imageNamed:@"Yellow" withColor:[UIColor yellowColor]] forState:UIControlStateNormal];
    [colorWhiteButton setImage:[UIImage imageNamed:@"White" withColor:[UIColor whiteColor]] forState:UIControlStateNormal];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    VLog(@"Loaded viewDidLoad");
    [super viewDidLoad];
    
    //    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeRotation:)
    //                                                 name:UIDeviceOrientationDidChangeNotification
    //                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeRotation:)
                                                 name:kMainRotateNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNoteNotification:)
                                                 name:kNoteDataNotification object:nil];
    
    [self _buildContextMenu];
    
    tapWindow = (TapDetectingWindow *)[[UIApplication sharedApplication].windows objectAtIndex:0];
    tapWindow.viewToObserve = webView;
    tapWindow.controllerThatObserves = self;
    
	[webView setDelegate:self];
    [webView setUserInteractionEnabled:YES];
    
	UIScrollView* sv = nil;
	for (UIView* v in  webView.subviews) {
		if([v isKindOfClass:[UIScrollView class]]){
			sv = (UIScrollView*) v;
			sv.scrollEnabled = NO;
			sv.bounces = NO;
		}
	}
    
    // Initialize Webview to remove gradient of top and bottom scroll
    [webView setBackgroundColor:[UIColor clearColor]];
    //[webView setOpaque:YES];
    [self hideWebViewGradient];
    [self updateOffsetPadding];
    
    [chapterSlider setDelegate:self];
    
    // Color Buttons
    [self initColors];
    
    [self initSlideHeader];
    
	currentTextSize = [Utils getFontSize];
    isTOC = NO;
    sideBarSelectedType = UITableOfContent;
    [currentPageInSpineLabel setText:@""];
    
    [showTOCButton setSelected:YES];
    //[slideTOCButton setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"slideHeader"]]];
    
    [self loadSubViews];

	UISwipeGestureRecognizer* rightSwipeRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoNextPage)] autorelease];
	[rightSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
	
	UISwipeGestureRecognizer* leftSwipeRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoPrevPage)] autorelease];
	[leftSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];

    UISwipeGestureRecognizer* topSwipeRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(adjustBrightness:)] autorelease];
	[topSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionUp];

    UISwipeGestureRecognizer* downSwipeRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(adjustBrightness:)] autorelease];
	[downSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionDown];
    
     UITapGestureRecognizer *tapRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)] autorelease];
     tapRecognizer.numberOfTapsRequired = 1;
     tapRecognizer.delegate = self;

    //tapRecognizer.numberOfTouchesRequired = 2;
    
    UITapGestureRecognizer *zoomImageRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleZoomImage:)] autorelease];
    zoomImageRecognizer.numberOfTapsRequired = 2;
    zoomImageRecognizer.delegate = self;
    
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longPressRecognizer.delegate = self;
    [webView addGestureRecognizer:longPressRecognizer];
    
    [webView addGestureRecognizer:tapRecognizer];
    [webView addGestureRecognizer:zoomImageRecognizer];

	[webView addGestureRecognizer:rightSwipeRecognizer];
	[webView addGestureRecognizer:leftSwipeRecognizer];
    [webView addGestureRecognizer:topSwipeRecognizer];
    [webView addGestureRecognizer:downSwipeRecognizer];
    
	searchResViewController = [[SearchResultsViewController alloc] initWithNibName:@"SearchResultsViewController" bundle:[NSBundle mainBundle]];
	searchResViewController.epubViewController = self;
    
	searchTextViewController = [[SearchTextViewController alloc] initWithNibName:@"SearchTextViewController" bundle:[NSBundle mainBundle]];
    searchTextViewController.epubViewController = self;
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.3 target:self selector:@selector(checkSelection:) userInfo:nil repeats:YES];
    
    // Initialize Annotation
    self.annotator = [[Annotate alloc] init];
    self.annotator.isActive = FALSE;
    self.annotator.ownerView = self.view;
    
    NSDictionary *settings = [Utils getBookSetings];
    brightnessLevel = [[settings objectForKey:@"brightness_level"] floatValue];
    
    self.unlockViewController = [AssessmentUnlockViewController new];
    self.unlockViewController.delegate = self;
    
    self.unlockListController = [AssessmentListViewController new];
}

- (void) buildBookHighlights {
    NSString *email = [VSmartHelpers getEmail];
    
    if (IsEmpty(email) || bookUUIDEmpty) {
        return;
    }
    self.bookHighlights = [[VibeDataManager sharedInstance] fetchCloudHighlightsForBook:email
                                                                               withUUID:self.loadedEpub.uuid];
    
    VLog(@"bookHighlights: %d", [self.bookHighlights count]);
}

- (void) buildMenuHeadeForTableOfContents {
    
    BOOL exists = [Utils isExerciseDataFileExists:self.currentBookname];
    VLog(@"currentBookname: %@", self.currentBookname);
    NSArray *sectionImages = nil;
    NSArray *sectionSelectedImages = nil;
    
    if (exists) {
        sectionImages = @[[UIImage imageNamed:@"icn_offcanvas-panel_toc"], [UIImage imageNamed:@"icn_offcanvas-panel_bookmark"], [UIImage imageNamed:@"icn_offcanvas-panel_drawing"], [UIImage imageNamed:@"icn_offcanvas-panel_annotations"], [UIImage imageNamed:@"icn_offcanvas-panel_exercises"], [UIImage imageNamed:@"icn_offcanvas-panel_encyclopedic"],[UIImage imageNamed:@"icn_offcanvas-panel_images"],[UIImage imageNamed:@"icn_offcanvas-panel_media"]];
        sectionSelectedImages = @[
                                  [UIImage imageNamed:@"icn_offcanvas-panel_toc_active"],
                                  [UIImage imageNamed:@"icn_offcanvas-panel_bookmark_active"],
                                  [UIImage imageNamed:@"icn_offcanvas-panel_drawing_active"],
                                  [UIImage imageNamed:@"icn_offcanvas-panel_annotations_active"],
                                  [UIImage imageNamed:@"icn_offcanvas-panel_exercises_active"],
                                  [UIImage imageNamed:@"icn_offcanvas-panel_encyclopedic_active"],
                                  [UIImage imageNamed:@"icn_offcanvas-panel_images_active"],
                                  [UIImage imageNamed:@"icn_offcanvas-panel_media_active"]
                                  ];
    } else {
        sectionImages = @[[UIImage imageNamed:@"icn_offcanvas-panel_toc"], [UIImage imageNamed:@"icn_offcanvas-panel_bookmark"], [UIImage imageNamed:@"icn_offcanvas-panel_drawing"], [UIImage imageNamed:@"icn_offcanvas-panel_annotations"]];
        sectionSelectedImages = @[
                                  [UIImage imageNamed:@"icn_offcanvas-panel_toc_active"],
                                  [UIImage imageNamed:@"icn_offcanvas-panel_bookmark_active"],
                                  [UIImage imageNamed:@"icn_offcanvas-panel_drawing_active"],
                                  [UIImage imageNamed:@"icn_offcanvas-panel_annotations_active"]
                                  ];
    }
    
    /*
     * LEGACY CODE
     *
    HMSegmentedControl *tocHeaderControl = [[HMSegmentedControl alloc] initWithSectionImages:sectionImages
                                                                       sectionSelectedImages:sectionSelectedImages];
    [tocHeaderControl setSelectionIndicatorHeight:4.0f];
    [tocHeaderControl setFrame:CGRectMake(20, 35, 460, 44)];
    [tocHeaderControl setSegmentEdgeInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [tocHeaderControl addTarget:self action:@selector(tocSelectionAction:) forControlEvents:UIControlEventValueChanged];
    [tocHeaderControl setBackgroundColor:[UIColor clearColor]];
    [tocHeaderControl setSelectionLocation:HMSegmentedControlSelectionIndicatorLocationDown];
    [tocHeaderControl setSelectionStyle:HMSegmentedControlSelectionStyleFullWidthStripe];
     */
    
    HMSegmentedControl *tocHeaderControl = [[HMSegmentedControl alloc] initWithSectionImages:sectionImages
                                                                       sectionSelectedImages:sectionSelectedImages];
    tocHeaderControl.selectionIndicatorHeight = 4.0f;
    tocHeaderControl.frame = CGRectMake(20, 35, 460, 44);
    tocHeaderControl.segmentEdgeInset = UIEdgeInsetsMake(0, 0, 0, 0);
    tocHeaderControl.backgroundColor = [UIColor clearColor];
    tocHeaderControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    tocHeaderControl.selectionStyle = HMSegmentedControlSelectionIndicatorLocationDown;
    [tocHeaderControl addTarget:self action:@selector(tocSelectionAction:) forControlEvents:UIControlEventValueChanged];
    
    [self.menuTOC addSubview:tocHeaderControl];
}

- (void)tocSelectionAction:(HMSegmentedControl *)segmentedControl {
	NSLog(@"Selected index %i (via UIControlEventValueChanged)", segmentedControl.selectedSegmentIndex);
    
    int selectedTOCIndex = segmentedControl.selectedSegmentIndex;
    
    //[self initSlideHeader];
    
    /* localizable strings */
    NSString *tableOfContentsLabel = NSLocalizedString(@"TABLE OF CONTENTS", nil); //checked
    NSString *bookMarkLabel = NSLocalizedString(@"BOOKMARKS", nil); //checked
    NSString *drawingLabel = NSLocalizedString(@"DRAWINGS", nil); //checked
    NSString *annotationLabel = NSLocalizedString(@"ANNOTATIONS", nil); //checked
    NSString *exerciseLabel = NSLocalizedString(@"EXERCISES", nil); //checked
    NSString *encyclopediaEntriesLabel = NSLocalizedString(@"ENCYCLOPEDIA ENTRIES", nil); //checked
    NSString *listOfFiguresLabel = NSLocalizedString(@"LIST OF FIGURES", nil); //checked
    NSString *audioAndVideoLabel = NSLocalizedString(@"AUDIO & VIDEO FILES", nil); //checked
    
//    NSString *segmentTitle = kTabTableOfContents;
    NSString *segmentTitle = tableOfContentsLabel;
    if (selectedTOCIndex == 0) {
        sideBarSelectedType = UITableOfContent;
    }
    if (selectedTOCIndex == 1) {
//        segmentTitle = kTabBookmarks;
        segmentTitle = bookMarkLabel;
        sideBarSelectedType = UIBookmark;
    }
    if (selectedTOCIndex == 2) {
//        segmentTitle = kTabDrawings;
        segmentTitle = drawingLabel;
        sideBarSelectedType = UIAnnotate;
    }
    if (selectedTOCIndex == 3) {
//        segmentTitle = kTabAnnotations;
        segmentTitle = annotationLabel;
        sideBarSelectedType = UIHighlight;
    }
    if (selectedTOCIndex == 4) {
//        segmentTitle = kTabExercises;
        segmentTitle = exerciseLabel;
        sideBarSelectedType = UIExercise;
    }
    if (selectedTOCIndex == 5) {
//        segmentTitle = kTabEncyclopedia;
        segmentTitle = encyclopediaEntriesLabel;
        sideBarSelectedType = UIEncyclopedia;
    }
    if (selectedTOCIndex == 6) {
//        segmentTitle = kTabFigures;
        segmentTitle = listOfFiguresLabel;
        sideBarSelectedType = UIFigure;
    }
    if (selectedTOCIndex == 7) {
//        segmentTitle = kTabMedia;
        segmentTitle = audioAndVideoLabel;
        sideBarSelectedType = UIMedia;
    }
    [self.segmentedLabel hide];
    self.segmentedLabel.text = segmentTitle;
    [self.segmentedLabel fadeIn];
    
    NSString *transition = kCATransitionFromRight;
    
    if (currentSelectedSegmentIndex > selectedTOCIndex) {
        transition = kCATransitionFromLeft;
    }
    
    currentSelectedSegmentIndex = selectedTOCIndex;
    
    [tocTable reloadData];
    [tocTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    //[self.tocTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    [self _loadEmptyViewWithTransition: transition];
    
    if (sideBarSelectedType == UITableOfContent) {
        [self.tocTable selectRowAtIndexPath:[self getCurrentChapterIndexPath] animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
}

- (void)checkSelection:(id)sender
{
    [self _checkTextSelection];
}

- (void) _checkTextSelection {
    NSString *jsonResult = [webView fetchAllHighlights];
    NSDictionary *jsonData = [Utils toDictionary:jsonResult];
    
    if (jsonData) {
        int latestCurrentSelectionNoteCount = [[jsonData objectForKey:@"noteCount"] intValue];
        int latestCurrentSelectionHighlightCount = [[jsonData objectForKey:@"highlightCount"] intValue];
        currentSelectionNoteId = [[jsonData objectForKey:@"noteId"] intValue];
        
        if (latestCurrentSelectionNoteCount != currentSelectionNoteCount ||
            latestCurrentSelectionHighlightCount != currentSelectionHighlightCount) {
            currentSelectionNoteCount = latestCurrentSelectionNoteCount;
            currentSelectionHighlightCount = latestCurrentSelectionHighlightCount;
            
            //VLog(@"jsonData: %@", jsonResult);
            [self _buildContextMenu];
        }
    }
}

- (CXAMenuItemSettings *) _buildImage: (NSString *) image {
    CXAMenuItemSettings *settings = [CXAMenuItemSettings settingsWithDictionary:@{@"image" : [UIImage imageNamed:[image lowercaseString]]}];
    return settings;
}

- (void) _buildColorContextMenu {
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    PSMenuItem *redItem = [[PSMenuItem alloc] initWithTitle:kHighlightRed block:^{
        [self executeHighlightAction:kHighlightRed forAction:UIHighlightActionUpdate];
    }];
    
    [redItem cxa_setSettings:[self _buildImage:kHighlightRed]];
    [items addObject:redItem];
    
    PSMenuItem *greenItem = [[PSMenuItem alloc] initWithTitle:kHighlightGreen block:^{
        [self executeHighlightAction:kHighlightGreen forAction:UIHighlightActionUpdate];
    }];
    [greenItem cxa_setSettings:[self _buildImage:kHighlightGreen]];
    [items addObject:greenItem];
    
    PSMenuItem *blueItem = [[PSMenuItem alloc] initWithTitle:kHighlightBlue block:^{
        [self executeHighlightAction:kHighlightBlue forAction:UIHighlightActionUpdate];
    }];
    [blueItem cxa_setSettings:[self _buildImage:kHighlightBlue]];
    [items addObject:blueItem];
    
    PSMenuItem *yellowItem = [[PSMenuItem alloc] initWithTitle:kHighlightYellow block:^{
        [self executeHighlightAction:kHighlightYellow forAction:UIHighlightActionUpdate];
    }];
    [yellowItem cxa_setSettings:[self _buildImage:kHighlightYellow]];
    [items addObject:yellowItem];

    PSMenuItem *deleteHighlightItem = [[PSMenuItem alloc] initWithTitle:@"Delete" block:^{
        //[self executeUnhighlightAction:kHighlightNote];
        [self executeHighlightAction:kHighlightNote forAction:UIHighlightActionDelete];
    }];
    [deleteHighlightItem cxa_setSettings:[self _buildImage:kHighlightDelete]];
    [items addObject:deleteHighlightItem];
    
    [UIMenuController sharedMenuController].menuItems = items;
    
    //VLog(@"MenuItems: %@", [UIMenuController sharedMenuController].menuItems);
    //[UIMenuController sharedMenuController].menuItems = @[yellowItem, greenItem, redItem, blueItem];
    CGRect selectionRect = CGRectMake(currentPoint.x, currentPoint.y + 60, 0, 0);
    [[UIMenuController sharedMenuController] setTargetRect:selectionRect inView:self.view];
    [[UIMenuController sharedMenuController] setMenuVisible:YES animated:YES];
}

- (void) _buildContextMenu {
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    if (currentSelectionNoteCount == 0) {
        PSMenuItem *redItem = [[PSMenuItem alloc] initWithTitle:kHighlightRed block:^{
            [self executeHighlightAction:kHighlightRed forAction:UIHighlightActionAdd];
        }];
        
        [redItem cxa_setSettings:[self _buildImage:kHighlightRed]];
        [items addObject:redItem];
        
        PSMenuItem *greenItem = [[PSMenuItem alloc] initWithTitle:kHighlightGreen block:^{
            [self executeHighlightAction:kHighlightGreen forAction:UIHighlightActionAdd];
        }];
        [greenItem cxa_setSettings:[self _buildImage:kHighlightGreen]];
        [items addObject:greenItem];
        
        PSMenuItem *blueItem = [[PSMenuItem alloc] initWithTitle:kHighlightBlue block:^{
            [self executeHighlightAction:kHighlightBlue forAction:UIHighlightActionAdd];
        }];
        [blueItem cxa_setSettings:[self _buildImage:kHighlightBlue]];
        [items addObject:blueItem];
        
        PSMenuItem *yellowItem = [[PSMenuItem alloc] initWithTitle:kHighlightYellow block:^{
            [self executeHighlightAction:kHighlightYellow forAction:UIHighlightActionAdd];
        }];
        [yellowItem cxa_setSettings:[self _buildImage:kHighlightYellow]];
        [items addObject:yellowItem];
    }

    if (currentSelectionHighlightCount > 0 && currentSelectionNoteCount == 0) {
        PSMenuItem *deleteHighlightItem = [[PSMenuItem alloc] initWithTitle:@"Delete" block:^{
            [self executeHighlightAction:kHighlightNote forAction:UIHighlightActionDelete];
            //[self executeUnhighlightAction:kHighlightNote];
        }];
        [deleteHighlightItem cxa_setSettings:[self _buildImage:kHighlightDelete]];
        [items addObject:deleteHighlightItem];
    }

    if (currentSelectionHighlightCount > 0 && currentSelectionNoteCount > 0) {
        
        NSString *deleteTitle = NSLocalizedString(@"Delete", nil);
        NSString *alertDeleteTitle = NSLocalizedString(@"Delete Note Item?", nil);
        NSString *alertDeleteMessage = NSLocalizedString(@"Are you sure you want to delete note associated with this?", nil);
        NSString *deleteItLabel = NSLocalizedString(@"Delete It", nil);
        NSString *noLabel = NSLocalizedString(@"No", nil);
        
        PSMenuItem *deleteHighlightItem = [[PSMenuItem alloc] initWithTitle:deleteTitle block:^{
            [[[UIAlertView alloc] initWithTitle:alertDeleteTitle
                                        message:alertDeleteMessage
                               cancelButtonItem:[RIButtonItem itemWithLabel:noLabel action:^{
                // Handle "Cancel"
            }]
            otherButtonItems:[RIButtonItem itemWithLabel:deleteItLabel action:^{
                // Handle "Delete"
                [self executeHighlightAction:kHighlightNote forAction:UIHighlightActionDelete];
            }], nil] show];
        }];
        [deleteHighlightItem cxa_setSettings:[self _buildImage:kHighlightDelete]];
        [items addObject:deleteHighlightItem];
    }
    
    if (currentSelectionNoteCount == 1) {
        NSString *editNoteLabel = NSLocalizedString(@"Edit Note", nil);
        PSMenuItem *editNoteItem = [[PSMenuItem alloc] initWithTitle:editNoteLabel block:^{
            //VLog(@"Data: %@", currentSelectionHighlightData);
            [self executeHighlightAction:kHighlightNote forAction:UIHighlightActionUpdate];
            //[self showNoteController:currentSelectionNoteId withRect:CGRectZero];
        }];
        [items addObject:editNoteItem];
//        PSMenuItem *deleteNoteItem = [[PSMenuItem alloc] initWithTitle:@"Delete" block:^{
//            [self executeUnhighlightAction:kHighlightNote];
//        }];
//        [deleteNoteItem cxa_setSettings:[self _buildImage:kHighlightDelete]];
//        [items addObject:deleteNoteItem];
    } else {
        NSString *addNoteLabel = NSLocalizedString(@"Add Note", nil);
        PSMenuItem *noteItem = [[PSMenuItem alloc] initWithTitle:addNoteLabel block:^{
            [self executeHighlightNoteAction:kHighlightNote];
            [self showNoteController:0 withRect:CGRectZero];
        }];
        [items addObject:noteItem];
    }

    [UIMenuController sharedMenuController].menuItems = items;
    
    //[UIMenuController sharedMenuController].menuItems = @[yellowItem, greenItem, redItem, blueItem];
//    CGRect selectionRect = CGRectMake(currentPoint.x - 45, currentPoint.y, 100, 100);
//    [[UIMenuController sharedMenuController] setTargetRect:selectionRect inView:webView];
//    [[UIMenuController sharedMenuController] setMenuVisible:YES animated:YES];
}

- (void) showNoteController: (int) noteId withRect:(CGRect) rect {
    NoteViewController *vc = [[NoteViewController alloc] initWithNibName:@"NoteViewController" bundle:[NSBundle mainBundle]];
    vc.highlightId = noteId;
    
    NSString *chapterFile = [self.currentChapter.spinePath lastPathComponent];
    NSString *email = [VSmartHelpers getEmail];
    NSString *uuid = self.loadedEpub.uuid;
    NSDictionary *bookData = @{kDBCloudHighlightChapterFile: chapterFile, kDBCloudUUID: uuid, kDBCloudAccountEmail: email};
    vc.bookData = bookData;
    
    notePopover = [[UIPopoverController alloc] initWithContentViewController:vc];
    notePopover.delegate = self;
    [notePopover setPopoverContentSize:vc.view.frame.size];
    CGRect selectionRect = CGRectMake(currentPoint.x, currentPoint.y + 60, 0, 0);
    [notePopover presentPopoverFromRect:selectionRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

// UIPopoverControllerDelegate
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    if ([popoverController isEqual:notePopover]) {
        [self revertSelectedHighlight];
        [self clearSelection];
    }
}

- (void) clearSelection
{
    [webView clearSelection];
}

- (void) executeUnhighlightAction: (NSString *) action {
    NSString *selectedRange = [webView undoHighlightForSelection];
    
    NSDictionary *json = [Utils toDictionary:selectedRange];
    
    NSArray *data = [json objectForKey:@"result"];

    // save to db!
    if ([data count]) {
        [self _removeHighlightToDb:data];
    }
    [self clearSelection];
}


- (void) executeHighlightNoteAction: (NSString *) action {
    NSString *selectedRange = [webView applyHighlightForSelection:action];
    
    NSDictionary *json = [Utils toDictionary:selectedRange];
    [self clearSelection];
    [VSmartHelpers saveTempHighlightNote:json];
}

- (void) executeHighlightAction: (NSString *) cssClass forAction:(UIHighlightAction) action {
    
    if(IsEmpty(self.loadedEpub.uuid)) return;
    
    if (action == UIHighlightActionAdd) {
        NSString *selectedRange = [webView applyHighlightForSelection:cssClass];
        
        VLog(@"HIGHLIGHTED:\n%@", selectedRange);
        
        NSDictionary *json = [Utils toDictionary:selectedRange];
        VLog(@"result: %@", [Utils toDictionary:[json objectForKey:@"result"]]);
        
        NSDictionary *result = [Utils toDictionary:[json objectForKey:@"result"]];
        NSString *cssClass = [result objectForKey:kDBCloudHighlightCss];
        
        NSArray *mergedItems = (NSArray *)[json objectForKey:@"mergedData"];
        int mergedCount = [mergedItems count];
        
        // save to db!
        if ([cssClass isEqualToString:kHighlightNote]) {
            // collect all notes first
        } else {
            [self _saveHighlightToDb:json];
            if (mergedCount > 0) {
                VLog(@"merged: %@", mergedItems);
                [self _removeHighlightToDb:mergedItems];
            }
        }
    }
    
    if (action == UIHighlightActionUpdate) {
        if (currentHighlightIdFromJS > 0) {
            CloudHighlight *cloudHighlight = [self _fetchCloudHighlightById: currentHighlightIdFromJS];
            if (cloudHighlight) {
                NSString *css = [NSString stringWithFormat:@"%@%@", kCloudHighlightPrefix, cssClass];
                cloudHighlight.cssClass = css;
                cloudHighlight.highlightedData = [NSString stringWithFormat:@"%d$%d$%d$%@$", [cloudHighlight.startRange intValue], [cloudHighlight.endRange intValue], currentHighlightIdFromJS, css];
                cloudHighlight.dateModified = [NSDate date];
                
                [self _updateHighlightToDb:cloudHighlight];
            }
            
            // reset
            currentHighlightIdFromJS = 0;
        }
        
        // Show Edit Notes
        if ([cssClass isEqualToString:kHighlightNote]) {
            [self showNoteController:currentSelectionNoteId  withRect:CGRectZero];
            return;
        }
    }

    if (action == UIHighlightActionDelete) {
        if (currentHighlightIdFromJS > 0) {
            [[VibeDataManager sharedInstance] removeCloudHighlightById:[VSmartHelpers getEmail]
                                                              withUUID:self.loadedEpub.uuid
                                                        forChapterFile:[self.currentChapter.spinePath lastPathComponent]
                                                        andHighlightId:currentHighlightIdFromJS];
            
            currentHighlightIdFromJS = 0;
        } else {
            NSString *selectedRange = [webView undoHighlightForSelection];
            NSDictionary *json = [Utils toDictionary:selectedRange];
            
            NSArray *data = [json objectForKey:@"result"];
            // save to db!
            if ([data count]) {
                [self _removeHighlightToDb:data];
            }
        }
    }
    
    [self clearSelection];
    [self _loadHighlights];
}

- (void)enableFullscreenView {
    float value = menuHeader.alpha == 1.0 ? 0.0 : 1.0;
    [UIView beginAnimations:@"bookView" context:nil];
    [UIView setAnimationDelegate:self];
    menuHeader.alpha = value;
    self.chapterSlider.alpha = value;
    [UIView commitAnimations];
}

- (void) loadSubViews {
    VLog(@"loadSubViews");
    //self.menuHeader.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bookHeader.png"]];
    //self.menuTOC.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"slidenav_bg.png"]];
    
    CGRect frame = self.menuHeader.frame;
    frame.origin.y += IOS7_HEIGHT;
    [self.menuHeader setFrame:frame];
    
    [self.view addSubview:menuHeader];
    [self.view bringSubviewToFront:menuHeader];
    CGRect buttonMenuRect = menuHeader.frame;
    buttonMenuRect.origin.x = 0;
    buttonMenuRect.origin.y = 400;
    
    CGRect tocRect = menuTOC.frame;
    tocRect.origin.y = 0;
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    if (UIDeviceOrientationIsLandscape(orientation)) {
        tocRect.origin.x = 1024;
    }
    else {
        tocRect.origin.x = 768;
    }
    
    menuTOC.frame = tocRect;
    
    menuTOC.layer.shadowColor = [[UIColor blackColor] CGColor];
    menuTOC.layer.shadowOpacity = 1.0f;
    menuTOC.layer.shadowRadius = 10.0f;
    
    menuTOC.hidden = YES;
    
    [self.view addSubview:menuTOC];
    [self.view bringSubviewToFront:menuTOC];
    
    //[self enterBookView];
    [self enableFullscreenView];
}

-(void) loadBookSettings {
    
    //BOOK SETTINGS
    NSDictionary *s = [NSDictionary dictionaryWithDictionary: [Utils getBookSetings]];
    
//    self.fontSizeSettings = [[settings objectForKey:@"font_size"] intValue];
//    self.themeNameSettings = [NSString stringWithFormat:@"%@", [[settings objectForKey:@"book_appearance_name"] lowercaseString]];
//    self.fontSettingName = [NSString stringWithFormat:@"%@", [settings objectForKey:@"font_name"]];
    
    self.fontSizeSettings = [s[@"font_size"] intValue];
    self.themeNameSettings = [NSString stringWithFormat:@"%@", [s[@"book_appearance_name"] lowercaseString] ];
    self.fontSettingName = [NSString stringWithFormat:@"%@", s[@"font_name"] ];
    
    currentTextSize = self.fontSizeSettings;
    [self updateBookSettingValues];
    VLog(@"loadBookSettings: %@", s);
}

- (void) updateBookSettingValues {
    VLog(@"Updating colors...");
    self.fontSizeSettings =  DEFAULT_FONT_PERCENTAGE + (currentTextSize * 10);
    self.mainColor = RGBFromColor(236,226,198);
    self.mainTextColor = [UIColor blackColor];

    statusBarStyle = UIStatusBarStyleDefault;
    if ([self.themeNameSettings isEqualToString:@"default"]) {
        self.themeNameSettings = @"reset";
        self.mainColor = [UIColor whiteColor];
        self.mainTextColor = [UIColor darkGrayColor];
    } else if ([self.themeNameSettings isEqualToString:@"night"]) {
        self.themeNameSettings = @"black";
        self.mainColor = RGBFromColor(58,61,67);
        self.mainTextColor = [UIColor whiteColor];
        statusBarStyle = UIStatusBarStyleLightContent;
    }
    
    [self.webView setBackgroundColor:self.mainColor];
    //[self setNeedsStatusBarAppearanceUpdate];
    [[UIApplication sharedApplication] setStatusBarStyle:statusBarStyle];
}

#pragma mark - SettingsViewController Delegate
- (void) didFinishChangeSettings:(SettingsViewController *)controller withValues:(NSDictionary *)values {

    [self updateEpubViewWithSettings:values];
    
//    int savedFontSize = DEFAULT_FONT_PERCENTAGE + ([[values objectForKey:@"font_size"] intValue] * 10);
//    int currentFontSize = self.fontSizeSettings;
//    VLog(@"Font Size: %d:%d", currentFontSize, savedFontSize);
//    BOOL reloadNeeded = NO;
//    
//    if (savedFontSize != currentFontSize) {
//        reloadNeeded = YES;
//    }
//    
//    if (![[values objectForKey:@"font_name"] isEqualToString:self.fontSettingName]) {
//        reloadNeeded = YES;
//    }
//    //self.fontSizeSettings = [[values objectForKey:@"font_size"] intValue];
//    currentTextSize = [[values objectForKey:@"font_size"] intValue];
//    self.fontSettingName = [values objectForKey:@"font_name"];
//    self.themeNameSettings = [[values objectForKey:@"theme"] lowercaseString];
//    brightnessLevel = [[values objectForKey:@"brightness_level"] floatValue];
//    [self adjustBrightness];
//    [self updateBookSettingValues];
//    [self relayoutContent];
//    
//    if (reloadNeeded) {
//        totalPagesCount = 0;
//        [self updatePagination];
//    }
////    if (reloadNeeded) {
////        [self updatePagination];
////    } else {
////        [self applyBookSettings];    
////    }
}

- (void)updateEpubViewWithSettings:(NSDictionary *)settings {
    
    if (settings != nil) {
        
        NSDictionary *values = [NSDictionary dictionaryWithDictionary:settings];
        
        NSLog(@"<-- X --> %s : ---> %@", __PRETTY_FUNCTION__, values);
        
        int savedFontSize = DEFAULT_FONT_PERCENTAGE + ([[values objectForKey:@"font_size"] intValue] * 10);
        int currentFontSize = self.fontSizeSettings;
        VLog(@"Font Size: %d:%d", currentFontSize, savedFontSize);
        BOOL reloadNeeded = NO;
        
        if (savedFontSize != currentFontSize) {
            reloadNeeded = YES;
        }
        
        if (![[values objectForKey:@"font_name"] isEqualToString:self.fontSettingName]) {
            reloadNeeded = YES;
        }
        //self.fontSizeSettings = [[values objectForKey:@"font_size"] intValue];
        currentTextSize = [[values objectForKey:@"font_size"] intValue];
        self.fontSettingName = [values objectForKey:@"font_name"];
        self.themeNameSettings = [[values objectForKey:@"theme"] lowercaseString];
        brightnessLevel = [[values objectForKey:@"brightness_level"] floatValue];
        [self adjustBrightness];
        [self updateBookSettingValues];
        [self relayoutContent];
        
        if (reloadNeeded) {
            totalPagesCount = 0;
            [self updatePagination];
        }
    }
}

- (void) applyBookSettings {
    NSString *bodyAnimation = [NSString stringWithFormat:@"addCSSRule('body', '-webkit-transition: background-color ease .80s;font-size ease 1s;-webkit-font-smoothing:antialiased;')"];
    [webView evalJS:bodyAnimation];
    
    NSString *setTextSizeRule = [NSString stringWithFormat:@"addCSSRule('body', '-webkit-text-size-adjust: %d%%;')", self.fontSizeSettings];
    [webView evalJS:setTextSizeRule];
    [webView evalJS:[NSString stringWithFormat:@"applyColor('%@')", self.themeNameSettings]];
    [webView evalJS:[NSString stringWithFormat:@"applyFontFamily('%@')", self.fontSettingName]];
}

- (void)viewDidUnload {
    [self setBookChapterLabel:nil];
	self.toolbar = nil;
	self.webView = nil;
	self.chapterListButton = nil;
	self.decTextSizeButton = nil;
	self.incTextSizeButton = nil;
	self.pageSlider = nil;
	self.currentPageLabel = nil;
	self.annotator = nil;
    self.chapterSlider.delegate = nil;
    self.chapterSlider = nil;
    self.btnBookmarkLarge = nil;
    
    tapWindow.viewToObserve = nil;
    tapWindow.controllerThatObserves = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:kMainRotateNotification];
}

- (id) initWithEpub:(NSString *) epub
{
    if((self=[super init])){
        currentSpineIndex = 0;
        currentPageInSpineIndex = 0;
        pagesInCurrentSpineCount = 0;
        totalPagesCount = 0;
        searching = NO;
        epubLoaded = NO;
        self.loadedEpub = [[EPub alloc] initWithEPubPath:epub];
        epubLoaded = YES;
        //VLog(@"initWithEpub");
        
        [self updatePagination];
    }
    return self;
}

#pragma mark - TapDetectingWindow Delegate
- (void) userDidTapWebView:(id)tapPoint
{
    if (self.annotator.isActive) return;
    if(!menuTOC.isHidden) return;
    
    if([fontPopover isPopoverVisible] || [chaptersPopover isPopoverVisible] || [searchTextPopover isPopoverVisible] || [notePopover isPopoverVisible]) return;

    if (needsHideContext) {
        needsHideContext = NO;
        if ([[UIMenuController sharedMenuController] isMenuVisible]) {
            [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];
        }
    }
    
    VLog(@"userDidTapWebView");
    
    CGRect viewRect = webView.bounds; // View bounds
    NSArray *points = (NSArray *)tapPoint;
    
    NSValue *val = [points objectAtIndex:0];
    
    CGPoint point = [val CGPointValue];
    //currentPoint = point;
    VLog(@"Point: %@", NSStringFromCGPoint(point));
    
    CGRect nextPageRect = viewRect;
    
    nextPageRect.size.width = TAP_AREA_SIZE;
    nextPageRect.origin.x = (viewRect.size.width - TAP_AREA_SIZE);
    
    if (CGRectContainsPoint(nextPageRect, point)) // page++ area
    {
        [self gotoNextPage]; return;
    }
    
    CGRect prevPageRect = viewRect;
    prevPageRect.size.width = TAP_AREA_SIZE;
    
    if (CGRectContainsPoint(prevPageRect, point)) // page-- area
    {
        [self gotoPrevPage]; return;
    }

    if (currentPoint.y > 0) {
        // Tapping on Table of Contents View, ignore show/hide
        if(!menuTOC.isHidden) return;
        [self enableFullscreenView];
    }
}

- (void) didLetGoSliderKnob:(int)bookIndex {
    VLog(@"Tap Chapter Index: %d", bookIndex);
    currentSpineIndex = bookIndex;
    [self loadSpine:currentSpineIndex atPageIndex:0];
    [self updateSpinePageLabel:currentPageInSpineIndex];
}

#pragma mark - Table of Contents Text Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (sideBarSelectedType == UITableOfContent) {
        NCXElement *e = (NCXElement *)[self.loadedEpub.tocArray objectAtIndex:[indexPath row]];
        return [e indent] + ([e indent] == 0 ? 0 : 2);
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (sideBarSelectedType == UITableOfContent)    return [self.loadedEpub.tocArray count];
    if (sideBarSelectedType == UIBookmark)          return [self.currentBookmark count];
    if (sideBarSelectedType == UIAnnotate)          return [annotator countItems:_currentBookname];
    if (sideBarSelectedType == UIHighlight)         return [self.bookHighlights count];
    if (sideBarSelectedType == UIExercise)          return self.bookExercises != nil ? [self.bookExercises count] : 0;
    if (sideBarSelectedType == UIEncyclopedia)      return self.bookEncyclopediaEntries != nil ? [self.bookEncyclopediaEntries count] : 0;
    if (sideBarSelectedType == UIFigure)            return self.bookFigureEntries != nil ? [self.bookFigureEntries count] : 0;
    if (sideBarSelectedType == UIMedia)             return self.bookMediaEntries != nil ? [self.bookMediaEntries count] : 0;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *cellIdentifiers = @[@"Cell", @"BookHighlightCellIdentifier", @"BookExcerciseCellIdentifier"];
    
    int customCellType = 0;
    
    if (sideBarSelectedType == UIHighlight) {
        customCellType = 1;
    } else if (sideBarSelectedType == UIExercise) {
        customCellType = 2;
    }
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier: cellIdentifiers[customCellType]];
    
    if (cell == nil) {
        switch (customCellType) {
            case 0:
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifiers[customCellType]] autorelease];
                cell.textLabel.numberOfLines = 3;
                cell.textLabel.lineBreakMode = NSLineBreakByTruncatingMiddle; //UILineBreakModeMiddleTruncation;
                //cell.textLabel.adjustsFontSizeToFitWidth = YES;
                cell.textLabel.font = FONT_NEUE_DEFAULT;
                
                UIView *bgColorView = [[UIView alloc] init];
                [bgColorView setBackgroundColor:UIColorFromHex(0xbee0f0)];
                
                [cell setSelectedBackgroundView:bgColorView];
                [bgColorView release];
                break;
            case 1: {
                static NSString *CellNib = @"BookHighlightCell";
                cell = (BookHighlightCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifiers[customCellType]];
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellNib owner:self options:nil];
                cell = (BookHighlightCell *)[nib objectAtIndex:0];
            }
                break;
            case 2: {
                static NSString *CellNib = @"BookExerciseCell";
                cell = (BookExerciseCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifiers[customCellType]];
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellNib owner:self options:nil];
                cell = (BookExerciseCell *)[nib objectAtIndex:0];
            }
                break;
        }
    }

    
    if (sideBarSelectedType == UITableOfContent) {
        //NSString *title = [[self.loadedEpub.spineArray objectAtIndex:[indexPath row]] title];
        //cell.textLabel.text = [self isEmpty:title] ? @"Book Cover" : title;
        
        NCXElement *ncxElement = (NCXElement *)[self.loadedEpub.tocArray objectAtIndex:[indexPath row]];
        NSString *title = ncxElement.text;
        //VLog(@"TOC: %@", [self isEmpty:title] ? @"Book Cover" : title);
        
        if (ncxElement.indent == 0) {
            cell.textLabel.font = FONT_NEUE_DEFAULT;
        }
        else {
            cell.textLabel.font = FONT_NEUE_LIGHT(14);
        }
        
        Chapter *chapter = [chapters valueForKey:title];
        cell.textLabel.text = [NSString stringWithFormat:@"%@", IsEmpty(title) ? @"*Book Cover" : title];
        if (chapter.pageInBook != 0) {
            cell.detailTextLabel.font = FONT_NEUE_LIGHT(13);
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", chapter.pageInBook];
        }
        
//        Chapter *currentChapter = (Chapter *)[loadedEpub.spineArray objectAtIndex:currentSpineIndex];
//        if ([currentChapter.text isEqualToString:title]) {
//            VLog(@"currentChapter: %@:%@", currentChapter.text, title);
//            [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
//        }
        cell.imageView.image = nil;
    }
    
    if (sideBarSelectedType == UIBookmark) {
        if(IsEmpty(self.currentBookmark)) return cell;
        
        NSDictionary *item = [self.currentBookmark objectAtIndex:indexPath.row];
        NSString *chapterTitle = [item objectForKey:kChapterName];
        int pageNum = [[item objectForKey:kChapterPageIndexInSpine] integerValue] + 1;
        NSString *page = [NSString stringWithFormat:@"p. %d", pageNum];
        cell.textLabel.font = FONT_NEUE_DEFAULT;
        cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", chapterTitle, page];
        cell.detailTextLabel.text = @"";
        cell.imageView.image = nil;
    }
    
    if (sideBarSelectedType == UIAnnotate) {
        cell.textLabel.font = FONT_NEUE_DEFAULT;
        cell.textLabel.text = [annotator getCellTextLabel:_currentBookname row:indexPath.row];
        cell.detailTextLabel.text = @"";
        cell.imageView.image = nil;
    }
    
    if (sideBarSelectedType == UIHighlight) {
        cell.textLabel.adjustsFontSizeToFitWidth = YES;

        CloudHighlight *highlight = (CloudHighlight *)[self.bookHighlights objectAtIndex:indexPath.row];

        // Truncate text
        NSString *shortString = [highlight.highlightedText stringByTruncatingText:180];
        //cell.detailTextLabel.font = FONT_NEUE_LIGHT(13);
        
        BOOL hasNote = [highlight.hasNote boolValue];
        UIImage *image = [UIImage imageNamed:@"icn_annotation-highlight"];
        if (hasNote) {
            image = [UIImage imageNamed:@"icn_toc_annotation"];
            shortString = [highlight.note stringByTruncatingText:180];
        } else {
            NSString *cssClass = [[highlight.cssClass stringByReplacingOccurrencesOfString:@"vibeHighlight" withString:@""] lowercaseString];
            image = [UIImage imageNamed:[NSString stringWithFormat:@"icn_highlight-%@", cssClass]];            
        }
//        cell.textLabel.font = FONT_NEUE_DEFAULT;
//        cell.textLabel.text = [NSString stringWithFormat:@"%@ ..", shortString];
//        cell.detailTextLabel.text = [highlight.dateAdded relativeDateString];
//        cell.imageView.image = image;
        
        [(UIImageView *)[cell viewWithTag:4] setImage:image];
        [(UILabel *)[cell viewWithTag:1] setText:hasNote ? @"NOTE" : @"HIGHLIGHT"];
        [(UILabel *)[cell viewWithTag:2] setText:[highlight.dateAdded relativeDateString]];
        [(UILabel *)[cell viewWithTag:3] setText:[shortString length] >= 180 ? [NSString stringWithFormat:@"%@ ..", shortString] : shortString];
    }
    
    if (sideBarSelectedType == UIExercise) {
        if (self.bookExercises) {
            CloudBookExercise *exercise = (CloudBookExercise *)[self.bookExercises objectAtIndex:indexPath.row];
//            cell.textLabel.font = FONT_NEUE_DEFAULT;
//            cell.textLabel.text = exercise.exerciseTitle;
//            cell.imageView.image = nil;
//            cell.detailTextLabel.text = [NSString stringWithFormat:@"%d items", [exercise.itemCount intValue]];
            
            Chapter *chapter = [self getChapterForFile:exercise.chapterFile];
            NSString *chapterTitle = [chapter.title length] >= 60 ? [NSString stringWithFormat:@"%@ ...", [chapter.title stringByTruncatingText:60]] : chapter.title;
            
            NSString *itemsLocalizedLabel = NSLocalizedString(@"items", nil);
            NSString *itemCount = [NSString stringWithFormat:@"%d %@", [exercise.itemCount intValue], itemsLocalizedLabel];
            [(UILabel *)[cell viewWithTag:1] setText:chapterTitle];
            [(UILabel *)[cell viewWithTag:2] setText:itemCount];
            
            
            NSString *exercise_tite = [NSString stringWithFormat:@"%@", exercise.exerciseTitle];
            NSString *exercise_password = @"";
            if ([[VSmart sharedInstance].account.user.position isEqualToString:kModeIsTeacher]) {
                exercise_password = [NSString stringWithFormat:@"pass:[ %@ ]", exercise.password];
            }
            
//            [(UILabel *)[cell viewWithTag:3] setText:exercise.exerciseTitle];
            [(UILabel *)[cell viewWithTag:3] setText:exercise_tite];
            [(UILabel *)[cell viewWithTag:7] setText:exercise_password];
            NSArray *answers = [[VibeDataManager sharedInstance] fetchCloudAnswer:[VSmartHelpers getEmail] forBook:exercise.uuid inChapterFile:exercise.chapterFile forExerciseId:exercise.exerciseId];

            UIImage *image = [UIImage imageNamed:[answers count] == 0  ? @"icn_offcanvas-panel_exercises" : @"icn_offcanvas-panel_exercises_active"];
            [(UIImageView *)[cell viewWithTag:4] setImage:image];
        }
    }

    if (sideBarSelectedType == UIEncyclopedia) {
        if (self.bookEncyclopediaEntries) {
            CloudBookEncyclopedia *entry = (CloudBookEncyclopedia *)[self.bookEncyclopediaEntries objectAtIndex:indexPath.row];
            cell.textLabel.font = FONT_NEUE_DEFAULT;
            cell.textLabel.text = [entry.text length] >= 80 ? [NSString stringWithFormat:@"%@ ..", [entry.text stringByTruncatingText:80]] : entry.text;
            cell.imageView.image = nil;
            cell.detailTextLabel.text = @"";
        }
    }

    if (sideBarSelectedType == UIFigure) {
        if (self.bookFigureEntries) {
            CloudBookFigure *entry = (CloudBookFigure *)[self.bookFigureEntries objectAtIndex:indexPath.row];
            cell.textLabel.font = FONT_NEUE_DEFAULT;

            int textLength = 130;
            cell.textLabel.textAlignment = NSTextAlignmentNatural;
            cell.textLabel.text = [entry.caption length] >= textLength ? [NSString stringWithFormat:@"%@ ..", [entry.caption stringByTruncatingText:textLength]] : entry.caption;
            
            CGSize itemSize = CGSizeMake(95, 95);
            
            cell.detailTextLabel.text = @"";
            cell.imageView.image = [[UIImage imageNamed:@"book-placeholder"] scaleImageToSize:itemSize];
            
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
            dispatch_async(queue, ^{
                NSString *imagePath = [self.loadedEpub.baseDirectory stringByAppendingFormat:@"/%@", entry.imagePath];
                UIImage *figureImage = [UIImage imageWithContentsOfFile:imagePath];
                UIImage *finalImage = [figureImage scaleImageToSize:itemSize];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    cell.imageView.image = finalImage;
                    [cell.imageView setNeedsDisplay];
                });
            });
        }
    }
    
    if (sideBarSelectedType == UIMedia) {
        if (self.bookMediaEntries) {
            CloudBookMedia *entry = (CloudBookMedia *)[self.bookMediaEntries objectAtIndex:indexPath.row];
            cell.textLabel.font = FONT_NEUE_DEFAULT;
            
            int textLength = 130;
            cell.textLabel.textAlignment = NSTextAlignmentNatural;
            cell.textLabel.text = [entry.mediaTitle length] >= textLength ? [NSString stringWithFormat:@"%@ ..", [entry.mediaTitle stringByTruncatingText:textLength]] : entry.mediaTitle;
            
            cell.detailTextLabel.text = @"";
            UIImage *image = [entry.mediaType isEqualToString:@"video"] ? [UIImage imageNamed:@"icn_video_list"] : [UIImage imageNamed:@"icn_audio_list"];
            cell.imageView.image = image;
        }
    }
    
    [cell.imageView setNeedsDisplay];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (sideBarSelectedType == UIHighlight) {
        return 90.0f;
    }
    if (sideBarSelectedType == UIFigure) {
        return 100.0f;
    }
    return 65.0f;
}

- (void) _loadSelectedExercise {
    if (needsReloadForBookExercise) {
        needsReloadForBookExercise = NO;
        
        int pageIndex = [webView getPageIndex:self.selectedCloudBookExercise.exerciseId forWebviewWidth:[webView width]];
        [self loadSpine:currentSpineIndex atPageIndex:pageIndex - 1];

        self.selectedCloudBookExercise = nil;
    }
}

- (void) _loadSelectedEncyclopedia {
    if (needsReloadForBookEncyclopedia) {
        needsReloadForBookEncyclopedia = NO;
        
        //NSString *query = [NSString stringWithFormat:@"vibeFindPageNumberForEncyclopedia('%@', %d)", self.selectedCloudBookEncyclopedia.filePath, [webView width]];
        
        //int pageIndex = [[webView evalJS:query] intValue];
        int pageIndex = [webView getPageIndex:self.selectedCloudBookEncyclopedia.filePath forWebviewWidth:[webView width] forEncyclopedia:YES];
        [self loadSpine:currentSpineIndex atPageIndex:pageIndex];
        
        self.selectedCloudBookEncyclopedia = nil;
    }
}

- (void) _loadSelectedFigure {
    if (needsReloadForBookFigure) {
        needsReloadForBookFigure = NO;
        
        int pageIndex = [webView getPageIndex:self.selectedCloudBookFigure.imageId forWebviewWidth:[webView width]];
        [self loadSpine:currentSpineIndex atPageIndex:pageIndex];
        
        self.selectedCloudBookFigure = nil;
    }
}

- (void) _loadSelectedMedia {
    if (needsReloadForBookMedia) {
        needsReloadForBookMedia = NO;
        
        int pageIndex = [webView getPageIndex:self.selectedCloudBookMedia.mediaId forWebviewWidth:[webView width]];
        [self loadSpine:currentSpineIndex atPageIndex:pageIndex];
        
        self.selectedCloudBookMedia = nil;
    }
}

- (void) _loadSelectedHighlight {
    if (needsReloadForHighlight) {
        needsReloadForHighlight = NO;
        
        int pageIndex = [webView getPageIndex:[self.selectedCloudHighlight.startRange intValue] andEndRange:[self.selectedCloudHighlight.endRange intValue] forWebviewWidth:[webView width]];
        [self loadSpine:currentSpineIndex atPageIndex:pageIndex];
        self.selectedCloudHighlight = nil;
    }
}

// Needed for Highlight Selection on TOC
- (Chapter *) getChapterForFile: (NSString *) chapterFile {
    NSArray *chaptersArray = loadedEpub.spineArray;
    int row = 0;
    Chapter *selectedChapter = nil;
    
    for (int i = 0; i < [chaptersArray count]; i++) {
        Chapter *chapter = (Chapter *)[chaptersArray objectAtIndex:i];
        NSString *chapterPath = [chapter.spinePath lastPathComponent];
        
        if ([chapterPath isEqualToString:chapterFile]) {
            row = i;
            selectedChapter = chapter;
            break;
        }
    }
    
    return selectedChapter;
}

- (NSIndexPath *) getCurrentChapterIndexPath {
    
    int row = 0;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    
    if ( !(currentSpineIndex < 0) && !(currentSpineIndex > [loadedEpub.spineArray count]) ) {
        Chapter *curChapter = (Chapter *)[loadedEpub.spineArray objectAtIndex:currentSpineIndex];
        NSArray *chaptersArray = loadedEpub.spineArray;
        
        for (int i = 0; i < [chaptersArray count]; i++) {
            Chapter *chapter = (Chapter *)[chaptersArray objectAtIndex:i];
            if (chapter == curChapter) {
                row = i;
                break;
            }
        }
        
        return [NSIndexPath indexPathForRow:row - 1 inSection:0];
    }
    
    return indexPath;
}

- (int) getChapterIndex: (NCXElement *) tocElement {
    NSArray *chaptersArray = loadedEpub.spineArray;
    NSArray *urlHash = [self saveHashTag:tocElement.url];
    NSString *rootUrlForHashPage = @"";
    
    if (urlHash) {
        rootUrlForHashPage = [urlHash objectAtIndex:0];
    }
    else {
        rootUrlForHashPage = tocElement.url;
    }
    
    for (int i = 0; i < [chaptersArray count]; i++) {
        Chapter *chapter = (Chapter *)[chaptersArray objectAtIndex:i];
        NCXElement *ncx = (NCXElement *)chapter.ncxElement;
        
        NSString *ncxUrl = [self getUrlFromHashedUrl:ncx.url];
        //VLog(@"ncxUrl: %@", ncxUrl);
        
        if([ncxUrl isEqualToString:rootUrlForHashPage])
        {
            [self saveDataForFootnote:@"SPINE_INDEX" forValue:i];
            return i;
        }
    }
    
    return -1;
}

- (NSString *) getUrlFromHashedUrl: (NSString *) url {
    
    NSRange hashTagRange = [url rangeOfString:@"#" options:NSCaseInsensitiveSearch];
    
    if (hashTagRange.location != NSNotFound) {
        NSArray *splittedUrl = [url componentsSeparatedByString:@"#"];
        NSString *urlText = [splittedUrl objectAtIndex:0];
        return urlText;
    }
    
    return url;
}

- (NSArray *) saveHashTag: (NSString *) url {
    [Utils clearHash];
    
    NSRange hashTagRange = [url rangeOfString:@"#" options:NSCaseInsensitiveSearch];
    
    if (hashTagRange.location != NSNotFound) {
        NSArray *splittedUrl = [url componentsSeparatedByString:@"#"];
        hashTag = [splittedUrl objectAtIndex:1];
        VLog(@"HashTag: %@", hashTag);
        [Utils saveHash:hashTag];
        return splittedUrl;
    }
    
    return nil;
}

- (NSDictionary *)buildAssessmentDataWithScore:(NSString *)string
{
    NSString *user_id = [NSString stringWithFormat:@"%d",[VSmart sharedInstance].account.user.id];
    NSString *book_uuid = [NSString stringWithFormat:@"%@", self.loadedEpub.uuid];
    NSString *assessment_name = [NSString stringWithFormat:@"%@", self.bookChapterLabel.text];
    NSString *assessment_description = [NSString stringWithFormat:@"%@", self.loadedEpub.title];
    NSString *score = [NSString stringWithFormat:@"%@", string];
    
    NSDictionary *data = @{@"user_id": user_id,
                           @"book_uuid": book_uuid,
                           @"assessment_name": assessment_name,
                           @"assessment_description": assessment_description,
                           @"score": score};
    
    return data;
}

- (void) processExerciseResult: (NSArray *) result {
    
    if (![Utils isExerciseDataFileExists:self.currentBookname]) {
        return;
    }
    
    VLog(@"EXERCISE_RESULT: %@", result);
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^{
        BookExerciseResult *exerciseResult = [[BookExerciseResult alloc] init];
        exerciseResult.bookName = self.currentBookname;
        exerciseResult.chapterName = self.bookChapterLabel.text;
        exerciseResult.result = [[result objectAtIndex:1] intValue];
        exerciseResult.dateCreated = [NSDate date];
        exerciseResult.bookTitle = self.loadedEpub.title;
        
        BookExercise *exercise = [VSmartHelpers getBookExercise:self.currentBookname withExerciseId:[[result objectAtIndex:0] intValue] ];
        exerciseResult.bookExercise = exercise;
        
        [VSmartHelpers saveBookExerciseResult:exerciseResult];
        VLog(@"ExerciseResult: %@", exerciseResult);
    });
}

#pragma mark - Table of Contents Text Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (sideBarSelectedType == UITableOfContent) {
        NCXElement *toc = (NCXElement *)[loadedEpub.tocArray objectAtIndex:indexPath.row];
        int chapterIndex = [self getChapterIndex:toc];
        
        if (chapterIndex < 0) {
            chapterIndex = 0;
        }
                
        currentSpineIndex = chapterIndex;
        [self loadSpine:currentSpineIndex atPageIndex:0 highlightSearchResult:nil];
    }
    
    if (sideBarSelectedType == UIBookmark) {
        NSDictionary *item = [self.currentBookmark objectAtIndex:indexPath.row];
        currentSpineIndex = [[item objectForKey:kChapterSpineIndex] integerValue];
        [self loadSpine:currentSpineIndex
            atPageIndex:[[item objectForKey: kChapterPageIndexInSpine] integerValue]];
        [self updateSpinePageLabel:currentPageInSpineIndex];
        [self setBookmarkState];
    }
    
    if (sideBarSelectedType == UIAnnotate) {
        int spine = [annotator getSpine:_currentBookname row:indexPath.row];
        currentSpineIndex = spine;
        int pageIndex = [annotator getPageIndex:_currentBookname row:indexPath.row];
        [self loadSpine:spine atPageIndex:pageIndex];
    }
    
    if (sideBarSelectedType == UIHighlight) {
        self.selectedCloudHighlight = (CloudHighlight *)[self.bookHighlights objectAtIndex:indexPath.row];
        Chapter *chapterHighlight = [self getChapterForFile:self.selectedCloudHighlight.chapterFile];
        if (chapterHighlight) {
            needsReloadForHighlight = YES;
            [self loadSpine:chapterHighlight.chapterIndex atPageIndex:0];
        }
    }
    
    if (sideBarSelectedType == UIExercise) {
        self.selectedCloudBookExercise = (CloudBookExercise *)[self.bookExercises objectAtIndex:indexPath.row];
        Chapter *chapterExercise = [self getChapterForFile:self.selectedCloudBookExercise.chapterFile];
        if (chapterExercise) {
            needsReloadForBookExercise = YES;
            [self loadSpine:chapterExercise.chapterIndex atPageIndex:0];
        }
    }
    
    if (sideBarSelectedType == UIEncyclopedia) {
        self.selectedCloudBookEncyclopedia = (CloudBookEncyclopedia *)[self.bookEncyclopediaEntries objectAtIndex:indexPath.row];
        Chapter *chapter = [self getChapterForFile:self.selectedCloudBookEncyclopedia.chapterFile];
        if (chapter) {
            needsReloadForBookEncyclopedia = YES;
            [self loadSpine:chapter.chapterIndex atPageIndex:0];
        }
    }

    if (sideBarSelectedType == UIFigure) {
        self.selectedCloudBookFigure = (CloudBookFigure *)[self.bookFigureEntries objectAtIndex:indexPath.row];
        Chapter *chapter = [self getChapterForFile:self.selectedCloudBookFigure.chapterFile];
        if (chapter) {
            needsReloadForBookFigure = YES;
            [self loadSpine:chapter.chapterIndex atPageIndex:0];
        }
    }

    if (sideBarSelectedType == UIMedia) {
        self.selectedCloudBookMedia = (CloudBookMedia *)[self.bookMediaEntries objectAtIndex:indexPath.row];
        Chapter *chapter = [self getChapterForFile:self.selectedCloudBookMedia.chapterFile];
        if (chapter) {
            needsReloadForBookMedia = YES;
            [self loadSpine:chapter.chapterIndex atPageIndex:0];
        }
    }
    
    [self closeTOC:nil];
    
    if (sideBarSelectedType == UIAnnotate) {
        [self enableDrawingMode: YES];
    }
}

#pragma mark - BibleInfoViewController Delegate
- (void) didFinishedLoadingBibleLink:(BibleInfoViewController *)popViewController {
    [self dismissPopupViewControllerWithanimationType:PopupViewAnimationFade];
}

#pragma mark - QuizResultViewController Delegate
- (void) didCloseQuizResultWindow:(QuizResultViewController *)popViewController {
    [self dismissPopupViewControllerWithanimationType:PopupViewAnimationSlideBottomTop];
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *) theWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    //hashTag = @"";
    
    //VLog(@"shouldStartLoadWithRequest:");
    NSString *url = [[request URL] absoluteString] ;
    VLog(@"shouldStartLoadWithRequest Full URL: %@", url);
    NSRange hashTagRange = [url rangeOfString:@"#" options:NSCaseInsensitiveSearch];
    NSString *filePath = [[request URL] path];  
    
    if (hashTagRange.location != NSNotFound) {
        NSArray *splittedUrl = [url componentsSeparatedByString:@"#"];
        hashTag = [splittedUrl objectAtIndex:1];
        VLog(@"HashTag: %@", hashTag);
        [Utils saveHash:hashTag];
        
        [self loadPage:filePath];
        //return NO;
    }
    
    VLog(@"shouldStartLoadWithRequest FilePath: %@", filePath);
    VLog(@"Relative Path: %@", url);
    
    if ([[filePath pathExtension] isIn:@"xhtml", @"html", @"htm", nil] && [[[request URL] scheme] isEqualToString:@"file"]) {
        NSString *spinePath = filePath;
        NSData *toWrite = nil;
        
        isCoverPage = [self.loadedEpub.coverPageUrl isEqualToString:[spinePath lastPathComponent]];
        //VLog(@"self.loadedEpub.coverPageUrl: %@", self.loadedEpub.coverPageUrl);
        //VLog(@"spinePath: %@", [spinePath lastPathComponent]);
        
        // Allows NonDRM to read from the Vibe Reader too
        if ([self isBookSecured:spinePath]) {
            toWrite = [self.vibeDRM decrypt:[NSData dataWithContentsOfFile:spinePath]];
        }
        else {
            toWrite = [NSData dataWithContentsOfFile:spinePath];
        }
        
        if (toWrite) {
            NSString *currWrite = [[[NSString alloc] initWithData:toWrite encoding:NSUTF8StringEncoding] autorelease];
            currWrite = [currWrite stringByReplacingOccurrencesOfString:@"<title></title>" withString:@"<title>Untitled</title>"];
            currWrite = [currWrite stringByReplacingOccurrencesOfString:@"<title/>" withString:@"<title>Untitled</title>"];
            currWrite = [currWrite stringByReplacingOccurrencesOfString:@"<title />" withString:@"<title>Untitled</title>"];
            
            [self checkjQuery:currWrite];
            
            NSURL *baseURL = [NSURL fileURLWithPath:[spinePath stringByDeletingLastPathComponent] isDirectory:YES];
            VLog(@"********\n%@\n**********", baseURL);
            
            /// DRM
            NSRange range = [url rangeOfString:@"/ENC/" options:NSCaseInsensitiveSearch];
            NSRange rangeLink = [url rangeOfString:@"/link" options:NSCaseInsensitiveSearch];

            if (range.location != NSNotFound) {
                [self loadEncyclopediaView:currWrite withBaseURL:baseURL];
            }
            else if (rangeLink.location != NSNotFound) {
                [self loadBibleView:currWrite withBaseURL:baseURL];
            }
            else {
                [theWebView loadHTMLString:currWrite baseURL:baseURL];    
            }
        }
        else {
            [SVProgressHUD dismiss];
            AlertWithMessageAndDelegate(@"Unknown Error", @"Content cannot be displayed.", nil);
        }
        
        return NO;
    }
    
    // Javascript commands embedded on the xhtml pages
    //This will catch clicked links and location changes made from Javascript, but no other request types
    if (navigationType == UIWebViewNavigationTypeLinkClicked || navigationType == UIWebViewNavigationTypeOther)
    {
        NSURL *URL = [request URL]; //Get the URL
        //The [URL scheme] is the "http" or "ftp" portion, for example
        //so let's make one up that isn't used at all -> "objc"
        //
        if ( [[URL scheme] isEqualToString:@"vibereader"] ) {
            //The [URL host] is the next part of the link
            //so we can use that like a selector
            
            NSString *selectorName = [URL host];
            id data = nil;
            
            NSMutableArray *parameters = [NSMutableArray array];
            if ( ![[URL path] isEqualToString:@""] )
            {
                selectorName =  [NSString stringWithFormat:@"%@:", selectorName];
                parameters = [NSMutableArray arrayWithArray: [[URL path] componentsSeparatedByString:@"/"] ];
                [parameters removeObjectAtIndex:0]; //first object is just a slash "/"
                if ( [parameters count] == 1 ){
                    data = [parameters objectAtIndex:0];
                }
                else{
                    data = parameters;
                }
            }
            
            VLogv(data);
            if ([data isIn:@"exercise", nil]) {
                // FOR FUTURE USE
            } else {
                //AlertWithMessageAndDelegate(@"Exercise Result", [NSString stringWithFormat:@"%@", data], nil);
                QuizResultViewController *resultView = [[QuizResultViewController alloc] initWithNibName:@"QuizResultViewController" bundle:nil];
                resultView.delegate = self;
                //resultView.result = [data stringByReplacingOccurrencesOfString:@"," withString:@" / "];
                NSArray *result = [data componentsSeparatedByString:@","];
                VLog(@"Result: %@", result);
                
                // Save Exercise result
                [self processExerciseResult:result];
                
                resultView.assessmentData = [self buildAssessmentDataWithScore:[result objectAtIndex:1]];
                resultView.result = VS_FMT(@"%@/%@", [result objectAtIndex:1], [result objectAtIndex:2]);
                
                [self presentPopupViewController:resultView animationType:PopupViewAnimationSlideBottomTop];
                
                int score = [[result objectAtIndex:1] intValue];
                int totalItem = [[result objectAtIndex:2] intValue];
                NSString *soundFile = @"";
                if (score == totalItem) {
                    soundFile = kQuizResultPerfectScore;
                } else {
                    float halfScore = totalItem / 2;
                    if (score <= halfScore) {
                        soundFile = kQuizResultFailedScore;
                    } else {
                        soundFile = kQuizResultAverageScore;
                    }
                }
                [Utils playSoundFile:soundFile withType:kJSQSystemSoundTypeWAV];
                
                if (!bookUUIDEmpty) {
                    NSString *result = [webView fetchExerciseAnswers];
                    //VLog(@"PLAIN-JSON:\n%@", result);
                    NSDictionary *dict = [Utils toDictionary:result];
                    //VLog(@"JSON:\n%@", dict);
                    NSString *chapterFile = [self.currentChapter.spinePath lastPathComponent];
                    [[VibeDataManager sharedInstance] addCloudAnswer:[VSmartHelpers getEmail] forBook:self.loadedEpub.uuid inChapterFile:chapterFile forBookAnswer:dict];
                }
            }
            return NO;
        }
        if ( [[URL scheme] isEqualToString:@"notify"] ) {
            NSString *selectorName = [URL host];
            id data = nil;
            
            NSMutableArray *parameters = [NSMutableArray array];
            if ( ![[URL path] isEqualToString:@""] )
            {
                selectorName =  [NSString stringWithFormat:@"%@:", selectorName];
                parameters = [NSMutableArray arrayWithArray: [[URL path] componentsSeparatedByString:@"/"] ];
                
                [parameters removeObjectAtIndex:0]; //first object is just a slash "/"
                
                if ( [parameters count] == 1 ){
                    data = [parameters objectAtIndex:0];
                } else {
                    data = parameters;
                }
                
                NSString *actionType = [parameters objectAtIndex:0];
                int highlightId = [[parameters objectAtIndex:1] intValue];
                VLog(@"ActionType: %@ with ID: %d", actionType, highlightId);
                
                if([actionType isEqualToString:@"note"]) {
                    CGRect rect = CGRectMake([[parameters objectAtIndex:2] intValue], [[parameters objectAtIndex:3] intValue], 100, 100);
                    [self showNoteController: highlightId withRect:rect];
                } else if([actionType isEqualToString:@"color"]){
                    currentHighlightIdFromJS = highlightId;
                    needsHideContext = YES;
                    [self _buildColorContextMenu];
                } else if([actionType isEqualToString:@"answer"]){
                    NSString *exerId = [parameters objectAtIndex:1];
                    //VLog(@"exerId: %@", exerId);
                    [self _resetSavedAnswer:exerId];
                } else if([actionType isEqualToString:@"security"]){
                    VLog(@"PUMASOK: %@", [parameters objectAtIndex:1]);
                    
                    if ([[VSmart sharedInstance].account.user.position isEqualToString:kModeIsStudent]) {
                        self.unlockViewController.exerId = [parameters objectAtIndex:1];
                        self.unlockViewController.textField.text = @"";
                        [self presentPopupViewController:self.unlockViewController animationType:PopupViewAnimationFade];
                    }
                    else{
                        [self.unlockListController initializeList:self.loadedEpub.uuid exerId:[parameters objectAtIndex:1]];
                        [self dismissPopupViewControllerWithanimationType:PopupViewAnimationFade];
                        [self presentPopupViewController:self.unlockListController animationType:PopupViewAnimationSlideBottomTop];
                        
                        self.unlockListController.bookTitle.text = @"";
                    }
                }
            }
            return NO;
        }
    }
    
    // Regular http://
    if ([[[request URL] scheme] isEqualToString:@"http"] || [[[request URL] scheme] isEqualToString:@"https"]){
		[[UIApplication sharedApplication] openURL:[request URL]];
		return NO;
	}
    
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)theWebView{
    
	VLog(@"webViewDidFinishLoad");
    [self relayoutContent];
    // Is user selected saved highlight/note?
    [self _loadSelectedHighlight];
    [self _loadSelectedExercise];
    [self _loadSelectedEncyclopedia];
    [self _loadSelectedFigure];
    [self _loadSelectedMedia];
}

- (void) checkjQuery: (NSString *) html {
    jQueryScriptExists = NO;
    
    NSRange range = [html rangeOfString:@"jquery.js" options:NSCaseInsensitiveSearch];
    if(range.location != NSNotFound) {
        jQueryScriptExists = YES;
    }
}

-(IBAction)assessmentCodeUnlock:(NSString*)exerId withCode:(NSString*)code{
    
    [self dismissPopupViewControllerWithanimationType:PopupViewAnimationFade];
    
    BOOL isValid = [VSmartHelpers validateAssessment:self.loadedEpub.uuid withExerciseId:exerId withCode:code];
    
    if (!isValid) {
        return;
    }

    [self _loadExerciseData];
}

- (void) relayoutContent {
    
    if (!jQueryScriptExists) {
        [webView attachjQuery];
    }
    
    // Rangy Stuff
    [webView attachRangyFiles];
    
    // Vibe Scripts
    [webView attachVibeFiles];
    
    if (exerciseFileExists) {
        [webView attachJS:@"vibebook-persistence"];
        [webView attachJS:@"vibebook-exersec"];
        [self _loadSavedAnswers];
        [self _loadExerciseData];
    }

    // Apply custom CSS styles
    [webView applyCustomStyles];
    [self updateOffsetPadding];
    
    float webWidth = 0;
    float webHeight = 0;
    float imagePercentAdjust = 0.75;
    
    NSString *insertRule1 = @"";
    
    if (self.loadedEpub.isFixedLayout) {
        [webView evalJS:@"injectFile('styles.css', 'css')"];
    }
    
    NSString *dashedColor = @"#000";
    if ([self.themeNameSettings isEqualToString:@"black"]) {
        dashedColor = @"#FFF";
    }
    
    if (IS_LANDSCAPE) {
        webWidth = (float)(webView.frame.size.width / 2) - 200;
        VLog(@"WebView Width: %f", (float)webView.frame.size.width);
        webHeight = webView.frame.size.height - 20;
        float paddingSize = 50;
        float paddingTopBottom = 10;
        
        imagePercentAdjust = 0.75;
        insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'padding: %fpx %fpx %fpx %fpx; height: %fpx; -webkit-column-gap: 100px; -webkit-column-width: %fpx; -webkit-column-fill: balance;-webkit-column-rule: 1px dashed %@;')", paddingTopBottom, paddingSize, paddingTopBottom, paddingSize, webHeight, webWidth, dashedColor];
        // Childrens Book
        if (self.loadedEpub.isFixedLayout || isCoverPage) {
            VLog(@"CHILDRENS_BOOK");
            imagePercentAdjust = 0.94;
            webWidth = webView.frame.size.width / 2 ;
            webHeight = webView.frame.size.height;
            insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; height: %fpx; -webkit-column-gap: 1px; -webkit-column-width: %fpx; -webkit-column-fill:balance; -webkit-column-rule: 1px solid red')", webHeight, webWidth];
        }
    } else {
        webWidth = webView.frame.size.width;
        webHeight = webView.frame.size.height - 15;
        insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'padding: 0px 30px 0px 30px; height: %fpx; -webkit-column-gap: 60px; -webkit-column-width: %fpx; -webkit-column-fill: balance;')", webHeight, webWidth];
    }
    
    if (self.loadedEpub.isFixedLayout && !IS_LANDSCAPE) {
        imagePercentAdjust = 1.0;
    }
    
	//NSString *insertRule2 = [NSString stringWithFormat:@"addCSSRule('p', 'text-align: justify;')"];
    NSString *setTextSizeRule = [NSString stringWithFormat:@"addCSSRule('body', '-webkit-text-size-adjust: %d%%;')", self.fontSizeSettings];
    //VLog(@"webViewDidFinishLoad: setTextSizeRule: %i", self.fontSizeSettings);
    
    NSString *setImageRule = [NSString stringWithFormat:@"addCSSRule('img', 'max-width: %fpx; width:auto; height:auto;')", webWidth * imagePercentAdjust];
    
    //VLog(@"webViewDidFinishLoad: webWidth: %f", webWidth);
    //VLog(@"webViewDidFinishLoad: webHeight: %f", webHeight);
    
    if(!self.loadedEpub.isFixedLayout) {
        [self _loadHighlights];
        if (!isCoverPage) {
            [webView evalJS:insertRule1];
        }
        [webView evalJS:setImageRule];
    }
    
    [webView applySmoothTransitionEffect];
    [webView applyPageTheme:self.themeNameSettings];
    [webView applyFontFamily:self.fontSettingName];
    
	//[webView evalJS:insertRule2];
	[webView evalJS:setTextSizeRule];
    
    if(currentSearchResult!=nil){
        //	NSLog(@"Highlighting %@", currentSearchResult.originatingQuery);
        [webView highlightAllOccurencesOfString:currentSearchResult.originatingQuery];
	}
    
    VLog(@"IS_COVER_PAGE: %@", isCoverPage ? @"YES" : @"NO");
    if (self.loadedEpub.isFixedLayout || isCoverPage) {
        VLog(@"FIXED_LAYOUT");
        insertRule1 = [NSString stringWithFormat:@"addCSSRule('body', 'margin: 0px; padding: 0px; height: %fpx; width: %fpx; -webkit-column-gap: 0px;-webkit-column-width: %fpx;')", webHeight, webWidth, webWidth];
        [webView evalJS:insertRule1];
        NSString *setParaRule = [NSString stringWithFormat:@"addCSSRule('p', 'margin:0px;padding:0px;')"];
        [webView evalJS:setParaRule];
        setImageRule = [NSString stringWithFormat:@"addCSSRule('img', 'width:%fpx; height:%fpx;')", webWidth, webHeight - 15];
        [webView evalJS:setImageRule];
    }
    
    int totalWidth = [webView scrollWidth];
    
    float theFloat = (float)(totalWidth / webWidth);
    int roundedPageCount = lroundf(theFloat);
    
    pagesInCurrentSpineCount = roundedPageCount;
    
    if (IS_LANDSCAPE) {
        float splittedPage = 0;
        if (!self.loadedEpub.isFixedLayout) {            
            pagesInCurrentSpineCount = (int)((float)totalWidth / (webWidth + 175));
        }
        splittedPage = (float)pagesInCurrentSpineCount / 2;
        pagesInCurrentSpineCount = lroundf(splittedPage);
    }
    
    if (!self.loadedEpub.isFixedLayout) {
        [webView.scrollView setContentSize:CGSizeMake(totalWidth + offsetPadding, webHeight)];
    }
    
    if (loadLastChapter) {
        [self gotoPageInCurrentSpine:totalWidth - 1];
        loadLastChapter = NO;
    }
    else
    {
        hashTag = [Utils getHash];
        if (hashTag) {
            int pageIndex = [self getHashPage:hashTag];
            [Utils clearHash];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            int spineIndex = [[userDefaults valueForKey:@"SPINE_INDEX"] intValue];
            [self loadSpine:spineIndex atPageIndex:pageIndex highlightSearchResult:nil];
            return;
        }
        
        [self gotoPageInCurrentSpine:currentPageInSpineIndex];
    }
	    
    [SVProgressHUD dismiss];
    
    // main view
    [self updateMainViewLayout];
}

- (void) _resetSavedAnswer: (NSString *) exerId {
    if (bookUUIDEmpty) return;
    [[VibeDataManager sharedInstance] removeCloudAnswer:[VSmartHelpers getEmail] forBook:self.loadedEpub.uuid forExerciseId:exerId];
}

- (void) _loadExerciseData {
    
    NSString *jsonString = [VSmartHelpers getAssessmentsAsJSONforExerJS:self.loadedEpub.uuid optionUnlock:self.loadedEpub.isUnlock];
    
    if ([[VSmart sharedInstance].account.user.position isEqualToString:kModeIsTeacher]) {
        [webView applyExerciseSecurityForJson:jsonString isTeacher:YES];
    } else {
        [webView applyExerciseSecurityForJson:jsonString isTeacher:NO];
    }
}

- (void) _loadSavedAnswers {
    if (bookUUIDEmpty) return;
    
    NSString *chapterFile = [self.currentChapter.spinePath lastPathComponent];
    NSArray *answers = [[VibeDataManager sharedInstance] fetchCloudAnswer:[VSmartHelpers getEmail]
                                                                  forBook:self.loadedEpub.uuid
                                                            inChapterFile: chapterFile];
    
    if (answers) {
        NSMutableArray *jsonData = [[NSMutableArray alloc] init];
        
        // compose JSON
        for (CloudAnswer *answer in answers) {
            
            NSNumber *itemNo = answer.item_no;
            NSNumber *exerType = answer.exer_type;
            NSString *exerId = answer.exer_id;
            NSString *actualAnswer = answer.actual_answer;
            NSString *answerStatus = answer.answer_status;
            NSString *correctAnswer = answer.correct_answer;
            
            NSDictionary *item = @{kDBCloudAnswerExerId: exerId, kDBCloudAnswerExerType: exerType, kDBCloudAnswerActualAnswer: actualAnswer, kDBCloudAnswerAnswerStatus: answerStatus, kDBCloudAnswerCorrectAnswer: correctAnswer, kDBCloudAnswerItemNo:itemNo};
            
            [jsonData addObject:item];
        }
        
        NSString *jsonString = [jsonData JSONString];
        //VLog(@"jsonData: %@", [jsonData JSONString]);
        [webView applyExerciseAnswersForJson:jsonString];
    }
}

- (void) updateMainViewLayout {
    self.view.backgroundColor = self.mainColor;
    self.webView.backgroundColor = self.mainColor;
    self.bookChapterLabel.textColor = self.mainTextColor;
}

- (void) hideWebViewGradient {
    for(UIView *view in webView.subviews){
        if ([view isKindOfClass:[UIImageView class]]) {
            // to transparent
            [view removeFromSuperview];
        }
        if ([view isKindOfClass:[UIScrollView class]]) {
            UIScrollView *sView = (UIScrollView *)view;
            //to hide Scroller bar
            sView.showsVerticalScrollIndicator = NO;
            sView.showsHorizontalScrollIndicator = NO;
            for (UIView* shadowView in [sView subviews]){
                //to remove shadow
                if ([shadowView isKindOfClass:[UIImageView class]]) {
                    [shadowView setHidden:TRUE];
                }
            }
        }
    }
}

- (void) loadBibleView:(NSString *) html withBaseURL:(NSURL *)baseURL {
    BibleInfoViewController *vc = [[BibleInfoViewController alloc] initWithNibName:@"BibleInfoViewController" bundle:nil];
    vc.htmlText = html;
    vc.baseURL = baseURL;
    vc.delegate = self;
    [self clearSelection];
    
    [self presentPopupViewController:vc animationType:PopupViewAnimationFade];
}

- (void) loadEncyclopediaView:(NSString *) html withBaseURL:(NSURL *)baseURL {
    [self clearSelection];
    
    EncyclopediaViewController *modalPanel = [[[EncyclopediaViewController alloc] initWithFrame:self.view.bounds
                                                                                          title:@"Encyclopedia"
                                                                                andHtmlString:html withBaseURL:baseURL] autorelease];
    modalPanel.contentColor = [UIColor whiteColor];
    
    modalPanel.onClosePressed = ^(UAModalPanel* panel) {
        [panel hideWithOnComplete:^(BOOL finished) {
            [panel removeFromSuperview];
        }];
    };
    
    ///////////////////////////////////
    // Add the panel to our view
    [self.view addSubview:modalPanel];
    
    ///////////////////////////////////
    // Show the panel from the center of the button that was pressed
    [modalPanel showFromPoint:currentPoint];
}

#pragma mark - Annotation Module
-(void)showDrawControls:(BOOL)visible{
    
    redColorButton.hidden = !visible;
    blueColorButton.hidden = !visible;
    greenColorButton.hidden = !visible;
    //colorRandomColorButton.hidden = !visible;
    
    btnDrawStyleHighlight.hidden = !visible;
    btnDrawStylePencil.hidden = !visible;
    btnResetDraw.hidden = !visible;
    btnUndoDraw.hidden = !visible;
    //btnAnnotate.hidden = visible;
    currentTitleLabel.hidden = visible;
    bookmarkButton.hidden = visible;
    fontListButton.hidden = visible;
    tocShowButton.hidden = visible;
    searchButton.hidden = visible;
}

-(void) changeDrawingStroke: (BOOL) toPencil
{
    if (toPencil) {
        btnDrawStylePencil.selected = YES;
        btnDrawStyleHighlight.selected = NO;
        
        annotator.alpha = 1.0;
        annotator.lineWidth = 5.0;
        annotator.lineCap = kCGLineCapRound;
    }
    else {
        btnDrawStyleHighlight.selected = YES;
        btnDrawStylePencil.selected = NO;
        
        annotator.alpha = 0.1;
        annotator.lineWidth = 25.0;
        annotator.lineCap = kCGLineCapSquare;
    }
}

- (IBAction) changeDrawingOption:(UIButton *)trigger
{
    int tag = (int)((UIButton *)trigger).tag;
    
    switch (tag) {
        case 0:
            [annotator clearDrawImageArea];
            [annotator removeDraw:_currentBookname page:currentPageInSpineIndex spine:currentSpineIndex textSize:currentTextSize];
            break;
        case 1:
            [annotator undoDrawTemp];
            
            if (btnDrawStyleHighlight.selected)
                [self changeDrawingStroke:NO];
            else
                [self changeDrawingStroke:YES];
            break;
        case 2:
            [self changeDrawingStroke:YES];
            break;
        case 3:
            [self changeDrawingStroke:NO];
            break;
    }
}

- (IBAction) changeDrawingColor:(UIButton *)trigger
{
    int tag = (int)((UIButton *)trigger).tag;
    
    if (tag == 0) {
        self.annotator.red = 0.0;
        self.annotator.green = 0.0;
        self.annotator.blue = 1.0;
    }
    else if (tag == 1) {
        self.annotator.red = 1.0;
        self.annotator.green = 0.0;
        self.annotator.blue = 0.0;
    }
    else if (tag == 2) {
        self.annotator.red = 0.0;
        self.annotator.green = 1.0;
        self.annotator.blue = 0.0;
    }
    else {
        //UIColor *color = [UIColor randomColor];
        UIColor *color = [UIColor randomCSSColor];
        VLog(@"Color: %@", [color stringFromColor]);
        CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha = 0.0;
        
        if ([color respondsToSelector:@selector(getRed:green:blue:alpha:)]) {
            [color getRed:&red green:&green blue:&blue alpha:&alpha];
        } else {
            const CGFloat *components = CGColorGetComponents(color.CGColor);
            red = components[0];
            green = components[1];
            blue = components[2];
            alpha = components[3];
        }
        
        self.annotator.red = red; //(((arc4random() % 4) + 1) * 0.1);
        self.annotator.green = green; //(((arc4random() % 4) + 1) * 0.1);
        self.annotator.blue = blue; //(((arc4random() % 4) + 1) * 0.1);
    }
    
}

- (NSDictionary *)restoreDefaultFontSize {
    
    /*
    dict[@"font_size"] = [NSNumber numberWithInt: fontSize];
    dict[@"book_appearance_index"] = [NSNumber numberWithInt: appearanceIndex];
    dict[@"font_name_index"] = [NSNumber numberWithInt: fontNameIndex];
    dict[@"font_name"] = [[fontOptions objectAtIndex:fontNameIndex] objectForKey:@"style"];
    dict[@"book_appearance_name"] = [themeOptions objectAtIndex:appearanceIndex];
    dict[@"brightness_level"] = [NSNumber numberWithFloat: brightness];
     */
    
    NSDictionary *s = [Utils getBookSetings];
    NSMutableDictionary *settings = [
                                     @{@"font_size":[NSNumber numberWithInt:0],
                                       @"font_name_index":s[@"font_name_index"],
                                       @"font_name":s[@"font_name"],
                                       @"book_appearance_index":s[@"book_appearance_index"],
                                       @"book_appearance_name":s[@"book_appearance_name"],
                                       @"brightness_level":s[@"brightness_level"]} mutableCopy];
    [Utils saveBookSettings:settings];
    
    NSDictionary *values = @{@"font_size": [NSNumber numberWithInt:0],
                             @"font_name": s[@"font_name"],
                             @"theme": s[@"book_appearance_name"],
                             @"brightness_level": s[@"brightness_level"] };
    
    return values;
}

- (void) enableDrawingMode: (BOOL) start
{
    NSString *notsupportedLabel = NSLocalizedString(@"Not Supported", nil);
    NSString *notsupportedMessage = NSLocalizedString(@"Sorry, currently drawing is only supported on default font size.", nil);
    NSString *okLabel = NSLocalizedString(@"OK", nil);
    
    if (start) {
        

        if (self.fontSizeSettings != 100)
        {
//            NSLog(@" <----> %s : %@", __PRETTY_FUNCTION__, @(self.fontSizeSettings) );
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:notsupportedLabel
//                                                                           message:notsupportedMessage
//                                                                    preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction *action = [UIAlertAction actionWithTitle:okLabel style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                [alert dismissViewControllerAnimated:YES completion:nil];
//            }];
//            
//            [alert addAction:action];
//            [self presentViewController:alert animated:YES completion:nil];
//            
//            return;
        }
        
        if (IS_LANDSCAPE)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:notsupportedLabel
                                                                           message:notsupportedMessage
                                                                    preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *action = [UIAlertAction actionWithTitle:okLabel style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];

            [alert addAction:action];
            [self presentViewController:alert animated:YES completion:nil];
            
//            UIAlertView *alert = [[UIAlertView alloc]
//                                  initWithTitle:notsupportedLabel
//                                  message: notsupportedMessage
//                                  delegate: nil
//                                  cancelButtonTitle:okLabel
//                                  otherButtonTitles:nil];
//            [alert show];
//            [alert release];
            
            return;
        }
        
        [btnAnnotate setTag:1];
//        [btnAnnotate setImage:[UIImage imageNamed:@"bookSaveDefault"] forState:UIControlStateNormal];
//        [btnAnnotate setImage:[UIImage imageNamed:@"bookSaveSelected"] forState:UIControlStateSelected];
        [btnAnnotate setImage:[UIImage imageNamed:@"icn_annotation-save"] forState:UIControlStateNormal];
        [btnAnnotate setImage:[UIImage imageNamed:@"icn_annotation-save"] forState:UIControlStateSelected];
        
        self.annotator.isActive = YES;
        [self showDrawControls:YES];
        
        webView.userInteractionEnabled = NO;
        //[pageSlider setEnabled:NO];
        //[chapterSlider setHidden:YES];
        
        [annotator initializeDrawing:_currentBookname page:currentPageInSpineIndex spine:currentSpineIndex textSize:currentTextSize];
        self.annotator.red = 0.0;
        self.annotator.green = 0.0;
        self.annotator.blue = 1.0;
    }
    else {
        [btnAnnotate setTag:0];
//        [btnAnnotate setImage:[UIImage imageNamed:@"bookEditDefault"] forState:UIControlStateNormal];
//        [btnAnnotate setImage:[UIImage imageNamed:@"bookEditSelected"] forState:UIControlStateSelected];
        [btnAnnotate setImage:[UIImage imageNamed:@"icn_freehand-annotation"] forState:UIControlStateNormal];
        [btnAnnotate setImage:[UIImage imageNamed:@"icn_freehand-annotation"] forState:UIControlStateSelected];

        //icn_freehand-annotation.png
        self.annotator.isActive = NO;
        [self showDrawControls:NO];
        
        webView.userInteractionEnabled = YES;
        //[pageSlider setEnabled:YES];
        //[chapterSlider setHidden:YES];
        
        if (![annotator isDrawImageAreaEmpty]){
            [annotator saveDraw:_currentBookname page:currentPageInSpineIndex spine:currentSpineIndex textSize:currentTextSize title:currentTitleLabel.text];
            
            [annotator clearDrawImageArea];
        }
    }
}

- (IBAction) annotatePage:(UIButton *)trigger
{
//    int tag = (int)((UIButton *)trigger).tag;
    
    NSInteger tag = trigger.tag;
    
    NSLog(@"<-----> %s : %@", __PRETTY_FUNCTION__, @(tag) );

    if (currentTextSize != 100) {
        [self updateEpubViewWithSettings:[self restoreDefaultFontSize] ];
    }
    [self enableDrawingMode:tag == 0];
}

#pragma mark - Touch events handlers (this is being used in the annotate)
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //VLog(@"ReaderSheet - touchesBegan");
    if (annotator.isActive)
        [annotator AtouchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    //VLog(@"ReaderSheet - touchesMoved");
    if (annotator.isActive)
        [annotator AtouchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    //VLog(@"ReaderSheet - touchesEnded");
    if (annotator.isActive)
        [annotator AtouchesEnded:touches withEvent:event];
}

#pragma mark -
#pragma mark Memory management

/*
 - (void)didReceiveMemoryWarning {
 // Releases the view if it doesn't have a superview.
 [super didReceiveMemoryWarning];
 
 // Release any cached data, images, etc that aren't in use.
 }
 */

- (void)dealloc {
    self.toolbar = nil;
	self.webView = nil;
	self.chapterListButton = nil;
	self.decTextSizeButton = nil;
	self.incTextSizeButton = nil;
	self.pageSlider = nil;
	self.currentPageLabel = nil;
    self.menuHeader = nil;
    self.menuTOC = nil;
    self.tocTable = nil;
    self.currentTitleLabel = nil;
    self.vibeDRM = nil;
    self.chapters = nil;
    self.currentChapter = nil;
    
    [tapWindow release];
    [vibeDRM release];
    [currentBookmark release];
    [currentChapterForBookmark release];
    [currentPageInSpineLabel release];
    [showBookmarkButton release];
    [showTOCButton release];
    [searchTextViewController release];
    [searchButton release];
    [currentTitleLabel release];
    [tocTable release];
    [menuTOC release];
    [bookmarkButton release];
    [menuHeader release];
	[loadedEpub release];
	[chaptersPopover release];
	[searchResultsPopover release];
	[searchResViewController release];
	[currentSearchResult release];
    
    [slideTOCButton release];
    [slideDrawButton release];
    [slideBookmarkButton release];
    [btnResetDraw release];
    [btnUndoDraw release];
    [btnDrawStylePencil release];
    [btnDrawStyleHighlight release];
    [redColorButton release];
    [greenColorButton release];
    [blueColorButton release];
    [btnAnnotate release];
    [annotator release];
    [tocShowButton release];
    [_bookChapterLabel release];
    [btnBookmarkLarge release];
    
    [segmentedLabel release];
    [bookIdentifier release];
    [drmKey release];
    [chapters release];
    [chaptersForSlider release];
    [currentChapter release];
    
    bookFigure = nil;
    [bookFigure release];
    bookMedia = nil;
    [bookMedia release];
    bookEncyclopedia = nil;
    [bookEncyclopedia release];

    [super dealloc];
}

@end
