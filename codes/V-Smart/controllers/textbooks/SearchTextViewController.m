//
//  SearchTextViewController.m
//
//  Created by Earljon Hidalgo on 3/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchTextViewController.h"
#import "SearchResult.h"
#import "UIWebView+Private.h"

@interface SearchTextViewController()

- (void) searchString:(NSString *)query inChapterAtIndex:(int)index;

@property (nonatomic, strong) UIWebView *theWebView;

@end

@implementation SearchTextViewController
@synthesize listContent, filteredListContent, savedSearchTerm, savedScopeButtonIndex, searchWasActive;
@synthesize epubViewController, currentQuery, results;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //VLog(@"SearchTextInitNib");
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.edgesForExtendedLayout = UIRectEdgeNone;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    //self.results = [[NSMutableArray alloc] init];
    self.searchDisplayController.delegate = self;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    CGRect newBounds = self.tableView.bounds;
//    newBounds.origin.y = newBounds.origin.y + self.searchDisplayController.searchBar.bounds.size.height;
//    self.searchDisplayController.searchResultsTableView.bounds = newBounds;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }

    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    SearchResult* hit = (SearchResult*)[results objectAtIndex:[indexPath row]];
    cell.textLabel.text = [NSString stringWithFormat:@"...%@...", hit.neighboringText];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Loc %d - page %d", hit.chapterIndex, hit.pageIndex+1];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //VLog(@"results: %i", [results count]);
    //return [results count];
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [self.results count];
    }
    else
    {
        return 0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SearchResult* hit = (SearchResult*)[results objectAtIndex:[indexPath row]];
    [epubViewController loadSpine:hit.chapterIndex atPageIndex:hit.pageIndex highlightSearchResult:hit];
}

- (void) searchString:(NSString *)query inChapterAtIndex:(int)index{
    
    //VLog(@"searchString");
    currentChapterIndex = index;
    
    Chapter *chapter = (Chapter *)[epubViewController.loadedEpub.spineArray objectAtIndex:index];
    //VLog(@"Text: %@", [chapter text]);
    
    NSRange range = NSMakeRange(0, chapter.text.length);
    range = [chapter.text rangeOfString:query options:NSCaseInsensitiveSearch range:range locale:nil];
    int hitCount=0;
    while (range.length != 0) {
        range = NSMakeRange(range.location+range.length, chapter.text.length-(range.location+range.length));
        range = [chapter.text rangeOfString:query options:NSCaseInsensitiveSearch range:range locale:nil];
        if (range.location != NSNotFound) {
        hitCount++;
    }
    }
    
    if(hitCount!=0){
        VLog("Found Query: %@ in SpinePath: %@", query, chapter.spinePath);
        self.theWebView = [[UIWebView alloc] initWithFrame:chapter.windowSize];
        [self.theWebView setDelegate:self];
        [self.hiddenView addSubview:self.theWebView];
        
        NSURLRequest* urlRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:chapter.spinePath]];
        [self.theWebView loadRequest:urlRequest];
    } else {
        if((currentChapterIndex+1)<[epubViewController.loadedEpub.spineArray count]){
            //VLog(@"self searchString:currentQuery");
            [self searchString:currentQuery inChapterAtIndex:(currentChapterIndex+1)];
        } else {
            epubViewController.searching = NO;
        }
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    VLog(@"%@", error);
	[webView release];
}

- (void) webViewDidFinishLoad:(UIWebView*)webView{
    VLog(@"webViewDidFinishLoad");
    
//    NSString *varMySheet = @"var mySheet = document.styleSheets[0];";
//	
//	NSString *addCSSRule =  @"function addCSSRule(selector, newRule) {"
//	"if (mySheet.addRule) {"
//    "mySheet.addRule(selector, newRule);"								// For Internet Explorer
//	"} else {"
//    "ruleIndex = mySheet.cssRules.length;"
//    "mySheet.insertRule(selector + '{' + newRule + ';}', ruleIndex);"   // For Firefox, Chrome, etc.
//    "}"
//	"}";
	
    //    VLog(@"w:%f h:%f", webView.bounds.size.width, webView.bounds.size.height);
	
	NSString *insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'padding: 10px; height: %fpx; -webkit-column-gap: 20px; -webkit-column-width: %fpx; -webkit-column-fill: balance')", webView.frame.size.height, webView.frame.size.width];
	NSString *insertRule2 = [NSString stringWithFormat:@"addCSSRule('p', 'text-align: justify;')"];
	NSString *setTextSizeRule = [NSString stringWithFormat:@"addCSSRule('body', '-webkit-text-size-adjust: %d%%;')",[[epubViewController.loadedEpub.spineArray objectAtIndex:currentChapterIndex] fontPercentSize]];
    NSString *setImageRule = [NSString stringWithFormat:@"addCSSRule('img', 'max-width: %fpx; height:auto;')", webView.bounds.size.width *0.75];
	
    [webView attachJS:@"jstools"];
	//[webView stringByEvaluatingJavaScriptFromString:varMySheet];
	//[webView stringByEvaluatingJavaScriptFromString:addCSSRule];
	[webView evalJS:insertRule1];
	[webView evalJS:insertRule2];
    [webView evalJS:setTextSizeRule];
    [webView evalJS:setImageRule];
    
    [webView highlightAllOccurencesOfString:currentQuery];
    
    NSString* foundHits = [webView stringByEvaluatingJavaScriptFromString:@"results"];
    
    VLog(@"Hits: %@", foundHits);
    
    NSMutableArray* objects = [[NSMutableArray alloc] init];
    
    NSArray* stringObjects = [foundHits componentsSeparatedByString:@";"];
    for(int i = 0; i < [stringObjects count]; i++){
        NSArray* strObj = [[stringObjects objectAtIndex:i] componentsSeparatedByString:@","];
        if([strObj count] == 3){
            [objects addObject:strObj];   
        }
    }
    
    NSArray* orderedRes = [objects sortedArrayUsingComparator:^(id obj1, id obj2){
        int x1 = [[obj1 objectAtIndex:0] intValue];
        int x2 = [[obj2 objectAtIndex:0] intValue];
        int y1 = [[obj1 objectAtIndex:1] intValue];
        int y2 = [[obj2 objectAtIndex:1] intValue];
        if(y1<y2){
            return NSOrderedAscending;
        } else if(y1>y2){
            return NSOrderedDescending;
        } else {
            if(x1<x2){
                return NSOrderedAscending;
            } else if (x1>x2){
                return NSOrderedDescending;
            } else {
                return NSOrderedSame;
            }
        }
    }];
    
    [objects release];
    
    for(int i=0; i<[orderedRes count]; i++){
        NSArray* currObj = [orderedRes objectAtIndex:i];
        
        SearchResult* searchRes = [[SearchResult alloc] initWithChapterIndex:currentChapterIndex pageIndex:([[currObj objectAtIndex:1] intValue]/webView.bounds.size.height) hitIndex:0 neighboringText:[webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"unescape('%@')", [currObj objectAtIndex:2]]] originatingQuery:currentQuery];
        [results addObject:searchRes];
        //VLog(@"Result Count So Far: %i", [results count]);
		[searchRes release];
    }
    
    //[webView dealloc];
    
    //[self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    [self.searchDisplayController.searchResultsTableView performSelectorOnMainThread:@selector(reloadData)
                                                                          withObject:nil waitUntilDone:NO];
    
    if((currentChapterIndex+1)<[epubViewController.loadedEpub.spineArray count]){
        [self searchString:currentQuery inChapterAtIndex:(currentChapterIndex+1)];
    } else {
        epubViewController.searching = NO;
    }
    [self.searchDisplayController.searchResultsTableView reloadData];
    
    self.theWebView = nil;
}

- (BOOL)webView:(UIWebView *) theWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

    NSString *filePath = [[request URL] path];
    
    if ([[filePath pathExtension] isIn:@"xhtml", @"html", @"htm", nil] && [[[request URL] scheme] isEqualToString:@"file"]) {
        NSLog(@"Chapter: shouldStartLoadWithRequest: filePath: %@", filePath);
        
        NSData *spineData = [NSData dataWithContentsOfFile:filePath];
        
        if ([self.epubViewController isBookSecured:filePath]) {
            spineData = [self.epubViewController.vibeDRM decrypt:[NSData dataWithContentsOfFile:filePath]];
        }
        
        NSURL *baseURL = [NSURL fileURLWithPath:[filePath stringByDeletingLastPathComponent] isDirectory:YES];
        NSString* html = [[[NSString alloc] initWithData:spineData encoding:NSUTF8StringEncoding] autorelease];
        //VLog(@"baseURL: %@", html);
        html = [html stringByReplacingOccurrencesOfString:@"<title></title>" withString:@"<title>Untitled</title>"];
        html = [html stringByReplacingOccurrencesOfString:@"<title/>" withString:@"<title>Untitled</title>"];
        html = [html stringByReplacingOccurrencesOfString:@"<title />" withString:@"<title>Untitled</title>"];
        
        [theWebView loadHTMLString:html baseURL:baseURL];
        
        return NO;
    }
    
    return YES;
}

- (void) searchString:(NSString*)query{
    self.results = [[NSMutableArray alloc] init];
//    [self.tableView reloadData];
//    [self.searchDisplayController.searchResultsTableView reloadData];
    self.currentQuery = query;
    VLog(@"Search String: %@", query);
    [self searchString:query inChapterAtIndex:0];    
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    VLog(@"Searching for %@", [searchBar text]);
    //[self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [self searchString:[searchBar text]];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    // Return YES to cause the search result table view to be reloaded.
    //[self searchString:searchString];
    return YES;
}


//- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
//{
////    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
////     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
//    [self searchString:[self.searchDisplayController.searchBar text]];
//    // Return YES to cause the search result table view to be reloaded.
//    return YES;
//}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (void)dealloc
{
	[results release];
	[currentQuery release];
    
    [super dealloc];
}


@end
