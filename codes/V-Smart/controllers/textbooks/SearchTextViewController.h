//
//  SearchTextViewController.h
//
//  Created by Earljon Hidalgo on 3/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPubViewController.h"

@interface SearchTextViewController : UITableViewController <UISearchDisplayDelegate, UISearchBarDelegate, UIWebViewDelegate>
{
	NSArray			*listContent;			// The master content.
	NSMutableArray	*filteredListContent;	// The content filtered as a result of a search.
	
	// The saved state of the search UI if a memory warning removed the view.
    NSString		*savedSearchTerm;
    NSInteger		savedScopeButtonIndex;
    BOOL			searchWasActive;
    
    NSMutableArray* results;
    EPubViewController* epubViewController;
    
    int currentChapterIndex;
    NSString* currentQuery;    
}

@property (nonatomic, retain) NSArray *listContent;
@property (nonatomic, retain) NSMutableArray *filteredListContent;

@property (nonatomic, copy) NSString *savedSearchTerm;
@property (nonatomic) NSInteger savedScopeButtonIndex;
@property (nonatomic) BOOL searchWasActive;

@property (nonatomic, assign) EPubViewController* epubViewController;
@property (nonatomic, retain) NSMutableArray* results;
@property (nonatomic, retain) NSString* currentQuery;

@property (nonatomic, retain) IBOutlet UIView *hiddenView;

@end
