//
//  BibleInfoViewController.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/12/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import "BibleInfoViewController.h"

@interface BibleInfoViewController ()

@end

@implementation BibleInfoViewController
@synthesize theWebView, htmlText, delegate, baseURL;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)close:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didFinishedLoadingBibleLink:)]) {
        [self.delegate didFinishedLoadingBibleLink:self];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.theWebView setDelegate:self];
    [self.theWebView loadHTMLString:self.htmlText baseURL:self.baseURL];
    
    for (id subview in self.theWebView.subviews)
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            ((UIScrollView *)subview).bounces = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [baseURL release];
    [htmlText release];
    self.theWebView.delegate = nil;
    self.theWebView = nil;
    [theWebView release];
    [super dealloc];
}

#pragma mark - UIWebView Delegate
- (void)webViewDidFinishLoad:(UIWebView *)theWebView
{
    VLog(@"Finished Loading Encyclopedia");
}

- (void)webView:(UIWebView *)theWebView didFailLoadWithError:(NSError *)error
{
	VLog(@"Error: %@", [error localizedDescription]);
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
	if ([[[request URL] scheme] isEqualToString:@"http"] || [[[request URL] scheme] isEqualToString:@"https"]){
		[[UIApplication sharedApplication] openURL:[request URL]];
		return NO;
	}
    
    return YES;
}

@end
