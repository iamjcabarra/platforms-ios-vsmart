//
//  ZoomImageViewController.m
//  Booklatan
//
//  Created by Earljon Hidalgo on 5/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZoomImageViewController.h"

//#define BLACK_BAR_COMPONENTS				{ 0.22, 0.22, 0.22, 1.0, 0.07, 0.07, 0.07, 1.0 }
#define BLACK_BAR_COMPONENTS				{ 0.72, 0.72, 0.72, 1.0, 0.07, 0.07, 0.07, 1.0 }
@interface ZoomImageViewController ()

@end

@implementation ZoomImageViewController
@synthesize baseURL, filePath, urlRequest;
@synthesize isFixedLayout;

#pragma mark - View lifecycle

- (id)initWithFrame:(CGRect)frame title:(NSString *)title andImagePath:(NSString *) imagePath {
	if ((self = [super initWithFrame:frame])) {
		
		//CGFloat colors[8] = BLACK_BAR_COMPONENTS;
		//[self.titleBar setColorComponents:colors];
		self.headerLabel.text = title;
        
			// The background color gradient of the title
//        CGFloat colors[8] = {
//            (arc4random() % 2), (arc4random() % 2), (arc4random() % 2), 1,
//            (arc4random() % 2), (arc4random() % 2), (arc4random() % 2), 1
//        };
        //[[self titleBar] setColorComponents:colors];
        //[self.titleBar setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_slide"]]];
        // The gradient style (Linear, linear reversed, radial, radial reversed, center highlight). Default = UAGradientBackgroundStyleLinear
        //[[self titleBar] setGradientStyle:(arc4random() % 5)];
        
//        self.contentColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"img_login_bg"]];
        self.contentColor = [UIColor whiteColor];
        // The line mode of the gradient view (top, bottom, both, none). Top is a white line, bottom is a black line.
        //[[self titleBar] setLineMode: pow(2, (arc4random() % 3))];
//        [[self titleBar] setLineMode:UAGradientBackgroundStyleLinearReversed];
        // The noise layer opacity. Default = 0.4
//        [[self titleBar] setNoiseOpacity:(((arc4random() % 10) + 1) * 0.1)];
        
        // The header label, a UILabel with the same frame as the titleBar
        [self headerLabel].font = [UIFont boldSystemFontOfSize:floor(self.titleBarHeight / 2.0)];
        
		UIWebView *wv = [[[UIWebView alloc] initWithFrame:CGRectZero] autorelease];
        wv.delegate = self;
        [wv setScalesPageToFit:YES];
        
        //VLog(@"EncyclopediaViewController: FILEPATH: %@", self.filePath);
        NSString *htmlString = [NSString stringWithFormat:@"<html><body><div align='center'><img src='%@'/></div></body></html>", imagePath];
        //NSURLRequest *currRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:htmlPath]];

        //NSData *htmlData = [NSData dataWithContentsOfFile:self.filePath];
        //NSString *currData = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
        NSURL *theBaseURL = [NSURL fileURLWithPath:[imagePath stringByDeletingLastPathComponent] isDirectory:YES];
        [wv loadHTMLString:htmlString baseURL:theBaseURL];

        for (id subview in wv.subviews)
            if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            {
                ((UIScrollView *)subview).bounces = NO;
                //[((UIScrollView *)subview) setZoomScale:10.5f animated:YES];
            }                
        
		v = [wv retain];
		[self.contentView addSubview:v];
		
	}	
	return self;
}

- (void)dealloc {
    [filePath release];
    [baseURL release];
    [urlRequest release];
    [v release];
    [super dealloc];
}

- (void)layoutSubviews {
	[super layoutSubviews];	
	[v setFrame:self.contentView.bounds];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - UIWebView Delegate
- (void)webViewDidFinishLoad:(UIWebView *)theWebView
{
    VLog(@"Finished Loading Zoom Image");
    if (!self.isFixedLayout) {
        [theWebView stringByEvaluatingJavaScriptFromString:@"document.body.style.zoom = 2.0;"];
    }
    
}

- (void)webView:(UIWebView *)theWebView didFailLoadWithError:(NSError *)error 
{
	VLog(@"Error: %@", [error localizedDescription]);
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    webView.scalesPageToFit = YES;
	if ([[[request URL] scheme] isEqualToString:@"http"] || [[[request URL] scheme] isEqualToString:@"https"]){
		[[UIApplication sharedApplication] openURL:[request URL]];
		return NO;
	}
    
    return YES;
}


@end
