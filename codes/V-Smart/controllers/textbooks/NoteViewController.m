//
//  NoteViewController.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/10/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "NoteViewController.h"

@interface NoteViewController ()

@end

@implementation NoteViewController
@synthesize saveButton, trashButton;
@synthesize highlightId, noteText, bookData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self loadNoteData];
}

- (void) loadNoteData {
    if (self.highlightId != 0) {
        NSString *chapterFile = [self.bookData objectForKey:@"chapterFile"];
        NSString *email = [self.bookData objectForKey:@"email"];
        NSString *uuid = [self.bookData objectForKey:@"uuid"];
        
        VLog(@"BookData: %@", self.bookData);
        CloudHighlight *cloud = [[VibeDataManager sharedInstance] fetchCloudHighlightById:email withUUID:uuid forChapterFile:chapterFile andHighlightId:self.highlightId];
        
        VLog(@"Note: %@", cloud.note);
        self.noteText.text = cloud.note;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)buttonAction:(id)sender {
    VLog(@"ID: %d", self.highlightId);
    NSString *action = @"";
    
    if ([sender isEqual:saveButton]) {
        VLog(@"saveButton");
        action = @"SAVE";
    } else {
        VLog(@"trashButton");
        action = @"DELETE";
    }
    NSDictionary *data = @{@"action": action, @"id": [NSNumber numberWithInt:self.highlightId], @"text": self.noteText.text};
    [[NSNotificationCenter defaultCenter] postNotificationName:kNoteDataNotification object:data];
}
@end
