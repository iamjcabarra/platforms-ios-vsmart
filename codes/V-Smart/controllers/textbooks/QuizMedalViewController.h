//
//  QuizMedalViewController.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 10/17/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TapDetectingWindow.h"

@protocol QuizMedalViewControllerDelegate;

@interface QuizMedalViewController : UIViewController
@property (nonatomic, assign) id<QuizMedalViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *bookTitle;
@property (weak, nonatomic) IBOutlet UITableView *tvwResult;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (nonatomic, strong) NSArray *items;

-(IBAction)close:(id)sender;
@end

@protocol QuizMedalViewControllerDelegate <NSObject>

@optional
-(void)didCloseQuizMedalWindow:(QuizMedalViewController *) popViewController;

@end