//
//  EncyclopediaViewController.h
//  Booklatan
//
//  Created by Earljon Hidalgo on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UATitledModalPanel.h"
//#import "VibeCrypto.h"
#import "VibeDRM.h"

@interface EncyclopediaViewController : UATitledModalPanel <UIWebViewDelegate>
{
    UIView			*v;
}

@property (nonatomic, retain) NSString *filePath;
@property (nonatomic, retain) NSString *fileUrl;
@property (nonatomic, retain) NSURL *baseURL;
@property (nonatomic, retain) NSURLRequest *urlRequest;
//@property (nonatomic, retain) VibeCrypto *vibeDRM;
@property (nonatomic, retain) VibeDRM *vibeDRM;
@property (nonatomic, retain) UIWebView *webView;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title andFilePath:(NSString *) htmlPath andUrl:(NSString *) url;
- (id)initWithFrame:(CGRect)frame title:(NSString *)title andHtmlString:(NSString *) html withBaseURL:(NSURL *)theBaseURL;
@end
