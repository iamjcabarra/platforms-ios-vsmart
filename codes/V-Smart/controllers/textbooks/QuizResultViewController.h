//
//  QuizResultViewController.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 8/1/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuizResultViewControllerDelegate;

@interface QuizResultViewController : UIViewController
@property (nonatomic, assign) id<QuizResultViewControllerDelegate> delegate;
@property (nonatomic, retain) IBOutlet UILabel *displayResult;

@property (retain, nonatomic) NSString *result;
@property (retain, nonatomic) NSDictionary *assessmentData;
@property (retain, nonatomic) IBOutlet UILabel *blackBackgroundLabelBottom;
@property (retain, nonatomic) IBOutlet UILabel *blackBackgroundLabelTop;
@property (retain, nonatomic) IBOutlet UIButton *closeWindowButton;
@property (retain, nonatomic) IBOutlet UILabel *headerLabel;

- (IBAction)closeWindow:(id)sender;
@end

@protocol QuizResultViewControllerDelegate <NSObject>

@optional
- (void)didCloseQuizResultWindow:(QuizResultViewController *) popViewController;

@end