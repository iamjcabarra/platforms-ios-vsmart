//
//  SettingsViewController.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 7/30/13.
//  Copyright (c) 2013 Vibe Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SettingsViewControllerDelegate;

@interface SettingsViewController : UITableViewController
@property (nonatomic, assign) id<SettingsViewControllerDelegate> delegate;
@end

@protocol SettingsViewControllerDelegate <NSObject>
- (void) didFinishChangeSettings:(SettingsViewController *) controller withValues:(NSDictionary *) values;
@end