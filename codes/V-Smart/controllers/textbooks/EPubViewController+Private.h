//
//  EPubViewController+Private.h
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/29/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "EPubViewController.h"

@interface EPubViewController (Private)

- (void) _saveHighlightToDb: (NSDictionary *) json;
- (void) _removeHighlightToDb: (NSArray *) highlights;
- (void) _removeHighlightToDbById: (int) highlightId;
- (void) _updateHighlightToDb: (CloudHighlight *) cloudHighlight;
- (void) _updateHighlightToDb: (int) highlightId withNote:(NSString *) note;
- (NSString *) _extractHighlightNotes: (NSArray *) highlights;
- (CloudHighlight *) _fetchCloudHighlightById: (int) highlightId;
- (void) _loadHighlights;
- (void) restoreHighlight: (NSString *) range;
- (void) revertSelectedHighlight;
@end
