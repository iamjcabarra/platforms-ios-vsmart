//
//  DetailViewController.h

#import <UIKit/UIKit.h>
#import "ZipArchive.h"
#import "EPub.h"
#import "Chapter.h"
#import "Annotate.h"
#import "TapDetectingWindow.h"
#import "ELCSlider.h"
#import "BibleInfoViewController.h"
#import "SettingsViewController.h"
#import "QuizResultViewController.h"
#import "NoteViewController.h"
#import "CloudModel.h"
#import "VibeDRM.h"
#import "AssessmentUnlockViewController.h"
#import "AssessmentListViewController.h"

@class SearchResultsViewController;
@class SearchTextViewController;
@class SearchResult;

typedef enum {
    UITableOfContent,
	UIBookmark,
	UIAnnotate,
    UIHighlight,
    UIExercise,
    UIFigure,
    UIEncyclopedia,
    UIMedia
} UISideBarMenu;

typedef enum {
    UIHighlightActionAdd,
	UIHighlightActionUpdate,
	UIHighlightActionDelete
} UIHighlightAction;

@interface EPubViewController : UIViewController <TapDetectingWindowDelegate, UIWebViewDelegate, UIGestureRecognizerDelegate, ChapterDelegate, UISearchBarDelegate, UIPopoverControllerDelegate, ELCSliderDelegate, BibleInfoViewControllerDelegate, SettingsViewControllerDelegate, QuizResultViewControllerDelegate> {
    UIToolbar *toolbar;
	UIWebView *webView;
    
    UIBarButtonItem* chapterListButton;
	
	UIBarButtonItem* decTextSizeButton;
	UIBarButtonItem* incTextSizeButton;
    
    UISlider* pageSlider;
    UILabel* currentPageLabel;
    
    NSTimer *timer;
    
	EPub* loadedEpub;
	int currentSpineIndex;
	int currentPageInSpineIndex;
	int pagesInCurrentSpineCount;
	int currentTextSize;
	int totalPagesCount;
    int sideBarSelectedType;
    
    int currentSelectionHighlightCount;
    int currentSelectionNoteCount;
    int currentSelectionNoteId;
    int currentHighlightIdFromJS;
    
    int currentSelectedSegmentIndex;
    
    float brightnessLevel;
    float offsetPadding;
    
    //NSString *currentBookname;
    NSString *hashTag;
    
    BOOL isCoverPage;
    BOOL epubLoaded;
    BOOL paginating;
    BOOL searching;
    BOOL isTOC;
    BOOL loadChapterContent;
    BOOL loadLastChapter;
    BOOL isLastChapter;
    BOOL isLastPageOfChapter;
    BOOL jQueryScriptExists;
    BOOL exerciseFileExists;
    BOOL bookUUIDEmpty;
    
    CGPoint currentPoint;
    
    NSArray *currentBookmark;
    NSDictionary *currentChapterForBookmark;
    
    UIPopoverController* chaptersPopover;
    UIPopoverController* searchResultsPopover;
    UIPopoverController* searchTextPopover;
    UIPopoverController *fontPopover;
    UIPopoverController *notePopover;
    
    SearchResultsViewController* searchResViewController;
    SearchResult* currentSearchResult;
    SearchTextViewController* searchTextViewController;
    SettingsViewController *settingsView;
    
    TapDetectingWindow *tapWindow;
}

- (IBAction) showChapterIndex:(id)sender;
- (IBAction) fontSizeClicked:(id)sender;
- (IBAction) increaseTextSizeClicked:(id)sender;
- (IBAction) decreaseTextSizeClicked:(id)sender;
- (IBAction) slidingStarted:(id)sender;
- (IBAction) slidingEnded:(id)sender;
- (IBAction) doneClicked:(id)sender;
- (IBAction) bookmarkCliked:(id)sender;
- (IBAction) titleCliked:(id)sender;
- (IBAction) closeTOC:(UIButton *) trigger;
- (IBAction) searchClicked:(id)sender;
- (IBAction) backToLibrary:(UIButton *)trigger;
- (IBAction) changeActionMode:(UIButton *) trigger;
- (IBAction) annotatePage:(UIButton *)trigger;
- (IBAction) changeDrawingColor:(UIButton *)trigger;
- (IBAction) changeDrawingOption:(UIButton *)trigger;
//- (IBAction) changeDrawingOption:(UIButton *)trigger;
- (IBAction) bookmarkLargeAction:(id)sender;

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex highlightSearchResult:(SearchResult*)theResult;

//- (void) loadEpub:(NSURL*) epubURL;
- (void) loadEpubBook:(NSString*) epubName;
- (id) initWithEpub:(NSString *) epub;
- (void) changeFontSize:(int)fontSize;
- (BOOL) isBookSecured: (NSString *) filePath;

@property (nonatomic, retain) NSDictionary *currentChapterForBookmark;
@property (nonatomic, retain) NSArray *currentBookmark;
@property (nonatomic, retain) NSArray *bookHighlights;
@property (nonatomic, retain) NSArray *bookExercises;
@property (nonatomic, retain) NSArray *bookEncyclopediaEntries;
@property (nonatomic, retain) NSArray *bookFigureEntries;
@property (nonatomic, retain) NSArray *bookMediaEntries;
@property (nonatomic, retain) EPub* loadedEpub;

@property (nonatomic, retain) SearchResult* currentSearchResult;

@property (nonatomic, retain) IBOutlet UITableView *tocTable;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;
@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *chapterListButton;

@property (nonatomic, retain) IBOutlet UIBarButtonItem *decTextSizeButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *incTextSizeButton;

@property (nonatomic, retain) IBOutlet UISlider *pageSlider;
@property (nonatomic, retain) IBOutlet ELCSlider *chapterSlider;
@property (nonatomic, retain) IBOutlet UILabel *currentPageLabel;
@property (nonatomic, retain) IBOutlet UILabel *currentTitleLabel;
@property (nonatomic, retain) IBOutlet UILabel *currentPageInSpineLabel;

@property (nonatomic, retain) IBOutlet UIView *menuHeader;
@property (nonatomic, retain) IBOutlet UIView *menuTOC;
@property (nonatomic, retain) IBOutlet UIButton *bookmarkButton;
@property (nonatomic, retain) IBOutlet UIButton *searchButton;
@property (nonatomic, retain) IBOutlet UIButton *showTOCButton;
@property (nonatomic, retain) IBOutlet UIButton *showBookmarkButton;
@property (nonatomic, retain) IBOutlet UIButton *showAnnotateButton;
@property (nonatomic, retain) IBOutlet UIButton *fontListButton;
@property (nonatomic, retain) IBOutlet UIButton *tocShowButton;

//@property (nonatomic, retain) VibeCrypto *vibeDRM;
@property (nonatomic, retain) VibeDRM *vibeDRM;
@property (nonatomic, retain) CloudHighlight *selectedCloudHighlight;
@property (nonatomic, retain) CloudBookExercise *selectedCloudBookExercise;
@property (nonatomic, retain) CloudBookEncyclopedia *selectedCloudBookEncyclopedia;
@property (nonatomic, retain) CloudBookFigure *selectedCloudBookFigure;
@property (nonatomic, retain) CloudBookMedia *selectedCloudBookMedia;

@property BOOL searching;
@property BOOL paginating;

@property (nonatomic, retain) IBOutlet UIButton *slideBookmarkButton;
@property (nonatomic, retain) IBOutlet UIButton *slideTOCButton;
@property (nonatomic, retain) IBOutlet UIButton *slideDrawButton;

// Color buttons
// blue, green, red, brown, orange, pink, indigo, grey, yellow, white
@property (nonatomic, retain) IBOutlet UIButton *colorBlueButton;
@property (nonatomic, retain) IBOutlet UIButton *colorGreenButton;
@property (nonatomic, retain) IBOutlet UIButton *colorRedButton;
@property (nonatomic, retain) IBOutlet UIButton *colorBrownButton;
@property (nonatomic, retain) IBOutlet UIButton *colorOrangeButton;
@property (nonatomic, retain) IBOutlet UIButton *colorPinkButton;
@property (nonatomic, retain) IBOutlet UIButton *colorIndigoButton;
@property (nonatomic, retain) IBOutlet UIButton *colorGreyButton;
@property (nonatomic, retain) IBOutlet UIButton *colorYellowButton;
@property (nonatomic, retain) IBOutlet UIButton *colorWhiteButton;

// Annotation
@property (nonatomic, retain) Annotate *annotator;
@property (nonatomic, retain) IBOutlet UIButton *redColorButton;
@property (nonatomic, retain) IBOutlet UIButton *blueColorButton;
@property (nonatomic, retain) IBOutlet UIButton *greenColorButton;
@property (nonatomic, retain) IBOutlet UIButton *colorRandomColorButton;
@property (nonatomic, retain) IBOutlet UIButton *btnAnnotate;

@property (nonatomic, retain) IBOutlet UIButton *btnBookmarkLarge;
@property (nonatomic, retain) IBOutlet UIButton *btnUndoDraw;
@property (nonatomic, retain) IBOutlet UIButton *btnResetDraw;
@property (nonatomic, retain) IBOutlet UIButton *btnDrawStylePencil;
@property (nonatomic, retain) IBOutlet UIButton *btnDrawStyleHighlight;
@property (nonatomic, retain) IBOutlet UILabel *bookChapterLabel;
@property (nonatomic, retain) IBOutlet UILabel *segmentedLabel;

@property (nonatomic, assign) int fontSizeSettings;
@property (nonatomic, retain) NSString *fontSettingName;
@property (nonatomic, retain) NSString *themeNameSettings;
@property (nonatomic, retain) UIColor *mainColor;
@property (nonatomic, retain) UIColor *mainTextColor;
@property (nonatomic, retain) NSString *bookIdentifier;

@property (nonatomic, retain) NSString *currentBookname;
@property (nonatomic, retain) NSString *drmKey;
@property (nonatomic, retain) NSMutableDictionary *chapters;
@property (nonatomic, retain) NSMutableArray *chaptersForSlider;
@property (nonatomic, retain) Chapter *currentChapter;

@property (nonatomic, retain) NSMutableArray *bookEncyclopedia;
@property (nonatomic, retain) NSMutableArray *bookFigure;
@property (nonatomic, retain) NSMutableArray *bookMedia;

// Assessment
@property (nonatomic, retain) AssessmentUnlockViewController *unlockViewController;
@property (nonatomic, retain) AssessmentListViewController *unlockListController;

- (void) buildBookHighlights;
@end
