//
//  Annotate.m
//  Booklatan
//
//  Created by Baltazar Lucas on 5/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Annotate.h"

@implementation Annotate
@synthesize red, green, blue, alpha, ownerView;
@synthesize isActive, lineCap, lineWidth;

#pragma mark - for SideBar
-(int)countItems:(NSString*)bookName{
    int returnValue = 0;
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    //load
    if ([defaults valueForKey:@"items"]){
        NSMutableArray *array = (NSMutableArray*)[defaults valueForKey:@"items"];
        [items release];
        items = [[NSMutableArray alloc] initWithArray:array];
    }
    
    //now for the drawing part
    for(NSDictionary *dictItems in items){
        NSString *bookNameFromDict = ((NSString*)[dictItems objectForKey:@"bookName"]);
//        int textSizeFromDict = [((NSNumber*)[dictItems objectForKey:@"textSize"]) intValue];
        
        if ([bookNameFromDict isEqualToString:bookName]) //&&textSize == textSizeFromDict
            returnValue++;
    }    
    
    return returnValue;
}

-(NSString*)getCellTextLabel:(NSString*)bookName row:(int)row{
    NSString *returnValue;
    int counter = 0;
    
    //prepare
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    //load
    if ([defaults valueForKey:@"items"]){
        NSMutableArray *array = (NSMutableArray*)[defaults valueForKey:@"items"];
        [items release];
        items = [[NSMutableArray alloc] initWithArray:array];
    }
    
    //now for the drawing part
    for(NSDictionary *dictItems in items){
        NSString *bookNameFromDict = ((NSString*)[dictItems objectForKey:@"bookName"]);
        if ([bookNameFromDict isEqualToString:bookName])
        {
            if (counter == row)
            {
                NSString *pageTitle = (NSString*)[dictItems objectForKey:@"pageTitle"];
                returnValue = [NSString stringWithFormat:@"%@", pageTitle];
                break;
            }
            else
                counter++;
        }
    }
    
    return returnValue;
}

-(int)getSpine:(NSString*)bookName row:(int)row{
    int returnValue;
    
    int counter = 0;
    
    //prepare
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    //load
    if ([defaults valueForKey:@"items"]){
        NSMutableArray *array = (NSMutableArray*)[defaults valueForKey:@"items"];
        [items release];
        items = [[NSMutableArray alloc] initWithArray:array];
    }
    
    //now for the drawing part
    for(NSDictionary *dictItems in items){
        NSString *bookNameFromDict = ((NSString*)[dictItems objectForKey:@"bookName"]);
//        int pageFromDict = [((NSNumber*)[dictItems objectForKey:@"page"]) intValue];
        int spineFromDict = [((NSNumber*)[dictItems objectForKey:@"spine"]) intValue];
        if ([bookNameFromDict isEqualToString:bookName])
        {
            if (counter == row)
            {
                returnValue = spineFromDict;
                break;
            }
            else
                counter++;
        }
    }
    return returnValue;
}

-(int)getPageIndex:(NSString*)bookName row:(int)row{
    int returnValue;
    
    int counter = 0;
    
    //prepare
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    //load
    if ([defaults valueForKey:@"items"]){
        NSMutableArray *array = (NSMutableArray*)[defaults valueForKey:@"items"];
        [items release];
        items = [[NSMutableArray alloc] initWithArray:array];
    }
    
    //now for the drawing part
    for(NSDictionary *dictItems in items){
        NSString *bookNameFromDict = ((NSString*)[dictItems objectForKey:@"bookName"]);
        int pageFromDict = [((NSNumber*)[dictItems objectForKey:@"page"]) intValue];
//        int spineFromDict = [((NSNumber*)[dictItems objectForKey:@"spine"]) intValue];
        if ([bookNameFromDict isEqualToString:bookName])
        {
            if (counter == row)
            {
                returnValue = pageFromDict;
                break;
            }
            else
                counter++;
        }
    }
    return returnValue;
}

#pragma mark Annotate Methods and Buttons (for uniformity useful when merging)
-(void)clearDrawImageArea{
    drawAreaImage.image = nil;
}

-(BOOL)isDrawImageAreaEmpty{
    if (drawAreaImage.image == nil)
        return YES;
    else
        return NO;
}

-(UIImage*)applyThisImageOverlay:(UIImage*)topImage fromBackgroundImage:(UIImage*)bottomImage{
    
    CGSize newSize = CGSizeMake(topImage.size.width, topImage.size.height);
    UIGraphicsBeginImageContext(newSize);
    
    // Use existing opacity as is
    [bottomImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    // Apply supplied opacity
    [topImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height) 
               blendMode:kCGBlendModeNormal alpha:1];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(void)draw:(CGFloat)x y:(CGFloat)y{
    //VLog(@"ReaderSheet - draw (%.0f,%.0f)", x, y);    
    
    UIGraphicsBeginImageContext(ownerView.frame.size);
    [drawAreaImage.image drawInRect:CGRectMake(0, 0, ownerView.frame.size.width, ownerView.frame.size.height)];
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), lineCap);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), lineWidth);
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, alpha);
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPositionPoint.x, lastPositionPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), x, y);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    
    
    //    CGContextFlush(UIGraphicsGetCurrentContext());
    /*    What it does is forces the content of the window context to be flushed immediately. In this scenario, it does nothing (as it shouldn't do anything). The reason it is in the code was because this sample app was to prep for something else. Also, you probably don't want to really cal it, as the OS flushes automatically. I believe Apple recommends avoiding using this function call.*/
    
    drawAreaImage.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

-(void)saveDraw:(NSString*)bookName page:(int)page spine:(int)spine textSize:(int)textSize title:(NSString*)title{
    //VLog(@"ReaderSheet - saveDrawWithBookName");
    
    //prepare
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    //load
    if ([defaults valueForKey:@"items"]){
        NSMutableArray *array = (NSMutableArray*)[defaults valueForKey:@"items"];
        [items release];
        items = [[NSMutableArray alloc] initWithArray:array];
    }
    
    //find
    int dictIndex = 0;
    bool willReplace = FALSE;
    
    for(NSDictionary *dictItems in items){
        
        NSString *bookNameFromDict = ((NSString*)[dictItems objectForKey:@"bookName"]);
        int pageFromDict = [((NSNumber*)[dictItems objectForKey:@"page"]) intValue];
        int spineFromDict = [((NSNumber*)[dictItems objectForKey:@"spine"]) intValue];
        int textSizeFromDict = [((NSNumber*)[dictItems objectForKey:@"textSize"]) intValue];
        
        if ([bookNameFromDict isEqualToString:bookName] && 
            page == pageFromDict && 
            spine == spineFromDict &&
            textSize == textSizeFromDict)
        {
            willReplace = TRUE;
            break;
        }
        
        dictIndex += 1;
    }
    
    //convert UIImage to NSData
    NSData *imageData = UIImagePNGRepresentation(drawAreaImage.image);
    
    //save
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                          bookName, @"bookName",
                          title, @"pageTitle",
                          [NSNumber numberWithInt:page], @"page",
                          [NSNumber numberWithInt:spine], @"spine",
                          [NSNumber numberWithInt:textSize], @"textSize",
                          imageData, @"image",
                          nil];
    
    if (willReplace)
        
        [items replaceObjectAtIndex:dictIndex withObject:dict];
    else
        [items addObject:dict];
    
    //commit
    [defaults setValue:items forKey:@"items"];
    [defaults synchronize];
    
    
    //for debug only
//    for(NSDictionary *dictItems in items){
//        NSString *bookNameFromDict = ((NSString*)[dictItems objectForKey:@"bookName"]);
//        int pageFromDict = [((NSNumber*)[dictItems objectForKey:@"page"]) intValue];
//        int spineFromDict = [((NSNumber*)[dictItems objectForKey:@"spine"]) intValue];        
//        int textSizeFromDict = [((NSNumber*)[dictItems objectForKey:@"textSize"]) intValue];
//        VLog(@"page:%i spine:%i fontSize:%i bookName: %@", pageFromDict, spineFromDict, textSizeFromDict, bookNameFromDict);
//    }
    
    //release
    [defaults release];
    [items release];
    
    //clear undos
    [undoArray removeAllObjects];
}

-(void)loadDraw:(NSString*)bookName page:(int)page spine:(int)spine textSize:(int)textSize{
    //VLog(@"ReaderSheet - loadDrawWithBookName");
    drawAreaImage.image = nil;
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    //load
    if ([defaults valueForKey:@"items"]){
        NSMutableArray *array = (NSMutableArray*)[defaults valueForKey:@"items"];
        [items release];
        items = [[NSMutableArray alloc] initWithArray:array];
    }
    
    //now for the drawing part
    for(NSDictionary *dictItems in items){
        NSString *bookNameFromDict = ((NSString*)[dictItems objectForKey:@"bookName"]);
        int pageFromDict = [((NSNumber*)[dictItems objectForKey:@"page"]) intValue];
        int spineFromDict = [((NSNumber*)[dictItems objectForKey:@"spine"]) intValue];
        int textSizeFromDict = [((NSNumber*)[dictItems objectForKey:@"textSize"]) intValue];
        
        if ([bookNameFromDict isEqualToString:bookName] && 
            page == pageFromDict && 
            spine == spineFromDict &&
            textSize == textSizeFromDict)
        {
            NSData *imageData = (NSData*)[dictItems objectForKey:@"image"];
            drawAreaImage.image = [UIImage imageWithData:imageData];
            loadedDrawImage = [[UIImage alloc] initWithData:imageData];
            break;
        }
    }    
    
    //for debug only
//    for(NSDictionary *dictItems in items){
//        NSString *bookNameFromDict = ((NSString*)[dictItems objectForKey:@"bookName"]);
//        int pageFromDict = [((NSNumber*)[dictItems objectForKey:@"page"]) intValue];
//        int spineFromDict = [((NSNumber*)[dictItems objectForKey:@"spine"]) intValue];
//        int textSizeFromDict = [((NSNumber*)[dictItems objectForKey:@"textSize"]) intValue];
//        //VLog(@"page:%i spine:%i fontSize:%i bookName: %@", pageFromDict, spineFromDict, textSizeFromDict, bookNameFromDict);
//    }
    
    [defaults release];
    [items release];
    
    //    UIGraphicsBeginImageContext(webView.bounds.size);
    //    [webView.layer renderInContext:UIGraphicsGetCurrentContext()];
    //    UIImage *imageFromMisc = UIGraphicsGetImageFromCurrentImageContext();
    //    UIGraphicsEndImageContext();
    //    UIImageWriteToSavedPhotosAlbum(imageFromMisc,nil,NULL,NULL);
    
    //    drawAreaImage.image = imageFromMisc;
    //    drawAreaImage.image = [self applyThisImageOverlay:drawAreaImage.image fromBackgroundImage:imageFromMisc];
    //    [self.view bringSubviewToFront:drawAreaImage];
}

-(void)removeDraw:(NSString*)bookName page:(int)page spine:(int)spine textSize:(int)textSize{
    //VLog(@"ReaderSheet - removeDraw");
    
    
    //prepare
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    //load
    if ([defaults valueForKey:@"items"]){
        NSMutableArray *array = (NSMutableArray*)[defaults valueForKey:@"items"];
        [items release];
        items = [[NSMutableArray alloc] initWithArray:array];
    }
    
    //now for the drawing part
    for(NSDictionary *dictItems in items){
        NSString *bookNameFromDict = ((NSString*)[dictItems objectForKey:@"bookName"]);
        int pageFromDict = [((NSNumber*)[dictItems objectForKey:@"page"]) intValue];
        int spineFromDict = [((NSNumber*)[dictItems objectForKey:@"spine"]) intValue];
        int textSizeFromDict = [((NSNumber*)[dictItems objectForKey:@"textSize"]) intValue];
        
        //match
        if ([bookNameFromDict isEqualToString:bookName] && 
            page == pageFromDict && 
            spine == spineFromDict &&
            textSize == textSizeFromDict)
        {
            [items removeObject:dictItems];
            break;
        }
    } 
    
    //commit
    [defaults setValue:items forKey:@"items"];
    [defaults synchronize];
    
    //for debug only
//    for(NSDictionary *dictItems in items){
//        NSString *bookNameFromDict = ((NSString*)[dictItems objectForKey:@"bookName"]);
//        int pageFromDict = [((NSNumber*)[dictItems objectForKey:@"page"]) intValue];
//        int spineFromDict = [((NSNumber*)[dictItems objectForKey:@"spine"]) intValue];
//        int textSizeFromDict = [((NSNumber*)[dictItems objectForKey:@"textSize"]) intValue];
//        //VLog(@"page:%i spine:%i fontSize:%i bookName: %@", pageFromDict, spineFromDict, textSizeFromDict, bookNameFromDict);
//    }
    
    [defaults release];
    [items release];
}

-(void)saveDrawTemp{
    //VLog(@"ReaderSheet - saveDrawTemp");
    
    if (undoArray)
    {
        //save
        [undoArray addObject:drawAreaImage.image];
    }    
}

-(void)undoDrawTemp{
    //VLog(@"ReaderSheet - undoDrawTemp");
    if ([undoArray count] > 0) {
        [undoArray removeLastObject];
    }
    
    if (undoArray.count > 0){
        drawAreaImage.image = (UIImage*)[undoArray lastObject];
    }
    else{
        drawAreaImage.image = loadedDrawImage;
    }
}

-(void)initializeDrawing:(NSString*)bookName page:(int)page spine:(int)spine textSize:(int)textSize{
    //VLog(@"ReaderSheet - initializeDrawing");
    undoArray = [[NSMutableArray alloc] init];
    
    //create play area (image) on top ;)
	drawAreaImage = [[UIImageView alloc] initWithImage:nil];
	drawAreaImage.frame = ownerView.frame;
    [ownerView insertSubview:drawAreaImage atIndex:1]; //after the image
    
//    	drawAreaImage.backgroundColor = [UIColor redColor];
    
    //assign initial values
    red = 1.0;
    lineWidth = 5;
    lineCap = kCGLineCapRound;
    alpha = 1;
    
    [self loadDraw:bookName page:page spine:spine textSize:textSize];
}


#pragma mark -
#pragma mark Touch events handlers (this is being in the annotate)

-(void)AtouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //VLog(@"Annotate - touchesBegan");
    
    if (!isActive)
        return;
    
	fingerSwiped = NO;
	UITouch *touch = [touches anyObject];
	
	if ([touch tapCount] == 4) {
		drawAreaImage.image = nil;
		return;
	}
    
	lastPositionPoint = [touch locationInView:ownerView];
    //	lastPositionPoint.y -= 20; //adjustment necessary. for finger = drawing position
}

-(void)AtouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    //VLog(@"Annotate - touchesMoved");
    
    if (!isActive)
        return;
    
	fingerSwiped = YES;
	
	UITouch *touch = [touches anyObject];	
	CGPoint currentTouchPoint = [touch locationInView:ownerView];
    //	currentPoint.y -= 20; //adjustment necessary. for finger = drawing position
    
	[self draw:currentTouchPoint.x y:currentTouchPoint.y];
	lastPositionPoint = currentTouchPoint;
}

-(void)AtouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    //VLog(@"Annotate - touchesEnded");
	
    if (!isActive)
        return;
    
	UITouch *touch = [touches anyObject];
	
	if ([touch tapCount] == 4) {
		drawAreaImage.image = nil;
		return;
	}
	
	if(!fingerSwiped){ //touch is not from touches moved 
        [self draw:lastPositionPoint.x y:lastPositionPoint.y];
	}
    
    [self saveDrawTemp];
}

-(void)dealloc{
    [super dealloc];
    [ownerView release];
}
@end
