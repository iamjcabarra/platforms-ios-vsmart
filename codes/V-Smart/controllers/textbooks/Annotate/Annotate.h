//
//  Annotate.h
//  Booklatan
//
//  Created by Baltazar Lucas on 5/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Annotate : NSObject <UIGestureRecognizerDelegate>{
    #pragma mark Annotate Declarations
    UIImage *loadedDrawImage;
    UIImageView *drawAreaImage;

    NSMutableArray *undoArray;

    BOOL fingerSwiped;
    CGPoint lastPositionPoint;

    CGFloat red;
    CGFloat green;
    CGFloat blue;
    CGFloat alpha;

    CGLineCap lineCap;
    CGFloat lineWidth;

    UISwipeGestureRecognizer *swipeRight;
    UISwipeGestureRecognizer *swipeLeft;
    UISwipeGestureRecognizer *swipeUp;
    UISwipeGestureRecognizer *swipeDown;
}
@property (nonatomic, assign) CGFloat red;
@property (nonatomic, assign) CGFloat green;
@property (nonatomic, assign) CGFloat blue;
@property (nonatomic, assign) CGFloat alpha;
@property (nonatomic, assign) CGLineCap lineCap;
@property (nonatomic, assign) CGFloat lineWidth;
@property (nonatomic, assign) UIView *ownerView;
@property (nonatomic, assign) BOOL isActive;


-(void)clearDrawImageArea;
-(BOOL)isDrawImageAreaEmpty;
-(void)undoDrawTemp;
-(void)initializeDrawing:(NSString*)bookName page:(int)page spine:(int)spine textSize:(int)textSize;
-(void)saveDraw:(NSString*)bookName page:(int)page spine:(int)spine textSize:(int)textSize title:(NSString*)title;
-(void)loadDraw:(NSString*)bookName page:(int)page spine:(int)spine textSize:(int)textSize;
-(void)removeDraw:(NSString*)bookName page:(int)page spine:(int)spine textSize:(int)textSize;
-(void)AtouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
-(void)AtouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
-(void)AtouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

#pragma mark - for SideBar
-(int)countItems:(NSString*)bookName;
-(NSString*)getCellTextLabel:(NSString*)bookName row:(int)row;
-(int)getSpine:(NSString*)bookName row:(int)row;
-(int)getPageIndex:(NSString*)bookName row:(int)row;
@end
