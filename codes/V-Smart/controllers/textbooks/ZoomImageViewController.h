//
//  ZoomImageViewController.h
//  Booklatan
//
//  Created by Earljon Hidalgo on 5/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UATitledModalPanel.h"

@interface ZoomImageViewController : UATitledModalPanel <UIWebViewDelegate>
{
    UIView			*v;
}

@property (nonatomic, retain) NSString *filePath;
@property (nonatomic, retain) NSURL *baseURL;
@property (nonatomic, retain) NSURLRequest *urlRequest;
@property (nonatomic, assign) BOOL isFixedLayout;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title andImagePath:(NSString *) imagePath;
@end