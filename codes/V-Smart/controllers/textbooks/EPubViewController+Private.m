//
//  EPubViewController+Private.m
//  VibeReader
//
//  Created by Earljon Hidalgo on 3/29/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "EPubViewController+Private.h"
#import "UIWebView+Private.h"

@implementation EPubViewController (Private)

#pragma mark - Private Methods for Core Data Operation
- (void) _saveHighlightToDb: (NSDictionary *) json {
    if(IsEmpty(self.loadedEpub.uuid)) return;
    
    NSDictionary *result = [Utils toDictionary:[json objectForKey:@"result"]];
    /*
     result:
     cssClass, endRange, id, serializedValue, startRange, textValue
     */
    NSString *cssClass = [result objectForKey:kDBCloudHighlightCss];
    NSString *serializedValue = [result objectForKey:@"serializedValue"];
    NSString *textValue = [result objectForKey:@"textValue"];
    NSNumber *highlightId = [NSNumber numberWithInt:[[result objectForKey:@"id"] intValue]];
    NSNumber *startRange = [NSNumber numberWithInt:[[result objectForKey:kDBCloudHighlightStartRange] intValue]];
    NSNumber *endRange = [NSNumber numberWithInt:[[result objectForKey:kDBCloudHighlightEndRange] intValue]];
    NSNumber *hasNote = [NSNumber numberWithInt:0];
    NSString *chapterFile = [self.currentChapter.spinePath lastPathComponent];
    NSString *note = @"";
    NSString *email = [VSmartHelpers getEmail];
    NSString *chapterTitle = self.currentChapter.title;
    NSString *uuid = self.loadedEpub.uuid;
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:email forKey:kDBCloudAccountEmail];
    [data setValue:chapterTitle forKey:kDBCloudHighlightChapterTitle];
    [data setValue:chapterFile forKey:kDBCloudHighlightChapterFile];
    [data setValue:serializedValue forKey:kDBCloudHighlightData];
    [data setValue:textValue forKey:kDBCloudHighlightText];
    [data setValue:highlightId forKey:kDBCloudHighlightId];
    [data setValue:note forKey:kDBCloudHighlightNote];
    [data setValue:hasNote forKey:kDBCloudHighlightHasNote];
    [data setValue:cssClass forKey:kDBCloudHighlightCss];
    [data setValue:startRange forKey:kDBCloudHighlightStartRange];
    [data setValue:endRange forKey:kDBCloudHighlightEndRange];
    [data setValue:uuid forKey:kDBCloudUUID];
    
    //VLog(@"Highlight: \n%@", data);
    [[VibeDataManager sharedInstance] addCloudHighlight:data];
}

- (void) _removeHighlightToDb: (NSArray *) highlights {
    for (int i = 0; i < [highlights count]; i++) {
        NSDictionary *highlight = (NSDictionary *)[Utils toDictionary:[highlights objectAtIndex:i]];
        //VLog(@"Highlight: %@", highlight);
        [[VibeDataManager sharedInstance] removeCloudHighlightById:[VSmartHelpers getEmail] withUUID:self.loadedEpub.uuid forChapterFile:[self.currentChapter.spinePath lastPathComponent] andHighlightId:[[highlight objectForKey:@"id"] intValue]];
    }
}

- (void) _removeHighlightToDbById: (int) highlightId {
    [[VibeDataManager sharedInstance] removeCloudHighlightById:[VSmartHelpers getEmail] withUUID:self.loadedEpub.uuid forChapterFile:[self.currentChapter.spinePath lastPathComponent] andHighlightId:highlightId];
}

- (void) _updateHighlightToDb: (CloudHighlight *) cloudHighlight {
    [[VibeDataManager sharedInstance] updateCloudHighlight:cloudHighlight];
}

- (void) _updateHighlightToDb: (int) highlightId withNote:(NSString *) note {
    if(IsEmpty(self.loadedEpub.uuid)) return;
    
    NSString *email = [VSmartHelpers getEmail];
    NSString *uuid = self.loadedEpub.uuid;
    NSString *chapterFile = [self.currentChapter.spinePath lastPathComponent];
    
    CloudHighlight *data = (CloudHighlight *)[[VibeDataManager sharedInstance] fetchCloudHighlightById:email withUUID:uuid forChapterFile:chapterFile andHighlightId:highlightId];
    
    if (data) {
        data.hasNote = [NSNumber numberWithInt:1];
        data.note = note;
        [[VibeDataManager sharedInstance] updateCloudHighlight:data];
    }
}

- (NSString *) _extractHighlightNotes: (NSArray *) highlights {
    NSMutableString *notes = [[NSMutableString alloc] init];
    
    for (int i = 0; i < [highlights count]; i++) {
        NSDictionary *highlight = (NSDictionary *)[Utils toDictionary:[highlights objectAtIndex:i]];
        //VLog(@"Highlight: %@", highlight);
        int highlightId = [[highlight objectForKey:@"id"] intValue];
        CloudHighlight *current = [self _fetchCloudHighlightById: highlightId];
        if (current.hasNote) {
            [notes appendFormat:@"\n\n%@", current.note];
        }
    }
    
    return notes;
}

- (CloudHighlight *) _fetchCloudHighlightById: (int) highlightId {
    NSString *email = [VSmartHelpers getEmail];
    NSString *uuid = self.loadedEpub.uuid;
    NSString *chapterFile = [self.currentChapter.spinePath lastPathComponent];
    
    // 1028$1056$1$vibeHighlightBlue$
    CloudHighlight *cloudHighlight = (CloudHighlight *)[[VibeDataManager sharedInstance] fetchCloudHighlightById:email withUUID:uuid forChapterFile:chapterFile andHighlightId:highlightId];
    
    return cloudHighlight;
}

- (void) _loadHighlights {
    // reset all highlights first
    if(IsEmpty(self.loadedEpub.uuid)) return;
    
    [webView removeHighlights];
    NSArray *highlights = [[VibeDataManager sharedInstance] fetchCloudHighlightsForChapter:[VSmartHelpers getEmail]
                                                                                  withUUID:self.loadedEpub.uuid
                                                                            forChapterFile:[self.currentChapter.spinePath lastPathComponent]];
    
    NSMutableString *highlightRange = [[NSMutableString alloc] initWithString:kCloudHighlightTextPrefix];
    
    if (highlights) {
        for (int i = 0; i < [highlights count]; i++) {
            CloudHighlight *highlight = (CloudHighlight *)[highlights objectAtIndex:i];
            [highlightRange appendFormat:@"|%@", highlight.highlightedData];
        }
        
        [self restoreHighlight:highlightRange];
    }
    
    [self buildBookHighlights];
}

#pragma mark - Private Methods for EpubViewController
- (void) restoreHighlight: (NSString *) range {
    //VLog(@"TEXT: %@", range);
    [webView applyHighlightForData:range];
}

- (void) revertSelectedHighlight {
    [self _loadHighlights];
}


@end
