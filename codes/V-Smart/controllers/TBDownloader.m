//
//  TBDownloader.m
//  V-Smart
//
//  Created by Ryan Migallos on 21/04/2016.
//  Copyright © 2016 Vibe Technologies. All rights reserved.
//

#import "TBDownloader.h"
#import "ZipArchive.h"
#import "Book.h"
#import "Utils.h"

@interface TBDownloader() <NSURLSessionDownloadDelegate>
@property (nonatomic, strong) NSManagedObjectContext *ctx;
@property (nonatomic, strong) NSOperationQueue *operation;
@property (nonatomic, strong) NSURLSessionDownloadTask *downloadTask;
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSManagedObject *mo;
@property (nonatomic, strong, readwrite) NSString *skuString;
@property (nonatomic, strong, readwrite) NSString *bookID;
@property (nonatomic, strong, readwrite) NSString *userID;
@property (nonatomic, strong, readwrite) NSString *extension;
@property (nonatomic, strong, readwrite) NSString *downloadURL;
@property (nonatomic, assign, readwrite) CGFloat progress;
@property (nonatomic, assign, readwrite) BOOL active;
@property (nonatomic, strong) NSDictionary *info;
@property (nonatomic, strong) NSData *resumeData;
@property (nonatomic, assign) NSUInteger identifier;
@property (nonatomic, strong) Book *book;
@property (nonatomic, strong) EPub *epub;
@property (nonatomic, assign) BOOL validFormat;

@end

@implementation TBDownloader

static NSString *kSessionIdentifier = @"com.vsmart.textbook.DOWNLOADER";

- (instancetype)initWithBook:(NSDictionary *)info url:(NSURLRequest *)url queue:(NSOperationQueue *)queue {
    
    if ( self = [super init] ) {
        
        self.skuString = [NSString stringWithFormat:@"%@", info[@"sku"] ];
        self.bookID = [NSString stringWithFormat:@"%@", info[@"book_id"] ];
        self.userID = [NSString stringWithFormat:@"%@", info[@"user_id"] ];
        self.info = [NSDictionary dictionaryWithDictionary:info];
        self.downloadTask = [self url:url queue:queue];
        self.operation = queue;
        self.book = (Book *)info[@"book"];
        self.mo = (NSManagedObject *)info[@"mo"];
        self.resumeData = [self.mo valueForKey:@"resume_data"];
        self.extension = [self extenstionForMediaType:self.book.mediaType];
        
        NSLog(@"INIT DOWNLOADER %@ ------ %@", [self.mo valueForKey:@"book_id"], self.bookID  );
        
        self.ctx = self.mo.managedObjectContext;
    }
    
    return self;
}

- (NSString *)extenstionForMediaType:(NSString *)mediaType {
    
    NSString *ext = @"epub";
    
    if ([self.book.mediaType isEqualToString:kBookExtensionEpub]) {
        ext = @"epub";
    }
    
    if ([self.book.mediaType isEqualToString:kBookExtensionVibe]) {
        ext = @"vibe";
    }
    
    if ([self.book.mediaType isEqualToString:kBookExtensionPdf]) {
        ext = @"pdf";
    }
    
    return ext;
}

- (NSURLSessionConfiguration *)configuration {
    
    NSString *identifier_string = [NSString stringWithFormat:@"%@_%@", kSessionIdentifier, self.bookID];
    
    // NOTE: Session Identifier needs to be unique for Multi-Core Session Threading
    
    // SESSION CONFIGURATION
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:identifier_string];
    config.allowsCellularAccess = YES;
//    config.shouldUseExtendedBackgroundIdleMode = YES; //iOS 9
    config.networkServiceType = NSURLNetworkServiceTypeBackground;
    
    //EXPERIMENTAL
    config.HTTPMaximumConnectionsPerHost = 1;
    
    return config;
}

- (NSURLSessionDownloadTask *)url:(NSURLRequest *)request queue:(NSOperationQueue *)queue {
    
    // SESSION CONFIGURATION
    NSURLSessionConfiguration *config = [self configuration];
    
    // SESSION OBJECT
    self.session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    NSURLSessionDownloadTask *task = [self.session downloadTaskWithRequest:request];
    
    self.identifier = task.taskIdentifier; //TASK IDENTIFIER
    self.downloadURL = [NSString stringWithFormat:@"%@", request.URL.absoluteString ];
    
    return task;
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    
    if (error != nil) {
        NSLog(@"DOWNLOAD WITH ERROR");
        [self downloadError];
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location {
    
    [self manageDownloadedFileFromURL:location];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    
    CGFloat progress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    NSLog(@"download progress : %f", progress);
    
    NSBlockOperation *rbo = [NSBlockOperation blockOperationWithBlock:^{
        [self.ctx performBlock:^{
            [self.mo setValue:@(progress) forKey:@"progress"];
            [self.mo setValue:@YES forKey:@"download_active"];
            [self.mo setValue:@(VSmartDownloadInProgress) forKey:@"download_status"];
            [self saveTreeContext:self.ctx];
        }];
    }];
    [self.operation addOperation:rbo];
}

- (void)cancel {
    
    [self.downloadTask suspend];
    [self.downloadTask cancel];
    [self.session finishTasksAndInvalidate];
    
    [self.ctx performBlock:^{
        [self.mo setValue:@0 forKey:@"progress"];
        [self.mo setValue:@NO forKey:@"download_active"];
        [self.mo setValue:@(VSmartDownloadNotStarted) forKey:@"download_status"];
        [self saveTreeContext:self.ctx];
    }];
    
    self.resumeData = nil;
    self.active = NO;
}

- (void)resume:(NSData *)data {
    
    if ( (self.active == NO) && (data != nil) ) {
        
        self.downloadTask = [self.session downloadTaskWithResumeData:data];
        [self.downloadTask resume];
        
        [self.ctx performBlock:^{
            [self.mo setValue:@YES forKey:@"download_active"];
            [self.mo setValue:@(VSmartDownloadInProgress) forKey:@"download_status"];
            [self saveTreeContext:self.ctx];
        }];
        
        self.active = YES;
    }
}

- (void)start {
    
    if (self.active == NO) {
        
        [self.downloadTask resume];
        
        [self.ctx performBlock:^{
            [self.mo setValue:@YES forKey:@"download_active"];
            [self.mo setValue:@(VSmartDownloadInProgress) forKey:@"download_status"];
            [self saveTreeContext:self.ctx];
        }];
        
        self.active = YES;
    }
}

- (void)pause {
    
    if (self.active == YES) {
        
        [self.downloadTask cancelByProducingResumeData:^(NSData *data) {
            NSData *resume_data = [NSData dataWithData:data];
            [self.ctx performBlock:^{
                [self.mo setValue:@NO forKey:@"download_active"];
                [self.mo setValue:resume_data forKey:@"resume_data"];
                [self.mo setValue:@(VSmartDownloadPause) forKey:@"download_status"];
                [self saveTreeContext:self.ctx];
            }];
            
        }];
        
        self.active = NO;
    }
}

- (void)downloadError {
    
    [self.ctx performBlock:^{
        [self.mo setValue:@(0) forKey:@"progress"];
        [self.mo setValue:@NO forKey:@"download_active"];
        [self.mo setValue:@(VSmartDownloadNotStarted) forKey:@"download_status"];
        [self saveTreeContext:self.ctx];
    }];
    
    [self.session finishTasksAndInvalidate];
}

- (void)processEpubFile:(NSString *)filePath {
    
    __weak typeof(self) wo = self;
    
    NSString *completedBookId = [NSString stringWithFormat:@"%@", self.bookID ];
    NSLog(@"STARING PROCESS EPUB FILE : %@", completedBookId);
    
    NSBlockOperation *bo0 = [NSBlockOperation blockOperationWithBlock:^{
        [Utils unzipEpubFile:completedBookId withFullPath:filePath];
    }];

    NSBlockOperation *bo1 = [NSBlockOperation blockOperationWithBlock:^{
        wo.epub = [[EPub alloc] initWithBookname:completedBookId];
        wo.validFormat = wo.epub.isValid;
    }];
    
    NSBlockOperation *bo2 = [NSBlockOperation blockOperationWithBlock:^{
        
//        if (wo.epub.isValid == NO) {
        if (wo.validFormat == NO) {
            NSLog(@"EPUB IS INVALID DATA...");
        }
        
//        if (wo.epub.isValid == YES) {
        if (wo.validFormat == YES) {
            NSLog(@"EPUB IS VALID...");
            NSString *uuid = wo.epub.uuid;
            if (!IsEmpty(uuid)) {
                Book *b = wo.book;
                Metadata *meta = [[Metadata alloc] init];
                meta.uuid = uuid;
                meta.title = b.title;
                meta.publisher = b.publisher;
                meta.authors = [VSmartHelpers getAuthors:b.authors];

                [VSmartHelpers addBook:b];
                [VSmartHelpers saveDRMinfoForBook:uuid drmversion:wo.epub.drmVersion];
                [VSmartHelpers addBookToCloud:meta];
                [VSmartHelpers addBookExerciseToCloud:completedBookId forBook:uuid];
                NSDictionary *exerciseData = [Utils getExerciseFileContents:completedBookId];
                if (exerciseData != nil) {
                    [VSmartHelpers saveBookExerciseData:completedBookId forExercise:exerciseData];
                }
            }
        }
    }];
    
    NSBlockOperation *bo3 = [NSBlockOperation blockOperationWithBlock:^{
        NSFileManager *manager = [NSFileManager defaultManager];
        NSError *error = nil;
        [manager removeItemAtPath:filePath error:&error];
        if (error && [manager fileExistsAtPath:filePath]) {
            [manager createFileAtPath:filePath contents:[NSData data] attributes:nil];
            [manager removeItemAtPath:filePath error:nil];
        }
    }];
    
    NSBlockOperation *bo4 = [NSBlockOperation blockOperationWithBlock:^{
        [self.ctx performBlock:^{
            
            if (wo.epub.isValid == YES) {

                NSLog(@"EPUB IS VALID...");
                [self.mo setValue:@100 forKey:@"progress"];
                [self.mo setValue:@YES forKey:@"download_active"];
                [self.mo setValue:@(VSmartDownloadExtracted) forKey:@"download_status"];
                [self saveTreeContext:self.ctx];
            }

            if (wo.epub.isValid == NO) {

                NSLog(@"EPUB IS INVALID DATA...");
                
                NSString *status = NSLocalizedString(@"Invalid ePub", nil);
                NSString *message = NSLocalizedString(@"please contact technical support", nil);
                NSString *object = [NSString stringWithFormat:@"%@, %@", status, message];
                NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:kNofiticationBookDownloadError object:object];

                [self.mo setValue:@(0) forKey:@"progress"];
                [self.mo setValue:@NO forKey:@"download_active"];
                [self.mo setValue:@(VSmartDownloadNotStarted) forKey:@"download_status"];
                [self saveTreeContext:self.ctx];
            }
        }];
    }];
    
    [self.operation addOperation:bo0];
    [self.operation addOperation:bo1];
    [self.operation addOperation:bo2];
    [self.operation addOperation:bo3];
    [self.operation addOperation:bo4];
    
    NSLog(@"---------------> BOOK DOWNLOAD COMPLETE");
    if ( [(NSObject *)self.delegate respondsToSelector:@selector(didFinishDownloadingBook:)]  ) {
        NSString *book_id = [NSString stringWithFormat:@"%@", self.bookID ];
        [self.session finishTasksAndInvalidate];
        [self.delegate didFinishDownloadingBook:book_id];
    }
}

- (void)processPdfFile:(NSString *)filePath fileSize:(NSInteger)fileSize {
    
    __weak typeof(self) wo = self;
    
    __block NSData *pngCoverData = nil;
    
    NSString *folderName = [NSString stringWithFormat:@"%@", self.bookID];
    NSString *pdfFile = [NSString stringWithFormat:@"%@", filePath];
    NSURL *url = [NSURL fileURLWithPath:pdfFile];
    
    NSBlockOperation *bo1 = [NSBlockOperation blockOperationWithBlock:^{
                
        NSDictionary *data = [Utils getPdfMetadata:url forPDFFile:pdfFile withFolderName:folderName];
        VLog(@"PDF_DATA: %@", data);
        
        NSString *folderPath = [Utils createBookFolder:folderName];
        
        NSError *error = nil;
        NSString *pdfFilePath = [folderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf", folderName]];
        NSFileManager *manager = [NSFileManager defaultManager];
        [manager copyItemAtPath:pdfFile toPath:pdfFilePath error:&error];
        
        NSString *password = [NSString stringWithFormat:@"%@", [self.mo valueForKey:@"password"] ];
        
        BOOL encrypt = [self canDecryptPDF:pdfFilePath password:password];
        
        UIImage *pdfCover = encrypt ? [UIImage imageFromPDF:url pass:password] : [UIImage imageFromPDF:url];
        
        NSString *imageFileName = [NSString stringWithFormat:@"%@.%@", folderName, @"png"];
        NSString *imagePath = [folderPath stringByAppendingPathComponent:imageFileName];
        NSLog(@"----------> pdf image path : %@", imagePath);
        pngCoverData = UIImagePNGRepresentation(pdfCover);
        [pngCoverData writeToFile:imagePath atomically:NO];
        
        wo.validFormat = encrypt;
        if (wo.validFormat == YES) {
            Author *author = [[Author alloc] init];
            author.authorName = [data objectForKey:@"authors_text"];
            self.book.estimatedFileSize = fileSize;
            [VSmartHelpers addBook:self.book];
        }
    }];
    
    NSBlockOperation *bo2 = [NSBlockOperation blockOperationWithBlock:^{
        
        NSError *error = nil;
        NSFileManager *manager = [NSFileManager defaultManager];
        [manager removeItemAtPath:filePath error:&error];
        if (error && [manager fileExistsAtPath:filePath]) {
            [manager createFileAtPath:filePath contents:[NSData data] attributes:nil];
            [manager removeItemAtPath:filePath error:nil];
        }
        
    }];
    
    NSBlockOperation *bo3 = [NSBlockOperation blockOperationWithBlock:^{
        
        [self.ctx performBlock:^{
            
            if (wo.validFormat == YES) {
                [self.mo setValue:@100 forKey:@"progress"];
                [self.mo setValue:@NO forKey:@"download_active"];
                [self.mo setValue:@(VSmartDownloadExtracted) forKey:@"download_status"];
                
                if (pngCoverData != nil) {
                    [self.mo setValue:pngCoverData forKey:@"thumbnail"];
                }
                
                [self saveTreeContext:self.ctx];
            }
            
            if (wo.validFormat == NO) {
                
                NSString *status = NSLocalizedString(@"Invalid PDF", nil);
                NSString *message = NSLocalizedString(@"please contact technical support", nil);
                NSString *object = [NSString stringWithFormat:@"%@, %@", status, message];
                NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:kNofiticationBookDownloadError object:object];
                
                [self.mo setValue:@(0) forKey:@"progress"];
                [self.mo setValue:@NO forKey:@"download_active"];
                [self.mo setValue:@(VSmartDownloadNotStarted) forKey:@"download_status"];
                [self saveTreeContext:self.ctx];
            }
            
        }];
        
    }];

    [self.operation addOperation:bo1];
    [self.operation addOperation:bo2];
    [self.operation addOperation:bo3];
    
    NSLog(@"---------------> BOOK DOWNLOAD COMPLETE");
    if ( [(NSObject *)self.delegate respondsToSelector:@selector(didFinishDownloadingBook:)]  ) {
        NSString *book_id = [NSString stringWithFormat:@"%@", self.bookID ];
        [self.session finishTasksAndInvalidate];
        [self.delegate didFinishDownloadingBook:book_id];
    }
    
}

// FILE MANAGER
- (void)manageDownloadedFileFromURL:(NSURL *)location {
    
    //NSFileManager
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *filePath = [NSString stringWithFormat:@"%@", [location path] ];
    
    NSDictionary *attributes = [fm attributesOfItemAtPath:filePath error:nil];
    NSInteger fileSize = [[attributes objectForKey:NSFileSize] integerValue];
    NSString *fileName = [NSString stringWithFormat:@"%@.%@", self.bookID, self.extension];
    NSURL *destinationURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:fileName]];
    
    // Hold this file as an NSData and write it to the new location
    NSData *fileData = [NSData dataWithContentsOfURL:location];
    [fileData writeToURL:destinationURL atomically:YES];
    
    NSString *destinationPath = [destinationURL path];
    NSLog(@"ABSOLUTE PATH: %@", destinationPath);
    
    NSString *fileExtension = [NSString stringWithFormat:@"%@", destinationPath.pathExtension ];
    
    //////////////////////////
    // HTML ERROR PAGE
    //////////////////////////
    if ( fileSize <= 100 ) {
        [self downloadError];
    }
    
    //////////////////////////
    // [ VIBE | EPUB ] file
    //////////////////////////
    if ( [fileExtension isEqualToString:@"vibe"] || [fileExtension isEqualToString:@"epub"]  ) {
        [self processEpubFile:destinationPath];
    }
    
    ////////////////////////
    // PDF file
    ////////////////////////
    if ( [fileExtension isEqualToString:@"pdf"] ) {
        [self processPdfFile:destinationPath fileSize:fileSize];
    }
}

// CORE DATA

- (void)saveTreeContext:(NSManagedObjectContext *)context
{
    if (!context) {
        return;
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"ERROR saving: %@", error);
    }
    
    if (context.parentContext) {
        [self saveTreeContext:context.parentContext];
    }
}

- (BOOL)canDecryptPDF:(NSString *)filepath password:(NSString *)password {
    
    BOOL status = NO;
    
    CFURLRef fileURL = (__bridge CFURLRef)[NSURL fileURLWithPath:filepath];
    CGPDFDocumentRef thePDFDocRef = CGPDFDocumentCreateWithURL(fileURL);
    if (thePDFDocRef != NULL) // Check for non-NULL CGPDFDocumentRef
    {
        if (CGPDFDocumentIsEncrypted(thePDFDocRef) == TRUE) // Encrypted
        {
            NSLog(@"PDF IS ENCRYPTED!!!");
            // Try a blank password first, per Apple's Quartz PDF example
            if (CGPDFDocumentUnlockWithPassword(thePDFDocRef, "") == FALSE)
            {
                
                NSLog(@"PDF PASSWORD IS NOT EMPTY STRING!!!");
                // Nope, now let's try the provided password to unlock the PDF
                if ((password != nil) && (password.length > 0)) // Not blank?
                {
                    char text[128]; // char array buffer for the string conversion
                    [password getCString:text maxLength:126 encoding:NSUTF8StringEncoding];
                    
                    if (CGPDFDocumentUnlockWithPassword(thePDFDocRef, text) == FALSE) // Log failure
                    {
                        NSLog(@"CGPDFDocumentCreateUsingUrl: Unable to unlock [%@] with [%@]", fileURL, password);
                        return NO;
                    }
                    
                    if (CGPDFDocumentUnlockWithPassword(thePDFDocRef, text) == TRUE) // Log failure
                    {
                        NSLog(@"PDF IS UNLOCKED WITH PASSWORD: %@", password);
                        return YES;
                    }

                    
                }
            }
            
            if (CGPDFDocumentIsUnlocked(thePDFDocRef) == FALSE) // Cleanup unlock failure
            {
                CGPDFDocumentRelease(thePDFDocRef), thePDFDocRef = NULL;
            }
        }
        
        return YES;
    }
    
    return status;
}

@end
