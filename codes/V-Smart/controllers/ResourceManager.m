//
//  ResourceManager.m
//  V-Smart
//
//  Created by Ryan Migallos on 5/6/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import "ResourceManager.h"
#import "SocketIO.h"
#import "SocketIOPacket.h"
#import "TBDownloader.h"
#import "Utils.h"
#import "AccountInfo.h"
#import "VSmartValues.h"
#import "NSDate+Helper.h"
#import "TBXML.h"
#import "Book.h"
#import "Author.h"
#import "NSString+MD5Addition.h"
#import "V_Smart-Swift.h"
#import <MobileCoreServices/MobileCoreServices.h>

static NSString *storeFilename = @"vsmart.sqlite";
static NSString *cloudDataFilename = @"CloudData.sqlite";
static NSString *moduleModel = @"ModuleModel.sqlite";

@interface ResourceManager() <SocketIODelegate, NSURLSessionDownloadDelegate, TBDownloaderDelegate>

#pragma mark - PROPERTIES (Private)

@property (nonatomic, readwrite) NSManagedObjectContext *masterContext;
@property (nonatomic, readwrite) NSManagedObjectContext *mainContext;

@property (nonatomic, readwrite) NSManagedObjectModel *model;
@property (nonatomic, readwrite) NSPersistentStoreCoordinator *coordinator;
@property (nonatomic, readwrite) NSPersistentStore *store;

@property SocketIO *socketIO; //OLD
@property SocketIOClient *socketClient; //NEW

@property NSMutableArray *message;

@property (nonatomic, strong) NSDateFormatter *formatter;

@property (nonatomic, strong) NSString *social_stream_group_id;

// BLOCK Callbacks
@property (nonatomic, copy, readwrite) ResourceManagerDownloadBlock downloadProgressHandler;

// OBJECTIVE-C QUEUEING IMPLEMENTATION
@property (nonatomic, strong) NSOperationQueue *queue;
@property (nonatomic, strong) NSMutableDictionary *lookupList;

@end

@implementation ResourceManager

#pragma mark - SINGLETON

+ (instancetype)sharedInstance
{
    VLog(@"%@ '%@'", self.class, NSStringFromSelector(_cmd));
    static ResourceManager *singleton = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        singleton = [[self alloc] init];
    });
    return singleton;
}

#pragma mark - SETUP

- (id)init
{
    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    
    self = [super init];
    if (!self) { return nil; }
    
    // OBJECTIVE-C NSOperationQueue
    self.queue = [[NSOperationQueue alloc] init];
    self.queue.maxConcurrentOperationCount = 1; //DEFAULT TO FIVE CONCURRENT CONNECTIONS
    self.queue.qualityOfService = NSQualityOfServiceBackground; //USE AS BACKGROUND SERVICE
    
    NSArray *models = [self mergeModels];
    
    self.model = [NSManagedObjectModel modelByMergingModels:models];
    
    // Persistent Store Coordinator
    self.coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
    
    BOOL compatible = [self isCompatibleWithCoreDataMetadata:[self storeURL]];
    if (!compatible) {
        self.store = nil;
    }
    
    // CORE DATA 3-LAYER STACK for MASTER, MAIN, WORKER
    [self initializeManagedObjectsWithCoordinator:self.coordinator];
    
    // DATE FORMATTER
    self.formatter = [[NSDateFormatter alloc] init];
    
    // TEXTBOOK ACCESS FLAG
    self.isTextBookModuleFirstLoaded = YES;
    
    return self;
}

- (void)initializeManagedObjectsWithCoordinator:(NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // DATA FLUSHER
    // [WARNING: YOU ARE NOT ALLOWED TO USE THIS CONTEXT]
    // Core Data Stack for the Master Thread [MASTER] -> [PERSISTENTSTORE]
    _masterContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_masterContext performBlockAndWait:^{
        [_masterContext setPersistentStoreCoordinator:persistentStoreCoordinator];
        [_masterContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // UI RELATED CONTEXT
    // Core Data Stack for the Main Thread [MAIN] -> [MASTER]
    _mainContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_mainContext setParentContext:_masterContext];
    [_mainContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    
    // BACKGROUND CONTEXT
    // Core Data Stack for the Worker Thread [WORKER] -> [MAIN]
    _workerContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_workerContext performBlockAndWait:^{
        [_workerContext setParentContext:_mainContext];
        [_workerContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    // LOAD STORE FILE
    [self loadStore];
}

- (void)setupCoreData
{
    VLog(@"Running %s", __PRETTY_FUNCTION__);
    [self loadStore];
}

- (void)loadStore
{
    VLog(@"Running %s", __PRETTY_FUNCTION__);
    BOOL useMigrationManager = NO;
    if (useMigrationManager && [self isMigrationNecessaryForStore:[self storeURL]]) {
        //Perform Custom Migration
        [self performBackgroundManagedMigrationForStore:[self storeURL]];
    }
    else {
        NSDictionary *sqliteConfig = @{@"journal_mode" : @"DELETE"};//@{@"journal_mode" : @"WAL"}
        NSDictionary *options = @{
                                  NSMigratePersistentStoresAutomaticallyOption  : @YES ,
                                  NSInferMappingModelAutomaticallyOption        : @YES , //Light Weight Migration
                                  NSSQLitePragmasOption                         : sqliteConfig
                                  };
        NSError *error = nil;
        _store = [_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:[self storeURL] options:options error:&error];
        if (!_store) {
            //            VLog(@"Failed to add store. WARNING : %@", error);
            //abort();
        } else {
            VLog(@"Successfully added store: %@", _store);
        }
        
        //        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"CloudData.sqlite"];
        //        NSURL *storeUrl = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ModuleModel.sqlite"]];
    }
}

#pragma mark - MODELS (Initialization)

- (NSArray *)mergeModels
{
    NSManagedObjectModel *cloudData = [self modelFromFrameWork:@"CloudData"];
    NSManagedObjectModel *moduleModel = [self modelFromFrameWork:@"ModuleModel"];
    NSManagedObjectModel *courseModel = [self modelFromFrameWork:@"CourseDataModel"];
    NSArray *models = @[cloudData, moduleModel, courseModel];
    
    return models;
}

- (NSManagedObjectModel *)modelFromFrameWork:(NSString *)name
{
    NSBundle *appBundle = [NSBundle mainBundle];
    NSURL *modelFileURL = [appBundle URLForResource:name withExtension:@"momd"];
    
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelFileURL];
    
    return model;
}

- (NSManagedObjectModel *)textBookModel
{
    //Coded Model
    NSManagedObjectModel *resource = [[NSManagedObjectModel alloc] init];
    
    //Create the entity
    NSEntityDescription *entity = [[NSEntityDescription alloc] init];
    [entity setName:kTextBookEntity];
    [entity setManagedObjectClassName:kTextBookEntity];
    
    /*
     book   			[TRANSFORMABLE]
     book_id			[STRING]
     download_active	[BOOLEAN]
     download_status	[Integer 32]
     progress		[Float]
     */
    
    // create the attributes
    NSMutableArray *properties = [NSMutableArray array];
    
    //BOOK [TRANSFORMABLE]
    NSAttributeDescription *bookAttrib = [self attributeWithName:@"book"];
    [bookAttrib setAttributeType:NSTransformableAttributeType];
    [properties addObject:bookAttrib];
    
    //BOOK_ID [NSSTRING]
    NSAttributeDescription *bookidAttrib = [self attributeWithName:@"book_id"];
    [bookidAttrib setAttributeType:NSStringAttributeType];
    [bookidAttrib setDefaultValue:@""];
    [properties addObject:bookidAttrib];
    
    //DOWNLOAD_ACTIVE [BOOLEAN]
    NSAttributeDescription *downloadActiveAttrib = [self attributeWithName:@"download_active"];
    [downloadActiveAttrib setAttributeType:NSBooleanAttributeType];
    [downloadActiveAttrib setDefaultValue:@NO];
    [properties addObject:downloadActiveAttrib];
    
    //DOWNLOAD_STATUS [INTEGER 32]
    NSAttributeDescription *downloadStatusAttrib = [self attributeWithName:@"download_status"];
    [downloadStatusAttrib setAttributeType:NSInteger32AttributeType];
    [downloadStatusAttrib setDefaultValue:@0];
    [properties addObject:downloadStatusAttrib];
    
    //PROGRESS [FLOAT]
    NSAttributeDescription *progressAttrib = [self attributeWithName:@"progress"];
    [progressAttrib setAttributeType:NSFloatAttributeType];
    [progressAttrib setDefaultValue:@0];
    [properties addObject:progressAttrib];
    
    //USER ID [NSSTRING]
    NSAttributeDescription *useridAttrib = [self attributeWithName:@"user_id"];
    [useridAttrib setAttributeType:NSStringAttributeType];
    [useridAttrib setDefaultValue:@""];
    [properties addObject:useridAttrib];
    
    // add attributes to entity
    [entity setProperties:properties];
    
    // add entity to model
    [resource setEntities:@[entity]];
    
    return resource;
}

- (NSManagedObjectModel *)schoolCodeModel
{
    //Coded Model
    NSManagedObjectModel *resource = [[NSManagedObjectModel alloc] init];
    
    //Create the entity
    NSEntityDescription *entity = [[NSEntityDescription alloc] init];
    [entity setName:kSchoolCodeEntity];
    [entity setManagedObjectClassName:kSchoolCodeEntity];
    
    /*
     schoolCode			[STRING]
     */
    
    // create the attributes
    NSMutableArray *properties = [NSMutableArray array];
    
    //SCHOOLCODE [NSSTRING]
    NSAttributeDescription *schoolCodeAttrib = [self attributeWithName:@"code"];
    [schoolCodeAttrib setAttributeType:NSStringAttributeType];
    [properties addObject:schoolCodeAttrib];
    
    //REDIRECT [BOOLEAN]
    NSAttributeDescription *downloadActiveAttrib = [self attributeWithName:@"redirect"];
    [downloadActiveAttrib setAttributeType:NSBooleanAttributeType];
    [downloadActiveAttrib setDefaultValue:@NO];
    [properties addObject:downloadActiveAttrib];
    
    // add attributes to entity
    [entity setProperties:properties];
    
    // add entity to model
    [resource setEntities:@[entity]];
    
    return resource;
}

- (NSAttributeDescription *)attributeWithName:(NSString *)attributeName
{
    NSAttributeDescription *attribute = [[NSAttributeDescription alloc] init];
    [attribute setName:attributeName];
    [attribute setOptional:YES];
    [attribute setIndexed:NO];
    [attribute setTransient:NO];
    
    return attribute;
}

#pragma mark - SAVING

- (void)saveContext
{
    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    //    if ([_mainContext hasChanges]) {
    //        NSError *error = nil;
    //        if ([_mainContext save:&error]) {
    //            VLog(@"_context SAVED changes to persistent store");
    //        } else {
    //            VLog(@"Failed to save _context: %@", error);
    //        }
    //    } else {
    //        VLog(@"SKIPPED _context save, there are no changes!");
    //    }
    
    NSManagedObjectContext *ctx = _workerContext;
    [ctx performBlockAndWait:^{
        [self saveTreeContext:ctx];
    }];
}

- (void)backgroundSaveContext
{
    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    // First, save the child context in the foreground (fast, all in memory)
    [self saveContext];
    
    [_masterContext performBlock:^{
        
        if ([_masterContext hasChanges]) {
            NSError *error = nil;
            if ([_masterContext save:&error]) {
                VLog(@"_masterContext SAVED changes to persistent store");
            }
            else {
                VLog(@"_masterContext FAILED to save: %@", error);
            }
        }
        else {
            VLog(@"master context SKIPPED saving as there are no changes");
        }
    }];
}

- (void)saveTreeContext:(NSManagedObjectContext*)context
{
    if (!context) {
        return;
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"ERROR saving: %@", error);
    }
    
    if (context.parentContext) {
        [self saveTreeContext:context.parentContext];
    }
}

#pragma mark - PATHS

- (NSURL *)storeURL
{
    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    return [[self applicationStoresDirectory] URLByAppendingPathComponent:storeFilename];
}

- (NSURL *)applicationStoresDirectory
{
    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    NSURL *storesDirectory = [[NSURL fileURLWithPath:[self applicationDocumentsDirectory]] URLByAppendingPathComponent:@"Stores"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtURL:storesDirectory withIntermediateDirectories:YES attributes:nil error:&error]) {
            VLog(@"Successfully created Stores directory");
            if (error) {
                VLog(@"FAILED to create Stores directory : %@", error);
            }
        }
    }
    return storesDirectory;
}

- (NSString *)applicationDocumentsDirectory
{
    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    return [NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - FAULTING (Memory Management)

- (void)faultObjectWithID:(NSManagedObjectID*)objectID inContext:(NSManagedObjectContext*)context
{
    //    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    if (!objectID || !context) { return; }
    
    //    [context performBlockAndWait:^{
    NSManagedObject *object = [context objectWithID:objectID];
    if (object.hasChanges) {
        NSError *error = nil;
        if (![context save:&error]) {
            VLog(@"ERROR saving: %@", error);
        }
    }
    if (!object.isFault) {
        //            VLog(@"Faulting object %@ in context %@", object.objectID, context);
        [context refreshObject:object mergeChanges:NO];
    }
    else {
        //            VLog(@"Skipped faulting an object that is already a fault");
    }
    // Repeat the process if the context has a parent
    if (context.parentContext) {
        [self faultObjectWithID:objectID inContext:context.parentContext];
    }
    
    //    }];
}

#pragma mark - MIGRATION

- (BOOL)isCompatibleWithCoreDataMetadata:(NSURL *)storeUrl
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *filePath = [NSString stringWithFormat:@"%@", storeUrl.path];
    
    if ([fm fileExistsAtPath:filePath]) {
        NSLog(@"checking model for compatibility...");
        
        NSDictionary *sqliteConfig = @{@"journal_mode" : @"DELETE"};//@{@"journal_mode" : @"WAL"}
        NSDictionary *options = @{
                                  NSMigratePersistentStoresAutomaticallyOption  : @YES ,
                                  NSInferMappingModelAutomaticallyOption        : @YES , //Light Weight Migration
                                  NSSQLitePragmasOption                         : sqliteConfig
                                  };
        NSError *error = nil;
        NSDictionary *metaData = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:storeUrl options:options error:&error];
        
        NSManagedObjectModel *model = self.coordinator.managedObjectModel;
        
        if ([model isConfiguration:nil compatibleWithStoreMetadata:metaData]) {
            NSLog(@"model is compatible...");
            return YES;
            
        } else {
            
            if ([fm removeItemAtPath:filePath error:NULL]) {
                NSLog(@"removed file : %@", filePath);
                NSLog(@"model is incompatible...");
                return NO;
            }//REMOVE THE STORE FILE
            
        }
    }
    
    NSLog(@"file does not exists..");
    return NO;
}

- (BOOL) isMigrationNecessaryForStore:(NSURL *)storeUrl
{
    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self storeURL].path]) {
        VLog(@"SKIPPED MIGRATION: Source database missing.");
        return NO;
    }

    NSDictionary *sqliteConfig = @{@"journal_mode" : @"DELETE"};//@{@"journal_mode" : @"WAL"}
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption  : @YES ,
                              NSInferMappingModelAutomaticallyOption        : @YES , //Light Weight Migration
                              NSSQLitePragmasOption                         : sqliteConfig
                              };
    NSError *error = nil;
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:storeUrl options:options error:&error];

    NSManagedObjectModel *destinationModel = _coordinator.managedObjectModel;
    if ([destinationModel isConfiguration:nil compatibleWithStoreMetadata:sourceMetadata]) {
        VLog(@"SKIPPED MIGRATION: Source is already compatible");
        return NO;
    }
    
    return YES;
}

- (BOOL)migrateStore:(NSURL *)sourceStore
{
    VLog(@"SKIPPED MIGRATION: Source is already compatible");
    
    BOOL success = NO;
    NSError *error = nil;
    
    
    NSDictionary *sqliteConfig = @{@"journal_mode" : @"DELETE"};//@{@"journal_mode" : @"WAL"}
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption  : @YES ,
                              NSInferMappingModelAutomaticallyOption        : @YES , //Light Weight Migration
                              NSSQLitePragmasOption                         : sqliteConfig
                              };
    //STEP 1 - Gather the Source, Destination and Mapping Model
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:sourceStore options:options error:&error];
    
    NSManagedObjectModel *sourceModel = [NSManagedObjectModel mergedModelFromBundles:nil forStoreMetadata:sourceMetadata];
    NSManagedObjectModel *destinModel = _model;
    NSMappingModel *mappingModel = [NSMappingModel mappingModelFromBundles:nil forSourceModel:sourceModel destinationModel:destinModel];
    
    //STEP 2 - Perform Migration, assuming the mapping model isn't null
    if (mappingModel) {
        NSMigrationManager *migrationManager = [[NSMigrationManager alloc] initWithSourceModel:sourceModel destinationModel:destinModel];
        
        //OBSERVER
        [migrationManager addObserver:self forKeyPath:@"migrationProgress" options:NSKeyValueObservingOptionNew context:NULL];
        
        NSURL *destinStore = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Temp.sqlite"];
        success = [migrationManager migrateStoreFromURL:sourceStore
                                                   type:NSSQLiteStoreType
                                                options:nil
                                       withMappingModel:mappingModel
                                       toDestinationURL:destinStore
                                        destinationType:NSSQLiteStoreType
                                     destinationOptions:nil
                                                  error:&error];
        if (success) {
            //STEP 3 - Replace the old store with the new migrated store
            if ([self replaceStore:sourceStore withStore:destinStore]) {
                VLog(@"SUCCESSFULLY MIGRATED %@ to the Current Model", sourceStore.path);
                [migrationManager removeObserver:self forKeyPath:@"migrationProgress"];
            }
            //STEP 3
        }
        else {
            VLog(@"FAILED MIGRATION : %@", error);
        }
    }
    else {
        VLog(@"FAILED MIGRATION: Mapping Model is null");
    }
    
    return YES;// indicates migration has finished, regardless of outcome
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"migrationProgress"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            float progress = [[change objectForKey:NSKeyValueChangeNewKey] floatValue];
            int percentage = progress * 100;
            NSString *string = [NSString stringWithFormat:@"Migration Progress: %i%%", percentage];
            NSLog(@"%@", string);
            VLog(@"%@",string);
        });
    }
}

- (BOOL)replaceStore:(NSURL *)old withStore:(NSURL *)new
{
    BOOL success = NO;
    NSError *Error = nil;
    if ([[NSFileManager defaultManager] removeItemAtURL:old error:&Error]) {
        
        Error = nil;
        if ([[NSFileManager defaultManager] moveItemAtURL:new toURL:old error:&Error]) {
            success = YES;
        }
        else {
            VLog(@"FAILED to re-home new store %@", Error);
        }
    }
    else {
        VLog(@"FAILED to remove old store %@: Error:%@", old, Error);
    }
    return success;
}

- (void)performBackgroundManagedMigrationForStore:(NSURL *)storeURL
{
    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    // Perform migration in the background, so it doesn't freeze the UI.
    // This way progress can be shown to the user
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        BOOL done = [self migrateStore:storeURL];
        if (done) {
            // When migration finishes, add the newly migrated store
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error = nil;
                _store = [_coordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                    configuration:nil
                                                              URL:[self storeURL]
                                                          options:nil
                                                            error:&error];
                if (!_store) {
                    //                    VLog(@"Failed to add a migrated store. Error: %@", error);
                    //                    abort();
                }
                else {
                    VLog(@"Successfully added a migrated store : %@", _store);
                }
                
            });
        }
    });
}

#pragma mark - WORKER (Methods)

- (NSString *)emptyString:(NSString *)value {
    
    if ([value isEqualToString:@"(null)"]) {
        return @"";
    }
    
    return value;
}

- (NSPredicate *)predicateForKeyPath:(NSString *)keypath andValue:(NSString *)value
{
    // create left and right expression
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    
    // Predicate options
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption | NSCaseInsensitivePredicateOption;
    
    // Create predicate
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:options];
    return predicate;
}

- (NSPredicate *)predicateForKeyPathContains:(NSString *)keypath value:(NSString *)value {
    NSExpression *left = [NSExpression expressionForKeyPath:keypath];
    NSExpression *right = [NSExpression expressionForConstantValue:value];
    NSComparisonPredicateOptions predicateOptions = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSContainsPredicateOperatorType
                                                                        options:predicateOptions];
    return predicate;
}

- (NSManagedObject *)getEntity:(NSString *)entity attribute:(NSString *)attribute parameter:(NSString *)parameter context:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (parameter) {
        
        // create predicate
        NSPredicate *predicate = [self predicateForKeyPath:attribute andValue:parameter];
        
        // set predicate
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (NSManagedObject *)getEntity:(NSString *)entity predicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)context
{
    //    VLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        VLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return nil;
}

- (BOOL)clearContentsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    [fetchRequest setPredicate:predicate];
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *lmo in items) {
            [context deleteObject:lmo];
        }
        [self saveTreeContext:context];
        
        return YES;
    }
    
    return NO;
}

- (BOOL)clearDataForEntity:(NSString *)entity withPredicate:(NSPredicate *)predicate context:(NSManagedObjectContext *)ctx {
    
    //CLEAR CONTENTS
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (predicate != nil) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *mo in items) {
            [ctx deleteObject:mo];
        }
        [self saveTreeContext:ctx];
    }
    
    return YES;
}

- (NSUInteger)countRecordsForEntity:(NSString *)entity {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
    [request setIncludesSubentities:NO]; //Omit subentities. Default is YES (i.e. include subentities)
    
    NSManagedObjectContext *ctx = _mainContext;
    
    NSError *err;
    NSUInteger count = [ctx countForFetchRequest:request error:&err];
    //    if(count == NSNotFound) {
    //    }
    
    return count;
}

- (void)moveTextBookResources {
    
    AccountInfo *account = [Utils getArchive:kGlobalAccount];
    if (account) {
        NSString *user_name_object = [NSString stringWithFormat:@"%@", [VSmart sharedInstance].account.user.email];
        
        if ([user_name_object isEqualToString:@"(null)"] || [user_name_object isEqualToString:@"<null>"] || [user_name_object isEqualToString:@"null"]) {
            user_name_object = [NSString stringWithFormat:@"%@", [VSmart sharedInstance].account.user.email];
        }
        NSString *useridPATH = VS_FMT(@"%@_%i", user_name_object, account.user.id);
        //        NSString *useridPATH = [NSString stringWithFormat:@"%@_%i", user_name_object, account.user.id];
        NSLog(@"USERID PATH : %@", useridPATH);
        
        // 1 check book folder for username_userid
        NSString *path = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *bookDirectoryString = [NSString stringWithFormat:@"%@/Books", path];
        
        NSFileManager *fm = [NSFileManager defaultManager];
        NSDirectoryEnumerator *dirEnumerator = [fm enumeratorAtURL:[NSURL fileURLWithPath:bookDirectoryString]
                                        includingPropertiesForKeys:@[NSURLNameKey, NSURLIsDirectoryKey]
                                                           options:NSDirectoryEnumerationSkipsSubdirectoryDescendants
                                                      errorHandler:nil];
        NSMutableArray *migration_list = [NSMutableArray array];
        for (NSURL *theURL in dirEnumerator) {
            NSString *fileName = nil;
            [theURL getResourceValue:&fileName forKey:NSURLNameKey error:NULL];
            NSNumber *isDirectory = nil;
            [theURL getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:NULL];
            if([isDirectory boolValue] == YES) {
                if ([fileName isEqualToString:useridPATH]) {
                    [migration_list addObject:fileName];
                    break;
                }
            }
        }
        
        NSLog(@"MIGRATION LIST : %@", migration_list);
        if (migration_list.count > 0) {
            
            NSString *file_name = migration_list.lastObject;
            NSString *source_path = [NSString stringWithFormat:@"%@/%@", bookDirectoryString, file_name ];
            NSString *destination_path = [NSString stringWithFormat:@"%@/%@", bookDirectoryString, user_name_object ];
            
            //PERFORM DIRECTORY STRUCTURE CHANGES
            NSError *error = nil;
            BOOL move_status = [fm moveItemAtPath:source_path toPath:destination_path error:&error];
            
            if (move_status) {
                //PERFORM CORE DATA CHANGES
                NSBatchUpdateRequest *req = [[NSBatchUpdateRequest alloc] initWithEntityName:kTextBookEntity];
                req.propertiesToUpdate = @{ @"user_id" : user_name_object };
                req.resultType = NSUpdatedObjectsCountResultType;
                
                NSError *err_object = nil;
                NSBatchUpdateResult *res = (NSBatchUpdateResult *)[self.masterContext executeRequest:req error:&err_object];
                NSLog(@"%@ objects updated", res.result);
            }
        }
    }
}

- (void)updateResourceWithObject:(NSDictionary *)data
{
    NSManagedObjectContext *ctx = _workerContext;
    
    [ctx performBlock:^{
        
        NSString *book_id = data[@"book_id"];
        NSString *user_id = data[@"user_id"];
        NSPredicate *p1 = [self predicateForKeyPath:@"book_id" andValue:book_id];
        NSPredicate *p2 = [self predicateForKeyPath:@"user_id" andValue:user_id];
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates: @[p1, p2]];
        
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTextBookEntity];
        [fetchRequest setPredicate:predicate];
        NSError *error = nil;
        NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
        
        NSManagedObject *mo = nil;
        if ([items count] > 0) {
            mo = (NSManagedObject *)[items lastObject];
        }
        
        if (mo == nil) {
            mo = [NSEntityDescription insertNewObjectForEntityForName:kTextBookEntity inManagedObjectContext:ctx];
        }
        
        Book *b = data[@"book"];
        NSString *thumbnail_url = [NSString stringWithFormat:@"%@", b.cover];
        
        NSData *image_data = [mo valueForKey:@"thumbnail"];
        if (image_data == nil) {
            [self prefetchImageDataUsingBookID:book_id withURL:thumbnail_url];
        }
        [mo setValue:data[@"book_id"] forKey:@"book_id"];
        [mo setValue:data[@"book"] forKey:@"book"];
        
        float dataProgress = [data[@"progress"] floatValue];//notif
        float moProgress = [[mo valueForKey:@"progress"] floatValue];//stored
        if (dataProgress > moProgress) {
            [mo setValue:data[@"progress"] forKey:@"progress"];
        }
        [mo setValue:data[@"download_active"] forKey:@"download_active"];
        [mo setValue:data[@"download_status"] forKey:@"download_status"];
        [mo setValue:data[@"user_id"] forKey:@"user_id"];
        [mo setValue:thumbnail_url forKey:@"thumbnail_url"];
        
        [self faultObjectWithID:mo.objectID inContext:ctx];
        
        [ctx save:&error];
        
    }];
}

- (void)updateResourceWithObject:(NSDictionary *)data import:(BOOL)flag {
    
    NSManagedObjectContext *ctx = _workerContext;
    
    [ctx performBlock:^{
        
        NSString *book_id = data[@"book_id"];
        NSString *user_id = data[@"user_id"];
        NSPredicate *p1 = [self predicateForKeyPath:@"book_id" andValue:book_id];
        NSPredicate *p2 = [self predicateForKeyPath:@"user_id" andValue:user_id];
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates: @[p1, p2]];
        
        VLog(@"[%@] %@", user_id, book_id);
        
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTextBookEntity];
        [fetchRequest setPredicate:predicate];
        NSError *error = nil;
        NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
        
        NSManagedObject *mo = nil;
        
        //IF IT EXISTS
        if ([items count] > 0) {
            mo = (NSManagedObject *)[items lastObject];
        }
        
        //IF IT DOESN'T EXISTS
        if (mo == nil) {
            mo = [NSEntityDescription insertNewObjectForEntityForName:kTextBookEntity inManagedObjectContext:ctx];
        }
        
        Book *b = data[@"book"];
        NSString *thumbnail_url = [NSString stringWithFormat:@"%@", b.cover];
        NSLog(@"thumbnail_url : %@", thumbnail_url);
        
        NSData *image_data = [mo valueForKey:@"thumbnail"];
        if (image_data == nil) {
            
            if (flag == YES) {
                [self storeImageDataUsingBookObject:mo withPath:thumbnail_url];
            }
            
            if (flag == NO) {
                [self prefetchImageDataUsingBookID:book_id withURL:thumbnail_url];
            }
            
        }
        [mo setValue:data[@"book_id"] forKey:@"book_id"];
        [mo setValue:data[@"book"] forKey:@"book"];
        
        float dataProgress = [data[@"progress"] floatValue];//notif
        float moProgress = [[mo valueForKey:@"progress"] floatValue];//stored
        if (dataProgress > moProgress) {
            [mo setValue:data[@"progress"] forKey:@"progress"];
        }
        [mo setValue:data[@"download_active"] forKey:@"download_active"];
        [mo setValue:data[@"download_status"] forKey:@"download_status"];
        [mo setValue:data[@"user_id"] forKey:@"user_id"];
        [mo setValue:thumbnail_url forKey:@"thumbnail_url"];
        
        [self saveTreeContext:ctx];
        
    }];
}

- (void)storeImageDataUsingBookObject:(NSManagedObject *)bookObject withPath:(NSString *)path {
    
    if (bookObject != nil) {
        NSData *data = [[NSFileManager defaultManager] contentsAtPath:path];
        NSLog(@"%s link: %@ data: %@", __PRETTY_FUNCTION__, path, data );
        if (data != nil) {
            [bookObject setValue:data forKey:@"thumbnail"];
            [bookObject setValue:path forKey:@"thumbnail_url"];
        }
    }
}

- (void)prefetchImageDataUsingBookID:(NSString *)bookid withURL:(NSString *)url {
    
    NSURLSession *s = [NSURLSession sharedSession];
    NSURL *link = [NSURL URLWithString:url];
    NSURLSessionDownloadTask *dt = [s downloadTaskWithURL:link completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        if (location != nil) {
            
            //            NSLog(@"%s link: %@", __PRETTY_FUNCTION__, location.absoluteString );
            
            NSData *data = [NSData dataWithContentsOfURL:location];
            
            NSManagedObjectContext *ctx = _workerContext;
            NSManagedObject *mo = [self getEntity:kTextBookEntity attribute:@"book_id"
                                        parameter:bookid
                                          context:ctx];
            if (mo != nil) {
                if (data != nil) {
                    [mo setValue:data forKey:@"thumbnail"];
                    [self saveTreeContext:ctx];
                }
            }
        }
    }];
    
    [dt resume];
}

- (void)prefetchImageDataUsingBookID:(NSString *)bookid userid:(NSString *)userid withURL:(NSString *)url {
    
    NSURLSession *s = [NSURLSession sharedSession];
    NSURL *link = [NSURL URLWithString:url];
    NSURLSessionDownloadTask *dt = [s downloadTaskWithURL:link completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        if (location) {
            
            NSLog(@"%s link: %@", __PRETTY_FUNCTION__, location.absoluteString );
            
            NSData *data = [NSData dataWithContentsOfURL:location];
            NSManagedObjectContext *ctx = _workerContext;
            
            NSPredicate *p1 = [self predicateForKeyPath:@"book_id" andValue:bookid];
            NSPredicate *p2 = [self predicateForKeyPath:@"user_id" andValue:userid];
            NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates: @[p1, p2]];
            NSManagedObject *mo = [self getEntity:kTextBookEntity predicate:predicate context:ctx];
            if (mo) {
                
                if (data) {
                    [mo setValue:data forKey:@"thumbnail"];
                    [self saveTreeContext:ctx];
                }
                
            }
        }
    }];
    
    [dt resume];
}

- (void)deleteManagedObject:(NSManagedObject *)object
{
    //Update UI context
    NSManagedObjectContext *ctx = self.mainContext;
    
    [ctx deleteObject:object];
    
    //delete operation upto master context
    [self saveContext];
}

- (void)setSchoolCodeValue:(NSString *)code redirect:(BOOL)redirect
{
    NSManagedObjectContext *ctx = self.mainContext;
    
    // clear everything
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kSchoolCodeEntity];
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *mo in items) {
            [self deleteManagedObject:mo];
        }
    }
    
    // insert new data
    NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kSchoolCodeEntity inManagedObjectContext:ctx];
    [mo setValue:code forKey:@"code"];
    [mo setValue:[NSNumber numberWithBool:redirect] forKey:@"redirect"];
    
    [self faultObjectWithID:mo.objectID inContext:ctx];
}

- (NSString *)getSchoolCodeValue
{
    NSManagedObjectContext *ctx = _masterContext;
    NSManagedObject *mo = [self getEntity:kSchoolCodeEntity attribute:@"code" parameter:nil context:ctx];
    return [NSString stringWithFormat:@"%@",[mo valueForKey:@"code"]];
}

#pragma mark - <PEARSON COURSE>

// -----------------------------------
// PEARSON COURSE API
// -----------------------------------

- (NSString *)baseURL {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *server = [NSString stringWithFormat:@"%@", [defaults stringForKey:@"baseurl_preference"] ];
    
    //    NSString *server = @"172.16.7.174"; //HARD CODING
    
    return server;
}

//- (void)requestCourseListForUserID:(NSString *)userid doneBlock:(ResourceManagerDoneBlock)doneBlock
//{
//    NSString *quizlistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointCourseList, userid]];
//    NSLog(@"quiz path : %@", quizlistPath);
//    NSURL *quizlistURL = [NSURL URLWithString:quizlistPath];
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:quizlistURL body:nil];
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
//          {
//              if (error) {
//                  NSLog(@"error %@", [error localizedDescription]);
//              }
//              
//              if (!error) {
//                  
//                  NSDictionary *dictionary = [self parseResponseData:responsedata];
//                  NSDictionary *meta = dictionary[@"_meta"];
//                  NSString *metaCount = [self stringValue:meta[@"count"]];
//                  NSString *metaStatus = meta[@"status"];
//                  
//                  NSManagedObjectContext *ctx = self.workerContext;
//                  
//                  if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
//                      [ctx performBlock:^{
//                          [self clearDataForEntity:kCourseEntity withPredicate:nil context:ctx];
//                          [self clearDataForEntity:kGradeBookEntity withPredicate:nil context:ctx];
//                      }];
//                      
//                      if (doneBlock) {
//                          doneBlock(NO);
//                      }
//                      
//                      return;
//                  }
//                  
//                  if (dictionary) {
//                      //                NSLog(@"dictionary : %@", dictionary);
//                      
//                      NSArray *records = dictionary[@"records"];
//                      
//                      if (records > 0) {
//                          
//                          NSManagedObjectContext *ctx = _workerContext;
//                          
//                          [ctx performBlock:^{
//                              
//                              //CLEAR CONTENTS (IMPORTANT)
//                              [self clearDataForEntity:kCourseEntity withPredicate:nil context:ctx];
//                              
//                              NSUInteger indexcount = 0;
//                              
//                              //REFRESH DATA
//                              for (NSDictionary *d in records) {
//                                  
//                                  /*
//                                   {
//                                   "course_id": "1",
//                                   "course_name": "English 6",
//                                   "initial": "",
//                                   "description": "English",
//                                   "course_code": "ENGLISH",
//                                   "cs_id": "9",
//                                   "section_id": "1",
//                                   "schedule": "TBA",
//                                   "venue": "TBA"
//                                   }
//                                   */
//                                  
//                                  NSString *course_code = [NSString stringWithFormat:@"%@", d[@"course_code"] ];
//                                  NSString *course_description = [NSString stringWithFormat:@"%@", d[@"description"] ];
//                                  NSString *course_id = [NSString stringWithFormat:@"%@", d[@"course_id"] ];
//                                  NSString *course_index = [NSString stringWithFormat:@"%lu", (unsigned long)indexcount ];
//                                  NSString *course_name = [NSString stringWithFormat:@"%@", d[@"course_name"] ];
//                                  NSString *cs_id = [NSString stringWithFormat:@"%@", d[@"cs_id"] ];
//                                  NSString *initial = [NSString stringWithFormat:@"%@", d[@"initial"] ];
//                                  NSString *schedule = [NSString stringWithFormat:@"%@", d[@"schedule"] ];
//                                  NSString *section_id = [NSString stringWithFormat:@"%@", d[@"section_id"] ];
//                                  NSString *venue = [NSString stringWithFormat:@"%@", d[@"venue"] ];
//                                  
//                                  NSString *search_string = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",
//                                                             course_description, course_name, initial, schedule, venue];
//                                  
//                                  //                              NSManagedObject *mo = [self getEntity:kCourseEntity attribute:@"course_id" parameter:course_id context:ctx];
//                                  
//                                  //                              if (mo == nil) {
//                                  NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kCourseEntity inManagedObjectContext:ctx];
//                                  //                              }
//                                  
//                                  [mo setValue:course_code forKey:@"course_code"];
//                                  [mo setValue:course_description forKey:@"course_description"];
//                                  [mo setValue:course_id forKey:@"course_id"];
//                                  [mo setValue:course_index forKey:@"course_index"];
//                                  [mo setValue:course_name forKey:@"course_name"];
//                                  [mo setValue:cs_id forKey:@"cs_id"];
//                                  [mo setValue:initial forKey:@"initial"];
//                                  [mo setValue:schedule forKey:@"schedule"];
//                                  [mo setValue:section_id forKey:@"section_id"];
//                                  [mo setValue:venue forKey:@"venue"];
//                                  [mo setValue:userid forKey:@"user_id"];
//                                  [mo setValue:search_string forKey:@"search_string"];
//                                  
//                                  indexcount++;
//                              }
//                              
//                              [self saveTreeContext:ctx];
//                              
//                              if (doneBlock) {
//                                  doneBlock(YES);
//                              }
//                          }];
//                      }
//                  }
//              }
//          }];
//    [task resume];
//}
//
//- (NSArray *)fetchCourseListForUserID:(NSString *)userid {
//    
//    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kCourseEntity];
//    
//    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"cs_id" ascending:YES];
//    [fetchRequest setSortDescriptors:@[descriptor]];
//    
//    NSManagedObjectContext *context = _workerContext;
//    
//    NSError *error = nil;
//    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
//    if (error) {
//        VLog(@"error: %@", [error localizedDescription]);
//    }
//    
//    
//    NSMutableArray *list = [NSMutableArray array];
//    if ([items count] > 0) {
//        for (NSManagedObject *mo in items) {
//            NSArray *keys = mo.entity.propertiesByName.allKeys;
//            NSMutableDictionary *d = [NSMutableDictionary dictionary];
//            for (NSString *k in keys) {
//                NSString *v = [NSString stringWithFormat:@"%@", [mo valueForKey:k] ];
//                [d setValue:v forKey:k];
//            }
//            [list addObject:d];
//        }
//    }
//    
//    return list;
                                                          //                              }
                                                          
// -----------------------------------
// PEARSON V-SMART ACTIVITY LOG API
// -----------------------------------

- (void) requestModuleTypes:(ResourceManagerDoneBlock)doneBlock
{
    NSLog(@"{PEARSON} %@", NSStringFromSelector(_cmd));
    
    NSString *dataUrl = [NSString stringWithFormat:@"http://%@/v1/module/types", [Utils getServiceAPI] ];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:dataUrl];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          NSString *dataString = [NSString stringWithUTF8String:[data bytes]];
                                          NSLog(@"data String : %@", dataString);
                                          
                                          if (error) {
                                              NSLog(@"Data Error : %@", error.localizedDescription);
                                          }
                                          
                                          NSError *jsonError;
                                          NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
                                          if (jsonError) {
                                              NSLog(@"JSON Error : %@", jsonError.localizedDescription);
                                          }
                                          
                                          NSArray *records = dictionary[@"records"];
                                          
                                          NSManagedObjectContext *ctx = _workerContext;
                                          
                                          [ctx performBlockAndWait:^{
                                              
                                              for (NSDictionary *d in records) {
                                                  
                                                  // PARSE COURSE ID
                                                  NSString *module_id = [NSString stringWithFormat:@"%@", d[@"id"]];
                                                  
                                                  // INITIALIZE ROW ITEM
                                                  NSManagedObject *mo = (NSManagedObject *)[self getEntity:kModuleEntity attribute:@"module_id" parameter:module_id context:ctx];
                                                  
                                                  // CREATE ROW IF IT DOES NOT EXIST
                                                  if (mo == nil) {
                                                      mo = [NSEntityDescription insertNewObjectForEntityForName:kModuleEntity inManagedObjectContext:ctx];
                                                  }
                                                  
                                                  /*
                                                   module_id
                                                   module_name
                                                   module_description
                                                   is_active
                                                   is_deleted
                                                   date_created
                                                   date_modified
                                                   */
                                                  
                                                  NSString *module_name = [NSString stringWithFormat:@"%@", d[@"name"] ];
                                                  NSString *module_description = [NSString stringWithFormat:@"%@", d[@"description"] ];
                                                  NSString *is_active = [NSString stringWithFormat:@"%@", d[@"is_active"]];
                                                  NSString *is_deleted = [NSString stringWithFormat:@"%@", d[@"is_deleted"]];
                                                  NSString *date_created = [NSString stringWithFormat:@"%@", d[@"date_created"]];
                                                  NSString *date_modified = [NSString stringWithFormat:@"%@", d[@"date_modified"]];
                                                  
                                                  [mo setValue:module_id forKey:@"module_id"];
                                                  [mo setValue:module_name forKey:@"module_name"];
                                                  [mo setValue:module_description forKey:@"module_description"];
                                                  [mo setValue:is_active forKey:@"is_active"];
                                                  [mo setValue:is_deleted forKey:@"is_deleted"];
                                                  [mo setValue:date_created forKey:@"date_created"];
                                                  [mo setValue:date_modified forKey:@"date_modified"];
                                              }
                                              
                                              NSError *ctxerror = nil;
                                              if (![ctx save:&ctxerror]) {
                                                  NSLog(@"ERROR saving: %@", ctxerror);
                                              }
                                              
                                          }];
                                          
                                          if (records.count > 0) {
                                              if (doneBlock) {
                                                  doneBlock(YES);
                                              }
                                          }
                                          
                                      }];
    
    [dataTask resume];
}

- (NSTimeInterval)parseTimeFromString:(NSString *)string {
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    
    NSInteger h, m, s;
    [scanner scanInteger:&h];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&m];
    [scanner scanString:@":" intoString:NULL];
    [scanner scanInteger:&s];
    
    return h * 3600 + m * 60 + s;
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (NSDate *)parseDateFromString:(NSString *)dateString {
    
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@".000Z" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    dateString = [dateString stringByReplacingOccurrencesOfString:@" togo" withString:@""];
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [self.formatter setTimeZone:gmt];
    NSString *template = @"yyyy-MM-dd HH:mm:ss";
    [self.formatter setDateFormat:template];
    NSDate *dateObject = [self.formatter dateFromString:dateString];
    return dateObject;
}

- (NSURL *)buildURL:(NSString *)string {
    
    NSString *path = [Utils buildUrl:string];
    NSLog(@"path : %@", path);
    return [NSURL URLWithString:path];
}

- (NSMutableURLRequest *)buildRequestWithMethod:(NSString *)method NSURL:(NSURL *)url body:(id)body {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:method];
    
    if (body) {
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:body options:0 error:&error];
        
        NSString *j_string = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        NSLog(@"j_string : <start>---- %@ ---<end>", j_string);
        [request setHTTPBody:postData];
    }
    
    return request;
}

- (void) stringByStrippingHTML:(NSString *)htmlString contentBlock:(ResourceManagerContent)contentBlock {
    
    NSError *error = nil;
    NSString *pattern = @"(<img\\s[\\s\\S]*?src\\s*?=\\s*?['\"](.*?)['\"][\\s\\S]*?>)+?";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    [regex enumerateMatchesInString:htmlString
                            options:0
                              range:NSMakeRange(0, [htmlString length])
                         usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                             
                             NSString *img = [htmlString substringWithRange:[result rangeAtIndex:2]];
                             if (contentBlock) {
                                 contentBlock([NSString stringWithFormat:@"%@", img]);
                             }
                         }];
}

- (NSUInteger)typeFromMessage:(NSString *)string {
    
    NSUInteger type = 0;
    
    
    if (string) {
        NSArray *tokens = [string componentsSeparatedByString:@" "];
        
        // more tokens
        if (tokens > 0) {
            for (NSString *w in tokens) {
                NSURL *url = [NSURL URLWithString:w];
                if (url) {
                    
                }
            }
        }
        
        
        
    }
    
    
    return type;
}


- (id)parseResponseData:(NSData *)data {
    
    if (data) {
        
        NSError *jsonError = nil;
        
        id object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
        
        NSString *jsonstring = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"<json> %@ <json>", jsonstring);
        
        if (jsonError) {
            NSLog(@"JSON Error : %@", jsonError.localizedDescription);
        }
        
        if (!jsonError) {
            return object;
        }
    }
    
    return nil;
}

- (NSMutableURLRequest *)buildRequestWithMethod:(NSString *)method url:(NSString *)string
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:string]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:method];
    
    return request;
}

- (void) requestLogActivityWithModuleType:(NSString *)type details:(NSString *)details
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSDictionary *activityLog = @{
                                  @"user_id" : [NSString stringWithFormat:@"%i",account.user.id],
                                  @"module_type_id": type,
                                  @"details" : details
                                  };
    
    NSString *path = [NSString stringWithFormat:@"http://%@/v1/activity/log", [Utils getServiceAPI]  ];
    NSLog(@"path : %@", path);
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:[NSURL URLWithString:path] body:activityLog];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              if (error) {
                                                  NSLog(@"error %@", [error localizedDescription]);
                                              }
                                              
                                              if (data != nil) {
                                                  NSError *jsonError;
                                                  NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
                                                  if (jsonError) {
                                                      NSLog(@"JSON Error : %@", jsonError.localizedDescription);
                                                  }
                                                  
                                                  NSLog(@"USER ACTIVITY LOG : %@", dictionary);
                                              }
                                              
                                          }];
    
    [postDataTask resume];
}

- (void) requestLogoutWithBlock:(ResourceManagerDoneBlock)doneBlock
{
    NSLog(@"{PEARSON} %@", NSStringFromSelector(_cmd));
    
    AccountInfo *info = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%i", info.user.id];
    NSString *api = [NSString stringWithFormat:@"http://%@/v1/users/logout/%@", [Utils getServiceAPI], user_id ];
    NSURL *url = [NSURL URLWithString:api];
    NSLog(@"url : %@", url);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          
                                          //        NSString *dataString = [NSString stringWithUTF8String:[data bytes]];
                                          //        NSLog(@"data string : %@", dataString);
                                          
                                          if (error) {
                                              NSLog(@"error %@", [error localizedDescription]);
                                          }
                                          
                                          if (data != nil) {
                                              NSError *jsonError;
                                              NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
                                              NSLog(@"dictionary : %@", dictionary);
                                              if (jsonError) {
                                                  NSLog(@"JSON Error : %@", jsonError.localizedDescription);
                                              }
                                          }
                                          
                                          if (doneBlock) {
                                              doneBlock(YES);
                                          }
                                      }];
    
    [dataTask resume];
}

// -----------------------------------
// PEARSON ANALYTICS API
// -----------------------------------

- (void)readAnalytics:(Course *)course doneBlock:(ResourceManagerDoneBlock)doneBlock
{
    NSLog(@"{PEARSON} %@", NSStringFromSelector(_cmd));
    
    //    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    //
    //    NSString *api_ip = [self baseURL];
    //
    //    NSDictionary *actor = @{
    //                            @"user_id" : [NSString stringWithFormat:@"%i", account.user.id],
    //                            @"email" : [NSString stringWithFormat:@"%@", account.user.email],
    //                            @"username" : [NSString stringWithFormat:@"%@", account.user.email],
    //                            @"first_name" : [NSString stringWithFormat:@"%@", account.user.firstname],
    //                            @"last_name" : [NSString stringWithFormat:@"%@", account.user.lastname]
    //                            };
    //
    //    NSDictionary *definition = @{
    //                                 @"course_id" : [NSString stringWithFormat:@"%@",course.course_id],
    //                                 @"name" : [NSString stringWithFormat:@"%@", course.course_name],
    //                                 @"description" : [NSString stringWithFormat:@"%@", course.course_summary]
    //                                 };
    //
    //    NSDictionary *object = @{
    //                             @"type" : @"course",
    //                             @"definition" : definition
    //                             };
    //
    //    NSDictionary *analytics = @{@"actor" : actor,
    //                                @"verb" : @"attempted",
    //                                @"object" : object};
    //
    //    NSURLSession *session = [NSURLSession sharedSession];
    //    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"http://%@/analytics/api/statements", api_ip ] ];
    //
    //
    //
    //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //    [request setHTTPMethod:@"POST"];
    //
    //    NSError *error;
    //    NSData *postData = [NSJSONSerialization dataWithJSONObject:analytics options:0 error:&error];
    //    [request setHTTPBody:postData];
    //    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    //        if (error) {
    //            NSLog(@"error %@", [error localizedDescription]);
    //        }
    //    }];
    //
    //    if (doneBlock) {
    //        doneBlock(YES);
    //    }
    //
    //    [postDataTask resume];
}

- (void)exitTopicAnalytics:(Course *)course doneBlock:(ResourceManagerDoneBlock)doneBlock
{
    //    NSLog(@"{PEARSON} %@", NSStringFromSelector(_cmd));
    //
    //    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    //
    //    NSString *api_ip = [self baseURL];
    //
    //    NSDictionary *actor = @{
    //                            @"user_id" : [NSString stringWithFormat:@"%i", account.user.id],
    //                            @"email" : [NSString stringWithFormat:@"%@", account.user.email],
    //                            @"username" : [NSString stringWithFormat:@"%@", account.user.email],
    //                            @"first_name" : [NSString stringWithFormat:@"%@", account.user.firstname],
    //                            @"last_name" : [NSString stringWithFormat:@"%@", account.user.lastname]
    //                            };
    //
    //    NSDictionary *definition = @{
    //                                 @"course_id" : [NSString stringWithFormat:@"%@",course.course_id],
    //                                 @"name" : [NSString stringWithFormat:@"%@", course.course_name],
    //                                 @"description" : [NSString stringWithFormat:@"%@", course.course_summary]
    //                                 };
    //
    //    NSDictionary *object = @{
    //                             @"type" : @"exited",
    //                             @"definition" : definition
    //                             };
    //
    //    NSDictionary *analytics = @{@"actor" : actor,
    //                                @"verb" : @"attempted",
    //                                @"object" : object};
    //
    //    NSURLSession *session = [NSURLSession sharedSession];
    //    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"http://%@/analytics/api/statements", api_ip ] ];
    //
    //
    //
    //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //    [request setHTTPMethod:@"POST"];
    //
    //    NSError *error;
    //    NSData *postData = [NSJSONSerialization dataWithJSONObject:analytics options:0 error:&error];
    //    [request setHTTPBody:postData];
    //    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    //        if (error) {
    //            NSLog(@"error %@", [error localizedDescription]);
    //        }
    //    }];
    //
    //    if (doneBlock) {
    //        doneBlock(YES);
    //    }
    //
    //    [postDataTask resume];
}

/////// SOCIAL STREAM //////
- (void)requestSocialStreamStickers:(ResourceManagerListBlock)listBlock {
    
    NSString *getPath = [Utils buildUrl:kEndPointGetStickers];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" url:getPath];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:data];
            if (dictionary) {
                //                NSLog(@"dictionary : %@", dictionary);
                
                NSArray *records = dictionary[@"records"];
                if (records > 0) {
                    
                    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                    
                    NSMutableArray *categoryList = [NSMutableArray array];
                    for (NSDictionary *d in records) {
                        
                        NSString *category = [NSString stringWithFormat:@"%@", d[@"category"] ];
                        NSString *cover_url = [NSString stringWithFormat:@"%@%@", homeurl, d[@"cover_url"] ];
                        NSArray *stickers = d[@"stickers"];
                        
                        NSMutableArray *list = [NSMutableArray array];
                        for (NSDictionary *i in stickers) {
                            
                            NSString *cat_id = [NSString stringWithFormat:@"%@", i[@"cat_id"] ];
                            NSString *sticker_id = [NSString stringWithFormat:@"%@", i[@"id"] ];
                            NSString *sticker_name = [NSString stringWithFormat:@"%@", i[@"name"] ];
                            NSString *sticker_url = [NSString stringWithFormat:@"%@%@", homeurl, i[@"sticker_url"] ];
                            NSString *stickerIMG = [NSString stringWithFormat:@"%@", i[@"sticker"] ];
                            
                            NSDictionary *item = @{@"catid" : cat_id,
                                                   @"id" : sticker_id,
                                                   @"name" : sticker_name,
                                                   @"sticker_url" : sticker_url,
                                                   @"sticker": stickerIMG};
                            
                            [list addObject:item];
                        }
                        
                        NSDictionary *c = @{@"category" : category,
                                            @"cover_url" : cover_url,
                                            @"stickers" : list};
                        
                        [categoryList addObject:c];
                    }
                    
                    
                    NSArray *list = [NSArray arrayWithArray:categoryList];
                    
                    if (listBlock) {
                        listBlock(list);
                    }
                    
                }
                
            }
        }
    }];
    
    
    [task resume];
}

- (void)requestSocialStreamEmojis:(ResourceManagerListBlock)listBlock {
    
    NSString *getPath = [Utils buildUrl:kEndPointGetEmojis];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" url:getPath];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:data];
            if (dictionary) {
                //                NSLog(@"dictionary : %@", dictionary);
                
                NSArray *records = dictionary[@"records"];
                if (records > 0) {
                    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                    
                    NSMutableArray *emojilist = [NSMutableArray array];
                    for (NSDictionary *d in records) {
                        
                        NSString *characters = [NSString stringWithFormat:@"%@", d[@"chars"] ];
                        NSString *icon_url = [NSString stringWithFormat:@"%@%@", homeurl, d[@"icon_url"] ];
                        NSString *emoji_id = [NSString stringWithFormat:@"%@", d[@"id"] ];
                        NSString *emoji_name = [NSString stringWithFormat:@"%@", d[@"name"] ];
                        
                        NSDictionary *i = @{@"chars":characters,
                                            @"icon_url":icon_url,
                                            @"id":emoji_id,
                                            @"name":emoji_name};
                        [emojilist addObject:i];
                    }
                    
                    NSArray *list = [NSArray arrayWithArray:emojilist];
                    if (listBlock) {
                        listBlock(list);
                    }
                }
            }
        }
    }];
    
    
    [task resume];
}

- (void)requestSocialStreamEmoticons:(ResourceManagerListBlock)listBlock {
    
    NSString *getPath = [Utils buildUrl:kEndPointGetEmoticons];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" url:getPath];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:data];
            if (dictionary) {
                //                NSLog(@"dictionary : %@", dictionary);
                
                NSArray *records = dictionary[@"records"];
                if (records > 0) {
                    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                    
                    NSMutableArray *emoticonslist = [NSMutableArray array];
                    for (NSDictionary *d in records) {
                        
                        NSString *characters = [NSString stringWithFormat:@"%@", d[@"chars"] ];
                        NSString *icon_url = [NSString stringWithFormat:@"%@%@", homeurl, d[@"icon_url"] ];
                        NSString *emoticon_id = [NSString stringWithFormat:@"%@", d[@"id"] ];
                        NSString *emoticon_name = [NSString stringWithFormat:@"%@", d[@"name"] ];
                        NSString *emoticon_text = [NSString stringWithFormat:@"%@", d[@"icon_text"] ];
                        
                        NSDictionary *i = @{@"chars":characters,
                                            @"icon_url":icon_url,
                                            @"id":emoticon_id,
                                            @"name":emoticon_name,
                                            @"icon_text":emoticon_text};
                        
                        [emoticonslist addObject:i];
                    }
                    
                    NSArray *list = [NSArray arrayWithArray:emoticonslist];
                    if (listBlock) {
                        listBlock(list);
                    }
                }
            }
        }
    }];
    
    
    [task resume];
}

- (NSString *)loginUser {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return user_id;
}

- (void)requestSocialStreamMessageWithGroupID:(NSString *)groupid doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSString *getPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointGetGroupMessage, groupid]];
    NSLog(@"PATH!! [%@]", getPath);
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" url:getPath];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:data];
            NSDictionary *meta = dictionary[@"_meta"];
            NSString *metaCount = [self stringValue:meta[@"count"]];
            NSString *metaStatus = meta[@"status"];
            
            NSManagedObjectContext *ctx = self.workerContext;
            // FIX VIBALHP
            if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
                [ctx performBlock:^{
                    [self clearDataForEntity:kSocialStreamFeedEntity withPredicate:nil context:ctx];
                }];
                
                if (doneBlock) {
                    doneBlock(NO);
                }
                
                return;
            }
            
            if (dictionary) {
                NSLog(@"dictionary : %@", dictionary);
                
                NSArray *records = dictionary[@"records"];
                if (records > 0) {
                    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                    
                    NSManagedObjectContext *ctx = _workerContext;
                    
                    [ctx performBlock:^{
                        
                        //CLEAR CONTENTS (IMPORTANT)
                        [self clearDataForEntity:kSocialStreamFeedEntity withPredicate:nil context:ctx];
                        
                        //REFRESH DATA
                        for (NSDictionary *d in records) {
                            
                            NSManagedObject *mo = nil;
                            
                            [self processData:d
                                managedObject:mo
                                      context:ctx
                                      options:homeurl];
                        }
                        
                        [self saveTreeContext:ctx];
                        
                    }];
                    
                    
                    if (doneBlock) {
                        doneBlock(YES);
                    }
                }
                
            }
            
            if (dictionary == nil) {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
            
            
        }
    }];
    
    [task resume];
}

- (void)requestSocialStreamMessageWithGroupID:(NSString *)groupid dataBlock:(ResourceManagerDataBlock)dataBlock {
    NSString *getPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointGetGroupMessage, groupid]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" url:getPath];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (dataBlock) {
                dataBlock(nil);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:data];
            NSDictionary *meta = dictionary[@"_meta"];
            NSString *metaCount = [self stringValue:meta[@"count"]];
            NSString *metaStatus = meta[@"status"];
            
            NSManagedObjectContext *ctx = self.workerContext;
            
            if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
                [self clearDataForEntity:kSocialStreamFeedEntity withPredicate:nil context:ctx];
                
                if (dataBlock) {
                    dataBlock(nil);
                }
                
                return;
            }
            
            if (dictionary) {
                NSLog(@"dictionary : %@", dictionary);
                NSArray *records = dictionary[@"records"];
                NSInteger count = records.count;
                
                if (records.count > 0) {
                    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                    
                    [ctx performBlock:^{
                        [self clearDataForEntity:kSocialStreamFeedEntity withPredicate:nil context:ctx];
                        
                        for (NSDictionary *d in records) {
                            NSManagedObject *mo = nil;
                            [self processData:d managedObject:mo context:ctx options:homeurl];
                        }
                        
                        [self saveTreeContext:ctx];
                    }];
                    
                    if (dataBlock) {
                        NSDictionary *data = @{@"count":@(count)};
                        dataBlock(data);
                    }
                }
            }
            
            if (dictionary == nil) {
                if (dataBlock) {
                    dataBlock(nil);
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestSocialStreamPrevMessageWithGroupID:(NSUInteger)groupid messageID:(NSUInteger)messageid doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSString *getPath = [Utils buildUrl:[NSString stringWithFormat:@"/vsmart-rest-dev/v1/stream/getprevgroupmessages/%lu/%lu", (unsigned long)groupid, (unsigned long)messageid] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" url:getPath];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (![response.MIMEType isEqualToString:@"application/json"]) {
            if (doneBlock) {
                doneBlock(NO);
                return;
            }
        }
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:data];
            NSDictionary *meta = dictionary[@"_meta"];
            NSString *metaCount = [self stringValue:meta[@"count"]];
            NSString *metaStatus = meta[@"status"];
            
            NSManagedObjectContext *ctx = self.workerContext;
            
            if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
                [ctx performBlock:^{
                    //                    [self clearDataForEntity:kSocialStreamFeedEntity withPredicate:nil context:ctx];
                }];
                
                if (doneBlock) {
                    doneBlock(NO);
                }
                
                return;
            }
            
            if (dictionary) {
                NSLog(@"dictionary : %@", dictionary);
                
                NSArray *records = dictionary[@"records"];
                if (records > 0) {
                    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                    
                    NSManagedObjectContext *ctx = _workerContext;
                    
                    [ctx performBlock:^{
                        
                        //REFRESH DATA
                        for (NSDictionary *d in records) {
                            
                            NSManagedObject *mo = nil;
                            
                            [self processData:d
                                managedObject:mo
                                      context:ctx
                                      options:homeurl];
                        }
                        
                        [self saveTreeContext:ctx];
                        
                    }];
                    
                    
                    if (doneBlock) {
                        doneBlock(YES);
                    }
                }
                
            }
            if (doneBlock) {
                doneBlock(NO);
            }
        }
    }];
    
    [task resume];
}

- (NSString *)messageTypeForObject:(NSDictionary *)data {
    
    NSString *type = @"message";
    
    if (data) {
        type = [NSString stringWithFormat:@"%@", data[@"msg_type"] ];
    }
    
    return type;
}

- (void)processData:(NSDictionary *)d managedObject:(NSManagedObject *)mo context:(NSManagedObjectContext *)ctx options:(NSString *)options {
    
    /*
     attachCount = 0;//checked
     avatar = "/socialstream/public/img/avatar/generic-boy.png";//checked
     "current_date" = "2015-01-20 14:24:07";//checked
     "date_created" = "2015-01-20 11:43:14";//checked
     "date_modified" = "2015-01-20 11:43:14";//checked
     "emoticon_id" = 7;//checked
     "user_id" = 123; //checked
     username = yyu; //checked
     "first_name" = Yuan;//checked
     "last_name" = Yu;//checked
     "group_id" = 12;//checked
     "icon_text" = "disappointed."; //checked
     "icon_url" = "/socialstream/img/emoji/disappointed.png";//checked
     id = 489; //checked
     "is_attached" = 0; //checked
     "is_deleted" = 0; //checked
     message = "<img src='/socialstream/public/img/stickers/cutie1.png'/>"; //checked
     "post_likes" = (); //checked
     comments = (); //checked
     
     "url_data": [
     {
     "content_id": "190",
     "uuid": "f46d3743e2874f15bd5863ed02e8f1e5",
     "text": " ",
     "image": "no",
     "thumb_img": "no",
     "title": "N\/A",
     "canonicalUrl": "",
     "url": "",
     "description": "N\/A",
     "is_video": "no",
     "is_image": "no",
     "msg_type": "sticker"
     }
     ],
     */
    
    NSString *homeurl = options;
    NSString *messageid = [NSString stringWithFormat:@"%@", d[@"id"] ];
    NSString *message = [NSString stringWithFormat:@"%@", d[@"message"] ];
    
    NSString *type = @"message";
    NSArray *urlDataItems = d[@"url_data"];
    if (urlDataItems.count > 0) {
        NSDictionary *url_data_obj = (NSDictionary *)[urlDataItems lastObject];
        NSString *canonicalUrl = [NSString stringWithFormat:@"%@", url_data_obj[@"canonicalUrl"] ];
        canonicalUrl = [canonicalUrl stringByReplacingOccurrencesOfString:@"<null>" withString:@""];
        
        type = [self messageTypeForObject:url_data_obj];
        if ([type isEqualToString:@"message"]) {
            if (canonicalUrl.length > 0) {
                type = @"image";
            }
        }
        
        if ([type isEqualToString:@"sticker"]) {
            
            if (canonicalUrl.length == 0) {
                __block NSString *msg;
                [self stringByStrippingHTML:message contentBlock:^(NSString *content) {
                    msg = [NSString stringWithFormat:@"%@%@",homeurl,content];
                }];
                message = [NSString stringWithFormat:@"%@", msg];
            }
            
            if (canonicalUrl.length > 0) {
                type = @"image";
            }
            
        }
        
        if ([type isEqualToString:@"image"] || [type isEqualToString:@"video"]) {
            [self parseUrlData:urlDataItems messageid:message context:ctx options:options];
        }
    }
    
    NSString *attachCount = [NSString stringWithFormat:@"%@", d[@"attachCount"] ];
    NSString *avatar = [NSString stringWithFormat:@"%@%@",homeurl,d[@"avatar"] ];
    NSString *current_date = [NSString stringWithFormat:@"%@", d[@"current_date"] ];
    NSDate *modifiedDate = [self parseDateFromString:d[@"date_modified"]];
    NSString *relativeDate = [modifiedDate timeAgo];
    
    NSString *date_created = [NSString stringWithFormat:@"%@", d[@"date_created"] ];
    NSString *date_modified = [NSString stringWithFormat:@"%@", relativeDate ];
    NSString *emoticon_id = [NSString stringWithFormat:@"%@", d[@"emoticon_id"] ];
    NSString *user_id = [NSString stringWithFormat:@"%@", d[@"user_id"] ];
    NSString *username = [NSString stringWithFormat:@"%@", d[@"username"] ];
    NSString *first_name = [NSString stringWithFormat:@"%@", d[@"first_name"] ];
    NSString *last_name = [NSString stringWithFormat:@"%@", d[@"last_name"] ];
    NSString *group_id = [NSString stringWithFormat:@"%@", d[@"group_id"] ];
    
    NSString *icon_text = [NSString stringWithFormat:@"%@", d[@"icon_text"] ];
    if ([icon_text isEqualToString:@"<null>"]) {
        icon_text = @"";
    }
    
    NSString *icon_url = [NSString stringWithFormat:@"%@%@",homeurl, d[@"icon_url"] ];
    NSString *is_attached = [NSString stringWithFormat:@"%@", d[@"is_attached"] ];
    NSString *is_deleted = [NSString stringWithFormat:@"%@", d[@"is_deleted"] ];
    
    // LIKES
    NSString *is_liked = @"0";
    NSArray *likeList = d[@"post_likes"];
    NSString *like_count = [NSString stringWithFormat:@"%lu", (unsigned long)likeList.count];
    NSString *post_likes = [NSString stringWithFormat:@"%@", [(d[@"post_likes"]) componentsJoinedByString:@"|"]];
    
    // BUG FIX #124
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *account_user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    if (likeList.count > 0) {
        
        //FIND Current User ID for button is liked
        //for (NSDictionary *l in likeList) {
        //    is_liked = ([l[@"user_id"] isEqualToString:account_user_id]) ? @"1" : @"0";
        //    NSLog(@"IS LIKED? [%@]", is_liked);
        //   break;
        //}
        
        // Enhancement #1303
        // jca-05062016
        // Should loop through the list of users who liked the status to determine
        // if the current user liked other user's status
        for (NSDictionary *l in likeList) {
            is_liked = ([l[@"user_id"] isEqualToString:account_user_id]) ? @"1" : @"0";
            NSLog(@"IS LIKED? [%@]", is_liked);
        }
        
        [self parseLikes:likeList messageid:messageid context:ctx options:homeurl];
    }
    
    // COMMENTS
    NSArray *commentList = d[@"comments"];
    NSString *comment_count = [NSString stringWithFormat:@"%lu", (unsigned long)commentList.count ];
    NSString *comments = [NSString stringWithFormat:@"%@", [(d[@"comments"]) componentsJoinedByString:@"|"]];
    if (commentList.count > 0) {
        [self parseComments:commentList messageid:messageid context:ctx options:homeurl];
    }
    
    mo = [self getEntity:kSocialStreamFeedEntity attribute:@"message_id" parameter:messageid context:ctx];
    if (mo == nil) {
        mo = [NSEntityDescription insertNewObjectForEntityForName:kSocialStreamFeedEntity inManagedObjectContext:ctx];
    }
    
    [mo setValue:messageid forKey:@"message_id"];
    [mo setValue:message forKey:@"message"];
    [mo setValue:attachCount forKey:@"attachCount"];
    [mo setValue:avatar forKey:@"avatar"];
    [mo setValue:current_date forKey:@"current_date"];
    [mo setValue:date_created forKey:@"date_created"];
    [mo setValue:date_modified forKey:@"date_modified"];
    [mo setValue:emoticon_id forKey:@"emoticon_id"];
    [mo setValue:user_id forKey:@"user_id"];
    [mo setValue:username forKey:@"username"];
    [mo setValue:first_name forKey:@"first_name"];
    [mo setValue:last_name forKey:@"last_name"];
    [mo setValue:group_id forKey:@"group_id"];
    [mo setValue:icon_text forKey:@"icon_text"];
    [mo setValue:icon_url forKey:@"icon_url"];
    [mo setValue:is_attached forKey:@"is_attached"];
    [mo setValue:is_deleted forKey:@"is_deleted"];
    [mo setValue:like_count forKey:@"likeCount"];
    [mo setValue:post_likes forKey:@"post_likes"];
    [mo setValue:comment_count forKey:@"commentCount"];
    [mo setValue:comments forKey:@"comments"];
    [mo setValue:type forKey:@"type"];
    [mo setValue:is_liked forKey:@"is_liked"];
    
    BOOL isOwned = [self ownedByUser:user_id];
    NSString *owned = [NSString stringWithFormat:@"%i", isOwned];
    [mo setValue:owned forKey:@"owned"];
    
}

- (void) parseUrlData:(NSArray *)list messageid:(NSString *)messageid context:(NSManagedObjectContext *)ctx options:(NSString *)options {
    
    //    "url_data" = (
    //                     {
    //                          canonicalUrl = "www.youtube.com";
    //                          "content_id" = 60;
    //                          description = "Ibahagi ang iyong mga video sa mga kaibigan, pamilya, at sa mundo";
    //                          image = "/img/preview/bc0c12e389044928b5b95c443591b281.jpg";
    //                          "is_image" = yes;
    //                          "is_video" = no;
    //                          "msg_type" = image;
    //                          text = " http://www.youtube.com";
    //                          "thumb_img" = "/img/preview/bc0c12e389044928b5b95c443591b281_thumb.jpg";
    //                          title = YouTube;
    //                          url = "https://www.youtube.com/";
    //                          uuid = bc0c12e389044928b5b95c443591b281;
    //                     }
    //                 );
    
    
    if (list.count > 0) {
        NSDictionary *url_data = (NSDictionary *)[list lastObject];
        
        //        //CLEAR CONTENTS
        //        NSPredicate *predicate = [self predicateForKeyPath:@"message_id" andValue:messageid];
        //        [self clearDataForEntity:kSocialStreamPreviewEntity withPredicate:predicate context:ctx];
        
        NSString *content_id = [NSString stringWithFormat:@"%@", url_data[@"content_id"] ];
        NSString *title = [NSString stringWithFormat:@"%@", url_data[@"title"] ];
        NSString *url = [NSString stringWithFormat:@"%@", url_data[@"url"] ];
        NSString *url_description = [NSString stringWithFormat:@"%@", url_data[@"description"] ];
        NSString *image = [NSString stringWithFormat:@"%@%@", options, url_data[@"image"] ];
        NSString *image_thumb = [NSString stringWithFormat:@"%@%@", options, url_data[@"thumb_img"] ];
        NSString *message_type = [NSString stringWithFormat:@"%@", url_data[@"msg_type"] ];
        
        NSManagedObject *mo = [self getEntity:kSocialStreamPreviewEntity attribute:@"content_id" parameter:content_id context:ctx];
        if (mo == nil) {
            mo = [NSEntityDescription insertNewObjectForEntityForName:kSocialStreamPreviewEntity inManagedObjectContext:ctx];
        }
        
        [mo setValue:content_id forKey:@"content_id"];
        [mo setValue:title forKey:@"title"];
        [mo setValue:url forKey:@"url"];
        [mo setValue:url_description forKey:@"url_description"];
        [mo setValue:image forKey:@"image"];
        [mo setValue:image_thumb forKey:@"image_thumb"];
        [mo setValue:message_type forKey:@"message_type"];
    }
    
    [self saveTreeContext:ctx];
}

- (void) parseComments:(NSArray *)list messageid:(NSString *)messageid context:(NSManagedObjectContext *)ctx options:(NSString *)options {
    
    //CLEAR CONTENTS
    NSPredicate *predicate = [self predicateForKeyPath:@"message_id" andValue:messageid];
    [self clearDataForEntity:kSocialStreamCommentEntity withPredicate:predicate context:ctx];
    
    //INSERT DATA
    for (NSDictionary *c in list) {
        NSManagedObject *cmo = nil;
        [self processComments:c managedObject:cmo context:ctx options:options];
    }
    
    [self saveTreeContext:ctx];
}

- (void)processComments:(NSDictionary *)d managedObject:(NSManagedObject *)mo context:(NSManagedObjectContext *)ctx options:(NSString *)options {
    
    /*
     {
     id = 66;
     comment = "hello array objects";
     "comment_likes" = ();
     avatar = "/socialstream/public/img/avatar/generic-boy.png";
     "current_date" = "2015-01-29 18:21:54";
     "date_created" = "2015-01-29 18:18:14";
     "date_modified" = "2015-01-29 18:18:14";
     "first_name" = Tennie;
     "last_name" = Lino;
     "is_deleted" = 0;
     "message_id" = 688;
     "user_id" = 115;
     username = tlino;
     },
     */
    
    NSString *comment_id = [NSString stringWithFormat:@"%@", d[@"id"] ];
    NSString *comment = [NSString stringWithFormat:@"%@", d[@"comment"] ];
    
    NSArray *commentLikes = d[@"comment_likes"];
    NSString *like_count = [NSString stringWithFormat:@"%lu", (unsigned long)commentLikes.count ];
    NSString *avatar = [NSString stringWithFormat:@"%@%@", options, d[@"avatar"] ];
    NSString *current_date = [NSString stringWithFormat:@"%@", d[@"current_date"] ];
    NSString *date_created = [NSString stringWithFormat:@"%@", d[@"date_created"] ];
    NSDate *modifiedDate = [self parseDateFromString:d[@"date_modified"]];
    NSString *relativeDate = [modifiedDate timeAgo];
    NSString *date_modified = [NSString stringWithFormat:@"%@", relativeDate ];
    NSString *first_name = [NSString stringWithFormat:@"%@", d[@"first_name"] ];
    NSString *last_name = [NSString stringWithFormat:@"%@", d[@"last_name"] ];
    NSString *is_deleted = [NSString stringWithFormat:@"%@", d[@"is_deleted"] ];
    NSString *message_id = [NSString stringWithFormat:@"%@", d[@"message_id"] ];
    NSString *user_id = [NSString stringWithFormat:@"%@", d[@"user_id"] ];
    NSString *username = [NSString stringWithFormat:@"%@", d[@"username"] ];
    
    mo = [self getEntity:kSocialStreamCommentEntity attribute:@"comment_id" parameter:comment_id context:ctx];
    if (mo == nil) {
        mo = [NSEntityDescription insertNewObjectForEntityForName:kSocialStreamCommentEntity inManagedObjectContext:ctx];
    }
    
    [mo setValue:comment_id forKey:@"comment_id"];
    [mo setValue:comment forKey:@"comment"];
    [mo setValue:like_count forKey:@"likeCount"];
    [mo setValue:avatar forKey:@"avatar"];
    [mo setValue:current_date forKey:@"current_date"];
    [mo setValue:date_created forKey:@"date_created"];
    [mo setValue:date_modified forKey:@"date_modified"];
    [mo setValue:first_name forKey:@"first_name"];
    [mo setValue:last_name forKey:@"last_name"];
    [mo setValue:is_deleted forKey:@"is_deleted"];
    [mo setValue:message_id forKey:@"message_id"];
    [mo setValue:user_id forKey:@"user_id"];
    [mo setValue:username forKey:@"username"];
    
    BOOL isOwned = [self ownedByUser:user_id];
    NSString *owned = [NSString stringWithFormat:@"%@", [NSNumber numberWithBool:isOwned]];
    [mo setValue:owned forKey:@"owned"];
}

- (void) parseLikes:(NSArray *)list messageid:(NSString *)messageid context:(NSManagedObjectContext *)ctx options:(NSString *)options {
    
    //CLEAR CONTENTS
    NSPredicate *predicate = [self predicateForKeyPath:@"message_id" andValue:messageid];
    [self clearDataForEntity:kSocialStreamLikeEntity withPredicate:predicate context:ctx];
    
    //INSERT DATA
    for (NSDictionary *c in list) {
        NSManagedObject *cmo = nil;
        [self processLikes:c managedObject:cmo context:ctx options:options];
    }
    
    [self saveTreeContext:ctx];
}

- (void)processLikes:(NSDictionary *)d managedObject:(NSManagedObject *)mo context:(NSManagedObjectContext *)ctx options:(NSString *)options {
    
    /*
     {
     id = 183;
     avatar = "/socialstream/public/img/avatar/generic-boy.png";
     "content_id" = 780;
     
     "date_created" = "2015-02-03 09:29:08";
     "date_modified" = "2015-02-03 09:29:08";
     
     "first_name" = Tennie;
     "last_name" = Lino;
     
     "is_deleted" = 0;
     "is_message" = 1;
     
     "user_id" = 115;
     "user_type" = student;
     username = tlino;
     },
     */
    
    NSLog(@"like object : %@", d);
    
    NSString *like_id = [NSString stringWithFormat:@"%@", d[@"id"] ];
    NSString *avatar = [NSString stringWithFormat:@"%@%@", options, d[@"avatar"] ];
    
    //content ID => message ID
    NSString *content_id = [NSString stringWithFormat:@"%@", d[@"content_id"] ];
    NSString *message_id = [NSString stringWithFormat:@"%@", d[@"content_id"] ];
    
    NSString *date_created = [NSString stringWithFormat:@"%@", d[@"date_created"] ];
    NSDate *modifiedDate = [self parseDateFromString:d[@"date_modified"]];
    NSString *relativeDate = [modifiedDate timeAgo];
    NSString *date_modified = [NSString stringWithFormat:@"%@", relativeDate ];
    NSString *first_name = [NSString stringWithFormat:@"%@", d[@"first_name"] ];
    NSString *last_name = [NSString stringWithFormat:@"%@", d[@"last_name"] ];
    NSString *is_deleted = [NSString stringWithFormat:@"%@", d[@"is_deleted"] ];
    NSString *is_message = [NSString stringWithFormat:@"%@", d[@"is_message"] ];
    NSString *user_id = [NSString stringWithFormat:@"%@", d[@"user_id"] ];
    NSString *user_type = [NSString stringWithFormat:@"%@", d[@"user_type"] ];
    NSString *username = [NSString stringWithFormat:@"%@", d[@"username"] ];
    
    mo = [self getEntity:kSocialStreamLikeEntity attribute:@"like_id" parameter:like_id context:ctx];
    if (mo == nil) {
        mo = [NSEntityDescription insertNewObjectForEntityForName:kSocialStreamLikeEntity inManagedObjectContext:ctx];
    }
    
    [mo setValue:like_id forKey:@"like_id"];
    [mo setValue:avatar forKey:@"avatar"];
    [mo setValue:content_id forKey:@"content_id"];
    [mo setValue:message_id forKey:@"message_id"];
    [mo setValue:date_created forKey:@"date_created"];
    [mo setValue:date_modified forKey:@"date_modified"];
    [mo setValue:first_name forKey:@"first_name"];
    [mo setValue:last_name forKey:@"last_name"];
    [mo setValue:is_deleted forKey:@"is_deleted"];
    [mo setValue:is_message forKey:@"is_message"];
    [mo setValue:user_id forKey:@"user_id"];
    [mo setValue:user_type forKey:@"user_type"];
    [mo setValue:username forKey:@"username"];
}


- (void)requestSocialStreamGroups:(ResourceManagerListBlock)listBlock {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    
    NSString *getPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointGetGroups, account.user.id]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" url:getPath];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:data];
            if (dictionary) {
                //                NSLog(@"dictionary : %@", dictionary);
                
                NSArray *records = dictionary[@"records"];
                if (records > 0) {
                    
                    if (listBlock) {
                        listBlock(records);
                    }
                }
            }
        }
    }];
    
    [task resume];
}

- (void)requestBadWordsDoneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSString *getPath = [Utils buildUrl:kEndPointGetBadWords];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" url:getPath];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
        }
        
        if (!error) {
            
            NSDictionary *dictionary = [self parseResponseData:data];
            
            if (dictionary) {
                //                NSLog(@"dictionary : %@", dictionary);
                NSArray *records = dictionary[@"records"];
                if (records > 0) {
                    
                    NSManagedObjectContext *ctx = _workerContext;
                    [ctx performBlockAndWait:^{
                        
                        for (NSDictionary *d in records) {
                            
                            /*
                             id = 2;
                             word = gago;
                             "replace_text" = "****";
                             "is_deleted" = 0;
                             "date_created" = "2014-03-06 00:00:00";
                             "date_modified" = "0000-00-00 00:00:00";
                             */
                            
                            NSString *word_id = [NSString stringWithFormat:@"%@", d[@"id"] ];
                            
                            NSString *word = [NSString stringWithFormat:@"%@", d[@"word"] ];
                            NSString *replace_text = [NSString stringWithFormat:@"%@", d[@"replace_text"] ];
                            NSString *is_deleted = [NSString stringWithFormat:@"%@", d[@"is_deleted"] ];
                            NSString *date_created = [NSString stringWithFormat:@"%@", d[@"date_created"] ];
                            NSString *date_modified = [NSString stringWithFormat:@"%@", d[@"date_modified"] ];
                            
                            NSManagedObject *mo = [self getEntity:kBadWordsEntity
                                                        attribute:@"word_id"
                                                        parameter:word_id
                                                          context:ctx];
                            if (mo == nil) {
                                mo = [NSEntityDescription insertNewObjectForEntityForName:kBadWordsEntity inManagedObjectContext:ctx];
                            }
                            
                            [mo setValue:word_id forKey:@"word_id"];
                            [mo setValue:word forKey:@"word"];
                            [mo setValue:replace_text forKey:@"replace_text"];
                            [mo setValue:is_deleted forKey:@"is_deleted"];
                            [mo setValue:date_created forKey:@"date_created"];
                            [mo setValue:date_modified forKey:@"date_modified"];
                            
                        }
                        
                        [self saveTreeContext:ctx];
                        
                        if (doneBlock) {
                            doneBlock(YES);
                        }
                    }];
                    
                }
            }
        }
    }];
    
    [task resume];
}

- (NSDictionary *)fetchBadwords {
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kBadWordsEntity];
    NSError *error = nil;
    
    NSManagedObjectContext *ctx = _workerContext;
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
    if (error) {
        VLog(@"error: %@", [error localizedDescription]);
    }
    
    NSMutableDictionary *d = [NSMutableDictionary dictionary];
    if ([items count] > 0) {
        for (NSManagedObject *mo in items) {
            NSString *word_key = [NSString stringWithFormat:@"%@", [mo valueForKey:@"word"] ];
            NSString *replace_text = [NSString stringWithFormat:@"%@", [mo valueForKey:@"replace_text"] ];
            [d setValue:replace_text forKey:word_key];
        }
    }
    
    NSDictionary *data = [NSDictionary dictionaryWithDictionary:d];
    
    return data;
}


- (void)postMessage:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *avatar = [NSString stringWithFormat:@"%@", account.user.avatar];
    NSString *username = [NSString stringWithFormat:@"%@", account.user.email];
    
    if ([username isEqualToString:@"(null)"] || [username isEqualToString:@"<null>"] || [username isEqualToString:@"null"]) {
        username = [NSString stringWithFormat:@"%@", [VSmart sharedInstance].account.user.email];
    }
    
    int userId = account.user.id;
    
    data[@"user_id"] = [NSNumber numberWithInt:userId];
    data[@"avatar"] = avatar;
    data[@"username"] = username;
    data[@"is_attached"] = [NSNumber numberWithInt:0];
    NSLog(@"post message : %@", data);
    
    NSURL *postURL = [NSURL URLWithString:[Utils buildUrl:kEndPointPostSocialStream]];
    NSLog(@"POST MESSAGE URL : %@", postURL.absoluteString);
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:postURL body:data];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          BOOL status = NO;
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              NSLog(@"dictionary : %@", dictionary);
                                              NSDictionary *meta = dictionary[@"_meta"];
                                              NSString *metaStatus = [NSString stringWithFormat:@"%@", meta[@"status"]];
                                              
                                              NSArray *records = dictionary[@"records"];
                                              if (records > 0) {
                                                  status = ![metaStatus isEqualToString:@"ERROR"] ? YES : NO;
                                              }
                                              
                                              if (doneBlock) {
                                                  doneBlock(status);
                                              }
                                          }
                                      }
                                  }];
    
    [task resume];
}

- (void)editMessage:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSLog(@"edit message : %@", data);
    NSString *message_id = [NSString stringWithFormat:@"%@", data[@"message_id"] ];
    //remove message id
    [data removeObjectForKey:@"message_id"];
    
    NSString *editPath = [NSString stringWithFormat:kEndPointPostEditSocialStream, message_id];
    NSURL *postURL = [NSURL URLWithString:[Utils buildUrl:editPath]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:postURL body:data];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          BOOL status = NO;
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              NSLog(@"dictionary : %@", dictionary);
                                              NSDictionary *meta = dictionary[@"_meta"];
                                              NSString *metaStatus = [NSString stringWithFormat:@"%@", meta[@"status"]];
                                              
                                              NSArray *records = dictionary[@"records"];
                                              if (records > 0) {
                                                  status = ![metaStatus isEqualToString:@"ERROR"] ? YES : NO;
                                              }
                                              
                                              if (doneBlock) {
                                                  doneBlock(status);
                                              }
                                          }
                                      }
                                  }];
    
    [task resume];
}

- (void)requestRemoveMessage:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    //To delete message/post - >/v1/stream/removemessage/{msg_id}
    
    // SEARCH MESSAGE AND USER WHO LIKE
    NSString *message_id = data[@"message_id"];
    NSString *user_id = data[@"user_id"];
    
    NSPredicate *p1 = [self predicateForKeyPath:@"message_id" andValue:message_id];
    NSPredicate *p2 = [self predicateForKeyPath:@"user_id" andValue:user_id];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates: @[p1, p2]];
    
    BOOL clearLikes = [self clearContentsForEntity:kSocialStreamLikeEntity predicate:predicate];
    NSLog(@"clear likes : %@", [NSNumber numberWithBool:clearLikes]);
    BOOL clearComments = [self clearContentsForEntity:kSocialStreamCommentEntity predicate:predicate];
    NSLog(@"clear comments : %@", [NSNumber numberWithBool:clearComments]);
    
    NSString *deletePath = [NSString stringWithFormat:kEndPointRemoveSocialStream, message_id];
    NSURL *postURL = [NSURL URLWithString:[Utils buildUrl:deletePath]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:postURL body:nil];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          BOOL status = YES;
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              NSLog(@"dictionary : %@", dictionary);
                                              //                  NSDictionary *meta = dictionary[@"_meta"];
                                              //                  NSString *metaStatus = [NSString stringWithFormat:@"%@", meta[@"status"]];
                                              //
                                              //                  NSArray *records = dictionary[@"records"];
                                              //                  if (records > 0) {
                                              //                      status = ![metaStatus isEqualToString:@"ERROR"] ? YES : NO;
                                              //                  }
                                              
                                              if (doneBlock) {
                                                  doneBlock(status);
                                              }
                                          }
                                      }
                                  }];
    
    [task resume];
}

- (void)editComment:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSLog(@"edit message : %@", data);
    NSString *comment_id = [NSString stringWithFormat:@"%@", data[@"comment_id"] ];
    //remove message id
    [data removeObjectForKey:@"comment_id"];
    
    NSString *editPath = [NSString stringWithFormat:kEndPointPostEditCommentSocialStream, comment_id];
    NSURL *postURL = [NSURL URLWithString:[Utils buildUrl:editPath]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:postURL body:data];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          BOOL status = NO;
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              NSLog(@"dictionary : %@", dictionary);
                                              NSDictionary *meta = dictionary[@"_meta"];
                                              NSString *metaStatus = [NSString stringWithFormat:@"%@", meta[@"status"]];
                                              
                                              NSArray *records = dictionary[@"records"];
                                              if (records > 0) {
                                                  status = ![metaStatus isEqualToString:@"ERROR"] ? YES : NO;
                                              }
                                              
                                              if (doneBlock) {
                                                  doneBlock(status);
                                              }
                                          }
                                      }
                                  }];
    
    [task resume];
}

- (void)requestRemoveComment:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    //To delete message/post - >/v1/stream/removemessage/{msg_id}
    
    // SEARCH MESSAGE AND USER WHO LIKE
    NSString *comment_id = data[@"comment_id"];
    NSString *user_id = data[@"user_id"];
    
    NSPredicate *p1 = [self predicateForKeyPath:@"message_id" andValue:comment_id];
    NSPredicate *p2 = [self predicateForKeyPath:@"user_id" andValue:user_id];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates: @[p1, p2]];
    
    BOOL clearLikes = [self clearContentsForEntity:kSocialStreamLikeEntity predicate:predicate];
    NSLog(@"clear likes : %@", [NSNumber numberWithBool:clearLikes]);
    BOOL clearComments = [self clearContentsForEntity:kSocialStreamCommentEntity predicate:predicate];
    NSLog(@"clear comments : %@", [NSNumber numberWithBool:clearComments]);
    NSString *deletePath = [NSString stringWithFormat:kEndPointPostRemoveCommentSocialStream, comment_id];
    NSURL *postURL = [NSURL URLWithString:[Utils buildUrl:deletePath]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:postURL body:nil];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          BOOL status = YES;
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              NSLog(@"dictionary : %@", dictionary);
                                              if (doneBlock) {
                                                  doneBlock(status);
                                              }
                                          }
                                      }
                                  }];
    
    [task resume];
}

- (void)postLikeMessage:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSURL *postURL = [NSURL URLWithString: [Utils buildUrl:kEndPointPostSocialStreamLikeMessage] ];
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:postURL body:data];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          if (doneBlock) {
                                              doneBlock(NO);
                                          }
                                      }
                                      
                                      if (!error) {
                                          
                                          //                                          BOOL status = NO;
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              
                                              NSLog(@"dictionary : %@", dictionary);
                                              
                                              //MESSAGE
                                              /*
                                               records =     {
                                               data =         {
                                               avatar = "http://lms.vsmart.info/uploads/123/avatars/20150203/828cd1c9f7274c7e8f508e7f0842e5b0.png";
                                               "content_id" = 854;
                                               "date_created" = "2015-02-05 12:56:37";
                                               "date_modified" = "2015-02-05 12:56:37";
                                               id = 239;
                                               "is_deleted" = 0;
                                               "is_message" = 1;
                                               "like_count" = 1;
                                               "user_id" = 123;
                                               username = yyu;
                                               };
                                               status = "new like was inserted.";
                                               };
                                               }
                                               */
                                              
                                              //COMMENT
                                              
                                              /*
                                               records =     {
                                               data =         {
                                               avatar = "http://lms.vsmart.info/uploads/123/avatars/20150203/828cd1c9f7274c7e8f508e7f0842e5b0.png";
                                               "content_id" = 189;
                                               "date_created" = "2015-02-05 12:53:16";
                                               "date_modified" = "2015-02-05 12:53:16";
                                               id = 238;
                                               "is_deleted" = 0;
                                               "is_message" = 0;
                                               "like_count" = 1;
                                               "user_id" = 123;
                                               username = yyu;
                                               };
                                               status = "new like was inserted.";
                                               };
                                               */
                                              
                                              NSDictionary *meta = dictionary[@"_meta"];
                                              NSString *metaStatus = [NSString stringWithFormat:@"%@", meta[@"status"]];
                                              BOOL status = ![metaStatus isEqualToString:@"ERROR"] ? YES : NO;
                                              
                                              
                                              if (status) {
                                                  NSArray *records = dictionary[@"records"];
                                                  
                                                  if (records > 0) {
                                                      //                                                  status = ![metaStatus isEqualToString:@"ERROR"] ? YES : NO;
                                                      //                                                  NSManagedObjectContext *ctx = _workerContext;
                                                      //                                                  NSString *message_id = [NSString stringWithFormat:@"%@", data[@"content_id"]];
                                                      //                                                  NSManagedObject *mo = [self getEntity:kSocialStreamFeedEntity attribute:@"message_id" parameter:message_id context:ctx];
                                                      //                                                  NSString *is_liked = [NSString stringWithFormat:@"%hhd", status];
                                                      //                                                  [mo setValue:is_liked forKey:@"is_liked"];
                                                      //                                                  [self saveTreeContext:ctx];
                                                      
                                                      // BUG FIX: app crash
                                                      // Ensuring if in the right queue
                                                      NSManagedObjectContext *ctx = _workerContext;
                                                      
                                                      [ctx performBlock:^{
                                                          NSString *message_id = [NSString stringWithFormat:@"%@", data[@"content_id"]];
                                                          NSString *is_liked = @"0"; // if meta status is error
                                                          
                                                          if (status) {
                                                              is_liked = @"1";
                                                          }
                                                          
                                                          NSManagedObject *mo = [self getEntity:kSocialStreamFeedEntity attribute:@"message_id" parameter:message_id context:ctx];
                                                          [mo setValue:is_liked forKey:@"is_liked"];
                                                          
                                                          [self saveTreeContext:ctx];
                                                      }];
                                                  }
                                              }
                                              
                                              if (doneBlock) {
                                                  doneBlock(status);
                                              }
                                          }
                                      }
                                  }];
    
    [task resume];
}

- (void)postUnlikeMessage:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    // SEARCH MESSAGE AND USER WHO LIKE
    NSString *message_id = data[@"content_id"];
    NSString *user_id = data[@"user_id"];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kSocialStreamLikeEntity];
    NSPredicate *p1 = [self predicateForKeyPath:@"message_id" andValue:message_id];
    NSPredicate *p2 = [self predicateForKeyPath:@"user_id" andValue:user_id];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates: @[p1, p2]];
    [fetchRequest setPredicate:predicate];
    
    NSString *like_id = @"";
    NSManagedObjectContext *context = _workerContext;
    NSArray *items = [context executeFetchRequest:fetchRequest error:nil];
    if ([items count] > 0) {
        for (NSManagedObject *lmo in items) {
            like_id = [NSString stringWithFormat:@"%@", [lmo valueForKey:@"like_id"]];
            [context deleteObject:lmo];
        }
        [self saveTreeContext:context];
    }
    
    NSString *path = [NSString stringWithFormat:kEndPointPostSocialStreamUnLikeMessage, like_id ];
    NSURL *postURL = [NSURL URLWithString: [Utils buildUrl:path] ];
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:postURL body:data];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          if (doneBlock) {
                                              doneBlock(NO);
                                          }
                                      }
                                      
                                      if (!error) {
                                          
                                          //                                          BOOL status = NO;
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              //NSLog(@"dictionary : %@", dictionary);
                                              NSDictionary *meta = dictionary[@"_meta"];
                                              NSString *metaStatus = [NSString stringWithFormat:@"%@", meta[@"status"]];
                                              BOOL status = ![metaStatus isEqualToString:@"ERROR"] ? YES : NO;
                                              
                                              if (status) {
                                                  NSArray *records = dictionary[@"records"];
                                                  
                                                  if (records > 0) {
                                                      //                                                  status = ![metaStatus isEqualToString:@"ERROR"] ? YES : NO;
                                                      //                                                  NSManagedObjectContext *ctx = _workerContext;
                                                      //                                                  NSString *message_id = [NSString stringWithFormat:@"%@", data[@"content_id"]];
                                                      //                                                  NSManagedObject *mo = [self getEntity:kSocialStreamFeedEntity attribute:@"message_id" parameter:message_id context:ctx];
                                                      //                                                  NSString *is_liked = [NSString stringWithFormat:@"%hhd", status];
                                                      //                                                  [mo setValue:is_liked forKey:@"is_liked"];
                                                      //                                                  [self saveTreeContext:ctx];
                                                      
                                                      // BUG FIX: app crash
                                                      // Ensuring if in the right queue
                                                      NSManagedObjectContext *ctx = _workerContext;
                                                      
                                                      [ctx performBlock:^{
                                                          NSString *message_id = [NSString stringWithFormat:@"%@", data[@"content_id"]];
                                                          NSString *is_liked = @"1"; // if meta status is error
                                                          
                                                          if (status) {
                                                              is_liked = @"0";
                                                          }
                                                          
                                                          NSManagedObject *mo = [self getEntity:kSocialStreamFeedEntity attribute:@"message_id" parameter:message_id context:ctx];
                                                          [mo setValue:is_liked forKey:@"is_liked"];
                                                          
                                                          [self saveTreeContext:ctx];
                                                      }];
                                                  }
                                              }
                                              
                                              if (doneBlock) {
                                                  doneBlock(status);
                                              }
                                          }
                                      }
                                  }];
    
    [task resume];
}

- (void)postComment:(NSMutableDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSURL *postURL = [NSURL URLWithString:[Utils buildUrl:kEndPointPostCommentSocialStream]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:postURL body:data];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          BOOL status = NO;
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              NSLog(@"dictionary : %@", dictionary);
                                              NSDictionary *meta = dictionary[@"_meta"];
                                              NSString *metaStatus = [NSString stringWithFormat:@"%@", meta[@"status"]];
                                              
                                              NSDictionary *records = dictionary[@"records"];
                                              if (records) {
                                                  
                                                  status = ![metaStatus isEqualToString:@"ERROR"] ? YES : NO;
                                                  
                                                  NSDictionary *d = records[@"data"];
                                                  
                                                  /*
                                                   avatar = "http://lms.vsmart.info/uploads/123/avatars/20150203/828cd1c9f7274c7e8f508e7f0842e5b0.png";
                                                   comment = "ios comment";
                                                   commentCount = 4;
                                                   "date_created" = "2015-02-05 10:49:29";
                                                   "date_modified" = "2015-02-05 10:49:29";
                                                   id = 180;
                                                   "is_deleted" = 0;
                                                   "message_id" = 853;
                                                   "user_id" = 123;
                                                   username = yyu;
                                                   */
                                                  
                                                  NSString *message_id = [NSString stringWithFormat:@"%@", d[@"message_id"] ];
                                                  NSString *commentCount = [NSString stringWithFormat:@"%@", d[@"commentCount"] ];
                                                  
                                                  NSManagedObjectContext *ctx = _workerContext;
                                                  [ctx performBlockAndWait:^{
                                                      
                                                      //FEED ENTITY UPDATE -----------------------------------------------------------------------------------------------
                                                      
                                                      NSManagedObject *mo = [self getEntity:kSocialStreamFeedEntity attribute:@"message_id" parameter:message_id context:ctx];
                                                      [mo setValue:commentCount forKey:@"commentCount"];
                                                      [self saveTreeContext:ctx];
                                                      
                                                  }];
                                                  
                                              }
                                              
                                              
                                              if (doneBlock) {
                                                  doneBlock(status);
                                              }
                                          }
                                      }
                                  }];
    
    [task resume];
}

#pragma mark - ResourceManager Authentication

- (void)requestAuthentication:(ResourceManagerDoneBlock)doneBlock {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *username = [NSString stringWithFormat:@"%@", account.user.email ];
    NSString *password = [NSString stringWithFormat:@"%@", account.user.password ];
    
    
    JSONModelError *jsonError = nil;
    NSError *error = nil;
    NSDictionary *params = @{@"grant_type": @"password", @"username" : username, @"password" : password};
    NSDictionary *headers = @{@"Authorization": @"Basic MTAwMDAwMTpzMmFMd3o4a0FuRW01MlNqT1BnVmViblI2NlI1cXZLYlF6Q3ozZDkzaDJEV0pkcEl3VGN5aEwwUXlCelloRHdXUjA1V2FRc1RjeGNoNmpwSQ=="};
    
    NSURL *oauthURL = [NSURL URLWithString:kServiceURLOAuthToken];
    NSData *responseData = [JSONHTTPClient syncRequestDataFromURL:oauthURL method:kHTTPMethodPOST params:params headers:headers etag:nil error:&jsonError];
    
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    NSLog(@"requestOAuth: %@", responseDict);
    
    NSString *access_token = [NSString stringWithFormat:@"%@", responseDict[@"access_token"] ];
    NSString *expires_in = [NSString stringWithFormat:@"%@", responseDict[@"expires_in"] ];
    NSString *refresh_token = [NSString stringWithFormat:@"%@", responseDict[@"refresh_token"] ];
    NSString *scope = [NSString stringWithFormat:@"%@", responseDict[@"scope"] ];
    NSString *token_type = [NSString stringWithFormat:@"%@", responseDict[@"token_type"] ];
    
    
    BOOL status = NO;
    if (!error) {
        NSDictionary *result = @{@"access_token":access_token,
                                 @"expires_in":expires_in,
                                 @"refresh_token":refresh_token,
                                 @"scope":scope,
                                 @"token_type":token_type};
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:result forKey:kOAuthResponseKey];
        status = [defaults synchronize];
    }
    
    if (doneBlock) {
        doneBlock(status);
    }
    
    //    ///////////////////
    //    // POST PARAMETER
    //    ///////////////////
    //    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    //    NSString *username = [NSString stringWithFormat:@"%@", account.user.email ];
    //    NSString *password = [NSString stringWithFormat:@"%@", account.user.password ];
    //    NSDictionary *parameter = @{ @"grant_type": @"password", @"username" : username, @"password" : password };
    //
    //    ///////////////////
    //    // REQUEST
    //    ///////////////////
    //    NSURL *oauth_url = [NSURL URLWithString:kServiceURLOAuthToken];
    //    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:oauth_url body:parameter];
    //
    //    ///////////////////
    //    // REQUEST HEADER
    //    ///////////////////
    //    NSString *authorization = @"Basic MTAwMDAwMTpzMmFMd3o4a0FuRW01MlNqT1BnVmViblI2NlI1cXZLYlF6Q3ozZDkzaDJEV0pkcEl3VGN5aEwwUXlCelloRHdXUjA1V2FRc1RjeGNoNmpwSQ==";
    //    [request setValue:authorization forHTTPHeaderField:@"Authorization"];
    //
    //    NSURLSession *session = [NSURLSession sharedSession];
    //    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
    //      {
    //          if (error) {
    //              NSLog(@"error %@", [error localizedDescription]);
    //          }
    //
    //          if (!error) {
    //              NSDictionary *dictionary = [self parseResponseData:responsedata];
    //              if (dictionary) {
    //                    NSLog(@"OAUTH RESPONSE : %@", dictionary);
    //
    //                  NSString *access_token = [NSString stringWithFormat:@"%@", dictionary[@"access_token"] ];
    //                  NSString *expires_in = [NSString stringWithFormat:@"%@", dictionary[@"expires_in"] ];
    //                  NSString *refresh_token = [NSString stringWithFormat:@"%@", dictionary[@"refresh_token"] ];
    //                  NSString *scope = [NSString stringWithFormat:@"%@", dictionary[@"scope"] ];
    //                  NSString *token_type = [NSString stringWithFormat:@"%@", dictionary[@"token_type"] ];
    //
    //                  NSDictionary *result = @{
    //                                           @"access_token":access_token,
    //                                           @"expires_in":expires_in,
    //                                           @"refresh_token":refresh_token,
    //                                           @"scope":scope,
    //                                           @"token_type":token_type
    //                                           };
    //                  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //                  [defaults setValue:result forKey:kOAuthResponseKey];
    //                  [defaults synchronize];
    //              }
    //          }
    //      }];
    //
    //    [task resume];
}

- (void)requestDeviceBind:(ResourceManagerDoneBlock)doneBlock {
    
    JSONModelError *jsonError = nil;
    NSError *error = nil;
    
    NSDictionary *oAuthResponse = [[NSUserDefaults standardUserDefaults] valueForKey:kOAuthResponseKey];
    NSString *oAuthToken = [oAuthResponse valueForKey:@"access_token"];
    
    NSDictionary *params = @{@"operating_system": @"iOS",
                             @"system_version" : [[UIDevice currentDevice] systemVersion],
                             @"manufacturer" : @"Apple",
                             @"model": [[UIDevice currentDevice] model],
                             @"serial_number" : @"na",
                             @"wifi_address" : @"na",
                             @"bluetooth" : @"na",
                             @"storage_capacity" : @"0",
                             @"uuid" : [VSmartHelpers uniqueGlobalDeviceIdentifier],
                             };
    
    NSDictionary *headers = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", oAuthToken]};
    
    NSURL *bind_url = [NSURL URLWithString:kServiceURLBind];
    NSData *responseData = [JSONHTTPClient syncRequestDataFromURL:bind_url
                                                           method:kHTTPMethodPOST
                                                           params:params
                                                          headers:headers
                                                             etag:nil
                                                            error:&jsonError];
    
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    
    NSLog(@"DEVICE BIND : %@", responseDict);
    
    BOOL status = NO;
    if (!error) {
        
        NSString *success = [NSString stringWithFormat:@"%@", responseDict[@"success"] ];
        
        if ( ![success isEqualToString:@"0"] ){
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:responseDict forKey:kBindResponseKey];
            status = [defaults synchronize];
            if (doneBlock) {
                doneBlock(YES);
            }
        }
        
        if ( [success isEqualToString:@"0"] ){
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
    }
    
    //    ///////////////////
    //    // POST PARAMETER
    //    ///////////////////
    //
    //    NSString *systemName = [NSString stringWithFormat:@"%@", [[UIDevice currentDevice] systemName] ];
    //    NSString *systemVersion = [NSString stringWithFormat:@"%@", [[UIDevice currentDevice] systemVersion] ];
    //    NSString *operating_system = [NSString stringWithFormat:@"%@ %@", systemName, systemVersion];
    //
    //    NSString *system_model = [NSString stringWithFormat:@"%@", [[UIDevice currentDevice] model] ];
    //
    //    NSDictionary *parameter = @{
    //                                @"operating_system": operating_system,
    //                                @"system_version" : systemVersion,
    //                                @"manufacturer" : @"Apple",
    //                                @"model": system_model,
    //                                @"serial_number" : @"na",
    //                                @"wifi_address" : @"na",
    //                                @"bluetooth" : @"na",
    //                                @"storage_capacity" : @"0",
    //                                @"uuid" : [VSmartHelpers uniqueGlobalDeviceIdentifier]
    //                             };
    //    ///////////////////
    //    // REQUEST
    //    ///////////////////
    //    NSURL *bind_url = [NSURL URLWithString:kServiceURLOAuthToken];
    //    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:bind_url body:parameter];
    //
    //    ///////////////////
    //    // REQUEST HEADER
    //    ///////////////////
    //    NSDictionary *oauth_key = [[NSUserDefaults standardUserDefaults] valueForKey:kOAuthResponseKey];
    //    NSString *authorization = [NSString stringWithFormat:@"Bearer %@", oauth_key[@"access_token"] ];
    //    [request setValue:authorization forHTTPHeaderField:@"Authorization"];
    //
    //
    //    NSURLSession *session = [NSURLSession sharedSession];
    //    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
    //      {
    //          if (error) {
    //              NSLog(@"error %@", [error localizedDescription]);
    //          }
    //
    //          if (!error) {
    //              NSDictionary *dictionary = [self parseResponseData:responsedata];
    //              if (dictionary) {
    //                  NSLog(@"BIND RESPONSE : %@", dictionary);
    //              }
    //          }
    //      }];
    //
    //    [task resume];
}

#pragma mark - ResourceManager Socket IO Client Methods

/////////////////////////////////////////////////////////////////////////

- (void)startSocketIOClient {
    
    NSString *socketserver = [NSString stringWithFormat:@"http://%@:%@", [Utils getVibeServer], @"8888"];
    NSLog(@"vibe server : %@", socketserver);
    NSURL *url = [NSURL URLWithString:socketserver];
    self.socketClient = [[SocketIOClient alloc] initWithSocketURL:url config:@{@"log": @YES, @"forcePolling": @YES}];
    
    [self.socketClient on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        
        NSLog(@"socket client connected...");
        
        NSString *user_id = [self loginUser];
        NSString *cliend_uuid = [self stringValue:[VSmartHelpers uniqueGlobalDeviceIdentifier]];
        NSDictionary *body = @{ @"user_id":user_id,@"client_uuid":cliend_uuid};
        NSData *postData = [NSJSONSerialization dataWithJSONObject:body options:0 error:nil];
        NSString *j_string = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        NSLog(@"j_string : <start>---- %@ ---<end>", j_string);
        [self.socketClient emit:@"register_client" with:@[j_string]];
        [self.socketClient onAny:^(SocketAnyEvent *ack) {
            NSLog(@"ack : %@", ack);
        }];
    }];
    
    [self.socketClient connect];
}

- (void)socketEvent:(NSString *)event info:(NSDictionary *)info dataBlock:(ResourceManagerDataBlock)dataBlock {

    
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    
//    // Set the text mode to show only text.
//    hud.mode = MBProgressHUDModeText;
//    hud.label.text = NSLocalizedString(@"Message here!", @"HUD message title");
//    // Move to bottm center.
//    hud.offset = CGPointMake(0.f, MBProgressMaxOffset);
//    
//    [hud hideAnimated:YES afterDelay:3.f];

    
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [ProgressHUD show:@"Generating PDF" Interaction:NO];
//    });
    
    
    NSString *pid = [self stringValue:info[@"process"]];
    NSString *testid = [self stringValue:info[@"testid"]];

    //NOT USED AT THIS TIME
//    BOOL enableEmail = [info[@"email"] boolValue];
//    BOOL enablePlaylist = [info[@"playlist"] boolValue];
    
    NSString *user_id = [self loginUser];
    NSString *cliend_uuid = [self stringValue:[VSmartHelpers uniqueGlobalDeviceIdentifier]];
    NSString *socketserver = [self stringValue:[Utils getVibeServer]];
    NSString *instance = [[socketserver componentsSeparatedByString:@"."] firstObject];
    NSDictionary *data = @{ @"pid":pid,@"user_id":user_id,@"client_uuid":cliend_uuid, @"instance":instance, @"test_id":@([testid integerValue])};
    NSData *postData = [NSJSONSerialization dataWithJSONObject:data options:0 error:nil];
    NSString *j_string = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    NSArray *items = @[ j_string ];
    [self.socketClient emit:event with:items];
    [self.socketClient on:event callback:^(NSArray *message, SocketAckEmitter *ack) {
        if (message != nil && message.count > 0) {
            NSLog(@"message : %@", message);
            NSString *json_string_object = (NSString *)[message lastObject];
            NSData *responseData = [json_string_object dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *dictionary = [self parseResponseData:responseData];
            if (dictionary != nil) {
                NSString *status = [self stringValue:dictionary[@"status"]];
                if ([status isEqualToString:@"0"]) {
                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [ProgressHUD dismiss];
//                    });
                    
                    NSString *filename = [self stringValue:dictionary[@"filename"]];
                    NSString *downloadlink = [NSString stringWithFormat:@"http://%@%@", [Utils getVibeServer], filename];
                    NSDictionary *socket_data = @{@"link":downloadlink};
                    if (dataBlock) {
                        dataBlock(socket_data);
                    }
                }
            }
        }
    }];
}

- (void)stopSocketIOClient {
    
    [self.socketClient disconnect];
}

/////////////////////////////////////////////////////////////////////////

#pragma mark - ResourceManager WebSockets Methods

- (void)reconnectSocketWithURL:(NSString *)connect port:(NSInteger)port {
    
    VLog(@"%s", __PRETTY_FUNCTION__);
    VLog(@"connect: %@", connect);
    VLog(@"port: %ld", (long)port);
    
    self.socketIO = [[SocketIO alloc] initWithDelegate:self];
    [self.socketIO connectToHost:connect onPort:port];
}

- (void)closeSocket {
    
    __weak typeof(self) wo = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [wo.socketIO disconnect];
    });
    
    //    [self.socketIO disconnectForced];
}

- (void) socketIODidConnect:(SocketIO *)socket {
    NSLog(@"socket.io connected.");
}

- (void) socketIODidDisconnect:(SocketIO *)socket disconnectedWithError:(NSError *)error {
    NSLog(@"socket.io disconnected. did error occur? %@", error);
}

- (void) socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet {
    //NSLog(@"didReceiveEvent()");
    
    NSString *emitterName = packet.name;
    NSDictionary *data = [packet.args lastObject];
    
    //    VLog(@"SOCKET_DATA: %@", data);
    
    if ([emitterName isEqualToString:@"online"]) {
        
        NSArray *users = data[@"users"];
        VLog(@"SOCKET_DATA_ONLINE: %@", users);
        NSLog(@"SOCKET_DATA_ONLINE: %@", data);
        if (users.count > 0) {
            NSDictionary *d = [data[@"users"] lastObject];
            NSString *type = [NSString stringWithFormat:@"%@", d[@"type"] ];
            if ([type isEqualToString:@"social"]) {
                
                // TODO: Update Cell values for Online Status
                // We don't monitor type: school
                [self updateFeedStatusWithList:users];
                
                
            }
            
        } else {
            
            // TODO: Update list cells for Offline status
            //          [self updateFeedStatusWithList:users];
            
        }
        
        //NOTIFICATIONS
    } else if ([emitterName isEqualToString:@"notification"]) {
        NSArray *users = data[@"users"];
        //VLog(@"SOCKET_DATA_NOTIFICATION: %@", users);
        if (users.count > 0) {
            NSDictionary *d = [data[@"users"] lastObject];
            
            NSLog(@"receive real time updates : %@", d);
            NSString *type = [NSString stringWithFormat:@"%@", d[@"type"] ];
            
            /*
             "parent_id" = "<null>";
             type = message;
             */
            
            if ([type isEqualToString:@"comment"]) {
                
                dispatch_queue_t socketq = dispatch_queue_create("com.vsmart.socket.SOCIALSTREAM", DISPATCH_QUEUE_SERIAL);
                dispatch_async(socketq, ^(void) {
                    ResourceManager *rm = [ResourceManager sharedInstance];
                    [rm updateMessageWithCommentData:d];
                });
                
            }
            
            if ([type isEqualToString:@"message"]) {
                
                dispatch_queue_t socketq = dispatch_queue_create("com.vsmart.socket.SOCIALSTREAM", DISPATCH_QUEUE_SERIAL);
                dispatch_async(socketq, ^(void) {
                    [self updateMessagesWithData:d];
                });
                
            }
            
            if ([type isEqualToString:@"message_likes"]) {
                [self updateLikeMessageWithData:d];
            }
        }
    }
}

- (void)updateFeedStatusWithList:(NSArray *)users
{
    
    NSArray *list = [NSArray arrayWithArray:users];
    
    
    NSManagedObjectContext *ctx = _workerContext;
    
    //            NSPredicate *predicateForOffline = [NSPredicate predicateWithFormat:@"user_id <> %@", user_id];
    NSArray *offline_mo_array = [self getObjectsForEntity:kSocialStreamFeedEntity predicate:nil];
    
    for (NSManagedObject *s_offline_mo in offline_mo_array) {
        [s_offline_mo setValue:@"0" forKey:@"status"];
    }
    
    
    for (NSDictionary *d in list) {
        
        /*
         user_id NSInteger
         model NSString
         
         "device_id" = bc86246f942e9cb8;
         id = 28;
         model = "LENOVO - Lenovo B6000-F";
         platform = android;
         "section_id" = "[]";
         "socket_id" = xSaNB4L4kKmUzBq65cSk;
         type = social;
         "user_id" = 2;
         username = "earljon@vibe.com";
         */
        
        NSString *model = [NSString stringWithFormat:@"%@", d[@"model"] ];
        NSString *user_id = [NSString stringWithFormat:@"%@", d[@"user_id"] ];
        
        NSPredicate *predicate = [self predicateForKeyPath:@"user_id" andValue:user_id];
        NSArray *mo_array = [self getObjectsForEntity:kSocialStreamFeedEntity predicate:predicate];
        for (NSManagedObject *s_mo in mo_array) {
            [s_mo setValue:@"1" forKey:@"status"];
            [s_mo setValue:model forKey:@"model"];
            
        }
        
    }
    
    [self saveTreeContext:ctx];
    
}

- (NSArray *)getObjectsForEntity:(NSString *)entity predicate:(NSPredicate *)predicate {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    NSManagedObjectContext *ctx = _workerContext;
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return items;
    }
    
    return nil;
}

- (void)updateMessageWithCommentData:(NSDictionary *)d {
    
    NSLog(@"update message with comment data...");
    
    /*
     avatar = "/socialstream/public/img/avatar/generic-boy.png";
     "date_created" = "2015-01-30T08:38:57.000Z";
     "date_modified" = "2015-01-30T08:38:57.000Z";
     
     "first_name" = Yuan;
     "last_name" = Yu;
     
     
     "emoticon_id" = "<null>";
     "group_id" = "<null>";
     "icon_text" = "<null>";
     "icon_url" = "<null>";
     id = 78;
     
     "is_deleted" = 0;
     
     message = "this is a comment";
     "parent_id" = 746;
     type = comment;
     "user_id" = 123;
     msg_type =
     */
    
    NSString *message_id = [NSString stringWithFormat:@"%@", d[@"parent_id"] ];
    NSString *is_deleted = [NSString stringWithFormat:@"%@", d[@"is_deleted"] ];
    
    NSString *commentid = [NSString stringWithFormat:@"%@", d[@"id"] ];
    NSString *comment = [NSString stringWithFormat:@"%@", d[@"message"] ];
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    NSString *avatar = [NSString stringWithFormat:@"%@%@", homeurl, d[@"avatar"] ];
    NSString *date_created = [NSString stringWithFormat:@"%@", d[@"date_created"] ];
    NSDate *modifiedDate = [self parseDateFromString:d[@"date_modified"]];
    NSString *relativeDate = [modifiedDate timeAgo];
    NSString *date_modified = [NSString stringWithFormat:@"%@", relativeDate ];
    
    NSString *first_name = [NSString stringWithFormat:@"%@", d[@"first_name"] ];
    NSString *last_name = [NSString stringWithFormat:@"%@", d[@"last_name"] ];
    NSString *user_id = [NSString stringWithFormat:@"%@", d[@"user_id"] ];
    
    NSManagedObjectContext *ctx = _workerContext;
    
    //COMMENT ENTITY UPDATE -----------------------------------------------------------------------------------------------
    
    NSManagedObject *cmo = [self getEntity:kSocialStreamCommentEntity attribute:@"comment_id" parameter:commentid context:ctx];
    if (cmo == nil) {
        cmo = [NSEntityDescription insertNewObjectForEntityForName:kSocialStreamCommentEntity inManagedObjectContext:ctx];
        [cmo setValue:@"0" forKey:@"likeCount"];
    }
    
    [cmo setValue:commentid forKey:@"comment_id"];
    [cmo setValue:comment forKey:@"comment"];
    [cmo setValue:avatar forKey:@"avatar"];
    [cmo setValue:date_created forKey:@"date_created"];
    [cmo setValue:date_modified forKey:@"date_modified"];
    [cmo setValue:first_name forKey:@"first_name"];
    [cmo setValue:last_name forKey:@"last_name"];
    [cmo setValue:is_deleted forKey:@"is_deleted"];
    [cmo setValue:message_id forKey:@"message_id"];
    [cmo setValue:user_id forKey:@"user_id"];
    
    BOOL isOwned = [self ownedByUser:user_id];
    NSString *owned = [NSString stringWithFormat:@"%@", [NSNumber numberWithBool:isOwned]];
    [cmo setValue:owned forKey:@"owned"];
    
    [self saveTreeContext:ctx];
}

- (void)notifiedBySocialStreamController:(NSNotification *)notification {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSString *group_id = [NSString stringWithFormat:@"%@", [notification object] ];
    self.social_stream_group_id = group_id;
}

- (void)updateMessagesWithData:(NSDictionary *)dictionary {
    
    NSDictionary *d = [NSDictionary dictionaryWithDictionary:dictionary];
    
    /*
     avatar = "/img/avatar/generic-girl.png";
     "b_title" = "<null>";
     canonicalUrl = "";
     "date_created" = "2015-02-26T23:36:17.000Z";
     "date_modified" = "2015-02-26T23:36:17.000Z";
     description = "N/A";
     "emoticon_id" = "<null>";
     "first_name" = Arminda;
     "group_id" = 9;
     "icon_text" = "<null>";
     "icon_url" = "<null>";
     id = 276;
     image = no;
     "is_deleted" = 0;
     "is_image" = no;
     "is_message" = 1;
     "is_video" = no;
     "last_name" = Reed;
     message = "socket test";
     "msg_type" = message;
     "parent_id" = "<null>";
     text = " socket test";
     "thumb_img" = no;
     title = "N/A";
     type = message;
     url = "";
     "user_id" = 7;
     */
    
    VS_NCADD(kNotificationReturnGroupIdResourceManager, @selector(notifiedBySocialStreamController:) )
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReceivedNewGroupMessage object:self];
    
    NSString *group_id = [NSString stringWithFormat:@"%@", d[@"group_id"] ];
    if ([self.social_stream_group_id isEqualToString:group_id]) {
        
        NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
        NSString *messageid = [NSString stringWithFormat:@"%@", d[@"id"] ];
        NSString *message = [NSString stringWithFormat:@"%@", d[@"message"] ];
        NSString *canonical_url = [NSString stringWithFormat:@"%@", d[@"canonicalUrl"] ];
        canonical_url = [canonical_url stringByReplacingOccurrencesOfString:@"<null>" withString:@""];
        
        NSManagedObjectContext *ctx = _workerContext;
        
        NSManagedObject *mo = [self getEntity:kSocialStreamFeedEntity attribute:@"message_id" parameter:messageid context:ctx];
        if (mo == nil) {
            mo = [NSEntityDescription insertNewObjectForEntityForName:kSocialStreamFeedEntity inManagedObjectContext:ctx];
        }
        
        NSString *type = @"message"; //Default
        
        // Determine type
        NSString *is_message = [NSString stringWithFormat:@"%@", d[@"is_message"] ];
        if ([is_message isEqualToString:@"0"]) {
            type = @"sticker";
        }
        
        if ([is_message isEqualToString:@"1"] ) {
            //URL PREVIEW SPECIAL HANDLING
            if ( canonical_url.length > 0 ) {
                type = @"image";
            }
        }
        
        if ([is_message isEqualToString:@"2"]) {
            type = @"image";
        }
        
        NSString *avatar = [NSString stringWithFormat:@"%@%@",homeurl,d[@"avatar"] ];
        NSString *date_created = [NSString stringWithFormat:@"%@", d[@"date_created"] ];
        //    NSString *date_modified = [NSString stringWithFormat:@"%@", d[@"date_modified"] ];
        NSString *emoticon_id = [NSString stringWithFormat:@"%@", d[@"emoticon_id"] ];
        NSString *user_id = [NSString stringWithFormat:@"%@", d[@"user_id"] ];
        NSString *first_name = [NSString stringWithFormat:@"%@", d[@"first_name"] ];
        NSString *last_name = [NSString stringWithFormat:@"%@", d[@"last_name"] ];
        NSString *icon_text = [NSString stringWithFormat:@"%@", d[@"icon_text"] ];
        if ([icon_text isEqualToString:@"<null>"]) {
            icon_text = @"";
        }
        
        NSString *icon_url = [NSString stringWithFormat:@"%@%@",homeurl, d[@"icon_url"] ];
        NSString *is_deleted = [NSString stringWithFormat:@"%@", d[@"is_deleted"] ];
        //    NSString *parent_id = [NSString stringWithFormat:@"%@", d[@"parent_id"] ];
        
        //    NSManagedObjectContext *ctx = _workerContext;
        //
        //    NSManagedObject *mo = [self getEntity:kSocialStreamFeedEntity attribute:@"message_id" parameter:messageid context:ctx];
        //    if (mo == nil) {
        //        mo = [NSEntityDescription insertNewObjectForEntityForName:kSocialStreamFeedEntity inManagedObjectContext:ctx];
        //    }
        
        //DEFUALT
        [mo setValue:icon_text forKey:@"icon_text"];
        [mo setValue:@"0" forKey:@"likeCount"];
        [mo setValue:@"0" forKey:@"commentCount"];
        
        [mo setValue:messageid forKey:@"message_id"];
        [mo setValue:message forKey:@"message"];
        [mo setValue:avatar forKey:@"avatar"];
        [mo setValue:date_created forKey:@"date_created"];
        
        NSDate *modifiedDate = [self parseDateFromString:d[@"date_modified"]];
        NSString *date_modified = [NSString stringWithFormat:@"%@", [modifiedDate timeAgo] ];
        [mo setValue:date_modified forKey:@"date_modified"];
        [mo setValue:emoticon_id forKey:@"emoticon_id"];
        [mo setValue:user_id forKey:@"user_id"];
        [mo setValue:first_name forKey:@"first_name"];
        [mo setValue:last_name forKey:@"last_name"];
        [mo setValue:group_id forKey:@"group_id"];
        [mo setValue:icon_text forKey:@"icon_text"];
        [mo setValue:icon_url forKey:@"icon_url"];
        [mo setValue:is_deleted forKey:@"is_deleted"];
        [mo setValue:type forKey:@"type"];
        
        BOOL isOwned = [self ownedByUser:user_id];
        NSString *owned = [NSString stringWithFormat:@"%@", [NSNumber numberWithBool:isOwned]];
        [mo setValue:owned forKey:@"owned"];
        
        [self saveTreeContext:ctx];
    } else {
        
    }
    
}

- (void)updateLikeMessageWithData:(NSDictionary *)d {
    
    /*
     "parent_id" = 580;
     avatar = "/socialstream/public/img/avatar/generic-boy.png";
     "date_created" = "2015-01-23T06:27:21.000Z";
     "date_modified" = "2015-01-23T06:27:21.000Z";
     "emoticon_id" = "<null>";//COMMENT
     "first_name" = Yuan;
     "last_name" = Yu;
     "group_id" = "<null>";//COMMENT
     "icon_text" = "<null>";//COMMENT
     "icon_url" = "<null>";//COMMENT
     id = 120;
     "is_deleted" = 0;
     message = "<null>";//COMMENT
     type = "message_likes";
     
     "user_id" = 123;
     */
    
    NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
    NSString *like_id = [NSString stringWithFormat:@"%@", d[@"id"] ];
    NSString *messageid = [NSString stringWithFormat:@"%@", d[@"parent_id"] ];
    NSString *avatar = [NSString stringWithFormat:@"%@%@",homeurl,d[@"avatar"] ];
    NSString *date_created = [NSString stringWithFormat:@"%@", d[@"date_created"] ];
    NSDate *modifiedDate = [self parseDateFromString:d[@"date_modified"]];
    NSString *relativeDate = [modifiedDate timeAgo];
    NSString *date_modified = [NSString stringWithFormat:@"%@", relativeDate ];
    //    NSString *emoticon_id = [NSString stringWithFormat:@"%@", d[@"emoticon_id"] ];
    NSString *first_name = [NSString stringWithFormat:@"%@", d[@"first_name"] ];
    NSString *last_name = [NSString stringWithFormat:@"%@", d[@"last_name"] ];
    //    NSString *group_id = [NSString stringWithFormat:@"%@", d[@"group_id"] ];
    //    NSString *icon_text = [NSString stringWithFormat:@"%@", d[@"icon_text"] ];
    //    NSString *icon_url = [NSString stringWithFormat:@"%@%@",homeurl, d[@"icon_url"] ];
    NSString *is_deleted = [NSString stringWithFormat:@"%@", d[@"is_deleted"] ];
    //    NSString *message = [NSString stringWithFormat:@"%@", d[@"message"] ];
    NSString *user_id = [NSString stringWithFormat:@"%@", d[@"user_id"] ];
    
    
    
    // else {
    
    
    NSManagedObjectContext *ctx = _workerContext;
    
    [ctx performBlock:^{
        
        // UPDATE FEED ENTITY ---------------------------------
        
        NSManagedObject *mo = [self getEntity:kSocialStreamFeedEntity attribute:@"message_id" parameter:messageid context:ctx];
        
        // DITO AKO BABALIK SA IS LIKED
        //        NSString *is_liked = ([is_deleted isEqualToString:@"1"]) ? @"0" : @"1";
        //        [mo setValue:is_liked forKey:@"is_liked"];
        
        //BUG FIX
        AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
        NSString *account_user_id = [NSString stringWithFormat:@"%d", account.user.id];
        NSString *is_liked = ([user_id isEqualToString:account_user_id]) ? @"1" : @"0";
        if (([is_deleted isEqualToString:@"1"])) {
            is_liked = @"0";
        }
        [mo setValue:is_liked forKey:@"is_liked"];
        
        // BUG FIX: Unliking not working well
        //            NSString *is_liked = ([is_deleted isEqualToString:@"1"]) ? @"0" : @"1";
        //            [mo setValue:is_liked forKey:@"is_liked"];
        
        [self saveTreeContext:ctx];
        
        // UPDATE LIKE ENTITY ---------------------------------
        
        NSManagedObject *lmo = [self getEntity:kSocialStreamLikeEntity attribute:@"like_id" parameter:like_id context:ctx];
        
        if (lmo == nil) {
            lmo = [NSEntityDescription insertNewObjectForEntityForName:kSocialStreamLikeEntity inManagedObjectContext:ctx];
        }
        
        [lmo setValue:like_id forKey:@"like_id"];
        [lmo setValue:messageid forKey:@"message_id"];
        [lmo setValue:avatar forKey:@"avatar"];
        [lmo setValue:date_created forKey:@"date_created"];
        [lmo setValue:date_modified forKey:@"date_modified"];
        [lmo setValue:first_name forKey:@"first_name"];
        [lmo setValue:last_name forKey:@"last_name"];
        [lmo setValue:is_deleted forKey:@"is_deleted"];
        [lmo setValue:user_id forKey:@"user_id"];
        
        NSUInteger count = [[mo valueForKey:@"likeCount"] integerValue];
        
        // Increment
        //        count =  ([is_deleted isEqualToString:@"1"]) ? (count - 1) : (count + 1);
        //        NSString *increment = [NSString stringWithFormat:@"%lu", (unsigned long)count];
        //        [mo setValue:increment forKey:@"likeCount"];
        
        // BUG FIX: Inaccurate count of likes (#735)
        // Count must not become negative
        if ([is_deleted isEqualToString:@"1"]) {
            if (count > 0) {
                count--;
            }
        }
        
        if ([is_deleted isEqualToString:@"0"]) {
            count++;
        }
        
        NSString *increment = [NSString stringWithFormat:@"%lu", (unsigned long)count];
        
        [mo setValue:increment forKey:@"likeCount"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReceivedRealTimeDataLike object:self];
        
        [self saveTreeContext:ctx];
    }];
    //    }
}

- (BOOL)ownedByUser:(NSString *)userid {
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    
    return [user_id isEqualToString:userid];
}

- (void)requestPreviewURL:(NSString *)url doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"text/html" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"text/html" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"html string error %@", [error localizedDescription]);
        }
        
        if (!error) {
            NSString *htmlString = [NSString stringWithUTF8String:[data bytes]];
            if (htmlString) {
                NSLog(@"html string : %@", htmlString);
                TBXML *tbxml = [TBXML tbxmlWithXMLString:htmlString];
                if (tbxml.rootXMLElement) {
                    [self traverseXMLElement:tbxml.rootXMLElement];
                }
            }
        }
    }];
    
    [task resume];
}

- (NSPredicate *)textBookPredicateKey:(NSString *)key value:(NSNumber *)number {
    
    NSExpression *left = [NSExpression expressionForKeyPath:key];
    NSExpression *right = [NSExpression expressionForConstantValue:number];
    
    NSComparisonPredicateOptions options = NSDiacriticInsensitivePredicateOption|NSCaseInsensitivePredicateOption;
    
    NSPredicate *predicate = [NSComparisonPredicate predicateWithLeftExpression:left
                                                                rightExpression:right
                                                                       modifier:NSDirectPredicateModifier
                                                                           type:NSEqualToPredicateOperatorType
                                                                        options:options];
    
    return predicate;
}

#pragma mark - TextBook Module

- (NSOrderedSet *)getCacheBookItems {
    
    NSManagedObjectContext *ctx = self.workerContext;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kTextBookEntity];
    request.predicate = [NSPredicate predicateWithFormat:@"download_status == %@", @(VSmartDownloadNotStarted)];
    request.resultType = NSDictionaryResultType;
    request.propertiesToFetch = @[@"sku"];
    request.returnsDistinctResults = YES;
    
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    NSMutableSet *list = [NSMutableSet set];
    if ([items count] > 0) {
        for (NSDictionary *d in items) {
            [list addObject:d[@"sku"]];
        }
        return [NSOrderedSet orderedSetWithSet:list];
    }
    
    return [NSOrderedSet orderedSet];
}

- (void)requestBookList:(ResourceManagerListBlock)listBlock {
    
    NSURL *booklistURL = [NSURL URLWithString:kServiceURLBookList];
    NSDictionary *oAuthResponse = [[NSUserDefaults standardUserDefaults] valueForKey:kOAuthResponseKey];
    NSString *oAuthToken = [oAuthResponse valueForKey:@"access_token"];
    NSString *authorization = [NSString stringWithFormat:@"Bearer %@", oAuthToken];
    
    AccountInfo *account = [Utils getArchive:kGlobalAccount];
    NSString *useridPATH = VS_FMT(@"%@", account.user.email);
    
    NSLog(@"book list URL : %@", booklistURL);
    NSLog(@"oauth token : %@", oAuthToken);
    NSLog(@"Authorization : %@", authorization);
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:booklistURL body:nil];
    [request setValue:authorization forHTTPHeaderField:@"Authorization"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
              
              if (listBlock) {
                  listBlock([NSArray array]);
              }
          }
          
          if (!error) {
              
              if (responsedata) {
                  NSDictionary *dictionary = [self parseResponseData:responsedata];
                  NSLog(@"dictionary : %@", dictionary);
                  
                  if (dictionary != nil) {
                      NSArray *records = dictionary[@"result"];
                      
                      if (records != nil) {
                          
                          
                          //////////////////////////////////////////////////////////////////////////////////////////////
                          // BOOK MAPPING AND UNMAPPING IMPLEMENTATION
                          //////////////////////////////////////////////////////////////////////////////////////////////
                          NSMutableArray *unmapList = [NSMutableArray array];
//                          if (records.count > 0) { // TEXTBOOK FIX, UNMAPP LAST TEXT BOOK
                              // PARSE REMOTE LIST OF SKUs
                              NSMutableSet *remoteList = [NSMutableSet set];
                              for (NSDictionary *d in records) {
                                  NSString *sku = [self stringValue: d[@"sku"] ];
                                  [remoteList addObject:sku];
                              }
                              NSOrderedSet *orderedRemoteList = [NSOrderedSet orderedSetWithSet:remoteList];
                              
                              // GET CACHE SKUs for comparison
                              NSOrderedSet *orderedCacheList = [self getCacheBookItems];
                              
                              NSLog(@"remote : %@ cache: %@", orderedRemoteList, orderedCacheList);
                              
                              for (NSString *j in orderedCacheList) {
                                  BOOL status = [orderedRemoteList containsObject:j];
                                  if (status == YES) {
                                      NSLog(@"contains : %@", j);
                                  }
                                  if (status == NO) {
                                      NSLog(@"does not contain : %@", j);
                                      // mark as delete item
                                      NSPredicate *p1 = [self predicateForKeyPath:@"sku" andValue:j];
                                      [unmapList addObject:p1];
                                  }
                              }
//                          }
                          //////////////////////////////////////////////////////////////////////////////////////////////
                          // BOOK MAPPING AND UNMAPPING IMPLEMENTATION
                          //////////////////////////////////////////////////////////////////////////////////////////////
                          
                          NSMutableArray *result_items = [NSMutableArray array];
                          NSManagedObjectContext *ctx = _workerContext;
                          [ctx performBlock:^{
                              
                              //START UNMAPPING
                              if (unmapList.count > 0) {
                                  NSArray *predicateList = [NSArray arrayWithArray:unmapList];
                                  NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicateList];
                                  NSLog(@"PREDICATES : %@", predicate);
                                  [self clearDataForEntity:kTextBookEntity withPredicate:predicate context:ctx];
                              }
                              
                              if (records.count > 0) {
                              
                                  for (NSDictionary *d in records) {
                                      //                              NSLog(@"book item : %@", d);
                                      
                                      /*
                                       authors = ( { "author_name" = "Araceli M. Villamin, et al";
                                       name = "Araceli M. Villamin, et al";
                                       ordering = 0; } );
                                       "book_id" = 80;
                                       categories = ( );
                                       category = ( );
                                       "content_protection" = "Vibe DRM 1.0";
                                       "content_type" = epub;
                                       cover = "http://dcsr.store.vsmartschool.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/e/p/epub_cover_9.png";
                                       currency = PHP;
                                       "download_link" = "http://dcsr.store.vsmartschool.com/storeapi/product/download/sku/VIBAL-PLP7-B3E15B26BF16BEDE78D48248594727578552660E19FE6C5520DB1/";
                                       "estimated_file_size" = 5300795;
                                       "estimated_size_in_bytes" = 5300795;
                                       "expires_in" = 31018123;
                                       "expiry_date" = 1466756323;
                                       identifier = "978\U00e2\U0080\U0093971\U00e2\U0080\U009306\U00e2\U0080\U00933406\U00e2\U0080\U00934";
                                       image = "http://dcsr.store.vsmartschool.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/e/p/epub_cover_9.png";
                                       "media_type" = epub;
                                       price = 0;
                                       publisher = "Unknown Publisher";
                                       "purchase_at" = 1435133923;
                                       "purchase_date" = 1435133923;
                                       sku = "VIBAL-PLP7-B3E15B26BF16BEDE78D48248594727578552660E19FE6C5520DB1";
                                       thumbnail = "http://dcsr.store.vsmartschool.com/media/catalog/product/cache/1/image/300x/17f82f742ffe127f42dca9de82fb58b1/e/p/epub_cover_9.png";
                                       title = "PLP 7";
                                       uuid = "978\U00e2\U0080\U0093971\U00e2\U0080\U009306\U00e2\U0080\U00933406\U00e2\U0080\U00934";
                                       "web_link" = "http://dcsr.store.vsmartschool.com/catalog/product/view/id/80/s/plp-7/";
                                       */
                                      
                                      //                              NSString *book_id = [NSString stringWithFormat:@"%@", d[@"book_id"] ];
                                      NSString *content_protection = [self stringValue: d[@"content_protection"] ];
                                      NSString *cover = [self stringValue: d[@"cover"] ];
                                      NSString *currency = [self stringValue: d[@"currency"] ];
                                      NSString *download_link = [self stringValue: d[@"download_link"] ];
                                      NSString *estimated_file_size = [self stringValue: d[@"estimated_file_size"] ];
                                    //                          NSString *estimated_size_in_bytes = [NSString stringWithFormat:@"%@", d[@"estimated_size_in_bytes"] ]; //WHY IS THIS NOT USED
                                    //                          NSString *expires_in = [NSString stringWithFormat:@"%@", d[@"expires_in"] ]; //WHY IS THIS NOT USED
                                      NSString *expiry_date = [self stringValue: d[@"expiry_date"] ];
                                      //                          NSString *identifier = [NSString stringWithFormat:@"%@", d[@"identifier"] ]; //WHY IS THIS NOT USED
                                      NSString *image = [self stringValue: d[@"image"] ];
                                      NSString *media_type = [self stringValue: d[@"media_type"] ];
                                      NSString *price = [self stringValue: d[@"price"] ];
                                      NSString *publisher = [self stringValue: d[@"publisher"] ];
                                      //                          NSString *purchase_at = [NSString stringWithFormat:@"%@", d[@"purchase_at"] ]; //WHY IS THIS NOT USED
                                      NSString *purchase_date = [self stringValue: d[@"purchase_date"] ];
                                      NSString *sku = [self stringValue: d[@"sku"] ];
                                      NSString *thumbnail = [self stringValue: d[@"thumbnail"] ];
                                      NSString *title = [self stringValue: d[@"title"] ];
//                                      NSString *uuid = [self stringValue: d[@"uuid"] ]; //WHY IS THIS NOT USED
                                      NSString *web_link = [self stringValue: d[@"web_link"] ];
                                      
                                      // JCA-07172017: Fix for books whose UUID contains UTF-8 accents
                                      // UUID is used as a unique book identifier replacing bookId (Sir Ryan's).
                                      // UUID which contains UTF-8 accents used unicode characterization when
                                      // saved as a directory name resulting to problem why epub path can't be
                                      // found when extracting epub.
                                      NSLog(@"=================> BOOK TITLE: %@", title);
                                      NSString *uuid = [self stringValue: d[@"uuid"]];
                                      uuid = [self removeUTF8AccentsFromString:uuid];
                                      uuid = [uuid isEqualToString:@""] ? [self stringValue: d[@"book_id"]] : uuid;
                                      
                                      NSString *isbn = @"";
                                      if (d[@"isbn"] != nil) {
                                          isbn = [NSString stringWithFormat:@"%@", d[@"isbn"] ];
                                          if ([isbn isEqualToString:@"(null)"]) {
                                              isbn = @"";
                                          }
                                      }
                                      
                                      NSString *content_type = [self stringValue: d[@"content_type"] ]; //WHY IS THIS NOT USED
                                      
                                      //BUILD AUTHOR LIST
                                      NSMutableArray *author_list = [NSMutableArray array];
                                      
                                      /////////////
                                      //EPUB BOOK
                                      /////////////
                                      
                                      // BUG FIX
                                      if ([content_type isEqualToString:@"epub"] || [content_type isEqualToString:@"epub,2.0"]) {
                                          NSArray *author_items = d[@"authors"];
                                          if (author_items.count > 0) {
                                              for (NSDictionary *item in author_items) {
                                                  NSString *author_name = [self stringValue: item[@"author_name"] ];
                                                  NSString *name = [self stringValue: item[@"name"] ];
                                                  NSString *ordering = [self stringValue:  item[@"ordering"] ];
                                                  
                                                  Author *author = [[Author alloc] init];
                                                  author.authorName = author_name;
                                                  author.name = name;
                                                  author.ordering = [ordering intValue];
                                                  
                                                  [author_list addObject:author];
                                              }
                                          }
                                      }
                                      
                                      if ([content_type isEqualToString:@"pdf"]) {
                                          
                                          NSString *author_name = @"";
                                          NSString *name = @"";
                                          NSString *ordering = @"";
                                          
                                          if ([d[@"authors"] isKindOfClass:[NSArray class]]) {
                                              
                                              for (NSDictionary *author_dictionary in d[@"authors"]) {
                                                  
                                                  author_name = [self stringValue: author_dictionary[@"author_name"] ];
                                                  name = [self stringValue: author_dictionary[@"name"] ];
                                                  ordering = [self stringValue: author_dictionary[@"ordering"] ];
                                                  
                                                  Author *author = [[Author alloc] init];
                                                  author.authorName = author_name;
                                                  author.name = name;
                                                  author.ordering = [ordering intValue];
                                                  
                                                  [author_list addObject:author];
                                              }
                                              
                                          }
                                          
                                          if ([d[@"authors"] isKindOfClass:[NSDictionary class]]) {
                                              
                                              NSDictionary *author_dictionary = d[@"authors"];
                                              author_name = [self stringValue:  author_dictionary[@"author_name"] ];
                                              name = [self stringValue: author_dictionary[@"name"] ];
                                              ordering = [self stringValue: author_dictionary[@"ordering"] ];
                                              
                                              Author *author = [[Author alloc] init];
                                              author.authorName = author_name;
                                              author.name = name;
                                              author.ordering = [ordering intValue];
                                              
                                              [author_list addObject:author];
                                          }
                                          
                                      }
                                      
                                      
                                      //BUILD BOOK OBJECT
                                      Book *b = [[Book alloc] init];
                                      
                                      /*
                                       @property (nonatomic, strong) NSArray<Author> *authors;
                                       @property (nonatomic, strong) NSString *bookId;
                                       @property (nonatomic, strong) NSString<Optional> *contentProtection;
                                       @property (nonatomic, strong) NSString<Optional> *cover;
                                       @property (nonatomic, strong) NSString<Optional> *currency;
                                       @property (nonatomic, strong) NSString<Optional> *downloadLink;
                                       @property (nonatomic, assign) NSInteger estimatedFileSize;
                                       @property (nonatomic, assign) int expiryDate;
                                       @property (nonatomic, strong) NSString<Optional> *image;
                                       @property (nonatomic, strong) NSString<Optional> *isbn;
                                       @property (nonatomic, strong) NSString<Optional> *mediaType;
                                       @property (nonatomic, assign) int price;
                                       @property (nonatomic, strong) NSString<Optional> *publisher;
                                       @property (nonatomic, assign) int purchaseDate;
                                       @property (nonatomic, strong) NSString<Optional> *sku;
                                       @property (nonatomic, strong) NSString<Optional> *thumbnail;
                                       @property (nonatomic, strong) NSString<Optional> *title;
                                       @property (nonatomic, strong) NSString<Optional> *webLink;
                                       */
                                      
                                      b.authors = (NSArray<Author> *)author_list;
                                      b.bookId = uuid; //book_id; //UUID IS MORE STABLE INDENTIFIER
                                      b.contentProtection = content_protection;
                                      b.cover = cover;
                                      b.currency = currency;
                                      b.downloadLink = download_link;
                                      b.estimatedFileSize = [estimated_file_size integerValue];
                                      b.expiryDate = [expiry_date intValue];
                                      b.image = image;
                                      b.isbn = isbn;
                                      b.mediaType = media_type;
                                      b.price = [price intValue];
                                      b.publisher = publisher;
                                      b.purchaseDate = [purchase_date intValue];
                                      b.sku = sku;
                                      b.thumbnail = thumbnail;
                                      b.title = title;
                                      b.webLink = web_link;
                                      b.fileSizeString = estimated_file_size;
                                      [result_items addObject:b];
                                      
                                      ///////////////////////////////////////////////////////////////
                                      // FIND BOOK ID AND USER ID
                                      ///////////////////////////////////////////////////////////////
                                      NSPredicate *sp1 = [self predicateForKeyPath:@"book_id" andValue:b.bookId];
                                      NSPredicate *sp2 = [self predicateForKeyPath:@"user_id" andValue:useridPATH];
                                      NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates: @[sp1, sp2]];
                                      NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kTextBookEntity];
                                      [fetchRequest setPredicate:predicate];
                                      NSError *error = nil;
                                      NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
                                      
                                      NSManagedObject *mo = nil;
                                      //IF IT EXISTS
                                      if ([items count] > 0) {
                                          mo = (NSManagedObject *)[items lastObject];
                                      }
                                      
                                      //IF IT DOESN'T EXISTS
                                      if (mo == nil) {
                                          mo = [NSEntityDescription insertNewObjectForEntityForName:kTextBookEntity inManagedObjectContext:ctx];
                                      }
                                      
                                      ///////////////////////////////////////////////////////////////
                                      // UPDATE BOOK LIST
                                      ///////////////////////////////////////////////////////////////
                                      NSString *thumbnail_url = [self stringValue: b.cover];
//                                      NSData *image_data = [mo valueForKey:@"thumbnail"];
//                                      if (image_data == nil) {
//                                          [self prefetchImageDataUsingBookID:b.bookId withURL:thumbnail_url];
//                                      }
                                      ///////////////////////////////////////////////////////////////
                                      
                                      // BOOL exist = [VSmartHelpers isBookOnShelf:b.bookId];
                                      // VSmartDownloadStatus downloadStatus = (exist) ? VSmartDownloadExtracted : VSmartDownloadNotStarted;
                                      
                                      VSmartDownloadStatus downloadStatus = VSmartDownloadNotStarted;
                                      float moProgress = [[mo valueForKey:@"progress"] floatValue];
                                      NSLog(@"moProgress: %f", moProgress);
                                      
                                      // Check if finished
                                      if (!(moProgress < 1)) { // PROGRESS IS 1.0 ==> 100%
                                          if ([VSmartHelpers isBookOnShelf:b.bookId thruAction:@"1"]) {
                                              downloadStatus = VSmartDownloadExtracted;
                                              NSLog(@"Book is on shelf as part of downloaded books.");
                                          }
                                          else if ([VSmartHelpers isBookOnShelf:b.bookId thruAction:@"2"]) {
                                              downloadStatus = VSmartDownloadExtracted;
                                              NSLog(@"Book is on shelf as part of imported books.");
                                          }
                                          else {
                                              downloadStatus = VSmartDownloadNotStarted;
                                          }
                                      }
                                      
                                      if ( (moProgress > 0) && (moProgress < 1)) {
                                          downloadStatus = VSmartDownloadPause;
                                      }
                                      
                                      
                                      NSLog(@"XXX [%@] download status : %@", title, @(downloadStatus) );
                                      
                                      
                                      ///////////////////////////////////////////////////////////////
                                      // BUILD CODE DATA OBJECT
                                      ///////////////////////////////////////////////////////////////
                                      NSDictionary *data = @{@"download_active": @NO,
                                                             @"book_id": b.bookId,
                                                             @"book": b,
                                                             @"progress": [NSNumber numberWithFloat:moProgress],
                                                             @"download_status": @(downloadStatus),
                                                             @"user_id" : useridPATH };
                                      
                                      [mo setValue:data[@"book_id"] forKey:@"book_id"];
                                      [mo setValue:data[@"book"] forKey:@"book"];
                                      
                                      //SKU SAVE
                                      [mo setValue:sku forKey:@"sku"];
                                      
                                      float dataProgress = [data[@"progress"] floatValue];//notif
                                      if (dataProgress > moProgress) {
                                          [mo setValue:data[@"progress"] forKey:@"progress"];
                                      }
                                      [mo setValue:data[@"download_active"] forKey:@"download_active"];
                                      [mo setValue:data[@"download_status"] forKey:@"download_status"];
                                      [mo setValue:data[@"user_id"] forKey:@"user_id"];
                                      [mo setValue:thumbnail_url forKey:@"thumbnail_url"];
                                  }
                                  
                              }
                              
                              [self saveTreeContext:ctx];
                              
                          }];
                          
                          //BUILD LIST
                          NSArray *results = [NSArray arrayWithArray:result_items];
                          
                          //SAVE FOR USER
                          NSString *userBooks = VS_FMT(kGlobalBooks, account.user.email);
                          [Utils saveArchive:results forKey:userBooks];
                          
                          ///////////////////////////////////
                          // GET IMPORT BOOKS FROM ARCHIVE
                          ///////////////////////////////////
                          // TODO
                          
                          if (listBlock) {
                              listBlock(results);
                          }
                      }
                  }
                  
                  if (dictionary == nil) {
                      
                      NSString *status = @"Unable to download book list";
                      NSString *message = NSLocalizedString(@"please contact technical support", nil);
                      NSString *object = [NSString stringWithFormat:@"%@, %@", status, message];
                      NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
                      [nc postNotificationName:kNofiticationBookDownloadError object:object];
                      
                      if (listBlock) {
                          listBlock([NSArray array]);
                      }
                  }
              }
          }
      }];
    
    NSBlockOperation *blockOperation = [NSBlockOperation blockOperationWithBlock:^{
            [task resume];
    }];
    [self.queue addOperation:blockOperation];
}

- (NSString *)removeUTF8AccentsFromString:(NSString *)string {
    NSMutableString *newString = [string mutableCopy];
    CFStringTransform((__bridge CFMutableStringRef)newString, NULL, kCFStringTransformStripCombiningMarks, NO);
    
    NSLog(@"REMOVING UTF-8 ACCENTS FROM STRING");
    NSLog(@"==========> OLD STRING: %@", string);
    NSLog(@"==========> NEW STRING: %@", newString);
    
    if (newString == nil || newString == NULL) {
        NSLog(@"==========> NEW STRING is either nil or null.");
        newString = [NSMutableString stringWithString:@""];
    }
    
    return newString;
}

//DOWNLOAD OPERATION
- (void)requestBookID:(NSString *)bookid sku:(NSString *)sku {
    
    if ( (sku == nil) && (sku.length == 0) ) {
        NSLog(@"BOOK DOES NOT HAVE SKU!!!");
        return;
    }
    
    NSString *version = [self stringValue:[Utils getServerVersion]];
    BOOL flag = [version isEqualToString:@"1"];
    NSString *endpoint =  flag ? kServiceURLRequestDL : kServiceURLRequestDLB;
    NSString *url = [NSString stringWithFormat:@"%@%@", endpoint, sku];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSLog(@"DOWNLOAD REQUEST %@", request.URL.absoluteString);
    
    // RETRIEVE ACCESS_TOKEN
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictionary = [ud valueForKey:kOAuthResponseKey];
    NSString *access_token = [self stringValue: dictionary[@"access_token"] ];
    NSString *authorization = [NSString stringWithFormat:@"Bearer %@", access_token];
    [request setValue:authorization forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"GET"];
    
    NSLog(@"HEADERS : %@", request.allHTTPHeaderFields);
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    [self requestBookID:bookid sku:sku version:version session:session request:request];
}

- (void)requestBookID:(NSString *)bookid sku:(NSString *)sku version:(NSString *)version session:(NSURLSession *)session
              request:(NSMutableURLRequest *)request {
    
    __weak typeof(self) wo = self;
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      //ERROR
                                      if (error != nil) {
                                          NSString *error_string = [error localizedDescription];
                                          NSLog(@"%s error : %@", __PRETTY_FUNCTION__, error_string);
                                          [self downloadErrorForBook:bookid sku:sku];
                                      }
                                      
                                      NSDictionary *d = [self parseResponseData:responsedata];
                                      if (d != nil) {
                                          NSLog(@"DATA : %@", d);
                                          
                                          if (d[@"status"] != nil) {
                                              
                                              NSString *status = [self stringValue: d[@"status"] ];
                                              
                                              if ([status isEqualToString:@"completed"]){
                                                  
                                                  //DEFAULT PASS KEY
                                                  NSString *passkey = @"";
                                                  if (d[@"passkey"] != nil) {
                                                      passkey = [self stringValue: d[@"passkey"]];
                                                  }
                                                  
                                                  NSString *link = [self stringValue: d[@"link"] ];
                                                  NSLog(@"BOOK STORE DOWNLOAD LINK : %@", link);
                                                  [wo downloadBookID:bookid
                                                                 sku:sku
                                                             passkey:passkey
                                                                link:link];
                                              }
                                              
                                              if (![status isEqualToString:@"completed"]) {
                                                  [wo requestBookID:bookid
                                                                sku:sku
                                                            version:version
                                                            session:session
                                                            request:request];
                                              }
                                              
                                          }
                                          
                                          // NO STATUS
                                          // SERVER SIDE ERROR
                                          if (d[@"status"] == nil) {
                                              [self downloadErrorForBook:bookid sku:sku];
                                          }
                                          
                                      }
                                      if (d == nil) {
                                          [wo downloadErrorForBook:bookid sku:sku];
                                      }
                                  }];
    [task resume];
}

- (void)downloadErrorForBook:(NSString *)bookid sku:(NSString *)sku {
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    NSString *status = NSLocalizedString(@"Download error", nil);
    NSString *message = NSLocalizedString(@"please contact technical support", nil);
    NSString *object = [NSString stringWithFormat:@"%@, %@", status, message];
    [nc postNotificationName:kNofiticationBookDownloadError object:object];
    
    NSPredicate *p1 = [self predicateForKeyPath:@"book_id" andValue:bookid];
    NSPredicate *p2 = [self predicateForKeyPath:@"sku" andValue:sku];
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[p1,p2]];
    
    NSManagedObjectContext *ctx = self.workerContext;
    NSManagedObject *mo = [self getEntity:kTextBookEntity predicate:predicate context:ctx];
    
    __weak typeof(self) wo = self;
    [ctx performBlockAndWait:^{
        [mo setValue:@(0) forKey:@"progress"];
        [mo setValue:@NO forKey:@"download_active"];
        [mo setValue:@(VSmartDownloadNotStarted) forKey:@"download_status"];
        [wo saveTreeContext:ctx];
    }];
}

- (void)downloadBookID:(NSString *)bookid sku:(NSString *)sku passkey:(NSString *)passkey link:(NSString *)link {
    
    if (self.lookupList == nil) {
        self.lookupList = [NSMutableDictionary dictionary];
    }
    
    TBDownloader *dl = (TBDownloader *)[self.lookupList objectForKey:bookid];
    
    if (dl == nil) {
        AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
        
        NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
        NSString *user_name = [NSString stringWithFormat:@"%@", account.user.email];
        NSPredicate *predicate = [self predicateForKeyPath:@"book_id" andValue:bookid];
        NSManagedObject *mo = [self getEntity:kTextBookEntity predicate:predicate context:self.workerContext];
        Book *b = [mo valueForKey:@"book"];
        
        NSString *download_url = [NSString stringWithFormat:@"%@", link];
        [mo setValue:download_url forKey:@"download_link"];
        
        // Variables
//        String username = "aquitayw";
//        String sku = "EF517E32-F357-48D4G";
//        String passkey = "251823831";
//        String hashValue = md5(username);
//        String s1 = md5(hashValue + md5(sku));
//        s1 = s1.substring(0, 10);
//        String s2 = md5(passkey);
//        s2 = s2.substring(0, 10);
//        String password = s1 + s2;
//        Log.d("TEST", "----- password: " + password);

        /*
         $username = $data['username'];
         $sku = $data['sku'];
         $hash_value = hash('md5', $username);
         // password
         $password = substr(hash('md5', $hash_value . md5($sku)), 0,10) . substr(hash('md5', $passkey),0,10);
         */
        
        // Data processing
        NSLog(@"PASSKEY : %@", passkey);
        NSString *username_hash = [user_name stringFromMD5];
        NSString *sku_hash = [sku stringFromMD5];
        
        NSString *username_sku = [NSString stringWithFormat:@"%@%@", username_hash, sku_hash];
        NSString *username_sku_hash = [username_sku stringFromMD5];
        
        NSRange range = NSMakeRange(0, 10);
        NSString *prefix = [username_sku_hash substringWithRange:range];
        
        NSString *passkey_hash = [passkey stringFromMD5];
        NSString *suffix = [passkey_hash substringWithRange:range];
        
        NSString *pdfpassword = [NSString stringWithFormat:@"%@%@", prefix, suffix];
        NSLog(@"SAVE PDF PASSWORD : %@", pdfpassword);
        [mo setValue:pdfpassword forKey:@"password"];
        
        [self saveTreeContext:mo.managedObjectContext];
        
        NSDictionary *info = @{@"sku":sku,
                               @"book_id":bookid,
                               @"user_id":user_id,
                               @"book":b,
                               @"mo": mo};
        NSURL *url = [NSURL URLWithString:link];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        dl = [[TBDownloader alloc] initWithBook:info url:request queue:self.queue];
        dl.delegate = self;
        [self.lookupList setObject:dl forKey:bookid];
        [dl start];
    }
}

- (void)didFinishDownloadingBook:(NSString *)bookid {
    
    if (self.lookupList != nil) {
        TBDownloader *dl = (TBDownloader *)[self.lookupList objectForKey:bookid];
        if (dl != nil ) {
            [self.lookupList removeObjectForKey:bookid];
            NSLog(@"attempting to clean resource....");
        }
    }
}

- (void)pauseDownloadForBook:(NSString *)bookid {
    
    if (self.lookupList != nil) {
        TBDownloader *dl = (TBDownloader *)[self.lookupList objectForKey:bookid];
        if (dl != nil ) {
            [dl pause];
        }
    }
}

- (void)resumeDownloadForBook:(NSString *)bookid sku:(NSString *)sku {
    
    NSPredicate *predicate = [self predicateForKeyPath:@"book_id" andValue:bookid];
    NSManagedObject *mo = [self getEntity:kTextBookEntity predicate:predicate context:self.workerContext];
    NSData *resume_data = [mo valueForKey:@"resume_data"];
    
    if (self.lookupList != nil) {
        TBDownloader *dl = (TBDownloader *)[self.lookupList objectForKey:bookid];
        if (dl != nil ) {
            NSLog(@"RESUME DOWNLOAD");
            [dl resume:resume_data];
        }
    }
    
    if (self.lookupList == nil) {
        [mo setValue:@(NO) forKey:@"download_active"];
        [mo setValue:@(VSmartDownloadNotStarted) forKey:@"download_status"];
        [self saveTreeContext:mo.managedObjectContext];
    }
}

- (void)updateDownloadFlagForBook:(NSString *)bookid {
    
    //TODO: Please check network reachability
    
    NSManagedObjectContext *ctx = self.workerContext;
    [ctx performBlockAndWait:^{
        NSPredicate *predicate = [self predicateForKeyPath:@"book_id" andValue:bookid];
        NSManagedObject *mo = [self getEntity:kTextBookEntity predicate:predicate context:self.workerContext];
        
        [mo setValue:@YES forKey:@"download_active"];
        [mo setValue:@(VSmartDownloadInProgress) forKey:@"download_status"];
        [self saveTreeContext:ctx];
    }];
}

- (void)requestVersionCheckBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSURL *versionURL = [NSURL URLWithString:kServiceURLServerVersion];
    NSDictionary *oAuthResponse = [[NSUserDefaults standardUserDefaults] valueForKey:kOAuthResponseKey];
    NSString *oAuthToken = [oAuthResponse valueForKey:@"access_token"];
    NSString *authorization = [NSString stringWithFormat:@"Bearer %@", oAuthToken];
    
    NSLog(@"version URL : %@", versionURL);
    NSLog(@"oauth token : %@", oAuthToken);
    NSLog(@"Authorization : %@", authorization);
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:versionURL body:nil];
    [request setValue:authorization forHTTPHeaderField:@"Authorization"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          //DEFAULT VALUE
          NSString *version = @"1";
          
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
              [Utils setServer:version];
          }
          
          if (!error) {
              
              if (responsedata != nil) {
                  
                  NSDictionary *dictionary = [self parseResponseData:responsedata];
                  NSLog(@"version check dictionary : %@", dictionary);
                  if (dictionary != nil) {
                      NSString *drm_version = [self stringValue: dictionary[@"drm_version"] ];
                      if (![drm_version isEqualToString:@""]) {
                          
                          // note: 0 - old server version
                          //       1 - new server version
                          version = [NSString stringWithFormat:@"%@", drm_version];
                          NSLog(@"DRM VERSION : %@", version);
                          [Utils setServer:version];
                          
                          if (doneBlock) {
                              doneBlock(YES);
                          }
                      }
                  }
                  
                  if (!dictionary) {
                      NSLog(@"STORING DEFAULT DRM VERSION : %@", version);
                      [Utils setServer:version];
                      if (doneBlock) {
                          doneBlock(YES);
                      }
                  }
              }
          }
      }];
    
    [task resume];
}

- (void)requestServerVersionBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    //DEFAULT VALUE
    NSString *version = @"2.1.0";
    [Utils setServerInstanceVersion:version];
    
    NSURL *versionURL = [NSURL URLWithString:kEndPointServiceInstanceVersion];
    NSLog(@"version URL : %@", versionURL);

    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:versionURL body:nil];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
      {
          if (error) {
              NSLog(@"error %@", [error localizedDescription]);
              [Utils setServerInstanceVersion:version];
          }
          
          if (!error) {
              
              if (responsedata != nil) {
                  
                  NSDictionary *dictionary = [self parseResponseData:responsedata];
                  NSLog(@"version check dictionary : %@", dictionary);
                  
                  if (dictionary != nil) {
                      
                      /*
                      "_meta" =     {
                          count = 3;
                          status = SUCCESS;
                      };
                      records =     {
                          code = Mariposa;
                          date = 20161017;
                          number = "2.5.0-beta";
                      };
                       */
                      
                      
                      if (dictionary[@"records"] != nil) {
                          NSDictionary *records = dictionary[@"records"];
                          if (records[@"number"] != nil) {
                              NSString *value = [self stringValue:records[@"number"]];
                              [Utils setServerInstanceVersion:value];
                          }
                      }

                      if (doneBlock) {
                          doneBlock(YES);
                      }
                  }
                  
                  
                  if (!dictionary) {
                      NSLog(@"STORING DEFAULT SERVER VERSION : %@", version);
                      [Utils setServerInstanceVersion:version];
                      if (doneBlock) {
                          doneBlock(YES);
                      }
                  }
              }
          }
      }];
    
    [task resume];
}

#pragma mark - ResourceManager Test Player

- (NSArray *)generateCourseList {
    
    NSMutableArray *list = [NSMutableArray array];
    
    int count = 100;
    for (int i = 0; i<count; i++) {
        
        NSDictionary *d = @{ @"course_id" : [NSString stringWithFormat:@"59%d",i],
                             @"course_title" : [NSString stringWithFormat:@"Math 0-%d",i],
                             @"course_description" : @"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                             @"course_schedule" : @"MWF, 1:00-3:00 PM",
                             @"room" : @"E205",
                             @"teacher" : @"Ms Legarda"
                             };
        
        [list addObject:d];
    }
    
    return [NSArray arrayWithArray:list];
}

- (void)requestQuizListForUser:(NSString *)userid course:(NSString *)courseid doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSString *quizlistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointQuizList, userid, courseid]];
    NSLog(@"quiz path : %@", quizlistPath);
    NSURL *quizlistURL = [NSURL URLWithString:quizlistPath];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:quizlistURL body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          
                                          if (dictionary) {
                                              NSLog(@"dictionary : %@", dictionary);
                                              
                                              NSArray *records = dictionary[@"records"];
                                              
                                              if (records > 0) {
                                                  
                                                  NSManagedObjectContext *ctx = _workerContext;
                                                  
                                                  [ctx performBlock:^{
                                                      
                                                      //CLEAR CONTENTS (IMPORTANT)
                                                      BOOL status = [self clearDataForEntity:kQuizItemEntity withPredicate:nil context:ctx];
                                                      
                                                      if (status) {
                                                          
                                                          //REFRESH DATA
                                                          for (NSDictionary *d in records) {
                                                              
                                                              /*
                                                               "allow_review" = 1;
                                                               answered = 0;
                                                               attempts = 20;
                                                               "attempts_count" = 0;
                                                               "category_name" = Test;
                                                               "course_details" = "No details provided";
                                                               "cs_id" = 6;
                                                               "date_close" = "2015-03-28 00:00:00";
                                                               "date_modified" = "2015-02-28 00:00:00";
                                                               "date_open" = "2015-02-28 00:00:00";
                                                               description = "Math 1 Exam for demonstration";
                                                               id = 107;
                                                               "item_count" = 2;
                                                               "learning_skills_name" = Knowledge;
                                                               name = "Math 1 Exam";
                                                               "passing_rate" = 60;
                                                               "proficiency_level_name" = "Begining (B)";
                                                               qid = 1;
                                                               "quiz_stage_name" = Graded;
                                                               "show_feedbacks" = 1;
                                                               "time_limit" = "00:15:00";
                                                               "type_name" = Short;
                                                               uid = 0;
                                                               */
                                                              
                                                              NSString *allow_review = [NSString stringWithFormat:@"%@", d[@"allow_review"] ];
                                                              NSString *answered = [NSString stringWithFormat:@"%@", d[@"answered"] ];
                                                              NSString *attempts = [NSString stringWithFormat:@"%@", d[@"attempts"] ];
                                                              NSString *attempts_count = [NSString stringWithFormat:@"%@", d[@"attempts_count"] ];
                                                              NSString *category_name = [NSString stringWithFormat:@"%@", d[@"category_name"] ];
                                                              NSString *course_details = [NSString stringWithFormat:@"%@", d[@"course_details"] ];
                                                              NSString *cs_id = [NSString stringWithFormat:@"%@", d[@"cs_id"] ];
                                                              NSString *date_close = [NSString stringWithFormat:@"%@", d[@"date_close"] ];
                                                              NSString *date_modified = [NSString stringWithFormat:@"%@", d[@"date_modified"] ];
                                                              NSString *date_open = [NSString stringWithFormat:@"%@", d[@"date_open"] ];
                                                              NSString *desc = [NSString stringWithFormat:@"%@", d[@"description"] ];
                                                              NSString *deployment_id = [NSString stringWithFormat:@"%@", d[@"id"] ];
                                                              NSString *item_count = [NSString stringWithFormat:@"%@", d[@"item_count"] ];
                                                              NSString *learning_skills_name = [NSString stringWithFormat:@"%@", d[@"learning_skills_name"] ];
                                                              NSString *name = [NSString stringWithFormat:@"%@", d[@"name"] ];
                                                              NSString *passing_rate = [NSString stringWithFormat:@"%@", d[@"passing_rate"] ];
                                                              NSString *proficiency_level_name = [NSString stringWithFormat:@"%@", d[@"proficiency_level_name"] ];
                                                              NSString *quiz_id = [NSString stringWithFormat:@"%@", d[@"qid"] ];
                                                              NSString *quiz_stage_name = [NSString stringWithFormat:@"%@", d[@"quiz_stage_name"] ];
                                                              NSString *show_feedbacks = [NSString stringWithFormat:@"%@", d[@"show_feedbacks"] ];
                                                              NSString *time_limit = [NSString stringWithFormat:@"%@", d[@"time_limit"] ];
                                                              NSString *type_name = [NSString stringWithFormat:@"%@", d[@"type_name"] ];
                                                              NSString *uid = [NSString stringWithFormat:@"%@", d[@"uid"] ];
                                                              
                                                              //                                NSManagedObject *mo = [self getEntity:kQuizItemEntity attribute:@"quiz_id" parameter:quiz_id context:ctx];
                                                              //                                if (mo == nil) {
                                                              //                                    mo = [NSEntityDescription insertNewObjectForEntityForName:kQuizItemEntity inManagedObjectContext:ctx];
                                                              //                                }
                                                              
                                                              NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:kQuizItemEntity inManagedObjectContext:ctx];
                                                              
                                                              [mo setValue:allow_review forKey:@"allow_review"];
                                                              [mo setValue:answered forKey:@"answered"];
                                                              [mo setValue:attempts forKey:@"attempts"];
                                                              [mo setValue:attempts_count forKey:@"attempts_count"];
                                                              [mo setValue:category_name forKey:@"category_name"];
                                                              [mo setValue:course_details forKey:@"course_details"];
                                                              [mo setValue:cs_id forKey:@"cs_id"];
                                                              [mo setValue:date_close forKey:@"date_close"];
                                                              [mo setValue:date_modified forKey:@"date_modified"];
                                                              [mo setValue:date_open forKey:@"date_open"];
                                                              [mo setValue:desc forKey:@"desc"];
                                                              [mo setValue:quiz_id forKey:@"quiz_id"];
                                                              [mo setValue:item_count forKey:@"item_count"];
                                                              [mo setValue:learning_skills_name forKey:@"learning_skills_name"];
                                                              [mo setValue:name forKey:@"name"];
                                                              [mo setValue:passing_rate forKey:@"passing_rate"];
                                                              [mo setValue:proficiency_level_name forKey:@"proficiency_level_name"];
                                                              [mo setValue:deployment_id forKey:@"deployment_id"];
                                                              [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
                                                              [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
                                                              [mo setValue:time_limit forKey:@"time_limit"];
                                                              [mo setValue:type_name forKey:@"type_name"];
                                                              [mo setValue:uid forKey:@"uid"];
                                                              
                                                          }
                                                          
                                                          [self saveTreeContext:ctx];
                                                          
                                                          if (doneBlock) {
                                                              doneBlock(YES);
                                                          }
                                                      }
                                                  }];
                                              }
                                          }
                                      }
                                  }];
    
    [task resume];
}

- (NSArray *)parseQuestionChoices:(NSString *)string {
    
    
    NSString *question_choices = [NSString stringWithFormat:@"%@", string ];
    
    question_choices = [question_choices stringByReplacingOccurrencesOfString:@"_qw_" withString:@"["];
    question_choices = [question_choices stringByReplacingOccurrencesOfString:@"_as_" withString:@"{"];
    question_choices = [question_choices stringByReplacingOccurrencesOfString:@"_ad_" withString:@"}"];
    question_choices = [question_choices stringByReplacingOccurrencesOfString:@"_qe_" withString:@"]"];
    
    NSData *jsondata = [question_choices dataUsingEncoding:NSUTF8StringEncoding];
    
    return [self parseResponseData:jsondata];
}


- (void)requestPreRunQuizDetailsForUser:(NSString *)userid quiz:(NSString *)quizid course:(NSString *)courseid doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSString *quizlistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointPreRunQuizDetail, userid, quizid, courseid]];
    NSLog(@"quiz path : %@", quizlistPath);
    NSURL *quizlistURL = [NSURL URLWithString:quizlistPath];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:quizlistURL body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              
                                              NSLog(@"PRE RUN QUIZ : %@", dictionary);
                                              
                                              
                                              NSManagedObjectContext *ctx = _workerContext;
                                              
                                              [ctx performBlock:^{
                                                  
                                                  NSDictionary *r = dictionary[@"records"];
                                                  NSDictionary *quiz_data = r[@"data"];
                                                  NSArray *questions = r[@"questions"];
                                                  
                                                  /*
                                                   "allow_review" = 1;
                                                   attempts = 1000;
                                                   "date_close" = "2015-03-28 00:00:00";
                                                   "date_open" = "2015-02-28 00:00:00";
                                                   description = "Math 1 Exam for demonstration";
                                                   "is_forced_complete" = 1;
                                                   "is_shuffle_answers" = 0;
                                                   "learning_skills_id" = 1;
                                                   "learning_skills_name" = Knowledge;
                                                   name = "Math 1 Exam";
                                                   "passing_rate" = 60;
                                                   password = "";
                                                   "proficiency_level_id" = 1;
                                                   "proficiency_level_name" = "Begining (B)";
                                                   "question_type_name" = Short;
                                                   "quiz_category_id" = 1;
                                                   "quiz_category_name" = Test;
                                                   "quiz_result_type_id" = 1;
                                                   "quiz_settings_id" = 1;
                                                   "quiz_shuffling_mode" = 1;
                                                   "quiz_stage_id" = 1;
                                                   "quiz_stage_name" = Graded;
                                                   "quiz_type_id" = 1;
                                                   "show_feedbacks" = 1;
                                                   "time_limit" = "00:15:00";
                                                   "total_score" = "10.00";
                                                   */
                                                  
                                                  NSString *allow_review = [NSString stringWithFormat:@"%@", quiz_data[@"allow_review"] ];
                                                  NSString *attempts = [NSString stringWithFormat:@"%@", quiz_data[@"attempts"] ];
                                                  NSString *date_close = [NSString stringWithFormat:@"%@", quiz_data[@"date_close"] ];
                                                  NSString *date_open = [NSString stringWithFormat:@"%@", quiz_data[@"date_open"] ];
                                                  NSString *description = [NSString stringWithFormat:@"%@", quiz_data[@"description"] ];
                                                  NSString *is_forced_complete = [NSString stringWithFormat:@"%@", quiz_data[@"is_forced_complete"] ];
                                                  NSString *is_shuffle_answers = [NSString stringWithFormat:@"%@", quiz_data[@"is_shuffle_answers"] ];
                                                  NSString *learning_skills_id = [NSString stringWithFormat:@"%@", quiz_data[@"learning_skills_id"] ];
                                                  NSString *learning_skills_name = [NSString stringWithFormat:@"%@", quiz_data[@"learning_skills_name"] ];
                                                  NSString *name = [NSString stringWithFormat:@"%@", quiz_data[@"name"] ];
                                                  NSString *passing_rate = [NSString stringWithFormat:@"%@", quiz_data[@"passing_rate"] ];
                                                  NSString *password = [NSString stringWithFormat:@"%@", quiz_data[@"password"] ];
                                                  NSString *proficiency_level_id = [NSString stringWithFormat:@"%@", quiz_data[@"proficiency_level_id"] ];
                                                  NSString *proficiency_level_name = [NSString stringWithFormat:@"%@", quiz_data[@"proficiency_level_name"] ];
                                                  NSString *question_type_name = [NSString stringWithFormat:@"%@", quiz_data[@"question_type_name"] ];
                                                  NSString *quiz_category_id = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_category_id"] ];
                                                  NSString *quiz_category_name = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_category_name"] ];
                                                  NSString *quiz_result_type_id = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_result_type_id"] ];
                                                  NSString *quiz_settings_id = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_settings_id"] ];
                                                  NSString *quiz_shuffling_mode = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_shuffling_mode"] ];
                                                  NSString *quiz_stage_id = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_stage_id"] ];
                                                  NSString *quiz_stage_name = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_stage_name"] ];
                                                  NSString *quiz_type_id = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_type_id"] ];
                                                  NSString *show_feedbacks = [NSString stringWithFormat:@"%@", quiz_data[@"show_feedbacks"] ];
                                                  NSString *attempts_count = [NSString stringWithFormat:@"%@", r[@"attempts_count"] ];
                                                  
                                                  NSString *time_limit = [NSString stringWithFormat:@"%@", quiz_data[@"time_limit"] ];
                                                  if ([time_limit isEqualToString:@"(null)"]) {
                                                      time_limit = @"00:00:10";
                                                  }
                                                  NSString *time_limit_seconds = [NSString stringWithFormat:@"%f", [self parseTimeFromString:time_limit] ];
                                                  NSString *total_score = [NSString stringWithFormat:@"%@", quiz_data[@"total_score"] ];
                                                  NSString *total_items = [NSString stringWithFormat:@"%lu", (unsigned long)questions.count ];
                                                  
                                                  NSString *time_remaining = [NSString stringWithFormat:@"%@", r[@"time_remaining"] ];
                                                  if ([time_limit isEqualToString:@"(null)"]) {
                                                      time_remaining = [NSString stringWithFormat:@"%f", [self parseTimeFromString:time_limit]];
                                                  }
                                                  
                                                  NSManagedObject *mo = [self getEntity:kQuizDetailsEntity attribute:@"quiz_id"
                                                                              parameter:quizid context:ctx];
                                                  if (mo == nil) {
                                                      mo = [NSEntityDescription insertNewObjectForEntityForName:kQuizDetailsEntity inManagedObjectContext:ctx];
                                                  }
                                                  
                                                  [mo setValue:quizid forKey:@"quiz_id"];
                                                  [mo setValue:courseid forKey:@"course_id"];
                                                  
                                                  [mo setValue:allow_review forKey:@"allow_review"];
                                                  [mo setValue:attempts forKey:@"attempts"];
                                                  [mo setValue:date_close forKey:@"date_close"];
                                                  [mo setValue:date_open forKey:@"date_open"];
                                                  [mo setValue:description forKey:@"quiz_desc"];
                                                  [mo setValue:is_forced_complete forKey:@"is_forced_complete"];
                                                  [mo setValue:is_shuffle_answers forKey:@"is_shuffle_answers"];
                                                  [mo setValue:is_shuffle_answers forKey:@"is_shuffle_answers"];
                                                  [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
                                                  [mo setValue:learning_skills_name forKey:@"learning_skills_name"];
                                                  [mo setValue:name forKey:@"name"];
                                                  [mo setValue:passing_rate forKey:@"passing_rate"];
                                                  [mo setValue:password forKey:@"password"];
                                                  [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];
                                                  [mo setValue:proficiency_level_name forKey:@"proficiency_level_name"];
                                                  [mo setValue:question_type_name forKey:@"question_type_name"];
                                                  [mo setValue:quiz_category_id forKey:@"quiz_category_id"];
                                                  [mo setValue:quiz_category_name forKey:@"quiz_category_name"];
                                                  [mo setValue:quiz_result_type_id forKey:@"quiz_result_type_id"];
                                                  [mo setValue:quiz_settings_id forKey:@"quiz_settings_id"];
                                                  [mo setValue:quiz_shuffling_mode forKey:@"quiz_shuffling_mode"];
                                                  [mo setValue:quiz_stage_id forKey:@"quiz_stage_id"];
                                                  [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
                                                  [mo setValue:quiz_type_id forKey:@"quiz_type_id"];
                                                  [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
                                                  [mo setValue:time_limit forKey:@"time_limit"];
                                                  [mo setValue:time_limit_seconds forKey:@"time_limit_seconds"];
                                                  [mo setValue:total_score forKey:@"total_score"];
                                                  [mo setValue:total_items forKey:@"total_items"];
                                                  
                                                  //VERY IMPORTANT
                                                  [mo setValue:attempts_count forKey:@"attempts_count"];
                                                  [mo setValue:time_remaining forKey:@"time_remaining"];
                                                  
                                                  for (int i = 0; i < [questions count]; i++) {
                                                      
                                                      NSDictionary *q = (NSDictionary *)questions[i];
                                                      
                                                      NSString *deployment_id = [NSString stringWithFormat:@"%@", q[@"id"] ];
                                                      NSString *points = [NSString stringWithFormat:@"%@", q[@"points"] ];
                                                      NSString *question_id = [NSString stringWithFormat:@"%@", q[@"question_id"] ];
                                                      NSString *question_text = [NSString stringWithFormat:@"%@", q[@"question_text"] ];
                                                      NSString *question_type_id = [NSString stringWithFormat:@"%@", q[@"question_type_id"] ];
                                                      NSString *question_type_name = [NSString stringWithFormat:@"%@", q[@"question_type_name"] ];
                                                      NSString *quiz_id = [NSString stringWithFormat:@"%@", q[@"quiz_id"] ];
                                                      NSString *question_bullet_number = [NSString stringWithFormat:@"%d", i];
                                                      
                                                      NSArray *question_choices = [self parseQuestionChoices: q[@"question_choices"] ];
                                                      
                                                      NSManagedObject *qmo = [self getEntity:kQuestionItemEntity attribute:@"question_id"
                                                                                   parameter:question_id context:ctx];
                                                      if (qmo == nil) {
                                                          qmo = [NSEntityDescription insertNewObjectForEntityForName:kQuestionItemEntity
                                                                                              inManagedObjectContext:ctx];
                                                      }
                                                      
                                                      [qmo setValue:deployment_id forKey:@"id"];
                                                      [qmo setValue:points forKey:@"points"];
                                                      [qmo setValue:question_id forKey:@"question_id"];
                                                      [qmo setValue:question_text forKey:@"question_text"];
                                                      [qmo setValue:question_type_id forKey:@"question_type_id"];
                                                      [qmo setValue:question_type_name forKey:@"question_type_name"];
                                                      [qmo setValue:quiz_id forKey:@"quiz_id"];
                                                      [qmo setValue:question_bullet_number forKey:@"question_bullet_number"];
                                                      
                                                      
                                                      BOOL cleared = [self clearDataForEntity:kQuestionChoiceItemEntity withPredicate:nil context:ctx];
                                                      if (cleared) {
                                                          
                                                          for ( NSDictionary *a in question_choices ) {
                                                              
                                                              /*
                                                               "date_created" = "2015-03-06 15:59:35";
                                                               "date_modified" = "2015-03-06 15:59:35";
                                                               id = 531;
                                                               "is_correct" = 100;
                                                               "is_deleted" = 0;
                                                               "order_number" = 2;
                                                               "question_id" = 135;
                                                               "suggestive_feedback" = "<null>";
                                                               text = Moon;
                                                               */
                                                              
                                                              NSString *date_created = [NSString stringWithFormat:@"%@", a[@"date_created"] ];
                                                              NSString *date_modified = [NSString stringWithFormat:@"%@", a[@"date_modified"] ];
                                                              NSString *answer_id = [NSString stringWithFormat:@"%@", a[@"id"] ];
                                                              NSString *is_correct = [NSString stringWithFormat:@"%@", a[@"is_correct"] ];
                                                              NSString *is_deleted = [NSString stringWithFormat:@"%@", a[@"is_deleted"] ];
                                                              NSString *order_number = [NSString stringWithFormat:@"%@", a[@"order_number"] ];
                                                              NSString *question_id = [NSString stringWithFormat:@"%@", a[@"question_id"] ];
                                                              
                                                              NSString *suggestive_feedback = [NSString stringWithFormat:@"%@", a[@"suggestive_feedback"] ];
                                                              NSString *answer_text = [NSString stringWithFormat:@"%@", a[@"text"] ];
                                                              answer_text = [answer_text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                                              
                                                              NSPredicate *p1 = [self predicateForKeyPath:@"answer_text" andValue:answer_text];
                                                              NSCompoundPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[p1]];
                                                              NSManagedObject *amo = [self getEntity:kQuestionChoiceItemEntity predicate:predicate context:ctx];
                                                              if (amo == nil) {
                                                                  amo = [NSEntityDescription insertNewObjectForEntityForName:kQuestionChoiceItemEntity
                                                                                                      inManagedObjectContext:ctx];
                                                              }
                                                              
                                                              [amo setValue:date_created forKey:@"date_created"];
                                                              [amo setValue:date_modified forKey:@"date_modified"];
                                                              [amo setValue:answer_id forKey:@"id"];
                                                              [amo setValue:is_correct forKey:@"is_correct"];
                                                              [amo setValue:is_deleted forKey:@"is_deleted"];
                                                              [amo setValue:[NSNumber numberWithDouble:[order_number doubleValue]] forKey:@"order_number"];
                                                              [amo setValue:question_id forKey:@"question_id"];
                                                              [amo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
                                                              [amo setValue:answer_text forKey:@"answer_text"];
                                                          }
                                                          
                                                      }
                                                      
                                                      
                                                  }
                                                  
                                                  [self saveTreeContext:ctx];
                                              }];
                                          }
                                          
                                          if (doneBlock) {
                                              doneBlock(YES);
                                          }
                                      }
                                      
                                  }];
    
    [task resume];
}

- (void)requestRunQuizDetailsForUser:(NSString *)userid quiz:(NSString *)quizid course:(NSString *)courseid run:(BOOL)run doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSString *quizlistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointPreRunQuizDetail, userid, quizid, courseid]];
    
    if (run) {
        quizlistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointStartQuiz, userid, quizid, courseid]];
    }
    
    NSLog(@"XXXX %s XXXX quiz path : %@", __PRETTY_FUNCTION__, quizlistPath);
    NSURL *quizlistURL = [NSURL URLWithString:quizlistPath];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:quizlistURL body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              
                                              NSLog(@"dictionary : %@", dictionary);
                                              NSDictionary *r = dictionary[@"records"];
                                              
                                              NSManagedObjectContext *ctx = _workerContext;
                                              
                                              [ctx performBlockAndWait:^{
                                                  
                                                  NSString *status = [NSString stringWithFormat:@"%@", r[@"status"] ];
                                                  NSDictionary *quiz_data = r[@"data"];
                                                  NSArray *questions = r[@"questions"];
                                                  
                                                  if ([status isEqualToString:@"ERROR"]) {
                                                  }
                                                  
                                                  /*
                                                   "allow_review" = 1;
                                                   attempts = 1000;
                                                   "date_close" = "2015-03-28 00:00:00";
                                                   "date_open" = "2015-02-28 00:00:00";
                                                   description = "Math 1 Exam for demonstration";
                                                   "is_forced_complete" = 1;
                                                   "is_shuffle_answers" = 0;
                                                   "learning_skills_id" = 1;
                                                   "learning_skills_name" = Knowledge;
                                                   name = "Math 1 Exam";
                                                   "passing_rate" = 60;
                                                   password = "";
                                                   "proficiency_level_id" = 1;
                                                   "proficiency_level_name" = "Begining (B)";
                                                   "question_type_name" = Short;
                                                   "quiz_category_id" = 1;
                                                   "quiz_category_name" = Test;
                                                   "quiz_result_type_id" = 1;
                                                   "quiz_settings_id" = 1;
                                                   "quiz_shuffling_mode" = 1;
                                                   "quiz_stage_id" = 1;
                                                   "quiz_stage_name" = Graded;
                                                   "quiz_type_id" = 1;
                                                   "show_feedbacks" = 1;
                                                   "time_limit" = "00:15:00";
                                                   "total_score" = "10.00";
                                                   */
                                                  
                                                  NSString *allow_review = [NSString stringWithFormat:@"%@", quiz_data[@"allow_review"] ];
                                                  NSString *attempts = [NSString stringWithFormat:@"%@", quiz_data[@"attempts"] ];
                                                  NSString *date_close = [NSString stringWithFormat:@"%@", quiz_data[@"date_close"] ];
                                                  NSString *date_open = [NSString stringWithFormat:@"%@", quiz_data[@"date_open"] ];
                                                  NSString *description = [NSString stringWithFormat:@"%@", quiz_data[@"description"] ];
                                                  NSString *is_forced_complete = [NSString stringWithFormat:@"%@", quiz_data[@"is_forced_complete"] ];
                                                  NSString *is_shuffle_answers = [NSString stringWithFormat:@"%@", quiz_data[@"is_shuffle_answers"] ];
                                                  NSString *learning_skills_id = [NSString stringWithFormat:@"%@", quiz_data[@"learning_skills_id"] ];
                                                  NSString *learning_skills_name = [NSString stringWithFormat:@"%@", quiz_data[@"learning_skills_name"] ];
                                                  NSString *name = [NSString stringWithFormat:@"%@", quiz_data[@"name"] ];
                                                  NSString *passing_rate = [NSString stringWithFormat:@"%@", quiz_data[@"passing_rate"] ];
                                                  NSString *password = [NSString stringWithFormat:@"%@", quiz_data[@"password"] ];
                                                  NSString *proficiency_level_id = [NSString stringWithFormat:@"%@", quiz_data[@"proficiency_level_id"] ];
                                                  NSString *proficiency_level_name = [NSString stringWithFormat:@"%@", quiz_data[@"proficiency_level_name"] ];
                                                  NSString *question_type_name = [NSString stringWithFormat:@"%@", quiz_data[@"question_type_name"] ];
                                                  NSString *quiz_category_id = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_category_id"] ];
                                                  NSString *quiz_category_name = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_category_name"] ];
                                                  NSString *quiz_result_type_id = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_result_type_id"] ];
                                                  NSString *quiz_settings_id = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_settings_id"] ];
                                                  NSString *quiz_shuffling_mode = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_shuffling_mode"] ];
                                                  NSString *quiz_stage_id = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_stage_id"] ];
                                                  NSString *quiz_stage_name = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_stage_name"] ];
                                                  NSString *quiz_type_id = [NSString stringWithFormat:@"%@", quiz_data[@"quiz_type_id"] ];
                                                  NSString *show_feedbacks = [NSString stringWithFormat:@"%@", quiz_data[@"show_feedbacks"] ];
                                                  NSString *attempts_count = [NSString stringWithFormat:@"%@", r[@"attempts_count"] ];
                                                  
                                                  NSString *time_limit = [NSString stringWithFormat:@"%@", quiz_data[@"time_limit"] ];
                                                  if ([time_limit isEqualToString:@"(null)"]) {
                                                      time_limit = @"00:00:00";
                                                  }
                                                  NSString *time_limit_seconds = [NSString stringWithFormat:@"%f", [self parseTimeFromString:time_limit] ];
                                                  NSString *total_score = [NSString stringWithFormat:@"%@", quiz_data[@"total_score"] ];
                                                  NSString *total_items = [NSString stringWithFormat:@"%lu", (unsigned long)questions.count ];
                                                  
                                                  NSString *time_remaining = [NSString stringWithFormat:@"%@", r[@"time_remaining"] ];
                                                  if ([time_limit isEqualToString:@"(null)"]) {
                                                      time_remaining = [NSString stringWithFormat:@"%f", [self parseTimeFromString:time_limit]];
                                                  }
                                                  
                                                  NSInteger attempts_value = [attempts integerValue];
                                                  NSInteger attempts_count_value = [attempts_count integerValue];
                                                  
                                                  if (attempts_value == attempts_count_value) {
                                                      time_limit = @"00:00:00";
                                                  }
                                                  
                                                  NSManagedObject *mo = [self getEntity:kQuizDetailsEntity attribute:@"quiz_id"
                                                                              parameter:quizid context:ctx];
                                                  if (mo == nil) {
                                                      mo = [NSEntityDescription insertNewObjectForEntityForName:kQuizDetailsEntity inManagedObjectContext:ctx];
                                                  }
                                                  
                                                  [mo setValue:quizid forKey:@"quiz_id"];
                                                  [mo setValue:courseid forKey:@"course_id"];
                                                  
                                                  [mo setValue:allow_review forKey:@"allow_review"];
                                                  [mo setValue:attempts forKey:@"attempts"];
                                                  [mo setValue:date_close forKey:@"date_close"];
                                                  [mo setValue:date_open forKey:@"date_open"];
                                                  [mo setValue:description forKey:@"quiz_desc"];
                                                  [mo setValue:is_forced_complete forKey:@"is_forced_complete"];
                                                  [mo setValue:is_shuffle_answers forKey:@"is_shuffle_answers"];
                                                  [mo setValue:is_shuffle_answers forKey:@"is_shuffle_answers"];
                                                  [mo setValue:learning_skills_id forKey:@"learning_skills_id"];
                                                  [mo setValue:learning_skills_name forKey:@"learning_skills_name"];
                                                  [mo setValue:name forKey:@"name"];
                                                  [mo setValue:passing_rate forKey:@"passing_rate"];
                                                  [mo setValue:password forKey:@"password"];
                                                  [mo setValue:proficiency_level_id forKey:@"proficiency_level_id"];
                                                  [mo setValue:proficiency_level_name forKey:@"proficiency_level_name"];
                                                  [mo setValue:question_type_name forKey:@"question_type_name"];
                                                  [mo setValue:quiz_category_id forKey:@"quiz_category_id"];
                                                  [mo setValue:quiz_category_name forKey:@"quiz_category_name"];
                                                  [mo setValue:quiz_result_type_id forKey:@"quiz_result_type_id"];
                                                  [mo setValue:quiz_settings_id forKey:@"quiz_settings_id"];
                                                  [mo setValue:quiz_shuffling_mode forKey:@"quiz_shuffling_mode"];
                                                  [mo setValue:quiz_stage_id forKey:@"quiz_stage_id"];
                                                  [mo setValue:quiz_stage_name forKey:@"quiz_stage_name"];
                                                  [mo setValue:quiz_type_id forKey:@"quiz_type_id"];
                                                  [mo setValue:show_feedbacks forKey:@"show_feedbacks"];
                                                  [mo setValue:time_limit forKey:@"time_limit"];
                                                  [mo setValue:time_limit_seconds forKey:@"time_limit_seconds"];
                                                  [mo setValue:total_score forKey:@"total_score"];
                                                  [mo setValue:total_items forKey:@"total_items"];
                                                  
                                                  //VERY IMPORTANT
                                                  [mo setValue:attempts_count forKey:@"attempts_count"];
                                                  [mo setValue:time_remaining forKey:@"time_remaining"];
                                                  
                                                  for (int i = 0; i < [questions count]; i++) {
                                                      
                                                      NSDictionary *q = (NSDictionary *)questions[i];
                                                      NSString *deployment_id = [NSString stringWithFormat:@"%@", q[@"id"] ];
                                                      NSString *points = [NSString stringWithFormat:@"%@", q[@"points"] ];
                                                      NSString *question_id = [NSString stringWithFormat:@"%@", q[@"question_id"] ];
                                                      NSString *question_text = [NSString stringWithFormat:@"%@", q[@"question_text"] ];
                                                      NSString *question_type_id = [NSString stringWithFormat:@"%@", q[@"question_type_id"] ];
                                                      NSString *question_type_name = [NSString stringWithFormat:@"%@", q[@"question_type_name"] ];
                                                      NSString *quiz_id = [NSString stringWithFormat:@"%@", q[@"quiz_id"] ];
                                                      NSString *question_bullet_number = [NSString stringWithFormat:@"%d", i];
                                                      NSString *image_url = [NSString stringWithFormat:@"%@", q[@"image_url"] ];
                                                      NSLog(@"XXXX BULLET NUMBER %@", question_bullet_number);
                                                      
                                                      NSArray *question_choices = [self parseQuestionChoices: q[@"question_choices"] ];
                                                      
                                                      NSManagedObject *qmo = [self getEntity:kQuestionItemEntity attribute:@"question_id"
                                                                                   parameter:question_id context:ctx];
                                                      if (qmo == nil) {
                                                          qmo = [NSEntityDescription insertNewObjectForEntityForName:kQuestionItemEntity
                                                                                              inManagedObjectContext:ctx];
                                                      }
                                                      
                                                      [qmo setValue:deployment_id forKey:@"id"];
                                                      [qmo setValue:points forKey:@"points"];
                                                      [qmo setValue:question_id forKey:@"question_id"];
                                                      [qmo setValue:question_text forKey:@"question_text"];
                                                      [qmo setValue:question_type_id forKey:@"question_type_id"];
                                                      [qmo setValue:question_type_name forKey:@"question_type_name"];
                                                      [qmo setValue:quiz_id forKey:@"quiz_id"];
                                                      [qmo setValue:question_bullet_number forKey:@"question_bullet_number"];
                                                      [qmo setValue:image_url forKey:@"image_url"];
                                                      
                                                      for ( NSDictionary *a in question_choices ) {
                                                          
                                                          /*
                                                           "date_created" = "2015-03-06 15:59:35";
                                                           "date_modified" = "2015-03-06 15:59:35";
                                                           id = 531;
                                                           "is_correct" = 100;
                                                           "is_deleted" = 0;
                                                           "order_number" = 2;
                                                           "question_id" = 135;
                                                           "suggestive_feedback" = "<null>";
                                                           text = Moon;
                                                           */
                                                          
                                                          NSString *date_created = [NSString stringWithFormat:@"%@", a[@"date_created"] ];
                                                          NSString *date_modified = [NSString stringWithFormat:@"%@", a[@"date_modified"] ];
                                                          NSString *answer_id = [NSString stringWithFormat:@"%@", a[@"id"] ];
                                                          NSString *is_correct = [NSString stringWithFormat:@"%@", a[@"is_correct"] ];
                                                          NSString *is_deleted = [NSString stringWithFormat:@"%@", a[@"is_deleted"] ];
                                                          NSString *order_number = [NSString stringWithFormat:@"%@", a[@"order_number"] ];
                                                          NSString *question_id = [NSString stringWithFormat:@"%@", a[@"question_id"] ];
                                                          
                                                          NSString *suggestive_feedback = [NSString stringWithFormat:@"%@", a[@"suggestive_feedback"] ];
                                                          NSString *answer_text = [NSString stringWithFormat:@"%@", a[@"text"] ];
                                                          answer_text = [answer_text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                                          
                                                          NSPredicate *p1 = [self predicateForKeyPath:@"answer_text" andValue:answer_text];
                                                          NSPredicate *p2 = [self predicateForKeyPath:@"question_id" andValue:question_id];
                                                          NSCompoundPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[p1,p2]];
                                                          
                                                          NSManagedObject *amo = [self getEntity:kQuestionChoiceItemEntity predicate:predicate context:ctx];
                                                          
                                                          if (amo == nil) {
                                                              amo = [NSEntityDescription insertNewObjectForEntityForName:kQuestionChoiceItemEntity
                                                                                                  inManagedObjectContext:ctx];
                                                          }
                                                          
                                                          [amo setValue:date_created forKey:@"date_created"];
                                                          [amo setValue:date_modified forKey:@"date_modified"];
                                                          [amo setValue:answer_id forKey:@"id"];
                                                          [amo setValue:is_correct forKey:@"is_correct"];
                                                          [amo setValue:is_deleted forKey:@"is_deleted"];
                                                          [amo setValue:order_number forKey:@"order_number"];
                                                          [amo setValue:question_id forKey:@"question_id"];
                                                          [amo setValue:suggestive_feedback forKey:@"suggestive_feedback"];
                                                          [amo setValue:answer_text forKey:@"answer_text"];
                                                      }
                                                      
                                                      NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionChoiceItemEntity];
                                                      NSArray *items = [ctx executeFetchRequest:fetchRequest error:nil];
                                                      NSLog(@"total choices : %lu", (unsigned long)items.count);
                                                      
                                                  }
                                                  
                                                  [self saveTreeContext:ctx];
                                                  
                                                  if (doneBlock) {
                                                      
                                                      BOOL flag = NO;
                                                      
                                                      if (![status isEqualToString:@"ERROR"]) {
                                                          flag = YES;
                                                      }
                                                      
                                                      if (attempts_value == attempts_count_value) {
                                                          flag = NO;
                                                      }
                                                      
                                                      doneBlock(flag);
                                                  }
                                                  
                                              }];
                                          }
                                      }
                                  }];
    
    [task resume];
}

- (void)downloadImageFromQuizID:(NSDictionary *)data dataBlock:(ResourceManagerBinaryBlock)dataBlock {
    
    NSManagedObjectContext *ctx = _workerContext;
    
    NSManagedObject *object = [self getEntity:kQuestionItemEntity
                                    attribute:@"quiz_id"
                                    parameter:data[@"quiz_id"]
                                      context:ctx];
    if (object != nil) {
        
        //URL PATH CREATION
        NSString *image_url = data[@"image_url"];
        NSString *imageUrlPath = [Utils buildUrl:[NSString stringWithFormat:@"/%@", image_url]];
        
        NSURLSession *s = [NSURLSession sharedSession];
        NSURL *imageURL = [NSURL URLWithString:imageUrlPath];
        NSURLSessionDownloadTask *dt = [s downloadTaskWithURL:imageURL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            
            if (location) {
                
                NSData *imageData = [NSData dataWithContentsOfURL:location];
                if (imageData != nil) {
                    if (dataBlock) {
                        dataBlock(imageData);
                    }
                }
            }
        }];
        [dt resume];
    }
    
}

//- (void)submitQuizForUser:(NSString *)userid quiz:(NSString *)quizid course:(NSString *)courseid doneBlock:(ResourceManagerDoneBlock)doneBlock {
- (void)submitQuizForUser:(NSDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSString *userid = [NSString stringWithFormat:@"%@", data[@"userid"] ];
    NSString *quizid = [NSString stringWithFormat:@"%@", data[@"quizid"] ];
    NSString *courseid = [NSString stringWithFormat:@"%@", data[@"courseid"] ];
    NSTimeInterval time_consumed = [data[@"seconds"] doubleValue];
    
    //TIME FINISH
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *time_finish = [formatter stringFromDate:[NSDate date]];
    NSLog(@"submit date string %@", time_finish);
    
    //FINAL SCORE
    NSInteger final_score = 0;
    NSInteger summarize_points = 0;
    
    //ANSWERS
    NSMutableArray *answers = [NSMutableArray array];
    NSArray *list = [self fetchQuestionsForQuizID:quizid];
    for (NSDictionary *d in list) {
        
        
        /*
         NSDictionary *d = @{@"id":qid,
         @"points":points,
         @"question_id":question_id,
         @"question_text":question_text,
         @"question_type_id":question_type_id,
         @"question_type_name":question_type_name,
         @"question_bullet_number":question_bullet_number,
         @"question_answer_text":question_answer_text,
         @"score":score,
         @"quiz_id":quiz_id};
         */
        
        
        NSString *question_id = [NSString stringWithFormat:@"%@", d[@"question_id"] ];
        NSString *question_answer_text = [NSString stringWithFormat:@"%@", d[@"question_answer_text"] ];
        NSString *score = [NSString stringWithFormat:@"%@", d[@"score"] ];
        NSString *points = [NSString stringWithFormat:@"%@", d[@"points"] ];
        
        if (![score isEqualToString:@"(null)"]) {
            final_score = final_score + [score integerValue];
            summarize_points = summarize_points + [points integerValue];
            
            NSDictionary *a = @{@"question_id":question_id,
                                @"question_answer_text":question_answer_text,
                                @"score":score};
            [answers addObject:a];
        }
    }
    
    NSLog(@"answers object : %@", answers);
    
    NSManagedObjectContext *ctx = _workerContext;
    
    NSManagedObject *mo = [self getEntity:kQuizDetailsEntity attribute:@"quiz_id" parameter:quizid context:ctx];
    
    //TOTAL SCORE
    NSNumber *ts = [NSNumber numberWithInteger:final_score];
    NSString *total_score = [NSString stringWithFormat:@"%.0f", [ts doubleValue] ];
    [mo setValue:total_score forKey:@"total_score"];
    
    NSString *total_items = [NSString stringWithFormat:@"%lu", (unsigned long)list.count];
    [mo setValue:total_items forKey:@"total_items"];
    
    NSString *total_points_string = [NSString stringWithFormat:@"%lu", (long)summarize_points];
    [mo setValue:total_points_string forKey:@"total_points"];
    
    NSString *total_time_consumed = [self stringFromTimeInterval:time_consumed];
    [mo setValue:total_time_consumed forKey:@"total_time_consumed"];
    
    [self saveTreeContext:ctx];
    
    //POST BODY
    NSDictionary *postbody = @{@"final_score":@(final_score),
                               @"time_finish":time_finish,
                               @"answers":answers};
    
    NSLog(@"POST BODY : %@", postbody);
    
    NSString *submitQuizPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointSubmitQuiz, userid, quizid, courseid]];
    NSLog(@"post url : %@", submitQuizPath);
    NSURL *postURL = [NSURL URLWithString:submitQuizPath];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:postURL body:postbody];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              NSLog(@"SUBMIT DICTIONARY dictionary : %@", dictionary);
                                              //                  NSArray *records = dictionary[@"records "];
                                              //
                                              //                  if (records > 0) {
                                              //
                                              //                      NSManagedObjectContext *ctx = _workerContext;
                                              //
                                              //                      [ctx performBlock:^{
                                              //
                                              //                      }];
                                              //                  }
                                              
                                              if (doneBlock) {
                                                  doneBlock(YES);
                                              }
                                          }
                                      }
                                  }];
    
    [task resume];
}

- (NSDictionary *)fetchQuizDataForQuizID:(NSString *)quizid
{
    NSManagedObjectContext *ctx = _workerContext;
    
    NSManagedObject *mo = [self getEntity:kQuizDetailsEntity
                                attribute:@"quiz_id"
                                parameter:quizid
                                  context:ctx];
    
    NSMutableDictionary *quizData = [NSMutableDictionary dictionary];
    
    if (mo != nil) {
        NSArray *keys = mo.entity.propertiesByName.allKeys;
        for (NSString *k in keys) {
            NSString *v = [NSString stringWithFormat:@"%@", [mo valueForKey:k] ];
            quizData[k] = v;
        }
    }
    
    return quizData;
}

- (void)setAnswerObject:(NSDictionary *)answer doneBlock:(ResourceManagerDoneBlock)doneBlock
{
    NSString *question_id = [NSString stringWithFormat:@"%@", answer[@"question_id"] ];
    NSString *question_answer_text = [NSString stringWithFormat:@"%@", answer[@"question_answer_text"] ];
    NSLog(@"question_answer_text : %@", question_answer_text);
    NSString *is_correct = [NSString stringWithFormat:@"%@", answer[@"is_correct"] ];
    
    NSManagedObjectContext *ctx = _workerContext;
    
    //RETRIEVE POINTS
    NSManagedObject *qmo = [self getEntity:kQuestionItemEntity
                                 attribute:@"question_id"
                                 parameter:question_id
                                   context:ctx];
    
    NSString *question_points = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"points"] ];
    
    NSInteger is_correct_value = [is_correct integerValue];
    NSInteger points_value = [question_points integerValue];
    NSInteger score = (is_correct_value / 100) * points_value;
    NSString *score_string = [NSString stringWithFormat:@"%ld", (long)score];
    
    [qmo setValue:score_string forKey:@"score"];
    [qmo setValue:question_answer_text forKey:@"question_answer_text"];
    
    [self faultObjectWithID:qmo.objectID inContext:ctx];
    
    NSArray *keys = qmo.entity.propertiesByName.allKeys;
    
    for (NSString *k in keys) {
        NSString *v = [qmo valueForKey:k];
        NSLog(@"XXXXXX [%@] = %@", k, v);
    }
    
    if (doneBlock) {
        doneBlock(YES);
    }
}

- (NSArray *)fetchQuestionsForQuizID:(NSString *)quizid
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionItemEntity];
    
    NSPredicate *predicate = [self predicateForKeyPath:@"quiz_id" andValue:quizid];
    [fetchRequest setPredicate:predicate];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"question_bullet_number"
                                                           ascending:YES
                                                            selector:@selector(localizedCaseInsensitiveCompare:)];
    [fetchRequest setSortDescriptors:@[sort]];
    
    NSManagedObjectContext *ctx = _mainContext;
    
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
    if (error) {
        VLog(@"error: %@", [error localizedDescription]);
    }
    
    NSMutableArray *questions = [NSMutableArray array];
    
    if ([items count] > 0) {
        
        //        for (NSManagedObject *qmo in items) {
        for (NSUInteger i = 0; i < [items count]; i++) {
            
            NSManagedObject *qmo = (NSManagedObject *)items[i];
            NSString *qid = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"id"] ];
            NSString *points = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"points"] ];
            NSString *question_id = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"question_id"] ];
            NSString *question_text = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"question_text"] ];
            NSString *question_type_id = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"question_type_id"] ];
            NSString *question_type_name = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"question_type_name"] ];
            NSString *quiz_id = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"quiz_id"] ];
            NSString *question_bullet_number = [NSString stringWithFormat:@"%lu", (unsigned long)i ];
            
            NSString *question_answer_text = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"question_answer_text"] ];
            NSString *score = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"score"] ];
            
            NSString *image_url = [NSString stringWithFormat:@"%@", [qmo valueForKey:@"image_url"] ];
            
            NSDictionary *d = @{@"id":qid,
                                @"points":points,
                                @"question_id":question_id,
                                @"question_text":question_text,
                                @"question_type_id":question_type_id,
                                @"question_type_name":question_type_name,
                                @"question_bullet_number":question_bullet_number,
                                @"question_answer_text":question_answer_text,
                                @"score":score,
                                @"quiz_id":quiz_id,
                                @"image_url":image_url};
            
            [questions addObject:d];
        }
        
    }
    
    return questions;
}

- (NSArray *)fetchChoicesForQuestionID:(NSString *)questionid
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kQuestionChoiceItemEntity];
    
    NSPredicate *predicate = [self predicateForKeyPath:@"question_id" andValue:questionid];
    [fetchRequest setPredicate:predicate];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"order_number"
                                                           ascending:YES
                                                            selector:@selector(localizedCaseInsensitiveCompare:)];
    [fetchRequest setSortDescriptors:@[sort]];
    
    NSManagedObjectContext *ctx = _mainContext;
    
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:fetchRequest error:&error];
    if (error) {
        VLog(@"error: %@", [error localizedDescription]);
    }
    
    NSMutableSet *mSet = [NSMutableSet set];
    
    if ([items count] > 0) {
        
        for (NSManagedObject *cmo in items) {
            
            NSString *date_created = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"date_created"] ];
            NSString *date_modified = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"date_modified"] ];
            NSString *answer_id = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"id"] ];
            NSString *is_correct = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"is_correct"] ];
            NSString *is_deleted = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"is_deleted"] ];
            NSString *order_number = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"order_number"] ];
            NSString *question_id = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"question_id"] ];
            NSString *suggestive_feedback = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"suggestive_feedback"] ];
            NSString *answer_text = [NSString stringWithFormat:@"%@", [cmo valueForKey:@"answer_text"] ];
            
            NSDictionary *a = @{@"date_created":date_created,
                                @"date_modified":date_modified,
                                @"id":answer_id,
                                @"is_correct":is_correct,
                                @"is_deleted":is_deleted,
                                @"order_number":order_number,
                                @"question_id":question_id,
                                @"suggestive_feedback":suggestive_feedback,
                                @"answer_text":answer_text};
            
            [mSet addObject:a];
        }
    }
    
    return [mSet allObjects];
}

#pragma mark - ResourceManager Grade Book

//- (void)requestGradeListForUser:(NSString *)userid course:(NSString *)courseid doneBlock:(ResourceManagerDoneBlock)doneBlock {
//    
//    NSString *gradelistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointGradeList, userid, courseid]];
//    NSLog(@"grade path : %@", gradelistPath);
//    NSURL *gradelistURL = [NSURL URLWithString:gradelistPath];
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:gradelistURL body:nil];
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
//                                  {
//                                      if (error) {
//                                          NSLog(@"error %@", [error localizedDescription]);
//                                      }
//                                      
//                                      if (!error) {
//                                          
//                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
//                                          NSDictionary *meta = dictionary[@"_meta"];
//                                          NSString *metaCount = [self stringValue:meta[@"count"]];
//                                          NSString *metaStatus = meta[@"status"];
//                                          
//                                          NSManagedObjectContext *ctx = self.workerContext;
//                                          
//                                          if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
//                                              [ctx performBlock:^{
//                                                  [self clearDataForEntity:kGradeBookEntity withPredicate:nil context:ctx];
//                                              }];
//                                              
//                                              if (doneBlock) {
//                                                  doneBlock(NO);
//                                              }
//                                              
//                                              return;
//                                          }
//                                          
//                                          
//                                          
//                                          if (dictionary) {
//                                              
//                                              //                  NSLog(@"dictionary : %@", dictionary);
//                                              NSArray *records = dictionary[@"records"];
//                                              
//                                              if (records > 0) {
//                                                  
//                                                  NSManagedObjectContext *ctx = _workerContext;
//                                                  
//                                                  [ctx performBlock:^{
//                                                      
//                                                      //CLEAR CONTENTS (IMPORTANT)
//                                                      [self clearDataForEntity:kGradeBookEntity withPredicate:nil context:ctx];
//                                                      
//                                                      //REFRESH DATA
//                                                      for (NSDictionary *d in records) {
//                                                          
//                                                          NSLog(@"GRADE BOOK : %@", d);
//                                                          /*
//                                                           {
//                                                           "activity_name" = "Science Final Exam";
//                                                           "activity_type_id" = 8;
//                                                           class = "Science 6";
//                                                           "date_performed" = "Nov 28 2014 12:51 PM";
//                                                           remarks = "Quiz taken: 2014-11-28 12:52:33";
//                                                           score = "2.00";
//                                                           }
//                                                           */
//                                                          
//                                                          NSString *activity_name = [NSString stringWithFormat:@"%@", d[@"activity_name"] ];
//                                                          NSString *activity_type_id = [NSString stringWithFormat:@"%@", d[@"activity_type_id"] ];
//                                                          NSString *class = [NSString stringWithFormat:@"%@", d[@"class"] ];
//                                                          NSString *date_performed = [NSString stringWithFormat:@"%@", d[@"date_performed"] ];
//                                                          NSString *remarks = [NSString stringWithFormat:@"%@", d[@"remarks"] ];
//                                                          NSString *score = [NSString stringWithFormat:@"%@", d[@"score"] ];
//                                                          NSString *search_string = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", activity_name, class, date_performed, remarks, score];
//                                                          
//                                                          NSManagedObject *mo = [self getEntity:kGradeBookEntity attribute:@"date_performed" parameter:date_performed context:ctx];
//                                                          
//                                                          if (mo == nil) {
//                                                              mo = [NSEntityDescription insertNewObjectForEntityForName:kGradeBookEntity inManagedObjectContext:ctx];
//                                                          }
//                                                          
//                                                          [mo setValue:activity_name forKey:@"activity_name"];
//                                                          [mo setValue:activity_type_id forKey:@"activity_type_id"];
//                                                          [mo setValue:class forKey:@"class_name"];
//                                                          [mo setValue:date_performed forKey:@"date_performed"];
//                                                          [mo setValue:remarks forKey:@"remarks"];
//                                                          [mo setValue:score forKey:@"score"];
//                                                          [mo setValue:search_string forKey:@"search_string"];
//                                                          
//                                                          //                              NSLog(@"search string : %@", search_string);
//                                                      }
//                                                      
//                                                      [self saveTreeContext:ctx];
//                                                      
//                                                  }];
//                                                  
//                                                  
//                                                  if (doneBlock) {
//                                                      doneBlock(YES);
//                                                  }
//                                              }
//                                              
//                                          }
//                                          
//                                      }
//                                      
//                                  }];
//    
//    [task resume];
//}

#pragma mark - ResourceManager Assessment

- (void)submitAssessmentForUser:(NSString *)userid withData:(NSDictionary *)data doneBlock:(ResourceManagerDoneBlock)doneBlock
{
    NSString *postAssessmentPath = [Utils buildUrl:kEndPointPostGradeAssessment];
    NSLog(@"post url : %@", postAssessmentPath);
    NSURL *postURL = [NSURL URLWithString:postAssessmentPath];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:postURL body:data];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              NSLog(@"dictionary : %@", dictionary);
                                              
                                              if (doneBlock) {
                                                  doneBlock(YES);
                                              }
                                              
                                          }
                                      }//end
                                  }];
    
    [task resume];
    
}

#pragma mark - ResourceManager Test Guru
- (void)requestQuestionCountForUser:(NSString *)userid dataBlock:(ResourceManagerDataBlock)dataBlock {
    
    NSString *path = [Utils buildUrl:[NSString stringWithFormat:kEndPointTestGuruQuestionCount, userid]];
    NSLog(@"path : %@", path);
    NSURL *url = [NSURL URLWithString:path];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              if (dictionary[@"records"] != nil ) {
                                                  if (dataBlock) {
                                                      dataBlock( dictionary[@"records"] );
                                                  }
                                              }
                                          }
                                      }//end
                                  }];
    
    [task resume];
}

- (void)requestTestCountForUser:(NSString *)userid dataBlock:(ResourceManagerDataBlock)dataBlock {
    
    NSString *path = [Utils buildUrl:[NSString stringWithFormat:kEndPointTestGuruTestCount, userid]];
    NSLog(@"path : %@", path);
    NSURL *url = [NSURL URLWithString:path];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              if (dictionary[@"records"] != nil ) {
                                                  if (dataBlock) {
                                                      dataBlock( dictionary[@"records"] );
                                                  }
                                              }
                                          }
                                      }//end
                                  }];
    
    [task resume];
}

- (void)requestDeployedTestCountForUser:(NSString *)userid dataBlock:(ResourceManagerDataBlock)dataBlock {
    
    NSString *path = [Utils buildUrl:[NSString stringWithFormat:kEndPointTestGuruDeployedTestCount, userid]];
    NSLog(@"path : %@", path);
    NSURL *url = [NSURL URLWithString:path];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              if (dictionary[@"records"] != nil ) {
                                                  if (dataBlock) {
                                                      dataBlock( dictionary[@"records"] );
                                                  }
                                              }
                                          }
                                          
                                      }
                                      
                                  }];
    
    [task resume];
}

- (void)requestQuestionListForUser:(NSString *)userid doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSString *path = [Utils buildUrl:[NSString stringWithFormat:kEndPointTestGuruQuestionList, userid]];
    NSLog(@"path : %@", path);
    NSURL *url = [NSURL URLWithString:path];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              NSLog(@"question list : %@", dictionary);
                                          }
                                      }
                                  }];
    
    [task resume];
}

- (void)downloadImageTestPreview:(NSString *)image_url_string dataBlock:(ResourceManagerBinaryBlock)dataBlock {
    //URL PATH CREATION
    NSString *image_url = image_url_string;
    NSString *imageUrlPath = [Utils buildUrl:[NSString stringWithFormat:@"/%@", image_url]];
    
    NSURLSession *s = [NSURLSession sharedSession];
    NSURL *imageURL = [NSURL URLWithString:imageUrlPath];
    NSURLSessionDownloadTask *dt = [s downloadTaskWithURL:imageURL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        
        if (location) {
            
            NSData *imageData = [NSData dataWithContentsOfURL:location];
            if (imageData != nil) {
                if (dataBlock) {
                    dataBlock(imageData);
                }
            }
        }
    }];
    [dt resume];
}

#pragma mark - ResourceManager Playlist

- (NSString *)stringValue:(id)object {
    
    NSString *value = [NSString stringWithFormat:@"%@", object];
    
    if ([value isEqualToString:@"(null)"] || [value isEqualToString:@"<null>"] ) {
        return @"";
    }
    
    return value;
}

//- (void)requestDeletePlayListItemWithUploadID:(NSString *)upload_id doneBlock:(ResourceManagerDoneBlock)doneBlock {
                                              //
//    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointPlayListDelete, upload_id]];
//    
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:nil];
//    
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
//                                            completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
//                                  {
//                                      if (error) {
//                                          NSLog(@"error %@", [error localizedDescription]);
//                                      }
//                                      
//                                      if (!error) {
//                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
//                                          if (dictionary) {
//                                              
//                                              NSManagedObjectContext *ctx = _workerContext;
//                                              NSLog(@"DELETION RESPONSE : %@", dictionary);
//                                              
//                                              NSPredicate *predicate = [NSPredicate predicateWithFormat:@"upload_id == %@", upload_id];
//                                              
//                                              [self clearDataForEntity:kPlayListEntity withPredicate:predicate context:ctx];
//                                              //
//                                          }
//                                          
//                                          if (doneBlock) {
//                                              doneBlock(YES);
//                                          }
//                                      }//end
//                                  }];
//    
//    [task resume];
//    
//}
                                                          
//- (void)requestPlayListForUser:(NSString *)userid doneBlock:(ResourceManagerDoneBlock)doneBlock {
//    
//    NSString *playlistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointPlayList, userid]];
//    NSLog(@"play list path : %@", playlistPath);
//    NSURL *playlistURL = [NSURL URLWithString:playlistPath];
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:playlistURL body:nil];
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
//                                  {
//                                      if (error) {
//                                          NSLog(@"error %@", [error localizedDescription]);
//                                      }
//                                      
//                                      if (!error) {
//                                          
//                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
//                                          NSDictionary *meta = dictionary[@"_meta"];
//                                          NSString *metaCount = [self stringValue:meta[@"count"]];
//                                          NSString *metaStatus = meta[@"status"];
//                                          
//                                          NSManagedObjectContext *ctx = self.workerContext;
//                                          
//                                          if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
//                                              [ctx performBlock:^{
//                                                  [self clearDataForEntity:kPlayListEntity withPredicate:nil context:ctx];
//                                              }];
//                                              
//                                              if (doneBlock) {
//                                                  doneBlock(NO);
//                                              }
//                                              
//                                              return;
//                                          }
//                                          
//                                          if (dictionary) {
//                                              
//                                              NSArray *records = dictionary[@"records"];
//                                              
//                                              if (records > 0) {
//                                                  
//                                                  NSManagedObjectContext *ctx = _workerContext;
//                                                  NSString *username = [NSString stringWithFormat:@"%@", [VSmart sharedInstance].account.user.email];
//                                                  
//                                                  if ([username isEqualToString:@"(null)"] || [username isEqualToString:@"<null>"] || [username isEqualToString:@"null"]) {
//                                                      username = [NSString stringWithFormat:@"%@", [VSmart sharedInstance].account.user.email];
//                                                  }
//                                                  
//                                                  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"owner_username == %@ AND is_downloaded == 0", username];
//                                                  
//                                                  [ctx performBlockAndWait:^{
//                                                      
//                                                      //CLEAR CONTENTS (IMPORTANT)
//                                                      [self clearDataForEntity:kPlayListEntity withPredicate:predicate context:ctx];
//                                                      
//                                                      //REFRESH DATA
//                                                      for (NSDictionary *d in records) {
//                                                          
//                                                          NSLog(@"play list item : %@", d);
//                                                          
//                                                          /*
//                                                           "date_created" = "2015-03-11 05:22:54";
//                                                           description = "my upload 1";
//                                                           "file_name" = "ios-android-windows-phone.png";
//                                                           "file_size" = 25489;
//                                                           "first_name" = Alexis;
//                                                           "last_name" = Johnson;
//                                                           "module_id" = 1;
//                                                           title = "my upload title 1";
//                                                           "upload_id" = 53;
//                                                           "user_id" = 3;
//                                                           uuid = 20132ec92d6e415a92b7d1610f8632ec;
//                                                           */
//                                                          
//                                                          // BUG FIX 595
                                                          //                                                          NSString *shared = @"0";
//                                                          NSMutableArray *section_names = [NSMutableArray array];
//                                                          NSMutableArray *sharedToArray = [NSMutableArray array];
//                                                          
//                                                          if (d[@"cs_ids"]) {
//                                                              NSArray *cs_ids = d[@"cs_ids"];
//                                                              sharedToArray = [NSMutableArray arrayWithArray:cs_ids];
//                                                              if (cs_ids.count > 0) {
                                                          //                                                              shared = @"1";
//                                                                  
//                                                                  for (NSDictionary *section in cs_ids) {
//                                                                      NSString *sectionName = [section valueForKey:@"name"];
////                                                                      NSString *sectionSubject = [section valueForKey:@"name"];
////                                                                      NSString *section_name_subject = [NSString stringWithFormat:@"%@- %@", sectionName, sectionSubject];
//                                                                      NSString *section_name_subject = [NSString stringWithFormat:@"%@", sectionName];
//                                                                      
//                                                                      [section_names addObject:section_name_subject];
                                                          //                                                          }
//                                                                  
//                                                              } else {
//                                                                  shared = @"0";
//                                                              }
//                                                              NSLog(@"Exists");
//                                                          }
//                                                          else {
//                                                              NSLog(@"Does not exist");
//                                                          }
//                                                          
//                                                          NSString *section_names_string = [section_names componentsJoinedByString: @", "];
//                                                          
//                                                          NSData *sharedArrayData = [NSKeyedArchiver archivedDataWithRootObject:sharedToArray];
//                                                          
//                                                          NSLog(@"SECTION NAMES STRING -> %@ <- ", section_names_string);
//                                                          
//                                                          NSString *dateCreated = [NSString stringWithFormat:@"%@", d[@"date_created"] ];
//                                                          NSDate *date_created = [self parseDateFromString:dateCreated];
//                                                          
//                                                          //                                                          NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//                                                          //                                                          NSString *template = @"yyyy-MM-dd HH:mm:ss";
//                                                          //                                                          [formatter setDateFormat:template];
//                                                          //                                                          return [formatter dateFromString:string];
//                          NSString *sharedToCount = [NSString stringWithFormat:@"%lu", (unsigned long)sharedToArray.count];
//                                                          NSString *description = [NSString stringWithFormat:@"%@", d[@"description"] ]; //SEARCH
//                                                          NSString *file_name = [NSString stringWithFormat:@"%@", d[@"file_name"] ]; //SEARCH
//                                                          NSString *mime_type = @"image/jpeg";
//                                                          if (![file_name isEqualToString:@"(null)"]) {
//                                                              mime_type = [NSString stringWithFormat:@"%@", [self mimeTypeForPath:file_name]];
//                                                          }
//                                                          
//                                                          NSString *file_extension = [NSString stringWithFormat:@"%@", [file_name pathExtension] ];
//                                                          file_extension = [NSString stringWithFormat:@"%@", [file_extension lowercaseString] ];
//                                                          
//                                                          
//                                                          NSString *file_type = [self categoryForFileExtension:file_extension];
//                                                          
//                                                          NSString *file_size = [NSString stringWithFormat:@"%@", d[@"file_size"] ];
//                                                          if ([file_size isEqualToString:@"(null)"]) {
//                                                              file_size = @"0";
//                                                          }
//                                                          
//                                                          NSString *first_name = [NSString stringWithFormat:@"%@", d[@"first_name"] ]; //SEARCH
//                                                          NSString *last_name = [NSString stringWithFormat:@"%@", d[@"last_name"] ]; //SEARCH
//                                                          NSString *module_id = [NSString stringWithFormat:@"%@", d[@"module_id"] ];
//                                                          NSString *title = [NSString stringWithFormat:@"%@", d[@"title"] ]; //SEARCH
//                                                          NSString *upload_id = [NSString stringWithFormat:@"%@", d[@"upload_id"] ];
//                                                          NSString *user_id = [NSString stringWithFormat:@"%@", d[@"user_id"] ];
//                                                          NSString *tags = [NSString stringWithFormat:@"%@", d[@"tags"] ];
//                                                          
//                                                          //                                                          NSString *shared = @"0";
//                                                          //                                                          if ([userid isEqualToString:user_id]) {
//                                                          //                                                              shared = @"1";
//                                                          //                                                          }
//                                                          
//                                                          NSString *uuid = [NSString stringWithFormat:@"%@", d[@"uuid"] ];
//                                                          NSString *search_string = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@",
//                                                                                     description, file_name, first_name, last_name, title, file_extension, file_type];
//                                                          
//                                                          NSManagedObject *mo = [self getEntity:kPlayListEntity attribute:@"uuid" parameter:uuid context:ctx];
//                                                          if (mo == nil) {
//                                                              mo = [NSEntityDescription insertNewObjectForEntityForName:kPlayListEntity inManagedObjectContext:ctx];
//                                                          }
//                                                          
//                                                          NSString *progress = [NSString stringWithFormat:@"%@", [mo valueForKey:@"progress"] ];
//                                                          if ([progress isEqualToString:@"(null)"]) {
//                                                              progress = @"";
//                                                          }
//                                                          
//                                                          [mo setValue:date_created forKey:@"date_created"];
//                                                          [mo setValue:description forKey:@"file_desc"];
//                                                          [mo setValue:file_name forKey:@"file_name"];
//                                                          [mo setValue:file_size forKey:@"file_size"];
//                                                          [mo setValue:first_name forKey:@"first_name"];
//                                                          [mo setValue:file_extension forKey:@"file_extension"];
//                                                          [mo setValue:file_type forKey:@"file_type"];
//                                                          [mo setValue:mime_type forKey:@"mime_type"];
//                                                          [mo setValue:last_name forKey:@"last_name"];
//                                                          [mo setValue:module_id forKey:@"module_id"];
//                                                          [mo setValue:title forKey:@"title"];
//                                                          [mo setValue:upload_id forKey:@"upload_id"];
//                                                          [mo setValue:user_id forKey:@"user_id"];
//                                                          [mo setValue:uuid forKey:@"uuid"];
//                                                          [mo setValue:search_string forKey:@"search_string"];
//                                                          [mo setValue:shared forKey:@"shared"];
//                                                          [mo setValue:progress forKey:@"progress"];
//                                                          [mo setValue:section_names_string forKey:@"section_names"];
//                                                          [mo setValue:sharedArrayData forKey:@"sharedArrayData"];
//                                                          [mo setValue:tags forKey:@"tags"];
//                          [mo setValue:sharedToCount forKey:@"sharedToCount"];
//                                                          
//                                                          if (![[mo valueForKey:@"is_downloaded"] isEqualToString:@"1"]) {
//                                                              [mo setValue:@"0" forKey:@"is_downloaded"];
//                                                          }
//                                                          
//                                                          // for checking purposes
//                                                          [mo setValue:username forKey:@"owner_username"];
//                                                          
//                                                      }
//                                                      
//                                                      [self saveTreeContext:ctx];
//                                                      
//                                                  }];
//                                                  
//                                                  
//                                                  if (doneBlock) {
//                                                      doneBlock(YES);
//                                                  }
//                                              }
//                                              
//                                          }
//                                          
//                                      }
//                                      
//                                  }];
//    [task resume];
//}
//
//- (void)requestUpdatePlayListItemWithMeta:(NSDictionary *)meta doneBlock:(ResourceManagerDoneBlock)doneBlock {
//    
//    NSString *upload_id = [NSString stringWithFormat:@"%@", meta[@"upload_id"] ];
//    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointPlayListUpdateMeta, upload_id]];
//    
//    NSString *module_id = [NSString stringWithFormat:@"%@", meta[@"module_id"] ];
//    NSString *title = [NSString stringWithFormat:@"%@", meta[@"title"] ];
//    NSString *description = [NSString stringWithFormat:@"%@", meta[@"description"] ];
//    NSDictionary *postBody = @{@"module_id":module_id,@"title":title,@"description":description};
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
//    
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
//                                            completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
//                                  {
//                                      if (error) {
//                                          NSLog(@"error %@", [error localizedDescription]);
//                                      }
//                                      
//                                      if (!error) {
//                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
//                                          if (dictionary) {
//                                              NSLog(@"updated data : %@", dictionary);
//                                          }
//                                          
//                                          if (doneBlock) {
//                                              doneBlock(YES);
//                                          }
//                                          
//                                      }//end
//                                      
//                                  }];
//    
//    [task resume];
//    
//}
//
//- (void)postUpdateOfUploadID:(NSString *)upload_id withPostBody:(NSDictionary *)postBody doneBlock:(ResourceManagerDoneBlock)doneBlock {
//    
////    NSString *upload_id = [NSString stringWithFormat:@"%@", meta[@"upload_id"] ];
//    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointPlayListUpdateMeta, upload_id]];
//    
//    
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:postBody];
//    
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
//                                            completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
//                                  {
//                                      if (error) {
//                                          NSLog(@"error %@", [error localizedDescription]);
//                                      }
//                                      
//                                      if (!error) {
//                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
//                                          if (dictionary) {
//                                              NSLog(@"updated data : %@", dictionary);
//                                          }
//                                          
//                                          if (doneBlock) {
//                                              doneBlock(YES);
//                                          }
//                                          
//                                      }//end
//                                      
//                                  }];
//    
//    [task resume];
//}

- (void)requestUpdatePlayListItemForUploadID:(NSString *)uploadid meta:(NSDictionary *)meta file:(NSDictionary *)object doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSString *query = [Utils buildUrl:[NSString stringWithFormat:kEndPointPlayListUpload, uploadid]];
    NSLog(@"path : %@", query);
    NSLog(@"meta : %@", meta);
    //    NSLog(@"file : %@", object);
    
    // configure the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:query]];
    [request setHTTPMethod:@"POST"];
    
    // boundary
    NSString *boundary = [self generateBoundaryString];
    
    // set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create body
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:meta file:object];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    [config setAllowsCellularAccess:YES];//USE CELLULAR DATA
    NSURLSession *uploadsession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    NSURLSessionUploadTask *uploadTask = [uploadsession uploadTaskWithRequest:request fromData:httpBody];
    [uploadTask resume];//START upload
    
    self.doneProgressBlock = ^(NSString *status) {
        NSLog(@"TASKS COMPLETE...");
        if ([status isEqualToString:@"TRUE"]) {
            if (doneBlock) {
                doneBlock(YES);
            }
        }
    };
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters
                              file:(NSDictionary *)fileObject
{
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    if (fileObject) {
        
        NSString *fieldname = @"file";
        NSString *filename = [NSString stringWithFormat:@"%@", fileObject[@"filename"] ];
        NSData *data = [NSData dataWithData: fileObject[@"filedata"] ];
        NSString *mimetype  = [NSString stringWithFormat:@"%@", fileObject[@"mimetype"] ];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldname, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

- (NSString *)mimeTypeForPath:(NSString *)path
{
    // get a mime type for an extension using MobileCoreServices.framework
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    //    CFStringRef extension = (__bridge CFStringRef)path;
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mime_type_object = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    NSString *mimetype = [NSString stringWithFormat:@"%@", mime_type_object];
    if ([mimetype isEqualToString:@"(null)"]) {
        mimetype = @"text/html";
    }
    
    assert(mimetype != NULL);
    
    return mimetype;
}

- (NSString *)generateBoundaryString
{
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
    
    // if supporting iOS versions prior to 6.0, you do something like:
    //
    // // generate boundary string
    // //
    // adapted from http://developer.apple.com/library/ios/#samplecode/SimpleURLConnections
    //
    // CFUUIDRef  uuid;
    // NSString  *uuidStr;
    //
    // uuid = CFUUIDCreate(NULL);
    // assert(uuid != NULL);
    //
    // uuidStr = CFBridgingRelease(CFUUIDCreateString(NULL, uuid));
    // assert(uuidStr != NULL);
    //
    // CFRelease(uuid);
    //
    // return uuidStr;
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
   didSendBodyData:(int64_t)bytesSent
    totalBytesSent:(int64_t)totalBytesSent
totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend {
    
    CGFloat progress = (double)totalBytesSent / (double)totalBytesExpectedToSend;
    NSLog(@"upload progress : %f", progress);
    
    if (self.progressBlock) {
        self.progressBlock(progress);
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    
    if (self.doneProgressBlock) {
        self.doneProgressBlock(@"TRUE");
    }
}

//- (void)updateProgressForUUID:(NSString *)uuid progress:(NSString *)progress {
//    
//    NSManagedObjectContext *ctx = _workerContext;
//    [ctx performBlock:^{
//        NSManagedObject *mo = [self getEntity:kPlayListEntity attribute:@"uuid" parameter:uuid context:ctx];
//        [mo setValue:progress forKey:@"progress"];
//        [self saveTreeContext:ctx];
//    }];
//}
//
//- (void)updatePlayListFileForUUID:(NSString *)uuid rawFile:(NSData *)rawFile {
//    
//    NSManagedObjectContext *ctx = _workerContext;
//    [ctx performBlock:^{
//        
//        NSManagedObject *mo = [self getEntity:kPlayListEntity attribute:@"uuid" parameter:uuid context:ctx];
//        
//        NSString *file_desc = [NSString stringWithFormat:@"%@", [mo valueForKey:@"file_desc"] ];
//        NSString *file_extension = [NSString stringWithFormat:@"%@", [mo valueForKey:@"file_extension"] ];
//        NSString *file_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"file_name"] ];
//        NSString *file_size = [NSString stringWithFormat:@"%@", [mo valueForKey:@"file_size"] ];
//        NSString *file_type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"file_type"] ];
//        NSString *first_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"first_name"] ];
//        NSString *last_name = [NSString stringWithFormat:@"%@", [mo valueForKey:@"last_name"] ];
//        NSString *title = [NSString stringWithFormat:@"%@", [mo valueForKey:@"title"] ];
//        NSString *shared = [NSString stringWithFormat:@"%@", [mo valueForKey:@"shared"] ];
//        NSString *mime_type = [NSString stringWithFormat:@"%@", [mo valueForKey:@"mime_type"] ];
//        NSString *upload_id = [NSString stringWithFormat:@"%@", [mo valueForKey:@"upload_id"] ];
//        [mo setValue:uuid forKey:@"uuid"];
//        
//        [mo setValue:@"1" forKey:@"is_downloaded"];
//        
//        
//        NSManagedObject *filemo = [self getEntity:kPlayListFileEntity attribute:@"uuid" parameter:uuid context:ctx];
//        if (filemo == nil) {
//            filemo = [NSEntityDescription insertNewObjectForEntityForName:kPlayListFileEntity inManagedObjectContext:ctx];
//        }
//        
//        [filemo setValue:uuid forKey:@"uuid"];
//        [filemo setValue:file_desc forKey:@"file_desc"];
//        [filemo setValue:file_extension forKey:@"file_extension"];
//        [filemo setValue:file_name forKey:@"file_name"];
//        [filemo setValue:file_size forKey:@"file_size"];
//        [filemo setValue:file_type forKey:@"file_type"];
//        [filemo setValue:mime_type forKey:@"mime_type"];
//        [filemo setValue:first_name forKey:@"first_name"];
//        [filemo setValue:last_name forKey:@"last_name"];
//        [filemo setValue:title forKey:@"title"];
//        [filemo setValue:shared forKey:@"shared"];
//        
//        NSLog(@"FILE TYPE !!!![%@]", file_type);
//        NSLog(@"MIME TYPE !!!![%@]", mime_type);
//        NSLog(@"FILE EXTENSION!!!! [%@]", file_extension);
//        NSLog(@"FILE NAME!!!! [%@]", file_name);
//        
//        // SAVE TO APPLICATION SUPPORT
//        if ([self isMedia:mime_type]) {
//            AccountInfo *account = [Utils getArchive:kGlobalAccount];
//            
//            NSString *playlistPath = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//            NSString *path = [NSString stringWithFormat:@"%@/PLAYLIST/%@",playlistPath, account.user.email];
//            
//            if (![[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NULL]) {
//                NSError *error = nil;
//                //Create one
//                if (![[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error]) {
//                    NSLog(@"%@", error.localizedDescription);
//                }
//            }
//            
//            NSLog(@"HERE IS THE PATH [%@]", path);
//            
//            NSString *filepath = [NSString stringWithFormat:@"%@/%@%@", path, upload_id, file_name];
//            
//            //saving is done on main thread
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [rawFile writeToFile:filepath atomically:YES];
//                NSLog(@"File Saved !");
//                NSLog(@"HERE IS THE FILE PATH [%@]",filepath);
//                NSData* data = [filepath dataUsingEncoding:NSUTF8StringEncoding];
//                [filemo setValue:data forKey:@"raw_file"];
//            });
//        } else {
//            [filemo setValue:rawFile forKey:@"raw_file"];
//        }
//        
//        [self saveTreeContext:ctx];
//    }];
//}

-(BOOL)isMedia:(NSString *)mimeType {
    
    BOOL isMedia;
    
    if ([mimeType rangeOfString:@"video"].location == NSNotFound) {
        isMedia = NO;
    } else {
        return YES;
    }
    
    if ([mimeType rangeOfString:@"audio"].location == NSNotFound) {
        isMedia = NO;
    } else {
        return  YES;
    }
    
    return isMedia;
}

/////// PEOPLESHORTCUT //////

- (void)requestUserProfileWithID:(NSString *)usedid dataBlock:(ResourceManagerDataBlock)dataBlock {
    
    NSString *useprofile = [NSString stringWithFormat:kEndPointUserProfile, usedid ];
    NSString *url = [Utils buildUrl:useprofile];
    NSLog(@"user profile path : %@", url);
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:[NSURL URLWithString:url] body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              
                                              NSArray *records = dictionary[@"records"];
                                              
                                              if (records > 0) {
                                                  
                                                  
                                                  NSMutableDictionary *profile_data = [NSMutableDictionary dictionary];
                                                  NSManagedObjectContext *ctx = _workerContext;
                                                  
                                                  [ctx performBlock:^{
                                                      
                                                      for (NSDictionary *d in records) {
                                                          
                                                          NSArray *keys = d.allKeys;
                                                          
                                                          for (NSString *k in keys) {
                                                              
                                                              ///////////////////////////////////////////////////////////////////////
                                                              
                                                              if ([k isEqualToString:@"Profile"]) {
                                                                  
                                                                  //PROFILE
                                                                  NSArray *profile_items = d[k];
                                                                  if (profile_items != nil) {
                                                                      NSDictionary *p = (NSDictionary *)[profile_items lastObject];
                                                                      //                                      NSLog(@"profile : %@", p);
                                                                      
                                                                      NSString *first_name = [NSString stringWithFormat:@"%@", p[@"first_name"] ];
                                                                      first_name = [self emptyString:first_name];
                                                                      
                                                                      NSString *last_name = [NSString stringWithFormat:@"%@", p[@"last_name"] ];
                                                                      last_name = [self emptyString:last_name];
                                                                      
                                                                      NSString *info_percentage = [NSString stringWithFormat:@"%@", p[@"info_percentage"] ];
                                                                      
                                                                      
                                                                      [profile_data setValue:first_name forKey:@"first_name"];
                                                                      [profile_data setValue:last_name forKey:@"last_name"];
                                                                      [profile_data setValue:info_percentage forKey:@"info_percentage"];
                                                                      
                                                                  }//PROFILE
                                                              }
                                                              
                                                              ///////////////////////////////////////////////////////////////////////
                                                              
                                                              if ([k isEqualToString:@"Statistics"]) {
                                                                  //STATISTICS
                                                                  
                                                                  NSString *statistics = [NSString stringWithFormat:@"%@", d[k] ];
                                                                  
                                                                  NSString *count_posted = @"0";
                                                                  NSString *count_liked = @"0";
                                                                  NSString *count_received_likes = @"0";
                                                                  NSString *count_replied = @"0";
                                                                  
                                                                  if (![statistics isEqualToString:@"0"]) {
                                                                      
                                                                      NSDictionary *statistics = d[k];
                                                                      
                                                                      count_posted = [NSString stringWithFormat:@"%@", statistics[@"count_posted"] ];
                                                                      count_posted = [self emptyString:count_posted];
                                                                      
                                                                      count_liked = [NSString stringWithFormat:@"%@", statistics[@"count_liked"] ];
                                                                      count_liked = [self emptyString:count_liked];
                                                                      
                                                                      count_received_likes = [NSString stringWithFormat:@"%@", statistics[@"count_received_likes"] ];
                                                                      count_received_likes = [self emptyString:count_received_likes];
                                                                      
                                                                      count_replied = [NSString stringWithFormat:@"%@", statistics[@"count_replied"] ];
                                                                      count_replied = [self emptyString:count_replied];
                                                                  }
                                                                  
                                                                  [profile_data setValue:count_posted forKey:@"count_posted"];
                                                                  [profile_data setValue:count_liked forKey:@"count_liked"];
                                                                  [profile_data setValue:count_received_likes forKey:@"count_received_likes"];
                                                                  [profile_data setValue:count_replied forKey:@"count_replied"];
                                                                  
                                                              }
                                                              
                                                              ///////////////////////////////////////////////////////////////////////
                                                          }
                                                      }
                                                      
                                                      NSManagedObject *mo = [self getEntity:kShortcutPeopleEntity
                                                                                  attribute:@"user_id"
                                                                                  parameter:usedid
                                                                                    context:ctx];
                                                      if (mo == nil) {
                                                          mo = [NSEntityDescription insertNewObjectForEntityForName:kShortcutPeopleEntity
                                                                                             inManagedObjectContext:ctx];
                                                      }
                                                      
                                                      
                                                      NSString *info_percentage = [NSString stringWithFormat:@"%@", profile_data[@"info_percentage"] ];
                                                      NSString *count_posted = [NSString stringWithFormat:@"%@", profile_data[@"count_posted"] ];
                                                      NSString *count_liked = [NSString stringWithFormat:@"%@", profile_data[@"count_liked"] ];
                                                      NSString *count_received_likes = [NSString stringWithFormat:@"%@", profile_data[@"count_received_likes"] ];
                                                      NSString *count_replied = [NSString stringWithFormat:@"%@", profile_data[@"count_replied"] ];
                                                      
                                                      [mo setValue:info_percentage forKey:@"info_percentage"];
                                                      [mo setValue:count_posted forKey:@"count_posted"];
                                                      [mo setValue:count_liked forKey:@"count_liked"];
                                                      [mo setValue:count_received_likes forKey:@"count_received_likes"];
                                                      [mo setValue:count_replied forKey:@"count_replied"];
                                                      
                                                      [self saveTreeContext:ctx];
                                                      
                                                      if (dataBlock) {
                                                          dataBlock(profile_data);
                                                      }
                                                  }];
                                                  
                                              }// records
                                              
                                          }// dictionary
                                          
                                      }// !error
                                  }];
    
    [task resume];
}

- (void)downloadImagePeopleShortcut:(NSManagedObject *)object dataBlock:(ResourceManagerBinaryBlock)dataBlock {
    if (object != nil) {
        
        //URL PATH CREATION
        NSString *image_url = [object valueForKey:@"avatar"];
        NSLog(@"IMAGE URL [%@]", image_url);
        NSURLSession *s = [NSURLSession sharedSession];
        NSURL *imageURL = [NSURL URLWithString:image_url];
        NSURLSessionDownloadTask *dt = [s downloadTaskWithURL:imageURL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            
            if (location) {
                NSData *data = [NSData dataWithContentsOfURL:location];
                if (data) {
                    NSManagedObjectContext *ctx = _workerContext;
                    [ctx performBlock:^{
                        [object setValue:data forKey:@"thumbnail"];
                        [self saveTreeContext:ctx];
                    }];
                    if (dataBlock) {
                        dataBlock(data);
                    }
                }
            }
        }];
        [dt resume];
    }
    
}

- (void)requestPeopleForUserID:(NSString *)userid doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSString *peopleInfo = [NSString stringWithFormat:kEndPointGetPeopleForUser, userid ];
    NSString *url = [Utils buildUrl:peopleInfo];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:[NSURL URLWithString:url] body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          
                                          // VIBALHP FIX
                                          NSDictionary *meta = dictionary[@"_meta"];
                                          NSString *metaCount = [self stringValue:meta[@"count"]];
                                          NSString *metaStatus = meta[@"status"];
                                          
                                          NSManagedObjectContext *ctx = self.workerContext;
                                          
                                          if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
                                              [ctx performBlock:^{
                                                  [self clearDataForEntity:kShortcutPeopleEntity withPredicate:nil context:ctx];
                                              }];
                                              
                                              if (doneBlock) {
                                                  doneBlock(NO);
                                              }
                                              
                                              return;
                                          }
                                          
                                          if (dictionary) {
                                              
                                              NSArray *records = [NSArray arrayWithArray: dictionary[@"records"] ];
                                              
                                              if (records > 0) {
                                                  
                                                  NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                                                  
                                                  NSManagedObjectContext *ctx = _workerContext;
                                                  
                                                  [ctx performBlock:^{
                                                      
                                                      //CLEAR CONTENTS (IMPORTANT)
                                                      [self clearDataForEntity:kShortcutPeopleEntity withPredicate:nil context:ctx];
                                                      
                                                      //REFRESH DATA
                                                      for (NSDictionary *d in records) {
                                                          //                              NSLog(@"data : object : %@", d);
                                                          
                                                          /*
                                                           avatar = "/uploads/16/avatars/20150401/a1c1add31cbeb7a0ee609b9f16d6daff.";
                                                           "first_name" = "Carryll ";
                                                           "is_logged_in" = 1;
                                                           "last_name" = Fritz;
                                                           "mid_name" = Patel;
                                                           "section_id" = 1;
                                                           "section_name" = Gold;
                                                           "user_id" = 16;
                                                           "user_type_id" = 4;
                                                           */
                                                          
                                                          NSString *avatar = [NSString stringWithFormat:@"%@%@", homeurl, d[@"avatar"] ];
                                                          NSString *first_name = [NSString stringWithFormat:@"%@", d[@"first_name"] ];
                                                          NSString *last_name = [NSString stringWithFormat:@"%@", d[@"last_name"] ];
                                                          NSString *mid_name = [NSString stringWithFormat:@"%@", d[@"mid_name"] ];
                                                          NSString *is_logged_in = [NSString stringWithFormat:@"%@", d[@"is_logged_in"] ];
                                                          NSString *section_id = [NSString stringWithFormat:@"%@", d[@"section_id"] ];
                                                          NSString *section_name = [NSString stringWithFormat:@"%@", d[@"section_name"] ];
                                                          NSString *other_user_id = [NSString stringWithFormat:@"%@", d[@"user_id"] ];
                                                          NSString *user_type_id = [NSString stringWithFormat:@"%@", d[@"user_type_id"] ];
                                                          
                                                          NSManagedObject *mo = [self getEntity:kShortcutPeopleEntity
                                                                                      attribute:@"user_id"
                                                                                      parameter:other_user_id
                                                                                        context:ctx];
                                                          if (mo == nil) {
                                                              mo = [NSEntityDescription insertNewObjectForEntityForName:kShortcutPeopleEntity
                                                                                                 inManagedObjectContext:ctx];
                                                          }
                                                          
                                                          [mo setValue:avatar forKey:@"avatar"];
                                                          [mo setValue:first_name forKey:@"first_name"];
                                                          [mo setValue:mid_name forKey:@"mid_name"];
                                                          [mo setValue:last_name forKey:@"last_name"];
                                                          [mo setValue:is_logged_in forKey:@"is_logged_in"];
                                                          [mo setValue:section_id forKey:@"section_id"];
                                                          [mo setValue:section_name forKey:@"section_name"];
                                                          [mo setValue:is_logged_in forKey:@"is_logged_in"];
                                                          [mo setValue:user_type_id forKey:@"user_type_id"];
                                                          [mo setValue:other_user_id forKey:@"user_id"];
                                                          
                                                          
                                                          //                              NSData *thumbnail = [mo valueForKey:@"thumbnail"];
                                                          //                              if (thumbnail == nil) {
                                                          //                                  [self prefetchImageDataForEntity:kShortcutPeopleEntity attribute:@"user_id"
                                                          //                                                          stringID:other_user_id withURL:avatar
                                                          //                                                            setKey:@"thumbnail"];
                                                          //                              }
                                                          
                                                      }
                                                      
                                                      [self saveTreeContext:ctx];
                                                      
                                                  }];
                                                  
                                                  
                                                  if (doneBlock) {
                                                      doneBlock(YES);
                                                  }
                                              }
                                              
                                          }
                                          
                                      }
                                      
                                  }];
    
    [task resume];
}

- (void)requestPeopleForUserID:(NSString *)userid dataBlock:(ResourceManagerDataBlock)dataBlock {
    
    NSString *peopleInfo = [NSString stringWithFormat:kEndPointGetPeopleForUser, userid ];
    NSString *url = [Utils buildUrl:peopleInfo];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:[NSURL URLWithString:url] body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              
                                              NSArray *records = dictionary[@"records"];
                                              NSDictionary *meta = dictionary[@"_meta"];
                                              NSString *metaCount = [self stringValue:meta[@"count"]];
                                              NSString *metaStatus = meta[@"status"];
                                              
                                              NSManagedObjectContext *ctx = self.workerContext;
                                              
                                              if (dictionary == nil || dictionary[@"records"] == nil || [metaCount isEqualToString:@"0"] || [metaStatus isEqualToString:@"ERROR"]) {
                                                  [ctx performBlock:^{
                                                      [self clearDataForEntity:kShortcutPeopleEntity withPredicate:nil context:ctx];
                                                  }];
                                                  
                                                  if (dataBlock) {
                                                      dataBlock(nil);
                                                  }
                                                  
                                                  return;
                                              }
                                              if (records > 0) {
                                                  
                                                  NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                                                  
                                                  NSManagedObjectContext *ctx = _workerContext;
                                                  
                                                  NSMutableDictionary *classObjects = [NSMutableDictionary dictionary];
                                                  
                                                  [ctx performBlock:^{
                                                      
                                                      //CLEAR CONTENTS (IMPORTANT)
                                                      [self clearDataForEntity:kShortcutPeopleEntity withPredicate:nil context:ctx];
                                                      
                                                      //REFRESH DATA
                                                      for (NSDictionary *d in records) {
                                                          //                              NSLog(@"data : object : %@", d);
                                                          
                                                          /*
                                                           avatar = "/uploads/16/avatars/20150401/a1c1add31cbeb7a0ee609b9f16d6daff.";
                                                           "first_name" = "Carryll ";
                                                           "is_logged_in" = 1;
                                                           "last_name" = Fritz;
                                                           "mid_name" = Patel;
                                                           "section_id" = 1;
                                                           "section_name" = Gold;
                                                           "user_id" = 16;
                                                           "user_type_id" = 4;
                                                           */
                                                          
                                                          NSString *avatar = [NSString stringWithFormat:@"%@%@", homeurl, d[@"avatar"] ];
                                                          NSString *first_name = [NSString stringWithFormat:@"%@", d[@"first_name"] ];
                                                          NSString *last_name = [NSString stringWithFormat:@"%@", d[@"last_name"] ];
                                                          NSString *mid_name = [NSString stringWithFormat:@"%@", d[@"mid_name"] ];
                                                          NSString *is_logged_in = [NSString stringWithFormat:@"%@", d[@"is_logged_in"] ];
                                                          NSString *section_id = [NSString stringWithFormat:@"%@", d[@"section_id"] ];
                                                          NSString *section_name = [NSString stringWithFormat:@"%@", d[@"section_name"] ];
                                                          NSString *other_user_id = [NSString stringWithFormat:@"%@", d[@"user_id"] ];
                                                          NSString *user_type_id = [NSString stringWithFormat:@"%@", d[@"user_type_id"] ];
                                                          
                                                          [classObjects setValue:section_name forKey:section_id];
                                                          
                                                          NSManagedObject *mo = [self getEntity:kShortcutPeopleEntity
                                                                                      attribute:@"user_id"
                                                                                      parameter:other_user_id
                                                                                        context:ctx];
                                                          if (mo == nil) {
                                                              mo = [NSEntityDescription insertNewObjectForEntityForName:kShortcutPeopleEntity
                                                                                                 inManagedObjectContext:ctx];
                                                          }
                                                          
                                                          [mo setValue:avatar forKey:@"avatar"];
                                                          [mo setValue:first_name forKey:@"first_name"];
                                                          [mo setValue:mid_name forKey:@"mid_name"];
                                                          [mo setValue:last_name forKey:@"last_name"];
                                                          [mo setValue:is_logged_in forKey:@"is_logged_in"];
                                                          [mo setValue:section_id forKey:@"section_id"];
                                                          [mo setValue:section_name forKey:@"section_name"];
                                                          [mo setValue:is_logged_in forKey:@"is_logged_in"];
                                                          [mo setValue:user_type_id forKey:@"user_type_id"];
                                                          [mo setValue:other_user_id forKey:@"user_id"];
                                                          
                                                          
                                                          //                                                          NSData *thumbnail = [mo valueForKey:@"thumbnail"];
                                                          //                                                          if (thumbnail == nil) {
                                                          //                                  [self prefetchImageDataForEntity:kShortcutPeopleEntity attribute:@"user_id"
                                                          //                                                          stringID:other_user_id withURL:avatar
                                                          //                                                            setKey:@"thumbnail"];
                                                          //                                                              [self prefetchImageDataForManagedObject:mo context:ctx withURL:avatar setKey:@"thumbnail"];
                                                          //                                                          }
                                                          
                                                      }
                                                      
                                                      [self saveTreeContext:ctx];
                                                      
                                                      
                                                      if (dataBlock) {
                                                          dataBlock(classObjects);
                                                      }
                                                      
                                                  }];
                                                  
                                                  
                                              }
                                              
                                          }
                                          
                                      }
                                      
                                  }];
    
    [task resume];
}

- (void)prefetchImageDataForManagedObject:(NSManagedObject *)mo
                                  context:(NSManagedObjectContext *)ctx
                                  withURL:(NSString *)url
                                   setKey:(NSString *)key {
    
    NSURLSession *s = [NSURLSession sharedSession];
    NSURL *link = [NSURL URLWithString:url];
    NSURLSessionDownloadTask *dt = [s downloadTaskWithURL:link completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        if (location) {
            NSData *data = [NSData dataWithContentsOfURL:location];
            
            if (mo) {
                [mo setValue:data forKey:key];
                //                [self saveTreeContext:ctx];
            }
        }
    }];
    
    [dt resume];
}

/* Sent periodically to notify the delegate of download progress. */
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    
    CGFloat progress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    if (self.downloadProgressHandler) {
        self.downloadProgressHandler(progress);
    }
}

- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    
    NSLog(@"DOWNLOAD FILE PATH : %@", location);
}

- (NSString *)categoryForFileExtension:(NSString *)extension {
    
    NSString *defaultType = @"OTHERS";
    
    /*
     FILE CLASSIFICATION:
     ALL 		- All files
     DOCUMENT 	- DOC, DOCX, RTF, TXT, XLS, XLSX, PPT, PPTX, PDF
     MEDIA		- MP3, MP4
     IMAGE 		- JPG, PNG, GIFF, TIFF
     OTHERS		- XML, HTML, undeclared file formats
     */
    
    NSArray *doctypes = @[@"doc",@"docx",@"rtf",@"txt",@"xls",@"xlsx",@"ppt",@"pdf",@"epub",@"csv"];
    NSArray *mediatypes = @[@"mp3",@"mp4"];
    NSArray *imagetypes = @[@"jpg",@"jpeg",@"png",@"giff",@"tiff"];
    //    NSArray *othertypes = @[@"xml",@"html"];
    
    NSString *file_extension = [NSString stringWithFormat:@"%@", [extension lowercaseString] ];
    
    for (NSString *ext in doctypes) {
        if ([ext isEqualToString:file_extension]) {
            return @"DOCUMENT";
        }
    }
    
    for (NSString *ext in mediatypes) {
        if ([ext isEqualToString:file_extension]) {
            return @"MEDIA";
        }
    }
    
    for (NSString *ext in imagetypes) {
        if ([ext isEqualToString:file_extension]) {
            return @"IMAGE";
        }
    }
    
    return defaultType;
}

#pragma mark - ResourceManager Global Notifications

- (void)requestNotificationWithClearData:(BOOL)clearData contentBlock:(ResourceManagerContent)contentBlock {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
    NSString *user_id = [NSString stringWithFormat:@"%d", account.user.id];
    NSString *device_id = [NSString stringWithFormat:@"%@", account.device.deviceId ];
    
    NSManagedObjectContext *ctx = _workerContext;
    if (clearData) {
        //CLEAR CONTENTS (IMPORTANT)
        [self clearDataForEntity:kSocialStreamFeedEntity withPredicate:nil context:ctx];
    }
    
    NSString *notificationPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointGlobalNotification, user_id, device_id]];
    NSURL *postURL = [NSURL URLWithString:notificationPath];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:postURL body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"%s error %@", __PRETTY_FUNCTION__, [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              
                                              //                  NSLog(@"dictionary : %@", dictionary);
                                              NSArray *records = dictionary[@"records"];
                                              
                                              NSString *count = [NSString stringWithFormat:@"%lu", (unsigned long)records.count];
                                              
                                              if (records > 0) {
                                                  
                                                  NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                                                  
                                                  
                                                  [ctx performBlock:^{
                                                      
                                                      //REFRESH DATA
                                                      for (NSDictionary *d in records) {
                                                          
                                                          /*
                                                           id = 14;
                                                           action = comment;
                                                           avatar = "/img/avatar/generic-boy.png";
                                                           "content_id" = 102;
                                                           "course_name" = Daffodils;
                                                           "course_section" = "English 7";
                                                           "date_created" = "2015-02-25 00:42:03";
                                                           "date_modified" = "2015-02-25 00:42:03";
                                                           "first_name" = Yuan;
                                                           "last_name" = Yu;
                                                           "user_id" = 123;
                                                           "group_id" = 12;
                                                           "module_id" = 3;
                                                           "module_name" = "Social Stream";
                                                           "is_deleted" = 0;
                                                           */
                                                          
                                                          NSString *notif_id = [NSString stringWithFormat:@"%@", d[@"id"] ];
                                                          NSString *action = [NSString stringWithFormat:@"%@", d[@"action"] ];
                                                          NSString *avatar = [NSString stringWithFormat:@"%@%@", homeurl, d[@"avatar"] ];
                                                          NSString *content_id = [NSString stringWithFormat:@"%@", d[@"content_id"] ];
                                                          NSString *course_name = [NSString stringWithFormat:@"%@", d[@"course_name"] ];
                                                          NSString *course_section = [NSString stringWithFormat:@"%@", d[@"course_section"] ];
                                                          NSString *date_created = [NSString stringWithFormat:@"%@", d[@"date_created"] ];
                                                          NSString *date_modified = [NSString stringWithFormat:@"%@", d[@"date_modified"] ];
                                                          NSString *first_name = [NSString stringWithFormat:@"%@", d[@"first_name"] ];
                                                          NSString *last_name = [NSString stringWithFormat:@"%@", d[@"last_name"] ];
                                                          NSString *user_id = [NSString stringWithFormat:@"%@", d[@"user_id"] ];
                                                          NSString *group_id = [NSString stringWithFormat:@"%@", d[@"group_id"] ];
                                                          NSString *module_id = [NSString stringWithFormat:@"%@", d[@"module_id"] ];
                                                          NSString *module_name = [NSString stringWithFormat:@"%@", d[@"module_name"] ];
                                                          NSString *is_deleted = [NSString stringWithFormat:@"%@", d[@"is_deleted"] ];
                                                          
                                                          NSManagedObject *mo = [self getEntity:kGlobalNotificationEntity
                                                                                      attribute:@"notification_id"
                                                                                      parameter:notif_id
                                                                                        context:ctx];
                                                          if (mo == nil) {
                                                              mo = [NSEntityDescription insertNewObjectForEntityForName:kGlobalNotificationEntity
                                                                                                 inManagedObjectContext:ctx];
                                                              [mo setValue:@"0" forKey:@"is_seen"];
                                                          }
                                                          
                                                          [mo setValue:notif_id forKey:@"notification_id"];
                                                          [mo setValue:action forKey:@"action"];
                                                          [mo setValue:avatar forKey:@"avatar"];
                                                          [mo setValue:content_id forKey:@"content_id"];
                                                          [mo setValue:course_name forKey:@"course_name"];
                                                          [mo setValue:course_section forKey:@"course_section"];
                                                          [mo setValue:date_created forKey:@"date_created"];
                                                          [mo setValue:date_modified forKey:@"date_modified"];
                                                          [mo setValue:first_name forKey:@"first_name"];
                                                          [mo setValue:last_name forKey:@"last_name"];
                                                          [mo setValue:user_id forKey:@"user_id"];
                                                          [mo setValue:group_id forKey:@"group_id"];
                                                          [mo setValue:module_id forKey:@"module_id"];
                                                          [mo setValue:module_name forKey:@"module_name"];
                                                          [mo setValue:is_deleted forKey:@"is_deleted"];
                                                      }
                                                      
                                                      [self saveTreeContext:ctx];
                                                      
                                                      
                                                      if (contentBlock) {
                                                          contentBlock(count);
                                                      }
                                                      
                                                  }];
                                              }
                                              
                                              
                                          }
                                      }
                                  }];
    
    [task resume];
    
}

- (void)requestNotificationDetailsWithMessageID:(NSString *)messageid doneBlock:(ResourceManagerDoneBlock)doneBlock {
    
    NSString *notificationPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointNotificationDetail, messageid]];
    
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET"
                                                          NSURL:[NSURL URLWithString:notificationPath]
                                                           body:nil];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              NSLog(@"dictionary : %@", dictionary);
                                              NSDictionary *meta = dictionary[@"_meta"];
                                              
                                              NSManagedObjectContext *ctx = _workerContext;
                                              if ([meta[@"count"] integerValue] > 0) {
                                                  NSArray *records = dictionary[@"records"];
                                                  NSString *homeurl = [NSString stringWithFormat:@"http://%@", [Utils getVibeServer] ];
                                                  [ctx performBlock:^{
                                                      for (NSDictionary *d in records) {
                                                          NSManagedObject *mo = nil;
                                                          [self processData:d
                                                              managedObject:mo
                                                                    context:ctx
                                                                    options:homeurl];
                                                      }
                                                      
                                                      [self saveTreeContext:ctx];
                                                      if (doneBlock) {
                                                          doneBlock(YES);
                                                      }
                                                  }];
                                              }// count > 0
                                              
                                              if ([meta[@"count"] integerValue] == 0) {
                                                  [ctx performBlock:^{
                                                      NSManagedObject *mo = [self getEntity:kGlobalNotificationEntity attribute:@"content_id"
                                                                                  parameter:messageid context:ctx];
                                                      [ctx deleteObject:mo];
                                                      [self saveTreeContext:ctx];
                                                      if (doneBlock) {
                                                          doneBlock(NO);
                                                      }
                                                  }];
                                              }// count = 0
                                              
                                          }
                                      }
                                  }];
    
    [task resume];
    
}

- (NSString *)readCountFromNotifications {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kGlobalNotificationEntity];
    NSPredicate *predicate = [self predicateForKeyPath:@"is_seen" andValue:@"0"];
    [request setPredicate:predicate];
    
    NSManagedObjectContext *ctx = _workerContext;
    
    NSError *error = nil;
    NSArray *items = [ctx executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"error : %@", [error localizedDescription] );
    }
    
    NSString *count = [NSString stringWithFormat:@"%lu", (unsigned long)items.count];
    
    return count;
}

- (void)traverseXMLElement:(TBXMLElement *)element {
    
    do {
        // Display the name of the element
        NSString *eName = [TBXML elementName:element];
        if ([eName isEqual:@"meta"]) {
            
            TBXMLAttribute *attribute = element->firstAttribute;
            while (attribute) {
                
                NSString *eAttrib = [TBXML attributeName:attribute];
                if ([eAttrib isEqualToString:@"property"]) {
                    
                    NSString *eVal = [TBXML attributeValue:attribute];
                    
                    NSScanner *scanner = [NSScanner scannerWithString:eVal];
                    NSString *pattern = nil;
                    while (![scanner isAtEnd]) {
                        
                        [scanner scanUpToString:@"<body>" intoString:NULL];
                        [scanner scanString:@"<body>" intoString:NULL];
                        [scanner scanUpToString:@"</body>" intoString:&pattern];
                    }
                    NSLog(@"%@",pattern); // 2015-01-28 15:58:00.360 ScanningOfHTMLProblem[1373:661934] this is the body
                    
                }
                
                //                NSLog(@"%@->%@ = %@", [TBXML elementName:element], [TBXML attributeName:attribute], [TBXML attributeValue:attribute]);
                attribute = attribute->next;
            }
            
        }
        
        if (element->firstChild) {
            [self traverseXMLElement:element->firstChild];
        }
        
    } while ( (element = element->nextSibling) );
}

#pragma mark - Change Password

/////// CHANGE PASSWORD //////

- (void)requestNewPasswordForUser:(NSString *)userid
                     withPassword:(NSString *)oldpassword
                      newPassword:(NSString *)newPassword doneBlock:(ResourceManagerDoneBlock)doneBlock
{
    
    NSString *access_token = [NSString stringWithFormat:@"%@", [Utils getAccessToken] ];
    
    NSString *user_id = [NSString stringWithFormat:@"%@", userid];
    NSString *old_password = [NSString stringWithFormat:@"%@", oldpassword];
    NSString *new_password = [NSString stringWithFormat:@"%@", newPassword];
    NSString *confirm_password = [NSString stringWithFormat:@"%@", newPassword];
    
    NSDictionary *data = @{ @"uid": user_id,
                            @"access_token": access_token,
                            @"oldpassword": old_password,
                            @"password": new_password,
                            @"password2": confirm_password };
    
    NSURL *postURL = [NSURL URLWithString:[Utils buildUrl:kEndPointChangePasswordForUser]];
    NSLog(@"post url : %@", postURL);
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:postURL body:data];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                      }
                                      
                                      if (!error) {
                                          
                                          /*
                                           {
                                           "_meta": {
                                           "status": "SUCCESS",
                                           "count": 2
                                           },
                                           
                                           "records": {
                                           "user_info": {
                                           "id": "25",
                                           "vibe_id": "25",
                                           "email": "csang@vsmart.com",
                                           "username": "csang@vsmart.com",
                                           "salt": null,
                                           "password": "password1",
                                           "user_type_id": "4",
                                           "status": "0",
                                           "avatar": "/public/img/avatar/generic-girl.png",
                                           "school_year": "SY2014-2015",
                                           "is_deleted": "0",
                                           "date_created": "2014-11-12 10:05:20",
                                           "date_modified": "2015-06-10 07:52:30",
                                           "is_logged_in": "0"
                                           },
                                           "message": "Password successfully updated!"
                                           }
                                           }
                                           */
                                          
                                          
                                          BOOL status = NO;
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary) {
                                              
                                              NSLog(@"dictionary : %@", dictionary);
                                              
                                              NSDictionary *meta = dictionary[@"_meta"];
                                              NSString *metaStatus = [NSString stringWithFormat:@"%@", meta[@"status"]];
                                              
                                              NSArray *records = dictionary[@"records"];
                                              if (records > 0) {
                                                  
                                                  status = ![metaStatus isEqualToString:@"ERROR"] ? YES : NO;
                                                  
                                                  if (status == YES) {
                                                      // UPDATE GLOBAL ACCOUNT
                                                      AccountInfo *account = (AccountInfo *)[Utils getArchive:kGlobalAccount];
                                                      account.user.password = [NSString stringWithFormat:@"%@", new_password];
                                                      [Utils saveArchive:account forKey:kGlobalAccount];
                                                      
                                                  }
                                                  
                                                  
                                              }
                                              
                                              if (doneBlock) {
                                                  doneBlock(status);
                                              }
                                          }
                                      }
                                  }];
    
    [task resume];
}

- (void)setNotificationAsSeen:(NSString *)notification_id {
    
    NSPredicate *predicate = [self predicateForKeyPath:@"notification_id" andValue:notification_id];
    NSManagedObjectContext *ctx = _workerContext;
    NSManagedObject *mo = [self getEntity:kGlobalNotificationEntity predicate:predicate context:ctx];
    
    [ctx performBlock:^{
        [mo setValue:@"1" forKey:@"is_seen"];
    }];
    [self saveTreeContext:ctx];
}

- (void)requestGradeBookForClassID:(NSString *)class_id doneBlock:(ResourceManagerDoneBlock)doneBlock  {
    
    NSManagedObjectContext *gradeBookCtx = self.workerContext;
    [self clearContentsForEntity:@"GradeBook" predicate:nil];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointGradeBook, class_id] ];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          if (doneBlock) {
                                              doneBlock(NO);
                                          }
                                      }
                                      
                                      if (!error) {
                                          
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          NSArray *records = dictionary[@"records"];
                                          
                                          NSDictionary *tableHeadDict = records[0];
                                          NSArray *tableHeadAr = tableHeadDict[@"table_head"];
                                          
                                          NSDictionary *tableBodyDict = records[1];
                                          NSArray *tableBodyAr = tableBodyDict[@"table_body"];
                                          
                                          
                                          
                                          //              {
                                          //                  "deploy_id": "44",
                                          //                  "activity_id": "57",
                                          //                  "activity_type_id": "8",
                                          //                  "column_name": "ACT44",
                                          //                  "activity_type": "V-Smart Deployed Test",
                                          //                  "activity_name": "SIR JULIUS TEST",
                                          //                  "item_id": "57"
                                          //              }
                                          
                                          /**
                                           id
                                           cell_value
                                           is_header
                                           index
                                           section_index
                                           **/
                                          
                                          [gradeBookCtx performBlockAndWait:^{
                                              NSMutableArray *collumn_nameArray = [NSMutableArray array];
                                              NSMutableArray *deploy_idArray = [NSMutableArray array];
                                              NSInteger index = 1;
                                              
                                              for (NSDictionary *tableHeadDict in tableHeadAr) {
                                                  
                                                  NSString *is_header = @"1";
                                                  
                                                  NSString *column_name = [self stringValue:tableHeadDict[@"column_name"]];
                                                  
                                                  if (![column_name isEqualToString:@"AVATAR"]) {
                                                      NSString *deploy_id = [self stringValue:tableHeadDict[@"deploy_id"]];
                                                      NSString *activity_id = [self stringValue:tableHeadDict[@"activity_id"]];
                                                      NSString *activity_type_id = [self stringValue:tableHeadDict[@"activity_type_id"]];
                                                      NSString *activity_type = [self stringValue:tableHeadDict[@"activity_type"]];
                                                      NSString *activity_name = [self stringValue:tableHeadDict[@"activity_name"]];
                                                      NSString *item_id = [self stringValue:tableHeadDict[@"item_id"]];
                                                      
                                                      
                                                      // adding "default" values for empty strings/no keys
                                                      deploy_id = ([deploy_id isEqualToString:@""]) ? @"0" : deploy_id;
                                                      activity_id = ([activity_id isEqualToString:@""]) ? @"0" : activity_id;
                                                      activity_type_id = ([activity_type_id isEqualToString:@""]) ? @"0" : activity_type_id;
                                                      activity_type = ([activity_type isEqualToString:@""]) ? @"0" : activity_type;
                                                      activity_name = ([activity_name isEqualToString:@""]) ? column_name : activity_name;
                                                      item_id = ([item_id isEqualToString:@""]) ? @"0" : item_id;
                                                      
                                                      NSManagedObject *g_mo = [NSEntityDescription insertNewObjectForEntityForName:@"GradeBook" inManagedObjectContext:gradeBookCtx];
                                                      
                                                      //                  [gradeBookCtx performBlock:^{
                                                      [g_mo setValue:deploy_id forKey:@"id"];
                                                      [g_mo setValue:activity_name forKey:@"cell_value"];
                                                      [g_mo setValue:is_header forKey:@"is_header"];
                                                      [g_mo setValue:@(index) forKey:@"index"];
                                                      [g_mo setValue:@(1) forKey:@"section_index"];
                                                      
                                                      //                      [c_mo setValue:collumn_name forKey:@"collumn_name"];
                                                      //                      [c_mo setValue:activity_id forKey:@"activity_id"];
                                                      //                      [c_mo setValue:activity_type_id forKey:@"activity_type_id"];
                                                      //                      [c_mo setValue:activity_type forKey:@"activity_type"];
                                                      //
                                                      //                      [c_mo setValue:item_id forKey:@"item_id"];
                                                      
                                                      //                  }];
                                                      
                                                      [collumn_nameArray addObject:column_name];
                                                      [deploy_idArray addObject:deploy_id];
                                                      
                                                      index += 1;
                                                  }
                                              }
                                              
                                              /**
                                               id
                                               cell_value
                                               is_header
                                               index
                                               section_index
                                               **/
                                              
                                              NSInteger body_index = 1;
                                              NSInteger section_body_index = 2;
                                              
                                              // TABLE BODY
                                              
                                              for (NSDictionary *tableBodyDict in tableBodyAr) {
                                                  
                                                  NSString *is_header = @"0";
                                                  
                                                  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                  // ID, NAME
                                                  //
                                                  NSString *user_id = [self stringValue:tableBodyDict[@"user_id"]]; //CHECKED
                                                  NSString *participant_id = [self stringValue:tableBodyDict[@"participant_id"]];
                                                  //                  NSString *avatar = [self stringValue:tableBodyDict[@"AVATAR"]];
                                                  NSString *name = [self stringValue:tableBodyDict[@"NAME"]];
                                                  
                                                  NSManagedObject *name_mo = [NSEntityDescription insertNewObjectForEntityForName:@"GradeBook" inManagedObjectContext:gradeBookCtx];
                                                  
                                                  [name_mo setValue:user_id forKey:@"id"];
                                                  [name_mo setValue:name forKey:@"cell_value"];
                                                  [name_mo setValue:is_header forKey:@"is_header"];
                                                  [name_mo setValue:@(body_index) forKey:@"index"];
                                                  [name_mo setValue:@(section_body_index) forKey:@"section_index"];
                                                  //
                                                  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                  
                                                  
                                                  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                  // ACTIVITY NAMES
                                                  //
                                                  
                                                  NSInteger arrayIndex = 0;
                                                  for (NSString *activity_score_key in collumn_nameArray) {
                                                      body_index += 1;
                                                      
                                                      if (![activity_score_key isEqualToString:@"NAME"] && ![activity_score_key isEqualToString:@"AVATAR"]) {
                                                          NSString *deploy_id = deploy_idArray[arrayIndex];
                                                          
                                                          NSString *activity_score = [self stringValue:tableBodyDict[activity_score_key]];
                                                          NSString *took_test = (activity_score.length == 0) ? @"0" : @"1";
                                                          activity_score = ((activity_score.length == 0)) ? @"0" : activity_score;
                                                          activity_score = [self formatStringNumber:activity_score];
                                                          
                                                          NSString *exp_activity_key = [NSString stringWithFormat:@"EXP_%@", activity_score_key];
                                                          NSString *exp_activity_score = [self stringValue:tableBodyDict[exp_activity_key]];
                                                          exp_activity_score = ((activity_score.length == 0)) ? @"0" : exp_activity_score;
                                                          exp_activity_score = [self formatStringNumber:exp_activity_score];
                                                          
                                                          NSString *rem_activity_key = [NSString stringWithFormat:@"REMARK_%@", activity_score_key];
                                                          NSString *remark_activity = [self stringValue:tableBodyDict[rem_activity_key]];
                                                          
                                                          NSString *totalAndExpected = [NSString stringWithFormat:@"%@ / %@", activity_score, exp_activity_score];
                                                          
                                                          NSManagedObject *act_mo = [NSEntityDescription insertNewObjectForEntityForName:@"GradeBook" inManagedObjectContext:gradeBookCtx];
                                                          
                                                          [act_mo setValue:user_id forKey:@"id"];
                                                          [act_mo setValue:totalAndExpected forKey:@"cell_value"];
                                                          [act_mo setValue:is_header forKey:@"is_header"];
                                                          [act_mo setValue:@(body_index) forKey:@"index"];
                                                          [act_mo setValue:@(section_body_index) forKey:@"section_index"];
                                                          
                                                          [act_mo setValue:class_id forKey:@"class_id"];
                                                          [act_mo setValue:name forKey:@"student_name"];
                                                          [act_mo setValue:participant_id forKey:@"participant_id"];
                                                          [act_mo setValue:deploy_id forKey:@"deploy_id"];
                                                          [act_mo setValue:exp_activity_score forKey:@"exp_score"];
                                                          [act_mo setValue:activity_score forKey:@"score"];
                                                          [act_mo setValue:remark_activity forKey:@"remark_activity"];
                                                          [act_mo setValue:took_test forKey:@"took_test"];
                                                      }
                                                      
                                                      arrayIndex += 1;
                                                  }
                                                  
                                                  body_index = 1;
                                                  section_body_index += 1;
                                              }
                                              
                                              [self saveTreeContext:gradeBookCtx];
                                              
                                              
                                              if (doneBlock) {
                                                  doneBlock(YES);
                                              }
                                          }];
                                          
                                      }//end
                                  }];
    
    [task resume];
}

//- (void)postGradeBookChangeScoreForData:(NSDictionary *)body doneBlock:(ResourceManagerDoneBlock)doneBlock {
//    
//    NSString *participant_id = body[@"participant_id"];
//    NSString *deploy_id = body[@"deploy_id"];
//    
//    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointGradeBookChangeScore, participant_id, deploy_id] ];
//    NSLog(@"path : %@", url);
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:body];
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
//                                          {
//                                              if (error) {
//                                                  NSLog(@"error %@", [error localizedDescription]);
//                                                  if (doneBlock) {
//                                                      doneBlock(NO);
//                                                  }
//                                              }
//                                              
//                                              if (data != nil) {
//                                                  NSDictionary *dictionary = [self parseResponseData:data];
//                                                  
//                                                  NSLog(@"RESPONSE DATA : %@", dictionary);
//                                                  if (doneBlock) {
//                                                      doneBlock(YES);
//                                                  }
//                                              }
//                                              
//                                          }];
//    
//    [postDataTask resume];
//}
//
//- (void)postGradeBookSetScoreForData:(NSDictionary *)body doneBlock:(ResourceManagerDoneBlock)doneBlock {
//    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointGradeBookSetScore]];
//    NSLog(@"path : %@", url);
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"POST" NSURL:url body:body];
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
//                                          {
//                                              if (error) {
//                                                  NSLog(@"error %@", [error localizedDescription]);
//                                                  if (doneBlock) {
//                                                      doneBlock(NO);
//                                                  }
//                                              }
//                                              
//                                              if (data != nil) {
//                                                  NSDictionary *dictionary = [self parseResponseData:data];
//                                                  
//                                                  NSLog(@"RESPONSE DATA : %@", dictionary);
//                                                  if (doneBlock) {
//                                                      doneBlock(YES);
//                                                  }
//                                              }
//                                              
//                                          }];
//    
//    [postDataTask resume];
//}
//
//- (NSUInteger)countRecordsForEntity:(NSString *)entity attribute:(NSString *)attribute value:(id)value {
//    
//    NSPredicate *predicate = [self predicateForKeyPath:attribute andValue:value];
//    
//    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entity];
//    [request setIncludesSubentities:NO]; //Omit subentities. Default is YES (i.e. include subentities)
//    
//    if (predicate) {
//        [request setPredicate:predicate];
//    }
//    
//    NSManagedObjectContext *ctx = _mainContext;
//    
//    NSError *err;
//    NSUInteger count = [ctx countForFetchRequest:request error:&err];
//    //    if(count == NSNotFound) {
//    //    }
//    
//    
//    
//    return count;
    //    }
    
- (void)updateSortKeyOfGradeBookSectionHeader:(BOOL)isAscending {
    NSPredicate *predicate = [self predicateForKeyPath:@"section_index" andValue:@"1"];
    NSArray *headers = [self getObjectsForEntity:@"GradeBook" predicate:predicate];
    
    NSString *new_sort_key = isAscending ? @"z" : @"0";
    
    for (NSManagedObject *mo in headers) {
        NSManagedObjectContext *ctx = mo.managedObjectContext;
        [mo setValue:new_sort_key forKey:@"sort_key"];
        [self saveTreeContext:ctx];
    }
}

- (void)requestGradebookForClass:(NSString *)classCode doneBlock:(ResourceManagerDoneBlock)doneBlock {
    NSManagedObjectContext *ctx = self.workerContext;
    [self clearDataForEntity:@"GradeBookHead" withPredicate:nil context:ctx];
    [self clearDataForEntity:@"GradeBookBody" withPredicate:nil context:ctx];
    
    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointClassGradebook, classCode]];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            if (doneBlock) {
                doneBlock(NO);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:responsedata];
            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
            
            if (isOkayToParse) {
                NSArray *records = dictionary[@"records"];
                NSLog(@"grade book records: %@", records);
                
                if (records.count > 0) {
                    [ctx performBlock:^{
                        NSMutableArray *column_name_list = [NSMutableArray array];
                        
                        for (NSDictionary *d in records) {
                            NSArray *dkeys = [d allKeys];
                            
                            for (NSString *dk in dkeys) {
                                if ([dk isEqualToString:@"table_head"]) {
                                    NSArray *cols = d[dk];
                                    NSLog(@"cols: %@", cols);
                                    
                                    for (NSDictionary *cd in cols) {
                                        NSString *deploy_id = [self stringValue:cd[@"deploy_id"]];
                                        NSString *activity_id = [self stringValue:cd[@"activity_id"]];
                                        NSString *activity_type_id = [self stringValue:cd[@"activity_type_id"]];
                                        NSString *column_name = [self stringValue:cd[@"column_name"]];
                                        NSString *activity_type = [self stringValue:cd[@"activity_type"]];
                                        NSString *activity_name = [self stringValue:cd[@"activity_name"]];
                                        NSString *item_id = [self stringValue:cd[@"item_id"]];
                                        
                                        NSManagedObject *mo = [self getNewEntity:@"GradeBookHead"
                                                                       attribute:@"deploy_id"
                                                                       parameter:deploy_id
                                                                         context:ctx];
                                        
                                        [mo setValue:deploy_id forKey:@"deploy_id"];
                                        [mo setValue:activity_id forKey:@"activity_id"];
                                        [mo setValue:activity_type_id forKey:@"activity_type_id"];
                                        [mo setValue:column_name forKey:@"column_name"];
                                        [mo setValue:activity_type forKey:@"activity_type"];
                                        [mo setValue:activity_name forKey:@"activity_name"];
                                        [mo setValue:item_id forKey:@"item_id"];
                                        
                                        [column_name_list addObject:column_name];
                                    }
                                }
                            }
                            
                            for (NSString *dk in dkeys) {
                                if ([dk isEqualToString:@"table_body"]) {
                                    NSArray *rows = d[dk];
                                    NSLog(@"rows: %@", rows);
                                    
                                    for (NSDictionary *rd in rows) {
                                        for (NSString *column_name in column_name_list) {
                                            NSString *user_id = [self stringValue:rd[@"user_id"]];
                                            NSString *participant_id = [self stringValue:rd[@"participant_id"]];
                                            NSString *actual_score = [self stringValue:rd[column_name]];
                                            NSString *expected_score_key = [NSString stringWithFormat:@"EXP_%@", column_name];
                                            NSString *expected_score = [self stringValue:rd[expected_score_key]];
                                            NSString *remark_key = [NSString stringWithFormat:@"REMARK_%@", column_name];
                                            NSString *remark = [self stringValue:rd[remark_key]];
                                            NSString *cell_code = [NSString stringWithFormat:@"%@%@", user_id, column_name];
                                            
                                            NSManagedObject *mo = [self getNewEntity:@"GradeBookBody"
                                                                           attribute:@"cell_code"
                                                                           parameter:cell_code
                                                                             context:ctx];
                                            
                                            [mo setValue:user_id forKey:@"user_id"];
                                            [mo setValue:participant_id forKey:@"participant_id"];
                                            [mo setValue:actual_score forKey:@"actual_score"];
                                            [mo setValue:expected_score forKey:@"expected_score"];
                                            [mo setValue:remark forKey:@"remark"];
                                            [mo setValue:cell_code forKey:@"cell_code"];
                                        }
                                    }
                                }
                            }
                        }
                        
                        [self saveTreeContext:ctx];
                        
                        if (doneBlock) {
                            doneBlock(YES);
                        }
                    }];
                }
            }
            else {
                if (doneBlock) {
                    doneBlock(NO);
                }
            }
        }
    }];
    
    [task resume];
}

//- (void)requestCourseListForUser:(NSString *)userid dataBlock:(ResourceManagerDataBlock)dataBlock {
//    NSManagedObjectContext *ctx = self.workerContext;
//    [self clearDataForEntity:@"GBCourse" withPredicate:nil context:ctx];
//    
//    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointGBCourseList, userid]];
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error) {
//        
//        if (error) {
//            NSLog(@"error %@", [error localizedDescription]);
//            
//            if (dataBlock) {
//                dataBlock(nil);
//            }
//        }
//        
//        if (!error) {
//            NSDictionary *dictionary = [self parseResponseData:responsedata];
//            BOOL isOkayToParse = [self isItOkayToParseUsingThisResponseData:dictionary];
//            
//            if (isOkayToParse) {
//                NSArray *records = dictionary[@"records"];
//                NSLog(@"course records: %@", records);
//                
//                if (records.count > 0) {
//                    [ctx performBlock:^{
//                        NSInteger counter = 0;
//                        NSDictionary *initial = [NSDictionary dictionary];
//                        
//                        for (NSDictionary *d in records) {
//                            NSString *obj_id = [self stringValue:d[@"id"]];
//                            NSString *course_id = [self stringValue:d[@"course_id"]];
//                            NSString *section_id = [self stringValue:d[@"section_id"]];
//                            NSString *details = [self stringValue:d[@"details"]];
//                            NSString *course_name = [self stringValue:d[@"course_name"]];
//                            NSString *section_name = [self stringValue:d[@"section_name"]];
//                            NSString *grade_level_id = [self stringValue:d[@"grade_level_id"]];
//                            
//                            NSManagedObject *mo = [self getNewEntity:@"GBCourse"
//                                                           attribute:@"id"
//                                                           parameter:obj_id
//                                                             context:ctx];
//                            
//                            [mo setValue:obj_id forKey:@"id"];
//                            [mo setValue:course_id forKey:@"course_id"];
//                            [mo setValue:section_id forKey:@"section_id"];
//                            [mo setValue:details forKey:@"details"];
//                            [mo setValue:course_name forKey:@"course_name"];
//                            [mo setValue:section_name forKey:@"section_name"];
//                            [mo setValue:grade_level_id forKey:@"grade_level_id"];
//                            
//                            if (counter == 0) {
//                                initial = @{@"id":obj_id, @"section_name":section_name, @"course_name":course_name};
//                            }
//                            
//                            counter++;
//                        }
//                        
//                        [self saveTreeContext:ctx];
//                        
//                        if (dataBlock) {
//                            dataBlock(initial);
//                        }
//                    }];
//                }
//            }
//            else {
//                if (dataBlock) {
//                    dataBlock(nil);
//                }
//            }
//        }
//    }];
//    
//    [task resume];
//}
    
//- (void)requestGradeBookForClassID:(NSString *)class_id isAscending:(BOOL)isAscending doneBlock:(ResourceManagerDoneBlock)doneBlock {
//    NSManagedObjectContext *gradeBookCtx = self.workerContext;
//    [self clearContentsForEntity:@"GradeBook" predicate:nil];
//    
//    NSURL *url = [self buildURL:[NSString stringWithFormat:kEndPointGradeBook, class_id] ];
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
//                                  {
//                                      if (error) {
//                                          NSLog(@"error %@", [error localizedDescription]);
//                                          if (doneBlock) {
//                                              doneBlock(NO);
//                                          }
//                                      }
//                                      
//                                      if (!error) {
//                                          
//                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
//                                          NSArray *records = dictionary[@"records"];
//                                          
//                                          NSDictionary *tableHeadDict = records[0];
//                                          NSArray *tableHeadAr = tableHeadDict[@"table_head"];
//                                          
//                                          NSDictionary *tableBodyDict = records[1];
//                                          NSArray *tableBodyAr = tableBodyDict[@"table_body"];
//                                          
//                                          [gradeBookCtx performBlockAndWait:^{
//                                              NSMutableArray *column_nameArray = [NSMutableArray array];
//                                              NSMutableArray *deploy_idArray = [NSMutableArray array];
//                                              NSInteger index = 1;
//                                              
//                                              // TABLE HEADER
//                                              for (NSDictionary *tableHeadDict in tableHeadAr) {
//                                                  NSString *is_header = @"1";
//                                                  NSString *column_name = [self stringValue:tableHeadDict[@"column_name"]];
//                                                  
//                                                  if (![column_name isEqualToString:@"AVATAR"]) {
//                                                      NSString *deploy_id = [self stringValue:tableHeadDict[@"deploy_id"]];
//                                                      NSString *activity_id = [self stringValue:tableHeadDict[@"activity_id"]];
//                                                      NSString *activity_type_id = [self stringValue:tableHeadDict[@"activity_type_id"]];
//                                                      NSString *activity_type = [self stringValue:tableHeadDict[@"activity_type"]];
//                                                      NSString *activity_name = [self stringValue:tableHeadDict[@"activity_name"]];
//                                                      NSString *item_id = [self stringValue:tableHeadDict[@"item_id"]];
//                                                      
//                                                      deploy_id = ([deploy_id isEqualToString:@""]) ? @"0" : deploy_id;
//                                                      activity_id = ([activity_id isEqualToString:@""]) ? @"0" : activity_id;
//                                                      activity_type_id = ([activity_type_id isEqualToString:@""]) ? @"0" : activity_type_id;
//                                                      activity_type = ([activity_type isEqualToString:@""]) ? @"0" : activity_type;
//                                                      activity_name = ([activity_name isEqualToString:@""]) ? column_name : activity_name;
//                                                      item_id = ([item_id isEqualToString:@""]) ? @"0" : item_id;
//                                                      
//                                                      NSManagedObject *g_mo = [NSEntityDescription insertNewObjectForEntityForName:@"GradeBook" inManagedObjectContext:gradeBookCtx];
//                                                      
//                                                      [g_mo setValue:deploy_id forKey:@"id"];
//                                                      [g_mo setValue:activity_name forKey:@"cell_value"];
//                                                      [g_mo setValue:is_header forKey:@"is_header"];
//                                                      [g_mo setValue:@(index) forKey:@"index"];
//                                                      [g_mo setValue:@(1) forKey:@"section_index"];
//                                                      [g_mo setValue:@"C02MM6N0FH01" forKey:@"search_key"]; // DEFAULT search key for header
//                                                      
//                                                      [column_nameArray addObject:column_name];
//                                                      [deploy_idArray addObject:deploy_id];
//                                                      
//                                                      index += 1;
//                                                  }
//                                              }
//                                              
//                                              // FOR SORTING
//                                              NSMutableDictionary *dict_for_sorting = [NSMutableDictionary dictionary];
//                                              for (NSDictionary *tableBodyDict in tableBodyAr) {
//                                                  NSString *key = [self stringValue:tableBodyDict[@"NAME"]];
//                                                  [dict_for_sorting setObject:tableBodyDict forKey:key];
//                                              }
//                                              
//                                              NSArray *sorted_keys = [[dict_for_sorting allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
//                                              
//                                              if (!isAscending) {
//                                                  sorted_keys = [[[sorted_keys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] reverseObjectEnumerator] allObjects];
//                                              }
//                                              
//                                              NSMutableArray *sorted_dict = [NSMutableArray array];
//                                              for (NSString *key in sorted_keys) {
//                                                  [sorted_dict addObject:[dict_for_sorting objectForKey:key]];
//                                              }
//                                              
//                                              NSInteger body_index = 1;
//                                              NSInteger section_body_index = 2;
//                                              
//                                              // TABLE BODY
//                                              for (NSDictionary *tableBodyDict in sorted_dict) {
//                                                  NSString *is_header = @"0";
//                                                  NSString *user_id = [self stringValue:tableBodyDict[@"user_id"]];
//                                                  NSString *participant_id = [self stringValue:tableBodyDict[@"participant_id"]];
//                                                  NSString *name = [self stringValue:tableBodyDict[@"NAME"]];
//                                                  
//                                                  NSManagedObject *name_mo = [NSEntityDescription insertNewObjectForEntityForName:@"GradeBook" inManagedObjectContext:gradeBookCtx];
//                                                  
//                                                  [name_mo setValue:user_id forKey:@"id"];
//                                                  [name_mo setValue:name forKey:@"cell_value"];
//                                                  [name_mo setValue:is_header forKey:@"is_header"];
//                                                  [name_mo setValue:@(body_index) forKey:@"index"];
//                                                  [name_mo setValue:@(section_body_index) forKey:@"section_index"];
//                                                  [name_mo setValue:[name lowercaseString] forKey:@"search_key"];
//                                                  
//                                                  NSInteger arrayIndex = 0;
//                                                  for (NSString *activity_score_key in column_nameArray) {
//                                                      body_index += 1;
//                                                      
//                                                      if (![activity_score_key isEqualToString:@"NAME"] && ![activity_score_key isEqualToString:@"AVATAR"]) {
//                                                          NSString *deploy_id = deploy_idArray[arrayIndex];
//                                                          
//                                                          NSString *activity_score = [self stringValue:tableBodyDict[activity_score_key]];
//                                                          NSString *took_test = (activity_score.length == 0) ? @"0" : @"1";
//                                                          activity_score = ((activity_score.length == 0)) ? @"0" : activity_score;
//                                                          activity_score = [self formatStringNumber:activity_score];
//                                                          
//                                                          NSString *exp_activity_key = [NSString stringWithFormat:@"EXP_%@", activity_score_key];
//                                                          NSString *exp_activity_score = [self stringValue:tableBodyDict[exp_activity_key]];
//                                                          exp_activity_score = ((activity_score.length == 0)) ? @"0" : exp_activity_score;
//                                                          exp_activity_score = [self formatStringNumber:exp_activity_score];
//                                                          
//                                                          NSString *rem_activity_key = [NSString stringWithFormat:@"REMARK_%@", activity_score_key];
//                                                          NSString *remark_activity = [self stringValue:tableBodyDict[rem_activity_key]];
//                                                          
//                                                          NSString *totalAndExpected = ([took_test isEqualToString:@"0"]) ? @"-" : [NSString stringWithFormat:@"%@ / %@", activity_score, exp_activity_score];
//                                                          
//                                                          NSManagedObject *act_mo = [NSEntityDescription insertNewObjectForEntityForName:@"GradeBook" inManagedObjectContext:gradeBookCtx];
//                                                          
//                                                          [act_mo setValue:user_id forKey:@"id"];
//                                                          [act_mo setValue:totalAndExpected forKey:@"cell_value"];
//                                                          [act_mo setValue:is_header forKey:@"is_header"];
//                                                          [act_mo setValue:@(body_index) forKey:@"index"];
//                                                          [act_mo setValue:@(section_body_index) forKey:@"section_index"];
//                                                          [act_mo setValue:[name lowercaseString] forKey:@"search_key"];
//                                                          [act_mo setValue:class_id forKey:@"class_id"];
//                                                          [act_mo setValue:name forKey:@"student_name"];
//                                                          [act_mo setValue:participant_id forKey:@"participant_id"];
//                                                          [act_mo setValue:deploy_id forKey:@"deploy_id"];
//                                                          [act_mo setValue:exp_activity_score forKey:@"exp_score"];
//                                                          [act_mo setValue:activity_score forKey:@"score"];
//                                                          [act_mo setValue:remark_activity forKey:@"remark_activity"];
//                                                          [act_mo setValue:took_test forKey:@"took_test"];
//                                                      }
//                                                      
//                                                      arrayIndex += 1;
//                                                  }
//                                                  
//                                                  body_index = 1;
//                                                  section_body_index += 1;
//                                              }
//                                              
//                                              [self saveTreeContext:gradeBookCtx];
//                                              
//                                              if (doneBlock) {
//                                                  doneBlock(YES);
//                                              }                  
//                                          }];
//                                      }
//                                  }];
//    
//    [task resume];
//}


#pragma mark - Avatar
- (void)requestProfileAvatars:(ResourceManagerListBlock)listBlock {
    
    NSURL *url = [self buildURL:kEndPointGetGenericAvatars];
    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:url body:nil];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
                                  {
                                      if (error) {
                                          NSLog(@"error %@", [error localizedDescription]);
                                          if (listBlock) {
                                              listBlock(nil);
                                          }
                                      }
                                      
                                      if (!error) {
                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
                                          if (dictionary != nil) {
                                              NSArray *records = dictionary[@"records"];
                                              
                                              if (records.count > 0) {
                                                  NSMutableArray *listOfAvatar = [NSMutableArray array];
                                                  
                                                  for (NSDictionary* dict in records) {
                                                      NSString *avatar_id = [self stringValue:dict[@"id"]];
                                                      NSString *name = [self stringValue:dict[@"name"]];
                                                      NSString *avatar_url = [self stringValue:dict[@"avatar_url"]];
                                                      NSString *is_deleted = [self stringValue:dict[@"is_deleted"]];
                                                      NSString *date_created = [self stringValue:dict[@"date_created"]];
                                                      NSString *date_modified = [self stringValue:dict[@"date_modified"]];
                                                      
                                                      
                                                      NSURL *avatar_nsurl = [self buildURL:[NSString stringWithFormat:@"/%@",avatar_url]];
                                                      
                                                      NSDictionary *avatar = @{@"avatar_id":avatar_id,
                                                                               @"name":name,
                                                                               @"avatar_url":avatar_nsurl,
                                                                               @"is_deleted":is_deleted,
                                                                               @"date_created":date_created,
                                                                               @"date_modified":date_modified};
                                                      
                                                      [listOfAvatar addObject:avatar];
                                                  }
                                                  
                                                  
                                                  if (listBlock) {
                                                      listBlock(listOfAvatar);
                                                  }
                                              } else {
                                                  if (listBlock) {
                                                      listBlock(nil);
                                                  }
                                              }
                                          } else {
                                              if (listBlock) {
                                                  listBlock(nil);
                                              }
                                          }
                                          
                                      }
                                  }];
    [task resume];
}

- (void)uploadAvatarImage:(UIImage *)image dataBlock:(ResourceManagerDataBlock)dataBlock {
    
    NSString *webQuery = [Utils buildUrl:[NSString stringWithFormat:kEndPointAvatarUploadV2, [self loginUser]]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:webQuery]];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:[Utils getSettingsSchoolCodeBase64] forHTTPHeaderField:@"code"];

    // boundary
    NSString *boundary = [self generateBoundaryString];
    
    // set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.8f);
    if(imageData==nil)  { return; }
    
    request.HTTPBody = [self createBodyWithBoundaryForAvatar:boundary data:imageData];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    [config setAllowsCellularAccess:YES];//USE CELLULAR DATA
    
    
    NSURLSession *dataSession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    NSURLSessionTask *task = [dataSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"error %@", [error localizedDescription]);
            
            
            if (dataBlock) {
                NSDictionary *errorDict = @{@"error":[error localizedDescription]};
                dataBlock(errorDict);
            }
        }
        
        if (!error) {
            NSDictionary *dictionary = [self parseResponseData:data];
            if (dictionary) {
                NSLog(@"response data : %@", dictionary);
                NSDictionary *records = dictionary[@"records"];
                if (records != nil) {
                    NSArray *data = records[@"data"];
                    
                    NSDictionary *firstData = data.firstObject;
                    
                    NSString *fileName = firstData[@"fileName"];
                    NSString *fileSize = firstData[@"fileSize"];
                    NSString *fileType = firstData[@"fileType"];
                    NSString *url = firstData[@"url"];
                    
                    NSDictionary *uploadData = @{@"fileName":fileName,
                                                @"fileSize":fileSize,
                                                @"fileType":fileType,
                                                @"url":url};
                    
                    if (dataBlock) {
                        dataBlock(uploadData);
                    }
                }
            }
        }//end
    }];
    [task resume];
}

- (NSData *)createBodyWithBoundaryForAvatar:(NSString *)boundary data:(NSData *)data {
    NSMutableData *httpBody = [NSMutableData data];
    
        NSString *fieldname = @"data";
        NSString *filename = @"my-image.jpg";
        NSString *mimetype  = @"image/jpg";
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldname, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}


#pragma mark - new playlist implementation
////////////////////////////////////////////////
////// TOTO- NEW PLAYLIST
////////////////////////////////////////////////

//- (void)requestListOfUserForSearchText:(NSString *)searchText listBlock:(ResourceManagerListBlock)listBlock {
//    
//    NSString *userid = [self loginUser];
//    
//    NSString *encodedSearchText = [self urlEncode:searchText];
//    
//    NSString *playlistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointPlayListSearchUser, encodedSearchText, userid]];
//    NSLog(@"play list path : %@", playlistPath);
//    NSURL *playlistURL = [NSURL URLWithString:playlistPath];
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:playlistURL body:nil];
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
//                                                      {
//          if (error) {
//              NSLog(@"error %@", [error localizedDescription]);
//              if (listBlock) {
//                  NSMutableArray *arrayOfUsers = [NSMutableArray array];
//                  listBlock(arrayOfUsers);
//              }
//          }
//          
//          if (!error) {
//              
//              NSDictionary *dictionary = [self parseResponseData:responsedata];
//              
//              if (dictionary) {
//                  
//                  if ([dictionary[@"records"] isKindOfClass:[NSArray class]]) {
//                  NSArray *records = dictionary[@"records"];
//                      if (records != nil) {
//                  if (records.count > 0) {
//                      
//                      NSMutableArray *arrayOfUsers = [NSMutableArray array];
//                      for (NSDictionary *record in records) {
//                          
//                                  NSString *context_id = [self stringValue:record[@"context_id"]];
//                                  NSString *name = [self stringValue:record[@"name"]];
//                                  NSString *group_level_id = [self stringValue:record[@"group_level_id"]];
//                                  NSString *category = [self stringValue:record[@"category"]];
//                                  
//                                  NSDictionary *userDict = @{@"context_id":context_id,
//                                                             @"name":name,
//                                                             @"group_level_id": group_level_id,
//                                                             @"category":category};
//                          
//                          [arrayOfUsers addObject:userDict];
//                      }
//                      
//                      if (listBlock) {
//                          listBlock(arrayOfUsers);
//                      }
//                  }
//              }
//          }
//              }
//          }
//          
//      }];
//    
//    [task resume];
//}
//
//- (void)requestListOfTeachers:(ResourceManagerListBlock)listBlock {
//    
//    NSString *userid = [self loginUser];
//    
//    NSString *playlistPath = [Utils buildUrl:[NSString stringWithFormat:kEndPointPlayListTeacherList, userid]];
//    NSLog(@"play list path : %@", playlistPath);
//    NSURL *playlistURL = [NSURL URLWithString:playlistPath];
//    NSMutableURLRequest *request = [self buildRequestWithMethod:@"GET" NSURL:playlistURL body:nil];
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *responsedata, NSURLResponse *response, NSError *error)
//                                  {
//                                      if (error) {
//                                          NSLog(@"error %@", [error localizedDescription]);
//                                          if (listBlock) {
//                                              listBlock(nil);
//                                          }
//                                      }
//                                      
//                                      if (!error) {
//                                          
//                                          NSDictionary *dictionary = [self parseResponseData:responsedata];
//                                          
//                                          if (dictionary) {
//                                              NSArray *records = dictionary[@"records"];
//                                              if (records.count > 0) {
//                                                  
//                                                  NSMutableArray *arrayOfUsers = [NSMutableArray array];
//                                                  for (NSDictionary *record in records) {
////                                                      {
////                                                          "context_id": "2",
////                                                          "name": "Web Teacher 1",
////                                                          "group_level_id": "4",
////                                                          "category": "Teacher"
////                                                      }
//                                                      NSString *context_id = [self stringValue:record[@"context_id"] ];
//                                                      NSString *name = [self stringValue:record[@"name"] ];
//                                                      NSString *group_level_id = [self stringValue:record[@"group_level_id"] ];
//                                                      NSString *category = [self stringValue:record[@"category"] ];
//                                                      
//                                                      NSDictionary *userDict = @{@"context_id":context_id,
//                                                                                 @"name":name,
//                                                                                 @"group_level_id":group_level_id,
//                                                                                 @"category":category};
//                                                      
//                                                      [arrayOfUsers addObject:userDict];
//                                                  }
//                                                  
//                                                  if (listBlock) {
//                                                      listBlock(arrayOfUsers);
//                                                  }
//                                              }
//                                          }
//                                      }
//                                      
//                                  }];
//    
//    [task resume];
//}
//
//- (NSString *)urlEncode:(id<NSObject>)value
//{
//    //make sure param is a string
//    if ([value isKindOfClass:[NSNumber class]]) {
//        value = [(NSNumber*)value stringValue];
//    }
//    
//    NSAssert([value isKindOfClass:[NSString class]], @"request parameters can be only of NSString or NSNumber classes. '%@' is of class %@.", value, [value class]);
//    
//    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
//                                                                                 NULL,
//                                                                                 (__bridge CFStringRef) value,
//                                                                                 NULL,
//                                                                                 (CFStringRef)@"!*'();:@&=+$,/?%#[]",
//                                                                                 kCFStringEncodingUTF8));
//                                                      }


#pragma mark - Parsing Helpers

- (NSManagedObject *)getNewEntity:(NSString *)entity attribute:(NSString *)attribute parameter:(NSString *)parameter context:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    if (parameter) {
        NSPredicate *predicate = [self predicateForKeyPath:attribute andValue:parameter];
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    
    if ([items count] > 0) {
        return [items lastObject];
    }
    
    return [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
}

- (BOOL)isItOkayToParseUsingThisResponseData:(NSDictionary *)response {
    NSLog(@"response: %@", response);
    
    if (response != nil) {
        NSDictionary *parsedmeta = response[@"_meta"];
        
        if (parsedmeta != nil) {
            if ([self doesKeyExistInDictionary:parsedmeta key:@"status"]) {
                NSString *status = [[self stringValue:parsedmeta[@"status"]] lowercaseString];
                
                if ([status isEqualToString:@"success"]) {
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

- (BOOL)doesKeyExistInDictionary:(NSDictionary *)d key:(NSString *)key {
    NSArray *keys = [d allKeys];
    
    for (NSString *k in keys) {
        if ([k isEqualToString:key]) {
            return YES;
        }
    }
    
    return NO;
}

- (NSString *)formatStringNumber:(NSString *)stringNumber {
    CGFloat floatNumber = [stringNumber floatValue];
    NSInteger intNumber = (NSInteger)floatNumber;
    
    if (floatNumber == intNumber) {
        stringNumber = [NSString stringWithFormat:@"%zd", intNumber];
    }
    else {
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        formatter.maximumFractionDigits = 2;
        formatter.roundingMode = NSNumberFormatterRoundUp;
        
        stringNumber = [formatter stringFromNumber:[NSNumber numberWithFloat:floatNumber]];
    }
    
    return stringNumber;
}

@end
