//
//  DocumentsSupport.h
//  V-Smart
//
//  Created by Earljon Hidalgo on 4/16/14.
//  Copyright (c) 2014 Vibe Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DocumentsSupport : NSObject

+ (DocumentsSupport *)sharedInstance;
+ (NSString *)documentsPath;
- (void)cancelAllOperations;
- (void)queueDocumentsSupport;
- (BOOL)handleOpenURL:(NSURL *) theURL;
@end

@interface DocumentsSupportOperation : NSOperation

extern NSString *const DocumentsUpdateNotification;
@end