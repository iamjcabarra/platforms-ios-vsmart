//
//  NTMNotebookActionMenuViewController.swift
//  V-Smart
//
//  Created by Julius Abarra on 06/09/2017.
//  Copyright © 2017 Vibe Technologies. All rights reserved.
//

import UIKit

class NTMNotebookActionMenuViewController: UITableViewController {
    
    fileprivate var actions = [String]()
    
    // MARK: - View Life Cycle
        
    override func viewDidLoad() {
        super.viewDidLoad()

        actions = ["Edit", "Duplicate", "Delete"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table View Data Source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actions.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        cell.textLabel?.text = actions[indexPath.row]
        return cell
    }
    
}
